<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Resetpassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $password;
    protected $email;
	protected $name;
	//thong tin web
	protected $link_web;
	protected $phone;
	protected $address;
	protected $link_fb;
	protected $link_logo;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $name, $password,$link_web,$phone,$address,$link_fb,$link_logo)
    {
        $this->password = $password;
        $this->email = $email;
        $this->name = $name;
		$this->link_web = $link_web;
		$this->phone = $phone;
		$this->address = $address;
		$this->link_fb = $link_fb;
		$this->link_logo = $link_logo;
	
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		
	   return $this->view('mail.sendmail')->with([
		'email' 	=> $this->email, 
		'password' 	=> $this->password,
		'name' 		=> $this->name,
		'link_web'  => $this->link_web, 
		'phone'     => $this->phone,
		'address'   => $this->address,
		'link_fb'   => $this->link_fb,
		'link_logo' => $this->link_logo,
	   ]);
    }
}

