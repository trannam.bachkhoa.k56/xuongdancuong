<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mail extends Mailable
{
    use Queueable, SerializesModels;

    protected $content;
    protected $sign;
    protected $contactId;
    protected $subjectContact;
    protected $emailManager;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content, $sign = '', $contactId = '', $subjectContact = '', $emailManager = '')
    {
        $this->content = $content;
        $this->sign = $sign;
        $this->contactId = $contactId;
        $this->subjectContact = $subjectContact;
        $this->emailManager = $emailManager;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.index')->with([
            'content' => $this->content, 
            'sign' => $this->sign, 
            'contact_id' => $this->contactId,
            'subjectContact' => $this->subjectContact,
            'emailManager' => $this->emailManager,
        ]);
    }
}
