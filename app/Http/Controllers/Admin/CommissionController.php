<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Domain;
use App\Entity\Template;
use App\Entity\Theme;
use App\Entity\Commission;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;

class CommissionController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;


            view()->share('menuTop', 'communicate');
            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commissions = Commission::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->orderBy('commission_id', 'asc')
            ->get();

        return View('admin.commission.list', compact('commissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.commission.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
          
            // insert to database
            $commission = new Commission();
            $commission->insert([
                'position' => $request->input('position'),
                'salary' => str_replace('.', '', $request->input('salary')),
                'sub_salary' => str_replace('.', '', $request->input('sub_salary')),
                'security' => str_replace('.', '', $request->input('security')),
                'revenue' => str_replace('.', '', $request->input('revenue')),
                'commission' => str_replace('.', '', $request->input('commission')),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime()
            ]);
        } catch (\Exception $e) {
            Log::error('http->admin->commissionController->store: Lỗi thêm mới cơ chế hoa hồng');
        } finally {
            return redirect('admin/commission');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(Template $template)
    {
        return redirect('admin/commission');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(Commission $commission)
    {
        if ($commission->theme_code != $this->themeCode || $commission->user_email != $this->emailUser) {
            return redirect('admin/commission');
        }

        return View('admin.commission.edit', compact('commission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commission $commission)
    {
        try {
            if ($commission->theme_code != $this->themeCode || $commission->user_email != $this->emailUser) {
                return redirect('admin/commission');
            }


            $commission->update([
                'position' => $request->input('position'),
                'salary' => str_replace('.', '', $request->input('salary')),
                'sub_salary' => str_replace('.', '', $request->input('sub_salary')),
                'security' => str_replace('.', '', $request->input('security')),
                'revenue' => str_replace('.', '', $request->input('revenue')),
                'commission' => str_replace('.', '', $request->input('commission')),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'updated_at' => new \DateTime()
            ]);

        } catch (\Exception $e) {
            Log::error('http->admin->CommissionController->update: Lỗi chỉnh sửa cơ chế hoa hồng');
        } finally {
            return redirect('admin/commission');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commission $commission)
    {
        try {
            if ($commission->theme_code != $this->themeCode || $commission->user_email != $this->emailUser) {
                return redirect('admin/templates');
            }

            Commission::where('commission_id', $commission->commission_id)->delete();
        } catch (\Exception $e) {
            Log::error('http->admin->CommissionController->destroy: Lỗi xóa cơ chế hoa hồng');
        } finally {
            return redirect('admin/commission');
        }
    }
}
