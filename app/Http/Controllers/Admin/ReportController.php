<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 11/30/2017
 * Time: 2:33 PM
 */

namespace App\Http\Controllers\Admin;


use App\Entity\CampaignCustomerContact;
use App\Entity\Commission;
use App\Entity\DomainUser;
use App\Entity\Goal;
use App\Entity\Notification;
use App\Entity\OrderCustomer;
use App\Entity\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReportController extends AdminController
{
    public function allReport(){
        try {
            $reports = Notification::orderBy('notify_id', 'desc')
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->get();
            $notification = new Notification();
            $notification->where('status',0)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->update([
                'status' => 1
            ]);
        } catch (\Exception $e) {
             Log::error('http->admin->ReportController->allReport: hiển thị tất cả thông báo.');
        } finally {
            return view('admin.report.index', compact('reports'));
        }
    }

    public function seenNotification() {
        try {
            $notification = new Notification();
            $notification->where('status',0)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->update([
                'status' => 1
            ]);
        } catch (\Exception $e) {
            Log::error('http->admin->ReportController->seenNotification: đã xem thông báo');

        } finally {
            return response([
                'status' => 200,
            ])->header('Content-Type', 'text/plain');
        }

    }
    public function readNotification(Request $request){
        try {
            // lấy ra id truyền lên
            $id = $request->input('id');
            // đổi trạng thái id tương ứng là đã đọc
            $notifications = new Notification();
            $notifications->where('notify_id', $id)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)
                ->update(['status' => 2]);
        } catch (\Exception $e) {
            Log::error('http->admin->ReportController->readNotification: đã đọc thông báo');
        } finally {
            // tra ve ket qua
            return response([
                'status' => 200,
            ])->header('Content-Type', 'text/plain');
        }
    }

    public function pushNotification(){
        try{
            $pushNotifies = Notification::where('status', 0)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->get();
            if (!empty($pushNotify)){
                $message = '';
                foreach ($pushNotifies as $notify) {
                    $message .= $notify->title. ': '.$notify->content;
                }

                return response([
                    'status' => 200,
                    'message' => $message,
                    'url' => route('report')
                ])->header('Content-Type', 'text/plain');
            }

            return response([
                'status' => 500,
                'message' => ''
            ])->header('Content-Type', 'text/plain');
        } catch (\Exception $e){
            Log::error('http->admin->ReportController->pushNotification: Thông báo đẩy');

            return response([
                'status' => 500,
            ])->header('Content-Type', 'text/plain');
        }
    }

    public function reportEmployee (Request $request) {
        view()->share('menuTop', 'communicate');
        // lấy ra tháng hiện tại

        $dateSelect = $this->getMonth($request);
        $month = $dateSelect['month'];
        $year = $dateSelect['year'];

        // lấy ra user_id
        if (empty($this->userIdPartner)) {
            $userId = $request->has('user_id') ? $request->input('user_id') : Auth::user()->id;
        } else {
            $userId = $this->userIdPartner;
        }

        $user = User::where('id', $userId)->first();

        // lấy ra mục tiêu kpi
        $goal = Goal::getGoalWithUserIdMonth($userId, $month, $year);

        // lấy ra tổng số đơn hàng / 1 user_id
        $orderCustomerCountOrder = OrderCustomer::orderCustomerCountOrderMonthYear($userId, $month, $year);

        // lấy ra các thành viên trong hệ thống
        $employees = DomainUser::getEmployees($this->domainUser->domain_id);

        // lấy ra tổng số khách hàng / 1 user_id
        $contactCount = CampaignCustomerContact::countCustomerContactUserIdMonthYear($userId, $month, $year);

        // lấy ra tổng số thanh toán  / 1 user_id
        $orderCustomer = OrderCustomer::where('user_id', $userId)
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year);

        $orderCustomerCountPayment = $orderCustomer
            ->where('payment', '>' ,0)
            ->count();

        // lấy ra tổng doanh số / 1 user_id
        $orderCustomer = OrderCustomer::where('user_id', $userId)
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year);
        $orderCustomerRevenue = $orderCustomer->sum('total_price');

        // tổng thanh toán:
        $orderCustomerPayment = $orderCustomer->sum('payment');

        // lấy ra tổng số trao đổi / 1 user_id

        // lấy ra số hẹn gặp / 1 user_id

        // lấy ra tổng số vốn = vốn sản phẩm + lương + trách nhiệm nv + hoa hồng nv
        $costOrder = $orderCustomer->sum('cost');

        $partner = DomainUser::join('commission', 'commission.commission_id', 'domain_user.commission_id')
            ->select(
                'commission.*'
            )
            ->where('domain_user.user_id', $userId)
            ->first();

        $orderCustomerUsers = OrderCustomer::join('contact', 'contact.contact_id', 'order_customer.contact_id')
            ->join('users', 'users.id', 'order_customer.user_id')
            ->select(
                'users.name as user_name',
                'contact.name as contact_name',
                'order_customer.*'
            )
            ->where('order_customer.user_id', $userId)
            ->where('order_customer.theme_code', $this->themeCode)
            ->where('order_customer.user_email', $this->emailUser)
            ->whereMonth('order_customer.created_at', $month)
            ->whereYear('order_customer.created_at', $year)
            ->get();

        return view ('admin.report.report_employee', compact(
            'orderCustomerCountOrder',
            'orderCustomerCountPayment',
            'orderCustomerRevenue',
            'orderCustomerPayment',
            'costOrder',
            'partner',
            'goal',
            'contactCount',
            'user',
            'employees',
            'dateSelect',
            'orderCustomerUsers'
        ));
    }

    private function getMonth ($request) {
        if ( ($request->has('month_report') && $request->input('month_report') == 'last_month' )) {
            $month =  date("m", strtotime("first day of previous month"));
            $year = date("Y", strtotime("first day of previous month"));
            
            return array(
                'month' => $month,
                'year' => $year
            );
        }

        if ( $request->has('month_report')
            && $request->input('month_report') == 'this_year') {
            $month =  date("m");
            $year = date("Y");

            return array(
                'month' => $month,
                'year' => $year
            );
        }

        if ( $request->has('month_report')
            && $request->input('month_report') == 'last_year') {
            //$month =  date("m",strtotime("-1 year"));
            $year = date("Y",strtotime("-1 year"));

            return array(
                'month' => 12,
                'year' => $year
            );
        }

        if ( $request->has('month_report')
            && $request->input('month_report') != 'this_month'
            && $request->input('month_report') != 'last_month')

        {

            $month =  date_format(date_create($request->input('month_report')), "m");
            $year =  date_format(date_create($request->input('month_report')), "Y");

            return array(
                'month' => $month,
                'year' => $year
            );
        }

        return array(
            'month' => date('m'),
            'year' => date('Y')
        );
    }

    public function revenue(Request $request) {
        view()->share('menuTop', 'communicate');

        $dateStart = $request->has('date_start') ? $request->input('date_start') : '2020-01-01';
        $dateEnd = $request->has('date_end') ? $request->input('date_end') : '2020-12-31';
        
        // lấy ra các thành viên trong hệ thống
        $employees = DomainUser::getEmployees($this->domainUser->domain_id);

        // lấy ra tổng doanh số / 1 user_id
        $orderCustomer = OrderCustomer::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->whereDate('created_at', '>=', $dateStart)
            ->whereDate('created_at', '<=' , $dateEnd);

        $orderCustomerRevenue = $orderCustomer->sum('total_price');

        // tổng thanh toán:
        $orderCustomerPayment = $orderCustomer->sum('payment');

        // lấy ra tổng số vốn = vốn sản phẩm + lương + trách nhiệm nv + hoa hồng nv
        $costOrder = $orderCustomer->sum('cost');

        $partner = DomainUser::join('commission', 'commission.commission_id', 'domain_user.commission_id')
            ->join('users', 'users.id', 'users.user_id')
            ->select(
                'commission.*',
                'users.name'
            )
            ->get();

        return view ('admin.report.report_revenue', compact(
            'employees',
            'orderCustomerRevenue',
            'orderCustomerPayment',
            'costOrder',
            'partner',
            'dateStart',
            'dateEnd'
        ));
    }
}
