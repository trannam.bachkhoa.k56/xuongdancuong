<?php

namespace App\Http\Controllers\Admin;

use App\Entity\TaskHelp;
use App\Mobile_Detect;
use App\Entity\User;
use App\Ultility\Error;
use Illuminate\Http\Request;
use App\Ultility\Ultility;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;

class UserController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isManager($this->role) && !User::isCreater($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $user = Auth::user();
            if (User::isCreater($user->role)) {
                $users = User::orderBy('id', 'desc')
                    ->get();
            } else {
                $users = User::orderBy('id', 'desc')
                    ->where('theme_code', $this->themeCode)
                    ->where('user_email', $this->emailUser)
					->orWhere('email', $this->emailUser)->get();
            }
            return view('admin.user.list', compact('users'));
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi hiển thị thành viên: dữ liệu không hợp lệ.');
            Log::error('http->admin->UserController->index: Lỗi xảy ra trong quá trình hiển thị thành viên');

            return redirect('admin/home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'email' => 'required | unique:users',
            ]);

            // if validation fail return error
            if ($validation->fails()) {
                return redirect('admin/users/create')
                    ->withErrors($validation)
                    ->withInput();
            }

            // insert to database
            $role = $request->input('role');
            $user = new User();
            $userId = $user->insertGetId([
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'phone' => $request->input('phone'),
                'image' => $request->input('image'),
                'name' => $request->input('name'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser
            ]);

            if (User::isCreater(Auth::user()->role) && ($role == 4) ) {
                $user->update([
                    'role' => empty($role) ? 1 : $role,
                    'vip' => $request->input('vip')
                ])->where('id', $userId);
            }

            if ( (User::isCreater(Auth::user()->role) || User::isManager(Auth::user()->role))
                && $role != 4 ) {
                $user->where('id', $userId)->update([
                    'role' => empty($role) ? 1 : $role,
                ]);
            }


        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi thêm mới thành viên: dữ liệu không hợp lệ.');
            Log::error('http->admin->UserController->store: Lỗi xảy ra trong quá trình thêm mới thành viên');
        } finally {
            return redirect('admin/users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('admin/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try  {
            $validation = Validator::make($request->all(), [
                'email' =>  Rule::unique('users')->ignore($user->id, 'id'),
            ]);

            // if validation fail return error
            if ($validation->fails()) {
                return redirect(route('users.edit', ['id' => $user->id]))
                    ->withErrors($validation)
                    ->withInput();
            }

            $isChangePassword = $request->input('is_change_password');
            if ($isChangePassword == 1) {
                $user->update([
                    'password' =>  bcrypt($request->input('password'))
                ]);
            }
            // insert to database
            $role = $request->input('role');
            $user->update([
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
                'image' => $request->input('image'),
                'name' => $request->input('name'),
                'bussiness' => $request->input('bussiness'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser
            ]);
            //Lấy gói email và xử lý thêm email
            $email_remaining = 0;

            $package_mail = $request->input('package_mail');
            switch ($package_mail) {
                case '1':
                    # code...
                    $email_remaining = 5000;

                    break;
                case '2':
                    # code...
                    $email_remaining = 20000;
                    break;
                
                default:
                    # code...
                    break;
            }
      
            //lấy thời gian nhập vào , và thời gian thời gian hiện tại để xử lý cộng ngày 
            $time_email = $request->input('time_email');
            $time_now = Carbon::now();
            switch ($time_email) {
                case '1_month':
                    # code...
                    $time_now = Carbon::now()->addMonth();
                    break;
                case '6_month':
                    # code...
                    $time_now = Carbon::now()->addMonth(6);
                    break;
                case '1_year':
                    # code...
                    $time_now = Carbon::now()->addYear();
                    break;
                case '2_year':
                    # code...
                    $time_now = Carbon::now()->addYears(2);
                    break;
                case '3_year':
                    # code...
                    $time_now = Carbon::now()->addYears(3);
                    break;
                default:
                    # code...
                    break;
            }

            if (User::isCreater(Auth::user()->role)) {
                 $user->update([
                    //'role' => empty($role) ? 0 : $role,
                     'vip' => $request->input('vip'),
                     'package_mail' => $package_mail,
                     'email_remaining' => $email_remaining,
                     'time_email' => $time_now,
                 ]);
              
            }

            if ( (User::isCreater(Auth::user()->role))
                && $role != 4 ) {
                $user->update([
                    'role' => empty($role) ? 1 : $role,
                ]);
            }

        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi chỉnh sửa thành viên: dữ liệu không hợp lệ.');
            Log::error('http->admin->UserController->update: Lỗi xảy ra trong quá trình chỉnh sửa thành viên');
        } finally {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $userLogin = Auth::user();
            if ($userLogin->role == 4 ) {
                User::where('id', $user->id)->delete();

                return redirect('admin/users');
            }

            if ($user->theme_code != $this->themeCode|| $user->user_email != $this->emailUser) {
                return redirect('admin/users');
            }

            User::where('id', $user->id)->delete();

            return redirect('admin/users');
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xóa thành viên: dữ liệu không hợp lệ.');
            Log::error('http->admin->UserController->destroy: Lỗi xảy ra trong quá trình xóa thành viên');
        }
    }

    public function updateStatusHelpGotIt(Request $request) {
        User::where('id', Auth::user()->id)
            ->update([
                'is_help' => 1
            ]);

        return redirect()->back();
    }

    public function updateEnableHelp () {
        User::where('id', Auth::user()->id)
            ->update([
                'is_help' => 0
            ]);

        return redirect('/admin/home');
    }
	
	public function saveAccessTokenFacebook (Request $request) {
		$accessToken = $request->input('access_token');
		
		User::where('id', Auth::user()->id)
            ->update([
                'accesstoken' => $accessToken
            ]);
			
			 // tra ve ket qua
		return response([
             'status' => 200
         ])->header('Content-Type', 'text/plain');
	}

    public function checkTask (Request $request) {
        $postId = $request->input('post_id');
        $type = $request->input('type');
        $userId = Auth::id();

        TaskHelp::insert([
            'post_id' => $postId,
            'user_id' => $userId,
            'type' => $type,
            'created_at' => new \DateTime()
        ]);

        // tra ve ket qua
        return response([
            'status' => 200
        ])->header('Content-Type', 'text/plain');
    }


}
