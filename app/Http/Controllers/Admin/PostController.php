<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Comment;
use App\Entity\Domain;
use App\Entity\Input;
use App\Entity\Post;
use App\Entity\PostFacebook;
use App\Entity\Template;
use App\Entity\Theme;
use App\Entity\OptinForm;
use App\Entity\TypeInput;
use App\Entity\User;
use App\Facebook\Fanpage;
use App\Ultility\CheckPermission;
use App\Ultility\Error;
use Illuminate\Support\Facades\Log;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;

class PostController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'websites');

            return $next($request);
        });
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('post_type', 'post')->select('posts.*')
        ->where('user_email', $this->emailUser)
        ->where('theme_code', $this->themeCode)
        ->get();

        return View('admin.post.list',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $messageErrorVip = CheckPermission::checkPosts(Auth::user());
            if (!empty($messageErrorVip)) {
                return redirect(route('posts.index', ['messageErrorVip' => $messageErrorVip]));
            }

            $category = new Category();
            $categories =$category->getCategory($this->themeCode, $this->emailUser);
            $templates = Template::getTemplate();
            // lọc bỏ những trường mà ko sử dụng trong post
            $typeInputDatabase = TypeInput::orderBy('type_input_id')
//                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->get();
            $typeInputs = array();
            foreach($typeInputDatabase as $typeInput) {
                $token = explode(',', $typeInput->post_used);
                if (in_array('post', $token)) {
                    $typeInputs[] = $typeInput;
                }
            }

            $productList = Post::join('products', 'products.post_id', '=', 'posts.post_id')
                ->select(
                    'products.product_id',
                    'posts.*'
                )
                ->where('post_type', 'product')->orderBy('posts.post_id', 'desc')
                ->where('posts.user_email', $this->emailUser)
                ->where('posts.theme_code', $this->themeCode)->get();

            $forms = OptinForm::where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)
                ->get();



            return view('admin.post.add', compact('categories', 'templates', 'typeInputs', 'productList','forms'));
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi tạo mới bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->PostController->create: Lỗi xảy ra trong quá trình tạo mới bài viết');

            return redirect('admin/home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            // lấy user id
            $userId = Auth::user()->id;

            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }

            // insert to database
            if (!empty($request->input('parents'))) {
                $categoriParents = Category::whereIn('category_id', $request->input('parents'))
                    ->where('user_email', $this->emailUser)
                    ->where('theme_code', $this->themeCode)->get();
                $categories = array();
                foreach ($categoriParents as $cate) {
                    $categories[] =  $cate->title;
                }
            }

            $post = new Post();
            $postId = $post->insertGetId([
                'title' => $request->input('title'),
                'post_type' => 'post',
                'template' =>  $request->input('template'),
                'description' => $request->input('description'),
                'tags' => $request->input('tags'),
                'image' =>  $request->input('image'),
                'content' =>  $request->input('content'),
                'visiable' => 0,
                'user_email' => $this->emailUser,
                'theme_code' => $this->themeCode,
                'category_string' => !empty($categories) ? implode(',', $categories) : '',
                'meta_title' => $request->input('meta_title'),
                'meta_description' => $request->input('meta_description'),
                'meta_keyword' => $request->input('meta_keyword'),
                'product_list' => !empty($request->input('product_list')) ? implode(',', $request->input('product_list')) : '',
                'created_at' => new \Datetime(),
                'updated_at' => new \Datetime(),
                'form_id' => $request->input('form_id'),
            ]);

            // insert slug
            $postWithSlug = $post->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)
				->where('slug', $slug)->first();
            if (empty($postWithSlug)) {
                $post->where('post_id', '=', $postId)
                    ->where('user_email', $this->emailUser)
                    ->where('theme_code', $this->themeCode)
                    ->update([
                        'slug' => $slug
                    ]);
            } else {
                $post->where('post_id', '=', $postId)
                    ->where('user_email', $this->emailUser)
                    ->where('theme_code', $this->themeCode)
                    ->update([
                        'slug' => $slug.'-'.$postId
                    ]);
            }

            // insert danh mục cha
            $categoryPost = new CategoryPost();
            if (!empty($request->input('parents'))) {
                foreach($request->input('parents') as $parent) {
                    $categoryPost->insert([
                        'category_id' => $parent,
                        'post_id' => $postId,
                        'theme_code' => $this->themeCode,
                        'user_email' => $this->emailUser
                    ]);
                }
            }

            // insert input
            $typeInputDatabase = TypeInput::orderBy('type_input_id')
                ->where('theme_code', $this->themeCode)->get();
				
            foreach($typeInputDatabase as $typeInput) {
                $token = explode(',', $typeInput->post_used);
                if (in_array('post', $token)) {
                    $contentInput =  $request->input($typeInput->slug);
                    if(!in_array($typeInput->type_input, array('one_line', 'multi_line', 'image', 'editor', 'image_list'), true) && strpos($typeInput->type_input, 'listMultil') >= 0) {
                        $contentInput = ( !empty($contentInput) && count($contentInput) >= 1) ? implode(',', $contentInput) : $contentInput;
                    }
                    $input = new Input();
                    $input->insert([
                        'type_input_slug' => $typeInput->slug,
                        'content' => $contentInput,
                        'post_id' => $postId,
                        'theme_code' => $this->themeCode,
                        'user_email' => $this->emailUser
                    ]);
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi tạo mới bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->PostController->store: Lỗi xảy ra trong quá trình tạo mới bài viết');
        } finally {
            return redirect('admin/posts');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        try {
            $postExist = Post::where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)
                ->where('post_id', $post->post_id)->exists();
            if (!$postExist) {
                return redirect('admin/posts');
            }

            $category = new Category();
            $categories =$category->getCategory($this->themeCode, $this->emailUser);
            $templates = Template::orderBy('template_id')
                ->where('theme_code', $this->themeCode)->get();
            // lọc bỏ những trường mà ko sử dụng trong post
            $typeInputDatabase = TypeInput::orderBy('type_input_id')
                ->where('theme_code', $this->themeCode)
                ->get();
            $typeInputs = array();
            foreach($typeInputDatabase as $typeInput) {
                $token = explode(',', $typeInput->post_used);
                if (in_array('post', $token)) {
                    $typeInputs[] = $typeInput;
                    $post[$typeInput->slug] = Input::getPostMeta($typeInput->slug, $post->post_id);
                }
            }
            $categoryPosts = CategoryPost::where('post_id', $post->post_id)
                ->get();
            $categoryPost = array();
            foreach($categoryPosts as $cate ) {
                $categoryPost[] = $cate->category_id;
            }

            $productList = Post::join('products', 'products.post_id', '=', 'posts.post_id')
                ->select(
                    'products.product_id',
                    'posts.*'
                )
                ->where('post_type', 'product')
                ->orderBy('posts.post_id', 'desc')
                ->where('posts.user_email', $this->emailUser)
                ->where('posts.theme_code', $this->themeCode)
                ->get();
                
            $forms = OptinForm::where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)
                ->get();

            return view('admin.post.edit', compact(
                'categories',
                'templates',
                'typeInputs',
                'post',
                'categoryPost',
                'productList',
                'forms'
            ));
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi chỉnh sửa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->PostController->edit: Lỗi xảy ra trong quá trình chỉnh sửa bài viết');

            return redirect('admin/home');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        try {
            DB::beginTransaction();
            $postExist = Post::where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)
                ->where('post_id', $post->post_id)->exists();
            if (!$postExist) {
                return redirect('admin/posts');
            }

            if ($post->user_email != $this->emailUser || $post->theme_code != $this->themeCode) {
                return redirect('admin/posts');
            }

            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }
            // update to database
            if (!empty($request->input('parents'))) {
                $categoriParents = Category::whereIn('category_id', $request->input('parents'))->get();
                $categories = array();
                foreach ($categoriParents as $cate) {
                    $categories[] =  $cate->title;
                }
            }
            $post->update([
                'title' => $request->input('title'),
                'post_type' => 'post',
                'template' =>  $request->input('template'),
                'description' => $request->input('description'),
                'tags' => $request->input('tags'),
                'image' =>  $request->input('image'),
                'content' =>  $request->input('content'),
                'user_email' => $this->emailUser,
                'theme_code' => $this->themeCode,
                'visiable' => 0,
                'category_string' => !empty($categories) ? implode(',', $categories) : '',
                'meta_title' => $request->input('meta_title'),
                'meta_description' => $request->input('meta_description'),
                'meta_keyword' => $request->input('meta_keyword'),
                'product_list' => !empty($request->input('product_list')) ? implode(',', $request->input('product_list')) : '',
                'updated_at' => new \Datetime(),
                'form_id' => $request->input('form_id'),
            ]);

            // insert slug
            $postWithSlug = Post::where('slug', $slug)
                ->where('post_id', '!=', $post->post_id)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->first();
            if (empty($postWithSlug)) {
                $post->where('post_id', $post->post_id)
                    ->update([
                        'slug' => $slug
                    ]);
            } else {
                $post->where('post_id', $post->post_id)
                    ->update([
                        'slug' => $slug.'-'.$post->post_id
                    ]);
            }

            // insert danh mục cha
            $categoryPost = new CategoryPost();
            $categoryPost->where('post_id', $post->post_id)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)
                ->delete();
            if (!empty($request->input('parents'))) {
                foreach($request->input('parents') as $parent) {
                    $categoryPost->insert([
                        'category_id' => $parent,
                        'post_id' => $post->post_id,
                        'theme_code' => $this->themeCode,
                        'user_email' => $this->emailUser
                    ]);
                }
            }

            // insert input
            $typeInputDatabase = TypeInput::orderBy('type_input_id')
//                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->get();
            $input = new Input();
            $input->updateInput($this->themeCode, $this->emailUser, $typeInputDatabase, $request, $post->post_id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi chỉnh sửa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->PostController->update: Lỗi xảy ra trong quá trình chỉnh sửa bài viết');
        } finally {
            return redirect('admin/posts');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        try {
            DB::beginTransaction();
            $this->removeItem($post->post_id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi xóa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->PostController->destroy: Lỗi xảy ra trong quá trình xóa bài viết');
        } finally {
            return redirect('admin/posts');
        }
    }

    public function visiable(Request $request) {
        $visiable = $request->input('visiable');
        $postId = $request->input('post_id');
        
        Post::where('post_id', $postId)->update([
            'visiable' => $visiable
        ])->where('user_email', $this->emailUser)
        ->where('theme_code', $this->themeCode);

        return response([
            'status' => 200
        ])->header('Content-Type', 'text/plain');
    }
    
    public function anyDatatables(Request $request) {
        $postdatatables = Post::where('post_type', 'post')->select('posts.*')
        ->where('user_email', $this->emailUser)
        ->where('theme_code', $this->themeCode);
		
        return Datatables::of($postdatatables)
           ->addColumn('action', function($post) {
               $string = '<input type="checkbox" class="flat-red" onclick="return visiablePost(this);" value="'.$post->post_id.'" '.( ($post->visiable == 0 || $post->visiable == null ) ? 'checked' : '' ).'/> Hiện ';
               $string .=  '<a href="'.route('posts.edit', ['post_id' => $post->post_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
               $string .= '<a  href="/admin/posts/'.$post->post_id.'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
               return $string;
           })
           
            ->orderColumn('post_id', 'post_id desc')
           ->make(true);
    }

    public function deleteAll(Request $request) {
        $idDeletes = $request->input('id_delete');

        $idDeletesToken = explode(',', $idDeletes);
        foreach ($idDeletesToken as $idDelete) {
            if (!empty($idDelete)) {
                $this->removeItem($idDelete);
            }
        }

        return redirect('admin/posts');
    }

    private function removeItem($postId) {
        try {
            DB::beginTransaction();
            CategoryPost::where('post_id', $postId)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->delete();

            Comment::where('post_id', $postId)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->delete();

            Input::where('post_id', $postId)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->delete();

            $postModel = new Post();
            $postModel->where('post_id', $postId)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi xóa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->PostController->destroy: Lỗi xảy ra trong quá trình xóa bài viết');
        } finally {
            return redirect('admin/posts');
        }


    }
}
