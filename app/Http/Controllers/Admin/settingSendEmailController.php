<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 12/19/2018
 * Time: 3:48 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\Contact;
use Illuminate\Support\Facades\Auth;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Entity\GroupMail;
use App\Entity\EmailSetting;
use App\Ultility\Error;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Expr\Array_;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class settingSendEmailController extends AdminController
{
	protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'cskh');

            return $next($request);
        });

    }


    public function index(){
		if(Auth::user()->email_remaining < 0){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        return view('admin.settingSendEmail.index');
    }

    public function showContact(Request $request){
		if(Auth::user()->email_remaining < 0){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $contact = new Contact();
        $contacts = $contact->where('theme_code', $this->themeCode)
                    ->where('user_email', $this->emailUser)
                    ->where('tag', 'LIKE', '%' . $request->input('tag') . '%')
                    ->get(['tag']);
        $contactArray = array();
        foreach ($contacts as $contact){
            $contactArray[] = $contact->tag;
        }
        return $contactArray;
    }

    public function anyDatatables() {
        $emails = EmailSetting::select(
                'emailsetting.id',
                'emailsetting.subject',
                // 'group_mail.name',
                'emailsetting.content',
                'emailsetting.updated_at'
            )
            ->where('emailsetting.theme_code', $this->themeCode)
            ->where('emailsetting.user_email', $this->emailUser)
            ;


        return Datatables::of($emails)
            ->addColumn('action', function($email) {
                $string = '<input type="checkbox" class="flat-red" onclick="return visiablePost(this);" value="'.$email->post_id.'" '.( ($email->visiable == 0 || $email->visiable == null ) ? 'checked' : '' ).'/> Hiện ';

                $string .=  '<a href="'.route('edit_mail', ['id' => $email->id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('destroy_mail', ['id' => $email->id]).'" class="btn btn-danger btnDelete" 
                            >
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';

                return $string;

            })
            ->orderColumn('emailsetting.id', 'emailsetting.id desc')
            ->make(true);
            // <a  href="'.route('destroy_mail', ['id' => $email->id]).'" class="btn btn-danger btnDelete" 
            //                 data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
            //                    <i class="fa fa-trash-o" aria-hidden="true"></i>
            //                 </a>

    }


      public function edit($id,Request $request){
		if(Auth::user()->email_remaining < 0){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
            $email = EmailSetting::where('emailsetting.id',$id)
            ->where('emailsetting.theme_code', $this->themeCode)
            ->where('emailsetting.user_email', $this->emailUser)
            ->select(
                    'emailsetting.*'
                )
            ->first();

        return view('admin.settingSendEmail.edit',compact('email'));

    }

     public function update($id ,Request $request){
        try {
		if(Auth::user()->email_remaining < 0){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        DB::beginTransaction();

        $email = EmailSetting::where('id',$id)
        ->update([
            'subject' => $request->input('subject'),
            'content'=> $request->input('content'),
            'group_id' => $request->input('group'),
            //'note' => $request->input('note'),
            'type' => $request->input('type'),
            // 1 là đặt lịch gửi  ----------- 2 là chọn thời gian sau khi đăng ký bao nhiêu ngày để gửi 
            'date_send' => $request->input('date_send'),
            'time_send' => $request->input('time_send'),
            'date_delay' => $request->input('date_delay'),
            'delay_time' => $request->input('delay_time'),
            //'status' => 0 ,
        ]);
        
        DB::commit();


        } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi chỉnh sửa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->PostController->update: Lỗi xảy ra trong quá trình chỉnh sửa bài viết');
        } finally {
            return redirect(route('settingSendEmail'));
        }

     }

      public function destroy($id){

        DB::beginTransaction();
        $email = EmailSetting::where('id',$id)
        ->first()
        ->delete();
   
        DB::commit();
      

        return redirect(route('settingSendEmail'));
    }


     public function showAdd(){
		if(Auth::user()->email_remaining < 0){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
       $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->get();

		return view('admin.settingSendEmail.add',compact('groupMails'));
	}	

    public function store(Request $request){
    //try {
		if(Auth::user()->email_remaining < 0){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $emailSettingModel = new EmailSetting();

        $email = $emailSettingModel->insertGetId([
            'subject' => $request->input('subject'),
            'content'=> $request->input('content'),
            'group_id' => $request->input('group'),
            //'note' => $request->input('note'),
            'type' => $request->input('type'),
            // 1 là đặt lịch gửi  ----------- 2 là chọn thời gian sau khi đăng ký bao nhiêu ngày để gửi 
            'date_send' => $request->input('date_send'),
            'time_send' => $request->input('time_send'),
            'date_delay' => $request->input('date_delay'),
            'delay_time' => $request->input('delay_time'),
            'status' => 0 ,
            'theme_code'=> $this->themeCode,
            'user_email'=> $this->emailUser,
            'updated_at' =>  new \DateTime(),
            'created_at' => new \DateTime(),
        ]);

            return redirect(route('settingSendEmail'))->with('success','Thêm mới thành công');

        // }
          // catch (\Exception $e) {
          //       Error::setErrorMessage('Lỗi xảy ra khi hiển thị đăng ký email kh: dữ liệu không hợp lệ.');
          //       Log::error('http->admin->SubcribeEmailController->index: Lỗi xảy ra trong quá trình hiển thị đăng ký email khách hàng');

          //       return redirect(route('settingSendEmail'));
          //   }
    }   
}