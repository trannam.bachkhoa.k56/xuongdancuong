<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Domain;
use App\Entity\DomainUser;
use App\Entity\Template;
use App\Entity\Theme;
use App\Entity\Goal;
use App\Entity\Commission;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;

class GoalController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;


            view()->share('menuTop', 'communicate');
            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $goals = Goal::join('users', 'users.id', 'goals.user_id')
            ->select(
                'users.name',
                'users.id',
                'goals.*'
            )
             ->where('goals.theme_code', $this->themeCode)
            ->where('goals.user_email', $this->emailUser)
            ->orderBy('goal_id', 'asc');

        // là cộng sự đăng nhập lấy ra tất cả chiến dịch liên quan đến cộng sự đó.
        if (!empty($this->userIdPartner)) {
            $goals = $goals->where('goals.user_id', $this->userIdPartner);
        }

        $goals = $goals->get();

        return View('admin.goal.list', compact('goals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = DomainUser::join('users', 'users.id', 'domain_user.user_id')
            ->where('domain_id', $this->domainUser->domain_id)->get();

        return View('admin.goal.add', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // insert to database
            $goal = new Goal();
                $goal->insert([
                'title' => $request->input('title'),
                'user_id' => $request->input('user_id'),
                'revenue' => str_replace('.', '', $request->input('revenue')),
                'customers' => $request->input('customers'),
                'orders' => $request->input('orders'),
                'notes' => $request->input('notes'),
                'purchases' => $request->input('purchases'),
                'reminders' => $request->input('reminders'),
                'calls' => $request->input('calls'),
                'month' => new \DateTime($request->input('month')),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime()
            ]);
        } catch (\Exception $e) {
            Log::error('http->admin->commissionController->store: Lỗi thêm mới cơ chế hoa hồng');
        } finally {
            return redirect('admin/goals');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(Goal $goal)
    {
        return redirect('admin/goals');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(Goal $goal)
    {
        if ($goal->theme_code != $this->themeCode || $goal->user_email != $this->emailUser) {
            return redirect('admin/commission');
        }

        $employees = DomainUser::join('users', 'users.id', 'domain_user.user_id')
            ->where('domain_id', $this->domainUser->domain_id)->get();

        return View('admin.goal.edit', compact('goal', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Goal $goal)
    {
        try {
            if ($goal->theme_code != $this->themeCode || $goal->user_email != $this->emailUser) {
                return redirect('admin/goals');
            }

            $goal->update([
                'title' => $request->input('title'),
                'user_id' => $request->input('user_id'),
                'revenue' => str_replace('.', '', $request->input('revenue')),
                'customers' => $request->input('customers'),
                'orders' => $request->input('orders'),
                'notes' => $request->input('notes'),
                'purchases' => $request->input('purchases'),
                'reminders' => $request->input('reminders'),
                'calls' => $request->input('calls'),
                'month' => new \DateTime($request->input('month')),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);

        } catch (\Exception $e) {
            Log::error('http->admin->CommissionController->update: Lỗi chỉnh sửa cơ chế hoa hồng');
        } finally {
            return redirect('admin/goals');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goal $goal)
    {
        try {
            if ($goal->theme_code != $this->themeCode || $goal->user_email != $this->emailUser) {
                return redirect('admin/goals');
            }

            Goal::where('goal_id', $goal->goal_id)->delete();
        } catch (\Exception $e) {
            Log::error('http->admin->CommissionController->destroy: Lỗi xóa cơ chế hoa hồng');
        } finally {
            return redirect('admin/goals');
        }
    }
}
