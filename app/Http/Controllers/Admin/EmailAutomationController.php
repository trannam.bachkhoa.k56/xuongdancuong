<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 1/3/2019
 * Time: 9:06 AM
 */

namespace App\Http\Controllers\Admin;


use App\Entity\EmailAutomation;
use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class EmailAutomationController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }


            return $next($request);
        });
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emailAutomations = EmailAutomation::orderBy('email_automation_id', 'desc')->paginate(10);

        return View('admin.email_automation.list', compact('emailAutomations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.email_automation.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }
            // insert to database
            $template = new Template();
            $template->insert([
                'title' => $request->input('title'),
                'slug' => $slug,
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);
        } catch (\Exception $e) {
            Log::error('http->admin->TemplateController->store: Lỗi thêm mới template');
        } finally {
            return redirect('admin/templates');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(Template $template)
    {
        return redirect('admin/templates');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {
        if ($template->theme_code != $this->themeCode || $template->user_email != $this->emailUser) {
            return redirect('admin/templates');
        }

        return View('admin.template.edit', compact('template'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Template $template)
    {
        try {
            if ($template->theme_code != $this->themeCode || $template->user_email != $this->emailUser) {
                return redirect('admin/templates');
            }

            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }
            // insert to database
            $template->update([
                'title' => $request->input('title'),
                'slug' => $slug,
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);
        } catch (\Exception $e) {
            Log::error('http->admin->TemplateController->update: Lỗi chỉnh sửa template');
        } finally {
            return redirect('admin/templates');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(Template $template)
    {
        try {
            if ($template->theme_code != $this->themeCode || $template->user_email != $this->emailUser) {
                return redirect('admin/templates');
            }

            Template::where('template_id', $template->template_id)->delete();
        } catch (\Exception $e) {
            Log::error('http->admin->TemplateController->destroy: Lỗi xóa template');
        } finally {
            return redirect('admin/templates');
        }
    }

}