<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/12/2019
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use App\Facebook\Comment;
use App\Entity\FanpageManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Facebook\Fanpage;
use App\Facebook\People;

class CommunicateController extends AdminController
{

    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

			
            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }
            $fanpages = FanpageManager::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->get();

            view()->share(['menuTop'=>'communicate', 'fanpages' => $fanpages]);

            return $next($request);
        });
    }

    public function index (Request $request) {

        //return redirect(route('communicate.sendo'));

        return view('admin.communicate.index');
    }

    public function googleAds() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
			
        return view ('admin.communicate.google_ads');
    }

    public function analytics() {
        // if (Auth::user()->vip < 1) {
            // return redirect()->back();
        // }

        return view ('admin.communicate.analytics');
    }

    public function sendo() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.sendo');
    }

    public function shopee() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.shopee');
    }
	
	public function lazada() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.lazada');
    }

    public function muare() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.muare');
    }

    public function muaban() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.muaban');
    }

    public function enbac() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.enbac');
    }

    public function voso() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.voso');
    }

    public function tiki() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.tiki');
    }

    public function raovatvietnamnet() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.raovatvietnamnet');
    }

    public function chophien() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.chophien');
    }

    public function ndfloodinfo() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.ndfloodinfo');
    }

    public function muabanraovat() {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/kenh-ban-hang');
		// }
		
        return view ('admin.communicate.muabanraovat');
    }
}