<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CCallController extends Controller
{
    public static function callAPI($url, $data){
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_USERAGENT, 'MOMA');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
        ]);

        $result = curl_exec($curl);
        if (!$result) {
            die('Đường truyền bị lỗi');
        }
        curl_close($curl);
        return json_decode($result);
    }

    public static function callCDR($contactId) {
        $baseUrl = 'https://api.ccall.vn/cdrs/json';
        $apiKey = 'bd8108a2';
        $apiSecret = '864e68f6';
        $data = json_encode([
            'submission' => [
                'api_key' => $apiKey,
                'api_secret' => $apiSecret
            ]
        ]);

        $resultArray = array();
        $results = (object)static::callAPI($baseUrl, $data);
        $responseObjects = (object)$results->response;
        foreach ($responseObjects as $item) {
            $cdrToArray = (array)$item->Cdr;
            if ($cdrToArray['destination'] === (string)$contactId) {
                $resultArray[] = [
//                    'cid_name'  => $cdrToArray['cid_name'],
//                    'destination' => $cdrToArray['destination'],
                    'recording_file' => $cdrToArray['recording_file'],
                    'start' => $cdrToArray['start'],
                    'tta' => $cdrToArray['tta'],
                    'duration' => $cdrToArray['duration'],
                ];
            }
        }

        return (object)$resultArray;
    }
}
