<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 12/19/2018
 * Time: 3:48 PM
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Entity\GroupMail;
use App\Entity\EmailSetting;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class SettingAutomationController extends AdminController
{
	protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }

        //hiển thị cả danh sách
       
       public function index(){

        return view('admin.settingCustomer.index');

       }

        //thêm mới
        
       public function create(){
         return view('admin.settingCustomer.add');
       }

        //lưu trữ một tài nguyên mới
        
       public function store(){
        
       }

        //hiển thị 1 tài nguyên theo tham số truyền vào
        
       public function show($id){
        
       }

        //sửa 1 tài nguyên theo tham số truyền vào
        
       public function edit($id){
        
       }

        //cập nhật 1 tài nguyên theo tham số truyền vào
        
       public function update($id){
        
       }

        //xóa 1 tài nguyên
        
       public function destroy($id){

       }


}