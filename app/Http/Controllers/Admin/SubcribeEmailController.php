<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/7/2017
 * Time: 2:47 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\CheckSendMail;
use App\Entity\GroupMail;
use App\Entity\MailConfig;
use App\Entity\Post;
use App\Entity\SendMailCustomer;
use App\Entity\SubcribeEmail;
use App\Entity\User;
use App\Mail\Mail;
use App\Entity\Contact;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;

class SubcribeEmailController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }

    public function index() {
        try {
            $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();

            return view('admin.subcribe_email.index', compact('groupMails'));
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi hiển thị đăng ký email kh: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->index: Lỗi xảy ra trong quá trình hiển thị đăng ký email khách hàng');
            return redirect('admin/home');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        try {
            $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();

            return view('admin.subcribe_email.add', compact('groupMails'));
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi tạo mới đăng ký email kh: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->create: Lỗi xảy ra trong quá trình tạo mới đăng ký email khách hàng');

            return redirect('admin/home');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $subcribeEmail = new SubcribeEmail();
            $subcribeEmail->insert([
                'email' => $request->input('email'),
                'name' => $request->input('name'),
                'group_id' => $request->input('group'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi tạo mới đăng ký email kh: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->store: Lỗi xảy ra trong quá trình tạo mới đăng ký email khách hàng');
        } finally {
            return redirect(route('subcribe-email.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\SubcribeEmail  $subcribeEmail
     * @return \Illuminate\Http\Response
     */
    public function show(SubcribeEmail $subcribeEmail)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\SubcribeEmail  $subcribeEmail
     * @return \Illuminate\Http\Response
     */
    public function edit(SubcribeEmail $subcribeEmail)
    {
        try {
            $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();

            return view('admin.subcribe_email.edit', compact('groupMails', 'subcribeEmail'));
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi chỉnh sửa đăng ký email kh: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->edit: Lỗi xảy ra trong quá trình chỉnh sửa đăng ký email khách hàng');

            return redirect('admin/home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\SubcribeEmail  $subcribeEmail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubcribeEmail $subcribeEmail)
    {
        try {
            $subcribeEmail->update([
                'email' => $request->input('email'),
                'name' => $request->input('name'),
                'group_id' => $request->input('group'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi chỉnh sửa đăng ký email kh: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->update: Lỗi xảy ra trong quá trình chỉnh sửa đăng ký email khách hàng');
        } finally {
            return redirect(route('subcribe-email.index'));
        }
    }

    public function anyDatatables(Request $request) {
        $subcribeEmail = new SubcribeEmail();
        $subcribeEmails = $subcribeEmail->orderBy('subcribe_email_id', 'desc')->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser);

        return Datatables::of($subcribeEmails)
            ->addColumn('action', function($subcribeEmail) {
                $string =  '<a href="'.route('subcribe-email.edit', ['subcribe_email_id' => $subcribeEmail->subcribe_email_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('subcribe-email.destroy', ['subcribe_email_id' => $subcribeEmail->subcribe_email_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })->make(true);
    }
    
    public function destroy(SubcribeEmail $subcribeEmail) {
        try {
            $subcribeEmails = new SubcribeEmail();
            $subcribeEmails->where('subcribe_email_id', $subcribeEmail->subcribe_email_id)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->delete();
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xóa đăng ký email kh: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->update: Lỗi xảy ra trong quá trình xóa đăng ký email khách hàng');
        } finally {
            return redirect(route('subcribe-email.index'));
        }

    }

    public function send(Request $request) {
        try {
            $group = $request->input('group');
            $subject = $request->input('subject');
            $message = $request->input('content');
            // get email to
            $emails = Contact::where('status', $group)
            ->where('theme_code', $this->themeCode) 
            ->where('user_email', $this->emailUser)
            ->get();

//            $emailSend = array();
            $countEmail = 0;
            foreach($emails as $email) {
                if($email->email != null){
//                      $emailSend[] =  $email->email;
                    $message = '<img src="https://moma.vn/image.php?receiver=' . $email->email . '&author=' . $this->emailUser .'"><br />' . $message;
                    MailConfig::sendMail($email->email, $subject, $message);
                    $countEmail++;
                }
            }
//            if( $emailSend != null){
//                MailConfig::sendMail($emailSend, $subject, $message);
//            }

            $this->insertOrUpdateDataEmail(Auth::id(), date('d-m-Y') , $countEmail);
        }
         catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi gửi email : dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->send: Lỗi xảy ra trong quá trình gửi email khách hàng');
        } finally {
            return redirect()->back();
        }
    }

    public function sendForTag(Request $request) {
        try {
            $tag = $request->input('tag');
            $subject = $request->input('subject');
            $message = $request->input('content');

            // get email to
            $emails = Contact::where('tag', $tag)
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->get();

            //$emailSend = array();
            $countEmail = 0;
            foreach($emails as $email) {
                if ($email->email != null) {
                    $message = '<img src="https://moma.vn/image.php?receiver=' . $email->email . '&author=' . $this->emailUser .'"><br />' . $message;
                    MailConfig::sendMail($email->email, $subject, $message);
                    $countEmail++;
                      //$emailSend[] =  $email->email;
                }
            }
//            if ( $emailSend != null){
//                MailConfig::sendMail($emailSend, $subject, $message);
//            }
            $this->insertOrUpdateDataEmail(Auth::id(), date('Y-m-d') , $countEmail);
        }
         catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi gửi email: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->send: Lỗi xảy ra trong quá trình gửi email khách hàng');
        } finally {
            return redirect()->back();
        }
    }

     public function sendForCampaign(Request $request) {
        try {
            $campaign = $request->input('campaign');
            $process = $request->input('process');

            $subject = $request->input('subject');
            $message = $request->input('content');

            // get email to
            $emails = Contact::where('campaign_id', $campaign)
            ->where('campaign_status', $process)           
            ->get();

            // print_r($process);
            // exit();

            //$emailSend = array();
            $countEmail = 0;
            foreach($emails as $email) {
                if ($email->email != null) {
                    $message = '<img src="https://moma.vn/image.php?receiver=' . $email->email . '&author=' . $this->emailUser .'"><br />' . $message;
                    MailConfig::sendMail($email->email, $subject, $message);
                    $countEmail++;
                      //$emailSend[] =  $email->email;
                }
            }
//            if ( $emailSend != null){
//                MailConfig::sendMail($emailSend, $subject, $message);
//            }
            //$this->insertOrUpdateDataEmail(Auth::id(), date('Y-m-d') , $countEmail);
        }
         catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi gửi email: dữ liệu không hợp lệ. hoặc không có khách hàng trong chiến dịch này');
            Log::error('http->admin->SubcribeEmailController->send: Lỗi xảy ra trong quá trình gửi email khách hàng');
        } finally {
            return redirect()->back();
        }
    }


    public function goToImageFile(){
        return View('admin.email.Image');
    }

    private function insertOrUpdateDataEmail($userId, $time, $totalMail) {
        $sendMail = new SendMailCustomer();
        if ( $sendMail->where('user_id', $userId)->where('time_send_mail', $time)->exists()) {
            $totalMailOld = $sendMail->where('user_id', $userId)->where('time_send_mail', $time)->first(['total_mail']);
            $sendMail->where('user_id', $userId)->where('time_send_mail', $time)->update([
                'total_mail' => $totalMailOld + $totalMail,
                'updated_at' => new \DateTime()
            ]);
            return;
        }
        $sendMail->insertDataCustomerSendMail($userId, $time, $totalMail);
    }
}
