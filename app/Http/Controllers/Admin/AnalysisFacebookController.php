<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\Input;
use App\Entity\Template;
use App\Entity\TypeInput;
use App\Entity\User;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;
use App\Ultility\Ultility;

class AnalysisFacebookController extends AdminController
{
    protected $role;
	protected $accessTokenFacebook = '';

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		// lấy ra id quảng cáo.
		$accountId = '';
		// nếu thực hiện tìm kiếm và có id của accountId
		if ($request->has('campaigns')) {
			$accountId = $request->input('campaigns');
		}
		
		$this->accessTokenFacebook = Auth::user()->accesstoken;
		
		// nếu không thực hiện tìm kiếm
		$responses = $this->getCallApiAccount();
		$accountAds = array();
		$campains = array();
		$actionApiResponseAccount = null;
		// nếu tồn tại tài khoản quảng cáo
		if (isset ($responses['decode']['data']) && !empty($responses['decode']['data'])) {
			$accountId = !empty($accountId)	? $accountId : $responses['decode']['data'][0]['id'];
			$accountAds = $responses['decode']['data'];
			// tính ra xem tài khoản quảng cáo đầu tiên đó có những action gì.
			$actionApiResponseAccount = $this->callActionAds($accountId, $request->input('date_preset'));
			$actionApiResponseAccount = (isset ($actionApiResponseAccount['decode']['data']) && !empty($actionApiResponseAccount['decode']['data'])) ? $actionApiResponseAccount['decode']['data'] : array();
			//var_dump($actionApiResponseAccount);exit;
			
			// lấy ra toàn bộ chiến dịch thuộc nhóm quảng cáo đó.
			$campains = $this->getCampaigns($accountId);
			
			// lấy ra toàn bộ những action liên quan từng chiến dịch đó.
			if (isset ($campains['decode']['data']) && !empty($campains['decode']['data'])) {
				$campains = $campains['decode']['data'];
				
				foreach ($campains as $id => $campain) {
					// lấy ra action của chiến dịch
					$campainsAction = $this->callActionAds($campain['id'], $request->input('date_preset'));
					if (!empty($campainsAction['decode']['data'])) {
						//var_dump($campainsAction);exit;
					}
					$campains[$id] = $campainsAction['decode']['data'];
				}
				
			}
		}
	
		
		return view('admin.analysis_facebook.analysis_facebook', compact(
			'accountAds',
			'actionApiResponseAccount',
			'campains'
		));	
    }

	
	private function getCallApiAccount () {
        // truyền dữ liệu lên
        $service_url = 'https://graph.facebook.com/v6.0/me/adaccounts?access_token='.$this->accessTokenFacebook;
		
        return $this->callApi($service_url);
    }
	
	private function callActionAds ($idAds, $datePreset) {
		$service_url = 'https://graph.facebook.com/v6.0/'.$idAds.'/insights?fields=["actions","ctr","clicks","spend","social_spend","campaign_name","account_name"]&date_preset='.$datePreset.'&access_token='.$this->accessTokenFacebook;
		
		return $this->callApi($service_url);
	}
	
	private function getCampaigns ($idAccount) {
		$service_url = 'https://graph.facebook.com/v6.0/'.$idAccount.'/campaigns?access_token='.$this->accessTokenFacebook;
		
		return $this->callApi($service_url);
	}
	
	private function callApi ($service_url) {
		// truyền dữ liệu lên
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // kết quả response trả về
        $curl_response = curl_exec($curl);
        if(curl_errno($curl)){      
            echo 'Request Error:' . curl_error($curl);
        }
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $decoded = json_decode($curl_response, true);
        curl_close($curl);
        return [
            'httpCode' => $httpCode,
            'decode' => $decoded
        ];
	}
}
