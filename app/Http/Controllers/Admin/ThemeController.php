<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Domain;
use App\Entity\GroupTheme;
use App\Entity\Theme;
use App\Entity\ThemeUse;
use App\Entity\User;
use App\Ultility\Error;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;

class ThemeController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {

            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }
            view()->share('menuTop', 'setting');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.theme.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groupThemes = GroupTheme::orderBy('group_id', 'desc')->get();

        return view('admin.theme.add', compact('groupThemes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->role =  Auth::user()->role;

        if (!User::isCreater($this->role)) {
            return redirect('admin/home');
        }

        $validation = Validator::make($request->all(), [
            'name' => 'unique:themes',
            'url_test' => 'unique:themes',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/themes/create')
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $theme = new Theme();
        $theme->insert([
            'group_id' => $request->input('group_id'),
            'name' => $request->input('name'),
            'code' => $request->input('code'),
            'image' => $request->input('image'),
            'image_phone' => $request->input('image_phone'),
            'url_test' => $request->input('url_test'),
            'description' => $request->input('description'),
            'is_sale' => empty( $request->input('is_sale') ) ? 0 : 1,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('themes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function show(Theme $theme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function edit(Theme $theme)
    {
        $groupThemes = GroupTheme::orderBy('group_id', 'desc')->get();

        return view('admin.theme.edit', compact('theme', 'groupThemes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Entity\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Theme $theme)
    {
        $this->role =  Auth::user()->role;

        if (!User::isCreater($this->role) ) {
            return redirect('admin/home');
        }

        $validation = Validator::make($request->all(), [
            'name' => Rule::unique('themes')->ignore($theme->theme_id, 'theme_id'),
            'url_test' => Rule::unique('themes')->ignore($theme->theme_id, 'theme_id'),
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('themes.edit', ['theme_id' => $theme->theme_id]))
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $theme->update([
            'group_id' => $request->input('group_id'),
            'name' => $request->input('name'),
            'code' => $request->input('code'),
            'image' => $request->input('image'),
            'image_phone' => $request->input('image_phone'),
            'url_test' => $request->input('url_test'),
            'description' => $request->input('description'),
            'is_sale' => empty( $request->input('is_sale') ) ? 0 : 1,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('themes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Theme $theme
     * @return \Illuminate\Http\Response
     */
    public function destroy(Theme $theme)
    {
        $this->role =  Auth::user()->role;

        if (!User::isCreater($this->role)) {
            return redirect('admin/home');
        }
        $theme->delete();

        return redirect(route('themes.index'));
    }

    public function anyDatatables(Request $request)
    {

        $themes = Theme::leftJoin('group_theme', 'themes.group_id', '=', 'group_theme.group_id')
            ->select('themes.*', 'group_theme.name as name_group');

//        $themes = Theme::select('themes.*');

        return Datatables::of($themes)
            ->addColumn('action', function ($theme) {
                $string = '<a href="' . route('themes.edit', ['theme_id' => $theme->theme_id]) . '">
                           <button class="btn btn-primary btn-sm btn-icon"><i class="flaticon-edit-1" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="' . route('themes.destroy', ['theme_id' => $theme->theme_id]) . '" class="btn btn-danger btn-sm btn-icon"
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="flaticon-delete" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('theme_id', 'theme_id desc')
            ->make(true);
    }

    
}
