<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TransportController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }
			
            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'communicate');

            return $next($request);
        });
    }

	public function index (Request $request) {
		return view('admin.transport.index');
	}
	
    public function giaohangtietkiem (Request $request) {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/quan-ly-van-chuyen');
		// }
		
        return view('admin.transport.giaohangtietkiem');
    }

    public function viettel (Request $request) {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/quan-ly-van-chuyen');
		// }
		
        return view('admin.transport.viettel');
    }

    public function giaohangnhanh (Request $request) {
		// if (Auth::user()->vip < 1) {
			// return redirect('/admin/quan-ly-van-chuyen');
		// }
		
        return view('admin.transport.giaohangnhanh');
    }
}