<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 3/22/2018
 * Time: 9:44 AM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\ColorWebsite;
use App\Entity\HistoryPayment;
use App\Entity\MailConfig;
use App\Entity\OrderBank;
use App\Entity\OrderCodeSale;
use App\Entity\OrderShip;
use App\Entity\SettingGetfly;
use App\Entity\SettingOrder;
use App\Entity\User;
use App\Entity\PaymentInfomation;
use App\Entity\Domain;
use App\Ultility\Ultility;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Ultility\CallApi;
use Redirect;

class SettingController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'websites');

            return $next($request);
        });

    }

    public function setting(Request $request) {
        try {
            if ($request->has('accesstoken')) {
                User::where('id', Auth::user()->id)->update([
                    'accesstoken' => $request->input('accesstoken')
                ]);
            }
            $orderShips = OrderShip::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();
            $orderBanks = OrderBank::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();
            $orderCodeSales = OrderCodeSale::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();
            $settingOrder = SettingOrder::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->first();
            $userId = Auth::user()->id;
            $settingEmail = MailConfig::where('user_id', $userId)->first();
            if (empty($settingEmail)) {
                $mailConfigModel =  new MailConfig();
                $mailConfigModel->insert([
                    'user_id' => $userId,
                    'created_at' => new \Datetime(),
                    'updated_at' => new \DateTime()
                ]);
                $settingEmail = MailConfig::where('user_id', $userId)->first();
            }

            $colorWebsite = ColorWebsite::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->first();

            $loginUrl = $this->getLoginFacebook();
			$userId = Auth::user()->id;
            $callApi = new CallApi();
            $campaigns = $callApi->getCampaigns();
			
            return view('admin.setting.setting', compact(
                'orderShips',
                'orderBanks',
                'orderCodeSales',
                'settingOrder',
                'settingEmail',
                'loginUrl',
                'campaigns',
                'colorWebsite'
            ));

        } catch(\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi hiển thị cài đặt thanh toán: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->setting: Lỗi xảy ra trong quá trình hiển thị cài đặt thanh toán');

            //return redirect('admin/home');
        }
    }

    private function getLoginFacebook() {
        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $urlLogin = 'http://facebook.vn3c.net/flogin?service_code=1c9b7597b1e8b09e61b419235f1d207a&currentUrl='.$actual_link;

        return $urlLogin;
    }

    public function updateSetting(Request $request) {
        try {
            $settingOrder = new SettingOrder();

            $settingOrder->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->delete();
            $settingOrder->insert([
                'point_to_currency' => $request->input('point_to_currency'),
                'currency_give_point' => $request->input('currency_give_point'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi cập nhật cài đặt thanh toán: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->setting: Lỗi xảy ra trong quá trình cập nhật cài đặt thanh toán');
        } finally {
            return redirect(route('method_payment'));
        }
    }
    public function updateBank(Request $request) {
        try {
            $orderBank = new OrderBank();
            $orderBank->insert([
                'name_bank' => $request->input('name_bank'),
                'number_bank' => $request->input('number_bank'),
                'manager_account' => $request->input('manager_account'),
                'branch' => $request->input('branch'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);


        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi cập nhật ngân hàng cài đặt thanh toán: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->updateBank: Lỗi xảy ra trong quá trình cập nhật ngân hàng cài đặt thanh toán');
        } finally {
            return redirect(route('method_payment'));
        }
    }
    public function deleteBank(OrderBank $orderBanks){
        try {
            OrderBank::where('order_bank_id' , $orderBanks->order_bank_id)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->delete();
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xóa ngân hàng cài đặt thanh toán: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->deleteBank: Lỗi xảy ra trong quá trình xóa ngân hàng cài đặt thanh toán');
        } finally {
            return redirect(route('method_payment'));
        }
    }
    public function updateCodeSale(Request $request) {
        try {
            $orderCodeSale = new OrderCodeSale();

            $discountStartEnd = $request->input('code_sale_start_end');
            $discountTime = explode('-', $discountStartEnd);
            $discountStart = new \DateTime($discountTime[0]);
            $discountEnd = new \DateTime($discountTime[1]);

            $orderCodeSale->insert([
                'code' => $request->input('code'),
                'method_sale' => $request->input('method_sale'),
                'sale' => $request->input('sale'),
                'start' =>  $discountStart,
                'end' => $discountEnd ,
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'many_use' => $request->input('many_use'),
            ]);
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi cập nhật mã giảm giá: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->updateCodeSale: Lỗi xảy ra trong quá trình cập nhật mã giảm giá ');
        } finally {
            return redirect(route('method_payment'));
        }
    }
    public function deleteCodeSale(OrderCodeSale $orderCodeSales){
        try {
            OrderCodeSale::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->where('order_code_sale_id' , $orderCodeSales->order_code_sale_id)->delete();
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xóa mã giảm giá: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->deleteCodeSale: Lỗi xảy ra trong quá trình xóa mã giảm giá ');
        } finally {
            return redirect(route('method_payment'));
        }
    }

    public function updateColor(Request $request) {
        $colorWebsite = ColorWebsite::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->first();

        if (empty($colorWebsite)) {
            ColorWebsite::insert([
                'text_full' => $request->input('text_full'),
                'color_hover' => $request->input('color_hover'),
                'color_title' => $request->input('color_title'),
                'color_footer' => $request->input('color_footer'),
                'background_full' => $request->input('background_full'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime()
            ]);

            return redirect()->back();
        }

        ColorWebsite::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->update([
                'text_full' => $request->input('text_full'),
                'color_hover' => $request->input('color_hover'),
                'color_title' => $request->input('color_title'),
                'color_footer' => $request->input('color_footer'),
                'background_full' => $request->input('background_full'),
                'updated_at' => new \DateTime()
            ]);

        return redirect()->back();
    }


    public function updateShip(Request $request) {
        try {
            $orderShip =  new OrderShip();
            $orderShip->insert([
                'method_ship' => $request->input('method_ship'),
                'cost' => $request->input('cost'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi cập nhật vận chuyển: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->updateShip: Lỗi xảy ra trong quá trình cập nhật vận chuyển');
        } finally {
            return redirect(route('method_payment'));
        }
    }

    public function updateSettingGetFly(Request $request) {
        // try {
			
            $user = Auth::user();
            $settingGetfly = SettingGetfly::where('user_id', $user->id)->first();
			
			$campain = $request->input('campaign');
            $campains = explode('-', $campain);
            $campainGetfly = 0;
            $campainStatus = 0;
            if (count($campains) == 2 ) {
                $campainGetfly = $campains[0];
                $campainId = $campains[1];
                $callApi = new CallApi();
                $campaignStatusList = $callApi->getCampaignStatus($campainId);
                $campainStatus = isset($campaignStatusList['decode'][0]['opportunity_status_id']) ? $campaignStatusList['decode'][0]['opportunity_status_id'] : 0;
            }
			
            // Nếu không tồn tại thì thêm mới
            if (empty($settingGetfly)) {
                SettingGetfly::insert([
                    'user_id' => $user->id,
                    'api_key' => $request->input('api_key'),
                    'base_url' => $request->input('base_url'),
					'campaign' => $campainGetfly,
                    'campaign_status' => $campainStatus,
                    'created_at' => new \Datetime(),
                    'updated_at' => new \DateTime()
                ]);

                return redirect(route('method_payment'));
            }

            SettingGetfly::where('setting_getfly_id', $settingGetfly->setting_getfly_id)->update([
                'api_key' => $request->input('api_key'),
                'base_url' => $request->input('base_url'),
				'campaign' => $campainGetfly,
				'campaign_status' => $campainStatus,
                'created_at' => new \Datetime(),
                'updated_at' => new \DateTime()
            ]);

        // } catch (\Exception $e) {
            // Error::setErrorMessage('Lỗi xảy ra khi cập nhật dữ liệu getfly: dữ liệu không hợp lệ.');
            // Log::error('http->admin->OrderController->updateSettingGetFly: Lỗi xảy ra trong quá trình cập nhật cài đặt getfly');
        // } finally {
            return redirect(route('method_payment'));
        // }
    }

    public function updateSettingEmail(Request $request) {
        try {
            $user = Auth::user();
            $mailConfigModel = new MailConfig();
            $mailConfig = $mailConfigModel->where('user_id', $user->id)->first();
            // Nếu không tồn tại thì thêm mới
            if (empty($mailConfig)) {
                $mailConfigModel->insert([
                    'user_id' => $user->id,
                    'email_send' => $request->input('email_send'),
                    'name_send' => $request->input('name_send'),
                    'email' => $request->input('email'),
                    'password' => $request->input('password'),
                    'address_server' => $request->input('address_server'),
                    'port' => $request->input('port'),
                    'sign' => $request->input('sign'),
                    'supplier' => $request->input('supplier'),
                    'method' => $request->input('method'),
                    'api_key' => $request->input('api_key'),
                    'driver' => $request->input('driver'),
                    'host' => $request->input('host'),
                    'email_receive' => $request->input('email_receive'),
                    'encryption' => $request->input('encryption'),
                    'created_at' => new \Datetime(),
                    'updated_at' => new \DateTime()
                ]);

                return redirect(route('method_payment').'#email');
            }

            $mailConfig->update([
                'user_id' => $user->id,
                'email_send' => $request->input('email_send'),
                'name_send' => $request->input('name_send'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'address_server' => $request->input('address_server'),
                'port' => $request->input('port'),
                'sign' => $request->input('sign'),
                'supplier' => $request->input('supplier'),
                'method' => $request->input('method'),
                'api_key' => $request->input('api_key'),
                'driver' => $request->input('driver'),
                'host' => $request->input('host'),
                'email_receive' => $request->input('email_receive'),
                'encryption' => $request->input('encryption'),
                'created_at' => new \Datetime(),
                'updated_at' => new \DateTime()
            ]);

        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi cập nhật cấu hình email: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->updateSettingEmail: Lỗi xảy ra trong quá trình cập nhật cấu hình email');
        } finally {
            return redirect(route('method_payment'));
        }
    }


    public function deleteShip(OrderShip $orderShips)
    {
        try {
            OrderShip::where('order_ship_id', $orderShips->order_ship_id)->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->delete();
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xóa ship hàng: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->deleteShip: Lỗi xảy ra trong quá trình xóa ship hàng ');
        } finally {
            return redirect(route('method_payment'));
        }
    }

    public function testEmail(Request $request) {
        // try {
            $emailTest = $request->input('email_test');

            $result = $this->sendMail($emailTest);

            if ($result == true) {
                return redirect(route('method_payment').'?error=0');
            }

            return redirect(route('method_payment').'?error=1');
        // } catch (\Exception $e) {
            // return redirect(route('method_payment').'?error=1');
        // }

    }

    private function sendMail($emailTest) {
        if (!empty($this->domainUser)) {
            $subject = 'Website '.$this->domainUser->name.' kiểm tra email';
        } else {
            $subject = 'Website vn3c kiểm tra email';
        }

        $content = 'Kiểm tra cài dặt thông tin email thành công. Bạn có thể sử dụng để gửi đơn hàng, hay chăm sóc khách hàng.';

        return MailConfig::send_mail_with_themecode($emailTest, $subject, $content, Auth::user()->email);

    }
	
	public function showGetfly() {
		$getflyUrl = SettingGetfly::getBaseUrl();
		view()->share('menuTop', 'getfly');
		 
		if (empty($getflyUrl)) {
			return redirect()->back();
		}
		
		return view('admin.getfly.index', compact('getflyUrl'));
	}
	
	public function showFacebook() {
		view()->share('menuTop', 'facebook');
		
		return view('admin.social.facebook');
	}
	
	public function showZalo() {
		view()->share('menuTop', 'zalo');
		
		return view('admin.social.zalo');
	}

	public function viewWebsite() {

    }

     public function showChangeUrl(){
		view()->share('menuTop', 'communicate');
        try {
            Domain::where('domain_id', $this->domainUser->domain_id)->update([
                'change_url' => 1
            ]);

            $domain = 'moma.vn';
            if (!empty($this->domainUser)) {
                $domain =  $this->domainUser->domain;
            }

            $domain = str_replace('http://','',$domain);
            $domain = str_replace('https://','',$domain);
        } catch (\Exception $e) {
            $domain = '';
        } finally {
            return view('admin.change_url.index',compact('domain'));
        }
    }

     public function changeUrl(Request $request){
         if(Auth::user()->vip == 0 ){
            return redirect('/admin/nang-cap-tai-khoan');
         }

        //if(Auth::user()->vip == 0 ){
			// return redirect('/admin/home');
        //}
        
		$url = $request->input('url');
		$userId = Auth::user()->id;
		// Kiểm tra domain đó đã tồn tại chưa
		$domainExist = Domain::where('url', 'http://'.$url)
		->orWhere('domain', 'http://'.$url)
		->first();
		
		if (!empty($domainExist)) {
			 return redirect('/admin/thay-doi-url?error=1');
		}
        
		view()->share('menuTop', 'products'); 
		
        $url = $request->input('url');
        $domainUrl = Ultility::getCurrentDomain();
		
        $domain = Domain::where('url', $domainUrl)
		->orWhere('domain', $domainUrl)
        ->update([
            'domain' => 'http://'.$url,
        ]);
        return redirect('/admin/thay-doi-url');
		
    }

    public function billingSummary (Request $request) {
        $notify = $request->input('notify');
        
        $payment =  PaymentInfomation::getWithUser(Auth::user()->id);
        $historyPayments = HistoryPayment::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->orderBy('history_payment_id', 'desc')
            ->paginate(20);

        return view('admin.setting.bill_summary', compact ('payment', 'historyPayments'));
    }

}
