<?php

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use Illuminate\Validation\Rule;

class UpdateMailController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }
            view()->share('menuTop', 'setting');
            return $next($request);
        });

    }

    public function index(){
    	return view('admin.update_email.index');
    }

    public function edit($id){
        $userModel = new User();
        $user = $userModel->where('id',$id)
        ->first();

        return view('admin.update_email.edit', compact('user'));

    }

      public function update(Request $request){
        $userModel = new User();

        $id = $request->userId;
        $user = $userModel->where('id', $id)
        ->update([
            'email_remaining' => $request->email_remaining
        ]);

       return redirect()->back()->with('success','Cập nhật dữ liệu thành công!');

    }



    public function showUserAjax(Request $request){
        $output="";
    	$userModel = new User();

    	$users = $userModel
    	->where('email','LIKE','%'.$request->value."%")
        ->orWhere('name','LIKE','%'.$request->value."%")
    	->get();

    	foreach ($users as $id => $user) {
	      # code...
		   $output .= '
                   <tr>
                        <td>' . $user->id . '</td>
                        <td>' . $user->name . '</td>
                        <td>' . $user->email . '</td>
                        <td>
                            <a href="'.route("edit_user_mail",["id" =>$user->id]).'">
                                <button class="btn btn-primary">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </button>
                            </a>
                        </td>
                    </tr>
                ';

    	}
        return $output;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
}
