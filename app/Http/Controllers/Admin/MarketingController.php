<?php

namespace App\Http\Controllers\Admin;

use App\Ultility\Error;
use Illuminate\Http\Request;
use App\Entity\User;
use App\Entity\AffiliateMoma;
use Carbon\Carbon;
use App\Ultility\Ultility;
use App\Entity\PaymentInfomation;
use App\Entity\HistoryPayment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Psy\Util\Str;
use Validator;
use Yajra\Datatables\Datatables;

class MarketingController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) { 
				return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'communicate');

            return $next($request);
        });

    }

    public function index(){
    	return view('admin.marketing.index');
    }

    public function momaMarketing () {
        return view ('admin.marketing.moma_marketing');
    }

    public function affliate() {
        $affiliateMoma = AffiliateMoma::where('theme_code', $this->themeCode)
        ->where('user_email', $this->emailUser)->first();

        $affiliateMomas = AffiliateMoma::join('users', 'users.email', 'affiliate_moma.user_email')
        ->join('domains', 'domains.user_id', 'users.id')
        ->select (
            'affiliate_moma.*',
            'domains.domain',
            'domains.url'
        )
        ->orderBy('affiliate_moma.money', 'desc')
        ->get();

        return view('admin.marketing.affiliate', compact('affiliateMoma', 'affiliateMomas'));
    }

    public function storeAffiliate(Request $request) {
        $affiliateMoma = AffiliateMoma::where('theme_code', $this->themeCode)
        ->where('user_email', $this->emailUser)->first();

        if (empty($affiliateMoma)) {
            AffiliateMoma::insert([
                'title' => $request->input('title'),
                'link' => $request->input('link'),
                'content_deal' => $request->input('content_deal'),
                'affiliate' => $request->input('affiliate'),
                'theme_code' => $this->themeCode,
                'user_email' =>  $this->emailUser,
                'created_at' => new \Datetime(),
                'updated_at' => new \Datetime()
            ]);        
            
            User::where('id', Auth::user()->id)->update([
                'affiliate_money' => $request->input('affiliate')
            ]);

            return redirect()->back();
        }

        AffiliateMoma::where('affiliate_moma_id', $affiliateMoma->affiliate_moma_id)->update([
            'title' => $request->input('title'),
            'link' => $request->input('link'),
            'content_deal' => $request->input('content_deal'),
            'affiliate' => $request->input('affiliate'),
            'theme_code' => $this->themeCode,
            'user_email' =>  $this->emailUser,
            'updated_at' => new \Datetime()
        ]);        
        
        User::where('id', Auth::user()->id)->update([
            'affiliate_money' => $request->input('affiliate')
        ]);

        return redirect()->back();
    }

    public function paymentAdsMoma(Request $request) {
        if (!Auth::check()) {
            return response([
                'status' => 500,
                'message' => 'Bạn chưa đăng nhập, hoặc lỗi trong quá trình đăng nhập!',
            ])->header('Content-Type', 'text/plain');
        }

        DB::beginTransaction();
        $user = Auth::user();

        $money = $request->input('money');

        if (empty($money)) {
            return response([
                'status' => 500,
                'message' => 'Đã có lỗi xảy ra, vui lòng chọn lại phương thức thanh toán',
            ])->header('Content-Type', 'text/plain');
        }

        // kiểm tra xem đủ phí chi trả mua theme không?
        $paymentInformation =  PaymentInfomation::getWithUser(Auth::user()->id);

        // nếu không đủ yêu cầu họ phải nạp tiền.
        if (empty($paymentInformation ) || $paymentInformation->coin_total < $money) {
            return response([
                'status' => 500,
                'message' => 'Tiền trong tài khoản của bạn không đủ để thực hiện giao dịch. Vui lòng truy cập <a href="/admin/tom-luoc-thanh-toan">nạp tiền</a> để nạp tiền vào tài khoản.',
            ])->header('Content-Type', 'text/plain');
        }

        // nếu đủ thì trừ tiền của họ. 
        PaymentInfomation::where('user_id', Auth::user()->id)->update([
            'coin_total' => ($paymentInformation->coin_total - $money),
            'updated_at' => new \DateTime()
        ]);

        HistoryPayment::insert([
            'content' => 'Trừ tiền quảng cáo moma '.number_format($money).' vnđ',
            'theme_code' => $this->themeCode,
            'user_email' =>$this->emailUser,
            'money' => $money,
            'created_at' => new \DateTime()
        ]);

        DB::commit();

        return response([
            'status' => 200,
            'message' => 'Bạn đã thay đổi thành công, cảm ơn bạn đã sử dụng dịch vụ của chúng tôi!',
        ])->header('Content-Type', 'text/plain');
    }
}
