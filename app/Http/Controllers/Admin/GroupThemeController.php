<?php

namespace App\Http\Controllers\Admin;

use App\Entity\GroupTheme;
use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;

class GroupThemeController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }
            view()->share('menuTop', 'setting');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.group_theme.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.group_theme.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'unique:group_theme',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/group-theme/create')
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $groupTheme = new GroupTheme();
        $groupTheme->insert([
            'name' => $request->input('name'),
            'image' => $request->input('image'),
            'description' => $request->input('description'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('group-theme.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\GroupTheme  $groupTheme
     * @return \Illuminate\Http\Response
     */
    public function show(GroupTheme $groupTheme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\GroupTheme  $groupTheme
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupTheme $groupTheme)
    {
        return view('admin.group_theme.edit', compact('groupTheme'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\GroupTheme  $groupTheme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupTheme $groupTheme)
    {
        $validation = Validator::make($request->all(), [
            'name' => Rule::unique('group_theme')->ignore($groupTheme->group_id , 'group_id'),
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('group-theme.edit', ['group_id' => $groupTheme->group_id]))
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $groupTheme->update([
            'name' => $request->input('name'),
            'image' => $request->input('image'),
            'description' => $request->input('description'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('group-theme.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\GroupTheme  $groupTheme
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupTheme $groupTheme)
    {
        $groupTheme->delete();

        return redirect(route('themes.index'));
    }

    public function anyDatatables(Request $request)
    {
        $groupThemes = GroupTheme::select('group_theme.*');

        return Datatables::of($groupThemes)
            ->addColumn('action', function ($groupTheme) {
                $string = '<a href="' . route('group-theme.edit', ['group_id' => $groupTheme->group_id]) . '">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="' . route('group-theme.destroy', ['group_id' => $groupTheme->group_id]) . '" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('group_id', 'group_id desc')
            ->make(true);
    }
}
