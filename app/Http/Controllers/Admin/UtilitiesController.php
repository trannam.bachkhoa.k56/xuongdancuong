<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/12/2019
 * Time: 3:08 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\Domain;
use App\Entity\User;
use App\Facebook\Comment;
use App\Entity\FanpageManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Facebook\Fanpage;
use App\Facebook\People;
use Illuminate\Support\Facades\Crypt;

class UtilitiesController extends AdminController
{

    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

			
            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }
            $fanpages = FanpageManager::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->get();

            view()->share(['menuTop'=>'utilities']);

            return $next($request);
        });
    }

    public function index (Request $request) {
        $company = isset($this->domainUser->name) ? $this->domainUser->name :  'moma.vn';
        $user = Auth::user();
        $name = $user->name;
        $phone = $user->phone;
        $email = $user->email;
        $password = $user->password;

        $encrypt = Crypt::encrypt([
            'random' => rand(1000000, 10000000),
            'company' => $company,
            'name' =>  $name,
            'phone' =>  $phone,
            'email' =>  $email,
            'password' =>  $password,
        ]);

        return view('admin.utilities.index', compact('encrypt'));
    }

    public function loginVpnChat() {
        $user = Auth::user();
        $this->domainUser;
        
        $data = [
            'email' => $user->email,
            'name' => $user->name,
            'phone' => $user->phone,
            'password' => $user->phone,
            'shop_name' => $this->domainUser->name,
            'source_register' => !empty($this->domainUser->domain) ? $this->domainUser->domain : $this->domainUser->url,
        ];

        $service_url = 'http://account.vnpsoftware.biz/api/v2/user/affilate/register';
        $curl = curl_init($service_url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POST, count($data));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

        $curl_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $decoded = json_decode($curl_response);

        curl_close($curl);

        var_dump($decoded);exit;

         // tra ve ket qua
         return response([
            'status' => 200,
            'url_redirect' => $decoded->url_redirect
        ])->header('Content-Type', 'text/plain');
    }
}
