<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 8/20/2019
 * Time: 2:44 PM
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;

class ChangeDomainController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }

            view()->share('menuTop', 'setting');
            return $next($request);
        });
    }

    public function index () {
        return view('admin.change_domain.index');
    }

    public function changeDomain(Request $request) {
        $domain = $request->input('domain');

        return view('admin.change_domain.change_domain');
    }
}
