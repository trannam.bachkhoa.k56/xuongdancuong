<?php

namespace App\Http\Controllers\Admin;


use App\Entity\Commission;
use Illuminate\Http\Request;
use App\Entity\User;
use App\Entity\Domain;
use App\Entity\DomainUser;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Validator;

class AddUserDomaninController extends AdminController
{
	protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                
				return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'communicate');

            return $next($request);
        });

    }

    public function showAddUser(){
        $commissions = Commission::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->orderBy('commission_id', 'asc')
            ->get();

        return view('admin.domain_user.list', compact('commissions'));
    }

    public function addUser(Request $request){
		$emailInvite = $request->input('email_invite');

		$userEmailInvite = User::where('email', $emailInvite)
		->first();

    	$domainUser = DomainUser::insertGetId([
    		'domain_id' => $this->domainUser->domain_id,
    		'user_id' => $userEmailInvite->id,
    		'role' => $request->input('role'),
    		'commission_id' => $request->input('commission_id'),
            'theme_code' => $request->input('theme_code'),
            'user_email' => $request->input('user_email'),
    		'created_at' => new \DateTime(),
    		'updated_at' => new \DateTime(),
    	]);

    	return redirect()->back();
    }

    public function editUser(Request $request){
    	$id = $request->input('id');
        DomainUser::where('domain_user_id', $id)
    	->update([
    		'commission_id' => $request->input('commission_id'),
    	]);
        return redirect()->back();
    }

    public function redirectUser($encrypt){

            $encrypt = Crypt::decrypt($encrypt);
            $domainUrl = $encrypt['domainUrl'];
            $userId = $encrypt['userId'];

            $domain = Domain::where('url', $domainUrl)
            ->first();
            
            $url = $domainUrl.'/check-user-redirect/'.$userId.'/'.$domain->domain_id;

            // echo $url;
            // exit();
            //$this->setCookieUser($userDomain);

            //return redirect($domainUrl.'/admin/check-user-redirect/'.$user_id.'/'.$domain->domain_id);
            //return redirect()->route('check_user_redirect',['user_id' => $userId, 'domain' => $domain->domain_id ]);
            return redirect($url);
    }

    private function setCookieUser($userDomain){ 

        // $userRedirect = array();
        // if (isset($_COOKIE["userRedirect"])) {
        //     $userRedirects = unserialize($_COOKIE['userRedirect'], ["allowed_classes" => false]);
        //     foreach ($userRedirects as $userRedirect) {
        //         if (!empty($userDomain)) {
        //             $userLogins[] = [
        //                 'url' => $domainUrl,
        //                 'user_id' => $userDomain->user_id,
        //                 'role' => $roleUser
        //             ];
        //         }
                
        //     }
        // }

       
        //return true ;     

        // $email = $userRedirect->email;
        // // check va luu cookie dang nhap hien thi trang chu
        // if (isset($_COOKIE['userRedirect'])) {
        //     $userLoginedes = unserialize($_COOKIE['userRedirect'], ["allowed_classes" => false]);
        //     //unset($_COOKIE['userLoginMoma']);
        //     setcookie ("userRedirect", "", time() - 3600);
        // }
        // if (!empty($userLoginedes) && in_array($email, $userLoginedes) == false) {
        //     $userLoginedes[] = $email;
            
        //     setcookie('userRedirect', serialize($userLoginedes), time()+ 3600*365, '/');
        //     return;
        // }        
        // if (empty($userLoginedes)) {
        //     $userLoginedes[] = $email;
            
        //     setcookie('userRedirect', serialize($userLoginedes), time()+ 3600*365, '/');
        //     return;
        // }
        
        // setcookie('userRedirect', serialize($userLoginedes), time()+ 3600*365, '/');
     }
    

}
