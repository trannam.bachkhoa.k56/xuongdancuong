<?php

namespace App\Http\Controllers\Admin;

use App\Entity\GroupHelpVideo;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;

class GroupHelpVideoController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }
            view()->share('menuTop', 'setting');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.group_help_video.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.group_help_video.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'unique:group_help_video',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/group-theme/create')
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $groupTheme = new GroupHelpVideo();
        $groupTheme->insert([
            'title' => $request->input('title'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('group-help-video.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\GroupHelpVideo  $groupHelpVideo
     * @return \Illuminate\Http\Response
     */
    public function show(GroupHelpVideo $groupHelpVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\GroupHelpVideo  $groupHelpVideo
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupHelpVideo $groupHelpVideo)
    {
        return view('admin.group_help_video.edit', compact('groupHelpVideo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\GroupHelpVideo  $groupHelpVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupHelpVideo $groupHelpVideo)
    {
        $validation = Validator::make($request->all(), [
            'title' => Rule::unique('group_help_video')->ignore($groupHelpVideo->group_id , 'group_id'),
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('group-help-video.edit', ['group_id' => $groupHelpVideo->group_id]))
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $groupHelpVideo->update([
            'title' => $request->input('title'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('group-help-video.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\GroupHelpVideo  $groupHelpVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupHelpVideo $groupHelpVideo)
    {
        $groupHelpVideo->delete();

        return redirect(route('group-help-video.index'));
    }

    public function anyDatatables(Request $request)
    {
        $groupHelpVideos = GroupHelpVideo::select('group_help_video.*');

        return Datatables::of($groupHelpVideos)
            ->addColumn('action', function ($groupHelpVideo) {
                $string = '<a href="' . route('group-help-video.edit', ['group_id' => $groupHelpVideo->group_id]) . '">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="' . route('group-help-video.destroy', ['group_id' => $groupHelpVideo->group_id]) . '" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('group_id', 'group_id desc')
            ->make(true);
    }
}
