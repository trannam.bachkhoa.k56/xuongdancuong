<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Domain;
use App\Entity\MailConfig;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Ultility\Ultility;
use App\Entity\DomainUser;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/information';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.login.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
		
        $user = User::where('email', $request->input('email'))->first();
        if (empty($user)) {
			
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }

        // kiem tra xem ton tai domain ko
        $domainUrl = Ultility::getCurrentDomain();

        $domainExist = Domain::where('user_id', $user->id)
            ->where('url', $domainUrl)
			->orWhere('domain', $domainUrl)
			->exists();
			
		// lay ra ten mien
		$nameUrl = Ultility::getCurrentHttpHost();
	
		if ($nameUrl != 'moma.vn' && $nameUrl != 'taowebsitemienphi.com' ) {
			$domainExist = Domain::where('url', 'like', '%//'.$nameUrl.'%')
			->orWhere('domain', 'like', '%'.$nameUrl.'%')
			->exists();
		}

        if (($user->role == 4 && $this->attemptLogin($request))) {
            return $this->sendLoginResponse($request);
        }

		$domain = Domain::where('url', $domainUrl)->first();
		if ($nameUrl != 'moma.vn' && $nameUrl != 'taowebsitemienphi.com' ) {
			
			$domain = Domain::where('url', 'like', '%//'.$nameUrl.'%')
			->orWhere('domain', 'like', '%'.$nameUrl.'%')
			->first();
		}
		
		if ($user->id != 1 && $domainExist && $domain->user_id != $user->id ) {
			$this->incrementLoginAttempts($request);

			return $this->sendFailedLoginResponse($request);
		}

        if ($domainExist && $this->attemptLogin($request)) {

            return $this->sendLoginResponse($request);
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
	
    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ], [
            'required' => 'Trường :attribute không được để trống'
        ]);
    }

  


    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
		// xóa session
        session_start();
        session_destroy();
		
        return redirect('/admin');
    }

    public function getReset() {
        return view('admin.login.email');
    }

    public function postReset(Request $request) {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validation->fails()) {
            return redirect()->back()
                ->withErrors($validation)
                ->withInput();
        }

        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        $message = 'Bạn vui lòng check lại email để khôi phục mật khẩu mới!';

        if (empty($user)) {
            $validations['emailFail'] = 'Email của bạn không tồn tại trong hệ thống';

            return redirect()->back()
                ->withErrors($validations)
                ->withInput();
        }

        // gửi email có đường link thay đổi mật khẩu faceboook.
        $urlRenewPassword = 'https://moma.vn/admin/create-new-password?token='.$user->password;

        $this->sendMainRenew($urlRenewPassword, $user->email);

        return view('admin.login.email_password_success', compact('message'));
    }

    public function getCreateNewPassword(Request $request) {
        $token = $request->input('token');

        return view('admin.login.reset_password', compact('token'));
    }

    public function postCreateNewPassword(Request $request) {
        $validation = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect()->back()
                ->withErrors($validation)
                ->withInput();
        }

        $user = User::where('password', $request->input('token'))->first();

        if (!empty($user)) {
            User::where('password', $request->input('token'))->update([
                'password' => bcrypt($request->input('password'))
            ]);

            $domain = Domain::where('user_id', $user->id)->first();

            return redirect($domain->url.'/admin/home');
        }

        return redirect()->back();
    }

    private function sendMainRenew($urlRenewPassword, $email)  {
        try {
            $subject =  'Moma Khôi phục mật khẩu';
            $content = '<p>Vui lòng ấn vào link dưới đây để tiến hành khôi phục mật khẩu: </p>';
            $content .= '<p><a href="'.$urlRenewPassword.'" style="boder: 1px solid #e5e5e5; padding: 5px 10px;">Thay Đổi Mật Khẩu</a></p>';

            MailConfig::sendMail($email, $subject, $content);
        } catch (\Exception $e) {
            return false;
        }
    }
}
