<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 12/19/2018
 * Time: 3:48 PM
 */

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Entity\User;
use App\Entity\Campaign;
use App\Entity\GroupMail;
use App\Entity\MailAutomation;
use App\Ultility\Error;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;




class SettingCampaignController extends AdminController
{
	protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'cskh');

            return $next($request);
        });

    }

    public function index(){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        return view('admin.SettingCampaign.index');
    }

     public function anyDatatables() {
        $campaigns = Campaign::select([
            'campaign_id',
            'campaign_name',
            'description'
        ])
        ->where('theme_code', $this->themeCode)
        ->where('user_email', $this->emailUser);

        return Datatables::of($campaigns)
            ->addColumn('action', function($campaign) {
                $string =  '<a href="'.route('edit_campaign',['id' => $campaign->campaign_id]).'">
                           <button class="btn btn-primary">
                           <i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('delete_campaign',['id' => $campaign->campaign_id]).'" class="btn btn-danger btnDelete" 
                            ">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';

                return $string;

            })
            ->orderColumn('campaign_id', 'campaign_id desc')
            ->make(true);
            // <a  href="'.route('destroy_mail', ['id' => $email->id]).'" class="btn btn-danger btnDelete" 
            //                 data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
            //                    <i class="fa fa-trash-o" aria-hidden="true"></i>
            //                 </a>

    }

    public function showAdd(){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->get();

        $emailModel = new MailAutomation();
        $emails = $emailModel->where('theme_code',$this->themeCode)
        ->where('user_email',$this->emailUser)
        ->paginate(5);

        return view('admin.SettingCampaign.add',compact('groupMails','emails'));
    }

     public function store(Request $request){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		} 
		 
        //try {
         $campaingn = Campaign::insertGetId([
            'campaign_name' =>$request->input('name'),
            'description' =>$request->input('description'),
            'group_id' =>$request->input('group'),
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

		return redirect()->route('edit_campaign', ['id' => $campaingn]);

        // }
        // catch (\Exception $e) {
        //     Error::setErrorMessage('Lỗi xảy ra : dữ liệu không hợp lệ.');
        //     Log::error('http->admin->SettingCampaignController->index: Lỗi xảy ra trong quá trình tạo mới bài viết');

        //     return redirect('admin/admin/settingCampaign')->with('delete','Xóa chiến dịch thành công ');
        // }
	}	

    public function edit($id ,Request $request){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}

        $campaign = Campaign::where('campaign_id',$id)
        ->select([
            'campaign_id',
            'campaign_name',
            'description',
            'group_id' 
        ])
        ->where('theme_code', $this->themeCode)
        ->where('user_email', $this->emailUser)
        ->first();

        $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->get();

        $emailModel = new MailAutomation();
        $emails = $emailModel->where('theme_code',$this->themeCode)
        ->where('user_email',$this->emailUser)
        ->paginate(5);

        return view('admin.SettingCampaign.edit',compact('campaign','groupMails','emails'));

    }   
    
     public function update($id,Request $request){
        try {
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
         $campaingn = Campaign::where('campaign_id',$id)
         ->update([
            'campaign_name' =>$request->input('name'),
            'description' =>$request->input('description'),
            'group_id' =>$request->input('group'),
        ]);

        return redirect(route('SettingCampaign'))->with('update','Chỉnh sửa chiến dịch thành công ');

        }
        catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra : dữ liệu không hợp lệ.');
            Log::error('http->admin->SettingCampaignController->index: Lỗi xảy ra trong quá trình tạo mới bài viết');

            return redirect('admin/settingCampaign');
        }

    }   

     public function delete($id){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $campaingn = Campaign::where('campaign_id',$id)
        ->first()
        ->delete();
        return redirect(route('SettingCampaign'))->with('remove','Xóa chiến dịch thành công ');
    }
}