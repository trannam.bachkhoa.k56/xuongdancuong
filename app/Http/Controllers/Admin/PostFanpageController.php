<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 4/25/2018
 * Time: 9:52 AM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\PostFacebook;
use App\Entity\User;
use App\Facebook\Fanpage;
use App\Facebook\People;
use App\Entity\Post;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Ultility\InforFacebook;
use App\Entity\FanpageSocial;



class PostFanpageController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            return $next($request);
        });
    }

    public function uploadFanpage(Request $request) {
         // try {
            $postFacebokModel = new PostFacebook();
            $postFacebok = $postFacebokModel->where('post_id', $request->input('post_id'))->first();
            // nếu chưa tồn tại thì insert
            if (empty($postFacebok)) {
                if (!empty($request->input('fanpages'))) {
                    $faceIds = $this->uploadToFanpage($request);
                }

                if ($request->input('post_me') == 1) {
                    $postIdOld = $this->uploadToMe($request);
                }
                $postFacebokModel->insert([
                    'post_id' => $request->input('post_id'),
                    'content' => $request->input('content'),
                    'images' => $request->input('images'),
                    'link' => $request->input('link'),
                    'name_album' => $request->input('name_album'),
                    'fanpages' => !empty($request->input('fanpages')) ? implode(',', $request->input('fanpages')) : '',
                    'post_me' => $request->has('post_me') ? $request->input('post_me') : 0,
                    'face_ids' => isset($faceIds) ? $faceIds : '',
                    'post_me_id_album' => isset($postIdOld) ? $postIdOld : '',
                    'created_at' => new \DateTime()
                ]);

                return redirect($request->input('current_url'));
            }
			if (!empty($request->input('fanpages'))) {
				$faceIds = $this->uploadToFanpage($request, $postFacebokModel->face_ids);
			}
			
            if ($request->input('post_me') == 1) {
                $postIdOld = $this->uploadToMe($request, $postFacebok->post_me_id_album);
            }

            // nếu tồn tại thì cập nhật lại thông tin
            $postFacebok->update([
                'content' => $request->input('content'),
                'images' => $request->input('images'),
                'link' => $request->input('link'),
                'name_album' => $request->input('name_album'),
                'fanpages' => !empty($request->input('fanpages')) ? implode(',', $request->input('fanpages')) : '',
                'post_me' => $request->has('post_me') ? $request->input('post_me') : 0,
                'face_ids' => isset($faceIds) ? $faceIds : '',
                'post_me_id_album' => isset($postIdOld) ? $postIdOld : '',
                'updated_at' => new \DateTime()
            ]);

            return redirect($request->input('current_url'));
         // } catch (\Exception $e) {
             // Error::setErrorMessage('Lỗi xảy ra khi tạo mới bài viết: dữ liệu không hợp lệ.');
             // Log::error('http->admin->PostController->create: Lỗi xảy ra trong quá trình tạo mới bài viết');

             // return redirect('admin/home');
         // }
    }

    private function uploadToMe($request, $postIdOld = '') {
        $people = new People();
        // up to me wall

        $nameAlbum = $request->input('name_album');
        $linkFacebook = $request->input('link');

        $postIdOld =  $people->upToMe(
            $postIdOld,
            mb_convert_encoding($request->input('content'), 'UTF-8'),
            $request->input('images'),
            $linkFacebook,
            $nameAlbum
        );
        return $postIdOld;
    }
    private function uploadToFanpage($request, $facebookSave = '') {
        $fanpagePosts = $request->input('fanpages');
        $fanpage = new Fanpage();

        $informationFanpage = $fanpage->getInforFanpage($fanpagePosts, $facebookSave);
		
		$fanpageSaveArray = explode(';', $informationFanpage);
		
        $fanpageSave = $fanpage->postFanpage(
            $fanpageSaveArray,
            mb_convert_encoding($request->input('content'), 'UTF-8'),
            $request->input('images'),
            $request->input('link'),
            $request->input('name_album')
        );

        return $fanpageSave;

    }
	
	 public function getPostFanpage($pageId){
		//cấu hình
	
		$app = new InforFacebook();
		$fb = new \Facebook\Facebook([
			'app_id' => $app->getAppId(),
			'app_secret' => $app->getAppSecret(),
			'default_graph_version' => $app->getDefaultGraphVersion()
		]);

		$userSocial = FanpageSocial::join('social_accounts', 'social_accounts.user_id', 'fanpage_social.user_id' )
		->where('fanpage_id',$pageId)
		->first();
		
		$userToken = $userSocial->token;
		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $response = $fb->get(
			'/'. $pageId .'/feed',
			$userToken
		  );
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		$graphEdge = $response->getGraphEdge();
		
		 return view('admin.facebook.post_item',compact('graphEdge','userToken'));

	 }
	 
	 public function savePostFanpage(Request $request){
		 
		$postId = $request->input('slug');
		$token = $request->input('access_token');
		
		$picture = FanpageSocial::getImagePost($postId, $token);

		 // insert to database
            $post = new Post();
            $postId = $post->insertGetId([
                'title' => $request->input('title'),
                'post_type' => 'post',
				'slug' => $request->input('slug'),
                'template' =>  'default',
                'description' => $request->input('description'),
                'image' =>  $picture['full_picture'],
                'content' =>  $request->input('content'),
                'visiable' => 0,
                'user_email' => $this->emailUser,
                'theme_code' => $this->themeCode,
                'created_at' => new \Datetime(),
                'updated_at' => new \Datetime(),
				
            ]);

			return redirect()->back();
		 
		 
	 }
	

}