<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Domain;
use App\Entity\GroupTheme;
use App\Entity\Theme;
use App\Entity\ThemeUse;
use App\Entity\User;
use App\Ultility\Error;
use App\Ultility\Ultility;
use App\Entity\PaymentInfomation;
use App\Entity\HistoryPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;

class ThemeWebsiteController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'websites');

            return $next($request);
        });
    }
    
    public function activeTheme() {
        try {
            $groupThemes = GroupTheme::orderBy('group_id', 'desc')->get();
            $themes = Theme::orderBy('theme_id', 'desc')
            ->where('is_sale', 1)->get();
            $domainUses = Domain::showDomainWithUser();
            $themeUses = array();
            foreach ($domainUses as $domain) {
                $themeUses[] = $domain->theme_id;
            }

            $user = Auth::user();
            $themeUserUses = ThemeUse::select('theme_code')->where('user_id', $user->id)->get();
            $themeCodeUse = array ();
            foreach($themeUserUses as $themeUserUse) {
                $themeCodeUse[] = $themeUserUse->theme_code;
            }

            return view('admin.theme.theme_active', compact('groupThemes', 'themes', 'themeUses', 'domainUses', 'user', 'themeCodeUse'));
        } catch(\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xóa danh mục: dữ liệu xóa không hợp lệ.');
            Log::error('http->admin->ThemeController->activeTheme: Lỗi xảy tra trong quá trình xóa danh mục');

            return redirect('admin/home');
        }
    }

    public function changeTheme(Request $request) {
        // try {
            if (!Auth::check()) {
                return response([
                    'status' => 500,
                    'message' => 'Bạn chưa đăng nhập, hoặc lỗi trong quá trình đăng nhập!',
                ])->header('Content-Type', 'text/plain');
            }

            DB::beginTransaction();
            $user = Auth::user();

            // user không có quyền thay đổi theme
            if ($user->vip == 0) {
                return response([
                    'status' => 500,
                    'message' => 'Bạn không có quyền thay đổi theme vì gói sử dụng!',
                ])->header('Content-Type', 'text/plain');
            }

            $themeId = $request->input('theme_id');

            if (empty($themeId)) {
                return response([
                    'status' => 500,
                    'message' => 'Đã có lỗi xảy ra, vui lòng chọn lại theme',
                ])->header('Content-Type', 'text/plain');
            }

            // cập nhật thông tin
            $theme = Theme::where('theme_id', $themeId)->first();

            // kiểm tra xem đủ phí chi trả mua theme không?
            $paymentInformation =  PaymentInfomation::getWithUser(Auth::user()->id);

            // nếu không đủ yêu cầu họ phải nạp tiền.
            if (empty($paymentInformation ) || $paymentInformation->coin_total < $theme->price) {
                return response([
                    'status' => 500,
                    'message' => 'Tiền trong tài khoản của bạn không đủ để thực hiện giao dịch. Vui lòng truy cập <a href="admin/tom-luoc-thanh-toan">nạp tiền</a> để nạp tiền vào tài khoản.',
                ])->header('Content-Type', 'text/plain');
            }

            // nếu đủ thì trừ tiền của họ. 
            PaymentInfomation::where('user_id', Auth::user()->id)->update([
                'coin_total' => ($paymentInformation->coin_total - $theme->price),
                'updated_at' => new \DateTime()
            ]);

            HistoryPayment::insert([
                'content' => 'Trừ tiền nâng cấp giao diện '.$theme->name,
                'theme_code' => $this->themeCode,
                'user_email' =>$this->emailUser,
                'money' => $theme->price,
                'created_at' => new \DateTime()
            ]);

            DB::commit();
            return response([
                'status' => 200,
                'message' => 'Tính năng thay đổi giao diện đang phát triển!',
            ])->header('Content-Type', 'text/plain');

            return response([
                'status' => 200,
                'message' => 'Bạn đã thay đổi thành công, cảm ơn bạn đã sử dụng dịch vụ của chúng tôi!',
            ])->header('Content-Type', 'text/plain');

        // } catch (\Exception $e) {
        //     DB::rollback();
        //     Log::error('http->admin->ThemeController->changeTheme: Lỗi xảy tra trong quá trình thay đổi theme');

        //     return response([
        //         'status' => 500,
        //         'message' => 'Có lỗi gì đó xảy ra trong quá trình thay đổi theme.!',
        //     ])->header('Content-Type', 'text/plain');
        // }

    }
}
