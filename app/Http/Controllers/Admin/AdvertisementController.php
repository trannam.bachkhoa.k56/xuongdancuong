<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 9/3/2019
 * Time: 4:47 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use App\Entity\Theme;
use App\Entity\Domain;
use App\Entity\Advertisement;
use App\Entity\CustomerDisplay;
use App\Facebook\Comment;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Facebook\Fanpage;
use App\Facebook\People;

class AdvertisementController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'websites');

            return $next($request);
        });
    }


    public function index(Request $request) {
        return view('admin.advertisement.list');
    }

    public function store(Request $request) {

        $adv = Advertisement::insertGetId([
            'post_id' => $request->input('post_id'),
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'website' => $request->input('website'),
            'message' => $request->input('message'),
            'method' => $request->input('method'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        ]);

        return redirect(route('advertisement.index'));
    }

    public function edit($id) {

         $adv = Advertisement::join('posts', 'posts.post_id', '=', 'advertisement.post_id')
         ->select('advertisement.*', 'posts.title', 'posts.post_id')         
         ->where('advertisement.adv_id', $id)
         ->first();

        return view('admin.advertisement.edit',compact('adv'));

    }

    public function update(Request $request, $id) {

        $adv = Advertisement::where('adv_id', $id)
        ->update([
            // 'post_id' => $request->input('post_id'),
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'website' => $request->input('website'),
            'message' => $request->input('message'),
            'method' => $request->input('method'),
            'total_money' => $request->input('total_money'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        ]);

        return redirect(route('advertisement.index'));
    }

    public function show($id) {
        // return view('admin.config_template.category_product');
    }

    public function destroy($id) {
        $adv = Advertisement::where('adv_id', $id)
        ->delete();
        return redirect(route('advertisement.index'));
    }

    public function anyDatatables(Request $request){
        $advs = Advertisement::join('posts', 'posts.post_id', '=', 'advertisement.post_id')
            ->select('advertisement.*', 'posts.title', 'posts.post_id', 'posts.views', 'posts.submit_form')
            ->get();
        return Datatables::of($advs)
            ->addColumn('action', function($adv) {
                $string =  '<a href="'.route('advertisement.edit', ['id' => $adv->adv_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="/admin/advertisement/'.$adv->adv_id.'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })

            ->orderColumn('adv_id', 'adv_id desc')
            ->make(true);
    } 
    
}