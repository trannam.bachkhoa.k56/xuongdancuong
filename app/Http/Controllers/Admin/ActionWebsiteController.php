<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\Input;
use App\Entity\Template;
use App\Entity\TypeInput;
use App\Entity\User;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;
use App\Ultility\Ultility;

class ActionWebsiteController extends AdminController
{
    protected $role;
	protected $accessTokenFacebook = '';

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		
    }


}
