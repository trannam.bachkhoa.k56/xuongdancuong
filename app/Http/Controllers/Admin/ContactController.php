<?php

namespace App\Http\Controllers\Admin;

use App\Entity\CheckSendMail;
use App\Entity\Contact;
use App\Entity\EmailContactCampaign;
use App\Entity\NoteContact;
use App\Entity\OrderContact;
use App\Entity\OrderCustomer;
use App\Entity\RemindContact;
use App\Entity\SettingGetfly;
use App\Ultility\CheckPermission;
use App\Ultility\Error;
use Illuminate\Http\Request;
use App\Entity\User;
use Carbon\Carbon;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Psy\Util\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Validator;
use Yajra\Datatables\Datatables;

class ContactController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) { 
				return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }

    public function contact () {
        return view ('admin.contact.list');
    }

    public function anyDatatable () {
        $contacts = Contact::leftJoin ('order_customer', 'order_customer.contact_id', 'contact.contact_id')
            ->leftJoin('contact as contact_affiliate', 'contact_affiliate.contact_id', 'contact.affiliate_person')
            ->select(
                'contact.*',
                'contact_affiliate.name as affiliate_name',
                'contact_affiliate.contact_id as affiliate_id',
                DB::raw("SUM(order_customer.total_price) as totalprice")
            )
            ->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->groupBy(
                'contact.contact_id',
                'contact.name',
                'contact_affiliate.name',
                'contact_affiliate.contact_id',
                'contact.phone',
                'contact.email',
				'contact.affiliate_money',
                'contact.address',
                'contact.utm_source',
                'order_customer.total_price'
            )->distinct();

        return Datatables::of($contacts)
            ->addColumn('action', function($contact) {
                $string =  '<a href="'.route('contact.edit', ['contact_id' => $contact->contact_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="/admin/contact/'.$contact->contact_id.'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->addColumn('convention', function ($contact){
                $noteContact = NoteContact::where('contact_id', $contact->contact_id)->first();

                if (!empty($noteContact) ) {
                    if (!empty($noteContact->content)) {
                        return $noteContact->content;
                    }

                    $date = date_create($noteContact->created_at);

                    return 'Ngày khởi tạo: '.date_format($date,"d/m/Y H:i");
                }

                return 'Chưa trao đổi!';
            })
            ->addColumn('dayConn', function ($contact){
                return $this->dayConnNoteContact($contact->contact_id);
            })
            ->orderColumn('contact.contact_id', 'contact.contact_id desc')
            ->make(true);
    }

    private function dayConnNoteContact($contactID) {
        $note = NoteContact::where('contact_id',$contactID)->orderBy('note_contact_id','desc')->first();
        $diff = '';
        if(empty($note)){
            $contact = Contact::where('contact_id', $contactID)->first();
            $noteDate = date('Y-m-d', strtotime($contact->created_at));
            $timeNow = Carbon::now('Asia/Ho_Chi_Minh');
            if($timeNow->toDateString() == $noteDate) {
                return $diff = '0';
            }
            $diff = $timeNow->diffInDays($noteDate).'';

            return $diff;
        }
        $noteDate = date('Y-m-d', strtotime($note->created_at));
        $timeNow = Carbon::now('Asia/Ho_Chi_Minh');
        if($timeNow->toDateString() == $noteDate) {
            return $diff = '0';
        }
        $diff = $timeNow->diffInDays($noteDate).'';
        return $diff;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		// if(Auth::user()->vip == 0 ){
			 // return view('admin.upgrade.upgrade');
		// }
        $contact = new Contact();
        try {
            $messageErrorVip = CheckPermission::checkContacts(Auth::user());
            if (!empty($messageErrorVip)) {
                return view('admin.contact.list', compact('messageErrorVip'));
            }

            $contacts = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->select(
                    'contact.contact_id',
                    'contact.name',
                    'contact.email',
                    'contact.phone',
                    'contact.address',
                    'contact.message',
                    'contact.status',
                    'contact.color',
                    'contact.created_at',
                    'contact.updated_at',
                    DB::raw("SUM(order_contact.price) as sum_order")
                )
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->groupBy(
                   'contact.contact_id',
                   'contact.name',
                   'contact.email',
                   'contact.phone',
                   'contact.address',
                   'contact.message',
                   'contact.color',
                   'contact.status',
                   'contact.created_at',
                   'contact.updated_at'
               )
                ->orderBy('contact.contact_id', 'desc')
                ->paginate(50);

            // lấy ra số khách hàng mới
            $null = array('NULL', 0);
            $countContactNews = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->count();
            // tính tổng số tiền của khách hàng
            $sumPriceContactNews = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->sum('order_contact.price');

            // tính số khách hàng tiếp cận
            $countContactApproachs = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 1)
                ->count();

            // tính tổng tiền khách hàng tiếp cận
            $sumPriceContactApproachs = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 1)
                ->sum('order_contact.price');

            // tính số khách hàng đã đặt hàng
            $countContactOrders = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 2)
                ->count();

            // tính tổng tiền khách hàng đặt hàng
            $sumPriceContactOrders = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 2)
                ->sum('order_contact.price');

            // tính số khách hàng đã hoàn đơn
            $countContactReturns = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 3)
                ->count();

            // tính tổng tiền khách hàng hoàn đơn
            $sumPriceContactReturns = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 3)
                ->sum('order_contact.price');


            return view('admin.contact.list_kanban', compact(
                'contacts',
                'countContactNews',
                'countContactApproachs',
                'countContactOrders',
                'countContactReturns',
                'sumPriceContactNews',
                'sumPriceContactApproachs',
                'sumPriceContactOrders',
                'sumPriceContactReturns'

            ));

       } catch (\Exception $e) {
           Error::setErrorMessage('Lỗi xảy ra khi hiển thị liên hệ: dữ liệu lỗi.');

           Log::error('http->admin->ContactController->index: Lỗi lấy dữ liệu contacts');

           $contacts = array();

           return view('admin.contact.list_kanban', compact('contacts'));
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $messageErrorVip = CheckPermission::checkContacts(Auth::user());
        if (!empty($messageErrorVip)) {
            return redirect(route('contact.index', ['messageErrorVip' => $messageErrorVip]));
        }

        return view('admin.contact.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $validation = Validator::make($request->all(), [
//        'name' => 'required',
//        'phone' => 'required',
//        'email' => 'required|email',
//        'address' => 'required',
//        ]);
//
//        // if validation fail return error
//        if ($validation->fails()) {
//            return redirect(route('contact.create'))
//                ->withErrors($validation)
//                ->withInput();
//        }

        // if slug null slug create as title
        $this->insertContact($request);

        return redirect(route('contact.index'));
    }

    private function insertContact($request) {
        // try {
            $contact = new Contact();
            $contact->insert([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'message' => $request->input('message'),
                'color' => $request->input('color'),
				'utm_source' => $request->input('utm_source'),
                'tag' => $request->has('tag') ? $request->input('tag') : '',
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        // } catch(\Exception $e) {
            // Error::setErrorMessage('Lỗi xảy ra khi thêm mới liên hệ: Dữ liệu nhập vào không hợp lệ');
            // Log::error(' http->admin->ContactController->insertContact: Lỗi thêm mới liên hệ');
        // }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        if ($contact->theme_code != $this->themeCode || $contact->user_email != $this->emailUser) {
            return redirect(route('contact.index'));
        }

        $contact = $contact->leftJoin('campaign_customer','campaign_customer.campaign_customer_id','contact.campaign_id')
        ->leftJoin('process','process.campain_customer_id','contact.campaign_id')
        ->leftJoin('users','users.id','contact.user_responsibility')
        ->where('contact.contact_id',$contact->contact_id)
        ->select(
            'contact.*',
            'campaign_customer.campaign_title',
            'process.process_name',
            'users.name as user_responsibility'
        )
        ->first();

        $noteContacts = NoteContact::where('contact_id', $contact->contact_id)
            ->orderBy('note_contact_id', 'desc')
            ->get();

        $remindContacts = RemindContact::where('contact_id', $contact->contact_id)
            ->orderBy('remind_contact_id', 'desc')
            ->get();

        $orderContacts = OrderContact::where('contact_id', $contact->contact_id)
            ->orderBy('order_contact_id', 'desc')
            ->get();

        $orderCustomer = new OrderCustomer();
        $orderCustomers = $orderCustomer->join('contact', 'contact.contact_id', 'order_customer.contact_id')
            ->leftJoin('users', 'users.id', 'order_customer.user_id')
            ->select(
                'users.name as user_name',
                'contact.name as contact_name',
                'order_customer.*'
            )
            ->where('order_customer.theme_code', $this->themeCode)
            ->where('order_customer.user_email', $this->emailUser)
            ->where('order_customer.contact_id', $contact->contact_id)->get();

        $emailCampaigns = EmailContactCampaign::getCampaignByContactId($contact->contact_id);
        //$historyCalls = CCallController::callCDR($contact->phone);
        $file = @fopen('log.txt', 'r');

        $email = new CheckSendMail();
        $emailSeenArray = [];
        if(!$file) {
            return view('admin.contact.show', compact(
                'contact',
                'noteContacts',
                'remindContacts',
                'orderContacts',
                'historyCalls',
                'emailCampaigns',
                'emailSeenArray',
                'orderCustomers'
                ));
        }

        while(!feof($file)) {
            $stringInLine =  fgets($file);
            $contentLineArray = explode(';', $stringInLine);

            if (isset($contentLineArray[0], $contentLineArray[1], $contentLineArray[2])) {
                $email->insertToDB($contentLineArray[0] , $contentLineArray[1], $contentLineArray[2], $contentLineArray[3], $this->emailUser);
            }
        }
		
        foreach($email->where('author', $this->emailUser)->where('receiver', $contact->email)->get(['seen_at']) as $key => $emailSeen){
            $emailSeenToArray[] = explode('-', $emailSeen->seen_at);
            $emailSeenArray[] = $emailSeenToArray[$key][0] . ' ngày ' . $emailSeenToArray[$key][1];
        }

        return view('admin.contact.show', compact('contact', 'noteContacts', 'remindContacts', 'orderContacts', 'emailCampaigns', 'emailSeenArray', 'orderCustomers'));
    }

    public function minusMoney (Request $request) {
        $contactId = $request->input('contact_id');
        $money = $request->input('money');

        $contact = Contact::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('contact_id', $contactId)->first();

        if (empty($contact)) {
            return redirect(route('contact.index'));
        }

        Contact::where('contact_id', $contactId)->update([
            'money' => ($contact->money - $money)
        ]);

        return redirect()->back();
    }

    public function qrCode (Request $request) {
        $link = $request->input('link');
        $affiliate = $request->input('affiliate');
		$nameCard = $request->input('name_card');

        $linkAffiliate = (empty($this->domainUser->domain) ? $this->domainUser->url : $this->domainUser->domain ).'/xac-minh-so-dien-thoai?link='.$link;
        /*User::where('id', Auth::user()->id)->update([
            'affiliate_money' => $affiliate
        ]); */

        $imageQr = QrCode::format('png')->size(250)
            ->errorCorrection('H')->generate($linkAffiliate);
        $imageQr = 'data:image/png;base64, '.base64_encode($imageQr);
		
        return view('admin.contact.affiliate', compact('linkAffiliate', 'affiliate', 'nameCard', 'imageQr'));
    }

    public function deleteAll (Request $request) {
        $idDeletes = $request->input('id_delete');

        $idDeletesToken = explode(',', $idDeletes);
        foreach ($idDeletesToken as $idDelete) {
            if (!empty($idDelete)) {
                Contact::where('contact_id', $idDelete)->delete();

                NoteContact::where('contact_id', $idDelete)
                    ->delete();

                RemindContact::where('contact_id', $idDelete)
                    ->delete();

                OrderContact::where('contact_id', $idDelete)
                    ->delete();
            }
        }

        return redirect('admin/danh-sach-khach-hang');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        if ($contact->theme_code != $this->themeCode || $contact->user_email != $this->emailUser) {
            return redirect(route('contact.index'));
        }

        return view('admin.contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact  $contact)
    {
        if ($contact->theme_code != $this->themeCode || $contact->user_email != $this->emailUser) {
            return redirect(route('contact.index'));
        }

//        $validation = Validator::make($request->all(), [
//            'name' => 'required',
//            'phone' => 'required',
//            'email' => 'required|email',
//            'address' => 'required',
//        ]);
//
//        // if validation fail return error
//        if ($validation->fails()) {
//            return redirect(route('contact.edit', ['contact_id' => $contact->contact_id]))
//                ->withErrors($validation)
//                ->withInput();
//        }
        $this->updateContact($contact, $request);


        return redirect('/admin/contact/'.$contact->contact_id);
    }

    private function updateContact($contact, $request) {
        // try {
            $contact->update([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'tag' => $request->input('tag'),
                'message' => $request->input('message'),
                'color' => $request->input('color'),
                'utm_source' => $request->input('utm_source'),
                'affiliate_money' => $request->input('affiliate_money'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        // } catch (\Exception $e) {
            // Error::setErrorMessage('Lỗi xảy khi cập nhật liên hệ: Dữ liệu nhập vào không hợp lệ');
            // Log::error('http->admin->ContactController->updateContact: Lỗi khi cập nhật liên hệ');
        // }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact  $contact)
    {
        if ($contact->theme_code != $this->themeCode || $contact->user_email != $this->emailUser) {
            return redirect(route('contact.index'));
        }

        $contact->delete();

        NoteContact::where('contact_id', $contact->contact_id)
            ->delete();

        RemindContact::where('contact_id', $contact->contact_id)
            ->delete();

        OrderContact::where('contact_id', $contact->contact_id)
            ->delete();

        return redirect(route('contact.index'));
    }

    public function updateStatus (Request $request){
        if($request->input('status') == 0){
            $status = null;
        }
        else{
            $status = $request->input('status');
        }
        Contact::where('contact_id', $request->input('contact_id'))->update([
            'status' => $status,
        ]);
    }

    public function noteContact (Request $request) {
        $contactId = $request->input('contact_id');
        $note = $request->input('note');

        NoteContact::insert([
            'contact_id' => $contactId,
            'content' => $note,
            'created_at' => new \DateTime()
        ]);

        return redirect()->back();
    }

    public function remindContact (Request $request) {
        RemindContact::insert([
            'contact_id' => $request->input('contact_id'),
            'content' => $request->input('note'),
            'date' => new \DateTime($request->input('date')),
            'status' => 0,
            'created_at' => new \DateTime(),
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
        ]);

        return redirect()->back();
    }

    public function updateRemindContact(Request $request) {
        RemindContact::where('remind_contact_id', $request->input('remind_contact_id'))->update([
            'status' => $request->input('status'),
        ]);

    }

    public function orderContact(Request $request) {
        OrderContact::insert([
            'contact_id' => $request->input('contact_id'),
            'content' => $request->input('content'),
            'price' => str_replace('.', '', $request->input('price')),
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
            'created_at' => new \DateTime(),
        ]);

        return redirect()->back()->with('order', 'success!');;
    }

    public function deleteOrderContact($orderContactId) {
        OrderContact::where('order_contact_id', $orderContactId)->delete();

        return redirect()->back()->with('order', 'success!');
    }

    public function storeContactEveryWhere(Request $request) {
        // Check xem khách hàng bạn vừa thêm đã tồn tại hay chưa
        $customer = Contact::where('name', $request->name);
        $customer = $customer->where('email', $request->email)
                             ->orWhere('phone', $request->phone)
                             ->first();
        if ($customer) {
            if ($request->has('is_json')) {
                return response([
                    'status' => 500,
                    'message' => 'Khách hàng bạn vừa thêm đã tồn tại. Mời bạn xem ở phần khách hàng.',
                ])->header('Content-Type', 'text/plain');
            }
            return redirect(route('contact.show',['contact_id' => $customer->contact_id]));
        }

        //success
        $messages = $request->input('message');
        $contact = new Contact();
        $contact->insert([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'address' => $request->has('address') ?  $request->input('address') : '',
			'utm_source' => $request->has('utm_source') ? $request->input('utm_source') : Ultility::getCurrentLink(),
            'message' => is_array($messages) && count($messages) > 1 ? implode('-', $messages) : $messages,
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        // Đồng bộ chiến dịch lên getfly
        SettingGetfly::addNewCampaignGetfly(
            $request->input('name'),
            $request->input('phone'),
            $request->input('email'),
            is_array($messages) && count($messages) > 1 ? implode('-', $messages) : $messages,
            $request->has('utm_source') ? $request->input('utm_source') : Ultility::getCurrentLink(),
            Ultility::getCurrentLink()
        );


        if ($request->has('is_json')) {
            return response([
                'status' => 200,
                'message' => 'Cảm ơn bạn đã liên hệ cho chúng tôi, chúng tôi sẽ sớm phản hồi sớm nhất.',
            ])->header('Content-Type', 'text/plain');
        }

        return view($this->themeCode.'.default.contact');
    }

    public function searchCustomer(Request $request){

        $user = $request->user();
        $messageErrorVip = CheckPermission::checkContacts($user);
        if (!empty($messageErrorVip)) {
            return view('admin.contact.list', compact('messageErrorVip'));
        }
        $name = $request->input('name');
        $tag = $request->input('tag');
        $contact = new Contact();
        if (!$name && !$tag ) {
            $contacts = $this->dataJoinTable($contact, [
               'name' => $name,
               'tag' => $tag
            ]);

            $countContactNews = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.name', $name)
                ->where('contact.tag', $tag)
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->count();

            $sumPriceContactNews = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.name', $name)
                ->where('contact.tag', $tag)
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->sum('order_contact.price');

            $countContactApproachs = $this->countContact($contact, 1, [
                [ 'name' => $name ],
                [ 'tag' => $tag ]
            ]);

            $sumPriceContactApproachs = $this->sumMoney($contact, 1, [
                [ 'name' => $name ],
                [ 'tag' => $tag ]
            ]);

            $countContactOrders = $this->countContact($contact, 2, [
                ['name' => $name],
                ['tag' => $tag]
            ]);

            $sumPriceContactOrders = $this->sumMoney($contact, 2, [
                [ 'name' => $name ],
                [ 'tag' => $tag ]
            ]);

            $countContactReturns = $this->countContact($contact, 3, [
                ['name' => $name],
                ['tag' => $tag]
            ]);

            $sumPriceContactReturns = $this->sumMoney($contact, 3, [
                [ 'name' => $name ],
                [ 'tag' => $tag ]
            ]);

            return view('admin.contact.search_customer', compact(
                'contacts',
                'countContactNews',
                'countContactApproachs',
                'countContactOrders',
                'countContactReturns',
                'sumPriceContactNews',
                'sumPriceContactApproachs',
                'sumPriceContactOrders',
                'sumPriceContactReturns',
                'name',
                'tag'
            ));
        }

        if ($name && !$tag) {
            $contacts = $this->dataJoinTable($contact, [
                ['name', 'LIKE', '%' . $name . '%']

            ]);

            $countContactNews = $contact->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->where('name', 'LIKE', '%' . $name . '%')
                //->where('contact.tag','like','%'.$time.'%')
                ->where(function ($query) {
                    $query ->where('status', 0)
                        ->orWhereNull('status');
                })
                ->count();

            $sumPriceContactNews = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.name', 'LIKE', '%' . $name . '%')
                //->where('contact.updated_at','like','%'.$time.'%')
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->sum('order_contact.price');

            $countContactApproachs = $this->countContact($contact, 1, [
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $sumPriceContactApproachs = $this->sumMoney($contact, 1, [
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $countContactOrders = $this->countContact($contact, 2, [
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $sumPriceContactOrders = $this->sumMoney($contact, 2, [
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $countContactReturns = $this->countContact($contact, 3, [
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $sumPriceContactReturns = $this->sumMoney($contact, 3, [
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            return view('admin.contact.search_customer', compact(
                'contacts',
                'countContactNews',
                'countContactApproachs',
                'countContactOrders',
                'countContactReturns',
                'sumPriceContactNews',
                'sumPriceContactApproachs',
                'sumPriceContactOrders',
                'sumPriceContactReturns',
                'name',
                'tag'
            ));

        }

        if (!$name && $tag) {
            $contacts = $this->dataJoinTable($contact, [
                [ 'tag', 'LIKE', '%' . $tag . '%']
            ]);

            $countContactNews = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.tag', 'LIKE', '%' . $tag . '%')
                //->where('contact.updated_at','like','%'.$time.'%')
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->count();

            $sumPriceContactNews = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.tag', 'LIKE', '%' . $tag . '%')
                //->where('contact.updated_at','like','%'.$time.'%')
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->sum('order_contact.price');

            $countContactApproachs = $this->countContact($contact, 1, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ]
            ]);

            $sumPriceContactApproachs = $this->sumMoney($contact, 1, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ]
            ]);

            $countContactOrders = $this->countContact($contact, 2, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ]
            ]);

            $sumPriceContactOrders = $this->sumMoney($contact, 2, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ]
            ]);

            $countContactReturns = $this->countContact($contact, 3, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ]
            ]);

            $sumPriceContactReturns = $this->sumMoney($contact, 3, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ]
            ]);

            return view('admin.contact.search_customer', compact(
                'contacts',
                'countContactNews',
                'countContactApproachs',
                'countContactOrders',
                'countContactReturns',
                'sumPriceContactNews',
                'sumPriceContactApproachs',
                'sumPriceContactOrders',
                'sumPriceContactReturns',
                'name',
                'tag'
            ));
        }

        if ($name && $tag) {
            $contacts = $this->dataJoinTable($contact, [
                ['name', 'LIKE', '%' . $name . '%'],
                ['tag', 'LIKE', '%' . $tag . '%' ]
            ]);

            $countContactNews = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.tag', 'LIKE', '%' . $tag . '%')
                ->where('contact.name', 'LIKE', '%' . $name . '%')
                //->where('contact.updated_at','like','%'.$time.'%')
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->count();

            $sumPriceContactNews = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.tag', 'LIKE', '%' . $tag . '%')
                ->where('contact.name', 'LIKE', '%' . $name . '%')
                //->where('contact.updated_at','like','%'.$time.'%')
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                })
                ->sum('order_contact.price');

            $countContactApproachs = $this->countContact($contact, 1, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ],
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $sumPriceContactApproachs = $this->sumMoney($contact, 1, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ],
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $countContactOrders = $this->countContact($contact, 2, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ],
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $sumPriceContactOrders = $this->sumMoney($contact, 2, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ],
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $countContactReturns = $this->countContact($contact, 3, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ],
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            $sumPriceContactReturns = $this->sumMoney($contact, 3, [
                [ 'tag', 'LIKE', '%' . $tag . '%' ],
                [ 'name', 'LIKE', '%' . $name . '%' ]
            ]);

            return view('admin.contact.search_customer', compact(
                'contacts',
                'countContactNews',
                'countContactApproachs',
                'countContactOrders',
                'countContactReturns',
                'sumPriceContactNews',
                'sumPriceContactApproachs',
                'sumPriceContactOrders',
                'sumPriceContactReturns',
                'name',
                'tag'
            ));
        }
        return redirect()->back();
    }

    public function filterContactForTime(Request $request){
          $time_input = $request->input('time');

          $date_start = $request->input('start_date');
          $date_end = $request->input('end_date');

          $contact = new Contact();
//        try {
            $messageErrorVip = CheckPermission::checkContacts(Auth::user());
            if (!empty($messageErrorVip)) {
                return view('admin.contact.list', compact('messageErrorVip'));
            }

            $contacts = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->select(
                    'contact.contact_id',
                    'contact.name',
                    'contact.email',
                    'contact.phone',
                    'contact.address',
                    'contact.message',
                    'contact.status',
                    'contact.color',
                    'contact.created_at',
                    'contact.updated_at',
                    DB::raw("SUM(order_contact.price) as sum_order")
                )
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->groupBy(
                   'contact.contact_id',
                   'contact.name',
                   'contact.email',
                   'contact.phone',
                   'contact.address',
                   'contact.message',
                   'contact.color',
                   'contact.status',
                   'contact.created_at',
                   'contact.updated_at'
               );
            // lấy ra số khách hàng mới
            $null = array('NULL', 0);
            $countContactNews = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                });
              
            // tính tổng số tiền của khách hàng
            $sumPriceContactNews = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where(function ($query) {
                    $query ->where('contact.status', 0)
                        ->orWhereNull('contact.status');
                });
               
            // tính số khách hàng tiếp cận
            $countContactApproachs = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 1);

            // tính tổng tiền khách hàng tiếp cận
            $sumPriceContactApproachs = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 1);


            // tính số khách hàng đã đặt hàng
            $countContactOrders = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 2);
                

            // tính tổng tiền khách hàng đặt hàng
            $sumPriceContactOrders = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 2);
                

            // tính số khách hàng đã hoàn đơn
            $countContactReturns = $contact->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 3);


            // tính tổng tiền khách hàng hoàn đơn
            $sumPriceContactReturns = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
                ->where('contact.theme_code', $this->themeCode)
                ->where('contact.user_email', $this->emailUser)
                ->where('contact.status', 3);
               
            if (!empty($time_input)) {
                $time_now = Carbon::now()->toDateString();

               switch ($time_input) {
                    case 'THIS_WEEK':
                       # code...
                       //lấy thời gian đầu và cuối tuần này 
                    $time_start = Carbon::parse('last monday of last week')->subDay(7)->toDateString();
                    $time_end = Carbon::parse('last sunday of last week')->subDay(7)->toDateString();

                    $contacts = $contacts->whereBetween('contact.updated_at', array($time_start,$time_end));
                    $countContactNews = $countContactNews->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $countContactApproachs = $countContactApproachs->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $countContactOrders = $countContactOrders->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $countContactReturns = $countContactReturns->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactNews = $sumPriceContactNews->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactApproachs = $sumPriceContactApproachs->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactOrders = $sumPriceContactOrders->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactReturns = $sumPriceContactReturns->whereBetween('contact.updated_at',array($time_start,$time_end));

                       break;
                    case 'LAST_WEEK':
                        # code...
                    //lấy thời gian đầu và cuối tuần này trừ 7 ngày 
                    $time_start = Carbon::parse('last monday of last week')->toDateString();
                    $time_end = Carbon::parse('last sunday of last week')->toDateString();
                  
                    $contacts = $contacts->whereBetween('contact.updated_at', array($time_start,$time_end));
                    $countContactNews = $countContactNews->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $countContactApproachs = $countContactApproachs->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $countContactOrders = $countContactOrders->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $countContactReturns = $countContactReturns->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactNews = $sumPriceContactNews->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactApproachs = $sumPriceContactApproachs->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactOrders = $sumPriceContactOrders->whereBetween('contact.updated_at',array($time_start,$time_end));
                    $sumPriceContactReturns = $sumPriceContactReturns->whereBetween('contact.updated_at',array($time_start,$time_end));

                       break;
                    case 'NEXT_WEEK':
                       # code...
                       break;
                    case 'THIS_MONTH':
                       # code...
                        //Lấy thời gian rồi tách tháng ra 
                        $time_explode = explode('-', $time_now);
                        $time = $time_explode[1];

                        $contacts = $contacts->whereMonth('contact.updated_at', $time);
                        $countContactNews = $countContactNews->whereMonth('contact.updated_at',$time);
                        $countContactApproachs = $countContactApproachs->whereMonth('contact.updated_at',$time);
                        $countContactOrders = $countContactOrders->whereMonth('contact.updated_at',$time);
                        $countContactReturns = $countContactReturns->whereMonth('contact.updated_at',$time);
                        $sumPriceContactNews = $sumPriceContactNews->whereMonth('contact.updated_at',$time);
                        $sumPriceContactApproachs = $sumPriceContactApproachs->whereMonth('contact.updated_at',$time);
                        $sumPriceContactOrders = $sumPriceContactOrders->whereMonth('contact.updated_at',$time);
                        $sumPriceContactReturns = $sumPriceContactReturns->whereMonth('contact.updated_at',$time);
                       break;
                    case 'LAST_MONTH':
                       # code...
                        $last_month = Carbon::now()->subMonth()->toDateString();
                        $time_explode = explode('-', $last_month);
                        $time = $time_explode[1];

                        $contacts = $contacts->whereMonth('contact.updated_at', $time);
                        $countContactNews = $countContactNews->whereMonth('contact.updated_at',$time);
                        $countContactApproachs = $countContactApproachs->whereMonth('contact.updated_at',$time);
                        $countContactOrders = $countContactOrders->whereMonth('contact.updated_at',$time);
                        $countContactReturns = $countContactReturns->whereMonth('contact.updated_at',$time);
                        $sumPriceContactNews = $sumPriceContactNews->whereMonth('contact.updated_at',$time);
                        $sumPriceContactApproachs = $sumPriceContactApproachs->whereMonth('contact.updated_at',$time);
                        $sumPriceContactOrders = $sumPriceContactOrders->whereMonth('contact.updated_at',$time);
                        $sumPriceContactReturns = $sumPriceContactReturns->whereMonth('contact.updated_at',$time);
                       break;

                    case 'NEXT_MONTH':
                       # code...

                       break;

                    case 'THIS_DAY':
                       # code...
                        //lấy ra thời gian hiện tại
                        $time = $time_now;
                        $contacts = $contacts->whereDate('contact.updated_at', $time);
                        $countContactNews = $countContactNews->whereDate('contact.updated_at',$time);
                        $countContactApproachs = $countContactApproachs->whereDate('contact.updated_at',$time);
                        $countContactOrders = $countContactOrders->whereDate('contact.updated_at',$time);
                        $countContactReturns = $countContactReturns->whereDate('contact.updated_at',$time);
                        $sumPriceContactNews = $sumPriceContactNews->whereDate('contact.updated_at',$time);
                        $sumPriceContactApproachs = $sumPriceContactApproachs->whereDate('contact.updated_at',$time);
                        $sumPriceContactOrders = $sumPriceContactOrders->whereDate('contact.updated_at',$time);
                        $sumPriceContactReturns = $sumPriceContactReturns->whereDate('contact.updated_at',$time);
                    break;
                   
                    case 'THIS_YEAR':
                       # code...
                    //lấy thời gian xong tách năm ra 
                        $time_explode = explode('-', $time_now);
                        $time = $time_explode[0];
                        $contacts = $contacts->whereYear('contact.updated_at', $time);
                        $countContactNews = $countContactNews->whereYear('contact.updated_at',$time);
                        $countContactApproachs = $countContactApproachs->whereYear('contact.updated_at',$time);
                        $countContactOrders = $countContactOrders->whereYear('contact.updated_at',$time);
                        $countContactReturns = $countContactReturns->whereYear('contact.updated_at',$time);
                        $sumPriceContactNews = $sumPriceContactNews->whereYear('contact.updated_at',$time);
                        $sumPriceContactApproachs = $sumPriceContactApproachs->whereYear('contact.updated_at',$time);
                        $sumPriceContactOrders = $sumPriceContactOrders->whereYear('contact.updated_at',$time);
                        $sumPriceContactReturns = $sumPriceContactReturns->whereYear('contact.updated_at',$time);
                        break;
                    case 'LAST_YEAR':
                       # code...
                        //lấy thời gian xong tách năm ra 
                        $last_year = Carbon::now()->subYear()->toDateString();
                        $time_explode = explode('-', $last_year);
                        $time = $time_explode[0];

                        $contacts = $contacts->whereYear('contact.updated_at', $time);
                        $countContactNews = $countContactNews->whereYear('contact.updated_at',$time);
                        $countContactApproachs = $countContactApproachs->whereYear('contact.updated_at',$time);
                        $countContactOrders = $countContactOrders->whereYear('contact.updated_at',$time);
                        $countContactReturns = $countContactReturns->whereYear('contact.updated_at',$time);
                        $sumPriceContactNews = $sumPriceContactNews->whereYear('contact.updated_at',$time);
                        $sumPriceContactApproachs = $sumPriceContactApproachs->whereYear('contact.updated_at',$time);
                        $sumPriceContactOrders = $sumPriceContactOrders->whereYear('contact.updated_at',$time);
                        $sumPriceContactReturns = $sumPriceContactReturns->whereYear('contact.updated_at',$time);
                       break;

                         case 'TIME_REQUEST':
                        # code...
                        //lấy thời gian đầu 
                         
                        //Lấy thời gian cuối

                        $time_start = $date_start;
                        $time_end = $date_end;
                        
                        $contacts = $contacts->whereBetween('contact.updated_at', array($time_start,$time_end));
                        $countContactNews = $countContactNews->whereBetween('contact.updated_at',array($time_start,$time_end));
                        $countContactApproachs = $countContactApproachs->whereBetween('contact.updated_at',array($time_start,$time_end));
                        $countContactOrders = $countContactOrders->whereBetween('contact.updated_at',array($time_start,$time_end));
                        $countContactReturns = $countContactReturns->whereBetween('contact.updated_at',array($time_start,$time_end));
                        $sumPriceContactNews = $sumPriceContactNews->whereBetween('contact.updated_at',array($time_start,$time_end));
                        $sumPriceContactApproachs = $sumPriceContactApproachs->whereBetween('contact.updated_at',array($time_start,$time_end));
                        $sumPriceContactOrders = $sumPriceContactOrders->whereBetween('contact.updated_at',array($time_start,$time_end));
                        $sumPriceContactReturns = $sumPriceContactReturns->whereBetween('contact.updated_at',array($time_start,$time_end));

                        break;
                    default:
                       # code...
                    break;
                   }

                    $contacts = $contacts->orderBy('contact.contact_id', 'desc')->paginate(50);
                    $countContactNews = $countContactNews->count();
                    $countContactApproachs = $countContactApproachs->count();
                    $countContactOrders = $countContactOrders->count();
                    $countContactReturns = $countContactReturns->count();
                    $sumPriceContactNews = $sumPriceContactNews->sum('order_contact.price');
                    $sumPriceContactApproachs = $sumPriceContactApproachs->sum('order_contact.price');
                    $sumPriceContactOrders = $sumPriceContactOrders->sum('order_contact.price');
                    $sumPriceContactReturns = $sumPriceContactReturns->sum('order_contact.price');

                    return view('admin.contact.filter_time', compact(
                        'contacts',
                        'countContactNews',
                        'countContactApproachs',
                        'countContactOrders',
                        'countContactReturns',
                        'sumPriceContactNews',
                        'sumPriceContactApproachs',
                        'sumPriceContactOrders',
                        'sumPriceContactReturns'
                    ));
                }
            return redirect()->back();
    }

    private function countContact($contact, $status, $where = []){
        return $contact ->where('theme_code', $this->themeCode)
                        ->where('user_email', $this->emailUser)
                        ->where($where)
                        ->where('status', $status)
                        ->count();
    }

    private function sumMoney($contact, $status, $where = []){
        return $contact ->leftJoin('order_contact', 'order_contact.contact_id', '=','contact.contact_id')
                        ->where('contact.theme_code', $this->themeCode)
                        ->where('contact.user_email', $this->emailUser)
                        ->where($where)
                        ->where('contact.status', $status)
                        ->sum('order_contact.price');
    }

    private function dataJoinTable($contact, $where = []){
        return $contact ->leftJoin('order_contact', 'order_contact.contact_id', '=','contact.contact_id')
            ->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->where($where)
            ->select(
                    'contact.contact_id',
                    'contact.name',
                    'contact.email',
                    'contact.phone',
                    'contact.address',
                    'contact.message',
                    'contact.status',
                    'contact.color',
                    'contact.created_at',
                    'contact.updated_at',
                    DB::raw('SUM(order_contact.price) as sum_order')
            )
            ->groupBy(
                'contact.contact_id',
                'contact.name',
                'contact.email',
                'contact.phone',
                'contact.address',
                'contact.message',
                'contact.color',
                'contact.status',
                'contact.created_at',
                'contact.updated_at'
            )
            ->orderByDesc('contact.contact_id')
            ->paginate(50);
    }

    public function exportExcel(Request $request){
        if(isset(Auth::user()->name)){
            $userName  = Auth::user()->name . '_' . Auth::id();
        }else{
            $userName = Auth::id();
        }
        $contacts = $this->getCustomer($request->input('status'));
        $title = $this->getTitle($request->input('status'));
        $fileName = 'Danh sách ' . $title . ' của ' . $userName .time();
        $slugTitle = str_slug($title);
        Excel::create($fileName, function($excel) use ($contacts, $slugTitle){
            $excel->sheet($slugTitle , function ($sheet) use ($contacts) {
//                $sheet->mergeCells('A1:F1');
//
//                $sheet->cell('A1', function ($cell) {
//                    $cell->setValue('Danh sách khách hàng');
//
//                    $cell->setFontWeight('bold');
//                });
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('STT');

                    $cell->setFontWeight('bold');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('Họ và tên khách hàng');

                    $cell->setFontWeight('bold');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('SĐT');

                    $cell->setFontWeight('bold');
                });
                $sheet->cell('D1', function ($cell) {
                    $cell->setValue('Email');

                    $cell->setFontWeight('bold');
                });
                $sheet->cell('E1', function ($cell) {
                    $cell->setValue('Tag khách hàng');

                    $cell->setFontWeight('bold');
                });
                $sheet->cell('F1', function ($cell) {
                    $cell->setValue('Trạng thái khách hàng');

                    $cell->setFontWeight('bold');
                });
                $result = $this->getDataToLaravelExcel($contacts);
                $sheet->fromArray($result, null, 'A2', false, false);
            });
        })->store('xlsx', public_path('/adminstration/excel/customer'));
        $path = 'adminstration/excel/customer/' . $fileName . '.xlsx';
        return redirect(url('/' . $path));
    }

    private function getDataToLaravelExcel($contacts)
    {
        $result = [];

        foreach ($contacts as $key => $contact) {
            $status = 'Chưa xác định';
            if($contact->status == 0){
                $status = 'Khách hàng mới';
            }
            if($contact->status == 1){
                $status = 'Khách hàng tiếp cận';
            }

            if ($contact->status == 2){
                $status = 'Khách hàng mua hàng';
            }

            if ($contact->status == 3){
                $status ='Khách hàng hoàn đơn (hoàn tiền)';
            }

            $result[] = [
                'STT' => $key + 1,
                'Tên khách hàng' => isset($contact->name) ? $contact->name : '',
                'Số điện thoại' => isset($contact->phone) ? $contact->phone : '',
                'Email' => isset($contact->email) ? $contact->email : '',
                'Tag' => isset($contact->tag) ? $contact->tag : '',
                'Trạng thái' => $status
            ];
        }
        return $result;
    }

    private function getCustomer($status){
        $contacts = new Contact();
        if($status == 4){
            return $contacts
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->get();
        }
        return $contacts->where('theme_code', $this->themeCode)
                        ->where('user_email', $this->emailUser)
                        ->where('status', $status)
                        ->get();
    }

    private function getTitle($status){
        if($status == 0){
            return 'Khách hàng mới';
        }

        if ($status == 1){
            return 'Khách hàng tiếp cận';
        }

        if($status == 2){
            return 'Khách hàng mua hàng';
        }

        if($status == 3){
            return 'Khách hàng hoàn đơn (hoàn tiền)';
        }
        return 'Tất cả khách hàng';
    }

    public function uploadExcel(Request $request){
        $userName = Auth::id();
        if(isset(Auth::user()->name)){
            $userName = Auth::user()->name;
        }
        if ($request->hasFile('file')){
            $file = $request->file('file');
            $fileName =  'Danh-sach-khach-hang-tai-len-' . str_slug($userName) . '-' . time() . '.' . $file->getClientOriginalExtension();
            $file->move('adminstration/excel/customer/', $fileName);
            $results =Excel::load('adminstration/excel/customer/'. $fileName, function($reader) {
                $reader->get();
            })->get();
            foreach ($results as $result){
                $name = $result->ho_va_ten_khach_hang;
                $phone = $result->sdt;
                $email = $result->email;
                $tag = $result->tag_khach_hang;
                $status = $result->trang_thai_khach_hang;
                try{
                    $statusContact = $this->getStatus($status);
                    $contact = new Contact();
                    $contact->insertOrUpdateCustomer($this->emailUser, $this->themeCode, $name, $phone, $email, $tag, $statusContact);
                }catch (\Exception $exception){
                    return redirect()->back()->with('failed', 'Không thể đọc dữ liệu khách hàng của bạn. File của bạn tải lên không đúng mẫu của chúng tôi.');
                }
            }
            return redirect()->back()->with('success', 'Bạn đã tải lên dữ liệu khách hàng thành công.');
        }
        return redirect()->back()->with('failed', 'Bạn cần tải lên dữ liệu khách hàng.');
    }

    private function getStatus($status){
        if ((string)$status === 'Khách hàng tiếp cận'){
            return 1;
        }
        if ((string)$status === 'Khách hàng mua hàng'){
            return 2;
        }
        if ((string)$status === 'Khách hàng hoàn đơn'){
            return 3;
        }
        return 0;
    }

    public function showContactName(Request $request){
        $contact = new Contact();
        $contacts = $contact->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('name', 'LIKE', '%' . $request->input('name') . '%')
            ->get(['name']);
        $contactArray = array();
        foreach ($contacts as $contact){
            $contactArray[] = $contact->name;
        }
        return $contactArray;
    }

}
