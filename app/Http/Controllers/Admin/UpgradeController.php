<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 5/14/2019
 * Time: 8:35 AM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\CustomerTransaction;
use App\Entity\Domain;
use App\Entity\MailConfig;
use App\Entity\Order;
use App\Entity\UserCustomer;
use App\Entity\OrderCustomer;
use App\Entity\NoteContact;
use App\Entity\Contact;
use App\Entity\CustomerIntroduct;
use App\Entity\User;
use Carbon\Carbon;
use Exception;
use App\Ultility\CallApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use function PHPSTORM_META\map;

class UpgradeController extends AdminController
{

    public function upgrade() {

        return view('admin.upgrade.index');
    }

    public function buyMore(Request $request) {

        return view('admin.upgrade.buy_more_index');
    }

    public function submitUpgradeVip(Request $request) {
        $month = $request->input('month');
        $phone = $request->input('phone');
        $vip = $request->input('vip');
        $name = $request->input('name');
        $totalPrice = $request->input('money');

        $order = new Order();
        $orderId = $order->insertGetId([
            'status' => '1', // trang thai đặt hàng thành công
            'shipping_name' => $name,
            'shipping_email' => Auth::user()->email,
            'shipping_phone' => $phone,
            'gift_code' => $request->input('gift_code'),
            'shipping_address' => "mua ".$month.' tháng, gói vip'.$vip,
            'total_price' => $request->input('money'),
            'theme_code' => 'vn3c',
            'user_email' => 'vn3ctran@gmail.com',
            'created_at' =>   new \DateTime(),
            'updated_at' =>   new \DateTime(),
            'user_id' => Auth::user()->id
        ]);

        $order->where('order_id', $orderId)->update([
            'shipping_note' => 'VN3C-NC'.$vip. $month."-".$orderId
        ]);

        $gift_code = $request->input('gift_code');

        if(!empty($gift_code)){
            $user_id = explode('-', $gift_code);
            $customerIntroductModel = new CustomerIntroduct();
            $customerIntroduct = $customerIntroductModel->insertGetId([
                'user_id' => $user_id[1],
                'order_id' => $orderId,
                'status' => 0 ,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);

        }
        //$this->sendMain('VN3C-NC'.$vip. $month."-".$orderId, $phone);
        return view('admin.upgrade.upgrade_success', compact('month', 'vip', 'totalPrice', 'orderId'));
    }

	public function submitUpgradeEmail(Request $request) {
        $month = $request->input('month');
        $phone = $request->input('phone');
        $vip = $request->input('vip');
        $name = $request->input('name');

        $totalPrice = $request->input('money');

        $order = new Order();
        $orderId = $order->insertGetId([
            'status' => '1', // trang thai đặt hàng thành công
            'shipping_name' => $name,
            'shipping_email' => Auth::user()->email,
            'shipping_phone' => $phone,
            'shipping_address' => "mua ".$month.' tháng, gói email marketing',
            'total_price' => $request->input('money'),
            'theme_code' => 'vn3c',
            'user_email' => 'vn3ctran@gmail.com',
            'created_at' =>   new \DateTime(),
            'updated_at' =>   new \DateTime(),
            'user_id' => Auth::user()->id
        ]);

        $order->where('order_id', $orderId)->update([
            'shipping_note' => 'VN3C-EMAILNC'.$vip. $month."-".$orderId
        ]);

        //$this->sendMain('VN3C-NC'.$vip. $month."-".$orderId, $phone);

        return view('admin.upgrade.upgrade_success_email', compact('month', 'vip', 'totalPrice', 'orderId') );
    }
	
    public function submitBuyMore(Request $request) {
        $month = $request->input('month');
        $totalPrice = $request->input('money');
        $phone = $request->input('phone');

        $order = new Order();
        $orderId = $order->insertGetId([
            'status' => '1', // trang thai đặt hàng thành công
            'shipping_name' => 'Ẩn danh',
            'shipping_email' => Auth::user()->email,
            'shipping_phone' => $phone,
            'shipping_address' => "mua thêm ".$month. " tháng gói email marketing",
            //'gift_code' => $request->input('gift_code'),
            'total_price' => $totalPrice,
            'theme_code' => 'vn3c',
            'user_email' => 'vn3ctran@gmail.com',
            'created_at' =>   new \DateTime(),
            'updated_at' =>   new \DateTime(),
            'user_id' => Auth::user()->id
        ]);

        $order->where('order_id', $orderId)->update([
            'shipping_note' => 'VN3C-MT'.$month."-".$orderId
        ]);

       // $this->sendMain('VN3C-MT'.$month."-".$orderId, $phone);

        return view('admin.upgrade.buy_more_success', compact('month', 'totalPrice', 'orderId') );
    }

    private function sendMain($code, $phone) {
        try {
            $subject = 'Có liên hệ mới từ website: '.$this->domainUser->name;
            $content = 'Anh '. Auth::user()->name.' Vừa nâng cấp website';
            $content .= '<p>Số điện thoại: '.$phone.'. </p>';
            $content .= '<p>Mã nâng cấp: '.$code.'. </p>';

            MailConfig::sendMail('', $subject, $content);
        } catch (\Exception $e) {

        }
    }

    public function indexVnpay(Request $request){
        $money = $request->input('money');
        $month = $request->input('month');
        $domain = substr($request->root(), 7, strlen($request->root()));
        $formality = $domain . ' gia han them thoi gian su dung goi';
        $device = 'Gia hạn thêm thời gian sử dụng gói';
        if (strpos(url()->previous(), 'nang-cap-tai-khoan')) {
            $formality = 'Nang cap goi';
            $device = 'Nâng cấp gói';
        }
        $formality .= !empty($request->input('type')) ? ' website ' . $month .  ' thang' : 'Email Marketing '. $month .' thang';
        $device .=  !empty($request->input('type')) ? ' website ' . $month .  ' tháng' : ' Email Marketing '. $month .' tháng';

        return view('admin.upgrade.info_vnpay', compact('money', 'formality', 'device'));
    }

    public function createPayment(Request $request){
        // key test
        //VNPAY_TMN_CODE = 'VIDAN001'
        //VNPAY_HASH_SECRECT = 'HTGFIXPQLAXYFUKYRYBWAHXJGSZGJCGS'
		
		// key su dung:
		//VNPAY_TMN_CODE = 'TTVIDAN2'
        //VNPAY_HASH_SECRECT = 'KXZJABNWUZKNEJBFLPXATDBYXJNMHODR'
		
        $gift = '';
		//check có người giới thiệu không 
		$contact = null;
		$giftContact = UserCustomer::where('customer_id', Auth::id())->first();
		if(!empty($giftContact)){
			if(!empty($giftContact->user_id)){
				//Lấy thông tin kh 
				$user = User::where('id', $giftContact->user_id)->first();
				// tạo Khách hàng 
				$contact = Contact::insertGetId([
					'name' => $user->name,
					'phone' => $user->phone,
					'email' => $user->email,
					'address' => $user->address,
					'theme_code' => 'vn3c',
					'user_email' => 'vn3ctran@gmail.com',
					'created_at' => new \DateTime(),
					'updated_at' => new \DateTime()
				]);
				
				//Tạo nnote contact
				 $note = 'Khách hàng nhận hoa hồng từ đơn hàng: '.$request->input('order_id');
					NoteContact::insert([
						'contact_id' => $contact,
						'content' => $note,
						'created_at' => new \DateTime()
					]);
				$gift = $giftContact->user_id;
			}
		}		
		// url thanh toan thuc
        $url = 'https://pay.vnpay.vn/vpcpay.html';
		
		// url test thanh toan
		//$url = 'http://sandbox.vnpayment.vn/paymentv2/vpcpay.html';
		
        $txnRef = $request->input('order_id');
        $orderInfo = $request->input('order_desc');
        $orderType = $request->input('order_type');
        $amount = $request->input('amount') * 100;
        $locale = $request->input('language');
        $bankCode = $request->input('bank_code');
        $ipAddr = $_SERVER['REMOTE_ADDR'];
        $returnUrl = 'https://moma.vn/thong-tin-thanh-toan-vnpay';
        $hashSecret = env('VNPAY_HASH_SECRECT');
        $inputData = array(
            'vnp_Version' => '2.0.0',
            'vnp_TmnCode' => env('VNPAY_TMN_CODE'),
            'vnp_Amount' => $amount,
            'vnp_Command' => 'pay',
            'vnp_CreateDate' => date('YmdHis'),
            'vnp_CurrCode' => 'VND',
            'vnp_IpAddr' => $ipAddr,
            'vnp_Locale' => $locale,
            'vnp_OrderInfo' => $orderInfo.';'.Auth::id(),
            'vnp_OrderType' => $orderType,
            'vnp_ReturnUrl' => $returnUrl,
            'vnp_TxnRef' => $txnRef,
        );

        if (isset($bankCode) && !empty($bankCode)) {
            $inputData['vnp_BankCode'] = $bankCode;
        }
        ksort($inputData);
        $query = '';
        $i = 0;
        $hashData = '';
        foreach ($inputData as $key => $value) {
            if ($i === 1) {
                $hashData .= '&' . $key . '=' . $value;
            } else {
                $hashData .= $key . '=' . $value;
                $i = 1;
            }
            $query .= urlencode($key) . '=' . urlencode($value) . '&';
        }

        $vnp_Url = $url . '?' . $query;
        if (isset($hashSecret)) {
            $vnpSecureHash = hash('sha256', env('VNPAY_HASH_SECRECT') . $hashData);
            $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }
        $contentArray = explode(' ', $orderInfo);
        $month = $contentArray[count($contentArray) - 2];
        $domain = 'http://' . $contentArray[0];
        $transaction = new CustomerTransaction();
        $transaction->insertData(Auth::id(), $request->input('amount'), $orderInfo);
		
        if (strpos($orderInfo, 'Email Marketing')) {
            $this->createOrderEmail(Auth::id(), $domain, Auth::user()->email, Auth::user()->phone, $request->input('amount'), $month, $txnRef,$gift);
            return redirect($vnp_Url);
        }
        $this->createOrderWebsite(Auth::id(), $domain, Auth::user()->email, Auth::user()->phone, $request->input('amount'), $month, $txnRef,$gift, $vnpSecureHash);
		
		$this->addOrderCustomer($request, $contact );
		
        return redirect($vnp_Url);
    }
	
	private function addOrderCustomer($request, $contact){

            $orderCustomer = new OrderCustomer();
			$notes = 'Hoa hồng giới thiệu đơn hàng : ' .$request->input('order_id');
            $orderCustomerId = $orderCustomer->insertGetId([
                'contact_id' => $contact,
                'total_price' => $request->input('amount'),
                'notes' => $notes,
				'theme_code' => 'vn3c',
				'user_email' => 'vn3ctran@gmail.com',
                'created_at' => new \DateTime()
            ]);

    }

    private function createOrderEmail($userId, $name, $email, $phone, $price, $month, $transId, $gift) {
        $order = new Order();
        $orderId = $order->insertGetId([
            'status' => '0',
            'shipping_name' => $name,
            'shipping_email' => $email,
            'method_payment' => $transId,
            'shipping_phone' => $phone,
            'gift_code' => $gift,
            'shipping_address' => 'mua thêm ' . $month . ' tháng gói email marketing',
            'total_price' => $price,
            'theme_code' => 'vn3c',
            'user_id' => $userId,
            'user_email' => 'vn3ctran@gmail.com',
            'created_at' =>   new \DateTime(),
            'updated_at' =>   new \DateTime()
        ]);
		
        $order->where('order_id', $orderId)->update([
            'shipping_note' => 'VN3C-MT'.$month.'-'.$orderId
        ]);
		
		//$this->createCustomerIntroduct($gift, $orderId);
    }
	
	private function createCustomerIntroduct($gift, $orderId, $price, $userId){
        if(!empty($gift)){
            $customerIntroductModel = new CustomerIntroduct();
            $customerIntroduct = $customerIntroductModel->insertGetId([
                'user_id' => $gift,
                'order_id' => $orderId,   
                'user_buy' => $userId,
                'price' => $price,           
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);

        //$this->discountCalculation($gift, $orderId, $price, $userId);
        }
	}

    private function discountCalculation($gift, $orderId, $price, $userId){
        //lấy ra số tiền user đã mua và tính hoa hồng
        // mức tiền max mỗi nấc
		$lever1 = 10000000;
		$lever2 = 20000000;
		$lever3 = 50000000;
		$lever4 = 70000000;
		$lever5 = 100000000;
		$lever6 = 200000000;
		
        //Mức hoa hồng 
		$discountLever1 = 0.23;
		$discountLever2 = 0.25;
		$discountLever3 = 0.27;
		$discountLever4 = 0.29;
		$discountLever5 = 0.30;
		$discountLever6 = 0.35;
		
		//mức Tiền max Theo từng mức
		$priceLever1 = $lever1 * $discountLever1;
		$priceLever2 = $lever2 * $discountLever2;
		$priceLever3 = $lever3 * $discountLever3;
		$priceLever4 = $lever4 * $discountLever4;
		$priceLever5 = $lever5 * $discountLever5;
		
		//Từ 100 -> 200 sẽ là 2.5
		$priceLever6 = $lever6 * $discountLever2;
		
        //Trên 200 sẽ là 3.5

		//lấy thông tin số tiền hiện có 
		$detailUser = User::where('id',$gift)->first();
		$thisCoin = $detailUser->coin;
		$totalCoinIntroduct = $detailUser->total_coin_introduct;
			
		//check khách hàng mua lần 2 thì hoa hồng mức 6
		$userPay = CustomerIntroduct::where('user_buy',$userId)
		->orWhere('user_id', $gift)
		->count();
		
		if($userPay > 1 ){
			//Tính hoa hồng
			$coin = $price * $discountLever6;
			 //Cộng tiền cho user
			$user = User::where('id',$gift)
			->update([
				'total_coin_introduct' => $totalCoinIntroduct + $price,  
				'coin' => $thisCoin + $coin                
			]);
			return ; 
		}
		
		//Tính hoa hồng  
		if($totalCoinIntroduct <= $lever1 ){
			$coin = $price * $discountLever1;
		}
		else if($totalCoinIntroduct > $lever1 || $totalCoinIntroduct <= $lever2){
			$coin = ($price - $lever1) * $discountLever2 +  $priceLever1;
		}
		else if($totalCoinIntroduct > $lever2 || $totalCoinIntroduct <= $lever3){
			$coin = ($price - $lever1 - $lever2) * $discountLever3 +  $priceLever1 + $priceLever2; 
		}
		else if($totalCoinIntroduct > $lever3 || $totalCoinIntroduct <= $lever4){
			$coin = ($price - $lever1 - $lever2 - $lever3) * $discountLever4 +  $priceLever1 + $priceLever2 + $priceLever3; 
		}
		else if($totalCoinIntroduct > $lever4 || $totalCoinIntroduct <= $lever5){
			$coin = ($price - $lever1 - $lever2 - $lever3 -$lever4) * $discountLever5 +  $priceLever1 + $priceLever2 + $priceLever3 + $priceLever4; 
		}
		else if($totalCoinIntroduct > $lever5 || $totalCoinIntroduct <= $lever6){
			$coin = ($price - $lever1 - $lever2 - $lever3 -$lever4 - $lever5) * $discountLever2 +  $priceLever1 + $priceLever2 + $priceLever3 + $priceLever4 + $priceLever5; 
		}
		else if($totalCoinIntroduct > $lever6 ){
			$coin = ($price - $lever1 - $lever2 - $lever3 -$lever4 - $lever5 - $lever6) * $discountLever2 +  $priceLever1 + $priceLever2 + $priceLever3 + $priceLever4 + $priceLever5 + $priceLever6; 
		}

		//Cộng tiền cho user
		$user = User::where('id',$gift)
		->update([
			'total_coin_introduct' => $totalCoinIntroduct + $price,  
			'coin' => $thisCoin + $coin,                
		]);
		
		return $coin;
    }

    private function createOrderWebsite($userId, $name, $email, $phone, $price, $month, $transId, $gift, $vnpSecureHash) {
        $order = new Order();
        switch ($price){
            case 300000:
                $package = 'website 1';
                break;
            case 806000:
                $package = 'website 2';
                break;
            case 2400000:
                $package = 'vip 1';
                break;
            case 4800000:
                $package = 'vip 2';
                break;
            default:
                $package = 'vip 3';
                break;
        }

        $orderId = $order->insertGetId([
            'status' => '1',
            'shipping_name' => $name,
            'shipping_email' => $email,
            'shipping_phone' => $phone,
            'vnp_secure_hash' => $vnpSecureHash,
            'shipping_address' => 'mua '.$month.' tháng, gói '. $package,
            'total_price' => $price,
            'method_payment' => $transId,
            'cost_point' => $month,
            'theme_code' => 'vn3c',
            'gift_code' => $gift,
            'user_email' => 'vn3ctran@gmail.com',
            'created_at' =>   new \DateTime(),
            'updated_at' =>   new \DateTime(),
            'user_id' => $userId
        ]);

        $this->postOrderToGetfly($price, $name,  $email, $phone, $month, $package);

        $order->where('order_id', $orderId)->update([
            'shipping_note' => 'VN3C-NC'.$package. $month.'-'.$orderId
        ]);

		$this->createCustomerIntroduct($gift, $orderId, $price, $userId);

    }

    private function postOrderToGetfly($price, $name,  $email, $phone, $month, $package) {
        // try {
            $now = new \DateTime();
   
            $orderInfor = [
                'account_name' => $name,
                'account_address' => '',
                'account_email' => $email,
                'account_phone' => $phone,
                'order_date' => date_format($now,"d/m/Y"),
                'discount' => 0,
                'discount_amount' => '',
                'vat' => 0,
                'vat_amount' => 0,
                'transport_amount' => 0,
                'assigned_username' => 'vn3ctran',
                'installation' => 0,
                'installation_amount' => 0,
                'amount' => $price,
            ];
   
            $products = array();
         
            $products[] = (object) [
                'product_code' => 'nangcapwebsite',
                'product_name' => 'Nâng cấp website moma '.$month.' tháng với gói '.$package,
                'quantity' => '1',
                'price' =>  $price,
                'product_sale_off' => '',
                'cash_discount' => ''
            ];
   
            $data = [
                'order_info' => $orderInfor,
                'products' => $products,
                'terms' => ['nâng cấp website tại website moma']
            ];
   

            $callApi = new CallApi();
            $callApi->postOrderSachvidan($data);
   
        // } catch (\Exception $e) {
        //      Log::error('http->site->OrderController->postOrderToGetfly: Lỗi đẩy đơn hàng lên getfly');
        // }
     }
}
