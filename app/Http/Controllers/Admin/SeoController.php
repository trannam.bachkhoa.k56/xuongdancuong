<?php

namespace App\Http\Controllers\Admin;

use App\Entity\ColorWebsite;
use App\Entity\MailConfig;
use App\Entity\OrderBank;
use App\Entity\OrderCodeSale;
use App\Entity\OrderShip;
use App\Entity\SettingGetfly;
use App\Entity\SettingOrder;
use App\Entity\User;
use App\Entity\Domain;
use App\Ultility\Ultility;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Ultility\CallApi;
use Redirect;

class SeoController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'websites');

            return $next($request);
        });

    }
	
	public function check(Request $request) {
		// try {
			$title = strtoupper($request->input('title'));
			$content = $request->input('content');
			$content = strtoupper(html_entity_decode($content));
			$metaDescription = strtoupper(html_entity_decode($request->input('description')));
			
			$messages = '';
			// lấy ra bộ từ khóa. 
			$metaKeywords = $request->input('keyword');
			$keyWords = explode(',', $metaKeywords);
			
			// so tu khoa xuat hien trong tieu de
			$countTitle = 0;
			// so tu khoa xua hien trong description
			$countDescription = 0;
			// so tu khoa xuat hien trong content
			$countContent = 0;
			// 5 từ khóa xuất hiện title, mô tả, content.
			$contentTrip = 0;
			// diem cho bai seo
			$point = 0;
			
			foreach ($keyWords as $keyWord) {
				$keyWord = strtoupper($keyWord);
				$countKeywordTitle = !empty($title) && !empty($keyWord) ? substr_count($title, trim($keyWord, ' ')) : 0;	
				$countKeywordDescription = !empty($metaDescription) && !empty($keyWord) ? substr_count($metaDescription, trim($keyWord, ' ')) : 0;
				$countKeywordContent = !empty($content) && !empty($keyWord) ? substr_count($content, trim($keyWord, ' ')) : 0;
				
				if ($countKeywordTitle > 0 && $countKeywordDescription> 0 && $countKeywordContent > 0) {
					$contentTrip++;
				}
				
				$countTitle += $countKeywordTitle > 0 ? 1 : 0;
				$countDescription += $countKeywordDescription ? 1 : 0;
				$countContent += $countKeywordContent ? 1 : 0;
			}
			// check diem 5/5 doi voi tung diem tu khoa
			
			// in ra diem tieu de
			$pointTitle = $this->checkPoint5To5($countTitle);
			$messages .= '<p class="green">từ khóa tiêu đề: '.$countTitle.'/5. '.$pointTitle.' điểm</p>';
			$point += $pointTitle;
			
			// in ra diem mo ta
			$pointDescription = $this->checkPoint5To5($countDescription);
			$messages .= '<p class="green">từ khóa trong mô tả: '.$countDescription.'/5. '.$pointDescription.' điểm</p>';
			$point += $pointDescription;
			
			// in ra diem noi dung
			$pointContent = $this->checkPoint5To5($countContent);
			$messages .= '<p class="green">từ khóa trong nội dung: '.$countContent.'/5. '.$pointContent.' điểm</p>';
			$point += $pointContent;
			
			// in ra diem trung noi dung tieu de va mo ta
			$pointTrip = $this->checkPoint5To5($contentTrip);
			$messages .= '<p class="green">từ khóa lap lai: '.$contentTrip.'/5. '.$pointTrip.' điểm</p>';
			$point += $pointTrip;
			
			// có chứa link bên trong website.
			$nameUrl = Ultility::getCurrentHttpHost();
			$countTimeWebsiteParent = substr_count($request->input('content'), $nameUrl);
			if ($countTimeWebsiteParent < 1) {
				$messages .= '<p class="red">Các đường dẫn nội bộ: Không có đường dẫn nội bộ trong trang này. (đường dẫn nội bộ là đường dẫn đến chính trang website hiện tại).</p>';
			} else {
				$messages .= '<p class="green">Các đường dẫn nội bộ: rất tốt. 10 điểm</p>';
				$point += 10;
			}
			
			// tiêu đề không vượt quá 70 ký tự - 5 diem
			if (strlen($title) > 70) {
				$messages .= '<p class="red">Tiêu đề tối đa 70 ký tự: Vượt quá.</p>';
			} else {
				$messages .= '<p class="green">Tiêu đề tối đa 70 ký tự: rất tốt. 5 điểm</p>';
				$point += 5;
			}
			
			// độ dài văn bản không được ngắn hơn 700 ký tự 5 diem
			if (strlen($content) < 700) {
				$messages .= '<p class="red">Nội dung tối thiểu 700 ký tự: Chưa đủ.</p>';
			} else {
				$messages .= '<p class="green">Nội dung tối thiểu 700 ký tự: rất tốt. 5 điểm</p>';
				$point += 5;
			}
			
			// chua chia se len youtube 10 diem
			$messages .= '<p class="red">Chia sẻ lên youtube: chưa làm</p>';
			
			// chua chia se len facebook 10 diem
			$messages .= '<p class="red">Chia sẻ lên facebook: chưa làm</p>';
			
			// chua chia se len twister 10 diem
			$messages .= '<p class="red">Chia sẻ lên twister: chưa làm</p>';
			
			// chua comment va like binh luan bai viet 10 diem
			$messages .= '<p class="red">Bình luận và like bình luận: chưa làm</p>';
			
			$messages .= '<h2 class="yellow">Tổng điểm: '.$point.' /100</h2>';
			
			return response([
				'status' => 200,
				'messages' => $messages,
			])->header('Content-Type', 'text/plain');
		// } catch (\Exception $e) {
			// $messages = 'Bạn hãy thử lại!';
			
			// return response([
				// 'status' => 200,
				// 'messages' => $messages,
			// ])->header('Content-Type', 'text/plain');
		// }	
	}

	private function checkPoint5To5($countKeyword) {
		if ($countKeyword < 5) {
			return $countKeyword*2;
		}
		
		return 10;
	}
}