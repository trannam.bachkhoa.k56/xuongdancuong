<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 12/19/2018
 * Time: 3:48 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\IpClient;
use Illuminate\Support\Facades\Auth;
use App\Entity\User;

class IpClientController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }

    public function index () {
        $ipClientModel = new IpClient();
        try {
            $ipClients = $ipClientModel->orderBy('ip_client_id', 'desc')
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();

        } catch (\Exception $e) {

            $ipClients = null;
        } finally {
            return view('admin.ipclient.list', compact('ipClients'));
        }
    }
}