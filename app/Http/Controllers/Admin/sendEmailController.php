<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 12/19/2018
 * Time: 3:48 PM
 */

namespace App\Http\Controllers\Admin;
use App\Entity\CheckSendMail;
use App\Entity\SendMailCustomer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entity\User;
use App\Entity\Process;
use App\Entity\CampaignCustomer;
use App\Ultility\Error;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;



class sendEmailController extends AdminController
{
	protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'cskh');

            return $next($request);
        });

    }

    public function index(){
        try {
			if(Auth::user()->email_remaining <-100){
				$menuTop = 'cskh';
				return view('admin.home.update_email',compact('menuTop'));
			}

            $campaigns = CampaignCustomer::where('user_email', $this->emailUser)
            ->where('theme_code', $this->themeCode)
            ->get();
		
            return view('admin.sendEmail.index',compact('campaigns'));
        } 
        catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi hiển thị đăng ký email kh: dữ liệu không hợp lệ.');
            Log::error('http->admin->SubcribeEmailController->index: Lỗi xảy ra trong quá trình hiển thị đăng ký email khách hàng');

            return redirect('admin/home');
        }
    }

    public function getOpportunity(Request $request){
        $campaign_id = $request->input('campaign_id');
        $proceses = Process::where('campain_customer_id',$campaign_id)
        ->get();

        return view('admin.sendEmail.group_opportunity',compact('proceses'));
    }

    

    public function managerEmail() {
		if(Auth::user()->email_remaining <-100){
            $menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $email = new CheckSendMail();
        $file = @fopen('log.txt', 'r');
        $lastDayOfLastMonth = $this->getExpEmailCustomer(Auth::id());
        $emailSeen = $email->where('author', $this->emailUser)
        //->whereMonth('time_seen', date('Y-m-d'))
        ->count();

        $totalMail = $this->getTotalCustomerHaveMail();
        $emailSend = $totalMail - (int)User::getUserByUserId(Auth::id())->email_remaining;
        if(!$file) {
            return view('admin.email.index', compact('lastDayOfLastMonth', 'emailSeen', 'totalMail', 'emailSend'));
        }
        while(!feof($file)) {
            $stringInLine =  fgets($file);
            $contentLineArray = explode(';', $stringInLine);

            if (isset($contentLineArray[0], $contentLineArray[1], $contentLineArray[2], $contentLineArray[3])) {
                $email->insertToDB($contentLineArray[0] , $contentLineArray[1], $contentLineArray[2], $contentLineArray[3], $this->emailUser);
            }
        }
        $emailSeen = $email->where('author', $this->emailUser)->count();
        return view('admin.email.index', compact('lastDayOfLastMonth', 'emailSeen', 'totalMail', 'emailSend'));
    }
	
    public function getDataEmailByTime(Request $request){
		if(Auth::user()->email_remaining <-100){
            $menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $totalMailSend = empty($this->getTotalEmailSend($request->input('time'))->sum_total_mail) ? 0 : $this->getTotalEmailSend($request->input('time'))->sum_total_mail;
        $lastDayOfLastMonth = date('t-m-Y');
        $totalMailSeen = $this->getTotalMailSeen($request->input('time'));
        $totalMail = $this->getTotalMail($request->input('time'));
        echo    '<td> ' . $totalMail . ' </td>'.
                '<td> ' . $totalMailSend . '</td>' .
                '<td> ' . $totalMailSeen . '</td>' .
                '<td> ' . (int)$totalMailSend. '</td>'.
                '<td>' . $lastDayOfLastMonth . '</td>';
    }

    private function getTotalEmailSend($time){
		if(Auth::user()->email_remaining <-100){
            $menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $sendMailCustomer = new SendMailCustomer();
        $sendMailCustomer = $sendMailCustomer->where('user_id', Auth::id());
        switch ((string)$time) {
            case 'THIS_WEEK':
                $timeFilter = date('Y-m-d', strtotime('this Monday'));
                if ($sendMailCustomer->whereDate('time_send_mail','>=', $timeFilter)->exists()) {
                    return $sendMailCustomer->whereDate('time_send_mail','>=', $timeFilter)
                        ->first([DB::raw('SUM(total_mail) as sum_total_mail')]);
                }
                return 0;
                break;

            case 'LAST_WEEK':
                $timeStart = date('Y-m-d', strtotime('last monday'));
                $timeFinish = date('Y-m-d', strtotime('last sunday'));
                if ($sendMailCustomer->whereDate('time_send_mail','>=', $timeStart)
                    ->whereDate('time_send_mail', '<=', $timeFinish)->exists()) {
                    return $sendMailCustomer->whereDate('time_send_mail','>=', $timeStart)
                        ->whereDate('time_send_mail', '<=', $timeFinish)
                        ->first([DB::raw('SUM(total_mail) as sum_total_mail')]);
                }
                return 0;
                break;

            case 'THIS_MONTH':
                if ($sendMailCustomer->whereMonth('time_send_mail', date('m'))->whereYear('time_send_mail', date('Y'))->exists()) {
                    return $sendMailCustomer->whereMonth('time_send_mail', date('m'))->whereYear('time_send_mail', date('Y'))
                        ->first([DB::raw('SUM(total_mail) as sum_total_mail')]);
                }
                return 0;
                break;

            case 'LAST_MONTH':
                $month = date('m', mktime(0, 0, 0 , date('m')-1, date('d'),  date('Y')));
                $year = date('Y', mktime(0, 0, 0 , date('m')-1, date('d'),  date('Y')));
                if ($sendMailCustomer->whereMonth('time_send_mail', $month)->whereYear('time_send_mail', $year)->exists()){
                    return $sendMailCustomer->whereMonth('time_send_mail', $month)->whereYear('time_send_mail', $year)
                        ->first([DB::raw('SUM(total_mail) as sum_total_mail')]);
                }
                return 0;
                break;

            case 'THIS_YEAR':
                if ($sendMailCustomer->whereYear('time_send_mail', date('Y'))->exists()) {
                    return $sendMailCustomer->whereYear('time_send_mail', date('Y'))
                        ->first([DB::raw('SUM(total_mail) as sum_total_mail')]);
                }
                return 0;
                break;

            case 'LAST_YEAR':
                $year = date('Y', mktime(0, 0, 0 , date('m'), date('d'), date('Y')-1));
                if ($sendMailCustomer->whereYear('time_send_mail', $year)->exists()) {
                    return $sendMailCustomer->whereYear('time_send_mail', $year)
                        ->first([DB::raw('SUM(total_mail) as sum_total_mail')]);
                }
                return 0;
                break;

            default:
                if ($sendMailCustomer->whereDate('time_send_mail',date('Y-m-d'))->exists()) {
                    return $sendMailCustomer->whereDate('time_send_mail',date('Y-m-d'))
                        ->first([DB::raw('SUM(total_mail) as sum_total_mail')]);
                }
                return 0;
                break;
        }
    }

    private function getTotalMail($time){
        $sendMailCustomer = new SendMailCustomer();
        switch ((string)$time) {
            case 'THIS_WEEK':
                return $this->getTotalCustomerHaveMail();
                break;

            case 'LAST_WEEK':
                $timeStart = date('Y-m-d', strtotime('last monday'));
                $timeFinish = date('Y-m-d', strtotime('last sunday'));
                if ($sendMailCustomer->where('user_id', Auth::id())->whereDate('time_send_mail','>=', $timeStart)
                    ->whereDate('time_send_mail', '<=', $timeFinish)->exists()) {
                    return $this->getTotalCustomerHaveMail();
                }
                return 0;
                break;

            case 'THIS_MONTH':
                return $this->getTotalCustomerHaveMail();
                break;

            case 'LAST_MONTH':
                $month = date('m', mktime(0, 0, 0 , date('m')-1, date('d'),  date('Y')));
                $year = date('Y', mktime(0, 0, 0 , date('m')-1, date('d'),  date('Y')));
                if ($sendMailCustomer->whereMonth('time_send_mail', $month)->whereYear('time_send_mail', $year)
                    ->exists()) {
                    return $this->getTotalCustomerHaveMail();
                }
                return 0;
                break;

            case 'THIS_YEAR':
                return $this->getTotalCustomerHaveMail();
                break;

            case 'LAST_YEAR':
                $year = date('Y', mktime(0, 0, 0 , date('m'), date('d'), date('Y')-1));
                if ($sendMailCustomer->whereYear('time_send_mail', $year)
                    ->exists()) {
                    return $this->getTotalCustomerHaveMail();
                }
                return 0;
                break;

            default:
                return $this->getTotalCustomerHaveMail();
        }
    }

    private function getTotalMailSeen($time) {
        $sendMailCustomer = new CheckSendMail();
        $sendMailCustomer = $sendMailCustomer->where('author', Auth::user()->email);
        switch ((string)$time) {
            case 'THIS_WEEK':
                $timeFilter = date('Y-m-d', strtotime('this Monday'));
                if ($sendMailCustomer->whereDate('time_seen','>=', $timeFilter)->exists()) {
                    return $sendMailCustomer->whereDate('time_seen','>=', $timeFilter)
                        ->count();
                }
                return 0;
                break;

            case 'LAST_WEEK':
                $timeStart = date('Y-m-d', strtotime('last monday'));
                $timeFinish = date('Y-m-d', strtotime('last sunday'));
                if ($sendMailCustomer->whereDate('time_seen','>=', $timeStart)
                    ->whereDate('time_seen', '<=', $timeFinish)->exists()) {
                    return $sendMailCustomer->whereDate('time_seen','>=', $timeStart)
                        ->whereDate('time_seen', '<=', $timeFinish)
                        ->count();
                }
                return 0;
                break;

            case 'THIS_MONTH':
                if ($sendMailCustomer->whereMonth('time_seen', date('m'))->whereYear('time_seen', date('Y'))->exists()) {
                    return $sendMailCustomer->whereMonth('time_seen', date('m'))->whereYear('time_seen', date('Y'))
                        ->count();
                }
                return 0;
                break;

            case 'LAST_MONTH':
                $month = date('m', mktime(0, 0, 0 , date('m')-1, date('d'),  date('Y')));
                $year = date('Y', mktime(0, 0, 0 , date('m')-1, date('d'),  date('Y')));
                if ($sendMailCustomer->whereMonth('time_seen', $month)->whereYear('time_seen', $year)->exists()){
                    return $sendMailCustomer->whereMonth('time_seen', $month)->whereYear('time_seen', $year)
                        ->count();
                }
                return 0;
                break;

            case 'THIS_YEAR':
                if ($sendMailCustomer->whereYear('time_seen', date('Y'))->exists()) {
                    return $sendMailCustomer->whereYear('time_seen', date('Y'))
                        ->count();
                }
                return 0;
                break;

            case 'LAST_YEAR':
                $year = date('Y', mktime(0, 0, 0 , date('m'), date('d'), date('Y')-1));
                if ($sendMailCustomer->whereYear('time_seen', $year)->exists()) {
                    return $sendMailCustomer->whereYear('time_seen', $year)
                        ->count();
                }
                return 0;
                break;

            default:
                if ($sendMailCustomer->whereDate('time_seen',date('Y-m-d'))->exists()) {
                    return $sendMailCustomer->whereDate('time_seen',date('Y-m-d'))
                        ->count();
                }
                return 0;
                break;
        }
    }

    private function getTotalCustomerHaveMail(){
        if (!empty(User::getUserByUserId(Auth::id())) && (int)User::getUserByUserId(Auth::id())->package_mail === 2) {
            return 20000;
        }

        if (!empty(User::getUserByUserId(Auth::id())) && (int)User::getUserByUserId(Auth::id())->package_mail === 1) {
            return 5000;
        }

        if (!empty(User::getUserByUserId(Auth::id())) && (int)User::getUserByUserId(Auth::id())->vip !== 0) {
            return 100;
        }

        return 0;
    }

    private function getExpEmailCustomer($userId) {
        $user = new User();
        $user = $user->where('id', $userId)->first(['time_email']);
        if (!empty($user)) {
            $strToTime = strtotime($user->time_email);
            return date('d-m-Y', $strToTime);
        }
    }

}