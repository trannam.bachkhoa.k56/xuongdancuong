<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 12/19/2018
 * Time: 3:48 PM
 */

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Entity\GroupMail;
use Carbon\Carbon;
use App\Entity\RemindContact;
use App\Entity\CustomerIntroduct;
use App\Entity\MailAutomation;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class settingCustomerController extends AdminController
{
	protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'cskh');

            return $next($request);
        });

    }

    public function index(){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->get();

        $emailModel = new MailAutomation();
        $emails = $emailModel->where('theme_code',$this->themeCode)
        ->where('user_email',$this->emailUser)
        ->paginate(5);

        return view('admin.settingCustomer.index',compact('groupMails','emails'));

    }

     public function store(Request $request){

		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $emailModel = new MailAutomation();

        $email = $emailModel->insertGetId([
            'group_id' => $request->input('group'),
            'campaign_id' => $request->input('campaign'),

            'campaign_customer_id' => $request->input('campaign_customer_id'),
            'process_id' => $request->input('process'),

            'type_id' => $request->input('type'),
            'time_delay' => $request->input('time_delay'),
            'subject' => $request->input('subject'),
            'content' => $request->input('content'),
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
            'updated_at' => new \DateTime(),
            'created_at' => new \DateTime(),
        ]);

        if($request->input('type') == 2 ){

            RemindContact::insertGetId([
                'date' => Carbon::now()->addDays($request->input('time_delay'))->toDateString(),
                'status' => 0,
                'content' => "Bạn có 1 nhắc nhở trong Cấu hình email khách hàng ngày hôm nay",
                'theme_code' => $this->themeCode,
                'contact_id' => null ,
                'user_email' => $this->emailUser,
                'updated_at' => new \DateTime(),
                'created_at' => new \DateTime(),
            ]);

        }
    return redirect()->back()->with('add','Thêm mới Automation thành công');
    }

    public function update(Request $request,$id){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
        $emailModel = new MailAutomation();

        $email = $emailModel->where('mail_id',$id)
        ->update([
            'group_id' => $request->input('group'),
            'campaign_customer_id' => $request->input('campaign_customer_id'),
            'process_id' => $request->input('process_id'),
            'time_delay' => $request->input('time_delay'),
            'subject' => $request->input('subject'),
            'content' => $request->input('content')
        ]);

        return redirect()->back()->with('update','Chỉnh sửa Automation thành công ');
    }

    public function delete($id){
		if(Auth::user()->email_remaining <-100){
			$menuTop = 'cskh';
			return view('admin.home.update_email',compact('menuTop'));
		}
		
         $email = MailAutomation::where('mail_id',$id)->first()->delete();
        return redirect()->back()->with('remove','Xóa Automation thành công ');
    }


    public function showGift(Request $request){
		if ($request->has('user_id')) {
			$userId = $request->input('user_id');
		} else {
			$userId = Auth::user()->id;
		}
		
        $user = User::where('id', $userId)->first();
		
        $customerIntroducts = CustomerIntroduct::leftJoin('orders','orders.order_id','customer_introduct.order_id')
            ->leftJoin('users','users.id','customer_introduct.user_id')
            ->select([
                'customer_introduct.user_id',
                'users.name as user_name',
                'users.id',
                'users.phone  as user_phone',
                'users.email  as user_email',
                'users.image as user_image',
                'orders.shipping_name',
                'orders.total_price',
                'orders.shipping_address'
            ])
            ->where('customer_introduct.user_id', $userId )
            ->first();  

        $topAffiliates = User::leftJoin('customer_introduct','users.id','customer_introduct.user_id')   
			->leftJoin('orders','orders.order_id','customer_introduct.order_id')
            ->leftJoin('domains', 'domains.user_id', 'customer_introduct.user_id') 
			->leftJoin('payment_information', 'payment_information.user_id', 'users.id')
            ->select(
                'domains.url', 
                'users.name',
				'users.coin',
				'payment_information.coin_total',
                DB::raw('SUM(orders.total_price) as total_affiliate')
            )
			->where('users.email', '!=', 'vn3ctran@gmail.com')
            ->orderByRaw('SUM(orders.total_price) DESC')
			->orderBy('coin', 'desc')
            ->groupBy(
                'domains.url',
                'users.name',
				'payment_information.coin_total'
            )
            ->limit(10)->get();
            
        return view('admin.gift.index', compact('user', 'customerIntroducts', 'topAffiliates'));
    }



}
