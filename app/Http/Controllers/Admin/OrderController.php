<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/3/2017
 * Time: 3:05 PM
 */

namespace App\Http\Controllers\Admin;


use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\OrderShip;
use App\Entity\Post;
use App\Entity\User;
use App\Ultility\CheckPermission;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'orders');

            return $next($request);
        });

    }


    public function listOrder(Request $request) {
        // try {
            $messageErrorVip = CheckPermission::checkOrders(Auth::user());
            if (!empty($messageErrorVip)) {
                return view('admin.order.list', compact('messageErrorVip'));
            }

            $orders = Order::orderBy('order_id', 'desc')
				->leftjoin('users', 'users.id', 'orders.user_id')
				->select('orders.*', 'users.name')
                ->where('orders.status', '>', 0)
                ->where('orders.theme_code', $this->themeCode)
                ->where('orders.user_email', $this->emailUser);

            if (!empty($request->input('order_id'))) {
                $orders = $orders->where('order_id', 'like', '%'.$request->input('order_id').'%');
            }
            if (!empty($request->input('phone'))) {
                $orders = $orders->where('shipping_phone', 'like', '%'.$request->input('phone').'%');
            }
            if (!empty($request->input('email'))) {
                $orders = $orders->where('shipping_email', 'like', '%'.$request->input('email').'%');
            }
            if (!empty($request->input('name'))) {
                $orders = $orders->where('shipping_name', 'like', '%'.$request->input('name').'%');
            }
            if (!empty($request->input('user_id'))) {
                $orders = $orders->where('user_id', '=', $request->input('user_id'));
            }
            if ($request->input('is_search_time') == 1) {
                $startEnd = $request->input('search_start_end');
                $time = explode('-', $startEnd);
                $start = $time[0];
                $end = $time[1];

                $orders = $orders->where('updated_at', '>=', new \DateTime($start))
                    ->where('updated_at', '<=', new \DateTime($end));
            }

            $orders = $orders->paginate(10);

            foreach($orders as $id => $order) {
                $orders[$id]->orderItems = OrderItem::join('products','products.product_id','=', 'order_items.product_id')
                    ->join('posts', 'products.post_id','=','posts.post_id')
                    ->select(
                        'posts.*',
                        'products.price',
                        'products.discount',
                        'products.code',
                        'order_items.*'
                    )
                    ->where('order_id', $order->order_id)
                    ->where('order_items.theme_code', $this->themeCode)
                    ->where('order_items.user_email', $this->emailUser)->get();
            }
            return view('admin.order.list', compact('orders'));
        // } catch (\Exception $e) {
            // Error::setErrorMessage('Lỗi xảy ra khi hiển thị danh sách đơn hàng: dữ liệu không hợp lệ.');
            // Log::error('http->admin->OrderController->listOrder: Lỗi xảy ra trong quá trình hhiển thị danh sách đơn hàng');

            // return redirect('admin/home');
        // }
    }

    public function showOrder(Order $order) {
//        try {
            $orderShips = OrderShip::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();
            // delete order item
            $orderItems = OrderItem::join('products','products.product_id','=', 'order_items.product_id')
                ->join('posts', 'products.post_id','=','posts.post_id')
                ->select(
                    'posts.*',
                    'products.price',
                    'products.discount',
                    'products.code',
                    'order_items.*'
                )
                ->where('order_id', $order->order_id)->get();

            return view('admin.order.detail', compact('order', 'orderItems', 'orderShips'));
//        } catch (\Exception $e) {
//            Error::setErrorMessage('Lỗi xảy ra khi xóa đơn hàng: dữ liệu không hợp lệ.');
//            Log::error('http->admin->OrderController->deleteOrder: Lỗi xảy ra trong quá trình xóa đơn hàng');
//        }

    }

    public function updateStatusOrder(Request $request) {
        try {
            $orderId = $request->input('order_id');
            $status = $request->input('status');
            $noteAdmin = $request->input('noteAdmin');
            $order = Order::where('order_id', $orderId)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->first();

            $order->update([
                'status' => $status,
                'note_admin' => $noteAdmin,
                'is_mail_customer' => $request->has('is_mail_customer') ? 1 : 0,
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);


            if ($request->has('is_mail_customer')) {
                $now = date_create("2013-03-15");
                $now =  date_format($now,"d/m/Y H:i:s");
                $orderItems = Post::join('products', 'products.post_id', '=', 'posts.post_id')
                    ->join('order_items', 'order_items.product_id', '=', 'products.product_id')
                    ->select(
                        'products.product_id',
                        'posts.*',
                        'products.price',
                        'products.discount'
                    )
                    ->where('order_items.order_id', $orderId)
                    ->where('posts.theme_code', $this->themeCode)
                    ->where('posts.user_email', $this->emailUser)->get();
            }

        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi cập nhật trạng thái đơn hàng: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->updateStatusOrder: Lỗi xảy ra trong quá trình cập nhật trạng thái đơn hàng');
        } finally {
            return redirect(route('orderAdmin'));
        }

    }

    public function deleteOrder(Order $order) {
        try {
            if ($order->theme_code != $this->themeCode || $order->user_email != $this->emailUser) {
                return redirect(route('orderAdmin'));
            }

            // delete order item
            OrderItem::where('order_id', $order->order_id)->delete();

            $order->delete();
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xóa đơn hàng: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->deleteOrder: Lỗi xảy ra trong quá trình xóa đơn hàng');
        } finally {
            return redirect(route('orderAdmin'));
        }
    }

    public function exportToExcel() {
        try {
            $orders = Order::orderBy('created_at', 'desc')
                ->where('status', '>', 0)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();
            $data = array();
            $data[] = array(
                'họ tên',
                'email',
                'số điện thoại',
                'địa chỉ',
                'sản phẩm',
                'ghi chú',
                'tổng tiền'
            );
            foreach ($orders as $order) {
                $orderItems = OrderItem::join('products','products.product_id','=', 'order_items.product_id')
                    ->join('posts', 'products.post_id','=','posts.post_id')
                    ->select(
                        'posts.*',
                        'products.price',
                        'products.discount',
                        'products.code',
                        'order_items.*'
                    )
                    ->where('order_id', $order->order_id)
                    ->where('order_items.theme_code', $this->themeCode)
                    ->where('order_items.user_email', $this->emailUser)->get();
                $products = '';
                foreach ($orderItems as $orderItem) {
                    $products .= $orderItem->quantity.' sản phẩm '.$orderItem->title.'('.route('product', ['post_slug' => $orderItem->slug]).'), ';
                }

                $data[] = array(
                    $order->shipping_name,
                    $order->shipping_email,
                    $order->shipping_phone,
                    $order->shipping_address,
                    $products,
                    $order->shipping_note,
                    $order->total_price
                );
            };

            $date = new \DateTime();
            $fileName = "don-hang-".$date->format("d/m/y");
            Excel::create($fileName, function($excel) use($data) {

                $excel->sheet('Sheetname', function($sheet) use($data) {

                    $sheet->fromArray($data);
                    $sheet->getStyle('E')->getAlignment()->setWrapText(true);

                });

            })->download('xls');
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi xuất đơn hàng: dữ liệu không hợp lệ.');
            Log::error('http->admin->OrderController->exportToExcel: Lỗi xảy ra trong quá trình xuất đơn hàng');

            return null;
        }

    }

	public function addOrder (Request $request) {
		
	}
}
