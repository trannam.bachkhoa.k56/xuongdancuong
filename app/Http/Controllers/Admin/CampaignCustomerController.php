<?php

namespace App\Http\Controllers\Admin;

use App\Entity\CampaignCustomerUser;
use App\Entity\CampaignCustomer;
use App\Entity\Process;
use App\Entity\Contact;
use App\Ultility\CheckPermission;
use App\Ultility\Error;
use Illuminate\Http\Request;
use App\Entity\User;
use App\Entity\MailAutomation;
use App\Entity\DomainUser;
use Carbon\Carbon;
use App\Entity\GroupMail;
use App\Ultility\Ultility;
use App\Entity\CampaignCustomerContact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;
use Psy\Util\Str;
use Validator;

class CampaignCustomerController  extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                
				return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'communicate');

            return $next($request);
        });

    }

    public function index() {

        return view('admin.campaign_customer.list');
    }

    public function create(){

         $managers = User::where('theme_code', $this->themeCode)
        ->where('user_email',$this->emailUser)
        ->orWhere('email', $this->emailUser )
        ->get();
        
        $employees = DomainUser::join('users', 'users.id', 'domain_user.user_id')
		->where('domain_id', $this->domainUser->domain_id)
        ->get();

        return view('admin.campaign_customer.add',compact('managers','employees'));
    }

    public function store(Request $request){

        $process = $request->input('process');
        DB::beginTransaction();
        //Lưu dữ liệu vào bảng campain_customer
        try {
            $campaign_customer = CampaignCustomer::insertGetId([
            'campaign_title' => $request->input('campaign_title'),
            'manager_id' => $request->input('manager_id'),
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
//
        } catch (Exception $e) {
            DB::rollback();
        }

        //Lưu dữ liệu người tham gia vào bảng campaign_cus_user
         try {
			if (!empty($request->input('employees'))) {
				foreach ($request->input('employees') as $id  => $employee_id) {
					$campaign_cus_user = CampaignCustomerUser::insertGetId([
						'campaign_cus_id' => $campaign_customer,
						'user_id' => $employee_id,
						'theme_code' => $this->themeCode,
						'user_email' => $this->emailUser,
						'created_at' => new \DateTime(),
						'updated_at' => new \DateTime()
					]);
				}
			}
        } catch (Exception $e) {
            DB::rollback();
        }
      
        // //Lưu dữ liệu người tham gia vào bảng Tiến trình theo từng userr
        try {
			if (!empty($request->input('process'))) {
				foreach ($request->input('process') as $id  => $process) {
					$process = Process::insertGetId([
						'process_name' => $process,
						'campain_customer_id' => $campaign_customer,
						'theme_code' => $this->themeCode,
						'user_email' => $this->emailUser,
						'created_at' => new \DateTime(),
						'updated_at' => new \DateTime()
					]);
				}	
			}
//
        } catch (Exception $e) {
            DB::rollback();
        }
//
        DB::commit();
        return redirect(route('campaign-customer.index'));
    }

    public function edit($id){

        $thisUser = Auth::user()->id;
        view()->share('menuTop', 'show_campains');

        $managers = User::where('theme_code', $this->themeCode)
        ->where('user_email',$this->emailUser)
        ->orWhere('email', $this->emailUser )
        ->get();

        // lấy ra toàn bộ thành viên tham gia vào website
        $employeesCampains = CampaignCustomerUser::where('campaign_cus_id',$id)
        ->pluck('user_id')->toArray();

        // lấy ra các thành viên trong hệ thống
		$employees = DomainUser::join('users', 'users.id', 'domain_user.user_id')
            ->select('users.name', 'domain_user.user_id')
		->where('domain_id', $this->domainUser->domain_id)
        ->get();
		
        // lấy ra toàn bộ khách hàng trong hệ thống để chọn
        $contactsNullCampaign = Contact::where('theme_code', $this->themeCode)
        ->where('user_email', $this->emailUser)
        ->get();

        // lấy ra thông tin của chiến dịch
        $campaign_customers = CampaignCustomer::where('campaign_customer_id',$id)
         ->where('theme_code', $this->themeCode)
         ->where('user_email',$this->emailUser)
        ->first();

        // lây ra toàn bộ tiến trình thuộc chiến dịch
        $proceses = Process::where('campain_customer_id',$id)
        ->get();

        //Tùy biến kabann chiến dịch 
         $contact = new Contact();
		$messageErrorVip = CheckPermission::checkContacts(Auth::user());
		if (!empty($messageErrorVip)) {
			return view('admin.contact.list', compact('messageErrorVip'));
		}

        // lấy ra toàn bộ khách hàng thuộc chiến dịch
		$contacts = $contact
        ->join('campaign_customer_contact', 'campaign_customer_contact.contact_id', 'contact.contact_id')
        ->leftJoin('campaign_customer', 'campaign_customer.campaign_customer_id', 'campaign_customer_contact.campaign_customer_id')
		->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
		->leftJoin('users','users.id','campaign_customer_contact.user_responsibility')
			->select(
				'contact.contact_id',
				'contact.name',
				'contact.color',
                'campaign_customer_contact.status',
                'users.name as user_name_responsibility',
				DB::raw("SUM(order_contact.price) as sum_order")
			)
			//->where('contact.theme_code', $this->themeCode)
			//->where('contact.user_email', $this->emailUser)
			->groupBy(
			     'contact.contact_id',
                'contact.name',
                'contact.color',
                'campaign_customer_contact.status',
                'users.name'
	      )
			->orderBy('contact.contact_id', 'desc')
			->paginate(50);

        return view('admin.campaign_customer.edit',compact('managers',
                'campaign_customers',
                'employeesCampains',
                'proceses',
                'contacts',
                'thisUser',
                'contactsNullCampaign',
				'employees'
             ));

    }

    public function filterTimeCampaign(Request $request){

        $time_input = $request->input('time');

        $date_start = $request->input('start_date');
        $date_end = $request->input('end_date');

        $id = $request->input('campaign_id');

        $proceses = Process::where('campain_customer_id',$id)
        ->get();

        //Tùy biến kabann chiến dịch 
         $contactModel = new Contact();

        //try {
        $messageErrorVip = CheckPermission::checkContacts(Auth::user());
        if (!empty($messageErrorVip)) {
            return view('admin.contact.list', compact('messageErrorVip'));
        }


        $contacts = $contactModel->leftJoin('campaign_customer','campaign_customer.campaign_customer_id','contact.campaign_id')
        ->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
            ->select(
                'contact.contact_id',
                'contact.name',
                'contact.email',
                'contact.phone',
                'contact.address',
                'contact.message',
                'contact.status',
                'contact.color',
                'contact.campaign_id',
                'contact.campaign_status',
                'contact.created_at',
                'contact.updated_at',
                DB::raw("SUM(order_contact.price) as sum_order")
            )
            ->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->groupBy(
               'contact.contact_id',
               'contact.name',
               'contact.email',
               'contact.phone',
               'contact.address',
               'contact.message',
               'contact.color',
               'contact.status',
               'contact.campaign_id',
               'contact.campaign_status',
               'contact.created_at',
               'contact.updated_at'
           );

            if (!empty($time_input)) {
                $time_now = Carbon::now()->toDateString();

               switch ($time_input) {
                    case 'THIS_WEEK':
                       # code...
                       //lấy thời gian đầu và cuối tuần này 
                    $time_start = Carbon::parse('last monday of last week')->subDay(7)->toDateString();
                    $time_end = Carbon::parse('last sunday of last week')->subDay(7)->toDateString();

                    $contacts = $contacts->whereBetween('contact.updated_at', array($time_start,$time_end));
                       break;
                    case 'LAST_WEEK':
                        # code...
                    //lấy thời gian đầu và cuối tuần này trừ 7 ngày 
                    $time_start = Carbon::parse('last monday of last week')->toDateString();
                    $time_end = Carbon::parse('last sunday of last week')->toDateString();
                  
                    $contacts = $contacts->whereBetween('contact.updated_at', array($time_start,$time_end));
                       break;
                    case 'NEXT_WEEK':
                       # code...
                       break;
                    case 'THIS_MONTH':
                       # code...
                        //Lấy thời gian rồi tách tháng ra 
                        $time_explode = explode('-', $time_now);
                        $time = $time_explode[1];

                        $contacts = $contacts->whereMonth('contact.updated_at', $time);
                      
                       break;
                    case 'LAST_MONTH':
                       # code...
                        $last_month = Carbon::now()->subMonth()->toDateString();
                        $time_explode = explode('-', $last_month);
                        $time = $time_explode[1];

                        $contacts = $contacts->whereMonth('contact.updated_at', $time);
                       
                       break;

                    case 'NEXT_MONTH':
                       # code...

                       break;

                    case 'THIS_DAY':
                       # code...
                        //lấy ra thời gian hiện tại
                        $time = $time_now;
                        $contacts = $contacts->whereDate('contact.updated_at', $time);
                     
                    break;
                   
                    case 'THIS_YEAR':
                       # code...
                    //lấy thời gian xong tách năm ra 
                        $time_explode = explode('-', $time_now);
                        $time = $time_explode[0];
                        $contacts = $contacts->whereYear('contact.updated_at', $time);
                      
                        break;
                    case 'LAST_YEAR':
                       # code...
                        //lấy thời gian xong tách năm ra 
                        $last_year = Carbon::now()->subYear()->toDateString();
                        $time_explode = explode('-', $last_year);
                        $time = $time_explode[0];

                        $contacts = $contacts->whereYear('contact.updated_at', $time);
                      
                       break;

                         case 'TIME_REQUEST':
                        # code...
                        //lấy thời gian đầu 
                         
                        //Lấy thời gian cuối

                        $time_start = $date_start;
                        $time_end = $date_end;
                        
                        $contacts = $contacts->whereBetween('contact.updated_at', array($time_start,$time_end));
                    

                        break;
                    default:
                       # code...
                    break;
                   }
            $contacts = $contacts->orderBy('contact.contact_id', 'desc')->paginate(50);

            return view('admin.campaign_customer.filter_time',compact(
                'proceses',
                'contacts'
             ));
        return redirect()->back();
    }
}




    public function update(Request $request, $id ){

        $campain_customer_model = new CampaignCustomer();

        $campaign_customers = $campain_customer_model->where('campaign_customer_id',$id)
        ->update([
            'campaign_title' => $request->input('campaign_title'),
            'manager_id' => $request->input('manager_id'),
        ]);

        
         return redirect(route('campaign-customer.index'));
    
    }

    public function show(){
    }

    public function destroy($id){

       try {
            DB::beginTransaction();
            $post = CampaignCustomer::where('campaign_customer_id',$id)
            ->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi xóa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->CampaignCustomerController->destroy: Lỗi xảy ra trong quá trình xóa bài viết');
        } finally {
            return redirect('admin/campaign-customer');
        }
    }

    public function anyDatatables(Request $request) {

        $campaignCustomers = CampaignCustomer::leftJoin('users','users.id','campaign_customer.manager_id')
        ->select(
            'campaign_customer.*',
            'users.name'
        )
        ->where('campaign_customer.user_email', $this->emailUser)
        ->where('campaign_customer.theme_code', $this->themeCode);

        // là cộng sự đăng nhập lấy ra tất cả chiến dịch liên quan đến cộng sự đó.
        if (!empty($this->userIdPartner)) {
            $campaignsWithUserId = CampaignCustomerUser::where('user_id', $this->userIdPartner)
                ->pluck('campaign_cus_id as campaign_customer_id')->toArray();

            $campaignCustomers = $campaignCustomers->whereIn('campaign_customer.campaign_customer_id', $campaignsWithUserId);
        }

        return Datatables::of($campaignCustomers)
           ->addColumn('action', function($campaign_customer) {
               $string =  '<a href="'.route('campaign-customer.edit', ['id' => $campaign_customer->campaign_customer_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                       </a>';
               $string .= '<a  href="/admin/campaign-customer/'.$campaign_customer->campaign_customer_id.'" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
               $string .= '<a style="margin-left:5px" href="'.route('add_mail_automation', ['id' => $campaign_customer->campaign_customer_id]).'" class="btn btn-success">
                               <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>';

               return $string;
           })
           
        ->orderColumn('campaign_customer.campaign_customer_id', 'campaign_customer.campaign_customer_id desc')
        ->make(true);
    }
    
    public function changeProcess(Request $request){

        $process_id = $request->input('process_id');
        $process_title =$request->input('process_title');

        $proceses = Process::where('process_id',$process_id)
        ->update([
            'process_name' => $process_title,
        ]);

    }

    public function updateCampaignStatus(Request $request){

        $contact_id = $request->input('contact_id');
        $campaign_status = $request->input('campaign_status');
        $campaignCustomerId = $request->input('campaign_customer_id');

        $contact = CampaignCustomerContact::where('contact_id', $contact_id)
        ->where('campaign_customer_id', $campaignCustomerId)
        ->update([
            'status' => $campaign_status,
        ]);

    }

    public function storeContact(Request $request){

        //print_r($request->input('name'));
        $campaignCustomerContact = new CampaignCustomerContact();

        $campaignCustomerContact->insert([
            'campaign_customer_id' => $request->input('campaign_id'),
            'contact_id' => $request->input('contact'),
            'user_responsibility' => $request->input('user_responsibility'),
            'status' => $request->input('campaign_status'),
            'created_at' => new \DateTime()
        ]);

       return redirect()->back();
    }


    public function listMailAutomation(){
        return view('admin.campaign_customer.list_email_campaign');
    }
     public function addMailAutomation($id){
        
        $campaign = CampaignCustomer::where('campaign_customer_id',$id)
        ->where('theme_code', $this->themeCode)
        ->where('user_email', $this->emailUser)
        ->first();

        $groupMails = GroupMail::orderBy('group_mail_id', 'desc')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->get();

        $emailModel = new MailAutomation();
        $emails = $emailModel->where('theme_code',$this->themeCode)
        ->where('user_email',$this->emailUser)
        ->paginate(5);

        $proceses = Process::where('campain_customer_id',$id)->get();

        return view('admin.campaign_customer.add_email_campaign',compact('campaign','groupMails','emails','proceses'));
      
    }

    public function uploadExcel(Request $request){
        $userName = Auth::id();
        if(isset(Auth::user()->name)){
            $userName = Auth::user()->name;
        }
        if ($request->hasFile('file')){
            $file = $request->file('file');
            $fileName =  'Danh-sach-khach-hang-tai-len-' . str_slug($userName) . '-' . time() . '.' . $file->getClientOriginalExtension();
            $file->move('adminstration/excel/customer/', $fileName);
            $results =Excel::load('adminstration/excel/customer/'. $fileName, function($reader) {
                $reader->get();
            })->get();
            foreach ($results as $result){
                $name = $result->ho_va_ten_khach_hang;
                $phone = $result->sdt;
                $email = $result->email;
                $tag = $result->tag_khach_hang;
                $status = $result->trang_thai_khach_hang;

                if (empty($email) || empty($phone)) {
                    continue;
                }
                
                try{
                    $contact = new Contact();
                    $contactId = $contact->insertOrUpdateCustomer($this->emailUser, $this->themeCode, $name, $phone, $email, $tag, 0, $status);

                    // lấy ra trạng thái chiến dịch
                    $proceses = Process::where('campain_customer_id', $request->input('campaign_customer_id'))
                        ->first();
                    //thêm vào chiến dịch
                    $campaignCustomerContact = new CampaignCustomerContact();

                    $campaignCustomerContact->insert([
                        'campaign_customer_id' => $request->input('campaign_customer_id'),
                        'contact_id' => $contactId,
                        'user_responsibility' => Auth::user()->id,
                        'status' => $proceses->process_id,
                        'created_at' => new \DateTime()
                    ]);
//
                } catch (\Exception $exception){
                    return redirect()->back()->with('failed', 'Không thể đọc dữ liệu khách hàng của bạn. File của bạn tải lên không đúng mẫu của chúng tôi.');
                }
            }
            return redirect()->back()->with('success', 'Bạn đã tải lên dữ liệu khách hàng thành công.');
        }
        return redirect()->back()->with('failed', 'Bạn cần tải lên dữ liệu khách hàng.');
    }
   

}
