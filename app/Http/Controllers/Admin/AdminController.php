<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/16/2017
 * Time: 9:24 AM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\OrderCustomer;
use App\Entity\ShowWebsite;
use Carbon\Carbon;
use App\Entity\Domain;
use App\Entity\NoteContact;
use App\Entity\Notification;
use App\Entity\Order;
use App\Entity\OrderContact;
use App\Entity\ViewsWebsite;
use App\Entity\Post;
use App\Entity\RemindContact;
use App\Entity\Theme;
use App\Entity\TypeSubPost;
use App\Entity\Contact;
use App\Entity\User;
use App\Entity\TypeInformation;
use App\Entity\Information;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ultility\CheckPermission;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
    protected $themeCode;
    protected $emailUser;
	protected $userIdCurrent;
    protected $isSale;
    protected $domainUser;
    protected $accountMessage;
    protected $userIdPartner;

    public function __construct($countNotifi = 12){		
        try {
		
            $this->domainUser = $this->checkUserAndDomain();

            $countNotification = new Notification();
            $countReport = $countNotification->countReport();
			
            $typeSubPostsAdmin = TypeSubPost::orderBy('type_sub_post_id')
                ->where('theme_code', $this->themeCode)
                ->get();
            $notifications = Notification::orderBy('notify_id', 'desc')
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->offset(0)->limit($countNotifi)->get();

            $user = User::where('email', $this->emailUser)->first();
            $checkPermission = new CheckPermission();
            $this->accountMessage = $checkPermission->checkVip($user);

            $gift = 0;
            if (isset($_COOKIE["gift"])) {
                $gift = unserialize($_COOKIE['gift'], ["allowed_classes" => false]);
            }
			
			$countViewsWebsite = Post::where('theme_code', $this->themeCode)
			->where('user_email', $this->emailUser)
			->sum('views');


            // lấy ra cookie đăng nhập của partner
            if (isset($_COOKIE["userIdPartner"])) {
                $userIdPartner = unserialize($_COOKIE['userIdPartner'], ["allowed_classes" => false]);
                $userIdPartner= Crypt::decrypt($userIdPartner);
                $this->userIdPartner = $userIdPartner['userId'];

            }

            $typeInformations = TypeInformation::orderBy('type_infor_id')
            ->where('theme_code', $this->themeCode)
            ->get();

            // get information
            $informations = Information::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->get();

            $informationShow = array();
            foreach($typeInformations as $id => $typeInformation) {
                $typeInformations[$id]['information'] = '';
                foreach ($informations as $information) {
                    if ($information->slug_type_input == $typeInformation->slug) {
                        $informationShow[$typeInformation->slug] = $information->content;
                        break;
                    }
                }
            }
           
        } catch (\Exception $e) {
            $countReport = 0;
            $notifications = null;
            $typeSubPostsAdmin = null;

            Log::error('Lấy dạng bài viết và thông báo: '.$e->getMessage());

        

        } finally {
            view()->share([
                'information' => $informationShow,
                'menuTop' => 'statistic',
				'countRp'=>$countReport,
                'notifications'=>$notifications,
                'typeSubPostsAdmin' => $typeSubPostsAdmin,
                'themeIsSale' => $this->isSale,
                'domainUser' => $this->domainUser,
				'emailUser' => $this->emailUser,
				'themeCode' => $this->themeCode,
                'accountMessage' => $this->accountMessage,
                'gift' => isset($gift) ? $gift : '',
				'countViewsWebsite' => isset($countViewsWebsite) ? $countViewsWebsite : '',
                'userIdPartner' => isset($this->userIdPartner) ? $this->userIdPartner : null  
            ]);
        }

        
    }

    private function checkUserAndDomain () {
        try {
            ini_set('session.gc_maxlifetime', 1000000);
            session_start();

            $domainUrl = Ultility::getCurrentDomain();
            $domain = Domain::where('url',  $domainUrl)
			->orWhere('domain', $domainUrl)
			->first();
			
            if (empty($domain)) {
                $this->themeCode = 'vn3c';
                $this->emailUser = 'vn3ctran@gmail.com';
                $this->isSale = 1;
                $_SESSION['loginSuccessAdmin'] = 'vn3ctran@gmail.com';
                $_SESSION['emailFolder'] = 'library'.'vn3c'.'-'.'1';
            } else {
                $user = User::where('id', $domain->user_id)->first();
                $theme = Theme::where('theme_id', $domain->theme_id)->first();
                $this->themeCode = $theme->code;
                $this->emailUser = $user->email;
                $this->isSale = $theme->is_sale;

                if (User::isManager($user->role) || User::isEditor($user->role)) {
                    $_SESSION['loginSuccessAdmin'] = $user->email;
                    $_SESSION['emailFolder'] = 'library'.$this->themeCode.'-'.$user->id;
                }
            }

            return $domain;
        } catch (\Exception $e) {
            $this->themeCode = 'vn3c';
            $this->emailUser = 'vn3ctran@gmail.com';
            $this->isSale = 1;
            $_SESSION['loginSuccessAdmin'] = 'vn3ctran@gmail.com';
            $_SESSION['emailFolder'] = 'library'.'vn3c'.'-'.'1';

            Log::error('kiểm tra user and domain: '.$e->getMessage());
        }
    }

    protected function createSlug($request) {
        try {
            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }
        } catch (\Exception $e) {
            $slug = rand(10,10000000);

        } finally {
            return $slug;
        }
    }

    public function home() {
        $user = Auth::user();
        $role = $user->role ;

        if (isset($_COOKIE["role"])) {
            $role = unserialize($_COOKIE['role'], ["allowed_classes" => false]);
        }

		// tính tổng lượt view về từ website
	
		/*$viewsWebsite = ViewsWebsite::where('theme_code', $this->themeCode)
		->where('user_email', $this->emailUser)
		->groupBy('source', 'count', 'theme_code', 'user_email')
		->select('source', 'count', 'theme_code', 'user_email')
		->get(); */
		
        $countPost = Post::where('post_type', 'post')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('views', '>', 50)
            ->whereDate('created_at', '<', Carbon::now()->subDays(30))
            ->count();

        $countProduct = Post::where('post_type', 'product')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('views', '>', 50)
            ->count();

        $countUser = User::count();
        $countOrder = Order::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->count();
        $orders = Order::
            select(
                DB::raw('SUM(total_price) as total_sum'),
                DB::raw('YEAR (created_at) as year'),
                DB::raw('QUARTER(created_at) as quarter')
            )
            ->groupBy (
                DB::raw('YEAR (created_at)'),
                DB::raw('QUARTER(created_at)')
                )
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->get();

        $contact = new Contact();

        // lấy ra số khách hàng mới
        $null = array('NULL', 0);
        $countContactNews = $contact->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->count();
			
		// nhóm khách hàng theo nguồn
		$customerNews = $contact->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
			->select(DB::raw('count(*) as count_customer_new, utm_source'))
			->groupBy('utm_source')
            ->get();

        // tính ra số trao đổi
        $noteContacts = NoteContact::join('contact', 'contact.contact_id', 'note_contact.contact_id')
            ->select('note_contact.*')
            ->orderBy('note_contact_id', 'desc')
            ->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->count();

        $remindContacts = RemindContact::join('contact', 'contact.contact_id', 'remind_contact.contact_id')
            ->select('remind_contact.*')
            ->orderBy('remind_contact_id', 'desc')
            ->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->count();

        $orderContacts = OrderContact::join('contact', 'contact.contact_id', 'order_contact.contact_id')
            ->select('order_contact.*')
            ->orderBy('order_contact_id', 'desc')
            ->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->count();

        // tính số khách hàng đã đặt hàng
        $countContactOrders = $contact->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->where('contact.status', 2)
            ->count();

        // tính tổng tiền khách hàng đặt hàng
        $sumPriceContactOrders = $contact->leftJoin('order_contact', 'order_contact.contact_id', 'contact.contact_id')
            ->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
            ->where('contact.status', 2)
            ->sum('order_contact.price');
			
		// nhóm khách hàng đã mua hàng theo nguồn
		$customerOrders = $contact->where('contact.theme_code', $this->themeCode)
            ->where('contact.user_email', $this->emailUser)
			->where('contact.status', 2)
			->select(DB::raw('count(*) as count_customer_new, utm_source'))
			->groupBy('utm_source')
            ->get();	
		
        $countNewContact = $contact->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->where(function ($query){
                $query->where('status', 0)->orWhereNull('status');
            })->count();
        $countApproachContact = $this->countContact($contact, 1);
        $countOrderContact = $this->countContact($contact, 2);
        $countCompletedContact = $this->countContact($contact, 3);

        return view('admin.home.index', compact(
            'countPost',
            'countProduct',
            'countUser',
            'orders',
            'countOrder',
            'countContactNews',
            'countContactOrders',
            'sumPriceContactOrders',
            'noteContacts',
            'remindContacts',
            'orderContacts',
			'customerNews',
			'customerOrders',
            'countNewContact',
            'countApproachContact',
            'countOrderContact',
            'countCompletedContact'
			// 'countViewsWebsite',
			//'viewsWebsite'
        ));
    }

    public function dateline() {
		
        return view('admin.upgrade.index');
    }

    private function countContact(Contact $contact, $status = ''){
        return $contact->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('status', $status)
            ->count();
    }
	
	public function help (Request $request) {
		$menuTop = 'help';
		
		return view('admin.help.index', compact('menuTop'));
    }
    
    public function statistic (Request $request) {
        $showWebsites = ShowWebsite::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser);
            
        $viewWebsites = ViewsWebsite::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser);

        $countContacts = Contact::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('utm_source', 'ads_moma');
            
        $sumOrderCustomer = OrderCustomer::join('contact', 'contact.contact_id', 'order_customer.contact_id')
            ->where('order_customer.theme_code', $this->themeCode)
            ->where('order_customer.user_email', $this->emailUser)
            ->where('utm_source', 'ads_moma');

        if ($request->has('start_date') && $request->has('end_date')) {
            $showWebsites =  $showWebsites->whereBetween('created_at', [$request->input('start_date'),  $request->input('end_date')]);
            $viewWebsites =  $viewWebsites->whereBetween('created_at', [$request->input('start_date'),  $request->input('end_date')]);
            $countContacts = $countContacts->whereBetween('contact.created_at', [$request->input('start_date'),  $request->input('end_date')]);
            $sumOrderCustomer = $sumOrderCustomer->whereBetween('contact.created_at', [$request->input('start_date'),  $request->input('end_date')]);
        }   

        $showWebsites =  $showWebsites->sum('count');
        $viewWebsites =  $viewWebsites->sum('count');
        $countContacts = $countContacts->count();
        $sumOrderCustomer =  $sumOrderCustomer->sum('total_price');

        return view ('admin.home.ads_statistic', compact('viewWebsites', 'countContacts', 'sumOrderCustomer', 'showWebsites'));
    }
}
