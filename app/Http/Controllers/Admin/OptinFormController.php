<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 1/3/2019
 * Time: 9:06 AM
 */

namespace App\Http\Controllers\Admin;


use App\Entity\EmailAutomation;
use App\Entity\User;
use App\Entity\OptinForm;
use App\Entity\CampaignCustomer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class OptinFormController extends AdminController
{
    protected $role;

     public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

			view()->share('menuTop', 'communicate');
			
            return $next($request);
        });

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = OptinForm::where('theme_code',$this->themeCode)
        ->where('user_email', $this->emailUser)
        ->paginate(10);
        return View('admin.optin_form.list',compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $campaigns = CampaignCustomer::where('user_email',$this->emailUser)
        ->where('theme_code', $this->themeCode)
        ->get();
        return View('admin.optin_form.add',compact('campaigns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {
            // insert to database
            $optinFormModel = new OptinForm();

            $template = $optinFormModel->insertGetId([

                'form_title' => $request->input('form_title'),
                'form_description' =>  $request->input('form_description'),
                'campaign_id' => $request->input('campaign'),
                'showName' => $request->input('showName'),
                'showPhone' =>  $request->input('showPhone'),
                'showEmail' => $request->input('showEmail'),
                //'showFacebook' => $request->input('showFacebook'),
                'showDescription' =>  $request->input('showDescription'),
                'showAddress' => $request->input('showAddress'),
                'formRedirect' => $request->input('formRedirect'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at'=> new \DateTime

            ]);


        return redirect(route('optin-form.index')); 

        } catch (\Exception $e) {
            Log::error('http->admin->OptinForm->store: Lỗi thêm mới template');
        } 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(OptinForm $optinForm)
    {   

         return View('admin.optin_form.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(OptinForm $optinForm){

        $form = OptinForm::where('form_id',$optinForm->form_id)
        ->first();

        $campaigns = CampaignCustomer::where('user_email',$this->emailUser)
        ->where('theme_code', $this->themeCode)
        ->get();
        return view('admin.optin_form.edit',compact('form','campaigns'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OptinForm $optinForm)
    {
        try {
            $optinFormModel = new OptinForm();
            $template = $optinFormModel->where('form_id',$optinForm->form_id)
            ->update([
                'form_title' => $request->input('form_title'),
                'form_description' =>  $request->input('form_description'),
                //'showCompanyName' => $request->input('showCompanyName'),
                'showName' => $request->input('showName'),
                'showPhone' =>  $request->input('showPhone'),
                'showEmail' => $request->input('showEmail'),
               // 'showFacebook' => $request->input('showFacebook'),
                'showDescription' =>  $request->input('showDescription'),
                'showAddress' => $request->input('showAddress'),
                'formRedirect' => $request->input('formRedirect')

            ]);


            return redirect(route('optin-form.index')); 

        } catch (\Exception $e) {
            Log::error('http->admin->OptinForm->update: Lỗi chỉnh sửa form');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(OptinForm $optinForm)
    {
        try {
            if ($optinForm->theme_code != $this->themeCode || $optinForm->user_email != $this->emailUser) {
                return redirect('admin/optin-form');
            }

            OptinForm::where('form_id', $optinForm->form_id)->delete();

        } catch (\Exception $e) {
            Log::error('http->admin->OptinFormController->destroy: Lỗi xóa template');
        } finally {
            return redirect('admin/optin-form');
        }
    }

}
