<?php

namespace App\Http\Controllers\Admin;

use App\Entity\FanpageManager;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FacebookManagerController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }
			

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            $fanpages = FanpageManager::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->get();

            view()->share([
                'menuTop' => 'facebook',
                'fanpages' => $fanpages
            ]);

            return $next($request);
        });
    }

    public function index (Request $request) {
        $fanpages = FanpageManager::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->get();

        return view('admin.facebook_manager.index', compact('fanpages'));
    }

    public function config (Request $request) {
        FanpageManager::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->delete();

        $nameFanpages = $request->input('name_fanpage');
        $linkFanpages = $request->input('link_fanpage');

        foreach ($nameFanpages as $id => $nameFanpage) {
            FanpageManager::insert([
                'name_fanpage' => $nameFanpage,
                'link_fanpage' => $linkFanpages[$id],
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'slug' => Ultility::createSlug($nameFanpage),
                'created_at' => new \DateTime()
            ]);
        }

        return redirect()->back();
    }

    public function detailFanpage($slug, Request $request) {
        $fanpage = FanpageManager::where('slug', $slug)
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->first();

        return view('admin.social.facebook', compact('fanpage'));
    }
}