<?php

namespace App\Http\Controllers\Admin;

use App\Entity\GroupHelpVideo;
use App\Entity\HelpVideo;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;

class HelpVideoController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }
            view()->share('menuTop', 'setting');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.help_video.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groupHelpVideos = GroupHelpVideo::orderBy('group_id', 'desc')->get();

        return view('admin.help_video.add', compact('groupHelpVideos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'unique:help_video',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/help-video/create')
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $helpVideo = new HelpVideo();
        $helpVideo->insert([
            'group_help_video' => $request->input('group_help_video'),
            'title' => $request->input('title'),
            'image' => $request->input('image'),
            'embbed_video' => $request->input('embbed_video'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('help-video.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\HelpVideo  $helpVideo
     * @return \Illuminate\Http\Response
     */
    public function show(HelpVideo $helpVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\HelpVideo  $helpVideo
     * @return \Illuminate\Http\Response
     */
    public function edit(HelpVideo $helpVideo)
    {
        $groupHelpVideos = GroupHelpVideo::orderBy('group_id', 'desc')->get();

        return view('admin.help_video.edit', compact('helpVideo', 'groupHelpVideos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\HelpVideo  $helpVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HelpVideo $helpVideo)
    {
        $validation = Validator::make($request->all(), [
            'title' => Rule::unique('help_video')->ignore($helpVideo->help_video_id, 'help_video_id'),
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('help_video.edit', ['help_video_id' => $helpVideo->help_video_id]))
                ->withErrors($validation)
                ->withInput();
        }

        // insert to database
        $helpVideo->update([
            'group_help_video' => $request->input('group_help_video'),
            'title' => $request->input('title'),
            'image' => $request->input('image'),
            'embbed_video' => $request->input('embbed_video'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect(route('help_video.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\HelpVideo  $helpVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(HelpVideo $helpVideo)
    {
        $helpVideo->delete();

        return redirect(route('help_video.index'));
    }

    public function anyDatatables(Request $request)
    {
        $themes = HelpVideo::leftJoin('group_help_video', 'help_video.group_help_video', '=', 'group_help_video.group_id')
            ->select('help_video.*', 'group_help_video.title as title_group');

        return Datatables::of($themes)
            ->addColumn('action', function ($theme) {
                $string = '<a href="' . route('themes.edit', ['theme_id' => $theme->theme_id]) . '">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="' . route('themes.destroy', ['theme_id' => $theme->theme_id]) . '" class="btn btn-danger btnDelete"
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->orderColumn('help_video_id', 'help_video_id desc')
            ->make(true);
    }
}
