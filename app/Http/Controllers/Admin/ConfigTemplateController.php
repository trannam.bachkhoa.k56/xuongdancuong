<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 9/3/2019
 * Time: 4:47 PM
 */

namespace App\Http\Controllers\Admin;

use App\Entity\User;
use App\Entity\Theme;
use App\Entity\Domain;
use App\Entity\CustomerDisplay;
use App\Facebook\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Facebook\Fanpage;
use App\Facebook\People;

class ConfigTemplateController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'websites');

            return $next($request);
        });
    }

    public function index (Request $request) {
        if (Auth::user()->vip == 0) {
            return view('admin.config_template.index');
        }

        return redirect(route('config_header'));
    }

    public function header() {
        return view('admin.config_template.header');
    }

    public function home() {
        return view('admin.config_template.home');
    }

    public function categoryProduct() {
        return view('admin.config_template.category_product');
    }

    public function detailProduct() {
        return view('admin.config_template.detail_product');
    }

    public function category() {
        return view('admin.config_template.category');
    }

    public function single() {
        return view('admin.config_template.single');
    }

    public function advice(){
        //lấy theme code và user email hiện tại 
        $themeCode = $this->themeCode ;
        $emailUser = $this->emailUser;

        //lấy usser id và theme id hiện tại 
        $theme = Theme::where('code',$themeCode)->first();

        $theme_id = isset($theme->theme_id) ? $theme->theme_id : '' ;
        $user = User::where('email',$emailUser)->first();
        $user_id = isset($user->id) ? $user->id : '' ;

        //lấy domain
        $domain = Domain::where('theme_id',$theme_id)->where('user_id',$user_id)->first();
        //Lay cac truong da thay doi
        $customer_display = CustomerDisplay::where('theme_code',$themeCode)->where('user_email', $emailUser)->first();

        return view('admin.config_template.advice', compact('domain','customer_display'));
    }


    public function addCustomerAdivce(Request $request){
        //lấy theme code và user email hiện tại 
        $themeCode = $this->themeCode ;
        $emailUser = $this->emailUser;

        //lấy usser id và theme id hiện tại 
        $theme = Theme::where('code',$themeCode)->first();

        $theme_id = isset($theme->theme_id) ? $theme->theme_id : '' ;
        $user = User::where('email',$emailUser)->first();
        $user_id = isset($user->id) ? $user->id : '' ;

        //lấy domain 
        $domain = Domain::where('theme_id',$theme_id)->where('user_id',$user_id)->first();

        //lấy thông tin hiển thị 
        $customer_display = CustomerDisplay::where('theme_code',$themeCode)->where('user_email', $emailUser)->first();

        
        //thêm thông tin các trường vào csdl 
        $statusName = $request->input('statusName');
        $statusEmail = $request->input('statusEmail');
        $statusPhone = $request->input('statusPhone');
        $statusMessage = $request->input('statusMessage');

        if($request->input('statusName') == NULL ){
            $statusName = 0 ;
        }
        if($request->input('statusEmail') == NULL ){
            $statusEmail = 0 ;
        }
        if($request->input('statusPhone') == NULL ){
             $statusPhone = 0 ;
        }
        if($request->input('statusMessage') == NULL){
            $statusMessage = 0 ;
        }
        
        $buttonName = $request->input('buttonName');

        $message = $request->input('message');
               
        $customer_display = CustomerDisplay::insertGetId([
           'statusName' =>$statusName ,
           'statusPhone'=> $statusPhone ,
           'statusEmail'=> $statusEmail ,
           'statusMessage'=> $statusMessage ,
           'buttonName' => $buttonName,
           'messageChange'=> $message,
           'theme_code' =>$themeCode,
           'user_email' =>$emailUser,
           'created_at' => new \DateTime(),
           'updated_at' => new \DateTime()                     
        ]);
        
        // return view('admin.config_template.advice', compact('domain','customer_display'));

    }

    public function editCustomerAdivce(Request $request){
        // //lấy theme code và user email hiện tại 
        $themeCode = $this->themeCode ;
        $emailUser = $this->emailUser;

        // //lấy usser id và theme id hiện tại 
        // $theme = Theme::where('code',$themeCode)->first();

        // $theme_id = isset($theme->theme_id) ? $theme->theme_id : '' ;
        // $user = User::where('email',$emailUser)->first();
        // $user_id = isset($user->id) ? $user->id : '' ;

        // //lấy domain 
        // $domain = Domain::where('theme_id',$theme_id)->where('user_id',$user_id)->first();

        //lấy thông tin hiển thị 
        $customer_display = CustomerDisplay::where('theme_code',$themeCode)->where('user_email', $emailUser)->first();

        if(!empty($customer_display))
        {
            $statusName = $request->input('statusName');
            $statusEmail = $request->input('statusEmail');
            $statusPhone = $request->input('statusPhone');
            $statusMessage = $request->input('statusMessage');

            //thêm thông tin các trường vào csdl 
            if($request->input('statusName') == NULL ){
                $statusName = 0 ;
            }
            if($request->input('statusEmail') == NULL ){
                $statusEmail = 0 ;
            }
            if($request->input('statusPhone') == NULL ){
                 $statusPhone = 0 ;
            }
            if($request->input('statusMessage') == NULL){
                $statusMessage = 0 ;
            }
            
            $buttonName = $request->input('buttonName');

            $message = $request->input('message');
                   
            $display = CustomerDisplay::where('id',$customer_display->id)->update([
               'statusName' =>$statusName ,
               'statusPhone'=> $statusPhone ,
               'statusEmail'=> $statusEmail ,
               'statusMessage'=> $statusMessage ,
               'buttonName' => $buttonName,
               'messageChange'=> $message,          
            ]);
        }
    }




    public function contact() {

        return view('admin.config_template.contact');

    }

    public function footer() {
        return view('admin.config_template.footer');
    }

    public function interestRate(Request $request){
        $month = $request->input('time') * 12;
        $money = (int) str_replace(',','', $request->input('money'));
        $interest = (int) $request->input('interest_rate');
        $result = view('admin.partials.popup_interest', compact('month', 'money', 'interest'));
		
        return $result;
    }

    //xử lý đổi thành chỉnh sửa bằng tay

    public function changeStatusAdivce(Request $request){
        $status = $request->input('status');
        //lấy theme code và user email hiện tại 
        $themeCode = $this->themeCode ;
        $emailUser = $this->emailUser;

        //lấy usser id và theme id hiện tại 
        $theme = Theme::where('code',$themeCode)->first();

        $theme_id = isset($theme->theme_id) ? $theme->theme_id : '' ;
        $user = User::where('email',$emailUser)->first();
        $user_id = isset($user->id) ? $user->id : '' ;

        //lấy domain 
        $domain = Domain::where('theme_id',$theme_id)->where('user_id',$user_id)->update([
            'show_advice_setting' => $status
        ]);
    }
}