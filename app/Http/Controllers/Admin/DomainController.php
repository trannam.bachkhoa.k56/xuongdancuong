<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Domain;
use App\Entity\Information;
use App\Entity\Input;
use App\Entity\Menu;
use App\Entity\NoteDomain;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\SubPost;
use App\Entity\Template;
use App\Entity\Theme;
use App\Entity\Contact;
use App\Entity\OrderCustomer;
use App\Entity\TypeInformation;
use App\Entity\TypeInput;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Ultility\Error;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use App\Ultility\Ultility;

class DomainController extends AdminController
{
    protected $role;
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }
            view()->share('menuTop', 'setting');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domainUrl = Ultility::getCurrentDomain();
        return view('admin.domain.list',compact('domainUrl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $themes = Theme::getAllTheme();
        $users = User::getAllUser();
        return view('admin.domain.add', compact('themes', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'url' => 'unique:domains',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('admin/domains/create')
                ->withErrors($validation)
                ->withInput();
        }

        //lấy thời gian sử dụng
        $userTime = $this->getUseTime($request);

        // insert to database
        $this->insertDomain($request, $userTime);

        return redirect(route('domains.index'));
    }

    private function getUseTime($request) {
        try {
            $userStartEnd = $request->input('use_start_end');
            $userTime = explode('-', $userStartEnd);

            return $userTime;
        } catch(\Exception $e) {
            return null;
        }
    }
    private function insertDomain($request, $userTime) {
        try {
            $domain = new Domain();
            $domain->insert([
                'name' => $request->input('name'),
                'url' => $request->input('url'),
                'description' => $request->input('description'),
                'theme_id' => $request->input('theme_id'),
                'user_id' => $request->input('user_id'),
                'start_at' => isset($userTime[0]) ? new \DateTime($userTime[0]) : null,
                'end_at' => isset($userTime[1]) ? new \DateTime($userTime[1]) : null,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi thêm mới domain: Dữ liệu nhập vào không hợp lệ');
            Log::error('http->admin->DomainController->insertDomain: Lỗi thêm mới domain');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function show(Domain $domain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function edit(Domain $domain)
    {
        $themes = Theme::getAllTheme();
		$user = User::where('id', $domain->user_id)->first();
        $users = array();

        return view('admin.domain.edit', compact('domain', 'themes', 'users', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Domain $domain)
    {
        $validation = Validator::make($request->all(), [
            'url' =>  Rule::unique('domains')->ignore($domain->domain_id, 'domain_id'),
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('domains.edit', ['domain_id' => $domain->domain_id]))
                ->withErrors($validation)
                ->withInput();
        }

        //lấy thời gian sử dụng
        $userTime = $this->getUseTime($request);

        // insert to database
        $this->updateDomain($domain, $request, $userTime);

        return redirect(route('domains.index'));
    }

    private function updateDomain($domain, $request, $userTime) {
        try {
            $domain->update([
                'name' => $request->input('name'),
                'url' => str_replace("https", "http", $request->input('url')),
                'description' => $request->input('description'),
                'theme_id' => $request->input('theme_id'),
                'user_id' => $request->input('user_id'),
                'start_at' => isset($userTime[0]) ? new \DateTime($userTime[0]) : null,
                'end_at' => isset($userTime[1]) ? new \DateTime($userTime[1]) : null,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi cập nhật domain: Dữ liệu không hợp lệ');
            Log::error('http->admin->DomainController->updateDomain: lỗi cập nhật domain');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function destroy(Domain $domain)
    {
        $this->destroyDomain($domain);

        return redirect(route('domains.index'));
    }

    public function deleteAll (Request $request) {
        $idDeletes = $request->input('id_delete');

        $idDeletesToken = explode(',', $idDeletes);
        foreach ($idDeletesToken as $idDelete) {
            if (!empty($idDelete)) {
                $domain = Domain::where('domain_id', $idDelete)->first();

                $this->destroyDomain($domain);
            }
        }

        return redirect(route('domains.index'));
    }

    private function destroyDomain($domain) {
        try {
            DB::beginTransaction();
            // xóa hết thông tin khi xóa domains
            $user = User::where('id', $domain->user_id)->first();
            $theme = Theme::where('theme_id', $domain->theme_id)->first();
            // xóa information
            $information = new Information();
            $information->where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->delete();
            // Xóa menu
            /*$menu = new Menu();
            $menu->where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->delete(); */
            // xóa category
            $category = new Category();
            $category->where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->delete();

            // xoa post
            $postIds = Post::select('post_id')
                ->where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->get();

            Input::whereIn('post_id', $postIds)
                ->delete();

            Product::whereIn('post_id', $postIds)->delete();
			
            // xoa subpost
            SubPost::where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->delete();
				
            // xoa post
            Post::where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->delete();

			// xóa contact
			Contact::where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->delete();
				
			// xóa orderCustomer
			OrderCustomer::where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->delete();
				
            User::where('id', $domain->user_id)->delete();

			
            $domain->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi trong quá trình xóa domain');
            Log::error('http->admin->DomainController->destroyDomain: Lỗi xóa domain');
        }
    }

    public function updateTimeOn(){
        $user = User::where('email', $this->emailUser)->first();
        $theme = Theme::where('code',$this->themeCode)->first();
        $domains = new Domain();
        $domains->where('domains.user_id', $user->id)
//                ->whereNotIn('domains.user_id',[1])
//                ->orWhere('domains.theme_id',$theme->theme_id)
                ->update([
                    'time_on'=>Carbon::now('Asia/Ho_Chi_Minh'),
                    'updated_at'=>Carbon::now('Asia/Ho_Chi_Minh')
        ]);
        return redirect()->back();
    }
	
    public function anyDatatables(Request $request) {
        $domains = Domain::join('themes', 'themes.theme_id', '=', 'domains.theme_id')
            ->join('users', 'users.id', '=', 'domains.user_id')
            ->leftJoin('user_customer_introduct', 'user_customer_introduct.customer_id', 'domains.user_id')
            ->leftJoin('users as user_invite', 'user_invite.id', 'user_customer_introduct.user_id')
            ->select(
                'domains.created_at',
                'domains.url',
                'domains.domain_id',
                'domains.domain',
                'domains.name',
                'domains.action',
                'domains.change_url',
				'domains.time_on',
                'themes.name as theme_name',
                'users.name as user_name',
                'user_invite.name as user_invite_name',
                'users.email',
                'users.phone',
                'users.vip'
            );

        if ($request->has('action')) {
            switch ($request->input('action')) {
                case 'news' :
                    $domains = $domains->where('action', 0);
                    break;
                case 'note' :
                    $domains = $domains->where('action', 1);
                    break;
                case 'change_url' :
                    $domains = $domains->where('change_url', 1);
                    break;
                case 'payment' :
                    $domains = $domains->where('users.vip', '>' ,0);
                    break;
                default:
                    break;
            }
        }

        if ($request->has('invite_user_id')) {
            $domains = $domains->join('customer_introduct', 'customer_introduct.user_id', 'domains.user_id');
        }

        if ($request->has('type')) {
            switch ($request->input('type')) {
                case 1:
                    # code...Mới tạo website
                    // $domains->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')
                    // ->leftJoin('posts', 'posts.user_email', 'users.email')
                   // ->groupBy('domains.domain_id')
                   // ->havingRaw('COUNT(posts.post_id) = 0');

                    break;
                case 2:
                    # code...Nhập sản phẩm
                    /*$domains->leftJoin('posts', 'posts.user_email', 'users.email')
                        ->where('posts.post_type', 'product')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(posts.post_id) > 0')
                        ->distinct(); */
                    break;
                case 3:
                    # code...Nhập tin tức

                    /*$domains->leftJoin('posts', 'posts.user_email', 'users.email')
                        ->where('posts.post_type', 'post')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(posts.post_id) > 0')
                        ->distinct(); */
                    break;
                case 4:
                    # code...Đã có khách hàng
                   /* $domains->leftJoin('contact','contact.user_email', 'users.email')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(contact.contact_id) > 0')
                        ->distinct(); */
                    break;
                case 5:
                    # code...Đã có đơn hàng
                    /*$domains->leftJoin('order_customer','order_customer.user_email', 'users.email')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(order_customer.order_customer_id) > 0')
                        ->distinct();*/

                    break;
                case 6:
                    # code...Đang online
					
                   /*$domains->leftJoin('order_customer','order_customer.user_email', 'users.email')
						->whereNotIn('domains.user_id',[1])
                        ->groupBy('domains.domain_id')
						->having('domains.time_on','>',Carbon::now('Asia/Ho_Chi_Minh')->addMinute(5))
						->having('domains.time_on','>',Carbon::now('Asia/Ho_Chi_Minh')->subMinute(5))
                        ->distinct(); */
						
                    break;

                default:
                    # code...
                    break;
            }
        }


        return Datatables::of($domains)
            ->addColumn('action', function($domain) {
                $string =  '<a href="'.route('domains.edit', ['domain_id' => $domain->domain_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="/admin/domains/'.$domain->domain_id.'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->addColumn('status', function ($domain){
                return $this->statusDomain($domain->domain_id);
            })->addColumn('convention', function ($domain){
                $noteDomain = NoteDomain::where('domain_id', $domain->domain_id)->first();

                if (!empty($noteDomain) ) {
                    if (!empty($noteDomain->content)) {
                        return $noteDomain->content;
                    }

                    $date = date_create($noteDomain->created_at);

                    return 'Ngày khởi tạo: '.date_format($date,"d/m/Y H:i");
                }

                return 'Chưa trao đổi!';
            })
            ->addColumn('dayConn', function ($domain){
                return $this->dayConnNoteDomain($domain->domain_id);
            })
            ->orderColumn('domain_id', 'domain_id desc')
            ->make(true);
    }

    private function dayConnNoteDomain($domainId) {
        $note = NoteDomain::where('domain_id',$domainId)->orderBy('note_domain_id','desc')->first();
        $diff = '';
        if(empty($note)){
            $domain = Domain::where('domain_id', $domainId)->first();
            $noteDate = date('Y-m-d', strtotime($domain->created_at));
            $timeNow = Carbon::now('Asia/Ho_Chi_Minh');
            if($timeNow->toDateString() == $noteDate) {
                return $diff = '0';
            }
            $diff = $timeNow->diffInDays($noteDate).'';

            return $diff;
        }
        $noteDate = date('Y-m-d', strtotime($note->created_at));
        $timeNow = Carbon::now('Asia/Ho_Chi_Minh');
        if($timeNow->toDateString() == $noteDate) {
            return $diff = '0';
        }
        $diff = $timeNow->diffInDays($noteDate).'';
        return $diff;
    }

    private function statusDomain($domainID) {
        $status = '';
        $domain = Domain::where('domain_id', $domainID)->first();
        if((Carbon::now('Asia/Ho_Chi_Minh')->minute - date('i', strtotime($domain->time_on))) < 5) {
           return $status = 'online';
            }
        $status = 'offline';
        return $status;
    }


    public function anyDatatablesFilter(Request $request, $sub) {

        $domainModel = new Domain();
        $domainPost = $domainModel->leftJoin('users', 'users.id','domains.user_id')
            ->leftJoin('posts','posts.user_email', 'users.email')
            ->leftJoin('themes', 'themes.theme_id', 'domains.theme_id');

        switch ($sub) {
            case 1:
                # code...Mới tạo website
                $countPost = 0 ;
                $domainNotEmpty = $domainPost->select('domains.*', DB::raw('COUNT(posts.post_id) as countPost'),'themes.name as theme_name', 'users.email', 'users.phone')
                    ->groupBy('domains.domain_id')
                    ->having('countPost', 0)
                    ->get();
                 return Datatables::of($domainPost)
                ->addColumn('action', function($domain) {
                    $string =  '<a href="'.route('domains.edit', ['domain_id' => $domain->domain_id]).'">
                               <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                           </a>';
                    $string .= '<a  href="/admin/domains/'.$domain->domain_id.'" class="btn btn-danger btnDelete" 
                                data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                   <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </a>';
                    return $string;
                })
                ->addColumn('status', function ($domain){
                    return $this->statusDomain($domain->domain_id);
                })
                ->orderColumn('domain_id', 'domain_id desc')
                ->make(true);
                break;

            case 2:
                # code...Nhập sản phẩm
               $countPost = $domainPost->where('posts.post_type','product')
                //->orWhere('posts.post_type','post')
                ->count();

                if($countPost > 0){
                    $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')
                    ->distinct();
                    return Datatables::of($domainPost)
                    ->addColumn('action', function($domain) {
                        $string =  '<a href="'.route('domains.edit', ['domain_id' => $domain->domain_id]).'">
                                   <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                               </a>';
                        $string .= '<a  href="/admin/domains/'.$domain->domain_id.'" class="btn btn-danger btnDelete" 
                                    data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                       <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>';
                        return $string;
                    })
                    ->addColumn('status', function ($domain){
                        return $this->statusDomain($domain->domain_id);
                    })
                    ->orderColumn('domain_id', 'domain_id desc')
                    ->make(true);

                }

                // return view('admin.domain.filter', compact('domainPost','countPost'));

                break;
            case 3:
                # code...Nhập tin tức 
                $countPost = $domainPost->where('posts.post_type','post')
                //->orWhere('posts.post_type','post')
                ->count();
                if($countPost > 0){
                    $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')->distinct();
                    return Datatables::of($domainPost)
                    ->addColumn('action', function($domain) {
                        $string =  '<a href="'.route('domains.edit', ['domain_id' => $domain->domain_id]).'">
                                   <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                               </a>';
                        $string .= '<a  href="/admin/domains/'.$domain->domain_id.'" class="btn btn-danger btnDelete" 
                                    data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                       <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>';
                        return $string;
                    })
                    ->addColumn('status', function ($domain){
                        return $this->statusDomain($domain->domain_id);
                    })
                    ->orderColumn('domain_id', 'domain_id desc')
                    ->make(true);
                }

                // return view('admin.domain.filter', compact('domainPost','countPost'));
                break;
            case 4:
                # code...Đã có khách hàng 
                $countPost = $domainPost->join('contact','contact.user_email', 'users.email')             
                ->count();

                if($countPost > 0){
                    $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')->distinct();
                     return Datatables::of($domainPost)
                    ->addColumn('action', function($domain) {
                        $string =  '<a href="'.route('domains.edit', ['domain_id' => $domain->domain_id]).'">
                                   <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                               </a>';
                        $string .= '<a  href="/admin/domains/'.$domain->domain_id.'" class="btn btn-danger btnDelete" 
                                    data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                       <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>';
                        return $string;
                    })
                    ->addColumn('status', function ($domain){
                        return $this->statusDomain($domain->domain_id);
                    })
                    ->orderColumn('domain_id', 'domain_id desc')
                    ->make(true);
                }

                // return view('admin.domain.filter', compact('domainPost','countPost'));
                break;
            case 5:
                # code...Đã có đơn hàng 

                $countPost = $domainPost->join('order_customer','order_customer.user_email', 'users.email')             
                ->count();

                if($countPost > 0){
                    $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')->distinct();

                     return Datatables::of($domainPost)
                    ->addColumn('action', function($domain) {
                        $string =  '<a href="'.route('domains.edit', ['domain_id' => $domain->domain_id]).'">
                                   <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                               </a>';
                        $string .= '<a  href="/admin/domains/'.$domain->domain_id.'" class="btn btn-danger btnDelete" 
                                    data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                       <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>';
                        return $string;
                    })
                    ->addColumn('status', function ($domain){
                        return $this->statusDomain($domain->domain_id);
                    })
                    ->orderColumn('domain_id', 'domain_id desc')
                    ->make(true);
                }

                // return view('admin.domain.filter', compact('domainPost','countPost'));

                break;
            case 6:
                # code...Đang online

                $domains = $domainPost->where('domains.time_on','>',Carbon::now('Asia/Ho_Chi_Minh')->subMinute(5))
                                        ->where('domains.time_on','<',Carbon::now('Asia/Ho_Chi_Minh')->addMinute(5))
                                        ->whereNotIn('domains.user_id',[1])
                                        ->count();

                if($domains > 0){
                    $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')->distinct();
                    return Datatables::of($domainPost)
                        ->addColumn('action', function($domain) {
                            $string =  '<a href="'.route('domains.edit', ['domain_id' => $domain->domain_id]).'">
                                   <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                               </a>';
                            $string .= '<a  href="/admin/domains/'.$domain->domain_id.'" class="btn btn-danger btnDelete" 
                                    data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                       <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>';
                            return $string;
                        })
                        ->addColumn('status', function ($domain){
                            return $this->statusDomain($domain->domain_id);
                        })
                        ->orderColumn('domain_id', 'domain_id desc')
                        ->make(true);
                }

                break;
                 
            default:
                # code...
                break;
        }

    }

    public function filterDomain($sub) {

        // $domainModel = new Domain(); 

        // $domainPost = $domainModel->leftJoin('users', 'users.id','domains.user_id')
        // ->leftJoin('posts','posts.user_email', 'users.email')
        // ->leftJoin('themes', 'themes.theme_id', 'domains.theme_id');

    
        switch ($sub) {
            case 1:
                # code...Mới tạo website
                $countPost = 0 ;

                // $domainPost = $domainPost->select('domains.*', DB::raw('COUNT(posts.post_id) as countPosts'))
                // ->groupBy('domains.domain_id')
                // ->having('countPosts', 0)
                // ->orderBy('domains.domain_id', 'DESC') 
                // ->get();

            
                return view('admin.domain.filter', compact('sub'));

                break;
            case 2:
                # code...Nhập sản phẩm
               // $countPost = $domainPost->where('posts.post_type','product')
                //->orWhere('posts.post_type','post')
                // ->count();

                // if($countPost > 0){
                //     $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')
                //     ->orderBy('domains.domain_id', 'desc')
                //     ->distinct()
                //     ->paginate(10);
                // }

               return view('admin.domain.filter', compact('sub'));

                break;
            case 3:
                # code...Nhập tin tức 
                // $countPost = $domainPost->where('posts.post_type','post')
                // //->orWhere('posts.post_type','post')
                // ->count();

                // if($countPost > 0){
                //     $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')
                //     ->orderBy('domains.domain_id', 'desc')
                //     ->distinct()
                //     ->paginate(10);
                // }

                return view('admin.domain.filter', compact('sub'));
               
                break;
            case 4:
                # code...Đã có khách hàng 
                // $countPost = $domainPost->join('contact','contact.user_email', 'users.email')             
                // ->count();

                // if($countPost > 0){
                //     $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')
                //     ->orderBy('domains.domain_id', 'desc')
                //     ->distinct()
                //     ->paginate(10);
                // }

               return view('admin.domain.filter', compact('sub'));
                
                break;
            case 5:
                # code...Đã có đơn hàng 

                // $countPost = $domainPost->join('order_customer','order_customer.user_email', 'users.email')             
                //     ->count();

                // if($countPost > 0){
                //     $domainPost = $domainPost->select('domains.*', 'themes.name as theme_name', 'users.email', 'users.phone')
                //     ->orderBy('domains.domain_id', 'desc')
                //     ->distinct()
                //     ->paginate(10);
                // }

                return view('admin.domain.filter', compact('sub'));
                

                break;
            case 6:
                # code...Đang online

                return view('admin.domain.filter', compact('sub'));
                break;
                 
            default:
                # code...
                break;
        }

        // $domainPost = $domainPost->get();

        // return $domainPost ; 


    }

    public function exportDomain($sub) {

            $domains = Domain::leftJoin('themes', 'themes.theme_id', '=', 'domains.theme_id')
            ->leftJoin('users', 'users.id', '=', 'domains.user_id')
            ->select(
                'domains.created_at',
                'domains.url',
                'domains.domain_id',
                'domains.name',
                'themes.name as theme_name',
                'users.email',
                'users.phone'
            );

			switch ($sub) {
                case 1:
                    # code...Mới tạo website                   
					$domains = $domains->distinct()
                    ->get();
					
					$data = array();
                    $data[] = array(
                        'ID',
                        'Tên domain',
                        'Email',
                        'Số điện thoại',
                        'Đường dẫn',
                        'Ngày hết hạn',
                        'Theme',
                    );
                    foreach ($domains as $domain) {
                        $data[] = array(
                            $domain->domain_id,
                            $domain->name,
                            $domain->email,
                            $domain->phone,
                            $domain->url,
                            $domain->end_at,
                            $domain->theme_name
                        );

                    };
			
					

                    $date = new \DateTime();
                    $fileName = "tat-ca-domain-".$date->format("d/m/y");

                    Excel::create($fileName, function($excel) use($data) {

                        $excel->sheet('Sheetname', function($sheet) use($data) {

                            $sheet->fromArray($data);
                            $sheet->getStyle('E')->getAlignment()->setWrapText(true);

                        });

                    })->download('xls');

                    break;
                case 2:
                    # code...Nhập sản phẩm
                    $domains = $domains->leftJoin('posts', 'posts.user_email', 'users.email')
                        ->where('posts.post_type', 'product')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(posts.post_id) > 0')
                        ->distinct()
                        ->get();
                    $data = array();
                    $data[] = array(
                        'ID',
                        'Tên domain',
                        'Email',
                        'Số điện thoại',
                        'Đường dẫn',
                        'Ngày hết hạn',
                        'Theme',
                    );
                    foreach ($domains as $domain) {
                        $data[] = array(
                            $domain->domain_id,
                            $domain->name,
                            $domain->email,
                            $domain->phone,
                            $domain->url,
                            $domain->end_at,
                            $domain->theme_name
                        );

                    };


                    $date = new \DateTime();
                    $fileName = "tat-ca-domain-".$date->format("d/m/y");

                    Excel::create($fileName, function($excel) use($data) {

                        $excel->sheet('Sheetname', function($sheet) use($data) {

                            $sheet->fromArray($data);
                            $sheet->getStyle('E')->getAlignment()->setWrapText(true);

                        });

                    })->download('xls');
                    break;
                case 3:
                    # code...Nhập tin tức

                    $domains = $domains->leftJoin('posts', 'posts.user_email', 'users.email')
                        ->where('posts.post_type', 'post')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(posts.post_id) > 0')
                    ->distinct()
                        ->get();
                    $data = array();
                    $data[] = array(
                        'ID',
                        'Tên domain',
                        'Email',
                        'Số điện thoại',
                        'Đường dẫn',
                        'Ngày hết hạn',
                        'Theme',
                    );
                    foreach ($domains as $domain) {
                        $data[] = array(
                            $domain->domain_id,
                            $domain->name,
                            $domain->email,
                            $domain->phone,
                            $domain->url,
                            $domain->end_at,
                            $domain->theme_name
                        );

                    };


                    $date = new \DateTime();
                    $fileName = "tat-ca-domain-".$date->format("d/m/y");

                    Excel::create($fileName, function($excel) use($data) {

                        $excel->sheet('Sheetname', function($sheet) use($data) {

                            $sheet->fromArray($data);
                            $sheet->getStyle('E')->getAlignment()->setWrapText(true);

                        });

                    })->download('xls');
                    break;
                case 4:
                    # code...Đã có khách hàng
                    $domains = $domains->leftJoin('contact','contact.user_email', 'users.email')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(contact.contact_id) > 0')
                        ->distinct();
                         $data = array();
                    $data[] = array(
                        'ID',
                        'Tên domain',
                        'Email',
                        'Số điện thoại',
                        'Đường dẫn',
                        'Ngày hết hạn',
                        'Theme',
                    );
                    foreach ($domains as $domain) {
                        $data[] = array(
                            $domain->domain_id,
                            $domain->name,
                            $domain->email,
                            $domain->phone,
                            $domain->url,
                            $domain->end_at,
                            $domain->theme_name
                        );

                    };


                    $date = new \DateTime();
                    $fileName = "tat-ca-domain-".$date->format("d/m/y");

                    Excel::create($fileName, function($excel) use($data) {

                        $excel->sheet('Sheetname', function($sheet) use($data) {

                            $sheet->fromArray($data);
                            $sheet->getStyle('E')->getAlignment()->setWrapText(true);

                        });

                    })->download('xls');
                    break;
                case 5:
                    # code...Đã có đơn hàng
                    $domains = $domains->leftJoin('order_customer','order_customer.user_email', 'users.email')
                        ->groupBy('domains.domain_id')
                        ->havingRaw('COUNT(order_customer.order_customer_id) > 0')
                        ->distinct()
                        ->get();
                    $data = array();
                    $data[] = array(
                        'ID',
                        'Tên domain',
                        'Email',
                        'Số điện thoại',
                        'Đường dẫn',
                        'Ngày hết hạn',
                        'Theme',
                    );
                    foreach ($domains as $domain) {
                        $data[] = array(
                            $domain->domain_id,
                            $domain->name,
                            $domain->email,
                            $domain->phone,
                            $domain->url,
                            $domain->end_at,
                            $domain->theme_name
                        );

                    };


                    $date = new \DateTime();
                    $fileName = "tat-ca-domain-".$date->format("d/m/y");

                    Excel::create($fileName, function($excel) use($data) {

                        $excel->sheet('Sheetname', function($sheet) use($data) {

                            $sheet->fromArray($data);
                            $sheet->getStyle('E')->getAlignment()->setWrapText(true);

                        });

                    })->download('xls');
                    break;
                case 6:
                    # code...Đang online
//                    $domains->where('domains.time_on','>',Carbon::now('Asia/Ho_Chi_Minh')->subMinute(5))
//                        ->where('domains.time_on','<',Carbon::now('Asia/Ho_Chi_Minh')->addMinute(5))
//                        ->whereNotIn('domains.user_id',[1])
//                        ->distinct();
                    break;

                default:
                    # code...
                    break;
            }
         
    }

    public function note(Request $request) {
        $domainId = $request->input('domain_id');
        $note = $request->input('note');

        NoteDomain::insert([
            'domain_id' => $domainId,
            'content' => $note,
            'created_at' => new \DateTime()
        ]);

        Domain::where('domain_id', $domainId)->update([
            'action' => 1
        ]);

        return redirect()->back();
    }
   
}
