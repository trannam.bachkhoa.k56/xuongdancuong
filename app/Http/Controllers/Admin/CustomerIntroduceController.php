<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\Domain;
use App\Entity\Menu;
use App\Entity\MenuElement;
use App\Entity\Post;
use App\Entity\CustomerIntroduct;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Entity\RequestMoney;
use App\Entity\MailConfig;
use App\Entity\PaymentInfomation;
use App\Ultility\Error;
use App\Ultility\Location;
use Illuminate\Http\Request;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class CustomerIntroduceController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }	
            }
			
            view()->share('menuTop', 'cskh');

            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
	
       try {
            $customerIntroducts = CustomerIntroduct::leftJoin('orders','orders.order_id','customer_introduct.order_id')
            ->leftJoin('users','users.id','customer_introduct.user_id')
            ->select([
                'customer_introduct.id',
                'users.name as user_name',
                'users.phone  as user_phone',
                'users.email  as user_email',
                'orders.shipping_name',
                'orders.total_price',
                'orders.shipping_address'
            ])
            ->distinct('customer_introduct.user_id')
            ->get();

            return view('admin.list_customer_introduce.index' ,compact('customerIntroducts'));

        } catch (\Exception $e) {
            Error::setErrorMessage('Lỗi xảy ra khi hiển thị thành viên: dữ liệu không hợp lệ.');
            Log::error('http->admin->CustomerIntroduct->index: Lỗi xảy ra trong quá trình hiển thị thành viên');

            return redirect('admin/home');
        }
    }

     public function show($id){
        try {
        $userId = Auth::user()->id;
        $user = User::where('id', $userId)->first();

           $customerIntroducts = CustomerIntroduct::leftJoin('orders','orders.order_id','customer_introduct.order_id')
            ->leftJoin('users','users.id','customer_introduct.user_id')        
            ->select([
                'customer_introduct.user_id',
                'users.name as user_name',
                'users.id',
                'users.phone  as user_phone',
                'users.email  as user_email',
                'users.image  as user_image',
                'orders.shipping_name',
                'orders.total_price',
                'orders.shipping_address'
            ])
            ->where('customer_introduct.id', $id )
            ->first();

            return view('admin.list_customer_introduce.detail',compact('customerIntroducts','user'));

         } catch (\Exception $e) {
        Error::setErrorMessage('Lỗi xảy ra khi hiển thị thành viên: dữ liệu không hợp lệ.');
        Log::error('http->admin->CustomerIntroduct->show: Lỗi xảy ra trong quá trình hiển thị thành viên');

        return redirect('admin/home');
        }
    }

     public function showWithUser(){        
        try {
        $userId = Auth::user()->id;
        $user = User::where('id', $userId)->first();
        $customerIntroducts = CustomerIntroduct::leftJoin('orders','orders.order_id','customer_introduct.order_id')
        ->leftJoin('users','users.id','customer_introduct.user_id')        
        ->select([
            'customer_introduct.user_id',
            'users.name as user_name',
            'users.id',
            'users.phone  as user_phone',
            'users.email  as user_email',
            'users.image as user_image',
            'orders.shipping_name',
            'orders.total_price',
            'orders.shipping_address'
        ])
        ->where('customer_introduct.user_id', $userId )
        ->first();

       
       return view('admin.list_customer_introduce.detail',compact('customerIntroducts','user'));
         } catch (\Exception $e) {
        Error::setErrorMessage('Lỗi xảy ra khi hiển thị thành viên: dữ liệu không hợp lệ.');
        Log::error('http->admin->CustomerIntroduct->showWithUser: Lỗi xảy ra trong quá trình hiển thị thành viên');

        return redirect('admin/home');
        }
    }


    public function changeIntroduce(Request $request){
        try {
            DB::beginTransaction();
                $paymentInformation = PaymentInfomation::where('user_id',$request->input('user_id'))
                ->first();

                $total_coin = $paymentInformation->coin_total;

                $updatePayment = PaymentInfomation::where('user_id',$request->input('user_id'))
                ->update([
                    'coin_total' => $total_coin + $request->input('coin')
                ]);

                $user = User::where('id', $request->input('user_id'))
                ->update([
                    'coin' => 0 ,
                ]);


            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            Error::setErrorMessage('Lỗi xảy ra : dữ liệu không hợp lệ.');
            Log::error('http->admin->CustomerIntroduct->changeIntroduce: Lỗi xảy ra trong quá trình hiển thị thành viên');
        }

    }

    //Thêm mới thông tin ví moma
    public function addPayment(Request $request){

        $payment = PaymentInfomation::insertGetId([
            'user_id' => $request->input('user_id'),
            'coin_total' => 0,
            'payment_name' => $request->input('payment_name'),
            'payment_bank' => $request->input('payment_bank'),
            'bank_branch' => $request->input('bank_branch'),
            'bank_number' => $request->input('bank_number'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return redirect()->back();
    }

    //CHỉnh sửa thông tin ví moma
     public function editPayment(Request $request){
        $id = $request->input('id');
        $payment = PaymentInfomation::where('payment_id',$id)
        ->update([
            'coin_total' => 0,
            'payment_name' => $request->input('payment_name'),
            'payment_bank' => $request->input('payment_bank'),
            'bank_branch' => $request->input('bank_branch'),
            'bank_number' => $request->input('bank_number'),
        ]);

        return redirect()->back();
    }

    //Gửi yêu cầu rút tiền 
    public function submitMoney(Request $request){

        $userId = $request->input('user_id');
        $userName = $request->input('user_name');
        $userEmail = $request->input('user_email');
        $moneyNumber = $request->input('money_number');

        //Add yêu cầu giao dịch 
        $requestMoney = RequestMoney::insertGetId([
            'user_id' => $userId,
            'status' => 0,
            'money_request' => $moneyNumber,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        $content = "Thành viên ".$userName."</br> Có id là : ".$userId ."</br> Email là : ".$userEmail."</br> Yêu cầu rút ".$moneyNumber." Từ ví MOMA" ;
        MailConfig::sendMailDefault('vn3ctran@gmail.com', 'Yêu cầu rút tiền từ tài khoản của '.$userName, $content);

    }

    public function anyDatatables(Request $request) {
		if ($request->has('user_id')) {
			$userId = $request->input('user_id');
		} else {
			$userId = Auth::user()->id;
		}
		
        $domains = Domain::join('themes', 'themes.theme_id', '=', 'domains.theme_id')
            ->join('users', 'users.id', '=', 'domains.user_id')
            ->join('user_customer_introduct', 'user_customer_introduct.customer_id', 'domains.user_id')
            ->select(
                'domains.created_at',
                'domains.url',
                'domains.domain_id',
                'domains.name',
                'domains.action',
                'domains.change_url',
                'domains.time_on',
                'themes.name as theme_name',
                'users.name as user_name',
                'users.email',
                'users.phone',
                'users.vip'
            )->where('user_customer_introduct.user_id', $userId);

        return Datatables::of($domains)
            ->orderColumn('domain_id', 'domain_id desc')
            ->make(true);
    }

}
