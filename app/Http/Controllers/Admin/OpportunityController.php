<?php

namespace App\Http\Controllers\Admin;

use App\Entity\NoteContact;
use App\Ultility\CheckPermission;
use App\Ultility\Error;
use Illuminate\Http\Request;
use App\Entity\User;
use Carbon\Carbon;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Psy\Util\Str;
use Validator;

class OpportunityController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                
				return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }

    public function index(){
        return view('admin.opportunity.list');
    }


    public function create(Request $request){

        $managers = User::where('theme_code', $this->themeCode)
        ->where('user_email',$this->emailUser)
        ->orWhere('email', $this->emailUser )
        ->get();
        return view('admin.opportunity.add',compact('managers'));
    }

    public function store(){

    }

    public function edit(){

    
    }

    public function update(){

    
    }

    public function show(){

    
    }

    public function destroy(){

    
    }
  
}
