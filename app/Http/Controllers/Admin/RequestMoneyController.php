<?php

namespace App\Http\Controllers\Admin;

use App\Entity\RequestMoney;
use App\Entity\PaymentInfomation;
use App\Ultility\CheckPermission;
use App\Ultility\Error;
use Illuminate\Http\Request;
use App\Entity\User;
use Carbon\Carbon;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Psy\Util\Str;
use Validator;

class RequestMoneyController extends AdminController
{

    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }

            view()->share('menuTop', 'customers');

            return $next($request);
        });

    }

    public function index(){
        try {
            $requestContacts = RequestMoney::join('payment_information','payment_information.user_id','request_money.user_id')
            ->where('request_money.status', 0)
            ->get();

            return view('admin.request_money.index', compact('requestContacts'));
        } catch(\Exception $e) {
            Error::setErrorMessage('Lỗi');
        }
    }

    public function confirm($id , $moneyMinius){
        try {
            //Cập nhật trạng thái Yêu cầu 
            $requestContacts = RequestMoney::where('request_id', $id)->update([
                'status' => 1
            ]);

            $money = $this->GetThisCoin($id);

            $thisMoney = $money->coin_total;
            $paymentId = $money->payment_id;

            //số tiền còn lại
            $restMoney = $thisMoney - $moneyMinius;
            
            //cập nhật Trừ tiền

            $coinUpdate = PaymentInfomation::where('payment_id', $paymentId)
            ->update([
                'coin_total' => $restMoney
            ]);

            return redirect()->back();

        } catch(\Exception $e) {
            Error::setErrorMessage('Lỗi');

        } finally {
            return redirect(route('request_money'));
        }

    }

    private function GetThisCoin($id){

        $coinTotal = RequestMoney::join('payment_information','payment_information.user_id','request_money.user_id')
        ->where('request_money.request_id', $id)
        ->first();

        return $coinTotal;
    }


    public function destroy($id){

        $requestContact = RequestMoney::where('request_id', $id)->delete();
        return redirect()->back();
    }
}
