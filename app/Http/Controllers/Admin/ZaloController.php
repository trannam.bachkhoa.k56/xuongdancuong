<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Entity\Input;
use App\Entity\ZaloManager;
use App\Entity\Template;
use App\Entity\TypeInput;
use App\Entity\User;
use App\Ultility\Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;
use App\Ultility\Ultility;

class ZaloController extends AdminController
{
    protected $role;
	protected $accessToken = '';

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (User::isMember($this->role)) {
                return redirect('admin/home');
            }

            if ( $this->themeCode == 'vn3c' && $this->emailUser == 'vn3ctran@gmail.com' && !User::isCreater($this->role)) {
                return redirect(route('admin_dateline'));
            }

            if (!empty($this->domainUser) && $this->emailUser != 'vn3ctran@gmail.com') {
                if ( strtotime($this->domainUser->end_at) < time() &&  Auth::user()->vip > 0) {
                    return redirect(route('admin_dateline'));
                }
            }
			
			$zaloManagers = ZaloManager::where('theme_code', $this->themeCode)
			->where('user_email', $this->emailUser)
			->get();
			
            view()->share([
				'menuTop' => 'zalocskh',
				'zaloManagers' => $zaloManagers
			]);
			
			
		
            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        
        return View('admin.zalo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.zalo.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // if slug null slug create as name zalo
			$slug = Ultility::createSlug($request->input('name_zalo'));
			
            // insert to database
            $zaloManager = new ZaloManager();
            $zaloManager->insert([
                'name_zalo' => $request->input('name_zalo'),
                'slug' => $slug,
                'phone_zalo' => $request->input('phone_zalo'),
                'link_token' => $request->input('link_token'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
            ]);
			
        } catch (\Exception $e) {
            Log::error('http->admin->ZaloController->store: Lỗi thêm mới zalo');
        } finally {
            return redirect('admin/cskhzalo');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$zaloManager = ZaloManager::where('zalo_manager_id', $id)
			->where('theme_code', $this->themeCode)
			->where('user_email', $this->emailUser)
			->first();
			
		$tokenLink = $zaloManager->link_token;
		$nameZalo = $zaloManager->name_zalo;

         return View('admin.zalo.show', compact('tokenLink', 'nameZalo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template)
    {

        return View('admin.zalo.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ZaloManager $zaloManager)
    {
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(ZaloManager $zaloManager)
    {
        // try {

            ZaloManager::where('zalo_manager_id', $zaloManager->zalo_manager_id)->delete();
        // } catch (\Exception $e) {
            // Log::error('http->admin->TemplateController->destroy: Lỗi xóa template');
        // } finally {
            return redirect('admin/cham-soc-zalo');
        // }
    }
	
	public function scanQrCode (Request $request) {
		$service_url = 'https://id.zalo.me/account/authen?a=qr&t=2&code='.$request->input('code').'&continue=https%3A%2F%2Fchat%2Ezalo%2Eme';
		
		$this->callApi($service_url);
		
		return $this->loginQrCode($request);
	}
	
	public function loginQrCode ($request) {
		$service_url = 'https://id.zalo.me/account/authen?a=qr&t=3&code='.$request->input('code').'&continue=https%3A%2F%2Fchat%2Ezalo%2Eme';
		
		$this->callApi($service_url);
		
		return $this->saveToken($request);
	}
	
	public function saveToken ($request) {
		$service_url = 'https://id.zalo.me/account/checksession?type=ajax&login_type=qr&continue=https%3A%2F%2Fchat.zalo.me';
		
		return $this->callApi($service_url);
	}
	
	private function callApi ($service_url) {
		// truyền dữ liệu lên
        $curl = curl_init($service_url);
		
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_COOKIE , true);
		curl_setopt($curl, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
		curl_setopt($curl, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        // kết quả response trả về
        $curl_response = curl_exec($curl);
        if(curl_errno($curl)){      
            echo 'Request Error:' . curl_error($curl);
        }
		
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		
        $decoded = $curl_response;
        curl_close($curl);
		
        return [
            'httpCode' => $httpCode,
            'decode' => $decoded
        ];
		
	}
}
