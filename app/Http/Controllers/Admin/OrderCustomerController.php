<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Contact;
use App\Entity\Domain;
use App\Entity\DomainUser;
use App\Entity\OrderCustomer;
use App\Entity\OrderCustomerItem;
use App\Entity\Product;
use App\Entity\Template;
use App\Entity\Theme;
use App\Entity\Goal;
use App\Entity\Commission;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Validator;

class OrderCustomerController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;


            view()->share('menuTop', 'orders');
            return $next($request);
        });

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderCustomer = new OrderCustomer();

        $orderCustomers = $orderCustomer->leftJoin('contact', 'contact.contact_id', 'order_customer.contact_id')
            ->leftJoin('users', 'users.id', 'order_customer.user_id')
            ->select(
                'users.name as user_name',
                'contact.name as contact_name',
                'order_customer.*'
            )
            ->where('order_customer.theme_code', $this->themeCode)
            ->where('order_customer.user_email', $this->emailUser)
			->orderBy('order_customer.order_customer_id', 'desc');

        // là cộng sự đăng nhập lấy ra tất cả chiến dịch liên quan đến cộng sự đó.
        if (!empty($this->userIdPartner)) {
            $orderCustomers = $orderCustomers->where('order_customer.user_id', $this->userIdPartner);
        }
         $orderCustomers = $orderCustomers->paginate(20);

        return View('admin.order_customer.list', compact('orderCustomers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $products = Product::join('posts', 'posts.post_id', 'products.post_id')
            ->select('posts.title', 'products.product_id')
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->get();

        $employees = DomainUser::join('users', 'users.id', 'domain_user.user_id')
            ->where('domain_id', $this->domainUser->domain_id)->get();

        $contact = Contact::where('contact_id', $request->input('contact_id'))->first();

        return View('admin.order_customer.add', compact('employees', 'products', 'contact'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $affiliateMoney = 0;
        // tính affiliate
        if (!empty($request->input('contact_affiliate'))) {
            $affiliateContact = $request->input('contact_affiliate');

            $contactAffiliate = Contact::where('phone', $affiliateContact)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->first();

            if (!empty( $contactAffiliate)) {
				
                Contact::where('contact_id', $request->input('contact_id') )
                    ->update([
                        'affiliate_person' => $contactAffiliate->contact_id,
                    ]);

				Contact::where('contact_id', $contactAffiliate->contact_id)
				->update([
					'money' => ($contactAffiliate->money + ($request->input('total_price')*$contactAffiliate->affiliate_money /100) )
				]);

                $affiliateMoney = $contactAffiliate->affiliate_money;
            }
        }
        $orderCustomer = new OrderCustomer();

        $orderCustomerId = $orderCustomer->insertGetId([
            'user_id' => $request->input('user_id'),
            'contact_id' => $request->input('contact_id'),
            'payment' => !empty($request->input('payment')) ? str_replace('.', '', $request->input('payment')) : 0,
            'surcharge' => !empty($request->input('surcharge')) ? str_replace('.', '', $request->input('surcharge')) : 0,
            'total_price' => $request->input('total_price'),
            'affiliate_money' => $affiliateMoney,
            'cost' => $request->input('cost_total'),
            'notes' => $request->input('notes'),
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
            'created_at' => new \DateTime()
        ]);

        foreach ($request->product_id as $id => $productId) {
            $orderCustomerItem = new OrderCustomerItem();
            $orderCustomerItem->insert([
                'order_customer_id' => $orderCustomerId,
                'product_id' => $productId,
                'price' => str_replace('.', '', $request->price[$id]),
               // 'cost' => str_replace('.', '', $request->cost[$id]),
                'qty' => str_replace('.', '', $request->qty[$id]),
                'tax' => $request->tax[$id],
                'vat' =>  $request->vat[$id],
                'created_at' => new \DateTime()
            ]);
        }


//        } catch (\Exception $e) {
//            Log::error('http->admin->commissionController->store: Lỗi thêm mới cơ chế hoa hồng');
//        } finally {
            return redirect('/admin/contact/'.$request->input('contact_id'));
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(OrderCustomer $orderCustomer)
    {
        view()->share('menuTop', 'detail_customer');
        $contact = Contact::where('contact_id', $orderCustomer->contact_id)->first();

        $orderCustomerItems = OrderCustomerItem::leftJoin('products', 'products.product_id', 'order_customer_item.product_id')
            ->leftJoin('posts', 'posts.post_id', 'products.post_id')
            ->select(
                'posts.title',
                'order_customer_item.*'
            )
            ->where('order_customer_id', $orderCustomer->order_customer_id)->get();

        return view('admin.order_customer.show', compact('orderCustomer', 'contact', 'orderCustomerItems'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderCustomer $orderCustomer)
    {
        if ($orderCustomer->theme_code != $this->themeCode || $orderCustomer->user_email != $this->emailUser) {
            return redirect('admin/orderCustomer');
        }

        $employees = DomainUser::join('users', 'users.id', 'domain_user.user_id')
            ->where('domain_id', $this->domainUser->domain_id)->get();

        return View('admin.order_customer.edit', compact('goal', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Goal $goal)
    {
        try {
            if ($goal->theme_code != $this->themeCode || $goal->user_email != $this->emailUser) {
                return redirect('admin/order-customer');
            }

            $goal->update([
                'title' => $request->input('title'),
                'user_id' => $request->input('user_id'),
                'revenue' => $request->input('revenue'),
                'customers' => $request->input('customers'),
                'orders' => $request->input('orders'),
                'notes' => $request->input('notes'),
                'purchases' => $request->input('purchases'),
                'reminders' => $request->input('reminders'),
                'calls' => $request->input('calls'),
                'month' => $request->input('month'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);

        } catch (\Exception $e) {
            Log::error('http->admin->CommissionController->update: Lỗi chỉnh sửa cơ chế hoa hồng');
        } finally {
            return redirect('admin/order-customer');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderCustomer $orderCustomer)
    {
        try {
            if ($orderCustomer->theme_code != $this->themeCode || $orderCustomer->user_email != $this->emailUser) {
                return redirect('admin/order-customer');
            }
			
			$contact = Contact::where('contact_id', $orderCustomer->contact_id)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->first();
				
			if (!empty($contact)) {
				$contactAffiliate = Contact::where('contact_id', $contact->affiliate_person)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->first();
			}

			if (!empty($contactAffiliate)) {
				Contact::where('contact_id', $contactAffiliate->contact_id)
				->update([
					'money' => ($contactAffiliate->money - ($orderCustomer->total_price*$contactAffiliate->affiliate_money /100) )
				]);
			}

            OrderCustomerItem::where('order_customer_id', $orderCustomer->order_customer_id)->delete();
            $orderCustomer->delete();
        } catch (\Exception $e) {
            Log::error('http->admin->OrderCustomerController->destroy: Lỗi xóa cơ chế hoa hồng');
        } finally {
            return redirect('admin/order-customer');
        }
    }

    public function addProductOrder (Request $request) {
//        try {
            $product = Product::join('posts', 'posts.post_id', 'products.post_id')
                ->select('posts.title', 'products.*')
                ->where('products.product_id', $request->input('product_id'))
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->first();

            if (empty($product)) {
                return response([
                    'status' => 500,
                    'message' => "không tồn tại sản phẩm trong hệ thống.",
                ])->header('Content-Type', 'text/plain');
            }

            return response([
                'status' => 200,
                'product' => $product,
            ])->header('Content-Type', 'text/plain');

//        } catch (\Exception $e) {
//            Log::error('http->admin->OrderCustomerController->getProductDetail: Lỗi lấy ra chi tiết 1 sản phẩm.');
//        } finally {
//            return redirect('admin/order-customer');
//        }
    }

    public function payment(Request $request) {
        $payment = $request->input('payment');
        $orderCustomerId = $request->input('order_customer_id');

        $orderCustomerModel = new OrderCustomer();

        $orderCustomer = $orderCustomerModel
            ->where('order_customer_id', $orderCustomerId)
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->first();

        // cập nhật payment
        $orderCustomerModel->where('order_customer_id', $orderCustomerId)
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->update([
                'payment' => $orderCustomer->payment + str_replace('.', '', $payment)
            ]);

        return redirect()->back();
    }
}
