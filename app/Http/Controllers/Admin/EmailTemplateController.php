<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 1/3/2019
 * Time: 9:06 AM
 */

namespace App\Http\Controllers\Admin;


use App\Entity\EmailTemplate;
use App\Entity\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmailTemplateController extends AdminController
{
    protected $role;

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $this->role =  Auth::user()->role;

            if (!User::isCreater($this->role)) {
                return redirect('admin/home');
            }

            view()->share('menuTop', 'cskh');

            return $next($request);
        });
    }

    public function index()
    {
        $emails = EmailTemplate::get();
        return  view('admin.email_template.list',compact('emails'));
    }

    public function create()
    {
        $emails = EmailTemplate::get();
        return  view('admin.email_template.add',compact($emails));
    }


    public function store(Request $request)
    {   
       
        $email = EmailTemplate::insertGetId([
            'template_title' => $request->input('template_title'),
            'content' => $request->input('content'),
            'group_customer' => $request->input('group_customer')
        ]);
        return redirect(route('group-template.index'));
    }


     public function show($id)
    {  
        return redirect(route('group-template.index'));
    } 


   
    public function edit($id)
    {
       $email = EmailTemplate::where('template_id', $id)->first();

       return  view('admin.email_template.edit',compact('email'));
    }

   
    public function update(Request $request, $id)
    {
     
        $email = EmailTemplate::where('template_id',$id)
        ->update([
            'template_title' => $request->input('template_title'),
            'content' => $request->input('content'),
            'group_customer' => $request->input('group_customer')
        ]);

        return redirect(route('group-template.index'));
     
    }

    public function destroy($id)
    {

       try {
           DB::beginTransaction();

            $post = EmailTemplate::where('template_id',$id)
            ->delete();
           DB::commit();

         } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi xóa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->EmailTemplateController->destroy: Lỗi xảy ra trong quá trình xóa bài viết');
        } finally {
            return redirect('admin/group-tmplate');
        }
    }

    public function delete($id)
    {
       try {
           DB::beginTransaction();
            $template = EmailTemplate::where('template_id',$id)
            ->delete();
           DB::commit();

         } catch (\Exception $e) {
            DB::rollback();
            Error::setErrorMessage('Lỗi xảy ra khi xóa bài viết: dữ liệu không hợp lệ.');
            Log::error('http->admin->EmailTemplateController->destroy: Lỗi xảy ra trong quá trình xóa bài viết');
        } finally {
            return redirect()->back();
        }
    }


    public function anyDatatables(Request $request) {

        $emails = EmailTemplate::get();

        return Datatables::of($emails)
            ->addColumn('action', function($email) {
                $string =  '<a href="'.route('group-template.edit', ['template_id' => $email->template_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('remove_template', ['template_id' => $email->template_id]).'" class="btn btn-danger btnDelete" >
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })

            ->orderColumn('template_id', 'template_id desc')
            ->make(true);
    }

    public function getTemplate(Request $request){
        
        $id = $request->input('id');
        $email = EmailTemplate::where('template_id', $id)->first();
        $content = $email->content; 

        return $content;
    }

}