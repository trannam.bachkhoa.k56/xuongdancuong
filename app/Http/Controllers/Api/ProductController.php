<?php

namespace App\Http\Controllers\Api;

use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Comment;
use App\Entity\Domain;
use App\Entity\Information;
use App\Entity\Input;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Template;
use App\Entity\TypeInformation;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

use Validator;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
                ->where('domains.url', $request->domain)
				->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();


            $products = Post::join('products', 'products.post_id', '=', 'posts.post_id')
                ->select(
                    'products.product_id',
                    'posts.*',
                    'products.price',
                    'products.code',
                    'products.discount'
                )
                ->where('posts.theme_code', $theme->code)
                ->where('posts.user_email', $user->email)
                ->where('post_type', 'product');
            
            if ($request->has('category_id')) {
                $products->leftJoin ('category_post', 'category_post.post_id', 'posts.post_id')
                ->where('category_post.category_id', $request->input('category_id'));
            }
            
            $products =  $products->paginate(10);
            
            return response([
                'status' => 200,
                'content' => $products
            ])->header('Content-Type', 'text/plain');
        } catch (\Exception $e) {
            return response('Lỗi đường truyền', 500)
                ->header('Content-Type', 'text/plain');
        }
        // lấy hết thông tin của sản phẩm

    }

    /**
     * Show the form for creating a new res    */
    public function create(Request $request)
    {
        try{
            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                ->where('domains.user_id',$user->id)
                ->where('domains.url', $request->domain)
				->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
            if(empty($theme)){
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên domain'
                ],404);
            }
            $category = new Category();
            $categories = $category->getCategory($theme->code, $user->email,'product');
            $templates = Template::orderBy('template_id')
                ->where('theme_code', $theme->code)->get();
            return response([
                'status' => 200,
                'categories' => $categories,
                'templates' => $templates
            ])->header('Content-Type', 'text/plain');
        }catch (\Exception $e){
            return response()->json([
                "message"=>"Có lỗi xảy ra: ".$e->getMessage(),
            ],404);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                    ->where('domains.user_id',$user->id)
                    ->where('domains.url', $request->domain)
					->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
            if(empty($theme)){
                 return response()->json([
                'message' => 'Thử kiểm tra lại tên domain'
                    ],404);
              }
            if(!$request->has('title')){
                return response()->json([
                    'message' => 'Vui lòng nhập tên sản phẩm'
                ],404);
            }
            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }

            if (!empty($request->input('parents'))) {
            // không có danh mục thì báo lỗi
            foreach ($request->input('parents') as $parent) {
                if(!Category::where('category_id', $parent)
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)->exists()) {
                    return response()->json([
                        "message"=>"Bạn chưa có danh mục này. Vui lòng tạo danh mục"
                    ],401);
                }
            }
                $categoryParents = Category::whereIn('category_id', $request->input('parents'))
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)->get();
                $categories = array();
                foreach ($categoryParents as $cate) {
                    $categories[] =  $cate->title;
                }
            }

            $post = new Post();
            $postId = $post->insertGetId([
                'title' => $request->input('title'),
                'post_type' => 'product',
                'template' =>  $request->input('template'),
                'description' => $request->input('description'),
                'image' =>  $request->input('image'),
                'user_email' => $user->email,
                'theme_code' => $theme->code,
                'visiable' => 0,
                'category_string' => !empty($categories) ? implode(',', $categories) : '',
            ]);

            // insert slug
            $postWithSlug = $post
                ->where('user_email', $user->email)
                ->where('theme_code', $theme->code)
				->where('slug', $slug)->first();
            if (empty($postWithSlug)) {
                $post->where('post_id', '=', $postId)
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)
                    ->update([
                        'slug' => $slug
                    ]);
            } else {
                $post->where('post_id', '=', $postId)
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)
                    ->update([
                        'slug' => $slug.'-'.$postId
                    ]);
            }

            // insert danh mục cha
            $categoryPost = new CategoryPost();
            if (!empty($request->input('parents'))) {
                foreach ($request->input('parents') as $parent) {
                    $categoryPost->insert([
                        'category_id' => $parent,
                        'post_id' => $postId,
                        'theme_code' => $theme->code,
                        'user_email' => $user->email
                    ]);
                }
            }

            $product = new Product();
            $price = !empty($request->input('price')) ? str_replace(".", "", $request->input('price')) : 0;
            $discount = !empty($request->input('discount')) ? str_replace(".", "", $request->input('discount')) : 0;
            $productId = $product->insertGetId([
                'post_id' => $postId,
                'code' =>  $request->input('code'),
                'price' => $price ,
                'discount' => $discount ,
            ]);

            return response([
                'status' => 200,
                'content' => 'Thêm mới thành công',
                'product_id' => $productId,
                'code' => $request->input('code'),
                'post_id' => $postId,
                'price' => $price,
                'discount' => $discount
            ])->header('Content-Type', 'text/plain');

       } catch (\Exception $e) {
            return response()->json([
                "message"=>"Có lỗi xảy ra: ".$e->getMessage(),
            ],404);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $product_id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $product_id)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                ->where('domains.user_id',$user->id)
                ->where('domains.url', $request->domain)
				->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
            if(empty($theme)) {
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên domain'
                ],404);
            }
            $category = new Category();
            $categories = $category->getCategory($theme->code, $user->email,'product');
            $templates = Template::orderBy('template_id')
                ->where('theme_code', $theme->code)->get();

            $productModel = new Product();
            $product = $productModel
                ->join('posts', 'posts.post_id', 'products.post_id')
                ->where('product_id', $product_id)
                ->where('user_email', $user->email)
                ->where('theme_code', $theme->code)
                ->select(
                    'products.product_id',
                    'posts.*',
                    'products.price',
                    'products.code',
                    'products.discount'
                )->first();

            return response([
                'status' => 200,
                'content' => $product,
                'categories' => $categories,
                'templates' => $templates
            ])->header('Content-Type', 'text/plain');
        } catch (\Exception $e) {
            return response()->json([
                "message"=>"Có lỗi xảy ra: ".$e->getMessage(),
            ],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $product_id
     * @param  int  $post_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                ->where('domains.user_id',$user->id)
                ->where('domains.url', $request->domain)
				->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
            if(empty($theme)){
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên domain'
                ],404);
            }
            $product = Product::where('product_id', $product_id)->first();
            $post = Post::where('post_id', $product->post_id)->first();

            // if slug null slug create as title
            $slug = $request->input('slug');
            if (empty($slug)) {
                $slug = Ultility::createSlug($request->input('title'));
            }
            if (!empty($request->input('parents'))) {
                $categoriParents = Category::whereIn('category_id', $request->input('parents'))
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)->get();
                $categories = array();
                foreach ($categoriParents as $cate) {
                    $categories[] = $cate->title;
                }
            }

            // update to database
            if(($request->input('image')) != ""){
                $image = $request-> input("image");
            }else{
                $image = $post->image;
            }
            $post->update([
                'title' => $request->has('title') ? $request->input('title') : $post->title,
                'post_type' => 'product',
                'template' =>  $request->has('template') ? $request->input('template') : $post->template,
                'description' =>  $request->has('description') ? $request->input('description') : $post->description,
                'image' =>  $image,
                'user_email' => $user->email,
                'theme_code' => $theme->code,
                'visiable' => 0,
                'category_string' => !empty($categories) ? implode(',', $categories) : $post->category_string,
            ]);

            // insert slug
            $postWithSlug = $post->where('user_email', $user->email)
                ->where('theme_code', $theme->code)
				->where('slug', $slug)->first();
				
            if (empty($postWithSlug)) {
                $post->where('post_id', '=', $post->post_id)
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)
                    ->update([
                        'slug' => $slug
                    ]);
            } else {
                $post->where('post_id', '=', $post->post_id)
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)
                    ->update([
                        'slug' => $slug.'-'.$post->post_id
                    ]);
            }

            // insert danh mục cha
            $categoryPost = new CategoryPost();
            $categoryPost->where('post_id', $post->post_id)->delete();
            if (!empty($request->input('parents'))) {
                foreach ($request->input('parents') as $parent) {
                    $categoryPost->insert([
                        'category_id' => $parent,
                        'post_id' => $post->post_id,
                        'theme_code' => $theme->code,
                        'user_email' => $user->email
                    ]);
                }
            }
            $price = !empty($request->input('price')) ? str_replace(".", "", $request->input('price')) : 0;
            $discount = !empty($request->input('discount')) ? str_replace(".", "", $request->input('discount')) : 0;
            $product->update([
                'code' =>  $request->input('code'),
                'price' => $request->has('price') ? $price : $product->price,
                'discount' => $request->has('discount') ? $discount : $product->discount ,
            ]);

            return response([
                'status' => 200,
                'message' => 'Cập nhật sản phẩm thành công!',
                'product_id' => $product->product_id,
                'code' => $request->input('code'),
                'price' => $price,
                'discount' => $discount
            ])->header('Content-Type', 'text/plain');

        } catch (\Exception $e) {
            return response()->json([
                "message"=>"Có lỗi xảy ra: ".$e->getMessage(),
            ],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $product_id)
    {
        try {
            DB::beginTransaction();

            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
                ->where('domains.url', $request->domain)
				->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();

            $product = Product::where('product_id', $product_id)->first();
            $post = Post::where('post_id', $product->post_id)->first();

            $productModel = new Product();
            $postModel = new Post();

            $productModel->where('product_id', $product_id)->delete();

            CategoryPost::where('post_id', $post->post_id)
                ->where('user_email', $user->email)
                ->where('theme_code', $theme->code)
                ->delete();

            Input::where('post_id', $post->post_id)
                ->where('user_email', $user->email)
                ->where('theme_code', $theme->code)
                ->delete();

            $postModel->where('post_id', $post->post_id)
                ->where('user_email', $user->email)
                ->where('theme_code', $theme->code)->delete();

            Comment::where('post_id', $post->post_id)
                ->where('user_email', $user->email)
                ->where('theme_code', $theme->code)->delete();

            return response([
                'status' => 200,
                'message' => 'xóa thành công'
            ])->header('Content-Type', 'text/plain');
            DB::commit();

        } catch (\Exception $e) {

            DB::rollback();
            return response()->json([
                'message' => 'Có lỗi xảy ra: '.$e->getMessage(),
            ], 404);
        }
    }
    public function getProductsFromDomain(Request $request){
        try {
//            $user = JWTAuth::toUser($request->input('token'));
            if(!$request->has('domain')){
                return response()->json([
                    'message' => 'Chưa nhập tên miền website'
                ],404);
            }
            
            if(!Domain::where('domains.url','like','%' . $request->input('domain') . '%')
            ->orWhere ('domain','like','%' . $request->input('domain') . '%')
            ->exists()){
                return response()->json([
                    'message' => 'Tên domain không tồn tại'
                ],404);
            }
            
            $domain = Domain::where('domains.url','like','%' . $request->input('domain') . '%')
            ->orWhere ('domain','like','%' . $request->input('domain') . '%')
                            ->select('user_id')->first();
            $emailUser = User::where('id',$domain->user_id)->select('email')->first();

            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                    ->where('domains.user_id',$domain->user_id)
                    ->where('domains.url','like','%' . $request->input('domain') . '%')
					->orWhere ('domain','like','%' . $request->input('domain') . '%')
                    ->select('themes.code')->first();
                    
            $posts = Post::leftJoin('products','products.post_id','posts.post_id')
                    ->where('posts.user_email',$emailUser->email)
                    ->where('posts.theme_code',$theme->code)
                    ->where('post_type','product')
                    ->select(
                        'posts.post_id',
                        'posts.title',
                        'posts.description',
                        'posts.image',
                        'posts.category_string as category',
                        'posts.created_at',
                        'posts.updated_at',
                        'products.*'
                    );
                    
            if($posts->get()->count() == 0){
                return response()->json([
                    'message' => 'Website này chưa có sản phẩm nào'
                ],200);
            }

            $posts = $posts->paginate(10);

            foreach ($posts as $id => $post) { 
                $posts[$id]->category_ids = CategoryPost::where('post_id', $post->post_id)->select('category_id')->get();
            }

            return response()->json([
                'message' => 'Lấy sản phẩm thành công',
                'data' => $posts
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Có gì đó xảy ra: '.$exception->getMessage(),
            ], 404);
        }
    }
    public function getPostsFromDomain(Request $request){
        try{
            if(!$request->has('domain')){
                return response()->json([
                    'message' => 'Chưa nhập tên miền website'
                ],404);
            }
            if(!Domain::where('domains.url','like','%' . $request->input('domain') . '%')
				->orWhere('domains.domain','like','%' . $request->input('domain') . '%')->exists()){
                return response()->json([
                    'message' => 'Tên domain không tồn tại'
                ],404);
            }
            $domain = Domain::where('domains.url','like','%' . $request->input('domain') . '%')
                ->select('user_id')->first();
            $emailUser = User::where('id',$domain->user_id)->select('email')->first();

            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                ->where('domains.user_id',$domain->user_id)
                ->where('domains.url','like','%' . $request->input('domain') . '%')
                ->select('themes.code')->first();
            $posts = Post::where('posts.user_email',$emailUser->email)
                ->where('posts.theme_code',$theme->code)
                ->where('post_type','post')
                ->select(
                    'posts.title',
                    'posts.content',
                    'posts.description',
                    'posts.category_string as category',
                    'posts.image',
                    'posts.tags',
                    'posts.meta_title',
                    'posts.meta_description',
                    'posts.meta_keyword',
                    'posts.product_list',
                    'posts.created_at',
                    'posts.updated_at'
                );
            if($posts->get()->count() == 0){
                return response()->json([
                    'message' => 'Website này chưa có bài viết nào'
                ],200);
            }
            return response()->json([
                'message' => 'Lấy bài viết thành công',
                'data' => $posts->paginate(10)
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Có gì đó xảy ra: '.$exception->getMessage(),
            ], 404);
        }
    }
}

