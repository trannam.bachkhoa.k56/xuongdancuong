<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 5/7/2018
 * Time: 9:58 AM
 */

namespace App\Http\Controllers\Api;

use App\Entity\Domain;
use App\Entity\Post;
use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTFactory;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    public function index (Request $request) {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // kiểm tra xem có tồn tại user không
            $user = User::where('email', $request->input('email'))->first();
            if (empty($user)) {
                return response()->json(['error' => 'không tồn tại user đăng nhập này.'], 401);
            }

            // nếu user mà có quyền cao nhất

            if ( ($user->role == 4) && $token = JWTAuth::attempt($credentials, ['domain' => $request->input('domain')])  ) {


                return response()->json(compact('token'));
            }

            // kiểm tra domain va user có khớp không
            $domain = Domain::join('users', 'users.id', 'domains.user_id')
                ->where('users.email', $request->input('email'))
                ->where('domains.url', $request->input('domain'))
				->orWhere('domains.domain', $request->input('domain'))
                ->first();

            // nếu không tồn tại domain và email thì trả về dăng nhập lỗi
            if (empty($domain)) {
                return response()->json(['error' => 'Đường dẫn website không tồn tại'], 401);
            }

            // nếu đúng check đăng nhập domain và password thì đăng nhập thành công
            if (! $token = JWTAuth::attempt($credentials, ['domain' => $request->input('domain')])) {
                return response()->json(['error' => 'Thông tin tài khoản hoặc mật khẩu không chính xác.'], 401);
            }

        } catch (JWTException $e) {
            // Bất kì có lỗi gì xảy ra thì báo không thành công
            return response()->json(['error' => 'Lỗi xảy ra trong quá trình đăng nhập, vui lòng đăng nhập lại.'], 500);
        }

        // all good so return the token
        $encrypt = Crypt::encrypt([
            'random' => rand(1000000, 10000000),
            'domainId' => $domain->domain_id,
            'userId' =>  $user->id,
        ]);


        return response()->json([[
            'token' => $token,
            'urlLogin' => $domain->url.'/check-user-redirect/'.$encrypt,
            'url' => $domain->url,
            'user_id' => $user->id
        ]]);
    }

    public function getAuthUser(Request $request){
        try{
            $user = JWTAuth::toUser($request->input('token'));
            return response()->json([
                'user' => $user
            ],200);
        }catch (JWTException $exception){
            return response()->json([
                'message' => 'Lỗi đường truyền.' . $exception->getMessage()
            ], 404);
        }
    }



}