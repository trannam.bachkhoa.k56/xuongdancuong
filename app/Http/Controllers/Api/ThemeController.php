<?php
/**
 * Created by PhpStorm.
 * User: Nam Moma
 * Date: 3/16/2020
 * Time: 11:29 AM
 */

namespace App\Http\Controllers\Api;

use App\Entity\Category;
use App\Entity\Domain;
use App\Entity\Information;
use App\Entity\MailConfig;
use App\Entity\Menu;
use App\Entity\Post;
use App\Entity\SettingGetfly;
use App\Entity\Template;
use App\Entity\Theme;
use App\Entity\TypeInformation;
use App\Entity\TypeInput;
use App\Entity\TypeSubPost;
use App\Entity\User;
use App\Entity\CategoryPost;
use App\Entity\Product;
use App\Entity\Input;
use App\Http\Controllers\Controller;
use App\Ultility\CallApi;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Entity\UserCustomer;

class ThemeController extends Controller
{
    protected $themeCode = 'xhome';
    protected $emailUser = 'vn3ctran@gmail.com';
    protected $website = 'moma.vn';
    protected $websiteDefault = 'moma.vn';
    
    public function index (Request $request) {
        $validation = Validator::make($request->all(), [
            'domain' => 'required|string',
            'email' => 'required|email|string|unique:users',
            'phone' => 'required',
            'theme_code' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $domainExist = Domain::where('name', $request->input('domain'))->exists();
        if (!empty($domainExist)) {
            $validation->errors()->add('errorDomain', 'Tên cửa hàng đã tồn tại vui lòng chọn domain khác!');
            return response([
                'status' => 500,
                'message' => 'Tên cửa hàng đã tồn tại vui lòng chọn domain khác!',
            ])->header('Content-Type', 'text/plain');
        }
        $theme = Theme::where('code', $request->input('theme_code'))->first();
        if (empty($theme)) {
            $validation->errors()->add('errorTheme', 'Không tồn tại Theme!');
            return response([
                'status' => 500,
                'message' => 'Theme không tồn tại!',
            ])->header('Content-Type', 'text/plain');
        }

//         if validation fail return error
        if ($validation->fails()) {
            return response([
                'status' => 500,
                'message' => $validation->errors(),
            ])->header('Content-Type', 'text/plain');
        }


        $url = 'http://'.Ultility::createSlug($request->input('domain')).'.'.$this->website;

        //$this->checkMailCustomer($request, $url);

        DB::beginTransaction();
        // tạo mới user
        $userId = $this->createUser($request);

        $this->sendMailAdmin($request);
        $this->sendMailCustomer($request, $url);
        // tạo mới user

        $theme = Theme::where('code', $request->input('theme_code'))->first();

        // tạo mới domain
        $domainId = $this->createDomain($request, $url, $userId, $theme);
        //$saveDomainToProduct = $this->saveDomainToProduct($request, $url);

        //tạo mới website
        $theme = new Theme();
        $theme->createTheme($request->input('theme_code'), $request->input('email'));

        // Đồng bộ chiến dịch lên getfly
        if ($domainId) {
            SettingGetfly::addNewCampaignGetfly(
                $request->input('domain'),
                $request->input('phone'),
                $request->input('email'),
                $request->input('address'),
                $url.'-'.$request->input('email').'/'.$request->input('password'),
                $request->has('utm_source') ? $request->input('utm_source') : 'https://moma.vn'
            );
        }

        DB::commit();

        $encrypt = Crypt::encrypt([
            'random' => rand(1000000, 10000000),
            'domainId' => $domainId,
            'userId' =>  $userId,
        ]);

        return response([
            'status' => 200,
            'urlLogin' => $url.'/check-user-redirect/'.$encrypt,
            'url' => $url,
            'user_id' => $userId
        ])->header('Content-Type', 'text/plain');
    }

    private function checkMailCustomer(Request $request, $url) {
        $subject = 'Moma website miễn phí kích hoạt tài khoản: ';
        $content =  '<p>Cảm ơn bạn đã đăng ký website tại moma.vn</p>';
        $content .= '<p>Để kích hoạt tài khoản bạn vui lòng kích vào đường dẫn dưới đây: </p>';
        $content .= '<p><a href="'.$url.'">Kích Hoạt</a></p>';

        MailConfig::sendMail($request->input('email'), $subject, $content);
    }

    private function saveDomainToProduct($request, $url){
        // try {
        // DB::beginTransaction();
        // lấy user id
        $userId = 1;

        // if slug null slug create as title
        $slug = $request->input('phone');
        if (empty($slug)) {
            $slug = Ultility::createSlug($request->input('phone'));
        }

        if (!empty($request->input('parents'))) {
            $categoriParents = Category::whereIn('category_id', $request->input('parents'))
                ->where('user_email', $this->emailUser)
                ->where('theme_code', $this->themeCode)->get();
            $categories = array();
            foreach ($categoriParents as $cate) {
                $categories[] =  $cate->title;
            }
        }

        $content = 'Website Của Anh /Chị'. $request->input('name_home'). ' được khởi tạo trên nền tảng Moma ';
        $description = 'Địa chỉ website là : '.$url ;

        // insert to database
        $post = new Post();
        $postId = $post->insertGetId([
            'title' => $request->input('domain'),
            'post_type' => 'product',
            'template' =>  $request->input('template'),
            'description' => $description,
            'tags' => $request->input('tags'),
            'image' =>  '/libraries/libraryvn3c-1/images/LogoMoMa%20(1).jpg',
            'content' =>  $content,
            'user_email' => 'dieuhanhmoma@gmail.com',
            'theme_code' => 'xhome',
            'visiable' => 0,
            'category_string' => !empty($categories) ? implode(',', $categories) : '',
            'meta_title' => $request->input('meta_title'),
            'meta_description' => $request->input('meta_description'),
            'meta_keyword' => $request->input('meta_keyword'),
        ]);

        // insert slug
        $postWithSlug = $post->where('slug', $slug)
            ->where('user_email', 'dieuhanhmoma@gmail.com')
            ->where('theme_code', 'xhome')
            ->first();
        if (empty($postWithSlug)) {
            $post->where('post_id', '=', $postId)
                ->where('user_email', 'dieuhanhmoma@gmail.com')
                ->where('theme_code', 'xhome')
                ->update([
                    'slug' => $slug
                ]);
        } else {
            $post->where('post_id', '=', $postId)
                ->where('user_email', 'dieuhanhmoma@gmail.com')
                ->where('theme_code', 'xhome')
                ->update([
                    'slug' => $slug.'-'.$postId
                ]);
        }

        // insert danh mục cha
        $categoryPost = new CategoryPost();
        if (!empty($request->input('parents'))) {
            foreach ($request->input('parents') as $parent) {
                $categoryPost->insert([
                    'category_id' => $parent,
                    'post_id' => $postId,
                    'user_email' => 'dieuhanhmoma@gmail.com',
                    'theme_code' => 'xhome',
                ]);
            }
        }

        $isDiscount = $request->input('is_discount');
        $discountStart = null;
        $discountEnd = null;
        if ($isDiscount == 1) {
            $discountStartEnd = $request->input('discount_start_end');
            $discountTime = explode('-', $discountStartEnd);
            $discountStart = $discountTime[0];
            $discountEnd = $discountTime[1];

        }
        $product = new Product();
        $product->insert([
            'post_id' => $postId,
            'code' =>  $request->input('domain'),
            'price' =>  !empty($request->input('price')) ? str_replace(".", "", $request->input('price')) : 0,
            'discount' =>  !empty($request->input('discount')) ? str_replace(".", "", $request->input('discount')) : 0,
            'price_deal' =>  $request->input('price_deal'),
            'cost' =>  $request->input('cost'),
            'wholesale' =>  $request->input('wholesale'),
            'discount_start' => new \Datetime($discountStart),
            'discount_end' => new \Datetime($discountEnd),
            'image_list' =>  $request->input('image_list'),
            'properties' =>  $request->input('properties'),
            'buy_together' => !empty($request->input('buy_together')) ? implode(',', $request->input('buy_together')) : null,
            'buy_after' => !empty($request->input('buy_after')) ?  implode(',', $request->input('buy_after')) : null,
            'filter' => !empty($request->input('filter')) ?  implode(',', $request->input('filter')) : null
        ]);


        // insert input
        $typeInputDatabase = TypeInput::orderBy('type_input_id')
            ->where('theme_code', $this->themeCode)->get();
        foreach($typeInputDatabase as $typeInput) {
            $token = explode(',', $typeInput->post_used);
            if (in_array('product', $token)) {
                $contentInput =  $request->input($typeInput->slug);
                $input = new Input();
                $input->insert([
                    'type_input_slug' => $typeInput->slug,
                    'content' => $contentInput,
                    'post_id' => $postId,
                    'user_email' => 'dieuhanhmoma@gmail.com',
                    'theme_code' => 'xhome',
                ]);
            }
        }

        // DB::commit();

        // } catch (\Exception $e) {
        //     DB::rollback();
        //     Error::setErrorMessage('Lỗi xảy ra khi tạo mới: dữ liệu không hợp lệ.');
        //     Log::error('Lỗi xảy ra trong quá trình tạo mới');
        // } finally {
        //     return redirect()->back();
        // }

    }

    public function storeAccountTheme(Request $request) {
        $validation = Validator::make($request->all(), [
            'domain' => 'required|string',
            'email' => 'required|email|string',
            'theme_code' => 'required',
        ]);
        $user = User::where('email', $request->input('email'))->first();
        $url = 'http://'.Ultility::createSlug($request->input('domain')).'.'.$this->website;

        if (empty($user)) {
            return redirect('/trang/tao-moi-website-thanh-cong?website='.$url )
                ->withErrors($validation)
                ->withInput();
        }

        $domainExist = Domain::where('name', $request->input('domain'))
            ->orWhere('user_id', $user->id)
            ->exists();

        if (!empty($domainExist)) {
            $validation->errors()->add('errorDomain', 'Tên cửa hàng đã tồn tại vui lòng chọn domain khác!');

            return redirect('/trang/tao-moi-website-thanh-cong?website='.$url )
                ->withErrors($validation)
                ->withInput();
        }

        $theme = Theme::where('code', $request->input('theme_code'))->first();
        if (empty($theme)) {
            $validation->errors()->add('errorDomain', 'Theme không tồn tại!');

            return redirect('/trang/tao-moi-website-thanh-cong?website='.$url )
                ->withErrors($validation)
                ->withInput();
        }

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('/trang/tao-moi-website-thanh-cong?website='.$url );
        }

        $fileName = null;
        try {

            $this->sendMailAdmin($request);
            $this->sendMailCustomer($request, $url);
            DB::beginTransaction();
            // tạo mới user

            $theme = Theme::where('code', $request->input('theme_code'))->first();
            // tạo mới domain
            $domainStatus = $this->createDomain($request, $url, $user->id, $theme);
            $saveDomainToProduct = $this->saveDomainToProduct($request, $url);
            //tạo mới website
            $theme = new Theme();
            $theme->createTheme($request->input('theme_code'), $request->input('email'));

            // Đồng bộ chiến dịch lên getfly
            if ($domainStatus) {
                SettingGetfly::addNewCampaignGetfly(
                    $user->name,
                    $user->phone,
                    $user->email,
                    $user->address,
                    $url.'-'.$request->input('email').'/'.$request->input('password'),
                    $request->has('utm_source') ? $request->input('utm_source') : 'https://moma.vn'
                );
            }

            DB::commit();

            return redirect('/trang/tao-moi-website-thanh-cong?website='.$url );

        } catch (\Exception $e) {
            Log::error('http->site->ThemeController->storeAccountTheme: lỗi tạo mới domains');
            DB::rollback();
        }
    }



    protected function sendMailAdmin($request) {
        if (!empty($this->domainUser)) {
            $subject = 'Có người tạo website tại: '.$this->domainUser->name;
            $content =  'Khách hàng vừa tạo website <br> ';
            $content .= 'Tên website : '.$request->input('domain');
            $content .= 'Số điện thoại: '.$request->input('phone');
            $content .= 'Email: '.$request->input('email');
        } else {
            $subject =  'Đơn hàng mới từ website bên phía: VN3C';
            $content =  'Khách hàng vừa tạo website <br> ';
            $content .= 'Tên website : '.$request->input('domain'). '<br>';
            $content .= 'Số điện thoại: '.$request->input('phone'). '<br>';
            $content .= 'Email: '.$request->input('email'). '<br>';
        }

        MailConfig::sendMail('vn3ctran@gmail.com', $subject, $content);
    }

    protected function sendMailCustomer($request, $domainAdmin) {

        $subject =  'Chúc mừng bạn đã tạo website miễn phí thành công!';
        $content =  '<p> Để quản trị website vui lòng truy cập </p>';
        $content .= '<p> '.$domainAdmin. '/admin </p>';
        $content .= '<p> Tài khoản truy cập: '.$request->input('email'). '</p>';
        $content .= 'Mật khẩu: '.$request->input('password'). '<br>';


        MailConfig::sendMail($request->input('email'), $subject, $content);
    }

    private function createUser($request) {
        $user = new User();
        $userId = $user->insertGetId([
            'name' => $request->input('name_home'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'role' => 3,
            'vip' => 0,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
        if (isset($_COOKIE["gift"])) {
            $gift = unserialize($_COOKIE['gift'], ["allowed_classes" => false]);
            $userCustomer = $this->addCustomerUser($gift, $userId);
        }

        return $userId;
    }

    private function addCustomerUser($gift, $userId){
        $userCustomer = UserCustomer::insertGetId([
            'user_id' => $gift,
            'customer_id' => $userId,
            'status' => 0,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }

    private function createDomain($request, $url, $userId, $theme) {
        $endUser = date('Y-m-d',strtotime('+3650 days', time()));
        $domain = Domain::where('name', $request->input('domain'))->first();

        if (empty($domain)) {
            $domainModel = new Domain();
            $domainId = $domainModel->insertGetId([
                'name' => $request->input('domain'),
                'url' => $url,
                'theme_id' => $theme->theme_id,
                'user_id' => $userId,
                'start_at' => new \DateTime(),
                'end_at' => $endUser,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);

            return $domainId;
        }

        return $domain->domain_id;
    }
}
