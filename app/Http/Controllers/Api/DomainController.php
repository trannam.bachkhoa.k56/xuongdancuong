<?php
namespace App\Http\Controllers\Api;
use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Comment;
use App\Entity\Domain;
use App\Entity\Information;
use App\Entity\Input;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Template;
use App\Entity\TypeInformation;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

use Validator;

class DomainController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new res    */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $product_id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $product_id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $product_id
     * @param int $post_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product_id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $product_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $product_id)
    {

    }

    public function searchDomain(Request $request)
    {
        try{
            if(!$request->has('domain')){
                return response()->json([
                    'message' => 'Chưa nhập tên miền website'
                ],404);
            }
            if(!Domain::where('domains.url','like','%' . $request->input('domain') . '%')->exists()){
                return response()->json([
                    'message' => 'Tên domain không tồn tại'
                ],404);
            }
            $domains = Domain::where('domains.url','like','%' . $request->input('domain') . '%')
                ->select('url')->get();
            $domains = $domains->map(function($domain){
                $domain->result = str_replace('http://','',$domain->url);
                return $domain;
            });
            return response()->json([
                "data"=>$domains,
            ],200);
        } catch (\Exception $exception){
            return response()->json([
                "message"=>"Có lỗi xảy ra: ".$exception->getMessage(),
            ],404);
        }
    }
}


