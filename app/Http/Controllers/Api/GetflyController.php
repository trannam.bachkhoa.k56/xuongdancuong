<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 7/23/2019
 * Time: 1:32 PM
 */

namespace  App\Http\Controllers\Api;


use App\Entity\Domain;
use App\Entity\Post;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class GetflyController extends Controller
{
    /*
     * name: get campaign Getfly
     * input 1: api-key-getfly
     * input 2: base-url-getfly
     *
     * output: array campaign getfly
     */
    public function getCampaignGetfly(Request $request) {
        try {
            $apiKey = $request->input('api-key-getfly');
            $baseUrl = $request->input('base-url-getfly');

            $campaigns = $this->getCampaigns($apiKey, $baseUrl);

            // campaign trả về null
            if (!isset($campaigns['decode']) || empty($campaigns['decode'])) {
                return response()->json([
                    'status' => 500,
                    'error' => 'Cấu hình của bạn chưa đúng!'
                ])->header('Content-Type', 'application/json');
            }

            $campaignsResponse = array();
            foreach ($campaigns['decode'] as $campaign) {
                $campaignsResponse[] = (object) [
                    'campaign_id' => $campaign['campaign_id'],
                    'campaign_name' => $campaign['campaign_name'],
                    'token_api' => $campaign['token_api'],
                ];
            }


            return response()->json($campaignsResponse)->header('Content-Type', 'application/json');

        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'error' => 'Cấu hình của bạn chưa đúng!'
            ])->header('Content-Type', 'application/json');
        }

    }

    /*
        * name: add campaign Getfly
        * input 1: api-key-getfly
        * input 2: base-url-getfly
        * input 3: name
        * input 4: phone
        * input 5: email
        * input 6: note
        * input 7: campain-getly: create campaignId-token
        *
        * output: array campaign getfly
        */
    public function addCampaignGetfly(Request $request) {
        try {
            $apiKey = $request->input('api-key-getfly');
            $baseUrl = $request->input('base-url-getfly');
            $campaignGetfly = $request->input('campain-getly');

            // nếu không lấy được campaign getfly thì trả về lỗi
            $tokenCampaignGetlies = explode('-', $campaignGetfly);
            if (count($tokenCampaignGetlies) != 2 || !isset($tokenCampaignGetlies[0]) || !isset($tokenCampaignGetlies[1])) {
                return response('Cấu hình của bạn có lỗi', 500)
                    ->header('Content-Type', 'text/plain');
            }

            $tokenApiCampain = $tokenCampaignGetlies[1];
            $campainId = $tokenCampaignGetlies[0];
            // lấy ra trạng thái cơ hội đầu tiên của chiến dịch
            $campaignStatusList = $this->getStatusCampaign($apiKey, $baseUrl, $campainId);
            $campainStatus = isset($campaignStatusList['decode'][0]['opportunity_status_id']) ? $campaignStatusList['decode'][0]['opportunity_status_id'] : 0;

            $account = (object) [
                "account_name" => $request->input('name'),
                "phone_office" => $request->input('phone'),
                "email" => $request->input('email'),
                "gender" =>  0,
                "billing_address_street" => $request->input('note'),
                // "birthday" => $request->input('birthday_day').'/'.$request->input('birthday_Month').'/'.$request->input('birthday_Year'),
                "account_type" =>  1,
                "industry" => "2,3"
            ];

            $opportunity = (object) [
                'token_api' => $tokenApiCampain,
                'user_id' => "",
                'recipient' => "",
                'opportunity_status' => $campainStatus,

            ];

            $contacts = [
                "first_name" => $request->input('name'),
                "email" => $request->input('email'),
                "phone_mobile" => $request->input('phone')
            ];

            $referer = (object) [
                "utm_source" =>  'extensions chrome',
                "utm_campaign" => 'extensions chrome',
            ];

            $data = (object) [
                'account' => $account,
                'contacts' => $contacts,
                'opportunity' => $opportunity,
                'referer' => $referer
            ];

            $this->addCampaign($apiKey, $baseUrl, $data);

            return response()->json([
                'status' => 200,
                'message' => 'Bạn đã tạo cơ hội thành công!'
            ])->header('Content-Type', 'application/json');

        } catch (\Exception $e) {
            return response()->json([
                'status' => 500,
                'message' => 'Cấu hình của bạn chưa đúng!'
            ])->header('Content-Type', 'application/json');
        }
    }

    private function getCampaigns($apiKey, $baseUrl) {
        // truyền dữ liệu lên
        $service_url = $baseUrl. '/api/v3/campaigns/';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'X-API-KEY: '. $apiKey,
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // kết quả response trả về
        $curl_response = curl_exec($curl);
        if(curl_errno($curl)){
            echo 'Request Error:' . curl_error($curl);
        }
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $decoded = json_decode($curl_response, true);
        curl_close($curl);

        return [
            'httpCode' => $httpCode,
            'decode' => $decoded
        ];
    }

    private function addCampaign($apiKey, $baseUrl, $data) {
        $service_url = $baseUrl. '/api/v3/opportunity/';
        $curl = curl_init($service_url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'X-API-KEY: '. $apiKey,
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POST, count($data));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        $curl_response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $decoded = json_decode($curl_response, true);
        if($httpcode == 200){
            return true;
        } else {
            return false;
        }
    }

    private function getStatusCampaign($apiKey, $baseUrl, $campainId) {
        // truyền dữ liệu lên
        $service_url = $baseUrl. 'api/v3/campaigns/'.$campainId.'/status';

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'X-API-KEY: '. $apiKey,
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // kết quả response trả về
        $curl_response = curl_exec($curl);
        if(curl_errno($curl)){
            echo 'Request Error:' . curl_error($curl);
        }
        //$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $decoded = json_decode($curl_response, true);
        curl_close($curl);

        return [
            //'httpCode' => $httpCode,
            'decode' => $decoded
        ];
    }


}