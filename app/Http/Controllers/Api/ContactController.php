<?php

namespace App\Http\Controllers\Api;

use App\Entity\Contact;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Entity\Domain;
use Illuminate\Support\Facades\DB;
use Validator;
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Hiển thị danh sách liên hệ
    public function index(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->input('token'));
            $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
                ->where('domains.user_id',$user->id)
                ->where('domains.url', $request->input('domain'))
				->orWhere('domains.domain', $request->input('domain'))
				->select('themes.code')->first();
            if(empty($theme)) {
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên domain'
                ],404);
            }
            $contacts = Contact::where('contact.user_email', $user->email)
                    ->where('contact.theme_code', $theme->code)
                    ->groupBy('contact.email');
            if($contacts->get()->count() == 0){
                return response()->json([
                    'status' => 200,
                    'message' => 'Hiện chưa có khách hàng nào',
                ])->header('Content-Type', 'text/plain');
            }
            return response()->json([
                'status' => 200,
                'data' => $contacts->paginate(10),
            ])->header('Content-Type', 'text/plain');
       } catch (\Exception $e) {
            return response('Có lỗi xảy ra', 500)
                ->header('Content-Type', 'text/plain');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // Tạo mới danh sách liên hệ
    public function store(Request $request)
    {
       try {
           $user = JWTAuth::toUser($request->token);
//            $theme = Domain::join('themes','themes.theme_id','domains.theme_id')
//            ->where('domains.url',$request->domain) -> select('themes.code')->first();
           $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
               ->where('domains.user_id',$user->id)
               ->where('domains.url', $request->input('domain'))
			   ->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
           if(empty($theme)){
               return response()->json([
                   'message' => 'Thử kiểm tra lại tên domain'
               ],404);
           }
            $contact = new Contact();
            $contactId = $contact->insertGetId([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'message' => $request->input('message'),
                'theme_code' => $theme->code,
                'user_email' => $user->email,
            ]);
            return response()->json([
                'status' => 200,
                'content' => ' Thêm mới thành công ',
                'contact_id' => $contactId,
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'message' => $request->input('message'),
                'theme_code' => $theme->code,
                'user_email' => $user->email,
            ])->header('Content-Type', 'text/plain');

        } catch (\Exception $exception) {
               return response()->json([
                   'message' => 'Có lỗi xảy ra: '.$exception->getMessage(),
               ], 404);
           }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $contact_id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$contact_id)
    {
       try {
           $user = JWTAuth::toUser($request->token);
           $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
               ->where('domains.user_id',$user->id)
               ->where('domains.url', $request->input('domain'))
			   ->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
           if(empty($theme)){
               return response()->json([
                   'message' => 'Thử kiểm tra lại tên domain'
               ],404);
           }
           $contacts = Contact::where('contact_id', $contact_id)
                ->where('user_email',$user->email)
                ->where('theme_code',$theme->code)->get();

            return response()->json([
                'status' => 200,
                'data' => $contacts
            ])->header('Content-Type', 'text/plain');
        } catch (\Exception $exception) {
           return response()->json([
               'message' => 'Có lỗi xảy ra: '.$exception->getMessage(),
           ], 404);
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $contact_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$contact_id)
    {
       try {
                 $user = JWTAuth::toUser($request->token);
                 $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
                    ->where('domains.user_id',$user->id)
                    ->where('domains.url', $request->input('domain'))
					->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
                 if(empty($theme)){
                     return response()->json([
                   'message' => 'Thử kiểm tra lại tên domain'
                     ],404);
                 }
                 $contact = Contact::where('contact_id',$contact_id)
                     ->where('user_email',$user->email)
                     ->where('theme_code',$theme->code)->first();

                $contact->update([
                    'name' => $request->has('name')?$request->input('name') : $contact->name,
                    'phone' => $request->has('phone')?$request->input('phone') : $contact->phone,
                    'email' => $request->has('email')?$request->input('email') : $contact->email,
                    'address' => $request->has('address')?$request->input('address') : $contact->address,
                    'message' => $request->has('message')?$request->input('message') : $contact->message,
                    'updated_at' => new \DateTime(),
                ]);

            return response()->json([
                'status' => 200,
                'content' => ' update thành công ! ',
                'contact_id' => $contact_id,
                'name' => $request->input("name"),
                'phone' => $request->input('phone') ,
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'message' => $request->input('message'),
            ])->header('Content-Type', 'text/plain');

        } catch (\Exception $e) {
           return response()->json([
               'message' => 'Có lỗi xảy ra: '.$e->getMessage(),
           ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $contact_id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$contact_id)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
                    ->where('domains.url', $request->domain)
					->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
            Contact::where('contact_id',$contact_id)
                ->where('user_email',$user->email)
                ->where('theme_code',$theme->code)
                ->delete();
            return response()->json([
                'status' => 200,
                'message' => 'xóa thành công'
            ])->header('Content-Type', 'text/plain');
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Có lỗi xảy ra: '.$e->getMessage(),
            ], 404);
        }
    }
}