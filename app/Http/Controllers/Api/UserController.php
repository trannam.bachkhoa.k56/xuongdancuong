<?php

namespace App\Http\Controllers\Api;

use App\Entity\Domain;
use App\Entity\Information;
use App\Entity\MailConfig;
use App\Entity\User;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $userLogin = JWTAuth::toUser($request->token);
        $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
            ->where('domains.url', $request->domain)
			->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();


        if (!empty($userLogin) && !empty($theme)) {
            try {
                $user = new User();
                $users = $user->orderBy('user_id')
                    ->select('email', 'phone', 'name', 'address', 'image', 'age','created_at')
                    ;

                return response([
                    'status' => 200,
                    'content' => $users->paginate(10)
                ])->header('Content-Type', 'text/plain');
            } catch (\Exception $e) {
                return response('Lỗi code', 500)
                    ->header('Content-Type', 'text/plain');
            }
        }
        return response()->json([
             'message' =>  'Bạn cần đăng nhập để có thể thực hiện thao tác này'
        ], 500);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getInforDomain(Request $request) {
        try{
            if(!$request->has('domain')){
                return response()->json([
                    'message' => 'Chưa nhập tên miền website'
                ],404);
            }
            if(!Domain::where('domains.url','like','%' . $request->input('domain') . '%')->exists()){
                return response()->json([
                    'message' => 'Tên domain không tồn tại'
                ],404);
            }
            $domain = Domain::where('domains.url','like','%' . $request->input('domain') . '%')
				->orWhere('domains.domain','like','%' . $request->input('domain') . '%')
                ->select('user_id')->first();
            $emailUser = User::where('id',$domain->user_id)->select('email')->first();

            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                ->where('domains.user_id',$domain->user_id)
                ->where('domains.url','like','%' . $request->input('domain') . '%')
				>orWhere('domains.domain','like','%' . $request->input('domain') . '%')
                ->select('themes.code')->first();

            $informations = Information::Join('type_information','information.slug_type_input','type_information.slug')
                ->where('information.user_email', $emailUser->email)
                ->where('information.theme_code',$theme->code)
                ->groupBy('slug_type_input')
                ->get(['title','content']);
            if($informations->count() == 0){
                return response()->json([
                    'message' => 'Website này chưa có thông tin nào'
                ],200);
            }
            return response()->json([
                'message' => 'Lấy thông tin website thành công',
                'data' => $informations
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Có gì đó xảy ra: '.$exception->getMessage(),
            ], 404);
        }

    }
    public function forgetPass(Request $request){
        try{
            $user = User::where('email', $request->input('email'))->first();
            if(!$request->has('domain')){
                return response()->json([
                    'message' => 'Chưa nhập tên miền website'
                ],404);
            }
            if(!Domain::where('domains.url','like','%' . $request->input('domain') . '%')
				>orWhere('domains.domain','like','%' . $request->input('domain') . '%')
                ->where('domains.user_id',$user->id)->exists()){
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên domain'
                ],404);
            }
            if(!empty($user)) {
                $this->ResetPass($user->id);
                $message = 'Tài khoản '.$user->email.' đã đặt lại mật khẩu vào lúc '.Carbon::now()->toDateTimeString().' .Mật khẩu mới của bạn: '.'abienkute' ;
                MailConfig::sendMail($user->email, 'Thư thông báo đặt lại mật khẩu', $message);
                return response()->json([
                    'message' => 'Mật khẩu mới đã được gửi qua email của bạn. Mời bạn vui lòng check email để nhận mật khẩu.',
                    'password'=>'abienkute'
                ], 200);
            }
            return response()->json([
                'message' => 'Email của bạn không đúng hoặc chưa được đăng ký.'
            ], 404);
        }catch (\Exception $exception){
            return response()->json([
                'message' => 'Đã có lỗi xảy ra : ' . $exception->getMessage()
            ], 404);
        }
    }
    private function ResetPass($id) {
        User::where('id',$id)->update(
            [
                'password'=>bcrypt('abienkute')
            ]
        );
    }
    public function updateUser(Request $request){
        try {
            $user = JWTAuth::toUser($request->input('token'));
                if(!empty($user)) {
                    DB::beginTransaction();
                    $infoUser = User::find($user->id);
                    $infoUser->image = $request->has('image')?$request->input('image'):$infoUser->image;
                    $infoUser->name = $request->has('name')?$request->input('name'):$infoUser->name;
                    $infoUser->phone = $request->has('phone')?$request->input('phone'):$infoUser->phone;
                    $infoUser->password = $request->has('password')?bcrypt($request->input('password')):$infoUser->password;
                    $infoUser->save();
                    $updateUser= User::select('email','name','phone','image')
                        ->where('id',$user->id)
                        ->first();
                    DB::commit();
                    return response()->json([
                        'status' => 200,
                        'message' => 'update info success',
                        'user'=> $updateUser
                    ]);
                }
                return response()->json([
                    'status' => 404,
                    'message' => 'Bạn cần đăng nhập để thực hiện chức năng này.'
                ]);
        }catch (\Exception $exception){
            DB::rollBack();
            return response()->json([
                'status' => 404,
                'message' => 'Bạn cần đăng nhập để thực hiện chức năng này.'.$exception->getMessage()
            ]);
        }
    }

}
