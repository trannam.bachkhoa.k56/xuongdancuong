<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Category;
use App\Entity\CategoryPost;
use App\Entity\Comment;
use App\Entity\Domain;
use App\Entity\Information;
use App\Entity\Input;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\Template;
use App\Entity\TypeInformation;
use App\Entity\User;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

use Validator;
class CategoryController extends Controller
{
    public function createCategory(Request $request) {
        try{
            $user = JWTAuth::toUser($request->input('token'));
            $theme = Domain::join('themes', 'domains.theme_id', 'themes.theme_id')
                ->where('domains.user_id',$user->id)
                ->where('domains.url', $request->input('domain'))
				->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();
            if(empty($theme)){
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên domain'
                ],404);
            }
            if(!$request->has('title')) {
                return response()->json(["message"=>"Vui lòng nhập tên danh mục"],401);
            }
            if(!$request->has('post_type')) {
                return response()->json(["message"=>"Vui lòng nhập loại danh mục (post hoặc product)"],401);
            }
            $category = new Category();
            $cateId = $category->insertGetId([
                'title' => $request->input('title'),
                'parent' => 0,
                'post_type' =>  $request->input('post_type'),
                'theme_code' => $theme->code,
                'user_email' => $user->email,
                'template' =>  $request->input('template'),
                'description' => $request->input('description'),
                'meta_title' => $request->input('meta_title'),
                'meta_description' => $request->input('meta_description'),
                'meta_keyword' => $request->input('meta_keyword'),
                'image' =>  $request->input('image'),
            ]);
            $slug = $request->input('slug');
            $cateWithSlug = $category->where('slug', $slug)
                    ->where('post_type', $request->input('post_type'))
                    ->where('user_email', $user->email)
                    ->where('theme_code', $theme->code)->first();
                if (empty($cateWithSlug)) {
                    $category->where('category_id', '=', $cateId)
                        ->where('user_email', $user->email)
                        ->where('theme_code', $theme->code)
                        ->update([
                            'slug' => $request->input('slug')
                        ]);
                } else {
                    $category->where('category_id', '=', $cateId)
                        ->where('user_email', $user->email)
                        ->where('theme_code', $theme->code)
                        ->update([
                            'slug' => $request->input('slug') . '-' . $cateId
                        ]);
                }
            return response()->json([
                "message"=>"Thêm mới thành công danh mục",
                "data"=> Category::where('category_id',$cateId)->first()
            ],200);
            }catch (\Exception $exception){
                return response()->json([
                "message"=>"Có lỗi xảy ra: ".$exception->getMessage(),
                ],404);
        }
    }
}
