<?php

namespace App\Http\Controllers\Api;

use App\Entity\Domain;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\OrderItem;
use App\Entity\Contact;
use App\Entity\MailConfig;
use App\Entity\OrderCustomer;
use App\Entity\OrderCustomerItem;
use App\Entity\Product;
use App\Entity\Theme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $theme = Domain::join('themes', 'themes.theme_id', 'domains.theme_id')
                ->where('domains.user_id',$user->id)
                ->where('domains.url', $request->domain)
				->orWhere('domains.domain', $request->input('domain'))->select('themes.code')->first();

            if(empty($theme)){
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên domain'
                ],404);
            }
            $orders = Order::orderBy('created_at', 'desc')
                ->where('status', '>', 0)
                ->where('theme_code', $theme->code)
                ->where('user_email', $user->email)
                ->paginate(10);

            foreach($orders as $id => $order) {
                $orders[$id]->orderItems = OrderItem::join('products','products.product_id','=', 'order_items.product_id')
                    ->join('posts', 'products.post_id','=','posts.post_id')
                    ->select(
                        'posts.*',
                        'products.price',
                        'products.discount',
                        'products.code',
                        'order_items.*'
                    )
                    ->where('order_id', $order->order_id)
                    ->where('order_items.theme_code', $theme->code)
                    ->where('order_items.user_email', $user->email)
                    ->get();
            }
            if($orders->count() == 0){
                return response()->json([
                    'status' => 200,
                    'message' => 'Hiện chưa có đơn hàng nào',
                ])->header('Content-Type', 'text/plain');
            }
            return response()->json([
                'status' => 200,
                'data' => $orders
            ])->header('Content-Type', 'text/plain');
        } catch (\Exception $e) {
            return response('Có lỗi xảy ra', 500)
                ->header('Content-Type', 'text/plain');
        }
    }

    /**
     * Store a newly created resource in storage.
     * varchar domain (url) 
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try {
            // truyền lên domain để xác định đơn hàng
            $domain = $this->getDomain($request->domain);
            if (empty ($domain)) {
                return response()->json([
                    'message' => 'Thử kiểm tra lại tên miền truyền lên'
                ],404);
            }

            // thông tin khách hàng
            $contactId = $this->storeContact($request, $domain->code, $domain->email);
            
            // lấy ra toàn bộ sản phẩm và tính tổng tiền
            $productIds = array();
            $totalPrice = 0;
            $totalCost = 0;
            if ($request->has('products')) {
                foreach ($request->input('products') as $product) {
                    $totalPrice += $product['price']*$product['quantity'];
                }
    
                // lấy ra sản phẩm truyền lên
                $productGetFromDBs = Product::whereIn('product_id', $productIds)
                ->get();
    
                // tính ra giá nhập
                foreach ($productGetFromDBs as $productGetFromDB) {
                    $totalCost += $productGetFromDB->cost;
                }
            }
            

            // truyền lên thôn tin sản phẩm đặt hàng        
            $orderCustomer = new OrderCustomer();
            $orderCustomerId = $orderCustomer->insertGetId([
                'user_id' => $domain->user_id,
                'contact_id' => $contactId,
                'payment' => 0,
                'surcharge' =>  0,
                'total_price' => $totalPrice,
                'cost' => $totalCost,
                'notes' => $request->input('message'),
                'theme_code' => $domain->code,
                'user_email' => $domain->email,
                'created_at' => new \DateTime()
            ]);
            
            if ($request->has('products')) {
                foreach ($request->input('products') as $id => $product) {
                    $orderCustomerItem = new OrderCustomerItem();
                    $orderCustomerItem->insert([
                        'order_customer_id' => $orderCustomerId,
                        'product_id' => $product['product_id'],
                        'price' => $product['price'],
                        'qty' => $product['quantity'],
                        'tax' => 0,
                        'vat' => 0,
                        'created_at' => new \DateTime()
                    ]);
                }
            }
            
            return response()->json([
                'message' => 'Cập nhật đơn hàng thành công!',
            ], 200);

    //    } catch (\Exception $e) {
    //         return response()->json([
    //             'message' => 'Nhập sai trường dữ liệu: '.$exception->getMessage(),
    //         ], 404);
    //    }
    }

    private function getDomain($domain) {
        $domain = Domain::join('users', 'users.id', 'domains.user_id')
            ->join('themes',  'domains.theme_id', 'themes.theme_id')
            ->where('domains.url','like','%' . $domain . '%')
            ->orWhere ('domain','like','%' . $domain . '%')
            ->select(
                'domains.user_id',
                 'themes.code',
                 'users.email'
            )->first();

        return $domain;
    }

    private function storeContact ($request, $themeCode, $userEmail) {
		$contact = $request->input('contact');
        // kiểm tra xem contact tồn tại chưa.
        $contact = Contact::where('email', $contact['email'])
        ->where('theme_code', $themeCode)
        ->where('user_email', $userEmail)
        ->where('phone', $contact['phone'])
        ->first();

        // nếu tồn tại trả về contact
        if (!empty($contact)) {
            $this->sendMailAdmin($request, $userEmail);

            return $contact->contact_id;
        }

        // thêm mới contact
        $contactId = Contact::insertGetId([
            'name' => $contact['name'],
            'phone' => $contact['phone'],
            'email' => $contact['email'],
            'address' => $contact['address'],
            'message' => $contact['message'],
            'theme_code' => $themeCode,
            'user_email' => $userEmail,
			'utm_source' => $contact['utm_source'],
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        $this->sendMailAdmin($request, $userEmail);
        return  $contactId;
    }

    private function sendMailAdmin($request, $userEmail) {
        $subject =  'Có liên hệ mới từ website bên phía Website: '.$request->domain;
        $content = 'Họ và tên: '. $request->input('name');
        $content .= '<p>Số điện thoại: '.$request->input('phone').'. </p>';
        $content .= '<p>Email: '.$request->input('email').'. </p>';
        $content .= '<p>Địa chỉ: '.$request->input('address').'. </p>';
        $content .= '<p>Nội dung: '.$request->input('message').'. </p>';   

        MailConfig::sendMail($userEmail, $subject, $content);
    }
}
