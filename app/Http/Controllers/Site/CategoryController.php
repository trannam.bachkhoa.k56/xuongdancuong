<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/19/2017
 * Time: 10:21 AM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Category;
use App\Entity\Input;
use App\Entity\Post;
use App\Ultility\Error;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoryController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index($cate_slug, Request $request) {
        // if (!empty($this->domainUser)) {
            // if ( strtotime($this->domainUser->end_at) < time() && ($this->emailUser != 'vn3ctran@gmail.com')) {
                // return redirect(route('admin_dateline'));
            // }
        // }

        $category = $this->getCategoryDetail($cate_slug);

//        $inputs = Input::where('cate_id', $category->category_id)->get();
//        foreach ($inputs as $input) {
//            $category[$input->type_input_slug] = $input->content;
//        }

        $posts = $this->getPosts($category, $request);

        if ( empty($category) || $category->template == 'default' || empty($category->template)  ) {

            return view($this->themeCode.'.default.category', compact('category', 'posts'));
        } else {
            return view($this->themeCode.'.template.'.$category->template, compact('category', 'posts'));
        }
    }

    private function getCategoryDetail($cateSlug) {
        try {
            $category = Category::where('slug', $cateSlug)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
				->where('post_type', 'post')
				->where('deleted_at', null)
                ->first();	
				
            return $category;
        } catch (\Exception $e) {
            Log::error('http->site->CategoryController->getCategoryDetail: Lỗi hiển thị category');

            return redirect('/');
        }
    }

    private function getPosts($category, $request) {
        try {
            $posts = Post::leftJoin('category_post', 'category_post.post_id', '=', 'posts.post_id')
                ->select('posts.*')
                ->where('visiable', 0)
                ->where('category_post.deleted_at','=' , null)
				->where('post_type', 'post')
                ->where('posts.theme_code', $this->themeCode)
                ->where('posts.user_email', $this->emailUser)
                ->orderBy('posts.post_id', 'desc');

            if (!empty($category)) {
                $posts = $posts->where('category_post.category_id', $category->category_id);
            }

            if (!empty($request->input('word'))) {
                $word = $request->input('word');
                $posts =  $posts->where('posts.slug', 'like', '%'.Ultility::createSlug($word).'%');
            }

            $posts = $posts->distinct()->paginate(12);

            return $posts;
        } catch(\Exception $e) {
            Log::error('http->site->CategoryController->getPosts: Lỗi hiển thị category');

            return array();
        }
    }
    
}
