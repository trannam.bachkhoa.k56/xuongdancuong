<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Entity\User;
use App\Entity\FanpageSocial;
use App\Entity\Domain;
use App\Entity\SocialAccount;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
// use Socialite;
use App\Ultility\InforFacebook;

use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Socialite\Facades\Socialite ;

class SocialAuthController  extends SiteController
{
	
    public function redirect($social)
    {	
		
        return Socialite::driver($social)
		->scopes([
            'manage_pages',
			'groups_access_member_info',
			'pages_show_list',
			'publish_pages'
		])
		->redirect();
    }

    public function callback($social)
    {
		
		$userSocial  = Socialite::driver($social)->user();
		$userGet = SocialAccount::where('email',$userSocial['email'])
		->select([
			'user_id',
			'provider_user_id',
			'provider',
			'email',
			'name',
			'avatar',
			'token',
		])
		->first();

		if(!empty($userGet)){
			
			$userMoma = User::where('email',$userSocial['email'] )->first();
		
		if(empty($userMoma)){
			 return redirect()->back();
		}
		
		$domain = Domain::where('user_id', $userMoma->id)->first();
		
		if(empty($domain)){
			 return redirect()->back();
		}
		
		$url = $domain->url;
			  //Auth::login($userGet);
			$userSocialUpdate = SocialAccount::where('email',$userSocial['email'] )->update([
				'provider_user_id' => $userSocial->id,
				'provider' => $social,
				'token' => $userSocial->token,
				'email' => $userSocial['email'],
				'name' => $userSocial['name'],
				'avatar' => $userSocial->avatar_original,
			]);
	
			$this->getPageId($userGet, $userSocial);
			
				
				return redirect($url.'/admin/get-post-facebook');
			}
		
	
        $user = SocialAccount::create([
			'provider_user_id' => $userSocial['id'],
			'provider' => $social,
			'token' => $userSocial->token,
			'email' => $userSocial['email'],
			'name' => $userSocial['name'],
			'avatar' => $userSocial->avatar_original,
		]);
		
		$userMoma = User::where('email',$userSocial['email'] )->first();
		
		if(empty($userMoma)){
			 return redirect()->back();
		}
		
		$domain = Domain::where('user_id', $userMoma->id)->first();
		
		if(empty($domain)){
			 return redirect()->back();
		}
		
		$url = $domain->url;
		
		
		$userFacebook = $userSocial['id'];
		
		if (isset($_COOKIE['userFacebook'])) {
            //unset($_COOKIE['userFacebook']);
            setcookie ("userFacebook", "", time() - 3600);
        }
        setcookie('userFacebook',$userFacebook, time()+ 3600*365, '/');
		
		 return redirect($url.'/admin/get-post-facebook');
    }
	
	
	protected function getPageId($userGet, $userSocial){
		//cấu hình
		$app = new InforFacebook();
		$fb = new \Facebook\Facebook([
			'app_id' => $app->getAppId(),
			'app_secret' => $app->getAppSecret(),
			'default_graph_version' => $app->getDefaultGraphVersion()
		]);
			
		//lấy page_id 
		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $response = $fb->get(
			'/'.$userSocial["id"].'/accounts',
			$userSocial->token
		  );
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		$graphEdge = $response->getGraphEdge();
		
		$this->storePage($graphEdge, $userGet);
		
			
		
	}
	
	private function storePage($graphEdge, $userGet){
		
		//xóa fanpage tồn tại 
		$removeFanpages = FanpageSocial::where('user_id', $userGet->user_id)->delete();

		/* handle the result */
		foreach ($graphEdge as $graphNode) {
				$page = FanpageSocial::insert([
					'user_id' => $userGet->user_id,
					'provider_user_id' => $userGet->provider_user_id,
					'fanpage_id' => $graphNode['id'],
					'fanpage_name' => $graphNode['name'],
					'created_at' => new \DateTime(),
					'updated_at' => new \DateTime()
			]);
		}
	}

	public function loginFacebook(Request $request) {
		$emailUser = $request->input('user_email');
		$currentUrl =  $_SERVER['HTTP_REFERER'];

        return view('social.index', compact('emailUser', 'currentUrl'));
    }
	
	public function saveAccessTokenFacebook (Request $request) {
		$accessToken = $request->input('access_token');
		$email = $request->input('email');

		User::where('email', $email)
            ->update([
                'accesstoken' => $accessToken
            ]);
			
			 // tra ve ket qua
		return response([
             'status' => 200
         ])->header('Content-Type', 'text/plain');
	}

}
