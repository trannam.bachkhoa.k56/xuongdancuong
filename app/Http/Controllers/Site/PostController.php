<?php

namespace App\Http\Controllers\Site;

use App\Entity\Category;
use App\Entity\Input;
use App\Entity\MailConfig;
use App\Entity\Post;
use Illuminate\Support\Facades\Log;

/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/19/2017
 * Time: 9:50 AM
 */
class PostController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index($cate_slug, $slug_post) {
		try {
			// if (!empty($this->domainUser)) {
				// if ( strtotime($this->domainUser->end_at) < time() && ($this->emailUser != 'vn3ctran@gmail.com')) {
					// return redirect(route('admin_dateline'));
				// }
			// }

			$post = $this->getPostDetail($slug_post);
			$category = $this->getCategory($post);

			if ($post->template == 'default') { 
			   return view($this->themeCode.'.default.single', compact('post', 'category'));
			} else {
				return view($this->themeCode.'.template.'.$post->template, compact('post', 'category'));
			}
		} catch (\Exception $e) {
            Log::error('http->site->ProductController->index: loi lay san pham');
            return redirect('/');
        }
    }

    private function getPostDetail($slug_post) {
        try {
            $post = Post::where('post_type', 'post')
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
				->where('slug', $slug_post)->first();

            $inputs = Input::where('post_id', $post->post_id)->get();
            foreach ($inputs as $input) {
                $post[$input->type_input_slug] = $input->content;
            }

            Post::where('post_id', $post->post_id)->update([
                'views' => !empty($post->views) ? $post->views + 1 : 1
            ]);

            if ($post->views % 50 == 0) {
                $this->sendMail($slug_post, $post->views, $post->title);
            }

            return $post;
        } catch (\Exception $e) {
            Log::error('http->site->PostController->getPostDetail: lỗi lấy dữ liệu post');

            return redirect('/');
        }
    }

    // send mail cho khách hàng khi mỗi lần lượt view của họ cán mốc 50
    private function sendMail ($slugPost, $views, $title) {
		try {
			$subject = 'Có '.$views.' lượt xem vào website của bạn';

			$content = '<p> Đã có '.$views.' lượt xem vào tin tức <a href="'.route('post', ['post_slug' => $slugPost, 'cate_slug' => 'tin-tuc']).'">'.$title.'</a></p>';

			$content .= '<p>Mau truy cập vào website của bạn để bổ sung thêm thông tin cho tin tức của bạn <a href="'.$this->domainUser->url.'/admin">tại đây</a></p>';

			MailConfig::sendMail($this->emailUser, $subject, $content);
		} catch (\Exception $e) {
			return;
		} 
    }

    private function getCategory($post) {
        try {
            $category = Category::join('category_post', 'categories.category_id', '=', 'category_post.category_id')
                ->select('categories.*')
                ->where('category_post.post_id', $post->post_id)
               ->first();

            if (empty($category)) {
                $category = Category::where('categories.theme_code', $this->themeCode)
                    ->where('categories.user_email', $this->emailUser)->first();
            }

            return $category;
        } catch (\Exception $e) {
            Log::error('http->site->PostController->getPostDetail: lỗi lấy dữ liệu post');

            return redirect('/');
        }
    }

}
