<?php
/**
 * Created by PhpStorm.
 * User: Nam Moma
 * Date: 2/1/2020
 * Time: 8:45 AM
 */

namespace App\Http\Controllers;


use App\Entity\Domain;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class RedirectLoginController extends Controller
{
    public function checkRedirectUser($encrypt){
        $encrypt = Crypt::decrypt($encrypt);
        $domainId = $encrypt['domainId'];
        $userId = $encrypt['userId'];

        // lấy ra domain cần đăng nhập
        $domain = Domain::where('domain_id',$domainId)->first();

        // xóa cookie
        if (isset($_COOKIE['userIdPartner'])) {
            //unset($_COOKIE['userLoginMoma']);
            setcookie ("role", "", time() - 3600);
        }

        $encryptpPartner = Crypt::encrypt([
            'userId' => $userId,
            'random' => rand(1000000, 10000000),
        ]);
        setcookie('userIdPartner',serialize($encryptpPartner), time()+ 3600*365, '/');

        Auth::loginUsingId($domain->user_id);

        return redirect('/admin');
    }
}
