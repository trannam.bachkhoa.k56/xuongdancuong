<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/19/2017
 * Time: 10:21 AM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Category;
use App\Entity\Input;
use App\Entity\Post;
use App\Entity\SocialAccount;
use App\Ultility\Error;
use App\Ultility\Ultility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class QuestionController extends SiteController
{
  public function __construct(){
        parent::__construct();
    }
	
    public function index() {
		
		$userLoginedes = 0 ;
		if (isset($_COOKIE["userFacebook"])) {
			$userLoginedes = $_COOKIE['userFacebook'];
			
			$socialAccount = SocialAccount::where('provider_user_id',$userLoginedes)
			->first();

		}
		
		$postModel = new Post();
        $posts = $postModel->where('template', 'hoi-dap')
        ->where('theme_code', $this->themeCode )
        ->where('user_email', $this->emailUser )
        ->paginate(10);
		
		return view('vn3c.default.question',compact('posts','socialAccount'));
    }
	
	public function detail($slug) {
		$userLoginedes = 0 ;
		if (isset($_COOKIE["userFacebook"])) {
			$userLoginedes = $_COOKIE['userFacebook'];
			
			$socialAccount = SocialAccount::where('provider_user_id',$userLoginedes)
			->first();
		}
		
		try {
			$post = Post::where('slug', $slug)
                ->where('post_type', 'post')
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->first();

            $inputs = Input::where('post_id', $post->post_id)->get();
            foreach ($inputs as $input) {
                $post[$input->type_input_slug] = $input->content;
            }

            Post::where('post_id', $post->post_id)->update([
                'views' => !empty($post->views) ? $post->views + 1 : 1
            ]);

            if ($post->views % 50 == 0) {
                $this->sendMail($slug_post, $post->views, $post->title);
            }
			
			
			return view($this->themeCode.'.template.'.$post->template, compact('post','socialAccount'));

		} catch (\Exception $e) {
            Log::error('http->site->Post->index: loi lay san pham');
            return redirect('/');
        }
    }
	
	public function filter(Request $request) {
		$word = $request->input("word");
		$postModel = new Post();
        $posts = $postModel->where('template', 'hoi-dap')
        ->where('theme_code', $this->themeCode )
        ->where('user_email', $this->emailUser );
       
		if(!empty($request->input('word'))){
			$posts = $posts->where('title','like', '%'.$word.'%');
		}
		$posts = $posts->paginate(10);
		
		return view('vn3c.partials.filter_question',compact('posts') );
    }

    
}
