<?php

namespace App\Http\Controllers\Site;

use App\Entity\User;
use App\Entity\Post;
use Illuminate\Http\Request;
use Yangqi\Htmldom\Htmldom;
use App\Entity\IpClient;
use App\Entity\ViewsWebsite;
use App\Entity\AffiliateMoma;
use App\Ultility\Ultility;
use Symfony\Component\HttpFoundation\Cookie;
use App\Entity\Domain;

class HomeController extends SiteController
{
    public function __construct(){
        parent::__construct();
		
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {	
		if ($this->themeCode == 'kiemtien') {
			$affiliateMoma = AffiliateMoma::where('theme_code', $this->themeCode)
			->where('user_email', $this->emailUser)->first();
	
			$affiliateMomas = AffiliateMoma::join('users', 'users.email', 'affiliate_moma.user_email')
			->join('domains', 'domains.user_id', 'users.id')
			->select (
				'affiliate_moma.*',
				'domains.domain',
				'domains.url'
			)
			->orderBy('affiliate_moma.money', 'desc')
			->orderBy('affiliate_moma.affiliate_moma_id', 'asc')
			->paginate(20);

			foreach ($affiliateMomas as $id => $affiliateMoma) {
				$affiliateMomas[$id]->views = Post::sum('views') / 1000;
			}

			return view('kiemtien.default.index', compact('affiliateMoma', 'affiliateMomas'));
		}

		$domainHttps = [
			'www.banbuonsach.com'
		];
		
		$domainUrl = Ultility::getCurrentDomain();
		
		$nameUrl = Ultility::getCurrentHttpHost();


		// lây ra tất cả user đã từng đăng nhập
		$userLogins = array();
		if (isset($_COOKIE["userLoginMoma"])) {
			$userLoginedes = unserialize($_COOKIE['userLoginMoma'], ["allowed_classes" => false]);
			foreach ($userLoginedes as $userLogined) {
				$domain = Domain::join('users', 'users.id', 'domains.user_id')
				->select('url', 'users.image', 'users.email')
				->where('users.email', $userLogined)->first();
				if (!empty($domain)) {
					$userLogins[] = [
						'url' => $domain->url,
						'email' => $userLogined,
						'image' => $domain->image
					];
				}
				
			}
		}
		if (strpos($nameUrl,"www") === 0) {
			$nameUrl = str_replace("www.","", $nameUrl);

			return redirect('http://'.$nameUrl);
		}
		
        $user = User::where('email', $this->emailUser)->first();

        if (!empty($this->domainUser)) {
            if ( strtotime($this->domainUser->end_at) < time() && ($this->emailUser != 'vn3ctran@gmail.com') && $user->vip > 0) {
                return redirect(route('admin_dateline'));
            }
        }

        if(empty($this->themeCode)){

        	return redirect('http://moma.vn');
        }
			
		if ($request->has('theme_show') ) {
			return view('themes_home.'.$request->input('theme_show'), compact('userLogins'));
		}
		
		//return view($this->themeCode.'.default.builder');
		if (!empty($user->theme_show) ) {
			return view('themes_home.eshop', compact('userLogins'));
		}

		

        return view($this->themeCode.'.default.index', compact('userLogins'));
    }
    public function setCookieGift($id){
        $gift = $id;
        // xóa cookie
        if (isset($_COOKIE['gift'])) {
			//unset($_COOKIE['userLoginMoma']);
			setcookie ("gift", "", time() - 3600);
		}

        // set cookie + thời gian tồn tại        
       	setcookie('gift',serialize($gift), time()+ 3600*365, '/');

        return redirect('/');    
    }
	
	public function updateViewsWebsite (Request $request) {
		try {
			// tinh nguon va view ve tu dau
			$source = $request->has('source') ? $request->input('source') : Ultility::getCurrentDomain();
			
			if (strpos($source, 'admin') > 0) {
				 return response([
                 'status' => 200,
             ])->header('Content-Type', 'text/plain');
			}
			
			$viewsWebsite = ViewsWebsite::where('source', $source)
			->where('theme_code', $this->themeCode)
			->where('user_email', $this->emailUser)
			->whereDay('created_at', '=', date('d'))
			->first();
			
			if (!empty($viewsWebsite)) {
				ViewsWebsite::where('source', $source)
				->where('theme_code', $this->themeCode)
				->where('user_email', $this->emailUser)
				->whereDay('created_at', '=', date('d'))
				->update([
					'count' => ($viewsWebsite->count + 1),
					'updated_at' => new \DateTime()
				]);
				
				return response([
                 'status' => 200,
				])->header('Content-Type', 'text/plain');
			} 
			
			ViewsWebsite::insert([
				'source' => $source,
				'count' => 1,
				'theme_code' => $this->themeCode,
				'user_email' => $this->emailUser,
				'created_at' => new \DateTime()
			]);
			
			 return response([
                 'status' => 200,
             ])->header('Content-Type', 'text/plain');
			 
		} catch(\Exception $e) {
			
			 return response([
                 'status' => 200,
             ])->header('Content-Type', 'text/plain');
		}
	}
//    public function checkDevice()
//    {
//       $detect = new \App\Mobile_Detect;
//
//       if( $detect->isiOS() ){
//        return redirect()->away('https://apps.apple.com/us/app/moma-mart/id1466381952?ls=1');
//       }
//
//       if( $detect->isAndroidOS() ){
//        return redirect()->away('https://play.google.com/store/apps/details?id=com.vn.moma');
//      }
//        return redirect(route('home'));
//   }
    public function downloadAppWithDevice(){
        if((int)$this->check_isMobile() === 1){
            return redirect()->away('https://play.google.com/store/apps/details?id=com.vn.moma');
        }

        if((int)$this->check_isMobile() === 2){
            return redirect()->away('https://apps.apple.com/us/app/moma-mart/id1466381952?ls=1');
        }
        return redirect(route('home'));
    }

    private function check_isMobile()
    {
        if(preg_match('/(iphone|ipad|mac)/i', strtolower($_SERVER['HTTP_USER_AGENT']))){
            return 2;
        }
        if (preg_match('/(android|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
            return 1;

        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']), 'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']))))
            return 1;
        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array('w3c ', 'acs-', 'alav', 'alca', 'amoi', 'andr', 'audi', 'avan', 'benq', 'bird', 'blac', 'blaz', 'brew', 'cell', 'cldc', 'cmd-', 'dang', 'doco', 'eric', 'hipt', 'inno', 'ipaq', 'java', 'jigs', 'kddi', 'keji', 'leno', 'lg-c', 'lg-d', 'lg-g', 'lge-', 'maui', 'maxo', 'midp', 'mits', 'mmef', 'mobi', 'mot-', 'moto', 'mwbp', 'nec-', 'newt', 'noki', 'oper', 'palm', 'pana', 'pant', 'phil', 'play', 'port', 'prox', 'qwap', 'sage', 'sams', 'sany', 'sch-', 'sec-', 'send', 'seri', 'sgh-', 'shar', 'sie-', 'siem', 'smal', 'smar', 'sony', 'sph-', 'symb', 't-mo', 'teli', 'tim-', 'tosh', 'tsm-', 'upg1', 'upsi', 'vk-v', 'voda', 'wap-', 'wapa', 'wapi', 'wapp', 'wapr', 'webc', 'winw', 'winw', 'xda', 'xda-');
        if (in_array($mobile_ua, $mobile_agents))
            return 1;
        if (isset($_SERVER['ALL_HTTP']) && strpos(strtolower($_SERVER['ALL_HTTP']), 'OperaMini') > 0 ) {
            return 1;
        }
        return 0;
	}
	

	public function callback(){
		
		return redirect('https://moma.vn/social/login');
	}

	public function socialLogin(Request $request){
		
		// if($request->has('code')){
			// return redirect(route('get_post_facebook'));
			
		// }
		 return view('vn3c.default.social_login');
	}

}
