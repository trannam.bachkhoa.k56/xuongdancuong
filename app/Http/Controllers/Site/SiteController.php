<?php
namespace App\Http\Controllers\Site;

use App\Entity\ColorWebsite;
use App\Entity\Domain;
use App\Entity\Information;
use App\Entity\IpClient;
use App\Entity\Order;
use App\Entity\CustomerDisplay;
use App\Entity\OrderItem;
use App\Entity\Theme;
use App\Entity\ViewsWebsite;
use App\Entity\TypeInformation;
use App\Entity\Contact;
use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Ultility\InforFacebook;
use App\Ultility\Ultility;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class SiteController extends Controller
{
    protected $themeCode;
    protected $emailUser;
    protected $phoneUser;
    protected $domainUser;

    public function __construct(){
        $domainUrl = Ultility::getCurrentDomain();
		
        $domain = Domain::where('url', $domainUrl)
		->orWhere('domain', $domainUrl)->first();
		
		// lay ra ten mien
		$nameUrl = Ultility::getCurrentHttpHost();
	
		/* if ($nameUrl != 'moma.vn' && $nameUrl != 'taowebsitemienphi.com' && $nameUrl != 'website-mienphi.com' ) {
			$domain = Domain::where('url', 'like', '%'.$nameUrl.'%')
			->orWhere('url', 'like', $nameUrl.'%')
			->first();
		}
		
		$domainHttps = [
			'nhanmavangnam.com'
		];*/
	
		
		/* if (in_array($nameUrl, $domainHttps) && !strpos($domainUrl,"https")) {
			echo $nameUrl;
			header("Location: https://".$nameUrl, true, 301);
			//redirect('https://'.$nameUrl);
		}*/

        $this->domainUser = $domain;

        if (empty($domain)) {
			if ($nameUrl == 'moma.vn') {
				$this->themeCode = 'vn3c';
				$this->emailUser = 'vn3ctran@gmail.com';
				 $this->phoneUser = '0974561735';
			} 
        } else {
            $user = User::where('id', $domain->user_id)->first();
            $theme = Theme::where('theme_id', $domain->theme_id)->first();
            $this->themeCode = $theme->code;
            $this->emailUser = $user->email;
            $this->phoneUser = $user->phone;
        }

        $typeInformations = TypeInformation::orderBy('type_infor_id')
            ->where('theme_code', $this->themeCode)
            ->get();

        // get information
        $informations = Information::where('user_email', $this->emailUser)
            ->where('theme_code', $this->themeCode)->get();

        $informationShow = array();
        foreach($typeInformations as $id => $typeInformation) {
            $typeInformations[$id]['information'] = '';
            foreach ($informations as $information) {
                if ($information->slug_type_input == $typeInformation->slug) {
                    $informationShow[$typeInformation->slug] = $information->content;
                    break;
                }
            }
        }

        // lấy ra màu sắc của trang
        $colorWebsite  = ColorWebsite::where('user_email', $this->emailUser)
            ->where('theme_code', $this->themeCode)->first();

        /*$ip = Ultility::get_client_ip();
        $urlCurrent = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $ipClient = IpClient::where('ip', $ip)
            ->where('user_email', $this->emailUser)
            ->where('theme_code', $this->themeCode)->first();
		
        if (!empty($ipClient)) {
            $timestamp = time() - 60*60;
            $time = date('Y-m-d H:i:s', $timestamp);
            IpClient::where('ip', $ip)
                ->where('updated_at', '<=', $time)
                ->update([
                    'number' => ($ipClient->number + 1),
                    'updated_at' => new \DateTime(),
                    'user_email' => $this->emailUser,
                    'theme_code' => $this->themeCode,
                    'urls' => $ipClient->urls.','.$urlCurrent
                ]);		
				
        } else {
            IpClient::insert([
                'number' => 1,
                'ip' => $ip,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
                'user_email' => $this->emailUser,
                'theme_code' => $this->themeCode,
                'urls' => $urlCurrent
            ]);
        }*/

        //lấy theme code và user email hiện tại 
        $themeCode = $this->themeCode ;
        $emailUser = $this->emailUser;

        //lấy usser id và theme id hiện tại 
        $theme = Theme::where('code',$themeCode)->first();

        $theme_id = isset($theme->theme_id) ? $theme->theme_id : '' ;
        $user = User::where('email',$emailUser)->first();
        $user_id = isset($user->id) ? $user->id : '' ;

        //lấy domain 
        $domain = Domain::where('theme_id', $theme_id)->where('user_id',$user_id)->first();

        //lấy thông tin hiển thị 
        $customerDisplay = CustomerDisplay::where('theme_code',$themeCode)->where('user_email', $emailUser)->first();

        $this->setCookieAffiliate();

        view()->share([
            'information' => $informationShow,
            //'urlLoginFace' => $urlLoginFace,
            'domainUrl' => $domainUrl,
			'userEmail' => $this->emailUser,
			'phoneUser' => $this->phoneUser,
            'colorWebsite' => $colorWebsite,
            'domain' => $domain,
            'customerDisplay' => $customerDisplay
        ]);

    }

	private function setCookieAffiliate () {
        if (!isset($_GET['affiliate_contact'])) {
            return ;
        }

        $affiliateContact = $_GET['affiliate_contact'];
        // xóa cookie
        if (isset($_COOKIE['affiliate_contact'])) {
			//unset($_COOKIE['userLoginMoma']);
			setcookie ("affiliate_contact", "", time() - 3600);
		}

        // set cookie + thời gian tồn tại        
       	setcookie('affiliate_contact',serialize($affiliateContact), time()+ 3600*365, '/');
    }

    private function getUrlLoginFacebook () {
        $app = new InforFacebook();

        $fb = new \Facebook\Facebook([
            'app_id' => $app->getAppId(),
            'app_secret' => $app->getAppSecret(),
            'default_graph_version' => $app->getDefaultGraphVersion()
        ]);

        $helper = $fb->getRedirectLoginHelper();


        $permissions = []; // optional
        $loginUrl = $helper->getLoginUrl('http://vn3c.net/callbacklogin', $permissions);
	
        return $loginUrl;
    }
}
