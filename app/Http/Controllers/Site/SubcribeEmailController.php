<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/7/2017
 * Time: 3:06 PM
 */

namespace App\Http\Controllers\Site;

use App\Entity\SubcribeEmail;
use Illuminate\Http\Request;

class SubcribeEmailController extends SiteController
{
    public function index(Request $request) {
        $email = $request->input('email');

        $isExistEmail = SubcribeEmail::where('email', $email)
            ->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->exists();
        // nếu tồn tại email thì trả ra lỗi email tồn tại
        if ($isExistEmail) {
            return response([
                'status' => 200,
                'message' => 'Email đã tồn tại, vui lòng nhập email khác!'
            ])->header('Content-Type', 'text/plain');

        }

        // nếu email truyền vào là rỗng thì trả ra là bắt buộc nhập email
        if (empty($email)) {
            return response([
                'status' => 200,
                'message' => 'Bạn chưa nhập email, vui lòng nhập email!'
            ])->header('Content-Type', 'text/plain');
        }

        // Nếu đúng thì thêm vào admin
        SubcribeEmail::insert([
            'email' => $email,
            'created_at' => new \DateTime(),
            'theme_code' => $this->themeCode,
            'user_email' => $this->emailUser,
        ]);

        return response([
            'status' => 200,
            'message' => 'Cảm ơn bạn đã đăng ký nhận mail của chúng tôi!'
        ])->header('Content-Type', 'text/plain');
    }

    public function checkSeenEmail(){
        error_reporting(0);
        Header("Content-Type: image/jpeg");

        $ip = $_SERVER['REMOTE_ADDR'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }

        $actual_time = time();
        $actual_day = date('Y.m.d', $actual_time);
        $actual_hour = date('H:i:s', $actual_time);
        $browser = $_SERVER['HTTP_USER_AGENT'];

        $fh = fopen('log.txt', 'a+');
        $stringData = $actual_day . ' ' . $actual_hour . ' ' . $ip . ' ' . $browser . ' ' . "\r\n";
        fwrite($fh, $stringData);
        fclose($fh);
        $newimage = imagecreate(1,1);
        imagejpeg($newimage);
        imagedestroy($newimage);
    }
}
