<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/19/2017
 * Time: 10:23 AM
 */

namespace App\Http\Controllers\Site;


use App\Entity\Category;
use App\Entity\Input;
use App\Entity\MailConfig;
use App\Entity\Post;
use App\Entity\Product;
use App\Ultility\Ultility;
use App\Entity\SettingOrder;
use App\Entity\ViewsWebsite;
use App\Entity\User;
use Carbon\Carbon;
use Faker\Provider\zh_TW\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProductController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index($slug_post, Request $request) {
        // try {
            // if (!empty($this->domainUser)) {
                // if ( strtotime($this->domainUser->end_at) < time() && ($this->emailUser != 'vn3ctran@gmail.com')) {
                    // return redirect(route('admin_dateline'));
                // }
            // }

            $product = $this->getProduct($slug_post);
            $averageRating = $product->avgRating;
            $sumRating = $product->countPositive;

            $categories = $this->getCategories($product);

            // product seen
            $productSeen = Product::saveProductSeen($request, $product);

            // kiểm tra xem phải view quảng cáo không?
            if ($request->has('ads_moma')) {
                // lưu view website
                $this->saveViewWebsiteAds();
            }

            //point
            $inforPoint = $this->getPoint($product);
            $point_price = $inforPoint['pointPrice'];
            $point_deal = $inforPoint['point_detal'];

            if ($product->template == 'default' || empty($product->template)) {
                return view($this->themeCode.'.default.product', compact('product', 'categories', 'productSeen', 'averageRating', 'sumRating', 'point_price','point_deal'));
            } else {
                return view($this->themeCode.'.template.'.$product->template, compact('product', 'categories', 'productSeen', 'averageRating', 'sumRating', 'point_price','point_deal'));
            }
        // } catch (\Exception $e) {
            // Log::error('http->site->ProductController->index: loi lay san pham');
            // return redirect('/');
        // }
    }

    private function saveViewWebsiteAds () {
        // lưu cookie là view từ ads.
        // xóa cookie
        if (isset($_COOKIE['ads_moma'])) {
            //unset($_COOKIE['userLoginMoma']);
            setcookie ("ads_moma", "", time() - 3600);
        }

        // set cookie + thời gian tồn tại
        setcookie('ads_moma', serialize('ads_moma'), time()+ 3600*365, '/');
        
        // tăng views lên 1 đơn vị
        // bước 1: lấy ra views hiện có trong ngày
        $viewWebsite = ViewsWebsite::where('theme_code', $this->themeCode)
		->where('user_email', $this->emailUser)
		->whereDate('created_at', Carbon::today())->first();

        // nếu chưa tồn tại view hôm nay thì thêm mới.
        if (empty($viewWebsite)) {
            ViewsWebsite::insert([
                'count' => 1,
                'created_at' => new \Datetime(),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser
            ]);

            return ;
        }

        // nếu đã tồn tại view rồi thì cộng thêm count.
        ViewsWebsite::where('views_id', $viewWebsite->views_id)->update([
            'count' => ($viewWebsite->count +1),
            'updated_at' => new \Datetime(),
        ]);

        return;
    }

    private function getProduct ($slug_post) {
        try {
            $product = Post::join('products', 'products.post_id','=', 'posts.post_id')
                ->select(
                    'products.price',
                    'products.image_list',
                    'products.discount',
                    'products.price_deal',
                    'products.code',
                    'products.product_id',
                    'products.properties',
                    'products.buy_together',
                    'products.buy_after',
                    'products.discount_start',
                    'products.discount_end',
                    'posts.*'
                )
                ->where('posts.theme_code', $this->themeCode)
                ->where('posts.user_email', $this->emailUser)
                ->where('post_type', 'product')
                ->where('visiable', 0)
                ->where('posts.slug', $slug_post)->first();

            $inputs = Input::where('post_id', $product->post_id)->get();
            foreach ($inputs as $input) {
                $product[$input->type_input_slug] = $input->content;
            }

            Post::where('post_id', $product->post_id)->update([
                'views' => !empty($product->views) ? $product->views + 1 : 1
            ]);

            if ($product->views % 50 == 0) {
                $this->sendMail($slug_post, ($product->views + 1), $product->title);
            }

            return $product;
        } catch (\Exception $e) {
            Log::error('http->site->ProductController->getProduct: lỗi lấy dữ liệu sản phẩm');

            return redirect('/');
        }
    }

    // send mail cho khách hàng khi mỗi lần lượt view của họ cán mốc 50
    private function sendMail ($slugPost, $views, $productName) {
		try {
			$subject = 'Có '.$views.' lượt xem vào website của bạn';

			$content = '<p> Đã có '.$views.' lượt xem vào sản phẩm <a href="'.route('product', ['post_slug' => $slugPost]).'">'.$productName.'</a></p>';

			$content .= '<p>Mau truy cập vào website của bạn để bổ sung thêm thông tin cho sản phẩm của bạn <a href="'.$this->domainUser->url.'/admin">tại đây</a></p>';

			MailConfig::sendMail($this->emailUser, $subject, $content);
		} catch (\Exception $e) {
			return false;
		}  
    }

    private function getCategories ($product) {
        try {
            $categories = Category::join('category_post', 'categories.category_id', '=', 'category_post.category_id')
                ->select('categories.*')
                ->where('categories.theme_code', $this->themeCode)
                ->where('categories.user_email', $this->emailUser)
                ->where('category_post.post_id', $product->post_id)->get();

            return $categories;
        } catch(\Exception $e) {
            Log::error('http->site->ProductController->getCategories: lỗi lấy dữ liệu danh mục');
            return null;
        }
    }

    private function getPoint($product) {
        try {
            $price = $product->price;
            $price_deal = $product->price_deal;

            $settingOrder = SettingOrder::where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)->first();
            if (!empty($settingOrder)) {
                $point_price = $price/$settingOrder->currency_give_point;
                $point_deal = $price_deal/$settingOrder->currency_give_point;
            } else {
                $point_price = 0;
                $point_deal = 0;
            } 

            return [
                'pointPrice' => $point_price,
                'point_detal' => $point_deal
            ];

        } catch (\Exception $e) {
            Log::error('http->site->ProductController ->getPoint');

            return [
                'pointPrice' => 0,
                'point_detal' => 0
            ];
        }
    }
    public function Rating(Request $request){
        $postId = $request->input('postid');
        $rating = $request->input('rating');

        $post = Post::where('post_id', $postId)->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)->first();
        $post->id = $post->post_id;
        $user = User::first();
        $rating = $post->rating([
            'rating' => $rating
        ], $user);
        $averageRating = $post->avgRating;
        $return_arr = array("averageRating"=>$averageRating);

        return response()->json($return_arr);
    }

    public function demoProduct($slug_post, Request $request) {
        try {
            if (!empty($this->domainUser)) {
                if ( strtotime($this->domainUser->end_at) < time() && ($this->emailUser != 'vn3ctran@gmail.com')) {
                    return redirect(route('admin_dateline'));
                }
            }

            $product = $this->getProduct($slug_post);
            $averageRating = $product->avgRating;
            $sumRating = $product->countPositive;

            $categories = $this->getCategories($product);

            // product seen
            $productSeen = Product::saveProductSeen($request, $product);
            //point
            $inforPoint = $this->getPoint($product);
            $point_price = $inforPoint['pointPrice'];
            $point_deal = $inforPoint['point_detal'];

            return view($this->themeCode.'.template.show-product', compact('product', 'categories', 'productSeen', 'averageRating', 'sumRating', 'point_price','point_deal'));
        } catch (\Exception $e) {
            Log::error('http->site->ProductController->index: loi lay san pham');
            return redirect('/');
        }
    }
	
}
