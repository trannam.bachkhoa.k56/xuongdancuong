<?php

namespace App\Http\Controllers\Site;

use App\Entity\Post;
use App\Entity\User;
use App\Mail\Mail;
use App\Ultility\InforFacebook;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Laravel\Socialite\Facades\Socialite;
use App\Entity\Domain;

class LoginController extends SiteController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(){
        return view('admin.login.login');
    }

  public function loginWebsite()
    {
		$userLogins = array();
		if (isset($_COOKIE["userLoginMoma"])) {
			$userLoginedes = unserialize($_COOKIE['userLoginMoma'], ["allowed_classes" => false]);
			foreach ($userLoginedes as $userLogined) {
				$domain = Domain::join('users', 'users.id', 'domains.user_id')
				->select('url', 'users.image', 'users.email')
				->where('users.email', $userLogined)->first();
				if (!empty($domain)) {
					$userLogins[] = [
						'url' => $domain->url,
						'email' => $userLogined,
						'image' => $domain->image
					];
				}
				
			}
		}
        return view('vn3c.default.login', compact('userLogins'));
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {

        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ], [
            'required' => 'Trường :attribute không được để trống'
        ]);
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // kiem tra xem ton tai domain ko
        $user = User::where('email', $request->input('email'))->first();
        if ($user->email == $this->emailUser && $this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        if ($user->theme_code != $this->themeCode || $user->user_email != $this->emailUser ) {
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);

        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

	
    public function callbackLogin(Request $request) {
        $app = new InforFacebook();

        $fb = new \Facebook\Facebook([
            'app_id' => $app->getAppId(),
            'app_secret' => $app->getAppSecret(),
            'default_graph_version' => $app->getDefaultGraphVersion()
        ]);
		
        $helper = $fb->getRedirectLoginHelper();
        if (isset($_GET['state'])) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }
        try {
            $accessToken = $helper->getAccessToken();
			
            if (Auth::check()) {
                User::where('id', Auth::user()->id)->update([
                    'accesstoken' => $accessToken
                ]);
                if ($request->has('current_url')) {
                    return redirect(URL::to('/'.$request->input('current_url')));
                }

                return redirect(URL::to('/'));

            }

            $response = $fb->get('/me', $accessToken);
            $userFacebook = $response->getDecodedBody();
            $user = User::where('email', $userFacebook['id'].'@vn3c.com')->first();
            if (!empty($user)) {
                Auth::login($user);

                return redirect(URL::to('/'));
            }

            $user = new User();
            $user->insert([
                'name' => $userFacebook['name'],
                'role' => 1,
                'email' => $userFacebook['id'].'@vn3c.net',
                'password' => bcrypt($userFacebook['id'].$userFacebook['name']),
                'remember_token' => str_random(10),
            ]);

            $userNew = User::where('email', $userFacebook['id'].'@vn3c.com')->first();
            Auth::login($userNew);

            return redirect(URL::to('/'));

        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
			
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        } catch (\Exception $e ) {
			echo 'error: ' . $e->getMessage(); 
			exit;
		}
    }

    public function redirectToProvider() {
        return Socialite::driver('google')->redirect();
    }
	
    public function handleProviderCallback() {
        $userGoogle = Socialite::driver('google')->user();

        $user = User::where('email',  $userGoogle->getEmail())->first();
        if (!empty($user)) {
            Auth::login($user);

            return redirect(URL::to('/'));
        }

        $user = new User();
        $user->insert([
            'name' => $userGoogle->getName(),
            'role' => 1,
            'email' => $userGoogle->getEmail(),
            'password' => bcrypt(str_random(10)),
            'remember_token' => str_random(10),
        ]);

        $userNew = User::where('email', $userGoogle->getEmail())->first();
        Auth::login($userNew);

        return redirect(URL::to('/'));
    }
	
    public function logout(Request $request) {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
    
	public function getInforLogin(Request $request) {
		$email = $request->input('email');

		if ($email == 'vn3ctran@gmail.com') {
		
			return response([
				'status' => 200,
				'url' => 'https://moma.vn/admin/login/',
			])->header('Content-Type', 'text/plain');
		}
		
		$domain = Domain::join('users', 'users.id', 'domains.user_id')
		->select('url')
		->where('users.email', $email)->first();
		
		$this->checkCookieLogin($request, $email);
		
		if (empty ($domain)) {
			return response([
                'status' => 200,
                'url' => 'https://moma.vn/admin/login/',
            ])->header('Content-Type', 'text/plain');
		}
		
		return response([
                'status' => 200,
                'url' => $domain->url.'/admin/login/',
            ])->header('Content-Type', 'text/plain');
	}
	
	private function checkCookieLogin ($request, $email) {
		$userLoginedes = array();
		
		// check va luu cookie dang nhap hien thi trang chu
		if (isset($_COOKIE['userLoginMoma'])) {
			$userLoginedes = unserialize($_COOKIE['userLoginMoma'], ["allowed_classes" => false]);
			//unset($_COOKIE['userLoginMoma']);
			setcookie ("userLoginMoma", "", time() - 3600);
		}
		if (!empty($userLoginedes) && in_array($email, $userLoginedes) == false) {
			$userLoginedes[] = $email;
			
			setcookie('userLoginMoma', serialize($userLoginedes), time()+ 3600*365, '/');
			return;
		}
		
		if (empty($userLoginedes)) {
			$userLoginedes[] = $email;
			
			setcookie('userLoginMoma', serialize($userLoginedes), time()+ 3600*365, '/');
			return;
		}
		
		setcookie('userLoginMoma', serialize($userLoginedes), time()+ 3600*365, '/');
    }
    
    public function loginFacebook(Request $request) {
        echo 1; exit;
        return view('social.index');
    }
}
