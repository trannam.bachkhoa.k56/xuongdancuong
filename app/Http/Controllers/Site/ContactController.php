<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/28/2017
 * Time: 10:07 PM
 */

namespace App\Http\Controllers\Site;

use App\Entity\Contact;
use App\Entity\CustomerTransaction;
use App\Entity\HistoryPayment;
use App\Entity\Input;
use App\Entity\Order;
use App\Entity\PaymentInfomation;
use App\Entity\Theme;
use App\Entity\NoteContact;
use App\Entity\Domain;
use App\Entity\RemindContact;
use App\Entity\User;
use App\Entity\Product;
use App\Entity\AffiliateMoma;
use App\Entity\OrderCustomerItem;
use App\Entity\CampaignCustomerContact;
use App\Entity\CheckSendMail;
use App\Entity\OrderCustomer;
use App\Entity\CustomerDisplay;
use App\Entity\CustomerIntroduct;
use App\Entity\MailConfig;
use App\Entity\Post;
use App\Mail\Mail;
use App\Ultility\AntiAttackForm;
use Carbon\Carbon;
use Illiminate\Http\Response; 
use Exception;
use App\Entity\Process;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Ultility\CallApi;
use App\Ultility\Ultility;
use App\Entity\SettingGetfly;
// use App\Ultility;

class ContactController extends SiteController
{
    public function __construct(){
        parent::__construct();
    }

    public function index() {
        
        // if (!empty($this->domainUser)) {
            // if ( strtotime($this->domainUser->end_at) < time() && ($this->emailUser != 'vn3ctran@gmail.com')) {
                // return redirect(route('admin_dateline'));
            // }
        // }

         return view($this->themeCode.'.default.contact');
    }
	public function submitMarketing(Request $request) {
		$validation = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|max:255',
        //    'email' => 'required|email',
        ]);

        // if validation fail return error
        if ($validation->fails() && $request->has('is_json')) {
            return response([
                'status' => 200,
                'message' => 'Có lỗi xảy ra, chúng tôi không nhận được thông tin. Bạn vui lòng nhập lại được không!',
            ])->header('Content-Type', 'text/plain');
        } else if($validation->fails()) {
            return redirect(route('contact_home'))
                ->withErrors($validation)
                ->withInput();
        }

        // check ip tấn công
        $antiAttackForm = new AntiAttackForm();
        $antiAttackForm->isUnderAttack();

        //success
        $messages = $request->input('message');

        $contact = Contact::insertGetId([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'address' => $request->input('address'),
            'campaign_id' => $request->input('campaign_id'),
            // 'campaign_status' => $process,
            'message' => is_array($messages) && count($messages) > 1 ? implode('-', $messages) : $messages,
            'theme_code' => 'vn3c',
            'user_email' => 'vn3ctran@gmail.com',
			'utm_source' => $request->has('utm_source') ? $request->input('utm_source') : Ultility::getCurrentLink(),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
		
		$success = 1;
        // $this->sendMainContact($request);

        if($request->has('note')){
            $note = $request->input('note');
            NoteContact::insert([
                'contact_id' => $contact,
                'content' => $note,
                'created_at' => new \DateTime()
            ]);
        }

        //Lưu thông báo 
        $remindContact = RemindContact::insertGetId([
            'date' =>  new \DateTime(),
            'status' => 0, // 0: chưa làm 1: đã làm
            'content' => 'Tư vấn khách hàng ' .$request->input('name') . '(' . $request->input('note') . ')',
            'theme_code' => 'vn3c',
            'user_email' => 'vn3ctran@gmail.com',
            'contact_id' => $contact,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
		
        // Tạo nội dung thông báo 
        $messageSuccess = 'Cảm ơn bạn đã liên hệ cho chúng tôi, chúng tôi sẽ sớm phản hồi sớm nhất.';

        // checkk  nếu có thông báo thì đổi nội dung Thông báo theo khách hàng
        // if(!empty($customer_display)){
            // $messageSuccess = $customer_display->messageChange ;
        // }

        if ($request->has('is_json')) {
            return response([
                'status' => 200,
                'message' => $messageSuccess ,
                'redirect' => $request->input('formRedirect'),

            ])->header('Content-Type', 'text/plain');
        }
	}
	
	

    public function submit(Request $request) {
        // $checkSubmit = AntiFlood::isUnderAttack();

        // if($checkSubmit == true ){
            
        // }
       $post_id = $request->input('post_id');


        $process = null ;
        if(!empty($request->input('campaign_id'))){
            $campaign_status = Process::where('campain_customer_id',$request->input('campaign_id'))
            ->first();
            $process = $campaign_status->process_id;
          
        }

        $themeCode = $this->themeCode ;
        $emailUser = $this->emailUser;
        //lấy usser id và theme id hiện tại 
        $theme = Theme::where('code', $themeCode)->first();

        $theme_id = isset($theme->theme_id) ? $theme->theme_id : '' ;
        $user = User::where('email',$emailUser)->first();
        $user_id = isset($user->id) ? $user->id : '' ;

        //lấy domain 
        $domain = Domain::where('theme_id',$theme_id)->where('user_id',$user_id)->first();

        //lấy thông tin hiển thị 
        $customer_display = CustomerDisplay::where('theme_code',$themeCode)->where('user_email', $emailUser)->first();

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|max:10',
        //    'email' => 'required|email',
        ]);

        // if validation fail return error
        if ($validation->fails() && $request->has('is_json')) {
			
            return response([
                'status' => 200,
                'message' => 'Bạn đã nhập sai định dạng: số điện thoại và tên không được để trống và số điện thoại nhỏ hơn 11 ký tự. ',
            ])->header('Content-Type', 'text/plain');
			
        } else if($validation->fails()) {
            return redirect(route('contact_home'))
                ->withErrors($validation)
                ->withInput();
        }

        // check ip tấn công
        $antiAttackForm = new AntiAttackForm();
        $antiAttackForm->isUnderAttack();

        //success
        $messages = $request->input('message');
        $utmSource = $request->has('utm_source') ? $request->input('utm_source') : Ultility::getCurrentLink();
        if (isset($_COOKIE['ads_moma'])) {
            $utmSource = 'ads_moma';
        }

        $contactModel = new Contact();
        // kiểm tra xem contact tồn tại không
        $contact = $contactModel->where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('phone', $request->input('phone'))
            ->first();
        if (empty($contact)) {
            $contact = $contactModel->insertGetId([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'campaign_id' => $request->input('campaign_id'),
                'campaign_status' => $process,
                'message' => is_array($messages) && count($messages) > 1 ? implode('-', $messages) : $messages,
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'utm_source' => $utmSource,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        } else {
            $contact = $contact->contact_id;
			$contactModel->where('contact_id', $contact)->update([
				'utm_source' => $utmSource
			]);
        }
		
		$campaignCustomerContact = new CampaignCustomerContact();

        if (!empty ($request->input('campaign_id'))) {
            $campaignCustomerContact->insert([
                'campaign_customer_id' => $request->input('campaign_id'),
                'contact_id' => $contact,
                'user_responsibility' => 0,
                'status' => $process,
                'created_at' => new \DateTime()
            ]);
        }
    	
        //Nếu có id tin thì update submit form
        if(!empty($post_id)){
            $this->updateSubmitProduct($post_id);
            //Thêm mới vào order_customer
            $this->addOrderCustomer($request, $contact, $post_id);

        }
      
		// Đồng bộ chiến dịch lên getfly
		SettingGetfly::addNewCampaignGetfly(
			$request->input('name'),
			$request->input('phone'),
			$request->input('email'),
			is_array($messages) && count($messages) > 1 ? implode('-', $messages) : $messages,
			$request->has('utm_source') ? $request->input('utm_source') : Ultility::getCurrentLink(),
			Ultility::getCurrentLink()
		);
		
        $success = 1;
        $this->sendMainContact($request);

        //tạo cookie
        $this->setCookieForm($request);

        // Tạo nội dung thông báo 
        $messageSuccess = 'Cảm ơn bạn đã liên hệ cho chúng tôi, chúng tôi sẽ sớm phản hồi sớm nhất.';

        // checkk  nếu có thông báo thì đổi nội dung Thông báo theo khách hàng
        if(!empty($customer_display) || $customer_display != '' ){
            $messageSuccess = $customer_display->messageChange ;
        }

        if ($request->has('is_json')) {
            return response([
                'status' => 200,
                'message' => $messageSuccess ,
                'redirect' => $request->input('formRedirect'),

            ])->header('Content-Type', 'text/plain');
        }
      
        return view($this->themeCode.'.default.contact', compact('activeMenu', 'success'));
    }

    private function addOrderCustomer($request, $contact, $post_id){

            $product = Product::where('post_id', $post_id)->first();

            $productId = null ;
            if (!empty($product)) {
                $productId = $product->product_id;
            }

            $user = User::where('email', $this->emailUser)->first();

            $affiliateMoney = 0;
            // tính affiliate
            if (isset($_COOKIE["affiliate_contact"])) {
                $affiliateContact = unserialize($_COOKIE['affiliate_contact'], ["allowed_classes" => false]);

                $contactAffiliate = Contact::where('phone', $affiliateContact)
                ->where('theme_code', $this->themeCode)
                ->where('user_email', $this->emailUser)
                ->first();

                if (!empty( $contactAffiliate)) {
                    Contact::where('contact_id', $contact)
                    ->update([
                        'affiliate_person' => $contactAffiliate->contact_id,
                    ]);

					Contact::where('contact_id', $contactAffiliate->contact_id)
                    ->update([
                        'money' => ($contactAffiliate->money + ($request->input('price')*$contactAffiliate->affiliate_money /100) )
                    ]);


                    $affiliateMoney = $contactAffiliate->affiliate_money;
                }
            }

            $orderCustomer = new OrderCustomer();
            $orderCustomerId = $orderCustomer->insertGetId([
                // 'user_id' => $request->input('user_id'),
                'contact_id' => $contact,
                // 'payment' => !empty($request->input('payment')) ? str_replace('.', '', $request->input('payment')) : 0,
                // 'surcharge' => !empty($request->input('surcharge')) ? str_replace('.', '', $request->input('surcharge')) : 0,
                'affiliate_money' => $affiliateMoney,
                'total_price' => $request->input('price'),
                // 'cost' => $request->input('cost_total'),
                'notes' => $request->input('message'),
                'theme_code' => $this->themeCode,
                'user_email' => $this->emailUser,
                'created_at' => new \DateTime()
            ]);

            $orderCustomerItem = new OrderCustomerItem();
            $orderCustomerItem->insert([
                'order_customer_id' => $orderCustomerId,
                'product_id' => $productId,
                'price' => $request->input('price'),
               // 'cost' => str_replace('.', '', $request->cost[$id]),
                'qty' => 1,
                // 'tax' => $request->tax[$id],
                // 'vat' =>  $request->vat[$id],
                'created_at' => new \DateTime()
            ]);
    
    }

    private function setCookieForm(Request $request){
        //set cookie lưu thông tin 
        $cookie = array (
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'address' => $request->input('address'),
        );

        if (isset($_COOKIE['form_value'])) {
            //unset($_COOKIE['userLoginMoma']);
            setcookie ("form_value", "", time() - 3600);
        }
        setcookie('form_value',serialize($cookie), time()+ 3600*365, '/');

    }

    private function updateSubmitProduct($post_id){

        $post = Post::where('post_id', $post_id)
        ->first();

        $countSubmit = $post->submit_form ;

        Post::where('post_id', $post_id)
        ->update([
            'submit_form' => $countSubmit + 1 , 
        ]);

    }
    
    private function sendMainContact($request)  {
		try {
            if (!empty($this->domainUser)) {
                $subject = 'Có liên hệ mới từ website: '.$this->domainUser->name;
                $content = 'Anh '. $request->input('name').' SDT: '.$request->input('phone').' Vừa liên hệ với bạn từ website '.$this->domainUser->name . ' <br> Vui lòng truy cập website '.$this->domainUser->url.' thông tin chi tiết: ';
                $content .= '<p>Số điện thoại: '.$request->input('phone').'. </p>';
                $content .= '<p>Email: '.$request->input('email').'. </p>';
                $content .= '<p>Địa chỉ: '.$request->input('address').'. </p>';
                $content .= '<p>Nội dung: '.$request->input('message').'. </p>';
            }
            else {
                $subject =  'Có liên hệ mới từ website bên phía: VN3C';
                $content = 'Anh '. $request->input('name').' Vừa liên hệ với bạn từ website bên phía VN3C. ';
                $content .= '<p>Số điện thoại: '.$request->input('phone').'. </p>';
                $content .= '<p>Email: '.$request->input('email').'. </p>';
                $content .= '<p>Địa chỉ: '.$request->input('address').'. </p>';
                $content .= '<p>Nội dung: '.$request->input('message').'. </p>';
            }
			
            MailConfig::sendMail('', $subject, $content);
		} catch (\Exception $e) {
			return false;
		}
    }

    public function infoTransactionVnPay() {
        $vnp_SecureHash = $_GET['vnp_SecureHash'];
        $inputData = array();
        foreach ($_GET as $key => $value) {
            if (substr($key, 0, 4) == 'vnp_') {
                $inputData[$key] = $value;
            }
        }
        unset($inputData['vnp_SecureHashType'], $inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . $key . "=" . $value;
            } else {
                $hashData = $hashData . $key . "=" . $value;
                $i = 1;
            }
        }
        $secureHash = hash('sha256',env('VNPAY_HASH_SECRECT') . $hashData);
        $orderId = $_GET['vnp_TxnRef'];
        $money = $_GET['vnp_Amount']/100;
        $content = $_GET['vnp_OrderInfo'];
        $transactionId = $_GET['vnp_TransactionNo'];
        $bankCode = $_GET['vnp_BankCode'];
        $status = 'Chữ ký không hợp lệ';
        $contentArray = explode(' ', $content);
        $month = $contentArray[count($contentArray) - 2];
        $domain = 'http://' . $contentArray[0];
        $type = strpos($content, 'gia han') ? 'Gia hạn' : 'Nâng cấp';
        if ($_GET['vnp_ResponseCode'] != '00') {
            $status = 'THANH TOÁN THẤT BẠI';
            $package = '';
            return view('vn3c.template.info_transaction_vnpay', compact( 'money', 'content', 'transactionId', 'bankCode', 'status', 'domain', 'type', 'package', 'month',  'status'));
        }
        
        if ($secureHash != $vnp_SecureHash) {
            $status = 'THANH TOÁN THẤT BẠI';
            $message = 'Sai chữ kỹ';
             return view('vn3c.template.info_transaction_vnpay', compact( 'status', 'message', 'domain'));
        }

        if ($secureHash == $vnp_SecureHash && $_GET['vnp_ResponseCode'] == '00') {
            $status = 'THANH TOÁN THÀNH CÔNG';
			$this->upgradeDomain($type, $content);
        }

        if (strpos($content, 'Email Marketing')) {
            $package = 'Email Marketing';
            return view('vn3c.template.info_transaction_vnpay', compact('money', 'content', 'transactionId', 'bankCode', 'status', 'domain', 'type', 'package', 'month', 'status'));
        }
        $package = 'Website';
        return view('vn3c.template.info_transaction_vnpay', compact('money', 'content', 'transactionId', 'bankCode', 'status', 'domain', 'type', 'package', 'month', 'status'));
    }

	private function upgradeDomain ($type, $content) {
		try {
			// lấy ra user đang đang nhập.
			$user = User::where('id', explode(';', $content)[1])->first();

			// lấy ra đơn hàng.
			$order = Order::where('user_id', $user->id)
			->orderBy('order_id', 'desc')->first();
			
			if ($order->status != 4 ){
				$customerIntroductModel = new CustomerIntroduct();
				$customerIntroduct = $customerIntroductModel->where('order_id', $order->order_id)->first();
				if (!empty($customerIntroduct)) {
					$this->discountCalculation($customerIntroduct->user_id, $order->order_id, $customerIntroduct->price, $customerIntroduct->user_buy);
                }
                
                if ($order->cost_point == 100) {
                    $affiliateMoma = AffiliateMoma::where('user_email', $user->email)->first();
    
                    AffiliateMoma::where('user_email', $user->email)->update([
                        'money' => ($affiliateMoma->money + $order->total_price),
                        'updated_at' => new \Datetime()
                    ]);
                }

                if ($order->cost_point == 8080) {
                    $paymentInformation = PaymentInfomation::where('user_id', $user->id)->first();

                    if (empty($paymentInformation)) {
                        PaymentInfomation::insert([
                            'user_id' => $user->id,
                            'coin_total' => $order->total_price,
                            'created_at' => new \DateTime()
                        ]);
                    } else {
                        PaymentInfomation::where('user_id', $user->id)->update([
                            'coin_total' => ($paymentInformation->coin_total + $order->total_price),
                            'updated_at' => new \DateTime()
                        ]);
                    }

                    HistoryPayment::insert([
                        'content' => 'Nạp tiền vào tài khoản',
                        'theme_code' => $this->themeCode,
                        'user_email' =>$this->emailUser,
                        'money' => $order->total_price,
                        'created_at' => new \DateTime()
                    ]);
                }
			}

			Order::where('order_id', $order->order_id)
				->update([
				'status' => 4
            ]);

            if ($order->cost_point == 100 || $order->cost_point == 8080) {
                return ;
            }

			
			// bổ sung thời gian sử dụng domain.
			if ($type == 'Nâng cấp') {
				Domain::where('user_id', $user->id)->update([
					'start_at' => new \DateTime(),
					'end_at' => new \DateTime( date('Y-m-d H:i:s', strtotime("+".$order->cost_point." months") ))
				]);
			} 
			
			if ($type == 'Gia hạn') {
				$domain = Domain::where('user_id', $user->id)->first();
				
				Domain::where('user_id', $user->id)->update([
					'start_at' => new \DateTime(),
					'end_at' => new \DateTime( date($domain->end_at , strtotime("+".$order->cost_point." days") ))
				]);
			}
			// cập nhật vip của user.
			User::where('id', $user->id)->update([
				'vip' => 1
			]);

		} catch(\exeption $e) {
			
		}
	}
	
	private function discountCalculation($gift, $orderId, $price, $userId){
        //lấy ra số tiền user đã mua và tính hoa hồng
        // mức tiền max mỗi nấc
		$lever1 = 10000000;
		$lever2 = 20000000;
		$lever3 = 50000000;
		$lever4 = 70000000;
		$lever5 = 100000000;
		$lever6 = 200000000;
		
        //Mức hoa hồng 
		$discountLever1 = 0.23;
		$discountLever2 = 0.25;
		$discountLever3 = 0.27;
		$discountLever4 = 0.29;
		$discountLever5 = 0.30;
		$discountLever6 = 0.35;
		
		//mức Tiền max Theo từng mức
		$priceLever1 = $lever1 * $discountLever1;
		$priceLever2 = $lever2 * $discountLever2;
		$priceLever3 = $lever3 * $discountLever3;
		$priceLever4 = $lever4 * $discountLever4;
		$priceLever5 = $lever5 * $discountLever5;
		
		//Từ 100 -> 200 sẽ là 2.5
		$priceLever6 = $lever6 * $discountLever2;
		
        //Trên 200 sẽ là 3.5

		//lấy thông tin số tiền hiện có 
		$detailUser = User::where('id',$gift)->first();
		$thisCoin = $detailUser->coin;
		$totalCoinIntroduct = $detailUser->total_coin_introduct;
			
		//check khách hàng mua lần 2 thì hoa hồng mức 6
		$userPay = CustomerIntroduct::join('orders', 'orders.order_id', 'customer_introduct.order_id')
		->where('orders.status', 4)
		->where('user_buy',$userId)
		->count();
		
		if($userPay > 0 ){
			//Tính hoa hồng
			$coin = $price * $discountLever6;
			 //Cộng tiền cho user
			$user = User::where('id',$gift)
			->update([
				'total_coin_introduct' => $totalCoinIntroduct + $price,  
				'coin' => $thisCoin + $coin                
			]);
			return ; 
		}
		
		//Tính hoa hồng  
		if($totalCoinIntroduct <= $lever1 ){
			$coin = $price * $discountLever1;
		}
		else if($totalCoinIntroduct > $lever1 || $totalCoinIntroduct <= $lever2){
			$coin = ($price - $lever1) * $discountLever2 +  $priceLever1;
		}
		else if($totalCoinIntroduct > $lever2 || $totalCoinIntroduct <= $lever3){
			$coin = ($price - $lever1 - $lever2) * $discountLever3 +  $priceLever1 + $priceLever2; 
		}
		else if($totalCoinIntroduct > $lever3 || $totalCoinIntroduct <= $lever4){
			$coin = ($price - $lever1 - $lever2 - $lever3) * $discountLever4 +  $priceLever1 + $priceLever2 + $priceLever3; 
		}
		else if($totalCoinIntroduct > $lever4 || $totalCoinIntroduct <= $lever5){
			$coin = ($price - $lever1 - $lever2 - $lever3 -$lever4) * $discountLever5 +  $priceLever1 + $priceLever2 + $priceLever3 + $priceLever4; 
		}
		else if($totalCoinIntroduct > $lever5 || $totalCoinIntroduct <= $lever6){
			$coin = ($price - $lever1 - $lever2 - $lever3 -$lever4 - $lever5) * $discountLever2 +  $priceLever1 + $priceLever2 + $priceLever3 + $priceLever4 + $priceLever5; 
		}
		else if($totalCoinIntroduct > $lever6 ){
			$coin = ($price - $lever1 - $lever2 - $lever3 -$lever4 - $lever5 - $lever6) * $discountLever2 +  $priceLever1 + $priceLever2 + $priceLever3 + $priceLever4 + $priceLever5 + $priceLever6; 
		}

		//Cộng tiền cho user
		$user = User::where('id',$gift)
		->update([
			'total_coin_introduct' => $totalCoinIntroduct + $price,  
			'coin' => $thisCoin + $coin,                
		]);
		
		return $coin;
    }

    public function updateTransactionVnPay() {
        $inputData = array();
        $order = new Order();
        $data = $_REQUEST;
        foreach ($data as $key => $value) {
            if (substr($key, 0, 4) == "vnp_") {
                $inputData[$key] = $value;
            }
        }

        $vnp_SecureHash = $inputData['vnp_SecureHash'];
        unset($inputData['vnp_SecureHashType'], $inputData['vnp_SecureHash']);
        ksort($inputData);
        $i = 0;
        $hashData = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashData = $hashData . '&' . $key . "=" . $value;
            } else {
                $hashData = $hashData . $key . "=" . $value;
                $i = 1;
            }
        }

        $vnpTranId = $inputData['vnp_TransactionNo']; //Mã giao dịch tại VNPAY
        $vnp_BankCode = $inputData['vnp_BankCode']; //Ngân hàng thanh toán
        $vnp_Amount = $inputData['vnp_Amount']; // Số tiền thanh toán
        $content = $inputData['vnp_OrderInfo'];
        $vnp_Amount = (int)$vnp_Amount / 100;
        $secureHash = hash('sha256',env('VNPAY_HASH_SECRECT') . $hashData);
        $status = 0; // Trạng thái chưa thanh toán / chưa hoàn tất
        $orderId = $inputData['vnp_TxnRef'];
        try {
            //Check Orderid
            //Kiểm tra checksum của dữ liệu
            if ($secureHash !==  (string)$vnp_SecureHash) {
                return response()->json([
                    'RspCode' => '97',
                    'Message' => 'Chu ky khong hop le'
                ]);
            }

            //Lấy thông tin đơn hàng lưu trong Database và kiểm tra trạng thái của đơn hàng, mã đơn hàng là: $orderId
            //Việc kiểm tra trạng thái của đơn hàng giúp hệ thống không xử lý trùng lặp, xử lý nhiều lần một giao dịch
            $order = $order->where('method_payment', $orderId)->first();

            // Nếu không có đơn hàng
            if (empty($order)) {
                return response()->json([
                    'RspCode' => '01',
                    'Message' => 'Order not found'
                ]);
            }

            // Nếu đơn hàng không được xác nhận
            if ( !isset($order->status) || (int)$order->status !== 0) {
                return response()->json([
                    'RspCode' => '02',
                    'Message' => 'Order already confirmed'
                ]);
            }

            if ( ! isset($order->total_price) || (int)$order->total_price !== (int)$vnp_Amount ) {
                return response()->json([
                    'RspCode' => '04',
                    'Message' => 'Invalid Amount'
                ]);
            }

            if ((string)$inputData['vnp_ResponseCode'] === '00') {
                $status = 1;
                $contentArray = explode(' ', $content);
                $month = $contentArray[count($contentArray) - 2];
                $domain = 'http://' . $contentArray[0];
                $user = User::getUserByUserId(Domain::getUserIdWithUrl($domain)->user_id);
                $transaction = new CustomerTransaction();
                $transaction->insertData($user->id, $vnp_Amount, $content);
                $this->updateOrder($order->order_id, $status);
                if (strpos($content, 'Email Marketing')) {
                    $this->createOrderEmail($user->id, $vnp_Amount, $month);
                    return response()->json([
                        'RspCode' => '00',
                        'Message' => 'Thanh toán thành công'
                    ]);
                }
                $this->createOrderWebsite($user->id, $domain, $month);
                return response()->json([
                    'RspCode' => '00',
                    'Message' => 'Thanh toán thành công'
                ]);
            }

            $status = -1;
            $this->updateOrder($order->order_id, $status);
            return response()->json([
                'RspCode' => '00',
                'Message' => 'confirm update payment status fail'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'RspCode' => '99',
                'Message' => 'Unknow error'
            ]);
        }
    }

    private function updateOrder($orderId, $status) {
        $order = new Order();
        $order->where('order_id', $orderId)->update([
            'status' => $status,
            'updated_at' => new \DateTime()
        ]);
    }

    private function createOrderEmail($userId, $price, $month) {
        $user = new User();
        if ((int)$price === 300000 || (int)$price === 806000 || (int)$price === 2400000) {
            $user->where('id', $userId)->update([
                'package_mail' => 1,
                'email_remaining' => 5000,
                'time_email' => Carbon::now()->addMonths($month),
                'updated_at' => new \DateTime()
            ]);
            return;
        }

        $user->where('id', $userId)->update([
            'package_mail' => 2,
            'email_remaining' => 20000,
            'time_email' => Carbon::now()->addMonths($month),
            'updated_at' => new \DateTime()
        ]);
    }

    private function createOrderWebsite($userId, $name, $month) {
        $user = new User();
        $domain = new Domain();
        switch ($month) {
            case 24:
                $dateEnd = 36;
                break;
            case 36:
                $dateEnd = 60;
                break;
            default:
                $dateEnd = $month;
                break;
        }

        $user->where('id', $userId)->update([
            'vip' => 2
        ]);

        $domainEnd = $domain->where('url', $name)->first(['end_at']);
        if (isset($domainEnd) && !empty($domainEnd) && strtotime(date('Y-m-d H:i:s')) < strtotime($domainEnd->end_at)) {
            $timeEnd = strtotime($domainEnd->end_at);
            $timeExtension = mktime(date('H', $timeEnd), date('i', $timeEnd), date('s', $timeEnd),
                date('m', $timeEnd) + $dateEnd  , date('d', $timeEnd), date('Y', $timeEnd));
            $timeExtensionString = date('Y-m-d H:i:s', $timeExtension);
            $domain->where('url', $name)->update([
                'end_at' => new \DateTime($timeExtensionString),
                'updated_at' => new \DateTime()
            ]);
            return;
        }
        $domain->where('url', $name)->update([
            'end_at' => Carbon::now()->addMonths($dateEnd),
            'updated_at' => new \DateTime()
        ]);
    }

    public function mailCheck (Request $request) {
        $contactId = $request->input('contact_id');
        $subject = $request->input('subject');
        $email = $request->input('email_manager');

        $noteContact = NoteContact::where('content', "đã đọc $subject")
        ->where('contact_id', $contactId)->first();

        if (!empty($noteContact)) { 
            return false;
        }

        NoteContact::insert ([
            'content' => "đã đọc $subject",
            'contact_id' => $contactId,
            'created_at' => new \Datetime()
        ]);

        CheckSendMail::insert ([
            'receiver' => $contactId,
            'author' => $email,
            'seen_at' => new \DateTime(),
            'time_seen' =>  new \DateTime(),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return ;
    }

    public function affiliatePhone(Request $request) {
        $link = $request->input('link');

        return view ('general.code_phone_affiliate', compact('link'));
    }

    public function searchAffiliate(Request $request) {
        $phone = $request->input('phone');
        
        $contact = Contact::where('theme_code', $this->themeCode)
            ->where('user_email', $this->emailUser)
            ->where('phone',  $phone)->first();

        return view($this->themeCode.'.default.affiliate', compact('contact'));
    }

    public function affiliateCheckPhone () {

        return view ($this->themeCode.'.default.search_affiliate_phone');
    }
}
