<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Storage;

class IpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		/* check ip tấn công
        $contents = Storage::disk('local')->get('ip_attack_five.txt');
		// nếu tấn công quá 4 lần
		$countIp = substr_count($contents, $request->ip());
		if ($countIp > 0) {
			return response()->json(['you dont have permission to access this application.']);
		}*/
		

        return $next($request);
    }
}
