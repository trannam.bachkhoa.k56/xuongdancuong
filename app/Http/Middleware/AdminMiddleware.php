<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Entity\Domain;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::toUser($request->token);
        $domainExist = Domain::join('users', 'users.id', 'domains.user_id')
            ->where('users.email', $request->input('email'))
            ->where('domains.url', $request->input('domain'))
            ->exists();
        if(isset($user)){
            if(($user->role == 4) || $domainExist == true ) {
                return $next($request);
            }
            return response()->json(
                ['error' => 'Bạn không được phép truy cập']
            );
        }
        return response()->json(
            ['error' => 'Bạn chưa đăng nhập'],
            500);
    }
}