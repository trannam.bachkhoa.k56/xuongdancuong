<?php

namespace App\Console\Commands;

use App\Entity\Campaign;
use App\Entity\EmailContactCampaign;
use Cron\DayOfWeekField;
use Illuminate\Console\Command;
use App\Entity\MailConfig;
use App\Entity\User;
use App\Entity\MailAutomation;
use App\Entity\SubcribEmail;
use App\Entity\EmailSetting;
use App\Entity\Contact;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;

class SendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gửi email automation chăm sóc khách hàng';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sendEmail();
        //$this->sendEmailToGroup();
        //$this->resetEmail();
        $this->sendEmailToCampaignCustomer();
    }

    //gửi mail cho khach hàng mới 
    private  function sendEmail(){
        //lấy  tất cả automation có dạng là email (type =1) và dành cho khách hàng mới 
        $emails = MailAutomation::where('type_id',1)
        ->get();

        $userEmails = array();
        foreach ($emails as $email) {
          $userEmails[] = $email->user_email;
        }

        // lấy ra tập khách hàng đã gửi mail
        $emailCampaign = new EmailContactCampaign();
        $emailSendeds = $emailCampaign->whereDate('created_at', Carbon::today())->select('contact_id')->get()->toArray();

        //lấy tất cả khách hàng là khách hàng mới 
        $customers = Contact::whereIn('user_email', $userEmails)
        ->whereNotIn('contact_id',  $emailSendeds)
        ->get();

        //lấy ra ngày thời gian hiện tại 
        $time_now = Carbon::now()->toDateString();
        Carbon::setLocale('vi');

        foreach ($customers as $customer) {
            //lấy ngày tạo của người dùng và xử lý
            $create_time_customer = explode(' ', $customer->updated_at);
            $create_day_customer = explode('-', $create_time_customer[0]);

            foreach ($emails as $id => $email) {
              // Nếu theme_code email != theme_code khách hàng và user_email != user_email của khách hàng  
              if ($email->theme_code != $customer->theme_code || $email->user_email != $customer->user_email) {
                continue;
              }
      
              //Nếu trạng thái khách hàng Khác nhóm của khách hàng 
              if($email->group_id != $customer->status ){
                continue;
              }

              //lấy thời gian tạo khách hàng cộng với số ngày trong automation để lấy thời gian gửi mail
              $year = $create_day_customer[0];
              $month = $create_day_customer[1];
              $day = $create_day_customer[2];

              $date_create = Carbon::create($year, $month, $day)->addDays($email->time_delay)->toDateString();

              //Check thời gian hiện tại và thời gian đã xử lý Nếu không bằng thì thoát
              if($time_now != $date_create){
                continue;
              }

              //gửi mail đến khách hàng 
              MailConfig::send_mail_with_themecode($customer->email,$email->subject, $email->content,$customer->user_email, $customer->contact_id, $customer->user_email);
              
                //$contact = Contact::getContactByEmail($customer->email);
                $campaign = Campaign::getCampaignByGroupId($customer->status);
                $emailCampaign->insert([
                    'contact_id' => $customer->contact_id,
                    'group_id' => $customer->status,
                    'campaign_id' => $campaign->campaign_name,
                    'created_at' => new \DateTime(),
                    'updated_at' =>new \DateTime()
              ]);
          }
        }
    }

    //gửi email theo nhóm khách hàng 
    private  function sendEmailToGroup(){
      //lấy tất cả email 
        $emailToGroups = EmailSetting::select([
            'subject',
            'content',
            'group_id',
            'type',
            'theme_code',
            'user_email',
            'date_send',
            'date_delay',
            'created_at'
        ])
        ->get();

      //lấy tất cả khách hàng ra 
        $customers = Contact::select([
            'email',
            'status',
            'theme_code',
            'user_email',
        ])
        ->get();


         //lấy ra ngày thời gian hiện tại 
        $time_now = Carbon::now()->toDateString();
        Carbon::setLocale('vi');

        foreach ($emailToGroups as $emailToGroup) {
          // lấy thời gian tạo mail hoặc ngày gừi mail 
            $create_time_email = explode(' ', $emailToGroup->created_at);
            $create_day_email = explode('-', $create_time_email[0]);
          foreach ($customers as $id => $customer) {
              // Nếu theme_code email != theme_code khách hàng và user_email != user_email của khách hàng  
                if ($customer->theme_code != $emailToGroup->theme_code || $customer->user_email != $emailToGroup->user_email) {
                continue;
                }
              // nếu liên hệ không có trạng thái Khac với email
              if ($emailToGroup->group_id != $customer->status ){
                continue;
              }

              //Nếu email có hình thức gửi bằng ngày 
              if($emailToGroup->type == 1) {
                  if($emailToGroup->date_send == $time_now){
                      MailConfig::send_mail_with_themecode($customer->email,$emailToGroup->subject, $emailToGroup->content,$customer->user_email);
                      $emailCampaign = new EmailContactCampaign();
                      $contact = Contact::getContactByEmail($customer->email);
                      $campaign = Campaign::getCampaignByGroupId($customer->status);
                      $emailCampaign->insert([
                          'contact_id' => $contact->contact_id,
                          'group_id' => $contact->status,
                          'campaign_id' => $campaign->campaign_name,
                          'created_at' => new \DateTime(),
                          'updated_at' =>new \DateTime()
                      ]);
                  }
              }

              //Nếu hình thức bằng độ trễ thì xử lý thời gian nhập vào
              if ($emailToGroup->type == 2){
                $date_create = Carbon::create($create_day_email[0], $create_day_email[1], $create_day_email[2])->addDays($emailToGroup->date_delay)->toDateString();
                //Nếu thời gian hiện tại bằng thời gian đã xử lý 
                  if($time_now == $date_create){
                        //print_r($emailToGroup->subject);
                      MailConfig::send_mail_with_themecode($customer->email,$emailToGroup->subject, $emailToGroup->content,$customer->user_email);
                      $emailCampaign = new EmailContactCampaign();
                      $contact = Contact::getContactByEmail($customer->email);
                      $campaign = Campaign::getCampaignByGroupId($customer->status);
                      $emailCampaign->insert([
                          'contact_id' => $contact->contact_id,
                          'group_id' => $contact->status,
                          'campaign_id' => $campaign->campaign_name,
                          'created_at' => new \DateTime(),
                          'updated_at' =>new \DateTime()
                      ]);
                    }
              }
          }
        }
    }

    //Gửi mail theo chiến dịch khách hàng
    private  function sendEmailToCampaignCustomer(){
      //lấy tất cả email 

        $emailToGroups = MailAutomation::select([
            'group_id',
            'type_id',
            'time_delay',
            'campaign_id',
            'campaign_customer_id',
            'process_id',
            'subject',
            'content',
            'theme_code',
            'user_email',
            'created_at',
        ])
        ->get();

        $userEmails = array();
        foreach ($emailToGroups as $email) {
          $userEmails[] = $email->user_email;
        }

        //lấy tất cả khách hàng ra 
        $customers = Contact::whereIn('user_email', $userEmails)->select([
            'email',
            'campaign_id',
            'campaign_status',            
            'theme_code',
            'user_email',
        ])
        ->get();

         //lấy ra ngày thời gian hiện tại 
        $time_now = Carbon::now()->toDateString();
        Carbon::setLocale('vi');

        foreach ($emailToGroups as $emailToGroup) {
          // lấy thời gian tạo mail hoặc ngày gừi mail 
            $create_time_email = explode(' ', $emailToGroup->created_at);
            $create_day_email = explode('-', $create_time_email[0]);
          foreach ($customers as $id => $customer) {
            // Nếu theme_code email != theme_code khách hàng và user_email != user_email của khách hàng  
              if ($customer->theme_code != $emailToGroup->theme_code || $customer->user_email != $emailToGroup->user_email) {
              continue;
              }

            // nếu liên hệ không có trạng thái Khac với email
             if ($emailToGroup->campaign_id != $customer->campaign_id || $emailToGroup->process_id != $customer->process_id ){
              continue;
             }

            //Nếu email có hình thức gửi bằng ngày 
              // if($emailToGroup->type == 1){
              //     if($emailToGroup->date_send == $time_now){
              //        MailConfig::send_mail_with_themecode($customer->email,$emailToGroup->subject, $emailToGroup->content,$customer->user_email);
              //        $emailCampaign = new EmailContactCampaign();
              //        $contact = Contact::getContactByEmail($customer->email);
              //        $campaign = Campaign::getCampaignByGroupId($customer->status);
              //        $emailCampaign->insert([
              //            'contact_id' => $contact->contact_id,
              //            'group_id' => $contact->status,
              //            'campaign_id' => $campaign->campaign_name,
              //            'created_at' => new \DateTime(),
              //            'updated_at' =>new \DateTime()
              //        ]);
              //     }
              // }

              //Nếu hình thức bằng độ trễ thì xử lý thời gian nhập vào
              if ($emailToGroup->type_id == 1){
                $date_send = Carbon::create($create_day_email[0], $create_day_email[1], $create_day_email[2])->addDays($emailToGroup->time_delay)->toDateString();
                //Nếu thời gian hiện tại bằng thời gian đã xử lý 
                  if($time_now == $date_send){
                     
                      MailConfig::send_mail_with_themecode($customer->email,$emailToGroup->subject, $emailToGroup->content,$customer->user_email);
                      $emailCampaign = new EmailContactCampaign();
                      $contact = Contact::getContactByEmail($customer->email);
                      // $campaign = Campaign::getCampaignByGroupId($customer->status);
                      $emailCampaign->insert([
                          'contact_id' => $contact->contact_id,
                          'campaign_customer_id' => $customer->campaign_id,
                          'process_id' => $customer->campaign_status,
                          'created_at' => new \DateTime(),
                          'updated_at' =>new \DateTime()
                      ]);
                    }
              }
          }
        }
        exit();
    }

     private function resetEmail(){
        // check thời gian ngày hôm nay và xử lý
          $userModel = new User();
          $date_now = Carbon::now()->toDateString();
          $time = explode('-', $date_now);
          $day_now = $time[2];
          $users = $userModel->get();
          
          foreach ($users as $user) {
                //nếu thời gian bây giờ = m1 reset email
                if($day_now == 1){
                      if($user->id != 1){
                        $userUpdate = $userModel->where('id',$user->id)
                        ->update(['email_remaining' => 50 ]);    
                      }
                }
                //Nếu thời gian = null thì tiếp tục  
                if($user->time_email == null){
                  continue ;
                }
                //Nếu thời gian hiện tại > hạn dùng email thì tiếp tục 
                if($user->time_email <= $date_now){
                  continue ;  
                }
                if($user->package_mail == 0 || $user->package_mail == null ) {
                  continue ;
                }
                //print_r($user);
               
                if($user->package_mail == 1 ){
                  $user = $userModel->where('id',$user->id)
                  ->update(['email_remaining' =>  5000 ]);
                  continue ;
                }
                if($user->package_mail == 2 ){
                  $user = $userModel->where('id',$user->id)
                  ->update(['email_remaining' => 20000 ]);
                  continue ;
                }
          }  
    } 
     

}
