<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 1/3/2019
 * Time: 9:10 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class EmailAutomation extends Model
{
    protected $table = 'email_automation';

    protected $primaryKey = 'email_automation_id';

    protected $fillable = [
        'email_automation_id',
        'message',
        'day',
        'month',
        'theme_code',
        'email_user',
        'date_special',
        'created_at',
        'updated_at'
    ];
}