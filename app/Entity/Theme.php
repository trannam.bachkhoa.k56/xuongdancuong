<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Theme extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'themes';

    protected $primaryKey = 'theme_id';

    protected $fillable = [
        'theme_id',
        'group_id',
        'name',
        'code',
        'image',
        'price',
        'image_phone',
        'url_test',
        'description',
        'is_sale',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public static function getAllTheme() {
        try {
            $themeModel = new Theme();

            $themes = $themeModel->orderBy('theme_id', 'desc')->get();

            return $themes;
        } catch (\Exception $e) {
            Log::error('Entity->Theme->getAllTheme: lấy tất cả theme ra hiển thị');

            return null;
        }
    }

    public function createTheme($themeCode, $email) {
        // nhân dữ liệu information
        $this->copyInformation($themeCode, $email);
        // nhân đôi menu
        //$this->copyMenu($themeCode, $email);
        // copy Danh mục
        //$this->copyCategory($themeCode, $email);
        // copy Bài viết không có danh mục
        //$this->CoppyPostNotHaveCate($themeCode, $email);
        // copy Trang
        $this->copyPage($themeCode, $email);

        $this->copySubpost($themeCode, $email);
    }

    private function copyInformation ($themeCode, $email) {
        $information = new Information();
        $infors = $information->where('theme_code', $themeCode)
            ->where('user_email', 'vn3ctran@gmail.com')->get();

        foreach ($infors as $infor) {
            $informationExist = $information->where('slug_type_input', $infor->slug_type_input)
                ->where('theme_code', $themeCode)
                ->where('user_email', $email)->exists();
            if (!$informationExist) {
                $information->insert([
                    'slug_type_input' => $infor->slug_type_input,
                    'content' => $infor->content,
                    'user_email' => $email,
                    'theme_code' => $themeCode,
                    'updated_at' => new \DateTime(),
                ]);
            }
        }
    }

    private function copyMenu($themeCode, $email) {
        $menu = new Menu();

        $menus = $menu->where('theme_code', $themeCode)
            ->where('user_email', 'vn3ctran@gmail.com')->get();

        foreach ($menus as $menuOld) {
            $menuExist = $menu->where('theme_code', $themeCode)
                ->where('user_email', $email)
				->where('slug', $menuOld->slug)->exists();
            if (!$menuExist) {
                $menu->insert([
                    'title' => $menuOld->title,
                    'slug' => $menuOld->slug,
                    'location' => $menuOld->location,
                    'image' => $menuOld->image,
                    'user_email' => $email,
                    'theme_code' => $themeCode
                ]);
                $menuElementModel = new MenuElement();
                $menuElements = $menuElementModel->where('menu_slug', $menuOld->slug)->where('theme_code', $themeCode)
                    ->where('user_email', 'vn3ctran@gmail.com')->get();
                foreach ($menuElements as $menuElement) {
                    $menuElementModel->insert([
                        'menu_slug' => $menuOld->slug,
                        'url' => $menuElement->url,
                        'title_show' => $menuElement->title_show,
                        'user_email' => $email,
                        'theme_code' => $themeCode,
                        'menu_level' => $menuElement->menu_level,
                        'menu_image' => $menuElement->menu_image
                    ]);
                }

            }
        }

    }

    private function copyCategory($themeCode, $email) {
        $category = new Category();

        $categories = $category->orderBy('category_id', 'asc')
            ->where('theme_code', $themeCode)
            ->where('user_email', 'vn3ctran@gmail.com')->get();

        $category = new Category();
        foreach ($categories as $cate) {
            $categoryExist = $category->where('theme_code', $themeCode)
                ->where('user_email', $email)
				->where('slug', $cate->slug)->exists();
            if ($categoryExist) {
                break;
            }

            $parentOld = $category->where('category_id', $cate->parent)
                ->where('theme_code', $themeCode)
                ->where('user_email', 'vn3ctran@gmail.com')
                ->first();
            $parentId = 0;
            if (!empty($parent)) {
                $parent = $category->where('theme_code', $themeCode)
                    ->where('user_email', $email)
					->where('slug', $parentOld->slug)->first();
                $parentId = $parent->category_id;
            }
            // insert category
            $cateId = $category->insertGetId([
                'title' => $cate->title,
                'slug' => $cate->slug,
                'description' => $cate->description,
                'image' => $cate->image,
                'template' => $cate->template,
                'parent' => $parentId,
                'post_type' => $cate->post_type,
                'is_hide' => $cate->is_hide,
                'user_email' => $email,
                'theme_code' => $themeCode,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);

        }
    }

    private function copyPage($themeCode, $email) {
        $postModel = new Post();

        $posts = $postModel->where('post_type', 'page')
            ->where('theme_code', $themeCode)
            ->where('user_email', 'vn3ctran@gmail.com')
            ->get();

        $postModel = new Post();
        foreach ($posts as $post) {
            $postExist = $postModel->where('theme_code', $themeCode)
                ->where('user_email', $email)
				->where('slug', $post->slug)
                ->exists();

            if ($postExist) {
                break;
            }

            $postModel->insert([
                'title' => $post->title,
                'slug' => $post->slug,
                'description' => $post->description,
                'tags' => $post->tags,
                'content' => $post->content,
                'template' => $post->template,
                'image' => $post->image,
                'user_email' => $email,
                'theme_code' => $themeCode,
                'post_type' => $post->post_type,
                'visiable' => 0,
                'meta_title' => $post->meta_title,
                'meta_description' => $post->meta_description,
                'meta_keyword' => $post->meta_keyword,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);
        }
    }

    private function copySubpost($themeCode, $email) {
        $postModel = new Post();
        $subPostModel = new SubPost();
        $subPosts = $subPostModel->where('theme_code', $themeCode)
            ->where('user_email', 'vn3ctran@gmail.com')
            ->get();

        $postModel = new Post();

        foreach ($subPosts as $subPost) {
            $post = $postModel->where('post_id', $subPost->post_id)
                ->where('theme_code', $themeCode)
                ->where('user_email', 'vn3ctran@gmail.com')->first();

            if (!empty($post)) {
                $postId = $postModel->insertGetId([
                    'title' => $post->title,
                    'slug' => $post->slug,
                    'description' => $post->description,
                    'tags' => $post->tags,
                    'content' => $post->content,
                    'template' => $post->template,
                    'image' => $post->image,
                    'user_email' => $email,
                    'theme_code' => $themeCode,
                    'post_type' => $post->post_type,
                    'visiable' => 0,
                    'meta_title' => $post->meta_title,
                    'meta_description' => $post->meta_description,
                    'meta_keyword' => $post->meta_keyword,
                    'created_at' => new \DateTime(),
                    'updated_at' => new \DateTime(),
                ]);

                $subPostModel->insert([
                    'post_id' => $postId,
                    'type_sub_post_slug' => $subPost->type_sub_post_slug,
                    'user_email' => $email,
                    'theme_code' => $themeCode,
                ]);
            }
        }
    }

    private function CoppyPostNotHaveCate($themeCode, $email) {
        $categoryPostModel = new CategoryPost();
        $postModel = new Post();
        $productModel = new Product();
        $subPostModel = new SubPost();
        $inputModel = new Input();
        $postIdHaveCates = $categoryPostModel->select('post_id')
            ->where('theme_code', $themeCode)
            ->where('user_email', 'vn3ctran@gmail.com')->get();

        $postOlds = $postModel->whereNotIn('post_id', $postIdHaveCates)
            ->where('theme_code', $themeCode)
            ->where('user_email', 'vn3ctran@gmail.com')->get();

        // tìm ra những post mà không có danh mục
        $postIdNotHaveCate = array();
        foreach ($postOlds as $post) {
            $postIdNotHaveCate[] = $post->post_id;
        }
        $productOlds = $productModel->whereIn('post_id', $postIdNotHaveCate)
            ->get();
        $subPosts  = $subPostModel->whereIn('post_id', $postIdNotHaveCate)
            ->get();
        $inputOlds = $inputModel->whereIn('post_id', $postIdNotHaveCate)
            ->get();
        $subPostIdArrayOlds = array();
        $productIdArrayOlds = array();
        $inputArrayOlds = array();
        foreach ($inputOlds as $input) {
            $inputArrayOlds[$input->post_id][] = $input;
        }
        foreach ($productOlds as $product) {
            $productIdArrayOlds[$product->post_id] = $product;
        }

        foreach ($subPosts as $subPost) {
            $subPostIdArrayOlds[$subPost->post_id] = $subPost;
        }
        // insert post
        foreach ($postOlds as $post) {
            // kiểm tra bài viết đã tồn tại chưa
            $postExist = $postModel->where('theme_code', $themeCode)
                ->where('user_email', $email)
				->where('slug', $post->slug)->exists();
            if ($postExist) {
                break;
            }

            $postId = $postModel->insertGetId([
                'title' => $post->title,
                'description' => $post->description,
                'content' => $post->content,
                'template' => $post->template,
                'tags' => $post->tags,
                'slug' => $post->slug,
                'is_hide' => $post->is_hide,
                'image' => $post->image,
                'post_type' => $post->post_type,
                'visiable' => $post->visiable,
                'category_string' => $post->category_string,
                'meta_title' => $post->meta_title,
                'meta_description' => $post->meta_description,
                'meta_keyword' => $post->meta_keyword,
                'user_email' => $email,
                'theme_code' => $themeCode,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime(),
            ]);

            // insert SubPost
            if (isset($subPostIdArrayOlds[$post->post_id])) {
                $subPostModel->insert([
                    'post_id' => $postId,
                    'type_sub_post_slug' => $subPostIdArrayOlds[$post->post_id]->type_sub_post_slug,
                    'user_email' => $email,
                    'theme_code' => $themeCode,
                ]);
            }
            //insert input
            if (isset($inputArrayOlds[$post->post_id])) {
                foreach ($inputArrayOlds[$post->post_id] as $input) {
                    $inputModel->insert([
                        'type_input_slug' => $input->type_input_slug,
                        'content' => $input->content,
                        'post_id' =>$postId,
                        'user_email' => $email,
                        'theme_code' => $themeCode,
                    ]);
                }
            }
            // insert product
            if (isset($productIdArrayOlds[$post->post_id])) {
                $productModel->insert([
                    'code' => $productIdArrayOlds[$post->post_id]->code,
                    'price' => $productIdArrayOlds[$post->post_id]->price,
                    'discount' => $productIdArrayOlds[$post->post_id]->discount,
                    'image_list' => $productIdArrayOlds[$post->post_id]->image_list,
                    'properties' => $productIdArrayOlds[$post->post_id]->properties,
                    'buy_together' => $productIdArrayOlds[$post->post_id]->buy_together,
                    'buy_after' => $productIdArrayOlds[$post->post_id]->buy_after,
                    'post_id' => $postId
                ]);
            }

        }
    }
}
