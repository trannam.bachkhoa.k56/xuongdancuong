<?php
/**
 * Created by PhpStorm.
 * User: Nam Moma
 * Date: 1/23/2020
 * Time: 3:53 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderCustomerItem extends Model
{
    protected $table = 'order_customer_item';

    protected $primaryKey = 'order_customer_item';

    protected $fillable = [
        'order_customer_item_id',
        'order_customer_id',
        'product_id',
        'price',
        'cost',
        'qty',
        'tax',
        'vat',
        'created_at',
        'updated_at'
    ];
}
