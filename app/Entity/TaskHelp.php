<?php


namespace App\Entity;


use Illuminate\Database\Eloquent\Model;

class TaskHelp extends Model
{
    protected $table = 'task_help';

    protected $primaryKey = 'task_help_id';

    protected $fillable = [
        'task_help_id',
        'user_id',
        'post_id',
        'type',
        'created_at'
    ];
}