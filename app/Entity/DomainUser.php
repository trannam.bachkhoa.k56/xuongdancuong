<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class DomainUser extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'domain_user';

    protected $primaryKey = 'domain_user_id';

    protected $fillable = [
        'domain_user_id',
        'domain_id',
        'user_id',
        'commission_id',
        'role',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at'
    ];

    public static function getUser($user_id){
        $domain = Domain::where('user_id', $user_id)
        ->first();
        return static::leftJoin('users','users.id','domain_user.user_id')
            ->leftJoin('commission', 'commission.commission_id', 'domain_user.commission_id')
        ->where('domain_user.domain_id', $domain->domain_id)
        ->select([
            'domain_user.domain_user_id',
            'users.id',
            'users.name',
            'users.email',
            'users.phone',
            'domain_user.role',
            'users.image',
            'domain_user.commission_id',
            'commission.position'
        ])
        ->get();
    }

    public static function getDomain($user_id){

        $domain = Domain::leftJoin('domain_user','domain_user.domain_id','domains.domain_id')
        ->where('domain_user.user_id', $user_id)
        ->get();

        return  $domain;
    }

    public static function getEmployees ($domainId) {
        return static::join('users', 'users.id', 'domain_user.user_id')
            ->select('users.*')
            ->where('domain_id', $domainId)
            ->get();
    }
}
