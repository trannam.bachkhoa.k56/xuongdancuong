<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class AffiliateMoma extends Model
{
    protected $table = 'affiliate_moma';

    protected $primaryKey = 'affiliate_moma_id';

    protected $fillable = [
        'affiliate_moma_id',
        'title',
        'link',
        'content_deal',
        'affiliate',
        'money',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at'
    ];

}
