<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/1/2017
 * Time: 2:14 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class EmailTemplate extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'email_template';

    protected $primaryKey = 'template_id';

    protected $fillable = [
        'template_id',
        'template_title',
        'content',
        'group_customer',
        'created_at',
        'updated_at'
    ];

    public static function getNewCustomer(){
        $template = static::where('group_customer', 0)->get();
        return $template;
    }
    public static function getApproachCustomer(){
        $template = static::where('group_customer', 1)->get();
        return $template;
    }
    public static function getBuyCustomer(){
        $template = static::where('group_customer', 2)->get();
        return $template;
    }
    public static function getReturnCustomer(){
        $template = static::where('group_customer', 3)->get();
        return $template;
    }

}
