<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 1/3/2019
 * Time: 9:10 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class MailAutomation extends Model
{
    protected $table = 'email_automation';

    protected $primaryKey = 'mail_id';

    protected $fillable = [
       	'mail_id',
		'group_id',
		'type_id',
		'time_delay',
		'campaign_id',
        'campaign_customer_id',
        'process_id',
		'subject',
		'content',
		'theme_code',
		'user_email',
		'updated_at',
		'created_at',
    ];
    public static function getAllWithId($campaign_id){

    	 $emails = static::where('campaign_id',$campaign_id)
    	// ->where('theme_code',$this->themeCode)
     //    ->where('user_email',$this->emailUser)
        ->paginate(5);
        
        return $emails;
    }

    public static function getAllWithCampaignId($campaign_customer_id){

        $emails = static::leftJoin('process','process.process_id','email_automation.process_id')
        ->where('email_automation.campaign_customer_id',$campaign_customer_id) 
        ->orderBy('process.process_id')       
        ->paginate(10);
        
        return $emails;

    }
}