<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Goal extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'goals';

    protected $primaryKey = 'goal_id';

    protected $fillable = [
        'goal_id',
        'title',
        'user_id',
        'revenue',
        'customers',
        'orders',
        'notes',
        'purchases',
        'reminders',
        'calls',
        'month',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at',
    ];

    public static function getGoalWithUserIdMonth ($userId, $month, $year) {
        return static::where('user_id', $userId)
            ->where('theme_code', ModelParent::getThemeCode())
            ->where('user_email', ModelParent::getEmailUser())
            ->whereMonth('month', $month)
            ->whereYear('month', $year)
            ->first();
    }
}
