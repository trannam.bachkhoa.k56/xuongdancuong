<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Opportunity extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'opportunity';

    protected $primaryKey = 'opportunity_id';

    protected $fillable = [
        'opportunity_id',
        'user_id',
        'contact_id',
        'status',
        'created_at',
        'updated_at'
    ];

   
}
