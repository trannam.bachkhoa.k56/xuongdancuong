<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class CampaignCustomerUser extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'campaign_cus_user';

    protected $primaryKey = 'campaign_cus_user_id';

    protected $fillable = [
        'campaign_cus_user_id',
        'campaign_cus_id',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public static function getAll(){
        $campaign = static::get();
        return $campaign ;
    }

    public static function getUserWithCampaignCusId($id){
       return static::leftJoin('users','users.id','campaign_cus_user.user_id')
       ->select(
        'users.name',
        'users.id'
        )
       ->where('campaign_cus_user.campaign_cus_id',$id)
       ->get();
    }

}
