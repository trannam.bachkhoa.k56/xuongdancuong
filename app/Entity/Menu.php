<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Menu extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'menus';

    protected $primaryKey = 'menu_id';

    protected $fillable = [
        'menu_id',
        'title',
        'slug',
        'location',
        'image',
        'user_email',
        'theme_code',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    // hiển thị tiêu đề menu
    public static function showTitleMenu($slug) {
        try {
            $menuModel = new Menu();

            $menu = $menuModel->where('slug', $slug)
                ->where('theme_code', ModelParent::getThemeCode())
                ->where('user_email', ModelParent::getEmailUser())->first();


            return $menu->title;
        } catch (\Exception $e) {
            Log::error('Entity->Menu->showTitleMenu: hiển thị tiêu đề menu');

            return null;
        }
    }

    public static function showTitleMenu2($slug) {
        $menuModel = new Menu();

        $menu = $menuModel->where('slug', $slug)->first();

        return $menu->title;
    }
    //hiển thị vị trí menu
    public static function showWithLocation($slug) {
        try {
            $menuModel = new Menu();

            $menus = $menuModel->orderBy('menu_id')->where('location', $slug)
                ->where('theme_code', ModelParent::getThemeCode())
                ->where('user_email', ModelParent::getEmailUser())->get();

            return $menus;
        } catch(\Exception $e) {
            Log::error('Entity->Menu->showWithLocation: hiển thị vị trí menu');

            return null;
        }
    }

}
