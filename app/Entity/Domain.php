<?php

namespace App\Entity;

use App\Http\Controllers\Admin\DomainController;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\Datatables\Datatables;

class Domain extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];


    protected $table = 'domains';

    protected $primaryKey = 'domain_id';

    protected $fillable = [
        'domain_id',
        'name',
        'url',
        'domain',
        'action',
        'change_url',
        'start_at',
        'end_at',
        'description',
        'show_advice_setting',
        'theme_id',
        'user_id',
        'time_on',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    
    public static function showDomainWithUser () {
        // Không có user đăng nhập
        if (!Auth::check()) {
            return null;
        }

        $user = Auth::user();
//        // quyền của user không được phép
//        if (!User::isManager($user->role)) {
//            return null;
//        }
        $domainModel =  new Domain();

        $domains = $domainModel->where('user_id', $user->id)
            ->orderBy('domain_id', 'desc')->get();

        return $domains;
    }

    public static function countDomainWithUser ($userID) {
        try {
            return static::where('user_id', $userID)
                ->orderBy('domain_id', 'desc')->count();

        } catch (\Exception $e) {
            Log::error('Entity->Domain->countDomainWithUser: Lỗi tính tổng user của domain');

            return 0;
        }

    }

    public static function getUlrWithUserId($id){

        $domainModel =  new Domain();

        $domain = $domainModel->where('user_id', $id)
        ->get();

        return $domain;
    }

    public static function getUserIdWithUrl($url){
        $domain = new self();
        return $domain->where('url', $url)->first(['user_id']);
    }
	
	public static function getUserIdWithUrlLike($url){
        $domain = new self();
        return $domain->where('url','like', '%'.$url)->first(['user_id']);
    }



    public static function countFilterDomain($type)
    {
        $domain = static::leftJoin('users', 'users.id', 'domains.user_id')
            ->leftJoin('themes', 'themes.theme_id', 'domains.theme_id');

        switch ($type) {
            case 1:
                # code...Mới tạo website
                return static::count();
                break;
            case 2:
                # code...Nhập sản phẩm
                 return $domain->join('posts', 'posts.user_email', 'users.email')
                 ->where('posts.post_type', 'product')
                 ->distinct()->pluck('domains.domain_id')->count();

                break;
            case 3:
                # code...Nhập tin tức

                return $domain->join('posts', 'posts.user_email', 'users.email')
                    ->where('posts.post_type', 'post')
                    ->groupBy('domains.domain_id')
					->distinct()
                    ->pluck('domains.domain_id')->count();
                break;
            case 4:
                # code...Đã có khách hàng
                return $domain->join('contact','contact.user_email', 'users.email')
                    ->groupBy('domains.domain_id')
                    ->distinct()->pluck('domains.domain_id')->count();

                break;
            case 5:
                # code...Đã có đơn hàng
                return $domain->join('order_customer','order_customer.user_email', 'users.email')
                    ->groupBy('domains.domain_id')
                    ->distinct()->pluck('domains.domain_id')->count();

                break;
            case 6:
                # code...Đang online
                return $domain->where('domains.time_on','>',Carbon::now('Asia/Ho_Chi_Minh')->subMinute(5))
                    ->where('domains.time_on','<',Carbon::now('Asia/Ho_Chi_Minh')->addMinute(5))
                    ->whereNotIn('domains.user_id',[1])
                    ->count('domains.domain_id');
                break;

            default:
                # code...
                break;
        }

    }

    public static function showDomainCustomer($number = 10){
        /*return $domain = Domain::leftJoin('users', 'users.id', 'domains.user_id')
            ->leftJoin('information', 'information.user_email', 'users.email')
            ->whereNotIn('users.id', [1])
            ->where('domains.url', 'like', '%moma.vn%') 
            ->where('information.updated_at', '!=', null)        
            ->select('domains.*', DB::raw('COUNT(information.infor_id) as countInfor'),'users.email', 'users.phone','information.content as logo','information.slug_type_input')
            ->groupBy('domains.domain_id')
            ->having('countInfor','>', '20')
            ->having('information.slug_type_input', 'logo')
            ->orderBy('countInfor', 'DESC')
            ->distinct()
            ->limit($number)
            ->get(); */
			
		return array();
    }

    public static function showUrlPostVip($themeId, $userId){
        $domain = static::where('theme_id',$themeId)
		->where('user_id', $userId)
        ->first();
		
		if (!empty($domain->domain)) {
			return $domain->domain;
		}
		
        return $domain->url;

    }

    public static function getAffiliateMoma () {
		
        $affiliateMomas = AffiliateMoma::join('users', 'users.email', 'affiliate_moma.user_email')
            ->join('domains', 'domains.user_id', 'users.id')
                ->join('themes', 'themes.theme_id', 'domains.theme_id')
            ->select (
                'users.email',
                'themes.code',
                'domains.*'
            )
            ->orderBy('affiliate_moma.money', 'desc')
            ->orderBy('affiliate_moma.affiliate_moma_id', 'asc')
            ->limit(20)
            ->get();

        $userEmails = array();
        $themeCodes = array();
        foreach ($affiliateMomas as $affiliateMoma) {
            $userEmails[] = $affiliateMoma->email;
            $themeCodes = $affiliateMoma->code;
        }

        // get information
        $informations = Information::whereIn('user_email', $userEmails)
            ->where('theme_code', $themeCodes)
            ->where('slug_type_input', 'logo')
            ->get();

        foreach ($affiliateMomas as $id => $affiliateMoma) {
            foreach ($informations as $information) {
                if ($information->user_email == $affiliateMoma->email
                    && $information->theme_code == $affiliateMoma->code
                ) {
                    $affiliateMomas[$id]->image = $information->content;
                    break;
                }
            }
        }

	
        return $affiliateMomas;
    }

}
