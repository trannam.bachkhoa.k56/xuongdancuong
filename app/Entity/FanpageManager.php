<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 9/4/2019
 * Time: 10:26 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class FanpageManager extends Model
{
    protected $table = 'fanpage_manager';

    protected $primaryKey = 'fanpage_manager_id';

    protected $fillable = [
        'fanpage_manager_id',
        'name_fanpage',
        'link_fanpage',
        'slug',
        'theme_code',
        'email_user',
        'created_at',
        'updated_at'
    ];
}