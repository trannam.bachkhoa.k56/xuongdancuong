<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HelpVideo extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'help_video';

    protected $primaryKey = 'help_video_id';

    protected $fillable = [
        'help_video_id',
        'group_help_video',
        'title',
        'image',
        'embbed_video',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
}
