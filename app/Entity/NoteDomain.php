<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 8/3/2019
 * Time: 4:39 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class NoteDomain extends Model
{
    protected $table = 'note_domain';

    protected $primaryKey = 'note_domain_id';

    protected $fillable = [
        'note_domain_id',
        'content',
        'domain_id',
        'created_at',
        'updated_at'
    ];

    public static function getDetailDomain($domainId) {
        $domains = static::where('domain_id', $domainId)
            ->get();

        return $domains;
    }
}
