<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 8/30/2019
 * Time: 9:56 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class ColorWebsite extends Model
{
    protected $table = 'color_web';

    protected $primaryKey = 'color_web_id';

    protected $fillable = [
        'color_web_id',
        'text_full',
        'color_hover',
        'color_title',
        'color_footer',
        'background_full',
        'theme_code',
        'user_email ',
        'created_at',
        'updated_at'
    ];
}