<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class PaymentInfomation extends Model
{
    protected $table = 'payment_information';

    protected $primaryKey = 'payment_id';

    protected $fillable = [
        'payment_id',
        'user_id',
        'coin_total',
        'payment_name',
        'payment_bank',
        'bank_branch',
        'bank_number',
        'created_at',
        'updated_at'
    ];

  	public static function getWithUser($user_id){
  	 	return static::where('user_id', $user_id)->first();
  }


}