<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Process extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'process';

    protected $primaryKey = 'process_id';

    protected $fillable = [
        'process_id',
        'process_name',
        'campain_customer_id',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at'
    ];

    public static function getAll(){
        return static::get();
    }

   
}
