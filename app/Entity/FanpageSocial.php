<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use App\Ultility\InforFacebook;


class FanpageSocial extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'fanpage_social';

    protected $primaryKey = 'page_social_id';

    protected $fillable = [
		'page_social_id',
        'user_id',
        'provider_user_id',
		'fanpage_id',
        'fanpage_name',
        'created_at',
        'updated_at'
    ];

    public static function getAll(){
        $page = static::join('social_accounts', 'social_accounts.user_id', 'fanpage_social.user_id')
		->select([
				'fanpage_social.fanpage_id',
				'fanpage_social.fanpage_name',
				'social_accounts.user_id',
				'social_accounts.token',
				'social_accounts.email'
			])
		->where('social_accounts.email', ModelParent::getEmailUser())
		->get();
        return $page;
    }
	
	public static function getImagePost($postId, $token){
		
		$app = new InforFacebook();
		$fb = new \Facebook\Facebook([
			'app_id' => $app->getAppId(),
			'app_secret' => $app->getAppSecret(),
			'default_graph_version' => $app->getDefaultGraphVersion()
		]);
		
		try {
		  // Returns a `Facebook\FacebookResponse` object
		  $response = $fb->get(
			'/'.$postId.'?fields=full_picture,picture',
			$token
		  );
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}
		$graphNode = $response->getGraphNode();
		/* handle the result */
		
		return $graphNode;
	}
	

   
}
