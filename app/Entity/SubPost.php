<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 10/13/2017
 * Time: 10:52 AM
 */

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SubPost  extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'sub_post';

    protected $primaryKey = 'sub_post_id';

    protected $fillable = [
        'sub_post_id',
        'post_id',
        'type_sub_post_slug',
        'theme_code',
        'deleted_at',
        'user_email',
        'updated_at'
    ];

    public static function showSubPost($typePost, $count = 10, $sort = 'desc') {
        try {
            $postModel = new Post();

            $posts = $postModel->join('sub_post', 'sub_post.post_id', '=', 'posts.post_id')
                ->select(
                    'sub_post.sub_post_id',
                    'posts.post_id',
                    'title',
                    'slug',
                    'image',
                    'content',
                    'description'
                )
                ->where('posts.theme_code', ModelParent::getThemeCode())
                ->where('posts.user_email', ModelParent::getEmailUser())
                ->where('post_type', $typePost)
                ->orderBy('posts.post_id', $sort)
                ->limit($count)
				->get();

            foreach($posts as $id => $post) {
                $inputs = Input::where('post_id', $post->post_id)
                    ->where('theme_code', ModelParent::getThemeCode())
                    ->get();
                foreach ($inputs as $input) {
                    $posts[$id][$input->type_input_slug] = $input->content;
                }
            }

            return $posts;
        } catch (\Exception $e) {
            Log::error('Entity->SubPost->showSubPost: lấy bài viết thuộc dạng bài viết');

            return null;
        }
    }
	
	
	public static function showSubPostMarketing($typePost, $count = 10, $sort = 'desc') {
        try {
            $postModel = new Post();

            $posts = $postModel->join('sub_post', 'sub_post.post_id', '=', 'posts.post_id')
                ->select(
                    'sub_post.sub_post_id',
                    'posts.post_id',
                    'title',
                    'slug',
                    'image',
                    'content',
                    'description'
                )
                ->where('post_type', $typePost)
                ->orderBy('posts.post_id', $sort)
                ->offset(0)
                ->limit($count)->get();

            foreach($posts as $id => $post) {
                $inputs = Input::where('post_id', $post->post_id)
                    ->get();
                foreach ($inputs as $input) {
                    $posts[$id][$input->type_input_slug] = $input->content;
                }
            }

            return $posts;
        } catch (\Exception $e) {
            Log::error('Entity->SubPost->showSubPost: lấy bài viết thuộc dạng bài viết');

            return null;
        }
    }

    public static function checkTaskMoma($typePost, $count = 10, $offset = 0, $sort = 'asc') {
        try {
            $postModel = new Post();

            $posts = $postModel->join('sub_post', 'sub_post.post_id', '=', 'posts.post_id')
                ->select(
                    'sub_post.sub_post_id',
                    'posts.post_id',
                    'title',
                    'slug',
                    'image',
                    'content',
                    'description'
                )
                ->where('posts.theme_code', 'vn3c')
                ->where('posts.user_email', 'vn3ctran@gmail.com')
                ->where('post_type', $typePost)
                ->orderBy('posts.post_id', $sort)
                ->offset($offset)
                ->limit($count);

            if ($typePost == 'nhiem-vu-chinh') {
                $taskHelps = TaskHelp::where('user_id', Auth::id())->where('type', 'main')
                        ->pluck('post_id')->toArray();
                $posts = $posts->whereNotIn('posts.post_id', $taskHelps);
            }

            if ($typePost == 'nhiem-vu-chinh') {
                $taskHelps = TaskHelp::where('user_id', Auth::id())
                    ->where('type', 'main')
                    ->whereDate('created_at', Carbon::today())->pluck('post_id')->toArray();
                $posts = $posts->whereNotIn('posts.post_id', $taskHelps);
            }

            $posts = $posts->get();

            foreach($posts as $id => $post) {
                $inputs = Input::where('post_id', $post->post_id)
                    ->where('theme_code', 'vn3c')
                    ->get();
                foreach ($inputs as $input) {
                    $posts[$id][$input->type_input_slug] = $input->content;
                }
            }

            return $posts;
        } catch (\Exception $e) {
            Log::error('Entity->SubPost->showSubPost: lấy bài viết thuộc dạng bài viết');

            return array();
        }
    }
}
