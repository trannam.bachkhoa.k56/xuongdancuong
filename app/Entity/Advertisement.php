<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 12/5/2017
 * Time: 2:07 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertisement extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'advertisement';

    protected $primaryKey = 'adv_id';

    protected $fillable = [
        'adv_id',
        'post_id',
        'name',
        'phone',
        'website',
        'message',
        'method',    
        'total_money',       
        'created_at',
        'updated_at'
    ];

  
}

