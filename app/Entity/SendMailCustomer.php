<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class SendMailCustomer extends Model
{
    protected $table = 'send_mail_customer';
    protected $primaryKey = 'send_mail_customer_id';
    protected $fillable = [
        'send_mail_customer_id',
        'user_id',
        'time_send_mail',
        'total_mail',
        'created_at',
        'updated_at'
    ];

    public function insertDataCustomerSendMail($userId, $time, $totalMail){
        $sendMail = new self();
        $sendMail->insert([
            'user_id' =>  $userId,
            'time_send_mail' => $time,
            'total_mail' => $totalMail,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
