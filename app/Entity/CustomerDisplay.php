<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 12/5/2017
 * Time: 2:07 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerDisplay extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'customer_display';

    protected $primaryKey = 'id';

    protected $fillable = [
       'id',
       'statusName',
       'statusPhone',
       'statusMessage',
       'buttonName',
       'theme_code',
       'user_email',
       'messageChange',
       'created_at',
       'updated_at',
    ];

}

