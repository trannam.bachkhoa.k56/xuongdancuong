<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class CampaignCustomer extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'campaign_customer';

    protected $primaryKey = 'campaign_customer_id';

    protected $fillable = [
        'campaign_customer_id',
        'campaign_title',
        'manager_id',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at'
    ];

    public static function getAll(){
  
        return static::get();
    }

   
}
