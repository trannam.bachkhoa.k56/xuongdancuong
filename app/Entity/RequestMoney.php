<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class RequestMoney extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'request_money';

    protected $primaryKey = 'request_id';

    protected $fillable = [
        'request_id',
        'user_id',
        'money_request',
        'status',
        'created_at',
        'updated_at'
    ];
   
}
