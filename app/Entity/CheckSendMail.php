<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class CheckSendMail extends Model
{
    protected $table = 'check_send_email';
    protected $primaryKey = 'check_send_email_id';
    protected $fillable = [
        'check_send_email_id',
        'receiver',
        'author',
        'seen_at',
        'time_seen',
        'created_at',
        'updated_at'
    ];

    public function insertToDB($receiver, $author, $time, $timeSeen, $emailUser){
        $mail = new self();
        if ($this->checkExitsCheckMail($receiver, $author, $time)) {
            return;
        }
        if ((string)$author === (string)$emailUser) {
            $mail->insert([
                'receiver' => $receiver,
                'author' => $author,
                'seen_at' => $time,
                'time_seen' => $timeSeen,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
        }
    }

    private function checkExitsCheckMail($receiver, $author, $seenAt){
        $checkMail = new self();
        if($checkMail->where('receiver', $receiver)->where('author', $author)->where('seen_at', $seenAt)->exists()) {
            return true;
        }
        return false;
    }
}
