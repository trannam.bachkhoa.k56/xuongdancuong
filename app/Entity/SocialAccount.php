<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model

{
    protected $fillable = [
		'user_id',
		'provider_user_id',
		'provider',
		'email',
		'name',
		'avatar',
		'token',
		'created_at',
		'updated_at'
	];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}