<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Category extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'categories';

    protected $primaryKey = 'category_id';

    protected $fillable = [
        'category_id',
        'title',
        'slug',
        'description',
        'image',
        'template',
        'parent',
        'cate_getfly',
        'post_type',
        'is_hide',
		'meta_title',
		'meta_description',
		'meta_keyword',
        'user_email',
        'theme_code',
        'deleted_at',
        'deleted_at',
        'created_at',
        'updated_at'
    ];
    // lấy ra danh mục
    public function getCategory($themeCode, $emailUser, $postType = 'post'){
        try {
            $categories = $this->where([
                'post_type' => $postType,
            ])->where('parent', 0)
                ->where('theme_code', $themeCode)
                ->where('user_email', $emailUser)
                ->orderBy('category_id')->get();

            foreach ($categories as $id => $cate) {
                $categories[$id]['sub_children'] = array();
                $categories[$id]['sub_children'] = $this->getSubCategory($cate->category_id, $categories[$id]['sub_children'], $postType);
            }

        } catch (\Exception $e) {
            $categories = array();

            Log::error('Entity->Category->getCategory: Lấy danh mục ra bị lỗi: '.$e->getMessage());
        } finally {
            return $categories;
        }
    }

    public static function getCategoryIndex ($postType = 'post') {
        try {
            $categories = static::where('post_type', $postType)
                ->select('slug', 'title', 'category_id')
                ->where('theme_code', ModelParent::getThemeCode())
                ->where('user_email', ModelParent::getEmailUser())
                ->orderBy('category_id')->get();

        } catch (\Exception $e) {
            $categories = array();

            Log::error('Entity->Category->getCategoryIndex: Lấy danh mục ra bị lỗi: '.$e->getMessage());
        } finally {
            return $categories;
        }
    }

    public function getSubCategory($cateId, $categorySource = array(), $postType = 'post', $subChild = '', $level = 2) {
        try {
            $categories = $this->where([
                'parent' => $cateId,
                'post_type' => $postType
            ])->orderBy('category_id')->get();

            if($categories->isEmpty()) {

                return $categorySource;
            }
            $subChild .= '---';
            $level ++;
            foreach ($categories as $cate) {
                $categorySource[] = [
                    'title' => $subChild.$cate->title,
                    'title_show' => $cate->title,
                    'category_id' => $cate->category_id,
                    'image' => $cate->image,
                    'slug' => $cate->slug,
					'updated_at' => $cate->updated_at,
                    'level' => $level
                ];
                $categorySource = $this->getSubCategory($cate->category_id, $categorySource, $postType, $subChild, $level);
            }
        } catch (\Exception $e) {
            $categorySource = null;

            Log::error('Entity->Category->getCategory: Lấy danh mục ra bị lỗi: '.$e->getMessage());
        } finally {
            return $categorySource;
        }
    }

    // láy ra chi tiết category với slug
    public static function getDetailCategory($slug) {
		try {
			$categoryModel = new Category();
			$category = $categoryModel->where('slug', $slug)
				->where('theme_code', ModelParent::getThemeCode())
				->where('user_email', ModelParent::getEmailUser())
				->first();

			$inputs = Input::where('cate_id', $category->category_id)->get();
			foreach ($inputs as $input) {
				$category[$input->type_input_slug] = $input->content;
			}

			return $category;
		} catch (\Exception $e) {
			return array();
		}
       
    }

    // kiểm tra xem với slug thì category tồn tại không
    public static function existCategory($slug) {
        $categoryModel = new Category();

        return $categoryModel->where('slug', $slug)
            ->where('theme_code', ModelParent::getThemeCode())
            ->where('user_email', ModelParent::getEmailUser())
            ->exists();
    }
    //lấy danh mục con theo danh mục cha
    // $postType = 'post' lấy theo danh mục bai viết
     // $postType = 'product' lấy theo danh mục sản phẳm
    public static function getChildrenCategory($parent, $postType = 'post') {
        $categories = static::where([
            'parent' => $parent,
            'post_type' => $postType
        ])->orderBy('category_id') 
        ->where('theme_code', ModelParent::getThemeCode())
        ->where('user_email', ModelParent::getEmailUser());
		
		if ($categories->count() > 0) {
			return $categories->get();
		}
       
		return null;
    }

    //lấy danh mục con theo danh mục cha
    // $postType = 'post' lấy theo danh mục bai viết
     // $postType = 'product' lấy theo danh mục sản phẳm
    public static function getChildrenCategoryWithSlug($slugCateParent, $postType = 'post') {

        $categoryParent = static::where([
            'slug' => $slugCateParent,
            'post_type' => $postType
        ])->orderBy('category_id') 
        ->where('theme_code', ModelParent::getThemeCode())
        ->where('user_email', ModelParent::getEmailUser())
        ->first();

        $categories = static::where([
            'parent' =>  $categoryParent->category_id,
            'post_type' => $postType
        ])->orderBy('category_id') 
        ->where('theme_code', ModelParent::getThemeCode())
        ->where('user_email', ModelParent::getEmailUser())
        ->get();

        return $categories;
    }

    // lấy ra danh mục
    public static function getCategoryProduct($count = 20){
        try {
            $categories = static::where([
                'post_type' => 'product',
            ])->where('parent', 0)
                ->where('theme_code', ModelParent::getThemeCode())
                ->where('user_email', ModelParent::getEmailUser())
                ->orderBy('category_id')
                ->limit($count)
                ->get();
               

//            foreach ($categories as $id => $cate) {
//                $categories[$id]['sub_children'] = array();
//                $categories[$id]['sub_children'] = $this->getSubCategory($cate->category_id, $categories[$id]['sub_children'], $postType);
//            }

        } catch (\Exception $e) {
            $categories = array();

            Log::error('Entity->Category->getCategory: Lấy danh mục ra bị lỗi: '.$e->getMessage());
        } finally {
            return $categories;
        }
    }
	
	 public static function getCategoryPost($postType = 'post'){
        try {
            $categories = static::where([
                'post_type' => $postType,
            ])->where('parent', 0)
                ->where('theme_code', ModelParent::getThemeCode())
                ->where('user_email', ModelParent::getEmailUser())
                ->orderBy('category_id')->get();

//            foreach ($categories as $id => $cate) {
//                $categories[$id]['sub_children'] = array();
//                $categories[$id]['sub_children'] = $this->getSubCategory($cate->category_id, $categories[$id]['sub_children'], $postType);
//            }

        } catch (\Exception $e) {
            $categories = array();

            Log::error('Entity->Category->getCategory: Lấy danh mục ra bị lỗi: '.$e->getMessage());
        } finally {
            return $categories;
        }
	 }
}
