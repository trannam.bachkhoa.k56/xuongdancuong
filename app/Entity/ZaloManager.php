<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 3/21/2018
 * Time: 3:08 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

use App\Mail\Mail;
use App\Entity\User;
use App\Entity\MailAutomation;
use App\Entity\ModelParent;
use App\Ultility\Ultility;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ZaloManager extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'zalo_manager';

    protected $primaryKey = 'zalo_manager_id';

    protected $fillable = [
        'zalo_manager_id',
        'slug',
        'name_zalo',
        'phone_zalo',
        'link_token',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at'
    ];

}