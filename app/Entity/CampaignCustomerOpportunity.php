<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class CampaignCustomerOpportunity extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'campaign_cus_opportunity';

    protected $primaryKey = 'campaign_cus_opportunity_id';

    protected $fillable = [
        'campaign_cus_opportunity_id',
        'campaign_cus_id',
        'opportunity_id',
        'created_at',
        'updated_at'
    ];

    public static function getAll(){
        $campaign = static::get();
        return $campaign ;
    }

   
}
