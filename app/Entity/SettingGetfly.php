<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 3/2/2018
 * Time: 8:29 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Ultility\CallApi;

class SettingGetfly extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'setting_getfly';

    protected $primaryKey = 'setting_getfly_id';

    protected $fillable = [
        'setting_getfly_id',
        'user_id',
        'api_key',
        'base_url',
		'campaign',
		'campaign_status',
        'created_at',
        'deleted_at',
        'updated_at'
    ];

    public static function getApiKey() {
        try {
            $userId = ModelParent::getUserId();

            $settingGetFly = static::where('user_id', $userId)->first();
            if(empty($settingGetFly)) {
                return null;
            }

            return $settingGetFly->api_key;
        } catch (\Exception $e) {
            Log::error('Entity->SettingGetFly->getApiKey: Lỗi lấy api_key của getfly');

            return null;
        }
    }

    public static function getBaseUrl() {
        try {
            $userId = ModelParent::getUserId();

            $settingGetFly = static::where('user_id', $userId)->first();
            if(empty($settingGetFly)) {
                return null;
            }

            return $settingGetFly->base_url;
        } catch (\Exception $e) {
            Log::error('Entity->SettingGetFly->getBaseUrl: Lỗi lấy api_key của getfly');

            return null;
        }
    }

    public static function checkSettingGetfly() {
        try {
            $userId = ModelParent::getUserId();

            $settingGetFly = static::where('user_id', $userId)->first();
            if(empty($settingGetFly)) {
                return false;
            }

            if (empty($settingGetFly->base_url) || empty($settingGetFly->api_key)) {
                return false;
            }

            return true;
        } catch (\Exception $e) {
            Log::error('Entity->SettingGetFly->checkSettingGetfly: Kiểm tra tồn tại cài đặt getfly');

            return false;
        }
    }
	
	public static function getCampaign() {
        try {
            $userId = ModelParent::getUserId();

            $settingGetFly = static::where('user_id', $userId)->first();
			
            if(empty($settingGetFly)) {
                return null;
            }

            return $settingGetFly->campaign;
        } catch (\Exception $e) {
            Log::error('Entity->SettingGetFly->getBaseUrl: Lỗi lấy api_key của getfly');

            return null;
        }
    }

    public static function getCampainStatus() {
        try {
            $userId = ModelParent::getUserId();

            $settingGetFly = static::where('user_id', $userId)->first();
			
            if(empty($settingGetFly)) {
                return null;
            }

            return $settingGetFly->campaign_status;
        } catch (\Exception $e) {
            Log::error('Entity->SettingGetFly->getBaseUrl: Lỗi lấy api_key của getfly');

            return null;
        }
    }
	
	public static function addNewCampaignGetfly ($name, $phone, $email, $address, $utmSource, $utmCampaign, $nameContact = null) {
		try {
			$userId = ModelParent::getUserId();

            $settingGetFly = static::where('user_id', $userId)->first();
			
            $account = (object) [
                "account_name" => $nameContact,
                "phone_office" => $phone,
                "email" => $email,
                "gender" =>  0,
                "billing_address_street" => $address,
                // "birthday" => $request->input('birthday_day').'/'.$request->input('birthday_Month').'/'.$request->input('birthday_Year'),
                "account_type" =>  1,
                "industry" => "2,3"
            ];

            $opportunity = (object) [
                'token_api' => $settingGetFly->campaign,
                'user_id' => "",
                'recipient' => "",
                'opportunity_status' => $settingGetFly->campaign_status,

            ];

            $contacts = [
                "first_name" => !empty($nameContact) ? $nameContact : $name,
                "email" => $email,
                "phone_mobile" => $phone
            ];

            $referer = (object) [
                "utm_source" =>  $utmSource,
                "utm_campaign" => $utmCampaign,
            ];

            $data = (object) [
                'account' => $account,
                'contacts' => $contacts,
                'opportunity' => $opportunity,
                'referer' => $referer
            ];

            // đồng bộ lên getfly.
            $callApi = new CallApi();
            $callApi->addNewCampaign($data);

            return true;
        } catch (\Exception $e) {
            return false;
        }
	}
}