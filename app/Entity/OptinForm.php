<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class OptinForm extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'optin_form';

    protected $primaryKey = 'form_id';

    protected $fillable = [
           'form_id',
           'post_id',
           'campaign_id',
           //'showCompanyName',
           'showName',
           'showPhone',
           'showFacebook',
           'showDescription',
           'showAddress',
           'formRedirect',
           'updated_at',
           'created_at'
    ];

    public static function getAll(){
      return static::get();
    }


    public static function showWithId($id){
      return static::where('form_id',$id)->first();
    }

}
