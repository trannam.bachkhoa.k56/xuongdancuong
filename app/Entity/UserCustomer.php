<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class UserCustomer extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'user_customer_introduct';

    protected $primaryKey = 'user_customer_introduct_id';

    protected $fillable = [
        'user_customer_introduct_id',
        'user_id',
        'customer_id',
		'status',
        'created_at',
        'updated_at'
    ];

}
