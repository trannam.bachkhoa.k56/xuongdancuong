<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 8/3/2019
 * Time: 4:39 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class NoteContact extends Model
{
    protected $table = 'note_contact';

    protected $primaryKey = 'note_contact_id';

    protected $fillable = [
        'note_contact_id',
        'content',
        'contact_id',
        'created_at',
        'updated_at'
    ];
}