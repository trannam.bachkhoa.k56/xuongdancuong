<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerIntroduct extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'customer_introduct';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'user_id',
        'order_id',
        'user_buy',
        'created_at',
        'updated_at'
    ];

    public static function getIntroduceWithUserID($user_id)
    {
        $introduce = static::leftJoin('orders','orders.order_id','customer_introduct.order_id')
        ->leftJoin('users','users.id', 'customer_introduct.user_id')
		->leftJoin('users as user_buy', 'user_buy.id', 'customer_introduct.user_buy')
        ->select([
            'orders.shipping_name',
            'orders.total_price',
            'orders.shipping_address',
            'orders.gift_code',
            'orders.shipping_email',
            'orders.created_at',
            'customer_introduct.*',
			'orders.status',
			'user_buy.name as user_buy_name'
        ])
        ->where('customer_introduct.user_id',$user_id)
        ->get();

        return $introduce; 
    }

     public static function getSumPriceWithUserID($user_id)
    {
        $sumPrice = static::leftJoin('orders','orders.order_id','customer_introduct.order_id')
        ->leftJoin('users','users.id','customer_introduct.user_id')     
        ->where('customer_introduct.user_id',$user_id)
        ->sum('orders.total_price');
        
        return $sumPrice; 
    }

 
}
