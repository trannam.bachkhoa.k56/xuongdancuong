OptinFormPost.php<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class OptinFormPost extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'optin_form_post';

    protected $primaryKey = 'optin_form_post_id';

    protected $fillable = [
        'optin_form_post_id',
        'post_id',
        'form_id',
        'created_at',
        'updated_at'
    ];
   
}
