<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Commission extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'commission';

    protected $primaryKey = 'commission_id';

    protected $fillable = [
        'commission_id',
        'position',
        'salary',
        'sub_salary',
        'security',
        'revenue',
        'commission',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at',
    ];

}
