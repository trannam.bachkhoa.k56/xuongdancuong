<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 8/3/2019
 * Time: 5:48 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class RemindContact extends Model
{
    protected $table = 'remind_contact';

    protected $primaryKey = 'remind_contact_id';

    protected $fillable = [
        'remind_contact_id',
        'date',
        'status', // 0: chưa làm 1: đã làm
        'content',
        'theme_code',
        'user_email',
        'contact_id',
        'created_at',
        'updated_at'
    ];


    public static function remindContact () {
        $remindContacts = static::join('contact', 'contact.contact_id', 'remind_contact.contact_id')
			->select('remind_contact.*')
			->where('remind_contact.theme_code', ModelParent::getThemeCode())
            ->where('remind_contact.user_email', ModelParent::getEmailUser())
            ->where('remind_contact.status', 0)
            ->where('remind_contact.date', '<=', date("Y-m-d"))
            ->get();

        return $remindContacts;
    }
}