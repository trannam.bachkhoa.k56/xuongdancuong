<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 12/5/2017
 * Time: 2:07 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShowWebsite extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'show_website';

    protected $primaryKey = 'views_id';

    protected $fillable = [
        'views_id',
		'source',
		'count',
		'user_email',
		'theme_code',
        'created_at',
        'updated_at'
    ];
	
}

