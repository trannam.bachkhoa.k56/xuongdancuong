<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class EmailContactCampaign extends Model
{
    protected $table = 'email_contact_campaign';
    protected $primaryKey = 'email_contact_campaign_id';
    protected $fillable = [
      'email_contact_campaign_id',
      'contact_id',
        'group_id',
        'campaign_id',
        'created_at',
        'updated_at'
    ];

    public static function getCampaignByContactId($contactId) {
        $contactCampaign = new static();
        $contactCampaign = $contactCampaign->where('contact_id', $contactId)->get();
        if (!empty($contactCampaign)) {
            return $contactCampaign;
        }
        return array();
    }

}
