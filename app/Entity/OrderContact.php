<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 8/6/2019
 * Time: 4:16 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class OrderContact extends Model
{
    protected $table = 'order_contact';

    protected $primaryKey = 'order_contact_id';

    protected $fillable = [
        'order_contact_id',
        'price',
        'content',
        'contact_id',
        'created_at',
        'updated_at'
    ];
}