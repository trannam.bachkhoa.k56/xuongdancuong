<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 1/16/2018
 * Time: 9:16 AM
 */

namespace App\Entity;

use App\Ultility\Ultility;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ModelParent extends Model
{
    public static function getThemeCode() {
        try {
            $domainUrl = Ultility::getCurrentDomain();
            $domain = Domain::where('url', $domainUrl)
			->orWhere('domain', $domainUrl)->first();
			// lay ra ten mien
			/*$nameUrl = Ultility::getCurrentHttpHost();
		
			if ($nameUrl != 'moma.vn' && $nameUrl != 'taowebsitemienphi.com'  && $nameUrl != 'website-mienphi.com'   ) {
				$domain = Domain::where('url', 'like', '%'.$nameUrl.'%')
				->orWhere('url', 'like', $nameUrl.'%')
				->first();
				
				
			}*/
		
            if (empty($domain)) {
                return 'vn3c';
            }
            $theme = Theme::where('theme_id', $domain->theme_id)->first();

            return $theme->code;
        } catch (\Exception $e) {
            Log::error('Entity->ModelParent: lỗi lấy mã theme');

            return null;
        }

    }

    public static function getEmailUser() {
        try {
            $domainUrl = Ultility::getCurrentDomain();
            $domain = Domain::where('url', $domainUrl)
			->orWhere('domain', $domainUrl)->first();
			// lay ra ten mien
			/*$nameUrl = Ultility::getCurrentHttpHost();
		
			if ($nameUrl != 'moma.vn' && $nameUrl != 'taowebsitemienphi.com'  && $nameUrl != 'website-mienphi.com'  ) {
				$domain = Domain::where('url', 'like', '%'.$nameUrl.'%')
				->orWhere('url', 'like', $nameUrl.'%')
				->first();
				
				
			}*/
			
            if (empty($domain)) {
                return 'vn3ctran@gmail.com';
            }
            $user = User::where('id', $domain->user_id)->first();

            return $user->email;
        } catch (\Exception $e) {
            Log::error('Entity->ModelParent: lỗi lấy email');

            return null;
        }
    }

    public static function getUserId() {
        try {
            $domainUrl = Ultility::getCurrentDomain();
            $domain = Domain::where('url', $domainUrl)
			->orWhere('domain', $domainUrl)->first();
			// lay ra ten mien
			/*$nameUrl = Ultility::getCurrentHttpHost();
		
			if ($nameUrl != 'moma.vn' && $nameUrl != 'taowebsitemienphi.com'  && $nameUrl != 'website-mienphi.com'  ) {
				$domain = Domain::where('url', 'like', '%'.$nameUrl.'%')
				->orWhere('url', 'like', $nameUrl.'%')
				->first();
				
				
			}*/
			
            if (empty($domain)) {
                return 1;
            }

            return $domain->user_id;
        } catch (\Exception $e) {
            Log::error('Entity->ModelParent: lỗi lấy mã user_id');

            return null;
        }
    }
}