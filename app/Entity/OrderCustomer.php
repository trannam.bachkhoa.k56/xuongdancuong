<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/3/2017
 * Time: 2:50 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class OrderCustomer extends Model
{
    protected $table = 'order_customer';

    protected $primaryKey = 'order_customer_id';

    protected $fillable = [
        'order_customer_id',
        'user_id',
        'contact_id',
        'payment',
        'surcharge',
        'affiliate_money',
        'notes',
        'total_price',
        'cost',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at'
    ];

    public static function getProfit($month, $year, $userId) {
        $partner = DomainUser::join('commission', 'commission.commission_id', 'domain_user.commission_id')
            ->select(
                'commission.*'
            )
            ->where('domain_user.user_id', $userId)
            ->first();

        $orderCustomer = OrderCustomer::where('user_id', $userId)
            ->where('theme_code', ModelParent::getThemeCode())
            ->where('user_email', ModelParent::getEmailUser())
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year);

        $costOrder = $orderCustomer->sum('cost');
        $orderCustomerPayment = $orderCustomer->sum('payment');
        if (!empty($partner)) {
            $costTotal = $costOrder + $orderCustomerPayment*$partner->commission/100 + $partner->salary + $partner->sub_salary;
        } else {
            $costTotal = 0;
        }

        return ($orderCustomerPayment - $costTotal);
    }

    public static function orderCustomerCountOrderMonthYear ($userId, $month, $year) {
        return static::where('user_id', $userId)
            ->where('theme_code', ModelParent::getThemeCode())
            ->where('user_email', ModelParent::getEmailUser())
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->count();
    }

    public static function orderCustomerTotalOrderMonthYear ($userId, $month, $year) {
        $orderCustomerTotal = static::where('user_id', $userId)
            ->where('theme_code', ModelParent::getThemeCode())
            ->where('user_email', ModelParent::getEmailUser())
            ->whereMonth('created_at', $month)
            ->whereYear('created_at', $year)
            ->sum('total_price');

        return $orderCustomerTotal;
    }
}
