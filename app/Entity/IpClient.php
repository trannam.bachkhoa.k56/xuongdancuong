<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 12/19/2018
 * Time: 3:15 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class IpClient extends Model
{
    protected $table = 'ip_client';

    protected $primaryKey = 'ip_client_id';

    protected $fillable = [
        'ip_client_id',
        'ip',
        'number',
        'created_at',
        'updated_at'
    ];
}