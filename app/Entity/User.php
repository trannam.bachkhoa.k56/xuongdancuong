<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'users';

    // thành viên
    protected static $member = 1;
    // Biên tập viên
    protected static $editor = 2;
    // Quản lý
    protected static $manager = 3;
    // Creater
    protected static $creater = 4;

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function isManager($role) {
        if ($role == User::$manager || $role == User::$creater) {
            return true;
        }

        return false;
    }

    public static function isEditor($role) {
        if ($role == User::$editor) {
            return true;
        }

        return false;
    }

    public static function isMember($role) {
        if ($role == User::$member) {
            return true;
        }

        return false;
    }

    public static function isCreater($role) {
        if ($role == User::$creater) {
            return true;
        }

        return false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'accesstoken',
        'email',
        'role',
        'password',
        'image',
        'point',
        'vip',
        'email_remaining',
        'affiliate_money',
        'user_id',
        'phone',
        'package_mail',
        'theme_show',
        'bussiness',
        // 1 là gói 5000 mail 1 tháng 
        // 2 là gói 20000 mail 1 tháng
        'time_email',
        'theme_code',
        'user_email',
        /*
            Ban free:
                website:
                khong qua 5 san pham.
                khong qua 50 don hang.
                khong qua 50 lien he.
                khong cai dat duoc gui mail thong bao.
                Khong qua 30 bai viet.

            upgrade 129k/ thang:
                cho phep up 100 san pham.
                don hang khong gioi han.
                duoc cai dat gui mail thong bao.
                khong qua 300 bai viet.

            upgrade 500k / thang:
                cho phep up san pham thoai mai.
                don hang khong gioi han.
                duoc cai dat gui mail thong bao.
                bai viet ko gioi han.

            chia lam 3 vip.
                0: free.
                1: upgrade 129k / thang.
                2: upgrade 500k / thang.
        */
        'vip',
        'is_help', // 0: chưa hướng dẫn; 1: đã hiểu.
        'coint',
        'total_coin_introduct',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getAllUser() {
        try {
            $users = static::select('id', 'email','name')->orderBy('id', 'desc')
                ->where('role', '>=', 3)->get();

            return $users;
        } catch (\Exception $e) {
            Log::error('Entity->User->getAllUser: Lỗi lấy tất cả user');
            return null;
        }
    }

    public static function getUserComment() {
        try {
            $userModel = new User();

            $users = $userModel->select('id', 'email')
                ->orderBy('id', 'desc')
                ->where('role', '<=', 3)
                ->where('theme_code', ModelParent::getThemeCode())
                ->where('user_email', ModelParent::getEmailUser())
                ->get();

            return $users;
        } catch (\Exception $e) {
            Log::error('Entity->User->getAllUser: Lỗi lấy tất cả user');
            return null;
        }
    }

    public static function getUserByUserId($userId) {
        $user = new self();
        $user = $user->where('id', $userId)->first();
        if (!empty($user)) {
            return $user;
        }
        return null;
    }

}
