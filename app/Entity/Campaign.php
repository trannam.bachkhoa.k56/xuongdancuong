<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Campaign extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'campaign';

    protected $primaryKey = 'campaign_id';

    protected $fillable = [
        'campaign_id',
        'campaign_name',
        'group_id',
        'description',
        'user_email',
        'theme_code',
        'created_at',
        'updated_at'
    ];

    public static function getAll(){
        $campaign = new static();
        return $campaign->get();
    }

    public static function getCampaignByGroupId($groupId) {
        $campaign = new static();
        $campaign = $campaign->where('group_id', $groupId)->first();
        if(!empty($campaign)) {
            return $campaign;
        }

        return null;
    }

    public static function getCampaignName($campaignId) {
        $campaign = new static();
        $campaign = $campaign->where('campaign_id', $campaignId)->first(['campaign_name']);
        if(!empty($campaign)) {
            return $campaign->campaign_name;
        }

        return null;
    }

   
}
