<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CustomerTransaction extends Model
{
    protected $table = 'customer_transaction';
    protected $primaryKey = 'customer_transaction_id';
    protected $fillable = [
        'customer_transaction_id',
        'user_id',
        'money',
        'content',
        'created_at',
        'updated_at'
    ];

    public function insertData($userId, $money, $content){
        $transaction = new self();
        $transaction->insert([
            'user_id' =>  $userId,
            'money' => $money,
            'content' => $content,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);
    }
}
