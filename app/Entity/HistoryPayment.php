<?php
/**
 * Created by PhpStorm.
 * User: Nam Love Huong
 * Date: 6/30/2020
 * Time: 10:59 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class HistoryPayment extends Model
{
    protected $table = 'history_payment';

    protected $primaryKey = 'history_payment_id';

    protected $fillable = [
        'history_payment_id',
        'content',
        'money',
        'theme_code',
        'user_email',
        'created_at',
        'updated_at'
    ];
}