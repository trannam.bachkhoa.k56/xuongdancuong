<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 1/3/2019
 * Time: 9:10 AM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class EmailSetting extends Model
{
    protected $table = 'emailsetting';

    protected $primaryKey = 'id';

    protected $fillable = [
       	'id',
		'subject',
		'content',
		'group_id',
		'note',
		'type',
		'date_send',
		'time_send',
		'date_delay',
		'delay_time',
		'status',
		'theme_code',
		'user_email',
		'updated_at',
		'created_at',
    ];
}