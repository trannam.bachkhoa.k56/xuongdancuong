<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 3/21/2018
 * Time: 3:08 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

use App\Mail\Mail;
use App\Entity\User;
use App\Entity\MailAutomation;
use App\Entity\ModelParent;
use App\Ultility\Ultility;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class MailConfig extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'mail_config';

    protected $primaryKey = 'mail_config_id';

    protected $fillable = [
        'mail_config_id',
        'user_id',
        'email_send',
        'name_send',
        'email',
        'password',
        'address_server',
        'port',
        'sign',
        'method',
        'driver',
        'host',
        'email_receive',
        'encryption',
        'supplier',
        'api_key',
        'created_at',
        'updated_at'
    ];

    public static function mailMoma($to = '', $subject, $content) {

    }

    public static function sendMail($to = '', $subject, $content) {
         try {

           $userId = ModelParent::getUserId();
            // Check số lượng mail còn không 
            static::empty_mails($userId);

            // Phần gửi email
           
            $emailConfig = static::where('user_id', $userId)->first();

            if (!isset($emailConfig->email_send) || empty($emailConfig->email_send)) {
              if (!empty($to)) {
                $mail = new Mail(
                  $content,
                  ''
                );
                \Mail::to($to)->send($mail->subject($subject));

                //static::miniusMail($userId);
                return true;
              }
            }
      
           // change config send mail
           // config(['mail.host' => $emailConfig->address_server]);
           // config(['mail.port' => $emailConfig->port]);
         //if (!empty($emailConfig->email_send)) {
               config(['mail.from' => [
                     'address' => $emailConfig->email_send,
                     'name' => $emailConfig->name_send
              ]]);
        //}
           
           // config(['mail.username' => $emailConfig->email]);
           // config(['mail.password' => $emailConfig->password]);
           // config(['mail.driver' => $emailConfig->driver]);
           // config(['mail.encryption' => $emailConfig->encryption]);
           // if ($emailConfig->method == 1) {
               // // config mailGun
               // config(['mail.driver' => $emailConfig->driver]);
               // config(['mail.host' => $emailConfig->host]);
               // config(['services.mailgun' => [
                   // 'domain' => $emailConfig->address_server,
                   // 'secret' => $emailConfig->api_key,
               // ]]);
           //}

		$emailReceive = explode(',', $emailConfig->email_receive);
            $to = (empty($to)) ? $emailReceive : $to;
            $mail = new Mail(
                $content,
                $emailConfig->sign
                
            );

            \Mail::to($to)->send($mail->subject($subject));

            //gọi đến hàm update để trừ số lượng email ;

            static::miniusMail($userId->id);

            return true;

        } catch (\Exception $e) {
      
            Log::error('Entity->MainConfig->sendMail: lỗi khi gửi mail');

            return false;
        }

    }

    //Gửi email không trừ Email Marketing
     public static function sendMailDefault($to = '', $subject, $content) {
         try {
            $userId = ModelParent::getUserId();
            $emailConfig = static::where('user_id', $userId)->first();

            if (!isset($emailConfig->email_send) || empty($emailConfig->email_send)) {
              if (!empty($to)) {
                $mail = new Mail(
                  $content,
                  ''
                );
                \Mail::to($to)->send($mail->subject($subject));
                return true;
              }
            }
      
           // change config send mail
           // config(['mail.host' => $emailConfig->address_server]);
           // config(['mail.port' => $emailConfig->port]);
           //if (!empty($emailConfig->email_send)) {
                    config(['mail.from' => [
                     'address' => $emailConfig->email_send,
                     'name' => $emailConfig->name_send
              ]]);
          //}
           
           // config(['mail.username' => $emailConfig->email]);
           // config(['mail.password' => $emailConfig->password]);
           // config(['mail.driver' => $emailConfig->driver]);
           // config(['mail.encryption' => $emailConfig->encryption]);
           // if ($emailConfig->method == 1) {
               // // config mailGun
               // config(['mail.driver' => $emailConfig->driver]);
               // config(['mail.host' => $emailConfig->host]);
               // config(['services.mailgun' => [
                   // 'domain' => $emailConfig->address_server,
                   // 'secret' => $emailConfig->api_key,
               // ]]);
           //}

          $emailReceive = explode(',', $emailConfig->email_receive);
      
            $to = (empty($to)) ? $emailReceive : $to;
            $mail = new Mail(
                $content,
                $emailConfig->sign
                
            );

            \Mail::to($to)->send($mail->subject($subject));
            return true;

        } catch (\Exception $e) {
      
            Log::error('Entity->MainConfig->sendMail: lỗi khi gửi mail');

            return false;
        }

    }



    public static function send_mail_with_themecode($to = '', $subject, $content, $userEmail, $contactId = null, $user_email = null) {
		// try{

			// lấy ra user_id từ theme code và user email
			$userId = User::where('email', $userEmail)->first();
			// Check số lượng mail còn không 
      $checkEmail = static::empty_mails($userId->id);
      
      if (!$checkEmail) {
          return false;
      }

			// Lấy ra mail config theo theme code và user email 
			$emailConfig = static::where('user_id', $userId->id)->first();
			
			//gửi mail 

			if (!isset($emailConfig->email) || empty($emailConfig->email)) {
          if (!empty($to)) {
			  
              $mail = new Mail(
                  $content,
                  '',
                  $contactId, 
                  $subject,
                  $user_email
              );
                    
			if (isset($emailConfig->email_send) && !empty($emailConfig->email_send)) {
				config(['mail.from' => [
					'address' => $emailConfig->email_send,
					'name' => $emailConfig->name_send
				]]);
			}
					
            \Mail::to($to)->send($mail->subject($subject));

            static::miniusMail($userId->id);
            return true;
          }
      }

      config(['mail.from' => [
              'address' => $emailConfig->email_send,
              'name' => $emailConfig->name_send
      ]]);

      $emailReceive = explode(',', $emailConfig->email_receive);
      
      $to = (empty($to)) ? $emailReceive : $to;
      $mail = new Mail(
          $content,
          $emailConfig->sign,
          $contactId, 
          $subject,
          $user_email
      );
      static::miniusMail($userId->id);
      \Mail::to($to)->send($mail->subject($subject));


      //gọi đến hàm update để trừ số lượng email ;
      return true;
      // } catch (\Exception $e) {
    
          // Log::error('Entity->MainConfig->sendMail: lỗi khi gửi mail');

          // return false;
      // }

    }

    public static function empty_mails($id) {
        //lấy danh sách domain theo id truyen vao
        $user = User::where('id',$id)->first();
          if($user['email_remaining'] <= -100 )
          {  
             // exit("<h1 style='text-align:center;color:red'>Bạn đã dùng hết số lượng email trong tháng - Vui Lòng Nâng cấp tài khoản để được sử dụng tiếp dịch vụ</h1>");
            return false;
          }
      return true;

    }

    public static function miniusMail($id){
      $userMail = User::where('id', $id)->first();
      $newMail = (int)$userMail->email_remaining - 1;
      $userMail->update(['email_remaining' => $newMail]);
    }

}