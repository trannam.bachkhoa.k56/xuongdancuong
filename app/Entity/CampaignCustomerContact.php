<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class CampaignCustomerContact extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'campaign_customer_contact';

    protected $primaryKey = 'campaign_customer_contact_id';

    protected $fillable = [
        'campaign_customer_contact_id',
        'campaign_customer_id',
        'contact_id',
        'user_responsibility',
        'status',
        'created_at',
        'updated_at'
    ];

    public static function countCustomerContactUserIdMonthYear ($userId, $month, $year) {
        return static::join('campaign_customer', 'campaign_customer.campaign_customer_id', 'campaign_customer_contact.campaign_customer_id')
            ->where('user_responsibility', $userId)
            ->where('theme_code', ModelParent::getThemeCode())
            ->where('user_email', ModelParent::getEmailUser())
            ->whereMonth('campaign_customer_contact.created_at', $month)
            ->whereYear('campaign_customer_contact.created_at', $year)
            ->count();
    }

    
}
