<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 12/5/2017
 * Time: 2:07 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
//    use SoftDeletes;
//
//    protected $softDelete = true;
//
//    protected $dates = ['deleted_at'];

    protected $table = 'contact';

    protected $primaryKey = 'contact_id';

    protected $fillable = [
        'contact_id',
        'name',
        'phone',
        'email',
        'address',
        'color',
        'message',
        'tag',
        'money',
        /*
            0: khách hàng mới.
            1: Khách hàng tiếp cận.
            2: khách hàng mua hàng.
            3: khách hàng hoàn tiền.
        */
        'status',
        'affiliate_money',
        'affiliate_person',
        'campaign_id',
        'campaign_status',
		'utm_source',
        'created_at',
        'updated_at'
    ];

    public static function getContactByEmail($email) {
        $contact = new static();
        $contact = $contact->where('email', $email)->first();
        
        if (!empty($contact)) {
            return $contact;
        }
        return array();
    }

    public function insertOrUpdateCustomer($userEmail, $themeCode, $name, $phone, $email, $tag, $status, $message = ''){
        $contact = new self();
        $contactExist = $contact->where('user_email', $userEmail)->where('name', $name)->where('phone', $phone)->where('email', $email)->first();
        if (!empty($contactExist)) {
            $contact->update([
                'name' => isset($name) ? $name : '',
                'phone' => isset($phone) ? $phone : '',
                'email' => isset($email) ? $email : '',
                'tag' => isset($tag) ? $tag : '',
                'message' => $message,
                'status' => isset($status) ? $status : 0,
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ]);
            return $contactExist->contact_id;
        }

        $contactId = $contact->insertGetId([
            'name' => isset($name) ? $name : '',
            'phone' => isset($phone) ? $phone : '',
            'email' => isset($email) ? $email : '',
            'tag' => isset($tag) ? $tag : '',
            'message' => $message,
            'status' => isset($status) ? $status : 0,
            'theme_code' => $themeCode,
            'user_email'=> $userEmail,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime()
        ]);

        return $contactId;
    }
	
	public static function getAllContact (){
		$contacts = static::where('theme_code', ModelParent::getThemeCode())
		->where('user_email', ModelParent::getEmailUser())->get();
		
		return $contacts;
	}
}

