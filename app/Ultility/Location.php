<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/6/2017
 * Time: 2:15 PM
 */

namespace App\Ultility;


class Location
{
      private $locationMenu = array(
          'menu-chinh' => 'menu chính',
          'menu-phu-tren-cung' => 'menu phụ trên cùng',
          'side-left-menu' => 'sidebar menu trái',
          'side-right-menu' => 'sidebar menu phải',
          'footer-first' => 'chân trang thứ 1',
          'footer-second' => 'chân trang thứ 2',
          'footer-third' => 'chân trang thứ 3',
          'menu-footer' => 'menu cuối trang',
          'menu-category_product' => 'menu danh mục sản phẩm',
          'menu-product' => 'menu sản phẩm',
          'anh-menu' => 'ảnh menu',
          'sider-bar-blog' => 'sidebar blog menu trái',
          'menu-tab-index' => 'Tab menu sản phẩm trang chủ',
          'show-category-index' => 'hiển thị danh mục trang chủ',
          'show-product-home' => 'menu sản phẩm trang chủ',
          'show-single-home' => 'menu tin tức trang chủ',

      );

      public function getLocationMenu() {
          return $this->locationMenu;
      }
}
