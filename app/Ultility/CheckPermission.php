<?php
/**
 * Created by PhpStorm.
 * User: nam tran
 * Date: 5/13/2019
 * Time: 1:54 PM
 */

namespace App\Ultility;


use App\Entity\Contact;
use App\Entity\Domain;
use App\Entity\Order;
use App\Entity\Post;
use App\Entity\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    static $btnUpgrade = '<p ><a href="/admin/nang-cap-tai-khoan" style="width: 100%;" class="btn btn-warning">Nâng cấp tài khoản</a><p>';
    static $btnBuyMore = '<p ><a href="/admin/mua-them-dung-luong" style="width: 100%;" class="btn btn-primary">Mua thêm thời gian sử dụng</a><p>';

    // check xem còn hạn sử dụng không
    public function  checkVip($user) {

        // nếu vip là vip 0
        if ($user->vip == 0 || empty($user->vip)) {
            return $this->vip0($user);
        }

        // nếu vip là vip 1
        if ($user->vip == 1 ) {
            return $this->vip1($user);
        }


        // nếu vip là vip 2
        if ($user->vip == 2 ) {
            return $this->vip2($user);
        }
    }

    /* vip là vip 0:
        Tối đa 5 sản phẩm.
        Tối đa 50 đơn hàng.
        Tối đa 50 liên hệ.
        30 bài viết.
        Không gửi được mail thông báo.
    */
    private function vip0($user) {
        $message = "";
        // check sản phẩm
        $products = Post::where('user_email', $user->email)
            ->where('post_type', 'product')
            ->count();

        $message .= "<p>Sản phẩm: ".$products."/không giới hạn. </p>";

        // check bài viết
        $posts = Post::where('user_email', $user->email)
            ->where('post_type', 'post')->count();

        $message .= "<p>Tin đã đăng: ".$posts."/không giới hạn. </p>";

        // check đơn hàng
        $orders = Order::where('user_email', $user->email)
            ->count();

        $message .= "<p>Số đơn hàng: ".$orders."/không giới hạn. </p>";

        // check liên hệ
        $contacts = Contact::where('user_email', $user->email)
            ->count();

        $message .= "<p>khách hàng: ".$contacts."/5000. </p>";

        //$message.= static::$btnUpgrade;

        return $message;
    }

    /* vip là vip 1:
        Tối đa 150 sản phẩm.
        500 Đơn hàng.
        500 khách hàng 
        200 bài viết.
        Không gửi được mail thông báo.
        Có hạn sử dụng.
    */
    private function vip1($user) {
        $message = "";
        // check sản phẩm
        $products = Post::where('user_email', $user->email)
            ->where('post_type', 'product')
            ->count();

        $message .= "<p>Sản phẩm: ".$products."/Không giới hạn. </p>";

        // check bài viết
        $posts = Post::where('user_email', $user->email)
            ->where('post_type', 'post')->count();

        $message .= "<p>Tin đã đăng: ".$posts."/Không giới hạn. </p>";

		// check liên hệ
        $contacts = Contact::where('user_email', $user->email)
            ->count();
			
		$message .= "<p>Khách hàng: ".$contacts."/Không giới hạn. </p>";
			
        $domainUrl = Ultility::getCurrentDomain();
        $domain = Domain::where('url', $domainUrl)->first();
        if (!empty($domain)) {
            $datetime1 = new \DateTime();
            $datetime2 = new \DateTime($domain->end_at);
            $interval = $datetime1->diff($datetime2);

            $message .= "<p>Thời hạn sử dụng: ".$interval->format('%a ngày')." </p>";
        }

        //$message.= static::$btnUpgrade;
        $message.= static::$btnBuyMore;

        return $message;
    }

    private function vip2($user) {
        $message = "";
        // check sản phẩm
        $domainUrl = Ultility::getCurrentDomain();
        $domain = Domain::where('url', $domainUrl)->first();

        if (!empty($domain)) {
            $datetime1 = new \DateTime();
            $datetime2 = new \DateTime($domain->end_at);
            $interval = $datetime1->diff($datetime2);

            $message .= "<p>Thời hạn sử dụng: ".$interval->format('%a ngày')." </p>";
        }

        $message.= static::$btnBuyMore;

        return $message;
    }

    public static function checkProducts ($user) {
        if ($user->vip == 0 || empty($user->vip) ) {
            $productsVip = Post::where('user_email',$user->email)
                ->where('post_type', 'product')
                ->count();

            if ($productsVip >= 2000) {
                return "<p>Tài khoản của bạn đã vượt quá số sản phẩm được nhập. </p>".static::$btnUpgrade;
            }
        }

        return "";
    }

    public static function checkPosts ($user) {
        if ($user->vip == 0 || empty($user->vip) ) {
            $postsVip = Post::where('user_email',$user->email)
                ->where('post_type', 'post')
                ->count();

            if ($postsVip >= 10000) {
                return "<p>Tài khoản của bạn đã vượt quá số tin được đăng. </p>".static::$btnUpgrade;
            }
        }

        return "";
    }

    public static function checkOrders ($user) {
        if ($user->vip == 0 || empty($user->vip) ) {
            $ordersVip = Order::where('user_email',$user->email)
                ->count();

            if ($ordersVip >= 1000000) {
                return "<p>Tài khoản của bạn đã vượt quá số đơn hàng được nhận. Để xem được đơn hàng mới vui lòng: </p>".static::$btnUpgrade;
            }
        }


        return "";
    }

    public static function checkContacts ($user) {
        if ($user->vip == 0 || empty($user->vip) ) {
            $contactVip = Contact::where('user_email',$user->email)
                ->count();

            if ($contactVip >= 5000) {
                return "<p>Tài khoản của bạn đã vượt quá số liên hệ được nhận. Để xem được liên hệ mới vui lòng: </p>".static::$btnUpgrade;
            }
        }
		
        return "";
    }

}