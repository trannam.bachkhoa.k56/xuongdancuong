<?php
/**
 * Thư viện kiểm tra xem hệ thống có bị DDOS hay không
 *
 * Căn cứ vào lượng truy cập trong một khoảng thời gian để xác định xem đó là human
 * hay là autobot
 *
 * PHP version >= 5.3
 *
 * @category Getfly\Guard
 * @package
 * @author hoangduy <vuhoangduy@getflycrm.com>
 * @copyright GetFly JSC
 * @since Aug 3, 2016 5:49:16 PM
*/

namespace App\Ultility;

use App\Entity\MailConfig;

class AntiFlood {
	
	/**
	 * Số lượng request tối đa của một ip trong một lần truy cập
	 * @var CONTROL_MAX_REQUESTS;
	 */
	const CONTROL_MAX_REQUESTS = '5';
	/**
	* Khoảng Thời gian để tính  
	* @var CONTROL_REQ_TIMEOUT
	*/
	const CONTROL_REQ_TIMEOUT = 1 * 60;
	/**
	* Khoảng thời gian(s) không cho phép ip đó truy cập  
	* @var CONTROL_BAN_TIME
	*/
	const CONTROL_BAN_TIME = 10 * 60;
	/**
	* Thư mục chính chứa dữ liệu của Anti Flood, hiện đang để trong thư mục 
	* App/Data/
	* 
	* @var SCRIPT_TMP_DIR
	*/
	protected $SCRIPT_TMP_DIR = '';
	/**
	* IP của đối tượng request tới, lấy theo biến $_SERVER
	* @var USER_IP
	*/
	protected $USER_IP;
	/**
	* Thư mục lock
	* @var CONTROL_LOCK_DIR
	*/
	protected $CONTROL_LOCK_DIR = '';
	/**
	* File tương tứng với mỗi ip request tới
	* @var CONTROL_LOCK_FILE
	*/
	protected $CONTROL_LOCK_FILE = '';
	/**
	* File DB chứa dữ liệu truy cập đến server
	* @var CONTROL_DB
	*/
	protected $CONTROL_DB = '';
	
	public function __construct(){
		$this->USER_IP = $_SERVER['REMOTE_ADDR'];
		$this->SCRIPT_TMP_DIR = FCPATH. 'Data/';
		$this->CONTROL_LOCK_DIR = $this->SCRIPT_TMP_DIR. 'Lock/';
		$this->CONTROL_DB = $this->SCRIPT_TMP_DIR. 'ctrl';
		//checkValidUploadDir($this->CONTROL_LOCK_DIR);
		$this->CONTROL_LOCK_FILE = $this->CONTROL_LOCK_DIR. md5($this->USER_IP);
	}
	
	/**
	* Kiểm tra xem request hiện tại có phải tới từ một cuộc tấn công DDOS hệ thống 
	* hay không
	* Ví dụ: request liên tục trong một thời gian quá ngắn
	* 
	* @param: none
	* @return: true /false
	*
	* @author: hoangduy <vuhoangduy@getflycrm.com>
	* @since: Aug 4, 2016 3:28:03 PM
	*/
	
	public function isUnderAttack(){
		if(file_exists($this->CONTROL_LOCK_FILE)){
			if(time() - filemtime($this->CONTROL_LOCK_FILE) > self::CONTROL_BAN_TIME) {
				unlink($this->CONTROL_LOCK_FILE);
				return false;
			} else {
				touch($this->CONTROL_LOCK_FILE);
				return true;
			}
		} else {
			$control = Array();
			if(file_exists($this->CONTROL_DB)){
				$fh = fopen($this->CONTROL_DB, 'r');
				$control = array_merge($control, unserialize(fread($fh, filesize($this->CONTROL_DB))));
				fclose($fh);
			}
			
			if(isset($control[$this->USER_IP])){
				if(time()- $control[$this->USER_IP]['t'] < self::CONTROL_REQ_TIMEOUT) {
					$control[$this->USER_IP]['c']++;
				} else {
					$control[$this->USER_IP]['c'] = 1;
				}
			} else {
				$control[$this->USER_IP]['c'] = 1;
			}
			$control[$this->USER_IP]['t'] = time();
			
			if($control[$this->USER_IP]['c'] >= self::CONTROL_MAX_REQUESTS){
				$fh = fopen($this->CONTROL_LOCK_FILE, 'w');
				fwrite($fh, $this->USER_IP);
				fclose($fh);
			}
			// writing updated control table
			$fh = fopen($this->CONTROL_DB, 'w');
			fwrite($fh, serialize($control));
			fclose($fh);

			return false;
		}
	}
	/**
	* Gửi email cảnh báo tới tất cả các nhân viên liên quan khi có sự cố xảy ra
	* @param: 
	* @return: none
	*
	* @author: hoangduy <vuhoangduy@getflycrm.com>
	* @since: Aug 4, 2016 3:49:30 PM
	*/
	
	public function sendNotifyMail(){
		$mailer = new mailMoma();
		// Danh sách người nhận email
		$receivers = ['buituanviet@gmail.com'];
		// cc to
		$cc = [];
		// Tiêu đề email
		$title = 'Server ['. $_SERVER['SERVER_ADDR']. '] is under attack !!!';
		// Nội dung email
		$content = 'Subdomain - '. base_url();
		$content.= '<br/>IP ['. $this->USER_IP. '] đã truy cập quá '. self::CONTROL_MAX_REQUESTS;
		$content.= ' lần trong thời gian '. self::CONTROL_REQ_TIMEOUT. ' giây';
		if(isset($_SERVER['HTTP_REFERER'])){
			$content.= 'Referer: '. $_SERVER['HTTP_REFERER'];
		}
		// Gửi email cảnh báo
		$mailer->send($receivers, $title, $title, $content);

		// $telegram = new \Getfly\Telegram\PushNotify();
		// $telegram->send($telegram::DEFAULT_GROUP_ID, $content);
	}
}