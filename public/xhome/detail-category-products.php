

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>
<style>
header{
	position: static !important;
	box-shadow: 0 0 6px rgba(0,0,0,0.3);
}
</style>
<div></div>
<!-- End Header -->
<!-- Inside Page -->

<!-- Inside Page -->
<section class="v2_bnc_inside_page">
<div class="v2_bnc_content_top">
</div>

<section id="products-main" class="v2_bnc_products_page">
<!-- Breadcrumbs -->
<!-- Breadcrumbs -->
<div class="clearfix"></div>
<div class="v2_breadcrumb_main padding-top-10">
  <div class="container">
    <ul class="breadcrumb">
                        <li ><a href="http://xhome.com.vn">Trang chủ</a></li>
                              <li ><a href="http://xhome.com.vn/product.html">Sản Phẩm</a></li>
                              <li ><a href="http://xhome.com.vn/san-pham-le-2-1-278880.html">Sản phẩm lẻ</a></li>
                              <li ><a href="http://xhome.com.vn/ghe-2-1-278881.html">Ghế</a></li>
                </ul>
  </div>
</div>
<!-- End Breadcrumbs --> 
<div class="container">
<div class="row">
<div class="col-xs-12">
        <div class="v2_bnc_title_main">
        	<h1>Ghế</h1>
        </div>
                <div class="v2_bnc_cate_page_pr">
<ul class="v2_bnc_cate_page_list_pr ">
<li>
<a href="http://xhome.com.vn/ghe-ban-an-2-1-278882.html" title="Ghế bàn ăn">Ghế bàn ăn</a>
</li>
<li>
<a href="http://xhome.com.vn/ghe-bar-2-1-278883.html" title="Ghế bar">Ghế bar</a>
</li>
<li>
<a href="http://xhome.com.vn/ghe-thu-gian-2-1-278884.html" title="Ghế thư giãn">Ghế thư giãn</a>
</li>
</ul>
</div> 

<div class="v2_bnc_products_page_body col-xs-12 no-padding">
<div class="tab-content margin-top-10 row">
<ul class="f-product-viewid f-product ">
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786803">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-bar-laminate-1-1-786803.html" title="Ghế Bar LAMINATE">
                    <img alt="Ghế Bar LAMINATE" id="f-pr-image-zoom-id-tab-home-786803" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/10/1506744366_bar-laminate.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/10/1506744366_bar-laminate.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786803 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-bar-laminate-1-1-786803.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-bar-laminate-1-1-786803.html" title="Ghế Bar LAMINATE">Ghế Bar LAMINATE</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    2.550.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786802">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh52-1-1-786802.html" title="Ghế Laminate GH52">
                    <img alt="Ghế Laminate GH52" id="f-pr-image-zoom-id-tab-home-786802" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744289_gh52-2.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/08/1506744289_gh52-2.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786802 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh52-1-1-786802.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh52-1-1-786802.html" title="Ghế Laminate GH52">Ghế Laminate GH52</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.100.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786801">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh45-1-1-786801.html" title="Ghế Laminate GH45">
                    <img alt="Ghế Laminate GH45" id="f-pr-image-zoom-id-tab-home-786801" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/07/1506744209_gh45.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/07/1506744209_gh45.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786801 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh45-1-1-786801.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh45-1-1-786801.html" title="Ghế Laminate GH45">Ghế Laminate GH45</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.100.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786800">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh31-1-1-786800.html" title="Ghế Laminate GH31">
                    <img alt="Ghế Laminate GH31" id="f-pr-image-zoom-id-tab-home-786800" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/06/1506744136_gh31.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/06/1506744136_gh31.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786800 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh31-1-1-786800.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh31-1-1-786800.html" title="Ghế Laminate GH31">Ghế Laminate GH31</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.100.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786797">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh30-1-1-786797.html" title="Ghế Laminate GH30">
                    <img alt="Ghế Laminate GH30" id="f-pr-image-zoom-id-tab-home-786797" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/05/1506744064_gh30.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/05/1506744064_gh30.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786797 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh30-1-1-786797.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh30-1-1-786797.html" title="Ghế Laminate GH30">Ghế Laminate GH30</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.300.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786796">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh29-1-1-786796.html" title="Ghế Laminate GH29">
                    <img alt="Ghế Laminate GH29" id="f-pr-image-zoom-id-tab-home-786796" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/04/1506744010_gh29.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/04/1506744010_gh29.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786796 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh29-1-1-786796.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh29-1-1-786796.html" title="Ghế Laminate GH29">Ghế Laminate GH29</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.000.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786793">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh28-1-1-786793.html" title="Ghế Laminate GH28">
                    <img alt="Ghế Laminate GH28" id="f-pr-image-zoom-id-tab-home-786793" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/02/1506743935_gh28.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/02/1506743935_gh28.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786793 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh28-1-1-786793.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh28-1-1-786793.html" title="Ghế Laminate GH28">Ghế Laminate GH28</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.100.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786791">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh27-1-1-786791.html" title="Ghế Laminate GH27">
                    <img alt="Ghế Laminate GH27" id="f-pr-image-zoom-id-tab-home-786791" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/01/1506743864_gh27.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/01/1506743864_gh27.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786791 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh27-1-1-786791.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh27-1-1-786791.html" title="Ghế Laminate GH27">Ghế Laminate GH27</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.000.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786790">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh25-1-1-786790.html" title="Ghế Laminate GH25">
                    <img alt="Ghế Laminate GH25" id="f-pr-image-zoom-id-tab-home-786790" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/00/1506743818_gh25.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/00/1506743818_gh25.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786790 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh25-1-1-786790.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh25-1-1-786790.html" title="Ghế Laminate GH25">Ghế Laminate GH25</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.300.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786789">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh20-1-1-786789.html" title="Ghế Laminate GH20">
                    <img alt="Ghế Laminate GH20" id="f-pr-image-zoom-id-tab-home-786789" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/00/1506743770_gh20.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/04/00/1506743770_gh20.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786789 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh20-1-1-786789.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh20-1-1-786789.html" title="Ghế Laminate GH20">Ghế Laminate GH20</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.300.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786759">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh19-1-1-786759.html" title="Ghế Laminate GH19">
                    <img alt="Ghế Laminate GH19" id="f-pr-image-zoom-id-tab-home-786759" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/03/58/1506743700_gh19.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/03/58/1506743700_gh19.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786759 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh19-1-1-786759.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh19-1-1-786759.html" title="Ghế Laminate GH19">Ghế Laminate GH19</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.000.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-786758">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/ghe-laminate-gh16-1-1-786758.html" title="Ghế Laminate GH16">
                    <img alt="Ghế Laminate GH16" id="f-pr-image-zoom-id-tab-home-786758" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/03/57/1506743629_gh16.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/30/03/57/1506743629_gh16.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-786758 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/ghe-laminate-gh16-1-1-786758.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/ghe-laminate-gh16-1-1-786758.html" title="Ghế Laminate GH16">Ghế Laminate GH16</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    1.100.000                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
</ul>
<div class="v2_bnc_pagination">
    <div class="row">
        <div class="col-md-6 hidden">
            <p class="v2_bnc_pagination_title"> Hiển thị từ                <strong>1  </strong> đến                <strong>12    </strong> trên                <strong>79  </strong> bản ghi - Trang số                <strong>1   </strong> trên                <strong>7  </strong> trang            </p>
        </div>
        <div class="v2_bnc_pagination_button text-center col-md-12 margin-top-20 margin-bottom-20">
            <ul class="pagination">
                                    <li class="disabled"><a> ← </a></li>
                
                                                            <li class="active"><a >1</a></li>
                                                               
                                                            <li ><a href="/ghe-2-1-278881-p2.html">2</a></li>
                                                               
                                                            <li ><a href="/ghe-2-1-278881-p3.html">3</a></li>
                                                               
                                                            <li ><a href="/ghe-2-1-278881-p4.html">4</a></li>
                                                               
                                                            <li ><a href="/ghe-2-1-278881-p5.html">5</a></li>
                                                               
                                
                                    <li><a href="/ghe-2-1-278881-p2.html">→</a></li>
                            </ul>
            <div class="clearfix"></div>
        </div>
    </div>    
</div>
            </div>
</div>
</div>	
        </div>    
</div>	
</section> 
<!-- End Products page --></section>





<!-- End Inside Page -->
<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>

<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->


<script type="text/javascript" src="js/product.js"></script>
<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
