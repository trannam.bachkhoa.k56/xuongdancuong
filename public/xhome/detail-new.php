

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>
<style>
header{
	position: static !important;
	box-shadow: 0 0 6px rgba(0,0,0,0.3);
}
</style>
<div></div>
<!-- End Header -->
<!-- Inside Page -->
<section class="v2_bnc_inside_page">
<div class="v2_bnc_content_top">
</div>
    <div class="v2_breadcrumb_main padding-top-10"><div class="container"><ul class="breadcrumb"><li><a href="http://xhome.com.vn">Trang chủ</a></li><li><a href="">Trang chuyên mục</a></li><li><a href="http://xhome.com.vn/thiet-ke-va-thi-cong-noi-that-kien-truc-tai-hnhpqnth-1-7-7072.html">Thiết kế và Thi công nội thất, kiến &hellip;</a></li></ul></div></div><div class="v2_bnc_category_details_page padding-top-20 padding-bottom-20"><div class="container"><hgroup class="v2_bnc_title_main"><h2>Thiết kế và Thi công nội thất, kiến trúc tại HN,HP,QN,TH</h2></hgroup><time class="v2_bnc_create_time"><i class="fa fa-calendar-o"></i> Ngày đăng: 14:44:08 07-08-2017 </time><div class="v2_bnc_body_main"><div class="v2_bnc_category_details_page_detail"><span style="font-size:14px">&nbsp;* Dịch vụ tư vấn thiết kế &nbsp;đối với khách hàng lên trực tiếp showroom -&nbsp;chúng tôi có 2 gói dịch vụ như sau:<br  />
<br  />
A./ GÓI DỊCH VỤ TƯ VẤN THIẾT KẾ MỘT PHÒNG<br  />
<br  />
Quy trình triển khai công việc như sau<br  />
<br  />
1 - Khách hàng lên trực tiếp showroom xem sản phẩm mẫu, KTS của X&#39;HOME sẽ tư vấn cho khách sơ bộ về vật liệu, phụ kiện và giới thiệu tới khách hàng một số thiết kế sản phẩm tiêu biểu của X&#39;HOME cũng như ước toán sơ bộ chi phí thi công lắp đặt để khách hàng nắm rõ trước.<br  />
<br  />
2 - Khách hàng làm đơn đặt hàng thiết kế tại Showroom, giá trị đơn hàng là 2.000.000 (hai triệu đồng), nếu không gian phòng lớn kết hợp (Khách bếp) thì phí thiết kế 3.000.000 (ba triệu đồng)&nbsp;. Trong đơn hàng sẽ ghi rõ ngày hẹn đo khảo sát tại nhà và ngày giao trả thiết kế cho khách hàng.<br  />
<br  />
Đối với các khách hàng cho nhu cấp cấp bách về thời gian sản xuất giao hàng sau khi chốt thiết kế (nhập trạch, phòng cưới..vv) nhân viên tư vấn của X&#39;HOME sẽ ghi lại chi tiết trong đơn hàng và chốt lịch sản xuất với khách hàng ngay từ thời điểm này.<br  />
<br  />
3 - Theo ngày hẹn đo khảo sát KTS sẽ có mặt tại nhà khách hàng, chụp ảnh hiện trạng, lấy yêu cầu chi tiết của khách hàng<br  />
<br  />
4 - Sau khi lên Phương án phối cảnh xong, KTS sẽ gửi cho khách hàng.&nbsp;Ngay từ khi gửi phương án&nbsp;sơ bộ X&#39;HOME sẽ gửi kèm luôn cho khách hàng tổng chi phí nội thất&nbsp;ước tính của&nbsp;phương án đó để khách hàng có thể kết hợp với KTS ra được phương án thiết kế ưng ý mà không vượt quá chi phí dự kiến đầu tư của khách hàng.&nbsp;Nếu&nbsp;Khách hàng yêu cầu chỉnh sửa thêm sao cho có được phương án ưng ý với chi phí hợp lý nhất,&nbsp;KTS sẽ chỉnh sửa lại để đi đến phương án chốt sau cùng.<br  />
<br  />
5 - Sau khi chốt phương án, Phòng kinh doanh sẽ lên báo giá chi tiết gửi khách hàng. Nếu khách hàng đồng ý đơn giá thì X&#39;HOME sẽ triển khai&nbsp;ký hợp đồng thi công.<br  />
<br  />
6 - Nếu sau khi ký hợp đồng thi công 1 phòng xong mà khách hàng có chu cầu làm thêm 1 phòng khác&nbsp;thì X&#39;HOME sẽ tiến hành các bước tương tự như&nbsp;ở trên.<br  />
<br  />
&nbsp;<br  />
<br  />
B./ GÓI DỊCH VỤ TƯ VẤN THIẾT KẾ TOÀN BỘ CĂN HỘ (HOẶC TỪ 2 PHÒNG TRỞ LÊN)<br  />
<br  />
Quy trình triển khai công việc như sau<br  />
<br  />
1 - Khách hàng lên trực tiếp showroom xem sản phẩm mẫu, KTS của X&#39;HOME sẽ tư vấn cho khách sơ bộ về vật liệu, phụ kiện và giới thiệu tới khách hàng một số thiết kế sản phẩm tiêu biểu của X&#39;HOME cũng như ước toán sơ bộ chi phí thi công lắp đặt để khách hàng nắm rõ trước.<br  />
<br  />
2 - Khách hàng làm Hợp đồng thiết kế tại showroom. Chi phí thiết kế là 150.000/m2 (đối với các căn hộ diện tích trên 120m2 thì phí thiết kế là 120.000/m2), khách hàng đặt trước 70% giá trị hợp đồng ngay sau khi ký. Trong hợp đồng sẽ ghi rất rõ tiến độ triển khai công việc từ khảo sát đến hoàn thành thiết kế phối cảnh<br  />
<br  />
Đối với các khách hàng cho nhu cấp cấp bách về thời gian sản xuất giao hàng sau khi chốt thiết kế (nhập trạch, phòng cưới..vv) nhân viên tư vấn của X&#39;HOME sẽ ghi lại chi tiết trong đơn hàng và chốt lịch sản xuất với khách hàng ngay từ thời điểm này.<br  />
<br  />
3 - Theo ngày hẹn đo khảo sát KTS sẽ có mặt tại nhà khách hàng, chụp ảnh hiện trạng, lấy yêu cầu chi tiết của khách hàng<br  />
<br  />
4 - Sau khi lên Phương án phối cảnh xong, KTS sẽ gửi cho khách hàng.&nbsp;Ngay từ khi gửi phương án&nbsp;sơ bộ X&#39;HOME sẽ gửi kèm luôn cho khách hàng tổng chi phí nội thất&nbsp;ước tính của&nbsp;phương án đó để khách hàng có thể kết hợp với KTS ra được phương án thiết kế ưng ý mà không vượt quá chi phí dự kiến đầu tư của khách hàng.&nbsp;Nếu&nbsp;Khách hàng yêu cầu chỉnh sửa thêm sao cho có được phương án ưng ý với chi phí hợp lý nhất,&nbsp;KTS sẽ chỉnh sửa lại để đi đến phương án chốt sau cùng.<br  />
<br  />
5 - Sau khi chốt phương án, Phòng kinh doanh sẽ lên báo giá chi tiết gửi khách hàng. Nếu khách hàng đồng ý đơn giá và ký hợp đồng thi công, toàn bộ&nbsp;chi phí thiết kế trong Hợp đồng thiết kế&nbsp;sẽ được trừ hoàn toàn vào chi phí sản xuất.&nbsp;Phí thiết kế sẽ được giảm trừ hoàn toàn. Ngoài ra khách hàng còn được TRANG TRÍ TOÀN BỘ CĂN HỘ MIỄN PHÍ TRƯỚC KHI BÀN GIAO NHÀ, GIÁ TRỊ GÓI TRANG TRÍ TẶNG KHÁCH HÀNG&nbsp;TRỊ GIÁ 20 TRIỆU ĐẾN 50 TRIỆU&nbsp;- ĐÂY LÀ DỊCH VỤ CHỈ CÓ DUY NHẤT TẠI X&#39;HOME. Các sản phẩm trang trí gồm: Vật phẩm trang trí nhỏ, bình lọ hoa, các bộ khung tranh ảnh, ga gối, thảm...vvv để hiện thực hóa thiết kế 3D đến 100%. Đây là sự tri ân X&#39;HOME dành cho các khách hàng đã theo dõi và ủng hộ suốt những năm vừa qua.<br  />
<br  />
6 - Sau khi chốt phương án và nhận báo giá mà khách hàng không muốn đặt X&#39;HOME thi công thì chúng tôi sẽ hoàn tất các bản vẽ kỹ thuật chi tiết và bào giao hồ sơ thiết kế cho khách hàng, khách hàng sau đó hoàn tất thanh toán và thanh lý hợp đồng thiết kế<br  />
<br  />
&nbsp;<br  />
<br  />
* Dịch vụ tư vấn thiết kế &nbsp;đối với khách hàng không có thời gian&nbsp;lên trực tiếp showroom -&nbsp;chúng tôi có 2 gói dịch vụ như sau:<br  />
<br  />
A./ GÓI DỊCH VỤ TƯ VẤN THIẾT KẾ MỘT PHÒNG<br  />
<br  />
Quy trình triển khai công việc như sau<br  />
<br  />
1 - KTS của X&#39;HOME sẽ tư vấn cho khách sơ bộ về vật liệu, phụ kiện và giới thiệu tới khách hàng một số thiết kế sản phẩm tiêu biểu của X&#39;HOME cũng như ước toán sơ bộ chi phí thi công lắp đặt để khách hàng nắm rõ trước, quá trình này sẽ tiến hành qua điện thoại.<br  />
<br  />
2 - Nhân viên kinh doanh của X&#39;HOME sẽ gửi vào email cho khách hàng&nbsp;đơn đặt hàng thiết kế , giá trị đơn hàng là 2.000.000 (hai triệu đồng) nếu không gian phòng lớn kết hợp (Khách bếp) thì phí thiết kế 3.000.000 (ba triệu đồng). Trong đơn hàng sẽ ghi rõ ngày hẹn đo khảo sát tại nhà và ngày giao trả thiết kế cho khách hàng. Sau khi xác nhận chuyển khoản, X&#39;HOME sẽ bố trí KTS đến nhà khách hàng để khảo sát.<br  />
<br  />
Đối với các khách hàng cho nhu cấp cấp bách về thời gian sản xuất giao hàng sau khi chốt thiết kế (nhập trạch, phòng cưới..vv) nhân viên tư vấn của X&#39;HOME sẽ ghi lại chi tiết trong đơn hàng và chốt lịch sản xuất với khách hàng ngay từ thời điểm này.<br  />
<br  />
3 - Theo ngày hẹn đo khảo sát KTS sẽ có mặt tại nhà khách hàng, chụp ảnh hiện trạng, lấy yêu cầu chi tiết của khách hàng<br  />
<br  />
4 - Sau khi lên Phương án phối cảnh xong, KTS sẽ gửi cho khách hàng.&nbsp;Ngay từ khi gửi phương án&nbsp;sơ bộ X&#39;HOME sẽ gửi kèm luôn cho khách hàng tổng chi phí nội thất&nbsp;ước tính của&nbsp;phương án đó để khách hàng có thể kết hợp với KTS ra được phương án thiết kế ưng ý mà không vượt quá chi phí dự kiến đầu tư của khách hàng.&nbsp;Nếu&nbsp;Khách hàng yêu cầu chỉnh sửa thêm sao cho có được phương án ưng ý với chi phí hợp lý nhất,&nbsp;KTS sẽ chỉnh sửa lại để đi đến phương án chốt sau cùng.<br  />
<br  />
5 - Sau khi chốt phương án, Phòng kinh doanh sẽ lên báo giá chi tiết gửi khách hàng. Nếu khách hàng đồng ý đơn giá X&#39;HOME sẽ triển khai&nbsp;ký hợp đồng thi công.<br  />
<br  />
6 - Nếu sau khi ký hợp đồng thi công 1 phòng xong mà khách hàng có chu cầu làm thêm 1 phòng khác&nbsp;thì X&#39;HOME sẽ triển khai các bước tương tự ở trên.<br  />
<br  />
&nbsp;<br  />
<br  />
B./ GÓI DỊCH VỤ TƯ VẤN THIẾT KẾ TOÀN BỘ CĂN HỘ (HOẶC TỪ 2 PHÒNG TRỞ LÊN)<br  />
<br  />
Quy trình triển khai công việc như sau<br  />
<br  />
1 -&nbsp;KTS của X&#39;HOME sẽ tư vấn cho khách sơ bộ về vật liệu, phụ kiện và giới thiệu tới khách hàng một số thiết kế sản phẩm tiêu biểu của X&#39;HOME cũng như ước toán sơ bộ chi phí thi công lắp đặt để khách hàng nắm rõ trước, quá trình này sẽ tiến hành qua điện thoại.<br  />
<br  />
2 - Nhân viên kinh doanh của X&#39;HOME sẽ gửi vào email cho khách hàng&nbsp;Hợp đồng&nbsp;thiết kế , Phí thiết kế là 150.000/m2 (đối với các căn hộ trên 120m2 thì phí thiết kế là 120.000/m2), khách hàng sẽ chuyển khoản trước 70% giá trị Hợp đồng. Trong Hợp đồng&nbsp;sẽ ghi rõ ngày hẹn đo khảo sát tại nhà và ngày giao trả thiết kế cho khách hàng. Sau khi xác nhận chuyển khoản, X&#39;HOME sẽ bố trí KTS đến nhà khách hàng để khảo sát.<br  />
<br  />
Đối với các khách hàng cho nhu cấp cấp bách về thời gian sản xuất giao hàng sau khi chốt thiết kế (nhập trạch, phòng cưới..vv) nhân viên tư vấn của X&#39;HOME sẽ ghi lại chi tiết trong đơn hàng và chốt lịch sản xuất với khách hàng ngay từ thời điểm này.<br  />
<br  />
3 - Theo ngày hẹn đo khảo sát KTS sẽ có mặt tại nhà khách hàng, chụp ảnh hiện trạng, lấy yêu cầu chi tiết của khách hàng<br  />
<br  />
4 - Sau khi lên Phương án phối cảnh xong, KTS sẽ gửi cho khách hàng.&nbsp;Ngay từ khi gửi phương án&nbsp;sơ bộ X&#39;HOME sẽ gửi kèm luôn cho khách hàng tổng chi phí nội thất&nbsp;ước tính của&nbsp;phương án đó để khách hàng có thể kết hợp với KTS ra được phương án thiết kế ưng ý mà không vượt quá chi phí dự kiến đầu tư của khách hàng.&nbsp;Nếu&nbsp;Khách hàng yêu cầu chỉnh sửa thêm sao cho có được phương án ưng ý với chi phí hợp lý nhất,&nbsp;KTS sẽ chỉnh sửa lại để đi đến phương án chốt sau cùng.<br  />
<br  />
5 - Sau khi chốt phương án, Phòng kinh doanh sẽ lên báo giá chi tiết gửi khách hàng. Nếu khách hàng đồng ý đơn giá và ký hợp đồng thi công, toàn bộ&nbsp;chi phí thiết kế trong Hợp đồng thiết kế&nbsp;sẽ được trừ hoàn toàn vào chi phí sản xuất.&nbsp;Phí thiết kế sẽ được giảm trừ hoàn toàn. Ngoài ra khách hàng còn được TRANG TRÍ TOÀN BỘ CĂN HỘ MIỄN PHÍ TRƯỚC KHI BÀN GIAO NHÀ, GIÁ TRỊ GÓI TRANG TRÍ TẶNG KHÁCH HÀNG&nbsp;TRỊ GIÁ 20 TRIỆU ĐẾN 50 TRIỆU&nbsp;- ĐÂY LÀ DỊCH VỤ CHỈ CÓ DUY NHẤT TẠI X&#39;HOME. Các sản phẩm trang trí gồm: Vật phẩm trang trí nhỏ, bình lọ hoa, các bộ khung tranh ảnh, ga gối, thảm...vvv để hiện thực hóa thiết kế 3D đến 100%. Đây là sự tri ân X&#39;HOME dành cho các khách hàng đã theo dõi và ủng hộ suốt những năm vừa qua.<br  />
<br  />
6 - Sau khi chốt phương án và nhận báo giá mà khách hàng không muốn đặt X&#39;HOME thi công thì chúng tôi sẽ hoàn tất các bản vẽ kỹ thuật chi tiết và bào giao hồ sơ thiết kế cho khách hàng, khách hàng sau đó hoàn tất thanh toán và thanh lý hợp đồng thiết kế<br  />
<br  />
HỆ THỐNG CHI NHÁNH VÀ SHOWROOM CỦA X&#39;HOME TRÊN TOÀN QUỐC:<br  />
<br  />
☀&nbsp;HÀ NỘI: SHOWROOM X&#39;HOME TÒA NHÀ 168 ĐƯỜNG LÁNG - P THỊNH QUANG - Q ĐỐNG ĐA<br  />
SĐT Phòng Kinh Doanh: 0466718333<br  />
<br  />
☀&nbsp;HẢI PHÒNG: SHOWROOM X&#39;HOME SỐ 130 P. CÁT CỤT - Q. LÊ CHÂN<br  />
SĐT Phòng Kinh Doanh: 0313 787 383<br  />
☀&nbsp;QUẢNG NINH: TẦNG 1 - CC XANH - ĐƯỜNG 25/4 - HÒN GAI - HẠ LONG<br  />
SĐT Phòng Kinh Doanh: 0333 825 583<br  />
☀&nbsp;SÀI GÒN: SHOWROOM X&#39;HOME SỐ 5 - TRẦN NÃO - P BÌNH AN - QUẬN 2<br  />
SĐT Phòng Kinh Doanh: 08 6656 2288<br  />
☀&nbsp;MIỀN TRUNG: SHOWROOM X&#39;HOME SỐ 57 HẠC THÀNH - TP THANH HÓA</span></div><div class="v2_bnc_tags hidden"><span><i class="fa fa-tags"></i> Tags:</span></div></div></div></div></section>
<!-- End Inside Page -->
<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>

<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->


<script type="text/javascript" src="js/product.js"></script>
<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
