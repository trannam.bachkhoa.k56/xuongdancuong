

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 


<link rel="stylesheet" type="text/css" href="css/_all.css"/>
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>
<!-- Content -->

 <style type="text/css">
		.wrapper {
			padding-top: 100px;
			margin-top: 30px;
			margin-bottom: 30px;
		}
		@media (min-width: 768px)
		{
			.wrapper {
				padding-top: 100px;
				margin-top: 150px;
				margin-bottom: 30px;
			}
		}

         .help-block-error2{
         display: block !important;
         margin-top: -12px !important;
         margin-bottom: 0px !important;
         font-size: 11px !important;
         }
         .focus{
         border: red solid 1px;
         }
         ul.bankList {
         clear: both;
         height: 202px;
         width: 636px;
         }
         ul.bankList li {
         list-style-position: outside;
         list-style-type: none;
         cursor: pointer;
         float: left;
         margin-right: 0;
         padding: 5px 2px;
         text-align: center;
         width: 90px;
         }
         i.VISA, i.MASTE, i.AMREX, i.JCB, i.VCB, i.TCB, i.MB, i.VIB, i.ICB, i.EXB, i.ACB, i.HDB, i.MSB, i.NVB, i.DAB, i.SHB, i.OJB, i.SEA, i.TPB, i.PGB, i.BIDV, i.AGB, i.SCB, i.VPB, i.VAB, i.GPB, i.SGB,i.NAB,i.BAB 
         { width:80px; height:30px; display:block; background:url(https://www.nganluong.vn/webskins/skins/nganluong/checkout/version3/images/bank_logo.png) no-repeat;}
         i.MASTE { background-position:0px -31px}
         i.AMREX { background-position:0px -62px}
         i.JCB { background-position:0px -93px;}
         i.VCB { background-position:0px -124px;}
         i.TCB { background-position:0px -155px;}
         i.MB { background-position:0px -186px;}
         i.VIB { background-position:0px -217px;}
         i.ICB { background-position:0px -248px;}
         i.EXB { background-position:0px -279px;}
         i.ACB { background-position:0px -310px;}
         i.HDB { background-position:0px -341px;}
         i.MSB { background-position:0px -372px;}
         i.NVB { background-position:0px -403px;}
         i.DAB { background-position:0px -434px;}
         i.SHB { background-position:0px -465px;}
         i.OJB { background-position:0px -496px;}
         i.SEA { background-position:0px -527px;}
         i.TPB { background-position:0px -558px;}
         i.PGB { background-position:0px -589px;}
         i.BIDV { background-position:0px -620px;}
         i.AGB { background-position:0px -651px;}
         i.SCB { background-position:0px -682px;}
         i.VPB { background-position:0px -713px;}
         i.VAB { background-position:0px -744px;}
         i.GPB { background-position:0px -775px;}
         i.SGB { background-position:0px -806px;}
         i.NAB { background-position:0px -837px;}
         i.BAB { background-position:0px -868px;}
         ul.cardList li {
         cursor: pointer;
         float: left;
         margin-right: 0;
         padding: 0px;
         text-align: center;
         width: 82px;
         box-shadow:none !important;
         margin: 3px 3px;
         border:1px solid #eee;
         }
         .bank-online-methods .iradio_minimal-blue{
         margin: 0 !important;
         display: none;
         }
         .bank-online-methods label{
         padding-left: 0 !important;
         }
         .bank-online-methods:hover{
         border:#0EB2EF solid 1px!important;
         }
      </style>
  <div id="fb-root"></div>
      <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, "script", "facebook-jssdk"));
      </script>
      <input type="hidden" name="nxtinfo" data-info-order="BNC1515461998" data-info-web="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam" data-info-phone="1900636558" data-info-address="Số 168 Đường Láng - P Thịnh Quang - Q Đống Đa">
      <input type="hidden" name="link_home" id="link_home" value="http://xhome.com.vn">
      <div id="loadding">
         <div class="small1">
            <div class="small ball smallball1"></div>
            <div class="small ball smallball2"></div>
            <div class="small ball smallball3"></div>
            <div class="small ball smallball4"></div>
         </div>
         <div class="small2">
            <div class="small ball smallball5"></div>
            <div class="small ball smallball6"></div>
            <div class="small ball smallball7"></div>
            <div class="small ball smallball8"></div>
         </div>
         <div class="bigcon">
            <div class="big ball"></div>
         </div>
      </div>
      <div class="wrapper">
         <div id="header">
            <div class="header-top">
               <div class="container">
                  <div class="row">
                     <!-- <div class="header-left col-md-6">
                        <ul class="">
                        <li><a href="#"></a>
                        </li>
                        <li><a href=""></a></li>
                        <li><a href=""></a></li>
                        <li><a href=""></a></li>
                        </ul>
                        </div>
                        <div class="header-right col-md-6">
                        <ul>
                        <li><a href="#">Thông báo</a></li>
                        <li><a href="#">Hỗ trợ</a></li>
                        <li><a href="#">Đăng ký</a></li>
                        <li><a href="#">Đăng nhập</a></li>
                        </ul>
                        </div> -->
                  </div>
               </div>
            </div>
            <div class="cf-logo">
               <div class="container">
                 
               </div>
            </div>
         </div>
         <div class="cf-main">
            <div class="container">
               <div class="row">
                  <form id="onePageSubmit" name="onePageSubmit" method="POST" action="http://xhome.com.vn/payment-oncepage-orderSubmit.html">
                     <div class="col-md-4 col-sm-4 col-xs-12">
                        <div  class="cf-title address-list">
                           <div class="top"><span class="icon">1</span>Địa chỉ</div>
                           <div class="cf-middle-content">
                              <div class="form-group">
                                 <div class="input-icon right inner-addon right-addo">
                                    <i class="glyphicon "></i>
                                    <input type="text" name="cus[name]" class="form-control" placeholder="Họ và tên" >
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="input-icon right">
                                    <i class="glyphicon "></i>
                                    <input type="text" name="cus[email]" class="form-control" placeholder="Email" value="">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="input-icon right">
                                    <i class="glyphicon "></i>
                                    <input type="text" name="cus[phone]" class="form-control" placeholder="Số điện thoại" value="">
                                 </div>
                              </div>
                              <div class="row BNC_address_genral" >
                                 <div class="col-md-6 form-group ">
                                    <select id="cityId" name="cus[cityId]" class="form-control BNC_cityId">
                                       <option name="" value="">Tỉnh/thành phố</option>
                                       <option name="An Giang" value="89" >An Giang</option>
                                       <option name="Bà Rịa - Vũng Tàu" value="77" >Bà Rịa - Vũng Tàu</option>
                                       <option name="Bắc Giang" value="24" >Bắc Giang</option>
                                       <option name="Bắc Kạn" value="06" >Bắc Kạn</option>
                                       <option name="Bạc Liêu" value="95" >Bạc Liêu</option>
                                       <option name="Bắc Ninh" value="27" >Bắc Ninh</option>
                                       <option name="Bến Tre" value="83" >Bến Tre</option>
                                       <option name="Bình Dương" value="74" >Bình Dương</option>
                                       <option name="Bình Phước" value="70" >Bình Phước</option>
                                       <option name="Bình Thuận" value="60" >Bình Thuận</option>
                                       <option name="Bình Định" value="52" >Bình Định</option>
                                       <option name="Cà Mau" value="96" >Cà Mau</option>
                                       <option name="Cần Thơ" value="92" >Cần Thơ</option>
                                       <option name="Cao Bằng" value="04" >Cao Bằng</option>
                                       <option name="Gia Lai" value="64" >Gia Lai</option>
                                       <option name="Hà Giang" value="02" >Hà Giang</option>
                                       <option name="Hà Nam" value="35" >Hà Nam</option>
                                       <option name="Hà Nội" value="01" >Hà Nội</option>
                                       <option name="Hà Tĩnh" value="42" >Hà Tĩnh</option>
                                       <option name="Hải Dương" value="30" >Hải Dương</option>
                                       <option name="Hải Phòng" value="31" >Hải Phòng</option>
                                       <option name="Hậu Giang" value="93" >Hậu Giang</option>
                                       <option name="Hồ Chí Minh" value="79" >Hồ Chí Minh</option>
                                       <option name="Hòa Bình" value="17" >Hòa Bình</option>
                                       <option name="Hưng Yên" value="33" >Hưng Yên</option>
                                       <option name="Khánh Hòa" value="56" >Khánh Hòa</option>
                                       <option name="Kiên Giang" value="91" >Kiên Giang</option>
                                       <option name="Kon Tum" value="62" >Kon Tum</option>
                                       <option name="Lai Châu" value="12" >Lai Châu</option>
                                       <option name="Lâm Đồng" value="68" >Lâm Đồng</option>
                                       <option name="Lạng Sơn" value="20" >Lạng Sơn</option>
                                       <option name="Lào Cai" value="10" >Lào Cai</option>
                                       <option name="Long An" value="80" >Long An</option>
                                       <option name="Nam Định" value="36" >Nam Định</option>
                                       <option name="Nghệ An" value="40" >Nghệ An</option>
                                       <option name="Ninh Bình" value="37" >Ninh Bình</option>
                                       <option name="Ninh Thuận" value="58" >Ninh Thuận</option>
                                       <option name="Phú Thọ" value="25" >Phú Thọ</option>
                                       <option name="Phú Yên" value="54" >Phú Yên</option>
                                       <option name="Quảng Bình" value="44" >Quảng Bình</option>
                                       <option name="Quảng Nam" value="49" >Quảng Nam</option>
                                       <option name="Quảng Ngãi" value="51" >Quảng Ngãi</option>
                                       <option name="Quảng Ninh" value="22" >Quảng Ninh</option>
                                       <option name="Quảng Trị" value="45" >Quảng Trị</option>
                                       <option name="Sóc Trăng" value="94" >Sóc Trăng</option>
                                       <option name="Sơn La" value="14" >Sơn La</option>
                                       <option name="Tây Ninh" value="72" >Tây Ninh</option>
                                       <option name="Thái Bình" value="34" >Thái Bình</option>
                                       <option name="Thái Nguyên" value="19" >Thái Nguyên</option>
                                       <option name="Thanh Hóa" value="38" >Thanh Hóa</option>
                                       <option name="Thừa Thiên Huế" value="46" >Thừa Thiên Huế</option>
                                       <option name="Tiền Giang" value="82" >Tiền Giang</option>
                                       <option name="Trà Vinh" value="84" >Trà Vinh</option>
                                       <option name="Tuyên Quang" value="08" >Tuyên Quang</option>
                                       <option name="Vĩnh Long" value="86" >Vĩnh Long</option>
                                       <option name="Vĩnh Phúc" value="26" >Vĩnh Phúc</option>
                                       <option name="Yên Bái" value="15" >Yên Bái</option>
                                       <option name="Đà Nẵng" value="48" >Đà Nẵng</option>
                                       <option name="Đắk Lắk" value="66" >Đắk Lắk</option>
                                       <option name="Đắk Nông" value="67" >Đắk Nông</option>
                                       <option name="Điện Biên" value="11" >Điện Biên</option>
                                       <option name="Đồng Nai" value="75" >Đồng Nai</option>
                                       <option name="Đồng Tháp" value="87" >Đồng Tháp</option>
                                    </select>
                                    <input type="hidden" value="" name="cus[cityname]"/>
                                 </div>
                                 <div class="col-md-6 form-group">
                                    <select id="districtId"  disabled name="cus[districtId]" class="form-control BNC_districtId">
                                       <option value="">Quận/Huyện</option>
                                    </select>
                                    <input type="hidden" value="" name="cus[districtname]"/>
                                 </div>
                                 <div class="col-md-12 form-group">
                                    <textarea class="form-control" name="cus[address]" placeholder="Số nhà, đường/phố, tòa nhà, xã/phường ,....... Vui lòng điền đầy đủ thông tin." required></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="clear"></div>
                        <div  class="cf-ship address-list">
                           <div class="ship-title">
                              <span><i class="fa fa-truck"></i> Dịch vụ giao hàng </span>
                           </div>
                           <style type="text/css">
                              .ulCarrier li{
                              list-style: none;
                              border: #F5F6F7 solid 1px;
                              padding: 10px;
                              margin-left: -40px;
                              padding-bottom: 0px;
                              margin-bottom: 7px;
                              }
                           </style>
                           <div id="methodShipping">
                              <!--phí vận chuyển-->
                              <p class="text-danger">Vui Lòng chọn địa điểm cần giao hàng tới.</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-4 col-xs-12 boderleft">
                        <div class="cf-title">
                           <span class="icon">2</span>Phương thức thanh toán
                        </div>
                        <div class="content">
                           <!-- <div id="ttonline_image"></div> -->
                           <!-- Trả góp -->
                        </div>
                     </div>
                     <div id="BNC_product_list" class="col-md-4 col-sm-4 col-xs-12 boderleft">
                        <div class="cf-title">
                           <span class="icon"><i class="icon_bill"></i></span>
                           Hóa đơn mua hàng<span class="icon print submitPrint"><i class="fa fa-print" aria-hidden="true"></i></span>
                        </div>
                        <div class="content rowNoMargin">
                           Danh sách sản phẩm
                           <div class="list-product row rowNoMargin">
                              <div class="img col-md-3 col-sm-4 col-xs-3">
                                 <a  href="http://xhome.com.vn/xhomever2-website-1-1-786803.html" target="_blank" tilte="Ghế Bar LAMINATE"><img  style="height: 45px !important;width:70px;" class="img-responsive" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/10/1506744366_bar-laminate.jpg.jpg"alt="Ghế Bar LAMINATE" title="Ghế Bar LAMINATE"></a>
                              </div>
                              <div class="product-info col-md-9 col-sm-8 col-xs-9">
                                 <div class="product-name">
                                    <a href="http://xhome.com.vn/xhomever2-website-1-1-786803.html" target="_blank" data-unit="" data-model="" data-name="Ghế Bar LAMINATE">Ghế Bar LAMINATE</a>
                                    <input type="hidden" name="namesp" id="namesp0" value="Ghế Bar LAMINATE" />
                                 </div>
                                 <div class="product_quantity">
                                    <span class="spanQuantitySelect" >
                                    <input type="number" style="width:50px;outline: none;" class="BNC_product_quantity" value="1" order-product-id="214600" product-id="786803">
                                    <input type="hidden" name="soluongsp" id="soluongsp0" value="1" />
                                    </span>
                                    <span class="spanQuantityPrice">&nbsp;x 2.550.000 đ</span>
                                    <input type="hidden" name="pricesp" id="pricesp0" value="2.550.000 đ" />
                                    <span class="spanQuantityPrice" style="margin-left: 55px;">= 2.550.000 đ</span>
                                    <br/>
                                    <span class="removeProduct" data-product-id="786803" data-order-id="190042" order-product-id="214600">
                                    <span class="fa fa-trash"></span>
                                    </span>
                                 </div>
                              </div>
                           </div>
                           <input type="hidden" name="" id="idk" value="1">
                           <div class="dola">
                              <div class="dola1">
                                 <span>Tổng tiền:</span>
                                 <span> 2.550.000 đ</span>
                              </div>
                              <input type="hidden" name="sub_total" id="sub_total" value="2.550.000 đ">
                              <div class="dola1">
                                 <span>Phí ship:</span>
                                 <span class="feeShip"> Liên hệ</span>
                              </div>
                              <div class="dola1">
                                 <span>Phí Cod ( thu hộ ):</span>
                                 <span class="feeCod"> Liên hệ</span>
                              </div>
                              <input type="hidden" name="shippingFee" id="shippingFee" value="Liên hệ">
                              <div class="dola1">
                                 <span>Mã giảm giá:</span>
                                 <span class="coupon">
                                 <input class="form-control" name="coupon" value="" placeholder="Mã giảm giá">
                                 <button type="button" class="btn btn-success btn-xs check_coupon"><i class="fa fa-check-circle-o"></i> Kiểm tra</button>
                                 </span>
                              </div>
                              <div class="dola1 divCost_coupon" style="display:none;">
                                 <span class="text-success cost_coupon" style="text-align: right;">
                                 </span>
                              </div>
                              <div class="dola1 end-dola">
                                 <span>Tổng tiền đơn hàng: </span>
                                 <span>2.550.000 đ</span>
                              </div>
                              <input type="hidden" name="total_order" id="total_order" value="2.550.000 đ">
                              <textarea name="cus[comment]" class="form-control" placeholder="Ghi chú thêm"></textarea>
                           </div>
                        </div>
                        <div class="dathang">
                           <button type="button" class="backOrder" onclick="window.location.href='/'"><i class="fa fa-arrow-left" aria-hidden="true"></i> Mua thêm</button>
                           <button type="submit" class="submitOrder"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Đặt hàng</button>
                           <button type="button" style="display: none;" id="sendInstallment"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Trả Góp</button>
                           <br>
                           <span id="alert"></span>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- Modal Login -->
      <div class="modal fade" id="BNC_login_vg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Đăng nhập tài khoản Bảo Kim</h4>
               </div>
               <div class="modal-body" style="overflow:hidden">
                  <div class="form-group" id="BNC_login_error"></div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Tên đăng nhập</label>
                     <div class="col-md-9">
                        <input id="username" name="username" class="form-control" placeholder="Tên đăng nhập" type="text">
                     </div>
                  </div>
                  <div class="form-group"></div>
                  <div class="form-group">
                     <label class="col-md-3 control-label">Mật khẩu</label>
                     <div class="col-md-9">
                        <input id="password" name="password" class="form-control" placeholder="Mật khẩu" type="password">
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <button id="BNC_login_idvg" type="button" class="btn btn-primary">Đăng nhập</button>
               </div>
            </div>
         </div>
      </div>
      <script>
         var lang_PleaseDelivery="Vui Lòng chọn địa điểm cần giao hàng tới.";
         
      </script>

<script>
    // Get the modal Adv Popup Home
     var modal = document.getElementById('myModal');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.close').click(function(){
        $('.modal').css('display','none');
    });
  });
</script>


<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>
<script> 
jQuery(function ($) { 
    $('#show').hide();
    $('#close').click(function (e) {
      $('#rich-media').hide('slow');
      return false;
    });
    $('#hide').click(function (e) {
      $('#show').show('slow');
      $('#hide').hide('slow');
      $('#rich-media-item').height(0);
      return false;
    });
    $('#show').click(function (e) {
      $('#hide').show('slow'); 
      $('#show').hide('slow');
      $('#rich-media-item').height(205);
      return false;
   });
});
</script>
<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->




<!-- End Full Code -->
<script type="text/javascript" src="js/product.js"></script>

<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
