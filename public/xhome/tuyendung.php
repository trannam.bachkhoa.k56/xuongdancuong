

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>


<style>
header{
	position: static !important;
	box-shadow: 0 0 6px rgba(0,0,0,0.3);
}
</style>
<div></div>
<!-- End Header -->
<!-- Inside Page -->
<section class="v2_bnc_inside_page">
   <div class="v2_bnc_content_top"></div>
   <div class="v2_breadcrumb_main padding-top-10">
      <div class="container">
         <ul class="breadcrumb">
            <li><a href="http://xhome.com.vn">Trang chủ</a></li>
            <li><a href="http://xhome.com.vn/recruit.html">Tuyển dụng</a></li>
         </ul>
      </div>
   </div>
   <div class="v2_bnc_recruit_page">
      <div class="container">
         <div class="v2_bnc_title_main">
            <h1>Tuyển dụng</h1>
         </div>
         <div class="v2_bnc_cate_page hidden">
            <ul class="v2_bnc_cate_page_list row">
               <li class="col-md-3 col-sm-6 col-xs-6 full-xs" style="margin-bottom:5px"><a href="http://xhome.com.vn/recruit.html?job=Kiến trúc-TK nội thất" title="Kiến trúc-TK nội thất">Kiến trúc-TK nội thất</a></li>
               <li class="col-md-3 col-sm-6 col-xs-6 full-xs" style="margin-bottom:5px"><a href="http://xhome.com.vn/recruit.html?job=Thiết kế-Mỹ thuật" title="Thiết kế-Mỹ thuật">Thiết kế-Mỹ thuật</a></li>
            </ul>
            <div class="clearfix"></div>
         </div>
         <div class="v2_bnc_description_page hidden" style="background-image:url(http://s2.webbnc.vn/uploadv2/web/18/180/recruit/2015/07/21/02/29/1437463627_131115hinh-nen-desktop-anh-nang-chieu-sang-nu-hoa..jpg);display: inline-block;">
            <div class="row">
               <div class="col-md-3 col-sm-6 col-xs-12"><img class="img-thumbnail img-responsive" src="https://cdn-img-v2.webbnc.net/uploadv2/web/18/180/recruit/2015/07/02/03/45/1435826625_khach-hang.jpeg_resize170x170.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/18/180/recruit/2015/07/02/03/45/1435826625_khach-hang.jpeg&mode=resize&size=170x170'"onerror="this.onerror=null;this.src='{loadImage upload/web/1/1/no-img.gif crop 170 170}'"></div>
               <div class="col-md-9 col-sm-6 col-xs-12">
                  <p>Tuyển dụng nhanh, tuyển dụng hiệu quả nhất trong 24h. Hàng trăm nghìn nhà tuyển dụng đăng việc làm mỗi ngày. Hàng triệu người cập nhật hồ sơ liên tục...
               </div>
            </div>
         </div>
         <div class="v2_bnc_recruit_page_body">
            <div class="row hidden">
               <form id="recruit_filter" method="get" action="http://xhome.com.vn/recruit.html" class="v2_bnc_filter_page">
                  <div class="col-md-3 col-sm-6 col-xs-6 full-xs">
                     <select class="form-control" name="sort">
                        <option selected="selected" value="new">Mới</option>
                        <option value="hot">Nổi bật</option>
                        <option value="az">A-Z</option>
                     </select>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-6 full-xs"><input type="text" name="q" placeholder="Tìm theo tiêu đề" class="form-control" value=""></div>
                  <div class="col-md-3 col-sm-6 col-xs-6 full-xs">
                     <select class="form-control" name="limit">
                        <option value="20" >Số lượng hiện thị</option>
                        <option value="20" >20</option>
                        <option value="30" >30</option>
                        <option value="40" >40</option>
                        <option value="50" >50</option>
                     </select>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-6 full-xs"><button type="submit" class="btn btn-warning">Tìm kiếm</button></div>
               </form>
            </div>
            <div class="v2_bnc_recruit_page_list text-center">
               <div class="table-responsive">
                  <table class="table table-bordered table-striped">
                     <tr>
                        <th style= "text-align:center;width: 5%;">STT</th>
                        <th style= "text-align:center;width: 30%;">Tên công việc</th>
                        <th style= "text-align:center">Mức lương</th>
                        <th style= "text-align:center">Hạn nộp hồ sơ</th>
                        <th style= "text-align:center">Nơi làm việc</th>
                     </tr>
                     <tr>
                        <td>1</td>
                        <td width ="150" style="text-align:left;"><a href="http://xhome.com.vn/nhan-vien-giam-sat-thi-cong-1-5-50890.html" title="Nhân viên giám sát thi công">Nhân viên giám sát thi công</a></td>
                        <td width ="120">trên 10 triệu</td>
                        <td width = "120">07-08-2017</td>
                        <td>Hà Nội</td>
                     </tr>
                     <tr>
                        <td>2</td>
                        <td width ="150" style="text-align:left;"><a href="http://xhome.com.vn/nhan-vien-du-toan-tai-ha-noi-1-5-50889.html" title="Nhân viên dự toán tại Hà nội">Nhân viên dự toán tại Hà nội</a></td>
                        <td width ="120">Thỏa Thuận</td>
                        <td width = "120">07-08-2017</td>
                        <td>Hà Nội</td>
                     </tr>
                     <tr>
                        <td>3</td>
                        <td width ="150" style="text-align:left;"><a href="http://xhome.com.vn/nhan-vien-boc-tach-ban-ve-ky-thuat-tai-ha-noi-1-5-50888.html" title="Nhân viên bóc tách bản vẽ kỹ thuật tại Hà nội">Nhân viên bóc tách bản vẽ kỹ thuật tại Hà nội</a></td>
                        <td width ="120">trên 10 triệu</td>
                        <td width = "120">07-08-2017</td>
                        <td>Hà Nội</td>
                     </tr>
                     <tr>
                        <td>4</td>
                        <td width ="150" style="text-align:left;"><a href="http://xhome.com.vn/nhan-vien-thiet-ke-noi-that-tai-ha-noi-1-5-50887.html" title="Nhân viên thiết kế nội thất tại Hà nội">Nhân viên thiết kế nội thất tại Hà nội</a></td>
                        <td width ="120">trên 10 triệu</td>
                        <td width = "120">31-03-2018</td>
                        <td>Hà Nội</td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
   <script>var per = new Array(); per['vui_long_nhap_tu_khoa'] = 'Vui lòng nhập từ khóa!';</script> <script src="http://xhome.com.vn/modules/recruit/themes/resource/js/recruit.js" type="text/javascript"></script> <script>jQuery(document).ready(function() { recruit.init(per); });</script>
</section>
<!-- End Inside Page -->
<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>

<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->


<script type="text/javascript" src="js/product.js"></script>
<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
