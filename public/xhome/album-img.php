

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>
<style>
header{
	position: static !important;
	box-shadow: 0 0 6px rgba(0,0,0,0.3);
}
</style>
<div></div>
<!-- End Header -->
<!-- Inside Page -->
<section class="v2_bnc_inside_page">
   <div class="v2_bnc_content_top"></div>
   <link rel="stylesheet" href="http://xhome.com.vn/modules/album/themes/resources/css/album.css?rs=192606403">
   <div class="v2_breadcrumb_main padding-top-10">
      <div class="container">
         <ul class="breadcrumb">
            <li><a href="http://xhome.com.vn">Trang chủ</a></li>
            <li><a href="http://xhome.com.vn/xhomever2-website-2-3-14801.html">Album</a></li>
         </ul>
      </div>
   </div>
   <div class="v2_bnc_album_page margin-top-10">
      <div class="container">
         <div class="v2_bnc_title_main">
            <h1><img src="https://cdn-img-v2.webbnc.net/"onerror="this.onerror=null;this.src='{loadImage upload/web/1/1/no-img.gif none 50 50}'" class="img-responsive hidden"/>Album</h1>
         </div>
         <div class="v2_bnc_cate_page hidden">
            <ul class="v2_bnc_cate_page_list row"></ul>
         </div>
         <div class="v2_bnc_album_page_body margin-top-10">
            <div class="row hidden">
               <form id="album_filter" class="v2_bnc_filter_page" method="get" action="http://xhome.com.vn/album.html">
                  <div class="col-md-3 col-sm-6 col-xs-12 full-xs">
                     <select class="form-control" name="sort">
                        <option selected="selected" value="new">Mới</option>
                        <option value="hot">Nổi bật</option>
                        <option value="az">A-Z</option>
                     </select>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12 full-xs"><input type="text" name="q" placeholder="Tiêu đề" class="form-control" value=""></div>
                  <div class="col-md-3 col-sm-6 col-xs-12 full-xs">
                     <select class="form-control" name="limit" id="album_limit">
                        <option value="12" >Số lượng hiện thị</option>
                        <option value="20" >20</option>
                        <option value="30" >30</option>
                        <option value="40" >40</option>
                        <option value="50" >50</option>
                     </select>
                  </div>
                  <div class="col-md-3 col-sm-6 col-xs-12 full-xs"><button type="submit" class="btn btn-danger" id="submitSearch"><i class="fa fa-search"></i> Tìm kiếm</button></div>
               </form>
            </div>
            <div class="v2_bnc_album_page_list">
               <ul class="v2_bnc_album_page grid row">
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/nhung-khoanh-khac-vui-nhon-cua-cac-thanh-vien-xhome-1-3-31985.html" title="NHỮNG KHOẢNH KHẮC VUI NHỘN CỦA CÁC THÀNH VIÊN X&#039;HOME"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/12/05/03/25/1512443990_6.jpg"class="img-responsive" alt="NHỮNG KHOẢNH KHẮC VUI NHỘN CỦA CÁC THÀNH VIÊN X&#039;HOME" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/nhung-khoanh-khac-vui-nhon-cua-cac-thanh-vien-xhome-1-3-31985.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/nhung-khoanh-khac-vui-nhon-cua-cac-thanh-vien-xhome-1-3-31985.html" title="NHỮNG KHOẢNH KHẮC VUI NHỘN CỦA CÁC THÀNH VIÊN X&#039;HOME"> NHỮNG KHOẢNH KHẮC VUI NHỘN CỦA CÁC THÀNH VIÊN X&#039;HOME </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/qua-trinh-phat-trien-cua-xhome-1-3-30912.html" title="QUÁ TRÌNH PHÁT TRIỂN CỦA X&#039;HOME"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/09/09/02/47/1504925065_1.jpg"class="img-responsive" alt="QUÁ TRÌNH PHÁT TRIỂN CỦA X&#039;HOME" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/qua-trinh-phat-trien-cua-xhome-1-3-30912.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/qua-trinh-phat-trien-cua-xhome-1-3-30912.html" title="QUÁ TRÌNH PHÁT TRIỂN CỦA X&#039;HOME"> QUÁ TRÌNH PHÁT TRIỂN CỦA X&#039;HOME </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/tru-so-chinh-xhome-ha-noi-1-3-31883.html" title="TRỤ SỞ CHÍNH X&#039;HOME HÀ NỘI"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/11/23/11/22/1511435816_64-01.jpg"class="img-responsive" alt="TRỤ SỞ CHÍNH X&#039;HOME HÀ NỘI" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/tru-so-chinh-xhome-ha-noi-1-3-31883.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/tru-so-chinh-xhome-ha-noi-1-3-31883.html" title="TRỤ SỞ CHÍNH X&#039;HOME HÀ NỘI"> TRỤ SỞ CHÍNH X&#039;HOME HÀ NỘI </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/chi-nhanh-xhome-sai-gon-1-3-31885.html" title="CHI NHÁNH X&#039;HOME SÀI GÒN"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/11/23/11/29/1511436210_84-01.jpg"class="img-responsive" alt="CHI NHÁNH X&#039;HOME SÀI GÒN" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-sai-gon-1-3-31885.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-sai-gon-1-3-31885.html" title="CHI NHÁNH X&#039;HOME SÀI GÒN"> CHI NHÁNH X&#039;HOME SÀI GÒN </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/chi-nhanh-xhome-mien-trung-1-3-31887.html" title="CHI NHÁNH X&#039;HOME MIỀN TRUNG"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/11/23/11/31/1511436368_88-01.jpg"class="img-responsive" alt="CHI NHÁNH X&#039;HOME MIỀN TRUNG" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-mien-trung-1-3-31887.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-mien-trung-1-3-31887.html" title="CHI NHÁNH X&#039;HOME MIỀN TRUNG"> CHI NHÁNH X&#039;HOME MIỀN TRUNG </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/chi-nhanh-xhome-hai-phong-1-3-31886.html" title="CHI NHÁNH X&#039;HOME HẢI PHÒNG"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/11/23/11/30/1511436288_74-01.jpg"class="img-responsive" alt="CHI NHÁNH X&#039;HOME HẢI PHÒNG" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-hai-phong-1-3-31886.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-hai-phong-1-3-31886.html" title="CHI NHÁNH X&#039;HOME HẢI PHÒNG"> CHI NHÁNH X&#039;HOME HẢI PHÒNG </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/chi-nhanh-xhome-da-nang-1-3-31884.html" title="CHI NHÁNH X&#039;HOME ĐÀ NẴNG"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/11/23/11/27/1511436093_92-01.jpg"class="img-responsive" alt="CHI NHÁNH X&#039;HOME ĐÀ NẴNG" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-da-nang-1-3-31884.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/chi-nhanh-xhome-da-nang-1-3-31884.html" title="CHI NHÁNH X&#039;HOME ĐÀ NẴNG"> CHI NHÁNH X&#039;HOME ĐÀ NẴNG </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/showroom-chi-nhanh-xhome-quang-ninh-1-3-31712.html" title="SHOWROOM CHI NHÁNH X&#039;HOME QUẢNG NINH"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/11/07/09/36/1510047098_1.jpg"class="img-responsive" alt="SHOWROOM CHI NHÁNH X&#039;HOME QUẢNG NINH" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/showroom-chi-nhanh-xhome-quang-ninh-1-3-31712.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/showroom-chi-nhanh-xhome-quang-ninh-1-3-31712.html" title="SHOWROOM CHI NHÁNH X&#039;HOME QUẢNG NINH"> SHOWROOM CHI NHÁNH X&#039;HOME QUẢNG NINH </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/xhome-va-dai-truyen-hinh-1-3-30630.html" title="X&#039;HOME VÀ ĐÀI TRUYỀN HÌNH VIỆT NAM VTV"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/08/07/04/04/1502078510_147287709583.jpg"class="img-responsive" alt="X&#039;HOME VÀ ĐÀI TRUYỀN HÌNH VIỆT NAM VTV" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/xhome-va-dai-truyen-hinh-1-3-30630.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/xhome-va-dai-truyen-hinh-1-3-30630.html" title="X&#039;HOME VÀ ĐÀI TRUYỀN HÌNH VIỆT NAM VTV"> X&#039;HOME VÀ ĐÀI TRUYỀN HÌNH VIỆT NAM VTV </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/so-do-to-chuc-hoat-dong-xhome-group-1-3-30910.html" title="Sơ đồ tổ chức hoạt động X&#039;HOME Group"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/09/08/10/30/1504866423_so-do-cay-01.jpg"class="img-responsive" alt="Sơ đồ tổ chức hoạt động X&#039;HOME Group" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/so-do-to-chuc-hoat-dong-xhome-group-1-3-30910.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/so-do-to-chuc-hoat-dong-xhome-group-1-3-30910.html" title="Sơ đồ tổ chức hoạt động X&#039;HOME Group"> Sơ đồ tổ chức hoạt động X&#039;HOME Group </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/sinh-nhat-xhome-ho-chi-minh-tron-1-tuoi-1-3-30903.html" title="SINH NHẬT X&#039;HOME HỒ CHÍ MINH TRÒN 1 TUỔI"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/09/07/10/47/1504781078_150207267439.jpg"class="img-responsive" alt="SINH NHẬT X&#039;HOME HỒ CHÍ MINH TRÒN 1 TUỔI" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/sinh-nhat-xhome-ho-chi-minh-tron-1-tuoi-1-3-30903.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/sinh-nhat-xhome-ho-chi-minh-tron-1-tuoi-1-3-30903.html" title="SINH NHẬT X&#039;HOME HỒ CHÍ MINH TRÒN 1 TUỔI"> SINH NHẬT X&#039;HOME HỒ CHÍ MINH TRÒN 1 TUỔI </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
                  <li class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                     <div class="v2_bnc_pr_item">
                        <figure class="v2_bnc_pr_item_img">
                           <a href="http://xhome.com.vn/nha-may-san-xuat-1-3-30614.html" title="NHÀ MÁY SẢN XUẤT X&#039;HOME 2018"><img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/album/2017/08/07/01/54/1502070729_150034507236.jpg"class="img-responsive" alt="NHÀ MÁY SẢN XUẤT X&#039;HOME 2018" /></a>
                           <figcaption class="v2_bnc_pr_item_boxdetails">
                              <a href="http://xhome.com.vn/nha-may-san-xuat-1-3-30614.html" class="links_fixed">
                                 <div class="hvr-shutter-in-vertical">
                                    <div class="v2_bnc_pr_item_boxdetails_border">
                                       <div class="v2_bnc_pr_item_boxdetails_content">
                                          <h3 class="v2_bnc_pr_item_name">
                              <a href="http://xhome.com.vn/nha-may-san-xuat-1-3-30614.html" title="NHÀ MÁY SẢN XUẤT X&#039;HOME 2018"> NHÀ MÁY SẢN XUẤT X&#039;HOME 2018 </a></h3></div></div></div></a>
                           </figcaption>
                        </figure>
                     </div>
                  </li>
               </ul>
               <div class="clearfix"></div>
               <div class="v2_bnc_pagination">
                  <div class="row">
                     <div class="col-md-6 hidden">
                        <p class="v2_bnc_pagination_title"> Hiển thị từ<strong>1 </strong> đến<strong>12 </strong> trên<strong>14 </strong> bản ghi - Trang số<strong>1 </strong> trên<strong>2 </strong> trang</p>
                     </div>
                     <div class="v2_bnc_pagination_button text-center col-xs-12 margin-bottom-20">
                        <ul class="pagination">
                           <li class="disabled"><a> ← </a></li>
                           <li class="active"><a >1</a></li>
                           <li ><a href="/album-2-3-14801-p2.html">2</a></li>
                           <li><a href="/album-2-3-14801-p2.html">→</a></li>
                        </ul>
                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script src="http://xhome.com.vn/modules/album/themes/resources/scripts/jquery.albumload.min.js?rs=192606403"></script> <script src="http://xhome.com.vn/modules/album/themes/resources/scripts/album.js?rs=192606403" type="text/javascript"></script> <script>jQuery(document).ready(function() { var per = new Array(); album.init(per); });</script>
</section>
<!-- End Inside Page -->
<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>

<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->


<script type="text/javascript" src="js/product.js"></script>
<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
