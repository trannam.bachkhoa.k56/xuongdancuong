

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>
<style>
.v2_bnc_inside_page
{
	margin-top:137px;
}
</style>
<!-- Content -->
<!-- Inside Page -->
      <section class="v2_bnc_inside_page">
         <div class="v2_bnc_content_top"></div>
         <div class="v2_breadcrumb_main">
            <div class="container">
               <ul class="breadcrumb">
                  <li><a href="http://xhome.com.vn">Trang chủ</a></li>
                  <li ><a href="">Đăng nhập</a></li>
               </ul>
            </div>
         </div>
         <div class="login border-user">
            <div class="container">
               <div class="v2_bnc_title_user text-center">
                  <h1>Đăng nhập</h1>
               </div>
               <div class="row">
                  <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="v2-login">
                        <form action="http://xhome.com.vn/user-login-submit.html" method="POST" />
                           <h2>Đăng nhập</h2>
                           <div class="input-group">
                              <input type="text" class="form-control" id ="txtname" name="email" value ="" placeholder="Tài khoản của bạn" required>
                              <div class="warning" id="nameError" style="display:none;width:100%;" ><span style="color: red;">Bạn chưa nhập tên đăng nhập</span></div>
                           </div>
                           <div class="input-group">
                              <input type="password" class="form-control" id="txtpassword" name="password" value="" placeholder="Mật khẩu" required>
                              <div class="warning" id="passwordError" style="display:none;width:100%;" ><span style="color: red;">Bạn chưa nhập mật khẩu</span></div>
                           </div>
                           <p><a class="text-center text-danger" href="http://xhome.com.vn/user-forgot_password.html">Bạn quên mật khẩu ?</a></p>
                           <button type="submit" class="btn btn-danger">Đăng nhập</button>
                        </form>
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="v2-reg">
                        <form action="http://xhome.com.vn/user-regis-submit.html" method="POST" />
                           <h2>Tạo tài khoản mới</h2>
                           <div class="input-group">
                              <input type="text" class="form-control email" id="email" name="email" value ="" placeholder="Email">
                              <div class="warning" id="emailNull" style="display:none;width:100%;" ><span style="color: red;">Bạn chưa nhập email</span></div>
                              <div class="warning" id="emailError" style="display:none;width:100%;" ><span style="color: red;">Email không hợp lệ</span></div>
                           </div>
                           <div class="input-group">
                              <input type="text" class="form-control name" id="name" name="username" value ="" placeholder="Tên đăng nhập">
                              <div class="warning name_Error" id="name_Error" style="display:none;width:100%;" ><span style="color: red;">Bạn chưa nhập tên đăng nhập</span></div>
                           </div>
                           <div class="input-group">
                              <input type="password" class="form-control" id = "password" name="password" value ="" placeholder="Mật khẩu">
                              <div class="warning" id="password_Error" style="display:none;width:100%;" ><span style="color: red;">Bạn chưa nhập mật khẩu</span></div>
                              <div class="warning" id="password_short" style="display:none;width:100%;" ><span style="color: red;">Mật khẩu quá ngắn</span></div>
                           </div>
                           <div class="input-group">
                              <input type="password" class="form-control" id = "repassword" name="repassword" value ="" placeholder="Nhập lại mật khẩu">
                              <div class="warning" id="repasswordError" style="display:none;width:100%;" ><span style="color: red;">Bạn chưa nhập lại mật khẩu</span></div>
                              <div class="warning" id="changepasswordError" style="display:none;width:100%;" ><span style="color: red;">Mật khẩu nhập lại không đúng</span></div>
                           </div>
                           <div class="form-group check_capcha" check_capcha= "http://xhome.com.vn/user-login-checkCaptcha.html" >
                              <input type="hidden" name="captcha" id="cap_md" value="0" />
                              <div style="float: left; padding: 1px 5px 0 0; width: 45%;"><input type="text" placeholder="Mã xác thực"class="form-control" id="captcha_cha" value="" /></div>
                              <span>
                                 <img id="capt_img_ct" src="/home-captcha.html" /> <img title="Tải lại mã bảo vệ" id="f5capt_cha" src="http://xhome.com.vn/modules/user/themes/resource/img/view-refresh-small.png" />
                                 <div class="warning" id="capchaError" style="display:none;width:100%;" ><span style="color: red;">Bạn chưa nhập hoặc nhập không đúng mã capcha</span></div>
                              </span>
                           </div>
                           <button type="submit" class="btn btn-danger">Đăng ký</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <script src="http://xhome.com.vn/modules/user/themes/resource/js/jquery.validate.min.js" type="text/javascript"></script> <script type="text/javascript" src="http://xhome.com.vn/modules/user/themes/resource/js/login.js"></script> <script>$(document).ready(function() { Login.init();});</script>
      </section>
<!-- End Content -->
<script>
    // Get the modal Adv Popup Home
     var modal = document.getElementById('myModal');

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.close').click(function(){
        $('.modal').css('display','none');
    });
  });
</script>
<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>
<script> 
jQuery(function ($) { 
    $('#show').hide();
    $('#close').click(function (e) {
      $('#rich-media').hide('slow');
      return false;
    });
    $('#hide').click(function (e) {
      $('#show').show('slow');
      $('#hide').hide('slow');
      $('#rich-media-item').height(0);
      return false;
    });
    $('#show').click(function (e) {
      $('#hide').show('slow'); 
      $('#show').hide('slow');
      $('#rich-media-item').height(205);
      return false;
   });
});
</script>
<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->




<!-- End Full Code -->
<script type="text/javascript" src="js/product.js"></script>

<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
