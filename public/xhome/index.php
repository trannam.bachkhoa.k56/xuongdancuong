<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
   <!-- Edit By @DTM -->
   <head>
      <!-- Include Header Meta -->
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
      <meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
      <meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
      <meta name="robots" content="INDEX, FOLLOW"/>
      <link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
      <link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
      <base href="" />
      <!-- Include CSS -->
      <!-- Reset Css-->
      <link href="css/reset.css" rel="stylesheet" media="screen">
      <!-- End Reset Css-->
      <!-- Bootstrap Css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <!-- End Bootstrap Css -->
      <!-- Css -->
      <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
      <link href="css/owl.theme.css" rel="stylesheet" media="screen">
      <link href="css/owl.transitions.css" rel="stylesheet" media="screen">
      <link href="css/animate.css" rel="stylesheet" media="screen">
      <link href="css/jquery.nouislider.min.css" rel="stylesheet">
      <link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
      <link href="css/style.css" rel="stylesheet" media="screen">
      <!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
      <link href="css/mobile.css" rel="stylesheet" media="screen">
      <!-- End Css -->
      <!-- owl2 -->
      <link rel="stylesheet" href="css/owl.carousel.min.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <!-- end 
         owl2 -->
      <link href="css/coverStyle.css" rel="stylesheet" media="screen">
      <!-- FontIcon -->
      <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="css/simple-line-icons.css">
      <!-- End FontIcon -->
      <!-- JS -->
      <script type="text/javascript" src="js/jquery.min.js"></script> 
      <!-- owl2 -->
      <script src="js/owl.carousel.min.js"></script>
      <!-- end  -->
      <script type="text/javascript" src="js/search.js"></script>
      <script type="text/javascript" src="js/jwplayer.js"></script>
      <script src="js/jquery.nouislider.all.min.js"></script>
      <script src="js/owl.carousel.js"></script>
      <script type="text/javascript" src="js/js_customs.js"></script> 
      <!-- End JS <!-- End Include CSS -->
   </head>
   <body>
      <div id="fb-root"></div>
      <!-- Include popup so sanh -->
      <!-- So sánh sánh sản phẩm -->
      <div id="f-compare" status="closed">
         <div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
         <div class="f-compare-body">
            <ul class="f-compare-ul no-margin no-padding">
            </ul>
            <div class="f-compare-info"><span id="compare-notify"></span></div>
            <div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
         </div>
      </div>
      <!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->
      <!-- Full Code -->
      <!-- Copyright -->
      <h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
      <!-- End Copyright -->
      <!-- Header -->
      <!------------------HEADER-------------->
      <?php include('header/header.php')?>
      <!-- Content -->
      <section class="v2_bnc_content_main">
         <div class="v2_bnc_body_main">
            <div class="v2_bnc_home">
               <link rel="stylesheet" type="text/css" href="css/BNCcart.css"/>
               <script src="js/jquery.cookie.js"></script>
               <script src="js/jquery.pjax.js"></script>
               <link rel="stylesheet" type="text/css" href="css/magnific-popup.css"/>
               <script src="js/jquery.magnific-popup.js"></script>
               <script type="text/javascript" src="js/productFast.js"></script>
               <!--Like Product-->
               <link rel="stylesheet" type="text/css" href="css/likeproduct.css"/>
               <script src="js/likeproduct.js"></script>
               <!--End Like Product-->
               <link rel="stylesheet" type="text/css" href="css/toastr.css"/>
               <script src="js/toastr.js"></script>
               <script type="text/javascript" src="js/productFillter.js"></script>
               <div id="fast-container"></div>
               <div id="fast-dialog" class="zoom-anim-dialog mfp-hide">
                  <div id="fast-product-content">
                  </div>
               </div>
               <!-- Slideshow Main -->
               <div class="slideshow_block_top col-xs-12 no-padding margin-bottom-30">
                  <div class="owl_slideshow">
                     <figure class="slideshow_block_top_img">
                        <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/slide/2017/11/23/08/03/1511423876_cam1.png"alt="" class="img-responsive img_cover">
                        <figcaption class="slideshow_block_top_content">
                           <h2></h2>
                           <p></p>
                        </figcaption>
                     </figure>
                     <figure class="slideshow_block_top_img">
                        <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/slide/2017/08/05/04/04/1501948968_bn1.jpg"alt="" class="img-responsive img_cover">
                        <figcaption class="slideshow_block_top_content">
                           <h2></h2>
                           <p></p>
                        </figcaption>
                     </figure>
                     <figure class="slideshow_block_top_img">
                        <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/slide/2017/08/09/09/40/1502271489_banner.jpg"alt="" class="img-responsive img_cover">
                        <figcaption class="slideshow_block_top_content">
                           <h2></h2>
                           <p></p>
                        </figcaption>
                     </figure>
                     <figure class="slideshow_block_top_img">
                        <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/slide/2017/08/05/04/05/1501948999_bn3.jpg"alt="" class="img-responsive img_cover">
                        <figcaption class="slideshow_block_top_content">
                           <h2></h2>
                           <p></p>
                        </figcaption>
                     </figure>
                     <figure class="slideshow_block_top_img">
                        <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/slide/2017/08/05/04/05/1501949011_bn4.jpg"alt="" class="img-responsive img_cover">
                        <figcaption class="slideshow_block_top_content">
                           <h2></h2>
                           <p></p>
                        </figcaption>
                     </figure>
                     <figure class="slideshow_block_top_img">
                        <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/slide/2017/08/05/04/05/1501949025_bn5.jpg"alt="" class="img-responsive img_cover">
                        <figcaption class="slideshow_block_top_content">
                           <h2></h2>
                           <p></p>
                        </figcaption>
                     </figure>
                  </div>
               </div>
               <!-- End Slideshow Main -->
               <!-- Intro Company -->
               <div class="v2_bnc_intro_company col-xs-12 no-padding margin-top-20 margin-bottom-30 text-center">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                           <div>
                              <div style="text-align: center;"><span style="font-size:14px">X&#39;HOME ra mắt vào cuối năm 2013, chỉ sau gần 4 năm hoạt động X&rsquo;HOME đã vươn mình trở thành doanh nghiệp Thiết kế và Thi công nội thất, Kiến trúc có lượng khách hàng lớn nhất toàn quốc.<br  />
                                 Sự phát triển này của X&rsquo;HOME là chưa từng có tiền lệ trong lịch sử ngành Thiết kế &amp; Thi công nội thất tại Việt Nam.</span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Intro Company --><!-- Products Tab -->
               <section class="v2_bnc_pr_tab_main  col-xs-12 no-padding">
                  <div class="container">
                     <div class="v2_bnc_title_tab_main">
                        <hgroup class="v2_bnc_title_main">
                           <h2>Công Trình Tiêu Biểu</h2>
                        </hgroup>
                        <ul id="v2_bnc_pr_tab_home" class="v2_bnc_title_tab_home hidden">
                           <li class="active"><a href="#f-pr-tab00" data-toggle="tab">Sản phẩm nổi bật</a></li>
                        </ul>
                     </div>
                     <div class="v2_bnc_body_main">
                        <div class="tab-content row">
                           <div id="f-pr-tab00" class="tab-pane active v2_bnc_tabhome_product">
                              <div class="row">
                                 <div class="categoryIndex list">
                                    <div class="item">
                                       <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-12">
                                          <div class="v2_bnc_pr_item">
                                             <figure class="v2_bnc_pr_item_img">
                                                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">
                                                <img alt="Chị Oanh - Masteri Thảo Điền" id="f-pr-image-zoom-id-tab-home-742729" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742729 img-responsive"/>
                                                </a>
                                                <figcaption class="v2_bnc_pr_item_boxdetails">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" class="links_fixed">
                                                      <div class="hvr-shutter-in-vertical">
                                                         <div class="v2_bnc_pr_item_boxdetails_border">
                                                            <div class="v2_bnc_pr_item_boxdetails_content">
                                                               <!-- Products Name -->  
                                                               <h3 class="v2_bnc_pr_item_name">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">Chị Oanh - Masteri Thảo Điền</a></h3>
                                                   <!-- End Products Name --> 
                                                   <!-- Details -->
                                                   <div class="v2_bnc_pr_item_short_info">
                                                   <p>Diện tích căn hộ: 68m2</p>                                </div>
                                                   <!-- End Details -->
                                                   </div>
                                                   </div>
                                                   </div>    
                                                   </a>  
                                                </figcaption>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-12">
                                          <div class="v2_bnc_pr_item">
                                             <figure class="v2_bnc_pr_item_img">
                                                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">
                                                <img alt="Chị Oanh - Masteri Thảo Điền" id="f-pr-image-zoom-id-tab-home-742729" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742729 img-responsive"/>
                                                </a>
                                                <figcaption class="v2_bnc_pr_item_boxdetails">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" class="links_fixed">
                                                      <div class="hvr-shutter-in-vertical">
                                                         <div class="v2_bnc_pr_item_boxdetails_border">
                                                            <div class="v2_bnc_pr_item_boxdetails_content">
                                                               <!-- Products Name -->  
                                                               <h3 class="v2_bnc_pr_item_name">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">Chị Oanh - Masteri Thảo Điền</a></h3>
                                                   <!-- End Products Name --> 
                                                   <!-- Details -->
                                                   <div class="v2_bnc_pr_item_short_info">
                                                   <p>Diện tích căn hộ: 68m2</p>                                </div>
                                                   <!-- End Details -->
                                                   </div>
                                                   </div>
                                                   </div>    
                                                   </a>  
                                                </figcaption>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-12">
                                          <div class="v2_bnc_pr_item">
                                             <figure class="v2_bnc_pr_item_img">
                                                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">
                                                <img alt="Chị Oanh - Masteri Thảo Điền" id="f-pr-image-zoom-id-tab-home-742729" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742729 img-responsive"/>
                                                </a>
                                                <figcaption class="v2_bnc_pr_item_boxdetails">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" class="links_fixed">
                                                      <div class="hvr-shutter-in-vertical">
                                                         <div class="v2_bnc_pr_item_boxdetails_border">
                                                            <div class="v2_bnc_pr_item_boxdetails_content">
                                                               <!-- Products Name -->  
                                                               <h3 class="v2_bnc_pr_item_name">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">Chị Oanh - Masteri Thảo Điền</a></h3>
                                                   <!-- End Products Name --> 
                                                   <!-- Details -->
                                                   <div class="v2_bnc_pr_item_short_info">
                                                   <p>Diện tích căn hộ: 68m2</p>                                </div>
                                                   <!-- End Details -->
                                                   </div>
                                                   </div>
                                                   </div>    
                                                   </a>  
                                                </figcaption>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-12">
                                          <div class="v2_bnc_pr_item">
                                             <figure class="v2_bnc_pr_item_img">
                                                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">
                                                <img alt="Chị Oanh - Masteri Thảo Điền" id="f-pr-image-zoom-id-tab-home-742729" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742729 img-responsive"/>
                                                </a>
                                                <figcaption class="v2_bnc_pr_item_boxdetails">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" class="links_fixed">
                                                      <div class="hvr-shutter-in-vertical">
                                                         <div class="v2_bnc_pr_item_boxdetails_border">
                                                            <div class="v2_bnc_pr_item_boxdetails_content">
                                                               <!-- Products Name -->  
                                                               <h3 class="v2_bnc_pr_item_name">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">Chị Oanh - Masteri Thảo Điền</a></h3>
                                                   <!-- End Products Name --> 
                                                   <!-- Details -->
                                                   <div class="v2_bnc_pr_item_short_info">
                                                   <p>Diện tích căn hộ: 68m2</p>                                </div>
                                                   <!-- End Details -->
                                                   </div>
                                                   </div>
                                                   </div>    
                                                   </a>  
                                                </figcaption>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-12">
                                          <div class="v2_bnc_pr_item">
                                             <figure class="v2_bnc_pr_item_img">
                                                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">
                                                <img alt="Chị Oanh - Masteri Thảo Điền" id="f-pr-image-zoom-id-tab-home-742729" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742729 img-responsive"/>
                                                </a>
                                                <figcaption class="v2_bnc_pr_item_boxdetails">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" class="links_fixed">
                                                      <div class="hvr-shutter-in-vertical">
                                                         <div class="v2_bnc_pr_item_boxdetails_border">
                                                            <div class="v2_bnc_pr_item_boxdetails_content">
                                                               <!-- Products Name -->  
                                                               <h3 class="v2_bnc_pr_item_name">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">Chị Oanh - Masteri Thảo Điền</a></h3>
                                                   <!-- End Products Name --> 
                                                   <!-- Details -->
                                                   <div class="v2_bnc_pr_item_short_info">
                                                   <p>Diện tích căn hộ: 68m2</p>                                </div>
                                                   <!-- End Details -->
                                                   </div>
                                                   </div>
                                                   </div>    
                                                   </a>  
                                                </figcaption>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-12">
                                          <div class="v2_bnc_pr_item">
                                             <figure class="v2_bnc_pr_item_img">
                                                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">
                                                <img alt="Chị Oanh - Masteri Thảo Điền" id="f-pr-image-zoom-id-tab-home-742729" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742729 img-responsive"/>
                                                </a>
                                                <figcaption class="v2_bnc_pr_item_boxdetails">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" class="links_fixed">
                                                      <div class="hvr-shutter-in-vertical">
                                                         <div class="v2_bnc_pr_item_boxdetails_border">
                                                            <div class="v2_bnc_pr_item_boxdetails_content">
                                                               <!-- Products Name -->  
                                                               <h3 class="v2_bnc_pr_item_name">
                                                   <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">Chị Oanh - Masteri Thảo Điền</a></h3>
                                                   <!-- End Products Name --> 
                                                   <!-- Details -->
                                                   <div class="v2_bnc_pr_item_short_info">
                                                   <p>Diện tích căn hộ: 68m2</p>                                </div>
                                                   <!-- End Details -->
                                                   </div>
                                                   </div>
                                                   </div>    
                                                   </a>  
                                                </figcaption>
                                             </figure>
                                          </div>
                                       </div>
                                    </div>
                                    <script>
                                       $('.categoryIndex').owlCarousel({
                                          loop:true,
                                          margin:30,
                                          responsiveClass:true,
                                          autoplaySpeed:true,
                                          autoplay:true,
                                          navigation:true,
                                          responsive:{
                                              0:{
                                                  items:1,
                                                  nav:true
                                              },
                                              600:{
                                                  items:2,
                                                  nav:false
                                              },
                                              1000:{
                                                  items:4,
                                                  nav:true,
                                                  loop:false
                                              }
                                          }
                                       });
                                       $('.list .owl-prev').html('<i class="fa fa-angle-left"></i>');
                                       $('.list .owl-next').html('<i class="fa fa-angle-right"></i>');
                                       
                                       
                                    </script>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <!-- End Products Tab -->      
            </div>
         </div>
         <div class="v2_bnc_adv_xhome col-xs-12 no-padding hidden">
            <div class="v2_bnc_title_xhome text-center" >
               <h2>@xhomenoithatthongminh</h2>
            </div>
            <div class="v2_bnc_content_xhome">
               <div class="container">
                  <div class="row">
                  </div>
               </div>
            </div>
         </div>
         <!-- Popup Adv Home Loadpage-->
         <!-- End Popup Adv Home Loadpage -->
      </section>
      <!-- End Content -->
      <script>
         // Get the modal Adv Popup Home
          var modal = document.getElementById('myModal');
         
         // Get the <span> element that closes the modal
         var span = document.getElementsByClassName("close")[0];
         
         // When the user clicks the button, open the modal
         
         // When the user clicks on <span> (x), close the modal
         span.onclick = function() {
             modal.style.display = "none";
         }
         
         // When the user clicks anywhere outside of the modal, close it
         window.onclick = function(event) {
             if (event.target == modal) {
                 modal.style.display = "none";
             }
         }
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
           $('.close').click(function(){
               $('.modal').css('display','none');
           });
         });
      </script>
      <!--Footer-->
      <!--------------FOOTER------------>
      <?php include('footer/footer.php')?>
      <!--End Footer-->
      <!-- Adv Rich Meida -->
      <div class="hidden-xs">
      </div>
      <script> 
         jQuery(function ($) { 
             $('#show').hide();
             $('#close').click(function (e) {
               $('#rich-media').hide('slow');
               return false;
             });
             $('#hide').click(function (e) {
               $('#show').show('slow');
               $('#hide').hide('slow');
               $('#rich-media-item').height(0);
               return false;
             });
             $('#show').click(function (e) {
               $('#hide').show('slow'); 
               $('#show').hide('slow');
               $('#rich-media-item').height(205);
               return false;
            });
         });
      </script>
      <!-- End Adv Rich Meida -->
      <!-- Scroll To Top -->
      <div class="v2_bnc_scrolltop">
         <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
         <i class="fa fa-caret-up fa-4x"></i>
         </a>
      </div>
      <!-- End Scroll To Top -->
      <!-- End Full Code -->
      <script type="text/javascript" src="js/product.js"></script>
      <!-- Include JS -->
      <script src="js/bootstrap.js"></script> 
      <script type="text/javascript" src="js/started_js.js"></script>
      <script type="text/javascript" src="js/webcommon.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
      <script type="text/javascript" src="js/loading-overlay.min.js"></script>
      <script type="text/javascript" src="js/load.js"></script>
      <script type="text/javascript" src="js/fastCart.js"></script>
      <link rel="stylesheet" href="css/jquery.fancybox.css" />
      <script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
      <script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
      <script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
      <!-- chat facebook -->
      <!--
         <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
         <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>
         
             <div class='support-icon-right'>
                 <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                 <div class='online-support'>
                     <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                     </div>
                 </div>
             </div> -->
      <!-- End chat facebook -->
      <!--<script type="text/javascript">
         function BNCcallback(data){
             console.log(data);
         }
             var url = document.URL;
             var idW = '7121';
             var uid='';
             var title = document.title;
         
             var appsScript = document.createElement('script');
             appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
             document.body.appendChild(appsScript);
         
             var appsScript = document.createElement('script');
             appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
             setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);
         
             var _gaq = _gaq || [];
             _gaq.push(["_setAccount", "UA-43176424-6"]);
             _gaq.push(["_trackPageview"]);
         
         
               (function() {
                 var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                 ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                 var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
               })();
         
         </script>-->
   </body>
</html>