
<section class="customer v2_bnc_pr_tab_main">
   <div class="container">
     <div class="row bg">
       <div class="col-12 bgCustomer">

        <div class="v2_bnc_title_tab_main">
            <hgroup class="v2_bnc_title_main"><h2>Khách hàng nói về chúng tôi</h2></hgroup>
        </div>

            
       
       

         <div class="owl-carousel customerSaid list">
           <div class="item">
             <a href="" title=""><img src="http://quynhmaikimcuong.com/public/librarydiamond-1/images/khachang/kh1.jpg" alt=""></a>
             <p>Nghệ sĩ Trí Trung</p>
           </div>
           <div class="item">
            <a href="" title=""><img src="http://quynhmaikimcuong.com/public/librarydiamond-1/images/khachang/kh2.jpg" alt=""></a>
            <p>Nghệ sĩ Hoàng Việt</p>
           </div>
           <div class="item">
             <a href="" title=""><img src="http://quynhmaikimcuong.com/public/librarydiamond-1/images/khachang/kh3.jpg" alt=""></a>
             <p>Ca sĩ Lý Hải</p>
           </div>
           <div class="item">
            <a href="" title=""><img src="http://quynhmaikimcuong.com/public/librarydiamond-1/images/khachang/kh4.jpg" alt=""></a>
            <p>Nghệ sĩ Xuân Bắc</p>
           </div>
            <div class="item">
            <a href="" title=""><img src="http://quynhmaikimcuong.com/public/librarydiamond-1/images/khachang/kh4.jpg" alt=""></a>
            <p>Nghệ sĩ Xuân Bắc</p>
           </div>
            <div class="item">
            <a href="" title=""><img src="http://quynhmaikimcuong.com/public/librarydiamond-1/images/khachang/kh4.jpg" alt=""></a>
            <p>Nghệ sĩ Xuân Bắc</p>
           </div>
         </div>

<!-- <i class="fa fa-angle-left"></i>
<i class="fa fa-angle-right"></i> -->

          <script>
         
            $('.customerSaid').owlCarousel({
               loop:true,
               margin:30,
               responsiveClass:true,
               autoplaySpeed:true,
               autoplay:true,
               navigation:true,
               responsive:{
                   0:{
                       items:1,
                       nav:true
                   },
                   600:{
                       items:2,
                       nav:false
                   },
                   1000:{
                       items:4,
                       nav:true,
                       loop:false
                   }
               }
            });
            $('.list .owl-prev').html('<i class="fa fa-angle-left"></i>');
            $('.list .owl-next').html('<i class="fa fa-angle-right"></i>');
        
           
         </script>

       </div>

     </div>
   </div>
</section>

<footer class="v2_bnc_footer">
   <div class="container">
      <div class="row">
         <!-- Info Company -->
         <div class="v2_bnc_footer_info_company">
            <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom-20">
               <div class="v2_bnc_footer_title">
                  <h4>Tìm kiếm chúng tôi trên Facebook</h4>
               </div>
               <div class="v2_bnc_footer_content">
                  
               </div>
            </div>


            <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom-20">
               <div class="v2_bnc_footer_title">
                  <h4>Liên hệ</h4>
               </div>
               <div class="v2_bnc_footer_content">
                  <p>LÀ CÔNG TY THIẾT KẾ VÀ THI CÔNG NỘI THẤT & KIẾN TRÚC LỚN NHẤT VIỆT NAM 
THÀNH ĐẠT đang có trong tay những gì?
- Hệ thống Showroom & Chi nhánh trải dài khắp 3 Miền: HÀ NỘI, HẢI PHÒNG, HẢI DƯƠNG, QUẢNG NINH, MIỀN TRUNG, ĐÀ NẴNG, SÀI GÒN. Bên cạnh đó, hàng loạt chi nhánh khác sẽ khai trương đầu năm 2018
- Là Doanh nghiệp duy nhất có thể cung cấp đầy đủ trọn gói các sản phẩm dịch vụ từ  thiết kế thi công nội thất đến Xây mới, cải tạo. Cung cấp các sản phẩm trang trí gia đình, Thiết bị đồ dùng nhà bếp, Đồ gia dụng… Các sản phẩm được phối hợp ngay từ khi thiết kế để hiện thực hóa đến 100% bản thiết kế 3D. Thi công chìa khóa trao tay cho khách hàng.</p>
               </div>
            </div>


            <div class="col-md-4 col-sm-12 col-xs-12">
               <div class="v2_bnc_footer_title">
                  <h4>Tin tức nổi bật</h4>
               </div>
               <div class="v2_bnc_footer_content">
                  
               </div>
            </div>
         </div>
         <!-- End Info Company <-->
      </div>
   </div>
</footer>
<div class="copyright text-center padding-10">
   <span>© Copyright 2018 - Design by VN3C</span>
</div>