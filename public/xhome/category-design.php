

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>
<style>
header{
	position: static !important;
	box-shadow: 0 0 6px rgba(0,0,0,0.3);
}
</style>
<div></div>
<!-- End Header -->
<!-- Inside Page -->
<section class="v2_bnc_inside_page">
<div class="v2_bnc_content_top">
</div>


<section id="products-main" class="v2_bnc_products_page">
<!-- Breadcrumbs -->
<!-- Breadcrumbs -->
<div class="clearfix"></div>
<div class="v2_breadcrumb_main padding-top-10">
  <div class="container">
    <ul class="breadcrumb">
                        <li ><a href="http://xhome.com.vn">Trang chủ</a></li>
                              <li ><a href="http://xhome.com.vn/product.html">Sản Phẩm</a></li>
                              <li ><a href="http://xhome.com.vn/thiet-ke-noi-that-2-1-286578.html">Thiết kế nội thất</a></li>
                </ul>
  </div>
</div>
<!-- End Breadcrumbs --> 
<div class="container">
<div class="row">
<div class="col-xs-12">
        <div class="v2_bnc_title_main">
        	<h1>Thiết kế nội thất</h1>
        </div>
                <div class="v2_bnc_cate_page_pr">
<ul class="v2_bnc_cate_page_list_pr ">
<li>
<a href="http://xhome.com.vn/thiet-ke-vp-shop-spa-2-1-278879.html" title="Thiết kế VP, SHOP, SPA">Thiết kế VP, SHOP, SPA</a>
</li>
<li>
<a href="http://xhome.com.vn/can-ho-2-1-278877.html" title="Thiết kế Căn hộ">Thiết kế Căn hộ</a>
</li>
</ul>
</div> 

<div class="v2_bnc_products_page_body col-xs-12 no-padding">
<div class="tab-content margin-top-10 row">
<ul class="f-product-viewid f-product ">
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-862270">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/van-phong-forbes-tai-viet-nam-1-1-862270.html" title="Văn Phòng Forbes tại Việt Nam">
                    <img alt="Văn Phòng Forbes tại Việt Nam" id="f-pr-image-zoom-id-tab-home-862270" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/12/05/07/04/1512457082_ceo-2.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/12/05/07/04/1512457082_ceo-2.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-862270 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/van-phong-forbes-tai-viet-nam-1-1-862270.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/van-phong-forbes-tai-viet-nam-1-1-862270.html" title="Văn Phòng Forbes tại Việt Nam">Văn Phòng Forbes tại Việt Nam</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-847355">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/anh-trung-p-an-khe-thanh-khe-da-nang-1-1-847355.html" title="Anh Trung - P An Khê - Thanh Khê - Đà Nẵng">
                    <img alt="Anh Trung - P An Khê - Thanh Khê - Đà Nẵng" id="f-pr-image-zoom-id-tab-home-847355" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/11/17/04/01/1510890983_1.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/11/17/04/01/1510890983_1.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-847355 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/anh-trung-p-an-khe-thanh-khe-da-nang-1-1-847355.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/anh-trung-p-an-khe-thanh-khe-da-nang-1-1-847355.html" title="Anh Trung - P An Khê - Thanh Khê - Đà Nẵng">Anh Trung - P An Khê - Thanh Khê - Đà Nẵng</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-833804">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/chi-hien-phong-80x-cc-doi-can-ha-noi-1-1-833804.html" title="Chị Hiền - Phòng 80X - CC Đội cấn - Hà Nội">
                    <img alt="Chị Hiền - Phòng 80X - CC Đội cấn - Hà Nội" id="f-pr-image-zoom-id-tab-home-833804" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/11/07/08/49/1510044243_1.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/11/07/08/49/1510044243_1.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-833804 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/chi-hien-phong-80x-cc-doi-can-ha-noi-1-1-833804.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/chi-hien-phong-80x-cc-doi-can-ha-noi-1-1-833804.html" title="Chị Hiền - Phòng 80X - CC Đội cấn - Hà Nội">Chị Hiền - Phòng 80X - CC Đội cấn - Hà Nội</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-833800">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/chi-huong-phong-a1x-udic-riverside-122-vinh-tuy-hai-ba-trung-ha-noi-1-1-833800.html" title="Chị Hương - Phòng A1X - UDIC Riverside - 122 Vĩnh Tuy - Hai Bà Trưng - Hà Nội">
                    <img alt="Chị Hương - Phòng A1X - UDIC Riverside - 122 Vĩnh Tuy - Hai Bà Trưng - Hà Nội" id="f-pr-image-zoom-id-tab-home-833800" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/11/07/08/41/1510043804_1.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/11/07/08/41/1510043804_1.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-833800 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/chi-huong-phong-a1x-udic-riverside-122-vinh-tuy-hai-ba-trung-ha-noi-1-1-833800.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/chi-huong-phong-a1x-udic-riverside-122-vinh-tuy-hai-ba-trung-ha-noi-1-1-833800.html" title="Chị Hương - Phòng A1X - UDIC Riverside - 122 Vĩnh Tuy - Hai Bà Trưng - Hà Nội">Chị Hương - Phòng A1X - UDIC Riverside - 122 Vĩnh Tuy - Hai Bà Trưng - Hà Nội</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-821564">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/anh-duc-phong-xxx-chung-cu-jarmona-quan-7-tp-hcm-1-1-821564.html" title="Anh Đức - Phòng xxx - Chung cư JARMONA - QUẬN 7 - TP HCM">
                    <img alt="Anh Đức - Phòng xxx - Chung cư JARMONA - QUẬN 7 - TP HCM" id="f-pr-image-zoom-id-tab-home-821564" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/10/28/07/15/1509174624_1.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/10/28/07/15/1509174624_1.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-821564 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/anh-duc-phong-xxx-chung-cu-jarmona-quan-7-tp-hcm-1-1-821564.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/anh-duc-phong-xxx-chung-cu-jarmona-quan-7-tp-hcm-1-1-821564.html" title="Anh Đức - Phòng xxx - Chung cư JARMONA - QUẬN 7 - TP HCM">Anh Đức - Phòng xxx - Chung cư JARMONA - QUẬN 7 - TP HCM</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-795750">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/chi-diep-park-7-time-city-1-1-795750.html" title="Chị Diệp - PARK 7 - TIME CITY">
                    <img alt="Chị Diệp - PARK 7 - TIME CITY" id="f-pr-image-zoom-id-tab-home-795750" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/10/10/02/15/1507601454_1.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/10/10/02/15/1507601454_1.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-795750 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/chi-diep-park-7-time-city-1-1-795750.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/chi-diep-park-7-time-city-1-1-795750.html" title="Chị Diệp - PARK 7 - TIME CITY">Chị Diệp - PARK 7 - TIME CITY</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-742729">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">
                    <img alt="Chị Oanh - Masteri Thảo Điền" id="f-pr-image-zoom-id-tab-home-742729" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/15/chi-oanh-masteri-thao-dien-1501866780.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742729 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/chi-oanh-masteri-thao-dien-1-1-742729.html" title="Chị Oanh - Masteri Thảo Điền">Chị Oanh - Masteri Thảo Điền</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    <p>Diện tích căn hộ: 68m2</p>                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-743152">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/chi-thanh-can-duplex-trang-an-1-1-743152.html" title="Chị Thanh - Căn Duplex, Tràng An">
                    <img alt="Chị Thanh - Căn Duplex, Tràng An" id="f-pr-image-zoom-id-tab-home-743152" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/05/07/45/1504597320_1.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/05/07/45/1504597320_1.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743152 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/chi-thanh-can-duplex-trang-an-1-1-743152.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/chi-thanh-can-duplex-trang-an-1-1-743152.html" title="Chị Thanh - Căn Duplex, Tràng An">Chị Thanh - Căn Duplex, Tràng An</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    <p>Diện tích căn hộ: 230m2</p>                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-765748">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/vinhome-tran-duy-hung-1-1-765748.html" title="Vinhome Trần Duy Hưng">
                    <img alt="Vinhome Trần Duy Hưng" id="f-pr-image-zoom-id-tab-home-765748" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/05/07/32/1504596543_cam-1.png_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/05/07/32/1504596543_cam-1.png&mode=resize&size=500x500'" class="BNC-image-add-cart-765748 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/vinhome-tran-duy-hung-1-1-765748.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/vinhome-tran-duy-hung-1-1-765748.html" title="Vinhome Trần Duy Hưng">Vinhome Trần Duy Hưng</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    Diện tích căn hộ: 131m2                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-742749">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/chi-lien-vinhomes-central-park-1-1-742749.html" title="Chị Liên - Vinhomes Central Park">
                    <img alt="Chị Liên - Vinhomes Central Park" id="f-pr-image-zoom-id-tab-home-742749" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/23/chi-lien-vinhomes-central-park-1501867288.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/23/chi-lien-vinhomes-central-park-1501867288.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742749 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/chi-lien-vinhomes-central-park-1-1-742749.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/chi-lien-vinhomes-central-park-1-1-742749.html" title="Chị Liên - Vinhomes Central Park">Chị Liên - Vinhomes Central Park</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    <p>Diện tích căn hộ: 120m2</p>                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-742734">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/anh-hoang-bt-dream-town-ha-noi-1-1-742734.html" title="Anh Hoàng - BT Dream Town, Hà nội">
                    <img alt="Anh Hoàng - BT Dream Town, Hà nội" id="f-pr-image-zoom-id-tab-home-742734" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/16/anh-hoang-bt-dream-town-ha-noi-1501866865.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/16/anh-hoang-bt-dream-town-ha-noi-1501866865.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742734 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/anh-hoang-bt-dream-town-ha-noi-1-1-742734.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/anh-hoang-bt-dream-town-ha-noi-1-1-742734.html" title="Anh Hoàng - BT Dream Town, Hà nội">Anh Hoàng - BT Dream Town, Hà nội</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
<li class="v2_bnc_pr_item_box col-xs-6 col-sm-6 col-md-3 col-lg-3 full-xs margin-bottom-30" id="like-product-item-742777">
        <div class="v2_bnc_pr_item">
            
            <figure class="v2_bnc_pr_item_img"> 
                <a href="http://xhome.com.vn/chi-nga-phong-1910-trang-an-complex-1-1-742777.html" title="Chị Nga - Phòng 1910, Tràng An Complex">
                    <img alt="Chị Nga - Phòng 1910, Tràng An Complex" id="f-pr-image-zoom-id-tab-home-742777" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/37/chi-nga-phong-1910-trang-an-complex-1501868105.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/37/chi-nga-phong-1910-trang-an-complex-1501868105.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742777 img-responsive"/>
                </a>
                <figcaption class="v2_bnc_pr_item_boxdetails">
                <a href="http://xhome.com.vn/chi-nga-phong-1910-trang-an-complex-1-1-742777.html" class="links_fixed">
                    <div class="hvr-shutter-in-vertical">
                        <div class="v2_bnc_pr_item_boxdetails_border">
                            <div class="v2_bnc_pr_item_boxdetails_content">
                                <!-- Products Name -->  
                                <h3 class="v2_bnc_pr_item_name"><a href="http://xhome.com.vn/chi-nga-phong-1910-trang-an-complex-1-1-742777.html" title="Chị Nga - Phòng 1910, Tràng An Complex">Chị Nga - Phòng 1910, Tràng An Complex</a></h3>
                                <!-- End Products Name --> 
                                <!-- Details -->
                                                                <div class="v2_bnc_pr_item_short_info">
                                    <p>Căn hộ Duplex 227m2</p>                                </div>
                                                                <!-- End Details -->
                            </div>
                        </div>
                    </div>    
                </a>  
                </figcaption>
            </figure>
        </div>
</li>
</ul>
<div class="v2_bnc_pagination">
    <div class="row">
        <div class="col-md-6 hidden">
            <p class="v2_bnc_pagination_title"> Hiển thị từ                <strong>1  </strong> đến                <strong>12    </strong> trên                <strong>476  </strong> bản ghi - Trang số                <strong>1   </strong> trên                <strong>40  </strong> trang            </p>
        </div>
        <div class="v2_bnc_pagination_button text-center col-md-12 margin-top-20 margin-bottom-20">
            <ul class="pagination">
                                    <li class="disabled"><a> ← </a></li>
                
                                                            <li class="active"><a >1</a></li>
                                                               
                                                            <li ><a href="/thiet-ke-noi-that-2-1-286578-p2.html">2</a></li>
                                                               
                                                            <li ><a href="/thiet-ke-noi-that-2-1-286578-p3.html">3</a></li>
                                                               
                                                            <li ><a href="/thiet-ke-noi-that-2-1-286578-p4.html">4</a></li>
                                                               
                                                            <li ><a href="/thiet-ke-noi-that-2-1-286578-p5.html">5</a></li>
                                                               
                                
                                    <li><a href="/thiet-ke-noi-that-2-1-286578-p2.html">→</a></li>
                            </ul>
            <div class="clearfix"></div>
        </div>
    </div>    
</div>
            </div>
</div>
</div>	
        </div>    
</div>	
</section> 
<!-- End Products page --></section>
<!-- End Inside Page -->
<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>
<script> 
jQuery(function ($) { 
    $('#show').hide();
    $('#close').click(function (e) {
      $('#rich-media').hide('slow');
      return false;
    });
    $('#hide').click(function (e) {
      $('#show').show('slow');
      $('#hide').hide('slow');
      $('#rich-media-item').height(0);
      return false;
    });
    $('#show').click(function (e) {
      $('#hide').show('slow'); 
      $('#show').hide('slow');
      $('#rich-media-item').height(205);
      return false;
   });
});
</script>
<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->


<script type="text/javascript" src="js/product.js"></script>
<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
