<header class="v2_bnc_header ">
   <!-- Header Topbar -->
   <div class="v2_bnc_header_topbar">
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 full-xs disflex">
               <div class="v2_bnc_phone">
                  <a href="tel:1900636558"><i class="fa fa-phone fa-lg"></i> 1900636558</a>
                  <a class="maps" href="http://xhome.com.vn/maps.html"><i class="fa fa-map-marker fa-lg"></i> Hệ thống chi nhánh</a>
               </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 full-xs disflex flexend">
               <!-- Login and Register -->
               <div class="v2_bnc_login_register">
                  <div class="v2_bnc_login_account">
                     <ul class="v2_bnc_login_bar no-margin">
                        <li><a href="http://xhome.com.vn/user-login.html" rel="nofollow">Đăng nhập</a>
                        </li>
                        <li><a href="http://xhome.com.vn/user-regis.html" rel="nofollow">Đăng ký</a>
                        </li>
                     </ul>
                  </div>
               </div>
               <!-- End Login and Register --> 
               <!-- Cart -->
               <div class="v2_bnc_cart_main">
                  <div class="f-miniCart-miniv2">
                     <div class="f-miniCart-miniv2-toolbar">
                        <div class="miniv2-toolbar-name">
                           <a class="miniv2-toolbar-barclick"> 
                           <span class="miniv2-toolbar-count">0</span>
                           </a>
                        </div>
                     </div>
                     <div class="wrap_cart">
                        <div class="miniCart-top">
                           <span>Giỏ hàng của tôi (0)</span>
                        </div>
                        <div class="miniCart-body">
                           <ul class="miniCartItem">
                              <li>
                                 <center>Hiện chưa có sản phẩm nào trong giỏ hàng của bạn</center>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Cart -->
            </div>
         </div>
      </div>
   </div>
   <!-- End Header Topbar -->
   <!-- Header Top -->
   <div class="v2_bnc_header_top">
      <div class="container">
         <div class="row">
            <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2">
               <!-- Logo -->
               <div id="logo">
                  <a href="http://xhome.com.vn" rel="nofollow" class="v2_bnc_logo" title="Công Ty Cổ Phần Nội Thất X'Home Việt Nam">
                  <img src="http://s2.webbnc.vn/uploadv2/web/71/7121/news/2017/08/07/04/34/1502080336_logo-xhome-01.png" width="130" height="130" class="img-responsive" alt="Công Ty Cổ Phần Nội Thất X'Home Việt Nam">
                  </a>
               </div>
               <!-- End Logo -->
            </div>
            <div class="v2_bnc_search_cart_mobile col-xs-7 col-sm-9 col-md-10 col-lg-10">
               <!-- Menu Mobile -->
               <div class="v2_bnc_menu_mobile hidden-lg hidden-md visible-sm visible-xs">
                  <section class="button_menu_mobile">
                     <div id="nav_list">
                        <span class="menu-line menu-line-1"></span>
                        <span class="menu-line menu-line-2"></span>
                        <span class="menu-line menu-line-3"></span>
                     </div>
                  </section>
                  <nav class="menutop">
                     <div class="menu-top-custom">
                        <div class="navbar-collapse pushmenu pushmenu-left">
                           <ul class="nav navbar-nav">
                              <li class="parent">
                                 <a class="txt" href="http://xhome.com.vn/thiet-ke-noi-that-2-1-286578.html">Thiết kế nội thất</a>
                                 <div class="top-menu-new">
                                    <ul>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/thiet-ke-vp-shop-spa-2-1-278879.html" title="Thiết kế Vp, Shop, Spa">Thiết kế Vp, Shop, Spa</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/thiet-ke-can-ho-2-1-278877.html" title="Thiết kế căn hộ">Thiết kế căn hộ</a>
                                          <span></span>
                                       </li>
                                    </ul>
                                 </div>
                                 <span></span>
                              </li>
                              <li class="">
                                 <a class="txt" href="http://xluxury.com.vn/">Nội thất cao cấp</a>
                              </li>
                              <li class="">
                                 <a class="txt" href="http://xhome.com.vn/thiet-ke-kien-truc-2-1-278878.html">Thiết kế xây dựng</a>
                              </li>
                              <li class="parent">
                                 <a class="txt" href="http://xhome.com.vn/san-pham-le-2-1-278880.html">Sản phẩm lẻ</a>
                                 <div class="top-menu-new">
                                    <ul>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/ghe-2-1-278881.html" title="Ghế">Ghế</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/ban-2-1-278885.html" title="Bàn">Bàn</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/den-2-1-278889.html" title="Đèn">Đèn</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/tham-2-1-278894.html" title="Thảm">Thảm</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/do-trang-tri-2-1-278897.html" title="Đồ trang trí">Đồ trang trí</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/sofa-2-1-278900.html" title="Sofa">Sofa</a>
                                          <span></span>
                                       </li>
                                    </ul>
                                 </div>
                                 <span></span>
                              </li>
                              <li class="parent">
                                 <a class="txt" href="http://xhome.com.vn/dich-vu-1-7-7072.html">Dịch vụ</a>
                                 <div class="top-menu-new">
                                    <ul>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/thiet-ke-va-thi-cong-noi-that-kien-truc-tai-hnhpqnth-1-7-7072.html" title="Thiết kế và Thi công nội thất, kiến trúc tại HN,HP,QN,TH">Thiết kế và Thi công nội thất, kiến trúc tại HN,HP,QN,TH</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/thiet-ke-va-thi-cong-cai-tao-xay-moi-can-ho-1-7-7073.html" title="Thiết kế và thi công Cải tạo, Xây mới căn hộ">Thiết kế và thi công Cải tạo, Xây mới căn hộ</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/dich-vu-danh-cho-khach-hang-o-cac-tinh-khac-ngoai-ha-noi-1-7-7074.html" title="Dịch vụ dành cho khách hàng ở các tỉnh khác ngoài Hà nội">Dịch vụ dành cho khách hàng ở các tỉnh khác ngoài Hà nội</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/dich-vu-dang-ky-bao-gia-thi-cong-khong-can-thiet-ke-1-7-7075.html" title="Dịch vụ đăng ký Báo giá thi công (Không cần thiết kế)">Dịch vụ đăng ký Báo giá thi công (Không cần thiết kế)</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/trang-tri-can-ho-tron-goi-1-7-7076.html" title="Trang trí căn hộ trọn gói">Trang trí căn hộ trọn gói</a>
                                          <span></span>
                                       </li>
                                    </ul>
                                 </div>
                                 <span></span>
                              </li>
                              <li class="">
                                 <a class="txt" href="http://xhome.com.vn/anh-cong-trinh-2-3-14835.html">Ảnh công trình</a>
                              </li>
                              <li class="parent">
                                 <a class="txt" href="http://xhome.com.vn/gioi-thieu-2-3-14801.html">Giới thiệu</a>
                                 <div class="top-menu-new">
                                    <ul>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/album-2-3-14801.html" title="Album">Album</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="https://www.youtube.com/watch?v=65so3eBdBEw" title="Video">Video</a>
                                          <span></span>
                                       </li>
                                       <li class="parent" "="">
                                          <a class="v2_link_submenu_1" href="http://xhome.com.vn/tuyen-dung-3-5.html" title="Tuyển dụng">Tuyển dụng</a>
                                          <span></span>
                                       </li>
                                    </ul>
                                 </div>
                                 <span></span>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </div>
			   
               <!-- End Menu Mobile -->
               <div class="v2_bnc_header_action">
                  <!-- Search -->
                  <div class="v2_bnc_search pull-right">
                    <!--  <i class="icon-magnifier icons icons-click-search"></i> -->
                    <i class=" icons-click-search  fa fa-search" aria-hidden="true"></i>


                     <div id="search-box" class="v2_bnc_search_main">
                        <div class="btn-close-search"><span class="fa fa-close" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tắt tìm kiếm !"></span></div>
                        <form role="search" action="http://xhome.com.vn/product.html" method="GET">
                           <div class="search search-area">
                              <div class="input-group v2_bnc_search_border">
                                 <div class="input-group-btn search-basic hidden">
                                    <select class="form-control" name="BNC_searchCategory">
                                       <option value="product">Sản phẩm</option>
                                       <option value="news">Tin tức</option>
                                       <option value="album">Album</option>
                                       <option value="video">Video</option>
                                       <option value="recruit">Tuyển dụng</option>
                                    </select>
                                 </div>
                                 <input type="search" class="form-control search-field" placeholder="Nhập nội dung tìm kiếm..." name="BNC_txt_search" required="" id="BNC_txt_search" value="">
                                 <div class="input-group-btn"> <a href="javascript:void(0);" class="search-button" id="BNC_btn_search">Search</a> </div>
                                 <!-- Search Smart -->
                                 <div class="searchAutoComplete hidden">
                                    <ul id="resSearch" style="display: none;">
                                       <li>
                                          <a href="">sản phẩm a</a>
                                       </li>
                                       <li>
                                          <a href="">sản phẩm b</a>
                                       </li>
                                       <li>
                                          <a href="">sản phẩm c</a>
                                       </li>
                                    </ul>
                                 </div>
                                 <!-- End Search Smart -->
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!-- End Search --> 
               </div>
               <!-- Menu Main -->
			   
               <nav class="v2_bnc_menu_main hidden-sm hidden-xs">
                  <div class="v2_menu_top">
                     <ul class="v2_menu_top_ul nxtActiveMenu no-margin">
                        <li class="parent">
                           <a class="v2_menu_first_link" href="http://xhome.com.vn/thiet-ke-noi-that-2-1-286578.html" title="Thiết kế nội thất">
                           Thiết kế nội thất</a> 
                           <ul class="v2_menu_top_sub">
                              <li>
                                 <a href="http://xhome.com.vn/thiet-ke-vp-shop-spa-2-1-278879.html" title="">Thiết kế Vp, Shop, Spa</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/thiet-ke-can-ho-2-1-278877.html" title="">Thiết kế căn hộ</a>
                              </li>
                           </ul>
                        </li>
                        <li class="">
                           <a class="v2_menu_first_link" href="http://xluxury.com.vn/" title="Nội thất cao cấp">
                           Nội thất cao cấp</a> 
                        </li>
                        <li class="">
                           <a class="v2_menu_first_link" href="http://xhome.com.vn/thiet-ke-kien-truc-2-1-278878.html" title="Thiết kế xây dựng">
                           Thiết kế xây dựng</a> 
                        </li>
                        <li class="parent">
                           <a class="v2_menu_first_link" href="http://xhome.com.vn/san-pham-le-2-1-278880.html" title="Sản phẩm lẻ">
                           Sản phẩm lẻ</a> 
                           <ul class="v2_menu_top_sub">
                              <li>
                                 <a href="http://xhome.com.vn/ghe-2-1-278881.html" title="">Ghế</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/ban-2-1-278885.html" title="">Bàn</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/den-2-1-278889.html" title="">Đèn</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/tham-2-1-278894.html" title="">Thảm</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/do-trang-tri-2-1-278897.html" title="">Đồ trang trí</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/sofa-2-1-278900.html" title="">Sofa</a>
                              </li>
                           </ul>
                        </li>
                        <li class="parent">
                           <a class="v2_menu_first_link" href="http://xhome.com.vn/dich-vu-1-7-7072.html" title="Dịch vụ">
                           Dịch vụ</a> 
                           <ul class="v2_menu_top_sub">
                              <li>
                                 <a href="http://xhome.com.vn/thiet-ke-va-thi-cong-noi-that-kien-truc-tai-hnhpqnth-1-7-7072.html" title="">Thiết kế và Thi công nội thất, kiến trúc tại HN,HP,QN,TH</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/thiet-ke-va-thi-cong-cai-tao-xay-moi-can-ho-1-7-7073.html" title="">Thiết kế và thi công Cải tạo, Xây mới căn hộ</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/dich-vu-danh-cho-khach-hang-o-cac-tinh-khac-ngoai-ha-noi-1-7-7074.html" title="">Dịch vụ dành cho khách hàng ở các tỉnh khác ngoài Hà nội</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/dich-vu-dang-ky-bao-gia-thi-cong-khong-can-thiet-ke-1-7-7075.html" title="">Dịch vụ đăng ký Báo giá thi công (Không cần thiết kế)</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/trang-tri-can-ho-tron-goi-1-7-7076.html" title="">Trang trí căn hộ trọn gói</a>
                              </li>
                           </ul>
                        </li>
                        <li class="">
                           <a class="v2_menu_first_link" href="http://xhome.com.vn/anh-cong-trinh-2-3-14835.html" title="Ảnh công trình">
                           Ảnh công trình</a> 
                        </li>
                        <li class="parent">
                           <a class="v2_menu_first_link" href="http://xhome.com.vn/gioi-thieu-2-3-14801.html" title="Giới thiệu">
                           Giới thiệu</a> 
                           <ul class="v2_menu_top_sub">
                              <li>
                                 <a href="http://xhome.com.vn/album-2-3-14801.html" title="">Album</a>
                              </li>
                              <li>
                                 <a href="https://www.youtube.com/watch?v=65so3eBdBEw" title="">Video</a>
                              </li>
                              <li>
                                 <a href="http://xhome.com.vn/tuyen-dung-3-5.html" title="">Tuyển dụng</a>
                              </li>
                           </ul>
                        </li>
                     </ul>
                  </div>
               </nav>
               <!-- End Menu Main -->
            </div>
         </div>
      </div>
   </div>
   <a target="_blank" class="button_yc" href="https://docs.google.com/forms/d/e/1FAIpQLSftgBJEMzNIm6SNpxcblp8v11SY9cvpgqS_A6miyBecd1fyUw/viewform?c=0&amp;w=1">ĐĂNG KÝ THIẾT KẾ</a>
   <style>
      .button_yc {
      z-index: 99;
      position: fixed;
      bottom: 0;
      right: 5%;
      background: #dc4e42;
      border-radius: 10px 10px 0 0;
      padding: 7px 45px;
      font-weight: bold;
      color: #fff;
      }
   </style>
   <!-- End Header Top -->
</header>



	  
	  
	
























