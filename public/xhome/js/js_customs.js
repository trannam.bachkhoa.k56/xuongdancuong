// Action category products left menu
$(document).ready(function(){
    // Active Menu
    function NxtActiveMenu(){
      var urlNow=window.location.href;
      var allA=$('.nxtActiveMenu').find('a');
      $.each(allA, function(k, v) {
            var self=$(this);
            if(self.attr('href')==urlNow){
                $('.nxtActiveMenu').find('li.active').removeClass('active');
                self.parents('li').addClass('active');
            }
      });
    };
    NxtActiveMenu();
    // Active Menu
    // Btn Search
    $('.icons-click-search').on('click', function(){
        $('.v2_bnc_search_main').addClass('active');
    });
    $('.btn-close-search').on('click', function(){
        $('.v2_bnc_search_main').removeClass('active');
    });
});
// End Action category products left menu

// Menu mobile
$(document).ready(function() {
    var removeClass = true;
    $menuLeft = $('.pushmenu-left');
    $nav_list = $('.button_menu_mobile');
    
    $nav_list.click(function(e) {
        $(this).toggleClass('active');
        $('body').toggleClass('pushmenu-push-toright');
        $menuLeft.toggleClass('pushmenu-open');
        removeClass = false;
    });
    $('html').click(function () {
        if (removeClass) {
            $('body').removeClass('pushmenu-push-toright');
            $('.pushmenu-left').removeClass('pushmenu-open');
            $('.button_menu_mobile').removeClass('active');
        }
        removeClass = true;
    });

    $('.navbar-nav').find('.parent').append('<span></span>');

    $('.navbar-nav .parent span').on("click", function() {
        if ($(this).attr('class') == 'opened') {
            $(this).removeClass().parent('.parent').find('ul').slideToggle();
        } else {
            $(this).addClass('opened').parent('.parent').find('ul').slideToggle();
        }
        removeClass = false;
    });
});

// Scroll To Top
$(document).ready(function(){
  $(".v2_bnc_icon_scrolltop").click(function(event){
   $('html, body').animate({ scrollTop: 0 }, 1000);
  });
  // Hide,Show ScrollToTop
  var num = 100;  
  $(window).bind('scroll', function () {
      if ($(window).scrollTop() > num) {   
          $('.v2_bnc_scrolltop').addClass('fixed');
      }
      else
      {
          $('.v2_bnc_scrolltop').removeClass('fixed');
      }
  });
  
  var num = 200;  
  $(window).bind('scroll', function () {
      if ($(window).scrollTop() > num) {   
          $('.v2_bnc_header_top').addClass('fixed');
      }
      else
      {
          $('.v2_bnc_header_top').removeClass('fixed');
      }
  });
}); 
// End Scroll To Top 

// Tooltip and Popover
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({
        html: true,
        content: function() {
            var content = $(this).find('.v2_bnc_pr_item_short_info');
            try { content = $(content).html() } catch(e) {/* Ignore */}
            return content;
        }
    });
});

