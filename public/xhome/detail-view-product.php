

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<!-- Edit By @DTM -->
<head>
<!-- Include Header Meta -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
<meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
<meta name="robots" content="INDEX, FOLLOW"/>
<link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
<link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />




<base href="" />
<!-- Include CSS -->
<!-- Reset Css-->
<link href="css/reset.css" rel="stylesheet" media="screen">
<!-- End Reset Css-->

<!-- Bootstrap Css -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- End Bootstrap Css -->

<!-- Css -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">
<link href="css/owl.transitions.css" rel="stylesheet" media="screen">
<link href="css/animate.css" rel="stylesheet" media="screen">
<link href="css/jquery.nouislider.min.css" rel="stylesheet">
<link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
<link href="css/mobile.css" rel="stylesheet" media="screen"> 
<!-- End Css -->

<!-- FontIcon -->
<link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/simple-line-icons.css">
<!-- End FontIcon -->

<!-- JS -->
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/jwplayer.js"></script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/js_customs.js"></script> 
<!-- End JS --> <!-- End Include CSS -->
</head>

<body><div id="fb-root"></div>


<!-- Include popup so sanh -->
<!-- So sánh sánh sản phẩm -->
<div id="f-compare" status="closed">
<div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
<div class="f-compare-body">
<ul class="f-compare-ul no-margin no-padding">

</ul>
<div class="f-compare-info"><span id="compare-notify"></span></div>
<div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
</div>
</div>
<!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->

<!-- Full Code -->
<!-- Copyright -->
<h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
<!-- End Copyright -->
<!-- Header -->

<!------------------HEADER-------------->
<?php include('header/header.php')?>
<style>
header{
	position: static !important;
	box-shadow: 0 0 6px rgba(0,0,0,0.3);
}
</style>
<div></div>
<!-- End Header -->
<!-- Inside Page -->

 <!-- Inside Page -->
      <section class="v2_bnc_inside_page">
         <div class="v2_bnc_content_top"></div>
         <div class="v2_bnc_products_view_details">
            <div class="v2_bnc_products_body">
               <!-- Products details -->
               <!-- Breadcrumbs -->
               <div class="v2_breadcrumb_main">
                  <div class="container">
                     <ul class="breadcrumb padding-top-30 padding-bottom-30" style="display:block;">
                        <li ><a href="http://xhome.com.vn">Trang chủ</a></li>
                        <li ><a href="http://xhome.com.vn/product.html">Sản Phẩm</a></li>
                        <li ><a href="http://xhome.com.vn/san-pham-le-2-1-278880.html">Sản phẩm lẻ</a></li>
                        <li ><a href="http://xhome.com.vn/ghe-2-1-278881.html">Ghế</a></li>
                        <li ><a href="http://xhome.com.vn/ghe-ban-an-2-1-278882.html">Ghế bàn ăn</a></li>
                        <li ><a href="#">Ghế Laminate GH52</a></li>
                     </ul>
                  </div>
               </div>
               <!-- End Breadcrumbs -->
               <div class="v2_bnc_product_details_page">
                  <div class="container">
                     <div class="row">
                        <!-- Zoom image -->
                        <div class="v2_bnc_products_details_zoom_img col-md-7 col-sm-12 col-xs-12">
                           <div class="f-pr-image-zoom">
                              <div class="zoomWrapper">
                                 <img id="img_01" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744289_gh52-2.jpg.jpg"data-zoom-href='https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744289_gh52-2.jpg.jpg'class="img-responsive BNC-image-add-cart-786802" alt="Ghế Laminate GH52" class="img-responsive" />
                              </div>
                           </div>
                           <div class="f-pr-image-zoom-gallery">
                              <div id="slidezoompage" class="row">
                                 <div class="owl_img_product_details">
                                    <div class="col-xs-12">
                                       <a href="#" data-href='https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744289_gh52-2.jpg.jpg'data-zoom-image="http://s2.webbnc.vn/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744289_gh52-2.jpg.jpg" title="GH52 (2)">
                                       <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744289_gh52-2.jpg.jpg" alt="GH52 (2)" class="v2_bnc_product_details_img_small img-responsive"/>
                                       </a>
                                    </div>
                                    <div class="col-xs-12">
                                       <a href="#" data-href='https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744293_gh52.jpg.jpg'data-zoom-image="http://s2.webbnc.vn/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744293_gh52.jpg.jpg" title="gh52">
                                       <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744293_gh52.jpg.jpg" alt="gh52" class="v2_bnc_product_details_img_small img-responsive"/>
                                       </a>
                                    </div>
                                    <div class="col-xs-12">
                                       <a href="#" data-href='https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744296_gh52-1.jpg.jpg'data-zoom-image="http://s2.webbnc.vn/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744296_gh52-1.jpg.jpg" title="gh52-1">
                                       <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/30/04/08/1506744296_gh52-1.jpg.jpg" alt="gh52-1" class="v2_bnc_product_details_img_small img-responsive"/>
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                           </div>
                        </div>
                        <!-- End Zoom image -->	
                        <!-- Details -->
                        <div class="v2_bnc_products_details_box col-md-5 col-sm-12 col-xs-12">
                           <div class="v2_bnc_products_details_box_name">
                              <h2>Ghế Laminate GH52</h2>
                           </div>
                           <br />	
                           <div class="v2_bnc_products_details_box_rating">
                              <div id="stars" data-text-login="Vui lòng đăng nhập trước khi đánh giá" data-is-login="0" onclick="relogin()" data-is-rater="" data-name="Ghế Laminate GH52" data-id-product="786802" class="starrr" data-rating='0' style="font-size: 17px;color: #fa930d;position: absolute;top: 2px;"></div>
                              <div style="margin-left: 94px;margin-top: -12px;">
                                 <span id="display-first-rating" style="font-size: 12px">Hãy trở thành người đầu tiên đánh giá sản phẩm này</span>
                                 <span id="display-total-rating" style="font-size: 12px">(<i class="fa fa-user"></i> <span id="rate" style="font-size: 12px;">0</span>)</span>
                              </div>
                           </div>
                           <div class="v2_bnc_products_details_box_social">
                              <ul class="no-margin no-padding">
                                 <li><a href="javascript:;" class="like-product like-product-786802" data-is-info-page="1" data-like="2" data-name="Ghế Laminate GH52" data-id="786802" data-text-like="Thích" data-text-unlike="Bỏ thích" data-login="Vui lòng đăng nhập trước <br/><a class='btn btn-success' href='http://xhome.com.vn/user-login.html'>Đăng nhập</a>"><i class="fa fa-thumbs-o-up"></i> Thích</a></li>
                                 <li style="display:none"><a href="javascript:;">So sánh</a></li>
                                 <li>Lượt xem: 472</li>
                                 <li>Ngày đăng: 30/09/2017</li>
                              </ul>
                              <div class="clearfix"></div>
                           </div>
                           <div class="v2_bnc_products_details_box_info">
                              <div class="v2_bnc_products_details_box_price">
                                 <span class="key">Giá sản phẩm: </span><span class="price">1.100.000 đ</span>
                              </div>
                              <div class="v2_bnc_products_details_box_description">
                                 * Chất liệu: mặt gỗ laminate, chân sắt sơn đen tĩnh điện<br  />
                                 * Nhận đặt hàng từ 6 chiếc&nbsp;<br  />
                                 * Thời gian đặt hàng : 10 ngày
                              </div>
                              <ul class="no-margin no-padding hidden">
                                 <!-- Color-->
                                 <!-- End Color-->
                                 <!-- Size -->
                                 <!-- End Size -->
                                 <!-- Code Products -->
                                 <!-- End Code Products -->
                                 <!-- Condition -->
                                 <li class="key">Tình trạng: </li>
                                 <li class="value condition"></li>
                                 <li class="clearfix"></li>
                                 <!-- End condition -->
                                 <li class="key">Nhà cung cấp: </li>
                                 <li class="value">
                                    <a class="supplier_link" href="http://xhome.com.vn/xhomever2-website-8-1-14157.html">X&#039;Home - Nội thất thông minh</a>
                                 </li>
                                 <li class="clearfix"></li>
                              </ul>
                           </div>
                           <div class="f-pr-view-choosesizeGroup padding-10 hidden">
                              <div class="dealer-tab" data-is-more="0" id="shop-content" style="">
                                 <label>Tên cửa hàng</label>
                                 <ul class="v2-dealer">
                                    <li>Còn 100 sp - Điện thoại:: -  - Cửa hàng BNC</li>
                                 </ul>
                              </div>
                           </div>
                           <div class="f-pr-view-box-size padding-10 hidden">
                              <span class="box-size-title"><strong>Số lượng</strong></span>
                              <div class="select-shop" id="shop-1">
                                 <select name="ProductQuantity">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                                    <option value="61">61</option>
                                    <option value="62">62</option>
                                    <option value="63">63</option>
                                    <option value="64">64</option>
                                    <option value="65">65</option>
                                    <option value="66">66</option>
                                    <option value="67">67</option>
                                    <option value="68">68</option>
                                    <option value="69">69</option>
                                    <option value="70">70</option>
                                    <option value="71">71</option>
                                    <option value="72">72</option>
                                    <option value="73">73</option>
                                    <option value="74">74</option>
                                    <option value="75">75</option>
                                    <option value="76">76</option>
                                    <option value="77">77</option>
                                    <option value="78">78</option>
                                    <option value="79">79</option>
                                    <option value="80">80</option>
                                    <option value="81">81</option>
                                    <option value="82">82</option>
                                    <option value="83">83</option>
                                    <option value="84">84</option>
                                    <option value="85">85</option>
                                    <option value="86">86</option>
                                    <option value="87">87</option>
                                    <option value="88">88</option>
                                    <option value="89">89</option>
                                    <option value="90">90</option>
                                    <option value="91">91</option>
                                    <option value="92">92</option>
                                    <option value="93">93</option>
                                    <option value="94">94</option>
                                    <option value="95">95</option>
                                    <option value="96">96</option>
                                    <option value="97">97</option>
                                    <option value="98">98</option>
                                    <option value="99">99</option>
                                    <option value="100">100</option>
                                 </select>
                              </div>
                           </div>
                           <a href="#" class="btn-buy hidden" data-toggle="modal" data-target="#myModal">
                           <i class="fa fa-pencil-square-o"></i> Đăng ký thiết kế
                           </a>
                           <a href="http://xhome.com.vn/payment-oncepage.html" id="payment" class="btn-buynow" id="add-cart">
                           <button class="add-cart btn-buy btn-buy-now quick-buy-custom" data-product="786802"><i class="fa fa-check"></i> Mua ngay</button>
                           </a>
                           <div class="clearfix"></div>
                        </div>
                        <!-- End Details -->
                     </div>
                  </div>
               </div>
               <script type="text/javascript">
                  $(document).ready(function() {
                  $("#img_01").elevateZoom(
                  {
                  gallery:'slidezoompage',
                  cursor: 'pointer',
                  galleryActiveClass: 'active',
                  imageCrossfade: true,
                  scrollZoom : true,
                  easing : true
                  });
                  //
                  $("#img_01").bind("click", function(e) {
                  var ez = $('#img_01').data('elevateZoom');
                  $.fancybox(ez.getGalleryList());
                  var src=$(this).find('img').attr('src');
                  $('#img_01').attr('src',src);
                  
                  return false;
                  });
                  $("#slidezoompage a").bind("click", function(e) {
                  var src=$(this).find('img').attr('src');
                  $('#img_01').attr('src',src);
                  
                  return false;
                  });
                  
                  });
               </script>
               <!-- End Products details -->
               <!-- Form Register Design -->
               <!-- Modal -->
               <div class="v2_bnc_form_register">
                  <div class="modal fade" id="myModal" role="dialog">
                     <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                           <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" title="Tắt">&times;</button>
                           </div>
                           <div class="modal-body">
                              <iframe src="https://docs.google.com/forms/d/1UOSdVxDtjPajSjXmvyNQuIyRU6-M_j6lcqpedO6PPtc/viewform?edit_requested=true" width="100%" height="1311" frameborder="0" marginheight="0" marginwidth="0"></iframe>
                              <a href="" title="THIẾT KẾ WEBSITE BNC" class="adv_bnc_2 hidden">
                              <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/63/6329/adv/2017/05/05/03/31/1493954299_a2.png"alt="THIẾT KẾ WEBSITE BNC" class="img-responsive"/>
                              </a>
                              <a href="" title="" class="adv_bnc_2 hidden">
                              <img src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/adv/2017/08/08/02/21/1502158778_neYYn.jpg"alt="" class="img-responsive"/>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Form Register Design --><!-- Products Details Tab -->
               <div class="f-product-view-tab">
                  <div class="container">
                     <div class="f-product-view-tab-header">
                        <ul id="f-pr-page-view-tabid" class="nav-tabs">
                           <li class="active"><a href="#f-pr-view-01" data-toggle="tab">Mô tả</a></li>
                           <li ><a href="#f-pr-view-02" data-toggle="tab">Hướng dẫn mua hàng</a></li>
                           <li ><a href="#f-pr-view-04" data-toggle="tab">Mô tả vắn tắt</a></li>
                        </ul>
                        <div class="clearfix"></div>
                     </div>
                     <div class="f-product-view-tab-body tab-content">
                        <div id="f-pr-view-01" class="tab-content tab-pane active">
                           * Chất liệu: mặt gỗ laminate, chân sắt sơn đen tĩnh điện<br  />
                           * Nhận đặt hàng từ 6 chiếc <br  />
                           * Thời gian đặt hàng : 10 ngày
                        </div>
                        <div id="f-pr-view-02" class="tab-content tab-pane ">
                           <p>Đặt hàng online hoặc liên hệ trực tiếp cửa hàng</p>
                           <p> </p>
                        </div>
                        <div id="f-pr-view-03" class="tab-callback tab-pane ">
                           <div class="f-callback-form">
                              <form class="form-horizontal no-padding">
                                 <fieldset>
                                    <div class="form-group">
                                       <label class="col-md-2 control-label" for="txtName">Họ tên:</label>
                                       <div class="col-md-4">
                                          <input id="txtName" name="txtName" type="text" placeholder="Họ tên:..." class="form-control input-md" required="">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-2 control-label" for="txtPhone">Điện thoại:</label>
                                       <div class="col-md-4">
                                          <input id="txtPhone" name="txtPhone" type="text" placeholder="Điện thoại:..." class="form-control input-md" required="">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-2 control-label" for="txtAddress">Địa chỉ:</label>
                                       <div class="col-md-4">
                                          <input id="txtAddress" name="txtAddress" type="text" placeholder="Địa chỉ:" class="form-control input-md" required="">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-2 control-label" for="txtContent">Nội dung</label>
                                       <div class="col-md-4">
                                          <textarea class="form-control" id="txtContent" name="txtContent"></textarea>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-2 control-label" for="btnSent"></label>
                                       <div class="col-md-4 ">
                                          <button id="btnSent" name="btnSent" class="btn btn-primary">Gửi yêu cầu</button>
                                       </div>
                                    </div>
                                 </fieldset>
                              </form>
                           </div>
                        </div>
                        <div id="f-pr-view-04" class="tab-content tab-pane ">
                           1.100.000
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Products Details Tab --><!-- Comment Social -->
               <div class="v2_bnc_comment_social">
                  <div class="container">
                     <div class="v2_bnc_comment_social_body">
                        <div class="v2_bnc_view_comment_social">
                           <div class="v2_bnc_view_comment_social_tab_header">
                              <ul id="v2_bnc_comment_social_tab_id" class="no-margin no-padding nav-tabs">
                                 <li class="active">
                                    <a href="#comment-facebook" data-toggle="tab">Bình luận bằng tài khoản facebook </a>
                                 </li>
                                 <li >
                                    <a href="#commnet-google" data-toggle="tab">Bình luận bằng tài khoản Google</a>
                                 </li>
                              </ul>
                           </div>
                           <div class="clearfix"></div>
                           <div class="v2_bnc_comment_social_body tab-content">
                              <div id="comment-facebook" class="tab-pane fade in active">
                                 <div class="fb-comments" data-href="http://xhome.com.vn/ghe-laminate-gh52-1-1-786802.html" data-width="828" data-numposts="5" data-colorscheme="light"></div>
                              </div>
                              <div id="commnet-google" class="tab-pane fade">
                                 <script src="https://apis.google.com/js/plusone.js"></script>
                                 <div class="g-comments"
                                    data-view_type="FILTERED_POSTMOD"
                                    data-first_party_property="BLOGGER"
                                    data-width="828"
                                    data-href="http://xhome.com.vn/ghe-laminate-gh52-1-1-786802.html">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End Comment Social -->
               <!-- Products Related -->
               <!-- End Products Related -->
            </div>
         </div>
         <!--Like Product-->
		 
         <link rel="stylesheet" type="text/css" href="css/likeproduct.css"/>
         <script src="js/likeproduct.js"></script>
         <link rel="stylesheet" type="text/css" href="css/toastr.css"/>
         <script src="js/toastr.js"></script>
         <link rel="stylesheet" type="text/css" href="css/rater.css"/>
         <script src="js/productrater.js"></script>
		 
		 
         <script type="text/javascript">
            $(document).ready(function() {
                  Likeproduct.init();
               });
         </script>
      </section>
      <!-- End Inside Page -->
<!--Footer-->
<!--------------FOOTER------------>
<?php include('footer/footer.php')?>
<!--End Footer-->

<!-- Adv Rich Meida -->
<div class="hidden-xs">
  </div>

<!-- End Adv Rich Meida -->

<!-- Scroll To Top -->
<div class="v2_bnc_scrolltop">
  <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
    <i class="fa fa-caret-up fa-4x"></i>
  </a>
</div>
<!-- End Scroll To Top -->


<script type="text/javascript" src="js/product.js"></script>
<!-- Include JS -->
<script src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/started_js.js"></script>
<script type="text/javascript" src="js/webcommon.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
<script type="text/javascript" src="js/loading-overlay.min.js"></script>
<script type="text/javascript" src="js/load.js"></script>
<script type="text/javascript" src="js/fastCart.js"></script>
<link rel="stylesheet" href="css/jquery.fancybox.css" /> 
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
<!-- chat facebook -->
				<!--
                <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
                <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>

                    <div class='support-icon-right'>
                        <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                        <div class='online-support'>
                            <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                            </div>
                        </div>
                    </div> -->
                    <!-- End chat facebook -->
					<!--<script type="text/javascript">
                function BNCcallback(data){
                    console.log(data);
                }
                    var url = document.URL;
                    var idW = '7121';
                    var uid='';
                    var title = document.title;

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
                    document.body.appendChild(appsScript);

                    var appsScript = document.createElement('script');
                    appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
                    setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);

                    var _gaq = _gaq || [];
                    _gaq.push(["_setAccount", "UA-43176424-6"]);
                    _gaq.push(["_trackPageview"]);


                      (function() {
                        var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                        ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
                      })();

                </script>-->

                </body>
</html>
