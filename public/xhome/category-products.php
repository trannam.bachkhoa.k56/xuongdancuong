<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
   <!-- Edit By @DTM -->
   <head>
      <!-- Include Header Meta -->
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</title>
      <meta name="description" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
      <meta name="keywords" content="Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam">
      <meta name="robots" content="INDEX, FOLLOW"/>
      <link rel="shortcut icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
      <link rel="icon" href="http://s2.webbnc.vn/uploadv2/web/71/7121/informationbasic/2017/08/07/06/49/1502088427_logo-xhome-01.png" />
      <base href="" />
      <!-- Include CSS -->
      <!-- Reset Css-->
      <link href="css/reset.css" rel="stylesheet" media="screen">
      <!-- End Reset Css-->
      <!-- Bootstrap Css -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <!-- End Bootstrap Css -->
      <!-- Css -->
      <link href="css/owl.carousel.css" rel="stylesheet" media="screen">
      <link href="css/owl.theme.css" rel="stylesheet" media="screen">
      <link href="css/owl.transitions.css" rel="stylesheet" media="screen">
      <link href="css/animate.css" rel="stylesheet" media="screen">
      <link href="css/jquery.nouislider.min.css" rel="stylesheet">
      <link href="css/owl-slideshow-main.css" rel="stylesheet" media="screen">
      <link href="css/style.css" rel="stylesheet" media="screen">
      <!--<link href="css/menu-mobi.css" rel="stylesheet" media="screen">-->
      <link href="css/mobile.css" rel="stylesheet" media="screen">
      <!-- End Css -->
      <!-- FontIcon -->
      <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
      <link rel="stylesheet" href="css/simple-line-icons.css">
      <!-- End FontIcon -->
      <!-- JS -->
      <script type="text/javascript" src="js/jquery.min.js"></script> 
      <script type="text/javascript" src="js/search.js"></script>
      <script type="text/javascript" src="js/jwplayer.js"></script>
      <script src="js/jquery.nouislider.all.min.js"></script>
      <script src="js/owl.carousel.js"></script>
      <script type="text/javascript" src="js/js_customs.js"></script> 
      <!-- End JS --> <!-- End Include CSS -->
   </head>
   <body>
      <div id="fb-root"></div>
      <!-- Include popup so sanh -->
      <!-- So sánh sánh sản phẩm -->
      <div id="f-compare" status="closed">
         <div class="f-compare-title"><i></i><span>So sánh sản phẩm</span> </div>
         <div class="f-compare-body">
            <ul class="f-compare-ul no-margin no-padding">
            </ul>
            <div class="f-compare-info"><span id="compare-notify"></span></div>
            <div class="f-compare-button" style="display: none;"> <a href="/product-compare.html">So sánh </a> </div>
         </div>
      </div>
      <!-- End So sánh sánh sản phẩm --><!-- End include popup so sanh-->
      <!-- Full Code -->
      <!-- Copyright -->
      <h1 style="display: none">Công Ty Cổ Phần Nội Thất X&#039;Home Việt Nam</h1>
      <!-- End Copyright -->
      <!-- Header -->
      <!------------------HEADER-------------->
      <?php include('header/header.php')?>
      <style>
         header{
         position: static !important;
         box-shadow: 0 0 6px rgba(0,0,0,0.3);
         }
      </style>
      <div></div>
      <!-- End Header -->
      <!-- Inside Page -->
      <section class="v2_bnc_inside_page">
         <div class="v2_bnc_content_top"></div>
         <script type="text/javascript">
            var urlNow=document.URL;
         </script>
         <section id="products-main" class="v2_bnc_products_page">
            <!-- Breadcrumbs -->
            <!-- Breadcrumbs -->
            <div class="clearfix"></div>
            <div class="v2_breadcrumb_main padding-top-10">
               <div class="container">
                  <ul class="breadcrumb">
                     <li ><a href="http://xhome.com.vn">Trang chủ</a></li>
                     <li ><a href="http://xhome.com.vn/product.html">Sản Phẩm</a></li>
                     <li ><a href="http://xhome.com.vn/san-pham-le-2-1-278880.html">Sản phẩm lẻ</a></li>
                  </ul>
               </div>
            </div>
            <!-- End Breadcrumbs --> 
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="v2_bnc_title_main">
                        <h1>Sản phẩm lẻ</h1>
                     </div>
                     <div class="v2_bnc_products_page_body col-xs-12 no-padding">
                        <div class="tab-content margin-top-10 row">
                           <div class="v2_bnc_tab_pr">
                              <ul class="nav nav-tabs" role="tablist">
                                 <li role="presentation" class="active"><a href="#catelis0" aria-controls="home" role="tab" data-toggle="tab">Thảm</a></li>
                                 <li role="presentation" class=""><a href="#catelis1" aria-controls="home" role="tab" data-toggle="tab">Đồ trang trí</a></li>
                                 <li role="presentation" class=""><a href="#catelis2" aria-controls="home" role="tab" data-toggle="tab">Sofa</a></li>
                                 <li role="presentation" class=""><a href="#catelis3" aria-controls="home" role="tab" data-toggle="tab">Ghế</a></li>
                                 <li role="presentation" class=""><a href="#catelis4" aria-controls="home" role="tab" data-toggle="tab">Bàn</a></li>
                                 <li role="presentation" class=""><a href="#catelis5" aria-controls="home" role="tab" data-toggle="tab">Đèn</a></li>
                              </ul>
                              <div class="tab-content f-product">
                                 <div role="tabpanel" class="tab-pane active" id="catelis0">
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-01-1-1-742799.html" title="Thảm Geomy 01">
                                             <img alt="Thảm Geomy 01" id="f-pr-image-zoom-id-tab-home-742799" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/41/tham-geomy-01-1501868376.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/41/tham-geomy-01-1501868376.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742799 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-01-1-1-742799.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-01-1-1-742799.html" title="Thảm Geomy 01">Thảm Geomy 01</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-02-vang-nau-1-1-742800.html" title="Thảm Geomy 02 vàng nâu">
                                             <img alt="Thảm Geomy 02 vàng nâu" id="f-pr-image-zoom-id-tab-home-742800" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/41/tham-geomy-02-vang-nau-1501868384.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/41/tham-geomy-02-vang-nau-1501868384.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742800 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-02-vang-nau-1-1-742800.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-02-vang-nau-1-1-742800.html" title="Thảm Geomy 02 vàng nâu">Thảm Geomy 02 vàng nâu</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-03-tam-giac-deu-1-1-742801.html" title="Thảm Geomy 03 tam giác đều">
                                             <img alt="Thảm Geomy 03 tam giác đều" id="f-pr-image-zoom-id-tab-home-742801" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-03-tam-giac-deu-1501868392.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-03-tam-giac-deu-1501868392.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742801 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-03-tam-giac-deu-1-1-742801.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-03-tam-giac-deu-1-1-742801.html" title="Thảm Geomy 03 tam giác đều">Thảm Geomy 03 tam giác đều</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-04-_-tam-giac-da-sac-1-1-742802.html" title="Thảm Geomy 04 _ tam giác đa sắc">
                                             <img alt="Thảm Geomy 04 _ tam giác đa sắc" id="f-pr-image-zoom-id-tab-home-742802" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-04-_-tam-giac-da-sac-1501868404.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-04-_-tam-giac-da-sac-1501868404.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742802 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-04-_-tam-giac-da-sac-1-1-742802.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-04-_-tam-giac-da-sac-1-1-742802.html" title="Thảm Geomy 04 _ tam giác đa sắc">Thảm Geomy 04 _ tam giác đa sắc</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-05-_-xanh-ghi-1-1-742803.html" title="Thảm Geomy 05 _ xanh ghi">
                                             <img alt="Thảm Geomy 05 _ xanh ghi" id="f-pr-image-zoom-id-tab-home-742803" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-05-_-xanh-ghi-1501868411.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-05-_-xanh-ghi-1501868411.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742803 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-05-_-xanh-ghi-1-1-742803.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-05-_-xanh-ghi-1-1-742803.html" title="Thảm Geomy 05 _ xanh ghi">Thảm Geomy 05 _ xanh ghi</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-06-_-nau-ghi-1-1-742804.html" title="Thảm Geomy 06 _ nâu ghi">
                                             <img alt="Thảm Geomy 06 _ nâu ghi" id="f-pr-image-zoom-id-tab-home-742804" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-06-_-nau-ghi-1501868419.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-06-_-nau-ghi-1501868419.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742804 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-06-_-nau-ghi-1-1-742804.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-06-_-nau-ghi-1-1-742804.html" title="Thảm Geomy 06 _ nâu ghi">Thảm Geomy 06 _ nâu ghi</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-07-_-hoa-tiet-den-xanh-1-1-742805.html" title="Thảm Geomy 07 _ họa tiết đen xanh">
                                             <img alt="Thảm Geomy 07 _ họa tiết đen xanh" id="f-pr-image-zoom-id-tab-home-742805" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-07-_-hoa-tiet-den-xanh-1501868426.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/42/tham-geomy-07-_-hoa-tiet-den-xanh-1501868426.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742805 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-07-_-hoa-tiet-den-xanh-1-1-742805.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-07-_-hoa-tiet-den-xanh-1-1-742805.html" title="Thảm Geomy 07 _ họa tiết đen xanh">Thảm Geomy 07 _ họa tiết đen xanh</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-08-_-nau-xanh-1-1-742891.html" title="Thảm Geomy 08 _ nâu xanh">
                                             <img alt="Thảm Geomy 08 _ nâu xanh" id="f-pr-image-zoom-id-tab-home-742891" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/56/tham-geomy-08-_-nau-xanh-1501869279.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/56/tham-geomy-08-_-nau-xanh-1501869279.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742891 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-08-_-nau-xanh-1-1-742891.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-08-_-nau-xanh-1-1-742891.html" title="Thảm Geomy 08 _ nâu xanh">Thảm Geomy 08 _ nâu xanh</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-09-_-nau-hong-1-1-742892.html" title="Thảm Geomy 09 _ nâu hồng">
                                             <img alt="Thảm Geomy 09 _ nâu hồng" id="f-pr-image-zoom-id-tab-home-742892" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/56/tham-geomy-09-_-nau-hong-1501869288.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/56/tham-geomy-09-_-nau-hong-1501869288.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742892 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-09-_-nau-hong-1-1-742892.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-09-_-nau-hong-1-1-742892.html" title="Thảm Geomy 09 _ nâu hồng">Thảm Geomy 09 _ nâu hồng</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-10-_-xanh-ghi-1-1-742893.html" title="Thảm Geomy 10 _ xanh ghi">
                                             <img alt="Thảm Geomy 10 _ xanh ghi" id="f-pr-image-zoom-id-tab-home-742893" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-geomy-10-_-xanh-ghi-1501869292.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-geomy-10-_-xanh-ghi-1501869292.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742893 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-10-_-xanh-ghi-1-1-742893.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-10-_-xanh-ghi-1-1-742893.html" title="Thảm Geomy 10 _ xanh ghi">Thảm Geomy 10 _ xanh ghi</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-geomy-11-_-xanh-be-1-1-742894.html" title="Thảm Geomy 11 _ xanh be">
                                             <img alt="Thảm Geomy 11 _ xanh be" id="f-pr-image-zoom-id-tab-home-742894" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-geomy-11-_-xanh-be-1501869296.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-geomy-11-_-xanh-be-1501869296.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742894 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-geomy-11-_-xanh-be-1-1-742894.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-geomy-11-_-xanh-be-1-1-742894.html" title="Thảm Geomy 11 _ xanh be">Thảm Geomy 11 _ xanh be</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000 - 3.290.000 - 4.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal007b-1-1-742895.html" title="Thảm Dệt Ba tư HAL007B">
                                             <img alt="Thảm Dệt Ba tư HAL007B" id="f-pr-image-zoom-id-tab-home-742895" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal007b-1501869303.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal007b-1501869303.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742895 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal007b-1-1-742895.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal007b-1-1-742895.html" title="Thảm Dệt Ba tư HAL007B">Thảm Dệt Ba tư HAL007B</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal007a-1-1-742896.html" title="Thảm Dệt Ba Tư HAL007A">
                                             <img alt="Thảm Dệt Ba Tư HAL007A" id="f-pr-image-zoom-id-tab-home-742896" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal007a-1501869310.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal007a-1501869310.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742896 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal007a-1-1-742896.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal007a-1-1-742896.html" title="Thảm Dệt Ba Tư HAL007A">Thảm Dệt Ba Tư HAL007A</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal007c-1-1-742897.html" title="Thảm Dệt Ba Tư HAL007C">
                                             <img alt="Thảm Dệt Ba Tư HAL007C" id="f-pr-image-zoom-id-tab-home-742897" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal007c-1501869318.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal007c-1501869318.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742897 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal007c-1-1-742897.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal007c-1-1-742897.html" title="Thảm Dệt Ba Tư HAL007C">Thảm Dệt Ba Tư HAL007C</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal005-1-1-742898.html" title="Thảm Dệt Ba Tư HAL005">
                                             <img alt="Thảm Dệt Ba Tư HAL005" id="f-pr-image-zoom-id-tab-home-742898" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal005-1501869324.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal005-1501869324.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742898 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal005-1-1-742898.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal005-1-1-742898.html" title="Thảm Dệt Ba Tư HAL005">Thảm Dệt Ba Tư HAL005</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal006a-1-1-742899.html" title="Thảm Dệt Ba Tư HAL006A">
                                             <img alt="Thảm Dệt Ba Tư HAL006A" id="f-pr-image-zoom-id-tab-home-742899" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal006a-1501869327.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal006a-1501869327.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742899 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal006a-1-1-742899.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal006a-1-1-742899.html" title="Thảm Dệt Ba Tư HAL006A">Thảm Dệt Ba Tư HAL006A</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal006c-1-1-742900.html" title="Thảm Dệt Ba Tư HAL006C">
                                             <img alt="Thảm Dệt Ba Tư HAL006C" id="f-pr-image-zoom-id-tab-home-742900" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal006c-1501869330.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal006c-1501869330.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742900 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal006c-1-1-742900.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal006c-1-1-742900.html" title="Thảm Dệt Ba Tư HAL006C">Thảm Dệt Ba Tư HAL006C</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal008-1-1-742901.html" title="Thảm Dệt Ba Tư HAL008">
                                             <img alt="Thảm Dệt Ba Tư HAL008" id="f-pr-image-zoom-id-tab-home-742901" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal008-1501869333.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal008-1501869333.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742901 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal008-1-1-742901.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal008-1-1-742901.html" title="Thảm Dệt Ba Tư HAL008">Thảm Dệt Ba Tư HAL008</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal009-1-1-742902.html" title="Thảm Dệt Ba Tư HAL009">
                                             <img alt="Thảm Dệt Ba Tư HAL009" id="f-pr-image-zoom-id-tab-home-742902" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal009-1501869336.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal009-1501869336.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742902 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal009-1-1-742902.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal009-1-1-742902.html" title="Thảm Dệt Ba Tư HAL009">Thảm Dệt Ba Tư HAL009</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tham-det-ba-tu-hal010-1-1-742903.html" title="Thảm Dệt Ba Tư HAl010">
                                             <img alt="Thảm Dệt Ba Tư HAl010" id="f-pr-image-zoom-id-tab-home-742903" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal010-1501869339.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/57/tham-det-ba-tu-hal010-1501869339.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742903 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal010-1-1-742903.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tham-det-ba-tu-hal010-1-1-742903.html" title="Thảm Dệt Ba Tư HAl010">Thảm Dệt Ba Tư HAl010</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000 - 4.750.000 - 7.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane " id="catelis1">
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/guong-simple-round-1-1-742978.html" title="Gương SIMPLE ROUND">
                                             <img alt="Gương SIMPLE ROUND" id="f-pr-image-zoom-id-tab-home-742978" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/05/guong-simple-round-1501869811.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/05/guong-simple-round-1501869811.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742978 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/guong-simple-round-1-1-742978.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/guong-simple-round-1-1-742978.html" title="Gương SIMPLE ROUND">Gương SIMPLE ROUND</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.750.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/guong-twist-line-1-1-742980.html" title="Gương TWISLINE">
                                             <img alt="Gương TWISLINE" id="f-pr-image-zoom-id-tab-home-742980" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/05/guong-twist-line-1501869826.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/05/guong-twist-line-1501869826.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742980 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/guong-twist-line-1-1-742980.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/guong-twist-line-1-1-742980.html" title="Gương TWISLINE">Gương TWISLINE</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.950.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/leony-clock-1-1-743056.html" title="Leony Clock">
                                             <img alt="Leony Clock" id="f-pr-image-zoom-id-tab-home-743056" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/35/leony-clock-1501871593.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/35/leony-clock-1501871593.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743056 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/leony-clock-1-1-743056.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/leony-clock-1-1-743056.html" title="Leony Clock">Leony Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.390.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/pearl-clock-1-1-743057.html" title="Pearl Clock">
                                             <img alt="Pearl Clock" id="f-pr-image-zoom-id-tab-home-743057" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/35/pearl-clock-1501871606.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/35/pearl-clock-1501871606.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743057 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/pearl-clock-1-1-743057.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/pearl-clock-1-1-743057.html" title="Pearl Clock">Pearl Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.390.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/daisy-clock-1-1-743058.html" title="Daisy Clock">
                                             <img alt="Daisy Clock" id="f-pr-image-zoom-id-tab-home-743058" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/35/daisy-clock-1501871621.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/35/daisy-clock-1501871621.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743058 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/daisy-clock-1-1-743058.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/daisy-clock-1-1-743058.html" title="Daisy Clock">Daisy Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.290.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/aroman-clock-1-1-743059.html" title="Aroman Clock">
                                             <img alt="Aroman Clock" id="f-pr-image-zoom-id-tab-home-743059" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/36/aroman-clock-1501871631.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/36/aroman-clock-1501871631.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743059 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/aroman-clock-1-1-743059.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/aroman-clock-1-1-743059.html" title="Aroman Clock">Aroman Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.090.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/jasmine-clock-1-1-743060.html" title="Jasmine Clock">
                                             <img alt="Jasmine Clock" id="f-pr-image-zoom-id-tab-home-743060" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/36/jasmine-clock-1501871644.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/36/jasmine-clock-1501871644.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743060 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/jasmine-clock-1-1-743060.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/jasmine-clock-1-1-743060.html" title="Jasmine Clock">Jasmine Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.090.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/butterfly-clock-1-1-743061.html" title="Butterfly Clock">
                                             <img alt="Butterfly Clock" id="f-pr-image-zoom-id-tab-home-743061" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/36/butterfly-clock-1501871654.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/36/butterfly-clock-1501871654.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743061 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/butterfly-clock-1-1-743061.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/butterfly-clock-1-1-743061.html" title="Butterfly Clock">Butterfly Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.090.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/lissie-clock-1-1-743062.html" title="Lissie Clock">
                                             <img alt="Lissie Clock" id="f-pr-image-zoom-id-tab-home-743062" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/36/lissie-clock-1501871665.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/36/lissie-clock-1501871665.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743062 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/lissie-clock-1-1-743062.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/lissie-clock-1-1-743062.html" title="Lissie Clock">Lissie Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                890.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/phoenix-clock-1-1-743063.html" title="Phoenix Clock">
                                             <img alt="Phoenix Clock" id="f-pr-image-zoom-id-tab-home-743063" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/36/phoenix-clock-1501871677.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/36/phoenix-clock-1501871677.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743063 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/phoenix-clock-1-1-743063.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/phoenix-clock-1-1-743063.html" title="Phoenix Clock">Phoenix Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                890.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/fiona-clock-1-1-743064.html" title="Fiona Clock">
                                             <img alt="Fiona Clock" id="f-pr-image-zoom-id-tab-home-743064" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/36/fiona-clock-1501871687.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/36/fiona-clock-1501871687.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743064 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/fiona-clock-1-1-743064.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/fiona-clock-1-1-743064.html" title="Fiona Clock">Fiona Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.390.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/peacock-clock-1-1-743065.html" title="Peacock Clock">
                                             <img alt="Peacock Clock" id="f-pr-image-zoom-id-tab-home-743065" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/37/peacock-clock-1501871702.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/37/peacock-clock-1501871702.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743065 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/peacock-clock-1-1-743065.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/peacock-clock-1-1-743065.html" title="Peacock Clock">Peacock Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                890.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/rossie-clock-1-1-743066.html" title="Rossie Clock">
                                             <img alt="Rossie Clock" id="f-pr-image-zoom-id-tab-home-743066" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/37/rossie-clock-1501871710.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/37/rossie-clock-1501871710.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743066 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/rossie-clock-1-1-743066.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/rossie-clock-1-1-743066.html" title="Rossie Clock">Rossie Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                890.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/amber-clock-1-1-743067.html" title="Amber Clock">
                                             <img alt="Amber Clock" id="f-pr-image-zoom-id-tab-home-743067" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/37/amber-clock-1501871718.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/37/amber-clock-1501871718.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743067 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/amber-clock-1-1-743067.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/amber-clock-1-1-743067.html" title="Amber Clock">Amber Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.390.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/luna-clock-1-1-743068.html" title="Luna Clock">
                                             <img alt="Luna Clock" id="f-pr-image-zoom-id-tab-home-743068" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/37/luna-clock-1501871721.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/37/luna-clock-1501871721.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743068 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/luna-clock-1-1-743068.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/luna-clock-1-1-743068.html" title="Luna Clock">Luna Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/solia-clock-1-1-743069.html" title="Solia Clock">
                                             <img alt="Solia Clock" id="f-pr-image-zoom-id-tab-home-743069" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/37/solia-clock-1501871733.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/37/solia-clock-1501871733.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743069 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/solia-clock-1-1-743069.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/solia-clock-1-1-743069.html" title="Solia Clock">Solia Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.290.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/lynie-clock-1-1-743070.html" title="Lynie Clock">
                                             <img alt="Lynie Clock" id="f-pr-image-zoom-id-tab-home-743070" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/37/lynie-clock-1501871744.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/37/lynie-clock-1501871744.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743070 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/lynie-clock-1-1-743070.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/lynie-clock-1-1-743070.html" title="Lynie Clock">Lynie Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.390.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/colorblock-clock-1-1-743071.html" title="Colorblock Clock">
                                             <img alt="Colorblock Clock" id="f-pr-image-zoom-id-tab-home-743071" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/38/colorblock-clock-1501871749.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/38/colorblock-clock-1501871749.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743071 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/colorblock-clock-1-1-743071.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/colorblock-clock-1-1-743071.html" title="Colorblock Clock">Colorblock Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/glassy-clock-1-1-743072.html" title="Glassy Clock">
                                             <img alt="Glassy Clock" id="f-pr-image-zoom-id-tab-home-743072" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/38/glassy-clock-1501871766.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/38/glassy-clock-1501871766.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743072 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/glassy-clock-1-1-743072.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/glassy-clock-1-1-743072.html" title="Glassy Clock">Glassy Clock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.150.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 9</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tridney-clock-1-1-743073.html" title="Tridney CLock">
                                             <img alt="Tridney CLock" id="f-pr-image-zoom-id-tab-home-743073" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/38/tridney-clock-1501871773.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/38/tridney-clock-1501871773.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743073 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tridney-clock-1-1-743073.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tridney-clock-1-1-743073.html" title="Tridney CLock">Tridney CLock</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                980.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane " id="catelis2">
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/walken-2-seater-1-1-742988.html" title="WALKEN 2 Seaters">
                                             <img alt="WALKEN 2 Seaters" id="f-pr-image-zoom-id-tab-home-742988" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/07/walken-2-seater-1501869902.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/07/walken-2-seater-1501869902.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742988 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/walken-2-seater-1-1-742988.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/walken-2-seater-1-1-742988.html" title="WALKEN 2 Seaters">WALKEN 2 Seaters</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                7.500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/bari-2-seater-1-1-742989.html" title="BARI 2 Seaters">
                                             <img alt="BARI 2 Seaters" id="f-pr-image-zoom-id-tab-home-742989" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/07/bari-2-seater-1501869927.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/07/bari-2-seater-1501869927.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742989 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/bari-2-seater-1-1-742989.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/bari-2-seater-1-1-742989.html" title="BARI 2 Seaters">BARI 2 Seaters</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                8.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/walken-armchair-1-1-742993.html" title="WALKEN Armchair">
                                             <img alt="WALKEN Armchair" id="f-pr-image-zoom-id-tab-home-742993" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/08/walken-armchair-1501869969.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/08/walken-armchair-1501869969.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742993 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/walken-armchair-1-1-742993.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/walken-armchair-1-1-742993.html" title="WALKEN Armchair">WALKEN Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/scott-armchair-1-1-742996.html" title="SCOTT Armchair">
                                             <img alt="SCOTT Armchair" id="f-pr-image-zoom-id-tab-home-742996" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/08/scott-armchair-1501869999.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/08/scott-armchair-1501869999.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742996 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/scott-armchair-1-1-742996.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/scott-armchair-1-1-742996.html" title="SCOTT Armchair">SCOTT Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/tubby-armchair-1-1-742997.html" title="TUBBY Armchair">
                                             <img alt="TUBBY Armchair" id="f-pr-image-zoom-id-tab-home-742997" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/08/tubby-armchair-1501870006.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/08/tubby-armchair-1501870006.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742997 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/tubby-armchair-1-1-742997.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/tubby-armchair-1-1-742997.html" title="TUBBY Armchair">TUBBY Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/kotka-armchair-1-1-742999.html" title="KOTKA Armchair">
                                             <img alt="KOTKA Armchair" id="f-pr-image-zoom-id-tab-home-742999" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/09/kotka-armchair-1501870032.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/09/kotka-armchair-1501870032.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742999 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/kotka-armchair-1-1-742999.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/kotka-armchair-1-1-742999.html" title="KOTKA Armchair">KOTKA Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/mendini-armchair-1-1-743000.html" title="MENDINI Armchair">
                                             <img alt="MENDINI Armchair" id="f-pr-image-zoom-id-tab-home-743000" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/09/mendini-armchair-1501870043.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/09/mendini-armchair-1501870043.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743000 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/mendini-armchair-1-1-743000.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/mendini-armchair-1-1-743000.html" title="MENDINI Armchair">MENDINI Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/quentin-armchair-1-1-743001.html" title="QUENTIN Armchair">
                                             <img alt="QUENTIN Armchair" id="f-pr-image-zoom-id-tab-home-743001" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/09/quentin-armchair-1501870054.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/09/quentin-armchair-1501870054.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743001 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/quentin-armchair-1-1-743001.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/quentin-armchair-1-1-743001.html" title="QUENTIN Armchair">QUENTIN Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/bantam-armchair-1-1-743002.html" title="BANTAM Armchair">
                                             <img alt="BANTAM Armchair" id="f-pr-image-zoom-id-tab-home-743002" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/09/bantam-armchair-1501870061.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/09/bantam-armchair-1501870061.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743002 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/bantam-armchair-1-1-743002.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/bantam-armchair-1-1-743002.html" title="BANTAM Armchair">BANTAM Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/roland-armchair-1-1-743004.html" title="ROLAND Armchair">
                                             <img alt="ROLAND Armchair" id="f-pr-image-zoom-id-tab-home-743004" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/10/roland-armchair-1501870078.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/10/roland-armchair-1501870078.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743004 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/roland-armchair-1-1-743004.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/roland-armchair-1-1-743004.html" title="ROLAND Armchair">ROLAND Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/halston-armchair-1-1-743005.html" title="HALSTON Armchair">
                                             <img alt="HALSTON Armchair" id="f-pr-image-zoom-id-tab-home-743005" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/10/halston-armchair-1501870089.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/10/halston-armchair-1501870089.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743005 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/halston-armchair-1-1-743005.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/halston-armchair-1-1-743005.html" title="HALSTON Armchair">HALSTON Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/rufus-armchair-1-1-743007.html" title="RUFUS Armchair">
                                             <img alt="RUFUS Armchair" id="f-pr-image-zoom-id-tab-home-743007" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/10/02/10/04/1506938448_2.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/10/02/10/04/1506938448_2.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743007 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/rufus-armchair-1-1-743007.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/rufus-armchair-1-1-743007.html" title="RUFUS Armchair">RUFUS Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/lottie-armchair-1-1-743015.html" title="LOTTIE Armchair">
                                             <img alt="LOTTIE Armchair" id="f-pr-image-zoom-id-tab-home-743015" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/10/02/09/56/1506937951_1.jpg.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/10/02/09/56/1506937951_1.jpg.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743015 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/lottie-armchair-1-1-743015.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/lottie-armchair-1-1-743015.html" title="LOTTIE Armchair">LOTTIE Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/dallas-2-seater-1-1-743026.html" title="DALLAS 2 Seaters">
                                             <img alt="DALLAS 2 Seaters" id="f-pr-image-zoom-id-tab-home-743026" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/26/dallas-2-seater-1501871072.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/26/dallas-2-seater-1501871072.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743026 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/dallas-2-seater-1-1-743026.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/dallas-2-seater-1-1-743026.html" title="DALLAS 2 Seaters">DALLAS 2 Seaters</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/welseley-armchair-1-1-743033.html" title="WOLSELEY Armchair">
                                             <img alt="WOLSELEY Armchair" id="f-pr-image-zoom-id-tab-home-743033" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/28/welseley-armchair-1501871174.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/28/welseley-armchair-1501871174.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743033 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/welseley-armchair-1-1-743033.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/welseley-armchair-1-1-743033.html" title="WOLSELEY Armchair">WOLSELEY Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 6</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/welseley-3-seater-1-1-743034.html" title="WOLSELEY Love Seat">
                                             <img alt="WOLSELEY Love Seat" id="f-pr-image-zoom-id-tab-home-743034" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/28/welseley-3-seater-1501871178.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/28/welseley-3-seater-1501871178.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743034 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/welseley-3-seater-1-1-743034.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/welseley-3-seater-1-1-743034.html" title="WOLSELEY Love Seat">WOLSELEY Love Seat</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                6.650.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/arthus-armchair-1-1-743035.html" title="ARTHUS Armchair">
                                             <img alt="ARTHUS Armchair" id="f-pr-image-zoom-id-tab-home-743035" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/28/arthus-armchair-1501871197.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/28/arthus-armchair-1501871197.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743035 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/arthus-armchair-1-1-743035.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/arthus-armchair-1-1-743035.html" title="ARTHUS Armchair">ARTHUS Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/arthus-2-seater-1-1-743036.html" title="ARTHUS 2 Seaters">
                                             <img alt="ARTHUS 2 Seaters" id="f-pr-image-zoom-id-tab-home-743036" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/29/arthus-2-seater-1501871210.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/29/arthus-2-seater-1501871210.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743036 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/arthus-2-seater-1-1-743036.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/arthus-2-seater-1-1-743036.html" title="ARTHUS 2 Seaters">ARTHUS 2 Seaters</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                7.500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/flynn-armchair-1-1-743039.html" title="FLYNN Armchair">
                                             <img alt="FLYNN Armchair" id="f-pr-image-zoom-id-tab-home-743039" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/29/flynn-armchair-1501871258.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/29/flynn-armchair-1501871258.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743039 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/flynn-armchair-1-1-743039.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/flynn-armchair-1-1-743039.html" title="FLYNN Armchair">FLYNN Armchair</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.900.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/flynn-2-seater-1-1-743040.html" title="FLYNN 2 Seaters">
                                             <img alt="FLYNN 2 Seaters" id="f-pr-image-zoom-id-tab-home-743040" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/30/flynn-2-seater-1501871275.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/30/flynn-2-seater-1501871275.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-743040 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/flynn-2-seater-1-1-743040.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/flynn-2-seater-1-1-743040.html" title="FLYNN 2 Seaters">FLYNN 2 Seaters</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                8.000.0000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane " id="catelis3">
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 6</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-eames-dsw-1-1-742778.html" title="Ghế EAMES DSW">
                                             <img alt="Ghế EAMES DSW" id="f-pr-image-zoom-id-tab-home-742778" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/37/ghe-eames-dsw-1501868130.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/37/ghe-eames-dsw-1501868130.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742778 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-eames-dsw-1-1-742778.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-eames-dsw-1-1-742778.html" title="Ghế EAMES DSW">Ghế EAMES DSW</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-eames-dwr-boc-vai-1-1-742779.html" title="Ghế EAMES DWR Bọc Vải">
                                             <img alt="Ghế EAMES DWR Bọc Vải" id="f-pr-image-zoom-id-tab-home-742779" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/37/ghe-eames-dwr-boc-vai-1501868146.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/37/ghe-eames-dwr-boc-vai-1501868146.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742779 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-eames-dwr-boc-vai-1-1-742779.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-eames-dwr-boc-vai-1-1-742779.html" title="Ghế EAMES DWR Bọc Vải">Ghế EAMES DWR Bọc Vải</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.490.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 9</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-brandon-1-1-742780.html" title="Ghế BRANDON">
                                             <img alt="Ghế BRANDON" id="f-pr-image-zoom-id-tab-home-742780" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-brandon-1501868150.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-brandon-1501868150.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742780 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-brandon-1-1-742780.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-brandon-1-1-742780.html" title="Ghế BRANDON">Ghế BRANDON</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                990.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 6</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-eames-dsr-metal-leg-1-1-742781.html" title="Ghế EAMES DSR Metal Leg">
                                             <img alt="Ghế EAMES DSR Metal Leg" id="f-pr-image-zoom-id-tab-home-742781" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-eames-dsr-metal-leg-1501868171.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-eames-dsr-metal-leg-1501868171.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742781 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-eames-dsr-metal-leg-1-1-742781.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-eames-dsr-metal-leg-1-1-742781.html" title="Ghế EAMES DSR Metal Leg">Ghế EAMES DSR Metal Leg</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-kartell-master-1-1-742783.html" title="Ghế KARTELL MASTER">
                                             <img alt="Ghế KARTELL MASTER" id="f-pr-image-zoom-id-tab-home-742783" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-kartell-master-1501868192.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-kartell-master-1501868192.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742783 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-kartell-master-1-1-742783.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-kartell-master-1-1-742783.html" title="Ghế KARTELL MASTER">Ghế KARTELL MASTER</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.190.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-magis-1-1-742784.html" title="Ghế MAGIS">
                                             <img alt="Ghế MAGIS" id="f-pr-image-zoom-id-tab-home-742784" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-magis-1501868204.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/38/ghe-magis-1501868204.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742784 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-magis-1-1-742784.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-magis-1-1-742784.html" title="Ghế MAGIS">Ghế MAGIS</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.190.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 9</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-viento-1-1-742785.html" title="Ghế VIENTO">
                                             <img alt="Ghế VIENTO" id="f-pr-image-zoom-id-tab-home-742785" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-viento-1501868216.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-viento-1501868216.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742785 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-viento-1-1-742785.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-viento-1-1-742785.html" title="Ghế VIENTO">Ghế VIENTO</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                950.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-tolix-1-1-742786.html" title="Ghế TOLIX">
                                             <img alt="Ghế TOLIX" id="f-pr-image-zoom-id-tab-home-742786" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-tolix-1501868233.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-tolix-1501868233.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742786 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-tolix-1-1-742786.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-tolix-1-1-742786.html" title="Ghế TOLIX">Ghế TOLIX</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                790.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-belloch-stacking-1-1-742787.html" title="Ghế BELLOCH STACKING">
                                             <img alt="Ghế BELLOCH STACKING" id="f-pr-image-zoom-id-tab-home-742787" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-belloch-stacking-1501868242.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-belloch-stacking-1501868242.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742787 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-belloch-stacking-1-1-742787.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-belloch-stacking-1-1-742787.html" title="Ghế BELLOCH STACKING">Ghế BELLOCH STACKING</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.190.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-ong-1-1-742788.html" title="Ghế ONG">
                                             <img alt="Ghế ONG" id="f-pr-image-zoom-id-tab-home-742788" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-ong-1501868255.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-ong-1501868255.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742788 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-ong-1-1-742788.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-ong-1-1-742788.html" title="Ghế ONG">Ghế ONG</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.600.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-eames-daw-1-1-742789.html" title="Ghế EAMES DAW">
                                             <img alt="Ghế EAMES DAW" id="f-pr-image-zoom-id-tab-home-742789" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-eames-daw-1501868269.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/39/ghe-eames-daw-1501868269.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742789 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-eames-daw-1-1-742789.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-eames-daw-1-1-742789.html" title="Ghế EAMES DAW">Ghế EAMES DAW</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-eames-daw-boc-vai-1-1-742790.html" title="Ghế EAMES DAW Bọc Vải">
                                             <img alt="Ghế EAMES DAW Bọc Vải" id="f-pr-image-zoom-id-tab-home-742790" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-eames-daw-boc-vai-1501868281.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-eames-daw-boc-vai-1501868281.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742790 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-eames-daw-boc-vai-1-1-742790.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-eames-daw-boc-vai-1-1-742790.html" title="Ghế EAMES DAW Bọc Vải">Ghế EAMES DAW Bọc Vải</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.990.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-eames-daw-chan-kim-loai-1-1-742791.html" title="Ghế EAMES DAW Chân Kim Loại">
                                             <img alt="Ghế EAMES DAW Chân Kim Loại" id="f-pr-image-zoom-id-tab-home-742791" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-eames-daw-chan-kim-loai-1501868286.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-eames-daw-chan-kim-loai-1501868286.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742791 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-eames-daw-chan-kim-loai-1-1-742791.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-eames-daw-chan-kim-loai-1-1-742791.html" title="Ghế EAMES DAW Chân Kim Loại">Ghế EAMES DAW Chân Kim Loại</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.390.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-ah01-1-1-742792.html" title="Ghế AH01">
                                             <img alt="Ghế AH01" id="f-pr-image-zoom-id-tab-home-742792" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-ah01-1501868299.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-ah01-1501868299.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742792 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-ah01-1-1-742792.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-ah01-1-1-742792.html" title="Ghế AH01">Ghế AH01</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.550.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-hay-acc-1-1-742794.html" title="Ghế HAY ACC">
                                             <img alt="Ghế HAY ACC" id="f-pr-image-zoom-id-tab-home-742794" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-hay-acc-1501868324.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/40/ghe-hay-acc-1501868324.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742794 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-hay-acc-1-1-742794.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-hay-acc-1-1-742794.html" title="Ghế HAY ACC">Ghế HAY ACC</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.690.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-muuto-1-1-742795.html" title="Ghế MUUTO">
                                             <img alt="Ghế MUUTO" id="f-pr-image-zoom-id-tab-home-742795" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/41/ghe-muuto-1501868334.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/41/ghe-muuto-1501868334.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742795 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-muuto-1-1-742795.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-muuto-1-1-742795.html" title="Ghế MUUTO">Ghế MUUTO</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.990.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-ghost-co-tay-1-1-742797.html" title="Ghế GHOST Có Tay">
                                             <img alt="Ghế GHOST Có Tay" id="f-pr-image-zoom-id-tab-home-742797" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/41/ghe-ghost-co-tay-1501868355.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/41/ghe-ghost-co-tay-1501868355.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742797 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-ghost-co-tay-1-1-742797.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-ghost-co-tay-1-1-742797.html" title="Ghế GHOST Có Tay">Ghế GHOST Có Tay</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.750.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-ghost-khong-tay-1-1-742798.html" title="Ghế GHOST Không Tay">
                                             <img alt="Ghế GHOST Không Tay" id="f-pr-image-zoom-id-tab-home-742798" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/41/ghe-ghost-khong-tay-1501868367.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/41/ghe-ghost-khong-tay-1501868367.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742798 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-ghost-khong-tay-1-1-742798.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-ghost-khong-tay-1-1-742798.html" title="Ghế GHOST Không Tay">Ghế GHOST Không Tay</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.550.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-eames-dkr-1-1-742808.html" title="Ghế EAMES DKR">
                                             <img alt="Ghế EAMES DKR" id="f-pr-image-zoom-id-tab-home-742808" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/43/ghe-eames-dkr-1501868454.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/43/ghe-eames-dkr-1501868454.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742808 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-eames-dkr-1-1-742808.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-eames-dkr-1-1-742808.html" title="Ghế EAMES DKR">Ghế EAMES DKR</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ghe-bertoia-1-1-742809.html" title="Ghế BERTOIA">
                                             <img alt="Ghế BERTOIA" id="f-pr-image-zoom-id-tab-home-742809" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/43/ghe-bertoia-1501868468.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/43/ghe-bertoia-1501868468.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742809 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ghe-bertoia-1-1-742809.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ghe-bertoia-1-1-742809.html" title="Ghế BERTOIA">Ghế BERTOIA</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane " id="catelis4">
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-shin_-tam-giac-1-1-742839.html" title="Bàn trà SHIN_ Tam giác">
                                             <img alt="Bàn trà SHIN_ Tam giác" id="f-pr-image-zoom-id-tab-home-742839" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/48/ban-tra-shin_-tam-giac-1501868795.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/48/ban-tra-shin_-tam-giac-1501868795.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742839 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-shin_-tam-giac-1-1-742839.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-shin_-tam-giac-1-1-742839.html" title="Bàn trà SHIN_ Tam giác">Bàn trà SHIN_ Tam giác</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.150.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 3</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-ovarl-ngan-keo-1-1-742841.html" title="Bàn trà OVAL ngăn kéo">
                                             <img alt="Bàn trà OVAL ngăn kéo" id="f-pr-image-zoom-id-tab-home-742841" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/48/ban-tra-ovarl-ngan-keo-1501868807.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/48/ban-tra-ovarl-ngan-keo-1501868807.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742841 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-ovarl-ngan-keo-1-1-742841.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-ovarl-ngan-keo-1-1-742841.html" title="Bàn trà OVAL ngăn kéo">Bàn trà OVAL ngăn kéo</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.450.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 3</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-deny-1-1-742843.html" title="Bàn trà DENY">
                                             <img alt="Bàn trà DENY" id="f-pr-image-zoom-id-tab-home-742843" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/49/ban-tra-deny-1501868836.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/49/ban-tra-deny-1501868836.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742843 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-deny-1-1-742843.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-deny-1-1-742843.html" title="Bàn trà DENY">Bàn trà DENY</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.450.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-jojo-1-1-742844.html" title="Bàn trà JOJO">
                                             <img alt="Bàn trà JOJO" id="f-pr-image-zoom-id-tab-home-742844" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/49/ban-tra-jojo-1501868851.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/49/ban-tra-jojo-1501868851.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742844 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-jojo-1-1-742844.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-jojo-1-1-742844.html" title="Bàn trà JOJO">Bàn trà JOJO</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.450.000 - 2.750.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 1</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-bosa-1-1-742845.html" title="Bàn trà BOSA">
                                             <img alt="Bàn trà BOSA" id="f-pr-image-zoom-id-tab-home-742845" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/49/ban-tra-bosa-1501868864.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/49/ban-tra-bosa-1501868864.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742845 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-bosa-1-1-742845.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-bosa-1-1-742845.html" title="Bàn trà BOSA">Bàn trà BOSA</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                1.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-basic-dang-chu-nhat-1-1-742846.html" title="Bàn trà BASIC dáng chữ nhật">
                                             <img alt="Bàn trà BASIC dáng chữ nhật" id="f-pr-image-zoom-id-tab-home-742846" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-basic-dang-chu-nhat-1501868883.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-basic-dang-chu-nhat-1501868883.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742846 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-basic-dang-chu-nhat-1-1-742846.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-basic-dang-chu-nhat-1-1-742846.html" title="Bàn trà BASIC dáng chữ nhật">Bàn trà BASIC dáng chữ nhật</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-basic-dang-tam-giac-1-1-742847.html" title="Bàn trà BASIC dáng tam giác">
                                             <img alt="Bàn trà BASIC dáng tam giác" id="f-pr-image-zoom-id-tab-home-742847" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-basic-dang-tam-giac-1501868889.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-basic-dang-tam-giac-1501868889.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742847 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-basic-dang-tam-giac-1-1-742847.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-basic-dang-tam-giac-1-1-742847.html" title="Bàn trà BASIC dáng tam giác">Bàn trà BASIC dáng tam giác</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-basic-dang-tron-1-1-742848.html" title="Bàn trà BASIC dáng tròn">
                                             <img alt="Bàn trà BASIC dáng tròn" id="f-pr-image-zoom-id-tab-home-742848" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-basic-dang-tron-1501868893.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-basic-dang-tron-1501868893.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742848 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-basic-dang-tron-1-1-742848.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-basic-dang-tron-1-1-742848.html" title="Bàn trà BASIC dáng tròn">Bàn trà BASIC dáng tròn</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-bo-tivoli-1-1-742849.html" title="Bàn trà bộ TIVOLI">
                                             <img alt="Bàn trà bộ TIVOLI" id="f-pr-image-zoom-id-tab-home-742849" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/09/28/09/28/1506590693_ban-tivoli2.png.png_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/09/28/09/28/1506590693_ban-tivoli2.png.png&mode=resize&size=500x500'" class="BNC-image-add-cart-742849 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-bo-tivoli-1-1-742849.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-bo-tivoli-1-1-742849.html" title="Bàn trà bộ TIVOLI">Bàn trà bộ TIVOLI</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.950.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-bo-ordrup-1-1-742850.html" title="Bàn trà bộ ORDRUP">
                                             <img alt="Bàn trà bộ ORDRUP" id="f-pr-image-zoom-id-tab-home-742850" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-bo-ordrup-1501868907.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-bo-ordrup-1501868907.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742850 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-bo-ordrup-1-1-742850.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-bo-ordrup-1-1-742850.html" title="Bàn trà bộ ORDRUP">Bàn trà bộ ORDRUP</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 5</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-bo-amos-1-1-742851.html" title="Bàn trà bộ AMOS">
                                             <img alt="Bàn trà bộ AMOS" id="f-pr-image-zoom-id-tab-home-742851" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-bo-amos-1501868920.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/50/ban-tra-bo-amos-1501868920.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742851 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-bo-amos-1-1-742851.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-bo-amos-1-1-742851.html" title="Bàn trà bộ AMOS">Bàn trà bộ AMOS</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                5.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 5</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-bo-naus-1-1-742852.html" title="Bàn trà bộ NAUS">
                                             <img alt="Bàn trà bộ NAUS" id="f-pr-image-zoom-id-tab-home-742852" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-bo-naus-1501868937.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-bo-naus-1501868937.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742852 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-bo-naus-1-1-742852.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-bo-naus-1-1-742852.html" title="Bàn trà bộ NAUS">Bàn trà bộ NAUS</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                5.500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-bo-novica-1-1-742853.html" title="Bàn trà bộ NOVICA">
                                             <img alt="Bàn trà bộ NOVICA" id="f-pr-image-zoom-id-tab-home-742853" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-bo-novica-1501868948.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-bo-novica-1501868948.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742853 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-bo-novica-1-1-742853.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-bo-novica-1-1-742853.html" title="Bàn trà bộ NOVICA">Bàn trà bộ NOVICA</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.450.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 6</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-noguchi-1-1-742854.html" title="Bàn trà NOGUCHI">
                                             <img alt="Bàn trà NOGUCHI" id="f-pr-image-zoom-id-tab-home-742854" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-noguchi-1501868959.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-noguchi-1501868959.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742854 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-noguchi-1-1-742854.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-noguchi-1-1-742854.html" title="Bàn trà NOGUCHI">Bàn trà NOGUCHI</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                6.500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-clover-1-1-742855.html" title="Bàn trà CLOVER">
                                             <img alt="Bàn trà CLOVER" id="f-pr-image-zoom-id-tab-home-742855" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-clover-1501868970.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-clover-1501868970.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742855 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-clover-1-1-742855.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-clover-1-1-742855.html" title="Bàn trà CLOVER">Bàn trà CLOVER</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                7.590.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-cao-cap-sterling-1-1-742856.html" title="Bàn trà STERLING">
                                             <img alt="Bàn trà STERLING" id="f-pr-image-zoom-id-tab-home-742856" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-cao-cap-sterling-1501868985.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/51/ban-tra-cao-cap-sterling-1501868985.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742856 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-cao-cap-sterling-1-1-742856.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-cao-cap-sterling-1-1-742856.html" title="Bàn trà STERLING">Bàn trà STERLING</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                7.250.000 - 7.950.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-aula-1-1-742857.html" title="Bàn trà AULA">
                                             <img alt="Bàn trà AULA" id="f-pr-image-zoom-id-tab-home-742857" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/52/ban-tra-aula-1501869000.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/52/ban-tra-aula-1501869000.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742857 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-aula-1-1-742857.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-aula-1-1-742857.html" title="Bàn trà AULA">Bàn trà AULA</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                7.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 6</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-origami-1-1-742858.html" title="Bàn trà ORIGAMI">
                                             <img alt="Bàn trà ORIGAMI" id="f-pr-image-zoom-id-tab-home-742858" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/52/ban-tra-origami-1501869012.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/52/ban-tra-origami-1501869012.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742858 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-origami-1-1-742858.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-origami-1-1-742858.html" title="Bàn trà ORIGAMI">Bàn trà ORIGAMI</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.450.000 - D46cm<br  />
                                                4.750.000 - D56cm<br  />
                                                6.550.000 - D74cm<br  />
                                                7.950.000 - D90cm<br  />
                                                &nbsp;                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 5</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-tra-rantan-1-1-742859.html" title="Bàn trà RANTAN">
                                             <img alt="Bàn trà RANTAN" id="f-pr-image-zoom-id-tab-home-742859" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/52/ban-tra-rantan-1501869027.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/52/ban-tra-rantan-1501869027.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742859 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-tra-rantan-1-1-742859.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-tra-rantan-1-1-742859.html" title="Bàn trà RANTAN">Bàn trà RANTAN</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.500.000 - 7.500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 3</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/ban-goc-rantan-1-1-742941.html" title="Bàn góc RANTAN">
                                             <img alt="Bàn góc RANTAN" id="f-pr-image-zoom-id-tab-home-742941" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/06/00/ban-goc-rantan-1501869494.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/06/00/ban-goc-rantan-1501869494.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742941 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/ban-goc-rantan-1-1-742941.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/ban-goc-rantan-1-1-742941.html" title="Bàn góc RANTAN">Bàn góc RANTAN</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane " id="catelis5">
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 5</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tran-diamond-1-1-742796.html" title="Đèn trần Diamond">
                                             <img alt="Đèn trần Diamond" id="f-pr-image-zoom-id-tab-home-742796" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/41/den-tran-diamond-1501868341.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/41/den-tran-diamond-1501868341.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742796 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tran-diamond-1-1-742796.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tran-diamond-1-1-742796.html" title="Đèn trần Diamond">Đèn trần Diamond</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                590.000 - 1.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 9</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-coco-flip-1-1-742836.html" title="Đèn Thả Coco Flip">
                                             <img alt="Đèn Thả Coco Flip" id="f-pr-image-zoom-id-tab-home-742836" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/48/den-tha-coco-flip-1501868770.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/48/den-tha-coco-flip-1501868770.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742836 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-coco-flip-1-1-742836.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-coco-flip-1-1-742836.html" title="Đèn Thả Coco Flip">Đèn Thả Coco Flip</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                990.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 8</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-mirror-ball-1-1-742837.html" title="Đèn thả Mirror Ball">
                                             <img alt="Đèn thả Mirror Ball" id="f-pr-image-zoom-id-tab-home-742837" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/48/den-tha-mirror-ball-1501868777.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/48/den-tha-mirror-ball-1501868777.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742837 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-mirror-ball-1-1-742837.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-mirror-ball-1-1-742837.html" title="Đèn thả Mirror Ball">Đèn thả Mirror Ball</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-019-1-1-742860.html" title="Đèn thả 019">
                                             <img alt="Đèn thả 019" id="f-pr-image-zoom-id-tab-home-742860" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/52/den-tha-019-1501869043.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/52/den-tha-019-1501869043.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742860 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-019-1-1-742860.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-019-1-1-742860.html" title="Đèn thả 019">Đèn thả 019</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                790.000 - 890.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 5</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-bo-1-1-742861.html" title="Đèn Thả Bo">
                                             <img alt="Đèn Thả Bo" id="f-pr-image-zoom-id-tab-home-742861" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-bo-1501869052.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-bo-1501869052.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742861 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-bo-1-1-742861.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-bo-1-1-742861.html" title="Đèn Thả Bo">Đèn Thả Bo</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                500.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-093-1-1-742862.html" title="Đèn Thả 093">
                                             <img alt="Đèn Thả 093" id="f-pr-image-zoom-id-tab-home-742862" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-093-1501869057.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-093-1501869057.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742862 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-093-1-1-742862.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-093-1-1-742862.html" title="Đèn Thả 093">Đèn Thả 093</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                790.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 5</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-dandelion-1-1-742863.html" title="Đèn Thả Dandelion">
                                             <img alt="Đèn Thả Dandelion" id="f-pr-image-zoom-id-tab-home-742863" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-dandelion-1501869060.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-dandelion-1501869060.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742863 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-dandelion-1-1-742863.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-dandelion-1-1-742863.html" title="Đèn Thả Dandelion">Đèn Thả Dandelion</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                5.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-raimond-1-1-742864.html" title="Đèn Thả Raimond">
                                             <img alt="Đèn Thả Raimond" id="f-pr-image-zoom-id-tab-home-742864" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-raimond-1501869066.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-raimond-1501869066.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742864 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-raimond-1-1-742864.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-raimond-1-1-742864.html" title="Đèn Thả Raimond">Đèn Thả Raimond</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.890.000 - 6.890.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-etch-01-1-1-742865.html" title="Đèn Thả Etch 01">
                                             <img alt="Đèn Thả Etch 01" id="f-pr-image-zoom-id-tab-home-742865" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-etch-01-1501869074.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-etch-01-1501869074.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742865 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-etch-01-1-1-742865.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-etch-01-1-1-742865.html" title="Đèn Thả Etch 01">Đèn Thả Etch 01</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.950.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-etch-02-1-1-742866.html" title="Đèn Thả Etch 02">
                                             <img alt="Đèn Thả Etch 02" id="f-pr-image-zoom-id-tab-home-742866" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-etch-02-1501869078.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-etch-02-1501869078.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742866 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-etch-02-1-1-742866.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-etch-02-1-1-742866.html" title="Đèn Thả Etch 02">Đèn Thả Etch 02</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.950.000-4.250.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 6</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-random-1-1-742867.html" title="Đèn Thả Random">
                                             <img alt="Đèn Thả Random" id="f-pr-image-zoom-id-tab-home-742867" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-random-1501869090.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-tha-random-1501869090.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742867 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-random-1-1-742867.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-random-1-1-742867.html" title="Đèn Thả Random">Đèn Thả Random</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                650.000&nbsp;- 2.750.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 3</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-chum-heracleum-1-1-742868.html" title="Đèn Chùm Heracleum">
                                             <img alt="Đèn Chùm Heracleum" id="f-pr-image-zoom-id-tab-home-742868" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-chum-heracleum-1501869098.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-chum-heracleum-1501869098.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742868 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-chum-heracleum-1-1-742868.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-chum-heracleum-1-1-742868.html" title="Đèn Chùm Heracleum">Đèn Chùm Heracleum</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.550.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 7</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-chum-crown-minor-1-1-742869.html" title="Đèn Chùm Crown Minor">
                                             <img alt="Đèn Chùm Crown Minor" id="f-pr-image-zoom-id-tab-home-742869" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/53/den-chum-crown-minor-1501869108.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/53/den-chum-crown-minor-1501869108.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742869 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-chum-crown-minor-1-1-742869.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-chum-crown-minor-1-1-742869.html" title="Đèn Chùm Crown Minor">Đèn Chùm Crown Minor</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                7.450.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-chum-modo-1-1-742870.html" title="Đèn Chùm Modo">
                                             <img alt="Đèn Chùm Modo" id="f-pr-image-zoom-id-tab-home-742870" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-modo-1501869114.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-modo-1501869114.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742870 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-chum-modo-1-1-742870.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-chum-modo-1-1-742870.html" title="Đèn Chùm Modo">Đèn Chùm Modo</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 4</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-chum-modo-vintage-1-1-742871.html" title="Đèn Chùm Modo Vintage">
                                             <img alt="Đèn Chùm Modo Vintage" id="f-pr-image-zoom-id-tab-home-742871" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-modo-vintage-1501869120.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-modo-vintage-1501869120.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742871 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-chum-modo-vintage-1-1-742871.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-chum-modo-vintage-1-1-742871.html" title="Đèn Chùm Modo Vintage">Đèn Chùm Modo Vintage</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                4.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 3</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-chum-adel-bubble-1-1-742872.html" title="Đèn Chùm Adel Bubble">
                                             <img alt="Đèn Chùm Adel Bubble" id="f-pr-image-zoom-id-tab-home-742872" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-adel-bubble-1501869128.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-adel-bubble-1501869128.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742872 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-chum-adel-bubble-1-1-742872.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-chum-adel-bubble-1-1-742872.html" title="Đèn Chùm Adel Bubble">Đèn Chùm Adel Bubble</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.850.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 6</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-chum-agnes-1-1-742873.html" title="Đèn Chùm AGNES">
                                             <img alt="Đèn Chùm AGNES" id="f-pr-image-zoom-id-tab-home-742873" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-agnes-1501869138.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-agnes-1501869138.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742873 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-chum-agnes-1-1-742873.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-chum-agnes-1-1-742873.html" title="Đèn Chùm AGNES">Đèn Chùm AGNES</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                <div>6.450.000</div>                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 2</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-origami-birds-1-1-742874.html" title="Đèn Thả Origami Birds">
                                             <img alt="Đèn Thả Origami Birds" id="f-pr-image-zoom-id-tab-home-742874" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/54/den-tha-origami-birds-1501869147.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/54/den-tha-origami-birds-1501869147.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742874 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-origami-birds-1-1-742874.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-origami-birds-1-1-742874.html" title="Đèn Thả Origami Birds">Đèn Thả Origami Birds</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                2.750.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 3</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-tha-bocci-1-1-742875.html" title="Đèn Thả Bocci">
                                             <img alt="Đèn Thả Bocci" id="f-pr-image-zoom-id-tab-home-742875" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/54/den-tha-bocci-1501869152.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/54/den-tha-bocci-1501869152.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742875 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-tha-bocci-1-1-742875.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-tha-bocci-1-1-742875.html" title="Đèn Thả Bocci">Đèn Thả Bocci</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                3.490.000&nbsp;                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                    <div class="v2_bnc_pr_item_box col-md-3 col-sm-6 col-xs-6 full-xs margin-bottom-30">
                                       <div class="v2_bnc_pr_item">
                                          <div class="v2_bnc_pr_item_saleof v2_bnc_btn_sale"><span>- 9</span></div>
                                          <figure class="v2_bnc_pr_item_img">
                                             <a href="http://xhome.com.vn/den-chum-twisty-1-1-742876.html" title="Đèn Chùm Twisty">
                                             <img alt="Đèn Chùm Twisty" id="f-pr-image-zoom-id-tab-home-742876" src="https://cdn-img-v2.webbnc.net/uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-twisty-1501869160.jpg_resize500x500.jpg" onerror="this.onerror=null;this.src='http://upload2.webbnc.vn/view.php?image=uploadv2/web/71/7121/product/2017/08/04/05/54/den-chum-twisty-1501869160.jpg&mode=resize&size=500x500'" class="BNC-image-add-cart-742876 img-responsive"/>
                                             </a>
                                             <figcaption class="v2_bnc_pr_item_boxdetails">
                                                <a href="http://xhome.com.vn/den-chum-twisty-1-1-742876.html" class="links_fixed">
                                                   <div class="hvr-shutter-in-vertical">
                                                      <div class="v2_bnc_pr_item_boxdetails_border">
                                                         <div class="v2_bnc_pr_item_boxdetails_content">
                                                            <!-- Products Name -->  
                                                            <h3 class="v2_bnc_pr_item_name">
                                                <a href="http://xhome.com.vn/den-chum-twisty-1-1-742876.html" title="Đèn Chùm Twisty">Đèn Chùm Twisty</a></h3>
                                                <!-- End Products Name --> 
                                                <!-- Details -->
                                                <div class="v2_bnc_pr_item_short_info">
                                                950.000                            </div>
                                                <!-- End Details -->
                                                </div>
                                                </div>
                                                </div>    
                                                </a>  
                                             </figcaption>
                                          </figure>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="v2_bnc_pagination">
                              <div class="row">
                                 <div class="col-md-6 hidden">
                                    <p class="v2_bnc_pagination_title"> Hiển thị từ                <strong>1  </strong> đến                <strong>12    </strong> trên                <strong>566  </strong> bản ghi - Trang số                <strong>1   </strong> trên                <strong>48  </strong> trang            </p>
                                 </div>
                                 <div class="v2_bnc_pagination_button text-center col-md-12 margin-top-20 margin-bottom-20">
                                    <ul class="pagination">
                                       <li class="disabled"><a> ← </a></li>
                                       <li class="active"><a >1</a></li>
                                       <li ><a href="/san-pham-le-2-1-278880-p2.html">2</a></li>
                                       <li ><a href="/san-pham-le-2-1-278880-p3.html">3</a></li>
                                       <li ><a href="/san-pham-le-2-1-278880-p4.html">4</a></li>
                                       <li ><a href="/san-pham-le-2-1-278880-p5.html">5</a></li>
                                       <li><a href="/san-pham-le-2-1-278880-p2.html">→</a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </section>
      <!-- End Inside Page -->
      <!--Footer-->
      <!--------------FOOTER------------>
      <?php include('footer/footer.php')?>
      <!--End Footer-->
      <!-- Adv Rich Meida -->
      <div class="hidden-xs">
      </div>
      <script> 
         jQuery(function ($) { 
             $('#show').hide();
             $('#close').click(function (e) {
               $('#rich-media').hide('slow');
               return false;
             });
             $('#hide').click(function (e) {
               $('#show').show('slow');
               $('#hide').hide('slow');
               $('#rich-media-item').height(0);
               return false;
             });
             $('#show').click(function (e) {
               $('#hide').show('slow'); 
               $('#show').hide('slow');
               $('#rich-media-item').height(205);
               return false;
            });
         });
      </script>
      <!-- End Adv Rich Meida -->
      <!-- Scroll To Top -->
      <div class="v2_bnc_scrolltop">
         <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
         <i class="fa fa-caret-up fa-4x"></i>
         </a>
      </div>
      <!-- End Scroll To Top -->
      <script type="text/javascript" src="js/product.js"></script>
      <!-- Include JS -->
      <script src="js/bootstrap.js"></script> 
      <script type="text/javascript" src="js/started_js.js"></script>
      <script type="text/javascript" src="js/webcommon.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine.js"></script>
      <script type="text/javascript" src="js/jquery.validationEngine-vi.js"></script>
      <script type="text/javascript" src="js/loading-overlay.min.js"></script>
      <script type="text/javascript" src="js/load.js"></script>
      <script type="text/javascript" src="js/fastCart.js"></script>
      <link rel="stylesheet" href="css/jquery.fancybox.css" />
      <script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
      <script type="text/javascript" src="js/jquery.elevatezoom.js"></script>
      <script type="text/javascript" src="js/jquery.cookie.js"></script><!-- End Include JS -->
      <!-- chat facebook -->
      <!--
         <link href='http://demo.v2.webbnc.net/themes/1/statics/css/hethong.css' rel='stylesheet' media='screen'>
         <script type='text/javascript' src='http://demo.v2.webbnc.net/themes//1/statics/js/hethong.js'></script>
         
             <div class='support-icon-right'>
                 <h3><i class='fa fa-comments'></i> Chat Facebook </h3>
                 <div class='online-support'>
                     <div class='fb-page' data-href='https://www.facebook.com/xhomenoithatthongminh' data-small-header='true' data-height='300' data-width='250' data-tabs='messages' data-adapt-container-width='false' data-hide-cover='false' data-show-facepile='false' data-show-posts='false'>
                     </div>
                 </div>
             </div> -->
      <!-- End chat facebook -->
      <!--<script type="text/javascript">
         function BNCcallback(data){
             console.log(data);
         }
             var url = document.URL;
             var idW = '7121';
             var uid='';
             var title = document.title;
         
             var appsScript = document.createElement('script');
             appsScript.src = "http://apps.webbnc.vn/app3/themes/default/js/iframeResizer.js";
             document.body.appendChild(appsScript);
         
             var appsScript = document.createElement('script');
             appsScript.src = "http://apps.webbnc.vn/app3/?token=t2d32243i202r2x272y2r2c352y2x1731223e2d3q2v112i11213w2c1s202t1i1g2v1l1r242f1w233q2e1d103421362a3q2b2h1e1q203b3a31223c1p1";
             setTimeout(function(){ document.body.appendChild(appsScript); }, 1000);
         
             var _gaq = _gaq || [];
             _gaq.push(["_setAccount", "UA-43176424-6"]);
             _gaq.push(["_trackPageview"]);
         
         
               (function() {
                 var ge = document.createElement("script"); ge.type = "text/javascript"; ge.async = true;
                 ge.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
                 var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ge, s);
               })();
         
         </script>-->
   </body>
</html>