      
$(document).ready(function(){

	var qc          = $('#qc_news').html();
	var length_div 	= $(".content_expand_content > div").length;
	var length_p   	= $(".content_expand_content > p").length;
	var length_span = $(".content_expand_content > span").length;
	var length_img  = $(".content_expand_content > img").length;
	
	//console.log(length_p);
	if(length_div>0){
			var i = Math.round(($(".content_expand_content > div").length)/2);
			$(".content_expand_content > div:nth-child("+i+")").after(qc);
		
		}
	else if(length_p>0){

			var i = Math.round(($(".content_expand_content > p").length)/2);
			$(".content_expand_content > p:nth-child("+i+")").after(qc);
		
		}
	else if(length_span>0){
			var i = Math.round(($(".content_expand_content > span").length)/2);
			$(".content_expand_content > span:nth-child("+i+")").after(qc);
		
		}
	else{
			var i = Math.round(($(".content_expand_content > img").length)/2);
			$(".content_expand_content > img:nth-child("+i+")").after(qc);
		}
	
	//clear trong qc tam
	$('#qc_news').html('');
	
	//dem tat ca cac img trong noi dung
	$('.content_expand_content img').each(function() {
		$(this).parent('p').addClass('image_content');
	});
	$('#content_expand_content img').each(function() {
		$(this).parent('p').addClass('image_content');
	});
	
	// ajax tin cung chuyen muc
	Ajax_chuyende(1);  	
	
});
						

$(function() {
	/************slide quangcao************/
	$("#quang_slide").home_slide({
		time: 8000,	
	});	
	
	$('.content_content_qc .slide_tungnhich6').slick({
		responsive: [
			{
			breakpoint: 1230,
				settings: {
					slidesToShow: 1,
				}
			},
			{
			breakpoint: 992,
				settings: {
					slidesToShow: 1,
				}
			},
			{
			breakpoint: 767,
				settings: {
					slidesToShow: 1,
				}
			},
			{
			breakpoint: 567,
				settings: {
					slidesToShow: 1,
				}
			},
			{
			breakpoint: 482,
				settings: {
					slidesToShow: 1,
				}
			}
		],
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000, 
			dots: true,
			arrows: false,
			pauseOnFocus:false,
		});
});

//ajax xkld
function Ajax_chuyende(page)
{
	
	data  = {page:page,id:id_news,id_cd:id_cd_news};
	//console.log(data);
	$.ajax({
	 	url :'/tintucsukien/ajax-chuyende',
        type: 'post',
        data: data,
        dataType:'json',
        beforeSend:function () {
        	
        },
        success: function (data) {
        	//console.log(data);
        	if(data !=0){
        		$("#ajax_chuyende").html(data);
        		$("#paging_chuyende").html(Paging_chuyenmuc(data));	

        	}else{
        		$("#news_chuyende").hide();	 
        	}
        }
    });
}

function Paging_chuyenmuc(str)
{
	var live_str = $('<div>',{html:str});
	var example  = live_str.find('#paging_chuyende_html').html();
	return example;
}
//ajax xkld