
/************scroll top************/
$(document).ready(function(){
	$(function(){	 
		$(document).on( 'scroll', function(){
	 
			if ($(window).scrollTop() > 100) {
				$('.scroll-top-wrapper').addClass('show');
			} else {
				$('.scroll-top-wrapper').removeClass('show');
			}
			
			//nut chia se viec lam chi tiet thuc tap sinh
			if ($(window).scrollTop() > ($('.content_expand.xuatkhaulaodong_chitiet').height() - 500)) {
				$('.content_social').addClass('share-pause');
				$('.content_social').removeClass('share-fix');
				$('.content_social').css('top',($('.content_expand.xuatkhaulaodong_chitiet').height() - 500)+'px');
			}else{
				$('.content_social').addClass('share-fix');
				$('.content_social').removeClass('share-pause');
				$('.content_social').css('top','');
			}
			//nut chia se viec lam chi tiet thuc tap sinh

		});
	 
		$('.scroll-top-wrapper').on('click', scrollToTop);
	});
	 
	function scrollToTop() {
		verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		element = $('body');
		offset = element.offset();
		offsetTop = offset.top;
		$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
	}
});
