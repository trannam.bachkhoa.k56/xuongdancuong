
	//slide 3 news
		var stPt = 0, elToShow = 3; //showing 5 elements
		var $nav_wrapper = $('.slide_tungnhich');
		var $list = $nav_wrapper.find('.carousel-item'); //get the list of div's
		var $copy_list = [];
		var copy_lgt = $list.length - elToShow;
	
		//call to set thumbnails based on what is set
		initNav();
		function initNav() {
		   var tmp;
		   for (var i = elToShow; i < $list.length; i++) {
		      tmp = $list.eq(i);
		      $copy_list.push(tmp.clone());
		      tmp.remove();
		   }
		}
	
		$('#nx_news').click (function () {
		    $list = $nav_wrapper.find('.carousel-item'); //get the list of div's
		        
		    //move the 1st element clone to the last position in copy_list
		    $copy_list.splice(copy_lgt, 0, $list.eq(0).clone() ); //array.splice(index,howmany,element1,.....,elementX)
		    
		    //kill the 1st element in the div
		    $list.eq(0).remove();
		    
		    //add to the last
		    $nav_wrapper.append($copy_list.shift());
		});
	
		$('#pr_news').click (function () {
		    $list = $nav_wrapper.find('.carousel-item'); //get the list of li's
		        
		    //move the 1st element clone to the last position in copy_li
		    $copy_list.splice(0, 0, $list.eq(elToShow-1).clone()); //array.splice(index,howmany,element1,.....,elementX)
		    
		    //kill the 1st element in the UL
		    $list.eq(elToShow-1).remove();
		    
		    //add to the last
		    $nav_wrapper.prepend($copy_list.pop());
	
		});
	//slide 3 news

	

$(document).ready(function(){
	
    /************slide tin admin chá»n************/	
	
	$('.content_content_news .slide_tungnhich5').slick({
	responsive: [
		{
		breakpoint: 1230,
			settings: {
				slidesToShow: 4,
			}
		},
		{
		breakpoint: 992,
			settings: {
				slidesToShow: 3,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 567,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 482,
			settings: {
				slidesToShow: 1,
			}
		}
	],
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 8000, 
	});
	
	/************slide magazine************/	
	
	$('.content_magazine_content .slide_tungnhich2').slick({
	responsive: [
		{
		breakpoint: 1230,
			settings: {
				slidesToShow: 4,
			}
		},
		{
		breakpoint: 992,
			settings: {
				slidesToShow: 3,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 567,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 482,
			settings: {
				slidesToShow: 1,
			}
		}
	],
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 8000, 
	});
	
	/************slide quangcao************/	
	$('.content_content_qc .slide_tungnhich6').slick({
	responsive: [
		{
		breakpoint: 1230,
			settings: {
				slidesToShow: 1,
			}
		},
		{
		breakpoint: 992,
			settings: {
				slidesToShow: 1,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 1,
			}
		},
		{
		breakpoint: 567,
			settings: {
				slidesToShow: 1,
			}
		},
		{
		breakpoint: 482,
			settings: {
				slidesToShow: 1,
			}
		}
	],
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000, 
		dots: true,
		arrows: false,
		pauseOnFocus:false,
	});
	
	/************slide logo************/	
	$('.slide_tungnhich3').slick({
	responsive: [
		{
		breakpoint: 1230,
			settings: {
				slidesToShow: 4,
			}
		},
		{
		breakpoint: 1024,
			settings: {
				slidesToShow: 3,
			}
		},
		{
		breakpoint: 768,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 740,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 567,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 482,
			settings: {
				slidesToShow: 1,
			}
		}
	],
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 8000, 
	});
	
	/************slide youtube************/
	
	$("#scroll_list").css('height',$(".content_slide_youtube_display").height());	//can bang chieu cao hai ben

	$(".video_ol_tablet").html($('.video_ol_desktop li.active').text());
	$("#scroll_list").mCustomScrollbar();
	
	//next video tablet
	$(".content_slide_youtube_prnt .next_news").click(function() {	
		var next = $('.video_ol_desktop li.active').next();
		if(next.length){
			$(".video_ol_desktop li").removeClass('active');
			var title_video = next.text();next.addClass('active');
			var src_video   = next.attr('data-rel');
		}else{
			$(".video_ol_desktop li").removeClass('active');
			$('.video_ol_desktop li').first().addClass('active');
			var title_video = $('.video_ol_desktop li').first().text();
			var src_video   = $('.video_ol_desktop li').first().attr('data-rel');
		}
		$('#video_show').attr('src','https://www.youtube.com/embed/'+src_video);
		$(".video_ol_tablet").html(title_video);
	});
	//pre video tablet
	$(".content_slide_youtube_prnt .pre_news").click(function() {	
		var prev = $('.video_ol_desktop li.active').prev();
		if(prev.length){
			$(".video_ol_desktop li").removeClass('active');
			var title_video = prev.text();prev.addClass('active');
			var src_video   = prev.attr('data-rel');
		}else{
			$(".video_ol_desktop li").removeClass('active');
			$('.video_ol_desktop li').last().addClass('active');
			var title_video = $('.video_ol_desktop li').last().text();
			var src_video   = $('.video_ol_desktop li').last().attr('data-rel');
		}
		$('#video_show').attr('src','https://www.youtube.com/embed/'+src_video);
		$(".video_ol_tablet").html(title_video);
		
	});
		
	$("#scroll_list ol li").click(function() {	
		$("#scroll_list ol li").removeClass('active');
		$(this).addClass('active');
		$('#video_show').attr('src','https://www.youtube.com/embed/'+$(this).attr('data-rel'));
		$('.content_slide_youtube_title').text($(this).text());
		
	});

	/************slide youtube************/
});

/************slide home************/
$(function() {
	$("#home_slide").home_slide({
		time: 8000,
		
	});
	$("#home_slide .paging .each").each(function() {
		$(this).home_slide_paging();
	});
	/************slide quangcao************/
	$("#quang_slide").home_slide({
		time: 8000,	
	});	
});

/************tab home************/
$('#myTabs a').click(function (e) {
	e.preventDefault();
	//$(this).tab('show');
});

/************scroll number************/
$(document).ready(function (){
	var total01 = parseInt($('.number_content_content_row_number .total01').text().replace('.', ''));
	$('.number_content_content_row_number .total01').spincrement({
		from: 0.0,
		to: total01,
		decimalPlaces: 0,
		duration: 2000,
		thousandSeparator: ''
	});
	var total02 = parseInt($('.number_content_content_row_number .total02').text().replace('.', ''));
	$('.number_content_content_row_number .total02').spincrement({
		from: 0.0,
		to: total02,
		decimalPlaces: 0,			
		duration: 2000,
		thousandSeparator: ''
	});
	var total03 = parseInt($('.number_content_content_row_number .total03').text().replace('.', ''));
	$('.number_content_content_row_number .total03').spincrement({
		from: 0.0,
		to: total03,
		decimalPlaces: 0,
		duration: 2000,
		thousandSeparator: ''
	});
});


