/************************************************************************
####################################################
home_slide_paging.js Version 1.0.0
Copyright by MĂƒÂ©o 2012
Website: meo.com.vn
Building based on
	+ jQuery library at http://jquery.com/
	+ jQuery UI library at http://jqueryui.com/
	+ jquery.MTool at http://meo.com.vn/

Public: 2012-11-21 14:14:12
####################################################
************************************************************************/
$.fn.home_slide_paging = function(config) {
	var e = this;
	e.config = {
		coutdown: "home_slide_paging" + Math.random(),
		class_rotate: ".img",
		time: 24,
		deg: 0,
		step: 0.5,
		number: 0
	};
	
	/* rotate_right */
	$.fn.home_slide_paging.rotate_right = function() {
		e.rotate_right();
	}
	e.rotate_right = function() {
		/*if(MTool.CheckBrowser("IE", 6)) {
			e.config.number++;
			if(e.config.number >= 1) {
				return false;
			}
		}*/
		
		var step = e.config.step;
		
		e.config.deg += step;
		e.config.deg = e.config.deg >= 360 ? 0 : e.config.deg;
		
		e.find(e.config.class_rotate).rotate(e.config.deg);
		
		$.doTimeout(e.config.coutdown, e.config.time, function() {
			e.rotate_right();
		});
	}
	/* rotate_right */
	
	/* config */
	if(typeof(config) == "object") {
		for(i in config) {
			eval("e.config." + i + " = config." + i);
		}
	}
	
	/*console.log(e.length);*/
	/*e.mouseover(function() {
		e.addClass("each_active");
	});*/
	/* config */
	
	/* start */
	e.rotate_right();
	/* start */
}