/************************************************************************
####################################################
home_slide.js Version 1.0.0
Copyright by MĂƒÂ©o 2012
Website: meo.com.vn
Building based on
	+ jQuery library at http://jquery.com/
	+ jQuery UI library at http://jqueryui.com/
	+ jquery.MTool at http://meo.com.vn/

Public: 2012-08-29 13:44:33
####################################################
************************************************************************/
$.fn.home_slide = function(config) {
	var e = this;
	e.config = {
		coutdown: "home_slide",
		id_each: ".slide_each",
		key: -1,
		finish: false,
		time: 5000
	};
	
	/* config */
	if(typeof(config) == "object") {
		for(i in config) {
			eval("e.config." + i + " = config." + i);
		}
	}
	
	e.show = function(key) {
		if(!e.config.finish) return;
		
		e.config.finish = false;
		clearTimeout(e.config.coutdown);
		
		key = (!key || key >= e.find(e.config.id_each).length) ? 0 : key;
		key = key < 0 ? e.find(e.config.id_each).length - 1 : key;
		
		if(key == e.config.key) {
			e.config.finish = true;
			return;
		}
		
		e.config.key = key;
		
		e.find(e.config.id_each).fadeOut("slow");
		e.find(e.config.id_each).eq(e.config.key).stop(true, true).fadeIn("slow", function() {
			e.config.finish = true;
			$.doTimeout(e.config.coutdown, e.config.time, function() {
				e.show(e.config.key + 1);
			});
			$('.header_slide').attr('class','header_slide ' + e.find(".slide_each:visible").attr('data-rel'));
			$('.header_news').attr('class','header_news ' + e.find(".slide_each:visible").attr('data-rel'));
		});
	}
	
	e.find(e.config.id_each).each(function() {
		$(this).find(".paging .each").each(function(i) {
			$(this).click(function() {
				e.show(i);
			});
		});
	});
	/* config */
	
	/* start */
	e.config.finish = true;
	e.show();
	/* start */
}