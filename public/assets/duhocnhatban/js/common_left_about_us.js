
/************slide magazine left************/	
$(document).ready(function(){
	$('.slide_tungnhich4').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		focusOnSelect: true,
		vertical: true,	
	});

	$('a[data-slide]').click(function(e) {
		e.preventDefault();
		var slideno = $(this).data('slide');
		$('.slide_tungnhich4').slick('slickGoTo', slideno - 1);
	});
});

/************slide magazine reponsive************/	
$(document).ready(function(){
	$('.content_magazine_content .slide_tungnhich2').slick({
	responsive: [
		{
		breakpoint: 1230,
			settings: {
				slidesToShow: 4,
			}
		},
		{
		breakpoint: 992,
			settings: {
				slidesToShow: 3,
			}
		},
		{
		breakpoint: 767,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 567,
			settings: {
				slidesToShow: 2,
			}
		},
		{
		breakpoint: 482,
			settings: {
				slidesToShow: 1,
			}
		}
	],
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 8000, 
	});
});

/************menu collaspe************/
$(document).ready(function(){
	$(".panel-default").click(function() {	
	$(".panel-default").removeClass('active');
	$(this).addClass('active');
	});
});


/************show ten trong menu tren dien thoai************/
$(document).ready(function(){
	
	$("#dLabel").text($('ul.dropdown-menu li.active_p a span').text());

});


  
