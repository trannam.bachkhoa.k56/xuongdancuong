

/************slide images_slider************/
var $gallery = $('.single-item');
var slideCount = null;

$( document ).ready(function() {
    $gallery.slick({
		speed: 250,
		fade: true,
		cssEase: 'linear',
		swipe: true,
		swipeToSlide: true,
		touchThreshold: 10
    });
});

$gallery.on('init', function(event, slick){
	slideCount = slick.slideCount;
	setSlideCount();
	setCurrentSlideNumber(slick.currentSlide);
});

$gallery.on('beforeChange', function(event, slick, currentSlide, nextSlide){
	setCurrentSlideNumber(nextSlide);
});

function setSlideCount() {
	var $el = $('.slide-count-wrap').find('.total');
	$el.text(slideCount);
}

function setCurrentSlideNumber(currentSlide) {
	var $el = $('.slide-count-wrap').find('.current');
	$el.text(currentSlide + 1);
}

/************scroll top************/
$(document).ready(function(){
	$(function(){	 
		$(document).on( 'scroll', function(){
	 
			if ($(window).scrollTop() > 100) {
				$('.scroll-top-wrapper').addClass('show');
			} else {
				$('.scroll-top-wrapper').removeClass('show');
			}
			//nut chia se
			if ($(window).scrollTop() > ($('.content_expand.news').height() - 500)) {
				$('.content_social').addClass('share-pause');
				$('.content_social').removeClass('share-fix');
				$('.content_social').css('top',($('.content_expand.news').height() - 500)+'px');
			}else{
				$('.content_social').addClass('share-fix');
				$('.content_social').removeClass('share-pause');
				$('.content_social').css('top','');
			}
			//nut chia se
			
		});
	 
		$('.scroll-top-wrapper').on('click', scrollToTop);
	});
	 
	function scrollToTop() {
		verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
		element = $('body');
		offset = element.offset();
		offsetTop = offset.top;
		$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
	}
});

/************scroll tooltip detail news************/
/*$(window).scroll(function() {                  

    var currentScroll = $(window).scrollTop(); 
	//console.log(currentScroll);
    if (currentScroll >= ($('.content_expand').height() - $('.content_social').height()) ) {           
		$('.content_social').fadeOut(100);
    } else {                                   
         $('.content_social').css({                      
            position: 'fixed',
            top: '25%',
            z_index: '10'
        }).fadeIn(100);
    }
});
*/
  


