<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Sản phẩm</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
      <base href="http://seine.vn/">
      <meta name="description" content="" >
      <meta name="keywords" content="" >
      <meta name="title" content="" >
      <meta http-equiv="Refresh" content="3600" >
      <meta http-equiv="content-language" content="vi" >
      <meta name="classification" content="" >
      <meta name="language" content="vietnamese,english" >
      <meta name="robots" content="index, follow, all" >
      <meta name="author" content="" >
      <meta name="copyright" content="Copyright (C) 2016" >
      <meta name="revisit-after" content="1 days" >
      <link href="http://vn3c.net/html/seine/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/style.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/slider.css" media="screen" rel="stylesheet" type="text/css" >
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/jquery.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/autoNumeric.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/slider.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/waterwheelCarousel.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/elevateZoom.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/function.js"></script>	
      <script type="text/javascript">var base_url = "http://seine.vn";</script>
      <link rel="icon" href="/favicon.png" type="image/x-icon" />
      <meta name="google-site-verification" content="rbEsAMzBo0PMIFQ7M4P2aSFROO4T_ABZF14iY2AoAVY" />
      <!-- Facebook Pixel Code -->
      <script>
         !function(f,b,e,v,n,t,s)
         {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};
         if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
         n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t,s)}(window, document,'script',
         'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1734585963504361');
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=1734585963504361&ev=PageView&noscript=1"
         /></noscript>
      <!-- End Facebook Pixel Code -->
   </head>
   <body class="body">
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1&appId=614473748567264";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script>    
      <div class="box_header">
         <div class="container">
            <div class="row">
               <div class="col-md-2">
                  <a href="/" class="logo"><img src="/public/files/editor-upload/images/he-thong/logo.png"></a>
               </div>
               <div class="col-md-10">
                  <div class="top">
                     <div class="box_menuTop">
                        <ul>
                           <li ><a href="/huong-dan-mua-hang-thanh-toan-a10" class="start" title="Hướng dẫn mua hàng/Thanh toán" target="_self">Hướng dẫn mua hàng/Thanh toán</a></li>
                        </ul>
                        <span class="hotline">Hotline <span>0976.155.755</span></span>
                     </div>
                     <script language="javascript">
                        function doSubmitSearch(){	
                        	if (document.frmSearch.keywords.value == "") {
                        		document.frmSearch.keywords.focus();
                        		return false;
                        	}
                        	document.frmSearch.submit();
                        	return;
                        }
                     </script>
                     <div class="box_timKiem">
                        <form action="/tim-kiem.html" method="get" name="frmSearch">
                           <div class="input-search">
                              <input type="text" class="form-control" name="keywords" placeholder="Tìm kiếm">
                           </div>
                           <button type="button" title="Tìm kiếm" class="form-button" onclick="doSubmitSearch();"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                     </div>
                     <div class="box_cart">
                        <a href="javascript:void(0)">
                        <i class="fa fa-cart-arrow-down"></i> Giỏ hàng <span>(0)</span>
                        </a>
                     </div>
                     <div class="clr"></div>
                  </div>
                  <div class="block_introMenu">
                     <ul>
                        <li ><a href="/tu-van-thiet-ke-dong-phuc-cong-so-cao-cap-a32" class="start" title="Tư vấn thiết kế Đồng phục Công sở cao cấp" target="_self">Tư vấn thiết kế Đồng phục Công sở cao cấp</a></li>
                     </ul>
                  </div>
                  <div class="menu">
                     <div class="facebook">
                        <a href="https://www.facebook.com/seine.vn/?fref=ts"><img src="/public/templates/public/default/images/facebook.png"></a>
                     </div>
                     <div class="box_menuMain hidden-xs hidden-sm" id="box_menuMain">
                        <ul>
                           <li><a href="http://seine.vn" class="start" title="Trang chủ" target="_self">Trang chủ</a></li>
                           <li ><a href="/gioi-thieu-a21"  title="Giới thiệu" target="_self">Giới thiệu</a></li>
                           <li ><a href="/tin-tuc-a2"  title="Tin tức" target="_self">Tin tức</a></li>
                           <li>
                              <a href="/san-pham.html"  title="Sản phẩm" target="_self">Sản phẩm</a>
                              <ul>
                                 <li><a href="/dam-s1" class="start" title="Đầm">Đầm</a></li>
                                 <li><a href="/chan-vay-s2"  title="Chân váy">Chân váy</a></li>
                                 <li><a href="/ao-so-mi-s3"  title="Áo sơ mi">Áo sơ mi</a></li>
                                 <li><a href="/quan-s4"  title="Quần">Quần</a></li>
                                 <li><a href="/vest-s7"  title="Vest">Vest</a></li>
                                 <li><a href="/phu-kien-s8"  title="Phụ kiện">Phụ kiện</a></li>
                                 <li><a href="/mang-to-s10"  title="Măng tô">Măng tô</a></li>
                                 <li><a href="/ao-khoac-s11"  title="Áo khoác ">Áo khoác </a></li>
                              </ul>
                           </li>
                           <li><a href="/bo-suu-tap-g1.html"  title="Bộ sưu tập" target="_self">Bộ sưu tập</a></li>
                           <li ><a href="/video.html"  title="Video" target="_self">Video</a></li>
                           <li ><a href="/showroom-a5"  title="Showroom" target="_self">Showroom</a></li>
                           <li><a href="/lien-he.html"  title="Liên hệ" target="_self">Liên hệ</a></li>
                           <li >
                              <a href="/tuyen-dung-a7"  title="Tuyển dụng" target="_self">Tuyển dụng</a>
                              <ul>
                                 <li><a href="/tuyen-nhan-vien-a26"  title="Tuyển nhân viên">Tuyển nhân viên</a></li>
                                 <li><a href="/tuyen-dai-ly-a27"  title="Tuyển Đại lý ">Tuyển Đại lý </a></li>
                                 <li><a href="/tuyen-thiet-ke-a28"  title="Tuyển thiết kế">Tuyển thiết kế</a></li>
                              </ul>
                           </li>
                        </ul>
                        <script type="text/javascript">
                           menuNgang('#box_menuMain');
                        </script>
                     </div>
                     <div class="box_menuMobile hidden-md hidden-lg" id="box_menuMobile">
                        <ul>
                           <li><a href="http://seine.vn" class="start" title="Trang chủ" target="_self">Trang chủ</a></li>
                           <li ><a href="/gioi-thieu-a21"  title="Giới thiệu" target="_self">Giới thiệu</a></li>
                           <li ><a href="/tin-tuc-a2"  title="Tin tức" target="_self">Tin tức</a></li>
                           <li>
                              <a href="/san-pham.html"  title="Sản phẩm" target="_self">Sản phẩm</a>
                              <ul>
                                 <li><a href="/dam-s1" class="start" title="Đầm">Đầm</a></li>
                                 <li><a href="/chan-vay-s2"  title="Chân váy">Chân váy</a></li>
                                 <li><a href="/ao-so-mi-s3"  title="Áo sơ mi">Áo sơ mi</a></li>
                                 <li><a href="/quan-s4"  title="Quần">Quần</a></li>
                                 <li><a href="/vest-s7"  title="Vest">Vest</a></li>
                                 <li><a href="/phu-kien-s8"  title="Phụ kiện">Phụ kiện</a></li>
                                 <li><a href="/mang-to-s10"  title="Măng tô">Măng tô</a></li>
                                 <li><a href="/ao-khoac-s11"  title="Áo khoác ">Áo khoác </a></li>
                              </ul>
                           </li>
                           <li><a href="/bo-suu-tap-g1.html"  title="Bộ sưu tập" target="_self">Bộ sưu tập</a></li>
                           <li ><a href="/video.html"  title="Video" target="_self">Video</a></li>
                           <li ><a href="/showroom-a5"  title="Showroom" target="_self">Showroom</a></li>
                           <li><a href="/lien-he.html"  title="Liên hệ" target="_self">Liên hệ</a></li>
                           <li >
                              <a href="/tuyen-dung-a7"  title="Tuyển dụng" target="_self">Tuyển dụng</a>
                              <ul>
                                 <li><a href="/tuyen-nhan-vien-a26"  title="Tuyển nhân viên">Tuyển nhân viên</a></li>
                                 <li><a href="/tuyen-dai-ly-a27"  title="Tuyển Đại lý ">Tuyển Đại lý </a></li>
                                 <li><a href="/tuyen-thiet-ke-a28"  title="Tuyển thiết kế">Tuyển thiết kế</a></li>
                              </ul>
                           </li>
                        </ul>
                        <div class="menu-bg"></div>
                     </div>
                     <script type="text/javascript">
                        $('#box_menuMobile ul > li').each(function(index) {
                        	if($('ul', this).length > 0) {
                           		$(this).addClass('submenu');
                           		$('a:first', this).attr('href', 'javascript:;');
                        	}
                        });
                        
                        
                        $('#box_menuMobile .submenu').click(function() {
                        	$('ul', this).toggleClass('show');
                        });
                        
                        $('#box_menuMobile .menu-bg').click(function() {
                        	$('#box_menuMobile').hide();
                        });
                     </script>                    
                     <div class="clr"></div>
                  </div>
               </div>
            </div>
         </div>
         <div class="btn-toggle-menu hidden-md hidden-lg">
            <!-- <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span> -->
            MENU
         </div>
         <script type="text/javascript">
            $('.btn-toggle-menu').click(function() {
            	$('#box_menuMobile').show();
            });
         </script>
      </div>
      <div class="container">
         <div class="wrapper">
            <div class="row">
               <div class="col-sm-10 col-sm-push-2">
                  <div class="module_shop">
                     <div class="module_title">
                        <h1 class="title"><a href="/-s0" title="Sản phẩm">Sản phẩm</a></h1>
                     </div>
                     <div class="module_content sanPham">
                        <div class="row">
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/quan-sqd2032xa-y-s4i307.html');" title="Quần SQD2032XA-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I0225.jpg" alt="Quần SQD2032XA-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/quan-sqd2032xa-y-s4i307.html');" title="Quần SQD2032XA-Y">Quần SQD2032XA-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">559000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/307" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/quan-sqd2032xa-y-s4i307.html" title="Quần SQD2032XA-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-sdk2196do-y-s1i306.html');" title="Đầm SDK2196DO-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Tran-To-Nhu/HUG-0682.jpg" alt="Đầm SDK2196DO-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2196do-y-s1i306.html');" title="Đầm SDK2196DO-Y">Đầm SDK2196DO-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1269000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/306" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-sdk2196do-y-s1i306.html" title="Đầm SDK2196DO-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-sdk2242hoa-y-s1i305.html');" title="Đầm SDK2242HOA-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Tran-To-Nhu/HUG-0152.jpg" alt="Đầm SDK2242HOA-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2242hoa-y-s1i305.html');" title="Đầm SDK2242HOA-Y">Đầm SDK2242HOA-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1229000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/305" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-sdk2242hoa-y-s1i305.html" title="Đầm SDK2242HOA-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-sdk2281be-y-s1i304.html');" title="Đầm SDK2281BE-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9438.jpg" alt="Đầm SDK2281BE-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2281be-y-s1i304.html');" title="Đầm SDK2281BE-Y">Đầm SDK2281BE-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1299000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/304" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-sdk2281be-y-s1i304.html" title="Đầm SDK2281BE-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/sjs2088de-y-s2i303.html');" title="SJS2088DE-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I0074.jpg" alt="SJS2088DE-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/sjs2088de-y-s2i303.html');" title="SJS2088DE-Y">SJS2088DE-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">689000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/303" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/sjs2088de-y-s2i303.html" title="SJS2088DE-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/mang-to-smt2042be-s10i302.html');" title="Măng tô SMT2042BE">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I0010.jpg" alt="Măng tô SMT2042BE"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2042be-s10i302.html');" title="Măng tô SMT2042BE">Măng tô SMT2042BE</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">2769000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/302" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/mang-to-smt2042be-s10i302.html" title="Măng tô SMT2042BE" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/ao-khoac-sak2014tr-y-s11i301.html');" title="Áo khoác SAK2014TR-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9603.jpg" alt="Áo khoác SAK2014TR-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/ao-khoac-sak2014tr-y-s11i301.html');" title="Áo khoác SAK2014TR-Y">Áo khoác SAK2014TR-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/301" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/ao-khoac-sak2014tr-y-s11i301.html" title="Áo khoác SAK2014TR-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/sjs2085xa-y-s2i300.html');" title="SJS2085XA-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I0192.jpg" alt="SJS2085XA-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/sjs2085xa-y-s2i300.html');" title="SJS2085XA-Y">SJS2085XA-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">659000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/300" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/sjs2085xa-y-s2i300.html" title="SJS2085XA-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/sjs2089gi-y-s2i299.html');" title="SJS2089GI-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9550.jpg" alt="SJS2089GI-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/sjs2089gi-y-s2i299.html');" title="SJS2089GI-Y">SJS2089GI-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">650000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/299" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/sjs2089gi-y-s2i299.html" title="SJS2089GI-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-mdk2217ti-s1i298.html');" title="Đầm MDK2217TI">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Tran-To-Nhu/HUG-0783.jpg" alt="Đầm MDK2217TI"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-mdk2217ti-s1i298.html');" title="Đầm MDK2217TI">Đầm MDK2217TI</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/298" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-mdk2217ti-s1i298.html" title="Đầm MDK2217TI" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-sdk2294tit-y-s1i297.html');" title="Đầm SDK2294TIT-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I0245.jpg" alt="Đầm SDK2294TIT-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2294tit-y-s1i297.html');" title="Đầm SDK2294TIT-Y">Đầm SDK2294TIT-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1289000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/297" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-sdk2294tit-y-s1i297.html" title="Đầm SDK2294TIT-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/ao-khoac-sak2040na-y-s11i296.html');" title="Áo khoác SAK2040NA-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9913.jpg" alt="Áo khoác SAK2040NA-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/ao-khoac-sak2040na-y-s11i296.html');" title="Áo khoác SAK2040NA-Y">Áo khoác SAK2040NA-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/296" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/ao-khoac-sak2040na-y-s11i296.html" title="Áo khoác SAK2040NA-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-sdk2273ho-s1i295.html');" title="Đầm SDK2273HO">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9471.jpg" alt="Đầm SDK2273HO"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2273ho-s1i295.html');" title="Đầm SDK2273HO">Đầm SDK2273HO</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1789000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/295" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-sdk2273ho-s1i295.html" title="Đầm SDK2273HO" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/mang-to-smt2023va-y-s10i294.html');" title="Măng tô SMT2023VA-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9831.jpg" alt="Măng tô SMT2023VA-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2023va-y-s10i294.html');" title="Măng tô SMT2023VA-Y">Măng tô SMT2023VA-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1869000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/294" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/mang-to-smt2023va-y-s10i294.html" title="Măng tô SMT2023VA-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/mang-to-smt2039-ydo-s10i293.html');" title="Măng tô SMT2039-YDO">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9754.jpg" alt="Măng tô SMT2039-YDO"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2039-ydo-s10i293.html');" title="Măng tô SMT2039-YDO">Măng tô SMT2039-YDO</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/293" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/mang-to-smt2039-ydo-s10i293.html" title="Măng tô SMT2039-YDO" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/ao-khoac-sak2037ti-y-s11i292.html');" title="Áo khoác SAK2037TI-Y">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9967.jpg" alt="Áo khoác SAK2037TI-Y"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/ao-khoac-sak2037ti-y-s11i292.html');" title="Áo khoác SAK2037TI-Y">Áo khoác SAK2037TI-Y</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1869000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/292" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/ao-khoac-sak2037ti-y-s11i292.html" title="Áo khoác SAK2037TI-Y" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-sdk2279ke-s1i291.html');" title="Đầm SDK2279KE">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9274.jpg" alt="Đầm SDK2279KE"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2279ke-s1i291.html');" title="Đầm SDK2279KE">Đầm SDK2279KE</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1889000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/291" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-sdk2279ke-s1i291.html" title="Đầm SDK2279KE" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/dam-sdk2209ke-s1i290.html');" title="Đầm SDK2209KE">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9362.jpg" alt="Đầm SDK2209KE"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2209ke-s1i290.html');" title="Đầm SDK2209KE">Đầm SDK2209KE</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">1890000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/290" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/dam-sdk2209ke-s1i290.html" title="Đầm SDK2209KE" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/mang-to-smt2038xa-s10i289.html');" title="Măng tô SMT2038XA">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I0121.jpg" alt="Măng tô SMT2038XA"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2038xa-s10i289.html');" title="Măng tô SMT2038XA">Măng tô SMT2038XA</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">2699000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/289" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/mang-to-smt2038xa-s10i289.html" title="Măng tô SMT2038XA" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                           <div class="col-xs-6 col-sm-3 item">
                              <div class="image">
                                 <a href="javascript:;" onclick="showProduct('/mang-to-smt2018va-s10i288.html');" title="Măng tô SMT2018VA">
                                 <img class="img" src="/public/files/editor-upload/_thumbs/images/san-pham/Mua-dong-28-12/AJ2I9447.jpg" alt="Măng tô SMT2018VA"/>						</a>
                              </div>
                              <h3 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2018va-s10i288.html');" title="Măng tô SMT2018VA">Măng tô SMT2018VA</a></h3>
                              <div class="price"><span class="value"><span class="auto_numberic">2799000</span> đ</span></div>
                              <div class="controls">
                                 <a href="/dat-mua/288" class="addCart" rel="nofollow"><i class="fa fa-shopping-cart"></i> Đặt hàng</a>
                                 <a href="/mang-to-smt2018va-s10i288.html" title="Măng tô SMT2018VA" class="detail"><i class="fa fa-search-plus"></i> Chi tiết</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="block_paging page2">
                        <div>
                           <span class="active">1</span><span class="link"><a href="san-pham.html?page=2" title="Page 2">2</a></span><span class="link"><a href="san-pham.html?page=3" title="Page 3">3</a></span><span class="link"><a href="san-pham.html?page=4" title="Page 4">4</a></span><span class="link"><a href="san-pham.html?page=5" title="Page 5">5</a></span><span class="link"><a href="san-pham.html?page=6" title="Page 6">6</a></span><span class="link"><a href="san-pham.html?page=7" title="Page 7">7</a></span><span class="link"><a href="san-pham.html?page=8" title="Page 8">8</a></span><span class="link"><a href="san-pham.html?page=9" title="Page 9">9</a></span><span class="link"><a href="san-pham.html?page=10" title="Page 10">10</a></span>				<span class="next"><a href="san-pham.html?page=2" title="Next">Tiếp</a></span>
                           <span class="end"><a href="san-pham.html?page=15" title="End">Cuối</a></span>
                           <div class="clr"></div>
                        </div>
                        <div class="clr"></div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-2 col-sm-pull-10">
                  <div id="scroll_left">
                     <div class="box_menuLeft" id="block_menuLeft">
                        <div class="box_title">
                           <div class="title">Danh mục sản phẩm</div>
                        </div>
                        <div class="box_content">
                           <ul>
                              <li><a href="/dam-s1" class="start item-1" title="Đầm">Đầm</a></li>
                              <li><a href="/chan-vay-s2" class="item-2" title="Chân váy">Chân váy</a></li>
                              <li><a href="/ao-so-mi-s3" class="item-3" title="Áo sơ mi">Áo sơ mi</a></li>
                              <li><a href="/quan-s4" class="item-4" title="Quần">Quần</a></li>
                              <li><a href="/vest-s7" class="item-7" title="Vest">Vest</a></li>
                              <li><a href="/phu-kien-s8" class="item-8" title="Phụ kiện">Phụ kiện</a></li>
                              <li><a href="/mang-to-s10" class="item-10" title="Măng tô">Măng tô</a></li>
                              <li><a href="/ao-khoac-s11" class="item-11" title="Áo khoác ">Áo khoác </a></li>
                           </ul>
                           <script type="text/javascript">
                              menuDoc('#block_menuLeft', 1);
                           </script>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $('#block_menuLeft .item-').addClass('active');
                     </script>	    			
                  </div>
                  <script type="text/javascript">
                     $(window).load(function () {
                         var offset = $('#scroll_left').offset();
                         var width = $('#scroll_left').outerWidth();
                     
                         $(window).scroll(function () {
                             var topPage = $(window).scrollTop() + 10;
                     
                             if (topPage >= offset.top) {
                             	$('#scroll_left').css({'position': 'fixed', 'top': '10px', 'left': offset.left + 'px', 'width': width + 'px', 'margin': '0px'});
                             	$('#scroll_left .box_menuLeft').css({'margin': '0px'});
                             } else {
                             	$('#scroll_left').removeAttr('style');
                             	$('#scroll_left .box_menuLeft').removeAttr('style');
                             }
                         });
                     });
                  </script>
               </div>
            </div>
         </div>
      </div>
      <div class="box_footer">
         <div class="container">
            <div class="row">
               <div class="col-sm-5">
                  <div style="text-transform: uppercase;">
                     Copyright © 2017, Seine Fashion. All Rights Reserved.
                  </div>
               </div>
               <div class="col-sm-2" style="text-align: right;">
                  Hotline: <span style="color:#fff0f5;"><strong><span style="font-size:16px;"><a href="tel:0976155755">0976.155.755</a></span></strong></span>
               </div>
               <div class="col-sm-2" style="text-align: right;">
                  &nbsp;
               </div>
               <div class="col-sm-5" style="text-align: right;">
                  <a href="https://www.instagram.com/seinefashion/"><img src="/public/files/editor-upload/images/he-thong/instagram.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">seinephap</span></a>&nbsp; &nbsp;&nbsp; &nbsp; <a href="javascript:;"><img src="/public/files/editor-upload/images/he-thong/zalo.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">0976.155.755</span></a>&nbsp; &nbsp;&nbsp; &nbsp;<a href="skype:abc"><img src="/public/files/editor-upload/images/he-thong/skype.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">Skype us now</span></a>
               </div>
            </div>
            <div>
               &nbsp;
            </div>
            <div>
               &nbsp;
            </div>
         </div>
      </div>
   </body>
</html>