
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Tinh tế, tỉ mỉ tạo sự khác biệt - nâng tầm thiết kế </title><base href="http://seine.vn/">    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<meta name="description" content="" >
<meta name="keywords" content="" >
<meta name="title" content="" >
<meta http-equiv="Refresh" content="3600" >
<meta http-equiv="content-language" content="vi" >
<meta name="classification" content="" >
<meta name="language" content="vietnamese,english" >
<meta name="robots" content="index, follow, all" >
<meta name="author" content="" >
<meta name="copyright" content="Copyright (C) 2016" >
<meta name="revisit-after" content="1 days" >	




<link href="http://vn3c.net/html/seine/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/style.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/slider.css" media="screen" rel="stylesheet" type="text/css" >
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/jquery.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/autoNumeric.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/slider.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/waterwheelCarousel.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/elevateZoom.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/function.js"></script>	
      <script type="text/javascript">var base_url = "http://seine.vn";</script>
      <link rel="icon" href="/favicon.png" type="image/x-icon" />




	
	<meta name="google-site-verification" content="rbEsAMzBo0PMIFQ7M4P2aSFROO4T_ABZF14iY2AoAVY" />
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '1734585963504361');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=1734585963504361&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head><body class="body">
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1&appId=614473748567264";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>    
<div class="box_header">
	<div class="container">
	    <div class="row">
            <div class="col-md-2">
                <a href="/" class="logo"><img src="/public/files/editor-upload/images/he-thong/logo.png"></a>
            </div>
            <div class="col-md-10">
                <div class="top">
                    <div class="box_menuTop">
	<ul><li ><a href="/huong-dan-mua-hang-thanh-toan-a10" class="start" title="Hướng dẫn mua hàng/Thanh toán" target="_self">Hướng dẫn mua hàng/Thanh toán</a></li></ul>	<span class="hotline">Hotline <span>0976.155.755</span></span>
</div>                    <script language="javascript">
	function doSubmitSearch(){	
		if (document.frmSearch.keywords.value == "") {
			document.frmSearch.keywords.focus();
			return false;
		}
		document.frmSearch.submit();
		return;
	}
</script>
<div class="box_timKiem">
	<form action="/tim-kiem.html" method="get" name="frmSearch">
		<div class="input-search">
			<input type="text" class="form-control" name="keywords" placeholder="Tìm kiếm">
		</div>
		<button type="button" title="Tìm kiếm" class="form-button" onclick="doSubmitSearch();"><i class="fa fa-search" aria-hidden="true"></i></button>
   	</form>
</div>                    <div class="box_cart">
    <a href="/shopping/public/view-cart">
		<i class="fa fa-cart-arrow-down"></i> Giỏ hàng <span>(1)</span>
	</a>
</div>                    <div class="clr"></div>
                </div>
                <div class="block_introMenu">
	<ul><li ><a href="/tu-van-thiet-ke-dong-phuc-cong-so-cao-cap-a32" class="start" title="Tư vấn thiết kế Đồng phục Công sở cao cấp" target="_self">Tư vấn thiết kế Đồng phục Công sở cao cấp</a></li></ul></div>                <div class="menu">
                    <div class="facebook">
                        <a href="https://www.facebook.com/seine.vn/?fref=ts"><img src="/public/templates/public/default/images/facebook.png"></a>
                    </div>
                    <div class="box_menuMain hidden-xs hidden-sm" id="box_menuMain">
	<ul><li><a href="http://seine.vn" class="start" title="Trang chủ" target="_self">Trang chủ</a></li><li ><a href="/gioi-thieu-a21"  title="Giới thiệu" target="_self">Giới thiệu</a></li><li ><a href="/tin-tuc-a2"  title="Tin tức" target="_self">Tin tức</a></li><li><a href="/san-pham.html"  title="Sản phẩm" target="_self">Sản phẩm</a><ul><li><a href="/dam-s1" class="start" title="Đầm">Đầm</a></li><li><a href="/chan-vay-s2"  title="Chân váy">Chân váy</a></li><li><a href="/ao-so-mi-s3"  title="Áo sơ mi">Áo sơ mi</a></li><li><a href="/quan-s4"  title="Quần">Quần</a></li><li><a href="/vest-s7"  title="Vest">Vest</a></li><li><a href="/phu-kien-s8"  title="Phụ kiện">Phụ kiện</a></li><li><a href="/mang-to-s10"  title="Măng tô">Măng tô</a></li><li><a href="/ao-khoac-s11"  title="Áo khoác ">Áo khoác </a></li></ul></li><li><a href="/bo-suu-tap-g1.html"  title="Bộ sưu tập" target="_self">Bộ sưu tập</a></li><li ><a href="/video.html"  title="Video" target="_self">Video</a></li><li ><a href="/showroom-a5"  title="Showroom" target="_self">Showroom</a></li><li><a href="/lien-he.html"  title="Liên hệ" target="_self">Liên hệ</a></li><li ><a href="/tuyen-dung-a7"  title="Tuyển dụng" target="_self">Tuyển dụng</a><ul><li><a href="/tuyen-nhan-vien-a26"  title="Tuyển nhân viên">Tuyển nhân viên</a></li><li><a href="/tuyen-dai-ly-a27"  title="Tuyển Đại lý ">Tuyển Đại lý </a></li><li><a href="/tuyen-thiet-ke-a28"  title="Tuyển thiết kế">Tuyển thiết kế</a></li></ul></li></ul>	<script type="text/javascript">
		menuNgang('#box_menuMain');
	</script>
</div>
<div class="box_menuMobile hidden-md hidden-lg" id="box_menuMobile">
	<ul><li><a href="http://seine.vn" class="start" title="Trang chủ" target="_self">Trang chủ</a></li><li ><a href="/gioi-thieu-a21"  title="Giới thiệu" target="_self">Giới thiệu</a></li><li ><a href="/tin-tuc-a2"  title="Tin tức" target="_self">Tin tức</a></li><li><a href="/san-pham.html"  title="Sản phẩm" target="_self">Sản phẩm</a><ul><li><a href="/dam-s1" class="start" title="Đầm">Đầm</a></li><li><a href="/chan-vay-s2"  title="Chân váy">Chân váy</a></li><li><a href="/ao-so-mi-s3"  title="Áo sơ mi">Áo sơ mi</a></li><li><a href="/quan-s4"  title="Quần">Quần</a></li><li><a href="/vest-s7"  title="Vest">Vest</a></li><li><a href="/phu-kien-s8"  title="Phụ kiện">Phụ kiện</a></li><li><a href="/mang-to-s10"  title="Măng tô">Măng tô</a></li><li><a href="/ao-khoac-s11"  title="Áo khoác ">Áo khoác </a></li></ul></li><li><a href="/bo-suu-tap-g1.html"  title="Bộ sưu tập" target="_self">Bộ sưu tập</a></li><li ><a href="/video.html"  title="Video" target="_self">Video</a></li><li ><a href="/showroom-a5"  title="Showroom" target="_self">Showroom</a></li><li><a href="/lien-he.html"  title="Liên hệ" target="_self">Liên hệ</a></li><li ><a href="/tuyen-dung-a7"  title="Tuyển dụng" target="_self">Tuyển dụng</a><ul><li><a href="/tuyen-nhan-vien-a26"  title="Tuyển nhân viên">Tuyển nhân viên</a></li><li><a href="/tuyen-dai-ly-a27"  title="Tuyển Đại lý ">Tuyển Đại lý </a></li><li><a href="/tuyen-thiet-ke-a28"  title="Tuyển thiết kế">Tuyển thiết kế</a></li></ul></li></ul>	<div class="menu-bg"></div>
</div>
<script type="text/javascript">

	$('#box_menuMobile ul > li').each(function(index) {
		if($('ul', this).length > 0) {
    		$(this).addClass('submenu');
    		$('a:first', this).attr('href', 'javascript:;');
		}
	});


	$('#box_menuMobile .submenu').click(function() {
		$('ul', this).toggleClass('show');
	});
	
	$('#box_menuMobile .menu-bg').click(function() {
		$('#box_menuMobile').hide();
	});
</script>                    <div class="clr"></div>
                </div>
            </div>
	    </div>
    </div>
    
	<div class="btn-toggle-menu hidden-md hidden-lg">
	   	<!-- <span class="icon-bar"></span>
	   	<span class="icon-bar"></span>
	   	<span class="icon-bar"></span> -->
	   	MENU
    </div>

	<script type="text/javascript">
		$('.btn-toggle-menu').click(function() {
			$('#box_menuMobile').show();
		});
	</script>
</div>

    	<div class="container">
        <div class="wrapper">
	    	<div class="row">
	    		<div class="col-sm-9">
	    		    <div class="module_article">
	<div class="module_title">
		<h1 class="title"><a href="/su-tinh-te-tit-mi-tao-ra-su-khac-biet-nang-tam-thiet-ke-a19i93.html" title="Tinh tế, tỉ mỉ tạo sự khác biệt - nâng tầm thiết kế ">Tinh tế, tỉ mỉ tạo sự khác biệt - nâng tầm thiết kế </a></h1>
	</div>
	<div class="module_content module_detail">
		<div class="fb-like" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div><br /><br />		<div>
	<div style="text-align: justify;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong><span style="color:#000000;"><span style="font-size:16px;"><span style="font-family:times new roman,times,serif;">Những bộ cánh mùa thu mới nhất của Seine là sự kết tinh hoàn hảo đến từ chất liệu cao cấp, from dáng hiện đại, họa tiết sang trọng và kỹ thuật cắt phối đầy điêu luyện.</span></span></span></strong></div>
	<div style="text-align: justify;">
		&nbsp;</div>
	<div style="text-align: justify;">
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="color:#000000;"><span style="font-size:16px;"><span style="font-family:times new roman,times,serif;">Mỗi thiết kế tuy đơn giản nhưng lại cực kỳ tinh xảo đến từng đường kim mũi chỉ, hoàn hảo từng đường cắt cup, pha phối màu sắc và chất liệu. Các họa tiết thêu hoa hay đính đá đều được những bàn tay vàng của Seine đính kết thủ công, cầu kỳ và tỷ mỉ, tạo nên những điểm nhấn khác biệt, không thể trộn lẫn.</span></span></span></div>
	<div style="text-align: justify;">
		&nbsp;</div>
	<div style="text-align: center;">
		<span style="color:#000000;"><span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/IMG-0837.jpg" style="width: 600px; height: 800px;" /></span></span></span></div>
	<div style="text-align: center;">
		<em><span style="color:#000000;"><span style="font-size:16px;"><span style="font-family:times new roman,times,serif;">Từng hạt, từng hạt cườm, đá được đính thủ công lên từng bông hoa&nbsp;</span></span></span></em></div>
	<div style="text-align: center;">
		&nbsp;</div>
	<div style="text-align: center;">
		<span style="color:#000000;"><span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/IMG-0171.JPG" style="width: 600px; height: 800px;" /></span></span></span></div>
	<div style="text-align: center;">
		<em><font color="#000000" face="times new roman, times, serif"><span style="font-size: 16px;">Sự tỉ mỉ đến từng đường nét</span></font></em></div>
	<div style="text-align: justify;">
		&nbsp;</div>
	<div style="text-align: center;">
		<span style="color:#000000;"><span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/IMG-0168.JPG" style="width: 600px; height: 800px;" /></span></span></span></div>
	<div style="text-align: center;">
		&nbsp;</div>
	<div style="text-align: center;">
		<span style="color:#000000;"><span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/IMG-0839(1).jpg" style="width: 600px; height: 800px;" /></span></span></span></div>
	<div style="text-align: center;">
		&nbsp;</div>
	<div style="text-align: center;">
		&nbsp;</div>
	<div>
		<div>
			<span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><strong>Và sau nh</strong><strong>ữ</strong><strong>ng công đo</strong><strong>ạ</strong><strong>n th</strong><strong>ủ</strong><strong> công t</strong><strong>ỉ</strong><strong> m</strong><strong>ẩ</strong><strong>n nh</strong><strong>ư</strong><strong> th</strong><strong>ế</strong><strong>, nh</strong><strong>ữ</strong><strong>ng s</strong><strong>ả</strong><strong>n ph</strong><strong>ẩ</strong><strong>m đ</strong><strong>ế</strong><strong>n tay khách hàng luôn là s</strong><strong>ự</strong><strong> hoàn h</strong><strong>ả</strong><strong>o nh</strong><strong>ấ</strong><strong>t, t</strong><strong>ự</strong><strong> tin nh</strong><strong>ấ</strong><strong>t:</strong></span></span></div>
		<div>
			&nbsp;</div>
		<div style="text-align: center;">
			<span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><strong><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/1.jpg" style="width: 600px; height: 400px;" /></strong></span></span></div>
		<div style="text-align: center;">
			&nbsp;</div>
		<div style="text-align: center;">
			<span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><strong><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0614.jpg" style="width: 600px; height: 400px;" /></strong></span></span></div>
		<div style="text-align: center;">
			&nbsp;</div>
		<div style="text-align: center;">
			<span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><strong><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0108.jpg" style="width: 600px; height: 900px;" /></strong></span></span></div>
		<div style="text-align: center;">
			&nbsp;</div>
		<div style="text-align: center;">
			<span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><strong><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0128.jpg" style="width: 600px; height: 900px;" /></strong></span></span></div>
		<div style="text-align: center;">
			&nbsp;</div>
		<div style="text-align: center;">
			<span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><strong><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0152.jpg" style="width: 600px; height: 900px;" /></strong></span></span></div>
		<div style="text-align: center;">
			&nbsp;</div>
		<div style="text-align: center;">
			<span style="font-size:16px;"><span style="font-family:times new roman,times,serif;"><strong><img alt="" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0268.jpg" style="width: 600px; height: 900px;" /></strong></span></span></div>
	</div>
</div>
<div>
	&nbsp;</div>
	</div>
</div>

<div class="module_article" id="comment">
	<div class="module_title">
		<div class="title"><a href="javascript:;">Bình luận bài viết</a></div>
	</div>
	<div class="module_content">
		<div class="fb-comments" data-href="" data-width="100%" data-num-posts="10" data-colorscheme="reverse_time"></div>	</div>
</div>

<div class="module_article">
	<div class="module_title">
		<div class="title"><a href="javascript:;">Tin mới hơn</a></div>
	</div>
	<div class="module_content">
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/6-xu-huong-duoc-du-doan-se-lam-mua-lam-gio-nam-2018-a19i102.html" title="6 xu hướng được dự đoán sẽ 'làm mưa gió' năm 2018">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/4.jpg" alt="6 xu hướng được dự đoán sẽ 'làm mưa gió' năm 2018"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/6-xu-huong-duoc-du-doan-se-lam-mua-lam-gio-nam-2018-a19i102.html" title="6 xu hướng được dự đoán sẽ 'làm mưa gió' năm 2018">6 xu hướng được dự đoán sẽ 'làm mưa gió' năm 2018</a>
    				</h3>
    				<div class="excerpt">
    				        				    <div class="read"><a href="/6-xu-huong-duoc-du-doan-se-lam-mua-lam-gio-nam-2018-a19i102.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/3-kieu-mu-nen-co-de-khong-bi-lac-trong-mua-dong-xuan-2018-a19i100.html" title="3 kiểu mũ NÊN có để không bị "lạc" trong mùa đông xuân 2018">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/Xu-huong/11111111.jpg" alt="3 kiểu mũ NÊN có để không bị "lạc" trong mùa đông xuân 2018"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/3-kieu-mu-nen-co-de-khong-bi-lac-trong-mua-dong-xuan-2018-a19i100.html" title="3 kiểu mũ NÊN có để không bị "lạc" trong mùa đông xuân 2018">3 kiểu mũ NÊN có để không bị "lạc" trong mùa đông xuân 2018</a>
    				</h3>
    				<div class="excerpt">
    				        				    <div class="read"><a href="/3-kieu-mu-nen-co-de-khong-bi-lac-trong-mua-dong-xuan-2018-a19i100.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/11-cach-phoi-mau-sac-cuc-chuan-cho-phai-dep-a19i99.html" title="11 cách phối màu sắc cực chuẩn cho phái đẹp">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/Xu-huong/6-89762.jpg" alt="11 cách phối màu sắc cực chuẩn cho phái đẹp"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/11-cach-phoi-mau-sac-cuc-chuan-cho-phai-dep-a19i99.html" title="11 cách phối màu sắc cực chuẩn cho phái đẹp">11 cách phối màu sắc cực chuẩn cho phái đẹp</a>
    				</h3>
    				<div class="excerpt">
    				        				    <div class="read"><a href="/11-cach-phoi-mau-sac-cuc-chuan-cho-phai-dep-a19i99.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/du-kieu-mix-chan-vay-ngan-ngay-dong-lanh-tu-sao-hollywood-a19i97.html" title="Đủ kiểu mix chân váy ngắn ngày đông lạnh từ sao Hollywood">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/Xu-huong/gigi2-6026-1513234951.jpg" alt="Đủ kiểu mix chân váy ngắn ngày đông lạnh từ sao Hollywood"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/du-kieu-mix-chan-vay-ngan-ngay-dong-lanh-tu-sao-hollywood-a19i97.html" title="Đủ kiểu mix chân váy ngắn ngày đông lạnh từ sao Hollywood">Đủ kiểu mix chân váy ngắn ngày đông lạnh từ sao Hollywood</a>
    				</h3>
    				<div class="excerpt">
    				    Quần tất đen và bốt cao cổ là những phụ kiện không thể thiếu để diện mini skirt trong ngày đông.    				    <div class="read"><a href="/du-kieu-mix-chan-vay-ngan-ngay-dong-lanh-tu-sao-hollywood-a19i97.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/goi-y-dien-ao-choang-tu-style-cua-cong-nuong-kate-a19i96.html" title=""Chất lừ" với style áo khoác mùa đông của công nương Kate">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/Xu-huong/Kate6.jpg" alt=""Chất lừ" với style áo khoác mùa đông của công nương Kate"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/goi-y-dien-ao-choang-tu-style-cua-cong-nuong-kate-a19i96.html" title=""Chất lừ" với style áo khoác mùa đông của công nương Kate">"Chất lừ" với style áo khoác mùa đông của công nương Kate</a>
    				</h3>
    				<div class="excerpt">
    				    Kiểu áo dạ dáng xòe được Công nương nước Anh ưa chuộng, cô thường mix cùng bốt cao quá gối hoặc giày cao gót.     				    <div class="read"><a href="/goi-y-dien-ao-choang-tu-style-cua-cong-nuong-kate-a19i96.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
			</div>
</div>

<div class="module_article">
	<div class="module_title clearfix">
		<div class="title"><a href="javascript:;">Tin cũ hơn</a></div>
	</div>
	<div class="module_content">
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/hoa-tiet-ke-xu-huong-ben-vung-cua-lang-thoi-trang-a19i86.html" title="Hoạ tiết kẻ - xu hướng bền vững của làng thời trang">
    					<img src="/public/files/editor-upload/_thumbs/images/san-pham/Lookbook-22-6/-MG-7386.jpg" alt="Hoạ tiết kẻ - xu hướng bền vững của làng thời trang"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/hoa-tiet-ke-xu-huong-ben-vung-cua-lang-thoi-trang-a19i86.html" title="Hoạ tiết kẻ - xu hướng bền vững của làng thời trang">Hoạ tiết kẻ - xu hướng bền vững của làng thời trang</a>
    				</h3>
    				<div class="excerpt">
    				    Hoạ tiết kẻ ngang, kẻ sọc không bao giờ lỗi mốt trong thế giới thời trang. Năm 2017 được dự đoán xu hướng này sẽ lên ngôi trở lại vô cùng mạnh mẽ, đặc biệt với mùa hè, chúng mang đến cho người mặc sự năng động và cá tính.    				    <div class="read"><a href="/hoa-tiet-ke-xu-huong-ben-vung-cua-lang-thoi-trang-a19i86.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/sao-ngoai-goi-y-mix-do-voi-chan-vay-tua-rua-mua-he-a19i74.html" title="SEINE Pháp ứng dụng xu hướng ren rua thế giới với SP xuân hè 2017">
    					<img src="/public/files/editor-upload/_thumbs/images/san-pham/8U9A4201.jpg" alt="SEINE Pháp ứng dụng xu hướng ren rua thế giới với SP xuân hè 2017"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/sao-ngoai-goi-y-mix-do-voi-chan-vay-tua-rua-mua-he-a19i74.html" title="SEINE Pháp ứng dụng xu hướng ren rua thế giới với SP xuân hè 2017">SEINE Pháp ứng dụng xu hướng ren rua thế giới với SP xuân hè 2017</a>
    				</h3>
    				<div class="excerpt">
    				    Những chiếc chân váy tua rua xinh xắn mang đậm phong cách phóng khoáng, lãng mạn được nhiều tín đồ thời trang, đặc biệt là các mỹ nhân Hollywood yêu thích vào mùa nắng. Thương hiệu SEINE Pháp nhanh chóng cập nhật xu hướng ren rua thế giới vào thiết kế xuân hè 2017.    				    <div class="read"><a href="/sao-ngoai-goi-y-mix-do-voi-chan-vay-tua-rua-mua-he-a19i74.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/voi-thoi-trang-mau-sac-noi-len-dieu-gi-a19i73.html" title="Với thời trang màu sắc nói lên điều gì?">
    					<img src="/public/files/editor-upload/_thumbs/images/san-pham/Lookbook-9-3/SDK054NA-2360.jpg" alt="Với thời trang màu sắc nói lên điều gì?"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/voi-thoi-trang-mau-sac-noi-len-dieu-gi-a19i73.html" title="Với thời trang màu sắc nói lên điều gì?">Với thời trang màu sắc nói lên điều gì?</a>
    				</h3>
    				<div class="excerpt">
    				    Màu đỏ nóng bỏng, màu hồng phớt đáng yêu hay màu xanh pastel dịu dàng… đều là những gam màu "thần thánh" của làng thời trang.    				    <div class="read"><a href="/voi-thoi-trang-mau-sac-noi-len-dieu-gi-a19i73.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/15-su-that-thoi-trang-chi-co-tin-do-mau-den-moi-hieu-a19i70.html" title="15 sự thật thời trang chỉ có "tín đồ màu đen" mới hiểu">
    					<img src="/public/files/editor-upload/_thumbs/images/san-pham/Lookbook-9-3/SDK049DE-1890.jpg" alt="15 sự thật thời trang chỉ có "tín đồ màu đen" mới hiểu"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/15-su-that-thoi-trang-chi-co-tin-do-mau-den-moi-hieu-a19i70.html" title="15 sự thật thời trang chỉ có "tín đồ màu đen" mới hiểu">15 sự thật thời trang chỉ có "tín đồ màu đen" mới hiểu</a>
    				</h3>
    				<div class="excerpt">
    				    Mặc đồ đen sang chảnh là vậy, nhưng cũng có những câu chuyện "cười ra nước mắt" đằng sau. Chỉ những tìn đồ chính hiệu của đồ đen mới hiểu!    				    <div class="read"><a href="/15-su-that-thoi-trang-chi-co-tin-do-mau-den-moi-hieu-a19i70.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/so-mi-hoa-cho-xuan-he-2017-a19i64.html" title="Sơ mi hoa cho Xuân Hè 2017">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/Xu-huong/Untitled-2.jpg" alt="Sơ mi hoa cho Xuân Hè 2017"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/so-mi-hoa-cho-xuan-he-2017-a19i64.html" title="Sơ mi hoa cho Xuân Hè 2017">Sơ mi hoa cho Xuân Hè 2017</a>
    				</h3>
    				<div class="excerpt">
    				    Không giống với bất kỳ những thiết kế in hoa của các mùa thời trang khác, Xuân/Hè 2017 các nàng dễ dàng thấy sự chuyển mình ngọt ngào, duyên dáng đến mê lòng của các mẫu sơmi/ blouse.     				    <div class="read"><a href="/so-mi-hoa-cho-xuan-he-2017-a19i64.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/xanh-la-ma-tro-thanh-mau-chu-dao-cua-nam-2017-a19i58.html" title="Xanh lá mạ trở thành màu chủ đạo của năm 2017!">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/5.jpg" alt="Xanh lá mạ trở thành màu chủ đạo của năm 2017!"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/xanh-la-ma-tro-thanh-mau-chu-dao-cua-nam-2017-a19i58.html" title="Xanh lá mạ trở thành màu chủ đạo của năm 2017!">Xanh lá mạ trở thành màu chủ đạo của năm 2017!</a>
    				</h3>
    				<div class="excerpt">
    				    Chỉ trong vòng vài tháng nữa bạn sẽ thấy màu xanh lá mạ phủ sóng trên khắp các sàn diễn thời trang cũng như đường phố.     				    <div class="read"><a href="/xanh-la-ma-tro-thanh-mau-chu-dao-cua-nam-2017-a19i58.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/xu-huong-thoi-trang-cong-so-xuan-he-nam-2017-a19i57.html" title="Xu hướng thời trang công sở xuân hè năm 2017">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/7.png" alt="Xu hướng thời trang công sở xuân hè năm 2017"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/xu-huong-thoi-trang-cong-so-xuan-he-nam-2017-a19i57.html" title="Xu hướng thời trang công sở xuân hè năm 2017">Xu hướng thời trang công sở xuân hè năm 2017</a>
    				</h3>
    				<div class="excerpt">
    				    Thời trang công sở luôn là chủ đề được chị em quan tâm nhiều nhất. Khoác lên mình những bộ cách sành điệu, thời trang, hợp mốt sẽ giúp chị em càng tự tin hơn trong công việc.     				    <div class="read"><a href="/xu-huong-thoi-trang-cong-so-xuan-he-nam-2017-a19i57.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
				<div class="row item">
			<div class="col-md-3">
    			<div class="hinhanh">
    				<a href="/don-dau-xu-huong-thoi-trang-2017-voi-nhung-bo-canh-pha-cach-sieu-an-tuong-a19i56.html" title="Đón đầu xu hướng thời trang 2017 với những 'bộ cánh' phá cách siêu ấn tượng">
    					<img src="/public/files/editor-upload/_thumbs/images/tin-tuc/2.jpg" alt="Đón đầu xu hướng thời trang 2017 với những 'bộ cánh' phá cách siêu ấn tượng"/>    				</a>
    			</div>
			</div>
			<div class="col-md-9">
    			<div class="desc">
    				<h3 class="title">
    					<a href="/don-dau-xu-huong-thoi-trang-2017-voi-nhung-bo-canh-pha-cach-sieu-an-tuong-a19i56.html" title="Đón đầu xu hướng thời trang 2017 với những 'bộ cánh' phá cách siêu ấn tượng">Đón đầu xu hướng thời trang 2017 với những 'bộ cánh' phá cách siêu ấn tượng</a>
    				</h3>
    				<div class="excerpt">
    				    Khi nói tới thời trang công sở, người ta thường nghĩ đến kiểu kết hợp truyền thống như vest, suit, áo sơ mi, quần âu, chân váy hoặc một chiếc đầm liền kín đáo. Nhưng nhu cầu của xã hội thay đổi khiến cho các nhà thiết kế mạnh dạn chinh phục khách hàng bằng những sản phẩm cách tân mềm mại, năng động, nhưng vẫn đảm bảo lịch sự và tính tiện dụng trong nhiều hoàn cảnh. Vì vậy thời trang công sở ngày càng phong phú, hấp dẫn và được yêu thích ở khắp nơi trên thế giới.    				    <div class="read"><a href="/don-dau-xu-huong-thoi-trang-2017-voi-nhung-bo-canh-pha-cach-sieu-an-tuong-a19i56.html">Xem chi tiết</a></div>
				    </div>
    			</div>
			</div>
		</div>
			</div>
</div>
	    		</div>
	    		<div class="col-sm-3">
	    			<div class="box_adsSlide">
    <div class="box_title">Mặc đẹp cùng Seine</div>
        <div class="box_content">
    	<ul id="box_adsSlide">
    		    		<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0340.jpg"/></a></li>
    		    		<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-18-09/IMG-6060.jpg"/></a></li>
    		    		<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/La-Thanh-Huyen/HNN-6642.jpg"/></a></li>
    		    		<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/bo-suu-tap/2017/The-Frets/8U9A3851.jpg"/></a></li>
    		    		<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0230.jpg"/></a></li>
    		    	</ul>
    	<script type="text/javascript">
    		$('#box_adsSlide').bxSlider({
    			//mode: 'fade',
    			controls: true,
    			auto: true,
    			speed: 1000,
    			pause: 5000
    		});
    	</script>
	</div>
</div>	    				    			<div class="box_mxhBox">
	<div class="box_content">
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<div class="fb-like-box" data-href="https://www.facebook.com/seine.vn/?fref=ts" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
	</div>
</div>	    			<div class="box_adsRight">
	<ul id="box_adsRight">
				<li><a href="http://seine.vn/dam-s1" target="_self"><img src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0108.jpg"/></a></li>
				<li><a href="http://seine.vn/ao-so-mi-s3" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-18-09/SDK2188-SVS2017.jpg"/></a></li>
				<li><a href="http://seine.vn/ao-so-mi-s3" target="_self"><img src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0152.jpg"/></a></li>
				<li><a href="http://seine.vn/ao-so-mi-s3" target="_self"><img src="http://seine.vn/san-pham.html"/></a></li>
				<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-23-8/SQD016DE-980.jpg"/></a></li>
				<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-23-8/SAS064HT-820.jpg"/></a></li>
				<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-23-8/SDK133DO-2180.jpg"/></a></li>
				<li><a href="http://seine.vn/dam-s1" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-23-8/Hang-Thu/-MG-6043.jpg"/></a></li>
				<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-18-09/IMG-6373.jpg"/></a></li>
				<li><a href="http://seine.vn/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/san-pham/Lookbook-18-09/SJS2059DE-890-SAS2084DE.jpg"/></a></li>
			</ul>
	<script type="text/javascript">
		$('#box_adsRight').bxSlider({
			mode: 'fade',
			controls: false,
			auto: true,
			speed: 1000,
			pause: 5000
		});
	</script>
</div>	    		</div>
	    	</div>
	    </div>
    </div>
    <div class="box_footer">
	<div class="container">
		<div class="row">
	<div class="col-sm-5">
		<div style="text-transform: uppercase;">
			Copyright © 2017, Seine Fashion. All Rights Reserved.</div>
	</div>
	<div class="col-sm-2" style="text-align: right;">
		Hotline: <span style="color:#fff0f5;"><strong><span style="font-size:16px;"><a href="tel:0976155755">0976.155.755</a></span></strong></span></div>
	<div class="col-sm-2" style="text-align: right;">
		&nbsp;</div>
	<div class="col-sm-5" style="text-align: right;">
		<a href="https://www.instagram.com/seinefashion/"><img src="/public/files/editor-upload/images/he-thong/instagram.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">seinephap</span></a>&nbsp; &nbsp;&nbsp; &nbsp; <a href="javascript:;"><img src="/public/files/editor-upload/images/he-thong/zalo.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">0976.155.755</span></a>&nbsp; &nbsp;&nbsp; &nbsp;<a href="skype:abc"><img src="/public/files/editor-upload/images/he-thong/skype.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">Skype us now</span></a></div>
</div>
<div>
	&nbsp;</div>
<div>
	&nbsp;</div>
	</div>
</div>
    </body>
</html>
