<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Seine Fashion - Đẳng cấp thời trang Pháp</title>
      <base href="http://seine.vn/">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
      <meta name="description" content="" >
      <meta name="keywords" content="" >
      <meta name="title" content="Seine Fashion - Đẳng cấp thời trang Pháp" >
      <meta http-equiv="Refresh" content="3600" >
      <meta http-equiv="content-language" content="vi" >
      <meta name="classification" content="" >
      <meta name="language" content="vietnamese,english" >
      <meta name="robots" content="index, follow, all" >
      <meta name="author" content="" >
      <meta name="copyright" content="Copyright (C) 2016" >
      <meta name="revisit-after" content="1 days" >

      <link href="http://vn3c.net/html/seine/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/style.css" media="screen" rel="stylesheet" type="text/css" >
      <link href="http://vn3c.net/html/seine/css/slider.css" media="screen" rel="stylesheet" type="text/css" >
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/jquery.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/autoNumeric.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/slider.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/waterwheelCarousel.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/elevateZoom.min.js"></script>
      <script type="text/javascript" src="http://vn3c.net/html/seine/js/function.js"></script>	
      <script type="text/javascript">var base_url = "http://seine.vn";</script>


      <link rel="icon" href="/favicon.png" type="image/x-icon" />
      <meta name="google-site-verification" content="rbEsAMzBo0PMIFQ7M4P2aSFROO4T_ABZF14iY2AoAVY" />
      <!-- Facebook Pixel Code -->
      <script>
         !function(f,b,e,v,n,t,s)
         {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};
         if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
         n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t,s)}(window, document,'script',
         'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1734585963504361');
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=1734585963504361&ev=PageView&noscript=1"
         /></noscript>
      <!-- End Facebook Pixel Code -->
   </head>
   <body class="body">
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1&appId=614473748567264";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script>    
      <div class="box_header">
         <div class="container">
            <div class="row">
               <div class="col-md-2">
                  <a href="/" class="logo"><img src="http://vn3c.net/html/seine/upload-img/logo.png"></a>
               </div>
               <div class="col-md-10">
                  <div class="top">
                     <div class="box_menuTop">
                        <ul>
                           <li ><a href="/huong-dan-mua-hang-thanh-toan-a10" class="start" title="Hướng dẫn mua hàng/Thanh toán" target="_self">Hướng dẫn mua hàng/Thanh toán</a></li>
                        </ul>
                        <span class="hotline">Hotline <span>0976.155.755</span></span>
                     </div>
                     <script language="javascript">
                        function doSubmitSearch(){	
                        	if (document.frmSearch.keywords.value == "") {
                        		document.frmSearch.keywords.focus();
                        		return false;
                        	}
                        	document.frmSearch.submit();
                        	return;
                        }
                     </script>
                     <div class="box_timKiem">
                        <form action="/tim-kiem.html" method="get" name="frmSearch">
                           <div class="input-search">
                              <input type="text" class="form-control" name="keywords" placeholder="Tìm kiếm">
                           </div>
                           <button type="button" title="Tìm kiếm" class="form-button" onclick="doSubmitSearch();"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                     </div>
                     <div class="box_cart">
                        <a href="javascript:void(0)">
                        <i class="fa fa-cart-arrow-down"></i> Giỏ hàng <span>(0)</span>
                        </a>
                     </div>
                     <div class="clr"></div>
                  </div>
                  <div class="block_introMenu">
                     <ul>
                        <li ><a href="/tu-van-thiet-ke-dong-phuc-cong-so-cao-cap-a32" class="start" title="Tư vấn thiết kế Đồng phục Công sở cao cấp" target="_self">Tư vấn thiết kế Đồng phục Công sở cao cấp</a></li>
                     </ul>
                  </div>
                  <div class="menu">
                     <div class="facebook">
                        <a href="https://www.facebook.com/seine.vn/?fref=ts"><img src="http://seine.vn/public/templates/public/default/images/facebook.png"></a>
                     </div>
                     <div class="box_menuMain hidden-xs hidden-sm" id="box_menuMain">
                        <ul>
                           <!-- <li><a href="" class="start" title="Trang chủ" target="_self">Trang chủ</a></li> -->
						   <li ><a href=""  title="Giới thiệu" target="_self">Thời trang nữ</a>
							<ul>
                                 <li><a href="/dam-s1" class="start" title="Đầm">Đầm</a></li>
                                 <li><a href="/chan-vay-s2"  title="Chân váy">Chân váy</a></li>
                                 <li><a href="/ao-so-mi-s3"  title="Áo sơ mi">Áo sơ mi</a></li>
                                 <li><a href="/quan-s4"  title="Quần">Quần</a></li>
                                 <li><a href="/vest-s7"  title="Vest">Vest</a></li>
                                 <li><a href="/phu-kien-s8"  title="Phụ kiện">Phụ kiện</a></li>
                                 <li><a href="/mang-to-s10"  title="Măng tô">Măng tô</a></li>
                                 <li><a href="/ao-khoac-s11"  title="Áo khoác ">Áo khoác </a></li>
                              </ul>
						   </li>
						   <li ><a href=""  title="Giới thiệu" target="_self">Thời trang nam</a></li>
						   <li ><a href=""  title="Giới thiệu" target="_self">Thời trangBé gái</a></li>
						   <li ><a href=""  title="Giới thiệu" target="_self">Thời trangBé trai</a></li>
						   <li ><a href=""  title="Giới thiệu" target="_self">Bảng chọn kích cỡ</a></li>
						   <li ><a href=""  title="Giới thiệu" target="_self">Cộng đồng</a></li>
						   <li ><a href=""  title="Giới thiệu" target="_self">Liên hệ</a></li>
                        </ul>
                        <script type="text/javascript">
                           menuNgang('#box_menuMain');
                        </script>
                     </div>
                     <div class="box_menuMobile hidden-md hidden-lg" id="box_menuMobile">
                        <ul>
                           <li><a href="http://seine.vn" class="start" title="Trang chủ" target="_self">Trang chủ</a></li>
                           <li ><a href="/gioi-thieu-a21"  title="Giới thiệu" target="_self">Giới thiệu</a></li>
                           <li ><a href="/tin-tuc-a2"  title="Tin tức" target="_self">Tin tức</a></li>
                           <li>
                              <a href="/san-pham.html"  title="Sản phẩm" target="_self">Sản phẩm</a>
                              <ul>
                                 <li><a href="/dam-s1" class="start" title="Đầm">Đầm</a></li>
                                 <li><a href="/chan-vay-s2"  title="Chân váy">Chân váy</a></li>
                                 <li><a href="/ao-so-mi-s3"  title="Áo sơ mi">Áo sơ mi</a></li>
                                 <li><a href="/quan-s4"  title="Quần">Quần</a></li>
                                 <li><a href="/vest-s7"  title="Vest">Vest</a></li>
                                 <li><a href="/phu-kien-s8"  title="Phụ kiện">Phụ kiện</a></li>
                                 <li><a href="/mang-to-s10"  title="Măng tô">Măng tô</a></li>
                                 <li><a href="/ao-khoac-s11"  title="Áo khoác ">Áo khoác </a></li>
                              </ul>
                           </li>
                           <li><a href="/bo-suu-tap-g1.html"  title="Bộ sưu tập" target="_self">Bộ sưu tập</a></li>
                           <li ><a href="/video.html"  title="Video" target="_self">Video</a></li>
                           <li ><a href="/showroom-a5"  title="Showroom" target="_self">Showroom</a></li>
                           <li><a href="/lien-he.html"  title="Liên hệ" target="_self">Liên hệ</a></li>
                           <li >
                              <a href="/tuyen-dung-a7"  title="Tuyển dụng" target="_self">Tuyển dụng</a>
                              <ul>
                                 <li><a href="/tuyen-nhan-vien-a26"  title="Tuyển nhân viên">Tuyển nhân viên</a></li>
                                 <li><a href="/tuyen-dai-ly-a27"  title="Tuyển Đại lý ">Tuyển Đại lý </a></li>
                                 <li><a href="/tuyen-thiet-ke-a28"  title="Tuyển thiết kế">Tuyển thiết kế</a></li>
                              </ul>
                           </li>
                        </ul>
                        <div class="menu-bg"></div>
                     </div>
                     <script type="text/javascript">
                        $('#box_menuMobile ul > li').each(function(index) {
                        	if($('ul', this).length > 0) {
                           		$(this).addClass('submenu');
                           		$('a:first', this).attr('href', 'javascript:;');
                        	}
                        });
                        
                        
                        $('#box_menuMobile .submenu').click(function() {
                        	$('ul', this).toggleClass('show');
                        });
                        
                        $('#box_menuMobile .menu-bg').click(function() {
                        	$('#box_menuMobile').hide();
                        });
                     </script>                    
                     <div class="clr"></div>
                  </div>
               </div>
            </div>
         </div>
         <div class="btn-toggle-menu hidden-md hidden-lg">
            <!-- <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span> -->
            MENU
         </div>
         <script type="text/javascript">
            $('.btn-toggle-menu').click(function() {
            	$('#box_menuMobile').show();
            });
         </script>
      </div>
      <div class="container">
         <div class="box_slide hidden-sm hidden-xs">
            <ul id="box_slide">
               <li><a href="/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/Banner/dong-gia-dot-3.1.jpg"/></a></li>
               <li><a href="http://seine.vn/bo-suu-tap-g1.html?album=5" target="_self"><img src="/public/files/editor-upload/images/Banner/BST-11.jpg"/></a></li>
               <li><a href="http://seine.vn/tin-tuc-a2" target="_self"><img src="/public/files/editor-upload/images/Banner/AJ2I9733.jpg"/></a></li>
               <li><a href="http://seine.vn/bo-suu-tap-g1.html?album=6" target="_self"><img src="/public/files/editor-upload/images/Banner/24474585-2025335194403232-1807333839-o.jpg"/></a></li>
               <li><a href="http://seine.vn/bo-suu-tap-g1.html" target="_self"><img src="/public/files/editor-upload/images/Banner/banner-the-frets-01--1-.jpg"/></a></li>
            </ul>
            <script type="text/javascript">
               $(document).ready(function() {
               	var carousel = $("#box_slide").waterwheelCarousel({
                       keyboardNav: true,
                       autoPlay: 3000,
                       imageNav: true,
                       flankingItems: 5,
                       separation: 250,
                       opacityMultiplier: 1,
                       movedToCenter: function($newCenterItem) {
                           var imageID = $newCenterItem.attr('id'); // Get the HTML element "id" for this image. Let's say it's "tigerpicture"
                           /* $(".slides .desc").hide();
                           $('#'+imageID+'-desc').show(); */
                       }
                   });
               
               	$('.next').bind('click', function () {
                     carousel.next();
                     return false;
                   });
               
                   $('.prev').bind('click', function () {
                     carousel.prev();
                     return false;
                   });
               });
            </script>
         </div>
         <div class="box_slide slide2 hidden-md hidden-lg">
            <ul id="box_slide2">
               <li><a href="/san-pham.html" target="_self"><img src="/public/files/editor-upload/images/Banner/dong-gia-dot-3.1.jpg"/></a></li>
               <li><a href="http://seine.vn/bo-suu-tap-g1.html?album=5" target="_self"><img src="/public/files/editor-upload/images/Banner/BST-11.jpg"/></a></li>
               <li><a href="http://seine.vn/tin-tuc-a2" target="_self"><img src="/public/files/editor-upload/images/Banner/AJ2I9733.jpg"/></a></li>
               <li><a href="http://seine.vn/bo-suu-tap-g1.html?album=6" target="_self"><img src="/public/files/editor-upload/images/Banner/24474585-2025335194403232-1807333839-o.jpg"/></a></li>
               <li><a href="http://seine.vn/bo-suu-tap-g1.html" target="_self"><img src="/public/files/editor-upload/images/Banner/banner-the-frets-01--1-.jpg"/></a></li>
            </ul>
            <script type="text/javascript">
               $('#box_slide2').bxSlider({
               	//mode: 'fade',
               	controls: false,
               	auto: true,
               	speed: 1000,
               	pause: 5000
               });
            </script>
         </div>
         <div class="box_productNoiBat sanPham">
            <div class="box_title">
               <h3 class="title">Sản phẩm mới cập nhật</h3>
            </div>
            <div class="box_content">
               <div id="box_productNoiBat">
                  <div class="item">
                     <div class="image">
                        <a href="#" title="Quần SQD2032XA-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I0225.jpg" alt="Quần SQD2032XA-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="#"  title="Quần SQD2032XA-Y">Quần SQD2032XA-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">559000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/dam-sdk2196do-y-s1i306.html');" title="Đầm SDK2196DO-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0682.jpg" alt="Đầm SDK2196DO-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2196do-y-s1i306.html');" title="Đầm SDK2196DO-Y">Đầm SDK2196DO-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1269000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/dam-sdk2242hoa-y-s1i305.html');" title="Đầm SDK2242HOA-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0152.jpg" alt="Đầm SDK2242HOA-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2242hoa-y-s1i305.html');" title="Đầm SDK2242HOA-Y">Đầm SDK2242HOA-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1229000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/dam-sdk2281be-y-s1i304.html');" title="Đầm SDK2281BE-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I9438.jpg" alt="Đầm SDK2281BE-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2281be-y-s1i304.html');" title="Đầm SDK2281BE-Y">Đầm SDK2281BE-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1299000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/sjs2088de-y-s2i303.html');" title="SJS2088DE-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I0074.jpg" alt="SJS2088DE-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/sjs2088de-y-s2i303.html');" title="SJS2088DE-Y">SJS2088DE-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">689000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/mang-to-smt2042be-s10i302.html');" title="Măng tô SMT2042BE">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I0010.jpg" alt="Măng tô SMT2042BE"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2042be-s10i302.html');" title="Măng tô SMT2042BE">Măng tô SMT2042BE</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">2769000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/ao-khoac-sak2014tr-y-s11i301.html');" title="Áo khoác SAK2014TR-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I9603.jpg" alt="Áo khoác SAK2014TR-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/ao-khoac-sak2014tr-y-s11i301.html');" title="Áo khoác SAK2014TR-Y">Áo khoác SAK2014TR-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/sjs2085xa-y-s2i300.html');" title="SJS2085XA-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I0192.jpg" alt="SJS2085XA-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/sjs2085xa-y-s2i300.html');" title="SJS2085XA-Y">SJS2085XA-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">659000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/sjs2089gi-y-s2i299.html');" title="SJS2089GI-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I9550.jpg" alt="SJS2089GI-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/sjs2089gi-y-s2i299.html');" title="SJS2089GI-Y">SJS2089GI-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">650000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/dam-mdk2217ti-s1i298.html');" title="Đầm MDK2217TI">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Tran-To-Nhu/HUG-0783.jpg" alt="Đầm MDK2217TI"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/dam-mdk2217ti-s1i298.html');" title="Đầm MDK2217TI">Đầm MDK2217TI</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/dam-sdk2294tit-y-s1i297.html');" title="Đầm SDK2294TIT-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I0245.jpg" alt="Đầm SDK2294TIT-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2294tit-y-s1i297.html');" title="Đầm SDK2294TIT-Y">Đầm SDK2294TIT-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1289000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/ao-khoac-sak2040na-y-s11i296.html');" title="Áo khoác SAK2040NA-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I9913.jpg" alt="Áo khoác SAK2040NA-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/ao-khoac-sak2040na-y-s11i296.html');" title="Áo khoác SAK2040NA-Y">Áo khoác SAK2040NA-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/dam-sdk2273ho-s1i295.html');" title="Đầm SDK2273HO">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I9471.jpg" alt="Đầm SDK2273HO"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/dam-sdk2273ho-s1i295.html');" title="Đầm SDK2273HO">Đầm SDK2273HO</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1789000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/mang-to-smt2023va-y-s10i294.html');" title="Măng tô SMT2023VA-Y">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I9831.jpg" alt="Măng tô SMT2023VA-Y"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2023va-y-s10i294.html');" title="Măng tô SMT2023VA-Y">Măng tô SMT2023VA-Y</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1869000</span> đ</span></div>
                  </div>
                  <div class="item">
                     <div class="image">
                        <a href="javascript:;" onclick="showProduct('/mang-to-smt2039-ydo-s10i293.html');" title="Măng tô SMT2039-YDO">
                        <img class="img" src="/public/files/editor-upload/images/san-pham/Mua-dong-28-12/AJ2I9754.jpg" alt="Măng tô SMT2039-YDO"/>						</a>
                     </div>
                     <h4 class="title"><a href="javascript:;" onclick="showProduct('/mang-to-smt2039-ydo-s10i293.html');" title="Măng tô SMT2039-YDO">Măng tô SMT2039-YDO</a></h4>
                     <div class="price"><span class="value"><span class="auto_numberic">1899000</span> đ</span></div>
                  </div>
               </div>
            </div>
            <script type="text/javascript">
               var w_window = $(document).width();
               var minSlides = 5;
               if(w_window < 1000) {
               	minSlides = 4;
               }
               if(w_window < 700) {
               	minSlides = 2;
               }
               $('#box_productNoiBat').bxSlider({
               	controls: true,
               	auto: false,
               	minSlides: minSlides,
                   	    maxSlides: minSlides,
                     	moveSlides: 1,
                   	    slideMargin: 20,
                     	slideWidth: 550,
               	speed: 1000,
               	pause: 5000
               });
            </script>
         </div>
         <div class="row">
            <div class="col-sm-6">
               <div class="box_adsHome">
                  <div class="box_title"><a href="http://seine.vn/bo-suu-tap-g1.html">Bộ sưu tập mới</a></div>
                  <div class="box_content">
                     <div class="item">
                        <a href="http://seine.vn/bo-suu-tap-g1.html" target="_self">
                        <img src="/public/files/editor-upload/images/Banner/BST111.jpg"/>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-6">
               <div class="box_videoHot">
                  <div class="box_title"><a href="/video.html">Video công ty</a></div>
                  <div class="box_content">
                     <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aLFPTt_yK3M?autoplay=0&controls=1&autohide=1&wmode=opaque&frameborder=0&rel=0&enablejsapi=1&origin=http://seine.vn&widgetid=1"></iframe>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="box_footer">
         <div class="container">
            <div class="row">
               <div class="col-sm-5">
                  <div style="text-transform: uppercase;">
                     Copyright © 2017, Seine Fashion. All Rights Reserved.
                  </div>
               </div>
               <div class="col-sm-2" style="text-align: right;">
                  Hotline: <span style="color:#fff0f5;"><strong><span style="font-size:16px;"><a href="tel:0976155755">0976.155.755</a></span></strong></span>
               </div>
               <div class="col-sm-2" style="text-align: right;">
                  &nbsp;
               </div>
               <div class="col-sm-5" style="text-align: right;">
                  <a href="https://www.instagram.com/seinefashion/"><img src="/public/files/editor-upload/images/he-thong/instagram.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">seinephap</span></a>&nbsp; &nbsp;&nbsp; &nbsp; <a href="javascript:;"><img src="/public/files/editor-upload/images/he-thong/zalo.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">0976.155.755</span></a>&nbsp; &nbsp;&nbsp; &nbsp;<a href="skype:abc"><img src="/public/files/editor-upload/images/he-thong/skype.png" style="width: 30px; height: 30px;" /> <span class="hidden-xs" style="color:#cc9933;">Skype us now</span></a>
               </div>
            </div>
            <div>
               &nbsp;
            </div>
            <div>
               &nbsp;
            </div>
         </div>
      </div>
   </body>
</html>