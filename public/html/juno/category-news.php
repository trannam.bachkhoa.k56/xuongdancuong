<!DOCTYPE HTML>
<html>
   <head>
      <meta property="fb:pages" content="172922056238158" />
      <meta name="p:domain_verify" content="50a57bef3e9a4ae42fbcd722c7074695"/>
      <meta http-equiv="content-type" content="text/html" />
      <meta charset="utf-8" />
      <title>JUNO - Magazine</title>
        <base href="http://vn3c.net/public/html/juno/" target="">
      <meta property="og:title" content="magazine" />
      <meta property="og:image" content="http://theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" />
      <meta property="og:image" content="https://theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" />
      <meta property="og:url" content="https://juno.vn/blogs/magazine" />
      <meta property="og:site_name" content="JUNO" />
      <link rel="canonical" href="https://juno.vn/blogs/magazine" />
      <meta name="google-site-verification" content="YyO2rAtS9EGrKuulJLkr6Czqc98au8arKS_APRqJDZY" />
      <meta property="fb:app_id" content="436645616485850" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0" />
      <link rel="shortcut icon" type="image/png" href="//theme.hstatic.net/1000003969/1000323463/14/favicon.png?v=2220" />
      <!--CSS-->

      <link rel="stylesheet" href="css/bootstrap.css" />
      <link href='css/font-awesome.css' rel='stylesheet' type='text/css'  media='all'  />
      <script src="js/jquery.min.1.11.0.js"></script>
      <script src='js/owl.carousel.2.js' type='text/javascript'></script>
      <link href='css/owl.carousel.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.theme.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.transitions.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/jquery.fancybox.css' rel='stylesheet' type='text/css'  media='all'  />
      <script src='js/jquery.fancybox.js' type='text/javascript'></script>
      <link rel="stylesheet" href="css/slicknav.css" type='text/css' >
      <script src="js/jquery.slicknav.min.js"></script>
      <link href='css/nobita.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/blog-new.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/juno.min.css' rel='stylesheet' type='text/css'  media='all'  />
      <script type='text/javascript'>
         //<![CDATA[
         if ((typeof Haravan) === 'undefined') {
           Haravan = {};
         }
         Haravan.culture = 'vi-VN';
         Haravan.shop = 'juno-1.myharavan.com';
         Haravan.theme = {"name":"desktop_by_nhan(uncompress)","id":1000323463,"role":"main"};
         Haravan.domain = 'juno.vn';
         //]]>
      </script>
      <script>
         //<![CDATA[
         (function() { function asyncLoad() { var urls = ["https://buyxgety.haravan.com/js/script_tag_production.js","https://combo.haravan.com/js/script_tag_production.js"];for (var i = 0; i < urls.length; i++) {var s = document.createElement('script');s.type = 'text/javascript';s.async = true;s.src = urls[i];var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);}}window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);})();
         //]]>
      </script>
      <script type='text/javascript'>
         window.HaravanAnalytics = window.HaravanAnalytics || {};
         window.HaravanAnalytics.meta = window.HaravanAnalytics.meta || {};
         window.HaravanAnalytics.meta.currency = 'VND';
         var meta = {"page":{"pageType":"blog","resourceType":"blog","resourceId":1000040975}};
         for (var attr in meta) {
         	window.HaravanAnalytics.meta[attr] = meta[attr];
         }
      </script>
      <script>
         //<![CDATA[
         window.HaravanAnalytics.ga = "UA-57206615-1";
         window.HaravanAnalytics.enhancedEcommerce = false;
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
         ga('create', window.HaravanAnalytics.ga, 'auto', {allowLinker: true});
         ga('send', 'pageview'); ga('require', 'linker');try {
         setTimeout(function(){
         	if( window.location.pathname == '/checkout' ) {
         		$.get('//file.hstatic.net/1000003969/file/script_checkout_1.jpg',function(data){
         			eval(data);
         		});
         	}
         if( $('.step3').length >= 1 && $(window).width() < 500 ) {
         $('body.step3 > a').remove();
         $('body.step3').prepend("<a href='/cart'><span class='btn-back'>Quay về giỏ hàng</span></a><a class='logo-checkout' href='/'><h1>Đặt hàng thành công</h1></a>");
         }
         
         },500);
         } catch (e) {};
                         //]]>
                         
      </script>
      <script>
         window.HaravanAnalytics.fb = "1566159606933153";
         //<![CDATA[
         !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         document,'script','//connect.facebook.net/en_US/fbevents.js');
         // Insert Your Facebook Pixel ID below. 
         fbq('init', window.HaravanAnalytics.fb );
         fbq('track', 'PageView');
         //]]>
      </script>
      <noscript><img height='1' width='1' style='display:none'src ='https://www.facebook.com/tr?id=1566159606933153&amp;ev=PageView&amp;noscript=1'/></noscript>
      <script>
         if(typeof fbq === 'undefined') {
         	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         	document,'script','//connect.facebook.net/en_US/fbevents.js');
         
         	fbq('init', '1566159606933153');
         	
         	
         	
         	
         	
         	
         	
         	
         	
         	
         	fbq('trackCustom', 'PageViewBlog');
         	
         	
         	
         	fbq('track', '');
         	}else {
         		fbq('track', '');
         	}
      </script>
      <!-- ematics Insight script -->
      <script type="text/javascript">
         var _siteId="580190698";
         (function(){
         var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//st-a.anthill.vn/adx.js";
         var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)})();
      </script>
      <script src='js/smartsearch.min.js' type='text/javascript'></script>
   </head>
   <body>
      <header class=" template-page-blog-magazine">
         <div class="top hidden-sm hidden-xs">
            <div class="container">
               <div class="row">
                  <!---->
                  <div class="col-md-5 col-sm-10 left no-padding">
                     <div class="col-md-3 col-sm-10 logoTop">
                        <div class="logo">
                           <a href="/" title="JUNO"><img alt="JUNO" class="" src="//theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" /></a>
                        </div>
                     </div>
                     <div class="col-md-7 col-sm-10 no-padding searchTop">
                        <div class="search-collection col-xs-10 no-padding">
                           <form class="search" action="/search">
                              <input id="text-product" class="col-xs-10 no-padding" type="text" name="q" placeholder="Bạn cần tìm gì?" />
                              <input type="hidden" value="product" name="type" />
                              <button id="submit-search">
                              <i class="fa fa-search" aria-hidden="true"></i>
                              </button>                        
                           </form>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-5 col-sm-10 no-padding rightTop_head">
                     <div class="col-md-5 col-sm-10 switchboardTop">
                        <div class="switchboard_wrapper">	
                           <i class="fa fa-phone" aria-hidden="true"></i>
                           <span>BÁN HÀNG: <strong>1800 1162</strong> (miễn phí)</span>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-10 no-padding storeTop">
                        <div class="headStore_wrapper">														
                           <a  href="/pages/tim-dia-chi-cua-hang">
                           <i class="fa fa-building" aria-hidden="true"></i>
                           <span>Xem hệ thống <strong>68</strong> cửa hàng</span>
                           </a>
                        </div>
                     </div>
                     <div class="col-md-1 col-sm-10 no-padding cartTop">
                        <div class="carttop_wrapper">
                           <div class="cart-relative">
                              <a href="/cart">
                                 <div class="cart-total-price">
                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                    <span class="price" >2 sản phẩm</span>
                                    <span class="hidden" >Giỏ Hàng</span>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!---->
               </div>
            </div>
         </div>
         <div id="fix-top-menu" class="top2 hidden-sm hidden-xs">
            <div class="container-fluid menutopid" style="">
               <div class="container" style="position:relative">
                  <div class="row">
                     <div class="col-lg-10 col-md-10">
                        <ul class="menu-top clearfix hidden-xs">
                           <li class="menu-li hasChild " >
                              <a href="/collections/tui-xach" class="" >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_tuixach.png?v=2">
                                       <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tuixach_hover.png?v=2') no-repeat center top;"></div>
                                       <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tuixach.png?v=2') no-repeat center top;"></div>
                                    </div>
                                    <span class="title-main-menu">
                                    Túi Xách
                                    </span>
                                 </div>
                              </a>
                              <ul class=" dropdown-menu drop-menu dropmenu_item_show_1">
                                 <li class="menu-hover-li true">
                                    <div class="col-lg-10 col-md-10 menu-back-new">
                                       <span class="menu-title-new" >Mới nhất hôm nay</span>
                                    </div>
                                    <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                       <div class="col-lg-5 col-md-5" style="padding:0">
                                          <a href="https://juno.vn/products/tui-xach-trung-txt124">
                                             <div class="field-sale-2"><span>MỚI</span></div>
                                             <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/xanh_txt124_1.jpg"/>
                                          </a>
                                       </div>
                                       <div class="col-lg-5 col-md-5 menu-content-new">
                                          <div style="padding-bottom: 10px;">
                                             <span class="menu-tilte-pr" >Túi xách trung sọc vân đính tua rua trang trí TXT124</span><br/>
                                          </div>
                                          <div class="menu-price-pr">750,000d<sup>đ</sup></div>
                                          <a href="https://juno.vn/products/tui-xach-trung-txt124">Xem chi tiết</a>
                                       </div>
                                    </div>
                                 </li>
                                 <li><a href="/collections/tui-xach-co-lon"><i class="fa fa-caret-right"></i> Túi cỡ lớn</a></li>
                                 <li><a href="/collections/tui-xach-co-trung"><i class="fa fa-caret-right"></i> Túi cỡ trung</a></li>
                                 <li><a href="/collections/tui-xach-co-nho"><i class="fa fa-caret-right"></i> Túi nhỏ</a></li>
                                 <li><a href="/collections/vi-cam-tay"><i class="fa fa-caret-right"></i> Ví cầm tay</a></li>
                                 <li><a href="/collections/clutch"><i class="fa fa-caret-right"></i> Clutch</a></li>
                                 <li><a href="/collections/ba-lo-thoi-trang"><i class="fa fa-caret-right"></i> Ba lô thời trang</a></li>
                                 <li><a href="/collections/tui-canvas"><i class="fa fa-caret-right"></i> Túi canvas</a></li>
                              </ul>
                           </li>
                           <li class="menu-li hasChild " >
                              <a href="/collections/giay-cao-got" class="" >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_caogot.png?v=2">
                                       <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_caogot_hover.png?v=2') no-repeat center top;"></div>
                                       <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_caogot.png?v=2') no-repeat center top;"></div>
                                    </div>
                                    <span class="title-main-menu">
                                    Giày Cao Gót
                                    </span>
                                 </div>
                              </a>
                              <ul class="first dropdown-menu drop-menu dropmenu_item_show_2">
                                 <li class="menu-hover-li true">
                                    <div class="col-lg-10 col-md-10 menu-back-new">
                                       <span class="menu-title-new" >Mới nhất hôm nay</span>
                                    </div>
                                    <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                       <div class="col-lg-5 col-md-5" style="padding:0">
                                          <a href="https://juno.vn/products/giay-cao-got-cg09092">
                                             <div class="field-sale-2"><span>MỚI</span></div>
                                             <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/bac_cg09092_1.jpg"/>
                                          </a>
                                       </div>
                                       <div class="col-lg-5 col-md-5 menu-content-new">
                                          <div style="padding-bottom: 10px;">
                                             <span class="menu-tilte-pr" >Giày cao gót 9cm mũi nhọn gót nhọn phối kim nhũ CG09092</span><br/>
                                          </div>
                                          <div class="menu-price-pr">430,000d<sup>đ</sup></div>
                                          <a href="https://juno.vn/products/giay-cao-got-cg09092">Xem chi tiết</a>
                                       </div>
                                    </div>
                                 </li>
                                 <li><a href="/collections/cao-got-cao-5cm"><i class="fa fa-caret-right"></i> Cao 5cm</a></li>
                                 <li><a href="/collections/cao-got-cao-7cm"><i class="fa fa-caret-right"></i> Cao 7cm</a></li>
                                 <li><a href="/collections/giay-cao-got-cao-9cm"><i class="fa fa-caret-right"></i> Cao 9cm</a></li>
                                 <li><a href="/collections/cao-got-cao-11cm"><i class="fa fa-caret-right"></i> Cao 11cm</a></li>
                                 <li><a href="/collections/cao-got-mui-tron"><i class="fa fa-caret-right"></i> Mũi Tròn</a></li>
                                 <li><a href="/collections/cao-got-mui-nhon"><i class="fa fa-caret-right"></i> Mũi Nhọn</a></li>
                                 <li><a href="/collections/cao-got-got-nhon"><i class="fa fa-caret-right"></i> Gót Nhọn</a></li>
                                 <li><a href="/collections/cao-got-got-vuong"><i class="fa fa-caret-right"></i> Gót Vuông</a></li>
                              </ul>
                           </li>
                           <li class="menu-li hasChild " >
                              <a href="/collections/giay-xang-dan" class="" >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_xangdan.png?v=2">
                                       <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_xangdan_hover.png?v=2') no-repeat center top;"></div>
                                       <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_xangdan.png?v=2') no-repeat center top;"></div>
                                    </div>
                                    <span class="title-main-menu">
                                    Giày Xăng Đan
                                    </span>
                                 </div>
                              </a>
                              <ul class="first dropdown-menu drop-menu dropmenu_item_show_3">
                                 <li class="menu-hover-li true">
                                    <div class="col-lg-10 col-md-10 menu-back-new">
                                       <span class="menu-title-new" >Mới nhất hôm nay</span>
                                    </div>
                                    <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                       <div class="col-lg-5 col-md-5" style="padding:0">
                                          <a href="https://juno.vn/products/giay-sandal-sd11005">
                                             <div class="field-sale-2"><span>MỚI</span></div>
                                             <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/den_sd11005_1.jpg"/>
                                          </a>
                                       </div>
                                       <div class="col-lg-5 col-md-5 menu-content-new">
                                          <div style="padding-bottom: 10px;">
                                             <span class="menu-tilte-pr" >Giày Sandal cao 11cm quai ngang chất liệu satin SD11005</span><br/>
                                          </div>
                                          <div class="menu-price-pr">430,000d<sup>đ</sup></div>
                                          <a href="https://juno.vn/products/giay-sandal-sd11005">Xem chi tiết</a>
                                       </div>
                                    </div>
                                 </li>
                                 <li><a href="/collections/xang-dan-cao-5cm"><i class="fa fa-caret-right"></i> Cao 5cm</a></li>
                                 <li><a href="/collections/xang-dan-cao-7cm"><i class="fa fa-caret-right"></i> Cao 7cm</a></li>
                                 <li><a href="/collections/xang-dan-cao-9cm"><i class="fa fa-caret-right"></i> Cao 9cm</a></li>
                                 <li><a href="/collections/xang-dan-cao-11cm"><i class="fa fa-caret-right"></i> Cao 11cm</a></li>
                                 <li><a href="/collections/xang-dan-de-xuong"><i class="fa fa-caret-right"></i> Xăng Đan Đế Xuồng</a></li>
                                 <li><a href="/collections/xang-dan-cao-got"><i class="fa fa-caret-right"></i> Xăng Đan Cao Gót</a></li>
                                 <li><a href="/collections/xang-dan-bet"><i class="fa fa-caret-right"></i> Xăng Đan Bệt</a></li>
                              </ul>
                           </li>
                           <li class="menu-li hasChild " >
                              <a href="/collections/giay-bup-be" class="" >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_bupbe.png?v=2">
                                       <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bupbe_hover.png?v=2') no-repeat center top;"></div>
                                       <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bupbe.png?v=2') no-repeat center top;"></div>
                                    </div>
                                    <span class="title-main-menu">
                                    Giày Búp Bê
                                    </span>
                                 </div>
                              </a>
                              <ul class="first dropdown-menu drop-menu dropmenu_item_show_4">
                                 <li class="menu-hover-li true">
                                    <div class="col-lg-10 col-md-10 menu-back-new">
                                       <span class="menu-title-new" >Mới nhất hôm nay</span>
                                    </div>
                                    <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                       <div class="col-lg-5 col-md-5" style="padding:0">
                                          <a href="https://juno.vn/products/giay-bup-be-bb01113">
                                             <div class="field-sale-2"><span>MỚI</span></div>
                                             <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/do_bb01113_1_c7cce527f1a34a468db3b52a7f02d6f8.jpg"/>
                                          </a>
                                       </div>
                                       <div class="col-lg-5 col-md-5 menu-content-new">
                                          <div style="padding-bottom: 10px;">
                                             <span class="menu-tilte-pr" >Giày búp bê mũi nhọn chất liệu Satin BB01113</span><br/>
                                          </div>
                                          <div class="menu-price-pr">390,000d<sup>đ</sup></div>
                                          <a href="https://juno.vn/products/giay-bup-be-bb01113">Xem chi tiết</a>
                                       </div>
                                    </div>
                                 </li>
                                 <li><a href="/collections/bup-be-mui-tron"><i class="fa fa-caret-right"></i> Giày Mũi Tròn</a></li>
                                 <li><a href="/collections/bup-be-mui-nhon"><i class="fa fa-caret-right"></i> Giày Mũi Nhọn</a></li>
                                 <li><a href="/collections/bup-be-co-trang-tri"><i class="fa fa-caret-right"></i> Giày có trang trí</a></li>
                              </ul>
                           </li>
                           <li class="menu-li   fix-icon-coll "
                              >
                              <a href="/collections/sneaker-collections" class=""  >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_sneaker.png?v=2">
                                       <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_sneaker.png?v=2') no-repeat center center;"></div>
                                       <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_sneaker.png?v=2') no-repeat center center;"></div>
                                    </div>
                                    <span class="title-main-menu ">Giày Sneaker</span>
                                 </div>
                              </a>
                           </li>
                           <li class="menu-li   fix-icon-coll "
                              >
                              <a href="/collections/giay-boots" class=""  >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_boots.png?v=2">
                                       <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_boots.png?v=2') no-repeat center center;"></div>
                                       <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_boots.png?v=2') no-repeat center center;"></div>
                                    </div>
                                    <span class="title-main-menu ">Giày Boots</span>
                                 </div>
                              </a>
                           </li>
                           <li class="menu-li   fix-icon-coll "
                              >
                              <a href="/collections/dep-guoc" class=""  >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_depguoc.png?v=2">
                                       <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_depguoc.png') no-repeat center center;"></div>
                                       <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_depguoc.png') no-repeat center center;"></div>
                                    </div>
                                    <span class="title-main-menu ">Dép Guốc</span>
                                 </div>
                              </a>
                           </li>
                           <li class="menu-li active  fix-icon-coll "
                              >
                              <a href="/collections/8-3-yeu-thuong-gap-doi" class=""  >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_yeuthuonggapdoi.png?v=2">
                                       <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_event.png') no-repeat center center;"></div>
                                       <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_event.png') no-repeat center center;"></div>
                                    </div>
                                    <span class="title-main-menu ">Yêu Thương Gấp Đôi</span>
                                 </div>
                              </a>
                           </li>
                           <li class="menu-li   fix-icon-coll "
                              >
                              <a href="/collections/juno-queens-of-fashion" class=""  >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_bosuutap.png?v=2">
                                       <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bosuutap.png?v=2') no-repeat center center;"></div>
                                       <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bosuutap.png?v=2') no-repeat center center;"></div>
                                    </div>
                                    <span class="title-main-menu ">Bộ sưu tập</span>
                                 </div>
                              </a>
                           </li>
                           <li class="menu-li  active fix-icon-coll active main"
                              >
                              <a href="/blogs/magazine" class="active main" target="_blank"  >
                                 <div class="coll-icon">
                                    <div class="ico-top" data-position="new_iconimport_tinthoitrang.png?v=2">
                                       <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tinthoitrang.png?v=2') no-repeat center center;"></div>
                                       <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tinthoitrang.png?v=2') no-repeat center center;"></div>
                                    </div>
                                    <span class="title-main-menu ">Tin Thời Trang</span>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <style>
            .coll-icon .ico-top {
            position: relative;
            overflow: hidden;
            width: 60px;
            height: 36px;
            padding: 0;
            margin: 0 auto;
            display: block;
            text-align: center;
            }
            .coll-icon .ico-top .img-on {
            opacity: 0;
            height: 0;
            transition: all 0.5s;
            }
            .coll-icon .ico-top .img-off{
            opacity: 1;
            width: 60px;
            height: 36px;
            transition: all 0.3s
            }
            /*.coll-icon .ico-top:hover .img-on{
            opacity: 1;
            height: 36px;
            width: 60px;
            }
            .coll-icon .ico-top:hover .img-off{
            opacity: 0;
            height: 0;
            }*/
         </style>
         <div class="top hidden-lg hidden-md" id="mobile-menu">
            <div class="fixed-nav">
               <button id="menu-toggle" class="navbar-toggle pull-left" type="button" data-toggle="modal" data-target="#menu-modal">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="/" title="JUNO" class="logo"><img class="lazy" alt="JUNO" src="" data-src="//theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" /></a>
               <a href="/cart" class="cart-link">
               <span ><i class="fa fa-shopping-bag" aria-hidden="true"></i> <strong>960,000₫</strong></span>
               </a>
               <form class="frm-search" action='/search'>
                  <input  type="text" name='q' value="" id="inputSearch" placeholder="Tìm kiếm...">	
               </form>
               <script>
                  $(document).ready(function(){
                  
                  	$('#inputSearch').smartSearch({searchdelay:400});
                  
                  	$('.btn-search').click(function(){
                  		$('.frm-search').find('input').val('');
                  		if($(this).hasClass('active')) {
                  			$(this).removeClass('active');
                  			$('.frm-search').find('input').css('width', '1px');
                  			setTimeout(function(){
                  				$('.frm-search').find('input').css('display', 'none');	
                  				$('.logo').show();
                  				$('#menu-toggle').show();
                  				$('body').removeClass('searching');
                  			},700);
                  
                  		}
                  		else {
                  			$(this).addClass('active');
                  			$('.logo').hide();
                  			$('#menu-toggle').hide();
                  			$('.frm-search').find('input').css('display', 'block');
                  			setTimeout(function(){
                  				var a = screen.width - 90;
                  				$('.frm-search').find('input').css('width', a);
                  				$('body').addClass('searching');
                  			},100);
                  		}
                  	});
                  
                  	$('#wrapper').click(function(e){
                  		if (e.target !== $('.btn-search')){
                  			return;
                  		}else{
                  			$('.btn-search').removeClass('active');
                  			$('.frm-search').find('input').css('width', '1px');
                  			setTimeout(function(){
                  				$('.frm-search').find('input').css('display', 'none');	
                  				$('.logo').show();
                  				$('#menu-toggle').show();
                  				$('body').removeClass('searching');
                  			},500);
                  		}
                  	});
                  
                  
                  
                  });
                  
                  function search(){
                  debugger
                  	var url = 'filter=((title:product**[KEY]))&type=product';
                  	var search_text = $('#inputSearch').val();
                  	if (search_text.length > 0){
                  		url = url.replace('[KEY]', search_text);
                  		window.location.href = '/search?q='+ encodeURIComponent(url);
                  	}
                  	return false;
                  }
               </script>
            </div>
         </div>
      </header>
      <div class="wrapper">
         <div class="container">
            <div class="top-container">
               <div style="margin-top:10px">
                  <div class="col-lg-4 col-md-4 col-sm-10 menu" style="padding:0px">
                     <ul>
                        <li class="-menu-blog" style="float:left;padding: 0 10px"><a style="color:#000;text-transform:uppercase;font-family:'SFU'" href="/blogs/mot">Mốt</a></li>
                        <li class="-menu-blog" style="float:left;padding: 0 10px"><a style="color:#000;text-transform:uppercase;font-family:'SFU'" href="/blogs/dep">Đẹp</a></li>
                        <li class="-menu-blog" style="float:left;padding: 0 10px"><a style="color:#000;text-transform:uppercase;font-family:'SFU'" href="/blogs/khoe">Khoẻ</a></li>
                        <li class="-menu-blog" style="float:left;padding: 0 10px"><a style="color:#000;text-transform:uppercase;font-family:'SFU'" href="/blogs/meo">Mẹo</a></li>
                        <li class="-menu-blog" style="float:left;padding: 0 10px"><a style="color:#000;text-transform:uppercase;font-family:'SFU'" href="/blogs/ngon">Ngon</a></li>
                        <li class="-menu-blog" style="float:left;padding: 0 10px"><a style="color:#000;text-transform:uppercase;font-family:'SFU'" href="/blogs/em">Em</a></li>
                        <li class="-menu-blog" style="float:left;padding: 0 10px"><a style="color:#000;text-transform:uppercase;font-family:'SFU'" href="/blogs/song">Sống</a></li>
                     </ul>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-10" >
                     <p style="float:right">
                        JUNO MAGAZINE <span>Khám phá, tận hưởng niềm đam mê thời trang và làm đẹp mỗi ngày</span>
                     </p>
                  </div>
               </div>
            </div>
            <div class="col-xs-10 col-sm-6">
               <div class="row">
                  <div id="owl-blog-slider" class="owl-carousel owl-theme">
                     <div class="blog-slider-item 5">
                        <a href="/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3"><img src="//file.hstatic.net/1000003969/file/banner-juno-giay-tui.jpg" alt="10 Cặp đôi giày túi đẹp mãn nhãn xứng đáng làm quà 8-3"></a>
                        <div class="blog-slider-info">
                           <a href="/blogs/dep" class="blog-category">Đẹp</a>
                           <h3>
                              <a href="/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3" class="blog-title">10 Cặp đôi giày túi đẹp mãn nhãn xứng đáng làm quà 8-3</a>
                           </h3>
                        </div>
                     </div>
                     <div class="blog-slider-item 5">
                        <a href="/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do"><img src="//file.hstatic.net/1000003969/file/giay-do-juno-1.jpg" alt="Này Anh, Anh có yêu một cô gái mang giày màu đỏ?"></a>
                        <div class="blog-slider-info">
                           <a href="/blogs/dep" class="blog-category">Đẹp</a>
                           <h3>
                              <a href="/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do" class="blog-title">Này Anh, Anh có yêu một cô gái mang giày màu đỏ?</a>
                           </h3>
                        </div>
                     </div>
                     <div class="blog-slider-item 5">
                        <a href="/blogs/dep/6-mau-tui-xach-hot-ran-ran-duoc-san-lung-truoc-them-8-3"><img src="//file.hstatic.net/1000003969/file/0-juno-8-3.jpg" alt="7 Mẫu túi xách hot rần rần được săn lùng trước thềm 8/3"></a>
                        <div class="blog-slider-info">
                           <a href="/blogs/dep" class="blog-category">Đẹp</a>
                           <h3>
                              <a href="/blogs/dep/6-mau-tui-xach-hot-ran-ran-duoc-san-lung-truoc-them-8-3" class="blog-title">7 Mẫu túi xách hot rần rần được săn lùng trước thềm 8/3</a>
                           </h3>
                        </div>
                     </div>
                     <div class="blog-slider-item 5">
                        <a href="/blogs/dep/cua-chang-them-lan-nua-voi-giay-do-quyen-ru-trong-ngay-8-3"><img src="//file.hstatic.net/1000003969/file/giay-do-juno.jpg" alt="Cưa chàng thêm lần nữa với giày đỏ quyến rũ trong ngày 8-3"></a>
                        <div class="blog-slider-info">
                           <a href="/blogs/dep" class="blog-category">Đẹp</a>
                           <h3>
                              <a href="/blogs/dep/cua-chang-them-lan-nua-voi-giay-do-quyen-ru-trong-ngay-8-3" class="blog-title">Cưa chàng thêm lần nữa với giày đỏ quyến rũ trong ngày 8-3</a>
                           </h3>
                        </div>
                     </div>
                     <div class="blog-slider-item 5">
                        <a href="/blogs/dep/mac-dam-dai-thuot-tha-mang-voi-giay-gi-de-8-3-toa-ra-khi-chat"><img src="//file.hstatic.net/1000003969/file/vay-dai-sandal-juno.jpg" alt="Mặc đầm dài thướt tha, mang với giày gì để 8-3 tỏa ra khí chất?"></a>
                        <div class="blog-slider-info">
                           <a href="/blogs/dep" class="blog-category">Đẹp</a>
                           <h3>
                              <a href="/blogs/dep/mac-dam-dai-thuot-tha-mang-voi-giay-gi-de-8-3-toa-ra-khi-chat" class="blog-title">Mặc đầm dài thướt tha, mang với giày gì để 8-3 tỏa ra khí chất?</a>
                           </h3>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xs-10 col-sm-4">
               <div class="row">
                  <div class="blog-read-viewed">
                     <h2>
                        <span>Bài được xem nhiều nhất</span>
                     </h2>
                     <div class="clearfix blog-read-item">
                        <a href="/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3" target="_blank">
                           <div class="item-img pull-left">
                              <img class="lazy" src="//file.hstatic.net/1000003969/file/banner-juno-giay-tui_compact.jpg"/><span style="margin-left:-100px">1</span>
                           </div>
                        </a>
                        <div class="caption pull-right" >
                           <h3 class="title"><a href="/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3" target="_blank">10 Cặp đôi giày túi đẹp mãn nhãn xứng đáng làm quà 8-3</a></h3>
                           <span class="date">02.03.2018</span>
                           <a href="/blogs/dep" target="_blank" class="title-category">Tin Đẹp</a>
                        </div>
                     </div>
                     <div class="clearfix blog-read-item">
                        <a href="/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do" target="_blank">
                           <div class="item-img pull-left">
                              <img class="lazy" src="//file.hstatic.net/1000003969/file/giay-do-juno-1_compact.jpg"/><span style="margin-left:-100px">2</span>
                           </div>
                        </a>
                        <div class="caption pull-right" >
                           <h3 class="title"><a href="/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do" target="_blank">Này Anh, Anh có yêu một cô gái mang giày màu đỏ?</a></h3>
                           <span class="date">02.03.2018</span>
                           <a href="/blogs/dep" target="_blank" class="title-category">Tin Đẹp</a>
                        </div>
                     </div>
                     <div class="clearfix blog-read-item">
                        <a href="/blogs/dep/6-mau-tui-xach-hot-ran-ran-duoc-san-lung-truoc-them-8-3" target="_blank">
                           <div class="item-img pull-left">
                              <img class="lazy" src="//file.hstatic.net/1000003969/file/0-juno-8-3_compact.jpg"/><span style="margin-left:-100px">3</span>
                           </div>
                        </a>
                        <div class="caption pull-right" >
                           <h3 class="title"><a href="/blogs/dep/6-mau-tui-xach-hot-ran-ran-duoc-san-lung-truoc-them-8-3" target="_blank">7 Mẫu túi xách hot rần rần được săn lùng trước thềm 8/3</a></h3>
                           <span class="date">02.03.2018</span>
                           <a href="/blogs/dep" target="_blank" class="title-category">Tin Đẹp</a>
                        </div>
                     </div>
                     <div class="clearfix blog-read-item">
                        <a href="/blogs/dep/cua-chang-them-lan-nua-voi-giay-do-quyen-ru-trong-ngay-8-3" target="_blank">
                           <div class="item-img pull-left">
                              <img class="lazy" src="//file.hstatic.net/1000003969/file/giay-do-juno_compact.jpg"/><span style="margin-left:-100px">4</span>
                           </div>
                        </a>
                        <div class="caption pull-right" >
                           <h3 class="title"><a href="/blogs/dep/cua-chang-them-lan-nua-voi-giay-do-quyen-ru-trong-ngay-8-3" target="_blank">Cưa chàng thêm lần nữa với giày đỏ quyến rũ trong ngày 8-3</a></h3>
                           <span class="date">01.03.2018</span>
                           <a href="/blogs/dep" target="_blank" class="title-category">Tin Đẹp</a>
                        </div>
                     </div>
                     <div class="clearfix blog-read-item">
                        <a href="/blogs/dep/mac-dam-dai-thuot-tha-mang-voi-giay-gi-de-8-3-toa-ra-khi-chat" target="_blank">
                           <div class="item-img pull-left">
                              <img class="lazy" src="//file.hstatic.net/1000003969/file/vay-dai-sandal-juno_compact.jpg"/><span style="margin-left:-100px">5</span>
                           </div>
                        </a>
                        <div class="caption pull-right" >
                           <h3 class="title"><a href="/blogs/dep/mac-dam-dai-thuot-tha-mang-voi-giay-gi-de-8-3-toa-ra-khi-chat" target="_blank">Mặc đầm dài thướt tha, mang với giày gì để 8-3 tỏa ra khí chất?</a></h3>
                           <span class="date">01.03.2018</span>
                           <a href="/blogs/dep" target="_blank" class="title-category">Tin Đẹp</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="blog-category-items">
               <h3 class="category-title">
                  <a href="/blogs/mot">Mốt</a>
               </h3>
               <div class="items-ngang">
                  <div class="row">
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/mot/ao-khoac-trench-coat-lot-top-5-xu-huong-thoi-trang-2018-dep-nin-tho-trong-mv-nguoi-la-oi-cua-karik">Áo khoác Trench Coat lọt top 5 xu hướng thời trang 2018, đẹp nín thở trong MV “Người lạ ơi” của Karik</a>
                           </h3>
                           <span class="date">08.01/2018</span>
                           <p class="des">
                              Xuất hiện trong MV “Người lạ ơi” của Karik với những khung hình đẹp long lanh tại Đà Lạt, áo khoác trench coat nhanh chóng hút hồn các tín đồ thời trang.
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000515921-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/mot/ao-khoac-trench-coat-lot-top-5-xu-huong-thoi-trang-2018-dep-nin-tho-trong-mv-nguoi-la-oi-cua-karik" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/mot/ao-khoac-trench-coat-lot-top-5-xu-huong-thoi-trang-2018-dep-nin-tho-trong-mv-nguoi-la-oi-cua-karik&t=Áo khoác Trench Coat lọt top 5 xu hướng thời trang 2018, đẹp nín thở trong MV “Người lạ ơi” của Karik" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/mot/ao-khoac-trench-coat-lot-top-5-xu-huong-thoi-trang-2018-dep-nin-tho-trong-mv-nguoi-la-oi-cua-karik" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/mot/ao-khoac-trench-coat-lot-top-5-xu-huong-thoi-trang-2018-dep-nin-tho-trong-mv-nguoi-la-oi-cua-karik">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/coat-lot-top-5-xu-huong-thoi-trang-2018-dep-ni-tho-mv-nguoi-la-oi__29__66833da23f214ac6afa850c90773b1ac.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/mot/don-dau-5-xu-huong-thoi-trang-du-bao-gay-bao-nam-2018">Đón đầu 5 xu hướng thời trang dự báo “gây bão” năm 2018</a>
                           </h3>
                           <span class="date">08.01/2018</span>
                           <p class="des">
                              Nắm chắc trong tay 5 xu hướng thời trang 2018 này, nàng sẽ tự tin trở thành cô nàng hợp mốt của năm.
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000515716-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/mot/don-dau-5-xu-huong-thoi-trang-du-bao-gay-bao-nam-2018" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/mot/don-dau-5-xu-huong-thoi-trang-du-bao-gay-bao-nam-2018&t=Đón đầu 5 xu hướng thời trang dự báo “gây bão” năm 2018" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/mot/don-dau-5-xu-huong-thoi-trang-du-bao-gay-bao-nam-2018" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/mot/don-dau-5-xu-huong-thoi-trang-du-bao-gay-bao-nam-2018">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/sp600.800-2.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="items-doc">
                  <div class="row">
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/mot/4-xu-huong-giay-duoc-yeu-thich-nhat-nam-2018">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/mau-tui-xach-giay-moi-bst-juno-queens-of-fashion-2017-_71__b1383c8c1ffd4f3abe02e87b1e41c73b.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/mot/4-xu-huong-giay-duoc-yeu-thich-nhat-nam-2018">4 Xu hướng giày dự đoán hot nhất năm 2018</a>
                        </h3>
                        <span class="date Mốt">06.01.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/mot/hai-thai-cuc-thoi-trang-cua-ca-si-huong-giang-idol-kieu-nao-cung-dep">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/-thai-cuc-thoi-trang-cua-ca-si-huong-giang-idol-kieu-nao-cung-dep__01_.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/mot/hai-thai-cuc-thoi-trang-cua-ca-si-huong-giang-idol-kieu-nao-cung-dep">Hai thái cực thời trang của ca sĩ Hương Giang Idol: kiểu nào cũng Đẹp</a>
                        </h3>
                        <span class="date Mốt">05.01.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/mot/neu-qua-thuc-tim-la-mau-hot-nhat-nam-2018-thi-day-la-5-giai-phap-giup-tim-bot-sen">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/au-hot-nhat-nam-2018-thi-day-la-5-gam-mau-che-ngu-mau-tim-bot-sen__12_.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/mot/neu-qua-thuc-tim-la-mau-hot-nhat-nam-2018-thi-day-la-5-giai-phap-giup-tim-bot-sen">Nếu quả thực Tím là màu hot nhất năm 2018, thì đây là 5 giải pháp giúp tím bớt “sến”</a>
                        </h3>
                        <span class="date Mốt">02.01.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/mot/chuyen-gi-xay-ra-khi-phai-dep-mac-suit">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/chuyen-gi-xay-ra-khi-phu-nu-mac-suit__14__1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/mot/chuyen-gi-xay-ra-khi-phai-dep-mac-suit">Chuyện gì xảy ra khi phái đẹp mặc Suit?</a>
                        </h3>
                        <span class="date Mốt">04.09.2017</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/mot/do-gam-mau-duoc-cac-nha-mot-dong-loat-lang-xe">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/do-gam-mau-duoc-cac-nha-mot-dong-loat-lang-xe__164__1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/mot/do-gam-mau-duoc-cac-nha-mot-dong-loat-lang-xe">Đỏ - Gam màu được các nhà Mốt đồng loạt lăng xê cuối năm 2017</a>
                        </h3>
                        <span class="date Mốt">25.07.2017</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="blog-category-items">
               <h3 class="category-title">
                  <a href="/blogs/dep">Đẹp</a>
               </h3>
               <div class="items-ngang">
                  <div class="row">
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3">10 Cặp đôi giày túi đẹp mãn nhãn xứng đáng làm quà 8-3</a>
                           </h3>
                           <span class="date">02.03/2018</span>
                           <p class="des">
                              Bước chân khỏi Tết, nàng chạm ngõ một trong những ngày đặc biệt nhất trong năm là 8/3. Đây là ngày mà mọi phụ nữ khắp nơi trên thế giới đều mong chờ, chờ yêu thương cất cánh!
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000538437-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3&t=10 Cặp đôi giày túi đẹp mãn nhãn xứng đáng làm quà 8-3" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/dep/10-cap-doi-giay-tui-dep-man-nhan-xung-dang-lam-qua-8-3">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/banner-juno_e8ea5cb4d647433083a2f7645f0c0ad5.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do">Này Anh, Anh có yêu một cô gái mang giày màu đỏ?</a>
                           </h3>
                           <span class="date">02.03/2018</span>
                           <p class="des">
                              Trước thềm 8/3 trong lúc các chàng đang hoang mang chưa biết tặng gì cho một nửa yêu thương của mình, thì cộng đồng mạng xôn xao câu chuyện chàng trai chia tay bạn gái vì cô mặc quần ren...
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000539065-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do&t=Này Anh, Anh có yêu một cô gái mang giày màu đỏ?" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/dep/nay-anh-anh-co-thich-mot-co-gai-mang-giay-mau-do">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/juno-giay-do-8-3.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="items-doc">
                  <div class="row">
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/dep/6-mau-tui-xach-hot-ran-ran-duoc-san-lung-truoc-them-8-3">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/0-juno-8-3-3.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/dep/6-mau-tui-xach-hot-ran-ran-duoc-san-lung-truoc-them-8-3">7 Mẫu túi xách hot rần rần được săn lùng trước thềm 8/3</a>
                        </h3>
                        <span class="date Đẹp">02.03.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/dep/cua-chang-them-lan-nua-voi-giay-do-quyen-ru-trong-ngay-8-3">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/juno-giay-do_f820401aba7d48b3850cf6ab565f437a.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/dep/cua-chang-them-lan-nua-voi-giay-do-quyen-ru-trong-ngay-8-3">Cưa chàng thêm lần nữa với giày đỏ quyến rũ trong ngày 8-3</a>
                        </h3>
                        <span class="date Đẹp">01.03.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/dep/mac-dam-dai-thuot-tha-mang-voi-giay-gi-de-8-3-toa-ra-khi-chat">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/juno-vay-dai.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/dep/mac-dam-dai-thuot-tha-mang-voi-giay-gi-de-8-3-toa-ra-khi-chat">Mặc đầm dài thướt tha, mang với giày gì để 8-3 tỏa ra khí chất?</a>
                        </h3>
                        <span class="date Đẹp">01.03.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/dep/8-3-nam-nay-ai-se-mang-hoa-den-cua-nha-nang">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/juno-8-3.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/dep/8-3-nam-nay-ai-se-mang-hoa-den-cua-nha-nang">8/3 năm nay, ai sẽ mang hoa đến cửa nhà nàng?</a>
                        </h3>
                        <span class="date Đẹp">01.03.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/dep/rot-cuoc-quan-trang-nu-mac-voi-ao-mau-gi-thi-dep">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/giay-juno-quan-trang_a496b2e5278a494f8f385ccc899504b8.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/dep/rot-cuoc-quan-trang-nu-mac-voi-ao-mau-gi-thi-dep">Rốt cuộc quần trắng nữ mặc với áo màu gì thì đẹp?</a>
                        </h3>
                        <span class="date Đẹp">28.02.2018</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="blog-category-items">
               <h3 class="category-title">
                  <a href="/blogs/khoe">Khoẻ</a>
               </h3>
               <div class="items-ngang">
                  <div class="row">
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/khoe/bi-kip-song-sot-qua-ngay-nang-nong">Bí kíp sống sót qua ngày nắng nóng</a>
                           </h3>
                           <span class="date">12.04/2017</span>
                           <p class="des">
                              Nắng nóng kéo dài và nhiệt độ tăng cao luôn là nỗi ám ảnh đối với sức khỏe và sắc đẹp của các cô gái.
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000348777-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/khoe/bi-kip-song-sot-qua-ngay-nang-nong" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/khoe/bi-kip-song-sot-qua-ngay-nang-nong&t=Bí kíp sống sót qua ngày nắng nóng" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/khoe/bi-kip-song-sot-qua-ngay-nang-nong" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/khoe/bi-kip-song-sot-qua-ngay-nang-nong">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/21-1491816800099_d7723b22549149e9bcef21c083649102_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/khoe/nhung-bai-tap-the-duc-don-gian-cho-da-luon-cang-min">Những bài tập thể dục đơn giản cho da luôn căng mịn</a>
                           </h3>
                           <span class="date">30.09/2016</span>
                           <p class="des">
                              Không những vóc dáng mà làn da của nàng cũng cần “thể dục thể thao” để đẹp khỏe mỗi ngày!!!
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000245425-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/khoe/nhung-bai-tap-the-duc-don-gian-cho-da-luon-cang-min" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/khoe/nhung-bai-tap-the-duc-don-gian-cho-da-luon-cang-min&t=Những bài tập thể dục đơn giản cho da luôn căng mịn" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/khoe/nhung-bai-tap-the-duc-don-gian-cho-da-luon-cang-min" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/khoe/nhung-bai-tap-the-duc-don-gian-cho-da-luon-cang-min">
                           <div class="item-img pull-right" style="background: url('//sw001.hstatic.net/12/0c37e06f532cd5/can-canh-nhan-sac-hot-girl-xinh-dep-hang-dau-thai-lan-_3_-c595c_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="items-doc">
                  <div class="row">
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/khoe/nhung-meo-don-gian-cho-giao-mua-chang-lo-cam-cum">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/6/018761d7ca98c7/tinh-dau-sa-chanh-nguyen-chat-extin-1_1024x1024.png') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/khoe/nhung-meo-don-gian-cho-giao-mua-chang-lo-cam-cum">Những mẹo đơn giản cho giao mùa chẳng lo cảm cúm</a>
                        </h3>
                        <span class="date Khỏe">08.09.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/khoe/giam-can-nhanh-bat-ngo-chi-bang-cach-tam">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//hstatic.net/969/1000003969/10/2016/8-15/6_2_ae82bd55-a574-4c64-40e2-56454c25d045.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/khoe/giam-can-nhanh-bat-ngo-chi-bang-cach-tam">Giảm cân nhanh bất ngờ chỉ bằng cách … tắm</a>
                        </h3>
                        <span class="date Khỏe">15.08.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/khoe/diem-danh-cac-loai-trai-cay-an-cang-nhieu-cang-tot-mua-nay">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//hstatic.net/969/1000003969/10/2016/8-9/soda-chanh-day1.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/khoe/diem-danh-cac-loai-trai-cay-an-cang-nhieu-cang-tot-mua-nay">Điểm danh các loại trái cây ăn “càng nhiều càng tốt” mùa này</a>
                        </h3>
                        <span class="date Khỏe">09.08.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/khoe/khoe-hon-bang-cach-chop-mat">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/9/0c389b8e5fe89e/44_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/khoe/khoe-hon-bang-cach-chop-mat">Khoẻ hơn bằng cách “chớp mắt”</a>
                        </h3>
                        <span class="date Khỏe">29.07.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/khoe/vi-sao-di-bo-giup-nang-khoe-hon">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/8/0c38c7ad24adf5/1434306494-9_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/khoe/vi-sao-di-bo-giup-nang-khoe-hon">Vì sao đi bộ giúp nàng khoẻ hơn?</a>
                        </h3>
                        <span class="date Khỏe">26.07.2016</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="blog-category-items">
               <h3 class="category-title">
                  <a href="/blogs/meo">Mẹo</a>
               </h3>
               <div class="items-ngang">
                  <div class="row">
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/meo/lam-sao-de-di-lai-thoai-mai-tren-doi-giay-cao-got">Làm sao để đi lại thoải mái trên đôi giày cao gót</a>
                           </h3>
                           <span class="date">15.01/2018</span>
                           <p class="des">
                              Tất cả phụ nữ trên thế giới này đều mắc nợ người tạo ra giày cao tót. Tuy nhiên để đi trên đôi giày cao gót lại là cả nghệ thuật!
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000520005-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/meo/lam-sao-de-di-lai-thoai-mai-tren-doi-giay-cao-got" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/meo/lam-sao-de-di-lai-thoai-mai-tren-doi-giay-cao-got&t=Làm sao để đi lại thoải mái trên đôi giày cao gót" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/meo/lam-sao-de-di-lai-thoai-mai-tren-doi-giay-cao-got" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/meo/lam-sao-de-di-lai-thoai-mai-tren-doi-giay-cao-got">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/mau-tui-xach-giay-moi-bst-juno-queens-of-fashion-2017-2018__45__8ce952d901094f34966a2b7ea14a5187.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/meo/bi-kip-mua-giay-online-chuan-size-de-khong-phai-doi-lai">Bí kíp mua giày online chuẩn size để không phải đổi lại</a>
                           </h3>
                           <span class="date">15.01/2018</span>
                           <p class="des">
                              Khi mua giày online, nhiều nàng phải đổi lại giày vì chọn sai size. Cùng Juno tìm hiểu cách chọn chuẩn size để mua sắm online tiện lợi và tiết kiệm thời gian nhé!
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000519877-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/meo/bi-kip-mua-giay-online-chuan-size-de-khong-phai-doi-lai" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/meo/bi-kip-mua-giay-online-chuan-size-de-khong-phai-doi-lai&t=Bí kíp mua giày online chuẩn size để không phải đổi lại" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/meo/bi-kip-mua-giay-online-chuan-size-de-khong-phai-doi-lai" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/meo/bi-kip-mua-giay-online-chuan-size-de-khong-phai-doi-lai">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/giay-nu-juno__5_.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="items-doc">
                  <div class="row">
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/meo/meo-mua-giay-online-ngay-can-tet-cua-ninh-duong-lan-ngoc-va-diem-my">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/600.800blog.png') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/meo/meo-mua-giay-online-ngay-can-tet-cua-ninh-duong-lan-ngoc-va-diem-my">Mẹo mua giày online ngày cận Tết của Ninh Dương Lan Ngọc và Diễm My 9x</a>
                        </h3>
                        <span class="date Mẹo">13.01.2018</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/meo/meo-sam-do-du-tiec-giang-sinh-vua-dep-vua-tiet-kiem">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/blog600.800.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/meo/meo-sam-do-du-tiec-giang-sinh-vua-dep-vua-tiet-kiem">Mẹo sắm đồ dự tiệc Giáng Sinh long lanh siêu tiết kiệm</a>
                        </h3>
                        <span class="date Mẹo">22.12.2017</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/meo/giang-sinh-nay-nang-muon-nhan-qua-gi">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/mau-tui-xach-giay-moi-bst-juno-queens-of-fashion-2017__55__da5d55ec134444f5bd05f8ca61811d1a.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/meo/giang-sinh-nay-nang-muon-nhan-qua-gi">Giáng Sinh này nàng muốn nhận quà gì?</a>
                        </h3>
                        <span class="date Mẹo">22.12.2017</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/meo/bi-quyet-cuoi-nam-long-lay-khi-vi-tien-khong-day">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/mau-tui-xach-giay-moi-bst-juno-queens-of-fashion-2017-2018__43__ecf11eb66af24becb3aa9bd8d70ebe9b_grande.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/meo/bi-quyet-cuoi-nam-long-lay-khi-vi-tien-khong-day">Bí quyết đầu năm lộng lẫy khi ví tiền không dày</a>
                        </h3>
                        <span class="date Mẹo">21.12.2017</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/meo/dang-thon-trong-tam-tay-chi-can-ap-dung-ngay-3-buoc">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/giay-tui-juno__42__1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/meo/dang-thon-trong-tam-tay-chi-can-ap-dung-ngay-3-buoc">Dáng thon trong tầm tay chỉ cần áp dụng ngay 3 bước</a>
                        </h3>
                        <span class="date Mẹo">31.08.2017</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="blog-insert-item">
               <div class="row">
                  <div class="content col-xs-5 col-sm-3">
                     Juno Sneaker Collections <br/>
                     Bộ sưu tập đề cao tinh thần thoải mái với thiết kế giày sneaker êm ái, áp dụng nhiều cải tiến về chất liệu cũng như kiểu dáng, màu sắc thời trang. Theo đó Juno mong muốn được đồng hành cùng các nàng tự tin sải bước trong mọi hoàn cảnh, được thoải mái làm những điều mình thích. Đi Juno sneaker nàng sẽ luôn là chính mình #Beyou.
                  </div>
                  <div class="col-xs-5 col-sm-7">
                     <div id="owl-insert-slider" class="owl-theme owl-carousel">
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__13__9936707afc5d439e86da63f38964b989.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__13__9936707afc5d439e86da63f38964b989_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__18__ea9beea0730d4ebab96de0e4c16781bb.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__18__ea9beea0730d4ebab96de0e4c16781bb_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__22__8202a38bfd544a2495af1d5594ac7555.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__22__8202a38bfd544a2495af1d5594ac7555_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__21__c2581aaab637455099c4611445a97dfb.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__21__c2581aaab637455099c4611445a97dfb_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__14__b8f5dcc2b25a402d8a41951e7376f21a.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__14__b8f5dcc2b25a402d8a41951e7376f21a_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__17__296f9670acb14660925f5c7e772f8316.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__17__296f9670acb14660925f5c7e772f8316_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__12__320314f3fbcb45c89a2185041ed2faed.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__12__320314f3fbcb45c89a2185041ed2faed_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__15__e8a2655c512a42b5b00ce82b0d96a4a2.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__15__e8a2655c512a42b5b00ce82b0d96a4a2_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__16__f4daad34097a41ed9cd89626a91f8530.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__16__f4daad34097a41ed9cd89626a91f8530_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__19__9699557ff8b14d6ea81630a9c1f3f600.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__19__9699557ff8b14d6ea81630a9c1f3f600_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__20__0aff525d135b4668a3cfc59d5e5f4261.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__20__0aff525d135b4668a3cfc59d5e5f4261_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__23__675d9da7cb6747189ffe3a3869360023.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__23__675d9da7cb6747189ffe3a3869360023_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__33__64f23b17cf934ef7b17e736d6f265bd8.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__33__64f23b17cf934ef7b17e736d6f265bd8_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__1__23c49f3bca3d4dcebe1168eb5b112a6b.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__1__23c49f3bca3d4dcebe1168eb5b112a6b_large.jpg" alt="">
                           </a>
                        </div>
                        <div class="owl-item">
                           <a href="https://file.hstatic.net/1000003969/file/juno-sneaker-collections__29__274fb583c00b49a3ad71f849b710fb1f.jpg" class="fancybox" rel="galary" caption="">
                           <img src="//file.hstatic.net/1000003969/file/juno-sneaker-collections__29__274fb583c00b49a3ad71f849b710fb1f_large.jpg" alt="">
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <script>
               $(document).ready(function(){
               
               	$("#owl-insert-slider .fancybox").fancybox({
               		'transitionIn'	:	'elastic',
               		'transitionOut'	:	'elastic',
               		'speedIn'		:	600, 
               		'speedOut'		:	200, 
               		'overlayShow'	:	false,
               		'hideOnContentClick': true,
               		beforeLoad: function() {
               			this.title = $(this.element).attr('caption');
               		}
               	});
               })
            </script>
            <div class="blog-category-items">
               <h3 class="category-title">
                  <a href="/blogs/ngon">Ngon</a>
               </h3>
               <div class="items-doc">
                  <div class="row">
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/ngon/nhung-mon-ngon-duoc-san-don-nhat-khu-thuong-mai-takashiyama">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/9/0c3aa98ae049c1/gianglinhhh-1470485797456_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/ngon/nhung-mon-ngon-duoc-san-don-nhat-khu-thuong-mai-takashiyama">Những món ngon được “săn đón” nhất khu thương mại Takashiyama</a>
                        </h3>
                        <span class="date Ngon">13.08.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/ngon/vi-sao-giam-can-luon-gan-lien-voi-salad">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//hstatic.net/969/1000003969/10/2016/8-5/vi-sao-giam-can-lai-can-salad__15_.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/ngon/vi-sao-giam-can-luon-gan-lien-voi-salad">Vì sao giảm cân luôn “gắn liền” với salad?</a>
                        </h3>
                        <span class="date Ngon">05.08.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/ngon/mat-ruoi-mua-he-voi-noi-canh-kho-qua">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//hstatic.net/969/1000003969/10/2016/8-1/canh-kho-qua_30e09e1f-e4f8-4bdc-6acb-82c7b86d41b9.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/ngon/mat-ruoi-mua-he-voi-noi-canh-kho-qua">Mát rượi mùa Hè với nồi canh Khổ Qua</a>
                        </h3>
                        <span class="date Ngon">01.08.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/ngon/an-the-nao-de-luon-vui-khoe">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/10/0c3c0c8955e9f6/0_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/ngon/an-the-nao-de-luon-vui-khoe">Ăn thế nào để luôn vui khoẻ?</a>
                        </h3>
                        <span class="date Ngon">29.07.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/ngon/thuc-pham-huu-co-xu-huong-vua-ngon-vua-khoe">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//hstatic.net/969/1000003969/10/2016/7-28/w620h405f1c1-files-articles-2016-1096254-organic-1.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/ngon/thuc-pham-huu-co-xu-huong-vua-ngon-vua-khoe">Thực phẩm hữu cơ – Xu hướng vừa ngon vừa khoẻ</a>
                        </h3>
                        <span class="date Ngon">28.07.2016</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="blog-category-items">
               <h3 class="category-title">
                  <a href="/blogs/em">Em</a>
               </h3>
               <div class="items-doc">
                  <div class="row">
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/em/mua-gi-tang-chinh-minh-ngay-valentine">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//file.hstatic.net/1000003969/file/img_0191_e10ebc2bac3244e18dba8bd65a23a4b7_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/em/mua-gi-tang-chinh-minh-ngay-valentine">Mua gì tặng chính mình ngày Valentine?</a>
                        </h3>
                        <span class="date Em">13.02.2017</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/em/ve-sexy-cua-mot-co-gai-tu-dau-ma-den">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/6/08a9014570e839/99fd8e143f_1024x1024.jpeg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/em/ve-sexy-cua-mot-co-gai-tu-dau-ma-den">Vẻ sexy của một cô gái từ đâu mà đến?</a>
                        </h3>
                        <span class="date Em">27.09.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/em/nhung-cach-don-gian-ho-bien-cho-can-phong-cua-nang">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/6/036b44f4af4ba8/4d54f142161bea3cf4eafc80aeab1900_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/em/nhung-cach-don-gian-ho-bien-cho-can-phong-cua-nang">Những cách đơn giản “hô biến” cho căn phòng của nàng</a>
                        </h3>
                        <span class="date Em">16.09.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/em/bi-kip-chup-anh-dep-nhu-fashionista-nhan-ngay-quoc-te-chup-anh">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/2/024f17b0d6a91f/img_1146_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/em/bi-kip-chup-anh-dep-nhu-fashionista-nhan-ngay-quoc-te-chup-anh">Bí kíp chụp ảnh đẹp như fashionista nhân ngày “quốc tế chụp ảnh”</a>
                        </h3>
                        <span class="date Em">14.09.2016</span>
                     </div>
                     <div class="col-xs-5 col-sm-25 col-md-2">
                        <a class="ClickBlogItem" href="/blogs/em/vi-sao-con-gai-cu-mua-moi-la-mua-sam">
                           <div class="item-img ClickBlogItem" style="height:305px !important;background: url('//sw001.hstatic.net/5/01b65da1e9b476/f3_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                        <h3 class="ClickBlogItem">
                           <a class="ClickBlogItem" href="/blogs/em/vi-sao-con-gai-cu-mua-moi-la-mua-sam">Vì sao con gái cứ mùa mới là mua sắm?</a>
                        </h3>
                        <span class="date Em">10.09.2016</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="blog-category-items">
               <h3 class="category-title">
                  <a href="/blogs/song">Sống</a>
               </h3>
               <div class="items-ngang">
                  <div class="row">
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="info pull-left">
                           <h3>
                              <a href="/blogs/song/4-su-that-tao-nen-trao-luu-tang-giay-ngay-8-3">4 Sự thật tạo nên trào lưu tặng giày ngày 8/3</a>
                           </h3>
                           <span class="date">28.02/2017</span>
                           <p class="des">
                              8/3 năm nay, phái mạnh chẳng còn đau đầu nghĩ quà nữa, bởi chị em đã thỏ thẻ bên tai điều nàng muốn - một đôi giày đúng ý, vừa size.
                           </p>
                           <div class="share">
                              <div class="link-icon clearfix">
                                 <div class="social-wrap 1000325795-art">
                                    <div class="fb-like" data-href="https://juno.vn/blogs/song/4-su-that-tao-nen-trao-luu-tang-giay-ngay-8-3" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                    <div id="fb-root" class="hidden"></div>
                                    <script>
                                       (function(d, s, id) {
                                       	var js, fjs = d.getElementsByTagName(s)[0];
                                       	if (d.getElementById(id)) return;
                                       	js = d.createElement(s); js.id = id;
                                       	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                       	fjs.parentNode.insertBefore(js, fjs);
                                       }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                 </div>
                              </div>
                              <div class="content">
                                 <ul class="icon-share-share">
                                    <li class="facebook">
                                       <a href="http://www.facebook.com/share.php?v=4&src=bm&u=https://juno.vn/blogs/song/4-su-that-tao-nen-trao-luu-tang-giay-ngay-8-3&t=4 Sự thật tạo nên trào lưu tặng giày ngày 8/3" 
                                          onclick="window.open(this.href,'sharer','toolbar=0,status=0,width=626,height=436');   return false;" 
                                          rel="nofollow" title="Share this on Facebook"></a>
                                    </li>
                                    <li class="google">
                                       <a href="https://plus.google.com/share?url=https://juno.vn/blogs/song/4-su-that-tao-nen-trao-luu-tang-giay-ngay-8-3" 
                                          onclick="javascript:window.open(this.href,'  ','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                          rel="nofollow" title="Share this on Google+"></a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <a href="/blogs/song/4-su-that-tao-nen-trao-luu-tang-giay-ngay-8-3">
                           <div class="item-img pull-right" style="background: url('//file.hstatic.net/1000003969/file/09_01a90cd9aa404d39a54352eadae8799a_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                           </div>
                        </a>
                     </div>
                     <div class="col-xs-10 col-sm-5 clearfix">
                        <div class="clearfix blog-read-item special">
                           <a href="/blogs/song/toi-mua-om-ap-roi-dua-nhau-di-tron-thoi">
                              <div class="item-img pull-left" style="background: url('//file.hstatic.net/1000003969/file/dung_1024x1024.jpg') no-repeat;background-size: cover;background-position: center;">
                              </div>
                           </a>
                           <div class="caption pull-right" >
                              <h3 class="title"><a href="/blogs/song/toi-mua-om-ap-roi-dua-nhau-di-tron-thoi">Tới “mùa ôm ấp” rồi, đưa nhau đi trốn thôi!</a></h3>
                              <span class="date">05.11/2016</span>
                              <p class="more">Dù FA hay đã có đôi có cặp cũng không nên bỏ qua những cái ôm trong thời tiết đẹp thế này. Lên đường thôi các nàng ạ!</p>
                           </div>
                        </div>
                        <div class="clearfix blog-read-item special">
                           <a href="/blogs/song/hoa-nhap-o-cong-ty-moi-khong-he-kho-voi-5-cach-nay">
                              <div class="item-img pull-left" style="background: url('//sw001.hstatic.net/6/06ce0c5cf72f58/popofcolorblue_1024x1024.png') no-repeat;background-size: cover;background-position: center;">
                              </div>
                           </a>
                           <div class="caption pull-right" >
                              <h3 class="title"><a href="/blogs/song/hoa-nhap-o-cong-ty-moi-khong-he-kho-voi-5-cach-nay">Hoà nhập ở công ty mới không hề khó với 5 cách này!</a></h3>
                              <span class="date">23.09/2016</span>
                              <p class="more">Làm sao để hoà nhập với những người nàng chưa hề quen biết? Hãy cùng tham khảo 5 cách siêu đơn giản dưới đây nhé!</p>
                           </div>
                        </div>
                        <div class="clearfix blog-read-item special">
                           <a href="/blogs/song/nhung-dieu-nen-lam-trong-100-ngay-cuoi-cung-de-co-mot-nam-that-thu-vi">
                              <div class="item-img pull-left" style="background: url('//sw001.hstatic.net/6/05f3daa3b7f1d5/di-du-lich_1024x1024.png') no-repeat;background-size: cover;background-position: center;">
                              </div>
                           </a>
                           <div class="caption pull-right" >
                              <h3 class="title"><a href="/blogs/song/nhung-dieu-nen-lam-trong-100-ngay-cuoi-cung-de-co-mot-nam-that-thu-vi">Những điều nên làm trong 100 ngày cuối cùng để có một năm thật thú vị</a></h3>
                              <span class="date">22.09/2016</span>
                              <p class="more">Vậy là chỉ còn 100 ngày nữa là đã kết thúc năm 2016 rồi các nàng ạ! Juno hy vọng nàng đã có một năm thật thú vị!</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <a rel="nofollow" title="Về đầu trang" id="back-top" href="javascript:void(0)" class="seoquake-nofollow"><i class="fa fa-caret-up"></i> Về đầu trang</a>
      <footer>
         <div class='footer'>
            <div class='container'>
               <div class='menu-foot hidden-xs'>
                  <ul>
                     <li><a href="/blogs/mot">Mốt</a></li>
                     <li><a href="/blogs/dep">Đẹp</a></li>
                     <li><a href="/blogs/khoe">Khoẻ</a></li>
                     <li><a href="/blogs/meo">Mẹo</a></li>
                     <li><a href="/blogs/ngon">Ngon</a></li>
                     <li><a href="/blogs/em">Em</a></li>
                     <li><a href="/blogs/song">Sống</a></li>
                  </ul>
                  <a title="Trang shop JUNO" id="home-foot" href="/" target="_blank" ><i class="fa fa-home"></i> Trang shop JUNO</a>
               </div>
               <div class='row foot'>
                  <div class='col-md-5 col-sm-10 col-xs-10'>
                     <div class='files'>
                        <ul>
                           <li><a href="/pages/gioi-thieu">Giới thiệu</a></li>
                           <li><a href="/pages/lien-he">Liên hệ</a></li>
                           <li><a href="/sitemap.xml">Site map</a></li>
                        </ul>
                     </div>
                     <div class='coppyright'>
                        <p>
                           @2015 Công ty cổ phần sản xuất thương mại dịch vụ JUNO
                        </p>
                        <p>
                           Địa chỉ văn phòng chính: 313 Nguyễn Thị Thập, Q.7, TP.HCM
                        </p>
                     </div>
                  </div>
                  <div class='col-md-4 col-sm-10 col-xs-10 pull-right'>
                     <div class='form-mail'>
                        <p>
                           Hãy đăng ký mail của bạn để chúng tôi gửi những thông tin cập nhật mới nhất
                        </p>
                        <form accept-charset='UTF-8' action='/account/contact' class='contact-form' method='post'>
                           <input name='form_type' type='hidden' value='customer'>
                           <input name='utf8' type='hidden' value='✓'>
                           <input type="hidden" id="contact_tags" name="contact[tags]" value="khách hàng tiềm năng, bản tin" />     
                           <input name="contact[email]" type="email" id="contact_email" placeholder="Nhập email của bạn" required="required" />
                           <input type='submit' value=''>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class='link'>
            <div class='container'>
               <div class='link-top'>
                  <ul>
                     <li class='fa'><a href='#'><img src='//hstatic.net/288/1000046288/1000069273/face.jpg?v=72' alt=''  /></a></li>
                     <li class='tt'><a href='#'><img src='//hstatic.net/288/1000046288/1000069273/tt.jpg?v=72' alt=''  /></a></li>
                     <li class='gg'><a href='#'><img src='//hstatic.net/288/1000046288/1000069273/gg.jpg?v=72' alt=''  /></a></li>
                     <li class='yt'><a href='#'><img src='//hstatic.net/288/1000046288/1000069273/yt.jpg?v=72' alt=''  /></a></li>
                  </ul>
               </div>
               <div class='link-bottom'>
                  <ul>
                     <li><a href='/collections/giay-xang-dan'>Giầy Xăng Đan</a></li>
                     <li><a href='/collections/giay-bup-be'>Giày Búp Bê</a></li>
                     <li><a href='/collections/giay-cao-got'>Giày Cao Gót</a></li>
                     <li><a href='/collections/giay-boots'>Giày Boots</a></li>
                     <li><a href='/collections/tui-xach'>Túi Xách</a></li>
                     <li><a href='/collections/that-lung'>Phụ Kiện</a></li>
                     <li><a href='/collections/clear-stock'>Clearstock</a></li>
                     <li><a href='/collections/san-pham-moi-nhat'>Sản Phẩm Mới</a></li>
                     <li><a href='/collections/san-pham-top'>Sản Phẩm Top</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </footer>
      <script>
         $(window).load(function() {
         	$('.cms-index-index').removeClass('bodyOnload');
         })
         $(document).ready(function(){
         	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
         			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
         			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
         																$.src="//v2.zopim.com/?2WPFlV9CV9qns47JPZwbcbX7TKng4VxH";z.t=+new Date;$.
         																type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
         	if(window.location.href=='//juno.vn/black-friday')
         	{
         
         		window.location = "/pages/black-friday";
         	}
         	$("#owl-blog-slider").owlCarousel({
         		pagination : true,
         		navigation : true,
         		navigationText:["<i class=\"insert-left\"><\/i>","<i class=\"insert-right\"><\/i>"],
         		autoPlay: 8000,
         		items :1,
         		itemsDesktop : [1024,1], 
         		itemsDesktopSmall : [967,1], 
         		itemsTablet: [600,1], 
         	});
         	$('#owl-insert-slider').owlCarousel({
         		pagination : false,
         		navigation : true,
         		items :4,
         		navigationText:["<i class=\"insert-left\"><\/i>","<i class=\"insert-right\"><\/i>"],
         		itemsDesktop : [1024,3], 
         		itemsDesktopSmall : [967,2], 
         		itemsTablet: [600,1], 
         		itemsMobile : [600,1]
         	});
         	var maxheight=200;
         	$('.list-r > .row > .one-r').each(function(){
         		if($(this).height() > maxheight)
         		{
         			maxheight=$(this).height();
         		}
         	});
         	$('.one-r').css("height",maxheight);
         	var heightbody=$(window).height()-$('.footer').height();
         	var heightfooter=$('footer').height();
         	$(window).scroll(function(){
         		if( $(window).scrollTop() < 200  ) {
         			$('#back-top').stop(false,true).fadeOut(600);
         		}
         		else{
         			$('#back-top').stop(false,true).fadeIn(600);
         		}
         		if($(document).height() - $(window).scrollTop() - $(window).height() < $('footer').height() - 30 ){
         			$('#back-top').css('bottom',$('footer').height() - 17);
         		}else{
         			$('#back-top').css('bottom', '45px');
         		}
         	});
         	$('#back-top').click(function(){
         		$('body,html').animate({scrollTop:0},600);
         		return false;
         	});
         	$('.wrapper').on("click",".share .share-icon",function(){
         		$(this).parent().next('.content').slideToggle(500);
         	})
         	$('.topsearch').each(function(){
         		$(this).submit(function() {
         			window.location = '/search?type=article&q=filter=((blogid:article=-1000009307)(title:article**' + $('input[name=q]', this).val() +')||(body:article**'+$('input[name=q]', this).val()+'))&view=articles';
         			return false;
         		});
         	});
         })
      </script>
      <script>
         var check_drop = true;
         timeOutMenu = setTimeout(function(){
         	if (check_drop) {
         		$('.drop-menu').stop().slideUp(200);
         		//$(this).removeClass('active');
         		//$(this).parent('li').removeClass('drop').children('.drop-menu').stop().slideUp(200);
         	}
         },500);
         $('.menu-top > li > a').hover(
         	function(){
         		clearTimeout(timeOutMenu);
         		$('.menu-top > li > a:not(.main)').removeClass('active');
         		$(this).addClass('active');
         		$('.drop-menu').stop().slideUp(200);
         		$(this).parent('li').addClass('drop').children('.drop-menu').slideDown(200);		
         	},
         	function(){
         		if ( $('.menu-top > li > a.main').length == 0 && $(this).parent('li.hasChild').length == 0 ) {
         			$(this).removeClass('active');
         		}
         		if ( $(this).parent('li:not(.hasChild)').length == 1 && $('.menu-top > li > a.main').length == 1 ) {
         			$(this).not('.main').removeClass('active');
         		}
         	}
         );
         $('.drop-menu').hover(
         	function() {
         		check_drop = false;
         	},
         	function() {
         		if ( $(this).parent('li').children('a:not(.main)') ) {
         			$(this).parent('li').children('a:not(.main)').removeClass('active');
         		}
         		check_drop = true;
         		$(this).stop().slideUp(200);
         	}
         );
      </script>
      <script async>
         /* <![CDATA[ */
         var google_conversion_id = 956357071;
         var google_custom_params = window.google_tag_params;
         var google_remarketing_only = true;
         /* ]]> */
      </script>
      <noscript>
         <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/956357071/?value=0&amp;guid=ON&amp;script=0"/>
         </div>
      </noscript>
      <!-- Google Tag Manager -->
      <noscript>
         <iframe src="//www.googletagmanager.com/ns.html?id=GTM-WM3J7N"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
      </noscript>
      <script async>
         (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-WM3J7N');
      </script>
      <!-- End Google Tag Manager -->
   </body>
</html>