 <div class="col-md-2 col-sm-3 col-6 item-mobile product-wrapper">
                     <div class="product-information">
                        <div class="product-detail product-detail-TXN039 height-index-1 background-size">
                           <div class="hidden no-tag-bottom hidden"></div>
                           <a href="/products/vi-cam-tay-clutch-winter-elegance-txn039?productid=1003737295&amp;view=product&amp;handle=vi-cam-tay-clutch-winter-elegance-txn039" target="_blank" title="Clutch cầm tay phong thư TXN039">
                              <div class="product-image" style="position:relative;overflow:hidden;"><img class="main-image image-default lazy" width="240" height="" alt="Clutch cầm tay phong thư TXN039" src="https://product.hstatic.net/1000003969/product/nau_txn039_1_medium.jpg" style="display: block;"><img class="image-hover lazy" width="240" height="" alt="Clutch cầm tay phong thư TXN039" src="https://product.hstatic.net/1000003969/product/nau_txn039_2_medium.jpg" style="display: block;"></div>
                           </a>
                           <div class="product-info">
                              <div class="hidden slch text-center col-xs-10 no-padding" style="opacity:0;height:30px;overflow:hidden"></div>
                              <div class="rating-box col-xs-10 no-padding">
                                 <ul class="text-center point-star">
                                    <li class="active 1"><i class="fa fa-star"></i></li>
                                    <li class="active 2"><i class="fa fa-star"></i></li>
                                    <li class="active 3"><i class="fa fa-star"></i></li>
                                    <li class="active 4"><i class="fa fa-star"></i></li>
                                    <li class="active 5"><i class="fa fa-star"></i></li>
                                 </ul>
                              </div>
                              <div class="title-sku">
                                 <a href="/products/vi-cam-tay-clutch-winter-elegance-txn039?productid=1003737295&amp;view=product&amp;handle=vi-cam-tay-clutch-winter-elegance-txn039" target="_blank" title="Clutch cầm tay phong thư TXN039">
                                    <h2>TXN039</h2>
                                 </a>
                              </div>
                              <div class="price-info clearfix">
                                 <div class="price-new-old"><span>410,000<sup>đ</sup></span></div>
                              </div>
                              <ul class="color-swatches">
                                 <li title="Nâu" class="color-item active" data-img="https://product.hstatic.net/1000003969/product/nau_txn039_1_medium.jpg" data-img-hover="https://product.hstatic.net/1000003969/product/nau_txn039_2.jpg"><img class="lazy" src="https://product.hstatic.net/1000003969/product/nau_txn039_1_medium.jpg" style="display: block;"></li>
                                 <li title="Trắng" class="color-item" data-img="https://product.hstatic.net/1000003969/product/trang_txn039_1_medium.jpg" data-img-hover="https://product.hstatic.net/1000003969/product/trang_txn039_2.jpg"><img class="lazy" src="https://product.hstatic.net/1000003969/product/trang_txn039_1_medium.jpg" style="display: block;"></li>
                                 <li title="Xanh lá" class="color-item" data-img="https://product.hstatic.net/1000003969/product/xanh-la_txn039_3_medium.jpg" data-img-hover="https://product.hstatic.net/1000003969/product/xanh-la_txn039_1.jpg"><img class="lazy" src="https://product.hstatic.net/1000003969/product/xanh-la_txn039_3_medium.jpg" style="display: block;"></li>
                              </ul>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>