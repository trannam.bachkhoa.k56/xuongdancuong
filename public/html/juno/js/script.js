$(".coll-products.cart-related-slider").owlCarousel({items:4,loop:false,autoPlay:false,navigation:true,navigationText:["",""],itemsDesktop:[1199,3],itemsDesktopSmall:[992,3],itemsTablet:[768,2],itemsTabletSmall:false,itemsMobile:[0,2]});$('form.search').submit(function(event){event.preventDefault();window.location="/search?q="+encodeURIComponent("filter=((title:product**"+$(this).find('input[name=q]').val()+")&&!(tag:product**chi nhanh))");})
$('#menu-toggle').click(function(e){
	e.stopPropagation();
	$('#sidebar-wrapper,#wrapper').toggleClass('toggled');
	$('html,body').toggleClass('watch_menu');
});
$('#hide-menu').click(function(e){e.stopPropagation();});
$('body,html').click(function(e){
	$('#sidebar-wrapper,#wrapper').removeClass('toggled');
	$('html,body').removeClass('watch_menu');
});
$('.sidebar-nav>li').click(function(){$('.sub-menu').removeClass('active');
																			$('.sub-menu',$(this)).addClass('active');}).find('ul').click(function(e){e.stopPropagation();});;
$('.sub-menu>li:first-child').click(function(){$(this).closest('.sub-menu').removeClass('active');});
$('.menu-top > li > a').hover(function(){});$('.drop-menu').hover(function(){check_drop=false;},function(){if($(this).parent('li').children('a:not(.main)')){$(this).parent('li').children('a:not(.main)').removeClass('active');}																																																	 check_drop=true;$(this).stop().slideUp(200);});
/*function openZim(){$zopim.livechat.window.show();*/
jQuery(document).on("click",".back-to-top",function(){jQuery(this).removeClass('display');jQuery('html, body').animate({scrollTop:0},600);});
jQuery(window).on("scroll",function(){
	if( jQuery('.back-to-top').length > 0 && jQuery(window).scrollTop() > 200 ){
		jQuery('.back-to-top').addClass('display');}
	else{
		jQuery('.back-to-top').removeClass('display');
	}
});
function handleOutboundLinkClicks(label){ga('send','event',{'eventCategory':'Category','eventAction':'Mua nhanh','eventLabel':label});}
$('body').on('click','.quick-view',function(e){var title=$(this).data('title');handleOutboundLinkClicks(title);e.preventDefault();var handle=$(this).data('handle');ga('ec:setAction','detail');$.ajax({url:'/products/'+handle+'?view=quickview',success:function(data){$('#quickView .modal-dialog').html("");$('#quickView .modal-dialog').html(data);}})});$('body').on('click','.quick-view .close',function(){$('#quickView').modal('hide');$('#quickView').find('.modal-dialog').html();});
function log(args) {
	var str = "";
	for (var i = 0; i < arguments.length; i++) {
		if (typeof arguments[i] === "object") {
			str += JSON.stringify(arguments[i]);
		} else {
			str += arguments[i];
		}
	}
	return str;
}
function addCommas(str) {
	var parts = (str + "").split("."),
			main = parts[0],
			len = main.length,
			output = "",
			i = len - 1;

	while(i >= 0) {
		output = main.charAt(i) + output;
		if ((len - i) % 3 === 0 && i > 0) {
			output = "," + output;
		}
		--i;
	}
	// put decimal part back
	if (parts.length > 1) {
		output += "," + parts[1];
	}
	return output;
}
function getCartModal(){
	var cart = null;
	jQuery('#cartform').hide();
	jQuery('#myCart #exampleModalLabel').text("Giỏ hàng");
	jQuery.getJSON('/cart.js', function(cart, textStatus) {
		if(cart) {
			jQuery('#cartform').show();
			jQuery('.line-item:not(.original)').remove();
			jQuery.each(cart.items,function(i,item){
				var total_line = 0;
				var total_line = item.quantity * item.price;
				var price_original = item.price_original;
				tr = jQuery('.original').clone().removeClass('original').appendTo('table#cart-table tbody');
				if(item.image != null)
					tr.find('.item-image').html("<img src=" + Haravan.resizeImage(item.image,'small') + ">");
				else
					tr.find('.item-image').html("<img src='//hstatic.net/0/0/global/design/theme-default/no-image.png'>");
				vt = item.variant_options;
				if(vt.indexOf('Default Title') != -1)
					vt = '';
				tr.find('.item-title').children('a').html(item.product_title + '<br><span>' + vt + '</span>').attr('href', item.url);
				tr.find('.item-quantity').html("<input id='quantity1' name='updates[]' min='1' disabled='disabled' max='2' type='number' value=" + item.quantity + " class='' />");
				tr.find('.item-price').html( '<p class="main-price">'+log(addCommas(total_line / 100)) + '<sup>đ</sup></p><p class="price_original-price">'+log(addCommas(price_original / 100))+'<sup>đ</sup></p>' );
				tr.find('.item-delete').html("<a href='javascript:void(0);' onclick='deleteCart(" + item.variant_id + ")' ><i class='fa fa-times'></i></a>");
			});
			jQuery('.item-total,.cart-total-price .price').html(log(addCommas(cart.total_price / 100)) + 'đ');
			jQuery('.modal-title').children('b').html(cart.item_count);
			jQuery('.cart-number').html(cart.item_count );
			if(cart.item_count == 0){				
				jQuery('#exampleModalLabel').html('Giỏ hàng của bạn đang trống. Mời bạn tiếp tục mua hàng.');
				jQuery('#cart-view').html('<tr><td>Hiện chưa có sản phẩm</td></tr>');
				jQuery('#cartform').hide();
			}
			else{			
				jQuery('#exampleModalLabel').html('Bạn có ' + cart.item_count + ' sản phẩm trong giỏ hàng.');
				jQuery('#cartform').removeClass('hidden');
				jQuery('#cart-view').html('');
			}
			if ( jQuery('#cart-pos-product').length > 0 ) {
				jQuery('#cart-pos-product span').html(cart.item_count + ' sản phẩm');
			}
			// Get product for cart view
			jQuery.each(cart.items,function(i,item){
				clone_item(item);
			});
			jQuery('#total-view-cart').html(log(addCommas(cart.total_price / 100)) + 'đ');
		}
		else{
			jQuery('#exampleModalLabel').html('Giỏ hàng của bạn đang trống. Mời bạn tiếp tục mua hàng.');
			if ( jQuery('#cart-pos-product').length > 0 ) {
				jQuery('#cart-pos-product span').html(cart.item_count + ' sản phẩm');
			}
			jQuery('#cart-view').html('<tr><td>Hiện chưa có sản phẩm</td></tr>');
			jQuery('#cartform').hide();
		}
	});
}
function clone_item(product){
	var item_product = jQuery('#clone-item-cart').find('.item_2');
	if ( product.image == null ) {
		item_product.find('img').attr('src','//hstatic.net/0/0/global/design/theme-default/no-image.png').attr('alt', product.url);
	} else {
		item_product.find('img').attr('src',Haravan.resizeImage(product.image,'small')).attr('alt', product.url);
	}
	item_product.find('a:not(.remove-cart)').attr('href', product.url).attr('title', product.url);
	item_product.find('.pro-title-view').html(product.title);
	item_product.find('.pro-quantity-view').html(product.quantity);
	item_product.find('.pro-price-view').html(log(addCommas(product.price / 100)) + 'đ');
	item_product.find('.remove-cart').html("<a href='javascript:void(0);' onclick='deleteCart(" + product.variant_id + ")' ><i class='fa fa-times'></i></a>");
	var title = '';
	if(product.variant_options.indexOf('Default Title') == -1){
		$.each(product.variant_options,function(i,v){
			title = title + v + ' / ';
		});
		title = title + '@@';
		title = title.replace(' / @@','')
		item_product.find('.variant').html(title);
	}else {
		item_product.find('.variant').html('');
	}
	item_product.clone().removeClass('hidden').prependTo('#cart-view');
}
function deleteCart(variant_id){
	var params = {
		type: 'POST',
		url: '/cart/change.js',
		data: 'quantity=0&id=' + variant_id,
		dataType: 'json',
		success: function(cart) {
			getCartModal();
		},
		error: function(XMLHttpRequest, textStatus) {
			Haravan.onError(XMLHttpRequest, textStatus);
		}
	};
	jQuery.ajax(params);
}
jQuery(document).on("click","#update-cart-modal",function(event){
	event.preventDefault();
	if (jQuery('#cartform').serialize().length <= 5) return;
	jQuery(this).html('Đang cập nhật');
	var params = {
		type: 'POST',
		url: '/cart/update.js',
		data: jQuery('#cartform').serialize(),
		dataType: 'json',
		success: function(cart) {
			if ((typeof callback) === 'function') {
				callback(cart);
			} else {
				getCartModal();
			}
			jQuery('#update-cart-modal').html('Cập nhật');
		},
		error: function(XMLHttpRequest, textStatus) {
			Haravan.onError(XMLHttpRequest, textStatus);
		}
	};
	jQuery.ajax(params);
}); 
function timeout_notification(){
	setTimeout(function(){
		jQuery('.noti_itemcount').removeClass('active').addClass('unactive')
	},5000)
}
jQuery(document).on("click","#checkout",function(event){
	event.preventDefault();
	jQuery.getJSON('/cart.js', function(cart, textStatus) {
		if(cart.item_count > 2){
			jQuery('.noti_itemcount').removeClass('unactive').addClass('active').html('<i class="fa fa-times-circle-o" aria-hidden="true"></i> Mỗi đơn hàng chỉ được mua 2 sản phẩm. Nếu mua thêm sản phẩm, vui lòng tạo thêm đơn hàng mới hoặc gọi tổng đài <strong>1800 1162</strong> để đặt hàng nhanh hơn!')
		} else {
			/*jQuery('.noti_itemcount').removeClass('unactive').addClass('active').html('<i class="fa fa-check-circle" aria-hidden="true"></i> Thanh toán thành công ...')*/
			window.location = "/checkout";
		}
		timeout_notification();
	});
})

$(document).ready(function(){ 
	$.getJSON('/cart.js', function(cart, textStatus) {
		if(cart) {
			$('.cart-total-price .price').html( cart.item_count+' sản phẩm');
			//console.log('load cart success');
		}
	})
})