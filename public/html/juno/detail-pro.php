<!DOCTYPE HTML>
<html>
   <head>
      <meta property="fb:pages" content="172922056238158" />
      <meta name="adx:sections" content="https://juno.vn/products/tui-xach-trung-txt123" />
      <meta name="p:domain_verify" content="50a57bef3e9a4ae42fbcd722c7074695"/>
      <meta name="google-site-verification" content="mr8z4Wdem8xgXrWSm1FLf8g96FRSgKwDdOyJ5JEpBmQ" />
      <meta name="google-site-verification" content="U_iG9lyKYTTeLB4qx3QlYZpTxpsO1g0uQYn6QWku-sM" />
      <meta http-equiv="content-type" content="ThemeSyntaxError" />
      <meta charset="utf-8" />
      <title>
         Túi xách trung khóa bấm kim loại hình tam giác TXT123
      </title>
       <base href="http://vn3c.net/public/html/juno/" target="">
      <meta name="description" content="Túi xách trung khóa bấm kim loại hình tam giác TXT123 thiết kế đơn giản, tiện dụng nhưng vẫn thời trang mang lại nét nổi bật cho bạn khi diện, tui xach trung" />
      <meta property="og:type" content="product">
      <meta property="og:title" content="T&#250;i x&#225;ch trung kh&#243;a bấm kim loại h&#236;nh tam gi&#225;c TXT123">
      <meta property="og:image" content="http://product.hstatic.net/1000003969/product/xanh-duong_txt123_1_medium.jpg">
      <meta property="og:image:secure_url" content="https://product.hstatic.net/1000003969/product/xanh-duong_txt123_1_medium.jpg">
      <meta property="og:image" content="http://product.hstatic.net/1000003969/product/xanh-duong_txt123_2_medium.jpg">
      <meta property="og:image:secure_url" content="https://product.hstatic.net/1000003969/product/xanh-duong_txt123_2_medium.jpg">
      <meta property="og:image" content="http://product.hstatic.net/1000003969/product/xanh-duong_txt123_3_medium.jpg">
      <meta property="og:image:secure_url" content="https://product.hstatic.net/1000003969/product/xanh-duong_txt123_3_medium.jpg">
      <meta property="og:price:amount" content="750000">
      <meta property="og:price:currency" content="VND">
      <meta property="og:description" content="T&#250;i x&#225;ch trung kh&#243;a bấm kim loại h&#236;nh tam gi&#225;c TXT123 thiết kế đơn giản, tiện dụng nhưng vẫn thời trang mang lại n&#233;t nổi bật cho bạn khi diện, tui xach trung">
      <meta property="og:url" content="https://juno.vn/products/tui-xach-trung-txt123">
      <meta property="og:site_name" content="">
      <link rel="canonical" href="https://juno.vn/products/tui-xach-trung-txt123" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0" />
      <link rel="shortcut icon" type="image/png" href="//theme.hstatic.net/1000003969/1000323463/14/favicon.png?v=2220" />

       <link href='css/font-awesome.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/font-awesome-animation.min.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/juno.min.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/popup-cart.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.carousel.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.theme.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.transitions.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/filter.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/styles.css' rel='stylesheet' type='text/css'  media='all'  />


      
      <script type='text/javascript'>
         //<![CDATA[
         if ((typeof Haravan) === 'undefined') {
           Haravan = {};
         }
         Haravan.culture = 'vi-VN';
         Haravan.shop = 'juno-1.myharavan.com';
         Haravan.theme = {"name":"desktop_by_nhan(uncompress)","id":1000323463,"role":"main"};
         Haravan.domain = 'juno.vn';
         //]]>
      </script>
      <script>
         //<![CDATA[
         (function() { function asyncLoad() { var urls = ["https://buyxgety.haravan.com/js/script_tag_production.js","https://combo.haravan.com/js/script_tag_production.js"];for (var i = 0; i < urls.length; i++) {var s = document.createElement('script');s.type = 'text/javascript';s.async = true;s.src = urls[i];var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);}}window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);})();
         //]]>
      </script>
      <script type='text/javascript'>
         window.HaravanAnalytics = window.HaravanAnalytics || {};
         window.HaravanAnalytics.meta = window.HaravanAnalytics.meta || {};
         window.HaravanAnalytics.meta.currency = 'VND';
         var meta = {"page":{"pageType":"product","resourceType":"product","resourceId":1011812654},"product":{"id":1011812654,"type":"Túi xách trung","title":"Túi xách trung khóa bấm kim loại hình tam giác TXT123","price":75000000.0,"selected_or_first_available_variant":{"id":1022737092,"price":75000000.0,"title":"Túi xách trung khóa bấm kim loại hình tam giác TXT123 - Xanh dương","sku":"TXT123","variant_title":"Xanh dương"},"vendor":"Juno"}};
         for (var attr in meta) {
         	window.HaravanAnalytics.meta[attr] = meta[attr];
         }
      </script>
      <script>
         //<![CDATA[
         window.HaravanAnalytics.ga = "UA-57206615-1";
         window.HaravanAnalytics.enhancedEcommerce = false;
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
         ga('create', window.HaravanAnalytics.ga, 'auto', {allowLinker: true});
         ga('send', 'pageview'); ga('require', 'linker');try {
         setTimeout(function(){
         	if( window.location.pathname == '/checkout' ) {
         		$.get('//file.hstatic.net/1000003969/file/script_checkout_1.jpg',function(data){
         			eval(data);
         		});
         	}
         if( $('.step3').length >= 1 && $(window).width() < 500 ) {
         $('body.step3 > a').remove();
         $('body.step3').prepend("<a href='/cart'><span class='btn-back'>Quay về giỏ hàng</span></a><a class='logo-checkout' href='/'><h1>Đặt hàng thành công</h1></a>");
         }
         
         },500);
         } catch (e) {};
                         //]]>
                         
      </script>
      <script>
         window.HaravanAnalytics.fb = "1566159606933153";
         //<![CDATA[
         !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         document,'script','//connect.facebook.net/en_US/fbevents.js');
         // Insert Your Facebook Pixel ID below. 
         fbq('init', window.HaravanAnalytics.fb );
         fbq('track', 'PageView');
         //]]>
      </script>
      

      <script src='js/bootstrap.min.js' type='text/javascript'></script>
      <script src='js/smartsearch.min.js' type='text/javascript'></script>
      <script src='js/province_hide.js' type='text/javascript'></script>

      
      <script>
         if(typeof fbq === 'undefined') {
         	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         	document,'script','//connect.facebook.net/en_US/fbevents.js');
         
         	fbq('init', '1566159606933153');
   
         	
         	fbq('track', '');
         	}else {
         		fbq('track', '');
         	}
      </script>
      <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/6767720e97fc0e6e564ed78ed/2bf2c58174507c12c59eae629.js");</script>
      <script src='js/eco_tracking.js' type='text/javascript'></script>
   </head>
   <body class="cms-index-index">
      <div id="script-head-body"></div>
      <div id="myModal-popup" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
         <div class="modal-dialog">
            <!-- Modal content-->
         </div>
      </div>
      <script>
         $(function() {
         	var currentAjaxRequest = null;
         	var searchForms = $('form[action="/search"]').css('position','relative').each(function() {
         		var input = $(this).find('input[name="q"]');
         		var offSet = input.position().top + input.innerHeight();
         		$('<ul class="search-results"></ul>').css( { 'position': 'absolute', 'left': '0px', 'top': offSet } ).appendTo($(this)).hide();    
         		input.attr('autocomplete', 'off').bind('keyup change', function() {
         			var term = $(this).val();
         			var form = $(this).closest('form');
         			/*var searchURL = '/search?type=product&q=' + term;*/
         			var searchURL = '/search?type=product&q=filter=(title:product**'+term+')';
         			var resultsList = form.find('.search-results');
         			if (term.length > 3 && term != $(this).attr('data-old-term')) {
         				$(this).attr('data-old-term', term);
         				if (currentAjaxRequest != null) currentAjaxRequest.abort();
         				console.log(searchURL + '&view=json');
         				currentAjaxRequest = $.getJSON(searchURL + '&view=json', function(data) {
         					console.log(searchURL);
         					resultsList.empty();
         					if(data.results_count == 0) {
         						// resultsList.html('<li><span class="title">No results.</span></li>');
         						// resultsList.fadeIn(200);
         						resultsList.hide();
         					} 
         					else {
         						resultsList.show();
         						var count_data = 0;
         						for (var i=0; i<data.results.length; i++) {
         							var item = data.results[i];
         							if (item.title.indexOf('quà tặng') < 0 && item.price !== '0₫' ){
         								var link = $('<a></a>').attr('href', item.url);
         								link.append('<span class="thumbnail"><img src="' + item.thumbnail + '" /></span>');
         								link.append('<span class="title">' + item.title + '</span>');
         								link.append('<span class="price">' + item.price + '</span>');
         								link.wrap('<li></li>');
         								resultsList.append(link.parent());
         								/*console.log(count_data)*/
         								count_data++;
         							}
         						}
         						if(count_data > 5) {
         							var url = '/search?q=filter=((title:product**[KEY]))&type=product';
         							var search_text = $('#text-product').val();
         							if (search_text.length > 0){
         								url = url.replace('[KEY]', search_text);
         							}
         							resultsList.append('<li><span class="title"><a href="' + url + '">Xem thêm sản phẩm</a></span></li>');
         						}
         						if(count_data <=0) {
         							resultsList.hide();
         						}
         						resultsList.fadeIn(200);
         					}        
         				});
         			}
         		});
         	});
         
         	$('body').bind('click', function(){
         		$('.search-results').hide();
         	});
         });
      </script>
      <!-- Some styles to get you started. -->
      <style>
         .search-results {
         display: block;
         width: 345px;
         margin: 0px auto 0;
         border: 1px solid #e2e2e2;
         border-radius: 4px;
         -webkit-border-radius: 4px;
         -moz-border-radius: 4px;
         box-shadow: 0 1px 3px 0 #7d7d7d;
         -webkit-box-shadow: 0 1px 3px 0 #7d7d7d;
         -moz-box-shadow: 0 1px 3px 0 #7d7d7d;
         background: #fff;
         position: absolute;
         z-index: 99999;
         left: 170px;
         }
         .search-results li {
         display: block;
         background: #fff;
         overflow: hidden;
         list-style: none;
         border-bottom: 1px dotted #ccc;
         float: none;
         }
         .search-results li:hover {
         background: #FFF5F5;
         }
         .search-results li a {
         position: relative;
         display: block;
         overflow: hidden;
         padding: 6px;
         }
         .search-results li:first-child {
         border-top: none;
         }
         .search-results .title {
         display: block;
         width: 72%;
         line-height: 1.3em;
         color: #333;
         font-size: 14px;
         text-align: left;
         margin-top:10px !important;
         font-weight: 700;
         overflow: hidden;
         text-overflow: ellipsis;
         white-space: nowrap;
         }
         .search-results .price {
         font-size: 14px;
         margin-top: 8px;
         color: red;
         }
         .search-results .thumbnail {
         float: left;
         width: 50px;
         height: 50px;
         margin: 0 6px 0 0;
         }
      </style>
      <section id="sidebar-wrapper" class="hidden-lg hidden-md">
         <ul class="sidebar-nav">
            <li class="title">Danh Mục
            </li>
            <li class="mobile-click">
               <a href="/collections/tui-xach" 
                  title="Túi xách" 
                  class="tui-xach-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_tuixach.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tuixach.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tuixach.png?v=2"/>
               </span>
               Túi xách
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-cao-got" 
                  title="Cao gót" 
                  class="cao-got-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_caogot.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_caogot.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_caogot.png?v=2"/>
               </span>
               Cao gót
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-xang-dan" 
                  title="Xăng đan" 
                  class="xang-dan-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_xangdan.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_xangdan.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_xangdan.png?v=2"/>
               </span>
               Xăng đan
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-bup-be" 
                  title="Búp bê" 
                  class="bup-be-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_bupbe.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_bupbe.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_bupbe.png?v=2"/>
               </span>
               Búp bê
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/sneaker-collections" 
                  title="Giày Sneaker" 
                  class="giay-sneaker-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_sneaker.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_sneaker.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_sneaker.png?v=2"/>
               </span>
               Giày Sneaker
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-boots" 
                  title="Giày Boots" 
                  class="giay-boots-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_boots.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_boots.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_boots.png?v=2"/>
               </span>
               Giày Boots
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/dep-guoc" 
                  title="Dép Guốc" 
                  class="dep-guoc-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_depguoc.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_depguoc.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_depguoc.png?v=2"/>
               </span>
               Dép Guốc
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/8-3-yeu-thuong-gap-doi" 
                  title="Yêu thương gấp đôi" 
                  class="yeu-thuong-gap-doi-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_yeuthuonggapdoi.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_event_hover.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_event_hover.png?v=2"/>
               </span>
               Yêu thương gấp đôi
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/juno-queens-of-fashion" 
                  title="Bộ Sưu Tập" 
                  class="bo-suu-tap-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_bosuutap.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_bosuutap_thumb.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_bosuutap_thumb.png?v=2"/>
               </span>
               Bộ Sưu Tập
               </a>
            </li>
            <li class="mobile-click">
               <a href="/blogs/magazine" 
                  title="Tin thời trang" 
                  class="tin-thoi-trang-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_tinthoitrang.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tinthoitrang.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tinthoitrang.png?v=2"/>
               </span>
               Tin thời trang
               </a>
            </li>
            <li class="more-info fa fa-phone">
               <a href="tel:18001162">
               <span>Gọi mua hàng: <strong>1800 1162</strong></span>
               <span>08:30 - 21:30 mỗi ngày trừ ngày Lễ, Tết</span>
               </a>
            </li>
            <li class="more-info bottom fa fa-building">
               <a href="/collections/cua-hang-khu-vuc-tp-ho-chi-minh?view=stores">
               <span> Xem hệ thống <strong>45</strong> cửa hàng</span>
               </a>
            </li>
         </ul>
      </section>
      <div id="wrapper">
         <header class=" template-page-product">
            <div class="top hidden-sm hidden-xs">
               <div class="container">
                  <div class="row">
                     <!---->
                     <div class="col-md-5 col-sm-10 left no-padding">
                        <div class="col-md-3 col-sm-10 logoTop">
                           <div class="logo">
                              <a href="/" title="JUNO"><img alt="JUNO" class="" src="//theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" /></a>
                           </div>
                        </div>
                        <div class="col-md-7 col-sm-10 no-padding searchTop">
                           <div class="search-collection col-xs-10 no-padding">
                              <form class="search" action="/search">
                                 <input id="text-product" class="col-xs-10 no-padding" type="text" name="q" placeholder="Bạn cần tìm gì?" />
                                 <input type="hidden" value="product" name="type" />
                                 <button id="submit-search">
                                 <i class="fa fa-search" aria-hidden="true"></i>
                                 </button>                        
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-sm-10 no-padding rightTop_head">
                        <div class="col-md-5 col-sm-10 switchboardTop">
                           <div class="switchboard_wrapper">	
                              <i class="fa fa-phone" aria-hidden="true"></i>
                              <span>BÁN HÀNG: <strong>1800 1162</strong> (miễn phí)</span>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-10 no-padding storeTop">
                           <div class="headStore_wrapper">														
                              <a  href="/pages/tim-dia-chi-cua-hang">
                              <i class="fa fa-building" aria-hidden="true"></i>
                              <span>Xem hệ thống <strong>68</strong> cửa hàng</span>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-1 col-sm-10 no-padding cartTop">
                           <div class="carttop_wrapper">
                              <div class="cart-relative">
                                 <a href="/cart">
                                    <div class="cart-total-price">
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span class="price" >0 sản phẩm</span>
                                       <span class="hidden" >Giỏ Hàng</span>
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!---->
                  </div>
               </div>
            </div>
            <div id="fix-top-menu" class="top2 hidden-sm hidden-xs">
               <div class="container-fluid menutopid" style="">
                  <div class="container" style="position:relative">
                     <div class="row">
                        <div class="col-lg-10 col-md-10">
                           <ul class="menu-top clearfix hidden-xs">
                              <li class="menu-li hasChild " >
                                 <a href="/collections/tui-xach" class="" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_tuixach.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tuixach_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tuixach.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Túi Xách
                                       </span>
                                    </div>
                                 </a>
                                 <ul class=" dropdown-menu drop-menu dropmenu_item_show_1">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/tui-xach-trung-txt124">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/xanh_txt124_1.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Túi xách trung sọc vân đính tua rua trang trí TXT124</span><br/>
                                             </div>
                                             <div class="menu-price-pr">750,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/tui-xach-trung-txt124">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/tui-xach-co-lon"><i class="fa fa-caret-right"></i> Túi cỡ lớn</a></li>
                                    <li><a href="/collections/tui-xach-co-trung"><i class="fa fa-caret-right"></i> Túi cỡ trung</a></li>
                                    <li><a href="/collections/tui-xach-co-nho"><i class="fa fa-caret-right"></i> Túi nhỏ</a></li>
                                    <li><a href="/collections/vi-cam-tay"><i class="fa fa-caret-right"></i> Ví cầm tay</a></li>
                                    <li><a href="/collections/clutch"><i class="fa fa-caret-right"></i> Clutch</a></li>
                                    <li><a href="/collections/ba-lo-thoi-trang"><i class="fa fa-caret-right"></i> Ba lô thời trang</a></li>
                                    <li><a href="/collections/tui-canvas"><i class="fa fa-caret-right"></i> Túi canvas</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li hasChild " >
                                 <a href="/collections/giay-cao-got" class="" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_caogot.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_caogot_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_caogot.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Giày Cao Gót
                                       </span>
                                    </div>
                                 </a>
                                 <ul class="first dropdown-menu drop-menu dropmenu_item_show_2">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/giay-cao-got-cg09092">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/bac_cg09092_1.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Giày cao gót 9cm mũi nhọn gót nhọn phối kim nhũ CG09092</span><br/>
                                             </div>
                                             <div class="menu-price-pr">430,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/giay-cao-got-cg09092">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/cao-got-cao-5cm"><i class="fa fa-caret-right"></i> Cao 5cm</a></li>
                                    <li><a href="/collections/cao-got-cao-7cm"><i class="fa fa-caret-right"></i> Cao 7cm</a></li>
                                    <li><a href="/collections/giay-cao-got-cao-9cm"><i class="fa fa-caret-right"></i> Cao 9cm</a></li>
                                    <li><a href="/collections/cao-got-cao-11cm"><i class="fa fa-caret-right"></i> Cao 11cm</a></li>
                                    <li><a href="/collections/cao-got-mui-tron"><i class="fa fa-caret-right"></i> Mũi Tròn</a></li>
                                    <li><a href="/collections/cao-got-mui-nhon"><i class="fa fa-caret-right"></i> Mũi Nhọn</a></li>
                                    <li><a href="/collections/cao-got-got-nhon"><i class="fa fa-caret-right"></i> Gót Nhọn</a></li>
                                    <li><a href="/collections/cao-got-got-vuong"><i class="fa fa-caret-right"></i> Gót Vuông</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li hasChild " >
                                 <a href="/collections/giay-xang-dan" class="" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_xangdan.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_xangdan_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_xangdan.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Giày Xăng Đan
                                       </span>
                                    </div>
                                 </a>
                                 <ul class="first dropdown-menu drop-menu dropmenu_item_show_3">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/giay-sandal-sd11005">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/den_sd11005_1.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Giày Sandal cao 11cm quai ngang chất liệu satin SD11005</span><br/>
                                             </div>
                                             <div class="menu-price-pr">430,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/giay-sandal-sd11005">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/xang-dan-cao-5cm"><i class="fa fa-caret-right"></i> Cao 5cm</a></li>
                                    <li><a href="/collections/xang-dan-cao-7cm"><i class="fa fa-caret-right"></i> Cao 7cm</a></li>
                                    <li><a href="/collections/xang-dan-cao-9cm"><i class="fa fa-caret-right"></i> Cao 9cm</a></li>
                                    <li><a href="/collections/xang-dan-cao-11cm"><i class="fa fa-caret-right"></i> Cao 11cm</a></li>
                                    <li><a href="/collections/xang-dan-de-xuong"><i class="fa fa-caret-right"></i> Xăng Đan Đế Xuồng</a></li>
                                    <li><a href="/collections/xang-dan-cao-got"><i class="fa fa-caret-right"></i> Xăng Đan Cao Gót</a></li>
                                    <li><a href="/collections/xang-dan-bet"><i class="fa fa-caret-right"></i> Xăng Đan Bệt</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li hasChild " >
                                 <a href="/collections/giay-bup-be" class="" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_bupbe.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bupbe_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bupbe.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Giày Búp Bê
                                       </span>
                                    </div>
                                 </a>
                                 <ul class="first dropdown-menu drop-menu dropmenu_item_show_4">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/giay-bup-be-bb01113">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/do_bb01113_1_c7cce527f1a34a468db3b52a7f02d6f8.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Giày búp bê mũi nhọn chất liệu Satin BB01113</span><br/>
                                             </div>
                                             <div class="menu-price-pr">390,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/giay-bup-be-bb01113">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/bup-be-mui-tron"><i class="fa fa-caret-right"></i> Giày Mũi Tròn</a></li>
                                    <li><a href="/collections/bup-be-mui-nhon"><i class="fa fa-caret-right"></i> Giày Mũi Nhọn</a></li>
                                    <li><a href="/collections/bup-be-co-trang-tri"><i class="fa fa-caret-right"></i> Giày có trang trí</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/sneaker-collections" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_sneaker.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_sneaker.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_sneaker.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Giày Sneaker</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/giay-boots" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_boots.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_boots.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_boots.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Giày Boots</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/dep-guoc" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_depguoc.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_depguoc.png') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_depguoc.png') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Dép Guốc</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li active  fix-icon-coll "
                                 >
                                 <a href="/collections/8-3-yeu-thuong-gap-doi" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_yeuthuonggapdoi.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_event.png') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_event.png') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Yêu Thương Gấp Đôi</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/juno-queens-of-fashion" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_bosuutap.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bosuutap.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bosuutap.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Bộ sưu tập</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/blogs/magazine" class="" target="_blank"  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_tinthoitrang.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tinthoitrang.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tinthoitrang.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Tin Thời Trang</span>
                                    </div>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <style>
               .coll-icon .ico-top {
               position: relative;
               overflow: hidden;
               width: 60px;
               height: 36px;
               padding: 0;
               margin: 0 auto;
               display: block;
               text-align: center;
               }
               .coll-icon .ico-top .img-on {
               opacity: 0;
               height: 0;
               transition: all 0.5s;
               }
               .coll-icon .ico-top .img-off{
               opacity: 1;
               width: 60px;
               height: 36px;
               transition: all 0.3s
               }
               /*.coll-icon .ico-top:hover .img-on{
               opacity: 1;
               height: 36px;
               width: 60px;
               }
               .coll-icon .ico-top:hover .img-off{
               opacity: 0;
               height: 0;
               }*/
            </style>
            <div class="top hidden-lg hidden-md" id="mobile-menu">
               <div class="fixed-nav">
                  <button id="menu-toggle" class="navbar-toggle pull-left" type="button" data-toggle="modal" data-target="#menu-modal">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a href="/" title="JUNO" class="logo"><img class="lazy" alt="JUNO" src="" data-src="//theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" /></a>
                  <a href="/cart" class="cart-link">
                  <span ><i class="fa fa-shopping-bag" aria-hidden="true"></i> <strong>0₫</strong></span>
                  </a>
                  <form class="frm-search" action='/search'>
                     <input  type="text" name='q' value="" id="inputSearch" placeholder="Tìm kiếm...">	
                  </form>
                  <script>
                     $(document).ready(function(){
                     
                     	$('#inputSearch').smartSearch({searchdelay:400});
                     
                     	$('.btn-search').click(function(){
                     		$('.frm-search').find('input').val('');
                     		if($(this).hasClass('active')) {
                     			$(this).removeClass('active');
                     			$('.frm-search').find('input').css('width', '1px');
                     			setTimeout(function(){
                     				$('.frm-search').find('input').css('display', 'none');	
                     				$('.logo').show();
                     				$('#menu-toggle').show();
                     				$('body').removeClass('searching');
                     			},700);
                     
                     		}
                     		else {
                     			$(this).addClass('active');
                     			$('.logo').hide();
                     			$('#menu-toggle').hide();
                     			$('.frm-search').find('input').css('display', 'block');
                     			setTimeout(function(){
                     				var a = screen.width - 90;
                     				$('.frm-search').find('input').css('width', a);
                     				$('body').addClass('searching');
                     			},100);
                     		}
                     	});
                     
                     	$('#wrapper').click(function(e){
                     		if (e.target !== $('.btn-search')){
                     			return;
                     		}else{
                     			$('.btn-search').removeClass('active');
                     			$('.frm-search').find('input').css('width', '1px');
                     			setTimeout(function(){
                     				$('.frm-search').find('input').css('display', 'none');	
                     				$('.logo').show();
                     				$('#menu-toggle').show();
                     				$('body').removeClass('searching');
                     			},500);
                     		}
                     	});
                     
                     
                     
                     });
                     
                     function search(){
                     debugger
                     	var url = 'filter=((title:product**[KEY]))&type=product';
                     	var search_text = $('#inputSearch').val();
                     	if (search_text.length > 0){
                     		url = url.replace('[KEY]', search_text);
                     		window.location.href = '/search?q='+ encodeURIComponent(url);
                     	}
                     	return false;
                     }
                  </script>
               </div>
            </div>
         </header>
         <link href='//theme.hstatic.net/1000003969/1000323463/14/picbox.css?v=2220' rel='stylesheet' type='text/css'  media='all'  />
         <script src='//theme.hstatic.net/1000003969/1000323463/14/picbox.js?v=2220' type='text/javascript'></script>
         <section id="product" data-template="product">
            <div class="container wow fadeIn">
               <style>
                  .breadcrumb > li + li::before
                  {
                  //content: " › ";
                  border-left: 0px solid transparent;
                  }
               </style>
               <!-- breadcrumbs -->
               <div class="breadcrumbs">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="col-lg-12">
                              <ol class="breadcrumb">
                                 <li class="home"> <a href="/" title="Trang chủ">Trang chủ</a></li>
                                 <li class="space"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                 <li class="collections">
                                    <a href="/collections/giao-nhanh-2h"> 
                                    Sản phẩm áp dụng Giao nhanh 2h - Juno flash</a>
                                 </li>
                                 <li class="space"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                 <li class="product active">
                                    <a href="/products/tui-xach-trung-txt123">Túi xách trung khóa bấm kim loại hình tam giác TXT123</a>
                                 <li>
                              </ol>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="top-detail" class="normal top-detail wow fadeIn">
               <div class="container">
                  <div class="content">
                     <div class="clearfix" id="detail-product">
                        <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10">
                           <div class="main-img col-lg-10 col-md-10 col-sm-10 no-padding slide-imagesm">
                              <div class="" id="slide-image"></div>
                              <div class="col-xs-10 col-sm-10 thumbnails small-img">
                                 <div class="row">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="images hidden">
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_1024x1024.jpg" 
                              data-option="xanh-duong">
                              <a href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_master.jpg" 
                                 title="Click để xem"
                                 data-option="xanh-duong"
                                 data-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_master.jpg" 
                                 rel="lightbox-xanh-duong">
                                 <img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_master.jpg" 
                                    rel="lightbox-xanh-duong" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_1024x1024.jpg" 
                              data-option="xanh-duong">
                              <a href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_master.jpg" 
                                 title="Click để xem"
                                 data-option="xanh-duong"
                                 data-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_master.jpg" 
                                 rel="lightbox-xanh-duong">
                                 <img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_master.jpg" 
                                    rel="lightbox-xanh-duong" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_1024x1024.jpg" 
                              data-option="xanh-duong">
                              <a href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_master.jpg" 
                                 title="Click để xem"
                                 data-option="xanh-duong"
                                 data-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_master.jpg" 
                                 rel="lightbox-xanh-duong">
                                 <img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_master.jpg" 
                                    rel="lightbox-xanh-duong" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_1024x1024.jpg" 
                              data-option="xanh-duong">
                              <a href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_master.jpg" 
                                 title="Click để xem"
                                 data-option="xanh-duong"
                                 data-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_master.jpg" 
                                 rel="lightbox-xanh-duong">
                                 <img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_master.jpg" 
                                    rel="lightbox-xanh-duong" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_1024x1024.jpg" 
                              data-option="xanh-duong">
                              <a href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_master.jpg" 
                                 title="Click để xem"
                                 data-option="xanh-duong"
                                 data-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_master.jpg" 
                                 rel="lightbox-xanh-duong">
                                 <img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_master.jpg" 
                                    rel="lightbox-xanh-duong" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_1024x1024.jpg" 
                              data-option="xanh-duong">
                              <a href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_master.jpg" 
                                 title="Click để xem"
                                 data-option="xanh-duong"
                                 data-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_master.jpg" 
                                 rel="lightbox-xanh-duong">
                                 <img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_master.jpg" 
                                    rel="lightbox-xanh-duong" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/trang_txt123_1_1024x1024.jpg" 
                              data-option="trang">
                              <a href="//product.hstatic.net/1000003969/product/trang_txt123_1_master.jpg" 
                                 title="Click để xem"
                                 data-option="trang"
                                 data-image="//product.hstatic.net/1000003969/product/trang_txt123_1_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_1_master.jpg" 
                                 rel="lightbox-trang">
                                 <img src="//product.hstatic.net/1000003969/product/trang_txt123_1_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/trang_txt123_1_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_1_master.jpg" 
                                    rel="lightbox-trang" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/trang_txt123_3_1024x1024.jpg" 
                              data-option="trang">
                              <a href="//product.hstatic.net/1000003969/product/trang_txt123_3_master.jpg" 
                                 title="Click để xem"
                                 data-option="trang"
                                 data-image="//product.hstatic.net/1000003969/product/trang_txt123_3_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_3_master.jpg" 
                                 rel="lightbox-trang">
                                 <img src="//product.hstatic.net/1000003969/product/trang_txt123_3_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/trang_txt123_3_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_3_master.jpg" 
                                    rel="lightbox-trang" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/trang_txt123_5_1024x1024.jpg" 
                              data-option="trang">
                              <a href="//product.hstatic.net/1000003969/product/trang_txt123_5_master.jpg" 
                                 title="Click để xem"
                                 data-option="trang"
                                 data-image="//product.hstatic.net/1000003969/product/trang_txt123_5_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_5_master.jpg" 
                                 rel="lightbox-trang">
                                 <img src="//product.hstatic.net/1000003969/product/trang_txt123_5_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/trang_txt123_5_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_5_master.jpg" 
                                    rel="lightbox-trang" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/trang_txt123_6_1024x1024.jpg" 
                              data-option="trang">
                              <a href="//product.hstatic.net/1000003969/product/trang_txt123_6_master.jpg" 
                                 title="Click để xem"
                                 data-option="trang"
                                 data-image="//product.hstatic.net/1000003969/product/trang_txt123_6_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_6_master.jpg" 
                                 rel="lightbox-trang">
                                 <img src="//product.hstatic.net/1000003969/product/trang_txt123_6_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/trang_txt123_6_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_6_master.jpg" 
                                    rel="lightbox-trang" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/trang_txt123_4_1024x1024.jpg" 
                              data-option="trang">
                              <a href="//product.hstatic.net/1000003969/product/trang_txt123_4_master.jpg" 
                                 title="Click để xem"
                                 data-option="trang"
                                 data-image="//product.hstatic.net/1000003969/product/trang_txt123_4_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_4_master.jpg" 
                                 rel="lightbox-trang">
                                 <img src="//product.hstatic.net/1000003969/product/trang_txt123_4_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/trang_txt123_4_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_4_master.jpg" 
                                    rel="lightbox-trang" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/trang_txt123_2_1024x1024.jpg" 
                              data-option="trang">
                              <a href="//product.hstatic.net/1000003969/product/trang_txt123_2_master.jpg" 
                                 title="Click để xem"
                                 data-option="trang"
                                 data-image="//product.hstatic.net/1000003969/product/trang_txt123_2_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_2_master.jpg" 
                                 rel="lightbox-trang">
                                 <img src="//product.hstatic.net/1000003969/product/trang_txt123_2_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/trang_txt123_2_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/trang_txt123_2_master.jpg" 
                                    rel="lightbox-trang" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/den_txt123_1_1024x1024.jpg" 
                              data-option="den">
                              <a href="//product.hstatic.net/1000003969/product/den_txt123_1_master.jpg" 
                                 title="Click để xem"
                                 data-option="den"
                                 data-image="//product.hstatic.net/1000003969/product/den_txt123_1_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_1_master.jpg" 
                                 rel="lightbox-den">
                                 <img src="//product.hstatic.net/1000003969/product/den_txt123_1_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/den_txt123_1_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_1_master.jpg" 
                                    rel="lightbox-den" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/den_txt123_2_1024x1024.jpg" 
                              data-option="den">
                              <a href="//product.hstatic.net/1000003969/product/den_txt123_2_master.jpg" 
                                 title="Click để xem"
                                 data-option="den"
                                 data-image="//product.hstatic.net/1000003969/product/den_txt123_2_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_2_master.jpg" 
                                 rel="lightbox-den">
                                 <img src="//product.hstatic.net/1000003969/product/den_txt123_2_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/den_txt123_2_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_2_master.jpg" 
                                    rel="lightbox-den" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/den_txt123_6_1024x1024.jpg" 
                              data-option="den">
                              <a href="//product.hstatic.net/1000003969/product/den_txt123_6_master.jpg" 
                                 title="Click để xem"
                                 data-option="den"
                                 data-image="//product.hstatic.net/1000003969/product/den_txt123_6_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_6_master.jpg" 
                                 rel="lightbox-den">
                                 <img src="//product.hstatic.net/1000003969/product/den_txt123_6_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/den_txt123_6_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_6_master.jpg" 
                                    rel="lightbox-den" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/den_txt123_3_1024x1024.jpg" 
                              data-option="den">
                              <a href="//product.hstatic.net/1000003969/product/den_txt123_3_master.jpg" 
                                 title="Click để xem"
                                 data-option="den"
                                 data-image="//product.hstatic.net/1000003969/product/den_txt123_3_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_3_master.jpg" 
                                 rel="lightbox-den">
                                 <img src="//product.hstatic.net/1000003969/product/den_txt123_3_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/den_txt123_3_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_3_master.jpg" 
                                    rel="lightbox-den" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/den_txt123_4_1024x1024.jpg" 
                              data-option="den">
                              <a href="//product.hstatic.net/1000003969/product/den_txt123_4_master.jpg" 
                                 title="Click để xem"
                                 data-option="den"
                                 data-image="//product.hstatic.net/1000003969/product/den_txt123_4_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_4_master.jpg" 
                                 rel="lightbox-den">
                                 <img src="//product.hstatic.net/1000003969/product/den_txt123_4_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/den_txt123_4_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_4_master.jpg" 
                                    rel="lightbox-den" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                           <div class="item itemdelete lazy" 
                              data-original="//product.hstatic.net/1000003969/product/den_txt123_5_1024x1024.jpg" 
                              data-option="den">
                              <a href="//product.hstatic.net/1000003969/product/den_txt123_5_master.jpg" 
                                 title="Click để xem"
                                 data-option="den"
                                 data-image="//product.hstatic.net/1000003969/product/den_txt123_5_master.jpg" 
                                 data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_5_master.jpg" 
                                 rel="lightbox-den">
                                 <img src="//product.hstatic.net/1000003969/product/den_txt123_5_grande.jpg"/>
                                 <p class="click-p" href="//product.hstatic.net/1000003969/product/den_txt123_5_master.jpg" 
                                    data-zoom-image="//product.hstatic.net/1000003969/product/den_txt123_5_master.jpg" 
                                    rel="lightbox-den" ><i class="fa fa-search" aria-hidden="true"></i> Click xem hình lớn hơn
                                 </p>
                              </a>
                           </div>
                        </div>
                        <div class="hidden thumbnails-hidden">
                           <div data-large="" class="thumbnail thumdelete" data-option="xanh-duong" data-zoom="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_large.jpg"><img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="xanh-duong" data-zoom="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_large.jpg"><img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_2_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="xanh-duong" data-zoom="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_large.jpg"><img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_3_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="xanh-duong" data-zoom="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_large.jpg"><img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_4_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="xanh-duong" data-zoom="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_large.jpg"><img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_5_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="xanh-duong" data-zoom="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_large.jpg"><img src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_6_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="trang" data-zoom="//product.hstatic.net/1000003969/product/trang_txt123_1_large.jpg"><img src="//product.hstatic.net/1000003969/product/trang_txt123_1_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="trang" data-zoom="//product.hstatic.net/1000003969/product/trang_txt123_3_large.jpg"><img src="//product.hstatic.net/1000003969/product/trang_txt123_3_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="trang" data-zoom="//product.hstatic.net/1000003969/product/trang_txt123_5_large.jpg"><img src="//product.hstatic.net/1000003969/product/trang_txt123_5_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="trang" data-zoom="//product.hstatic.net/1000003969/product/trang_txt123_6_large.jpg"><img src="//product.hstatic.net/1000003969/product/trang_txt123_6_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="trang" data-zoom="//product.hstatic.net/1000003969/product/trang_txt123_4_large.jpg"><img src="//product.hstatic.net/1000003969/product/trang_txt123_4_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="trang" data-zoom="//product.hstatic.net/1000003969/product/trang_txt123_2_large.jpg"><img src="//product.hstatic.net/1000003969/product/trang_txt123_2_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="den" data-zoom="//product.hstatic.net/1000003969/product/den_txt123_1_large.jpg"><img src="//product.hstatic.net/1000003969/product/den_txt123_1_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="den" data-zoom="//product.hstatic.net/1000003969/product/den_txt123_2_large.jpg"><img src="//product.hstatic.net/1000003969/product/den_txt123_2_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="den" data-zoom="//product.hstatic.net/1000003969/product/den_txt123_6_large.jpg"><img src="//product.hstatic.net/1000003969/product/den_txt123_6_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="den" data-zoom="//product.hstatic.net/1000003969/product/den_txt123_3_large.jpg"><img src="//product.hstatic.net/1000003969/product/den_txt123_3_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="den" data-zoom="//product.hstatic.net/1000003969/product/den_txt123_4_large.jpg"><img src="//product.hstatic.net/1000003969/product/den_txt123_4_thumb.jpg"></div>
                           <div data-large="" class="thumbnail thumdelete" data-option="den" data-zoom="//product.hstatic.net/1000003969/product/den_txt123_5_large.jpg"><img src="//product.hstatic.net/1000003969/product/den_txt123_5_thumb.jpg"></div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-10 col-xs-10 product">
                           <div class="thongtingia clearfix">
                              <div class="changeLabel col-sm-4 no-padding">
                                 <div class="spnew-tags redTag"><span>Sản phẩm mới</span></div>
                              </div>
                              <div class="plugin-box col-sm-6 no-padding">
                                 <div class="fb-like" data-href="https://juno.vn/products/tui-xach-trung-txt123" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                 <div class="fb-save" data-uri="https://juno.vn/products/tui-xach-trung-txt123" data-size="small"></div>
                                 <div id="fb-root" class="hidden"></div>
                                 <script>
                                    (function(d, s, id) {
                                    	var js, fjs = d.getElementsByTagName(s)[0];
                                    	if (d.getElementById(id)) return;
                                    	js = d.createElement(s); js.id = id;
                                    	js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
                                    	fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                 </script>
                              </div>
                           </div>
                           <div class="thongtingia">
                              <h1>Túi xách trung khóa bấm kim loại hình tam giác TXT123</h1>
                              <h4>Mã SP : <span class="maspd">TXT123</span><span class="instock">còn hàng</span></h4>
                              <div class="price">
                                 <label class="variant-price red">750,000<sup>đ</sup></label>
                              </div>
                           </div>
                           <style>.rating-box span{font-family:Roboto-Regular;color:#000;}.point-star{display:inline-block;}.point-star li{display:inline-block;color:#999999}.point-star li.active{color:#f77705}</style>
                           <div class="rating-box col-xs-10 no-padding">
                              <span>Đánh giá sản phẩm: </span>
                              <ul class="point-star 5 0">
                                 <li class="active">
                                    <i class="fa fa-star"></i>
                                 </li>
                                 <li class="active">
                                    <i class="fa fa-star"></i>
                                 </li>
                                 <li class="active">
                                    <i class="fa fa-star"></i>
                                 </li>
                                 <li class="active">
                                    <i class="fa fa-star"></i>
                                 </li>
                                 <li class="active">
                                    <i class="fa fa-star"></i>
                                 </li>
                              </ul>
                           </div>
                           <div id="show-bonus"></div>
                           <form action="/cart" menthod="post" class="product-form" id="product-form">
                              <h3 class="">Chọn màu yêu thích</h3>
                              <ul class="option option1">
                              </ul>
                              <div class="clearfix"></div>
                              <div class="flash-delivery col-xs-10 getFlow">
                                 <div class='wrap_gift'>
                                    <h3 style="margin:0 0 10px 0">CHƯƠNG TRÌNH KHUYẾN MÃI</h3>
                                    <p>Mua 2 sản phẩm Giày/Ví + Túi (<500K), giá trọn bộ 600K</p>
                                    <p>Mua 2 sản phẩm Giày/Ví + Túi (>500K), giá trọn bộ 800K<br><a data-toggle="modal" data-target="#myModalFlash" href="javascript:;" >(Xem chi tiết)</a></p>
                                 </div>
                              </div>
                              <div id="myModalFlash" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
                                 <div class="modal-dialog modal-dialog-size fit">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header" style="border:none">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title hidden" style="color:#000;text-align:center;"></h4>
                                       </div>
                                       <div class="modal-body">
                                          <div class="row">
                                             <div class="col-md-10 col-xs-10" style="color:#000">
                                                <div class="text-center" style="text-transform: uppercase;font-size: 20px;line-height: 28px;">
                                                   <p>Mua 2 sản phẩm <strong>“Giày/Ví + Túi (<500K)”</strong> hoặc <strong>“Ví + Ví”</strong> hoặc <strong>“Giày + Giày”</strong></p>
                                                   <p style="font-size: 22px;margin: 10px 0;"><strong>600,000đ</strong></p>
                                                   <p class="" style="display:  block; height: 1.2px; width: 100%; background: #ccc; margin: 10px 0; "></p>
                                                   <p>Mua 2 sản phẩm <strong>“Giày/Ví + Túi (>500K)”</strong></p>
                                                   <p style="font-size: 22px;margin: 10px 0;"><strong>800,000đ</strong></p>
                                                   <p style="font-family:SFU;color:#000;text-transform:uppercase">Cách thức mua bộ sản phẩm như sau:</p>
                                                   <p><img src="//file.hstatic.net/1000003969/file/huong-dan-mua-combo.jpg" /></p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal-footer hidden"> 
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <select id="product-select" name="id" class="varian-select hidden" >
                              </select> 
                              <div class="wrappbtn col-xs-10 no-padding">
                                 <div class="product-btn-buy">
                                    <a href="javascript:void(0)" title="Đặt mua" class="add addnow">
                                       MUA NGAY<br/>
                                       <div>Mua online giao hàng tận nơi</div>
                                    </a>
                                 </div>
                              </div>
                           </form>
                           <div class="col-lg-10 col-md-10 col-xs-10 no-padding deliver-module">
                              <div class="item deliver-top-support">
                                 <p style="font-family:Roboto-Regular;">Gọi tổng đài để mua nhanh hơn</p>
                                 <p> 
                                    <a class="call_easyshopping" href="javascript:;" title="Dễ dàng mua sắm">
                                    1800 1162</a> 
                                    <b>(8:30 - 21:30)</b> <span>mỗi ngày</span>
                                 </p>
                              </div>
                              <div class="item deliver-top-info hidden">
                                 <p><b>Sẽ có tại nhà bạn</b> từ 1-5 ngày làm việc</p>
                              </div>
                              <div class="item deliver-top-on">
                                 <p class="">
                                    <a class="call_freeshipping" href="javascript:;" title="Miễn phí giao hàng toàn quốc">
                                    Miễn phí giao hàng toàn quốc
                                    </a> (Sản phẩm trên 300,000đ)
                                 </p>
                              </div>
                              <div class="item deliver-top-payment">
                                 <p>
                                    <a class="call_easypayment" href="javascript:;" title="Thanh toán tiện lợi">
                                    Thanh toán tiện lợi
                                    </a> (nhiều hình thức thanh toán)
                                 </p>
                              </div>
                              <div class="item deliver-top-policy">
                                 <p>
                                    <a class="call_easyremind" href="javascript:;" title="Đổi trả dễ dàng">
                                    Đổi trả dễ dàng
                                    </a> 
                                    (Giày 90 ngày, Túi 30 ngày)
                                 </p>
                              </div>
                              <div id="easy_payment" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
                                 <div class="modal-dialog modal-dialog-size" style="padding-top:2%;width:700px !important">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <div class="modal-title"></div>
                                       </div>
                                       <div class="modal-body no-padding">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="suggestion">
               <style>
                  .product-item-suggestion .product-wrapper .product-information .product-detail a .product-image{height:250px}
                  .product-item-suggestion .owl-controls{top:calc(38% + 5px) !important; position: absolute; left: 0; right: 0; }
                  .product-item-suggestion .owl-controls .owl-buttons div{position:absolute;font-size:55px;color:#000;background:transparent;opacity: 1;}
                  .product-item-suggestion .owl-controls .owl-buttons div img{width:50px}
                  .product-item-suggestion .owl-controls .owl-buttons .owl-prev{left:-14px}
                  .product-item-suggestion .owl-controls .owl-buttons .owl-next{right:-14px}
               </style>
               <div class="list-suggestion-product container">
                  <div class="row">
                     <div class="col-sm-10">
                        <h4 class="title-review title-bold" style="margin:22px 0 0 0px;text-align: left;">
                           <span style="">
                           Mua thêm sản phẩm dưới đây để có giá combo tốt hơn
                           </span>
                        </h4>
                        <div class="product-item-suggestion owl-carousel owl-theme">
                        </div>
                     </div>
                  </div>
               </div>
               <script>
                  $(document).ready(function(){
                  	$('.product-item-suggestion').owlCarousel({
                  		lazyLoad:true,
                  		items:5,
                  		itemsDesktop:5,
                  		slideSpeed : 1000,
                  		scrollPerPage : true,
                  		autoPlay:false,
                  		navigation: true,
                  		pagination:false,
                  		navigationText: ['<img style="" src="//file.hstatic.net/1000003969/file/if_basics-05_296830.png">','<img style="" src="//file.hstatic.net/1000003969/file/if_basics-06_296832.png">'],
                  
                  	});
                  })
               </script>
            </div>
            <div id="descriptionproduct" class="container wow fadeIn">
                    <div class="row clearfix">
                        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10 info-product-left">
                            <div class="thongso" role="tabpanel" id="tab-product">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" ><a href="#home" aria-controls="home1" role="tab" data-toggle="tab">Thông số kỹ thuật</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home1">
                                        <!--DESCRIPTION-->
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                    <img style="margin-left:50px" class="img-responsive" src="//product.hstatic.net/1000003969/product/xanh-duong_txt123_1_medium.jpg" alt="Túi xách trung khóa bấm kim loại hình tam giác TXT123" title="Túi xách trung khóa bấm kim loại hình tam giác TXT123"/>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                    <div class="backsys" >
                                                        <div><span class="infobe">Mã SP</span><span class="infoaf">TXT123</span></div>
                                                        <div><span class="infobe">Kiểu dáng</span><span class="infoaf"> Túi xách thời trang</span></div>
                                                        <div><span class="infobe">Chất liệu</span><span class="infoaf"> Da tổng hợp</span></div>
                                                        <div><span class="infobe">Màu sắc</span><span class="infoaf"> Đen-Trắng-Xanh dương</span></div>
                                                        <div><span class="infobe">Kích cỡ</span><span class="infoaf"> 18.5cmx15.5cmx9.5cm</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-10 no-padding description" role="tabpanel" id="tab-product">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Chi tiết sản phẩm</a></li>
                                    <li role="presentation" class=""><a data-toggle="modal" data-target="#myModalbaoquan"  href="#huongdanbaoquan" aria-controls="huongdanbaoquan" role="tab" data-toggle="tab">Hướng dẫn bảo quản</a></li>
                                    <li role="presentation"><a onclick="$('body').animate({scrollTop: $('#product-review').offset().top}, 500)" href="#review" aria-controls="review" role="tab" data-toggle="tab">Đánh Giá - Hỏi Đáp</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="col-xs-10 no-padding">
                                            <p><span style="font-size: 16px;" data-mce-style="font-size: 16px;">- Túi dạng trung, kiểu dáng đơn giản và tiện dụng</span><br></p>
                                            <p><span style="font-size: 16px;" data-mce-style="font-size: 16px;">- Khóa bấm bằng kim loại hình tam giác cách điệu, đẹp mắt và sang chảnh</span></p>
                                            <p><span style="font-size: 12pt;" data-mce-style="font-size: 12pt;">- Có 1 ngăn chính bên trong cho bạn thoải mái mang theo bên mình những vật dụng cần thiết như điện thoại, số tay, ví....</span></p>
                                            <p><span style="font-size: 12pt;" data-mce-style="font-size: 12pt;">- Chất liệu da tổng hợp cao cấp, dễ vệ sinh, bền đẹp</span></p>
                                            <p><span style="font-size: 12pt;" data-mce-style="font-size: 12pt;">- Có kèm thêm dây xích kim loại giúp bạn dễ dàng thay đổi phong cách&nbsp;</span></p>
                                            <p><span style="font-size: 12pt;" data-mce-style="font-size: 12pt;">- Thích hợp đi tiệc, dạo phố, đi làm... kết hợp với bất kể loại trang phục nào cũng giúp bạn luôn nổi bật và đầy tự tin</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="recommend-products" class="col-sm-10  no-padding">
                                <div class="col-sm-10 no-padding">
                                    <div class="title-review title-bold "  >
                                        <span>Gợi ý sản phẩm nên mua cùng bộ</span>
                                    </div>

                                    <div class="list-related-product product-collection-view-top clearfix row detail-product">
                                        <div class="product-item">
                                            <div class="item col-sm-3 col-6 item-mobile ">
                                                <div id="1010795070" class="">
                                                    <div class="product-information">
                                                        <div class="product-detail product-detail-1010795070 height-index-1 background-size ppclick">
                                                            <style>
                                                                .hover-back-1010795070{opacity: 1;background:url(//hstatic.net/0/0/global/noDefaultImage6_large.gif) no-repeat;cursor: pointer;-webkit-animation: fadein 2s;-moz-animation: fadein 2s;-ms-animation: fadein 2s;-o-animation: fadein 2s;animation: fadein 2s;}.hover-info-1010795070{visibility:hidden;}@keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-moz-keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-webkit-keyframes fadein {from { opacity: 0; }to{opacity: 1;}}@-ms-keyframes fadein{from{opacity:0;}to{ opacity:1;}@-o-keyframes fadein {from { opacity: 0; }to{opacity:1;}}
                                                            </style>
                                                            <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                <div class="product-image" style="position:relative;overflow:hidden;">
                                                                    <img class="image lazy" data-src="//product.hstatic.net/1000003969/product/xam_txt117_1_medium.jpg" alt="Túi xách trung trang trí viền đinh tán TXT117" />
                                                                </div>
                                                            </a>
                                                            <div class="product-info " >
                                                                <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                    <h2>
                                                                        TXT117
                                                                    </h2>
                                                                </a>
                                                                <div class="price-info clearfix">
                                                                    <div class="price-new-old">
                                                                        <span>590,000<sup>đ</sup></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item col-sm-3 col-6 item-mobile ">
                                                <div id="1010795070" class="">
                                                    <div class="product-information">
                                                        <div class="product-detail product-detail-1010795070 height-index-1 background-size ppclick">
                                                            <style>
                                                                .hover-back-1010795070{opacity: 1;background:url(//hstatic.net/0/0/global/noDefaultImage6_large.gif) no-repeat;cursor: pointer;-webkit-animation: fadein 2s;-moz-animation: fadein 2s;-ms-animation: fadein 2s;-o-animation: fadein 2s;animation: fadein 2s;}.hover-info-1010795070{visibility:hidden;}@keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-moz-keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-webkit-keyframes fadein {from { opacity: 0; }to{opacity: 1;}}@-ms-keyframes fadein{from{opacity:0;}to{ opacity:1;}@-o-keyframes fadein {from { opacity: 0; }to{opacity:1;}}
                                                            </style>
                                                            <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                <div class="product-image" style="position:relative;overflow:hidden;">
                                                                    <img class="image lazy" data-src="//product.hstatic.net/1000003969/product/xam_txt117_1_medium.jpg" alt="Túi xách trung trang trí viền đinh tán TXT117" />
                                                                </div>
                                                            </a>
                                                            <div class="product-info " >
                                                                <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                    <h2>
                                                                        TXT117
                                                                    </h2>
                                                                </a>
                                                                <div class="price-info clearfix">
                                                                    <div class="price-new-old">
                                                                        <span>590,000<sup>đ</sup></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 padding-none">
                                    <div class="comment" style="padding-right: 30px;margin-top: 40px;display:block" id="">
                                        <div id="product-review" style="position:relative;z-index:2;">
                                            <h3>Mời bạn đánh giá hoặc đặt câu hỏi về Túi xách trung khóa bấm kim loại hình tam giác TXT123</h3>
                                            <div id="fb-root"></div>
                                            <div class="fb-comment">
                                                <div class="comment-content">
                                                    <div id="__izi-comment_widget"></div>
                                                    <script>!function(a,b){var c=a.createElement("iframe"),e=(b.location.href,"https://juno.izicomment.com"),f=function(a,b){var c=document.getElementById(a);c.style.visibility="hidden",c.style.height=b+4+"px",c.style.visibility="visible"},g=function(){for(var a=document.getElementsByTagName("meta"),b=[],c=0;c<a.length;c++){for(var d=[],e=a[c].getAttribute("content"),f=0;f<a[c].attributes.length;f++){var g=a[c].attributes[f];d.push(g.name+"="+g.value)}e&&b.push({attributes:d,content:e})}return b},h=function(d){if(d.origin===e)try{var h=JSON.parse(d.data);if("height"==h.topic&&f("__izi-iframe_comment_widget",h.docHeight),"ready"==h.topic){var i={topic:"ready",meta:g(),location:b.location,document:{title:a.title,referrer:a.referrer}};c.contentWindow.postMessage(JSON.stringify(i),"*")}}catch(a){console.log(a,"Init comment widget failure!")}};c.src=e+"/widget/index.html",c.scrolling="no",c.style.border="none",c.style.overflow="hidden",c.style.height="100%",c.style.width="100%",c.style.minHeight="250px",c.id="__izi-iframe_comment_widget",a.getElementById("__izi-comment_widget").appendChild(c),window.addEventListener?window.addEventListener("message",h,!1):window.attachEvent&&window.attachEvent("onmessage",h)}(document,window);</script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="more-sp-detail" class="col-sm-10 no-padding">
                                    <div class="col-sm-10 no-padding" >
                                        <div class="related" style="position:relative;">
                                            <div class="title-review title-bold">
                                                <span>TOP 10 SẢN PHẨM ĐƯỢC MUA NHIỀU NHẤT TRONG TUẦN</span>
                                            </div>
                                            <div class="list-related-product product-collection-view-top clearfix row detail-product">
                                                <div class="product-item">
                                                    <div class="item col-sm-3 col-6 item-mobile ">
                                                        <div id="1010795070" class="">
                                                            <div class="product-information">
                                                                <div class="product-detail product-detail-1010795070 height-index-1 background-size ppclick">
                                                                    <style>
                                                                        .hover-back-1010795070{opacity: 1;background:url(//hstatic.net/0/0/global/noDefaultImage6_large.gif) no-repeat;cursor: pointer;-webkit-animation: fadein 2s;-moz-animation: fadein 2s;-ms-animation: fadein 2s;-o-animation: fadein 2s;animation: fadein 2s;}.hover-info-1010795070{visibility:hidden;}@keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-moz-keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-webkit-keyframes fadein {from { opacity: 0; }to{opacity: 1;}}@-ms-keyframes fadein{from{opacity:0;}to{ opacity:1;}@-o-keyframes fadein {from { opacity: 0; }to{opacity:1;}}
                                                                    </style>
                                                                    <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                        <div class="product-image" style="position:relative;overflow:hidden;">
                                                                            <img class="image lazy" data-src="//product.hstatic.net/1000003969/product/xam_txt117_1_medium.jpg" alt="Túi xách trung trang trí viền đinh tán TXT117" />
                                                                        </div>
                                                                    </a>
                                                                    <div class="product-info " >
                                                                        <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                            <h2>
                                                                                TXT117
                                                                            </h2>
                                                                        </a>
                                                                        <div class="price-info clearfix">
                                                                            <div class="price-new-old">
                                                                                <span>590,000<sup>đ</sup></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item col-sm-3 col-6 item-mobile ">
                                                        <div id="1010795070" class="">
                                                            <div class="product-information">
                                                                <div class="product-detail product-detail-1010795070 height-index-1 background-size ppclick">
                                                                    <style>
                                                                        .hover-back-1010795070{opacity: 1;background:url(//hstatic.net/0/0/global/noDefaultImage6_large.gif) no-repeat;cursor: pointer;-webkit-animation: fadein 2s;-moz-animation: fadein 2s;-ms-animation: fadein 2s;-o-animation: fadein 2s;animation: fadein 2s;}.hover-info-1010795070{visibility:hidden;}@keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-moz-keyframes fadein {from { opacity: 0; }to   { opacity: 1; }}@-webkit-keyframes fadein {from { opacity: 0; }to{opacity: 1;}}@-ms-keyframes fadein{from{opacity:0;}to{ opacity:1;}@-o-keyframes fadein {from { opacity: 0; }to{opacity:1;}}
                                                                    </style>
                                                                    <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                        <div class="product-image" style="position:relative;overflow:hidden;">
                                                                            <img class="image lazy" data-src="//product.hstatic.net/1000003969/product/xam_txt117_1_medium.jpg" alt="Túi xách trung trang trí viền đinh tán TXT117" />
                                                                        </div>
                                                                    </a>
                                                                    <div class="product-info " >
                                                                        <a class="ppclick" href="/products/tui-xach-trung-txt117#xam" title="Túi xách trung trang trí viền đinh tán TXT117">
                                                                            <h2>
                                                                                TXT117
                                                                            </h2>
                                                                        </a>
                                                                        <div class="price-info clearfix">
                                                                            <div class="price-new-old">
                                                                                <span>590,000<sup>đ</sup></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="sanphamdaxem col-sm-10 no-padding">
                                    <div class="col-sm-10 no-padding" >
                                        <div class="product-seen" style="position:relative;">
                                            <div class="title-review title-bold">
                                                <span>Sản phẩm đã xem</span>
                                            </div>
                                            <div id="owl-spdx" class="owl-carousel new-slide">
                                            </div>
                                            <div id="clone-item">
                                                <div class="item hidden">
                                                    <a class="products-block-image css_boxImgTop" href="#">
                                                        <img src="" alt="">
                                                    </a>
                                                    <div class="product-content-des col-xs-10 no-padding">
                                                        <h5>
                                                            <a style="padding: 0px 20px;" class="product-name" href="#" title="">
                                                            </a>
                                                        </h5>
                                                        <p class="product-description"></p>
                                                        <p class="pro-price-recent"></p>
                                                        <del class="pro-price-compare-recent"></del>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <style>
                                    #owl-spdx .item{padding:10px}
                                    .product-content-des{text-align:center}
                                    .product-content-des a{font-size:13px;color:#999;font-family:Roboto-Regular;margin:0;height:20px!important;overflow:hidden}
                                    .product-content-des .pro-price-recent{font-size:15px;color:#333;font-weight:700;font-family:Roboto-Bold;display:inline-block}
                                </style>
                                <script>
                                    var product_handle = 'tui-xach-trung-txt123';
                                    var last_view_products_current = localStorage.getItem('last_view_products');
                                    var max_last_view_products = 20;

                                    if( ! last_view_products_current ) {
                                        last_view_products_current = [];
                                    } else {
                                        last_view_products_current = JSON.parse(last_view_products_current);
                                    }

                                    if( last_view_products_current.indexOf(product_handle) == -1 ) {
                                        if( last_view_products_current.length >= max_last_view_products ) {
                                            last_view_products_current.shift();
                                        }

                                        last_view_products_current.push( product_handle );
                                        localStorage.setItem('last_view_products', JSON.stringify(last_view_products_current));
                                    }else{
                                        var remove_at = last_view_products_current.indexOf(product_handle);
                                        last_view_products_current.splice(remove_at, 1);
                                        last_view_products_current.push(product_handle);
                                        if( last_view_products_current.length >= max_last_view_products ) {
                                            last_view_products_current.shift();
                                        }
                                    }

                                </script>
                                <script>
                                    $(document).ready(function() {
                                        var num_show_item = 5;
                                        var last_view_products = JSON.parse(localStorage.getItem('last_view_products'));

                                        if( ! last_view_products ) {
                                            $(document).ready(function() {
                                                $('.product-viewed').hide();
                                            });
                                            last_view_products = [];
                                            return;
                                        }
                                        var	display = 5;

                                        var display_items = last_view_products.length < display ? last_view_products.length : display;
                                        var item_idx = 1;

                                        while( item_idx <= display_items ) {
                                            var handle = last_view_products[last_view_products.length -  item_idx];
                                            Haravan.getProduct( handle, function( product ) {
                                                if(product){
                                                    var img_url = product.images[1];
                                                    var item_product = $('#clone-item .item').clone().removeClass('hidden').appendTo('#owl-spdx');
                                                    item_product.find('.product-name').html(product.variants[0].sku);
                                                    item_product.find('.products-block-image img').attr('src',Haravan.resizeImage(img_url, 'medium'));
                                                    item_product.find('a').attr('href', product.url);
                                                    item_product.find('.pro-price-recent').html(Haravan.formatMoney(product.price,"{{amount}}₫" ));
                                                    item_product.find('.pro-price-compare-recent').html(Haravan.formatMoney(product.compare_at_price,"{{amount}}₫" ));
                                                }
                                            });
                                            item_idx++;
                                        }

                                        //$('#clone-item').remove();

                                        //Carousel/////////////////////////////////////////

                                        setTimeout(function() {
                                            var owl_1 = $("#owl-spdx").owlCarousel({
                                                autoPlay: true,
                                                items : 5
                                            });
                                            $(".spdx-next").click(function(){
                                                owl_1.trigger('owl.next');
                                            });
                                            $(".spdx-prev").click(function(){
                                                owl_1.trigger('owl.prev');
                                            });
                                            $('.pro-price-compare-recent').each(function() {
                                                var val = parseInt($(this).html());
                                                if(val == 0) {
                                                    $(this).remove();
                                                }
                                            });
                                        }, 2000);

                                    });

                                </script>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-10 col-xs-10 info-product-right hidden-sm hidden-xs " style="padding-left:0px;padding-right:15px">
                                <div id="detail-two" class="">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding product" style="padding-left:0px;">
                                        <h1>Túi xách trung khóa bấm kim loại hình tam giác TXT123</h1>
                                        <h4>Mã SP : TXT123</h4>
                                        <div class="wrapper-rightInfo">
                                            <div class="price">
                                                <label class="variant-price">750,000<sup>đ</sup></label>
                                            </div>
                                            <div id="show-bonus"></div>
                                            <form action="/cart"  class="product-form">
                                                <div class="clearfix"></div>
                                                <div>
                                                    <div class="product-btn-buy-2">
                                                        <a href="javascript:void(0)" title="Đặt mua" class="scroll_buy">MUA NGAY</a>
                                                    </div>
                                                    <div class="product-btn-buy-2" style="display: none">
                                                        <button href="" title="Đặt mua" class="add">THÊM VÀO GIỎ HÀNG</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="hidden deliver-top-no deliver-phone">
                                                <div class="tit">Hỗ trợ mua nhanh</div>
                                                <div class="tit-color">1800 1162</div>
                                                <div class="descrip">Mã SP: TXT123</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-xs-10 no-padding deliver-module fix-scroll">
                                        <div class="item deliver-top-support">
                                            <p style="font-family:Roboto-Regular;">Gọi tổng đài để mua nhanh hơn</p>
                                            <p>
                                                <a class="call_easyshopping" href="javascript:;" title="Dễ dàng mua sắm">
                                                    1800 1162</a>
                                                <b>(8:30 - 21:30)</b> <span>mỗi ngày</span>
                                            </p>
                                        </div>
                                        <div class="item deliver-top-info hidden">
                                            <p><b>Sẽ có tại nhà bạn</b> từ 1-5 ngày làm việc</p>
                                        </div>
                                        <div class="item deliver-top-on">
                                            <p class="">
                                                <a class="call_freeshipping" href="javascript:;" title="Miễn phí giao hàng toàn quốc">
                                                    Miễn phí giao hàng toàn quốc
                                                </a> (Sản phẩm trên 300,000đ)
                                            </p>
                                        </div>
                                        <div class="item deliver-top-payment">
                                            <p>
                                                <a class="call_easypayment" href="javascript:;" title="Thanh toán tiện lợi">
                                                    Thanh toán tiện lợi
                                                </a> (nhiều hình thức thanh toán)
                                            </p>
                                        </div>
                                        <div class="item deliver-top-policy">
                                            <p>
                                                <a class="call_easyremind" href="javascript:;" title="Đổi trả dễ dàng">
                                                    Đổi trả dễ dàng
                                                </a>
                                                (Giày 90 ngày, Túi 30 ngày)
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
         </section>
         <!--popup content--> 
         <div id="myModal" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
            <div class="modal-dialog modal-dialog-size">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Cách tính size giày</h4>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-md-10 col-lg-10">
                           <div class="content-huongdan">
                              <div class="title-huongdan">
                                 Hướng dẫn cách đo chân
                              </div>
                           </div>
                           <div style="width:100%;color:#333;text-align:center">
                              <img src="//file.hstatic.net/1000003969/file/sizegiay.png"/>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                  </div>
               </div>
            </div>
         </div>
         <div id="myModalmultistock" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
            <div class="modal-dialog modal-dialog-size-2" >
               <!-- Modal content-->
               <div class="modal-content" style="height:670px">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Các cửa hàng còn sản phẩm này</h4>
                  </div>
                  <div class="modal-body">
                     <div class="detail-product">
                        <div class="thongtin product">
                        </div>
                        <div class="titlemt">Chọn Màu để xem cửa hàng còn sản phẩm này</div>
                        <div class="col-lg-10 col-md-10 col-sm-10 product-form-multi" style="margin-top:10px;">
                           <div class="col-lg-10 col-md-10 col-sm-10"  style="padding:0px">
                              <span  class="col-lg-1 col-md-1 col-sm-1 titlemt"  style="margin-top:7px">Màu  </span>
                              <span class="col-lg-9 col-md-9 col-sm-9 chonmau" style="display:inline-block">
                                 <ul class="option option1 option1multi">
                                 </ul>
                              </span>
                           </div>
                        </div>
                     </div>
                     <div class="chontinhthanh">
                        <select id="tinhthanh" style="width: 100%;height:40px;font-weight: bold;color: #333;border-color: #dcdbdb;margin-top:10px">
                        </select>
                     </div>
                     <div id="stock-box" style="margin-top:10px">
                     </div>
                     <hr>
                     <div style="display:none;margin-top: 15px;">
                        <div style="text-align:center;font-family:'Roboto-Light';font-size:13px;color:#000;padding-top:10px;padding-bottom:10px;text-transform:uppercase">
                           <span>Mời bạn đến cửa hàng để mua hoặc<span style="color:#337ab7;font-weight:800;"> "Bấm nút" </span>dưới đây để được giao hàng tại nhà</span>
                        </div>
                        <div class="product-btn-buy-3">
                           <a href="" title="Đặt mua" class="add addnowmulti">Đặt hàng giao tại nhà</a>
                        </div>
                        <div style="text-align:center;font-family:'Roboto-Light';font-size:12px;color:#000;padding-top:10px">
                           <span>Gọi mua hàng<span style="font-size:16px;color:#cc0000;font-weight:800;"> 1800 1162 </span> Từ <span style="font-weight:700;">8:30 - 21:30 </span>mỗi ngày</span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="myModalvideo" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Video sản phẩm</h4>
                  </div>
                  <div class="modal-body">
                     <div class="out-video">
                        <div>
                           Chưa có video về sản phẩm này
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="myModalbaoquan" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
            <div class="modal-dialog">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Hướng dẫn bảo quản</h4>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-lg-10 col-md-10 popup-baoquan">
                           <p><strong>- Cách vệ sinh túi/ ví</p>
                           </strong>
                           <br/>
                           <p>Để làm sạch túi/ ví da, bạn hãy dùng khăn ẩm lau nhẹ nhàng bề mặt túi. Đối với những chất bẩn bám dính, hãy thoa một chút dầu gội lên giẻ lau và lau thật nhẹ nhàng cho đến khi vết bẩn mất hẳn, lau lại bằng khăn ẩm, sau đó dùng khăn mềm lau khô trở lại. </p>
                           <p>Để túi/ví được bền lâu, bạn nhớ hạn chế làm ướt túi/ví. Khi túi/ví bị ướt, không nên hơ bằng lửa, phơi nắng hoặc dùng máy sấy nóng, sẽ làm túi/ ví bị co lại hoặc hỏng bề mặt da. Hãy dùng giấy khô vò lại đặt vào trong túi/ ví để hút ẩm, phơi túi/ ví ở nơi râm mát. Sau khi khô, với túi/ í da thì nên thoa vaseline, đánh si để da mềm và bóng đẹp trở lại (không áp dụng với túi bằng da lộn).</p>
                           <br/>
                           <p><strong>- Bảo quản túi khi không sử dụng</p>
                           </strong>
                           <br/>
                           <p>Khi bạn mua túi tại Juno, túi sẽ được đặt trong một túi vải  kèm với túi chống ẩm và một ít giấy vụn bên trongi. Khi không sử dụng, hãy nhét giấy vụn vào bên trong túi để giữ cho dáng túi luôn chuẩn, đẹp. Sau đó đặt túi vào túi vải và túi chống ẩm, treo hoặc đặt ở nơi thoáng mát. </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="myModalcanvas" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
            <div class="modal-dialog  modal-dialog-size">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <h4 class="modal-title">Chi tiết chương trình tặng túi canvas</h4>
                  </div>
                  <div class="modal-body">
                     <div class="row">
                        <div class="col-lg-10 col-md-10 popup-baoquan">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="BuySuccess" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
            <div class="modal-dialog cart-desktop__bg">
               <div class="modal-content">
                  <div class="modal-header">
                     <button class="button cart-desktop__close" data-dismiss="modal" aria-label="Close">X</button>
                     <p class="cart-desktop__status text-center">
                        <i class="fa fa-check" aria-hidden="true"></i> Bạn vừa chọn mua <a href="/products/tui-xach-trung-txt123" class="cart-desktop__status_item">Túi xách trung khóa bấm kim loại hình tam giác TXT123</a>
                     </p>
                  </div>
                  <div class="modal-body">
                     <div class="cart-desktop__tfooter">
                        <div class="cart-desktop__tfooter-action clearfix">
                           <a class="cart-desktop__tfooter-checkout" title="Xem giỏ hàng và thanh toán" href="/cart">Xem giỏ hàng và thanh toán</a>
                           <a class="cart-desktop__tfooter-continue" title="Mua thêm sản phẩm khác" href="https://juno.vn">Mua thêm sản phẩm khác</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--script product-->
         <script>
            $('.call_easyshopping').click(function(e){
            	e.preventDefault();
            	$('#easy_payment .modal-title').html($(this).attr('title'));							
            	$('#easy_payment .modal-body').html('<iframe width="100%" height="550" src="//player.vimeo.com/video/254462534?autoplay=1&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            	$('#easy_payment').modal('show');
            });
            $('.call_freeshipping').click(function(e){
            	e.preventDefault();
            	$('#easy_payment .modal-title').html($(this).attr('title'));							
            	$('#easy_payment .modal-body').html('<iframe width="100%" height="550" src="//player.vimeo.com/video/254610972?autoplay=1&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            	$('#easy_payment').modal('show');
            });
            $('.call_easypayment').click(function(e){
            	e.preventDefault();
            	$('#easy_payment .modal-title').html($(this).attr('title'));							
            	$('#easy_payment .modal-body').html('<iframe width="100%" height="550" src="//player.vimeo.com/video/254455774?autoplay=1&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            	$('#easy_payment').modal('show');
            })
            $('.call_easyremind').click(function(e){
            	e.preventDefault();
            	$('#easy_payment .modal-title').html($(this).attr('title'));							
            	$('#easy_payment .modal-body').html('<iframe width="100%" height="550" src="//player.vimeo.com/video/254460104?autoplay=1&title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
            	$('#easy_payment').modal('show');
            })
            $('#easy_payment .close, #easy_payment').click(function(e){
            	e.preventDefault();
            	$('#easy_payment .modal-title').html('');
            	$('#easy_payment .modal-body').html('');
            });
         </script>
         <script>
            var inventory_quantity_numb = 2;
            $(window).scroll(function() {
            	$("#detail-two").affix({
            		offset: {
            			/*top: $("#detail-product").outerHeight(true)+ 150,
            			bottom:$("#more-sp-detail").offset().top - $('#more-sp-detail').height() - $('.sanphamdaxem').height() - 200*/
            			top: $("#descriptionproduct").offset().top,
            			bottom:$("#more-sp-detail").offset().top - $('#more-sp-detail').height() - $('.sanphamdaxem').height() - 200
            		} 
            	});
            });
            $('.scroll_buy').click(function(e){
            	$("html, body").animate({ scrollTop: $('#detail-product').offset().top }, 800);
            });
            var productprices=75000000/100;
            var tinhthanh='';
            var url = window.location.href;fbq 
            var urlsplit=url.split('#');
            var colorurl='';
            var slug = function(str) {
            	if(str){
            		str = str.toLowerCase();
            		str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            		str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            		str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            		str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            		str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            		str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            		str = str.replace(/đ/g, "d");
            		str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
            		str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1- 
            		str = str.replace(/^\-+|\-+$/g, "");
            		return str;
            	}
            }
            fbq('track', 'ViewContent');
            fbq('trackCustom','PageViewProduct',{
            	content_name: 
            	'Túi xách trung khóa bấm kim loại hình tam giác TXT123',
            	value:productprices,
            	content_category: 'Sản phẩm áp dụng Giao nhanh 2h - Juno flash', 
            	content_ids:['1011812654'],
            	content_type:'product'
            });
            if(urlsplit.length>1){
            	var colorurla=urlsplit[1].split('?');
            	colorurl=colorurla[0];
            }
            var colorsellect;
            var sizesellect;
            var colorcheck='';
            var colorcheckmt='';
            var color_hiden='';
            var color_hiden_split=color_hiden.split('##');
            var size_hiden='';
            var size_hiden_split=size_hiden.split('##');
            console.log(color_hiden);
            getcolor();
            var hiden_color=[];
            for( var c=0; c< color_hiden_split.length;c++ ) {
            	hiden_color.push(color_hiden_split[c]);
            }
            var hiden_size=[];
            for( var s=0; s< size_hiden_split.length;s++ ) {
            	hiden_size.push(size_hiden_split[s]);
            }
            productjson ={"available":true,"compare_at_price_max":0.0,"compare_at_price_min":0.0,"compare_at_price_varies":false,"compare_at_price":0.0,"content":null,"description":"<p><span style=\"font-size: 16px;\" data-mce-style=\"font-size: 16px;\">- Túi dạng trung, kiểu dáng đơn giản và tiện dụng</span><br></p><p><span style=\"font-size: 16px;\" data-mce-style=\"font-size: 16px;\">- Khóa bấm bằng kim loại hình tam giác cách điệu, đẹp mắt và sang chảnh</span></p><p><span style=\"font-size: 12pt;\" data-mce-style=\"font-size: 12pt;\">- Có 1 ngăn chính bên trong cho bạn thoải mái mang theo bên mình những vật dụng cần thiết như điện thoại, số tay, ví....</span></p><p><span style=\"font-size: 12pt;\" data-mce-style=\"font-size: 12pt;\">- Chất liệu da tổng hợp cao cấp, dễ vệ sinh, bền đẹp</span></p><p><span style=\"font-size: 12pt;\" data-mce-style=\"font-size: 12pt;\">- Có kèm thêm dây xích kim loại giúp bạn dễ dàng thay đổi phong cách&nbsp;</span></p><p><span style=\"font-size: 12pt;\" data-mce-style=\"font-size: 12pt;\">- Thích hợp đi tiệc, dạo phố, đi làm... kết hợp với bất kể loại trang phục nào cũng giúp bạn luôn nổi bật và đầy tự tin</span></p>","featured_image":"https://product.hstatic.net/1000003969/product/xanh-duong_txt123_1.jpg","handle":"tui-xach-trung-txt123","id":1011812654,"images":["https://product.hstatic.net/1000003969/product/xanh-duong_txt123_1.jpg","https://product.hstatic.net/1000003969/product/xanh-duong_txt123_2.jpg","https://product.hstatic.net/1000003969/product/xanh-duong_txt123_3.jpg","https://product.hstatic.net/1000003969/product/xanh-duong_txt123_4.jpg","https://product.hstatic.net/1000003969/product/xanh-duong_txt123_5.jpg","https://product.hstatic.net/1000003969/product/xanh-duong_txt123_6.jpg","https://product.hstatic.net/1000003969/product/trang_txt123_1.jpg","https://product.hstatic.net/1000003969/product/trang_txt123_3.jpg","https://product.hstatic.net/1000003969/product/trang_txt123_5.jpg","https://product.hstatic.net/1000003969/product/trang_txt123_6.jpg","https://product.hstatic.net/1000003969/product/trang_txt123_4.jpg","https://product.hstatic.net/1000003969/product/trang_txt123_2.jpg","https://product.hstatic.net/1000003969/product/den_txt123_1.jpg","https://product.hstatic.net/1000003969/product/den_txt123_2.jpg","https://product.hstatic.net/1000003969/product/den_txt123_6.jpg","https://product.hstatic.net/1000003969/product/den_txt123_3.jpg","https://product.hstatic.net/1000003969/product/den_txt123_4.jpg","https://product.hstatic.net/1000003969/product/den_txt123_5.jpg"],"options":["Màu sắc"],"price":75000000.0,"price_max":75000000.0,"price_min":75000000.0,"price_varies":false,"tags":["label:runfast","chatlieu: Da tổng hợp","danhgia:5","kichco: 18.5cmx15.5cmx9.5cm","kieu: Túi xách thời trang","tui-co-trung","mausac: Đen-Trắng-Xanh dương","tui-xach"],"template_suffix":null,"title":"Túi xách trung khóa bấm kim loại hình tam giác TXT123","type":"Túi xách trung","url":"/products/tui-xach-trung-txt123","pagetitle":"Túi xách trung khóa bấm kim loại hình tam giác TXT123","metadescription":"Túi xách trung khóa bấm kim loại hình tam giác TXT123 thiết kế đơn giản, tiện dụng nhưng vẫn thời trang mang lại nét nổi bật cho bạn khi diện, tui xach trung","variants":[{"id":1022737092,"barcode":"1402000000327","available":true,"price":75000000.0,"sku":"TXT123","option1":"Xanh dương","option2":"","option3":"","options":["Xanh dương"],"inventory_quantity":24,"old_inventory_quantity":243,"title":"Xanh dương","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":{"id":1070639837,"created_at":"0001-01-01T00:00:00","position":1,"product_id":1011812654,"updated_at":"0001-01-01T00:00:00","src":"https://product.hstatic.net/1000003969/product/xanh-duong_txt123_1.jpg","variant_ids":[1022737092]}},{"id":1022737091,"barcode":"1402000000326","available":true,"price":75000000.0,"sku":"TXT123","option1":"Trắng","option2":"","option3":"","options":["Trắng"],"inventory_quantity":88,"old_inventory_quantity":313,"title":"Trắng","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":{"id":1070639822,"created_at":"0001-01-01T00:00:00","position":7,"product_id":1011812654,"updated_at":"0001-01-01T00:00:00","src":"https://product.hstatic.net/1000003969/product/trang_txt123_1.jpg","variant_ids":[1022737091]}},{"id":1022737090,"barcode":"1402000000325","available":true,"price":75000000.0,"sku":"TXT123","option1":"Đen","option2":"","option3":"","options":["Đen"],"inventory_quantity":72,"old_inventory_quantity":201,"title":"Đen","weight":0.0,"compare_at_price":0.0,"inventory_management":"haravan","inventory_policy":"deny","selected":false,"url":null,"featured_image":{"id":1070639855,"created_at":"0001-01-01T00:00:00","position":13,"product_id":1011812654,"updated_at":"0001-01-01T00:00:00","src":"https://product.hstatic.net/1000003969/product/den_txt123_1.jpg","variant_ids":[1022737090]}}],"vendor":"Juno","published_at":"2018-01-29T02:58:38.509Z","created_at":"2018-01-17T03:46:38.721Z"};
            /*console.log(productjson);*/
            
            if(productjson.options.length>1){
            	var checked='';
            	var checkedbool=false;
            	var colors='';
            	var boolcolor=false;
            	var colorchecked='';
            	for( var k=0; k<productjson.variants.length; k++ ) {
            		if( productjson.variants[k].inventory_quantity > inventory_quantity_numb && /*productjson.variants[k].old_inventory_quantity >0 &&*/ $.inArray(productjson.variants[k].option2,hiden_color) == -1 ) {
            			var colors2=colors.split('-');
            			for( var c=0;c<colors2.length-1;c++ ) {
            				if( colors2[c] == productjson.variants[k].option2 ) {
            					boolcolor=true;         
            					break;          
            				}
            			}
            			if( boolcolor==false ){
            				if( productjson.variants[k].featured_image.src == '' ) {
            					$('.product-form ul.option2').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+' type="radio" value="'+productjson.variants[k].option2+'" name="colorkm"/><span>'+productjson.variants[k].option2+'</span></label></li>');
            					$('.product-form-multi ul.option2multi').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+' type="radio" value="'+productjson.variants[k].option2+'" name="colorkmmt"/><span>'+productjson.variants[k].option2+'</span></label></li>');
            				} 
            				else {
            					var urlimage=productjson.variants[k].featured_image.src.split('.');
            
            					if( urlimage.length <4) {
            						urlimage=urlimage[0]+'.'+urlimage[1]+'_icon.'+urlimage[2];
            					}
            					if( urlimage.length == 4) {
            						urlimage=urlimage[0]+'.'+urlimage[1]+'.'+urlimage[2]+'_icon.'+urlimage[3];
            					}
            					if(urlimage.includes('http:') ){
            						urlimage=urlimage.split('http:');
            						urlimage=urlimage[1];
            					}
            
            
            					$('.product-form ul.option2').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+'  type="radio" value="'+productjson.variants[k].option2+'" name="colorkm"/><span><img src="'+urlimage+'" />'+productjson.variants[k].option2+'</span></label></li>');
            					$('.product-form-multi ul.option2multi').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+'  type="radio" value="'+productjson.variants[k].option2+'" name="colorkmmt"/><span><img src="'+urlimage+'" />'+productjson.variants[k].option2+'</span></label></li>');
            				}
            			}
            			boolcolor=false;
            			colors=colors+productjson.variants[k].option2+'-';
            		}
            	}
            	$('.product-form ul.option2 li input[name="colorkm"]').each(function(){
            		$(this).attr('checked','checked');
            		return false;
            	});
            	getcolor();
            	var checked2 = '';
            	var checkedbool2 = false;
            	for( var k=0;k<productjson.variants.length;k++ ) {
            		var titile_size=productjson.variants[k].option1+"#"+productjson.variants[k].option2;
            		if(productjson.variants[k].inventory_quantity> inventory_quantity_numb && $.inArray(titile_size,hiden_size)==-1) {
            			if(checkedbool2 == false){
            				checked2 = 'checked=checked';
            				checkedbool2 = true ;
            			}
            			else{
            				checked2 = '';
            			}
            			$('.product-form ul.option1').append('<li class= "size '+slug(productjson.variants[k].option2)+'"><label><input data-productid="'+productjson.variants[k].id+'" ' + checked2 + ' value="'+productjson.variants[k].option1+'" type="radio" name="sizekm"/><span>'+productjson.variants[k].option1+'</span></label></li>');
            			$('.product-form-multi ul.option1multi').append('<li class= "sizemt '+slug(productjson.variants[k].option2)+'"><label><input data-productid="'+productjson.variants[k].id+'" ' + checked2 + ' value="'+productjson.variants[k].option1+'" type="radio" name="sizekmmt"/><span>'+productjson.variants[k].option1+'</span></label></li>');
            		}
            	}
            	checksizeforcolor(colorcheck);} 
            else {
            	var checked='';
            	var check_variants = false
            	var checkedbool=false;
            	var colors='';
            	var boolcolor=false;
            	for( var k=0;k<productjson.variants.length;k++ ){
            		if(productjson.variants[k].inventory_quantity> inventory_quantity_numb && $.inArray(productjson.variants[k].option1,hiden_color) == -1) {
            			var colors2=colors.split('-');
            
            			for(var c=0;c<colors2.length-1;c++) {
            				if(colors2[c] == productjson.variants[k].option1) {
            					boolcolor=true;
            					break;
            				}
            			}
            			if(boolcolor==false){
            				if(productjson.variants[k].featured_image.src == '') {
            					$('.product-form ul.option1').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+' type="radio" value="'+productjson.variants[k].option1+'" name="colorkm"/><span>'+productjson.variants[k].option1+'</span></label></li>');
            					$('.product-form-multi ul.option1multi').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+' type="radio" value="'+productjson.variants[k].option1+'" name="colorkmmt"/><span>'+productjson.variants[k].option1+'</span></label></li>');
            				} 
            				else {
            					var urlimage=productjson.variants[k].featured_image.src.split('.');
            					/*console.log(urlimage); */
            					if(urlimage.length <4) {
            						urlimage=urlimage[0]+'.'+urlimage[1]+'_icon.'+urlimage[2];
            					}
            					if(urlimage.length == 4) {
            						urlimage=urlimage[0]+'.'+urlimage[1]+'.'+urlimage[2]+'_icon.'+urlimage[3];
            					}
            					if(urlimage.indexOf('http:') > 0 ){
            						urlimage=urlimage.split('http:');
            						urlimage=urlimage[1];
            					}
            					$('.product-form ul.option1').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+'  type="radio" value="'+productjson.variants[k].option1+'" name="colorkm"/><span><img src="'+urlimage+'" />'+productjson.variants[k].option1+'</span></label></li>');
            					$('.product-form-multi ul.option1multi').append('<li><label class="color"><input data-productid="'+productjson.variants[k].id+'" '+checked+'  type="radio" value="'+productjson.variants[k].option1+'" name="colorkmmt"/><span><img src="'+urlimage+'" />'+productjson.variants[k].option1+'</span></label></li>');
            				}
            			}
            			boolcolor=false;
            			colors=colors+productjson.variants[k].option1+'-';
            		}
            	}
            	$('.product-form ul.option1 li input[name="colorkm"]').each(function(){
            		$(this).attr('checked','checked');
            		return false;
            	});}
            function getcolor(){
            	$('input[name="colorkm"]').each(function() {
            		if($(this).attr('checked')=='checked') {
            			colorchecked = this.value;
            			colorcheck = this.value;
            		}
            	});
            }
            function checkedsize(){
            	$('input[name="sizekm"]').each(function() {
            		$(this).removeAttr('checked');
            	});
            	$('li.displayshow label input[name="sizekm"]').each(function(){
            		$(this).attr('checked','checked');
            		return false;
            	});
            }
            function checksizeforcolor(color){
            	$('li.size').each(function() {
            		if($(this).hasClass(color))
            		{
            			$(this).removeClass('displaynone');
            			$(this).addClass('displayshow');
            		}
            		else{
            			$(this).addClass('displaynone');
            			$(this).removeClass('displayshow');
            		}
            	});
            }
            /////////////////////////////click chọn màu////////////////////////////////////////////////
            $('input[name="colorkm"]').click(function(){
            	$('input[name="colorkm"]').each(function() {
            		$(this).removeAttr('checked'); 
            	});
            	$(this).attr('checked', 'checked');
            	getcolor();
            	colorsellect=$(this).attr('data-productid');
            	$('#product-select').html('<option selected="selected" value="'+colorsellect+'">'+colorsellect+'</option>');
            	debugger
            	if(productjson.options.length>1) {
            		checksizeforcolor(slug(colorcheck));
            		var urlimage;
            		for(var k=0;k<productjson.variants.length;k++){
            			if(productjson.variants[k].inventory_quantity> inventory_quantity_numb){
            				if(productjson.variants[k].option2 == colorcheck){
            					urlimage=productjson.variants[k].featured_image.src.split('.')
            					urlimage=urlimage[0]+'.'+urlimage[1]+'_medium.'+urlimage[2];
            					urlimage=urlimage.split('http:');
            					urlimage=urlimage[1];
            				}
            
            			}
            		}
            		checkedsize();
            		switchSlide(slug($('ul.option2 li input[checked=checked]').val()));
            
            	} else {
            		var checked2 = '';
            		var checkedbool2 = false;
            		var urlimage;
            		for(var k=0;k<productjson.variants.length;k++){
            			if(productjson.variants[k].inventory_quantity> inventory_quantity_numb){
            				if(productjson.variants[k].option1 == colorcheck){
            					urlimage=productjson.variants[k].featured_image.src.split('.')
            					urlimage=urlimage[0]+'.'+urlimage[1]+'_medium.'+urlimage[2];
            					urlimage=urlimage.split('http:');
            					urlimage=urlimage[1];
            				}
            
            			}
            		}
            		switchSlide(slug($('ul.option1 li input[checked=checked]').val()));
            	}
            	$("a[rel^='lightbox']").picbox({}, null, function(el) {
            		/*console.log(el);*/
            		return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
            	});
            });
            $('input[name="sizekm"]').click(function(){
            	$('input[name="sizekm"]').each(function() {
            		$(this).removeAttr('checked');
            	});
            	$(this).attr('checked', 'checked');
            	$('#product-select').html('');
            	sizesellect=$('ul.option1 input[checked="checked"]').attr('data-productid');
            	$('#product-select').html('<option selected="selected" value="'+sizesellect+'">'+sizesellect+'</option>');
            });
            var owl, soption = {
            	items: 1,
            	navigation: true,
            	pagination:false,
            	navigationText: ["", ""],
            	transitionStyle : "fade",
            	singleItem: true,
            	afterInit: function (elem) {
            		var current = this.currentItem;
            
            	},
            	afterMove: function (elem) {
            		var current = this.currentItem;
            	}
            };
            var switchSlide = function(opt){
            	
            	
            	/*console.log("1");*/
            	
            	/*console.log("0");*/
            	var imagelast='';
            	 if($("#slide-image").data('owlCarousel')) {
            		 owl.data('owlCarousel').destroy();
            	 }
            	 $("#slide-image").html('');
            	 $('#slide-image .item').remove();
            	 $('.images .item[data-option='+opt+']').clone().appendTo('#slide-image');
            	 $('.thumbnails .row .thumbnail').remove();
            	 $('.thumbnails .row').html('');
            	 $('.thumbnails-hidden .thumbnail[data-option='+opt+']').clone().appendTo('.thumbnails .row');
            	 
            		owl = $("#slide-image").owlCarousel(soption);
            		//$("#slide-image").append('<div class="frame_tet"></div>');
            		}
            		if(productjson.options.length>1){
            			if(urlsplit.length>1){
            				$('input[name="colorkm"]').each(function() {
            					$(this).removeAttr('checked');
            				});
            				$('input[name="colorkm"]').each(function(){
            					if( slug($(this).val())==colorurl) {
            						$(this).attr('checked','checked');
            					}
            				});
            				checksizeforcolor(slug($('ul.option2 li input[checked=checked]').val()));
            				checkedsize();
            				if($('ul.option2 li input[checked=checked]').val() == null){
            					switchSlide(slug("Trắng"));
            				} else{
            					switchSlide(slug($('ul.option2 li input[checked=checked]').val()));
            				}
            
            				colorurl='';
            			} 
            			else {
            
            				$('input[name="colorkm"]').each(function() {
            					$(this).removeAttr('checked');
            				});
            				$('input[name="colorkm"]').each(function(){
            					$(this).attr('checked','checked');
            					return false;
            				});
            				getcolor();
            				checksizeforcolor(slug($('ul.option2 li input[checked=checked]').val()));
            				checkedsize();
            				if($('ul.option2 li input[checked=checked]').val() == null){
            					switchSlide(slug("Trắng"));
            				} else {
            					switchSlide(slug($('ul.option2 li input[checked=checked]').val()));
            
            				}
            			}
            		} 
            		else {
            			if( urlsplit.length>1 ){
            				$('input[name="colorkm"]').each(function() {
            					$(this).removeAttr('checked');
            				});
            				$('input[name="colorkm"]').each(function(){
            					if( slug($(this).val())==colorurl)
            					{
            						$(this).attr('checked','checked');
            					}
            				});
            				if( $('ul.option1 li input[checked=checked]').val() == null ){
            					switchSlide(slug("Trắng"));
            				} else{
            					switchSlide(slug($('ul.option1 li input[checked=checked]').val()));
            				}
            				colorurl='';
            			} 
            			else{
            				if( $('ul.option1 li input[checked=checked]').val() == null ){
            					switchSlide( slug("Trắng") );
            				} else {
            					switchSlide( slug( $('ul.option1 li input[checked=checked]').val() ) );
            				}
            			}
            		}
            		$('body').on('click', '.thumbnails .thumbnail',  function(){
            			$('.thumbnails .thumbnail').removeClass('active');
            			$(this).addClass('active');
            			
            			owl.trigger('owl.goTo', $(this).index());
            			 
            				})
            				function postselect(){
            					$('#product-select').html('');
            					if(productjson.options.length>1) {
            						colorsellect=$('ul.option2 input[checked="checked"]').attr('data-productid');
            						sizesellect=$('ul.option1 input[checked="checked"]').attr('data-productid');
            						$('#product-select').html('<option selected="selected" value="'+sizesellect+'">'+sizesellect+'</option>');
            					} else {
            						colorsellect=$('ul.option1 input[checked="checked"]').attr('data-productid');
            						$('#product-select').html('<option selected="selected" value="'+colorsellect+'">'+colorsellect+'</option>');
            					}
            				}
            				$('.disable_btn').click(function(e){
            					e.preventDefault();
            					$('#product-form').attr("action", "javascript:void(0);"); 
            				});
            				
            
            
            
            				var numcolor=0;
            				$('input[name="colorkm"]').each(function() {
            					numcolor++;
            				});
            				if(numcolor<=0) {
            					$('.product-btn-buy,.product-btn-buy-2').html('<button href="javascript:void(0)" disabled="disabled" class="disable_btn">HẾT HÀNG<br><div>Vui lòng quay lại sau</div></button>');
            					$('.addnow').attr('disabled', 'disabled');
            					$('.variant-compare-price').hide();
            					$('.variant-coupon').hide();
            				}
            				$(window).load(function() {
            					if($('#product-select').val() == null){
            						sizesellect=$('ul.option1 input[checked="checked"]').attr('data-productid');
            						$('#product-select').html('<option selected="selected" value="'+sizesellect+'">'+sizesellect+'</option>');
            					}
            					if(productjson.options.length>1){
            						var colorcheckkk='';
            						$('input[name="colorkm"]').each(function() {
            							if($(this).attr('checked')=='checked'){
            								colorcheckkk = this.value;
            							}
            						});
            					}
            					else{
            						var colorcheckkk='';
            						$('input[name="colorkm"]').each(function() {
            							if($(this).attr('checked')=='checked'){
            								colorcheckkk = this.value;
            							}
            						});
            					}
            				});
            				
         </script>
         < <div class="testimonials-wrapper vietstar container-fluid " style="border-top: 1px solid #f8f8f8;">
            <div class="container" >
               <div class="row">
                  <div class="testimonials">
                     <div class="new_title center">
                        <div class="col-md-10 col-lg-10 padding-none title-y-kien" >
                           <div>
                              <span class="ta title-mobile-juno">SAO VIỆT "ĐỔ BỘ" RA JUNO MUA GIÀY, TÚI NHÂN NGÀY 8/3 VÌ SỢ HẾT MẪU ĐẸP</span>
                           </div>
                           <div>
                              <span class="tb">Hơn 10,000 phụ nữ được phục vụ mỗi ngày tại JUNO</span>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-10 new-item">
                        <div class="slider-testimonials owl-carousel">
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="MC Phan Anh" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/phananh-juno-8-3_large.jpg" alt="">
                                    <div class="name">
                                       <i>MC Phan Anh</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Diễn viên Quyền Linh" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/quyenlinh-juno-8-3_large.jpg" alt="">
                                    <div class="name">
                                       <i>Diễn viên Quyền Linh</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Ca sĩ Gil Lê" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/gille-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>Ca sĩ Gil Lê</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="MC Hoàng Oanh" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/mchoangoanh-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>MC Hoàng Oanh</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Diễn viên Diệu Nhi" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/dieunhi-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>Diễn viên Diệu Nhi</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Diễn viên Hương Giang" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/huonggiang-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>Diễn viên Hương Giang</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>


                  </div>
               </div>
            </div>
         </div>
         <script>
            $(document).ready(function(){
               $('.slider-testimonials').owlCarousel({
                  autoPlay: 4500, //Set AutoPlay to 3 seconds
                  pagination: false,
                  navigation: false,
                  loop:true,
                  lazyLoad : true,
                  navigationText: false,
                  items: 4, 
                  itemsTablet: 4,
                  itemsMobile:1,
               });
            
            });
         </script>
         <div class="hidden bottom-content container">
            <div class="row">
               <div class="col-xs-10 text-center">
                  <p class="one">còn rất nhiều sản phẩm khác tại hệ thống cửa hàng</p>
                  <p class="two"><a href="/collections/cua-hang-khu-vuc-tp-ho-chi-minh?view=stores">Ấn xem địa chỉ hệ thống 68 cửa hàng</a></p>
               </div>
            </div>
         </div>
         <div class="bottom-content container">
            <div class="row">
               <div class="col-xs-10 text-center mov_to_store">
                  <p class="title title-collection-middle title-mobile-juno">Đặt hàng online giao hàng toàn quốc hoặc ra hệ thống 68 cửa hàng </p>
                  <p class="title-collection-bottom">
                     <a>Click vào hình ảnh để xem vị trí cửa hàng</a>
                  </p>
               </div>
            </div>
         </div>
         <div class="footer-map container-fluid wow fadeIn" style="background:transparent url('//file.hstatic.net/1000003969/file/bg-ch-salebf2017.jpg') repeat scroll 0 0">
            <div class="row">
               <div class=" col-lg-10 col-md-10 button-store" style="margin: 140px 0;">
                  <a class="store-footer" target="_blank" href="http://juno.vn/collections/cua-hang-khu-vuc-tp-ho-chi-minh?view=stores">Mời bạn xem địa chỉ hệ thống 68 cửa hàng</a>
               </div>
            </div>
         </div>
         <footer >
            <div class="container-fluid 5icons" style="background:#e5e5e5">
               <div class="">
                  <div class="bottom">
                     <div class="container">
                        <div class="row">
                           <ul class="menu_footer">
                              <li>
                                 <a href="/collections/tui-xach" title="Túi xách">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_5.svg" alt="Túi xách" class="img-responsive" />
                                 <span class="label_footer">Túi/Ví</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-cao-got" title="Giày cao gót">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_2.svg" alt="Giày cao gót" class="img-responsive" />
                                 <span class="label_footer">Cao gót</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-xang-dan" title="Giày xăng đan">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_3.svg" alt="Giày xăng đan" class="img-responsive" />
                                 <span class="label_footer">Xăng đan</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-bup-be" title="Giày búp bê">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_1.svg" alt="Giày búp bê" class="img-responsive" />
                                 <span class="label_footer">Búp bê</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-boots" title="Giày Boots">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_4.svg" alt="Dép guốc" class="img-responsive" />
                                 <span class="label_footer">Boots</span>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-fluid signUp" style="background:#fff">
               <div class="">
                  <div class="container">
                     <div class="row" style="padding-top:20px;padding-bottom:20px">
                        <div class="wrap_foo_switchboard col-md-5 col-lg-5 col-sm-10 col-xs-10 juno-phone-mobile">
                           <div class="row">
                              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-10 no-padding item-p text-align-center">
                                 <div class="icon_phone">
                                    <img src="//theme.hstatic.net/1000003969/1000323463/14/icon_phone_circle.jpg?v=2220" />
                                 </div>
                                 <div class="phone_footer">
                                    <strong>Gọi mua hàng(08:30-21:30)</strong>
                                    <br>
                                    <span class="number_phone">1800 1162</span>
                                    <span class="moreinfo">Tất cả các ngày trong tuần</span>
                                 </div>
                              </div>
                              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-10 no-padding item-p text-align-center">
                                 <div class="icon_phone">
                                    <img src="//theme.hstatic.net/1000003969/1000323463/14/icon_phone_circle.jpg?v=2220" />
                                 </div>
                                 <div class="phone_footer">
                                    <strong>Góp ý, khiếu nại(08:30-20:30)</strong>
                                    <br>
                                    <span class="number_phone">1800 1160</span>
                                    <span class="moreinfo">Các ngày trong tuần ( trừ ngày lễ )</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wrap_foo_social col-md-5 col-lg-5 col-sm-10 col-xs-10 text-align-center">
                           <div class="wrapper_embed col-sm-5 col-xs-10 no-padding">
                              <div class="ttmail">
                                 <span>
                                 <strong>đăng ký nhận thông tin mới từ Juno</strong>
                                 </span>
                              </div>
                              <div id="mc_embed_signup" style="margin-bottom: 10px;">
                                 <form action="//juno.us14.list-manage.com/subscribe/post?u=6767720e97fc0e6e564ed78ed&amp;id=fbf6273ec0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">
                                       <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Nhập email của bạn tại đây..." required>
                                       <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                       <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                          <input type="text" name="b_c6a1ff4613972aee4c6da0254_380ebe08a5" tabindex="-1" value="">
                                       </div>
                                       <div class="clear">
                                          <input type="submit" value="Đăng ký" name="subscribe" id="mc-embedded-subscribe" class="button" style="background:#3c3c3c;border:1px solid #3c3c3c !important">
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                           <div class="social col-sm-5 col-xs-10 text-align-center-mobile text-align-center">
                              <p class="title-md-footer">
                                 <strong>Like Juno trên mạng xã hội</strong>
                              </p>
                              <ul class="navbar-social">
                                 <li class="social-face">
                                    <a href="https://www.facebook.com/giayjuno" target="_blank" rel="nofollow"> 
                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    </a>
                                 </li>
                                 <li> 
                                    <a href="https://www.instagram.com/juno.vn/" target="_blank" rel="nofollow">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                 </li>
                                 <li> 
                                    <a href="https://www.youtube.com/user/JunoShoesVn" target="_blank" rel="nofollow">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                    </a>
                                 </li>
                                 <li> 
                                    <a href="http://zalo.me/giayjuno" target="_blank" rel="nofollow">
                                    <i class="fa fa-zalo" aria-hidden="true"></i>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-fluid fooMenu" style="background:#f4f4f4">
               <div class="">
                  <div class="top">
                     <div class="container">
                        <div class="row">
                           <ul class="instuction_footer instuction_footer-mobile">
                              <li><a href="/blogs/tin-tuc-juno" title="Hướng dẫn chọn cỡ giày">Tin tức, khuyến mãi JUNO</a></li>
                              <li><a href="/pages/huong-dan-chon-size-giay" title="Hướng dẫn chọn cỡ giày">Hướng dẫn chọn cỡ giày</a></li>
                              <li><a href="/pages/huong-dan-mua-hang-online" title="Hướng dẫn mua hàng online">Hướng dẫn mua hàng online</a></li>
                              <li><a href="/pages/chinh-sach-khach-hang-than-thiet" title="Chính sách khách hàng thân thiết">Chính sách khách hàng thân thiết</a></li>
                              <li><a href="/pages/chinh-sach-doi-tra-va-hoan-tien" title="Chính sách đổi trả">Chính sách Đổi/Trả</a></li>
                              <li><a href="/pages/thanh-toan-giao-nhan" title="Thanh toán giao nhận">Thanh toán giao nhận</a></li>
                              <li class="hidden"><a href="/pages/chinh-sach-bao-mat" title="Chính sách bảo mật">Chính sách bảo mật</a></li>
                              <li><a href="/pages/gioi-thieu" title="Giới thiệu; Liên hệ...">Các thông tin khác</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-fluid wow fadeIn" style="background:#f4f4f4">
               <div class="">
                  <div class="copyright">
                     <div class="container">
                        <div class="row">
                           <div class=" copy">
                              <div class="col-xs-12 col-sm-6">
                                 © 2015 JUNO. Công ty cổ phần sản xuất thương mại dịch vụ JUNO.<br>
                                 Văn phòng: 313 Nguyễn Thị Thập, Q.7, TP.HCM. GP số 0310350452-002 do Sở Kế Hoạch và Đầu Tư TP.HCM cấp ngày 29/06/2011
                              </div>
                              <div class="col-xs-12 col-sm-2 col-sm-offset-2 gov">
                                 <a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=13954" target="_blank"><img src="//theme.hstatic.net/1000003969/1000323463/14/icon-dangky.png?v=2220"></a>
                                 <div class="text-center">
                                    <span class="pull-left"> Powered by <a target="_blank" href="https://www.haravan.com/?hchan=juno">Haravan Enterprise.</a></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <div class="back-to-top">
         <a href="javascript:void(0);">Top</a>
      </div>
      <div id="quickView" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5); z-index: 999999;">
         <div class="modal-dialog"></div>
      </div>
      <div id="myCart"    class="modal fade" role="dialog" >
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel">
                     Bạn có <b></b> sản phẩm trong giỏ hàng
                  </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span class="hrv-close-modal"></span>
                  </button>
               </div>
               <form action="/cart" method="post" id="cartform">
                  <div class="modal-body">
                     <table style="width:100%;" id="cart-table">
                        <tr>
                           <th></th>
                           <th>Tên sản phẩm</th>
                           <th>Số lượng</th>
                           <th>Giá tiền</th>
                           <th></th>
                        </tr>
                        <tr class="line-item original">
                           <td class="item-image"></td>
                           <td class="item-title">
                              <a></a>
                           </td>
                           <td class="item-quantity"></td>
                           <td class="item-price"></td>
                           <td class="item-delete text-center"></td>
                        </tr>
                     </table>
                  </div>
                  <div class="modal-footer">
                     <div class="row">
                        <div class="col-sm-5">
                           <div class="modal-note">
                              <textarea placeholder="Viết ghi chú" id="note" name="note" rows="5"></textarea>
                           </div>
                        </div>
                        <div class="col-sm-5">
                           <div class="total-price-modal">
                              Tổng cộng : <span class="item-total"></span>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="margin-top:10px;">
                        <div class="col-lg-5">
                           <div class="comeback">
                              <a href="/collections/all">
                              <i class="fa fa-caret-left mr10" ></i>Tiếp tục mua hàng
                              </a>
                           </div>
                        </div>
                        <div class="col-lg-5 text-right">
                           <div class="buttons btn-modal-cart">
                              <button type="submit" class="button-default" id="checkout" name="checkout">
                              Đặt hàng
                              <i class="fa fa-caret-right"></i>
                              </button>
                           </div>
                           <div class="buttons btn-modal-cart">
                              <button type="submit" class="hidden button-default" id="update-cart-modal" name="">
                              <i class="fa fa-caret-left"></i>
                              Cập nhật
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="noti_itemcount unactive"></div>
      


      <script src='js/script.js' type='text/javascript'></script>
      <script src='js/option_selection.js' type='text/javascript'></script>
      <script src='js/api.jquery.js' type='text/javascript'></script>
      <script src='js/owl.carousel.2.js' type='text/javascript'></script>
      <script src='js/jquery.lazyload.js' type='text/javascript'></script>
      <script src='js/lazysizes.min.js' type='text/javascript'></script>
   

      <script>
         $(function(){
         	$('.lazy').lazy({
         		effect: 'fadeIn',
         		skip_invisible:false,
         		onError: function(element){
         			console.log('error loading ' + element.data('src'));
         		}
         	})
         }); 
      </script>
      <script> $.post('//graph.facebook.com', {id:'https://juno.vn',scrape: true}, function(response){console.log(response)}); </script>
      <div class="sweettooth-widget"
         data-widget-type="tab"
         data-channel-key="pk_5fd7c940761b11e582a4959ab4f60d20"
         data-channel-customer-id=""
         data-digest=""></div>
      <script>
         if(typeof Haravan == undefined) {
         	Haravan = {};
         }
         
         Haravan.ProductURL = 'https://juno.vn/products/tui-xach-trung-txt123';
         Haravan.ProductId = '1011812654';
         
      </script>

   </body>
</html>