<!DOCTYPE HTML>
<html>
   <head>
      <meta property="fb:pages" content="172922056238158" />
      <meta name="adx:sections" content="https://juno.vn/collections/tui-xach" />
      <meta name="p:domain_verify" content="50a57bef3e9a4ae42fbcd722c7074695"/>
      <meta name="google-site-verification" content="mr8z4Wdem8xgXrWSm1FLf8g96FRSgKwDdOyJ5JEpBmQ" />
      <meta name="google-site-verification" content="U_iG9lyKYTTeLB4qx3QlYZpTxpsO1g0uQYn6QWku-sM" />
      <meta http-equiv="content-type" content="text/html" />
      <meta charset="utf-8" />
      <title>
         Túi Xách nữ đẹp
      </title>
       <base href="http://vn3c.net/public/html/juno/" target="">
      <meta name="description" content="Sản phẩm túi xách nữ hàng hiệu đẹp của thương hiệu thời trang JUNO, giá tốt ✓  Giao hàng miễn phí tận nơi khi mua online ✓ 30 ngày đổi trả MIỄN PHÍ" />
      <meta property="og:type" content="website">
      <meta property="og:title" content="T&#250;i X&#225;ch nữ đẹp">
      <meta property="og:image" content="http://file.hstatic.net/1000003969/file/800x400-mobile-combo-600k-yeu-thuong-gap-doi_1024x1024.jpg">
      <meta property="og:image:secure_url" content="https://file.hstatic.net/1000003969/file/800x400-mobile-combo-600k-yeu-thuong-gap-doi_grande.jpg">
      <meta property="og:description" content="Sản phẩm t&#250;i x&#225;ch nữ h&#224;ng hiệu đẹp của thương hiệu thời trang JUNO, gi&#225; tốt ✓  Giao h&#224;ng miễn ph&#237; tận nơi khi mua online ✓ 30 ng&#224;y đổi trả MIỄN PH&#205;">
      <meta property="og:url" content="https://juno.vn/collections/tui-xach">
      <meta property="og:site_name" content="">
      <link rel="canonical" href="https://juno.vn/collections/tui-xach" />
      <meta name="viewport" content="width=device-width,initial-scale=1.0" />
      <link rel="shortcut icon" type="image/png" href="//theme.hstatic.net/1000003969/1000323463/14/favicon.png?v=2220" />


      <link href='css/font-awesome.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/font-awesome-animation.min.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/juno.min.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/popup-cart.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.carousel.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.theme.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/owl.transitions.2.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/filter.css' rel='stylesheet' type='text/css'  media='all'  />
      <link href='css/styles.css' rel='stylesheet' type='text/css'  media='all'  />
      <script type='text/javascript'>
         //<![CDATA[
         if ((typeof Haravan) === 'undefined') {
           Haravan = {};
         }
         Haravan.culture = 'vi-VN';
         Haravan.shop = 'juno-1.myharavan.com';
         Haravan.theme = {"name":"desktop_by_nhan(uncompress)","id":1000323463,"role":"main"};
         Haravan.domain = 'juno.vn';
         //]]>
      </script>
      <script>
         //<![CDATA[
         (function() { function asyncLoad() { var urls = ["https://buyxgety.haravan.com/js/script_tag_production.js","https://combo.haravan.com/js/script_tag_production.js"];for (var i = 0; i < urls.length; i++) {var s = document.createElement('script');s.type = 'text/javascript';s.async = true;s.src = urls[i];var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);}}window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);})();
         //]]>
      </script>
      <script type='text/javascript'>
         window.HaravanAnalytics = window.HaravanAnalytics || {};
         window.HaravanAnalytics.meta = window.HaravanAnalytics.meta || {};
         window.HaravanAnalytics.meta.currency = 'VND';
         var meta = {"page":{"pageType":"collection","resourceType":"collection","resourceId":1000199518}};
         for (var attr in meta) {
            window.HaravanAnalytics.meta[attr] = meta[attr];
         }
      </script>
      <script>
         //<![CDATA[
         window.HaravanAnalytics.ga = "UA-57206615-1";
         window.HaravanAnalytics.enhancedEcommerce = false;
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
         ga('create', window.HaravanAnalytics.ga, 'auto', {allowLinker: true});
         ga('send', 'pageview'); ga('require', 'linker');try {
         setTimeout(function(){
            if( window.location.pathname == '/checkout' ) {
               $.get('//file.hstatic.net/1000003969/file/script_checkout_1.jpg',function(data){
                  eval(data);
               });
            }
         if( $('.step3').length >= 1 && $(window).width() < 500 ) {
         $('body.step3 > a').remove();
         $('body.step3').prepend("<a href='/cart'><span class='btn-back'>Quay về giỏ hàng</span></a><a class='logo-checkout' href='/'><h1>Đặt hàng thành công</h1></a>");
         }
         
         },500);
         } catch (e) {};
                         //]]>
                         
      </script>
      <script>
         window.HaravanAnalytics.fb = "1566159606933153";
         //<![CDATA[
         !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         document,'script','//connect.facebook.net/en_US/fbevents.js');
         // Insert Your Facebook Pixel ID below. 
         fbq('init', window.HaravanAnalytics.fb );
         fbq('track', 'PageView');
         //]]>
      </script>
      
      <script src='js/bootstrap.min.js' type='text/javascript'></script>
      <script src='js/smartsearch.min.js' type='text/javascript'></script>
      <script src='js/province_hide.js' type='text/javascript'></script>


      <script>
         if(typeof fbq === 'undefined') {
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
         
            fbq('init', '1566159606933153');
            
            //fbq('trackCustom', 'PageViewProduct');
         
            
            fbq('track', '');
            }else {
               fbq('track', '');
            }
      </script>
      <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/6767720e97fc0e6e564ed78ed/2bf2c58174507c12c59eae629.js");</script>
      <script src='js/eco_tracking.js' type='text/javascript'></script>
   </head>
   <body class="cms-index-index">
      <div id="script-head-body"></div>
      <div id="myModal-popup" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5);z-index: 999999;">
         <div class="modal-dialog">
            <!-- Modal content-->
         </div>
      </div>
      <script>
         $(function() {
            var currentAjaxRequest = null;
            var searchForms = $('form[action="/search"]').css('position','relative').each(function() {
               var input = $(this).find('input[name="q"]');
               var offSet = input.position().top + input.innerHeight();
               $('<ul class="search-results"></ul>').css( { 'position': 'absolute', 'left': '0px', 'top': offSet } ).appendTo($(this)).hide();    
               input.attr('autocomplete', 'off').bind('keyup change', function() {
                  var term = $(this).val();
                  var form = $(this).closest('form');
                  /*var searchURL = '/search?type=product&q=' + term;*/
                  var searchURL = '/search?type=product&q=filter=(title:product**'+term+')';
                  var resultsList = form.find('.search-results');
                  if (term.length > 3 && term != $(this).attr('data-old-term')) {
                     $(this).attr('data-old-term', term);
                     if (currentAjaxRequest != null) currentAjaxRequest.abort();
                     console.log(searchURL + '&view=json');
                     currentAjaxRequest = $.getJSON(searchURL + '&view=json', function(data) {
                        console.log(searchURL);
                        resultsList.empty();
                        if(data.results_count == 0) {
                           // resultsList.html('<li><span class="title">No results.</span></li>');
                           // resultsList.fadeIn(200);
                           resultsList.hide();
                        } 
                        else {
                           resultsList.show();
                           var count_data = 0;
                           for (var i=0; i<data.results.length; i++) {
                              var item = data.results[i];
                              if (item.title.indexOf('quà tặng') < 0 && item.price !== '0₫' ){
                                 var link = $('<a></a>').attr('href', item.url);
                                 link.append('<span class="thumbnail"><img src="' + item.thumbnail + '" /></span>');
                                 link.append('<span class="title">' + item.title + '</span>');
                                 link.append('<span class="price">' + item.price + '</span>');
                                 link.wrap('<li></li>');
                                 resultsList.append(link.parent());
                                 /*console.log(count_data)*/
                                 count_data++;
                              }
                           }
                           if(count_data > 5) {
                              var url = '/search?q=filter=((title:product**[KEY]))&type=product';
                              var search_text = $('#text-product').val();
                              if (search_text.length > 0){
                                 url = url.replace('[KEY]', search_text);
                              }
                              resultsList.append('<li><span class="title"><a href="' + url + '">Xem thêm sản phẩm</a></span></li>');
                           }
                           if(count_data <=0) {
                              resultsList.hide();
                           }
                           resultsList.fadeIn(200);
                        }        
                     });
                  }
               });
            });
         
            $('body').bind('click', function(){
               $('.search-results').hide();
            });
         });
      </script>
      <!-- Some styles to get you started. -->
      <style>
         .search-results {
         display: block;
         width: 345px;
         margin: 0px auto 0;
         border: 1px solid #e2e2e2;
         border-radius: 4px;
         -webkit-border-radius: 4px;
         -moz-border-radius: 4px;
         box-shadow: 0 1px 3px 0 #7d7d7d;
         -webkit-box-shadow: 0 1px 3px 0 #7d7d7d;
         -moz-box-shadow: 0 1px 3px 0 #7d7d7d;
         background: #fff;
         position: absolute;
         z-index: 99999;
         left: 170px;
         }
         .search-results li {
         display: block;
         background: #fff;
         overflow: hidden;
         list-style: none;
         border-bottom: 1px dotted #ccc;
         float: none;
         }
         .search-results li:hover {
         background: #FFF5F5;
         }
         .search-results li a {
         position: relative;
         display: block;
         overflow: hidden;
         padding: 6px;
         }
         .search-results li:first-child {
         border-top: none;
         }
         .search-results .title {
         display: block;
         width: 72%;
         line-height: 1.3em;
         color: #333;
         font-size: 14px;
         text-align: left;
         margin-top:10px !important;
         font-weight: 700;
         overflow: hidden;
         text-overflow: ellipsis;
         white-space: nowrap;
         }
         .search-results .price {
         font-size: 14px;
         margin-top: 8px;
         color: red;
         }
         .search-results .thumbnail {
         float: left;
         width: 50px;
         height: 50px;
         margin: 0 6px 0 0;
         }
      </style>
      <section id="sidebar-wrapper" class="hidden-lg hidden-md">
         <ul class="sidebar-nav">
            <li class="title">Danh Mục
            </li>
            <li class="mobile-click">
               <a href="/collections/tui-xach" 
                  title="Túi xách" 
                  class="tui-xach-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_tuixach.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tuixach.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tuixach.png?v=2"/>
               </span>
               Túi xách
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-cao-got" 
                  title="Cao gót" 
                  class="cao-got-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_caogot.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_caogot.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_caogot.png?v=2"/>
               </span>
               Cao gót
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-xang-dan" 
                  title="Xăng đan" 
                  class="xang-dan-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_xangdan.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_xangdan.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_xangdan.png?v=2"/>
               </span>
               Xăng đan
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-bup-be" 
                  title="Búp bê" 
                  class="bup-be-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_bupbe.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_bupbe.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_bupbe.png?v=2"/>
               </span>
               Búp bê
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/sneaker-collections" 
                  title="Giày Sneaker" 
                  class="giay-sneaker-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_sneaker.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_sneaker.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_sneaker.png?v=2"/>
               </span>
               Giày Sneaker
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/giay-boots" 
                  title="Giày Boots" 
                  class="giay-boots-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_boots.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_boots.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_boots.png?v=2"/>
               </span>
               Giày Boots
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/dep-guoc" 
                  title="Dép Guốc" 
                  class="dep-guoc-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_depguoc.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_depguoc.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_depguoc.png?v=2"/>
               </span>
               Dép Guốc
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/8-3-yeu-thuong-gap-doi" 
                  title="Yêu thương gấp đôi" 
                  class="yeu-thuong-gap-doi-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_yeuthuonggapdoi.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_event_hover.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_event_hover.png?v=2"/>
               </span>
               Yêu thương gấp đôi
               </a>
            </li>
            <li class="mobile-click">
               <a href="/collections/juno-queens-of-fashion" 
                  title="Bộ Sưu Tập" 
                  class="bo-suu-tap-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_bosuutap.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_bosuutap_thumb.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconimport_bosuutap_thumb.png?v=2"/>
               </span>
               Bộ Sưu Tập
               </a>
            </li>
            <li class="mobile-click">
               <a href="/blogs/magazine" 
                  title="Tin thời trang" 
                  class="tin-thoi-trang-mobile-click  ">
               <span class="icon ico-top" data-position="new_iconImport_tinthoitrang.png">
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tinthoitrang.png?v=2"/>
               <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/new_iconImport_tinthoitrang.png?v=2"/>
               </span>
               Tin thời trang
               </a>
            </li>
            <li class="more-info fa fa-phone">
               <a href="tel:18001162">
               <span>Gọi mua hàng: <strong>1800 1162</strong></span>
               <span>08:30 - 21:30 mỗi ngày trừ ngày Lễ, Tết</span>
               </a>
            </li>
            <li class="more-info bottom fa fa-building">
               <a href="/collections/cua-hang-khu-vuc-tp-ho-chi-minh?view=stores">
               <span> Xem hệ thống <strong>45</strong> cửa hàng</span>
               </a>
            </li>
         </ul>
      </section>
      <div id="wrapper">
         <header class=" template-page-collection">
            <div class="top hidden-sm hidden-xs">
               <div class="container">
                  <div class="row">
                     <!---->
                     <div class="col-md-5 col-sm-10 left no-padding">
                        <div class="col-md-3 col-sm-10 logoTop">
                           <div class="logo">
                              <a href="/" title="JUNO"><img alt="JUNO" class="" src="//theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" /></a>
                           </div>
                        </div>
                        <div class="col-md-7 col-sm-10 no-padding searchTop">
                           <div class="search-collection col-xs-10 no-padding">
                              <form class="search" action="/search">
                                 <input id="text-product" class="col-xs-10 no-padding" type="text" name="q" placeholder="Bạn cần tìm gì?" />
                                 <input type="hidden" value="product" name="type" />
                                 <button id="submit-search">
                                 <i class="fa fa-search" aria-hidden="true"></i>
                                 </button>                        
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-5 col-sm-10 no-padding rightTop_head">
                        <div class="col-md-5 col-sm-10 switchboardTop">
                           <div class="switchboard_wrapper">   
                              <i class="fa fa-phone" aria-hidden="true"></i>
                              <span>BÁN HÀNG: <strong>1800 1162</strong> (miễn phí)</span>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-10 no-padding storeTop">
                           <div class="headStore_wrapper">                                         
                              <a  href="/pages/tim-dia-chi-cua-hang">
                              <i class="fa fa-building" aria-hidden="true"></i>
                              <span>Xem hệ thống <strong>68</strong> cửa hàng</span>
                              </a>
                           </div>
                        </div>
                        <div class="col-md-1 col-sm-10 no-padding cartTop">
                           <div class="carttop_wrapper">
                              <div class="cart-relative">
                                 <a href="/cart">
                                    <div class="cart-total-price">
                                       <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                       <span class="price" >2 sản phẩm</span>
                                       <span class="hidden" >Giỏ Hàng</span>
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!---->
                  </div>
               </div>
            </div>
            <div id="fix-top-menu" class="top2 hidden-sm hidden-xs">
               <div class="container-fluid menutopid" style="">
                  <div class="container" style="position:relative">
                     <div class="row">
                        <div class="col-lg-10 col-md-10">
                           <ul class="menu-top clearfix hidden-xs">
                              <li class="menu-li hasChild active main" >
                                 <a href="/collections/tui-xach" class="active main" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_tuixach.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tuixach_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tuixach.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Túi Xách
                                       </span>
                                    </div>
                                 </a>
                                 <ul class=" dropdown-menu drop-menu dropmenu_item_show_1">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/tui-xach-trung-txt124">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/xanh_txt124_1.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Túi xách trung sọc vân đính tua rua trang trí TXT124</span><br/>
                                             </div>
                                             <div class="menu-price-pr">750,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/tui-xach-trung-txt124">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/tui-xach-co-lon"><i class="fa fa-caret-right"></i> Túi cỡ lớn</a></li>
                                    <li><a href="/collections/tui-xach-co-trung"><i class="fa fa-caret-right"></i> Túi cỡ trung</a></li>
                                    <li><a href="/collections/tui-xach-co-nho"><i class="fa fa-caret-right"></i> Túi nhỏ</a></li>
                                    <li><a href="/collections/vi-cam-tay"><i class="fa fa-caret-right"></i> Ví cầm tay</a></li>
                                    <li><a href="/collections/clutch"><i class="fa fa-caret-right"></i> Clutch</a></li>
                                    <li><a href="/collections/ba-lo-thoi-trang"><i class="fa fa-caret-right"></i> Ba lô thời trang</a></li>
                                    <li><a href="/collections/tui-canvas"><i class="fa fa-caret-right"></i> Túi canvas</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li hasChild " >
                                 <a href="/collections/giay-cao-got" class="" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_caogot.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_caogot_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_caogot.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Giày Cao Gót
                                       </span>
                                    </div>
                                 </a>
                                 <ul class="first dropdown-menu drop-menu dropmenu_item_show_2">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/giay-cao-got-cg09092">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/bac_cg09092_1.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Giày cao gót 9cm mũi nhọn gót nhọn phối kim nhũ CG09092</span><br/>
                                             </div>
                                             <div class="menu-price-pr">430,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/giay-cao-got-cg09092">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/cao-got-cao-5cm"><i class="fa fa-caret-right"></i> Cao 5cm</a></li>
                                    <li><a href="/collections/cao-got-cao-7cm"><i class="fa fa-caret-right"></i> Cao 7cm</a></li>
                                    <li><a href="/collections/giay-cao-got-cao-9cm"><i class="fa fa-caret-right"></i> Cao 9cm</a></li>
                                    <li><a href="/collections/cao-got-cao-11cm"><i class="fa fa-caret-right"></i> Cao 11cm</a></li>
                                    <li><a href="/collections/cao-got-mui-tron"><i class="fa fa-caret-right"></i> Mũi Tròn</a></li>
                                    <li><a href="/collections/cao-got-mui-nhon"><i class="fa fa-caret-right"></i> Mũi Nhọn</a></li>
                                    <li><a href="/collections/cao-got-got-nhon"><i class="fa fa-caret-right"></i> Gót Nhọn</a></li>
                                    <li><a href="/collections/cao-got-got-vuong"><i class="fa fa-caret-right"></i> Gót Vuông</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li hasChild " >
                                 <a href="/collections/giay-xang-dan" class="" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_xangdan.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_xangdan_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_xangdan.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Giày Xăng Đan
                                       </span>
                                    </div>
                                 </a>
                                 <ul class="first dropdown-menu drop-menu dropmenu_item_show_3">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/giay-sandal-sd11005">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/den_sd11005_1.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Giày Sandal cao 11cm quai ngang chất liệu satin SD11005</span><br/>
                                             </div>
                                             <div class="menu-price-pr">430,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/giay-sandal-sd11005">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/xang-dan-cao-5cm"><i class="fa fa-caret-right"></i> Cao 5cm</a></li>
                                    <li><a href="/collections/xang-dan-cao-7cm"><i class="fa fa-caret-right"></i> Cao 7cm</a></li>
                                    <li><a href="/collections/xang-dan-cao-9cm"><i class="fa fa-caret-right"></i> Cao 9cm</a></li>
                                    <li><a href="/collections/xang-dan-cao-11cm"><i class="fa fa-caret-right"></i> Cao 11cm</a></li>
                                    <li><a href="/collections/xang-dan-de-xuong"><i class="fa fa-caret-right"></i> Xăng Đan Đế Xuồng</a></li>
                                    <li><a href="/collections/xang-dan-cao-got"><i class="fa fa-caret-right"></i> Xăng Đan Cao Gót</a></li>
                                    <li><a href="/collections/xang-dan-bet"><i class="fa fa-caret-right"></i> Xăng Đan Bệt</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li hasChild " >
                                 <a href="/collections/giay-bup-be" class="" >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_bupbe.png?v=2">
                                          <div class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bupbe_hover.png?v=2') no-repeat center top;"></div>
                                          <div class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bupbe.png?v=2') no-repeat center top;"></div>
                                       </div>
                                       <span class="title-main-menu">
                                       Giày Búp Bê
                                       </span>
                                    </div>
                                 </a>
                                 <ul class="first dropdown-menu drop-menu dropmenu_item_show_4">
                                    <li class="menu-hover-li true">
                                       <div class="col-lg-10 col-md-10 menu-back-new">
                                          <span class="menu-title-new" >Mới nhất hôm nay</span>
                                       </div>
                                       <div class="col-lg-10 col-md-10" style="padding-right:5px">
                                          <div class="col-lg-5 col-md-5" style="padding:0">
                                             <a href="https://juno.vn/products/giay-bup-be-bb01113">
                                                <div class="field-sale-2"><span>MỚI</span></div>
                                                <img class="lazy" src="" data-src="//file.hstatic.net/1000003969/file/do_bb01113_1_c7cce527f1a34a468db3b52a7f02d6f8.jpg"/>
                                             </a>
                                          </div>
                                          <div class="col-lg-5 col-md-5 menu-content-new">
                                             <div style="padding-bottom: 10px;">
                                                <span class="menu-tilte-pr" >Giày búp bê mũi nhọn chất liệu Satin BB01113</span><br/>
                                             </div>
                                             <div class="menu-price-pr">390,000d<sup>đ</sup></div>
                                             <a href="https://juno.vn/products/giay-bup-be-bb01113">Xem chi tiết</a>
                                          </div>
                                       </div>
                                    </li>
                                    <li><a href="/collections/bup-be-mui-tron"><i class="fa fa-caret-right"></i> Giày Mũi Tròn</a></li>
                                    <li><a href="/collections/bup-be-mui-nhon"><i class="fa fa-caret-right"></i> Giày Mũi Nhọn</a></li>
                                    <li><a href="/collections/bup-be-co-trang-tri"><i class="fa fa-caret-right"></i> Giày có trang trí</a></li>
                                 </ul>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/sneaker-collections" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_sneaker.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_sneaker.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_sneaker.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Giày Sneaker</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/giay-boots" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_boots.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_boots.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_boots.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Giày Boots</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/dep-guoc" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_depguoc.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_depguoc.png') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_depguoc.png') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Dép Guốc</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li active  fix-icon-coll "
                                 >
                                 <a href="/collections/8-3-yeu-thuong-gap-doi" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_yeuthuonggapdoi.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_event.png') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_event.png') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Yêu Thương Gấp Đôi</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/collections/juno-queens-of-fashion" class=""  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_bosuutap.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bosuutap.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_bosuutap.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Bộ sưu tập</span>
                                    </div>
                                 </a>
                              </li>
                              <li class="menu-li   fix-icon-coll "
                                 >
                                 <a href="/blogs/magazine" class="" target="_blank"  >
                                    <div class="coll-icon">
                                       <div class="ico-top" data-position="new_iconimport_tinthoitrang.png?v=2">
                                          <div data-hover="true" class="img-on" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tinthoitrang.png?v=2') no-repeat center center;"></div>
                                          <div data-nonhover="true" class="img-off" style="background:url('//file.hstatic.net/1000003969/file/new_iconimport_tinthoitrang.png?v=2') no-repeat center center;"></div>
                                       </div>
                                       <span class="title-main-menu ">Tin Thời Trang</span>
                                    </div>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <style>
               .coll-icon .ico-top {
               position: relative;
               overflow: hidden;
               width: 60px;
               height: 36px;
               padding: 0;
               margin: 0 auto;
               display: block;
               text-align: center;
               }
               .coll-icon .ico-top .img-on {
               opacity: 0;
               height: 0;
               transition: all 0.5s;
               }
               .coll-icon .ico-top .img-off{
               opacity: 1;
               width: 60px;
               height: 36px;
               transition: all 0.3s
               }
               /*.coll-icon .ico-top:hover .img-on{
               opacity: 1;
               height: 36px;
               width: 60px;
               }
               .coll-icon .ico-top:hover .img-off{
               opacity: 0;
               height: 0;
               }*/
            </style>
            <div class="top hidden-lg hidden-md" id="mobile-menu">
               <div class="fixed-nav">
                  <button id="menu-toggle" class="navbar-toggle pull-left" type="button" data-toggle="modal" data-target="#menu-modal">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a href="/" title="JUNO" class="logo"><img class="lazy" alt="JUNO" src="" data-src="//theme.hstatic.net/1000003969/1000323463/14/logo.png?v=2220" /></a>
                  <a href="/cart" class="cart-link">
                  <span ><i class="fa fa-shopping-bag" aria-hidden="true"></i> <strong>820,000₫</strong></span>
                  </a>
                  <form class="frm-search" action='/search'>
                     <input  type="text" name='q' value="" id="inputSearch" placeholder="Tìm kiếm...">   
                  </form>
                  <script>
                     $(document).ready(function(){
                     
                        $('#inputSearch').smartSearch({searchdelay:400});
                     
                        $('.btn-search').click(function(){
                           $('.frm-search').find('input').val('');
                           if($(this).hasClass('active')) {
                              $(this).removeClass('active');
                              $('.frm-search').find('input').css('width', '1px');
                              setTimeout(function(){
                                 $('.frm-search').find('input').css('display', 'none');   
                                 $('.logo').show();
                                 $('#menu-toggle').show();
                                 $('body').removeClass('searching');
                              },700);
                     
                           }
                           else {
                              $(this).addClass('active');
                              $('.logo').hide();
                              $('#menu-toggle').hide();
                              $('.frm-search').find('input').css('display', 'block');
                              setTimeout(function(){
                                 var a = screen.width - 90;
                                 $('.frm-search').find('input').css('width', a);
                                 $('body').addClass('searching');
                              },100);
                           }
                        });
                     
                        $('#wrapper').click(function(e){
                           if (e.target !== $('.btn-search')){
                              return;
                           }else{
                              $('.btn-search').removeClass('active');
                              $('.frm-search').find('input').css('width', '1px');
                              setTimeout(function(){
                                 $('.frm-search').find('input').css('display', 'none');   
                                 $('.logo').show();
                                 $('#menu-toggle').show();
                                 $('body').removeClass('searching');
                              },500);
                           }
                        });
                     
                     
                     
                     });
                     
                     function search(){
                     debugger
                        var url = 'filter=((title:product**[KEY]))&type=product';
                        var search_text = $('#inputSearch').val();
                        if (search_text.length > 0){
                           url = url.replace('[KEY]', search_text);
                           window.location.href = '/search?q='+ encodeURIComponent(url);
                        }
                        return false;
                     }
                  </script>
               </div>
            </div>
         </header>
         <script>
            var collectionAll = '/collections/tui-xach';
            if ( collectionAll == '/collections/all' ) {
               window.location = '/';
            }
         </script>
         <section>
            <div class="container">
               <div class="wrapper-collection">
                  <style>
                     .breadcrumb > li + li::before
                     {
                     //content: " › ";
                     border-left: 0px solid transparent;
                     }
                  </style>
                  <!-- breadcrumbs -->
                  <div class="breadcrumbs">
                     <div class="container">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="col-lg-12">
                                 <ol class="breadcrumb">
                                    <li class="home"> <a href="/" title="Trang chủ">Trang chủ</a></li>
                                    <li class="space"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
                                    <li class="active">Túi Xách</li>
                                 </ol>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row wrap-banner-description">
                     <div class="col-xs-10">
                        <div id="banner-description" style="margin-top:0">
                           <p><br data-mce-bogus="1"></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <div class="container">
               <div class="row">
                  <div class="col-md-2 col-sm-3 col-6">
                  </div>
                   <div class="collec-quickview-top topbar">
              <span class="first">Bạn đang xem: </span><span class="checkedvalactive">Túi Xách</span> <span class="checkedval"></span>
         
                 </div>
               </div> 
             
            <div class="collection-index-1 color2 clearfix row">
               <div class="col-md-2 col-sm-3 col-6 sidebar-category-pr" id="collection-option-filter">
                  <div class="catalog_filters_module tags">
                     <ul class="main_item_left">
                        <li class="item ">
                           <a href="/collections/giay-bup-be">Búp bê</a>
                           <div class="catalog_filters filter-tag bupbe">
                              <ul class="check-box-list" data-id="1000199528">
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**mui-tron)"
                                    data-name="mui-tron">
                                    <a class="tag-choise" href="javascript:void(0)">Mũi tròn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**mui-nhon)"
                                    data-name="mui-nhon">
                                    <a class="tag-choise" href="javascript:void(0)">Mũi nhọn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**co-trang-tri)"
                                    data-name="co-trang-tri">
                                    <a class="tag-choise" href="javascript:void(0)">Có trang trí</a>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="item ">
                           <a href="/collections/giay-cao-got">Cao gót</a>
                           <div class="catalog_filters filter-tag caogot">
                              <ul class="boots check-box-list" data-id="1000199526">
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:3cm)"
                                    data-name="3cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 3cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:5cm)"
                                    data-name="5cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 5cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:7cm)"
                                    data-name="7cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 7cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:9cm)"
                                    data-name="9cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 9cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:11cm)"
                                    data-name="11cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 11cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**mui-tron)"
                                    data-name="mui-tron">
                                    <a class="tag-choise" href="javascript:void(0)">Mũi tròn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**mui-nhon)"
                                    data-name="mui-nhon">
                                    <a class="tag-choise" href="javascript:void(0)">Mũi nhọn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**got-nhon)"
                                    data-name="got-nhon">
                                    <a class="tag-choise" href="javascript:void(0)">Gót nhọn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**got-vuong)"
                                    data-name="got-vuong">
                                    <a class="tag-choise" href="javascript:void(0)">Gót vuông</a>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="item ">
                           <a href="/collections/giay-xang-dan">Xăng đan</a>
                           <div class="catalog_filters filter-tag xangdan">
                              <ul class="boots check-box-list" data-id="1000199529">
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:3cm)"
                                    data-name="3cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 3cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:5cm)"
                                    data-name="5cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 5cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:7cm)"
                                    data-name="7cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 7cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:9cm)"
                                    data-name="9cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 9cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**docao:11cm)"
                                    data-name="11cm">
                                    <a class="tag-choise" href="javascript:void(0)">Độ cao 11cm</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**xang-dan-de-xuong)"
                                    data-name="xang-dan-de-xuong">
                                    <a class="tag-choise" href="javascript:void(0)">Đế xuồng</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**xang-dan-cao-got)"
                                    data-name="xang-dan-cao-got">
                                    <a class="tag-choise" href="javascript:void(0)">Cao gót</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**xang-dan-bet)"
                                    data-name="xang-dan-bet">
                                    <a class="tag-choise" href="javascript:void(0)">Bệt</a>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="item ">
                           <a href="/collections/giay-boots">Boots</a>
                           <div class="catalog_filters filter-tag boots">
                              <ul class="boots check-box-list" data-id="1000199525">
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**mui-nhon)"
                                    data-name="mui-nhon">
                                    <a class="tag-choise" href="javascript:void(0)">Mũi nhọn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**mui-tron)"
                                    data-name="mui-tron">
                                    <a class="tag-choise" href="javascript:void(0)">Mũi tròn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**got-nhon)"
                                    data-name="got-nhon">
                                    <a class="tag-choise" href="javascript:void(0)">Gót nhọn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**got-vuong)"
                                    data-name="got-vuong">
                                    <a class="tag-choise" href="javascript:void(0)">Gót vuông</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**co-cao)"
                                    data-name="co-cao">
                                    <a class="tag-choise" href="javascript:void(0)"> Cổ cao</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**co-thap)"
                                    data-name="co-thap">
                                    <a class="tag-choise" href="javascript:void(0)"> Cổ thấp</a>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="item active">
                           <a href="/collections/tui-xach">Túi xách</a>
                           <div class="catalog_filters filter-tag tuixach">
                              <ul class="boots check-box-list" data-id="1000199518">
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**co-lon)"
                                    data-name="co-lon">
                                    <a class="tag-choise" href="javascript:void(0)">Cỡ lớn</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**co-trung)"
                                    data-name="co-trung">
                                    <a class="tag-choise" href="javascript:void(0)">Cỡ trung</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**nho)"
                                    data-name="nho">
                                    <a class="tag-choise" href="javascript:void(0)">Nhỏ</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**vi)"
                                    data-name="vi">
                                    <a class="tag-choise" href="javascript:void(0)">Ví</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**clutch)"
                                    data-name="clutch">
                                    <a class="tag-choise" href="javascript:void(0)">Clutch</a>
                                 </li>
                                 <li class="advanced-filter" 
                                    data-tag="(tag:product**ba-lo-thoi-trang)"
                                    data-name="ba-lo-thoi-trang">
                                    <a class="tag-choise" href="javascript:void(0)">Ba lô thời trang</a>
                                 </li>
                              </ul>
                           </div>
                        </li>
                        <li class="item ">
                           <a href="/collections/that-lung">Thắt lưng</a>
                        </li>
                     </ul>
                  </div>
                  <script>
                     $(document).ready(function(){
                        $('ul.check-box-list li').click(function(e){
                           e.preventDefault();
                           var $this = $(this);
                           var id = $this.parent().data("id");
                           var name = $this.children().text();
                           var dataname = $this.data('name');
                     
                           if ( $this.hasClass('check') ) {
                              $this.removeClass('check');
                              if ( $('.collec-quickview-top.topbar .item-check').hasClass(dataname) ){
                                 $('.item-check.'+dataname).remove();
                              }
                           } else {
                              $this.addClass('check');
                              $('.collec-quickview-top.topbar').append("<span class='item-check "+dataname+"' >"+name+"</span>");
                           }
                           Stringfilter(id);
                        });
                        $('.collec-quickview-top.topbar .item-check').click(function(e){
                           e.preventDefault();
                           var $this = $(this);
                        });
                        //string filter
                        function Stringfilter(id){
                           var q="", tag = "", total_page=0, cur_page=1;    
                     
                           
                            q="filter=(collectionid:product=1000199518)";
                              
                     
                              /*by tag*/
                              $('ul.check-box-list li.check').each(function(){
                                 tag = tag + $(this).data('tag') + '||';
                              })
                     
                              tag=tag.substring(0,tag.length -2);
                               if(tag != ""){
                                  tag='&&('+tag+')';
                                  q+=tag;
                               }
                     
                               /*console.log(q);*/
                               var url_search = "/search?q="+q+"&view=pagesize";
                               console.log(url_search)
                               $.ajax({ /* lấy tổng số trang của kết quả filter*/
                                  url: url_search, 
                                  async: false,
                                  success:function(data){
                                     console.log(data)
                                     total_page = parseInt(data);
                                  }
                               })
                     
                               
                               if(cur_page <= total_page){
                                  var url = "/search?q="+q+"&view=filter_collection";
                                  console.log(url)
                                  $.ajax({
                                     url : url,
                                     success: function(data){
                                        $(".load-wrapp").show(300);
                                        $("#product-lists").html(data,300); /*Class chứa sản phẩm ở collection*/
                                        $(".load-wrapp").hide(300);
                                     }
                     
                                  })
                                  $.ajax({  
                                     url: "/search?q="+q+"&view=paginate",
                                     async: false,
                                     success:function(data){
                                        $(".paginate").html(data); // in phân trang
                                     }
                                  })
                               }else{
                                  $("#product-lists").html("<div class='col-sm-12 text-center'>Không có sản phẩm phù hợp!</div>");
                               }
                     
                              }
                            });
                     
                            
                  </script>
               </div>
               
             
                  

                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
                  <?php include('item-pro.php')?>
              



               <div id="product-lists" class="product-item product-index clearfix active left-border">
               </div>
               <div class="col-sm-8 col-sm-offset-2 viewmore_product text-center Haravan.Liquid.Tags.Html.PagingLink" style="display:none">
                  <a href="javascript:;" class="btn btn-primary" data-href="/collections/tui-xach?page=2" title="Xem thêm sản phẩm">Xem thêm sản phẩm</a>
                  <a href="javascript:;" class="disabled btn alert alert-danger" style="display:none" title="Hết sản phẩm">Không còn sản phẩm nào trong danh mục</a>
               </div>
            </div>
         </div>
         <script>
            fbq('trackCustom', 'ViewCollection',{
               content_category:'tui-xach',
               content_name:'Túi Xách'
            });
            $(".viewmore_product a").click(function(e){
               e.preventDefault();
               $(this).addClass('active');
               var n = $("#product-lists");
               var r = $(".viewmore_product a").first();
               $.ajax({
                  type: "GET",
                  url: r.attr("data-href"),
                  beforeSend: function() {
                  },
                  success: function(i) {
                     var s = $(i).find("#product-lists");
                     if (s.length) {
                        n.append(s.children());
                        n.find("img.lazy").lazy();
                        n.find('.color-swatches').each(function() {
                           $(this).find('li').first().addClass('active');
                        })
                        n.find('.color-swatches').find('li').click(function(event) {
                           $(this).parent().find('li').removeClass('active');
                           $(this).addClass('active');
                           var img_change = $(this).data('img');
                           var img_change_hover = $(this).data('img-hover');
                           $(this).parents('.product-detail').find('.main-image').attr('data-src',img_change);
                           $(this).parents('.product-detail').find('.main-image').attr('src',img_change);
                           $(this).parents('.product-detail').find('.image-hover').attr('data-src',img_change_hover);
                           $(this).parents('.product-detail').find('.image-hover').attr('src',img_change_hover);
                        })
                        if ($(i).find(".viewmore_product").length > 0) {
                           r.removeClass('active');
                           r.attr("data-href", $(i).find(".viewmore_product a").attr("data-href"))
                        } else {
                           r.hide();
                           r.next().show()
                        }
                     }
                  },
                  error: function(n, r) {
                  },
                  dataType: "html"
               })
            });
            $(document).ready(function(){ 
               $.getJSON('/collections/tui-xach?page=2&view=js_paginate',function(nextpage){console.log(nextpage);if(nextpage.results_count > 0){$('.viewmore_product').show()}else{$('.viewmore_product').hide()}})
               var selection = $('.color-swatches');
               var select = selection.find('li');
               selection.each(function() {
                  $(this).find('li').first().addClass('active');
               })
               select.click(function(event) {
                  console.log('click')
                  $(this).parent().find('li').removeClass('active');
                  $(this).addClass('active');
                  var img_change = $(this).data('img');
                  var img_change_hover = $(this).data('img-hover');
                  $(this).parents('.product-detail').find('.main-image').attr('data-src',img_change);
                  $(this).parents('.product-detail').find('.main-image').attr('src',img_change);
                  $(this).parents('.product-detail').find('.image-hover').attr('data-src',img_change_hover);
                  $(this).parents('.product-detail').find('.image-hover').attr('src',img_change_hover);
               });
            });
         </script>
        
         <div class="testimonials-wrapper vietstar container-fluid " style="border-top: 1px solid #f8f8f8;">
            <div class="container" >
               <div class="row">
                  <div class="testimonials">
                     <div class="new_title center">
                        <div class="col-md-10 col-lg-10 padding-none title-y-kien" >
                           <div>
                              <span class="ta title-mobile-juno">SAO VIỆT "ĐỔ BỘ" RA JUNO MUA GIÀY, TÚI NHÂN NGÀY 8/3 VÌ SỢ HẾT MẪU ĐẸP</span>
                           </div>
                           <div>
                              <span class="tb">Hơn 10,000 phụ nữ được phục vụ mỗi ngày tại JUNO</span>
                           </div>
                        </div>
                     </div>
                     <div class="col-xs-10 new-item">
                        <div class="slider-testimonials owl-carousel">
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="MC Phan Anh" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/phananh-juno-8-3_large.jpg" alt="">
                                    <div class="name">
                                       <i>MC Phan Anh</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Diễn viên Quyền Linh" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/quyenlinh-juno-8-3_large.jpg" alt="">
                                    <div class="name">
                                       <i>Diễn viên Quyền Linh</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Ca sĩ Gil Lê" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/gille-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>Ca sĩ Gil Lê</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="MC Hoàng Oanh" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/mchoangoanh-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>MC Hoàng Oanh</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Diễn viên Diệu Nhi" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/dieunhi-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>Diễn viên Diệu Nhi</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                           <div class="item">
                              <div class="mainn col-xs-10">
                                 <a title="Diễn viên Hương Giang" >
                                    <img class="lazy lazyOwl" data-src="//file.hstatic.net/1000003969/file/huonggiang-juno-bf_large.jpg" alt="">
                                    <div class="name">
                                       <i>Diễn viên Hương Giang</i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>


                  </div>
               </div>
            </div>
         </div>
         <script>
            $(document).ready(function(){
               $('.slider-testimonials').owlCarousel({
                  autoPlay: 4500, //Set AutoPlay to 3 seconds
                  pagination: false,
                  navigation: false,
                  loop:true,
                  lazyLoad : true,
                  navigationText: false,
                  items: 4, 
                  itemsTablet: 4,
                  itemsMobile:1,
               });
            
            });
         </script>
         <div class="hidden bottom-content container">
            <div class="row">
               <div class="col-xs-10 text-center">
                  <p class="one">còn rất nhiều sản phẩm khác tại hệ thống cửa hàng</p>
                  <p class="two"><a href="/collections/cua-hang-khu-vuc-tp-ho-chi-minh?view=stores">Ấn xem địa chỉ hệ thống 68 cửa hàng</a></p>
               </div>
            </div>
         </div>
         <div class="bottom-content container">
            <div class="row">
               <div class="col-xs-10 text-center mov_to_store">
                  <p class="title title-collection-middle title-mobile-juno">Đặt hàng online giao hàng toàn quốc hoặc ra hệ thống 68 cửa hàng </p>
                  <p class="title-collection-bottom">
                     <a>Click vào hình ảnh để xem vị trí cửa hàng</a>
                  </p>
               </div>
            </div>
         </div>
         <div class="footer-map container-fluid wow fadeIn" style="background:transparent url('//file.hstatic.net/1000003969/file/bg-ch-salebf2017.jpg') repeat scroll 0 0">
            <div class="row">
               <div class=" col-lg-10 col-md-10 button-store" style="margin: 140px 0;">
                  <a class="store-footer" target="_blank" href="http://juno.vn/collections/cua-hang-khu-vuc-tp-ho-chi-minh?view=stores">Mời bạn xem địa chỉ hệ thống 68 cửa hàng</a>
               </div>
            </div>
         </div>
         <footer >
            <div class="container-fluid 5icons" style="background:#e5e5e5">
               <div class="">
                  <div class="bottom">
                     <div class="container">
                        <div class="row">
                           <ul class="menu_footer">
                              <li>
                                 <a href="/collections/tui-xach" title="Túi xách">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_5.svg" alt="Túi xách" class="img-responsive" />
                                 <span class="label_footer">Túi/Ví</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-cao-got" title="Giày cao gót">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_2.svg" alt="Giày cao gót" class="img-responsive" />
                                 <span class="label_footer">Cao gót</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-xang-dan" title="Giày xăng đan">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_3.svg" alt="Giày xăng đan" class="img-responsive" />
                                 <span class="label_footer">Xăng đan</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-bup-be" title="Giày búp bê">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_1.svg" alt="Giày búp bê" class="img-responsive" />
                                 <span class="label_footer">Búp bê</span>
                                 </a>
                              </li>
                              <li>
                                 <a href="/collections/giay-boots" title="Giày Boots">
                                 <img class="lazy" data-src="//file.hstatic.net/1000003969/file/icon_footer_4.svg" alt="Dép guốc" class="img-responsive" />
                                 <span class="label_footer">Boots</span>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-fluid signUp" style="background:#fff">
               <div class="">
                  <div class="container">
                     <div class="row" style="padding-top:20px;padding-bottom:20px">
                        <div class="wrap_foo_switchboard col-md-5 col-lg-5 col-sm-10 col-xs-10 juno-phone-mobile">
                           <div class="row">
                              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-10 no-padding item-p text-align-center">
                                 <div class="icon_phone">
                                    <img src="//theme.hstatic.net/1000003969/1000323463/14/icon_phone_circle.jpg?v=2220" />
                                 </div>
                                 <div class="phone_footer">
                                    <strong>Gọi mua hàng(08:30-21:30)</strong>
                                    <br>
                                    <span class="number_phone">1800 1162</span>
                                    <span class="moreinfo">Tất cả các ngày trong tuần</span>
                                 </div>
                              </div>
                              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-10 no-padding item-p text-align-center">
                                 <div class="icon_phone">
                                    <img src="//theme.hstatic.net/1000003969/1000323463/14/icon_phone_circle.jpg?v=2220" />
                                 </div>
                                 <div class="phone_footer">
                                    <strong>Góp ý, khiếu nại(08:30-20:30)</strong>
                                    <br>
                                    <span class="number_phone">1800 1160</span>
                                    <span class="moreinfo">Các ngày trong tuần ( trừ ngày lễ )</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="wrap_foo_social col-md-5 col-lg-5 col-sm-10 col-xs-10 text-align-center">
                           <div class="wrapper_embed col-sm-5 col-xs-10 no-padding">
                              <div class="ttmail">
                                 <span>
                                 <strong>đăng ký nhận thông tin mới từ Juno</strong>
                                 </span>
                              </div>
                              <div id="mc_embed_signup" style="margin-bottom: 10px;">
                                 <form action="//juno.us14.list-manage.com/subscribe/post?u=6767720e97fc0e6e564ed78ed&amp;id=fbf6273ec0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">
                                       <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Nhập email của bạn tại đây..." required>
                                       <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                       <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                          <input type="text" name="b_c6a1ff4613972aee4c6da0254_380ebe08a5" tabindex="-1" value="">
                                       </div>
                                       <div class="clear">
                                          <input type="submit" value="Đăng ký" name="subscribe" id="mc-embedded-subscribe" class="button" style="background:#3c3c3c;border:1px solid #3c3c3c !important">
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </div>
                           <div class="social col-sm-5 col-xs-10 text-align-center-mobile text-align-center">
                              <p class="title-md-footer">
                                 <strong>Like Juno trên mạng xã hội</strong>
                              </p>
                              <ul class="navbar-social">
                                 <li class="social-face">
                                    <a href="https://www.facebook.com/giayjuno" target="_blank" rel="nofollow"> 
                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    </a>
                                 </li>
                                 <li> 
                                    <a href="https://www.instagram.com/juno.vn/" target="_blank" rel="nofollow">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                 </li>
                                 <li> 
                                    <a href="https://www.youtube.com/user/JunoShoesVn" target="_blank" rel="nofollow">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                    </a>
                                 </li>
                                 <li> 
                                    <a href="http://zalo.me/giayjuno" target="_blank" rel="nofollow">
                                    <i class="fa fa-zalo" aria-hidden="true"></i>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-fluid fooMenu" style="background:#f4f4f4">
               <div class="">
                  <div class="top">
                     <div class="container">
                        <div class="row">
                           <ul class="instuction_footer instuction_footer-mobile">
                              <li><a href="/blogs/tin-tuc-juno" title="Hướng dẫn chọn cỡ giày">Tin tức, khuyến mãi JUNO</a></li>
                              <li><a href="/pages/huong-dan-chon-size-giay" title="Hướng dẫn chọn cỡ giày">Hướng dẫn chọn cỡ giày</a></li>
                              <li><a href="/pages/huong-dan-mua-hang-online" title="Hướng dẫn mua hàng online">Hướng dẫn mua hàng online</a></li>
                              <li><a href="/pages/chinh-sach-khach-hang-than-thiet" title="Chính sách khách hàng thân thiết">Chính sách khách hàng thân thiết</a></li>
                              <li><a href="/pages/chinh-sach-doi-tra-va-hoan-tien" title="Chính sách đổi trả">Chính sách Đổi/Trả</a></li>
                              <li><a href="/pages/thanh-toan-giao-nhan" title="Thanh toán giao nhận">Thanh toán giao nhận</a></li>
                              <li class="hidden"><a href="/pages/chinh-sach-bao-mat" title="Chính sách bảo mật">Chính sách bảo mật</a></li>
                              <li><a href="/pages/gioi-thieu" title="Giới thiệu; Liên hệ...">Các thông tin khác</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-fluid wow fadeIn" style="background:#f4f4f4">
               <div class="">
                  <div class="copyright">
                     <div class="container">
                        <div class="row">
                           <div class=" copy">
                              <div class="col-xs-12 col-sm-6">
                                 © 2015 JUNO. Công ty cổ phần sản xuất thương mại dịch vụ JUNO.<br>
                                 Văn phòng: 313 Nguyễn Thị Thập, Q.7, TP.HCM. GP số 0310350452-002 do Sở Kế Hoạch và Đầu Tư TP.HCM cấp ngày 29/06/2011
                              </div>
                              <div class="col-xs-12 col-sm-2 col-sm-offset-2 gov">
                                 <a href="http://online.gov.vn/CustomWebsiteDisplay.aspx?DocId=13954" target="_blank"><img src="//theme.hstatic.net/1000003969/1000323463/14/icon-dangky.png?v=2220"></a>
                                 <div class="text-center">
                                    <span class="pull-left"> Powered by <a target="_blank" href="https://www.haravan.com/?hchan=juno">Haravan Enterprise.</a></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <div class="back-to-top">
         <a href="javascript:void(0);">Top</a>
      </div>
      <div id="quickView" class="modal fade" role="dialog" style="background: rgba(0, 0, 0, 0.5); z-index: 999999;">
         <div class="modal-dialog"></div>
      </div>
      <div id="myCart"    class="modal fade" role="dialog" >
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel">
                     Bạn có <b></b> sản phẩm trong giỏ hàng
                  </h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span class="hrv-close-modal"></span>
                  </button>
               </div>
               <form action="/cart" method="post" id="cartform">
                  <div class="modal-body">
                     <table style="width:100%;" id="cart-table">
                        <tr>
                           <th></th>
                           <th>Tên sản phẩm</th>
                           <th>Số lượng</th>
                           <th>Giá tiền</th>
                           <th></th>
                        </tr>
                        <tr class="line-item original">
                           <td class="item-image"></td>
                           <td class="item-title">
                              <a></a>
                           </td>
                           <td class="item-quantity"></td>
                           <td class="item-price"></td>
                           <td class="item-delete text-center"></td>
                        </tr>
                     </table>
                  </div>
                  <div class="modal-footer">
                     <div class="row">
                        <div class="col-sm-5">
                           <div class="modal-note">
                              <textarea placeholder="Viết ghi chú" id="note" name="note" rows="5"></textarea>
                           </div>
                        </div>
                        <div class="col-sm-5">
                           <div class="total-price-modal">
                              Tổng cộng : <span class="item-total"></span>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="margin-top:10px;">
                        <div class="col-lg-5">
                           <div class="comeback">
                              <a href="/collections/all">
                              <i class="fa fa-caret-left mr10" ></i>Tiếp tục mua hàng
                              </a>
                           </div>
                        </div>
                        <div class="col-lg-5 text-right">
                           <div class="buttons btn-modal-cart">
                              <button type="submit" class="button-default" id="checkout" name="checkout">
                              Đặt hàng
                              <i class="fa fa-caret-right"></i>
                              </button>
                           </div>
                           <div class="buttons btn-modal-cart">
                              <button type="submit" class="hidden button-default" id="update-cart-modal" name="">
                              <i class="fa fa-caret-left"></i>
                              Cập nhật
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="noti_itemcount unactive"></div>



      <script src='js/script.js' type='text/javascript'></script>
      <script src='js/option_selection.js' type='text/javascript'></script>
      <script src='js/api.jquery.js' type='text/javascript'></script>
      <script src='js/owl.carousel.2.js' type='text/javascript'></script>
      <script src='js/jquery.lazyload.js' type='text/javascript'></script>
      <script src='js/lazysizes.min.js' type='text/javascript'></script>

   
   
      <script>
         $(function(){
            $('.lazy').lazy({
               effect: 'fadeIn',
               skip_invisible:false,
               onError: function(element){
                  console.log('error loading ' + element.data('src'));
               }
            })
         }); 
      </script>
      <script> $.post('//graph.facebook.com', {id:'https://juno.vn',scrape: true}, function(response){console.log(response)}); </script>
      <div class="sweettooth-widget"
         data-widget-type="tab"
         data-channel-key="pk_5fd7c940761b11e582a4959ab4f60d20"
         data-channel-customer-id=""
         data-digest=""></div>
      <script>
         if(typeof Haravan == undefined) {
            Haravan = {};
         }
         
      </script>
      <!-- Google Code for Remarketing Tag -->
      <!--------------------------------------------------
         Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
         --------------------------------------------------->
      <script>
         var google_tag_params = {
         
         
         dynx_itemid: 'Collection-dynxid',
         dynx_pagetype: 'Collection',
         dynx_totalvalue: 0,
         
         };
      </script>
      <script type="text/javascript">
         /* <![CDATA[ */
         var google_conversion_id = 956357071;
         var google_custom_params = window.google_tag_params;
         var google_remarketing_only = true;
         /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
      <noscript>
         <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/956357071/?guid=ON&amp;script=0"/>
         </div>
      </noscript>
      <script>
         /*tracking desktop index*/
         /*tracking desktop index*/
         
         /*tracking desktop product*/
         $('.sale-addmore .content-bonus a').click(function(){
            ga('send', {
               hitType: 'event',
               eventCategory: 'click_view_info_Mobile',
               eventAction: 'Click',
               eventLabel: 'click_to_view_info'
            });
         });
         
         $('.tinhsize a').click(function(){
            ga('send', {
               hitType: 'event',
               eventCategory: 'click_view_size_Mobile',
               eventAction: 'Click',
               eventLabel: 'click_to_view_size'
            });
         });
         
         $('.cart-desktop__tfooter-checkout').click(function(){
            ga('send', {
               hitType: 'event',
               eventCategory: 'Add_to_cart_now_popup_Desktop',
               eventAction: 'Click',
               eventLabel: 'click_to_add_to_cart_desktop'
            });
         });
         
         
         /*tracking desktop product*/
      </script>
      <script type='text/javascript'>var _spapi = _spapi || []; _spapi.push(['_partner', 'juno']); (function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'juno.api.sociaplus.com/partner.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); } )(); </script>
   </body>
</html>
