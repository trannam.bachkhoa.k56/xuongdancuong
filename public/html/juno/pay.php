


	
		<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" class="flexbox">
			<head>
				<link rel="shortcut icon" href="//theme.hstatic.net/1000003969/1000323463/14/favicon.png?v=2220" type="image/png" />
				<title>
					JUNO - Thanh toán đơn hàng
				</title>
	
				
					<meta name="description" content="JUNO - Thanh to&#225;n đơn h&#224;ng" />
				<base href="http://vn3c.net/public/html/juno/" target="">
				<link href='css/checkouts.css' rel='stylesheet' type='text/css'  media='all'  />
				<link href='css/check_out.css' rel='stylesheet' type='text/css'  media='all'  />
				<script src='js/jquery.min.js' type='text/javascript'></script>
				<script src='js/jquery.validate.js' type='text/javascript'></script>
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2, user-scalable=no">

			</head>
			<body>
	
                <input id="reloadValue" type="hidden" name="reloadValue" value="" />
				<div class="banner">
					<div class="wrap">
						<a href="/" class="logo">
							<h1 class="logo-text">JUNO</h1>
						</a>
					</div>
				</div>
				<button class="order-summary-toggle order-summary-toggle-hide">
					<div class="wrap">
						<div class="order-summary-toggle-inner">
							<div class="order-summary-toggle-icon-wrapper">
								<svg width="20" height="19" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle-icon"><path d="M17.178 13.088H5.453c-.454 0-.91-.364-.91-.818L3.727 1.818H0V0h4.544c.455 0 .91.364.91.818l.09 1.272h13.45c.274 0 .547.09.73.364.18.182.27.454.18.727l-1.817 9.18c-.09.455-.455.728-.91.728zM6.27 11.27h10.09l1.454-7.362H5.634l.637 7.362zm.092 7.715c1.004 0 1.818-.813 1.818-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817zm9.18 0c1.004 0 1.817-.813 1.817-1.817s-.814-1.818-1.818-1.818-1.818.814-1.818 1.818.814 1.817 1.818 1.817z"></path></svg>
							</div>
							<div class="order-summary-toggle-text order-summary-toggle-text-show">
								<span>Hiển thị thông tin đơn hàng</span>
								<svg width="11" height="6" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle-dropdown" fill="#000"><path d="M.504 1.813l4.358 3.845.496.438.496-.438 4.642-4.096L9.504.438 4.862 4.534h.992L1.496.69.504 1.812z"></path></svg>
							</div>
							<div class="order-summary-toggle-text order-summary-toggle-text-hide">
								<span>Ẩn thông tin đơn hàng</span>
								<svg width="11" height="7" xmlns="http://www.w3.org/2000/svg" class="order-summary-toggle-dropdown" fill="#000"><path d="M6.138.876L5.642.438l-.496.438L.504 4.972l.992 1.124L6.138 2l-.496.436 3.862 3.408.992-1.122L6.138.876z"></path></svg>
							</div>
							<div class="order-summary-toggle-total-recap" data-checkout-payment-due-target="60000000">
								<span class="total-recap-final-price">600,000₫</span>
							</div>
						</div>
					</div>
				</button>
				<div class="content content-second">
					<div class="wrap">
						<div class="sidebar sidebar-second">
							<div class="sidebar-content">
								<div class="order-summary">
									<div class="order-summary-sections">
										
											
												<div class="order-summary-section order-summary-section-discount" data-order-summary-section="discount">
													<form id="form_discount_add" accept-charset="UTF-8" method="post">
														<input name="utf8" type="hidden" value="✓">
														<div class="fieldset">
															<div class="field  ">
																<div class="field-input-btn-wrapper">
																	<div class="field-input-wrapper">
																		<label class="field-label" for="discount.code">Mã giảm giá</label>
																		<input placeholder="Mã giảm giá" class="field-input" data-discount-field="true" autocomplete="off" autocapitalize="off" spellcheck="false" size="30" type="text" id="discount.code" name="discount.code" value="" />
																	</div>
																	<button type="submit" class="field-input-btn btn btn-default btn-disabled">
																		<span class="btn-content">Sử dụng</span>
																		<i class="btn-spinner icon icon-button-spinner"></i>
																	</button>
																</div>
																
															</div>
														</div>
													</form>
												</div>
											
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="content">
				
					<div class="wrap">
						<div class="sidebar">
							<div class="sidebar-content">
								<div class="order-summary order-summary-is-collapsed">
									<h2 class="visually-hidden">Thông tin đơn hàng</h2>
									<div class="order-summary-sections">
										<div class="order-summary-section order-summary-section-product-list" data-order-summary-section="line-items">
											<table class="product-table">
												<thead>
													<tr>
														<th scope="col"><span class="visually-hidden">Hình ảnh</span></th>
														<th scope="col"><span class="visually-hidden">Mô tả</span></th>
														<th scope="col"><span class="visually-hidden">Số lượng</span></th>
														<th scope="col"><span class="visually-hidden">Giá</span></th>
													</tr>
												</thead>
												<tbody>
													
														<tr class="product" data-product-id="1009700761" data-variant-id="1019644192">
															<td class="product-image">
																<div class="product-thumbnail">
																	<div class="product-thumbnail-wrapper">
																		<img class="product-thumbnail-image" alt="Giày cao gót 5cm mũi nhọn gót nhọn phối trang trí nơ CG05047" src="//product.hstatic.net/1000003969/product/do_cg05047_2_small.jpg" />
																	</div>
																	<span class="product-thumbnail-quantity" aria-hidden="true">1</span>
																</div>
															</td>
															<td class="product-description">
																<span class="product-description-name order-summary-emphasis">Giày cao gót 5cm mũi nhọn gót nhọn phối trang trí nơ CG05047</span>
																
																	<span class="product-description-variant order-summary-small-text">
																		34 / Đỏ
																	</span>
																
															</td>
															<td class="product-quantity visually-hidden">1</td>
															<td class="product-price">
																<span class="order-summary-emphasis">300,000₫</span>
															</td>
														</tr>
													
														<tr class="product" data-product-id="1009988865" data-variant-id="1020200507">
															<td class="product-image">
																<div class="product-thumbnail">
																	<div class="product-thumbnail-wrapper">
																		<img class="product-thumbnail-image" alt="Giày búp bê mũi nhọn họa tiết lượn sóng BB01109" src="//product.hstatic.net/1000003969/product/do_bb01109_2_small.jpg" />
																	</div>
																	<span class="product-thumbnail-quantity" aria-hidden="true">1</span>
																</div>
															</td>
															<td class="product-description">
																<span class="product-description-name order-summary-emphasis">Giày búp bê mũi nhọn họa tiết lượn sóng BB01109</span>
																
																	<span class="product-description-variant order-summary-small-text">
																		34 / Đỏ
																	</span>
																
															</td>
															<td class="product-quantity visually-hidden">1</td>
															<td class="product-price">
																<span class="order-summary-emphasis">300,000₫</span>
															</td>
														</tr>
													
												</tbody>
											</table>
										</div>
										
											<div class="order-summary-section order-summary-section-discount" data-order-summary-section="discount">
												<form id="form_discount_add" accept-charset="UTF-8" method="post">
													<input name="utf8" type="hidden" value="✓">
													<div class="fieldset">
														<div class="field  ">
															<div class="field-input-btn-wrapper">
																<div class="field-input-wrapper">
																	<label class="field-label" for="discount.code">Mã giảm giá</label>
																	<input placeholder="Mã giảm giá" class="field-input" data-discount-field="true" autocomplete="off" autocapitalize="off" spellcheck="false" size="30" type="text" id="discount.code" name="discount.code" value="" />
																</div>
																<button type="submit" class="field-input-btn btn btn-default btn-disabled">
																	<span class="btn-content">Sử dụng</span>
																	<i class="btn-spinner icon icon-button-spinner"></i>
																</button>
															</div>
															
														</div>
													</div>
												</form>
											</div>
										
										<div class="order-summary-section order-summary-section-total-lines" data-order-summary-section="payment-lines">
											<table class="total-line-table">
												<thead>
													<tr>
														<th scope="col"><span class="visually-hidden">Mô tả</span></th>
														<th scope="col"><span class="visually-hidden">Giá</span></th>
													</tr>
												</thead>
												<tbody>
													<tr class="total-line total-line-subtotal">
														<td class="total-line-name">Tạm tính</td>
														<td class="total-line-price">
															<span class="order-summary-emphasis" data-checkout-subtotal-price-target="60000000">
																600,000₫
															</span>
														</td>
													</tr>
													
													<tr class="total-line total-line-shipping">
														<td class="total-line-name">Phí vận chuyển</td>
														<td class="total-line-price">
															<span class="order-summary-emphasis" data-checkout-total-shipping-target="0">
																
																	—
																
															</span>
														</td>
													</tr>
												</tbody>
												<tfoot class="total-line-table-footer">
													<tr class="total-line">
														<td class="total-line-name payment-due-label">
															<span class="payment-due-label-total">Tổng cộng</span>
														</td>
														<td class="total-line-name payment-due">
															<span class="payment-due-currency">VND</span>
															<span class="payment-due-price" data-checkout-payment-due-target="60000000">
																600,000₫
															</span>
														</td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="main">
							<div class="main-header">
								<a href="/" class="logo">
									<h1 class="logo-text">JUNO</h1>
								</a>
								
									<ul class="breadcrumb">
										<li class="breadcrumb-item">
											<a href="/cart">Giỏ hàng</a>
										</li>
                                        
                                            <li class="breadcrumb-item breadcrumb-item-current">
                                                Thông tin giao hàng
                                            </li>
                                        
									</ul>
								
							</div>
							<div class="main-content">
								
									<div class="step">
										<div class="step-sections steps-onepage" step="1">
                                            
                                                
<script src='//go.ecotrackings.com/eco_tracking.js' type='text/javascript'></script>
<script>
	var eco_token = "anVuby52bg==";
	var eco_data = {
	"offer_id": "junovn", 
	"transaction_id": "",
	"transaction_time": "",
	"session_ip": "",
	"traffic_id": eco_cps.get_traffic_id(),
	"products": [
		
		
		{
		"id": "1009700761",
		"sku": "1104000002880",
		"category": "8-3 Yêu thương gấp đôi",
		"price": "300000",
		"name": "Giày cao gót 5cm mũi nhọn gót nhọn phối trang trí nơ CG05047",
		"getdata":"line_price_orginal:43000000 and line_price:30000000 and price_original:43000000" ,
		"commission": "8%",
		"status_code": "0",
		"quantity": "1",
		}
		,
		
		
		{
		"id": "1009988865",
		"sku": "1103000001957",
		"category": "8-3 Yêu thương gấp đôi",
		"price": "300000",
		"name": "Giày búp bê mũi nhọn họa tiết lượn sóng BB01109",
		"getdata":"line_price_orginal:39000000 and line_price:30000000 and price_original:39000000" ,
		"commission": "8%",
		"status_code": "0",
		"quantity": "1",
		}
		
		
	]
};
console.log(eco_data.products);
</script>
<script src='//go.ecotrackings.com/eco_px.js' type='text/javascript'></script>

<script>
	$(document).ready(function(){
		$('.section-customer-information .section-content-text').hide();
	if ( window.location.href.includes('checkout') ) {
		console.log('step1');
		$('#form_update_location_hide').parent().append('<h3 class="notice" style="color:#f77705;font-weight:bold;margin: 1.5em auto 0.3em 5px;">Chỉ nhận các đơn hàng tại TP.Hồ Chí Minh khu vực Quận 1, Quận 3, Quận 10</h3>');
	}
})
</script>

<!-- Google Code for Successful checkout Page -->
<script async>
	if(typeof ga != 'undefined') {
		ga('require', 'ecommerce');
		
		ga('ecommerce:addItem', {
			'id': '1009700761', /*Transaction ID. Required*/ 
			'name': 'Giày cao gót 5cm mũi nhọn g&#243;t nhọn phối trang tr&#237; nơ CG05047', /*Product name. Required*/
			'sku': '1104000002880', /*SKU/code.*/
			'category': '8-3 Yêu thương gấp đôi', /*Category or variation.*/ 
			'price': '300000', /*Unit price.*/ 
			'quantity': '1' /*Quantity.*/ 
		});
		
		ga('ecommerce:addItem', {
			'id': '1009988865', /*Transaction ID. Required*/ 
			'name': 'Gi&#224;y b&#250;p b&#234; mũi nhọn họa tiết lượn s&#243;ng BB01109', /*Product name. Required*/
			'sku': '1103000001957', /*SKU/code.*/
			'category': '8-3 Yêu thương gấp đôi', /*Category or variation.*/ 
			'price': '300000', /*Unit price.*/ 
			'quantity': '1' /*Quantity.*/ 
		});
		

		ga('ecommerce:addTransaction', {
			'id': '',
			'affiliation': '',
			'revenue': '600000',
			'shipping': '0',
			'tax': '',
			'currency': 'VND'  /*local currency code.*/ 
		});
		ga('ecommerce:send');
	}
</script>

<!-- Facebook Pixel Code -->
<script>
	if ( window.location.href.includes('thank_you') == true ) {
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
				document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1566159606933153');
			fbq('track', 'Purchase', { content_ids: ['1009700761','1009988865'],content_type: 'product',value: 600000, currency: 'VND'});
			console.log('FB Pixel Purchase');
		}
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1566159606933153&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<!-- Ants convertion script -->
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 874352237;
		var google_conversion_language = "en";
		var google_conversion_format = "3";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "tr17CM-9h2oQ7Zz2oAM";
		var google_conversion_value = 0.00;
		var google_conversion_currency = "VND";
		var google_remarketing_only = false;
		/* ]]> */
	</script>
	<noscript async>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/874352237/?value=0.00&amp;currency_code=VND&amp;label=tr17CM-9h2oQ7Zz2oAM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
	<!-- Google Code for Checkout Conversion Page -->
	<script type="text/javascript" async>
		/* <![CDATA[ */
		var google_conversion_id = 956357071;
		var google_conversion_language = "en";
		var google_conversion_format = "3";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "N1UDCNatu14Qz7ODyAM";
		var google_remarketing_only = false;
		/* ]]> */
	</script>
	<script async="true" type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/956357071/?label=N1UDCNatu14Qz7ODyAM&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
	<script>
		if ( window.location.href.indexOf('thank_you') > 0 ) {
			var google_tag_params = {
				ecomm_pagetype: 'Thanks',
				ecomm_prodid: 'Thanks-proid',
				dynx_itemid: 'Thanks-dynxid',
				dynx_pagetype: 'Thanks',
				dynx_totalvalue: 0,
				ecomm_totalvalue: 0,
			};
		} else {
			var google_tag_params = {
				ecomm_pagetype: 'Checkout',
				ecomm_prodid: 'Checkout-proid',
				dynx_itemid: 'Checkout-dynxid',
				dynx_pagetype: 'Checkout',
				dynx_totalvalue: 0,
				ecomm_totalvalue: 0,
			};
		}
	</script>
	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 956357071;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/956357071/?guid=ON&amp;script=0"/>
		</div>
	</noscript>
	<script type='text/javascript'>var _spapi = _spapi || []; _spapi.push(['_partner', 'juno']); (function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'juno.api.sociaplus.com/partner.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); } )(); </script>
	<script src='//file.hstatic.net/1000003969/file/insider-sw-sdk.js' type='text/javascript'></script>
                                            
											
												<div class="section">
													<div class="section-header">
														<h2 class="section-title">Thông tin giao hàng</h2>
													</div>
													<div class="section-content section-customer-information no-mb">
                                                        
														
															<p class="section-content-text">
																Bạn đã có tài khoản?
																<a href="/account/login?urlredirect=%2Fcheckouts%2Ffa387af01a704505ba2b55e2320b1279%3Fstep%3D1">Đăng nhập</a>
															</p>
														
														
														<div class="fieldset">
															
															
																<div class="field field-required  ">
																	<div class="field-input-wrapper">
																		<label class="field-label" for="billing_address_full_name">Họ và tên</label>
																		<input placeholder="Họ và tên" autocapitalize="off" spellcheck="false" class="field-input" size="30" type="text" id="billing_address_full_name" name="billing_address[full_name]" value="" />
																	</div>
																	
																</div>
															
															
																
																	<div class="field  field-two-thirds  ">
																		<div class="field-input-wrapper">
																			<label class="field-label" for="checkout_user_email">Email</label>
																			<input placeholder="Email" autocapitalize="off" spellcheck="false" class="field-input" size="30" type="email" id="checkout_user_email" name="checkout_user[email]" value="" />
																		</div>
																		
																	</div>
																
															
															
																<div class="field field-required field-third  ">
																	<div class="field-input-wrapper">
																		<label class="field-label" for="billing_address_phone">Số điện thoại</label>
																		<input placeholder="Số điện thoại" autocapitalize="off" spellcheck="false" class="field-input" size="30" maxlength="11" type="tel" id="billing_address_phone" name="billing_address[phone]" value="" />
																	</div>
																	
																</div>
															
															
																<div class="field field-required  ">
																	<div class="field-input-wrapper">
																		<label class="field-label" for="billing_address_address1">Địa chỉ</label>
																		<input placeholder="Địa chỉ" autocapitalize="off" spellcheck="false" class="field-input" size="30" type="text" id="billing_address_address1" name="billing_address[address1]" value="" />
																	</div>
																	
																</div>
															
														</div>
													</div>
													<div class="section-content">
														<div class="fieldset">
															
																<form id="form_update_location" class="clearfix" accept-charset="UTF-8" method="post">
																	<input name="utf8" type="hidden" value="✓">
																	<div class="field field-required field-third  ">
																		<div class="field-input-wrapper field-input-wrapper-select">
																			<label class="field-label" for="customer_shipping_province">Tỉnh / thành</label>
																			<select class="field-input" id="customer_shipping_province" name="customer_shipping_province">
																				<option data-code="null" value="null" selected>Chọn tỉnh / thành</option>
																				
																					
																						
																							<option data-code="HC" value="50" >Hồ Chí Minh</option>
																						
																					
																						
																							<option data-code="HI" value="1" >Hà Nội</option>
																						
																					
																						
																							<option data-code="DA" value="32" >Đà Nẵng</option>
																						
																					
																						
																							<option data-code="AG" value="57" >An Giang</option>
																						
																					
																						
																							<option data-code="BV" value="49" >Bà Rịa - Vũng Tàu</option>
																						
																					
																						
																							<option data-code="BG" value="15" >Bắc Giang</option>
																						
																					
																						
																							<option data-code="BK" value="4" >Bắc Kạn</option>
																						
																					
																						
																							<option data-code="BL" value="62" >Bạc Liêu</option>
																						
																					
																						
																							<option data-code="BN" value="18" >Bắc Ninh</option>
																						
																					
																						
																							<option data-code="BT" value="53" >Bến Tre</option>
																						
																					
																						
																							<option data-code="BD" value="35" >Bình Định</option>
																						
																					
																						
																							<option data-code="BI" value="47" >Bình Dương</option>
																						
																					
																						
																							<option data-code="BP" value="45" >Bình Phước</option>
																						
																					
																						
																							<option data-code="BU" value="39" >Bình Thuận</option>
																						
																					
																						
																							<option data-code="CM" value="63" >Cà Mau</option>
																						
																					
																						
																							<option data-code="CN" value="59" >Cần Thơ</option>
																						
																					
																						
																							<option data-code="CB" value="3" >Cao Bằng</option>
																						
																					
																						
																							<option data-code="DC" value="42" >Đắk Lắk</option>
																						
																					
																						
																							<option data-code="DO" value="43" >Đắk Nông</option>
																						
																					
																						
																							<option data-code="DB" value="7" >Điện Biên</option>
																						
																					
																						
																							<option data-code="DN" value="48" >Đồng Nai</option>
																						
																					
																						
																							<option data-code="DT" value="56" >Đồng Tháp</option>
																						
																					
																						
																							<option data-code="GL" value="41" >Gia Lai</option>
																						
																					
																						
																							<option data-code="HG" value="2" >Hà Giang</option>
																						
																					
																						
																							<option data-code="HM" value="23" >Hà Nam</option>
																						
																					
																						
																							<option data-code="HT" value="28" >Hà Tĩnh</option>
																						
																					
																						
																							<option data-code="HD" value="19" >Hải Dương</option>
																						
																					
																						
																							<option data-code="HP" value="20" >Hải Phòng</option>
																						
																					
																						
																							<option data-code="HU" value="60" >Hậu Giang</option>
																						
																					
																						
																							<option data-code="HO" value="11" >Hòa Bình</option>
																						
																					
																						
																							<option data-code="HY" value="21" >Hưng Yên</option>
																						
																					
																						
																							<option data-code="KH" value="37" >Khánh Hòa</option>
																						
																					
																						
																							<option data-code="KG" value="58" >Kiên Giang</option>
																						
																					
																						
																							<option data-code="KT" value="40" >Kon Tum</option>
																						
																					
																						
																							<option data-code="LI" value="8" >Lai Châu</option>
																						
																					
																						
																							<option data-code="LD" value="44" >Lâm Đồng</option>
																						
																					
																						
																							<option data-code="LS" value="13" >Lạng Sơn</option>
																						
																					
																						
																							<option data-code="LO" value="6" >Lào Cai</option>
																						
																					
																						
																							<option data-code="LA" value="51" >Long An</option>
																						
																					
																						
																							<option data-code="ND" value="24" >Nam Định</option>
																						
																					
																						
																							<option data-code="NA" value="27" >Nghệ An</option>
																						
																					
																						
																							<option data-code="NB" value="25" >Ninh Bình</option>
																						
																					
																						
																							<option data-code="NT" value="38" >Ninh Thuận</option>
																						
																					
																						
																							<option data-code="PT" value="16" >Phú Thọ</option>
																						
																					
																						
																							<option data-code="PY" value="36" >Phú Yên</option>
																						
																					
																						
																							<option data-code="QB" value="29" >Quảng Bình</option>
																						
																					
																						
																							<option data-code="QM" value="33" >Quảng Nam</option>
																						
																					
																						
																							<option data-code="QG" value="34" >Quảng Ngãi</option>
																						
																					
																						
																							<option data-code="QN" value="14" >Quảng Ninh</option>
																						
																					
																						
																							<option data-code="QT" value="30" >Quảng Trị</option>
																						
																					
																						
																							<option data-code="ST" value="61" >Sóc Trăng</option>
																						
																					
																						
																							<option data-code="SL" value="9" >Sơn La</option>
																						
																					
																						
																							<option data-code="TN" value="46" >Tây Ninh</option>
																						
																					
																						
																							<option data-code="TB" value="22" >Thái Bình</option>
																						
																					
																						
																							<option data-code="TY" value="12" >Thái Nguyên</option>
																						
																					
																						
																							<option data-code="TH" value="26" >Thanh Hóa</option>
																						
																					
																						
																							<option data-code="TT" value="31" >Thừa Thiên Huế</option>
																						
																					
																						
																							<option data-code="TG" value="52" >Tiền Giang</option>
																						
																					
																						
																							<option data-code="TV" value="54" >Trà Vinh</option>
																						
																					
																						
																							<option data-code="TQ" value="5" >Tuyên Quang</option>
																						
																					
																						
																							<option data-code="VL" value="55" >Vĩnh Long</option>
																						
																					
																						
																							<option data-code="VT" value="17" >Vĩnh Phúc</option>
																						
																					
																						
																							<option data-code="YB" value="10" >Yên Bái</option>
																						
																					 
																				  
																			</select>
																		</div>
																		
																	</div>
																	
																		<div class="field field-required field-third  ">
																			<div class="field-input-wrapper field-input-wrapper-select">
																				<label class="field-label" for="customer_shipping_district">Quận / huyện</label>
																				<select class="field-input" id="customer_shipping_district" name="customer_shipping_district">
																					<option data-code="null" value="null" selected>Chọn quận / huyện</option>
																					
																				</select>
																			</div>
																			
																		</div>
																		
																			<div class="field  field-third  ">
																				<div class="field-input-wrapper field-input-wrapper-select">
																					<label class="field-label" for="customer_shipping_ward">Phường / xã</label>
																					<select class="field-input" id="customer_shipping_ward" name="customer_shipping_ward">
																						<option data-code="null" value="null" selected>Chọn phường / xã</option>
																						
																					</select>
																				</div>
																				
																			</div>
																		
																	
																</form>
															
														</div>
													</div>
                                                    <div id="change_pick_location_or_shipping">
													    
                                                        
                                                            
                                                                <div id="section-shipping-rate">
                                                                    <div class="section-header">
                                                                        <h2 class="section-title">Phương thức vận chuyển</h2>
                                                                    </div>
                                                                    <div class="section-content">
                                                                        
                                                                        <div class="content-box  blank-slate">
                                                                            <i class="blank-slate-icon icon icon-closed-box "></i>
                                                                            <p>Vui lòng chọn tỉnh / thành để có danh sách phương thức vận chuyển.</p>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </div>
                                                            
                                                            <div id="section-payment-method" class="section">
                                                                <div class="section-header">
                                                                    <h2 class="section-title">Phương thức thanh toán</h2>
                                                                </div>
                                                                <div class="section-content">
                                                                    <div class="content-box">
                                                                        
                                                                        
                                                                        <div class="radio-wrapper content-box-row">
                                                                            <label class="radio-label" for="payment_method_id_6381">
                                                                                <div class="radio-input">
                                                                                    <input id="payment_method_id_6381" class="input-radio" name="payment_method_id" type="radio" value="6381" checked />
                                                                                </div>
                                                                                <span class="radio-label-primary">Thanh toán khi nhận hàng (COD)</span>
                                                                            </label>
                                                                        </div>
                                                                        
                                                                        
                                                                        <div class="radio-wrapper content-box-row">
                                                                            <label class="radio-label" for="payment_method_id_532042">
                                                                                <div class="radio-input">
                                                                                    <input id="payment_method_id_532042" class="input-radio" name="payment_method_id" type="radio" value="532042"  />
                                                                                </div>
                                                                                <span class="radio-label-primary">Thanh toán online qua cổng Napas bằng thẻ ATM nội địa</span>
                                                                            </label>
                                                                        </div>
                                                                        
                                                                        
                                                                        <div class="radio-wrapper content-box-row">
                                                                            <label class="radio-label" for="payment_method_id_600773">
                                                                                <div class="radio-input">
                                                                                    <input id="payment_method_id_600773" class="input-radio" name="payment_method_id" type="radio" value="600773"  />
                                                                                </div>
                                                                                <span class="radio-label-primary">Thanh toán online qua cổng Napas bằng thẻ Visa/Master Card</span>
                                                                            </label>
                                                                        </div>
                                                                        
                                                                        
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                    </div>
												</div>
                                            
											
										</div>
										<div class="step-footer">
                                            
                                                <form id="form_next_step" accept-charset="UTF-8" method="post">
                                                    <input name="utf8" type="hidden" value="✓">
                                                    <button type="submit" class="step-footer-continue-btn btn">
                                                        <span class="btn-content">Hoàn tất đơn hàng</span>
                                                        <i class="btn-spinner icon icon-button-spinner"></i>
                                                    </button>
                                                </form>
                                                <a class="step-footer-previous-link" href="/cart">
                                                    <svg class="previous-link-icon icon-chevron icon" xmlns="http://www.w3.org/2000/svg" width="6.7" height="11.3" viewBox="0 0 6.7 11.3"><path d="M6.7 1.1l-1-1.1-4.6 4.6-1.1 1.1 1.1 1 4.6 4.6 1-1-4.6-4.6z"></path></svg>
                                                    Giỏ hàng
                                                </a>
                                            
										</div>
									</div>
								
							</div>
							<div class="main-footer">
							
							</div>
						</div>
					</div>
				
				</div>
	
			</body>
		</html>
	
