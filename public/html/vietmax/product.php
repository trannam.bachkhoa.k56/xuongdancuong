

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wood & Land</title>
     <base href="http://woodland.vn/" target="_blank, _self, _parent, _top">

	<meta name="keywords" content="Công ty gỗ,Wood & Land,gỗ công nghiệp,go cong nghiep,Thuận An Bình Dương,thuan an bình duong,thuan an,binh duong,chất lượng quốc tế,chat luong quoc te" />
    <meta name="description" content="Công ty TNHH WOOD & LAND là nhà cung cấp gỗ công nghiệp đạt chất lượng quốc tế. Hệ thống kho của Wood & Land được đặt tại thị xã Thuận An, tỉnh Bình Dương" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png" />
    <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192" />


    <link href="http://vn3c.net/html/vietmax/css/css.css" rel="stylesheet">
      <link href="http://vn3c.net/html/vietmax/css/bootstrap.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/font-awesome.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/layout.css" rel="stylesheet"/>
      <script src="http://vn3c.net/html/vietmax/js/modernizr-2.6.2.js"></script>
      <link href="http://vn3c.net/html/vietmax/css/bootstrap-dialog.css" rel="stylesheet"/>
       <link href="http://vn3c.net/html/vietmax/css/product.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/home.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/slick.css" rel="stylesheet"/>




	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-100089538-1', 'auto');
	  ga('send', 'pageview');

	</script>
    
</head>
<body>
    
      <header>
         <!--Begin header top-->
         <div class="container" id="header-top">
            <div class="row">
               <!--Logo-->
               <div class="col-md-2 logo">
                  <a href="/trang-chu">
                  <img src="http://vn3c.net/html/vietmax/upload-img/logoviet.png" class="img-logo" />
                  </a>
               </div>
               <!--End Logo-->
               <div class="col-lg-1 col-md-2 lg" style="float:right;">
                  <select id="LangId" class="language">
                     <option selected value="2">Vietnamese</option>
                     <option value="1">English</option>
                  </select>
               </div>
               <div class="col-lg-2 col-md-3 icon-lang">
                  <div class="public">
                     <a href="https://plus.google.com">
                     <i class="fa fa-google-plus-square fa-2x" style="color:#e2720e;" aria-hidden="true"></i>
                     </a>
                     <a href="https://twitter.com">
                     <i class="fa fa-twitter-square fa-2x" style="color:#34c1ef;" aria-hidden="true"></i>
                     </a>
                     <a href="https://www.facebook.com">
                     <i class="fa fa-facebook-square fa-2x" style="color:#507dcf;" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <!--End header top-->
         <!--Menu-->
         <div id="my-top-menu">
            <nav class="navbar navbar-default">
               <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav">
                        <li>
                           <a href="/trang-chu">Trang chủ</a>
                        </li>
                        <li>
                           <a href="/gioi-thieu">Giới Thiệu</a>
                        </li>
                        <li>
                           <a href="/bai-viet">B&#224;i viết</a>
                        </li>
                        <li class="dropdown">
                           <a href="/san-pham">Sản Phẩm</a>
                           <ul class="dropdown-menu">
                              <li>
                                 <a href="/san-pham/van-mdf--medium-density-fibreboard-.html">
                                 V&#193;N MDF (Medium Density Fibreboard)
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-ep-plywood.html">
                                 V&#225;n &#201;p Plywood
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-okal.html">
                                 V&#225;n Okal
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/go-tram-ghep.html">
                                 Gỗ Tr&#224;m Gh&#233;p
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/gia-cong-be-mat.html">
                                 Gia C&#244;ng Bề Mặt
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="/tin-tuc-su-kien">Tin tức &amp; Sự kiện</a>
                        </li>
                        <li>
                           <a href="/lien-he">Li&#234;n hệ</a>
                        </li>
                     </ul>
                     <form action="/tim-kiem" class="navbar-form navbar-right input-search" method="get">
                        <div class="input-group">
                           <input class="form-control" id="searchString" name="searchString" placeholder="Tìm kiếm" type="text" value="" />
                           <span class="input-group-btn">
                           <button class="btn btn-default btn-search" type="submit">
                           <i class="fa fa-search" aria-hidden="true"></i>
                           </button>
                           </span>
                        </div>
                     </form>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container-fluid -->
            </nav>
         </div>
         <!--End Menu-->
      </header>




    <div class="icon-social">
                <div class="row facebook">
                    <div class="rectangle-fb">
                        <a href="https://www.facebook.com">FACEBOOK</a>
                    </div>
                    <a href="https://www.facebook.com"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row twitter">
                    <div class="rectangle-tw">
                        <a href="https://twitter.com">TWITTER</a>
                    </div>
                    <a href="https://twitter.com"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row g-plus">
                    <div class="rectangle-gg">
                        <a href="https://plus.google.com">GOOGLE PLUS</a>
                    </div>
                    <a href="https://plus.google.com"><i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a>
                </div>
        <div class="row abc">
            <div class="rectangle-video">
                <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank">VIDEO CLIPS</a>
            </div>
            <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank"><i class="fa fa-video-camera fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="row pen">
            <div class="rectangle-pen">
                <a href="/tin-tuc-su-kien">NOTE</a>
            </div>
            <a href="#"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
    





<div class="extra">
    <img src="/Content/images/public/extra-menu.png"/>
    <div class="container">
        <div class="extra-menu">
            <ul>
                <li class="index">
                    <a href="/">
                        Trang chủ
                    </a>
                </li>
                <li><i class="fa fa-caret-right index" aria-hidden="true"></i></li>
                <li id="introduce">Sản Phẩm</li>
            </ul>
        </div>
    </div>
</div>


<div class="slide-pr">
    <div class="container">
        <div class="discover">
            <h2 style="color:#1e8c0a; font-size:18px;">
                sản phẩm của c&#244;ng ty
            </h2>
            <h2 class="hoanglam" style="font-size: 36px;margin-top: 0;">
                WOOD &amp; LAND
            </h2>
        </div>
        <div class="col-md-4">
            <div class="block-left">
                    <div class="block-left-item">
                        <img src="/Content/images/vanmdf/036784e743-67c2-4a7b-8aeb-bc11885e4072.jpg" />
                    </div>
                    <div class="block-left-item">
                        <img src="/Content/images/vanmdf/7.jpg" />
                    </div>
                    <div class="block-left-item">
                        <img src="/Content/images/vanmdf/mdf.JPG" />
                    </div>
                    <div class="block-left-item">
                        <img src="/Content/images/vanmdf/mdf3.jpg" />
                    </div>
            </div>
        </div>
        <div class="col-md-1 col-sm-8 my-block-style">
            <div class="block-center">
                    <div class="block-center-item">
                        <div class="under-img hidden-sm hidden-xs"></div>
                        <div><img class="img-hover" src="/Content/images/vanmdf/036784e743-67c2-4a7b-8aeb-bc11885e4072.jpg" /></div>
                    </div>
                    <div class="block-center-item">
                        <div class="under-img hidden-sm hidden-xs"></div>
                        <div><img class="img-hover" src="/Content/images/vanmdf/7.jpg" /></div>
                    </div>
                    <div class="block-center-item">
                        <div class="under-img hidden-sm hidden-xs"></div>
                        <div><img class="img-hover" src="/Content/images/vanmdf/mdf.JPG" /></div>
                    </div>
                    <div class="block-center-item">
                        <div class="under-img hidden-sm hidden-xs"></div>
                        <div><img class="img-hover" src="/Content/images/vanmdf/mdf3.jpg" /></div>
                    </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block-right">
                <div class="block-right-item">
                    <h1 style="font-size:14px;text-transform:uppercase;font-weight:700;">
                        V&#193;N MDF (Medium Density Fibreboard)
                    </h1>
<div style="text-align: justify;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">- &nbsp;Hiện nay v&aacute;n mdf được ứng dụng rất rộng r&atilde;i trong c&aacute;c lĩnh vực nội thất văn ph&ograve;ng, căn hộ, nh&agrave; ở, biệt thự, trường học, bệnh viện...v.v... hiện nay to&agrave;n bộ nội thất đều sử dụng v&aacute;n mdf v&igrave; gi&aacute; cả rẻ, ph&ugrave; hợp với nhiều đ&ocirc;&iacute; tượng kh&aacute;ch h&agrave;ng, m&agrave;u sắc đẹp phong ph&uacute; đa dạng.<br />
	- &nbsp;Tất cả c&aacute;c sản phẩm gỗ của Wood &amp; Land đều đạt ti&ecirc;u chuẩn Carb P2 l&agrave; quy định về ti&ecirc;u chuẩn về nồng độ độc hại trong keo để sản xuất gỗ , chất đ&oacute; gọi l&agrave; Formaldehyde. B&ecirc;n cạnh đ&oacute; gỗ Ho&agrave;ng L&acirc;m đều đạt chất lượng theo ti&ecirc;u chuẩn ch&acirc;u &acirc;u v&agrave; mỹ<br />
	- &nbsp;To&agrave;n bộ v&aacute;n mdf của wood &amp; Land đều c&oacute; chất lượng tốt , to&agrave;n bộ bề mặt gỗ kh&ocirc;ng bị cong v&ecirc;nh, bay m&agrave;u nứt mẻ...như c&aacute;c sản phẩm gỗ c&ocirc;ng nghiệp k&eacute;m chất lượng kh&aacute;c. bề mặt gỗ v&agrave; v&aacute;n&nbsp;</span></span></span></div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="choose my-choose" id="frm-quote-bottom">
    <div class="container">
        <div class="col-md-12">
            <div class="comment col-md-10">
                <div class="head-box">
                    <h5>
                        b&#225;o gi&#225; nhanh
                    </h5>
                </div>
                <p>
                    Điền th&ocirc;ng tin theo mẫu, Ch&uacute;ng t&ocirc;i sẽ b&aacute;o gi&aacute; cho bạn chỉ trong 60 ph&uacute;t
                </p>
                <form method="post" action="">
                    <div class="form-group quote-input">
                        <input id="Name1" class="input" type="text" placeholder="Name" />
                        <input id="Phone1" class="input" type="text" placeholder="Phone" />
                        <input id="Email1" class="input" type="text" placeholder="Email" />
                        <select class="form-control type myselectstyle" id="Listpro1" name="Listpro1"><option value="">--None---</option>
<option value="2">V&#193;N MDF (Medium Density Fibreboard)</option>
<option value="4">V&#225;n &#201;p Plywood</option>
<option value="6">V&#225;n Okal</option>
<option value="8">Gỗ Tr&#224;m Gh&#233;p</option>
<option value="10">Gia C&#244;ng Bề Mặt</option>
</select>
                        <input id="Numbers1" class="input" type="text" placeholder="Numbers (m3)" />
                    </div>
                    <div class="form-group block-textarea">
                        <textarea id="Requests1" class="txtinput" rows="9" placeholder="Another Request"></textarea>
                    </div>
                    <div class="clearfix"></div>
                    <div class="send-info form-group">
                        <button id="btnsendQuote1" type="button" class="btn btn-default btn-input">
                            gửi y&#234;u cầu b&#225;o gi&#225;
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="menu-02">
    <div class="container">
        <ul class="nav nav-tabs">
                <li class="active" id="df"><a data-toggle="tab" href="#home">TỔNG QUAN</a></li>
                <li><a data-toggle="tab" href="#menu1">KỸ THUẬT</a></li>
                <li><a data-toggle="tab" href="#menu2">ỨNG DỤNG</a></li>
                <li><a data-toggle="tab" href="#menu3">NHÀ CUNG CẤP</a></li>
        </ul>
    </div>
</div>
<div class="detail-content">
    <div class="container">
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="text-detail col-sm-8">
<p style="text-align: justify;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">Ván g&ocirc;̃ MDF (Medium Density Fibreboard) là loại sản ph&acirc;̉m ván g&ocirc;̃ kỹ thu&acirc;̣t, đặc bi&ecirc;̣t được sản xu&acirc;́t dùng cho ngành c&ocirc;ng nghi&ecirc;̣p sản xu&acirc;́t đ&ocirc;̀ n&ocirc;̣i th&acirc;́t và ngành m&ocirc;̣c. Những ti&ecirc;u chu&acirc;̉n kỹ thu&acirc;̣t đ&ocirc;̀ng nh&acirc;́t áp dụng tr&ecirc;n ván g&ocirc;̃ có tính nh&acirc;́t quán từ b&ecirc;̀ mặt đ&ecirc;́n t&acirc;̣n b&ecirc;n trong. Và tính đa dụng của sản ph&acirc;́m ván g&ocirc;̃ cho phép thích ứng với mọi loại máy móc gia c&ocirc;ng phức tạp nh&acirc;́t cũng như với những kỹ thu&acirc;̣t ti&ecirc;n ti&ecirc;́n nh&acirc;́t dùng trong sản xu&acirc;́t đ&ocirc;̀ n&ocirc;̣i th&acirc;́t ch&acirc;́t lượng cao, linh ki&ecirc;̣n đ&ocirc;̀ g&ocirc;̃ mỹ ngh&ecirc;̣, làm đ&ocirc;̀ g&ocirc;̃ thủ c&ocirc;ng và lót sàn.</span></span></span></p>
                </div>
                <div class="col-sm-4">
                        <div class="images">
                            <img src="/Content/images/van-mdf-dep.jpg" />
                        </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <div class="text-detail col-sm-8">
<p style="text-align: justify;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;"><strong>Quy tr&igrave;nh sản xuất MDF c&oacute; dạng: quy tr&igrave;nh kh&ocirc; v&agrave; quy tr&igrave;nh ướt<br />
	<br />
	Quy tr&igrave;nh kh&ocirc;:</strong> sẽ d&ugrave;ng keo v&agrave; phụ gia ho&agrave; trộn v&agrave;o bột gỗ kh&ocirc; trong m&aacute;y trộn sau đ&oacute; đem sấy kh&ocirc;. Bột sợi đ&atilde; c&oacute; keo sẽ bỏ v&agrave;o m&aacute;y rải sẽ được rải th&agrave;nh 2- 3 lớp thuỳ theo khổ k&iacute;ch thước của v&aacute;n cần sản xuất. c&aacute;c lớp n&agrave;y sẽ được đưa v&ocirc; m&aacute;y &eacute;p nhiệt, m&aacute;y &eacute;p thực hiện &eacute;p nhiều lần. lần 1 ( l&agrave; &eacute;p sơ bộ) cho lớp tr&ecirc;n, lớp thứ 2, lớp thứ 3, lần thứ 2 sẽ &eacute;p cả ba lớp lại. khi &eacute;p nhiệt độ sẽ được thiết lập sao cho keo v&agrave; nước sẽ cứng từ từ. sau khi &eacute;p v&aacute;n &eacute;p được lấy ra cắt bỏ bi&ecirc;n, sau đ&oacute; ch&agrave; nh&aacute;m v&agrave; ph&acirc;n loại.<br />
	&nbsp;<br />
	<strong>Quy tr&igrave;nh ướt</strong>: bột gỗ sẽ được trộn với nước kết th&agrave;nh dạng vầy ( mat formation ) sau đ&oacute; sẽ rải ch&uacute;ng l&ecirc;n m&acirc;m &eacute;p. D&ugrave;ng m&aacute;y nhiệt &eacute;p một lần đến độ d&agrave;y sơ bộ, sau đ&oacute; đ&oacute; v&aacute;n mdf sẽ được đưa m&aacute;y c&aacute;n hơi nhiệt ở nhiệt độ cao giống như b&ecirc;n l&agrave;m giấy để n&eacute;n chặt hai mặt v&agrave; r&uacute;t nước đưa ra.</span></span></span></p>

                </div>
                <div class="col-sm-4">
                        <div class="images">
                            <img src="/Content/images/van-mdf.jpg" />
                        </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <div id="menu2" class="tab-pane fade">
                <div class="text-detail col-sm-8">
<p>
	<span style="color:#000000;"><span style="font-family:tahoma,geneva,sans-serif;"><span style="font-size:14px;">V&aacute;n MDF được d&ugrave;ng trong thiết kế nội thất nh&agrave; ở, nội thất văn ph&ograve;ng như : tủ ,b&agrave;n ghế, giường ngủ, tủ nh&agrave; bếp,...<br />
	<br />
	V&aacute;n MDF ng&agrave;y nay được rất nhiều người ưa chuộng , ngo&agrave;i đặc t&iacute;nh đẹp bền th&igrave; gi&aacute; th&agrave;nh của v&aacute;n MDF rất rẻ, dễ mua. N&ecirc;n được ứng dụng nhiều trogn nội thất v&agrave; nội thất văn ph&ograve;ng, v&agrave; trong x&acirc;y dựng. c&ugrave;ng với sự ph&aacute;t triển của khoa học kỹ thuật c&aacute;c nh&agrave; m&aacute;y sản xuất v&aacute;n MDF họ đ&atilde; kiểm so&aacute;t được độ ẩm trong gỗ, l&agrave;m cho tuổi thọ sử dụng tăng l&ecirc;n, n&oacute; gi&uacute;p tao ra nhiều loại v&aacute;n gỗ MDF kh&aacute;c nhau ứng dụng trong nhiều lĩnh vực đời sống h&agrave;ng ng&agrave;y.<br />
	<br />
	Trong x&acirc;y dựng v&aacute;n gỗ MDF được d&ugrave;ng l&agrave;m v&aacute;ch trần nh&agrave;, v&aacute;ch ngăn, trang tr&iacute; cửa ra vao...</span></span></span></p>
<p style="font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); margin-top: 0px; text-align: justify; font-size: 13px;">
	<span style="color:#000000;"><span style="font-family:tahoma,geneva,sans-serif;"><span style="font-size:14px;">MDF c&ograve;n nhiều ứng dụng kh&aacute;c: d&ugrave;ng trong xe lửa, t&agrave;u b&egrave;, bao b&igrave;, th&ugrave;ng loa, &acirc;m ly.</span></span></span></p>
<div>
	&nbsp;</div>
<br />

                </div>
                <div class="col-sm-4">
                        <div class="images">
                            <img src="/Content/images/Product1/ung-dung-van-mdf.jpg" />
                        </div>

                </div>
                <div class="clearfix"></div>
            </div>
            <div id="menu3" class="tab-pane fade">
                <div class="text-detail col-sm-8">
<div style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify; outline: 0px !important;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">C&ocirc;ng ty TNHH WOOD &amp; LAND&nbsp; l&agrave; nh&agrave; cung cấp gỗ c&ocirc;ng nghiệp đạt chất lượng quốc tế tại thị trường Việt Nam. Hệ Thống kho của WOOD &amp; LAND được đặt tại &nbsp;TX. Thuận An, Tỉnh B&igrave;nh Dương.</span></span></span></div>
<div>
	&nbsp;</div>

                </div>
                <div class="col-sm-4">
                        <div class="images">
                            <img src="/Content/images/ncc.jpg" />
                        </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="button">
    <div class="container">
        <div class="col-sm-4 return">
            <a href="/san-pham">
                quay lại trang sản phẩm
            </a>
        </div>
        <div class="col-sm-4 col-sm-offset-4 return">
            <a role="button" class="scroll-to-frm-pro" idproduct="2">
                y&#234;u cầu b&#225;o gi&#225;
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>


    <div class="loadding">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
    



<footer>
    <div class="container info">
            <div class="row">
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-phone-square fa-2x" aria-hidden="true"></i>
                    <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">HOTLINE</strong>
                    <a href="tel:0908759007">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0908759007</strong>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-briefcase fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kinh doanh</strong>
                    <a href="tel:0932100411">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0932100411</strong>
                    </a>
                    <strong class="mail"> minhphuongwl@gmail.com</strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-credit-card-alt fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kế toán</strong>
                    <a href="tel:0987477937">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0987477937</strong>
                    </a>
                    <strong class="mail">huyenngoc2001@yahoo.com </strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-truck fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">giao nhận</strong>
                    <a href="tel:0274 6515129">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0274 6515129</strong>
                    </a>
                    <strong class="mail">daoquynhnhu98@gmail.com </strong>
                </div>
                <div class="clearfix"></div>
            </div>

    </div>
    <div class="form-email">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-4 email">
                    <h4 style="color:white;">
                        nhận tin từ ch&#250;ng t&#244;i
                    </h4>
                </div>
                <div class="col-sm-5 col-xs-7 newsletter-outer">
                    <form class="navbar-form navbar-right input-email">
                        <div class="input-group">
                            <input type="text" class="txt-mail" id="txtEmail" placeholder="Nhập địa chỉ email" />
                            <span class="input-group-btn">
                                <button id="msg" class="btn btn-secondary" type="button">
                                    <i class="fa fa-caret-right" style="color:#fff;font-size:18px;"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/dia-diem.png" /> 
                            th&#244;ng tin li&#234;n hệ
                        </strong>
                        <p class="p-margin">
                            121/62 Phạm Ngọc Thạch, Khu 5, Phường Hi&#234;̣p Thành, Tp Thủ D&#226;̀u M&#244;̣t, Bình Dương
                        </p>
                            <p>
                                Điện thoại: 0274 6515129
                            </p>
                        <p>
                            Fax: 0274 3515129
                        </p>
                    </div>
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/hop-thu.png" /> 
                                chi tiết li&#234;n hệ
                        </strong>
                        <p class="p-margin">Email:<br />lienphuonghl@yahoo.com</p>
                        <div class="col-md-4 contact-us">
                            <a href="/lien-he">li&#234;n hệ</a>
                        </div>
                    </div>
                    <div class="col-md-4 contact" style="margin-top:20px;">
                        <a href="tel:0908759007">
                            <strong style="color:#1e8c0a; font-size:30px; font-weight:700;">
                                0908759007
                            </strong>
                        </a>
                            <p class="p-margin">Giờ l&#224;m việc 7:30am - 4:30pm</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        
    </div>
</footer>
<a id="back-top"><i class="fa fa-play"></i></a>

 <script src="http://vn3c.net/html/vietmax/js/jquery-1.10.2.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/register-email-news.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/respond.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap-dialog.js"></script>


    
    
    <script type="text/javascript">
        $(document).ready(function () {
            //change language
            $("#LangId").change(function () {
                var langId = this.value;
                $.ajax({
                    type: 'POST',
                    url: "/CommonPartial/ChangeLanguage",
                    data: { langId: langId },
                    success: function (response) {
                        window.location.assign('/');
                    }
                });
            });
            //fix menu
            $(window).scroll(function () {
                if ($(this).scrollTop() > 136) {
                    $("#my-top-menu").addClass("fix-menu");
                }
                else {
                    $("#my-top-menu").removeClass("fix-menu");
                }
                //back top
                if ($(this).scrollTop() > 400) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });
            $('#back-top').click(function () {
                $('html,body').animate({
                    scrollTop: 0
                }, "slow");
            });
            $(".topic-content p").each(function () {
                text = $(this).text();
                if (text.length > 280) {
                    $(this).html(text.substr(0, 275) + '<a class="elipsis">...</a>');
                }
            });
        });
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/57f7620b0188071f8b8efd21/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->


      <script src="http://vn3c.net/html/vietmax/js/slick.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/frmQuote.js"></script>
    
   

    <script>
        $(document).ready(function () {
            var idproduct = 2;
            $("#frm-quote-bottom #Listpro1").val(idproduct);
            $(".scroll-to-frm-pro").click(function () {
                $("html,body").animate({
                    scrollTop:$(".my-choose").offset().top}, "slow");
            });

            $('.block-center').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                asNavFor: '.block-left',
                focusOnSelect: true,
                infinite:true,
                vertical: true,
                autoplay: true,
                autoplaySpeed: 5000,
                speed: 1000,
                responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        vertical: false
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        vertical: false
                    }
                },
                {
                    breakpoint: 560,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll:1,
                        arrows: false,
                        vertical: false
                    }
                },
                 {
                     breakpoint: 435,
                     settings: {
                         slidesToShow: 3,
                         slidesToScroll: 1,
                         arrows: false,
                         vertical: false
                     }
                 },
                  {
                      breakpoint: 350,
                      settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                          arrows: false,
                          vertical: false
                      }
                  }
                ]
            });
            $('.block-left').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                infinite: true,
                dots: false,
                asNavFor: '.block-center',
                vertical: true,
                autoplay: true,
                autoplaySpeed: 5000,
                speed: 1000
            });
        });
    </script>


</body>
</html>
