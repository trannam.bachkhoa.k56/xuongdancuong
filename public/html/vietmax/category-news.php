

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wood & Land</title>
    <base href="http://woodland.vn/" target="_blank, _self, _parent, _top">
	<meta name="keywords" content="Công ty gỗ,Wood & Land,gỗ công nghiệp,go cong nghiep,Thuận An Bình Dương,thuan an bình duong,thuan an,binh duong,chất lượng quốc tế,chat luong quoc te" />
    <meta name="description" content="Công ty TNHH WOOD & LAND là nhà cung cấp gỗ công nghiệp đạt chất lượng quốc tế. Hệ thống kho của Wood & Land được đặt tại thị xã Thuận An, tỉnh Bình Dương" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png" />
    <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192" />


    <link href="http://vn3c.net/html/vietmax/css/css.css" rel="stylesheet">
    <link href="http://vn3c.net/html/vietmax/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="http://vn3c.net/html/vietmax/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="http://vn3c.net/html/vietmax/css/layout.css" rel="stylesheet"/>
    <script src="http://vn3c.net/html/vietmax/js/modernizr-2.6.2.js"></script>
    <link href="http://vn3c.net/html/vietmax/css/bootstrap-dialog.css" rel="stylesheet"/>
    <link href="http://vn3c.net/html/vietmax/css/home.css" rel="stylesheet"/>
    <link href="http://vn3c.net/html/vietmax/css/slick.css" rel="stylesheet"/>


    

    
    <link href="http://vn3c.net/html/vietmax/css/news.css" rel="stylesheet"/>


	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-100089538-1', 'auto');
	  ga('send', 'pageview');

	</script>
    
</head>
<body>
    
     <header>
         <!--Begin header top-->
         <div class="container" id="header-top">
            <div class="row">
               <!--Logo-->
               <div class="col-md-2 logo">
                  <a href="/trang-chu">
                  <img src="http://vn3c.net/html/vietmax/upload-img/logoviet.png" class="img-logo" />
                  </a>
               </div>
               <!--End Logo-->
               <div class="col-lg-1 col-md-2 lg" style="float:right;">
                  <select id="LangId" class="language">
                     <option selected value="2">Vietnamese</option>
                     <option value="1">English</option>
                  </select>
               </div>
               <div class="col-lg-2 col-md-3 icon-lang">
                  <div class="public">
                     <a href="https://plus.google.com">
                     <i class="fa fa-google-plus-square fa-2x" style="color:#e2720e;" aria-hidden="true"></i>
                     </a>
                     <a href="https://twitter.com">
                     <i class="fa fa-twitter-square fa-2x" style="color:#34c1ef;" aria-hidden="true"></i>
                     </a>
                     <a href="https://www.facebook.com">
                     <i class="fa fa-facebook-square fa-2x" style="color:#507dcf;" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <!--End header top-->
         <!--Menu-->
         <div id="my-top-menu">
            <nav class="navbar navbar-default">
               <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav">
                        <li>
                           <a href="/trang-chu">Trang chủ</a>
                        </li>
                        <li>
                           <a href="/gioi-thieu">Giới Thiệu</a>
                        </li>
                        <li>
                           <a href="/bai-viet">B&#224;i viết</a>
                        </li>
                        <li class="dropdown">
                           <a href="/san-pham">Sản Phẩm</a>
                           <ul class="dropdown-menu">
                              <li>
                                 <a href="/san-pham/van-mdf--medium-density-fibreboard-.html">
                                 V&#193;N MDF (Medium Density Fibreboard)
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-ep-plywood.html">
                                 V&#225;n &#201;p Plywood
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-okal.html">
                                 V&#225;n Okal
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/go-tram-ghep.html">
                                 Gỗ Tr&#224;m Gh&#233;p
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/gia-cong-be-mat.html">
                                 Gia C&#244;ng Bề Mặt
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="/tin-tuc-su-kien">Tin tức &amp; Sự kiện</a>
                        </li>
                        <li>
                           <a href="/lien-he">Li&#234;n hệ</a>
                        </li>
                     </ul>
                     <form action="/tim-kiem" class="navbar-form navbar-right input-search" method="get">
                        <div class="input-group">
                           <input class="form-control" id="searchString" name="searchString" placeholder="Tìm kiếm" type="text" value="" />
                           <span class="input-group-btn">
                           <button class="btn btn-default btn-search" type="submit">
                           <i class="fa fa-search" aria-hidden="true"></i>
                           </button>
                           </span>
                        </div>
                     </form>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container-fluid -->
            </nav>
         </div>
         <!--End Menu-->
      </header>


    <div class="icon-social">
                <div class="row facebook">
                    <div class="rectangle-fb">
                        <a href="https://www.facebook.com">FACEBOOK</a>
                    </div>
                    <a href="https://www.facebook.com"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row twitter">
                    <div class="rectangle-tw">
                        <a href="https://twitter.com">TWITTER</a>
                    </div>
                    <a href="https://twitter.com"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row g-plus">
                    <div class="rectangle-gg">
                        <a href="https://plus.google.com">GOOGLE PLUS</a>
                    </div>
                    <a href="https://plus.google.com"><i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a>
                </div>
        <div class="row abc">
            <div class="rectangle-video">
                <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank">VIDEO CLIPS</a>
            </div>
            <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank"><i class="fa fa-video-camera fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="row pen">
            <div class="rectangle-pen">
                <a href="/tin-tuc-su-kien">NOTE</a>
            </div>
            <a href="#"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
    







<div class="extra">
    <img src="/Content/images/public/extra-menu.png"/>
    <div class="container">
        <div class="extra-menu">
            <ul>
                <li class="index">
                    <a href="/">
                        Trang chủ
                    </a>
                </li>
                <li><i class="fa fa-caret-right index" aria-hidden="true"></i></li>
                <li id="introduce">Tin tức &amp; Sự kiện</li>
            </ul>
        </div>
    </div>
</div>

<div class="article-outer">
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="discover">
                <h2 style="color:#1e8c0a; font-size:18px;">
                    tin tức &amp; sự kiện của
                </h2>
                <h1 class="hoanglam">
                    WOOD &amp; LAND
                </h1>
            </div>
            <div class="article-title">
                <p>
                    <div style="text-align: justify;">
	<span style="font-family: tahoma, geneva, sans-serif; font-size: 16px;">WOOD &amp; LAND</span><span style="font-size:16px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;cam kết cung cấp sản phẩm đạt ti&ecirc;u chuẩn Ch&acirc;u &Acirc;u, ti&ecirc;u chuẩn về hệ thống kho vận. Tầm nh&igrave;n của&nbsp;</span></span><span style="font-family: tahoma, geneva, sans-serif; font-size: 16px;">WOOD &amp; LAND</span><span style="font-size:16px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;l&agrave; TRỞ TH&Agrave;NH NH&Agrave; CUNG CẤP GỖ C&Ocirc;NG NGHIỆP CHẤT LƯỢNG QUỐC TẾ H&Agrave;NG ĐẦU TẠI VIỆT NAM. Ch&uacute;ng t&ocirc;i đạt được mục ti&ecirc;u th&ocirc;ng qua c&aacute;c quy tr&igrave;nh đảm bảo chất lượng, đổi mới kỹ thuật v&agrave; dịch vụ kh&aacute;ch h&agrave;ng xuất sắc.</span></span></div>

                </p>
            </div>
        </div>
    </div>
</div>



<div class="topic">
    <div class="container">
        <div class="tab-content">
            <div id="home">
                <!-- Topic 01 -->
                <div class="topic-01">
                            <div class="col-md-6">
                                <div class="images">
                                    <a href="/tin-tuc-su-kien/thong-bao-thay-doi-ten-va-ma-so-thue-doanh-nghiep.html">
                                        <img src="/Content/images/26112417_716925615166686_1070601193420164657_n.jpg" />
                                    </a>
                                </div>
                                <div class="topic-content">
                                    <a href="/tin-tuc-su-kien/thong-bao-thay-doi-ten-va-ma-so-thue-doanh-nghiep.html">
                                        <h5>
                                            TH&#212;NG B&#193;O THAY ĐỔI T&#202;N V&#192; M&#195; SỐ THUẾ DOANH NGHIỆP
                                        </h5>
                                    </a>
                                    <p>
                                        C&oacute; hiệu lực kể từ ng&agrave;y 01/01/2018
                                    </p>

                                </div>
                                <div class="details">
                                    <a href="/tin-tuc-su-kien/thong-bao-thay-doi-ten-va-ma-so-thue-doanh-nghiep.html">
                                        chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="images">
                                    <a href="/tin-tuc-su-kien/xuat-kho-don-hang-van-san.html">
                                        <img src="/Content/images/VAN-EP.jpg" />
                                    </a>
                                </div>
                                <div class="topic-content">
                                    <a href="/tin-tuc-su-kien/xuat-kho-don-hang-van-san.html">
                                        <h5>
                                            XUẤT KHO ĐƠN H&#192;NG V&#193;N S&#192;N
                                        </h5>
                                    </a>
                                    <p>
                                        <div style="text-align: justify;">
	<span style="font-family:times new roman,times,serif;"><span style="font-size:12px;">&nbsp; &nbsp; C&ocirc;ng ty TNHH&nbsp;</span></span><span style="color: rgb(0, 0, 0); font-family: tahoma, geneva, sans-serif; font-size: 14px;">WOOD &amp; LAND</span><span style="font-family:times new roman,times,serif;"><span style="font-size:12px;">&nbsp;vừa mới xuất kho đơn h&agrave;ng v&aacute;n &eacute;p chịu nước 2 mặt bạch dương phủ 1 mặt veneer Ash d&ugrave;ng l&agrave;m v&aacute;n s&agrave;n cho kh&aacute;ch h&agrave;ng tại Tp.HCM. Nguy&ecirc;n liệu v&aacute;n &eacute;p đảm bảo được kh&aacute;ch h&agrave;ng tin d&ugrave;ng trong thời gian qua.</span></span></div>

                                    </p>

                                </div>
                                <div class="details">
                                    <a href="/tin-tuc-su-kien/xuat-kho-don-hang-van-san.html">
                                        chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="images">
                                    <a href="/tin-tuc-su-kien/trang-tri-mat-tren-tam-van-mdf--van-okal--pb---van-ep-la-gi--.html">
                                        <img src="/Content/images/tin%20tuc/tin%20tiuc%20174.jpg" />
                                    </a>
                                </div>
                                <div class="topic-content">
                                    <a href="/tin-tuc-su-kien/trang-tri-mat-tren-tam-van-mdf--van-okal--pb---van-ep-la-gi--.html">
                                        <h5>
                                            Trang tr&#237; mặt tr&#234;n tấm V&#225;n MDF, V&#225;n Okal( PB), V&#225;n &#233;p l&#224; g&#236;? 
                                        </h5>
                                    </a>
                                    <p>
                                        <p style="text-align: justify;">
	<span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;"><span style="color: rgb(0, 0, 0);">B&agrave;i viết trước M&igrave;nh c&oacute; đề cập đến vấn đề trang tr&iacute; mặt tr&ecirc;n tấm Gỗ c&ocirc;ng nghiệp( V&aacute;n MDF, V&Aacute;N OKAL, V&Aacute;N &Eacute;P). Vậy TRANG TR&Iacute; MẶT l&agrave; g&igrave; :</span></span></span><br />
	<span style="font-family: tahoma, geneva, sans-serif; font-size: 14px; color: rgb(0, 0, 0);">Phủ Veneer, Melamin, Giấy( &nbsp;Giấy v&acirc;n gỗ, Giấy đơn sắc), Poly, UV được gọi l&agrave; TRANG TR&Iacute; MẶT. &nbsp;T&ugrave;y v&agrave;o nhu cầu sản xuất của mỗi nh&agrave; m&aacute;y c&oacute; y&ecirc;u cầu trang tr&iacute; kh&aacute;c nhau.</span></p>
<br />

                                    </p>

                                </div>
                                <div class="details">
                                    <a href="/tin-tuc-su-kien/trang-tri-mat-tren-tam-van-mdf--van-okal--pb---van-ep-la-gi--.html">
                                        chi tiết
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="images">
                                    <a href="/tin-tuc-su-kien/nhap-kho-go-tram-ghep-20mmx1200x2400-tai-kho-cong-ty-wood---land.html">
                                        <img src="/Content/images/tin%20tuc/tin%20tiuc%20163.jpg" />
                                    </a>
                                </div>
                                <div class="topic-content">
                                    <a href="/tin-tuc-su-kien/nhap-kho-go-tram-ghep-20mmx1200x2400-tai-kho-cong-ty-wood---land.html">
                                        <h5>
                                            Nhập kho Gỗ tr&#224;m gh&#233;p 20mmx1200x2400 tại Kho C&#244;ng ty WOOD &amp; LAND
                                        </h5>
                                    </a>
                                    <p>
                                        <div style="text-align: justify;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">Đầu năm mới , cont Gỗ TR&Agrave;M GH&Eacute;P 20mm &nbsp;ti&ecirc;u chuẩn QC đ&atilde; về đến kho. C&aacute;c anh chị em C&ocirc;ng ty&nbsp;</span></span></span><span style="color: rgb(0, 0, 0); font-family: tahoma, geneva, sans-serif; font-size: 14px;">WOOD &amp; LAND</span><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;hăng h&aacute;i xuống cont giữa trời nắng ch&oacute;i chang để kịp thời gian giao nhanh cho những đơn h&agrave;ng của kh&aacute;ch h&agrave;ng đ&atilde; đặt trước tết.</span></span></span></div>

                                    </p>

                                </div>
                                <div class="details">
                                    <a href="/tin-tuc-su-kien/nhap-kho-go-tram-ghep-20mmx1200x2400-tai-kho-cong-ty-wood---land.html">
                                        chi tiết
                                    </a>
                                </div>
                            </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="line"></div>
<div class="container">
    <div class="pagination-outer">
        <div class="paging">
            <div class="row">
                <nav aria-label="Page navigation">
                    <div class="pagination-container"><ul class="pagination"><li class="active"><a>1</a></li><li><a href="/tin-tuc-su-kien?page=2">2</a></li><li><a href="/tin-tuc-su-kien?page=3">3</a></li><li><a href="/tin-tuc-su-kien?page=4">4</a></li><li class="PagedList-skipToNext"><a href="/tin-tuc-su-kien?page=2" rel="next">»</a></li></ul></div>
                </nav>
            </div>
        </div>
    </div>
    
</div>

    <div class="loadding">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
    



<footer>
    <div class="container info">
            <div class="row">
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-phone-square fa-2x" aria-hidden="true"></i>
                    <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">HOTLINE</strong>
                    <a href="tel:0908759007">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0908759007</strong>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-briefcase fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kinh doanh</strong>
                    <a href="tel:0932100411">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0932100411</strong>
                    </a>
                    <strong class="mail"> minhphuongwl@gmail.com</strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-credit-card-alt fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kế toán</strong>
                    <a href="tel:0987477937">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0987477937</strong>
                    </a>
                    <strong class="mail">huyenngoc2001@yahoo.com </strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-truck fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">giao nhận</strong>
                    <a href="tel:0274 6515129">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0274 6515129</strong>
                    </a>
                    <strong class="mail">daoquynhnhu98@gmail.com </strong>
                </div>
                <div class="clearfix"></div>
            </div>

    </div>
    <div class="form-email">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-4 email">
                    <h4 style="color:white;">
                        nhận tin từ ch&#250;ng t&#244;i
                    </h4>
                </div>
                <div class="col-sm-5 col-xs-7 newsletter-outer">
                    <form class="navbar-form navbar-right input-email">
                        <div class="input-group">
                            <input type="text" class="txt-mail" id="txtEmail" placeholder="Nhập địa chỉ email" />
                            <span class="input-group-btn">
                                <button id="msg" class="btn btn-secondary" type="button">
                                    <i class="fa fa-caret-right" style="color:#fff;font-size:18px;"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/dia-diem.png" /> 
                            th&#244;ng tin li&#234;n hệ
                        </strong>
                        <p class="p-margin">
                            121/62 Phạm Ngọc Thạch, Khu 5, Phường Hi&#234;̣p Thành, Tp Thủ D&#226;̀u M&#244;̣t, Bình Dương
                        </p>
                            <p>
                                Điện thoại: 0274 6515129
                            </p>
                        <p>
                            Fax: 0274 3515129
                        </p>
                    </div>
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/hop-thu.png" /> 
                                chi tiết li&#234;n hệ
                        </strong>
                        <p class="p-margin">Email:<br />lienphuonghl@yahoo.com</p>
                        <div class="col-md-4 contact-us">
                            <a href="/lien-he">li&#234;n hệ</a>
                        </div>
                    </div>
                    <div class="col-md-4 contact" style="margin-top:20px;">
                        <a href="tel:0908759007">
                            <strong style="color:#1e8c0a; font-size:30px; font-weight:700;">
                                0908759007
                            </strong>
                        </a>
                            <p class="p-margin">Giờ l&#224;m việc 7:30am - 4:30pm</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        
    </div>
</footer>
<a id="back-top"><i class="fa fa-play"></i></a>
    
     <script src="http://vn3c.net/html/vietmax/js/jquery-1.10.2.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/register-email-news.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/respond.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap-dialog.js"></script>

   

    
    <script type="text/javascript">
        $(document).ready(function () {
            //change language
            $("#LangId").change(function () {
                var langId = this.value;
                $.ajax({
                    type: 'POST',
                    url: "/CommonPartial/ChangeLanguage",
                    data: { langId: langId },
                    success: function (response) {
                        window.location.assign('/');
                    }
                });
            });
            //fix menu
            $(window).scroll(function () {
                if ($(this).scrollTop() > 136) {
                    $("#my-top-menu").addClass("fix-menu");
                }
                else {
                    $("#my-top-menu").removeClass("fix-menu");
                }
                //back top
                if ($(this).scrollTop() > 400) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });
            $('#back-top').click(function () {
                $('html,body').animate({
                    scrollTop: 0
                }, "slow");
            });
            $(".topic-content p").each(function () {
                text = $(this).text();
                if (text.length > 280) {
                    $(this).html(text.substr(0, 275) + '<a class="elipsis">...</a>');
                }
            });
        });
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/57f7620b0188071f8b8efd21/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    
</body>
</html>
