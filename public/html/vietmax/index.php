<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Wood & Land</title>
	  
      <base href="http://woodland.vn/" target="_blank, _self, _parent, _top">
	  

      <meta name="keywords" content="Công ty gỗ,Wood & Land,gỗ công nghiệp,go cong nghiep,Thuận An Bình Dương,thuan an bình duong,thuan an,binh duong,chất lượng quốc tế,chat luong quoc te" />
      <meta name="description" content="Công ty TNHH WOOD & LAND là nhà cung cấp gỗ công nghiệp đạt chất lượng quốc tế. Hệ thống kho của Wood & Land được đặt tại thị xã Thuận An, tỉnh Bình Dương" />
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png" />
      <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192" />
      
      <link href="http://vn3c.net/html/vietmax/css/css.css" rel="stylesheet">
      <link href="http://vn3c.net/html/vietmax/css/bootstrap.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/font-awesome.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/layout.css" rel="stylesheet"/>
      <script src="http://vn3c.net/html/vietmax/js/modernizr-2.6.2.js"></script>
      <link href="http://vn3c.net/html/vietmax/css/bootstrap-dialog.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/home.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/slick.css" rel="stylesheet"/>
      <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','http://localhost/woodland/js/analytics.js','ga');
         
         ga('create', 'UA-100089538-1', 'auto');
         ga('send', 'pageview');
         
      </script>
   </head>
   <body>
      <header>
         <!--Begin header top-->
         <div class="container" id="header-top">
            <div class="row">
               <!--Logo-->
               <div class="col-md-2 logo">
                  <a href="/trang-chu">
                  <img src="http://vn3c.net/html/vietmax/upload-img/logoviet.png" class="img-logo" />
                  </a>
               </div>
               <!--End Logo-->
               <div class="col-lg-1 col-md-2 lg" style="float:right;">
                  <select id="LangId" class="language">
                     <option selected value="2">Vietnamese</option>
                     <option value="1">English</option>
                  </select>
               </div>
               <div class="col-lg-2 col-md-3 icon-lang">
                  <div class="public">
                     <a href="https://plus.google.com">
                     <i class="fa fa-google-plus-square fa-2x" style="color:#e2720e;" aria-hidden="true"></i>
                     </a>
                     <a href="https://twitter.com">
                     <i class="fa fa-twitter-square fa-2x" style="color:#34c1ef;" aria-hidden="true"></i>
                     </a>
                     <a href="https://www.facebook.com">
                     <i class="fa fa-facebook-square fa-2x" style="color:#507dcf;" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <!--End header top-->
         <!--Menu-->
         <div id="my-top-menu">
            <nav class="navbar navbar-default">
               <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav">
                        <li>
                           <a href="/trang-chu">Trang chủ</a>
                        </li>
                        <li>
                           <a href="/gioi-thieu">Giới Thiệu</a>
                        </li>
                        <li>
                           <a href="/bai-viet">B&#224;i viết</a>
                        </li>
                        <li class="dropdown">
                           <a href="/san-pham">Sản Phẩm</a>
                           <ul class="dropdown-menu">
                              <li>
                                 <a href="/san-pham/van-mdf--medium-density-fibreboard-.html">
                                 V&#193;N MDF (Medium Density Fibreboard)
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-ep-plywood.html">
                                 V&#225;n &#201;p Plywood
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-okal.html">
                                 V&#225;n Okal
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/go-tram-ghep.html">
                                 Gỗ Tr&#224;m Gh&#233;p
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/gia-cong-be-mat.html">
                                 Gia C&#244;ng Bề Mặt
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="/tin-tuc-su-kien">Tin tức &amp; Sự kiện</a>
                        </li>
                        <li>
                           <a href="/lien-he">Li&#234;n hệ</a>
                        </li>
                     </ul>
                     <form action="/tim-kiem" class="navbar-form navbar-right input-search" method="get">
                        <div class="input-group">
                           <input class="form-control" id="searchString" name="searchString" placeholder="Tìm kiếm" type="text" value="" />
                           <span class="input-group-btn">
                           <button class="btn btn-default btn-search" type="submit">
                           <i class="fa fa-search" aria-hidden="true"></i>
                           </button>
                           </span>
                        </div>
                     </form>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container-fluid -->
            </nav>
         </div>
         <!--End Menu-->
      </header>
      <div class="icon-social">
         <div class="row facebook">
            <div class="rectangle-fb">
               <a href="https://www.facebook.com">FACEBOOK</a>
            </div>
            <a href="https://www.facebook.com"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
         </div>
         <div class="row twitter">
            <div class="rectangle-tw">
               <a href="https://twitter.com">TWITTER</a>
            </div>
            <a href="https://twitter.com"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
         </div>
         <div class="row g-plus">
            <div class="rectangle-gg">
               <a href="https://plus.google.com">GOOGLE PLUS</a>
            </div>
            <a href="https://plus.google.com"><i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a>
         </div>
         <div class="row abc">
            <div class="rectangle-video">
               <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank">VIDEO CLIPS</a>
            </div>
            <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank"><i class="fa fa-video-camera fa-2x" aria-hidden="true"></i></a>
         </div>
         <div class="row pen">
            <div class="rectangle-pen">
               <a href="/tin-tuc-su-kien">NOTE</a>
            </div>
            <a href="#"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>
         </div>
         <div class="clearfix"></div>
      </div>
      <!-- Slide -->
      <div class="banner-slider">
         <div class="slider-item">
            <img class="images" src="/Content/images/trangchu/new-banner-top.png" />
            <div class="title col-md-7 col-md-offset-2">
               <div class="container text-left">
                  <h5>nhà cung cấp sản phẩm</h5>
                  <p class="okal">okal/mdf</p>
                  <p class="flywood">flywood</p>
                  <div class="line-decorate">
                     <span class="sub-line-1"></span>
                  </div>
                  <div class="clearfix"></div>
                  <p class="text">
                  <div style="text-align: justify;">
                     <div>
                        <div>
                           <div>
                              Với Hơn 15 đối t&aacute;c lớn tr&ecirc;n thế giới, V&agrave; hệ thống kho tr&ecirc;n 5000m2<br />
                              Wood &amp; Land cung cấp cho kh&aacute;ch h&agrave;ng hơn 500.000 m3 gỗ chất lượng mỗi năm.
                           </div>
                        </div>
                     </div>
                  </div>
                  <br />
                  </p>
               </div>
            </div>
         </div>
         <div class="slider-item">
            <img class="images" src="/Content/images/trangchu/01-bnhoanglam.png" />
            <div class="title col-md-7 col-md-offset-2">
               <div class="container text-left">
                  <h5>nhà cung cấp sản phẩm</h5>
                  <p class="okal">okal/mdf</p>
                  <p class="flywood">flywood</p>
                  <div class="line-decorate">
                     <span class="sub-line-1"></span>
                  </div>
                  <div class="clearfix"></div>
                  <p class="text">
                  <div style="text-align: justify;">
                     <div>
                        <div>
                           <div>
                              Với Hơn 15 đối t&aacute;c lớn tr&ecirc;n thế giới, V&agrave; hệ thống kho tr&ecirc;n 5000m2<br />
                              Wood &amp; Land cung cấp cho kh&aacute;ch h&agrave;ng hơn 500.000 m3 gỗ chất lượng mỗi năm.
                           </div>
                        </div>
                     </div>
                  </div>
                  <br />
                  </p>
               </div>
            </div>
         </div>
         <div class="slider-item">
            <img class="images" src="/Content/images/trangchu/02-bnhoanglam.png" />
            <div class="title col-md-7 col-md-offset-2">
               <div class="container text-left">
                  <h5>nhà cung cấp sản phẩm</h5>
                  <p class="okal">okal/mdf</p>
                  <p class="flywood">flywood</p>
                  <div class="line-decorate">
                     <span class="sub-line-1"></span>
                  </div>
                  <div class="clearfix"></div>
                  <p class="text">
                  <div style="text-align: justify;">
                     <div>
                        <div>
                           <div>
                              Với Hơn 15 đối t&aacute;c lớn tr&ecirc;n thế giới, V&agrave; hệ thống kho tr&ecirc;n 5000m2<br />
                              Wood &amp; Land cung cấp cho kh&aacute;ch h&agrave;ng hơn 500.000 m3 gỗ chất lượng mỗi năm.
                           </div>
                        </div>
                     </div>
                  </div>
                  <br />
                  </p>
               </div>
            </div>
         </div>
      </div>
      <!-- End Slide-->
      <div class="connect">
         <div class="container">
            <div class="row">
               <div class="col-md-4 my-fix-form">
                  <div class="comment">
                     <div class="head-box">
                        <h5>
                           b&#225;o gi&#225; nhanh
                        </h5>
                     </div>
                     <p>
                        Điền th&ocirc;ng tin theo mẫu, Ch&uacute;ng t&ocirc;i sẽ b&aacute;o gi&aacute; cho bạn chỉ trong 60 ph&uacute;t
                     </p>
                     <div class="line-decorate col-sm-7 line-top">
                        <span class="sub-line-1"></span>
                     </div>
                     <form method="post" action="">
                        <div class="form-group myform-group">
                           <input id="Name" class="Name input" type="text" placeholder="Họ và tên" />
                           <input id="Phone" class="Phone input" type="text" placeholder="Số điện thoại" />
                           <input id="Email" class="Email input" type="text" placeholder="Email" />
                           <select class="form-control type Listpro" id="Listpro" name="Listpro">
                              <option value="">--None---</option>
                              <option value="2">V&#193;N MDF (Medium Density Fibreboard)</option>
                              <option value="4">V&#225;n &#201;p Plywood</option>
                              <option value="6">V&#225;n Okal</option>
                              <option value="8">Gỗ Tr&#224;m Gh&#233;p</option>
                              <option value="10">Gia C&#244;ng Bề Mặt</option>
                           </select>
                           <input id="Numbers" class="Numbers input" type="text" placeholder="Số lượng (m3)" />
                           <div class="form-group block-textarea">
                              <textarea id="Requests" class="Requests txtinput" rows="9" placeholder="Yêu cầu khác"></textarea>
                           </div>
                           <div class="send-info form-group">
                              <button id="btnsendQuote" type="button" class="btnsendQuote btn btn-default btn-input">
                              gửi y&#234;u cầu b&#225;o gi&#225;
                              </button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-md-4 col-sm-4 phone">
                  <i class="fa fa-envelope fa-2x" style="color:#a88144;" aria-hidden="true"></i>
                  <h6 class="callnow">gửi email để nhận b&#225;o gi&#225;</h6>
                  <h4 style="color:#a88144;">lienphuonghl@yahoo.com</h4>
               </div>
               <div class="col-md-2 col-sm-4 phone">
                  <i class="fa fa-truck fa-2x" style="color:#a88144;" aria-hidden="true"></i>
                  <h6 class="callnow">bộ phận vận chuyển</h6>
                  <a href="tel:0274 6515129">
                     <h4 style="color:#a88144;">0274 6515129</h4>
                  </a>
               </div>
               <div class="col-md-2 col-sm-4 phone">
                  <i class="fa fa-phone-square fa-2x" style="color:#a88144;" aria-hidden="true"></i>
                  <h6 class="callnow">Bộ phận kinh doanh</h6>
                  <a href="tel:0932100411">
                     <h4 style="color:#a88144;">0932100411</h4>
                  </a>
               </div>
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
      <div class="container discover">
         <h2 style="color:#1e8c0a; font-size:18px;">KHÁM PHÁ CÔNG TY</h2>
         <h1 class="hoanglam">WOOD & LAND</h1>
         <div class="discover-content col-md-8">
            <div style="text-align: justify;">
               <span style="font-size:16px;"><span style="font-family:tahoma,geneva,sans-serif;">WOOD &amp; LAND cam kết cung cấp sản phẩm đạt ti&ecirc;u chuẩn Ch&acirc;u &Acirc;u, ti&ecirc;u chuẩn về hệ thống kho vận. Tầm nh&igrave;n của&nbsp;</span></span><span style="font-family: tahoma, geneva, sans-serif; font-size: 16px;">WOOD &amp; LAND</span><span style="font-size:16px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;l&agrave; TRỞ TH&Agrave;NH NH&Agrave; CUNG CẤP GỖ C&Ocirc;NG NGHIỆP CHẤT LƯỢNG QUỐC TẾ H&Agrave;NG ĐẦU TẠI VIỆT NAM. Ch&uacute;ng t&ocirc;i đạt được mục ti&ecirc;u th&ocirc;ng qua c&aacute;c quy tr&igrave;nh đảm bảo chất lượng, đổi mới kỹ thuật v&agrave; dịch vụ kh&aacute;ch h&agrave;ng xuất sắc.</span></span>
            </div>
         </div>
      </div>
      <div class="product">
         <div class="container">
            <div class="row">
               <div class="col-sm-3 masisa">
                  <h3>SẢN PHẨM</h3>
                  <h2>V&#193;N MDF (Medium Density Fibreboard)</h2>
               </div>
               <div class="col-sm-5 item-content">
                  <div style="text-align: justify;">
                     <span style="color: rgb(0, 0, 0);">C&ocirc;ng ty TNHH&nbsp;</span><span style="color:#000000;">wood &amp; Land là nh&agrave; ph&acirc;n phối c&aacute;c sản phẩm tấm xơ &eacute;p &quot;MDF&quot; nhập khẩu từ New Zealand, Th&aacute;i Lan, Malaysia, Indonesia v&agrave; l&agrave; nh&agrave; ph&acirc;n phối của c&aacute;c thương hiệu trong nước như DongWha, Quảng Trị, Ki&ecirc;n giang.....được sản xuất theo ti&ecirc;u chuẩn CARB, E0, E1, E2 để đ&aacute;p ứng nhu cầu trong nước cũng như thỏa m&atilde;n y&ecirc;u cầu gia c&ocirc;ng xuất khẩu v&agrave;o c&aacute;c thị trường Bắc Mỹ, Nhật Bản, Ch&acirc;u &Acirc;u.</span>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="send">
                     <a href="javascripts:;" class="scroll-to-frm">
                     y&#234;u cầu b&#225;o gi&#225;
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <div class="all-product">
         <div class="container">
            <div class="discover">
               <h2 style="color:#1e8c0a; font-size:18px;">sản phẩm đặc trưng của</h2>
               <h1 class="hoanglam">WOOD &amp; LAND</h1>
            </div>
            <div class="feature-product-outer">
               <div class="product-02">
                  <div class="row">
                     <div class="col-md-4 pull-right hidden-sm hidden-xs"></div>
                     <div class="col-md-4 pull-right">
                        <div class="item">
                           <div class="images">
                              <a href="/product/details/2?SeoUrl=van-mdf--medium-density-fibreboard-" class="under-img hidden-sm hidden-xs"></a>
                              <a href="/product/details/2?SeoUrl=van-mdf--medium-density-fibreboard-"><img class="img-hover" src="/Content/images/vanmdf/mdf.JPG" /></a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="text">
                           <a href="/product/details/2?SeoUrl=van-mdf--medium-density-fibreboard-">
                              <h5>V&#193;N MDF (Medium Density Fibreboard)</h5>
                           </a>
                           <p>
                           <div style="text-align: justify;">
                              <span style="color: rgb(0, 0, 0);">C&ocirc;ng ty TNHH&nbsp;</span><span style="color:#000000;">wood &amp; Land là nh&agrave; ph&acirc;n phối c&aacute;c sản phẩm tấm xơ &eacute;p &quot;MDF&quot; nhập khẩu từ New Zealand, Th&aacute;i Lan, Malaysia, Indonesia v&agrave; l&agrave; nh&agrave; ph&acirc;n phối của c&aacute;c thương hiệu trong nước như DongWha, Quảng Trị, Ki&ecirc;n giang.....được sản xuất theo ti&ecirc;u chuẩn CARB, E0, E1, E2 để đ&aacute;p ứng nhu cầu trong nước cũng như thỏa m&atilde;n y&ecirc;u cầu gia c&ocirc;ng xuất khẩu v&agrave;o c&aacute;c thị trường Bắc Mỹ, Nhật Bản, Ch&acirc;u &Acirc;u.</span>
                           </div>
                           </p>
                        </div>
                        <div class="contact">
                           <div class="col-md-4 details">
                              <a href="/product/details/2?SeoUrl=van-mdf--medium-density-fibreboard-">
                              chi tiết
                              </a>
                           </div>
                           <div class="col-md-7 report">
                              <a href="javascripts:;" class="scroll-to-frm-pro" idproduct="2">
                              y&#234;u cầu b&#225;o gi&#225;
                              </a>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="product-01">
                  <div class="row">
                     <div class="col-md-4 hidden-sm hidden-xs"></div>
                     <div class="col-md-4">
                        <div class="item">
                           <div class="images">
                              <a href="/product/details/4?SeoUrl=van-ep-plywood" class="under-img hidden-sm hidden-xs"></a>
                              <a href="/product/details/4?SeoUrl=van-ep-plywood"><img class="img-hover" src="/Content/images/Product1/block-0%20-%20Copy%20-%20Copy%20-%20Copy.png" /></a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="text">
                           <a href="/product/details/4?SeoUrl=van-ep-plywood">
                              <h5>V&#225;n &#201;p Plywood</h5>
                           </a>
                           <p>
                           <p style="text-align: justify;">
                              <span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">v&aacute;n &eacute;p Polywood chiụ nước (hay gỗ d&aacute;n) đ&acirc;y l&agrave; sự s&aacute;ng tạo trong ng&agrave;nh sản xuất gỗ, v&aacute;n &eacute;p n&agrave;y được l&agrave;m từ nhiều lớp gỗ mỏng được sắp xếp li&ecirc;n tục l&ecirc;n nhau theo hướng v&acirc;n gỗ của mỗi lớp, c&aacute;c lớp n&agrave;y được d&aacute;n với nhau bằng keo ( như phenol formaldehyde ) dưới sự t&aacute;c dụng của nhiệt v&agrave; lực &eacute;p của m&aacute;y &eacute;p.</span></span></span>
                           </p>
                           <div style="text-align: justify;">
                              <span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">T&ugrave;y v&agrave;o nhu cầu sử dụng m&agrave; ch&uacute;ng ta ph&acirc;n ra nhiều loại v&aacute;n &eacute;p kh&aacute;c nhau. V&aacute;n &eacute;p gỗ mềm được l&agrave;m từ gỗ th&ocirc;ng radiata v&agrave; bạch dương. V&aacute;n &eacute;p gỗ cứng được l&agrave;m từ lọai gỗ như meranti (c&ograve;n gọi l&agrave; gỗ d&aacute;i ngựa Philippine hay c&acirc;y lauan) hay gỗ bulo.</span></span></span>
                           </div>
                           </p>
                        </div>
                        <div class="contact">
                           <div class="col-md-4 details">
                              <a href="/product/details/4?SeoUrl=van-ep-plywood">
                              chi tiết
                              </a>
                           </div>
                           <div class="col-md-7 report">
                              <a href="javascripts:;" class="scroll-to-frm-pro" idproduct="4">
                              y&#234;u cầu b&#225;o gi&#225;
                              </a>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="product-02">
                  <div class="row">
                     <div class="col-md-4 pull-right hidden-sm hidden-xs"></div>
                     <div class="col-md-4 pull-right">
                        <div class="item">
                           <div class="images">
                              <a href="/product/details/6?SeoUrl=van-okal" class="under-img hidden-sm hidden-xs"></a>
                              <a href="/product/details/6?SeoUrl=van-okal"><img class="img-hover" src="/Content/images/Product3/van%20okal.jpg" /></a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="text">
                           <a href="/product/details/6?SeoUrl=van-okal">
                              <h5>V&#225;n Okal</h5>
                           </a>
                           <p>
                           <div style="text-align: justify;">
                              <span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">V&aacute;n Okal c&ograve;n gọi l&agrave; V&aacute;n dăm l&agrave; lọai v&aacute;n nh&acirc;n tạo c&oacute; ruột được l&agrave;m từ lớp dăm gỗ đặc trộn keo ( nguy&ecirc;n liệu l&agrave; gỗ bạch đ&agrave;n, keo, cao su...). v&aacute;n okal c&oacute; thể đ&oacute;ng đinh , bắt ốc đều tốt, đặc biệt gi&aacute; th&agrave;nh sản xuất rất rẻ n&ecirc;n được sử dụng rất nhiều trong đời sống. Bề mặt v&aacute;n phẳng được d&ugrave;ng d&aacute;n phủ bằng những loại vật liệu trang tr&iacute; kh&aacute;c nhau: melamine, veneer (gỗ lạng).<br />
                              <br />
                              Hiện tại tr&ecirc;n thị trường ng&agrave;nh gỗ c&oacute; 2 d&ograve;ng sản phẩm đối với Okal l&agrave; h&agrave;ng nhập khẩu v&agrave; h&agrave;ng Việt Nam.&nbsp;</span></span></span>
                           </div>
                           </p>
                        </div>
                        <div class="contact">
                           <div class="col-md-4 details">
                              <a href="/product/details/6?SeoUrl=van-okal">
                              chi tiết
                              </a>
                           </div>
                           <div class="col-md-7 report">
                              <a href="javascripts:;" class="scroll-to-frm-pro" idproduct="6">
                              y&#234;u cầu b&#225;o gi&#225;
                              </a>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="product-01">
                  <div class="row">
                     <div class="col-md-4 hidden-sm hidden-xs"></div>
                     <div class="col-md-4">
                        <div class="item">
                           <div class="images">
                              <a href="/product/details/8?SeoUrl=go-tram-ghep" class="under-img hidden-sm hidden-xs"></a>
                              <a href="/product/details/8?SeoUrl=go-tram-ghep"><img class="img-hover" src="/Content/images/Product4/go-tram-ghep-1.jpg" /></a>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="text">
                           <a href="/product/details/8?SeoUrl=go-tram-ghep">
                              <h5>Gỗ Tr&#224;m Gh&#233;p</h5>
                           </a>
                           <p>
                           <p style="text-align: justify;">
                              <span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">Gỗ tr&agrave;m gh&eacute;p l&agrave; một sản phẩm được tạo ra từ c&aacute;c thanh gỗ tr&agrave;m nhỏ để gh&eacute;p với nhau, vừa đẹp mắt vừa tiết kiệm được nguồn nguy&ecirc;n liệu gỗ v&agrave; đ&aacute;p ưng được c&aacute;c nhu cầu sử dụng sản phẩm của người nh&agrave; sản xuất.&nbsp;<br />
                              <br />
                              C&ocirc;ng ty&nbsp;</span></span><span style="color: rgb(0, 0, 0); font-family: tahoma, geneva, sans-serif; font-size: 14px;">WOOD &amp; LAND</span><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;chuy&ecirc;n ph&acirc;n phối c&aacute;c loại Tr&agrave;m gh&eacute;p ti&ecirc;u chuẩn xuất khẩu xuất xứ từ: Đồng Nai, B&igrave;nh Định, Hải Dương. Ch&iacute;nh v&igrave; vậy gỗ tr&agrave;m gh&eacute;p hiện tại tr&ecirc;n thị trường ng&agrave;nh gỗ rất được ưa chuộng.</span></span>
                           </p>
                           </p>
                        </div>
                        <div class="contact">
                           <div class="col-md-4 details">
                              <a href="/product/details/8?SeoUrl=go-tram-ghep">
                              chi tiết
                              </a>
                           </div>
                           <div class="col-md-7 report">
                              <a href="javascripts:;" class="scroll-to-frm-pro" idproduct="8">
                              y&#234;u cầu b&#225;o gi&#225;
                              </a>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4 hidden-xs"></div>
                  <div class="col-md-4">
                     <div class="details show">
                        <a href="/Product/Index">XEM TẤT CẢ</a>
                     </div>
                  </div>
                  <div class="col-md-4 hidden-xs"></div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
      <div class="auto-play">
         <div class="container">
            <div class="discover">
               <h2 style="color:#1e8c0a; font-size:18px;">
                  đội ngủ của
               </h2>
               <h1 class="hoanglam">
                  WOOD &amp; LAND
               </h1>
            </div>
            <div class="home-news-content">
               <div class="news-event-slider">
                  <div class="col-md-3">
                     <div class="item">
                        <a href="javascripts:;" class="under-img hidden-xs">
                           <h5>Minh Phương</h5>
                           <p>Trưởng Ph&#242;ng Kinh Doanh</p>
                        </a>
                        <a href="#"><img class="img-hover" src="/Content/images/nhanvien/nv-6.png" /></a>
                        <ul class="staff-socials">
                           <li>
                              <a href="https://www.facebook.com">
                              <i class="fa fa-facebook-square fa-2x" href="#" style="color:#507dcf;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://twitter.com">
                              <i class="fa fa-twitter-square fa-2x" href="#" style="color:#34c1ef;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://plus.google.com">
                              <i class="fa fa-google-plus-square fa-2x" href="#" style="color:#e2720e;" aria-hidden="true"></i>
                              </a>
                           </li>
                        </ul>
                        <div class="name-staff hidden-lg hidden-md hidden-sm">
                           <h5>Minh Phương</h5>
                           <p>Trưởng Ph&#242;ng Kinh Doanh</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="item">
                        <a href="javascripts:;" class="under-img hidden-xs">
                           <h5>Tr&#250;c Ly</h5>
                           <p>Nh&#226;n vi&#234;n kinh doanh</p>
                        </a>
                        <a href="#"><img class="img-hover" src="/Content/images/nhanvien/nv-04.png" /></a>
                        <ul class="staff-socials">
                           <li>
                              <a href="https://www.facebook.com">
                              <i class="fa fa-facebook-square fa-2x" href="#" style="color:#507dcf;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://twitter.com">
                              <i class="fa fa-twitter-square fa-2x" href="#" style="color:#34c1ef;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://plus.google.com">
                              <i class="fa fa-google-plus-square fa-2x" href="#" style="color:#e2720e;" aria-hidden="true"></i>
                              </a>
                           </li>
                        </ul>
                        <div class="name-staff hidden-lg hidden-md hidden-sm">
                           <h5>Tr&#250;c Ly</h5>
                           <p>Nh&#226;n vi&#234;n kinh doanh</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="item">
                        <a href="javascripts:;" class="under-img hidden-xs">
                           <h5>Huyền Ngọc</h5>
                           <p>Kế To&#225;n.</p>
                        </a>
                        <a href="#"><img class="img-hover" src="/Content/images/nhanvien/nv-2.png" /></a>
                        <ul class="staff-socials">
                           <li>
                              <a href="https://www.facebook.com">
                              <i class="fa fa-facebook-square fa-2x" href="#" style="color:#507dcf;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://twitter.com">
                              <i class="fa fa-twitter-square fa-2x" href="#" style="color:#34c1ef;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://plus.google.com">
                              <i class="fa fa-google-plus-square fa-2x" href="#" style="color:#e2720e;" aria-hidden="true"></i>
                              </a>
                           </li>
                        </ul>
                        <div class="name-staff hidden-lg hidden-md hidden-sm">
                           <h5>Huyền Ngọc</h5>
                           <p>Kế To&#225;n.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="item">
                        <a href="javascripts:;" class="under-img hidden-xs">
                           <h5>Ho&#224;ng Diễm</h5>
                           <p>Kế To&#225;n</p>
                        </a>
                        <a href="#"><img class="img-hover" src="/Content/images/nhanvien/nv-5.png" /></a>
                        <ul class="staff-socials">
                           <li>
                              <a href="https://www.facebook.com">
                              <i class="fa fa-facebook-square fa-2x" href="#" style="color:#507dcf;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://twitter.com">
                              <i class="fa fa-twitter-square fa-2x" href="#" style="color:#34c1ef;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://plus.google.com">
                              <i class="fa fa-google-plus-square fa-2x" href="#" style="color:#e2720e;" aria-hidden="true"></i>
                              </a>
                           </li>
                        </ul>
                        <div class="name-staff hidden-lg hidden-md hidden-sm">
                           <h5>Ho&#224;ng Diễm</h5>
                           <p>Kế To&#225;n</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="item">
                        <a href="javascripts:;" class="under-img hidden-xs">
                           <h5>Ngoc Diep</h5>
                           <p>Thủ Kho</p>
                        </a>
                        <a href="#"><img class="img-hover" src="/Content/images/nhanvien/nv-3.png" /></a>
                        <ul class="staff-socials">
                           <li>
                              <a href="https://www.facebook.com">
                              <i class="fa fa-facebook-square fa-2x" href="#" style="color:#507dcf;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://twitter.com">
                              <i class="fa fa-twitter-square fa-2x" href="#" style="color:#34c1ef;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://plus.google.com">
                              <i class="fa fa-google-plus-square fa-2x" href="#" style="color:#e2720e;" aria-hidden="true"></i>
                              </a>
                           </li>
                        </ul>
                        <div class="name-staff hidden-lg hidden-md hidden-sm">
                           <h5>Ngoc Diep</h5>
                           <p>Thủ Kho</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="item">
                        <a href="javascripts:;" class="under-img hidden-xs">
                           <h5>Quỳnh Như</h5>
                           <p>Thủ Kho</p>
                        </a>
                        <a href="#"><img class="img-hover" src="/Content/images/nhanvien/nv-1.png" /></a>
                        <ul class="staff-socials">
                           <li>
                              <a href="https://www.facebook.com ">
                              <i class="fa fa-facebook-square fa-2x" href="#" style="color:#507dcf;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://twitter.com">
                              <i class="fa fa-twitter-square fa-2x" href="#" style="color:#34c1ef;" aria-hidden="true"></i>
                              </a>
                           </li>
                           <li>
                              <a href="https://plus.google.com">
                              <i class="fa fa-google-plus-square fa-2x" href="#" style="color:#e2720e;" aria-hidden="true"></i>
                              </a>
                           </li>
                        </ul>
                        <div class="name-staff hidden-lg hidden-md hidden-sm">
                           <h5>Quỳnh Như</h5>
                           <p>Thủ Kho</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="text-medium">
         <div class="container">
            <div class="col-lg-8 col-md-8 col-md-offset-2">
               <p>
               <div style="text-align: justify;">
                  <span style="font-size:16px;"><span style="font-family:tahoma,geneva,sans-serif;">WOOD &amp; LAND cam kết cung cấp sản phẩm đạt ti&ecirc;u chuẩn Ch&acirc;u &Acirc;u, ti&ecirc;u chuẩn về hệ thống kho vận. Tầm nh&igrave;n của&nbsp;</span></span><span style="font-family: tahoma, geneva, sans-serif; font-size: 16px;">WOOD &amp; LAND</span><span style="font-size:16px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;l&agrave; TRỞ TH&Agrave;NH NH&Agrave; CUNG CẤP GỖ C&Ocirc;NG NGHIỆP CHẤT LƯỢNG QUỐC TẾ H&Agrave;NG ĐẦU TẠI VIỆT NAM. Ch&uacute;ng t&ocirc;i đạt được mục ti&ecirc;u th&ocirc;ng qua c&aacute;c quy tr&igrave;nh đảm bảo chất lượng, đổi mới kỹ thuật v&agrave; dịch vụ kh&aacute;ch h&agrave;ng xuất sắc.</span></span>
               </div>
               </p>
            </div>
         </div>
      </div>
      <div class="customer">
         <div class="container">
            <div class="discover">
               <h2 style="color:#1e8c0a; font-size:18px;">
                  kh&#225;ch h&#224;ng n&#243;i về
               </h2>
               <h1 class="hoanglam">
                  WOOD &amp; LAND
               </h1>
            </div>
            <div class="cus-content">
               <div class="col-md-3">
                  <div class="images">
                     <img src="/Content/images/09.png" />
                  </div>
                  <div class="text">
                     <div class="cus-content-desc">
                        <div style="text-align: justify;">
                           Chỉ cần d&acirc;y chuyền sản xuất thiếu nguy&ecirc;n liệu hoạt động trong 1 giờ đ&oacute; l&agrave; một tổn thất kh&ocirc;ng nhỏ cho doanh nghiệp, Ch&uacute;ng t&ocirc;i chọn Wood &amp; Land để gi&uacute;p cho d&acirc;y chuyền lu&ocirc;n hoạt động ổn định.
                        </div>
                     </div>
                     <h5> G&#225;m đốc Hi Furniture</h5>
                     <h5>Hồng Dung</h5>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="images">
                     <img src="/Content/images/KH%20phung%20tan%20cong.png" />
                  </div>
                  <div class="text">
                     <div class="cus-content-desc">
                        <div style="text-align: justify;">
                           Gỗ d&ugrave;ng trong kết cấu c&ocirc;ng tr&igrave;nh x&acirc;y dựng đang bắt đầu được c&aacute;c chủ đầu tư ch&uacute; &yacute;, v&agrave; Wood &amp; Land l&agrave; một trong những c&ocirc;ng ty đi đầu về cung cấp những sản phẩm cho những c&ocirc;ng tr&igrave;nh như vậy .
                        </div>
                     </div>
                     <h5>GĐ Cty X&#226;y Dựng Futaco</h5>
                     <h5>Ph&#249;ng Tấn C&#244;ng</h5>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="images">
                     <img src="/Content/images/Kh-trinh.png" />
                  </div>
                  <div class="text">
                     <div class="cus-content-desc">
                        <div style="text-align: justify;">
                           <span style="text-align: justify;">Những đối t&aacute;c nước ngo&agrave;i lu&ocirc;n lu&ocirc;n y&ecirc;u cầu cao về mặt chất lượng sản phẩm, do đ&oacute; c&ocirc;ng ty ch&uacute;ng t&ocirc;i chọn&nbsp;</span>Wood &amp; Land<span style="text-align: justify;">&nbsp;l&agrave;m đơn vị cung cấp sản phẩm &nbsp;để đảm bảo uy t&iacute;n cho doanh nghiệp của m&igrave;nh.</span>
                        </div>
                     </div>
                     <h5>GĐ Cty Tatoco SJC</h5>
                     <h5>Đức Tr&#236;nh</h5>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="images">
                     <img src="/Content/images/Kh%20Thao.png" />
                  </div>
                  <div class="text">
                     <div class="cus-content-desc">
                        <div style="text-align: justify;">
                           Đ&ocirc;i khi ch&uacute;ng t&ocirc;i c&oacute; những đơn h&agrave;ng thiết kế cần phải xử l&yacute; gấp, Wood &amp; Land đ&atilde; gi&uacute;p cho những kh&oacute; khăn như vậy trở n&ecirc;n đơn giản hơn, do đ&oacute; ch&uacute;ng t&ocirc;i chọn Wood &amp; Land l&agrave; đối t&aacute;c chiến lược của c&ocirc;ng ty.
                        </div>
                     </div>
                     <h5>GĐ IFIX DESIGN</h5>
                     <h5>Ninh B&#237;ch Thảo</h5>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="pr2">
         <div class="container">
            <div class="row">
               <div class="col-sm-3 masisa">
                  <h3>SẢN PHẨM</h3>
                  <h2>V&#193;N MDF (Medium Density Fibreboard)</h2>
               </div>
               <div class="col-sm-5 item-content">
                  <div style="text-align: justify;">
                     <span style="color: rgb(0, 0, 0);">C&ocirc;ng ty TNHH&nbsp;</span><span style="color:#000000;">wood &amp; Land là nh&agrave; ph&acirc;n phối c&aacute;c sản phẩm tấm xơ &eacute;p &quot;MDF&quot; nhập khẩu từ New Zealand, Th&aacute;i Lan, Malaysia, Indonesia v&agrave; l&agrave; nh&agrave; ph&acirc;n phối của c&aacute;c thương hiệu trong nước như DongWha, Quảng Trị, Ki&ecirc;n giang.....được sản xuất theo ti&ecirc;u chuẩn CARB, E0, E1, E2 để đ&aacute;p ứng nhu cầu trong nước cũng như thỏa m&atilde;n y&ecirc;u cầu gia c&ocirc;ng xuất khẩu v&agrave;o c&aacute;c thị trường Bắc Mỹ, Nhật Bản, Ch&acirc;u &Acirc;u.</span>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="send">
                     <a href="javascripts:;" class="scroll-to-frm">
                     y&#234;u cầu b&#225;o gi&#225;
                     </a>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <div class="topic-share">
         <div class="container">
            <div class="discover">
               <h2 style="color:#1e8c0a; font-size:18px;">
                  b&#224;i viết chia sẻ từ
               </h2>
               <h1 class="hoanglam">
                  WOOD &amp; LAND
               </h1>
            </div>
            <div class="topic-content">
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/12?SeoUrl=nhap-van-mdf-carb-p2-dongwha-18mmx1830x2440-tai-wood---land" class="under-img hidden-xs"></a>
                        <a href="/article/details/12?SeoUrl=nhap-van-mdf-carb-p2-dongwha-18mmx1830x2440-tai-wood---land"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tuc%2041.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/12?SeoUrl=nhap-van-mdf-carb-p2-dongwha-18mmx1830x2440-tai-wood---land">
                           <h5>
                              Nhập V&#225;n MDF Carb P2 Dongwha 18mmx1830x2440 tại Wood &amp; Land
                           </h5>
                        </a>
                        <p>
                        <div style="text-align: justify;">
                           <span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;"><span style="color: rgb(0, 0, 0);">C&ocirc;ng ty&nbsp;</span></span></span><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium;">Wood &amp; Land</span><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;"><span style="color: rgb(0, 0, 0);">&nbsp; lu&ocirc;n chuẩn bị đầy đủ c&aacute;c chủng loại h&agrave;ng h&oacute;a để cung cấp cho Kh&aacute;ch h&agrave;ng, V&aacute;n MDF Dongwha E2 v&agrave; CARB P2 lu&ocirc;n lu&ocirc;n tồn kho để đ&aacute;p ứng nhanh cho c&aacute;c đơn h&agrave;ng sản xuất của Kh&aacute;ch.</span></span></span>
                        </div>
                        <br />
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/14?SeoUrl=tap-the-cty-wood---land-giao-luu-lien-hoan-tat-nien-tai-cong-ty-khach-hang-" class="under-img hidden-xs"></a>
                        <a href="/article/details/14?SeoUrl=tap-the-cty-wood---land-giao-luu-lien-hoan-tat-nien-tai-cong-ty-khach-hang-"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tuc%2054.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/14?SeoUrl=tap-the-cty-wood---land-giao-luu-lien-hoan-tat-nien-tai-cong-ty-khach-hang-">
                           <h5>
                              Tập thể Cty Wood &amp; Land giao lưu li&#234;n hoan tất ni&#234;n tại c&#244;ng ty Kh&#225;ch h&#224;ng 
                           </h5>
                        </a>
                        <p>
                           <span style="color: rgb(0, 0, 0); font-family: tahoma, geneva, sans-serif; font-size: 14px;">Đ&acirc;y l&agrave; buổi giao lưu li&ecirc;n hoan cuối năm giữa c&ocirc;ng ty&nbsp;</span><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium; text-align: justify;">Wood &amp; Land</span><span style="color: rgb(0, 0, 0); font-family: tahoma, geneva, sans-serif; font-size: 14px;">&nbsp;với kh&aacute;ch h&agrave;ng th&acirc;n thiết của m&igrave;nh.</span>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/17?SeoUrl=huong-dan-sinh-vien-dh-qte-mien-dong-lam-luan-van-tot-nghiep-tai-cty-wood---land-" class="under-img hidden-xs"></a>
                        <a href="/article/details/17?SeoUrl=huong-dan-sinh-vien-dh-qte-mien-dong-lam-luan-van-tot-nghiep-tai-cty-wood---land-"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tuc%20104.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/17?SeoUrl=huong-dan-sinh-vien-dh-qte-mien-dong-lam-luan-van-tot-nghiep-tai-cty-wood---land-">
                           <h5>
                              Hướng dẫn sinh vi&#234;n ĐH QTẾ MIỀN Đ&#212;NG l&#224;m luận văn tốt nghiệp tại Cty Wood &amp; Land 
                           </h5>
                        </a>
                        <p>
                           <span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">Đ&acirc;y l&agrave; h&igrave;nh ảnh về buổi chia sẻ của Chị Li&ecirc;n Phương - Gi&aacute;m đốc c&ocirc;ng ty&nbsp;</span></span></span><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium; text-align: justify;">Wood &amp; Land</span><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;c&ugrave;ng c&aacute;c bạn Sinh Vi&ecirc;n Đại Học Quốc Tế Miền Đ&ocirc;ng chuy&ecirc;n ng&agrave;nh kinh tế.</span></span></span><br style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium;" />
                           <br />
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/19?SeoUrl=cty-wood---land-chuan-bi-giao-van-mdf--tram-ghep-cho-khach-hang-" class="under-img hidden-xs"></a>
                        <a href="/article/details/19?SeoUrl=cty-wood---land-chuan-bi-giao-van-mdf--tram-ghep-cho-khach-hang-"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tuc%20121.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/19?SeoUrl=cty-wood---land-chuan-bi-giao-van-mdf--tram-ghep-cho-khach-hang-">
                           <h5>
                              Cty Wood &amp; Land chuẩn bị giao V&#225;n MDF, Tr&#224;m gh&#233;p cho kh&#225;ch h&#224;ng.
                           </h5>
                        </a>
                        <p>
                           <span style="color:#000000;"><span style="font-family:tahoma,geneva,sans-serif;"><span style="font-size: 14px; text-align: center;">C&aacute;c xe h&agrave;ng MDF, V&aacute;n &eacute;p, Tr&agrave;m gh&eacute;p h&agrave;ng ng&agrave;y được chuẩn bị sẵn s&agrave;ng để đưa đến c&aacute;c kh&aacute;ch h&agrave;ng th&acirc;n y&ecirc;u.&nbsp;</span></span></span><br />
                        <div class="separator" style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium; clear: both; text-align: center;">
                           &nbsp;
                        </div>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/20?SeoUrl=tim-hieu-ve-mdf-chong-am-nhe---" class="under-img hidden-xs"></a>
                        <a href="/article/details/20?SeoUrl=tim-hieu-ve-mdf-chong-am-nhe---"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tiuc%20153.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/20?SeoUrl=tim-hieu-ve-mdf-chong-am-nhe---">
                           <h5>
                              T&#204;M HIỂU VỀ MDF CHỐNG ẨM NH&#201;!!!
                           </h5>
                        </a>
                        <p>
                        <div style="text-align: justify;">
                           <span style="font-family:tahoma,geneva,sans-serif;"><span style="color: rgb(29, 33, 41); font-size: 14px;">C&Ocirc;NG TY&nbsp;</span></span><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium;">Wood &amp; Land</span><span style="font-family:tahoma,geneva,sans-serif;"><span style="color: rgb(29, 33, 41); font-size: 14px;">&nbsp;&nbsp;xin giới thiệu c&ugrave;ng c&aacute;c bạn một trong những sản phẩm m&igrave;nh đang kinh doanh &nbsp;! Đ&oacute; l&agrave; gỗ c&ocirc;ng nghi&ecirc;̣p - v&aacute;n mdf chống ẩm, V&aacute;n MDF chống ẩm được sản xuất tr&ecirc;n d&acirc;y chuyền c&ocirc;ng nghệ hiện đại của ch&acirc;u &Acirc;u , V&aacute;n xay từ bột gỗ 100% gỗ tự nhi&ecirc;n</span></span>
                        </div>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/24?SeoUrl=van-ep-trung-quoc-mat-bach-duong-phang--khong-gon-song--lo-cot-" class="under-img hidden-xs"></a>
                        <a href="/article/details/24?SeoUrl=van-ep-trung-quoc-mat-bach-duong-phang--khong-gon-song--lo-cot-"><img class="img-hover" src="/Content/images/19553108_1390032887711863_1266089098_n.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/24?SeoUrl=van-ep-trung-quoc-mat-bach-duong-phang--khong-gon-song--lo-cot-">
                           <h5>
                              V&#225;n &#233;p Trung quốc mặt bạch dương Phẳng, kh&#244;ng gợn s&#243;ng, lộ cốt.
                           </h5>
                        </a>
                        <p>
                           V&aacute;n &eacute;p Trung quốc, 1 mặt bạch dương, 1 mặt nh&acirc;n tạo. Độ d&agrave;y 3mm. k&iacute;ch thướt 1220x2440. Dung sai +-0.3mm. bề mặt phẳng đẹp, kh&ocirc;ng lộ cốt, veneer kh&ocirc;ng chồng m&iacute;. Đ&aacute;p ứng nhu cầu d&aacute;n Veneer trang tr&iacute; mặt.
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/25?SeoUrl=vieng-tham-khach-hang-go-o-dong-nai-" class="under-img hidden-xs"></a>
                        <a href="/article/details/25?SeoUrl=vieng-tham-khach-hang-go-o-dong-nai-"><img class="img-hover" src="/Content/images/21751985_676132619245986_2217625180637807859_n.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/25?SeoUrl=vieng-tham-khach-hang-go-o-dong-nai-">
                           <h5>
                              Viếng thăm kh&#225;ch h&#224;ng Gỗ ở Đồng Nai.
                           </h5>
                        </a>
                        <p>
                           C&ocirc;ng ty&nbsp;<span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium; text-align: justify;">Wood &amp; Land</span>&nbsp;thường xuy&ecirc;n đến thăm hỏi kh&aacute;ch h&agrave;ng , c&ugrave;ng kh&aacute;ch h&agrave;ng giải quyết c&aacute;c vấn đề kh&oacute; khăn v&agrave; đồng thời tạo mối quan hệ giao thương ng&agrave;y c&agrave;ng bền chặt.<br />
                           <br />
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/26?SeoUrl=gap-go-doi-tac-tai-trien-lam-may-che-bien-go-" class="under-img hidden-xs"></a>
                        <a href="/article/details/26?SeoUrl=gap-go-doi-tac-tai-trien-lam-may-che-bien-go-"><img class="img-hover" src="/Content/images/21151443_666421906883724_8257716802058585295_n.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/26?SeoUrl=gap-go-doi-tac-tai-trien-lam-may-che-bien-go-">
                           <h5>
                              GẶP GỠ ĐỐI T&#193;C TẠI TRIỂN L&#195;M M&#193;Y CHẾ BIẾN GỖ.
                           </h5>
                        </a>
                        <p>
                           Tham dự c&aacute;c hội chợ triển l&atilde;m Gỗ, Mấy chế biến gỗ để mở rộng kiến thức về ng&agrave;nh gỗ v&agrave; t&igrave;m kiếm th&ecirc;m nguồn kh&aacute;ch h&agrave;ng mới từ c&aacute;c sự kiện li&ecirc;n quan đến ng&agrave;nh gỗ tự nhi&ecirc;n v&agrave; gỗ c&ocirc;ng nghiệp tại thị trường Việt Nam lu&ocirc;n l&agrave; chiến lược của Gi&aacute;m đốc C&ocirc;ng ty&nbsp;<span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium; text-align: justify;">Wood &amp; Land</span>&nbsp;v&agrave; tập thể c&aacute;c nh&acirc;n vi&ecirc;n Ph&ograve;ng kinh doanh.
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/27?SeoUrl=van-ep-viet-nam-ung-dung-trong-nganh-furniture-" class="under-img hidden-xs"></a>
                        <a href="/article/details/27?SeoUrl=van-ep-viet-nam-ung-dung-trong-nganh-furniture-"><img class="img-hover" src="/Content/images/086200b2189ef7c0ae8f.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/27?SeoUrl=van-ep-viet-nam-ung-dung-trong-nganh-furniture-">
                           <h5>
                              V&#225;n &#233;p Việt Nam ứng dụng trong ng&#224;nh Furniture.
                           </h5>
                        </a>
                        <p>
                           <img alt="" src="/Content/images/086200b2189ef7c0ae8f(1).jpg" style="width: 1220px; height: 915px;" />V&aacute;n &eacute;p Việt nam được sản xuất theo d&acirc;y chuyền c&ocirc;ng nghệ hiện đại, với một số t&iacute;nh năng như sau:&nbsp;<br />
                           <br />
                           1. V&aacute;n sản xuất bằng keo Ure<br />
                           <br />
                           2. Cốt tạp rừng, bạch đ&agrave;n.<br />
                           <br />
                           3.Mặt Th&ocirc;ng, Bintanggo, Okume, Poplar....<br />
                           <br />
                           4. Mặt được ch&agrave; nh&aacute;m phẳng, kh&ocirc;ng gợn s&oacute;ng, cắt kh&ocirc;ng t&aacute;ch lớp.<br />
                           <br />
                           C&ocirc;ng dụng :&nbsp;&nbsp;Sản phẩm d&ugrave;ng trong ng&agrave;nh furniture, d&ugrave;ng cho hộc tủ, mặt hộc k&eacute;o, d&aacute;n PVC bọc nệm.<br />
                           <br />
                           <br />
                           <img alt="" src="/Content/images/dd9ca957b17b5e25076a(1).jpg" style="width: 1220px; height: 915px;" /><img alt="" src="/Content/images/7ab1855a9d7672282b67(1).jpg" style="width: 1220px; height: 915px;" /><br />
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/2?SeoUrl=tim-kiem-nguon-go-ghep-tram-" class="under-img hidden-xs"></a>
                        <a href="/article/details/2?SeoUrl=tim-kiem-nguon-go-ghep-tram-"><img class="img-hover" src="/Content/images/ncc.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/2?SeoUrl=tim-kiem-nguon-go-ghep-tram-">
                           <h5>
                              T&#236;m kiếm nguồn Gỗ gh&#233;p Tr&#224;m.
                           </h5>
                        </a>
                        <p>
                        <div class="_5pbx userContent" data-ft="{&quot;tn&quot;:&quot;K&quot;}" id="js_ew" style="color: rgb(29, 33, 41); font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.38;">
                           <div style="display: inline; font-family: inherit; text-align: justify;">
                              <span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium;">Để c&oacute; được nguồn h&agrave;ng ổn định về mọi mặt như chất lượng, số lượng, v&agrave; gi&aacute; cả l&agrave; điều trăn trở tất cả c&aacute;c nh&agrave; m&aacute;y tinh chế l&agrave;m Furniture xuất khẩu. Ch&iacute;nh v&igrave; điều n&agrave;y m&agrave; c&ocirc;ng ty Wood &amp; Land&nbsp;</span>
                           </div>
                           <span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium; text-align: justify;">lu&ocirc;n lu&ocirc;n t&igrave;m kiếm v&agrave; thiết lập mối quan hệ bền vững &nbsp;với c&aacute;c nh&agrave; cung cấp</span>
                        </div>
                        <br />
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/4?SeoUrl=tieu-chuan-carb-p2-danh-cho-go-cong-nghiep-" class="under-img hidden-xs"></a>
                        <a href="/article/details/4?SeoUrl=tieu-chuan-carb-p2-danh-cho-go-cong-nghiep-"><img class="img-hover" src="/Content/images/tin%20tuc/carb%201.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/4?SeoUrl=tieu-chuan-carb-p2-danh-cho-go-cong-nghiep-">
                           <h5>
                              Ti&#234;u chuẩn CARB P2 d&#224;nh cho Gỗ c&#244;ng nghiệp.
                           </h5>
                        </a>
                        <p>
                        <div style="text-align: justify;">
                           <span style="color:#000000;"><span style="font-family:tahoma,geneva,sans-serif;"><span style="font-size: 14px; line-height: 19.32px;">Nếu n&oacute;i về sản phẩm gỗ nh&acirc;n tạo th&igrave; hiện tại tr&ecirc;n thị trường rất đa dạng về chủng loại , chất lượng v&agrave; thương hiệu , h&ocirc;m nay m&igrave;nh chia sẽ với c&aacute;c bạn về một sản phẩm gỗ nh&acirc;n tạo l&agrave; &nbsp;MDF ti&ecirc;u chuẩn Carb P2</span></span></span>
                        </div>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/6?SeoUrl=cty-wood---land-dong-hanh-cung-khach-hang-tim-giai-phap-" class="under-img hidden-xs"></a>
                        <a href="/article/details/6?SeoUrl=cty-wood---land-dong-hanh-cung-khach-hang-tim-giai-phap-"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tuc%2011.JPG" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/6?SeoUrl=cty-wood---land-dong-hanh-cung-khach-hang-tim-giai-phap-">
                           <h5>
                              Cty Wood &amp; Land đồng h&#224;nh c&#249;ng kh&#225;ch h&#224;ng t&#236;m giải ph&#225;p!
                           </h5>
                        </a>
                        <p>
                        <div style="text-align: justify;">
                           <span style="font-family:tahoma,geneva,sans-serif;"><span style="color:#000000;"><span style="font-size:14px;">&nbsp;Gi&aacute;m đốc C&ocirc;ng ty&nbsp;</span></span></span><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium;">Wood &amp; Land</span><span style="font-family:tahoma,geneva,sans-serif;"><span style="color:#000000;"><span style="font-size:14px;">&nbsp;đến thăm nh&agrave; m&aacute;y sản xuất của Kh&aacute;ch h&agrave;ng.Chị Li&ecirc;n Phương lu&ocirc;n d&agrave;nh thời gian đến thăm tất cả c&aacute;c kh&aacute;ch h&agrave;ng của m&igrave;nh 1 đến 2 lần trong năm.</span></span></span><br />
                           &nbsp;
                        </div>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/8?SeoUrl=san-pham-kinh-doanh-tai-cong-ty-wood---land-" class="under-img hidden-xs"></a>
                        <a href="/article/details/8?SeoUrl=san-pham-kinh-doanh-tai-cong-ty-wood---land-"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tuc%2025.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/8?SeoUrl=san-pham-kinh-doanh-tai-cong-ty-wood---land-">
                           <h5>
                              Sản phẩm kinh doanh tại c&#244;ng ty Wood &amp; Land.
                           </h5>
                        </a>
                        <p>
                        <div style="text-align: justify;">
                           <span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">C&aacute;c bạn Th&acirc;n mến! H&ocirc;m nay Li&ecirc;n Phương xin giới thiệu đ&eacute;n c&aacute;c bạn những sản phẩm m&agrave; c&ocirc;ng ty&nbsp;</span></span></span><span style="color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;; font-size: medium;">Wood &amp; Land</span><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;của Li&ecirc;n Phương kinh doanh n&egrave;. Dưới đ&acirc;y l&agrave; những sản phẩm gỗ được d&ugrave;ng l&agrave;m đồ trang tr&iacute; nội thất, c&aacute;c bạn c&oacute; muốn t&igrave;m hiểu th&ecirc;m về sản phẩm th&igrave; li&ecirc;n hệ với Li&ecirc;n Phương nhe!&nbsp;</span></span></span>
                        </div>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="item">
                     <div class="images">
                        <a href="/article/details/10?SeoUrl=go-tram-ghep-gia-re-tai-wood---land" class="under-img hidden-xs"></a>
                        <a href="/article/details/10?SeoUrl=go-tram-ghep-gia-re-tai-wood---land"><img class="img-hover" src="/Content/images/tin%20tuc/tin%20tuc%2033.jpg" /></a>
                     </div>
                     <div class="text">
                        <a href="/article/details/10?SeoUrl=go-tram-ghep-gia-re-tai-wood---land">
                           <h5>
                              Gỗ tr&#224;m gh&#233;p gi&#225; rẻ tại Wood &amp; Land
                           </h5>
                        </a>
                        <p>
                        <div style="text-align: justify;">
                           <span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;Tr&agrave;m gh&eacute;p l&agrave; một sản phẩm được tạo từ thanh gỗ nhỏ tận dụng để gh&eacute;p lại. Tấm tr&agrave;m gh&eacute;p n&oacute; vừa đẹp mắt vừa tiết kiệm được nguồn nguy&ecirc;n liệu gỗ, nhưng quan trọng l&agrave; n&oacute; đ&aacute;p ứng được nhu cầu sử dụng của nh&agrave; sản xuất.</span></span>
                        </div>
                        <br />
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="choose" id="frm-quote-bottom">
         <div class="container">
            <div class="col-md-4">
               <div class="comment">
                  <div class="head-box">
                     <h5>
                        b&#225;o gi&#225; nhanh
                     </h5>
                  </div>
                  <p>
                     Điền th&ocirc;ng tin theo mẫu, Ch&uacute;ng t&ocirc;i sẽ b&aacute;o gi&aacute; cho bạn chỉ trong 60 ph&uacute;t
                  </p>
                  <div class="line-decorate col-sm-7 line-top">
                     <span class="sub-line-1"></span>
                  </div>
                  <form class="myform-fix" method="post" action="">
                     <div class="form-group myform-group">
                        <input id="Name1" class="input" type="text" placeholder="Họ và tên" />
                        <input id="Phone1" class="input" type="text" placeholder="Số điện thoại" />
                        <input id="Email1" class="input" type="text" placeholder="Email" />
                        <select class="form-control type" id="Listpro1" name="Listpro1">
                           <option value="">--None---</option>
                           <option value="2">V&#193;N MDF (Medium Density Fibreboard)</option>
                           <option value="4">V&#225;n &#201;p Plywood</option>
                           <option value="6">V&#225;n Okal</option>
                           <option value="8">Gỗ Tr&#224;m Gh&#233;p</option>
                           <option value="10">Gia C&#244;ng Bề Mặt</option>
                        </select>
                        <input id="Numbers1" class="input" type="text" placeholder="Số lượng (m3)" />
                        <div class="form-group block-textarea">
                           <textarea id="Requests1" class="txtinput" rows="9" placeholder="Yêu cầu khác"></textarea>
                        </div>
                        <div class="send-info form-group">
                           <button id="btnsendQuote1" type="button" class="btn btn-default btn-input">
                           gửi y&#234;u cầu b&#225;o gi&#225;
                           </button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="col-md-6 col-md-offset-1 choose-content">
               <a>
                  <h5>Tại sao bạn n&#234;n chọn ch&#250;ng t&#244;i?</h5>
               </a>
               <div>
                  <div style="text-align: justify;">
                     Wood &amp; Land<span style="color:#ffffff;"><span style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;tăng trưởng ổn định trong 15 năm qua l&agrave; do thực tế ch&uacute;ng t&ocirc;i cung cấp sản phẩm cao cấp v&agrave; dịch vụ tuyệt vời cho kh&aacute;ch h&agrave;ng của ch&uacute;ng t&ocirc;i. Bằng c&aacute;ch tập trung v&agrave;o c&aacute;c y&ecirc;u cầu của kh&aacute;ch h&agrave;ng v&agrave; lắng nghe nhu cầu của họ, ch&uacute;ng t&ocirc;i hy vọng sẽ tiếp tục tăng trưởng n&agrave;y trong những năm tới. &nbsp;Với nhiều năm kinh nghiệm, Ch&uacute;ng t&ocirc;i c&oacute; khả năng cung ứng một khối lượng lớn sản phẩm mỗi năm. Nguồn cung cấp &nbsp;h&agrave;ng năm của ch&uacute;ng t&ocirc;i cho MDF l&agrave; 200, 000M3; Verneer l&agrave; 150, 000M3, v&agrave; V&aacute;n &eacute;p l&agrave; 100, 000M3.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify; outline: none !important;" />
                     <br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify; outline: none !important;" />
                     <span style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">V&igrave; Vậy&nbsp;</span></span>Wood &amp; Land<span style="color:#ffffff;"><span style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;&nbsp;lu&ocirc;n c&oacute; lượng sảng phẩm tồn kho lớn cho ph&eacute;p ch&uacute;ng t&ocirc;i để nhanh ch&oacute;ng cung cấp c&aacute;c sản phẩm của ch&uacute;ng t&ocirc;i với kh&aacute;ch h&agrave;ng tr&ecirc;n to&agrave;n Việt Nam. Ngo&agrave;i ra, ch&uacute;ng t&ocirc;i c&oacute; thể gia c&ocirc;ng bất kỳ sản phẩm theo th&ocirc;ng số kỹ thuật k&iacute;ch thước cụ thể của kh&aacute;ch h&agrave;ng. C&aacute;c sản phẩm bị lỗi c&oacute; thể được trả lại v&agrave; thay thế theo y&ecirc;u cầu. Với c&aacute;c sản phẩm chất lượng cao v&agrave; dịch vụ kh&aacute;ch h&agrave;ng chu đ&aacute;o, kh&aacute;ch h&agrave;ng của ch&uacute;ng t&ocirc;i c&oacute; thể cảm thấy an to&agrave;n trong sản phẩm đồ gỗ của họ dựa tr&ecirc;n thương hiệu&nbsp;</span></span>Wood &amp; Land<span style="color:#ffffff;"><span style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px; text-align: justify;">.&nbsp;</span></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="loadding">
         <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
      </div>
      <footer>
         <div class="container info">
            <div class="row">
               <div class="col-lg-3 col-md-6 icon">
                  <i class="fa fa-phone-square fa-2x" aria-hidden="true"></i>
                  <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">HOTLINE</strong>
                  <a href="tel:0908759007">
                  <strong style="font-weight:700;color: #565656;font-size: 20px;">0908759007</strong>
                  </a>
               </div>
               <div class="col-lg-3 col-md-6 icon">
                  <i class="fa fa-briefcase fa-2x" aria-hidden="true"></i>
                  <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kinh doanh</strong>
                  <a href="tel:0932100411">
                  <strong style="font-weight:700;color: #565656;font-size: 20px;">0932100411</strong>
                  </a>
                  <strong class="mail"> minhphuongwl@gmail.com</strong>
               </div>
               <div class="col-lg-3 col-md-6 icon">
                  <i class="fa fa-credit-card-alt fa-2x" aria-hidden="true"></i>
                  <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kế toán</strong>
                  <a href="tel:0987477937">
                  <strong style="font-weight:700;color: #565656;font-size: 20px;">0987477937</strong>
                  </a>
                  <strong class="mail">huyenngoc2001@yahoo.com </strong>
               </div>
               <div class="col-lg-3 col-md-6 icon">
                  <i class="fa fa-truck fa-2x" aria-hidden="true"></i>
                  <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">giao nhận</strong>
                  <a href="tel:0274 6515129">
                  <strong style="font-weight:700;color: #565656;font-size: 20px;">0274 6515129</strong>
                  </a>
                  <strong class="mail">daoquynhnhu98@gmail.com </strong>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <div class="form-email">
            <div class="container">
               <div class="row">
                  <div class="col-sm-5 col-xs-4 email">
                     <h4 style="color:white;">
                        nhận tin từ ch&#250;ng t&#244;i
                     </h4>
                  </div>
                  <div class="col-sm-5 col-xs-7 newsletter-outer">
                     <form class="navbar-form navbar-right input-email">
                        <div class="input-group">
                           <input type="text" class="txt-mail" id="txtEmail" placeholder="Nhập địa chỉ email" />
                           <span class="input-group-btn">
                           <button id="msg" class="btn btn-secondary" type="button">
                           <i class="fa fa-caret-right" style="color:#fff;font-size:18px;"></i>
                           </button>
                           </span>
                        </div>
                     </form>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
         <div class="footer">
            <div class="container">
               <div class="row">
                  <div class="col-md-4 contact">
                     <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                     <img src="/Content/images/icon/dia-diem.png" /> 
                     th&#244;ng tin li&#234;n hệ
                     </strong>
                     <p class="p-margin">
                        121/62 Phạm Ngọc Thạch, Khu 5, Phường Hi&#234;̣p Thành, Tp Thủ D&#226;̀u M&#244;̣t, Bình Dương
                     </p>
                     <p>
                        Điện thoại: 0274 6515129
                     </p>
                     <p>
                        Fax: 0274 3515129
                     </p>
                  </div>
                  <div class="col-md-4 contact">
                     <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                     <img src="/Content/images/icon/hop-thu.png" /> 
                     chi tiết li&#234;n hệ
                     </strong>
                     <p class="p-margin">Email:<br />lienphuonghl@yahoo.com</p>
                     <div class="col-md-4 contact-us">
                        <a href="/lien-he">li&#234;n hệ</a>
                     </div>
                  </div>
                  <div class="col-md-4 contact" style="margin-top:20px;">
                     <a href="tel:0908759007">
                     <strong style="color:#1e8c0a; font-size:30px; font-weight:700;">
                     0908759007
                     </strong>
                     </a>
                     <p class="p-margin">Giờ l&#224;m việc 7:30am - 4:30pm</p>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </footer>
      <a id="back-top"><i class="fa fa-play"></i></a>
      <script src="http://vn3c.net/html/vietmax/js/jquery-1.10.2.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/register-email-news.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/respond.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap-dialog.js"></script>
      <script type="text/javascript">
         $(document).ready(function () {
             //change language
             $("#LangId").change(function () {
                 var langId = this.value;
                 $.ajax({
                     type: 'POST',
                     url: "/CommonPartial/ChangeLanguage",
                     data: { langId: langId },
                     success: function (response) {
                         window.location.assign('/');
                     }
                 });
             });
             //fix menu
             $(window).scroll(function () {
                 if ($(this).scrollTop() > 136) {
                     $("#my-top-menu").addClass("fix-menu");
                 }
                 else {
                     $("#my-top-menu").removeClass("fix-menu");
                 }
                 //back top
                 if ($(this).scrollTop() > 400) {
                     $('#back-top').fadeIn();
                 } else {
                     $('#back-top').fadeOut();
                 }
             });
             $('#back-top').click(function () {
                 $('html,body').animate({
                     scrollTop: 0
                 }, "slow");
             });
             $(".topic-content p").each(function () {
                 text = $(this).text();
                 if (text.length > 280) {
                     $(this).html(text.substr(0, 275) + '<a class="elipsis">...</a>');
                 }
             });
         });
      </script>
      <!--Start of Tawk.to Script-->
      <script type="text/javascript">
         var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
         (function () {
             var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
             s1.async = true;
             s1.src = 'https://embed.tawk.to/57f7620b0188071f8b8efd21/default';
             s1.charset = 'UTF-8';
             s1.setAttribute('crossorigin', '*');
             s0.parentNode.insertBefore(s1, s0);
         })();
      </script>
      <!--End of Tawk.to Script-->

      <script src="http://vn3c.net/html/vietmax/js/home.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/jquery-1.10.2.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/slick.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/frmQuote.js"></script>
      <script>
         $(document).ready(function () {
             var url1 = '/Content/images/public/yeucau-1.png';
             if (url1 != null) {
                 $(".product").css("background-image", "url('" + url1 + "')");
             }
             var url2 = '/Content/images/public/yeucau-2.png';
             $(".pr2").css("background-image", "url('" + url2 + "')");
             var urlPr = '/Content/images/public/background-sp.png';
             $(".all-product").css("background-image", "url('" + urlPr + "')");
             var urlbgbottom = '/Content/images/public/yeucau-3.png';
             $(".choose").css("background-image", "url('" + urlbgbottom + "')");
         
             $(".scroll-to-frm").click(function () {
                 $('html, body').animate({
                     scrollTop: $("#frm-quote-bottom").offset().top
                 }, "slow");
             });
             $(".scroll-to-frm-pro").click(function () {
                 $('html, body').animate({
                     scrollTop: $("#frm-quote-bottom").offset().top
                 }, "slow");
                 var idproduct = $(this).attr("idproduct");
                 $("#frm-quote-bottom #Listpro1").val(idproduct);
             });
         });
      </script>
   </body>
</html>