

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wood & Land</title>
    <base href="http://woodland.vn/" target="_blank, _self, _parent, _top">

	<meta name="keywords" content="Công ty gỗ,Wood & Land,gỗ công nghiệp,go cong nghiep,Thuận An Bình Dương,thuan an bình duong,thuan an,binh duong,chất lượng quốc tế,chat luong quoc te" />
    <meta name="description" content="Công ty TNHH WOOD & LAND là nhà cung cấp gỗ công nghiệp đạt chất lượng quốc tế. Hệ thống kho của Wood & Land được đặt tại thị xã Thuận An, tỉnh Bình Dương" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png" />
    <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192" />


    
    <link href="http://vn3c.net/html/vietmax/css/css.css" rel="stylesheet">
      <link href="http://vn3c.net/html/vietmax/css/bootstrap.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/font-awesome.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/layout.css" rel="stylesheet"/>
      <script src="http://vn3c.net/html/vietmax/js/modernizr-2.6.2.js"></script>
      <link href="http://vn3c.net/html/vietmax/css/bootstrap-dialog.css" rel="stylesheet"/>
       <link href="http://vn3c.net/html/vietmax/css/product.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/home.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/slick.css" rel="stylesheet"/>





	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-100089538-1', 'auto');
	  ga('send', 'pageview');

	</script>
    
</head>
<body>
    
      <header>
         <!--Begin header top-->
         <div class="container" id="header-top">
            <div class="row">
               <!--Logo-->
               <div class="col-md-2 logo">
                  <a href="/trang-chu">
                  <img src="http://vn3c.net/html/vietmax/upload-img/logoviet.png" class="img-logo" />
                  </a>
               </div>
               <!--End Logo-->
               <div class="col-lg-1 col-md-2 lg" style="float:right;">
                  <select id="LangId" class="language">
                     <option selected value="2">Vietnamese</option>
                     <option value="1">English</option>
                  </select>
               </div>
               <div class="col-lg-2 col-md-3 icon-lang">
                  <div class="public">
                     <a href="https://plus.google.com">
                     <i class="fa fa-google-plus-square fa-2x" style="color:#e2720e;" aria-hidden="true"></i>
                     </a>
                     <a href="https://twitter.com">
                     <i class="fa fa-twitter-square fa-2x" style="color:#34c1ef;" aria-hidden="true"></i>
                     </a>
                     <a href="https://www.facebook.com">
                     <i class="fa fa-facebook-square fa-2x" style="color:#507dcf;" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <!--End header top-->
         <!--Menu-->
         <div id="my-top-menu">
            <nav class="navbar navbar-default">
               <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav">
                        <li>
                           <a href="/trang-chu">Trang chủ</a>
                        </li>
                        <li>
                           <a href="/gioi-thieu">Giới Thiệu</a>
                        </li>
                        <li>
                           <a href="/bai-viet">B&#224;i viết</a>
                        </li>
                        <li class="dropdown">
                           <a href="/san-pham">Sản Phẩm</a>
                           <ul class="dropdown-menu">
                              <li>
                                 <a href="/san-pham/van-mdf--medium-density-fibreboard-.html">
                                 V&#193;N MDF (Medium Density Fibreboard)
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-ep-plywood.html">
                                 V&#225;n &#201;p Plywood
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-okal.html">
                                 V&#225;n Okal
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/go-tram-ghep.html">
                                 Gỗ Tr&#224;m Gh&#233;p
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/gia-cong-be-mat.html">
                                 Gia C&#244;ng Bề Mặt
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="/tin-tuc-su-kien">Tin tức &amp; Sự kiện</a>
                        </li>
                        <li>
                           <a href="/lien-he">Li&#234;n hệ</a>
                        </li>
                     </ul>
                     <form action="/tim-kiem" class="navbar-form navbar-right input-search" method="get">
                        <div class="input-group">
                           <input class="form-control" id="searchString" name="searchString" placeholder="Tìm kiếm" type="text" value="" />
                           <span class="input-group-btn">
                           <button class="btn btn-default btn-search" type="submit">
                           <i class="fa fa-search" aria-hidden="true"></i>
                           </button>
                           </span>
                        </div>
                     </form>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container-fluid -->
            </nav>
         </div>
         <!--End Menu-->
      </header>

    <div class="icon-social">
                <div class="row facebook">
                    <div class="rectangle-fb">
                        <a href="https://www.facebook.com">FACEBOOK</a>
                    </div>
                    <a href="https://www.facebook.com"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row twitter">
                    <div class="rectangle-tw">
                        <a href="https://twitter.com">TWITTER</a>
                    </div>
                    <a href="https://twitter.com"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row g-plus">
                    <div class="rectangle-gg">
                        <a href="https://plus.google.com">GOOGLE PLUS</a>
                    </div>
                    <a href="https://plus.google.com"><i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a>
                </div>
        <div class="row abc">
            <div class="rectangle-video">
                <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank">VIDEO CLIPS</a>
            </div>
            <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank"><i class="fa fa-video-camera fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="row pen">
            <div class="rectangle-pen">
                <a href="/tin-tuc-su-kien">NOTE</a>
            </div>
            <a href="#"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
    





<div class="extra">
    <img src="/Content/images/public/extra-menu.png"/>
    <div class="container">
        <div class="extra-menu">
            <ul>
                <li class="index">
                    <a href="/">
                        Trang chủ
                    </a>
                </li>
                <li><i class="fa fa-caret-right index" aria-hidden="true"></i></li>
                <li id="introduce">Sản Phẩm</li>
            </ul>
        </div>
    </div>
</div>




<div class="all-product">
    <div class="container">
        <div class="discover">
            <h2 style="color:#1e8c0a; font-size:18px;">
                sản phẩm đặc trưng của
            </h2>
            <h1 class="hoanglam">
                WOOD &amp; LAND
            </h1>
        </div>
        <!-- Product 1-->
                    <div class="product-01">
                        <div class="row">
                            <div class="col-md-4 pull-right hidden-sm hidden-xs"></div>
                            <div class="col-md-4 pull-right">
                                <div class="item">
                                    <div class="images">
                                        <a href="/san-pham/van-mdf--medium-density-fibreboard-.html" class="under-img hidden-sm hidden-xs"></a>
                                        <a href="/san-pham/van-mdf--medium-density-fibreboard-.html"><img class="img-hover" src="/Content/images/vanmdf/mdf.JPG" /></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="text">
                                    <a href="/san-pham/van-mdf--medium-density-fibreboard-.html">
                                        <h5>
                                            V&#193;N MDF (Medium Density Fibreboard)
                                        </h5>
                                    </a>
                                    <p>
                                        <div style="text-align: justify;">
	<span style="color: rgb(0, 0, 0);">C&ocirc;ng ty TNHH&nbsp;</span><span style="color:#000000;">wood &amp; Land là nh&agrave; ph&acirc;n phối c&aacute;c sản phẩm tấm xơ &eacute;p &quot;MDF&quot; nhập khẩu từ New Zealand, Th&aacute;i Lan, Malaysia, Indonesia v&agrave; l&agrave; nh&agrave; ph&acirc;n phối của c&aacute;c thương hiệu trong nước như DongWha, Quảng Trị, Ki&ecirc;n giang.....được sản xuất theo ti&ecirc;u chuẩn CARB, E0, E1, E2 để đ&aacute;p ứng nhu cầu trong nước cũng như thỏa m&atilde;n y&ecirc;u cầu gia c&ocirc;ng xuất khẩu v&agrave;o c&aacute;c thị trường Bắc Mỹ, Nhật Bản, Ch&acirc;u &Acirc;u.</span></div>

                                    </p>
                                </div>
                                <div class="contact">
                                    <div class="col-md-4 details">
                                        <a href="/san-pham/van-mdf--medium-density-fibreboard-.html">
                                            chi tiết
                                        </a>
                                    </div>
                                    <div class="col-md-7 report">
                                        <a role="button" data-toggle="modal" data-target="#myModal" class="scroll-to-frm-pro" idproduct="2">
                                            y&#234;u cầu b&#225;o gi&#225;
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="product-02">
                        <div class="row">
                            <div class="col-md-4 hidden-sm hidden-xs"></div>
                            <div class="col-md-4">
                                <div class="item">
                                    <div class="images">
                                        <a href="/san-pham/van-ep-plywood.html" class="under-img hidden-sm hidden-xs"></a>
                                        <a href="/san-pham/van-ep-plywood.html"><img class="img-hover" src="/Content/images/Product1/block-0%20-%20Copy%20-%20Copy%20-%20Copy.png" /></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="text">
                                    <a href="/san-pham/van-ep-plywood.html">
                                        <h5>
                                            V&#225;n &#201;p Plywood
                                        </h5>
                                    </a>
                                    <p>
                                        <p style="text-align: justify;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">v&aacute;n &eacute;p Polywood chiụ nước (hay gỗ d&aacute;n) đ&acirc;y l&agrave; sự s&aacute;ng tạo trong ng&agrave;nh sản xuất gỗ, v&aacute;n &eacute;p n&agrave;y được l&agrave;m từ nhiều lớp gỗ mỏng được sắp xếp li&ecirc;n tục l&ecirc;n nhau theo hướng v&acirc;n gỗ của mỗi lớp, c&aacute;c lớp n&agrave;y được d&aacute;n với nhau bằng keo ( như phenol formaldehyde ) dưới sự t&aacute;c dụng của nhiệt v&agrave; lực &eacute;p của m&aacute;y &eacute;p.</span></span></span></p>
<div style="text-align: justify;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">T&ugrave;y v&agrave;o nhu cầu sử dụng m&agrave; ch&uacute;ng ta ph&acirc;n ra nhiều loại v&aacute;n &eacute;p kh&aacute;c nhau. V&aacute;n &eacute;p gỗ mềm được l&agrave;m từ gỗ th&ocirc;ng radiata v&agrave; bạch dương. V&aacute;n &eacute;p gỗ cứng được l&agrave;m từ lọai gỗ như meranti (c&ograve;n gọi l&agrave; gỗ d&aacute;i ngựa Philippine hay c&acirc;y lauan) hay gỗ bulo.</span></span></span></div>

                                    </p>
                                </div>
                                <div class="contact">
                                    <div class="col-md-4 details">
                                        <a href="/san-pham/van-ep-plywood.html">
                                            chi tiết
                                        </a>
                                    </div>
                                    <div class="col-md-7 report">
                                        <a role="button" data-toggle="modal" data-target="#myModal" class="scroll-to-frm-pro" idproduct="4">
                                            y&#234;u cầu b&#225;o gi&#225;
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="product-01">
                        <div class="row">
                            <div class="col-md-4 pull-right hidden-sm hidden-xs"></div>
                            <div class="col-md-4 pull-right">
                                <div class="item">
                                    <div class="images">
                                        <a href="/san-pham/van-okal.html" class="under-img hidden-sm hidden-xs"></a>
                                        <a href="/san-pham/van-okal.html"><img class="img-hover" src="/Content/images/Product3/van%20okal.jpg" /></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="text">
                                    <a href="/san-pham/van-okal.html">
                                        <h5>
                                            V&#225;n Okal
                                        </h5>
                                    </a>
                                    <p>
                                        <div style="text-align: justify;">
	<span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">V&aacute;n Okal c&ograve;n gọi l&agrave; V&aacute;n dăm l&agrave; lọai v&aacute;n nh&acirc;n tạo c&oacute; ruột được l&agrave;m từ lớp dăm gỗ đặc trộn keo ( nguy&ecirc;n liệu l&agrave; gỗ bạch đ&agrave;n, keo, cao su...). v&aacute;n okal c&oacute; thể đ&oacute;ng đinh , bắt ốc đều tốt, đặc biệt gi&aacute; th&agrave;nh sản xuất rất rẻ n&ecirc;n được sử dụng rất nhiều trong đời sống. Bề mặt v&aacute;n phẳng được d&ugrave;ng d&aacute;n phủ bằng những loại vật liệu trang tr&iacute; kh&aacute;c nhau: melamine, veneer (gỗ lạng).<br />
	<br />
	Hiện tại tr&ecirc;n thị trường ng&agrave;nh gỗ c&oacute; 2 d&ograve;ng sản phẩm đối với Okal l&agrave; h&agrave;ng nhập khẩu v&agrave; h&agrave;ng Việt Nam.&nbsp;</span></span></span></div>

                                    </p>
                                </div>
                                <div class="contact">
                                    <div class="col-md-4 details">
                                        <a href="/san-pham/van-okal.html">
                                            chi tiết
                                        </a>
                                    </div>
                                    <div class="col-md-7 report">
                                        <a role="button" data-toggle="modal" data-target="#myModal" class="scroll-to-frm-pro" idproduct="6">
                                            y&#234;u cầu b&#225;o gi&#225;
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="product-02">
                        <div class="row">
                            <div class="col-md-4 hidden-sm hidden-xs"></div>
                            <div class="col-md-4">
                                <div class="item">
                                    <div class="images">
                                        <a href="/san-pham/go-tram-ghep.html" class="under-img hidden-sm hidden-xs"></a>
                                        <a href="/san-pham/go-tram-ghep.html"><img class="img-hover" src="/Content/images/Product4/go-tram-ghep-1.jpg" /></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="text">
                                    <a href="/san-pham/go-tram-ghep.html">
                                        <h5>
                                            Gỗ Tr&#224;m Gh&#233;p
                                        </h5>
                                    </a>
                                    <p>
                                        <p style="text-align: justify;">
	<span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">Gỗ tr&agrave;m gh&eacute;p l&agrave; một sản phẩm được tạo ra từ c&aacute;c thanh gỗ tr&agrave;m nhỏ để gh&eacute;p với nhau, vừa đẹp mắt vừa tiết kiệm được nguồn nguy&ecirc;n liệu gỗ v&agrave; đ&aacute;p ưng được c&aacute;c nhu cầu sử dụng sản phẩm của người nh&agrave; sản xuất.&nbsp;<br />
	<br />
	C&ocirc;ng ty&nbsp;</span></span><span style="color: rgb(0, 0, 0); font-family: tahoma, geneva, sans-serif; font-size: 14px;">WOOD &amp; LAND</span><span style="font-size:14px;"><span style="font-family:tahoma,geneva,sans-serif;">&nbsp;chuy&ecirc;n ph&acirc;n phối c&aacute;c loại Tr&agrave;m gh&eacute;p ti&ecirc;u chuẩn xuất khẩu xuất xứ từ: Đồng Nai, B&igrave;nh Định, Hải Dương. Ch&iacute;nh v&igrave; vậy gỗ tr&agrave;m gh&eacute;p hiện tại tr&ecirc;n thị trường ng&agrave;nh gỗ rất được ưa chuộng.</span></span></p>

                                    </p>
                                </div>
                                <div class="contact">
                                    <div class="col-md-4 details">
                                        <a href="/san-pham/go-tram-ghep.html">
                                            chi tiết
                                        </a>
                                    </div>
                                    <div class="col-md-7 report">
                                        <a role="button" data-toggle="modal" data-target="#myModal" class="scroll-to-frm-pro" idproduct="8">
                                            y&#234;u cầu b&#225;o gi&#225;
                                        </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
            
        <!-- End Product 2-->
    </div>
</div>
<div class="container">
    <div class="paging">
        <div class="row">
            <nav aria-label="Page navigation">
                <div class="pagination-outer">
                    <nav aria-label="Page navigation">
                        <div class="pagination-container"><ul class="pagination"><li class="active"><a>1</a></li><li><a href="/san-pham?page=2">2</a></li><li class="PagedList-skipToNext"><a href="/san-pham?page=2" rel="next">»</a></li></ul></div>
                    </nav>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
            <div class="modal-body">
                <div class="choose" id="frm-quote-bottom">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="comment">
                                <div class="head-box">
                                    <h5>
                                        b&#225;o gi&#225; nhanh
                                    </h5>
                                </div>
                                <p>
                                    Điền th&ocirc;ng tin theo mẫu, Ch&uacute;ng t&ocirc;i sẽ b&aacute;o gi&aacute; cho bạn chỉ trong 60 ph&uacute;t
                                </p>
                                <div class="line-decorate col-sm-7 line-top">
                                    <span class="sub-line-1"></span>
                                </div>
                                <form method="post" action="">
                                    <div class="form-group">
                                            <input id="Name1" class="input" type="text" placeholder="Họ và tên" />
                                            <input id="Phone1" class="input" type="text" placeholder="Số điện thoại" />
                                        <input id="Email1" class="input" type="text" placeholder="Email" />
                                        <select class="form-control type" id="Listpro1" name="Listpro1"><option value="">--None---</option>
<option value="2">V&#193;N MDF (Medium Density Fibreboard)</option>
<option value="4">V&#225;n &#201;p Plywood</option>
<option value="6">V&#225;n Okal</option>
<option value="8">Gỗ Tr&#224;m Gh&#233;p</option>
<option value="10">Gia C&#244;ng Bề Mặt</option>
</select>
                                            <input id="Numbers1" class="input" type="text" placeholder="Số lượng (m3)" />
                                            <div class="form-group block-textarea">
                                                <textarea id="Requests1" class="txtinput" rows="9" placeholder="Yêu cầu khác"></textarea>
                                            </div>

                                        <div class="send-info form-group">
                                            <button id="btnsendQuote1" type="button" class="btn btn-default btn-input">
                                                gửi y&#234;u cầu b&#225;o gi&#225;
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


    <div class="loadding">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
    



<footer>
    <div class="container info">
            <div class="row">
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-phone-square fa-2x" aria-hidden="true"></i>
                    <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">HOTLINE</strong>
                    <a href="tel:0908759007">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0908759007</strong>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-briefcase fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kinh doanh</strong>
                    <a href="tel:0932100411">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0932100411</strong>
                    </a>
                    <strong class="mail"> minhphuongwl@gmail.com</strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-credit-card-alt fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kế toán</strong>
                    <a href="tel:0987477937">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0987477937</strong>
                    </a>
                    <strong class="mail">huyenngoc2001@yahoo.com </strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-truck fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">giao nhận</strong>
                    <a href="tel:0274 6515129">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0274 6515129</strong>
                    </a>
                    <strong class="mail">daoquynhnhu98@gmail.com </strong>
                </div>
                <div class="clearfix"></div>
            </div>

    </div>
    <div class="form-email">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-4 email">
                    <h4 style="color:white;">
                        nhận tin từ ch&#250;ng t&#244;i
                    </h4>
                </div>
                <div class="col-sm-5 col-xs-7 newsletter-outer">
                    <form class="navbar-form navbar-right input-email">
                        <div class="input-group">
                            <input type="text" class="txt-mail" id="txtEmail" placeholder="Nhập địa chỉ email" />
                            <span class="input-group-btn">
                                <button id="msg" class="btn btn-secondary" type="button">
                                    <i class="fa fa-caret-right" style="color:#fff;font-size:18px;"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/dia-diem.png" /> 
                            th&#244;ng tin li&#234;n hệ
                        </strong>
                        <p class="p-margin">
                            121/62 Phạm Ngọc Thạch, Khu 5, Phường Hi&#234;̣p Thành, Tp Thủ D&#226;̀u M&#244;̣t, Bình Dương
                        </p>
                            <p>
                                Điện thoại: 0274 6515129
                            </p>
                        <p>
                            Fax: 0274 3515129
                        </p>
                    </div>
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/hop-thu.png" /> 
                                chi tiết li&#234;n hệ
                        </strong>
                        <p class="p-margin">Email:<br />lienphuonghl@yahoo.com</p>
                        <div class="col-md-4 contact-us">
                            <a href="/lien-he">li&#234;n hệ</a>
                        </div>
                    </div>
                    <div class="col-md-4 contact" style="margin-top:20px;">
                        <a href="tel:0908759007">
                            <strong style="color:#1e8c0a; font-size:30px; font-weight:700;">
                                0908759007
                            </strong>
                        </a>
                            <p class="p-margin">Giờ l&#224;m việc 7:30am - 4:30pm</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        
    </div>
</footer>
<a id="back-top"><i class="fa fa-play"></i></a>

 <script src="http://vn3c.net/html/vietmax/js/jquery-1.10.2.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/register-email-news.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/respond.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap-dialog.js"></script>



    
    <script type="text/javascript">
        $(document).ready(function () {
            //change language
            $("#LangId").change(function () {
                var langId = this.value;
                $.ajax({
                    type: 'POST',
                    url: "/CommonPartial/ChangeLanguage",
                    data: { langId: langId },
                    success: function (response) {
                        window.location.assign('/');
                    }
                });
            });
            //fix menu
            $(window).scroll(function () {
                if ($(this).scrollTop() > 136) {
                    $("#my-top-menu").addClass("fix-menu");
                }
                else {
                    $("#my-top-menu").removeClass("fix-menu");
                }
                //back top
                if ($(this).scrollTop() > 400) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });
            $('#back-top').click(function () {
                $('html,body').animate({
                    scrollTop: 0
                }, "slow");
            });
            $(".topic-content p").each(function () {
                text = $(this).text();
                if (text.length > 280) {
                    $(this).html(text.substr(0, 275) + '<a class="elipsis">...</a>');
                }
            });
        });
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/57f7620b0188071f8b8efd21/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    
    <script src="http://vn3c.net/html/vietmax/js/frmQuote.js"></script>

<script>
    $(document).ready(function () {
        var urlPr = '/Content/images/public/background-sp.png';
        $(".all-product").css("background-image", "url('" + urlPr + "')");
        $(".scroll-to-frm-pro").click(function () {
            var idproduct = $(this).attr("idproduct");
            $("#frm-quote-bottom #Listpro1").val(idproduct);
        });
    });
    </script>
    
</body>
</html>
