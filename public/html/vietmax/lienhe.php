

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wood & Land</title>
    <base href="http://woodland.vn/" target="_blank, _self, _parent, _top">
	<meta name="keywords" content="Công ty gỗ,Wood & Land,gỗ công nghiệp,go cong nghiep,Thuận An Bình Dương,thuan an bình duong,thuan an,binh duong,chất lượng quốc tế,chat luong quoc te" />
    <meta name="description" content="Công ty TNHH WOOD & LAND là nhà cung cấp gỗ công nghiệp đạt chất lượng quốc tế. Hệ thống kho của Wood & Land được đặt tại thị xã Thuận An, tỉnh Bình Dương" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png" />
    <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192" />



     <link href="http://vn3c.net/html/vietmax/css/css.css" rel="stylesheet">
      <link href="http://vn3c.net/html/vietmax/css/bootstrap.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/font-awesome.min.css" rel="stylesheet"/>
      <link href="http://vn3c.net/html/vietmax/css/layout.css" rel="stylesheet"/>
      <script src="http://vn3c.net/html/vietmax/js/modernizr-2.6.2.js"></script>
      <link href="http://vn3c.net/html/vietmax/css/bootstrap-dialog.css" rel="stylesheet"/>
     

    

    
    <link href="http://vn3c.net/html/vietmax/css/contact.css" rel="stylesheet"/>


    




	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-100089538-1', 'auto');
	  ga('send', 'pageview');

	</script>
    
</head>
<body>
    
    <header>
         <!--Begin header top-->
         <div class="container" id="header-top">
            <div class="row">
               <!--Logo-->
               <div class="col-md-2 logo">
                  <a href="/trang-chu">
                  <img src="http://vn3c.net/html/vietmax/upload-img/logoviet.png" class="img-logo" />
                  </a>
               </div>
               <!--End Logo-->
               <div class="col-lg-1 col-md-2 lg" style="float:right;">
                  <select id="LangId" class="language">
                     <option selected value="2">Vietnamese</option>
                     <option value="1">English</option>
                  </select>
               </div>
               <div class="col-lg-2 col-md-3 icon-lang">
                  <div class="public">
                     <a href="https://plus.google.com">
                     <i class="fa fa-google-plus-square fa-2x" style="color:#e2720e;" aria-hidden="true"></i>
                     </a>
                     <a href="https://twitter.com">
                     <i class="fa fa-twitter-square fa-2x" style="color:#34c1ef;" aria-hidden="true"></i>
                     </a>
                     <a href="https://www.facebook.com">
                     <i class="fa fa-facebook-square fa-2x" style="color:#507dcf;" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
         <!--End header top-->
         <!--Menu-->
         <div id="my-top-menu">
            <nav class="navbar navbar-default">
               <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav">
                        <li>
                           <a href="/trang-chu">Trang chủ</a>
                        </li>
                        <li>
                           <a href="/gioi-thieu">Giới Thiệu</a>
                        </li>
                        <li>
                           <a href="/bai-viet">B&#224;i viết</a>
                        </li>
                        <li class="dropdown">
                           <a href="/san-pham">Sản Phẩm</a>
                           <ul class="dropdown-menu">
                              <li>
                                 <a href="/san-pham/van-mdf--medium-density-fibreboard-.html">
                                 V&#193;N MDF (Medium Density Fibreboard)
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-ep-plywood.html">
                                 V&#225;n &#201;p Plywood
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/van-okal.html">
                                 V&#225;n Okal
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/go-tram-ghep.html">
                                 Gỗ Tr&#224;m Gh&#233;p
                                 </a>
                              </li>
                              <li>
                                 <a href="/san-pham/gia-cong-be-mat.html">
                                 Gia C&#244;ng Bề Mặt
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <li>
                           <a href="/tin-tuc-su-kien">Tin tức &amp; Sự kiện</a>
                        </li>
                        <li>
                           <a href="/lien-he">Li&#234;n hệ</a>
                        </li>
                     </ul>
                     <form action="/tim-kiem" class="navbar-form navbar-right input-search" method="get">
                        <div class="input-group">
                           <input class="form-control" id="searchString" name="searchString" placeholder="Tìm kiếm" type="text" value="" />
                           <span class="input-group-btn">
                           <button class="btn btn-default btn-search" type="submit">
                           <i class="fa fa-search" aria-hidden="true"></i>
                           </button>
                           </span>
                        </div>
                     </form>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container-fluid -->
            </nav>
         </div>
         <!--End Menu-->
      </header>





    <div class="icon-social">
                <div class="row facebook">
                    <div class="rectangle-fb">
                        <a href="https://www.facebook.com">FACEBOOK</a>
                    </div>
                    <a href="https://www.facebook.com"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row twitter">
                    <div class="rectangle-tw">
                        <a href="https://twitter.com">TWITTER</a>
                    </div>
                    <a href="https://twitter.com"><i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i></a>
                </div>
                <div class="row g-plus">
                    <div class="rectangle-gg">
                        <a href="https://plus.google.com">GOOGLE PLUS</a>
                    </div>
                    <a href="https://plus.google.com"><i class="fa fa-google-plus-square fa-2x" aria-hidden="true"></i></a>
                </div>
        <div class="row abc">
            <div class="rectangle-video">
                <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank">VIDEO CLIPS</a>
            </div>
            <a href="https://www.youtube.com/watch?v=8Sk2QAlxohM&amp;feature=youtu.be&amp;t=10" target="_blank"><i class="fa fa-video-camera fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="row pen">
            <div class="rectangle-pen">
                <a href="/tin-tuc-su-kien">NOTE</a>
            </div>
            <a href="#"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
    




<div class="extra">
    <img src="/Content/images/public/extra-menu.png"/>
    <div class="container">
        <div class="extra-menu">
            <ul>
                <li class="index">
                    <a href="/">
                        Trang chủ
                    </a>
                </li>
                <li><i class="fa fa-caret-right index" aria-hidden="true"></i></li>
                <li id="introduce">Li&#234;n hệ</li>
            </ul>
        </div>
    </div>
</div>


<div class="article-outer">
    <div class="container">
        <div class="discover">
            <h2 style="color:#1e8c0a; font-size:18px;">
                li&#234;n hệ với c&#244;ng ty
            </h2>
            <h1 class="hoanglam">
                TNHH WOOD &amp; LAND
            </h1>
        </div>
    </div>
</div>
<div class="container" style="margin-bottom:30px;">
    <div class="row">
        <div class="col-md-4">
                <div class="address">
                        <h5>ĐỊA CHỈ</h5>
                    <p>
                        121/62 Phạm Ngọc Thạch, Khu 5, Phường Hi&#234;̣p Thành, Tp Thủ D&#226;̀u M&#244;̣t, Bình Dương
                    </p>
                    <p>
                        Giờ l&#224;m việc 7:30am - 4:30pm
                    </p>
                    <p>
                        7:30am - 4:30pm
                    </p>
                </div>
            
            <div id="map_canvas" style="height:300px;">

            </div>
        </div>
        <div class="col-md-4">
            <div class="comment">
                <div class="head-box">
                        <h5>ĐÓNG GÓP Ý KIẾN</h5>

                    <p>
Để Wood &amp; Land phục vụ bạn tốt hơn nữa. Ch&uacute;ng t&ocirc;i rất vui nếu bạn đ&oacute;ng g&oacute;p cho những thiếu s&oacute;t của Wood &amp; Land trong suốt thời gian phục vụ bạn. Đội ngũ Wood &amp; Land ch&acirc;n th&agrave;nh cảm ơn bạn.&nbsp;                    </p>
                </div>

                <form method="post" action="">
                    <div class="form-group">
                        <input id="Name" class="input" type="text" placeholder="Name" />
                        <input id="Email" class="input" type="text" placeholder="Email" />
                        <input id="Phone" class="input" type="text" placeholder="Tel" />
                        <div class="form-group block-textarea">
                            <textarea id="Comment" class="txtinput" rows="5" placeholder="Your comment(s)"></textarea>
                        </div>
                        <div class="send-info form-group">
                            <button id="btSend" type="button" class="btn btn-default btn-input">
                                gửi đi
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
                <div class="img-tel">
                    <img src="/Content/images/public/telephone.png" />
                </div>
        </div>
    </div>
</div>

    <div class="loadding">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
    



<footer>
    <div class="container info">
            <div class="row">
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-phone-square fa-2x" aria-hidden="true"></i>
                    <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">HOTLINE</strong>
                    <a href="tel:0908759007">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0908759007</strong>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-briefcase fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kinh doanh</strong>
                    <a href="tel:0932100411">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0932100411</strong>
                    </a>
                    <strong class="mail"> minhphuongwl@gmail.com</strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-credit-card-alt fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">kế toán</strong>
                    <a href="tel:0987477937">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0987477937</strong>
                    </a>
                    <strong class="mail">huyenngoc2001@yahoo.com </strong>
                </div>
                <div class="col-lg-3 col-md-6 icon">
                    <i class="fa fa-truck fa-2x" aria-hidden="true"></i>
                        <strong style="display:block;margin-top:10px;text-transform:uppercase;font-size:16px;color:#565656;font-weight:700;">giao nhận</strong>
                    <a href="tel:0274 6515129">
                        <strong style="font-weight:700;color: #565656;font-size: 20px;">0274 6515129</strong>
                    </a>
                    <strong class="mail">daoquynhnhu98@gmail.com </strong>
                </div>
                <div class="clearfix"></div>
            </div>

    </div>
    <div class="form-email">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-4 email">
                    <h4 style="color:white;">
                        nhận tin từ ch&#250;ng t&#244;i
                    </h4>
                </div>
                <div class="col-sm-5 col-xs-7 newsletter-outer">
                    <form class="navbar-form navbar-right input-email">
                        <div class="input-group">
                            <input type="text" class="txt-mail" id="txtEmail" placeholder="Nhập địa chỉ email" />
                            <span class="input-group-btn">
                                <button id="msg" class="btn btn-secondary" type="button">
                                    <i class="fa fa-caret-right" style="color:#fff;font-size:18px;"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/dia-diem.png" /> 
                            th&#244;ng tin li&#234;n hệ
                        </strong>
                        <p class="p-margin">
                            121/62 Phạm Ngọc Thạch, Khu 5, Phường Hi&#234;̣p Thành, Tp Thủ D&#226;̀u M&#244;̣t, Bình Dương
                        </p>
                            <p>
                                Điện thoại: 0274 6515129
                            </p>
                        <p>
                            Fax: 0274 3515129
                        </p>
                    </div>
                    <div class="col-md-4 contact">
                        <strong style="font-weight:700;text-transform:uppercase;color:#1e8c0a;font-size:16px;margin-top:20px;margin-bottom:10px;">
                            <img src="/Content/images/icon/hop-thu.png" /> 
                                chi tiết li&#234;n hệ
                        </strong>
                        <p class="p-margin">Email:<br />lienphuonghl@yahoo.com</p>
                        <div class="col-md-4 contact-us">
                            <a href="/lien-he">li&#234;n hệ</a>
                        </div>
                    </div>
                    <div class="col-md-4 contact" style="margin-top:20px;">
                        <a href="tel:0908759007">
                            <strong style="color:#1e8c0a; font-size:30px; font-weight:700;">
                                0908759007
                            </strong>
                        </a>
                            <p class="p-margin">Giờ l&#224;m việc 7:30am - 4:30pm</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        
    </div>
</footer>
<a id="back-top"><i class="fa fa-play"></i></a>
 <script src="http://vn3c.net/html/vietmax/js/jquery-1.10.2.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/register-email-news.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/respond.min.js"></script>
      <script src="http://vn3c.net/html/vietmax/js/bootstrap-dialog.js"></script>


    
    <script type="text/javascript">
        $(document).ready(function () {
            //change language
            $("#LangId").change(function () {
                var langId = this.value;
                $.ajax({
                    type: 'POST',
                    url: "/CommonPartial/ChangeLanguage",
                    data: { langId: langId },
                    success: function (response) {
                        window.location.assign('/');
                    }
                });
            });
            //fix menu
            $(window).scroll(function () {
                if ($(this).scrollTop() > 136) {
                    $("#my-top-menu").addClass("fix-menu");
                }
                else {
                    $("#my-top-menu").removeClass("fix-menu");
                }
                //back top
                if ($(this).scrollTop() > 400) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });
            $('#back-top').click(function () {
                $('html,body').animate({
                    scrollTop: 0
                }, "slow");
            });
            $(".topic-content p").each(function () {
                text = $(this).text();
                if (text.length > 280) {
                    $(this).html(text.substr(0, 275) + '<a class="elipsis">...</a>');
                }
            });
        });
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/57f7620b0188071f8b8efd21/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCro_dH_n_ev14E-O8a5h20mJpuD0ZtuHc&callback=initialize" type="text/javascript"></script>
    <script src="http://vn3c.net/html/vietmax/js/contact.js"></script>

    <script>
        /*google map*/
        function initialize() {
            //tọa độ
            var myLatlng = new google.maps.LatLng(10.9763491,106.741585);
            var mapOptions = {
                zoom: 16,
                center: myLatlng
            };
            var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            //var image = '../Content/images/icon/map-marker.png';
            var contentString = "";
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'HoangLam office',
                icon: image
            });
            //infowindow.open(map, marker);
            //google.maps.event.addListener(marker, 'click', function () {
            //    infowindow.open(map, marker);
            //});
        }
    //google.maps.event.addDomListener(window, 'load', initialize);


    //////
</script>


</body>
</html>
