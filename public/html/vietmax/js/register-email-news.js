﻿$("#msg").click(function () {    
    email = $("#txtEmail").val();
    if (email == "") {
        BootstrapDialog.alert("Bạn chưa nhập email đăng ký.");
        return;
    }
    //var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    //var emailReg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var emailReg = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    var valid = emailReg.test(email);

    if (!valid) {
        //alert("Email is not correct format return false.");
        BootstrapDialog.alert("Email không hợp lệ.");
        return;
    }

    $.ajax({
        url: "/Register/RegisterEmail",
        data: { Email: email },
        success: function (response) {
            BootstrapDialog.alert(response);
            $("#txtEmail").val("");
        }
    });
});