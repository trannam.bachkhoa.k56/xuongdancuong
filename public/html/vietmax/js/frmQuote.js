﻿$(document).ready(function () {
    var ajax_sendding = false;

    $("#btnsendQuote").click(function () {
        name = $("#Name").val();
        if (name.trim() == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập vào tên!'
            })
            return;
        }
        phone = $("#Phone").val();
        if (phone == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập số điện thoại!'
            })
            return;
        }
        email = $("#Email").val();
        if (email == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập vào Email!'
            })
            return;
        }
        var emailReg = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        var valid = emailReg.test(email);

        if (!valid) {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Email không hợp lệ!'
            })
            return;
        }

        product = $("#Listpro").val();
        if (product == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa chọn sản phẩm'
            })
            return;
        }
        namePro = $("#Listpro option:selected").text();
        numbers = $("#Numbers").val();
        if (numbers == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập số lượng'
            })
            return;
        }
        requests = $("#Requests").val();
        //if (comment == "") {
        //    BootstrapDialog.show({
        //        title: 'Thông Báo',
        //        message: 'Bạn chưa nhập nội dung!'
        //    })
        //    return;
        //}
        if (ajax_sendding == true) {
            return false;
        }
        ajax_sendding = true;
        $('.loadding').show();
        $.ajax({
            type: 'POST',
            url: "/CommonPartial/SendQuote",
            data: {
                Name: name,
                Email: email,
                Phone: phone,
                nameProduct:namePro,
                Product: product,
                Numbers:numbers,
                Requests: requests
            },
            success: function (response) {
                if (response != "-1") {
                    //BootstrapDialog.show({
                    //    title: 'Thông Báo',
                    //    message: 'Gửi thành công!'
                    //})
                    window.location.assign("/cam-on");
                    $("#Name").val(""),
                    $("#Email").val(""),
                    $("#Phone").val(""),
                    $("#Numbers").val(""),
                    $("#Listpro").val(""),
                    $("#Requests").val("")
                }
                else {
                    BootstrapDialog.alert("Contacts failed, please try again.");
                }
            }
        }).always(function () {
            ajax_sendding = false;
            $('.loadding').hide();
        });
    });
    $("#btnsendQuote1").click(function () {
        name = $("#Name1").val();
        if (name.trim() == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập vào tên!'
            })
            return;
        }
        phone = $("#Phone1").val();
        if (phone == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập số điện thoại!'
            })
            return;
        }
        email = $("#Email1").val();
        if (email == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập vào Email!'
            })
            return;
        }
        var emailReg = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        var valid = emailReg.test(email);

        if (!valid) {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Email không hợp lệ!'
            })
            return;
        }

        product = $("#Listpro1").val();
        if (product == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa chọn sản phẩm'
            })
            return;
        }
        namePro = $("#Listpro1 option:selected").text();
        numbers = $("#Numbers1").val();
        if (numbers == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập số lượng'
            })
            return;
        }
        requests = $("#Requests1").val();
        //if (comment == "") {
        //    BootstrapDialog.show({
        //        title: 'Thông Báo',
        //        message: 'Bạn chưa nhập nội dung!'
        //    })
        //    return;
        //}
        if (ajax_sendding == true) {
            return false;
        }
        ajax_sendding = true;
        $('.loadding').show();
        $.ajax({
            type: 'POST',
            url: "/CommonPartial/SendQuote",
            data: {
                Name: name,
                Email: email,
                Phone: phone,
                nameProduct: namePro,
                Product: product,
                Numbers: numbers,
                Requests: requests
            },
            success: function (response) {
                if (response != "-1") {
                    //BootstrapDialog.show({
                    //    title: 'Thông Báo',
                    //    message: 'Gửi thành công!'
                    //})
                    //var url = '/Thanks/Index';
                    window.location.assign("/cam-on");
                    $("#Name1").val(""),
                    $("#Email1").val(""),
                    $("#Phone1").val(""),
                    $("#Numbers1").val(""),
                    $("#Listpro1").val(""),
                    $("#Requests1").val("")
                }
                else {
                    BootstrapDialog.alert("Contacts failed, please try again.");
                }
            }
        }).always(function () {
            ajax_sendding = false;
            $('.loadding').hide();
        });
    });
});