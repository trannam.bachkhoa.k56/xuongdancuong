﻿$(document).ready(function () {
    var ajax_sendding = false;
    $("#btSend").click(function () {
        name = $("#Name").val();
        if (name.trim() == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập vào tên!'
            })
            return;
        }
        email = $("#Email").val();
        if (email == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập vào Email!'
            })
            return;
        }
        var emailReg = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        var valid = emailReg.test(email);

        if (!valid) {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Email không hợp lệ!'
            })
            return;
        }
        phone = $("#Phone").val();
        if (phone == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập số điện thoại!'
            })
            return;
        }
        comment = $("#Comment").val();
        if (comment == "") {
            BootstrapDialog.show({
                title: 'Thông Báo',
                message: 'Bạn chưa nhập nội dung!'
            })
            return;
        }
        ajax_sendding = true;
        $('.loadding').show();
        $.ajax({
            type: 'POST',
            url: "/Contact/SendContact",
            data: {
                Name: name,
                Email: email,
                Phone: phone,
                Comment: comment
            },
            success: function (response) {
                if (response != "-1") {
                    BootstrapDialog.show({
                        title: 'Thông Báo',
                        message: 'Gửi thành công!'
                    })
                    $("#Name").val(""),
                    $("#Email").val(""),
                    $("#Phone").val(""),
                    $("#Comment").val("")
                }
                else {
                    BootstrapDialog.alert("Contacts failed, please try again.");
                }
            }
        }).always(function () {
            ajax_sendding = false;
            $('.loadding').hide();
        });
    });
});