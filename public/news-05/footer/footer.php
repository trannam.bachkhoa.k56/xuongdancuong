<footer id="Footer" class="clearfix">
            <div class="widgets_wrapper" style="padding:15px;">
               <div class="container">
                  <div class="column one-third">
                     <aside id="text-5" class="widget widget_text">
                        <h4>CHÍNH SÁCH &#038; QUY ĐỊNH</h4>
                        <div class="textwidget">
                           <ul class="footer_links">
                              <li><a href="http://metaldoor.vn/chinh-sach-va-quy-dinh-chung/">Chính sách và Quy định chung</a></li>
                              <li><a href="http://metaldoor.vn/quy-dinh-va-hinh-thuc-thanh-toan/">Quy định và hình thức thanh toán</a></li>
                              <li><a href="http://metaldoor.vn/bao-hanh-san-pham/">Bảo hành sản phẩm</a></li>
                              <li><a href="http://metaldoor.vn/quy-trinh-xu-ly-khieu-nai/">Quy trình xử lý khiếu nại</a></li>
                              <li><a href="http://metaldoor.vn/chinh-sach-bao-mat-thong-tin/">Chính sách bảo mật thông tin</a></li>
                           </ul>
                        </div>
                     </aside>
                  </div>
                  <div class="column one-third">
                     <aside id="text-7" class="widget widget_text">
                        <h4>TƯ VẤN KHÁCH HÀNG</h4>
                        <div class="textwidget">
                           <ul class="footer_links">
                              <li>Tel: <span style="font-size: 16pt;"><strong>(04) 35 202 228</strong></span></li>
                              <li>Hotline: <span style="font-size: 16pt;"><strong>0942 088 885</strong></span></li>
                              <li>Skype: <a href="skype:ngoisaobang.1120?chat">MetalDoor</a></li>
                              <li>Email: <a href="mailto:contact@metaldoor.vn">contact@metaldoor.vn</a></li>
                              <li>Website: <a href="http://metaldoor.vn">www.metaldoor.vn</a></li>
                           </ul>
                        </div>
                     </aside>
                  </div>
                  <div class="column one-third">
                     <aside id="text-2" class="widget widget_text">
                        <h4>THÔNG TIN LIÊN HỆ</h4>
                        <div class="textwidget">
                           <ul class="footer_links">
                              <li><b>CÔNG TY CP XÂY DỰNG CÔNG NGHIỆP METAL VIỆT NAM</b></li>
                              <li>Tầng 18, CT3 The Pride, Tố Hữu, Phường La Khê, Quận Hà Đông, TP Hà Nội</li>
                              <li>Nhà máy sản xuất HN: KCN Bắc Thăng Long - Hà Nội</li>
                              <li>VP đại diện & Nhà máy sản xuất TPHCM: Lê Thị Riêng, Phường Thới An, Quận 12, TP.HCM</li>
                           </ul>
                        </div>
                     </aside>
                  </div>
               </div>
            </div>
            <div class="footer_copy">
               <div class="container">
                  <div class="column one">
                     <a id="back_to_top" class="button button_left button_js" href=""><span class="button_icon"><i class="icon-up-open-big"></i></span></a>
                     <!-- Copyrights -->
                     <div class="copyright">
                        &copy; 2018 Metal Door. All Rights Reserved. 
                     </div>
                     <ul class="social"></ul>
                  </div>
               </div>
            </div>
         </footer>