
<!-- .header_placeholder 4sticky  -->
<div class="header_placeholder"></div>
<div id="Top_bar" class="loading">
  <div class="container">
     <div class="column one">
        <div class="top_bar_left clearfix">
           <!-- .logo -->
           <div class="logo">
              <a id="logo" href="http://metaldoor.vn" title="Metal Door"><img class="logo-main scale-with-grid" src="http://metaldoor.vn/wp-content/uploads/2016/12/metaldoor-vietnam.png" alt="metaldoor-vietnam" /><img class="logo-sticky scale-with-grid" src="http://metaldoor.vn/wp-content/uploads/2016/12/metaldoor-vietnam.png" alt="metaldoor-vietnam" /><img class="logo-mobile scale-with-grid" src="http://metaldoor.vn/wp-content/uploads/2016/12/metaldoor-vietnam.png" alt="metaldoor-vietnam" /></a>				
           </div>
           <div class="menu_wrapper">
              <nav id="menu" class="menu-main-menu-container">
                 <ul id="menu-main-menu" class="menu">
                    <li id="menu-item-3835" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2324 current_page_item"><a href="http://metaldoor.vn/"><span>TRANG CHỦ</span></a></li>
                    <li id="menu-item-2361" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                       <a href="http://metaldoor.vn/gioi-thieu/"><span>GIỚI THIỆU</span></a>
                       <ul class="sub-menu">
                          <li id="menu-item-2604" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/gioi-thieu/"><span>Giới thiệu chung</span></a></li>
                          <li id="menu-item-2605" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/tam-nhin-va-dinh-huong/"><span>Tầm nhìn và Định hướng</span></a></li>
                          <li id="menu-item-2714" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/he-thong-nha-may/"><span>Hệ thống nhà máy</span></a></li>
                          <li id="menu-item-3046" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/doi-tac/"><span>Đối tác</span></a></li>
                          <li id="menu-item-3877" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/chinh-sach-va-quy-dinh/"><span>Chính sách và Quy định</span></a></li>
                       </ul>
                    </li>
                    <li id="menu-item-3484" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                       <a href="http://metaldoor.vn/san-pham-nhom-kinh-cao-cap/"><span>SẢN PHẨM</span></a>
                       <ul class="sub-menu">
                          <li id="menu-item-2717" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                             <a href="http://metaldoor.vn/cua-nhom-kinh-metal-door/"><span>Cửa nhôm kính</span></a>
                             <ul class="sub-menu">
                                <li id="menu-item-3587" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/cua-di-mo-quay/"><span>Cửa đi mở quay</span></a></li>
                                <li id="menu-item-3589" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/cua-di-mo-truot-lua-2/"><span>Cửa đi mở trượt/lùa</span></a></li>
                                <li id="menu-item-3353" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/cua-di-xep-truot/"><span>Cửa đi xếp trượt</span></a></li>
                                <li id="menu-item-2755" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/cua-thong-minh/"><span>Cửa thủy lực</span></a></li>
                                <li id="menu-item-3359" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/cua-xoay/"><span>Cửa tự động</span></a></li>
                             </ul>
                          </li>
                          <li id="menu-item-2734" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                             <a href="http://metaldoor.vn/cua-so-nhom-kinh/"><span>Cửa sổ nhôm kính</span></a>
                             <ul class="sub-menu">
                                <li id="menu-item-3562" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/cua-so-mo-quay-mo-hat/"><span>Cửa sổ mở quay</span></a></li>
                                <li id="menu-item-3617" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/he-thong-cua-so-truot/"><span>Cửa sổ mở trượt/lùa</span></a></li>
                             </ul>
                          </li>
                          <li id="menu-item-3189" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                             <a href="http://metaldoor.vn/mat-dung-nhom-kinh/"><span>Mặt dựng nhôm kính</span></a>
                             <ul class="sub-menu">
                                <li id="menu-item-2773" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/mat-dung-kinh-he-stick/"><span>Mặt dựng kính hệ Stick</span></a></li>
                                <li id="menu-item-2784" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/mat-dung-kinh-he-unitized/"><span>Mặt dựng kính hệ Unitized</span></a></li>
                                <li id="menu-item-3371" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/mat-dung-he-stick/"><span>Mặt dựng kính hệ Semi</span></a></li>
                             </ul>
                          </li>
                          <li id="menu-item-2992" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/vach-ngan/"><span>Vách kính</span></a></li>
                          <li id="menu-item-2764" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/lam-chan-nang/"><span>Lam chắn nắng</span></a></li>
                          <li id="menu-item-2885" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/tam-op/"><span>Tấm ốp nhôm</span></a></li>
                          <li id="menu-item-2970" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/he-thong-sun-control/"><span>Phòng tắm kính</span></a></li>
                          <li id="menu-item-3348" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/mai-don/"><span>Mái kính</span></a></li>
                          <li id="menu-item-3188" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                             <a href="http://metaldoor.vn/tuong-kinh/"><span>Cầu thang kính</span></a>
                             <ul class="sub-menu">
                                <li id="menu-item-2903" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/tuong-kinh-su-dung-spider/"><span>Tổng quan cầu thang kính</span></a></li>
                                <li id="menu-item-2993" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/tuong-kinh-khong-su-dung-khung/"><span>Cầu thang kính cường lực</span></a></li>
                             </ul>
                          </li>
                       </ul>
                    </li>
                    <li id="menu-item-2363" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                       <a href="http://metaldoor.vn/tin-tuc/"><span>TIN TỨC</span></a>
                       <ul class="sub-menu">
                          <li id="menu-item-2457" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/tin-tuc/tin-tuc-va-su-kien/"><span>Tin tức và Sự kiện</span></a></li>
                          <li id="menu-item-2456" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/tin-tuc/tin-chuyen-nganh/"><span>Tin chuyên ngành</span></a></li>
                          <li id="menu-item-3372" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/tin-tuc/tin-hoat-dong-lien-quan/"><span>Tin hoạt động liên quan</span></a></li>
                       </ul>
                    </li>
                    <li id="menu-item-2942" class="menu-item menu-item-type-taxonomy menu-item-object-portfolio-types menu-item-has-children">
                       <a href="http://metaldoor.vn/portfolio-types/cong-trinh/"><span>DỰ ÁN</span></a>
                       <ul class="sub-menu">
                          <li id="menu-item-2943" class="menu-item menu-item-type-taxonomy menu-item-object-portfolio-types"><a href="http://metaldoor.vn/portfolio-types/cong-trinh-da-trien-khai/"><span>Công trình đã triển khai</span></a></li>
                          <li id="menu-item-2944" class="menu-item menu-item-type-taxonomy menu-item-object-portfolio-types"><a href="http://metaldoor.vn/portfolio-types/cong-trinh-dang-trien-khai/"><span>Công trình đang triển khai</span></a></li>
                       </ul>
                    </li>
                    <li id="menu-item-3488" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                       <a href="http://metaldoor.vn/cong-nghe/"><span>CÔNG NGHỆ</span></a>
                       <ul class="sub-menu">
                          <li id="menu-item-3519" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/kinh/"><span>Kính</span></a></li>
                          <li id="menu-item-3520" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/nhom/"><span>Nhôm</span></a></li>
                          <li id="menu-item-3521" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/phu-kien/"><span>Phụ kiện</span></a></li>
                          <li id="menu-item-3522" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/video-clips-cong-nghe/"><span>Video Clips</span></a></li>
                       </ul>
                    </li>
                    <li id="menu-item-3870" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/hinh-anh/"><span>HÌNH ẢNH</span></a></li>
                    <li id="menu-item-2368" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/lien-he/"><span>LIÊN HỆ</span></a></li>
                 </ul>
              </nav>
              <a class="responsive-menu-toggle " href="#"><i class="icon-menu"></i></a>					
           </div>
           <div class="secondary_menu_wrapper">
              <!-- #secondary-menu -->
           </div>
           <div class="banner_wrapper">
           </div>
           <div class="search_wrapper">
              <!-- #searchform -->
              <form method="get" id="searchform" action="http://metaldoor.vn/">
                 <i class="icon_search icon-search"></i>
                 <a href="#" class="icon_close"><i class="icon-cancel"></i></a>
                 <input type="text" class="field" name="s" id="s" placeholder="Enter your search" />			
                 <input type="submit" class="submit" value="" style="display:none;" />
              </form>
           </div>
        </div>
        <div class="top_bar_right">
           <div class="top_bar_right_wrapper"><a id="search_button" href="#"><i class="icon-search"></i></a></div>
        </div>
     </div>
  </div>
</div>

   