<!DOCTYPE html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
   <!-- head -->
   <head>
      <!-- meta -->
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <!--Start of Tawk.to Script-->
      <script type="text/javascript">
         var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
         (function(){
         var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
         s1.async=true;
         s1.src='https://embed.tawk.to/589e763bbd8b9009f77a2cd2/default';
         s1.charset='UTF-8';
         s1.setAttribute('crossorigin','*');
         s0.parentNode.insertBefore(s1,s0);
         })();
      </script>
      <!--End of Tawk.to Script-->
      <link rel="shortcut icon" href="http://metaldoor.vn/wp-content/themes/betheme/images/favicon.ico" />
      <!-- wp_head() -->
      <title>Công ty thi công nhôm kính Metal Door - Uy tín hàng đầu Việt Nam</title>
      <!-- script | dynamic -->
      <script id="mfn-dnmc-config-js">
         //<![CDATA[
         window.mfn_ajax = "http://metaldoor.vn/wp-admin/admin-ajax.php";
         window.mfn_mobile_init = 1240;
         window.mfn_nicescroll = 40;
         window.mfn_parallax = "translate3d";
         window.mfn_prettyphoto = {style:"pp_default", width:0, height:0};
         window.mfn_sliders = {blog:0, clients:0, offer:0, portfolio:0, shop:0, slider:0, testimonials:0};
         window.mfn_retina_disable = 0;
         //]]>
      </script>
      <!-- This site is optimized with the Yoast SEO Premium plugin v3.2.5 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Công ty thi công nhôm kính Metal Door đi đầu về lĩnh vực tư vấn, thiết kế, sản xuất và lắp đặt hệ thống nhôm kính kiến trúc cho các tòa nhà tại Việt Nam."/>
      <meta name="robots" content="noodp"/>
      <link rel="canonical" href="http://metaldoor.vn/" />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty thi công nhôm kính Metal Door - Uy tín hàng đầu Việt Nam" />
      <meta property="og:description" content="Metal Door công ty hàng đầu về lĩnh vực tư vấn, thiết kế, sản xuất và lắp đặt hệ thống nhôm kính kiến trúc cho các tòa nhà tại Việt Nam." />
      <meta property="og:url" content="http://metaldoor.vn/" />
      <meta property="og:site_name" content="Metal Door" />
      <meta property="og:image" content="http://metaldoor.vn/wp-content/uploads/2015/05/mocku-logo-metaldoor.jpg" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Công ty thi công nhôm kính Metal Door đi đầu về lĩnh vực tư vấn, thiết kế, sản xuất và lắp đặt hệ thống nhôm kính kiến trúc cho các tòa nhà tại Việt Nam." />
      <meta name="twitter:title" content="Công ty thi công nhôm kính Metal Door - Uy tín hàng đầu Việt Nam" />
      <meta name="twitter:image" content="http://metaldoor.vn/wp-content/uploads/2015/05/mocku-logo-metaldoor.jpg" />
      <meta property="DC.date.issued" content="2015-05-04T11:11:10+00:00" />
      <script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"http:\/\/metaldoor.vn\/","name":"Metal Door","potentialAction":{"@type":"SearchAction","target":"http:\/\/metaldoor.vn\/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//platform-api.sharethis.com' />
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="alternate" type="application/rss+xml" title="Metal Door &raquo; Feed" href="http://metaldoor.vn/feed/" />
      <link rel="alternate" type="application/rss+xml" title="Metal Door &raquo; Comments Feed" href="http://metaldoor.vn/comments/feed/" />
      <link rel="alternate" type="application/rss+xml" title="Metal Door &raquo; Công ty thi công nhôm kính Metal Door Comments Feed" href="http://metaldoor.vn/cong-ty-thi-cong-nhom-kinh-metal-door-uy-tin-hang-dau-viet-nam/feed/" />
      <script type="text/javascript">
         window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/metaldoor.vn\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
         !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
      </script>
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel='stylesheet' id='foogallery-core-css'  href='http:css/foogallery.min.css' type='text/css' media='all' />
      <link rel='stylesheet' id='layerslider-css'  href='css/layerslider.css' type='text/css' media='all' />

      <link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />

      <link rel='stylesheet' id='foobox-free-min-css'  href='css/foobox.free.min.css' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='css/settings.css' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         #rs-demo-id {}
      </style>
      <link rel='stylesheet' id='style-css'  href='css/style.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-base-css'  href='css/base.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-layout-css'  href='css/layout.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-shortcodes-css'  href='css/shortcodes.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-animations-css'  href='css/animations.min.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-jquery-ui-css'  href='css/jquery.ui.all.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-prettyPhoto-css'  href='css/prettyPhoto.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-jplayer-css'  href='css/jplayer.blue.monday.css' type='text/css' media='all' />
      <link rel='stylesheet' id='mfn-responsive-css'  href='css/responsive.css' type='text/css' media='all' />

      <link rel='stylesheet' id='mfn-responsive-css'  href='css/bootstrap.min.css' type='text/css' media='all' />

      <link rel='stylesheet' id='mfn-responsive-css'  href='css/bootstrap-grid.min.css' type='text/css' media='all' />

      <link rel='stylesheet' id='Roboto-css'  href='http://fonts.googleapis.com/css?family=Roboto%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='Roboto+Slab-css'  href='http://fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='Lora-css'  href='http://fonts.googleapis.com/css?family=Lora%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />

      <link rel='stylesheet' id='scStyleSheets-css'  href='css/gt-styles.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/greensock.js'></script>
      <script type='text/javascript' src='js/jquery.js'></script>
      <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
      <script type='text/javascript' src='js/layerslider.kreaturamedia.jquery.js'></script>
      <script type='text/javascript' src='js/layerslider.transitions.js'></script>
      <script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
      <script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>
      <script type='text/javascript' src='js/sharethis.js'></script>
      <script type='text/javascript' src='js/foobox.free.min.js'></script>
      <link rel='https://api.w.org/' href='http://metaldoor.vn/wp-json/' />
     
      <!-- style | dynamic -->
      <style id="mfn-dnmc-style-css">
         @media only screen and (min-width: 1240px) {body:not(.header-simple) #Top_bar #menu { display:block !important; }.tr-menu #Top_bar #menu { background:none !important;}#Top_bar .menu > li > ul.mfn-megamenu { width:984px; }#Top_bar .menu > li > ul.mfn-megamenu > li { float:left;}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-1 { width:100%;}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-2 { width:50%;}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-3 { width:33.33%;}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-4 { width:25%;}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-5 { width:20%;}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-6 { width:16.66%;}#Top_bar .menu > li > ul.mfn-megamenu > li > ul { display:block !important; position:inherit; left:auto; top:auto; border-width: 0 1px 0 0; }#Top_bar .menu > li > ul.mfn-megamenu > li:last-child > ul{ border: 0; }#Top_bar .menu > li > ul.mfn-megamenu > li > ul li { width: auto; }#Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title { text-transform: uppercase; font-weight:400; background:none;}#Top_bar .menu > li > ul.mfn-megamenu a .menu-arrow { display: none; }.menuo-right #Top_bar .menu > li > ul.mfn-megamenu { left:auto; right:0;}.menuo-right #Top_bar .menu > li > ul.mfn-megamenu-bg { box-sizing:border-box;}#Top_bar .menu > li > ul.mfn-megamenu-bg { padding:20px 166px 20px 20px; background-repeat:no-repeat; background-position: bottom right; }#Top_bar .menu > li > ul.mfn-megamenu-bg > li { background:none;}#Top_bar .menu > li > ul.mfn-megamenu-bg > li a { border:none;}#Top_bar .menu > li > ul.mfn-megamenu-bg > li > ul { background:none !important;-webkit-box-shadow: 0 0 0 0;-moz-box-shadow: 0 0 0 0;box-shadow: 0 0 0 0;}.header-plain:not(.menuo-right) #Header .top_bar_left { width:auto !important;}.header-stack.header-center #Top_bar #menu { display: inline-block !important;}.header-simple {}.header-simple #Top_bar #menu { display:none; height: auto; width: 300px; bottom: auto; top: 100%; right: 1px; position: absolute; margin: 0px;}.header-simple #Header a.responsive-menu-toggle { display:block; line-height: 35px; font-size: 25px; position:absolute; right: 10px; }.header-simple #Header a:hover.responsive-menu-toggle { text-decoration: none; }.header-simple #Top_bar #menu > ul { width:100%; float: left; }.header-simple #Top_bar #menu ul li { width: 100%; padding-bottom: 0; border-right: 0; position: relative; }.header-simple #Top_bar #menu ul li a { padding:0 20px; margin:0; display: block; height: auto; line-height: normal; border:none; }.header-simple #Top_bar #menu ul li a:after { display:none;}.header-simple #Top_bar #menu ul li a span { border:none; line-height:48px; display:inline; padding:0;}.header-simple #Top_bar #menu ul li.submenu .menu-toggle { display:block; position:absolute; right:0; top:0; width:48px; height:48px; line-height:48px; font-size:30px; text-align:center; color:#d6d6d6; border-left:1px solid #eee; cursor:pointer;}.header-simple #Top_bar #menu ul li.submenu .menu-toggle:after { content:"+"}.header-simple #Top_bar #menu ul li.hover > .menu-toggle:after { content:"-"}.header-simple #Top_bar #menu ul li.hover a { border-bottom: 0; }.header-simple #Top_bar #menu ul.mfn-megamenu li .menu-toggle { display:none;}.header-simple #Top_bar #menu ul li ul { position:relative !important; left:0 !important; top:0; padding: 0; margin-left: 0 !important; width:auto !important; background-image:none;}.header-simple #Top_bar #menu ul li ul li { width:100% !important;}.header-simple #Top_bar #menu ul li ul li a { padding: 0 20px 0 30px;}.header-simple #Top_bar #menu ul li ul li a .menu-arrow { display: none;}.header-simple #Top_bar #menu ul li ul li a span { padding:0;}.header-simple #Top_bar #menu ul li ul li a span:after { display:none !important;}.header-simple #Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title { text-transform: uppercase; font-weight:400;}.header-simple #Top_bar .menu > li > ul.mfn-megamenu > li > ul { display:block !important; position:inherit; left:auto; top:auto;}.header-simple #Top_bar #menu ul li ul li ul { border-left: 0 !important; padding: 0; top: 0; }.header-simple #Top_bar #menu ul li ul li ul li a { padding: 0 20px 0 40px;}.rtl.header-simple#Top_bar #menu { left: 1px; right: auto;}.rtl.header-simple #Top_bar a.responsive-menu-toggle { left:10px; right:auto; }.rtl.header-simple #Top_bar #menu ul li.submenu .menu-toggle { left:0; right:auto; border-left:none; border-right:1px solid #eee;}.rtl.header-simple #Top_bar #menu ul li ul { left:auto !important; right:0 !important;}.rtl.header-simple #Top_bar #menu ul li ul li a { padding: 0 30px 0 20px;}.rtl.header-simple #Top_bar #menu ul li ul li ul li a { padding: 0 40px 0 20px;}.menu-highlight #Top_bar .menu > li { margin: 0 2px; }.menu-highlight:not(.header-creative) #Top_bar .menu > li > a { margin: 20px 0; padding: 0; -webkit-border-radius: 5px; border-radius: 5px; }.menu-highlight #Top_bar .menu > li > a:after { display: none; }.menu-highlight #Top_bar .menu > li > a span:not(.description) { line-height: 50px; }.menu-highlight #Top_bar .menu > li > a span.description { display: none; }.menu-highlight.header-stack #Top_bar .menu > li > a { margin: 10px 0 !important; }.menu-highlight.header-stack #Top_bar .menu > li > a span:not(.description) { line-height: 40px; }.menu-highlight.header-fixed #Top_bar .menu > li > a { margin: 10px 0 !important; padding: 5px 0; }.menu-highlight.header-fixed #Top_bar .menu > li > a span { line-height:30px;}.menu-highlight.header-transparent #Top_bar .menu > li > a { margin: 5px 0; }.menu-highlight.header-simple #Top_bar #menu ul li,.menu-highlight.header-creative #Top_bar #menu ul li { margin: 0; }.menu-highlight.header-simple #Top_bar #menu ul li > a,.menu-highlight.header-creative #Top_bar #menu ul li > a { -webkit-border-radius: 0; border-radius: 0; }.menu-highlight:not(.header-simple) #Top_bar.is-sticky .menu > li > a { margin: 10px 0 !important; padding: 5px 0 !important; }.menu-highlight:not(.header-simple) #Top_bar.is-sticky .menu > li > a span { line-height:30px !important;}.header-modern.menu-highlight.menuo-right .menu_wrapper { margin-right: 20px;}.menu-line-below #Top_bar .menu > li > a:after { top: auto; bottom: -4px; }.menu-line-below #Top_bar.is-sticky .menu > li > a:after { top: auto; bottom: -4px; }.menu-line-below-80 #Top_bar:not(.is-sticky) .menu > li > a:after { height: 4px; left: 10%; top: 50%; margin-top: 20px; width: 80%; } .menu-line-below-80-1 #Top_bar:not(.is-sticky) .menu > li > a:after { height: 1px; left: 10%; top: 50%; margin-top: 20px; width: 80%; }.menu-arrow-top #Top_bar .menu > li > a:after { background: none repeat scroll 0 0 rgba(0, 0, 0, 0) !important; border-color: #cccccc transparent transparent transparent; border-style: solid; border-width: 7px 7px 0 7px; display: block; height: 0; left: 50%; margin-left: -7px; top: 0 !important; width: 0; }.menu-arrow-top.header-transparent #Top_bar .menu > li > a:after,.menu-arrow-top.header-plain #Top_bar .menu > li > a:after { display: none; }.menu-arrow-top #Top_bar.is-sticky .menu > li > a:after { top: 0px !important; }.menu-arrow-bottom #Top_bar .menu > li > a:after { background: none !important; border-color: transparent transparent #cccccc transparent; border-style: solid; border-width: 0 7px 7px; display: block; height: 0; left: 50%; margin-left: -7px; top: auto; bottom: 0; width: 0; }.menu-arrow-bottom.header-transparent #Top_bar .menu > li > a:after,.menu-arrow-bottom.header-plain #Top_bar .menu > li > a:after { display: none; }.menu-arrow-bottom #Top_bar.is-sticky .menu > li > a:after { top: auto; bottom: 0; }.menuo-no-borders #Top_bar .menu > li > a span:not(.description) { border-right-width: 0; }.menuo-no-borders #Header_creative #Top_bar .menu > li > a span { border-bottom-width: 0; }}@media only screen and (min-width: 1240px) {#Top_bar.is-sticky { position:fixed !important; width:100%; left:0; top:-60px; height:60px; z-index:701; background:#fff; opacity:.97; filter: alpha(opacity = 97);-webkit-box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1); -moz-box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1);box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1);}.layout-boxed.header-boxed #Top_bar.is-sticky { max-width:1240px; left:50%; -webkit-transform: translateX(-50%); transform: translateX(-50%);}.layout-boxed.header-boxed.nice-scroll #Top_bar.is-sticky { margin-left:-5px;}#Top_bar.is-sticky .top_bar_left,#Top_bar.is-sticky .top_bar_right,#Top_bar.is-sticky .top_bar_right:before { background:none;}#Top_bar.is-sticky .top_bar_right { top:-4px;}#Top_bar.is-sticky .logo { width:auto; margin: 0 30px 0 20px; padding:0;}#Top_bar.is-sticky #logo { padding:5px 0 !important; height:50px !important; line-height:50px !important;}#Top_bar.is-sticky #logo img:not(.svg) { max-height:35px; width: auto !important;}#Top_bar.is-sticky #logo img.logo-main { display:none;}#Top_bar.is-sticky #logo img.logo-sticky { display:inline;}#Top_bar.is-sticky .menu_wrapper { clear:none;}#Top_bar.is-sticky .menu_wrapper .menu > li > a{ padding:15px 0;}#Top_bar.is-sticky .menu > li > a,#Top_bar.is-sticky .menu > li > a span { line-height:30px;}#Top_bar.is-sticky .menu > li > a:after { top:auto; bottom:-4px;}#Top_bar.is-sticky .menu > li > a span.description { display:none;}#Top_bar.is-sticky a.responsive-menu-toggle { top: 14px;}#Top_bar.is-sticky .top_bar_right_wrapper { top:15px;}.header-plain #Top_bar.is-sticky .top_bar_right_wrapper { top:0;}#Top_bar.is-sticky .secondary_menu_wrapper,#Top_bar.is-sticky .banner_wrapper { display:none;}.header-simple #Top_bar.is-sticky .responsive-menu-toggle { top:12px;}.header-overlay #Top_bar.is-sticky { display:none;}.sticky-dark #Top_bar.is-sticky { background: rgba(0,0,0,.8); }.sticky-dark #Top_bar.is-sticky #menu { background: none; }.sticky-dark #Top_bar.is-sticky .menu > li > a { color: #fff; }.sticky-dark #Top_bar.is-sticky .top_bar_right a { color: rgba(255,255,255,.5); }.sticky-dark #Top_bar.is-sticky .wpml-languages a.active,.sticky-dark #Top_bar.is-sticky .wpml-languages ul.wpml-lang-dropdown { background: rgba(0,0,0,0.3); border-color: rgba(0, 0, 0, 0.1); }}@media only screen and (max-width: 1239px){.header_placeholder { height: 0 !important;}#Top_bar #menu { display:none; height: auto; width: 300px; bottom: auto; top: 100%; right: 1px; position: absolute; margin: 0px;}#Top_bar a.responsive-menu-toggle { display:block; width: 35px; height: 35px; text-align: center; position:absolute; top: 28px; right: 10px; -webkit-border-radius: 3px; border-radius: 3px;}#Top_bar a:hover.responsive-menu-toggle { text-decoration: none;}#Top_bar a.responsive-menu-toggle i { font-size: 25px; line-height: 35px;}#Top_bar a.responsive-menu-toggle span { float:right; padding:10px 5px; line-height:14px;}#Top_bar #menu > ul { width:100%; float: left; }#Top_bar #menu ul li { width: 100%; padding-bottom: 0; border-right: 0; position: relative; }#Top_bar #menu ul li a { padding:0 20px; margin:0; display: block; height: auto; line-height: normal; border:none; }#Top_bar #menu ul li a:after { display:none;}#Top_bar #menu ul li a span { border:none; line-height:48px; display:inline; padding:0;}#Top_bar #menu ul li a span.description { margin:0 0 0 5px;}#Top_bar #menu ul li.submenu .menu-toggle { display:block; position:absolute; right:0; top:0; width:48px; height:48px; line-height:48px; font-size:30px; text-align:center; color:#d6d6d6; border-left:1px solid #eee; cursor:pointer;}#Top_bar #menu ul li.submenu .menu-toggle:after { content:"+"}#Top_bar #menu ul li.hover > .menu-toggle:after { content:"-"}#Top_bar #menu ul li.hover a { border-bottom: 0; }#Top_bar #menu ul li a span:after { display:none !important;} #Top_bar #menu ul.mfn-megamenu li .menu-toggle { display:none;}#Top_bar #menu ul li ul { position:relative !important; left:0 !important; top:0; padding: 0; margin-left: 0 !important; width:auto !important; background-image:none !important;box-shadow: 0 0 0 0 transparent !important; -webkit-box-shadow: 0 0 0 0 transparent !important;}#Top_bar #menu ul li ul li { width:100% !important;}#Top_bar #menu ul li ul li a { padding: 0 20px 0 30px;}#Top_bar #menu ul li ul li a .menu-arrow { display: none;}#Top_bar #menu ul li ul li a span { padding:0;}#Top_bar #menu ul li ul li a span:after { display:none !important;}#Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title { text-transform: uppercase; font-weight:400;}#Top_bar .menu > li > ul.mfn-megamenu > li > ul { display:block !important; position:inherit; left:auto; top:auto;}#Top_bar #menu ul li ul li ul { border-left: 0 !important; padding: 0; top: 0; }#Top_bar #menu ul li ul li ul li a { padding: 0 20px 0 40px;}.rtl #Top_bar #menu { left: 1px; right: auto;}.rtl #Top_bar a.responsive-menu-toggle { left:10px; right:auto; }.rtl #Top_bar #menu ul li.submenu .menu-toggle { left:0; right:auto; border-left:none; border-right:1px solid #eee;}.rtl #Top_bar #menu ul li ul { left:auto !important; right:0 !important;}.rtl #Top_bar #menu ul li ul li a { padding: 0 30px 0 20px;}.rtl #Top_bar #menu ul li ul li ul li a { padding: 0 40px 0 20px;}.header-stack #Top_bar {}.header-stack .menu_wrapper a.responsive-menu-toggle { position: static !important; margin: 11px 0; }.header-stack .menu_wrapper #menu { left: 0; right: auto; }.rtl.header-stack #Top_bar #menu { left: auto; right: 0; }}#Header_wrapper, #Intro {background-color: #f2f2f2;}#Subheader {background-color: rgba(247, 247, 247, 0);}.header-classic #Action_bar, .header-plain #Action_bar, .header-stack #Action_bar {background-color: #2C2C2C;}#Sliding-top {background-color: #545454;}#Sliding-top a.sliding-top-control {border-right-color: #545454;}#Sliding-top.st-center a.sliding-top-control,#Sliding-top.st-left a.sliding-top-control {border-top-color: #545454;}#Footer {background-color: #003b1d;}body, ul.timeline_items, .icon_box a .desc, .icon_box a:hover .desc, .feature_list ul li a, .list_item a, .list_item a:hover,.widget_recent_entries ul li a, .flat_box a, .flat_box a:hover, .story_box .desc, .content_slider.carouselul li a .title,.content_slider.flat.description ul li .desc, .content_slider.flat.description ul li a .desc {color: #000000;}.themecolor, .opening_hours .opening_hours_wrapper li span, .fancy_heading_icon .icon_top,.fancy_heading_arrows .icon-right-dir, .fancy_heading_arrows .icon-left-dir, .fancy_heading_line .title,.button-love a.mfn-love, .format-link .post-title .icon-link, .pager-single > span, .pager-single a:hover,.widget_meta ul, .widget_pages ul, .widget_rss ul, .widget_mfn_recent_comments ul li:after, .widget_archive ul, .widget_recent_comments ul li:after, .widget_nav_menu ul, .woocommerce ul.products li.product .price, .shop_slider .shop_slider_ul li .item_wrapper .price, .woocommerce-page ul.products li.product .price, .widget_price_filter .price_label .from, .widget_price_filter .price_label .to,.woocommerce ul.product_list_widget li .quantity .amount, .woocommerce .product div.entry-summary .price, .woocommerce .star-rating span,#Error_404 .error_pic i, .style-simple #Filters .filters_wrapper ul li a:hover, .style-simple #Filters .filters_wrapper ul li.current-cat a,.style-simple .quick_fact .title {color: #f56711;}.themebg, .pager .pages a:hover, .pager .pages a.active, .pager .pages span.page-numbers.current, .pager-single span:after, #comments .commentlist > li .reply a.comment-reply-link,.fixed-nav .arrow, #Filters .filters_wrapper ul li a:hover, #Filters .filters_wrapper ul li.current-cat a, .widget_categories ul, .Recent_posts ul li .desc:after, .Recent_posts ul li .photo .c,.widget_recent_entries ul li:after, .widget_product_categories ul, div.jp-interface, #Top_bar a#header_cart span,.widget_mfn_menu ul li a:hover, .widget_mfn_menu ul li.current-menu-item:not(.current-menu-ancestor) > a, .widget_mfn_menu ul li.current_page_item:not(.current_page_ancestor) > a,.testimonials_slider .slider_images, .testimonials_slider .slider_images a:after, .testimonials_slider .slider_images:before,.slider_pagination a.selected, .slider_pagination a.selected:after, .tp-bullets.simplebullets.round .bullet.selected, .tp-bullets.simplebullets.round .bullet.selected:after,.tparrows.default, .tp-bullets.tp-thumbs .bullet.selected:after, .offer_thumb .slider_pagination a:before, .offer_thumb .slider_pagination a.selected:after,.style-simple .accordion .question:after, .style-simple .faq .question:after, .style-simple .icon_box .desc_wrapper h4:before,.style-simple #Filters .filters_wrapper ul li a:after, .style-simple .article_box .desc_wrapper p:after, .style-simple .sliding_box .desc_wrapper:after,.style-simple .trailer_box:hover .desc, .woocommerce-account table.my_account_orders .order-number a, .portfolio_group.exposure .portfolio-item .desc-inner .line,.style-simple .zoom_box .desc .desc_txt {background-color: #f56711;}.Latest_news ul li .photo, .style-simple .opening_hours .opening_hours_wrapper li label,.style-simple .timeline_items li:hover h3, .style-simple .timeline_items li:nth-child(even):hover h3, .style-simple .timeline_items li:hover .desc, .style-simple .timeline_items li:nth-child(even):hover,.style-simple .offer_thumb .slider_pagination a.selected {border-color: #f56711;}a {color: #f56711;}a:hover {color: #0076d1;}*::-moz-selection {background-color: #f56711;}*::selection {background-color: #f56711;}.blockquote p.author span, .counter .desc_wrapper .title, .article_box .desc_wrapper p, .team .desc_wrapper p.subtitle, .pricing-box .plan-header p.subtitle, .pricing-box .plan-header .price sup.period, .chart_box p, .fancy_heading .inside,.fancy_heading_line .slogan, .post-meta, .post-meta a, .post-footer, .post-footer a span.label, .pager .pages a, .button-love a .label,.pager-single a, #comments .commentlist > li .comment-author .says, .fixed-nav .desc .date, .filters_buttons li.label, .Recent_posts ul li a .desc .date,.widget_recent_entries ul li .post-date, .tp_recent_tweets .twitter_time, .widget_price_filter .price_label, .shop-filters .woocommerce-result-count,.woocommerce ul.product_list_widget li .quantity, .widget_shopping_cart ul.product_list_widget li dl, .product_meta .posted_in,.woocommerce .shop_table .product-name .variation > dd, .shipping-calculator-button:after,.shop_slider .shop_slider_ul li .item_wrapper .price del,.testimonials_slider .testimonials_slider_ul li .author span, .testimonials_slider .testimonials_slider_ul li .author span a, .Latest_news ul li .desc_footer {color: #575757;}h1, h1 a, h1 a:hover, .text-logo #logo { color: #1a1b1c; }h2, h2 a, h2 a:hover { color: #1a1b1c; }h3, h3 a, h3 a:hover { color: #1a1b1c; }h4, h4 a, h4 a:hover, .style-simple .sliding_box .desc_wrapper h4 { color: #1a1b1c; }h5, h5 a, h5 a:hover { color: #1a1b1c; }h6, h6 a, h6 a:hover, a.content_link .title { color: #1a1b1c; }.dropcap, .highlight:not(.highlight_image) {background-color: #f56711;}a.button, a.tp-button {background-color: #f7f7f7;color: #747474;}.button-stroke a.button, .button-stroke a.button .button_icon i, .button-stroke a.tp-button {border-color: #f7f7f7;color: #747474;}.button-stroke a:hover.button, .button-stroke a:hover.tp-button {background-color: #f7f7f7 !important;color: #fff;}a.button_theme, a.tp-button.button_theme,button, input[type="submit"], input[type="reset"], input[type="button"] {background-color: #f56711;color: #fff;}.button-stroke a.button.button_theme:not(.action_button), .button-stroke a.button.button_theme:not(.action_button),.button-stroke a.button.button_theme .button_icon i, .button-stroke a.tp-button.button_theme,.button-stroke button, .button-stroke input[type="submit"], .button-stroke input[type="reset"], .button-stroke input[type="button"] {border-color: #f56711;color: #f56711 !important;}.button-stroke a.button.button_theme:hover, .button-stroke a.tp-button.button_theme:hover,.button-stroke button:hover, .button-stroke input[type="submit"]:hover, .button-stroke input[type="reset"]:hover, .button-stroke input[type="button"]:hover {background-color: #f56711 !important;color: #fff !important;}a.mfn-link { color: #656B6F; }a.mfn-link-2 span, a:hover.mfn-link-2 span:before, a.hover.mfn-link-2 span:before, a.mfn-link-5 span, a.mfn-link-8:after, a.mfn-link-8:before { background: #f56711; }a:hover.mfn-link { color: #f56711;}a.mfn-link-2 span:before, a:hover.mfn-link-4:before, a:hover.mfn-link-4:after, a.hover.mfn-link-4:before, a.hover.mfn-link-4:after, a.mfn-link-5:before, a.mfn-link-7:after, a.mfn-link-7:before { background: #0076d1; }a.mfn-link-6:before {border-bottom-color: #0076d1;}.woocommerce a.button, .woocommerce .quantity input.plus, .woocommerce .quantity input.minus {background-color: #f7f7f7 !important;color: #747474 !important;}.woocommerce a.button_theme, .woocommerce a.checkout-button, .woocommerce button.button,.woocommerce .button.add_to_cart_button, .woocommerce .button.product_type_external,.woocommerce input[type="submit"], .woocommerce input[type="reset"], .woocommerce input[type="button"],.button-stroke .woocommerce a.checkout-button {background-color: #f56711 !important;color: #fff !important;}.column_column ul, .column_column ol, .the_content_wrapper ul, .the_content_wrapper ol {color: #737E86;}.hr_color, .hr_color hr, .hr_dots span {color: #f56711;background: #f56711;}.hr_zigzag i {color: #f56711;} .highlight-left:after,.highlight-right:after {background: #f56711;}@media only screen and (max-width: 767px) {.highlight-left .wrap:first-child,.highlight-right .wrap:last-child {background: #f56711;}}#Header .top_bar_left, .header-classic #Top_bar, .header-plain #Top_bar, .header-stack #Top_bar, .header-split #Top_bar,.header-fixed #Top_bar, .header-below #Top_bar, #Header_creative, #Top_bar #menu, .sticky-tb-color #Top_bar.is-sticky {background-color: #f2f2f2;}#Top_bar .top_bar_right:before {background-color: #f2f2f2;}#Header .top_bar_right {background-color: #f5f5f5;}#Top_bar .top_bar_right a { color: #017f40;}#Top_bar .menu > li > a { color: #017f40;}#Top_bar .menu > li.current-menu-item > a,#Top_bar .menu > li.current_page_item > a,#Top_bar .menu > li.current-menu-parent > a,#Top_bar .menu > li.current-page-parent > a,#Top_bar .menu > li.current-menu-ancestor > a,#Top_bar .menu > li.current-page-ancestor > a,#Top_bar .menu > li.current_page_ancestor > a,#Top_bar .menu > li.hover > a { color: #f56711; }#Top_bar .menu > li a:after { background: #f56711; }.menuo-arrows #Top_bar .menu > li.submenu > a > span:not(.description)::after { border-top-color: #017f40;}#Top_bar .menu > li.current-menu-item.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current_page_item.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-menu-parent.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-page-parent.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-menu-ancestor.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-page-ancestor.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current_page_ancestor.submenu > a > span:not(.description)::after,#Top_bar .menu > li.hover.submenu > a > span:not(.description)::after { border-top-color: #f56711; }.menu-highlight #Top_bar #menu > ul > li.current-menu-item > a,.menu-highlight #Top_bar #menu > ul > li.current_page_item > a,.menu-highlight #Top_bar #menu > ul > li.current-menu-parent > a,.menu-highlight #Top_bar #menu > ul > li.current-page-parent > a,.menu-highlight #Top_bar #menu > ul > li.current-menu-ancestor > a,.menu-highlight #Top_bar #menu > ul > li.current-page-ancestor > a,.menu-highlight #Top_bar #menu > ul > li.current_page_ancestor > a,.menu-highlight #Top_bar #menu > ul > li.hover > a { background: #f7f7f7; }.menu-arrow-bottom #Top_bar .menu > li > a:after { border-bottom-color: #f56711;}.menu-arrow-top #Top_bar .menu > li > a:after {border-top-color: #f56711;}.header-plain #Top_bar .menu > li.current-menu-item > a,.header-plain #Top_bar .menu > li.current_page_item > a,.header-plain #Top_bar .menu > li.current-menu-parent > a,.header-plain #Top_bar .menu > li.current-page-parent > a,.header-plain #Top_bar .menu > li.current-menu-ancestor > a,.header-plain #Top_bar .menu > li.current-page-ancestor > a,.header-plain #Top_bar .menu > li.current_page_ancestor > a,.header-plain #Top_bar .menu > li.hover > a,.header-plain #Top_bar a:hover#header_cart,.header-plain #Top_bar a:hover#search_button,.header-plain #Top_bar .wpml-languages:hover,.header-plain #Top_bar .wpml-languages ul.wpml-lang-dropdown {background: #f7f7f7; color: #f56711;}.header-plain #Top_bar,.header-plain #Top_bar .menu > li > a span:not(.description),.header-plain #Top_bar a#header_cart,.header-plain #Top_bar a#search_button,.header-plain #Top_bar .wpml-languages,.header-plain #Top_bar a.button.action_button {border-color: #F2F2F2;}#Top_bar .menu > li ul {background-color: #F2F2F2;}#Top_bar .menu > li ul li a {color: #5f5f5f;}#Top_bar .menu > li ul li a:hover,#Top_bar .menu > li ul li.hover > a {color: #2e2e2e;}#Top_bar .search_wrapper { background: #f56711; }.overlay-menu-toggle {color: #2991d6 !important; }#Overlay {background: rgba(245, 103, 17, 0.95);}#overlay-menu ul li a, .header-overlay .overlay-menu-toggle.focus {color: #ffffff;}#overlay-menu ul li.current-menu-item > a,#overlay-menu ul li.current_page_item > a,#overlay-menu ul li.current-menu-parent > a,#overlay-menu ul li.current-page-parent > a,#overlay-menu ul li.current-menu-ancestor > a,#overlay-menu ul li.current-page-ancestor > a,#overlay-menu ul li.current_page_ancestor > a { color: #B1DCFB; }#Top_bar .responsive-menu-toggle {color: #2991d6; }#Subheader .title{color: #017f40;}#Subheader ul.breadcrumbs li, #Subheader ul.breadcrumbs li a{color: rgba(1, 127, 64, 0.6);}#Footer, #Footer .widget_recent_entries ul li a {color: #ffffff;}#Footer a {color: #ffba00;}#Footer a:hover {color: #ffffff;}#Footer h1, #Footer h1 a, #Footer h1 a:hover,#Footer h2, #Footer h2 a, #Footer h2 a:hover,#Footer h3, #Footer h3 a, #Footer h3 a:hover,#Footer h4, #Footer h4 a, #Footer h4 a:hover,#Footer h5, #Footer h5 a, #Footer h5 a:hover,#Footer h6, #Footer h6 a, #Footer h6 a:hover {color: #ffffff;}#Footer .themecolor, #Footer .widget_meta ul, #Footer .widget_pages ul, #Footer .widget_rss ul, #Footer .widget_mfn_recent_comments ul li:after, #Footer .widget_archive ul, #Footer .widget_recent_comments ul li:after, #Footer .widget_nav_menu ul, #Footer .widget_price_filter .price_label .from, #Footer .widget_price_filter .price_label .to,#Footer .star-rating span {color: #ffba00;}#Footer .themebg, #Footer .widget_categories ul, #Footer .Recent_posts ul li .desc:after, #Footer .Recent_posts ul li .photo .c,#Footer .widget_recent_entries ul li:after, #Footer .widget_mfn_menu ul li a:hover, #Footer .widget_product_categories ul {background-color: #ffba00;}#Footer .Recent_posts ul li a .desc .date, #Footer .widget_recent_entries ul li .post-date, #Footer .tp_recent_tweets .twitter_time, #Footer .widget_price_filter .price_label, #Footer .shop-filters .woocommerce-result-count, #Footer ul.product_list_widget li .quantity, #Footer .widget_shopping_cart ul.product_list_widget li dl {color: #ffffff;}#Sliding-top, #Sliding-top .widget_recent_entries ul li a {color: #cccccc;}#Sliding-top a {color: #f56711;}#Sliding-top a:hover {color: #007eb2;}#Sliding-top h1, #Sliding-top h1 a, #Sliding-top h1 a:hover,#Sliding-top h2, #Sliding-top h2 a, #Sliding-top h2 a:hover,#Sliding-top h3, #Sliding-top h3 a, #Sliding-top h3 a:hover,#Sliding-top h4, #Sliding-top h4 a, #Sliding-top h4 a:hover,#Sliding-top h5, #Sliding-top h5 a, #Sliding-top h5 a:hover,#Sliding-top h6, #Sliding-top h6 a, #Sliding-top h6 a:hover {color: #ffffff;}#Sliding-top .themecolor, #Sliding-top .widget_meta ul, #Sliding-top .widget_pages ul, #Sliding-top .widget_rss ul, #Sliding-top .widget_mfn_recent_comments ul li:after, #Sliding-top .widget_archive ul, #Sliding-top .widget_recent_comments ul li:after, #Sliding-top .widget_nav_menu ul, #Sliding-top .widget_price_filter .price_label .from, #Sliding-top .widget_price_filter .price_label .to,#Sliding-top .star-rating span {color: #f56711;}#Sliding-top .themebg, #Sliding-top .widget_categories ul, #Sliding-top .Recent_posts ul li .desc:after, #Sliding-top .Recent_posts ul li .photo .c,#Sliding-top .widget_recent_entries ul li:after, #Sliding-top .widget_mfn_menu ul li a:hover, #Sliding-top .widget_product_categories ul {background-color: #f56711;}#Sliding-top .Recent_posts ul li a .desc .date, #Sliding-top .widget_recent_entries ul li .post-date, #Sliding-top .tp_recent_tweets .twitter_time, #Sliding-top .widget_price_filter .price_label, #Sliding-top .shop-filters .woocommerce-result-count, #Sliding-top ul.product_list_widget li .quantity, #Sliding-top .widget_shopping_cart ul.product_list_widget li dl {color: #575757;}blockquote, blockquote a, blockquote a:hover {color: #444444;}.image_frame .image_wrapper .image_links,.portfolio_group.masonry-hover .portfolio-item .masonry-hover-wrapper .hover-desc { background: rgba(245, 103, 17, 0.8);}.masonry.tiles .post-item .post-desc-wrapper .post-desc .post-title:after, .masonry.tiles .post-item.no-img, .masonry.tiles .post-item.format-quote {background: #f56711;} .image_frame .image_wrapper .image_links a {color: #ffffff;}.image_frame .image_wrapper .image_links a:hover {background: #ffffff;color: #f56711;}.sliding_box .desc_wrapper {background: #f56711;}.sliding_box .desc_wrapper:after {border-bottom-color: #f56711;}.counter .icon_wrapper i {color: #f56711;}.quick_fact .number-wrapper {color: #f56711;}.progress_bars .bars_list li .bar .progress { background-color: #f56711;}a:hover.icon_bar {color: #0076d1 !important;}a.content_link, a:hover.content_link {color: #f56711;}a.content_link:before {border-bottom-color: #f56711;}a.content_link:after {border-color: #f56711;}.get_in_touch, .infobox {background-color: #f56711;}.column_map .google-map-contact-wrapper .get_in_touch:after {border-top-color: #f56711;}.timeline_items li h3:before,.timeline_items:after,.timeline .post-item:before { border-color: #f56711;}.how_it_works .image .number { background: #f56711;}.trailer_box .desc .subtitle {background-color: #f56711;}.icon_box .icon_wrapper, .icon_box a .icon_wrapper,.style-simple .icon_box:hover .icon_wrapper {color: #f56711;}.icon_box:hover .icon_wrapper:before, .icon_box a:hover .icon_wrapper:before { background-color: #f56711;}ul.clients.clients_tiles li .client_wrapper:hover:before { background: #f56711;}ul.clients.clients_tiles li .client_wrapper:after { border-bottom-color: #f56711;}.list_item.lists_1 .list_left {background-color: #f56711;}.list_item .list_left {color: #f56711;}.feature_list ul li .icon i { color: #f56711;}.feature_list ul li:hover,.feature_list ul li:hover a {background: #f56711;}.ui-tabs .ui-tabs-nav li.ui-state-active a,.accordion .question.active .title > .acc-icon-plus,.accordion .question.active .title > .acc-icon-minus,.faq .question.active .title > .acc-icon-plus,.faq .question.active .title,.accordion .question.active .title {color: #f56711;}.ui-tabs .ui-tabs-nav li.ui-state-active a:after {background: #f56711;}body.table-hover:not(.woocommerce-page) table tr:hover td {background: #f56711;}.pricing-box .plan-header .price sup.currency,.pricing-box .plan-header .price > span {color: #f56711;}.pricing-box .plan-inside ul li .yes { background: #f56711;}.pricing-box-box.pricing-box-featured {background: #0076d1;}input[type="date"], input[type="email"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="url"],select, textarea, .woocommerce .quantity input.qty {color: #626262;background-color: rgba(255, 255, 255, 1);border-color: #EBEBEB;}input[type="date"]:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="text"]:focus, input[type="url"]:focus, select:focus, textarea:focus {color: #575757;background-color: rgba(255, 238, 195, 1) !important;border-color: #efd081;}.woocommerce span.onsale, .shop_slider .shop_slider_ul li .item_wrapper span.onsale {border-top-color: #f56711 !important;}.woocommerce .widget_price_filter .ui-slider .ui-slider-handle {border-color: #f56711 !important;}@media only screen and (min-width: 768px){.header-semi #Top_bar:not(.is-sticky) {background-color: rgba(242, 242, 242, 0.8);}}@media only screen and (max-width: 767px){#Top_bar, #Action_bar { background: #f2f2f2 !important;}}html { background-color: #FCFCFC;}#Wrapper, #Content { background-color: #FCFCFC;}body:not(.template-slider) #Header_wrapper { background-image: url("http://metaldoor.vn/wp-content/uploads/2015/05/brg02.jpg"); }body, button, span.date_label, .timeline_items li h3 span, input[type="submit"], input[type="reset"], input[type="button"],input[type="text"], input[type="password"], input[type="tel"], input[type="email"], textarea, select, .offer_li .title h3 {font-family: "Roboto", Arial, Tahoma, sans-serif;font-weight: 400;}#menu > ul > li > a, .action_button, #overlay-menu ul li a {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 400;}#Subheader .title {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 400;}h1, .text-logo #logo {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 300;}h2 {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 300;}h3 {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 300;}h4 {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 300;}h5 {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 700;}h6 {font-family: "Roboto Slab", Arial, Tahoma, sans-serif;font-weight: 400;}blockquote {font-family: "Lora", Arial, Tahoma, sans-serif;}.chart_box .chart .num, .counter .desc_wrapper .number-wrapper, .how_it_works .image .number,.pricing-box .plan-header .price, .quick_fact .number-wrapper, .woocommerce .product div.entry-summary .price {font-family: "Arial", Arial, Tahoma, sans-serif;}body {font-size: 15px;line-height: 23px;}#menu > ul > li > a, .action_button {font-size: 14px;}#Subheader .title {font-size: 30px;line-height: 30px;}h1, .text-logo #logo { font-size: 30px;line-height: 30px;}h2 { font-size: 25px;line-height: 25px;}h3 {font-size: 22px;line-height: 24px;}h4 {font-size: 18px;line-height: 22px;}h5 {font-size: 16px;line-height: 21px;}h6 {font-size: 13px;line-height: 20px;}#Intro .intro-title { font-size: 70px;line-height: 70px;}@media only screen and (min-width: 768px) and (max-width: 959px){body {font-size: 13px;line-height: 20px;}#menu > ul > li > a {font-size: 13px;}#Subheader .title {font-size: 26px;line-height: 26px;}h1, .text-logo #logo {font-size: 26px;line-height: 26px;}h2 {font-size: 21px;line-height: 21px;}h3 {font-size: 19px;line-height: 21px;}h4 {font-size: 15px;line-height: 19px;}h5 {font-size: 14px;line-height: 18px;}h6 {font-size: 13px;line-height: 19px;}#Intro .intro-title { font-size: 60px;line-height: 60px;}blockquote { font-size: 15px;}.chart_box .chart .num { font-size: 45px; line-height: 45px; }.counter .desc_wrapper .number-wrapper { font-size: 45px; line-height: 45px;}.counter .desc_wrapper .title { font-size: 14px; line-height: 18px;}.faq .question .title { font-size: 14px; }.fancy_heading .title { font-size: 38px; line-height: 38px; }.offer .offer_li .desc_wrapper .title h3 { font-size: 32px; line-height: 32px; }.offer_thumb_ul li.offer_thumb_li .desc_wrapper .title h3 {font-size: 32px; line-height: 32px; }.pricing-box .plan-header h2 { font-size: 27px; line-height: 27px; }.pricing-box .plan-header .price > span { font-size: 40px; line-height: 40px; }.pricing-box .plan-header .price sup.currency { font-size: 18px; line-height: 18px; }.pricing-box .plan-header .price sup.period { font-size: 14px; line-height: 14px;}.quick_fact .number { font-size: 80px; line-height: 80px;}.trailer_box .desc h2 { font-size: 27px; line-height: 27px; }}@media only screen and (min-width: 480px) and (max-width: 767px){body {font-size: 13px;line-height: 20px;}#menu > ul > li > a {font-size: 13px;}#Subheader .title {font-size: 23px;line-height: 23px;}h1, .text-logo #logo {font-size: 23px;line-height: 23px;}h2 {font-size: 19px;line-height: 19px;}h3 {font-size: 17px;line-height: 19px;}h4 {font-size: 14px;line-height: 18px;}h5 {font-size: 13px;line-height: 17px;}h6 {font-size: 13px;line-height: 18px;}#Intro .intro-title { font-size: 53px;line-height: 53px;}blockquote { font-size: 14px;}.chart_box .chart .num { font-size: 40px; line-height: 40px; }.counter .desc_wrapper .number-wrapper { font-size: 40px; line-height: 40px;}.counter .desc_wrapper .title { font-size: 13px; line-height: 16px;}.faq .question .title { font-size: 13px; }.fancy_heading .title { font-size: 34px; line-height: 34px; }.offer .offer_li .desc_wrapper .title h3 { font-size: 28px; line-height: 28px; }.offer_thumb_ul li.offer_thumb_li .desc_wrapper .title h3 {font-size: 28px; line-height: 28px; }.pricing-box .plan-header h2 { font-size: 24px; line-height: 24px; }.pricing-box .plan-header .price > span { font-size: 34px; line-height: 34px; }.pricing-box .plan-header .price sup.currency { font-size: 16px; line-height: 16px; }.pricing-box .plan-header .price sup.period { font-size: 13px; line-height: 13px;}.quick_fact .number { font-size: 70px; line-height: 70px;}.trailer_box .desc h2 { font-size: 24px; line-height: 24px; }}@media only screen and (max-width: 479px){body {font-size: 13px;line-height: 20px;}#menu > ul > li > a {font-size: 13px;}#Subheader .title {font-size: 18px;line-height: 18px;}h1, .text-logo #logo {font-size: 18px;line-height: 18px;}h2 { font-size: 15px;line-height: 15px;}h3 {font-size: 13px;line-height: 15px;}h4 {font-size: 13px;line-height: 16px;}h5 {font-size: 13px;line-height: 16px;}h6 {font-size: 13px;line-height: 17px;}#Intro .intro-title { font-size: 42px;line-height: 42px;}blockquote { font-size: 13px;}.chart_box .chart .num { font-size: 35px; line-height: 35px; }.counter .desc_wrapper .number-wrapper { font-size: 35px; line-height: 35px;}.counter .desc_wrapper .title { font-size: 13px; line-height: 26px;}.faq .question .title { font-size: 13px; }.fancy_heading .title { font-size: 30px; line-height: 30px; }.offer .offer_li .desc_wrapper .title h3 { font-size: 26px; line-height: 26px; }.offer_thumb_ul li.offer_thumb_li .desc_wrapper .title h3 {font-size: 26px; line-height: 26px; }.pricing-box .plan-header h2 { font-size: 21px; line-height: 21px; }.pricing-box .plan-header .price > span { font-size: 32px; line-height: 32px; }.pricing-box .plan-header .price sup.currency { font-size: 14px; line-height: 14px; }.pricing-box .plan-header .price sup.period { font-size: 13px; line-height: 13px;}.quick_fact .number { font-size: 60px; line-height: 60px;}.trailer_box .desc h2 { font-size: 21px; line-height: 21px; }}.with_aside .sidebar.columns {width: 23%;}.with_aside .sections_group {width: 77%;}.aside_both .sidebar.columns {width: 18%;}.aside_both .sidebar.sidebar-1{ margin-left: -82%;}.aside_both .sections_group {width: 64%;margin-left: 18%;}@media only screen and (min-width:1240px){#Wrapper, .with_aside .content_wrapper {max-width: 1240px;}.section_wrapper, .container {max-width: 1220px;}.layout-boxed.header-boxed #Top_bar.is-sticky{max-width: 1240px;}}#Top_bar #logo,.header-fixed #Top_bar #logo,.header-plain #Top_bar #logo,.header-transparent #Top_bar #logo {height: 60px;line-height: 60px;padding: 5px 0;}#Top_bar .menu > li > a {padding: 5px 0;}.menu-highlight:not(.header-creative) #Top_bar .menu > li > a {margin: 10px 0;}.header-plain:not(.menu-highlight) #Top_bar .menu > li > a span:not(.description) {line-height: 70px;}.header-fixed #Top_bar .menu > li > a {padding: 20px 0;}#Top_bar .top_bar_right,.header-plain #Top_bar .top_bar_right {height: 70px;}#Top_bar .top_bar_right_wrapper { top: 15px;}.header-plain #Top_bar a#header_cart, .header-plain #Top_bar a#search_button,.header-plain #Top_bar .wpml-languages,.header-plain #Top_bar a.button.action_button {line-height: 70px;}#Top_bar a.responsive-menu-toggle,.header-plain #Top_bar a.responsive-menu-toggle,.header-transparent #Top_bar a.responsive-menu-toggle { top: 18px;}.twentytwenty-before-label::before { content: "Before";}.twentytwenty-after-label::before { content: "After";}dl, ol, ul {  margin-top: 0; margin-bottom: 0;}
      </style>
      <!-- style | custom css | theme options -->
      <style id="mfn-dnmc-theme-css">
         h3:after { content: ""; display: block; width: 30px; height: 3px; background: #f1991b; margin-top: 15px; }
         .align_center h3:after { margin: 15px auto 0; }
         h5 { font-weight: 400; }
         /* Top bar */
         #Top_bar { top: 20px; }
         /* Subheader */
         body:not(.template-slider) #Header { min-height: 200px; }
         #Subheader { padding: 0 0 200px }
         @media only screen and (max-width: 767px)  {
         body:not(.template-slider):not(.header-simple) #Header { min-height: 200px; }
         #Subheader { padding: 0 0 100px }
         }
         /* Tabs */
         .ui-tabs .ui-tabs-panel { padding: 90px  8% 0 !important; }
         .ui-tabs .ui-tabs-nav { border-color: #cfcfcf !important; }
         .ui-tabs .ui-tabs-nav li a { font-size: 18px; padding: 20px 30px !important; font-weight: 400; letter-spacing: 2px; color: #84878a; }
         /* Read more */
         a.read_more { font-family: Montserrat; letter-spacing: 2px; font-weight: 700; background: url(http://phikha.com/wp-content/uploads/2015/05/home_callcenter_arrow_right.png) no-repeat right 2px; padding-right: 45px; }
         /* Footer links */
         ul.footer_links { font-size: 13px; margin-left: 7%; }
         ul.footer_links li { list-style-image: url("http://phikha.com/wp-content/uploads/2015/05/home_callcenter_footer_links.png"); padding: 5px 0 5px 5px; }
         /* Contact icons */
         .contact_icons { line-height: 30px; }
         .contact_icons span { position: relative; top: -4px; margin-right: 5px; }
         .contact_icons a { font-size: 24px; }
         /* Footer */
         #Footer .widgets_wrapper .widget h4 { font-size: 15px; line-height: 15px; }
         #Footer .widgets_wrapper { padding: 70px 0 60px; }
         #Footer .footer_copy { background: none; border-top: 1px solid rgba(0, 0, 0, 0.1); }
      </style>
      <!--[if lt IE 9]>
      <script id="mfn-html5" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      <!-- script | retina -->
      <script id="mfn-dnmc-retina-js">
         //<![CDATA[
         jQuery(window).load(function(){
         var retina = window.devicePixelRatio > 1 ? true : false;if( retina ){var retinaEl = jQuery("#logo img.logo-main");var retinaLogoW = retinaEl.width();var retinaLogoH = retinaEl.height();retinaEl.attr( "src", "http://metaldoor.vn/wp-content/uploads/2016/12/metaldoor-vietnam.png" ).width( retinaLogoW ).height( retinaLogoH );var stickyEl = jQuery("#logo img.logo-sticky");var stickyLogoW = stickyEl.width();var stickyLogoH = stickyEl.height();stickyEl.attr( "src", "http://metaldoor.vn/wp-content/uploads/2016/12/metaldoor-vietnam.png" ).width( stickyLogoW ).height( stickyLogoH );var mobileEl = jQuery("#logo img.logo-mobile");var mobileLogoW = mobileEl.width();var mobileLogoH = mobileEl.height();mobileEl.attr( "src", "http://metaldoor.vn/wp-content/uploads/2016/12/metaldoor-vietnam.png" ).width( mobileLogoW ).height( mobileLogoH );}});
         //]]>
      </script>
     
   </head>
   <!-- body -->
   <body class="home page-template-default page page-id-2324 template-slider  color-custom style-simple layout-full-width nice-scroll-on mobile-tb-left button-stroke no-content-padding header-classic sticky-header sticky-white ab-hide subheader-both-left menu-line-below-80">
      <!-- mfn_hook_top --><!-- mfn_hook_top -->   
      <!-- #Wrapper -->
      <div id="Wrapper">
         <!-- #Header_bg -->
         <div id="Header_wrapper"  style="background-image:url(http://metaldoor.vn/wp-content/uploads/2016/07/000.png);" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
           

            <header id="Header">
               <?php include('header/header.php')?>
               <?php include('slider/slider.php')?>
            </header>
         </div>
         <!-- mfn_hook_content_before --><!-- mfn_hook_content_before -->  
         <!-- #Content -->
           <div id="Content">
            <div class="content_wrapper clearfix">
               <!-- .sections_group -->
               <div class="sections_group">
                  <div class="entry-content" itemprop="mainContentOfPage">
                     <div class="section mcb-section   "  style="padding-top:20px; padding-bottom:0px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <h2 class="title"><i class="icon-right-dir"></i>MẶT DỰNG NHÔM KÍNH<i class="icon-left-dir"></i></h2>
                                       <div class="inside">
                                          <h4>
                                          Hệ thống mặt dựng nhôm kính là kết cấu bao che của công trình. Mặt dựng kính Metaldoor thiết kế đạt tiêu chuẩn Châu Âu cách nhiệt, kín khít, giá tốt. Đặc điểm hệ mặt dựng nhôm kính:
                                          -> Tùy theo thiết kế kiến trúc của công trình, mặt dựng nhôm kính được thiết kế có hoặc không có nan nhôm trang trí mặt ngoài 
                                          -> Cách âm, cách nhiệt, bảo đảm ánh sáng tự nhiên
                                          -> Đa dạng kiểu dáng, phù hợp với nhiều dạng kiến trúc
                                          -> Đạt tiêu chuẩn châu Âu về cách nhiệt, độ kín khít (chống gió, nước, không khí)
                                          -> Thi công đơn giản và gọn nhẹ
                                          -> Tải trọng nhẹ, khả năng chịu lực tốt và độ bền cao
                                          -> Kinh tế trong sử dụng
                                          -> Tính thẩm mỹ cao: Có thể trang trí theo ý muốn nhờ sự đa dạng của nhôm và kính
                                          <h4>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wrap mcb-wrap one  column-margin-40px valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one-third column_flat_box ">
                                    <div class="flat_box">
                                       <div class="animate" data-anim-type="zoomIn">
                                          <a href="http://metaldoor.vn/mat-dung-kinh-he-stick/" >
                                             <div class="photo_wrapper">
                                                <div class="icon themebg" ><i class="icon-database-line"></i></div>
                                                <img class="scale-with-grid" src="http://metaldoor.vn/wp-content/uploads/2016/07/mat-dung-kinh-stick-1.jpg" alt="mat-dung-kinh-stick-1" width="800" height="600"/>
                                             </div>
                                             <div class="desc_wrapper">
                                                <h4>Mặt dựng kính hệ Stick</h4>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-third column_flat_box ">
                                    <div class="flat_box">
                                       <div class="animate" data-anim-type="zoomIn">
                                          <a href="#" >
                                             <div class="photo_wrapper">
                                                <div class="icon themebg" ><i class="icon-network"></i></div>
                                                <img class="scale-with-grid" src="http://phikha.com/wp-content/uploads/2016/07/matdung5.jpg" alt="matdung5" width="700" height="525"/>
                                             </div>
                                             <div class="desc_wrapper">
                                                <h4>Mặt dựng kính hệ Unitized</h4>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-third column_flat_box ">
                                    <div class="flat_box">
                                       <div class="animate" data-anim-type="zoomIn">
                                          <a href="#" >
                                             <div class="photo_wrapper">
                                                <div class="icon themebg" ><i class="icon-network"></i></div>
                                                <img class="scale-with-grid" src="http://phikha.com/wp-content/uploads/2016/07/matdung3.jpg" alt="matdung3" width="800" height="600"/>
                                             </div>
                                             <div class="desc_wrapper">
                                                <h4>Mặt dựng kính hệ Semi</h4>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section mcb-section   "  style="padding-top:50px; padding-bottom:0px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <h2 class="title"><i class="icon-right-dir"></i>ƯU ĐIỂM HỆ THỐNG MẶT DỰNG KÍNH<i class="icon-left-dir"></i></h2>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-fourth column_icon_box ">
                                    <div class="animate" data-anim-type="fadeIn">
                                       <div class="icon_box icon_position_top no_border">
                                          <div class="icon_wrapper">
                                             <div class="icon"><i class="icon-paper-plane-line"></i></div>
                                          </div>
                                          <div class="desc_wrapper">
                                             <h4>Tầm nhìn không bị hạn chế</h4>
                                             <div class="desc">Đảm bảo ánh sáng tự nhiên</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-fourth column_icon_box ">
                                    <div class="animate" data-anim-type="fadeIn">
                                       <div class="icon_box icon_position_top no_border">
                                          <div class="icon_wrapper">
                                             <div class="icon"><i class="icon-cd-line"></i></div>
                                          </div>
                                          <div class="desc_wrapper">
                                             <h4>Không gian tươi mới</h4>
                                             <div class="desc">Qua lăng kính cường lực bền, chắc và an toàn.</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-fourth column_icon_box ">
                                    <div class="animate" data-anim-type="fadeIn">
                                       <div class="icon_box icon_position_top no_border">
                                          <div class="icon_wrapper">
                                             <div class="icon"><i class="icon-network"></i></div>
                                          </div>
                                          <div class="desc_wrapper">
                                             <h4>Thiết kế sang trọng</h4>
                                             <div class="desc">Giúp không gian thân thiện với môi trường nhất có thể.</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-fourth column_icon_box ">
                                    <div class="animate" data-anim-type="fadeIn">
                                       <div class="icon_box icon_position_top no_border">
                                          <div class="icon_wrapper">
                                             <div class="icon"><i class="icon-thumbs-up-line"></i></div>
                                          </div>
                                          <div class="desc_wrapper">
                                             <h4>Hiện đại, sang trọng</h4>
                                             <div class="desc">Từ các công trình sử dụng mặt dựng nhôm kính</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one column_visual ">
                                    <p style="text-align: justify;">Hệ thống mặt dựng nhôm kính sử dụng vật liệu kính có độ bền, tính chịu lực, màu sắc, kiểu dáng với độ dày chỉ giới hạn một mức độ nhất định. Với công nghệ hiện đại, người ta có thể làm tăng độ cứng, tính chịu đựng của kính để tạo thành kính cường lực hoặc dán một lớp phim ở giữa hai lớp kính để có độ an toàn cao vì khi có sự cố xảy ra, lớp phim này nhằm giảm nhiều mảnh nhọn hiểm vì nó đã bám dính phần lớn vào lớp phim. Ban có thể chọn làm kính có màu để hạn chế ánh sáng và nhiệt lượng hấp thụ, Với kỹ thuật tiến bộ thì kính vừa được nhuộm màu vừa được tráng một lớp phản quang để giảm bớt lượng ánh sáng và nhiệt độ vào trong nhà..</p>
                                 </div>
                                 <div class="column mcb-column one-third column_zoom_box ">
                                    <div class="zoom_box">
                                       <div class="photo"><img class="scale-with-grid" src="http://phikha.com/wp-content/uploads/2016/07/dongnhat01.jpg" alt="dongnhat01" width="350" height="350"/></div>
                                       <div class="desc" style="background-color:rgba(0, 0, 0, 0.8);">
                                          <div class="desc_wrap"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-third column_zoom_box ">
                                    <div class="zoom_box">
                                       <div class="photo"><img class="scale-with-grid" src="http://phikha.com/wp-content/uploads/2016/07/dongnhat02.jpg" alt="dongnhat02" width="350" height="350"/></div>
                                       <div class="desc" style="background-color:rgba(0, 0, 0, 0.8);">
                                          <div class="desc_wrap"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-third column_zoom_box ">
                                    <div class="zoom_box">
                                       <div class="photo"><img class="scale-with-grid" src="http://phikha.com/wp-content/uploads/2016/07/dongnhat03.jpg" alt="dongnhat03" width="350" height="350"/></div>
                                       <div class="desc" style="background-color:rgba(0, 0, 0, 0.8);">
                                          <div class="desc_wrap"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section the_content no_content">
                        <div class="section_wrapper">
                           <div class="the_content_wrapper"></div>
                        </div>
                     </div>
                     <div class="section section-page-footer">
                        <div class="section_wrapper clearfix">
                           <div class="column one page-pager">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
            </div>
         </div>
         


         <!-- mfn_hook_content_after --><!-- mfn_hook_content_after -->
         <!-- #Footer -->     
         <?php include('footer/footer.php')?>
      </div>
      <!-- #Wrapper -->
      <!-- mfn_hook_bottom --><!-- mfn_hook_bottom -->
      <!-- wp_footer() -->
      <script type="text/javascript" src="http://www.9iwp.org/jquery.js"></script><script>
         jQuery(document).ready(function () {
         jQuery.post('http://metaldoor.vn?ga_action=googleanalytics_get_script', {action: 'googleanalytics_get_script'}, function(response) {
         var F = new Function ( response );
         return( F() );
         });
         });
      </script>
      <script type="text/javascript">
         function revslider_showDoubleJqueryError(sliderID) {
         
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
         
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
         
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
         
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
         
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
         
               jQuery(sliderID).show().html(errorMessage);
         
         }
         
      </script>
      <script type='text/javascript' src='js/core.min.js'></script>
      <script type='text/javascript' src='js/widget.min.js'></script>
      <script type='text/javascript' src='js/mouse.min.js'></script>
      <script type='text/javascript' src='js/sortable.min.js'></script>
      <script type='text/javascript' src='js/tabs.min.js'></script>
      <script type='text/javascript' src='js/accordion.min.js'></script>
      <script type='text/javascript' src='js/plugins.js'></script>
      <script type='text/javascript' src='js/menu.js'></script>
      <script type='text/javascript' src='js/animations.min.js'></script>
      <script type='text/javascript' src='js/jplayer.min.js'></script>
      <script type='text/javascript' src='js/translate3d.js'></script>
      <script type='text/javascript' src='js/scripts.js'></script>
      <script type='text/javascript' src='js/comment-reply.min.js'></script>
      <script type='text/javascript' src='js/wp-embed.min.js'></script>
      <script type='text/javascript' src='js/foogallery.min.js'></script>

      
      <script type='text/javascript' src='js/bootstrap.min.js'></script> 
      <script type='text/javascript' src='js/bootstrap.bundle.min.js'></script>

      <script type="text/foobox">/* Run FooBox FREE (v2.5.2) */
         (function( FOOBOX, $, undefined ) {
           FOOBOX.o = {wordpress: { enabled: true }, countMessage:'image %index of %total', excludes:'.fbx-link,.nofoobox,.nolightbox,a[href*="pinterest.com/pin/create/button/"]', affiliate : { enabled: false }};
           FOOBOX.init = function() {
             $(".fbx-link").removeClass("fbx-link");
             $(".foogallery-container.foogallery-lightbox-foobox, .foogallery-container.foogallery-lightbox-foobox-free, .gallery, .wp-caption, a:has(img[class*=wp-image-]), .foobox").foobox(FOOBOX.o);
           };
         }( window.FOOBOX = window.FOOBOX || {}, FooBox.$ ));
         
         FooBox.ready(function() {
         
           jQuery("body").append("<span style=\"font-family:'foobox'; color:transparent; position:absolute; top:-1000em;\">f</span>");
           FOOBOX.init();
           jQuery('body').on('post-load', function(){ FOOBOX.init(); });
         
         });
      </script>            <script type="text/javascript">
         if (window.addEventListener){
            window.addEventListener("DOMContentLoaded", function() {
               var arr = document.querySelectorAll("script[type='text/foobox']");
               for (var x = 0; x < arr.length; x++) {
                  var script = document.createElement("script");
                  script.type = "text/javascript";
                  script.innerHTML = arr[x].innerHTML;
                  arr[x].parentNode.replaceChild(script, arr[x]);
               }
            });
         } else {
            console.log("FooBox does not support the current browser.");
         }
      </script>
   </body>
</html>