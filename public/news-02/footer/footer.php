 </div>
            <div
               id="footer-wrapper">
               <div
                  id="footer" class="row">
                  <div
                     id="column1" class="footer-column section">
                     <div
                        id="HTML17" class="widget HTML">
                        <div
                           class="widget-title">
                           <h2 class="title">Liên hệ</h2>
                        </div>
                        <div
                           class="widget-content">
                           <ul
                              class="post-widget">
                              <li>
                                 <address>
                                    <p
                                       style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; font-family: Helvetica, Arial, sans-serif; font-size: 11.9999990463257px; line-height: 23.9999980926514px;">C&Ocirc;NG TY TNHH B&Aacute;N LẺ NHANH</p>
                                    <p
                                       style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; font-family: Helvetica, Arial, sans-serif; font-size: 11.9999990463257px; line-height: 23.9999980926514px;">Trụ sở ch&iacute;nh: T&ograve;a nh&agrave; V&acirc;n Hồ, 51 L&ecirc; Đại H&agrave;nh, Hai B&agrave; Trưng, H&agrave; Nội</p>
                                    <p
                                       style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; font-family: Helvetica, Arial, sans-serif; font-size: 11.9999990463257px; line-height: 23.9999980926514px;">Hồ Ch&iacute; Minh: Tầng 4 - Số 70 Lữ Gia Plaza - Phường 15 - Quận 11</p>
                                    <p
                                       style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; font-family: Helvetica, Arial, sans-serif; font-size: 11.9999990463257px; line-height: 23.9999980926514px;">Hotline: 1900.2008</p>
                                    <p
                                       style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; font-family: Helvetica, Arial, sans-serif; font-size: 11.9999990463257px; line-height: 23.9999980926514px;">Email: contact@nhanh.vn</p>
                                 </address>
                              </li>
                           </ul>
                           <div
                              class="clear"></div>
                        </div>
                        <div
                           class="clear"></div>
                     </div>
                  </div>
                  <div
                     id="column2" class="footer-column section">
                     <div
                        id="HTML16" class="widget HTML">
                        <div
                           class="widget-title">
                           <h2 class="title">Về chúng tôi</h2>
                        </div>
                        <div
                           class="widget-content">
                           <ul
                              class="post-widget">
                              <li>
                                 <p
                                    style="line-height: 20.7999992370605px;"><a
                                    href="https://nhanh.vn/" target="_blank">Li&ecirc;n hệ&nbsp;</a></p>
                                 <p
                                    style="line-height: 20.7999992370605px;"><a
                                    href="http://t0073.store.nhanh.vn/news/5073-nhanh.vn-phan-mem-quan-ly-ban-hang-thiet-ke-website--dich-vu-giao-hang-chuyen-nghiep" target="_blank">Giới thiệu&nbsp;</a></p>
                              </li>
                           </ul>
                           <div
                              class="clear"></div>
                        </div>
                        <div
                           class="clear"></div>
                     </div>
                  </div>
                  <div
                     id="column3" class="footer-column section mostView">
                     <div
                        id="HTML18" class="widget HTML">
                        <div
                           class="widget-title">
                           <h2 class="title">Tin tức xem nhiều</h2>
                        </div>
                        <div
                           class="widget-content">
                           <ul
                              class="post-widget">
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574782_839.jpg) no-repeat center center;background-size: cover"
                                    href="/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html">Đội tuyển Việt Nam: 84 ngày tập trung, 6 trận vòng loại World Cup</a></h3>
                                    <span
                                       class="recent-date">09-04-2015</span>
                                 </div>
                              </li>
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428631404_309.jpg) no-repeat center center;background-size: cover"
                                    href="/7-kieu-giay-can-co-trong-tu-do-cua-co-nang-tuoi-20-n4990.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/7-kieu-giay-can-co-trong-tu-do-cua-co-nang-tuoi-20-n4990.html">7 kiểu giày cần có trong tủ đồ của cô nàng tuổi 20</a></h3>
                                    <span
                                       class="recent-date">10-04-2015</span>
                                 </div>
                              </li>
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428634846_17.jpg) no-repeat center center;background-size: cover"
                                    href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html">Cảnh giác những lời hoa mỹ của nhân viên môi giới nhà đất</a></h3>
                                    <span
                                       class="recent-date">10-04-2015</span>
                                 </div>
                              </li>
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428715141_640.jpg) no-repeat center center;background-size: cover"
                                    href="/nguoi-nhac-cong-vo-danh-n5082.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/nguoi-nhac-cong-vo-danh-n5082.html">Người nhạc công vô danh</a></h3>
                                    <span
                                       class="recent-date">11-04-2015</span>
                                 </div>
                              </li>
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641175_406.jpg) no-repeat center center;background-size: cover"
                                    href="/nhung-quy-tac-ung-xu-ban-phai-biet-khi-an-tiec-n5018.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/nhung-quy-tac-ung-xu-ban-phai-biet-khi-an-tiec-n5018.html">Những quy tắc ứng xử bạn phải biết khi ăn tiệc</a></h3>
                                    <span
                                       class="recent-date">10-04-2015</span>
                                 </div>
                              </li>
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428649993_676.jpg) no-repeat center center;background-size: cover"
                                    href="/5-cach-chac-chan-giup-tang-thoi-luong-pin-tren-laptop-n5034.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/5-cach-chac-chan-giup-tang-thoi-luong-pin-tren-laptop-n5034.html">5 cách chắc chắn giúp tăng thời lượng pin trên laptop</a></h3>
                                    <span
                                       class="recent-date">10-04-2015</span>
                                 </div>
                              </li>
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428655442_843.jpg) no-repeat center center;background-size: cover"
                                    href="/tuyen-sinh-di-hoc-tai-hunggari-nam-2015-n5048.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/tuyen-sinh-di-hoc-tai-hunggari-nam-2015-n5048.html">Tuyển sinh đi học tại Hung-ga-ri năm 2015 </a></h3>
                                    <span
                                       class="recent-date">10-04-2015</span>
                                 </div>
                              </li>
                              <li>
                                 <a
                                    style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428657424_239.jpg) no-repeat center center;background-size: cover"
                                    href="/my-la-diem-sang-trong-buc-tranh-kinh-te-toan-cau-2015-n5062.html"
                                    class="rcp-thumb show-with"></a>
                                 <div
                                    class="post-panel">
                                    <h3 class="rcp-title"><a
                                       href="/my-la-diem-sang-trong-buc-tranh-kinh-te-toan-cau-2015-n5062.html">Mỹ là 'điểm sáng' trong bức tranh kinh tế toàn cầu 2015</a></h3>
                                    <span
                                       class="recent-date">10-04-2015</span>
                                 </div>
                              </li>
                           </ul>
                           <div
                              class="clear"></div>
                        </div>
                        <div
                           class="clear"></div>
                     </div>
                  </div>
               </div>
               <a
                  href="tel:0908257968" id="call-mobile"><i
                  class="fa fa-phone"></i></a>
            </div>
            <div
               id="copyrights">
               <a
                  href="#top" class="upbt"><i
                  class="fa fa-angle-up"></i></a>
               <div
                  class="copyrights row">
                  <div
                     class="copy-left" data-runtime="Memory: 2 mb - Time: 0,22s" style="display: block">
                     Thiết kế web bởi <a
                        href="http://localhost/tintucbmag/"><img
                        src="/images/favicon.ico" alt="" title=""> Nhanh.vn</a>
                  </div>
               </div>
            </div>