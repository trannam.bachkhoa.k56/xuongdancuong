<!DOCTYPE html>
<html
   lang="vi-VN" data-nhanh.vn-template="T0073">
   <head>
      <meta
         name="robots" content="noindex" />
      <meta
         name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
      <meta
         charset="utf-8">
      <meta
         content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
      <title>Thể thao</title>
       <base href="http://t0073.store.nhanh.vn/" target="_blank, _self, _parent, _top">
      <meta
         property="og:image" content="">
      <meta
         name="description" content="Shopping online">
      <meta
         property="og:image" content="">
      <meta
         name="description" content="Shopping online">
      <meta
         name="keywords" content="Shopping online">
      <link
         href="//cdn.nhanh.vn/cdn/store/2558/store_1428717578_761.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/tintuc.css" type="text/css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/font-awesome.min.css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/style.css">
   </head>
   <body>
      <div
         id="pages-wrapper" class="index home">
         <div
            id="top"></div>
         <div
            id="outer-wrapper">
            <!-- HEADER -->
           <?php include('header/header.php')?>
            <div
               class="content-wrapper">
               <div
                  id="content-wrapper" class="row">
                  <div
                     id="ticker" class="ticker section">
                     <div
                        id="HTML20" class="widget HTML">
                        <h2 class="title"><i
                           class="fa fa-thumb-tack"></i>Tin tức</h2>
                        <div
                           class="layout-content">
                           <div
                              class="tickercontainer">
                              <div
                                 class="mask">
                                 <ul
                                    class="newsticker">
                                    <li>
                                       <a
                                          class="post-tag" href="/the-thao-nc958.html">Thể thao</a>
                                       <h3 class="recent-title"><a
                                          href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html">HLV Miura chỉ hứa đưa U23 Việt Nam vào bán kết SEA Games</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/thoi-trang-nc1249.html">Thời trang</a>
                                       <h3 class="recent-title"><a
                                          href="/bi-quyet-phoi-do-voi-mau-do-ruou-hot-nhat-mua-xuan-2015-n4991.html">Bí quyết phối đồ với màu đỏ rượu hot nhất mùa xuân 2015</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/an-ninh-xa-hoi-nc1247.html">An ninh Xã hội</a>
                                       <h3 class="recent-title"><a
                                          href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html">Cảnh giác những lời hoa mỹ của nhân viên môi giới nhà đất</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/giai-tri-nc1244.html">Giải trí</a>
                                       <h3 class="recent-title"><a
                                          href="/nguoi-nhac-cong-vo-danh-n5082.html">Người nhạc công vô danh</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/am-thuc-nc1248.html">Ẩm thực</a>
                                       <h3 class="recent-title"><a
                                          href="/van-hoa-bia-n5021.html">Văn hóa bia</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/cong-nghe-nc946.html">Công nghệ</a>
                                       <h3 class="recent-title"><a
                                          href="/huong-dan-su-dung-laptop-dung-cach-n5081.html">HƯỚNG DẪN SỬ DỤNG LAPTOP ĐÚNG CÁCH</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/giao-duc-nc957.html">Giáo dục </a>
                                       <h3 class="recent-title"><a
                                          href="/quy-che-tuyen-sinh-2015-cho-he-dai-hoc-cao-dang-n5052.html">Quy chế tuyển sinh 2015 cho hệ đại học, cao đẳng</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/kinh-doanh-nc955.html">Kinh doanh</a>
                                       <h3 class="recent-title"><a
                                          href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html">Elon Musk, người muốn thay đổi tương lai nhân loại</a></h3>
                                    </li>
                                 </ul>
                                 <span
                                    class="tickeroverlay-left">&nbsp;</span><span
                                    class="tickeroverlay-right">&nbsp;</span>
                              </div>
                           </div>
                        </div>
                        <div
                           class="clear"></div>
                        <span
                           class="widget-item-control">
                        <span
                           class="item-control blog-admin">
                        <a
                           title="Edit" target="configHTML20" href="/" class="quickedit"><img
                           width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                        </span>
                        </span>
                        <div
                           class="clear"></div>
                     </div>
                  </div>
                  <h1 class="hidden">Thể thao</h1>
                  <div
                     id="main-wrapper">
                     <div
                        id="main" class="main section">
                        <div
                           id="Blog1" class="widget Blog">
                           <div
                              class="blog-posts hfeed">
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574898_986.jpg) no-repeat center center;background-size:cover"
                                          href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html">HLV Miura chỉ hứa đưa U23 Việt Nam vào bán kết SEA Games</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574782_839.jpg) no-repeat center center;background-size:cover"
                                          href="/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html">Đội tuyển Việt Nam: 84 ngày tập trung, 6 trận vòng loại World Cup</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574555_102.jpg) no-repeat center center;background-size:cover"
                                          href="/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html">Ngô Hoàng Thịnh lỡ dịp đối đầu Công Phượng</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574384_796.jpg) no-repeat center center;background-size:cover"
                                          href="/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html">Top 5 bản hợp đồng đắt giá nhất trong lịch sử Ligue 1</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574282_732.jpg) no-repeat center center;background-size:cover"
                                          href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html">Đây! Bằng chứng cho thấy Cavani sắp là người của M.U</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574178_745.jpg) no-repeat center center;background-size:cover"
                                          href="/ibrahimovic-can-moc-100-ban-cho-psg-ga-khung-dang-yeu-n4962.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/ibrahimovic-can-moc-100-ban-cho-psg-ga-khung-dang-yeu-n4962.html">Ibrahimovic cán mốc 100 bàn cho PSG: “Gã khùng” đáng yêu</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/ibrahimovic-can-moc-100-ban-cho-psg-ga-khung-dang-yeu-n4962.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/ibrahimovic-can-moc-100-ban-cho-psg-ga-khung-dang-yeu-n4962.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/ibrahimovic-can-moc-100-ban-cho-psg-ga-khung-dang-yeu-n4962.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/ibrahimovic-can-moc-100-ban-cho-psg-ga-khung-dang-yeu-n4962.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428573921_592.jpg) no-repeat center center;background-size:cover"
                                          href="/pep-guardiola-khong-phai-ke-ham-tien-n4961.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/pep-guardiola-khong-phai-ke-ham-tien-n4961.html">Pep Guardiola không phải kẻ hám tiền</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/pep-guardiola-khong-phai-ke-ham-tien-n4961.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/pep-guardiola-khong-phai-ke-ham-tien-n4961.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/pep-guardiola-khong-phai-ke-ham-tien-n4961.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/pep-guardiola-khong-phai-ke-ham-tien-n4961.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428573819_858.jpg) no-repeat center center;background-size:cover"
                                          href="/dortmund-sap-co-bien-dai-phau-luc-luong-n4960.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/dortmund-sap-co-bien-dai-phau-luc-luong-n4960.html">Dortmund sắp có biến: Đại phẫu lực lượng</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/dortmund-sap-co-bien-dai-phau-luc-luong-n4960.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/dortmund-sap-co-bien-dai-phau-luc-luong-n4960.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/dortmund-sap-co-bien-dai-phau-luc-luong-n4960.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/dortmund-sap-co-bien-dai-phau-luc-luong-n4960.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428573716_342.jpg) no-repeat center center;background-size:cover"
                                          href="/thomas-mueller-bayern-khong-bao-gio-biet-met-n4959.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/thomas-mueller-bayern-khong-bao-gio-biet-met-n4959.html">Thomas Mueller: ‘Bayern không bao giờ biết mệt’</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/thomas-mueller-bayern-khong-bao-gio-biet-met-n4959.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/thomas-mueller-bayern-khong-bao-gio-biet-met-n4959.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/thomas-mueller-bayern-khong-bao-gio-biet-met-n4959.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/thomas-mueller-bayern-khong-bao-gio-biet-met-n4959.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428573600_64.jpg) no-repeat center center;background-size:cover"
                                          href="/nong-xac-dinh-tuong-lai-cua-falcao-n4958.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/nong-xac-dinh-tuong-lai-cua-falcao-n4958.html">Nóng: Xác định tương lai của Falcao</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/nong-xac-dinh-tuong-lai-cua-falcao-n4958.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/nong-xac-dinh-tuong-lai-cua-falcao-n4958.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/nong-xac-dinh-tuong-lai-cua-falcao-n4958.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/nong-xac-dinh-tuong-lai-cua-falcao-n4958.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428573509_673.jpg) no-repeat center center;background-size:cover"
                                          href="/ngay-ca-juventus-va-roma-cung-them-khat-salah-n4957.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/ngay-ca-juventus-va-roma-cung-them-khat-salah-n4957.html">Ngay cả Juventus và Roma cũng thèm khát Salah</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/ngay-ca-juventus-va-roma-cung-them-khat-salah-n4957.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/ngay-ca-juventus-va-roma-cung-them-khat-salah-n4957.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/ngay-ca-juventus-va-roma-cung-them-khat-salah-n4957.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/ngay-ca-juventus-va-roma-cung-them-khat-salah-n4957.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428573362_530.jpg) no-repeat center center;background-size:cover"
                                          href="/juventus-vao-chung-ket-coppa-italia-keo-dai-mong-chinh-phat-n4956.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/juventus-vao-chung-ket-coppa-italia-keo-dai-mong-chinh-phat-n4956.html">Juventus vào chung kết Coppa Italia: Kéo dài mộng chinh phạt</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/juventus-vao-chung-ket-coppa-italia-keo-dai-mong-chinh-phat-n4956.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/juventus-vao-chung-ket-coppa-italia-keo-dai-mong-chinh-phat-n4956.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/juventus-vao-chung-ket-coppa-italia-keo-dai-mong-chinh-phat-n4956.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/juventus-vao-chung-ket-coppa-italia-keo-dai-mong-chinh-phat-n4956.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428573258_569.jpg) no-repeat center center;background-size:cover"
                                          href="/paulo-dybala-hay-nho-premier-league-chang-phai-thien-duong-n4955.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/paulo-dybala-hay-nho-premier-league-chang-phai-thien-duong-n4955.html">Paulo Dybala: Hãy nhớ Premier League chẳng phải thiên đường</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-04-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/paulo-dybala-hay-nho-premier-league-chang-phai-thien-duong-n4955.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/paulo-dybala-hay-nho-premier-league-chang-phai-thien-duong-n4955.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/paulo-dybala-hay-nho-premier-league-chang-phai-thien-duong-n4955.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/paulo-dybala-hay-nho-premier-league-chang-phai-thien-duong-n4955.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1425887368_586.png) no-repeat center center;background-size:cover"
                                          href="/liverpool-blackburn-gai-hong-kho-be-n3680.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/liverpool-blackburn-gai-hong-kho-be-n3680.html">Liverpool - Blackburn: "Gai hồng" khó bẻ</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-03-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/liverpool-blackburn-gai-hong-kho-be-n3680.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/liverpool-blackburn-gai-hong-kho-be-n3680.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/liverpool-blackburn-gai-hong-kho-be-n3680.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/liverpool-blackburn-gai-hong-kho-be-n3680.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1425887113_200.jpg) no-repeat center center;background-size:cover"
                                          href="/van-gaal-tinh-buong-fa-cup-wenger-van-bi-giggs-am-anh-n3679.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/van-gaal-tinh-buong-fa-cup-wenger-van-bi-giggs-am-anh-n3679.html">Van Gaal tính buông FA cup, Wenger vẫn bị Giggs “ám ảnh”</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-03-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/van-gaal-tinh-buong-fa-cup-wenger-van-bi-giggs-am-anh-n3679.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/van-gaal-tinh-buong-fa-cup-wenger-van-bi-giggs-am-anh-n3679.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/van-gaal-tinh-buong-fa-cup-wenger-van-bi-giggs-am-anh-n3679.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/van-gaal-tinh-buong-fa-cup-wenger-van-bi-giggs-am-anh-n3679.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1425886772_190.jpg) no-repeat center center;background-size:cover"
                                          href="/mu-arsenal-thien-duong-vay-goi-n3677.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/mu-arsenal-thien-duong-vay-goi-n3677.html">MU - Arsenal: Thiên đường vẫy gọi</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-03-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/mu-arsenal-thien-duong-vay-goi-n3677.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/mu-arsenal-thien-duong-vay-goi-n3677.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/mu-arsenal-thien-duong-vay-goi-n3677.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/mu-arsenal-thien-duong-vay-goi-n3677.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                              <div
                                 class="post-outer post-category">
                                 <div
                                    class="post">
                                    <div
                                       class="post-thumb">
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1425886231_785.jpg) no-repeat center center;background-size:cover"
                                          href="/barca-vuot-real-m10-bat-kip-cr7-the-cuc-xoay-van-n3676.html"></a>
                                    </div>
                                    <div
                                       class="post-header"></div>
                                    <article>
                                       <div
                                          class="post-home">
                                          <div
                                             class="post-info">
                                             <h2 class="post-title">
                                                <a
                                                   href="/barca-vuot-real-m10-bat-kip-cr7-the-cuc-xoay-van-n3676.html">Barca vượt Real, M10 bắt kịp CR7: Thế cục xoay vần</a>
                                             </h2>
                                             <div
                                                class="post-meta">
                                                <span
                                                   class="post-author vcard">
                                                <i
                                                   class="fa fa-user"></i>
                                                <span
                                                   class="fn">
                                                <a
                                                   title="author profile"class="g-profile" data-gapiscan="true"
                                                   data-onload="true" data-gapiattached="true">
                                                <span></span>
                                                </a>
                                                </span>
                                                </span>
                                                <span
                                                   class="post-timestamp">
                                                <i
                                                   class="fa fa-clock-o"></i>
                                                <a
                                                   href="" class="timestamp-link"><abbr
                                                   class="published timeago">09-03-2015</abbr></a>
                                                </span>
                                             </div>
                                             <div
                                                class="post-snippet"></div>
                                             <div
                                                id="post-foot">
                                                <div
                                                   class="post-readmore"><a
                                                   href="/barca-vuot-real-m10-bat-kip-cr7-the-cuc-xoay-van-n3676.html">Đọc thêm<i
                                                   class="fa fa-long-arrow-right"></i></a></div>
                                                <div
                                                   class="share-container">
                                                   <div
                                                      class="post-sharebtn"><i
                                                      class="fa fa-share-alt"></i> Share</div>
                                                   <div
                                                      class="post-share">
                                                      <ul>
                                                         <li><a
                                                            title="Twitter Tweet" target="_blank" rel="nofollow"
                                                            href="http://twitter.com/home?status=t0073.store.nhanh.vn/barca-vuot-real-m10-bat-kip-cr7-the-cuc-xoay-van-n3676.html"
                                                            data-via="SweethemeDotCom" class="twitter"><i
                                                            class="fa fa-twitter"></i>Tweet</a></li>
                                                         <li><a
                                                            title="Facebook Share" target="_blank" rel="nofollow"
                                                            onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                            href="http://www.facebook.com/share.php? v=4&amp;src=bm&amp;u=t0073.store.nhanh.vn/barca-vuot-real-m10-bat-kip-cr7-the-cuc-xoay-van-n3676.html"
                                                            class="share"><i
                                                            class="fa fa-facebook"></i>Facebook</a></li>
                                                         <li><a
                                                            title="Google Plus Share" target="_blank" rel="nofollow"
                                                            href="https://plus.google.com/share?url=t0073.store.nhanh.vn/barca-vuot-real-m10-bat-kip-cr7-the-cuc-xoay-van-n3676.html"
                                                            class="plus"><i
                                                            class="fa fa-google-plus"></i>Google+</a></li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </article>
                                    <div
                                       class="post-footer"></div>
                                 </div>
                              </div>
                           </div>
                           <div
                              class="pagenavi">
                              <div
                                 class="paginator"><span
                                 class="labelPages">1 - 17 / 17</span><span
                                 class="titlePages">&nbsp;&nbsp;Trang: </span></div>
                           </div>
                        </div>
                        <div
                           id="HTML901" class="widget HTML"></div>
                        <div
                           id="HTML902" class="widget HTML"></div>
                     </div>
                  </div>
                  <!-- BLOCK -->
                    <?php include('block/sideright.php')?>
      

               </div>
            </div>
             <!-- FOOTER -->
           <?php include('footer/footer.php')?>
         </div>
         <input
            type="hidden" id="checkStoreId" value="2558">
      </div>
       <script type="text/javascript" src="http://localhost/tintucbmag/js/tintuc.js"></script>
      <script type="text/javascript" src="http://localhost/tintucbmag/js/main.js"></script> 
      <div
         id="fb-root"></div>
      <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script> 
      <div
         style="display: none;">
         <div
            id="dMsg"></div>
      </div>
   </body>
</html>