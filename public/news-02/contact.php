<!DOCTYPE html>
<html
   lang="vi-VN" data-nhanh.vn-template="T0073">
   <head>
      <meta
         name="robots" content="noindex" />
      <meta
         name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
      <meta
         charset="utf-8">
      <meta
         content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
      <title>Thể thao</title>
       <base href="http://t0073.store.nhanh.vn/" target="_blank, _self, _parent, _top">
      <meta
         property="og:image" content="">
      <meta
         name="description" content="Shopping online">
      <meta
         property="og:image" content="">
      <meta
         name="description" content="Shopping online">
      <meta
         name="keywords" content="Shopping online">
      <link
         href="//cdn.nhanh.vn/cdn/store/2558/store_1428717578_761.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/tintuc.css" type="text/css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/font-awesome.min.css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/style.css">
   </head>
   <body>
      <div
         id="pages-wrapper" class="index home">
         <div
            id="top"></div>
         <div
            id="outer-wrapper">
            <!-- HEADER -->
           <?php include('header/header.php')?>
            

            <div
               class="content-wrapper">
               <div
                  class="formContact" style="max-width: 600px; margin: 0px auto;">
                  <h1 style="font-size: 18px;text-align: center;padding: 10px 0;">Liên hệ với t0073.store.nhanh.vn</h1>
                  <form
                     id="fContact" action="" method="post">
                     <label>
                     <span>Họ và tên</span>
                     <input
                        type="text" name="name" value=""/>
                     </label>
                     <label>
                     <span>Email</span>
                     <input
                        type="text" name="email" value=""/>
                     </label>
                     <label>
                     <span>Điện thoại</span>
                     <input
                        type="text" name="mobile" value=""/>
                     </label>
                     <label>
                     <span>Địa chỉ</span>
                     <input
                        type="text" name="address" value=""/>
                     </label>
                     <label>
                     <span>Nội dung liên hệ</span><textarea name="content"></textarea></label>
                     <p>
                        <button
                           type="submit">Gửi</button>
                     </p>
                  </form>
               </div>
               <style type="text/css">form#fContact{
                  width: 600px; margin: 20px auto;
                  }
                  form#fContact label{
                  display: block; margin: 12px 0;
                  }
                  form#fContact label span{
                  float: left; width: 150px;
                  }
                  form#fContact label input{
                  display: inline-block; width: 300px; height: 28px; border: 1px solid #ccc;
                  padding: 0 5px; box-sizing: border-box;
                  }
                  form#fContact label textarea{
                  display: inline-block; width: 400px; height: 150px; resize: none; box-sizing: border-box;
                  border: 1px solid #ccc; padding: 5px;
                  }
                  form#fContact button[type="submit"]{
                  display: inline-block; padding: 5px 40px; border: none; background-color: #19749B; color: #fff;
                  margin-left: 150px;
                  }
                  form#fContact #showErrorContact{
                  border: 1px dashed #F04E23; margin-bottom: 20px; color: #F04E23; padding: 2px 5px; text-align: center;
                  }
                  form#fContact #showSuccessContact{
                  border: 1px dashed #008000; margin-bottom: 20px; color: #008000; padding: 2px 5px; text-align: center;
                  }
               </style>
            </div>
             <!-- FOOTER -->
           <?php include('footer/footer.php')?>
         </div>
         <input
            type="hidden" id="checkStoreId" value="2558">
      </div>
       <script type="text/javascript" src="http://localhost/tintucbmag/js/tintuc.js"></script>
      <script type="text/javascript" src="http://localhost/tintucbmag/js/main.js"></script> 
      <div
         id="fb-root"></div>
      <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script> 
      <div
         style="display: none;">
         <div
            id="dMsg"></div>
      </div>
   </body>
</html>