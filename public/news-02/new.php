<!DOCTYPE html>
<html
   lang="vi-VN" data-nhanh.vn-template="T0073">
   <head>
      <meta
         name="robots" content="noindex" />
      <meta
         name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
      <meta
         charset="utf-8">
      <meta
         content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
      <title>Thể thao</title>
       <base href="http://t0073.store.nhanh.vn/" target="_blank, _self, _parent, _top">
      <meta
         property="og:image" content="">
      <meta
         name="description" content="Shopping online">
      <meta
         property="og:image" content="">
      <meta
         name="description" content="Shopping online">
      <meta
         name="keywords" content="Shopping online">
      <link
         href="//cdn.nhanh.vn/cdn/store/2558/store_1428717578_761.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/tintuc.css" type="text/css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/font-awesome.min.css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/style.css">
   </head>
   <body>
      <div
         id="pages-wrapper" class="index home">
         <div
            id="top"></div>
         <div
            id="outer-wrapper">
            <!-- HEADER -->
           <?php include('header/header.php')?>
            

            <div
               class="content-wrapper">
               <div
                  id="content-wrapper" class="row">
                  <div
                     id="ticker" class="ticker section">
                     <div
                        id="HTML20" class="widget HTML">
                        <h2 class="title"><i
                           class="fa fa-thumb-tack"></i>Tin tức</h2>
                        <div
                           class="layout-content">
                           <div
                              class="tickercontainer">
                              <div
                                 class="mask">
                                 <ul
                                    class="newsticker">
                                    <li>
                                       <a
                                          class="post-tag" href="/the-thao-nc958.html">Thể thao</a>
                                       <h3 class="recent-title"><a
                                          href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html">HLV Miura chỉ hứa đưa U23 Việt Nam vào bán kết SEA Games</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/thoi-trang-nc1249.html">Thời trang</a>
                                       <h3 class="recent-title"><a
                                          href="/bi-quyet-phoi-do-voi-mau-do-ruou-hot-nhat-mua-xuan-2015-n4991.html">Bí quyết phối đồ với màu đỏ rượu hot nhất mùa xuân 2015</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/an-ninh-xa-hoi-nc1247.html">An ninh Xã hội</a>
                                       <h3 class="recent-title"><a
                                          href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html">Cảnh giác những lời hoa mỹ của nhân viên môi giới nhà đất</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/giai-tri-nc1244.html">Giải trí</a>
                                       <h3 class="recent-title"><a
                                          href="/nguoi-nhac-cong-vo-danh-n5082.html">Người nhạc công vô danh</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/am-thuc-nc1248.html">Ẩm thực</a>
                                       <h3 class="recent-title"><a
                                          href="/van-hoa-bia-n5021.html">Văn hóa bia</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/cong-nghe-nc946.html">Công nghệ</a>
                                       <h3 class="recent-title"><a
                                          href="/huong-dan-su-dung-laptop-dung-cach-n5081.html">HƯỚNG DẪN SỬ DỤNG LAPTOP ĐÚNG CÁCH</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/giao-duc-nc957.html">Giáo dục </a>
                                       <h3 class="recent-title"><a
                                          href="/quy-che-tuyen-sinh-2015-cho-he-dai-hoc-cao-dang-n5052.html">Quy chế tuyển sinh 2015 cho hệ đại học, cao đẳng</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/kinh-doanh-nc955.html">Kinh doanh</a>
                                       <h3 class="recent-title"><a
                                          href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html">Elon Musk, người muốn thay đổi tương lai nhân loại</a></h3>
                                    </li>
                                 </ul>
                                 <span
                                    class="tickeroverlay-left">&nbsp;</span><span
                                    class="tickeroverlay-right">&nbsp;</span>
                              </div>
                           </div>
                        </div>
                        <div
                           class="clear"></div>
                        <span
                           class="widget-item-control">
                        <span
                           class="item-control blog-admin">
                        <a
                           title="Edit" target="configHTML20" href="/" class="quickedit"><img
                           width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                        </span>
                        </span>
                        <div
                           class="clear"></div>
                     </div>
                  </div>
                  <div
                     id="main-wrapper">
                     <div
                        id="main" class="main section">
                        <div
                           id="HTML900" class="widget HTML">
                           <div
                              class="ad-inside">
                              <a
                                 href=""><img
                                 src="/tp/T0073/images/bmag.png"></a>
                           </div>
                        </div>
                        <div
                           id="Blog1" class="widget Blog">
                           <div
                              class="blog-posts hfeed">
                              <div
                                 class="post-outer">
                                 <div
                                    class="post">
                                    <div>
                                       <div
                                          class="post-header">
                                          <div
                                             class="breadcrumbs">
                                             <span><a
                                                href="/" class="bhome">Trang chủ</a></span><i
                                                class="fa fa-angle-right"></i>
                                             <span>
                                          </div>
                                          <div
                                             class="post-heading">
                                             <h1 class="post-title entry-title news-title">
                                                HLV Miura chỉ hứa đưa U23 Việt Nam vào bán kết SEA Games
                                             </h1>
                                          </div>
                                          <div
                                             class="post-meta"><span
                                             class="post-timestamp">
                                             <i
                                                class="fa fa-clock-o"></i><a
                                                title="permanent link" rel="bookmark" href="/" class="timestamp-link">
                                             <abbr
                                                title=""  class="published timeago">
                                             09-04-2015    </abbr>
                                             </a>
                                             </span>
                                          </div>
                                       </div>
                                       <article>
                                          <div
                                             id="post-body-1260884640087456780"  class="post-body entry-content">
                                             <div
                                                style="text-align: left;">
                                                <style>#post-body-1260884640087456780 img{
                                                   width: 100% !important;
                                                   }
                                                </style>
                                                <p><img
                                                   alt="" src="//cdn.nhanh.vn/cdn/store/2558/artCT/4967/miura-cong-phuong-d_xhkj.jpg" style="width:600px;height:422px;" /></p>
                                                <p><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;">N<span
                                                   style="line-height:24px;text-align:justify;">ói bất ngờ bởi tuần trước, khi trao đổi với báo chí, HLV Miura đã có những phát biểu đầy ấn tượng: </span><em
                                                   style="font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;">“Đội U23 VN đã có một suất dự vòng chung kết U23 châu Á. Đó là một động lực rất lớn để cá nhân tôi cũng như các cầu thủ có thêm niềm tin hướng về SEA Games 28.</em></span></span></p>
                                                <p
                                                   style="margin:0px 0px 10px;font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;"><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;"><em>Tôi hoàn toàn không bị áp lực, mà rất hào hứng và rất vui khi lại cùng các cầu thủ trẻ – nay đã trưởng thành rất nhiều sau các giải đấu – chinh phục đỉnh cao mới. Tôi tin rằng với những gì mình đang có trong tay, <strong>U23 VN xứng đáng có được tấm HCV SEA Games.</strong> Trước sự kỳ vọng lớn lao của người hâm mộ, tôi sẽ cố gắng phá dớp 3 lần về nhì của U23 VN”.</em></span></span></p>
                                                <p
                                                   style="margin:0px 0px 10px;font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;"><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;">Thế nhưng trong cuộc gặp gỡ với Phó tổng cục trưởng Tổng cục TDTT, Trưởng đoàn thể thao VN tại SEA Games 28 Trần Đức Phấn hôm qua, ông Miura lại hạ thấp chỉ tiêu của môn bóng đá nam. Điều này gây ra một chút bối rối cho lãnh đạo ngành bởi trong bản “quy hoạch” thành tích của thể thao VN tại SEA Games 28, Tổng cục TDTT giao nhiệm vụ cho VFF và đội U23 VN là ít nhất lọt vào đến trận chung kết.</span></span></p>
                                                <p
                                                   style="margin:0px 0px 10px;font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;"><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;">Tuy nhiên, lãnh đạo tổng cục không “đôi co” mà cam kết sẽ tiếp tục tạo điều kiện tốt nhất cho đội U23 để có kết quả thành công ở SEA Games 28. Trao đổi với chúng tôi, ông Trần Đức Phấn cho hay, trước đề xuất vào bán kết của HLV Miura, ngành thể thao sẽ xem xét lại vấn đề chỉ tiêu của đội U23 VN ở SEA Games 28 và có câu trả lời chính thức với giới truyền thông trong cuộc họp hôm nay, 9.4.</span></span></p>
                                                <p
                                                   style="margin:0px 0px 10px;font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;"><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;">Trở lại buổi làm việc sáng qua, HLV Miura cũng đã đề nghị tổng cục, Ủy ban Olympic VN sớm đăng ký danh sách sơ bộ 35 cầu thủ của đội U23 VN với BTC SEA Games 28.Ông Miura cũng nói rõ, ở danh sách này, ngoài những cầu thủ đã thi đấu tại vòng loại U23 châu Á như Công Phượng, Tuấn Anh, Huy Toàn, Hoài Anh, Minh Long…, những cầu thủ đã hồi phục sau chấn thương cũng sẽ được triệu tập như Xuân Trường, Hoàng Lâm, Thanh Hiền. Ngoài ra, những cầu thủ sinh năm 1992 cũng sẽ có tên như Huy Hùng, Hoàng Thịnh, Minh Tùng, Hồng Quân, Phi Sơn.</span></span></p>
                                                <p
                                                   style="margin:0px 0px 10px;font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;"><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;">Ông Miura nhấn mạnh, nhiệm vụ trọng tâm của bóng đá nam năm 2015 là SEA Games 28 nhưng với ông, thành tích của đội tuyển VN tại vòng loại World Cup 2018 khu vực châu Á cũng là cái đích mà ông không muốn từ bỏ.</span></span></p>
                                                <p
                                                   style="margin:0px 0px 10px;font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;"><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;">HLV Miura phát biểu: <em>“Năm nay, vòng loại World Cup rất quan trọng bởi đây cũng là vòng loại Asian Cup 2019 nên chúng ta không thể xem nhẹ. Sẽ có 12 đội bao gồm 8 đội nhất bảng tham dự vòng loại World Cup và 4 đội nhì bảng có thành tích tốt nhất vào thẳng Asian Cup 2019. Vì thế, đội tuyển VN cũng cần phải có sự chuẩn bị tốt nhất”.</em></span></span></p>
                                                <p
                                                   style="margin:0px 0px 10px;font-family:Tahoma, Geneva, sans-serif;font-size:16px;line-height:24px;text-align:justify;"><span
                                                   style="font-size:14px;"><span
                                                   style="font-family:arial, helvetica, sans-serif;">Lịch tập luyện và thi đấu của U23 VN trong tháng 5 (dự kiến): Ngày 9.5 gặp U23 Hàn Quốc, ngày 10.5 có thể gặp Hà Nội T&amp;T, ngày 16 hoặc 18.5 gặp một đội U23 Đông Nam Á. Trước ngày lên đường đi SEA Games, U23 sẽ đá với đội tuyển VN.</span></span></p>
                                             </div>
                                          </div>
                                       </article>
                                       <br/>
                                       <div
                                          class="post-footer">
                                          <div
                                             class="postarea-wrapper">
                                             <div
                                                class="sharepost">
                                                <ul>
                                                   <li><a
                                                      title="Twitter Tweet" target="_blank" rel="nofollow"
                                                      href="http://twitter.com/home?status=http://t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                      data-via="SweethemeDotCom" class="twitter"><i
                                                      class="fa fa-twitter"></i>Tweet</a></li>
                                                   <li><a
                                                      title="Facebook Share" target="_blank" rel="nofollow"
                                                      onclick="window.open(this.href,'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;"
                                                      href="http://www.facebook.com/share.php?v=4&amp;u=http://t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                      class="facebook"><i
                                                      class="fa fa-facebook"></i>Share</a></li>
                                                   <li><a
                                                      title="Google Plus Share" target="_blank" rel="nofollow"
                                                      href="https://plus.google.com/share?url=http://t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                      class="gplus"><i
                                                      class="fa fa-google-plus"></i>Share</a></li>
                                                   <li><a
                                                      target="_blank"
                                                      href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                      class="linkedin"><i
                                                      class="fa fa-linkedin"></i>Share</a></li>
                                                   <li><a
                                                      target="_blank"
                                                      href="http://pinterest.com/pin/create/button/?url=http://t0073.store.nhanh.vn/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                                      class="pinterest"><i
                                                      class="fa fa-pinterest"></i>Share</a></li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                 <!-- BLOCK -->
                 <?php include('block/sideright.php')?>
               </div>
            </div>
             <!-- FOOTER -->
           <?php include('footer/footer.php')?>
         </div>
         <input
            type="hidden" id="checkStoreId" value="2558">
      </div>
       <script type="text/javascript" src="http://localhost/tintucbmag/js/tintuc.js"></script>
      <script type="text/javascript" src="http://localhost/tintucbmag/js/main.js"></script> 
      <div
         id="fb-root"></div>
      <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script> 
      <div
         style="display: none;">
         <div
            id="dMsg"></div>
      </div>
   </body>
</html>