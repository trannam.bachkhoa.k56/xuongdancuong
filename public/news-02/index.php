<!DOCTYPE html>
<html
   lang="vi-VN" data-nhanh.vn-template="T0073">
   <head>
      <meta
         name="robots" content="noindex" />
      <meta
         name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
      <meta
         charset="utf-8">
      <meta
         content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
      <title>T0073</title>
      <base href="http://t0073.store.nhanh.vn/" target="_blank, _self, _parent, _top">
      <meta
         name="keywords" content="Shopping online">
      <meta
         name="description" content="Shopping online">
      <meta
         property="og:title" content="T0073">
      <meta
         property="og:type" content="product">
      <meta
         property="og:image" content="http://t0073.store.nhanh.vn//cdn.nhanh.vn/cdn/store/2558/store_1428717549_415.png">
      <meta
         name="description" content="Shopping online">
      <meta
         name="keywords" content="Shopping online">
      <meta
         name="description" content="Shopping online">
      <link
         href="//cdn.nhanh.vn/cdn/store/2558/store_1428717578_761.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/tintuc.css" type="text/css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/font-awesome.min.css">
      <link rel="stylesheet" href="http://localhost/tintucbmag/css/style.css">
   </head>
   <body>
      <div
         id="pages-wrapper" class="index home">
         <div
            id="top"></div>
         <div
            id="outer-wrapper">
           <!-- HEADER -->
           <?php include('header/header.php')?>
            <div
               class="content-wrapper">
               <div
                  id="content-wrapper" class="row">
                  <div
                     id="ticker" class="ticker section">
                     <div
                        id="HTML20" class="widget HTML">
                        <h2 class="title"><i
                           class="fa fa-thumb-tack"></i>Tin tức</h2>
                        <div
                           class="layout-content">
                           <div
                              class="tickercontainer">
                              <div
                                 class="mask">
                                 <ul
                                    class="newsticker">
                                    <li>
                                       <a
                                          class="post-tag" href="/the-thao-nc958.html">Thể thao</a>
                                       <h3 class="recent-title"><a
                                          href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html">HLV Miura chỉ hứa đưa U23 Việt Nam vào bán kết SEA Games</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/thoi-trang-nc1249.html">Thời trang</a>
                                       <h3 class="recent-title"><a
                                          href="/bi-quyet-phoi-do-voi-mau-do-ruou-hot-nhat-mua-xuan-2015-n4991.html">Bí quyết phối đồ với màu đỏ rượu hot nhất mùa xuân 2015</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/an-ninh-xa-hoi-nc1247.html">An ninh Xã hội</a>
                                       <h3 class="recent-title"><a
                                          href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html">Cảnh giác những lời hoa mỹ của nhân viên môi giới nhà đất</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/giai-tri-nc1244.html">Giải trí</a>
                                       <h3 class="recent-title"><a
                                          href="/nguoi-nhac-cong-vo-danh-n5082.html">Người nhạc công vô danh</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/am-thuc-nc1248.html">Ẩm thực</a>
                                       <h3 class="recent-title"><a
                                          href="/van-hoa-bia-n5021.html">Văn hóa bia</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/cong-nghe-nc946.html">Công nghệ</a>
                                       <h3 class="recent-title"><a
                                          href="/huong-dan-su-dung-laptop-dung-cach-n5081.html">HƯỚNG DẪN SỬ DỤNG LAPTOP ĐÚNG CÁCH</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/giao-duc-nc957.html">Giáo dục </a>
                                       <h3 class="recent-title"><a
                                          href="/quy-che-tuyen-sinh-2015-cho-he-dai-hoc-cao-dang-n5052.html">Quy chế tuyển sinh 2015 cho hệ đại học, cao đẳng</a></h3>
                                    </li>
                                    <li>
                                       <a
                                          class="post-tag" href="/kinh-doanh-nc955.html">Kinh doanh</a>
                                       <h3 class="recent-title"><a
                                          href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html">Elon Musk, người muốn thay đổi tương lai nhân loại</a></h3>
                                    </li>
                                 </ul>
                                 <span
                                    class="tickeroverlay-left">&nbsp;</span><span
                                    class="tickeroverlay-right">&nbsp;</span>
                              </div>
                           </div>
                        </div>
                        <div
                           class="clear"></div>
                        <span
                           class="widget-item-control">
                        <span
                           class="item-control blog-admin">
                        <a
                           title="Edit" target="configHTML20" href="/" class="quickedit"><img
                           width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                        </span>
                        </span>
                        <div
                           class="clear"></div>
                     </div>
                  </div>
                  <div
                     id="intro" class="intro">
                     <div
                        id="intro-sec" class="intro-sec section">
                        <div
                           id="HTML2" class="widget HTML">
                           <div
                              class="layout-content">
                              <ul>
                                 <li>
                                    <a
                                       style="background:url(//cdn.nhanh.vn/cdn/store/2558/bn/sb_1428714273_344.jpg) no-repeat center center;background-size: cover" href="http://t0073.store.nhanh.vn/news/5081-huong-dan-su-dung-laptop-dung-cach" class="rcp-thumb show-with"></a>
                                    <div
                                       class="post-panel">
                                       <h3 class="rcp-title">
                                          <a
                                             href="http://t0073.store.nhanh.vn/news/5081-huong-dan-su-dung-laptop-dung-cach">Cách sử dụng máy tính xách tay</a>
                                       </h3>
                                    </div>
                                 </li>
                                 <li>
                                    <a
                                       style="background:url(//cdn.nhanh.vn/cdn/store/2558/bn/sb_1428662907_538.jpg) no-repeat center center;background-size: cover" href="http://t0073.store.nhanh.vn/news/5082-nguoi-nhac-cong-vo-danh" class="rcp-thumb show-with"></a>
                                    <div
                                       class="post-panel">
                                       <h3 class="rcp-title">
                                          <a
                                             href="http://t0073.store.nhanh.vn/news/5082-nguoi-nhac-cong-vo-danh">Ca hát và chơi trên đường phố</a>
                                       </h3>
                                    </div>
                                 </li>
                                 <li>
                                    <a
                                       style="background:url(//cdn.nhanh.vn/cdn/store/2558/bn/sb_1428662868_447.jpg) no-repeat center center;background-size: cover" href="http://t0073.store.nhanh.vn/news/5083-elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai" class="rcp-thumb show-with"></a>
                                    <div
                                       class="post-panel">
                                       <h3 class="rcp-title">
                                          <a
                                             href="http://t0073.store.nhanh.vn/news/5083-elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai">Doanh nhân</a>
                                       </h3>
                                    </div>
                                 </li>
                                 <div
                                    class="clear"></div>
                              </ul>
                           </div>
                           <div
                              class="clear"></div>
                           <span
                              class="widget-item-control">
                           <span
                              class="item-control blog-admin">
                           <a
                              title="Edit" target="configHTML2"
                              href="/"
                              class="quickedit">
                           <img
                              width="18" height="18" src="/tp/T0073/images/icon18.png" alt="">
                           </a>
                           </span>
                           </span>
                           <div
                              class="clear"></div>
                        </div>
                     </div>
                  </div>
                  <h1 class="hidden">T0073</h1>
                  <div
                     id="main-wrapper">
                     <div
                        id="recent-layout" class="recent-layout">
                        <div
                           id="recent-sec1" class="recent-sec section">
                           <div
                              id="HTML8" class="widget HTML list fbig">
                              <div
                                 class="box-title" style="border-color: rgb(78, 114, 154);">
                                 <h2 class="title"
                                    style="background: none repeat scroll 0% 0% rgb(78, 114, 154);">
                                    <a>Tin mới</a>
                                 </h2>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574282_732.jpg) no-repeat center center;background-size: cover"
                                             href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html">Đây! Bằng chứng cho thấy Cavani sắp là người của M.U</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428630059_640.jpg) no-repeat center center;background-size: cover"
                                             href="/5-kieu-quan-jeans-dac-tri-nhuoc-diem-co-the-n4986.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/5-kieu-quan-jeans-dac-tri-nhuoc-diem-co-the-n4986.html">5 kiểu quần jeans "đặc trị" nhược điểm cơ thể</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428634846_17.jpg) no-repeat center center;background-size: cover"
                                             href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html">Cảnh giác những lời hoa mỹ của nhân viên môi giới nhà đất</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428637610_599.jpg) no-repeat center center;background-size: cover"
                                             href="/avengers-age-of-ultron-co-boi-canh-o-4-chau-luc-n5003.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/avengers-age-of-ultron-co-boi-canh-o-4-chau-luc-n5003.html">‘Avengers: Age of Ultron’ có bối cảnh ở 4 châu lục</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641175_406.jpg) no-repeat center center;background-size: cover"
                                             href="/nhung-quy-tac-ung-xu-ban-phai-biet-khi-an-tiec-n5018.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/nhung-quy-tac-ung-xu-ban-phai-biet-khi-an-tiec-n5018.html">Những quy tắc ứng xử bạn phải biết khi ăn tiệc</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428714585_872.jpg) no-repeat center center;background-size: cover"
                                             href="/huong-dan-su-dung-laptop-dung-cach-n5081.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/huong-dan-su-dung-laptop-dung-cach-n5081.html">HƯỚNG DẪN SỬ DỤNG LAPTOP ĐÚNG CÁCH</a></h3>
                                          <span
                                             class="recent-date">11-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428654014_143.jpg) no-repeat center center;background-size: cover"
                                             href="/hoc-bong-chinh-phu-australia-nam-2016-n5047.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/hoc-bong-chinh-phu-australia-nam-2016-n5047.html">Học bổng Chính phủ Australia năm 2016</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428658467_463.jpg) no-repeat center center;background-size: cover"
                                             href="/thuc-te-dang-sau-con-so-giam-ngheo-an-tuong-cua-trung-quoc-n5068.html"
                                             class="recent-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/thuc-te-dang-sau-con-so-giam-ngheo-an-tuong-cua-trung-quoc-n5068.html">Thực tế đằng sau con số giảm nghèo ấn tượng của Trung Quốc</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML21" class="widget HTML carousel slider recent-block">
                              <div
                                 class="layout-content">
                                 <div
                                    class="slider-items" style="display: block; opacity: 1;">
                                    <div
                                       class="mSlider">
                                       <div
                                          class="flexslider">
                                          <div
                                             id="circleGs" class="loadingslider">
                                             <div
                                                id="circleG_1" class="circleG"></div>
                                             <div
                                                id="circleG_2" class="circleG"></div>
                                             <div
                                                id="circleG_3" class="circleG"></div>
                                          </div>
                                          <ul
                                             class="slides">
                                             <li>
                                                <a
                                                   href="/"><img
                                                   src="http://anco6.vn/cdn/store/10101/bn/sb_1465114358_975.jpg" alt=""/></a>
                                             </li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML21" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle1">
                                 <h2 class="title bgTitle2">
                                    <a
                                       href="/the-thao-nc958.html">Thể thao</a>
                                 </h2>
                                 <a
                                    href="/the-thao-nc958.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574898_986.jpg) no-repeat center center;background-size: cover"
                                             href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html">HLV Miura chỉ hứa đưa U23 Việt Nam vào bán kết SEA Games</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle4"
                                             href="/hlv-miura-chi-hua-dua-u23-viet-nam-vao-ban-ket-sea-games-n4967.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574782_839.jpg) no-repeat center center;background-size: cover"
                                          href="/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/doi-tuyen-viet-nam-84-ngay-tap-trung-6-tran-vong-loai-world-cup-n4966.html">Đội tuyển Việt Nam: 84 ngày tập trung, 6 trận vòng loại World Cup</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574555_102.jpg) no-repeat center center;background-size: cover"
                                          href="/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/ngo-hoang-thinh-lo-dip-doi-dau-cong-phuong-n4965.html">Ngô Hoàng Thịnh lỡ dịp đối đầu Công Phượng</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574384_796.jpg) no-repeat center center;background-size: cover"
                                          href="/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html">Top 5 bản hợp đồng đắt giá nhất trong lịch sử Ligue 1</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574282_732.jpg) no-repeat center center;background-size: cover"
                                          href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html">Đây! Bằng chứng cho thấy Cavani sắp là người của M.U</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle3">
                                 <h2 class="title bgTitle4">
                                    <a
                                       href="/thoi-trang-nc1249.html">Thời trang</a>
                                 </h2>
                                 <a
                                    href="/thoi-trang-nc1249.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428631917_769.jpeg) no-repeat center center;background-size: cover"
                                             href="/bi-quyet-phoi-do-voi-mau-do-ruou-hot-nhat-mua-xuan-2015-n4991.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/bi-quyet-phoi-do-voi-mau-do-ruou-hot-nhat-mua-xuan-2015-n4991.html">Bí quyết phối đồ với màu đỏ rượu hot nhất mùa xuân 2015</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle6"
                                             href="/bi-quyet-phoi-do-voi-mau-do-ruou-hot-nhat-mua-xuan-2015-n4991.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428631404_309.jpg) no-repeat center center;background-size: cover"
                                          href="/7-kieu-giay-can-co-trong-tu-do-cua-co-nang-tuoi-20-n4990.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/7-kieu-giay-can-co-trong-tu-do-cua-co-nang-tuoi-20-n4990.html">7 kiểu giày cần có trong tủ đồ của cô nàng tuổi 20</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428630810_430.jpg) no-repeat center center;background-size: cover"
                                          href="/hoa-tiet-ke-ngang-moi-goi-mua-he!-n4989.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/hoa-tiet-ke-ngang-moi-goi-mua-he!-n4989.html">Hoạ tiết kẻ ngang 'mời gọi' mùa hè!</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428630291_527.jpg) no-repeat center center;background-size: cover"
                                          href="/cac-kieu-toc-ngan-ep-phong-nen-thu-trong-he-nay-n4987.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/cac-kieu-toc-ngan-ep-phong-nen-thu-trong-he-nay-n4987.html">Các kiểu tóc ngắn ép phồng nên thử trong hè này</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428630059_640.jpg) no-repeat center center;background-size: cover"
                                          href="/5-kieu-quan-jeans-dac-tri-nhuoc-diem-co-the-n4986.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/5-kieu-quan-jeans-dac-tri-nhuoc-diem-co-the-n4986.html">5 kiểu quần jeans "đặc trị" nhược điểm cơ thể</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle5">
                                 <h2 class="title bgTitle6">
                                    <a
                                       href="/an-ninh-xa-hoi-nc1247.html">An ninh Xã hội</a>
                                 </h2>
                                 <a
                                    href="/an-ninh-xa-hoi-nc1247.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428634846_17.jpg) no-repeat center center;background-size: cover"
                                             href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html">Cảnh giác những lời hoa mỹ của nhân viên môi giới nhà đất</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle8"
                                             href="/canh-giac-nhung-loi-hoa-my-cua-nhan-vien-moi-gioi-nha-dat-n4999.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428634155_845.jpg) no-repeat center center;background-size: cover"
                                          href="/canh-giac-chieu-lua-mua-the-tin-dung-n4998.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/canh-giac-chieu-lua-mua-the-tin-dung-n4998.html">Cảnh giác chiêu lừa mua thẻ tín dụng</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428633931_722.jpg) no-repeat center center;background-size: cover"
                                          href="/sat-thu-vu-nam-chiec-cuc-ao-vao-trai-tu-tuoi-13-n4997.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/sat-thu-vu-nam-chiec-cuc-ao-vao-trai-tu-tuoi-13-n4997.html">Sát thủ vụ 'năm chiếc cúc áo' vào trại từ tuổi 13</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428633791_269.JPG) no-repeat center center;background-size: cover"
                                          href="/chia-se-cua-mot-pham-nhan-ve-ban-an-luong-tam-nang-ne-gap-nghin-lan-ban-an-chung-than-n4996.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/chia-se-cua-mot-pham-nhan-ve-ban-an-luong-tam-nang-ne-gap-nghin-lan-ban-an-chung-than-n4996.html">Chia sẻ của một phạm nhân về “bản án lương tâm” nặng nề gấp nghìn lần bản án chung thân</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428633658_830.JPG) no-repeat center center;background-size: cover"
                                          href="/tam-su-cua-hoa-khoi-tay-bac-trong-duong-day-ma-tuy-khung-n4995.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/tam-su-cua-hoa-khoi-tay-bac-trong-duong-day-ma-tuy-khung-n4995.html">Tâm sự của 'hoa khôi' Tây Bắc trong đường dây ma túy 'khủng'</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle7">
                                 <h2 class="title bgTitle8">
                                    <a
                                       href="/giai-tri-nc1244.html">Giải trí</a>
                                 </h2>
                                 <a
                                    href="/giai-tri-nc1244.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428715141_640.jpg) no-repeat center center;background-size: cover"
                                             href="/nguoi-nhac-cong-vo-danh-n5082.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/nguoi-nhac-cong-vo-danh-n5082.html">Người nhạc công vô danh</a></h3>
                                          <span
                                             class="recent-date">11-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle10"
                                             href="/nguoi-nhac-cong-vo-danh-n5082.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428637893_857.jpg) no-repeat center center;background-size: cover"
                                          href="/daniel-craig-nhap-vien-do-chan-thuong-o-truong-quay-007-n5004.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/daniel-craig-nhap-vien-do-chan-thuong-o-truong-quay-007-n5004.html">Daniel Craig nhập viện do chấn thương ở trường quay 007</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428637610_599.jpg) no-repeat center center;background-size: cover"
                                          href="/avengers-age-of-ultron-co-boi-canh-o-4-chau-luc-n5003.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/avengers-age-of-ultron-co-boi-canh-o-4-chau-luc-n5003.html">‘Avengers: Age of Ultron’ có bối cảnh ở 4 châu lục</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428637518_181.jpg) no-repeat center center;background-size: cover"
                                          href="/7-nhan-vat-phan-dien-noi-bat-cua-loat-phim-fast-furious-n5002.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/7-nhan-vat-phan-dien-noi-bat-cua-loat-phim-fast-furious-n5002.html">7 nhân vật phản diện nổi bật của loạt phim ‘Fast & Furious’</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428636616_804.jpg) no-repeat center center;background-size: cover"
                                          href="/tieu-chau-nhu-quynh-lam-co-dau-trong-mv-moi-n5001.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/tieu-chau-nhu-quynh-lam-co-dau-trong-mv-moi-n5001.html">Tiêu Châu Như Quỳnh làm cô dâu trong MV mới</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle9">
                                 <h2 class="title bgTitle10">
                                    <a
                                       href="/am-thuc-nc1248.html">Ẩm thực</a>
                                 </h2>
                                 <a
                                    href="/am-thuc-nc1248.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641454_126.jpg) no-repeat center center;background-size: cover"
                                             href="/van-hoa-bia-n5021.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/van-hoa-bia-n5021.html">Văn hóa bia</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle12"
                                             href="/van-hoa-bia-n5021.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641385_805.jpg) no-repeat center center;background-size: cover"
                                          href="/cau-chuyen-ve-ten-goi-cocktail-n5020.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/cau-chuyen-ve-ten-goi-cocktail-n5020.html">Câu chuyện về tên gọi "Cocktail"</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641273_90.jpg) no-repeat center center;background-size: cover"
                                          href="/net-dep-me-hon-trong-am-thuc-nhat-ban.-n5019.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/net-dep-me-hon-trong-am-thuc-nhat-ban.-n5019.html">Nét đẹp mê hồn trong ẩm thực Nhật Bản.</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641175_406.jpg) no-repeat center center;background-size: cover"
                                          href="/nhung-quy-tac-ung-xu-ban-phai-biet-khi-an-tiec-n5018.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/nhung-quy-tac-ung-xu-ban-phai-biet-khi-an-tiec-n5018.html">Những quy tắc ứng xử bạn phải biết khi ăn tiệc</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428640774_243.jpg) no-repeat center center;background-size: cover"
                                          href="/nhung-quy-tac-tren-ban-an-o-cac-quoc-gia-khong-phai-ai-cung-biet-n5015.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/nhung-quy-tac-tren-ban-an-o-cac-quoc-gia-khong-phai-ai-cung-biet-n5015.html">Những quy tắc trên bàn ăn ở các Quốc gia không phải ai cũng biết</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle11">
                                 <h2 class="title bgTitle12">
                                    <a
                                       href="/cong-nghe-nc946.html">Công nghệ</a>
                                 </h2>
                                 <a
                                    href="/cong-nghe-nc946.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428714585_872.jpg) no-repeat center center;background-size: cover"
                                             href="/huong-dan-su-dung-laptop-dung-cach-n5081.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/huong-dan-su-dung-laptop-dung-cach-n5081.html">HƯỚNG DẪN SỬ DỤNG LAPTOP ĐÚNG CÁCH</a></h3>
                                          <span
                                             class="recent-date">11-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle14"
                                             href="/huong-dan-su-dung-laptop-dung-cach-n5081.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428651615_402.jpg) no-repeat center center;background-size: cover"
                                          href="/lenh-cam-be-khoa-dien-thoai-gian-truan-trac-tro-n5040.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/lenh-cam-be-khoa-dien-thoai-gian-truan-trac-tro-n5040.html">Lệnh cấm bẻ khóa điện thoại: Gian truân, trắc trở</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428651059_762.jpg) no-repeat center center;background-size: cover"
                                          href="/10-dien-thoai-ban-chay-nhat-moi-thoi-dai-n5038.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/10-dien-thoai-ban-chay-nhat-moi-thoi-dai-n5038.html">10 điện thoại bán chạy nhất mọi thời đại</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428650870_942.jpg) no-repeat center center;background-size: cover"
                                          href="/top-5-dien-thoai-tam-trung-co-camera-xin-nhat-n5037.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/top-5-dien-thoai-tam-trung-co-camera-xin-nhat-n5037.html">Top 5 điện thoại tầm trung có camera xịn nhất</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428650387_597.jpg) no-repeat center center;background-size: cover"
                                          href="/laptop-khong-vao-duoc-mang-va-cach-khac-phuc-n5036.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/laptop-khong-vao-duoc-mang-va-cach-khac-phuc-n5036.html">Laptop không vào được mạng và cách khắc phục</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle13">
                                 <h2 class="title bgTitle14">
                                    <a
                                       href="/giao-duc-nc957.html">Giáo dục </a>
                                 </h2>
                                 <a
                                    href="/giao-duc-nc957.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428655105_411.jpg) no-repeat center center;background-size: cover"
                                             href="/quy-che-tuyen-sinh-2015-cho-he-dai-hoc-cao-dang-n5052.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/quy-che-tuyen-sinh-2015-cho-he-dai-hoc-cao-dang-n5052.html">Quy chế tuyển sinh 2015 cho hệ đại học, cao đẳng</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle16"
                                             href="/quy-che-tuyen-sinh-2015-cho-he-dai-hoc-cao-dang-n5052.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428655008_427.jpg) no-repeat center center;background-size: cover"
                                          href="/tuyen-sinh-du-hoc-dai-hoc-tai-brunei-darussalam-2015-bgddt-n5051.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/tuyen-sinh-du-hoc-dai-hoc-tai-brunei-darussalam-2015-bgddt-n5051.html">Tuyển sinh du học đại học tại Brunei Darussalam 2015 - BGD&DT</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428654459_44.jpg) no-repeat center center;background-size: cover"
                                          href="/hoc-bong-100-hoc-phi-khoa-thac-si-tai-warsaw-n5049.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/hoc-bong-100-hoc-phi-khoa-thac-si-tai-warsaw-n5049.html">Học bổng 100% học phí khóa thạc sĩ tại Warsaw</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428655442_843.jpg) no-repeat center center;background-size: cover"
                                          href="/tuyen-sinh-di-hoc-tai-hunggari-nam-2015-n5048.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/tuyen-sinh-di-hoc-tai-hunggari-nam-2015-n5048.html">Tuyển sinh đi học tại Hung-ga-ri năm 2015 </a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428654014_143.jpg) no-repeat center center;background-size: cover"
                                          href="/hoc-bong-chinh-phu-australia-nam-2016-n5047.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/hoc-bong-chinh-phu-australia-nam-2016-n5047.html">Học bổng Chính phủ Australia năm 2016</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                           <div
                              id="HTML3" class="widget block HTML fbig1 fbig">
                              <div
                                 class="box-title bodyTitle15">
                                 <h2 class="title bgTitle16">
                                    <a
                                       href="/kinh-doanh-nc955.html">Kinh doanh</a>
                                 </h2>
                                 <a
                                    href="/kinh-doanh-nc955.html" class="more-link">Xem thêm</a>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <div
                                       class="blockMain first">
                                       <div
                                          class="rthumbc">
                                          <a
                                             style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428715739_911.jpg) no-repeat center center;background-size: cover"
                                             href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html"
                                             class="first-thumb show-with"></a>
                                       </div>
                                       <div
                                          class="first-content">
                                          <h3 class="recent-title"><a
                                             href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html">Elon Musk, người muốn thay đổi tương lai nhân loại</a></h3>
                                          <span
                                             class="recent-date">11-04-2015</span>
                                          <p
                                             class="recent-des"></p>
                                          <p></p>
                                          <div
                                             class="post-readmore"><a
                                             class="bgTitle18"
                                             href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html">Đọc thêm<i
                                             class="fa fa-long-arrow-right"></i></a></div>
                                       </div>
                                    </div>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428658467_463.jpg) no-repeat center center;background-size: cover"
                                          href="/thuc-te-dang-sau-con-so-giam-ngheo-an-tuong-cua-trung-quoc-n5068.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/thuc-te-dang-sau-con-so-giam-ngheo-an-tuong-cua-trung-quoc-n5068.html">Thực tế đằng sau con số giảm nghèo ấn tượng của Trung Quốc</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428657996_505.jpg) no-repeat center center;background-size: cover"
                                          href="/hsbc-doi-mat-voi-dieu-tra-tai-phap-lien-quan-be-boi-tron-thue-n5066.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/hsbc-doi-mat-voi-dieu-tra-tai-phap-lien-quan-be-boi-tron-thue-n5066.html">HSBC đối mặt với điều tra tại Pháp liên quan bê bối trốn thuế</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428657850_874.jpg) no-repeat center center;background-size: cover"
                                          href="/anh-phat-hien-mo-dau-co-tru-luong-100-ty-thung-n5065.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/anh-phat-hien-mo-dau-co-tru-luong-100-ty-thung-n5065.html">Anh phát hiện mỏ dầu có trữ lượng 100 tỷ thùng</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <div
                                          class="rthumbc"><a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428657795_943.jpg) no-repeat center center;background-size: cover"
                                          href="/nga-met-vi-rup-xuong-kho-vi-rup-len-n5064.html"
                                          class="recent-thumb show-with"></a></div>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/nga-met-vi-rup-xuong-kho-vi-rup-len-n5064.html">Nga mệt vì Rúp xuống, khổ vì Rúp… lên</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                              <span
                                 class="widget-item-control">
                              <span
                                 class="item-control blog-admin">
                              <a
                                 title="Edit" target="configHTML3" href="/" class="quickedit"><img
                                 width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                              </span>
                              <div
                                 class="clear"></div>
                           </div>
                        </div>
                     </div>
                     <div
                        id="recent-layout2" class="recent-layout hot-news">
                        <div
                           id="recent-sec2" class="recent-sec section">
                           <div
                              id="HTML9" class="widget HTML gallery recent-block">
                              <div
                                 class="box-title" style="border-color: rgb(255, 192, 0);">
                                 <h2 class="title" style="background: none repeat scroll 0% 0% rgb(255, 192, 0);">
                                    <a>Tin hot</a>
                                 </h2>
                              </div>
                              <div
                                 class="layout-content">
                                 <ul>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574384_796.jpg) no-repeat center center;background-size: cover"
                                          href="/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/top-5-ban-hop-dong-dat-gia-nhat-trong-lich-su-ligue-1-n4964.html">Top 5 bản hợp đồng đắt giá nhất trong lịch sử Ligue 1</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428574282_732.jpg) no-repeat center center;background-size: cover"
                                          href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/day!-bang-chung-cho-thay-cavani-sap-la-nguoi-cua-m.u-n4963.html">Đây! Bằng chứng cho thấy Cavani sắp là người của M.U</a></h3>
                                          <span
                                             class="recent-date">09-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428631404_309.jpg) no-repeat center center;background-size: cover"
                                          href="/7-kieu-giay-can-co-trong-tu-do-cua-co-nang-tuoi-20-n4990.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/7-kieu-giay-can-co-trong-tu-do-cua-co-nang-tuoi-20-n4990.html">7 kiểu giày cần có trong tủ đồ của cô nàng tuổi 20</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428630810_430.jpg) no-repeat center center;background-size: cover"
                                          href="/hoa-tiet-ke-ngang-moi-goi-mua-he!-n4989.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/hoa-tiet-ke-ngang-moi-goi-mua-he!-n4989.html">Hoạ tiết kẻ ngang 'mời gọi' mùa hè!</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428634155_845.jpg) no-repeat center center;background-size: cover"
                                          href="/canh-giac-chieu-lua-mua-the-tin-dung-n4998.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/canh-giac-chieu-lua-mua-the-tin-dung-n4998.html">Cảnh giác chiêu lừa mua thẻ tín dụng</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428633931_722.jpg) no-repeat center center;background-size: cover"
                                          href="/sat-thu-vu-nam-chiec-cuc-ao-vao-trai-tu-tuoi-13-n4997.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/sat-thu-vu-nam-chiec-cuc-ao-vao-trai-tu-tuoi-13-n4997.html">Sát thủ vụ 'năm chiếc cúc áo' vào trại từ tuổi 13</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428637518_181.jpg) no-repeat center center;background-size: cover"
                                          href="/7-nhan-vat-phan-dien-noi-bat-cua-loat-phim-fast-furious-n5002.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/7-nhan-vat-phan-dien-noi-bat-cua-loat-phim-fast-furious-n5002.html">7 nhân vật phản diện nổi bật của loạt phim ‘Fast & Furious’</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428636616_804.jpg) no-repeat center center;background-size: cover"
                                          href="/tieu-chau-nhu-quynh-lam-co-dau-trong-mv-moi-n5001.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/tieu-chau-nhu-quynh-lam-co-dau-trong-mv-moi-n5001.html">Tiêu Châu Như Quỳnh làm cô dâu trong MV mới</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641454_126.jpg) no-repeat center center;background-size: cover"
                                          href="/van-hoa-bia-n5021.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/van-hoa-bia-n5021.html">Văn hóa bia</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428641385_805.jpg) no-repeat center center;background-size: cover"
                                          href="/cau-chuyen-ve-ten-goi-cocktail-n5020.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/cau-chuyen-ve-ten-goi-cocktail-n5020.html">Câu chuyện về tên gọi "Cocktail"</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428649993_676.jpg) no-repeat center center;background-size: cover"
                                          href="/5-cach-chac-chan-giup-tang-thoi-luong-pin-tren-laptop-n5034.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/5-cach-chac-chan-giup-tang-thoi-luong-pin-tren-laptop-n5034.html">5 cách chắc chắn giúp tăng thời lượng pin trên laptop</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428649843_914.jpg) no-repeat center center;background-size: cover"
                                          href="/xu-ly-nhanh-6-su-co-khi-su-dung-laptop-n5033.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/xu-ly-nhanh-6-su-co-khi-su-dung-laptop-n5033.html">Xử lý nhanh 6 sự cố khi sử dụng laptop</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428655008_427.jpg) no-repeat center center;background-size: cover"
                                          href="/tuyen-sinh-du-hoc-dai-hoc-tai-brunei-darussalam-2015-bgddt-n5051.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/tuyen-sinh-du-hoc-dai-hoc-tai-brunei-darussalam-2015-bgddt-n5051.html">Tuyển sinh du học đại học tại Brunei Darussalam 2015 - BGD&DT</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428654459_44.jpg) no-repeat center center;background-size: cover"
                                          href="/hoc-bong-100-hoc-phi-khoa-thac-si-tai-warsaw-n5049.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/hoc-bong-100-hoc-phi-khoa-thac-si-tai-warsaw-n5049.html">Học bổng 100% học phí khóa thạc sĩ tại Warsaw</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428715739_911.jpg) no-repeat center center;background-size: cover"
                                          href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/elon-musk-nguoi-muon-thay-doi-tuong-lai-nhan-loai-n5083.html">Elon Musk, người muốn thay đổi tương lai nhân loại</a></h3>
                                          <span
                                             class="recent-date">11-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                    <li>
                                       <a
                                          style="background:url(//cdn.nhanh.vn/cdn/store/2558/art/article_1428658467_463.jpg) no-repeat center center;background-size: cover"
                                          href="/thuc-te-dang-sau-con-so-giam-ngheo-an-tuong-cua-trung-quoc-n5068.html"
                                          class="recent-thumb"></a>
                                       <div
                                          class="recent-content">
                                          <h3 class="recent-title"><a
                                             href="/thuc-te-dang-sau-con-so-giam-ngheo-an-tuong-cua-trung-quoc-n5068.html">Thực tế đằng sau con số giảm nghèo ấn tượng của Trung Quốc</a></h3>
                                          <span
                                             class="recent-date">10-04-2015</span>
                                       </div>
                                       <div
                                          class="clear"></div>
                                    </li>
                                 </ul>
                              </div>
                              <div
                                 class="clear"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                 <!-- BLOCK -->
                 <?php include('block/sideright.php')?>
               </div>
               <div
                  class="clear"></div>
           <!-- FOOTER -->
           <?php include('footer/footer.php')?>
         </div>
         <input
            type="hidden" id="checkStoreId" value="2558">
      </div>
      <script type="text/javascript" src="http://localhost/tintucbmag/js/tintuc.js"></script>
      <script type="text/javascript" src="http://localhost/tintucbmag/js/main.js"></script> 
      <div
         id="fb-root"></div>
      <script>(function(d, s, id) {
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) return;
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
         fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
      </script> 
      <div
         style="display: none;">
         <div
            id="dMsg"></div>
      </div>
   </body>
</html>