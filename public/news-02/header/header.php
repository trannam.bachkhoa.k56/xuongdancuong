 <div
               id="header-wrapper">
               <div
                  id='header-top'>
                  <div
                     class='row'>
                     <div
                        class='menu-top section' id='menu-top'>
                        <div
                           class='widget LinkList' id='LinkList101'>
                           <div
                              class='widget-content'>
                              <ul
                                 id='nav1'>
                                 <li><a
                                    href='/'>Trang chủ</a></li>
                                 <li><a
                                    href='/news/category'>Tin tức</a></li>
                                 <li><a
                                    href="#">Về chúng tôi</a></li>
                                 <li><a
                                    href='/lien-he'>Liên hệ</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div
                        class='social-sec section' id='social-sec'>
                        <div
                           class='widget LinkList' id='LinkList55'>
                           <div
                              class='widget-content'>
                              <ul
                                 id='social'>
                                 <li><a
                                    class='facebook' href='https://www.facebook.com/www.nhanh.vn?_rdr' title='facebook'></a></li>
                                 <li><a
                                    class='twitter' href='' title='twitter'></a></li>
                                 <li><a
                                    class='gplus' href='' title='gplus'></a></li>
                                 <li><a
                                    class='pinterest' href='' title='pinterest'></a></li>
                                 <li><a
                                    class='youtube' href='https://www.youtube.com/watch?v=DQF8qx1qxe0' title='youtube'></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div
                  class='clear'></div>
               <div
                  class='row' id='header-content'>
                  <div
                     class='header section' id='header'>
                     <div
                        class='widget Header' id='Header1'>
                        <div
                           id='header-inner'>
                           <a
                              href="/"><img
                              src="//cdn.nhanh.vn/cdn/store/2558/store_1428717549_415.png" alt="LOGO" style="max-width: 100% !important;"/></a>
                        </div>
                     </div>
                  </div>
                  <div
                     class='topad section' id='topad'>
                     <div
                        class='widget HTML' id='HTML22'>
                        <div
                           class='widget-content'>
                           <a
                              href='javascript:void(0);'><img
                              src='//cdn.nhanh.vn/cdn/store/2558/bn/sb_1428726574_480.png'/></a>
                        </div>
                        <div
                           class='clear'></div>
                        <span
                           class='widget-item-control'>
                        <span
                           class='item-control blog-admin'>
                        <a
                           class='quickedit'
                           href='/' target='configHTML22' title='Edit'>
                        <img
                           alt='' height='18' src='/tp/T0073/images/icon18.png' width='18'/>
                        </a>
                        </span>
                        </span>
                        <div
                           class='clear'></div>
                     </div>
                  </div>
               </div>
               <div
                  class='clear'></div>
               <div
                  class='row' id='header-tail'>
                  <a
                     class='home-icon' href='/'><i
                     class='fa fa-home'></i></a>
                  <div
                     class='menu section' id='menu'>
                     <div
                        class='widget LinkList' id='LinkList100'>
                        <div
                           class="widget-content">
                           <ul
                              id="nav">
                              <li>
                                 <a
                                    href="/the-thao-nc958.html">Thể thao</a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/bong-da-ngoai-hang-anh-nc1250.html">Bóng đá Ngoại hạng Anh</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/bong-da-tay-ban-nha-nc1252.html">Bóng đá Tây Ban Nha</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/bong-da-y-nc1251.html">Bóng đá Ý</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/bong-da-duc-nc1253.html">Bóng đá Đức</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/bong-da-phap-nc1254.html">Bóng đá Pháp</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/bong-da-viet-nam-nc1255.html">Bóng đá Việt Nam</a>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a
                                    href="/thoi-trang-nc1249.html">Thời trang</a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/the-gioi-thoi-trang-nc1272.html">Thế giới thời trang</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/bi-quyet-mac-dep-nc1273.html">Bí quyết mặc đẹp</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/thoi-trang-bon-mua-nc1274.html">Thời trang bốn mùa</a>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a
                                    href="/an-ninh-xa-hoi-nc1247.html">An ninh Xã hội</a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/te-nan-xa-hoi-nc1275.html">Tệ nạn xã hội</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/ho-so-vu-an-nc1276.html">Hồ sơ vụ án</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/canh-giac-nc1277.html">Cảnh giác</a>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a
                                    href="/giai-tri-nc1244.html">Giải trí</a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/ca-nhac-nc1245.html">Ca nhạc</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/phim-nc1246.html">Phim</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/lich-phim-chieu-rap-nc1278.html">Lịch phim chiếu rạp</a>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a
                                    href="/am-thuc-nc1248.html">Ẩm thực</a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/tin-tuc-am-thuc-nc1279.html">Tin tức ẩm thực</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/dac-san-ba-mien-nc1280.html">Đặc sản ba miền</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/van-hoa-am-thuc-nc1281.html">Văn hóa ẩm thực</a>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a
                                    href="/cong-nghe-nc946.html">Công nghệ</a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/dien-thoai-nc1256.html">Điện thoại</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/laptop-nc1257.html">Laptop</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/phan-mem-may-tinh-nc1258.html">Phần mềm máy tính</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/ung-dung-mobile-nc1259.html">Ứng dụng Mobile</a>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a
                                    href="/giao-duc-nc957.html">Giáo dục </a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/tin-tuc-giao-duc-nc1282.html">Tin tức giáo dục</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/tuyen-sinh-nc1283.html">Tuyển sinh</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/du-hoc-nc1284.html">Du học</a>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a
                                    href="/kinh-doanh-nc955.html">Kinh doanh</a>
                                 <span
                                    class="hardSub"></span>
                                 <ul
                                    id="sub-menu" style="display: none;">
                                    <li>
                                       <a
                                          href="/tai-chinh-dau-tu-nc1285.html">Tài chính - Đầu tư</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/bat-dong-san-nc1286.html">Bất động sản</a>
                                    </li>
                                    <li>
                                       <a
                                          href="/kinh-te-quoc-te-nc1287.html">Kinh tế quốc tế</a>
                                    </li>
                                 </ul>
                              </li>
                           </ul>
                           <select
                              id="selectnav1" class="selectnav"  onchange="location = this.options[this.selectedIndex].value;">
                              <option
                                 value="">- Danh mục -</option>
                              <option
                                 value="/the-thao-nc958.html">Thể thao</option>
                              <option
                                 value="/thoi-trang-nc1249.html">Thời trang</option>
                              <option
                                 value="/an-ninh-xa-hoi-nc1247.html">An ninh Xã hội</option>
                              <option
                                 value="/giai-tri-nc1244.html">Giải trí</option>
                              <option
                                 value="/am-thuc-nc1248.html">Ẩm thực</option>
                              <option
                                 value="/cong-nghe-nc946.html">Công nghệ</option>
                              <option
                                 value="/giao-duc-nc957.html">Giáo dục</option>
                              <option
                                 value="/kinh-doanh-nc955.html">Kinh doanh</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div
                     id='header-search'>
                     <form
                        action='/search/newssearch' class='searchform clearfix' method='get' >
                        <button
                           type="submit" class='search-icon'></button>
                        <input
                           class='searchbox' name='q' placeholder='Tìm kiếm...' style='display: none;' type='text'/>
                     </form>
                  </div>
               </div>
            </div>