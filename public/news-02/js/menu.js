$(document).ready(function(){
    var ww = document.body.clientWidth;
    if (ww < 800) {
        $(".toggleMenu").css("display", "inline-block");
        $(".nav li a").click(function() {
            $(this).parent("li").toggleClass('hover');
        });
    } else {
        $(".toggleMenu").css("display", "none");
        $(".nav li").hover(function() {
            $(this).addClass('hover');
        }, function() {
            $(this).removeClass('hover');
        });
    }
});
$(".nav li a").each(function() {
    if ($(this).next().length > 0) {
        $(this).addClass("parent");
    };
});
if (ww < 800) {
    $(".toggleMenu").css("display", "inline-block");
    $(".nav li a.parent").click(function(e) {
        e.preventDefault();
        $(this).parent("li").toggleClass('hover');
    });
} else {
 }

$(".toggleMenu").click(function(e) {
    e.preventDefault();
    $(".nav").toggle();
});

if (ww < 800) {
    $(".toggleMenu").css("display", "inline-block");
    $(".nav").hide();
} else {
}
var ww = document.body.clientWidth;
$(document).ready(function() {
    $(".toggleMenu").click(function(e) {
        e.preventDefault();
        $(".nav").toggle();
    });
    $(".nav li a").each(function() {
        if ($(this).next().length > 0) {
            $(this).addClass("parent");
        };
    })
    adjustMenu();
});
function adjustMenu() {
    if (ww < 800) {
        $(".toggleMenu").css("display", "inline-block");
        $(".nav").hide();
        $(".nav li a.parent").click(function(e) {
            e.preventDefault();
            $(this).parent("li").toggleClass('hover');
        });
    } else {
        $(".toggleMenu").css("display", "none");
        $(".nav li").hover(function() {
            $(this).addClass('hover');
        }, function() {
            $(this).removeClass('hover');
        });
    }
}
$(document).ready(function() {
    $(".toggleMenu").click(function(e) {
        e.preventDefault();
        $(this).toggleClass("active");
        $(".nav").toggle();
    });
});
if (ww < 800) {
    $(".toggleMenu").css("display", "inline-block");
    if (!$(".toggleMenu").hasClass("active")) {
        $(".nav").hide();
    } else {
        $(".nav").show();
    }
    $(".nav li a.parent").click(function(e) {
        e.preventDefault();
        $(this).parent("li").toggleClass('hover');
    });
}

$(".nav li").unbind('mouseenter mouseleave');
if (ww < 800) {
    $(".toggleMenu").css("display", "inline-block");
    if (!$(".toggleMenu").hasClass("active")) {
        $(".nav").hide();
    } else {
        $(".nav").show();
    }
    $(".nav li").unbind('mouseenter mouseleave');
    $(".nav li a.parent").unbind("click").bind("click", function(e){
        e.preventDefault();
        $(this).parent("li").toggleClass('hover');
    });
} else {
    $(".toggleMenu").css("display", "none");
    $(".nav").show();
    $(".nav li").removeClass("hover");
    $(".nav li a").unbind("click");
    $(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
        $(this).toggleClass('hover');
    });
}