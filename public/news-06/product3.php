<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->
      <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-13012" class="post-13012 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Dự án Roman Plaza</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2018-08-30T16:52:53+00:00</span>                 
                     <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/welcome.png" width="225" height="56" alt="Welcome to Roman Plaza" title="welcome" class="img-responsive wp-image-13253" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/welcome-200x50.png 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/welcome.png 225w" sizes="(max-width: 800px) 100vw, 225px" /></span></div>
                                       </p>
                                       <h2 style="text-align: center;">DỰ ÁN ROMAN PLAZA</h2>
                                       <p>Mong ước được chung tay kiến tạo những giá trị tinh hoa tốt đẹp, góp phần tô điểm cho bản sắc văn hóa của Thủ đô với những chuỗi tổ hợp bất động sản hoàn hảo như những “KIỆT TÁC vượt THỜI GIAN”, Hải Phát Group đã phát triển dự án mang tên Roman Plaza, dự án được định vị là công trình kiến trúc độc đáo, mang đậm dấu ấn nghệ thuật.</p>
                                       <p>Roman Plaza hội tụ những giá trị vàng của một dự án bất động sản cao cấp mang phong cách Hoàng Gia Châu Âu thế kỷ XVII, áp dụng những công nghệ xây dựng tiên tiến tạo thành một công trình có khối đế vững chắc và mặt đứng tuyệt đẹp.</p>
                                       <p>Roman Plaza – hai Tòa tháp với hơn 800 căn hộ sang trọng bậc nhất, một không gian hoàn mỹ và duy nhất tại Việt Nam, một công trình hoàn hảo, hoàn hảo đến từng chi tiết, chất lượng bền vững với thời gian, vật liệu và nội thất xa xỉ, tinh xảo hiếm có song hành cùng tiện nghi ưu việt dành riêng cho các chủ nhân xứng tầm.</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #5c422e;">TỔNG QUAN DỰ ÁN ROMAN PLAZA</span></h2>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #5c422e;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2017/03/background-container-tong-quan-roman-plaza-1.png");background-position: center top;background-repeat: repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5"  style='margin-top:30px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:15px;">
                                       <h3 class="title-heading-left"><span style="color: #d6a10f;">VIDEO DỰ ÁN ROMAN PLAZA</span></p></h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-video fusion-youtube" style="max-width:810px;max-height:473px;">
                                       <div class="video-shortcode">
                                          <div class="rll-youtube-player" data-id="D1XXlK00kTo"></div>
                                          <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/D1XXlK00kTo?wmode=transparent&autoplay=0" width="810" height="473" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last 2_5"  style='margin-top:25px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p><span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Tên dự án:</span> <span style="color: #d6a10f;"><b>Roman Plaza</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Vị trí:<b> Mặt đường Lê Văn Lương kéo dài, Nam Từ Liêm,Hà Nội</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Chủ đầu tư: <b>Tập đoàn Hải Phát</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Đơn vị phát triển: <b>MIK Group</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Tổng diện tích đất dự án: <b>38,155.8 m2</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Diện tích đất xây dựng: <b>10,842.6 m2</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Diện tích xây dựng công trình nhà ở cao tầng: <b>7,841.7m2</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Diện tích xây dựng công trình trường học, nhà trẻ: <b>2,955m2</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Diện tích dành cho cây xanh và hạ tầng kỹ thuật: <b>3,469.9m2</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Tổng số tầng nổi: <b>27 tầng</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Số tầng hầm: <b>03 tầng, diện tích 43,416 m2</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Tổng số căn hộ: <b>1866 căn hộ/ 4 tháp</b></span><br />
                                          <span style="color: #d6a10f; font-size: 22px; font-family: UTM-Avo;">♣</span> <span style="color: #ffffff;">Diện tích căn hộ: <b>58,78m2 &#8211; 106,65m2</b></span>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 30px;margin-top: 20px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #5c422e;">ROMAN PLAZA – VỊ TRÍ VÀNG ĐẮC ĐỊA GIỮA CHỐN NỘI ĐÔ</span></h2>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-3 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p><a href="https://lamrealtor.com/du-an/20180818-roman-plaza/"><strong>Dự án Roman Plaza</strong></a> có vị trí thuận tiện về giao thông khi nằm tại góc 2 trục đường chính là đường Tố Hữu (Lê Văn Lương kéo dài) và đường vào khu đô thị Mỗ Lao, Hà Đông, Hà Nội. Đây chính là nơi giao thao của các quận Nam Từ Liêm, Bắc Từ Liêm, Thanh Xuân và khu Trung Hòa Nhân Chính:</p>
                                       <ul>
                                          <li>Lê Trọng Tấn – Đại lộ Thăng Long – Trần Duy Hưng</li>
                                          <li>Lê Trọng Tấn – Tố Hữu – Lê Văn Lương</li>
                                          <li>Lê Trọng Tấn – Trần Phú – Nguyễn Trãi</li>
                                          <li>Bến xe Yên Nghĩa 3km</li>
                                          <li>Bệnh viện Quân Y 103 4km, bệnh viện Quốc Tế 1km</li>
                                          <li>Ga tàu điện trên cao 1km.</li>
                                          <li>Siêu thị Big C 6km, Metro 5km</li>
                                       </ul>
                                       <p>Ngoài ra dự án <strong><a href="https://lamrealtor.com/du-an/20180818-roman-plaza/">Roman Plaza</a></strong> còn rất gần đại siêu thị AEON Mall rộng 9ha của Tập Đoàn BIM Group và AEON Mall Việt Nam. Với vị trí đắc địa như vậy hứa hẹn khi AEON Mall chính thức đi vào hoạt động sẽ tạo 1 cú huých lớn cho các dự án bất động sản phía tây Hà Nội nói chung và <a href="https://lamrealtor.com/du-an/20180818-roman-plaza/"><strong>Roman Plaza</strong></a> nói riêng</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/vi-tri-du-an-roman-plaza.jpg" width="800" height="748" alt="" title="vi-tri-du-an-roman-plaza" class="img-responsive wp-image-13491" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/vi-tri-du-an-roman-plaza-200x187.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/vi-tri-du-an-roman-plaza-400x374.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/vi-tri-du-an-roman-plaza-600x561.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/vi-tri-du-an-roman-plaza.jpg 800w" sizes="(max-width: 800px) 100vw, 600px" /></span>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 30px;margin-top: 10px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #5c422e;">PHÁP LÝ DỰ ÁN ROMAN PLAZA</span></h2>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-5 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-6 hover-type-none fusion-animated" data-animationType="fadeInDown" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/avatar-phap-ly-roman-plaza.jpg" width="600" height="314" alt="Avatar Phap ly Roman Plaza" title="avatar-phap-ly-roman-plaza" class="img-responsive wp-image-13495" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/avatar-phap-ly-roman-plaza-200x105.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/avatar-phap-ly-roman-plaza-400x209.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/avatar-phap-ly-roman-plaza.jpg 600w" sizes="(max-width: 800px) 100vw, 600px" /></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p><strong>Chung cư Roman Plaza</strong> là dự án hiếm hoi tại Hà Nội có đầy đủ các giấy tờ pháp lý cần thiết trước khi chính thức mở bán. Bao gồm:</p>
                                       <ul>
                                          <li>Giấy phép quy hoạch của Sở quy hoạch – Kiến trúc UBND Thành phố Hà Nội</li>
                                          <li>Quyết định Chủ trương Đầu tư của UBND Thành phố Hà Nội</li>
                                          <li>Giấy phép xây dựng của Sở Xây dựng – UBND Thành phố Hà Nội</li>
                                          <li>Giấy chứng nhận Thẩm duyệt thiết kế về Phòng cháy chữa cháy của Bộ công an</li>
                                       </ul>
                                    </div>
                                    <div class="fusion-button-wrapper fusion-aligncenter">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:rgba(255,255,255,.8);}.fusion-button.button-1 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-1 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-1.button-3d{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.button-1.button-3d:active{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:rgba(255,255,255,.9);}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-1{background: #b7960b;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #876f08;}.fusion-button.button-1{width:auto;}</style>
                                       <a class="fusion-button button-3d fusion-button-round button-large button-custom button-1 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="100%" target="_self"><span class="fusion-button-text">Xem thêm Pháp lý Dự án Roman Plaza</span></a>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #5c422e;">KHU NHÀ LIỀN KỀ ROMAN PLAZA</span></h2>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-7 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:5px;">
                                       <h3 class="title-heading-left"><span style="font-size: 20px; color: #5c422e;">KHU NHÀ LIỀN KỀ</span></h3>
                                    </div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left"><span style="font-size: 16px; color: #5c422e;">KHI NGÔI NHÀ LÀ CỦA BẠN, VÀ MÁI ẤM LÀ CUỘC ĐỜI</span></h3>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Thông tin khu nhà liền kề:</p>
                                       <ul>
                                          <li>Số lượng: 39 căn nhà</li>
                                          <li>Diện tích ô đất: từ 72 m2 đến 170 m2</li>
                                          <li>Mật độ xây dựng: 48% &#8211; 83%</li>
                                          <li>Số tầng cao: 4 tầng 1 tum</li>
                                       </ul>
                                       <p><img class="aligncenter" src="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-nha-lien-ke-roman-plaza-271x300.jpg" alt="" width="271" height="300" /></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-8 element-bottomshadow fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-nha-lien-ke-roman-plaza.jpg" width="800" height="799" alt="Khu Nha lien ke Du an Roman Plaza" title="phan-khu-nha-lien-ke-roman-plaza" class="img-responsive wp-image-13518" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-nha-lien-ke-roman-plaza-200x200.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-nha-lien-ke-roman-plaza-400x400.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-nha-lien-ke-roman-plaza-600x599.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-nha-lien-ke-roman-plaza.jpg 800w" sizes="(max-width: 800px) 100vw, 600px" /></span></div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #5c422e;">KHU BIỆT THỰ ROMAN PLAZA</span></h2>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-9 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-10 element-bottomshadow fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-biet-thu-roman-plaza.jpg" width="800" height="799" alt="Khu Biet thu Du an Roman Plaza" title="phan-khu-biet-thu-roman-plaza" class="img-responsive wp-image-13517" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-biet-thu-roman-plaza-200x200.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-biet-thu-roman-plaza-400x400.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-biet-thu-roman-plaza-600x599.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-khu-biet-thu-roman-plaza.jpg 800w" sizes="(max-width: 800px) 100vw, 600px" /></span></div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:5px;">
                                       <h3 class="title-heading-left"><span style="font-size: 20px; color: #5c422e;">KHU BIỆT THỰ</span></p></h3>
                                    </div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">CUỘC SỐNG THƯỢNG LƯU CỦA NHỮNG NGƯỜI TINH TẾ</h3>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Thông tin khu biệt thự:</p>
                                       <ul>
                                          <li>Số lượng: 20 căn nhà</li>
                                          <li>Diện tích ô đất: 190 m2 và 241 m2</li>
                                          <li>Mật độ xây dựng: 37% &#8211; 43%</li>
                                          <li>Số tầng cao: 3 tầng 1 tum</li>
                                       </ul>
                                       <p><img class="size-medium wp-image-13521 aligncenter" src="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-biet-thu-roman-plaza-271x300.jpg" alt="" width="271" height="300" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-biet-thu-roman-plaza-200x221.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-biet-thu-roman-plaza-271x300.jpg 271w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-biet-thu-roman-plaza-400x443.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-biet-thu-roman-plaza-600x664.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-biet-thu-roman-plaza-768x850.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-biet-thu-roman-plaza.jpg 800w" sizes="(max-width: 271px) 100vw, 271px" /></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #5c422e;">KHU CĂN HỘ CAO CẤP ROMAN PLAZA</span></h2>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-11 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:5px;">
                                       <h3 class="title-heading-left"><span style="font-size: 20px; color: #5c422e;">HẠNH PHÚC GIẢN ĐƠN,</span></p></h3>
                                    </div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:15px;">
                                       <h3 class="title-heading-left"><span style="font-size: 16px; color: #5c422e;">GIÁ TRỊ NẰM Ở MỖI PHÚT GIÂY TẬN HƯỞNG</span></h3>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Thông tin khu căn hộ (804 căn):</p>
                                       <ul>
                                          <li>Diện tích căn hộ: từ 68 m2 đến 138 m2</li>
                                          <li>Số tầng cao: 25 tầng và 3 tầng hầm</li>
                                          <li>
                                             Các loại căn hộ:
                                             <ul>
                                                <li>2 phòng ngủ &amp; 3 phòng ngủ</li>
                                                <li>Căn hộ Duplex</li>
                                             </ul>
                                          </li>
                                       </ul>
                                       <p><img class="size-medium wp-image-13522 aligncenter" src="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-can-ho-cao-cap-roman-plaza-271x300.jpg" alt="" width="271" height="300" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-can-ho-cao-cap-roman-plaza-200x221.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-can-ho-cao-cap-roman-plaza-271x300.jpg 271w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-can-ho-cao-cap-roman-plaza-400x442.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-can-ho-cao-cap-roman-plaza-600x663.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-can-ho-cao-cap-roman-plaza-768x849.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2017/03/phan-vung-can-ho-cao-cap-roman-plaza.jpg 800w" sizes="(max-width: 271px) 100vw, 271px" /></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-12 element-bottomshadow fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-can-ho-cao-cap-roman-plaza.jpg" width="800" height="800" alt="Khu Nha lien ke Du an Roman Plaza" title="khu-can-ho-cao-cap-roman-plaza" class="img-responsive wp-image-13530" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-can-ho-cao-cap-roman-plaza-200x200.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-can-ho-cao-cap-roman-plaza-400x400.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-can-ho-cao-cap-roman-plaza-600x600.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-can-ho-cao-cap-roman-plaza.jpg 800w" sizes="(max-width: 800px) 100vw, 600px" /></span></div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2017/03/bg.jpg");background-position: center top;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">CÁC LOẠI CĂN HỘ</h2>
                                       <h3 style="text-align: center;">ROMAN PLAZA</h3>
                                       <p style="text-align: center;"><img class="alignnone size-thumbnail wp-image-12696" src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land-150x14.png" alt="" width="150" height="14" /></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-13">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-13{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-13 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01.jpg" class="fusion-lightbox" data-rel="iLightbox[e2d1a5394edc70140ae]" data-title="noi-that-can-ho-roman-plaza-01" title="noi-that-can-ho-roman-plaza-01"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01.jpg" width="1600" height="1000" alt="Noi that can ho Roman Plaza 01" class="img-responsive wp-image-13275" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01-200x125.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01-400x250.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01-600x375.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01-800x500.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01-1200x750.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-01.jpg 1600w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-14">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-14{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-14 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-02.jpg" class="fusion-lightbox" data-rel="iLightbox[4f285919ddb1b3adeeb]" data-title="noi-that-can-ho-roman-plaza-02" title="noi-that-can-ho-roman-plaza-02"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-02.jpg" width="1024" height="718" alt="Noi that can ho Roman Plaza 02" class="img-responsive wp-image-13276" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-02-200x140.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-02-400x280.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-02-600x421.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-02-800x561.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-02.jpg 1024w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-15">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-15{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-15 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03.jpg" class="fusion-lightbox" data-rel="iLightbox[ab273f7897aa7ec613d]" data-title="noi-that-can-ho-roman-plaza-03" title="noi-that-can-ho-roman-plaza-03"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03.jpg" width="1513" height="1137" alt="Noi that can ho Roman Plaza 03" class="img-responsive wp-image-13277" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03-200x150.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03-400x301.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03-600x451.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03-800x601.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03-1200x902.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-03.jpg 1513w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-16">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-16{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-16 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-04.jpg" class="fusion-lightbox" data-rel="iLightbox[1c2d9c1111328db311b]" data-title="noi-that-can-ho-roman-plaza-04" title="noi-that-can-ho-roman-plaza-04"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-04.jpg" width="1024" height="827" alt="Noi that can ho Roman Plaza 04" class="img-responsive wp-image-13278" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-04-200x162.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-04-400x323.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-04-600x485.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-04-800x646.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-04.jpg 1024w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-17">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-17{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-17 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05.jpg" class="fusion-lightbox" data-rel="iLightbox[116404cd6ef230ebbac]" data-title="noi-that-can-ho-roman-plaza-05" title="noi-that-can-ho-roman-plaza-05"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05.jpg" width="1372" height="1000" alt="Noi that can ho Roman Plaza 05" class="img-responsive wp-image-13279" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05-200x146.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05-400x292.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05-600x437.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05-800x583.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05-1200x875.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-05.jpg 1372w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-18">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-18{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-18 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06.jpg" class="fusion-lightbox" data-rel="iLightbox[6f337057c9d576d1184]" data-title="noi-that-can-ho-roman-plaza-06" title="noi-that-can-ho-roman-plaza-06"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06.jpg" width="2000" height="1500" alt="Noi that can ho Roman Plaza 06" class="img-responsive wp-image-13280" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06-200x150.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06-400x300.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06-600x450.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06-800x600.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06-1200x900.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-06.jpg 2000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-19">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-19{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-19 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07.jpg" class="fusion-lightbox" data-rel="iLightbox[6e0523c11be366053a9]" data-title="noi-that-can-ho-roman-plaza-07" title="noi-that-can-ho-roman-plaza-07"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07.jpg" width="1334" height="800" alt="Noi that can ho Roman Plaza 07" class="img-responsive wp-image-13281" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07-200x120.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07-400x240.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07-600x360.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07-800x480.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07-1200x720.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-07.jpg 1334w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-last 1_4"  style='margin-top:0px;margin-bottom:20px;width:25%;width:calc(25% - ( ( 4% + 4% + 4% ) * 0.25 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-20">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-20{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-20 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-08.jpg" class="fusion-lightbox" data-rel="iLightbox[c4fa0f9ee67fa985941]" data-title="noi-that-can-ho-roman-plaza-08" title="noi-that-can-ho-roman-plaza-08"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-08.jpg" width="1024" height="863" alt="Noi that can ho Roman Plaza 08" class="img-responsive wp-image-13282" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-08-200x169.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-08-400x337.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-08-600x506.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-08-800x674.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/noi-that-can-ho-roman-plaza-08.jpg 1024w" sizes="(max-width: 800px) 100vw, 400px" /></a></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Liên hệ với Chúng tôi</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                       <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right:4%;'>
                                          <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                             <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                                <h3 class="title-heading-left">VIDEO DỰ ÁN ROMAN PLAZA</h3>
                                                <div class="title-sep-container">
                                                   <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                                                </div>
                                             </div>
                                             <div class="fusion-video fusion-youtube" style="max-width:768px;max-height:432px;">
                                                <div class="video-shortcode">
                                                   <div class="rll-youtube-player" data-id="cG0nCEpJ2hw"></div>
                                                   <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/cG0nCEpJ2hw?wmode=transparent&autoplay=0" width="768" height="432" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                          <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                             <div class="fusion-text">
                                                <p>ĐĂNG KÝ NHẬN BẢNG GIÁ</p>
                                             </div>
                                             <div class="fusion-text">
                                                <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của Dự án Roman Plaza tới Quý khách!</p>
                                             </div>
                                             <p style="text-align: center;">
                                             <div role="form" class="wpcf7" id="wpcf7-f14105-p13012-o1" lang="vi" dir="ltr">
                                                <div class="screen-reader-response"></div>
                                                <form action="/du-roman-plaza#wpcf7-f14105-p13012-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                   <div style="display: none;">
                                                      <input type="hidden" name="_wpcf7" value="14105" />
                                                      <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                      <input type="hidden" name="_wpcf7_locale" value="vi" />
                                                      <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f14105-p13012-o1" />
                                                      <input type="hidden" name="_wpcf7_container_post" value="13012" />
                                                   </div>
                                                   <p><span class="wpcf7-form-control-wrap name-roman-plaza"><input type="text" name="name-roman-plaza" value="Nhập Họ tên..." size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" /></span></p>
                                                   <p><span class="wpcf7-form-control-wrap email-roman-plaza"><input type="email" name="email-roman-plaza" value="Nhập Email..." size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span></p>
                                                   <p><span class="wpcf7-form-control-wrap tel-romanplaza"><input type="tel" name="tel-romanplaza" value="Nhập SĐT..." size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" /></span></p>
                                                   <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                                   <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                      <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                      <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                                   </div>
                                                </form>
                                             </div>
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
         
        
      <!-- CONTENT -->
      
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   </body>
</html>
