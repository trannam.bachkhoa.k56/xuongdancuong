<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->

         <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-12024" class="post-12024 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Nhà ở xã hội The Vesta</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-09-28T11:47:18+00:00</span>                  
                     <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/bg-intro-hai-phat-land-1.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;border-top-width:1px;border-bottom-width:1px;border-color:#eae9e9;border-top-style:solid;border-bottom-style:solid;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/title-nha-o-xa-hoi-the-vesta.png" width="778" height="57" alt="Title Nha o xa hoi The Vesta" title="title-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12744" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/title-nha-o-xa-hoi-the-vesta-200x15.png 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/title-nha-o-xa-hoi-the-vesta-400x29.png 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/title-nha-o-xa-hoi-the-vesta-600x44.png 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/title-nha-o-xa-hoi-the-vesta.png 778w" sizes="(max-width: 800px) 100vw, 778px" /></span></div>
                                       </p>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="font-size: 19px; color: #e1c55b;">Với mong muốn mang lại cơ hội sở hữu nhà ở tới bộ phận dân cư có mức thu nhập trung bình, chúng tôi tạo dựng nên The Vesta –Khu nhà ở xã hội quy mô và tiện ích bậc nhất của khu vực phía Tây Hà Nội. Với nỗ lực của Chủ đầu tư, chúng tôi mong muốn cư dân tương lai của THE VESTA sẽ tận hưởng được giá trị sống lý tưởng bậc nhất. “CHÚNG TÔI NỖ LỰC – VÌ BẠN XỨNG ĐÁNG”</span></p>
                                    </div>
                                    <div class="fusion-button-wrapper fusion-aligncenter">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:rgba(255,255,255,.8);}.fusion-button.button-1 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-1 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-1.button-3d{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.button-1.button-3d:active{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#fffe40;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:#fffe40;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#fffe40;}.fusion-button.button-1{background: #d8a43c;
                                          background-image: -webkit-gradient( linear, left bottom, left top, from( #bc8f34 ), to( #d8a43c ) );
                                          background-image: -webkit-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image:   -moz-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image:     -o-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image: linear-gradient( to top, #bc8f34, #d8a43c );}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #bc8f34;
                                          background-image: -webkit-gradient( linear, left bottom, left top, from( #d8a43c ), to( #bc8f34 ) );
                                          background-image: -webkit-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image:   -moz-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image:     -o-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image: linear-gradient( to top, #d8a43c, #bc8f34 );}.fusion-button.button-1{width:auto;}
                                       </style>
                                       <a class="fusion-button button-3d fusion-button-pill button-large button-custom button-1" target="_self" title="Video du an The Vesta" href="#" data-toggle="modal" data-target=".fusion-modal.modalv3prime"><span class="fusion-button-icon-divider button-icon-divider-left"><i class=" fa fa-video-camera"></i></span><span class="fusion-button-text fusion-button-text-left">Video dự án The Vesta</span></a>
                                    </div>
                                    <div class="fusion-modal modal fade modal-1 modalv3prime" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
                                       <style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
                                       <div class="modal-dialog modal-lg">
                                          <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                                             <div class="modal-header">
                                                <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Video dự án Nhà ở xã hội The Vesta</h3>
                                             </div>
                                             <div class="modal-body fusion-clearfix">
                                                <p style="text-align: center;">
                                                <div class="fusion-video fusion-youtube" style="max-width:880px;max-height:504px;" data-autoplay="1">
                                                   <div class="video-shortcode">
                                                      <div class="rll-youtube-player" data-id="T2iBjSQoGnE"></div>
                                                      <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/T2iBjSQoGnE?wmode=transparent&autoplay=0" width="880" height="504" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h1 style="font-family: UVF-Candlescript-Pro; font-size: 38px; text-align: center; margin-bottom: 0.2em;"><span style="color: #006e61;">Nhà ở xã hội The Vesta</span></h1>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-3 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-nha-o-xa-hoi-the-vesta.png" width="756" height="569" alt="Chu dau tu Chung cu V3 Prime The Vesta" title="chu-dau-tu-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12850" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-nha-o-xa-hoi-the-vesta-200x151.png 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-nha-o-xa-hoi-the-vesta-400x301.png 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-nha-o-xa-hoi-the-vesta-600x452.png 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-nha-o-xa-hoi-the-vesta.png 756w" sizes="(max-width: 800px) 100vw, 756px" /></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <table id="tablepress-3" class="tablepress tablepress-id-3">
                                          <thead>
                                             <tr class="row-1 odd">
                                                <th class="column-1">Tên Dự án</th>
                                                <th class="column-2">Khu Nhà ở xã hội The Vesta</th>
                                             </tr>
                                          </thead>
                                          <tbody class="row-hover">
                                             <tr class="row-2 even">
                                                <td class="column-1">Chủ đầu tư</td>
                                                <td class="column-2">Công ty CP đầu tư Hải Phát</td>
                                             </tr>
                                             <tr class="row-3 odd">
                                                <td class="column-1">Ngân hàng bảo lãnh</td>
                                                <td class="column-2">VietinBank</td>
                                             </tr>
                                             <tr class="row-4 even">
                                                <td class="column-1">Vị trí</td>
                                                <td class="column-2">Số 50 - Phú Lãm - Hà Đông - Hà Nội</td>
                                             </tr>
                                             <tr class="row-5 odd">
                                                <td class="column-1">Diện tích quy hoạch</td>
                                                <td class="column-2">45.000m2</td>
                                             </tr>
                                             <tr class="row-6 even">
                                                <td class="column-1">Mật độ xây dựng</td>
                                                <td class="column-2">Khoảng 44%</td>
                                             </tr>
                                             <tr class="row-7 odd">
                                                <td class="column-1">Quy hoạch dân số</td>
                                                <td class="column-2">Khoảng 4000 người</td>
                                             </tr>
                                             <tr class="row-8 even">
                                                <td class="column-1">Số tầng</td>
                                                <td class="column-2">18 tầng</td>
                                             </tr>
                                             <tr class="row-9 odd">
                                                <td class="column-1">Tầng hầm</td>
                                                <td class="column-2">1 tầng</td>
                                             </tr>
                                             <tr class="row-10 even">
                                                <td class="column-1">Tổng mức đầu tư</td>
                                                <td class="column-2">2300 tỷ đồng</td>
                                             </tr>
                                             <tr class="row-11 odd">
                                                <td class="column-1">Dự kiến bàn giao</td>
                                                <td class="column-2">Quý III - 2017</td>
                                             </tr>
                                          </tbody>
                                       </table>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-5 fusion-no-small-visibility">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-5{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-5 element-bottomshadow hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01.jpg" width="1140" height="50" alt="Hotline Hai Phat Group" title="hai-phat-land-hotline-01" class="img-responsive wp-image-13917" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-200x9.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-400x18.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-600x26.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-800x35.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01.jpg 1140w" sizes="(max-width: 800px) 100vw, 1140px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2017/02/bg-location-hai-phat-land.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">VỊ TRÍ &amp; LIÊN KẾT VÙNG THE VESTA</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-6 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5"  style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-7 hover-type-none fusion-animated" data-animationType="fadeInLeft" data-animationDuration="1.0" data-animationOffset="100%"><a href="https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-nha-o-xa-hoi-the-vesta.png" class="fusion-lightbox" data-rel="iLightbox[1ce5a1b6ddcf61f8ae3]" data-title="vi-tri-nha-o-xa-hoi-the-vesta" title="vi-tri-nha-o-xa-hoi-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-nha-o-xa-hoi-the-vesta.png" width="887" height="592" alt="Vi tri chung cu V3 Prime The Vesta" class="img-responsive wp-image-12851" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-nha-o-xa-hoi-the-vesta-200x133.png 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-nha-o-xa-hoi-the-vesta-400x267.png 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-nha-o-xa-hoi-the-vesta-600x400.png 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-nha-o-xa-hoi-the-vesta-800x534.png 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-nha-o-xa-hoi-the-vesta.png 887w" sizes="(max-width: 800px) 100vw, 887px" /></a></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last 2_5"  style='margin-top:0px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h3 style="margin-top: 0px; text-align: center;">VỊ TRÍ ĐẮC ĐỊA QUỐC LỘ 21B</h3>
                                       <p>Tọa lạc tại vị trí trung tâm Quốc lộ 21B nơi có cơ sở giao thông và hạ tầng đồng bộ được đánh giá cao nhất khu vực Tây Hà Nội</p>
                                       <p>Từ dự án, cư dân có thể dễ dàng tiếp cận các tiện ích, dịch vụ xã hội sẵn có về trường học, bệnh viện, siêu thị, đường giao thông huyết mạch nối vào trung tâm thành phố…</p>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-8 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-nha-o-xa-hoi-the-vesta.png" width="746" height="501" alt="Lien ket vung Chung cu V3 Prime The Vesta" title="lien-ket-vung-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12852" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-nha-o-xa-hoi-the-vesta-200x134.png 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-nha-o-xa-hoi-the-vesta-400x269.png 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-nha-o-xa-hoi-the-vesta-600x403.png 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-nha-o-xa-hoi-the-vesta.png 746w" sizes="(max-width: 800px) 100vw, 746px" /></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">NỘI THẤT &#8211; THIẾT KẾ CĂN HỘ THE VESTA</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-9 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                       <p style="text-align: center;">Để xem chi tiết mặt bằng căn hộ The Vesta, quý khách vui lòng nhấp (click) vào ảnh</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-tabs fusion-tabs-1 clean vertical-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#143c57;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#b7960b;border-top-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style>
                                       <div class="nav">
                                          <ul class="nav-tabs">
                                             <li class="active">
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtÒav3" href="#tab-e97a186fbb8c635c790">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V3</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtÒav6" href="#tab-8a1e437a227ebdb5c41">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V6</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtÒav7" href="#tab-8d748abfd78eccdb533">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V7</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtÒav8" href="#tab-1b23ffb88d03475b65d">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V8</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-nỘithẤtcĂnhỘ" href="#tab-a63a20c796405d78c54">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-home" style="font-size:13px;"></i>NỘI THẤT CĂN HỘ</h4>
                                                </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="tab-content">
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li class="active">
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtÒav3" href="#tab-e97a186fbb8c635c790">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V3</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix in active" id="tab-e97a186fbb8c635c790">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-10">
                                                            <style scoped="scoped">.element-bottomshadow.image-frame-shadow-10{display:inline-block}</style>
                                                            <span class="fusion-imageframe imageframe-bottomshadow imageframe-10 element-bottomshadow hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta.jpg" class="fusion-lightbox" data-rel="iLightbox[efde07c2ae7565cb1e7]" data-title="mat-bang-dien-hinh-toa-v3-the-vesta" title="mat-bang-dien-hinh-toa-v3-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta.jpg" width="2921" height="1500" alt="Mat bang tong the V3 Prime The Vesta" class="img-responsive wp-image-12873" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/mat-bang-dien-hinh-toa-v3-the-vesta.jpg 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtÒav6" href="#tab-8a1e437a227ebdb5c41">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V6</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-8a1e437a227ebdb5c41">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-11">
                                                            <style scoped="scoped">.element-bottomshadow.image-frame-shadow-11{display:inline-block}</style>
                                                            <span class="fusion-imageframe imageframe-bottomshadow imageframe-11 element-bottomshadow hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta.jpg" class="fusion-lightbox" data-rel="iLightbox[47f38025906f813a2da]" data-title="mat-bang-dien-hinh-toa-v6-the-vesta" title="mat-bang-dien-hinh-toa-v6-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta.jpg" width="2921" height="1500" alt="Mat bang tong the Toa V6 The Vesta" class="img-responsive wp-image-13919" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/mat-bang-dien-hinh-toa-v6-the-vesta.jpg 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtÒav7" href="#tab-8d748abfd78eccdb533">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V7</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-8d748abfd78eccdb533">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-12">
                                                            <style scoped="scoped">.element-bottomshadow.image-frame-shadow-12{display:inline-block}</style>
                                                            <span class="fusion-imageframe imageframe-bottomshadow imageframe-12 element-bottomshadow hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta.jpg" class="fusion-lightbox" data-rel="iLightbox[b64a0fc380c492307a0]" data-title="mat-bang-dien-hinh-toa-v7-the-vesta" title="mat-bang-dien-hinh-toa-v7-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta.jpg" width="1920" height="986" alt="Mat bang tong the Toa V7 The Vesta" class="img-responsive wp-image-13550" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v7-the-vesta.jpg 1920w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtÒav8" href="#tab-1b23ffb88d03475b65d">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>MẶT BẰNG TÒA V8</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-1b23ffb88d03475b65d">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-13">
                                                            <style scoped="scoped">.element-bottomshadow.image-frame-shadow-13{display:inline-block}</style>
                                                            <span class="fusion-imageframe imageframe-bottomshadow imageframe-13 element-bottomshadow hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta.jpg" class="fusion-lightbox" data-rel="iLightbox[938ae16539bd098b1a4]" data-title="mat-bang-dien-hinh-toa-v8-the-vesta" title="mat-bang-dien-hinh-toa-v8-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta.jpg" width="1920" height="986" alt="Mat bang tong the Toa V8 The Vesta" class="img-responsive wp-image-13551" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-dien-hinh-toa-v8-the-vesta.jpg 1920w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-nỘithẤtcĂnhỘ" href="#tab-a63a20c796405d78c54">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-home" style="font-size:13px;"></i>NỘI THẤT CĂN HỘ</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-a63a20c796405d78c54">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-14 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-2-nha-o-xa-hoi-the-vesta.jpg" width="1056" height="594" alt="Phong an Nha o xa hoi The Vesta" title="phong-ngu-2-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12885" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-2-nha-o-xa-hoi-the-vesta-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-2-nha-o-xa-hoi-the-vesta-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-2-nha-o-xa-hoi-the-vesta-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-2-nha-o-xa-hoi-the-vesta-800x450.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-2-nha-o-xa-hoi-the-vesta.jpg 1056w" sizes="(max-width: 800px) 100vw, 1056px" /></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-15 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-nha-o-xa-hoi-the-vesta.jpg" width="1056" height="594" alt="Phong khach Nha o xa hoi The Vesta" title="phong-khach-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12884" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-nha-o-xa-hoi-the-vesta-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-nha-o-xa-hoi-the-vesta-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-nha-o-xa-hoi-the-vesta-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-nha-o-xa-hoi-the-vesta-800x450.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-nha-o-xa-hoi-the-vesta.jpg 1056w" sizes="(max-width: 800px) 100vw, 1056px" /></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-16 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-nha-o-xa-hoi-the-vesta.jpg" width="1056" height="594" alt="Khong gian noi that Nha o xa hoi The Vesta" title="phong-ngu-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12886" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-nha-o-xa-hoi-the-vesta-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-nha-o-xa-hoi-the-vesta-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-nha-o-xa-hoi-the-vesta-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-nha-o-xa-hoi-the-vesta-800x450.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-nha-o-xa-hoi-the-vesta.jpg 1056w" sizes="(max-width: 800px) 100vw, 1056px" /></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-17 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-2-nha-o-xa-hoi-the-vesta.jpg" width="1056" height="594" alt="Phong ngu Nha o xa hoi The Vesta" title="phong-khach-2-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12883" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-2-nha-o-xa-hoi-the-vesta-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-2-nha-o-xa-hoi-the-vesta-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-2-nha-o-xa-hoi-the-vesta-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-2-nha-o-xa-hoi-the-vesta-800x450.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-khach-2-nha-o-xa-hoi-the-vesta.jpg 1056w" sizes="(max-width: 800px) 100vw, 1056px" /></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-18 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-tam-nha-o-xa-hoi-the-vesta.jpg" width="1056" height="594" alt="Phong khach Nha o xa hoi The Vesta" title="phong-tam-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-12887" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-tam-nha-o-xa-hoi-the-vesta-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-tam-nha-o-xa-hoi-the-vesta-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-tam-nha-o-xa-hoi-the-vesta-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-tam-nha-o-xa-hoi-the-vesta-800x450.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/02/phong-tam-nha-o-xa-hoi-the-vesta.jpg 1056w" sizes="(max-width: 800px) 100vw, 1056px" /></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-19 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/phong-an-nha-o-xa-hoi-the-vesta.jpg" width="800" height="450" alt="Phong tam Nha o xa hoi The Vesta" title="phong-an-nha-o-xa-hoi-the-vesta" class="img-responsive wp-image-13922" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/phong-an-nha-o-xa-hoi-the-vesta-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/phong-an-nha-o-xa-hoi-the-vesta-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/08/phong-an-nha-o-xa-hoi-the-vesta-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/08/phong-an-nha-o-xa-hoi-the-vesta.jpg 800w" sizes="(max-width: 800px) 100vw, 800px" /></span></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">MẶT BẰNG CĂN HỘ CHI TIẾT THE VESTA</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-20 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-text">
                                       <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-21 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-04-v6-the-vesta.jpg" class="fusion-lightbox" data-rel="iLightbox[3558916c0a5aba05276]" data-title="can-04-v6-the-vesta" title="can-04-v6-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-04-v6-the-vesta.jpg" width="372" height="526" alt="" class="img-responsive wp-image-13914" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-04-v6-the-vesta-200x283.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/can-04-v6-the-vesta.jpg 372w" sizes="(max-width: 800px) 100vw, 372px" /></a></span></div>
                                             </div>
                                          </div>
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-22 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-12-v6-the-vesta.jpg" class="fusion-lightbox" data-rel="iLightbox[b74dce54d4b22919c4b]" data-title="can-12-v6-the-vesta" title="can-12-v6-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-12-v6-the-vesta.jpg" width="372" height="526" alt="" class="img-responsive wp-image-13915" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-12-v6-the-vesta-200x283.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/can-12-v6-the-vesta.jpg 372w" sizes="(max-width: 800px) 100vw, 372px" /></a></span></div>
                                             </div>
                                          </div>
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="fusion-column-content-centered">
                                                   <div class="fusion-column-content">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-23 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-16-v6-the-vesta.jpg" class="fusion-lightbox" data-rel="iLightbox[5cd368982dcf84af65f]" data-title="can-16-v6-the-vesta" title="can-16-v6-the-vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-16-v6-the-vesta.jpg" width="372" height="526" alt="" class="img-responsive wp-image-13916" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/can-16-v6-the-vesta-200x283.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/can-16-v6-the-vesta.jpg 372w" sizes="(max-width: 800px) 100vw, 372px" /></a></span></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-24 fusion-no-small-visibility">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-24{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-24 element-bottomshadow hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01.jpg" width="1140" height="50" alt="Hotline Hai Phat Group" title="hai-phat-land-hotline-01" class="img-responsive wp-image-13917" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-200x9.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-400x18.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-600x26.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01-800x35.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hai-phat-land-hotline-01.jpg 1140w" sizes="(max-width: 800px) 100vw, 1140px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/bg-diamond-hai-phat-land.png");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">CHÍNH SÁCH ƯU ĐÃI &#8211; QUYỀN LỢI KHÁCH HÀNG</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-25 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInUp data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h4><i class="fontawesome-icon  fa fa-gift circle-yes" style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;margin-right:9px;color:#d82731;"></i> CHƯƠNG TRÌNH HỖ TRỢ 1</h4>
                                       <h4 data-fontsize="17" data-lineheight="24"><em><strong>– Nội dung chính sách The Vesta:</strong></em></h4>
                                       <ul>
                                          <li>Khách hàng được hỗ trợ cố định lãi suất vay 5%</li>
                                          <li>Thời gian hỗ trợ: 7 năm</li>
                                          <li>Giá trị hỗ trợ: tối đa 70% giá trị căn hộ</li>
                                          <li>Ngân hàng thực hiện: Vietinbank – Chi nhánh Sông Nhuệ</li>
                                       </ul>
                                       <h4 data-fontsize="17" data-lineheight="24"><em><strong>– Tiến độ nộp tiền The Vesta:</strong></em></h4>
                                       <ul>
                                          <li>Đợt 1: 30% GTHĐ tại thời điểm ký HĐ</li>
                                          <li>Đợt 2: 40% GTHĐ sau 3 tháng kể từ ngày ký HĐ</li>
                                          <li>Đợt 3: 25% GTHĐ sau 11 tháng kể từ ngày ký HĐ</li>
                                          <li>Đợt 4: 5% GTHĐ khi hoàn thiện thủ tục cấp GCN</li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h4><i class="fontawesome-icon  fa fa-gift circle-yes" style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;margin-right:9px;color:#d82731;"></i> CHƯƠNG TRÌNH HỖ TRỢ 2</h4>
                                       <h4 data-fontsize="17" data-lineheight="24"><em><strong>– Nội dung chính sách The Vesta:</strong></em></h4>
                                       <ul>
                                          <li>Đối tượng dành cho những Khách hàng không có nhu cầu vay ngân hàng</li>
                                          <li>Khách hàng được nhận mức chiết khấu là 5% giá trị trước thuế của căn hộ theo hợp đồng</li>
                                          <li>Hình thức thực hiện: giảm trừ tại thời điểm đóng tiền đợt 1</li>
                                       </ul>
                                       <h4 data-fontsize="17" data-lineheight="24"><em><strong>– Tiến độ nộp tiền The Vesta:</strong></em></h4>
                                       <ul>
                                          <li>Đợt 1: 50% GTHĐ tại thời điểm ký HĐ</li>
                                          <li>Đợt 2: 20% GTHĐ sau 3 tháng kể từ ngày ký HĐ</li>
                                          <li>Đợt 3: 25% GTHĐ sau 11 tháng kể từ ngày ký HĐ</li>
                                          <li>Đợt 4: 5% GTHĐ khi hoàn thiện thủ tục cấp GCN</li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/background-tree-hai-phat-land.svg");background-position: right bottom;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  data-animationType=fadeInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first fusion-animated 1_3"  style='margin-top:40px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="margin-bottom: 0px;"><span style="font-family: UVF-Candlescript-Pro; font-size: 38px; color: #006e61;">The Vesta</span></h2>
                                       <h2 style="padding-left: 100px;"><span style="font-family: UVF-Candlescript-Pro; font-size: 38px; color: #006e61;">An cư nơi đất lành</span></h2>
                                       <p><span style="font-family: UTM-Wedding; font-weight: 500; font-size: 25px; color: #b7960b;">Giá trị mà The Vesta mang đến cho khách hàng và những cư dân tương lai không chỉ đơn thuần là một căn hộ thông thường, đó còn là giá trị sống và một không gian xanh &#8211; sống trong lành, gửi cư dân sự nâng tầm cuộc sống. Căn hộ The Vesta tạo nên bởi một hệ thống các dịch vụ tiện ích, giá trị gia tăng và không gian sống trong lành, hài hòa cùng thiên nhiên. The Vesta trở thành điểm sáng trên thị trường nhà ở xã hội mang đến tiêu chuẩn sống mới cho những người lao động có thu nhập thấp.</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-last 2_3"  style='margin-top:0px;margin-bottom:0px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <span style="margin-left:25px;float:right;" class="fusion-imageframe imageframe-none imageframe-26 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/10-gia-tri-cua-the-vesta.svg" width="" height="" alt="" title="10-gia-tri-cua-the-vesta" class="img-responsive wp-image-12892"/></span>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0px;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-question-circle" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">TƯ VẤN NHÀ Ở XÃ HỘI THE VESTA?</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-27 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-flip-boxes flip-boxes row fusion-columns-4">
                                       <div class="fusion-flip-box-wrapper fusion-column col-lg-3 col-md-3 col-sm-3">
                                          <div class="fusion-flip-box fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="flip-box-inner-wrapper">
                                                <div class="flip-box-front" style="background-color:#a2cc55;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-front-inner">
                                                      <div class="flip-box-grafix flip-box-circle" style="background-color:#92be43;border-color:#a2cc55;"><i class=" fa fa-envelope-o fa-flip-horizontal" style="color:#ffffff;"></i></div>
                                                      <h2 class="flip-box-heading" style="color:#ffffff;">Thông tin chuẩn</h2>
                                                      Chúng tôi cam kết cung cấp thông tin đầy đủ và chính xác nhất về dự án Nhà ở xã hội The Vesta...
                                                   </div>
                                                </div>
                                                <div class="flip-box-back" style="background-color:#92be43;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-back-inner">
                                                      <h3 class="flip-box-heading-back" style="color:#eeeded;">Thông tin chuẩn</h3>
                                                      Chúng tôi cam kết cung cấp những thông tin đầy đủ và chính xác nhất về dự án <strong>Nhà ở xã hội &#8211; The Vesta</strong>. Thông tin mở bán, chính sách mua bán và bảng giá liên tục được cập nhật.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-flip-box-wrapper fusion-column col-lg-3 col-md-3 col-sm-3">
                                          <div class="fusion-flip-box fusion-animated" data-animationType="fadeInDown" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="flip-box-inner-wrapper">
                                                <div class="flip-box-front" style="background-color:#74badb;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-front-inner">
                                                      <div class="flip-box-grafix flip-box-circle" style="background-color:#62a7c9;border-color:#74badb;"><i class=" fa fa-balance-scale fa-flip-horizontal" style="color:#ffffff;"></i></div>
                                                      <h2 class="flip-box-heading" style="color:#ffffff;">Tư vấn pháp lý tốt</h2>
                                                      Chúng tôi có kinh nghiệm pháp lý, am hiểu về luật và chính sách mới nhất được ban hành. Quý khách sẽ nhận được
                                                   </div>
                                                </div>
                                                <div class="flip-box-back" style="background-color:#62a7c9;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-back-inner">
                                                      <h3 class="flip-box-heading-back" style="color:#eeeded;">Tư vấn pháp lý tốt</h3>
                                                      <p>Chúng tôi có kinh nghiệm pháp lý, am hiểu luật và chính sách mới ban hành. Qúy khách sẽ nhận được thông tin tư vấn hữu ích nhất về: giấy tờ cần thiết, quy trình đặt cọc và ký kết hợp đồng…</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-flip-box-wrapper fusion-column col-lg-3 col-md-3 col-sm-3">
                                          <div class="fusion-flip-box fusion-animated" data-animationType="flash" data-animationDuration="1" data-animationOffset="bottom-in-view">
                                             <div class="flip-box-inner-wrapper">
                                                <div class="flip-box-front" style="background-color:#d5962c;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-front-inner">
                                                      <div class="flip-box-grafix flip-box-circle" style="background-color:#ca7b25;border-color:#d5962c;"><i class=" fa fa-heart fa-flip-horizontal" style="color:#ffffff;"></i></div>
                                                      <h2 class="flip-box-heading" style="color:#ffffff;">Am hiểu về dự án</h2>
                                                      Chúng tôi am hiểu sâu sắc dự án, lựa chọn thông tin kỹ càng trước khi tư vấn cho khách hàng...
                                                   </div>
                                                </div>
                                                <div class="flip-box-back" style="background-color:#ca7b25;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-back-inner">
                                                      <h3 class="flip-box-heading-back" style="color:#eeeded;">Am hiểu về dự án</h3>
                                                      <p>Chúng tôi am hiểu sâu sắc dự án, lựa chọn thông tin kỹ càng trước khi tư vấn. Tất cả thông tin về tiến độ, chính sách bán hàng <strong>Nhà ở xã hội &#8211; The Vesta </strong>liên tục được cập nhật.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-flip-box-wrapper fusion-column col-lg-3 col-md-3 col-sm-3">
                                          <div class="fusion-flip-box fusion-animated" data-animationType="fadeInUp" data-animationDuration="1" data-animationOffset="bottom-in-view">
                                             <div class="flip-box-inner-wrapper">
                                                <div class="flip-box-front" style="background-color:#05435c;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-front-inner">
                                                      <div class="flip-box-grafix flip-box-circle" style="background-color:#003a51;border-color:#05435c;"><i class=" fa fa-line-chart fa-flip-horizontal" style="color:#ffffff;"></i></div>
                                                      <h2 class="flip-box-heading" style="color:#ffffff;">P. Tích tài chính</h2>
                                                      Chúng tôi nghiên cứu, đánh giá về dòng tiền tài chính từ những kiến thức chuyên môn. Từ đó, chúng tôi...
                                                   </div>
                                                </div>
                                                <div class="flip-box-back" style="background-color:#003a51;border-color:rgba(255,255,255,0);border-radius:4px;border-style:solid;border-width:1px;color:#ffffff;">
                                                   <div class="flip-box-back-inner">
                                                      <h3 class="flip-box-heading-back" style="color:#eeeded;">Phân tích tài chính</h3>
                                                      <p>Chúng tôi nghiên cứu, đánh giá về dòng tiền tài chính từ những kiến thức chuyên môn. Từ đó, chúng tôi sẽ cung cấp cho Quý khách hàng phương án lựa chọn tối ưu nhất.</p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">TIN TỨC &amp; SỰ KIỆN THE VESTA</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-28 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-recent-posts fusion-recent-posts-1 avada-container layout-default layout-columns-4">
                                       <section class="fusion-columns columns fusion-columns-4 columns-4">
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3 fusion-animated" data-animationType="fadeInLeft" data-animationDuration="1.0" data-animationOffset="100%">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-none">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html" class="hover-type-none" aria-label="Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-700x441.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:25:19+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html">Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:25:19+00:00</span><span>8, Tháng Tám, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html#respond">0 Comments</a></span></p>
                                                <p>Tiếp nối thành công từ đợt mở bán tháng 10, ngày 13/11 vừa qua, công ty cổ phần bất động sản Hải [...]</p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3 fusion-animated" data-animationType="fadeInLeft" data-animationDuration="1.0" data-animationOffset="100%">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-none">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/tin-tuc/co-hoi-cuoi-cung-de-mua-nha-xa-hoi-voi-lai-suat-co-dinh-vesta.html" class="hover-type-none" aria-label="Cơ hội cuối cùng để mua nhà xã hội với lãi suất cố định &#8211; The Vesta"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/nha-o-xa-hoi-the-vesta-da-xay-xong-va-dang-hoan-thien.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-04T17:12:28+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/tin-tuc/co-hoi-cuoi-cung-de-mua-nha-xa-hoi-voi-lai-suat-co-dinh-vesta.html">Cơ hội cuối cùng để mua nhà xã hội với lãi suất cố định &#8211; The Vesta</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-04T17:12:28+00:00</span><span>29, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/tin-tuc/co-hoi-cuoi-cung-de-mua-nha-xa-hoi-voi-lai-suat-co-dinh-vesta.html#respond">0 Comments</a></span></p>
                                                <p>Ngay trước Tết nguyên đán 2017, thị trường ghi nhận sự nóng lên cao độ của phân khúc nhà ở xã hội [...]</p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3 fusion-animated" data-animationType="fadeInLeft" data-animationDuration="1.0" data-animationOffset="100%">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-none">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/the-vesta/tien-thi-cong-du-vesta-den-ngay-28022017.html" class="hover-type-none" aria-label="Tiến độ thi công Dự án The Vesta đến ngày 28/02/2017"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/tien-do-du-an-vesta-28-2-2017-7.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:39:01+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/the-vesta/tien-thi-cong-du-vesta-den-ngay-28022017.html">Tiến độ thi công Dự án The Vesta đến ngày 28/02/2017</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:39:01+00:00</span><span>28, Tháng Hai, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/the-vesta/tien-thi-cong-du-vesta-den-ngay-28022017.html#respond">0 Comments</a></span></p>
                                                <p>Dự án The Vesta đang được triển khai hoàn thiện các hạng mục điện nước, trần thạch cao và ốp lát. Dưới [...]</p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3 fusion-animated" data-animationType="fadeInLeft" data-animationDuration="1.0" data-animationOffset="100%">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-none">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/dien-dan/top-3-du-an-dang-song-nhat-cua-hai-phat.html" class="hover-type-none" aria-label="Top 3 dự án đáng sống nhất của Hải Phát"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/avatar-du-an-roman-plaza.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:35:51+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/dien-dan/top-3-du-an-dang-song-nhat-cua-hai-phat.html">Top 3 dự án đáng sống nhất của Hải Phát</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:35:51+00:00</span><span>22, Tháng Mười, 2016</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/dien-dan/top-3-du-an-dang-song-nhat-cua-hai-phat.html#respond">0 Comments</a></span></p>
                                                <p>Trước sự bùng nổ nguồn cung và yêu cầu khắt khe của khách hàng cho một dự án bất động sản, những [...]</p>
                                             </div>
                                          </article>
                                       </section>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">LIÊN HỆ VỚI CHÚNG TÔI</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-29 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                       <p style="text-align: center;">Quý khách vui lòng liên hệ với phòng kinh doanh <a href="https://www.haiphatland.vn/"><b>Khu Nhà ở xã hội The Vesta</b></a> để được sự chăm sóc tốt nhất.</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">PHIM NGẮN &#8220;VÌ EM XỨNG ĐÁNG&#8221; &#8211; THE VESTA</h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-video fusion-youtube" style="max-width:768px;max-height:448px;">
                                       <div class="video-shortcode">
                                          <div class="rll-youtube-player" data-id="2zW1fTrHXtc"></div>
                                          <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/2zW1fTrHXtc?wmode=transparent&autoplay=0" width="768" height="448" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">
                                          ĐĂNG KÝ NHẬN BẢN TIN</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của dự án nhà ở xã hội The Vesta tới Quý khách!</p>
                                    </div>
                                    <div class="fusion-text">
                                       <div role="form" class="wpcf7" id="wpcf7-f11814-p12024-o1" lang="vi" dir="ltr">
                                          <div class="screen-reader-response"></div>
                                          <form action="/nha-o-xa-hoi-the-vesta#wpcf7-f11814-p12024-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                             <div style="display: none;">
                                                <input type="hidden" name="_wpcf7" value="11814" />
                                                <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                <input type="hidden" name="_wpcf7_locale" value="vi" />
                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f11814-p12024-o1" />
                                                <input type="hidden" name="_wpcf7_container_post" value="12024" />
                                             </div>
                                             <p><span class="wpcf7-form-control-wrap text-833"><input type="text" name="text-833" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span></p>
                                             <p><span class="wpcf7-form-control-wrap email-20"><input type="email" name="email-20" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email*" /></span></p>
                                             <p><span class="wpcf7-form-control-wrap tel-146"><input type="tel" name="tel-146" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span></p>
                                             <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                             <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #5a4a42;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/snare-bg-page-head.png");background-position: center center;background-repeat: repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-30 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-tai-lieu-ban-hang.png" width="36" height="50" alt="Tai lieu du an V3 Prime The Vesta" title="icon-tai-lieu-ban-hang" class="img-responsive wp-image-12910"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">TÀI LIỆU DỰ ÁN</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-31 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-tien-do-thi-cong.png" width="31" height="50" alt="Tien do thi cong V3 Prime The Vesta" title="icon-tien-do-thi-cong" class="img-responsive wp-image-12911"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">TIẾN ĐỘ THI CÔNG</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-none imageframe-32"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/logo-hai-phat-invest.png" width="94" height="110" alt="Logo Hai Phat Invest" title="logo-hai-phat-invest" class="img-responsive wp-image-13292"/></span></div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-33 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-tin-tuc.png" width="50" height="50" alt="Tin tuc su kien V3 Prime The Vesta" title="icon-tin-tuc" class="img-responsive wp-image-12912"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">TIN TỨC &amp; SỰ KIỆN</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-34 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-lien-he-tu-van.png" width="50" height="50" alt="Lien he tu van V3 Prime The Vesta" title="icon-lien-he-tu-van" class="img-responsive wp-image-12909"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">LIÊN HỆ TƯ VẤN</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <a href="tel:0938 072 999" class="hotline-sticky">Hotline<br><strong id="txtHotline">0986 205 333</strong></a>
                                    <style>
                                       .hotline-sticky {
                                       background: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/background-hotline-thanh-xuan-complex-hapulico-24t3.png) no-repeat scroll 0 0;
                                       top: 100px;
                                       height: 70px;
                                       position: fixed;
                                       right: 0;
                                       width: 189px;
                                       text-transform: uppercase;
                                       color: #fff;
                                       font-weight: 500;
                                       z-index: 2000;
                                       padding: 5px 0 0 10px;
                                       }
                                       .hotline-sticky strong {
                                       font-size: 18px;
                                       margin-left: 10px;
                                       }
                                    </style>
                                    <div class="fusion-text">
                                       <p style="text-align: center;">Các từ khóa được quan tâm: <strong><a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta">nhà ở xã hội the vesta</a></strong>, <strong><a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta">the vesta</a></strong>, <strong><a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta">dự án the vesta</a></strong>, <strong><a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta">the vesta hải phát</a></strong>,<strong><a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta"> hải phát land</a></strong>, <strong><a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta">sàn hải phát</a></strong>,<strong><a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta"> bất động sản hải phát</a>,<a href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta"> nhà ở xã hội vesta</a></strong></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
         <!-- #main -->
      <!-- CONTENT -->
      
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   </body>
</html>
