<main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-12020" class="post-12020 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Chung cư cao cấp Sky Central</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-05-12T15:38:29+00:00</span>                  
                     <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/bg-intro-hai-phat-land.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;border-top-width:1px;border-bottom-width:1px;border-color:#eae9e9;border-top-style:dotted;border-bottom-style:dotted;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong.png" width="778" height="57" alt="Title Chung cu 176 Dinh Cong" title="title-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13441" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong-200x15.png 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong-400x29.png 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong-600x44.png 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong.png 778w" sizes="(max-width: 800px) 100vw, 778px" /></span></div>
                                       </p>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="font-size: 19px; color: #e1c55b;">Chung cư 176 Định Công sở hữu những tiện ích 5 sao thời thượng cho cuộc sống đẳng cấp của giới thượng lưu. Sống ở vị trí trung tâm thành phố Hà Nội, hòa nhịp cùng sự phát triển của Khu đô thị, cư dân tương lai của Chung cư 176 Định Công sẽ được tận hưởng những giá trị sống lý tưởng bậc nhất.</span></p>
                                    </div>
                                    <div class="fusion-button-wrapper fusion-aligncenter">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:rgba(255,255,255,.8);}.fusion-button.button-1 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-1 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-1.button-3d{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.button-1.button-3d:active{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:rgba(255,255,255,.9);}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-1{background: #d8a43c;
                                          background-image: -webkit-gradient( linear, left bottom, left top, from( #bc8f34 ), to( #d8a43c ) );
                                          background-image: -webkit-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image:   -moz-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image:     -o-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image: linear-gradient( to top, #bc8f34, #d8a43c );}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #bc8f34;
                                          background-image: -webkit-gradient( linear, left bottom, left top, from( #d8a43c ), to( #bc8f34 ) );
                                          background-image: -webkit-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image:   -moz-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image:     -o-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image: linear-gradient( to top, #d8a43c, #bc8f34 );}.fusion-button.button-1{width:auto;}
                                       </style>
                                       <a class="fusion-button button-3d fusion-button-pill button-large button-custom button-1 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view" target="_self" title="Gioi thieu Chu dau tu"><span class="fusion-button-icon-divider button-icon-divider-left"><i class=" fa fa-video-camera"></i></span><span class="fusion-button-text fusion-button-text-left">Video dự án 176 Định Công</span></a>
                                    </div>
                                    <div class="fusion-modal modal fade modal-1 modal176dinhcong" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
                                       <style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
                                       <div class="modal-dialog modal-lg">
                                          <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                                             <div class="modal-header">
                                                <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Giới thiệu dự án chung cư 176 Định Công</h3>
                                             </div>
                                             <div class="modal-body fusion-clearfix">
                                                <p style="text-align: center;">
                                                <div class="fusion-video fusion-youtube" style="max-width:880px;max-height:504px;" data-autoplay="1">
                                                   <div class="video-shortcode">
                                                      <div class="rll-youtube-player" data-id="GU6Evywv1RY"></div>
                                                      <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/GU6Evywv1RY?wmode=transparent&autoplay=0" width="880" height="504" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h1 style="text-align: center;">CHUNG CƯ CAO CẤP 176 ĐỊNH CÔNG</h1>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-3 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-chung-cu-sky-central-176-dinh-cong.png" width="756" height="569" alt="Cac don vi hop tac Chung cu 176 Dinh Cong" title="chu-dau-tu-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13411" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-chung-cu-sky-central-176-dinh-cong-200x151.png 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-chung-cu-sky-central-176-dinh-cong-400x301.png 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-chung-cu-sky-central-176-dinh-cong-600x452.png 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-chung-cu-sky-central-176-dinh-cong.png 756w" sizes="(max-width: 800px) 100vw, 756px" /></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <table id="tablepress-4" class="tablepress tablepress-id-4">
                                          <thead>
                                             <tr class="row-1 odd">
                                                <th class="column-1">Tên Dự án:</th>
                                                <th class="column-2">Chung cư  cao cấp 176 Định Công</th>
                                             </tr>
                                          </thead>
                                          <tbody class="row-hover">
                                             <tr class="row-2 even">
                                                <td class="column-1">Chủ đầu tư:</td>
                                                <td class="column-2">HUD1</td>
                                             </tr>
                                             <tr class="row-3 odd">
                                                <td class="column-1">Bảo lãnh Dự án:</td>
                                                <td class="column-2">Ngân hàng SHB</td>
                                             </tr>
                                             <tr class="row-4 even">
                                                <td class="column-1">Dự kiến bàn giao:</td>
                                                <td class="column-2">Quý 2/2018</td>
                                             </tr>
                                             <tr class="row-5 odd">
                                                <td class="column-1">Vị trí dự án:</td>
                                                <td class="column-2">Mặt đường Vành đai 2.5</td>
                                             </tr>
                                             <tr class="row-6 even">
                                                <td class="column-1">Diện tích khu đất:</td>
                                                <td class="column-2">13461.51 m2</td>
                                             </tr>
                                             <tr class="row-7 odd">
                                                <td class="column-1">Khu cao tầng:</td>
                                                <td class="column-2">11080 m2</td>
                                             </tr>
                                             <tr class="row-8 even">
                                                <td class="column-1">Tầng hầm:</td>
                                                <td class="column-2">3 sàn đỗ xe</td>
                                             </tr>
                                             <tr class="row-9 odd">
                                                <td class="column-1">Tổng số tầng cao:</td>
                                                <td class="column-2">26 tầng</td>
                                             </tr>
                                             <tr class="row-10 even">
                                                <td class="column-1">Tổng số căn hộ:</td>
                                                <td class="column-2">902 căn</td>
                                             </tr>
                                             <tr class="row-11 odd">
                                                <td class="column-1">Mật độ xây dựng</td>
                                                <td class="column-2">40%</td>
                                             </tr>
                                             <tr class="row-12 even">
                                                <td class="column-1">Diện tích để xe</td>
                                                <td class="column-2">19336 m2</td>
                                             </tr>
                                             <tr class="row-13 odd">
                                                <td class="column-1">Diện tích nhà trẻ</td>
                                                <td class="column-2">645 m2</td>
                                             </tr>
                                          </tbody>
                                       </table>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-5 fusion-no-small-visibility">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-5{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-5 element-bottomshadow hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01.jpg" width="1140" height="50" alt="Hotline Hai Phat Group" title="hai-phat-land-hotline-01" class="img-responsive wp-image-12741" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-200x9.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-400x18.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-600x26.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-800x35.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01.jpg 1140w" sizes="(max-width: 800px) 100vw, 1140px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatgroup.com/wp-content/uploads/2016/11/background-container-hai-phat-group.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">VỊ TRÍ &amp; LIÊN KẾT VÙNG 176 ĐỊNH CÔNG</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-6 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5"  style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-7 hover-type-none fusion-animated" data-animationType="zoomInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-du-an-chung-cu-176-dinh-cong.png" width="2196" height="2022" alt="Vi tri chung cu 176 Dinh Cong" title="vi-tri-du-an-chung-cu-176-dinh-cong" class="img-responsive wp-image-13412" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-du-an-chung-cu-176-dinh-cong-200x184.png 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-du-an-chung-cu-176-dinh-cong-400x368.png 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-du-an-chung-cu-176-dinh-cong-600x552.png 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-du-an-chung-cu-176-dinh-cong-800x737.png 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-du-an-chung-cu-176-dinh-cong-1200x1105.png 1200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/vi-tri-du-an-chung-cu-176-dinh-cong.png 2196w" sizes="(max-width: 800px) 100vw, 1200px" /></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=zoomInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last fusion-animated 2_5"  style='margin-top:0px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h3 style="text-align: center;">VỊ TRÍ GIAO THÔNG HUYẾT MẠCH</h3>
                                       <p>Tọa lạc tại mặt đường Vành Đai 2.5, cách ngã tư Kim Đồng &#8211; Giải Phóng 500m, nơi có giao thông vô cùng thuận lợi, kết nối các tuyến đường huyết mạch Giải Phóng &#8211; Nguyễn Trãi, Vành Đai 2, Vành Đai 3&#8230;</p>
                                       <p>Từ dự án, cư dân có thể dễ dàng tiếp cận các tiện ích, dịch vụ xã hội sẵn có về trường học, bệnh viện, siêu thị:</p>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-8 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-chung-cu-176-dinh-cong.png" width="2070" height="2008" alt="Lien ket vung Chung cu V3 Prime The Vesta" title="lien-ket-vung-chung-cu-176-dinh-cong" class="img-responsive wp-image-13413" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-chung-cu-176-dinh-cong-200x194.png 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-chung-cu-176-dinh-cong-400x388.png 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-chung-cu-176-dinh-cong-600x582.png 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-chung-cu-176-dinh-cong-800x776.png 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-chung-cu-176-dinh-cong-1200x1164.png 1200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/lien-ket-vung-chung-cu-176-dinh-cong.png 2070w" sizes="(max-width: 800px) 100vw, 800px" /></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">TIỆN ÍCH HOÀN HẢO</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-9 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInDown data-animationDuration=1.0 data-animationOffset=50% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-10 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong.jpg" width="1094" height="656" alt="" title="trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13457" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong-200x120.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong-400x240.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong-600x360.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong-800x480.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong.jpg 1094w" sizes="(max-width: 800px) 100vw, 1094px" /></span></div>
                                             </div>
                                          </div>
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-11 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/tien-ich-vuon-hoa-dai-phun-nuoc-chung-cu-sky-central-176-dinh-cong.jpg" width="1094" height="656" alt="" title="tien-ich-vuon-hoa-dai-phun-nuoc-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13456" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/tien-ich-vuon-hoa-dai-phun-nuoc-chung-cu-sky-central-176-dinh-cong-200x120.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/tien-ich-vuon-hoa-dai-phun-nuoc-chung-cu-sky-central-176-dinh-cong-400x240.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/tien-ich-vuon-hoa-dai-phun-nuoc-chung-cu-sky-central-176-dinh-cong-600x360.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/tien-ich-vuon-hoa-dai-phun-nuoc-chung-cu-sky-central-176-dinh-cong-800x480.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/tien-ich-vuon-hoa-dai-phun-nuoc-chung-cu-sky-central-176-dinh-cong.jpg 1094w" sizes="(max-width: 800px) 100vw, 1094px" /></span></div>
                                             </div>
                                          </div>
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-12 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong.jpg" width="1800" height="1080" alt="" title="khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13454" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong-200x120.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong-400x240.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong-600x360.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong-800x480.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong-1200x720.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-lien-hop-the-thao-chung-cu-sky-central-176-dinh-cong.jpg 1800w" sizes="(max-width: 800px) 100vw, 1200px" /></span></div>
                                             </div>
                                          </div>
                                       </div>
                                       </p>
                                       <p style="text-align: center;">
                                       <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-13 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/he-thong-an-ninh-chung-cu-sky-central-176-dinh-cong.jpg" width="1094" height="656" alt="" title="he-thong-an-ninh-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13453" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/he-thong-an-ninh-chung-cu-sky-central-176-dinh-cong-200x120.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/he-thong-an-ninh-chung-cu-sky-central-176-dinh-cong-400x240.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/he-thong-an-ninh-chung-cu-sky-central-176-dinh-cong-600x360.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/he-thong-an-ninh-chung-cu-sky-central-176-dinh-cong-800x480.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/he-thong-an-ninh-chung-cu-sky-central-176-dinh-cong.jpg 1094w" sizes="(max-width: 800px) 100vw, 1094px" /></span></div>
                                             </div>
                                          </div>
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-14 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-vui-choi-tre-em-chung-cu-sky-central-176-dinh-cong.jpg" width="1094" height="656" alt="" title="khu-vui-choi-tre-em-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13455" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-vui-choi-tre-em-chung-cu-sky-central-176-dinh-cong-200x120.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-vui-choi-tre-em-chung-cu-sky-central-176-dinh-cong-400x240.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-vui-choi-tre-em-chung-cu-sky-central-176-dinh-cong-600x360.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-vui-choi-tre-em-chung-cu-sky-central-176-dinh-cong-800x480.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/khu-vui-choi-tre-em-chung-cu-sky-central-176-dinh-cong.jpg 1094w" sizes="(max-width: 800px) 100vw, 1094px" /></span></div>
                                             </div>
                                          </div>
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-15 hover-type-zoomin"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/be-boi-trong-nha-chung-cu-sky-central-176-dinh-cong.jpg" width="1094" height="656" alt="" title="be-boi-trong-nha-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13452" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/be-boi-trong-nha-chung-cu-sky-central-176-dinh-cong-200x120.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/be-boi-trong-nha-chung-cu-sky-central-176-dinh-cong-400x240.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/be-boi-trong-nha-chung-cu-sky-central-176-dinh-cong-600x360.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/be-boi-trong-nha-chung-cu-sky-central-176-dinh-cong-800x480.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/be-boi-trong-nha-chung-cu-sky-central-176-dinh-cong.jpg 1094w" sizes="(max-width: 800px) 100vw, 1094px" /></span></div>
                                             </div>
                                          </div>
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">MẶT BẰNG TỔNG THỂ DỰ ÁN 176 ĐỊNH CÔNG</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-16 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-17">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-17{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-17 element-bottomshadow hover-type-none fusion-animated" data-animationType="zoomIn" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong.jpg" width="1970" height="1040" alt="Mat bang tong quan Chung cu 176 Dinh Cong" title="mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13450" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong-200x106.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong-400x211.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong-600x317.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong-800x422.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong-1200x634.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-tong-the-chung-cu-sky-central-176-dinh-cong.jpg 1970w" sizes="(max-width: 800px) 100vw, 1970px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">NỘI THẤT CAO CẤP &#8211; THIẾT KẾ TINH TẾ</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-18 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-tabs fusion-tabs-1 clean vertical-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#e8943b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#5a4a42;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ebeaea;border-top-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#e8943b;}</style>
                                       <div class="nav">
                                          <ul class="nav-tabs">
                                             <li class="active">
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-nỘithẤtcĂnhỘ" href="#tab-c37aba6d67ba0440c33">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-home" style="font-size:13px;"></i>NỘI THẤT CĂN HỘ</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-thiẾtkẾmẶtbẰng" href="#tab-c9e8aace7b557be3e1d">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>THIẾT KẾ MẶT BẰNG</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngcĂnhỘ" href="#tab-dc8a6995f644c33fd70">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-university" style="font-size:13px;"></i>MẶT BẰNG CĂN HỘ</h4>
                                                </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="tab-content">
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li class="active">
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-nỘithẤtcĂnhỘ" href="#tab-c37aba6d67ba0440c33">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-home" style="font-size:13px;"></i>NỘI THẤT CĂN HỘ</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix in active" id="tab-c37aba6d67ba0440c33">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-19 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2017/02/phong-ngu-2-nha-o-xa-hoi-the-vesta.jpg" width="" height="" alt="Phong an Nha o xa hoi The Vesta" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-20 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2017/02/phong-khach-nha-o-xa-hoi-the-vesta.jpg" width="" height="" alt="Phong khach Nha o xa hoi The Vesta" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-21 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2017/02/phong-ngu-nha-o-xa-hoi-the-vesta.jpg" width="" height="" alt="Khong gian noi that Nha o xa hoi The Vesta" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-22 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2017/02/phong-khach-2-nha-o-xa-hoi-the-vesta.jpg" width="" height="" alt="Phong ngu Nha o xa hoi The Vesta" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-23 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2017/02/phong-tam-nha-o-xa-hoi-the-vesta.jpg" width="" height="" alt="Phong khach Nha o xa hoi The Vesta" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-24 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2017/02/phong-an-nha-o-xa-hoi-the-vesta.jpg" width="" height="" alt="Phong tam Nha o xa hoi The Vesta" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-thiẾtkẾmẶtbẰng" href="#tab-c9e8aace7b557be3e1d">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-tasks" style="font-size:13px;"></i>THIẾT KẾ MẶT BẰNG</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-c9e8aace7b557be3e1d">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-25">
                                                            <style scoped="scoped">.element-bottomshadow.image-frame-shadow-25{display:inline-block}</style>
                                                            <span class="fusion-imageframe imageframe-bottomshadow imageframe-25 element-bottomshadow hover-type-zoomin"><a href="http://haiphatgroup.com/wp-content/uploads/2016/11/mat-bang-toa-B-chung-cu-176-dinh-cong.jpg" class="fusion-lightbox" data-rel="iLightbox[4361c31397483e0359e]"><img src="https://haiphatgroup.com/wp-content/uploads/2016/11/mat-bang-toa-B-chung-cu-176-dinh-cong.jpg" width="" height="" alt="Mat bang tong the V3 Prime The Vesta" class="img-responsive"/></a></span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngcĂnhỘ" href="#tab-dc8a6995f644c33fd70">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-university" style="font-size:13px;"></i>MẶT BẰNG CĂN HỘ</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-dc8a6995f644c33fd70">
                                             <p style="text-align: center;">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-26 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2016/11/can-ho-01-chung-cu-176-dinh-cong.jpg" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-27 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2016/11/can-ho-02-chung-cu-176-dinh-cong.jpg" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-28 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2016/11/can-ho-03-chung-cu-176-dinh-cong.jpg" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                             </div>
                                             <p style="text-align: center;">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-29 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2016/11/can-ho-04-chung-cu-176-dinh-cong.jpg" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-30 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2016/11/can-ho-05-chung-cu-176-dinh-cong.jpg" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-31 hover-type-zoomin"><img src="https://haiphatgroup.com/wp-content/uploads/2016/11/can-ho-06-chung-cu-176-dinh-cong.jpg" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-32 fusion-no-small-visibility">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-32{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-32 element-bottomshadow hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01.jpg" width="1140" height="50" alt="Hotline Hai Phat Group" title="hai-phat-land-hotline-01" class="img-responsive wp-image-12741" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-200x9.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-400x18.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-600x26.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01-800x35.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/hai-phat-land-hotline-01.jpg 1140w" sizes="(max-width: 800px) 100vw, 1140px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/bg-diamond-hai-phat-land.png");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">CHÍNH SÁCH ƯU ĐÃI &#8211; QUYỀN LỢI KHÁCH HÀNG</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-33 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h4><i class="fontawesome-icon  fa fa-gift circle-yes" style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;margin-right:9px;color:#d82731;"></i> VAY VỐN NGÂN HÀNG</h4>
                                       <p><strong>Ngân hàng SHB bảo lãnh tiến độ Dự án:</strong></p>
                                       <ul>
                                          <li><strong>Ngân hàng SHB hỗ  trợ vay lên đến 70% giá trị căn hộ</strong></li>
                                          <li>Ân hạn nợ gốc năm đầu.</li>
                                          <li>Lãi suất 7% / năm</li>
                                          <li>Miễn phí trả nợ trước hạn.</li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInUp data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h4><i class="fontawesome-icon  fa fa-gift circle-yes" style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;margin-right:9px;color:#d82731;"></i> GIÁ BÁN &amp; CƠ CẤU CĂN HỘ</h4>
                                       <p><strong><i><span lang="VI">Giá bán hấp dẫn, thiết kế đẹp, đa dạng, tối ưu hóa</span></i>:</strong></p>
                                       <ul>
                                          <li>Giá bán<b> chỉ từ </b>25tr/m2.</li>
                                          <li>Mỗi tầng được thiết kế 22 Căn hộ vuông vắn.</li>
                                          <li>Bao gồm 5 loại diện tích<b>: 64m2; 68m2; 71m2; 80m2; 92m2; 99m2</b>.</li>
                                          <li>Số lượng thang máy:<b> 10 thang máy/sàn.</b></li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h4><i class="fontawesome-icon  fa fa-gift circle-yes" style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;margin-right:9px;color:#d82731;"></i> TIẾN ĐỘ THANH TOÁN</h4>
                                       <p><strong><i><span lang="VI">Hỗ trợ thanh toán linh hoạt</span></i>:</strong></p>
                                       <ul>
                                          <li>Đợt 1: <b>10% Giá trị căn hộ, ký trực tiếp với Chủ đầu tư.</b></li>
                                          <li>Các đợt đóng tiền tiếp theo linh hoạt.</li>
                                          <li>Nhận bàn giao Căn hộ mới chỉ phải nộp <strong>95%</strong>, nhận <strong>SỔ ĐỎ</strong> mới phải nộp <strong>5%</strong> còn lại.</li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatgroup.com/wp-content/uploads/2016/11/background-container-hai-phat-group.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">TẠI SAO NÊN LỰA CHỌN CHUNG CƯ CAO CẤP 176 ĐỊNH CÔNG?</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-34 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span style="border:15px solid #c8943b;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-35 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong.jpg" width="1271" height="871" alt="Phoi canh Chung cu V3 prime The Vesta" title="phoi-canh-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13444" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong-200x137.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong-400x274.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong-600x411.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong-800x548.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong-1200x822.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong.jpg 1271w" sizes="(max-width: 800px) 100vw, 600px" /></span></div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:5px;">
                                       <h3 class="title-heading-left">5 lý do lựa chọn Chung cư 176 Định Công</p></h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <style type="text/css" scoped="scoped">.fusion-accordian  #accordion-12020-1 .panel-title a .fa-fusion-box{ color: #ffffff;}.fusion-accordian  #accordion-12020-1 .panel-title a .fa-fusion-box:before{ font-size: 13px; width: 13px;}.fusion-accordian  #accordion-12020-1 .panel-title a{font-size:17px;}.fusion-accordian  #accordion-12020-1 .fa-fusion-box { background-color: #333333;border-color: #333333;}.fusion-accordian  #accordion-12020-1 .panel-title a:hover, #accordion-12020-1 .fusion-toggle-boxed-mode:hover .panel-title a { color: #b7960b;}.fusion-accordian  #accordion-12020-1 .panel-title .active .fa-fusion-box,.fusion-accordian  #accordion-12020-1 .panel-title a:hover .fa-fusion-box { background-color: #b7960b!important;border-color: #b7960b!important;}</style>
                                    <div class="accordian fusion-accordian">
                                       <div class="panel-group" id="accordion-12020-1">
                                          <div class="fusion-panel panel-default">
                                             <div class="panel-heading">
                                                <h4 class="panel-title toggle">
                                                   <a class="active" data-toggle="collapse" data-parent="#accordion-12020-1" data-target="#2d41853b507222f94" href="#2d41853b507222f94">
                                                      <div class="fusion-toggle-icon-wrapper"><i class="fa-fusion-box"></i></div>
                                                      <div class="fusion-toggle-heading">Dự án được xây dựng bởi Chủ đầu tư uy tín</div>
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="2d41853b507222f94" class="panel-collapse collapse in">
                                                <div class="panel-body toggle-content fusion-clearfix">
                                                   <p>HUD1 là một thành viên của Tổng công ty Đầu tư Phát triển nhà và đô thị HUD trực thuộc Bộ xây dựng. Dạo quanh trên các khu phố của Hà Nội, không khó để bắt gặp các biển hiệu mang tên HUD – đó như là một minh chứng, một cam kết của CĐT.</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-panel panel-default">
                                             <div class="panel-heading">
                                                <h4 class="panel-title toggle">
                                                   <a data-toggle="collapse" data-parent="#accordion-12020-1" data-target="#191b11360fe661524" href="#191b11360fe661524">
                                                      <div class="fusion-toggle-icon-wrapper"><i class="fa-fusion-box"></i></div>
                                                      <div class="fusion-toggle-heading">Vị trí đắc địa - Giao thông thuận tiên</div>
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="191b11360fe661524" class="panel-collapse collapse ">
                                                <div class="panel-body toggle-content fusion-clearfix">
                                                   Dự án nằm trên mặt đường Vành Đai 2.5, giao thông vô cùng thuận lợi. Khách hàng có thể dễ dàng đi tới Trung tâm Thành phố: Quận Hoàn Kiếm, Quận Ba Đình chỉ với 9 phút lái xe; đi tới Bến xe Giáp Bát, bến xe Nước Ngầm chỉ mất 5 phút&#8230;</p>
                                                   <p style="text-align: center;">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-panel panel-default">
                                             <div class="panel-heading">
                                                <h4 class="panel-title toggle">
                                                   <a data-toggle="collapse" data-parent="#accordion-12020-1" data-target="#4e73b6dae2b7b3835" href="#4e73b6dae2b7b3835">
                                                      <div class="fusion-toggle-icon-wrapper"><i class="fa-fusion-box"></i></div>
                                                      <div class="fusion-toggle-heading">Thiết kế căn hộ hợp lý, ngập tràn ánh sáng tự nhiên và gió trời</div>
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="4e73b6dae2b7b3835" class="panel-collapse collapse ">
                                                <div class="panel-body toggle-content fusion-clearfix">
                                                   Cơ cấu diện tích căn hộ của dự án khá hợp lý. Các căn hộ 2 Phòng Ngủ từ 65 &#8211; 71 m2. Các căn 3 Phòng Ngủ từ 80 &#8211; 100 m2. Các phòng chức năng căn hộ được phân chia hợp lý với nhiều mặt thoáng, tận dụng ánh sáng tự nhiên.</p>
                                                   <p style="text-align: center;">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-panel panel-default">
                                             <div class="panel-heading">
                                                <h4 class="panel-title toggle">
                                                   <a data-toggle="collapse" data-parent="#accordion-12020-1" data-target="#10ae11743e1f21de7" href="#10ae11743e1f21de7">
                                                      <div class="fusion-toggle-icon-wrapper"><i class="fa-fusion-box"></i></div>
                                                      <div class="fusion-toggle-heading">Tiện ích đáp ứng mọi nhu cầu của khách hàng</div>
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="10ae11743e1f21de7" class="panel-collapse collapse ">
                                                <div class="panel-body toggle-content fusion-clearfix">
                                                   Mật độ xây dựng chỉ chiếm 35%, rõ ràng, không gian cảnh quan thiên nhiên nhiên, các khu vực chức năng khác sẽ được đảm bảo. Ngoài ra, trong dự án còn có khu liền kề Shophouse sẽ làm gia tăng các dịch vụ, tiện ích cho cả dự án.</p>
                                                   <p style="text-align: center;">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-panel panel-default">
                                             <div class="panel-heading">
                                                <h4 class="panel-title toggle">
                                                   <a data-toggle="collapse" data-parent="#accordion-12020-1" data-target="#ac708816eeaf6294b" href="#ac708816eeaf6294b">
                                                      <div class="fusion-toggle-icon-wrapper"><i class="fa-fusion-box"></i></div>
                                                      <div class="fusion-toggle-heading">Mức giá hấp dẫn dành cho Quý khách hàng</div>
                                                   </a>
                                                </h4>
                                             </div>
                                             <div id="ac708816eeaf6294b" class="panel-collapse collapse ">
                                                <div class="panel-body toggle-content fusion-clearfix">
                                                   Với mức giá 25 triệu/m2 – Quý khách sở hữu căn hộ 2 Phòng ngủ 70 m2 chỉ với 1,6 tỷ đồng. So sánh với 1 số dự án xung quanh như: 360 Giải Phóng, CT36 Dream Home thì đây là một mức giá phù hợp.</p>
                                                   <p style="text-align: center;">
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">LIÊN HỆ VỚI CHÚNG TÔI</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-36 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">
                                          VIDEO DỰ ÁN 176 ĐỊNH CÔNG</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-video fusion-youtube" style="max-width:768px;max-height:448px;">
                                       <div class="video-shortcode">
                                          <div class="rll-youtube-player" data-id="tH_Je_4U9jo"></div>
                                          <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/tH_Je_4U9jo?wmode=transparent&autoplay=0" width="768" height="448" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">
                                          ĐĂNG KÝ NHẬN BẢN TIN</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của dự án chung cư 176 Định Công &#8211; Hoàng Mai tới Quý khách!</p>
                                       <p style="text-align: center;">
                                    </div>
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div role="form" class="wpcf7" id="wpcf7-f11814-p12020-o1" lang="vi" dir="ltr">
                                          <div class="screen-reader-response"></div>
                                          <form action="/chung-cu-cao-cap-sky-central#wpcf7-f11814-p12020-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                             <div style="display: none;">
                                                <input type="hidden" name="_wpcf7" value="11814" />
                                                <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                <input type="hidden" name="_wpcf7_locale" value="vi" />
                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f11814-p12020-o1" />
                                                <input type="hidden" name="_wpcf7_container_post" value="12020" />
                                             </div>
                                             <p><span class="wpcf7-form-control-wrap text-833"><input type="text" name="text-833" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span></p>
                                             <p><span class="wpcf7-form-control-wrap email-20"><input type="email" name="email-20" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email*" /></span></p>
                                             <p><span class="wpcf7-form-control-wrap tel-146"><input type="tel" name="tel-146" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span></p>
                                             <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                             <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                             </div>
                                          </form>
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #5a4a42;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/snare-bg-page-head.png");background-position: center center;background-repeat: repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-first fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-37 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-tai-lieu-ban-hang.png" width="36" height="50" alt="Tai lieu du an V3 Prime The Vesta" title="icon-tai-lieu-ban-hang" class="img-responsive wp-image-12910"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">TÀI LIỆU DỰ ÁN</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-38 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-tien-do-thi-cong.png" width="31" height="50" alt="Tien do thi cong V3 Prime The Vesta" title="icon-tien-do-thi-cong" class="img-responsive wp-image-12911"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">TIẾN ĐỘ THI CÔNG</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-39 hover-type-none fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/hud-logo.png" width="167" height="105" alt="Logo HUD Chung cu 176 Dinh Cong" title="hud-logo" class="img-responsive wp-image-13405"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-40 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-tin-tuc.png" width="50" height="50" alt="Tin tuc su kien V3 Prime The Vesta" title="icon-tin-tuc" class="img-responsive wp-image-12912"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">TIN TỨC &amp; SỰ KIỆN</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_5  fusion-one-fifth fusion-column-last fusion-animated 1_5"  style='margin-top:0px;margin-bottom:0px;width:20%;width:calc(20% - ( ( 4% + 4% + 4% + 4% ) * 0.2 ) );'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-41 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/icon-lien-he-tu-van.png" width="50" height="50" alt="Lien he tu van V3 Prime The Vesta" title="icon-lien-he-tu-van" class="img-responsive wp-image-12909"/></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #c8943b;">LIÊN HỆ TƯ VẤN</span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <a href="tel:0986 205 333" class="hotline-sticky">Hotline<br><strong id="txtHotline">0986 205 333</strong></a>
                                    <style>
                                       .hotline-sticky {
                                       background: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/background-hotline-thanh-xuan-complex-hapulico-24t3.png) no-repeat scroll 0 0;
                                       top: 100px;
                                       height: 70px;
                                       position: fixed;
                                       right: 0;
                                       width: 189px;
                                       text-transform: uppercase;
                                       color: #fff;
                                       font-weight: 500;
                                       z-index: 2000;
                                       padding: 5px 0 0 10px;
                                       }
                                       .hotline-sticky strong {
                                       font-size: 18px;
                                       margin-left: 10px;
                                       }
                                    </style>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>