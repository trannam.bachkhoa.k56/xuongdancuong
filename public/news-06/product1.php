<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->
<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-center">
            <div class="fusion-page-title-row">
               <div class="fusion-page-title-wrapper">
                  <div class="fusion-page-title-captions">
                     <h1 class="entry-title">Dự án khu đô thị Thuận Thành 3 Bắc Ninh</h1>
                     <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs"><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="https://www.haiphatland.vn"><span itemprop="title">Home</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Dự án khu đô thị Thuận Thành 3 Bắc Ninh</span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <main id="main" role="main" class="clearfix " style="">
            <div class="fusion-row" style="">
               <section id="content" style="width: 100%;">
                  <div id="post-14111" class="post-14111 page type-page status-publish hentry">
                     <span class="entry-title rich-snippet-hidden">Dự án khu đô thị Thuận Thành 3 Bắc Ninh</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2018-09-02T16:01:39+00:00</span>                                                       
                     <div class="post-content">
                        <blockquote class="wp-block-quote">
                           <p>Nơi yên bình giữa xứ kinh bắc ngàn năm văn hiến</p>
                        </blockquote>
                        <h2>Tổng quan dự án Thuận Thành 3</h2>
                        <figure class="wp-block-image alignnone size-full wp-image-567">
                           <img class="wp-image-567" src="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-phoi-canh-tong-the.jpg" sizes="(max-width: 800px) 100vw, 800px" srcset="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-phoi-canh-tong-the.jpg 800w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-phoi-canh-tong-the-300x169.jpg 300w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-phoi-canh-tong-the-768x432.jpg 768w" alt="Phối cảnh tổng thế dự án khu đô thị Thuận Thành 3 tỉnh Bắc Ninh" />
                           <figcaption>Phối cảnh tổng thế dự án khu đô thị Thuận Thành 3 tỉnh Bắc Ninh</figcaption>
                        </figure>
                        <ul>
                           <li>Tên dự án: <a href="http://lamrealtor.com/khac/20180902-khu-cong-nghiep-va-nha-o-do-thi-thuan-thanh-3-bac-ninh/">Khu công nghiệp kết hợp nhà ở khu đô thị Thuận Thành 3</a></li>
                           <li>Vị trí: Mặt đường QL 17 đoạn thuộc phố Tam Á – Xã Gia Đông – Huyện Thuận Thành – Tỉnh Bắc Ninh</li>
                           <li>Chủ đầu tư: công ty cổ phần đầu tư Trung Quý</li>
                           <li>Tổng diện tích: 70 ha</li>
                           <li>Quy mô: Khu nhà ở chung cư, nhà ở xã hội, công viên, hồ điều hòa, bể bơi và hơn 1500 lô đất</li>
                           <li>Mật độ xây dựng: 40%</li>
                           <li>Hình thức sở hữu: Sổ đỏ lâu dài</li>
                        </ul>
                        <h2>Vị trí dự án khu đô thị Thuận Thành Bắc Ninh</h2>
                        <figure class="wp-block-image alignnone size-full wp-image-568">
                           <img class="wp-image-568" src="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-quy-hoach-tong-the-cac-khu-do-thi-cong-nghiep.jpg" sizes="(max-width: 800px) 100vw, 800px" srcset="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-quy-hoach-tong-the-cac-khu-do-thi-cong-nghiep.jpg 800w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-quy-hoach-tong-the-cac-khu-do-thi-cong-nghiep-300x201.jpg 300w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-quy-hoach-tong-the-cac-khu-do-thi-cong-nghiep-768x515.jpg 768w" alt="Quy hoạch tổng thể các khu đô thị và công nghiệp tỉnh Bắc Ninh" />
                           <figcaption>Quy hoạch tổng thể các khu đô thị và công nghiệp tỉnh Bắc Ninh</figcaption>
                        </figure>
                        <p>Nằm cách trung tâm Hà Nội 27km về phía Đông Bắc, <a href="http://lamrealtor.com/khac/20180902-khu-cong-nghiep-va-nha-o-do-thi-thuan-thanh-3-bac-ninh/">Khu Đô thị Thuận Thành III</a>, phân khu B, dự án thuộc địa phận xã Gia Đông, nằm dọc theo Quốc lộ 17 – trục đường lõi của huyện Thuận Thành. Theo quy hoạch và định hướng phát triển kinh tế vùng thì quốc lộ 17 là trục giao thông huyết mạch nối liền khu đô thị với Hà Nội về hướng Đông và các huyện của tỉnh về hướng Tây.</p>
                        <figure>
                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9272907909185!2d106.07191371477117!3d21.035595085994668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a16d0dfdc2a3%3A0x16b12b76c18f3b05!2zS2h1IMSRw7QgdGjhu4sgVGh14bqtbiBUaMOgbmggMw!5e0!3m2!1svi!2s!4v1535877344511" width="600" height="450" allowfullscreen="allowfullscreen" data-mce-fragment="1" data-rocket-lazyload="fitvidscompatible" data-lazy-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9272907909185!2d106.07191371477117!3d21.035595085994668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a16d0dfdc2a3%3A0x16b12b76c18f3b05!2zS2h1IMSRw7QgdGjhu4sgVGh14bqtbiBUaMOgbmggMw!5e0!3m2!1svi!2s!4v1535877344511"></iframe>
                           <noscript><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9272907909185!2d106.07191371477117!3d21.035595085994668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a16d0dfdc2a3%3A0x16b12b76c18f3b05!2zS2h1IMSRw7QgdGjhu4sgVGh14bqtbiBUaMOgbmggMw!5e0!3m2!1svi!2s!4v1535877344511" width="600" height="450" allowfullscreen="allowfullscreen" data-mce-fragment="1"></iframe></noscript>
                        </figure>
                        <p><a href="http://lamrealtor.com/khac/20180902-khu-cong-nghiep-va-nha-o-do-thi-thuan-thanh-3-bac-ninh/">Khu đô thị Thuận Thành 3 Bắc Ninh</a> nằm trong quần thể 4 khu công nghiệp sầm uất, khu công nghiệp thuận thành 1, thuận thành 2, khu công nghiệp Khai Sơn đã đi vào sử dụng. Khu công nghiệp Thuận Thành 3 với quy mô lên tới 240 ha đang được triển khai song song với dự án. Khi hình thành đây sẽ là trung tâm phát triển nhanh và mang lại giá trị kinh doanh lớn</p>
                        <figure class="wp-block-image alignnone size-full wp-image-569">
                           <img class="wp-image-569" src="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-vi-tri-du-an-khu-do-thi.jpg" sizes="(max-width: 800px) 100vw, 800px" srcset="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-vi-tri-du-an-khu-do-thi.jpg 800w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-vi-tri-du-an-khu-do-thi-300x240.jpg 300w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-vi-tri-du-an-khu-do-thi-768x614.jpg 768w" alt="vị trí các khu đô thị và công nghiệp Bắc Ninh" />
                           <figcaption>vị trí các khu đô thị và công nghiệp Bắc Ninh</figcaption>
                        </figure>
                        <ul>
                           <li>Sát khu công nghiệp Khai Sơn và Thuận Thành 3</li>
                           <li>Cách thị trấn Hồ 2 km</li>
                           <li>Cách thành phố Bắc Ninh 17 km</li>
                           <li>Cách trung tâm thành phố Hà Nội 25 km</li>
                        </ul>
                        <h2>Mặt bằng khu đô thị Thuận Thành 3</h2>
                        <figure class="wp-block-image alignnone size-full wp-image-570">
                           <img class="wp-image-570" src="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-mat-bang-phan-lo.jpg" sizes="(max-width: 800px) 100vw, 800px" srcset="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-mat-bang-phan-lo.jpg 800w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-mat-bang-phan-lo-300x212.jpg 300w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-mat-bang-phan-lo-768x542.jpg 768w" alt="mặt bằng phân lô khu đô thị Thuận Thành 3" />
                           <figcaption>mặt bằng phân lô khu đô thị Thuận Thành 3</figcaption>
                        </figure>
                        <p><a href="http://lamrealtor.com/khac/20180902-khu-cong-nghiep-va-nha-o-do-thi-thuan-thanh-3-bac-ninh/">Khu đô thị Thuận Thành Bắc Ninh</a> được chia làm 2 giai đoạn, giai đoạn 1 đã hoàn thành và bắt đầu đi vào sử dụng vào quý III năm 2018. Giai đoạn 2 với quy mô lớn 70 ha bao gồm: khu nhà ở xã hội, khu trung tâm thương mại kết hợp chung cư cao tầng, nhà văn hóa, trường học, công viên và hơn 1.000 lô đất nền để bán</p>
                        <ul>
                           <li>Diện tích nhà liền kề: 85 m2 – 100 m2</li>
                           <li>Diện tích shophouse: 150 m2 – 200 m2</li>
                           <li>Diện tích nhà biệt thự: 200 m2 – 250 m2 – 300 m2</li>
                        </ul>
                        <h2>Tiện ích khu đô thị</h2>
                        <figure class="wp-block-image">
                           <img class="wp-image-573" src="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich.jpg" sizes="(max-width: 800px) 100vw, 800px" srcset="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich.jpg 800w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich-300x150.jpg 300w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich-768x384.jpg 768w" alt="" />
                           <figcaption>tiện ích khu đô thị</figcaption>
                        </figure>
                        <figure class="wp-block-image"><img class="wp-image-574" src="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich-1.jpg" sizes="(max-width: 800px) 100vw, 800px" srcset="https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich-1.jpg 800w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich-1-300x180.jpg 300w, https://lamrealtor.com/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-tien-ich-1-768x461.jpg 768w" alt="" /></figure>
                        <p>Với không gian xanh bao phủ trong toàn bộ dự án, Khu Đô thị Thuận Thành Bắc Ninh 3 là sự kết hợp hài hòa giữa những ngôi biệt thự kiểu cách, các lô nhà liền kề và các công trình phúc lợi khác như: Khu trung tâm thương mại; Trung tâm văn hóa, công viên cây xanh mặt nước, khu hành chính, trường học, y tế.</p>
                        <h2>Hình ảnh thực tế</h2>
                        <style type="text/css" scoped="scoped">.fusion-gallery-1 .fusion-gallery-image {border:0px solid #f6f6f6;}</style>
                        <div class="fusion-gallery fusion-gallery-container fusion-grid-3 fusion-columns-total-5 fusion-gallery-layout-grid fusion-gallery-1" style="margin:-5px;">
                           <div style="padding:5px;" class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3 hover-type-none">
                              <div class="fusion-gallery-image"><a href="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-0.jpg" class="fusion-lightbox" data-rel="iLightbox[857d4dc483a43a31e99]"><img src="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-0-400x300.jpg" alt="" title="thuan-thanh-bac-ninh-anh-thuc-te-0" aria-label="thuan-thanh-bac-ninh-anh-thuc-te-0" class="img-responsive wp-image-14116" srcset="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-0-200x150.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-0-400x300.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-0-600x450.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-0.jpg 800w" sizes="(min-width: 2200px) 100vw, (min-width: 824px) 387px, (min-width: 732px) 580px, (min-width: 640px) 732px, " /></a></div>
                           </div>
                           <div style="padding:5px;" class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3 hover-type-none">
                              <div class="fusion-gallery-image"><a href="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-1.jpg" class="fusion-lightbox" data-rel="iLightbox[857d4dc483a43a31e99]"><img src="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-1-400x533.jpg" alt="" title="thuan-thanh-bac-ninh-anh-thuc-te-1" aria-label="thuan-thanh-bac-ninh-anh-thuc-te-1" class="img-responsive wp-image-14117" srcset="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-1-200x267.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-1-400x533.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-1-600x800.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-1.jpg 800w" sizes="(min-width: 2200px) 100vw, (min-width: 824px) 387px, (min-width: 732px) 580px, (min-width: 640px) 732px, " /></a></div>
                           </div>
                           <div style="padding:5px;" class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3 hover-type-none">
                              <div class="fusion-gallery-image"><a href="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-2.jpg" class="fusion-lightbox" data-rel="iLightbox[857d4dc483a43a31e99]"><img src="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-2-400x300.jpg" alt="" title="thuan-thanh-bac-ninh-anh-thuc-te-2" aria-label="thuan-thanh-bac-ninh-anh-thuc-te-2" class="img-responsive wp-image-14118" srcset="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-2-200x150.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-2-400x300.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-2-600x450.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-2.jpg 800w" sizes="(min-width: 2200px) 100vw, (min-width: 824px) 387px, (min-width: 732px) 580px, (min-width: 640px) 732px, " /></a></div>
                           </div>
                           <div class="clearfix"></div>
                           <div style="padding:5px;" class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3 hover-type-none">
                              <div class="fusion-gallery-image"><a href="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-3.jpg" class="fusion-lightbox" data-rel="iLightbox[857d4dc483a43a31e99]"><img src="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-3-400x300.jpg" alt="" title="thuan-thanh-bac-ninh-anh-thuc-te-3" aria-label="thuan-thanh-bac-ninh-anh-thuc-te-3" class="img-responsive wp-image-14119" srcset="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-3-200x150.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-3-400x300.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-3-600x450.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-3.jpg 800w" sizes="(min-width: 2200px) 100vw, (min-width: 824px) 387px, (min-width: 732px) 580px, (min-width: 640px) 732px, " /></a></div>
                           </div>
                           <div style="padding:5px;" class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3 hover-type-none">
                              <div class="fusion-gallery-image"><a href="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-4.jpg" class="fusion-lightbox" data-rel="iLightbox[857d4dc483a43a31e99]"><img src="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-4-400x300.jpg" alt="" title="thuan-thanh-bac-ninh-anh-thuc-te-4" aria-label="thuan-thanh-bac-ninh-anh-thuc-te-4" class="img-responsive wp-image-14120" srcset="https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-4-200x150.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-4-400x300.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-4-600x450.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2018/09/thuan-thanh-bac-ninh-anh-thuc-te-4.jpg 800w" sizes="(min-width: 2200px) 100vw, (min-width: 824px) 387px, (min-width: 732px) 580px, (min-width: 640px) 732px, " /></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
        
      <!-- CONTENT -->
      
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   </body>
</html>
