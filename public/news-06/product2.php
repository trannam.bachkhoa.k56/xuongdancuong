<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->
      <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-14064" class="post-14064 page type-page status-publish hentry">
                     <span class="entry-title rich-snippet-hidden">Chung cư cao cấp Booyoung</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/admin" title="Đăng bởi Hải Phát Land" rel="author">Hải Phát Land</a></span></span><span class="updated rich-snippet-hidden">2018-08-29T13:29:21+00:00</span>                 
                     <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p>
                                          <!-- wp:heading -->
                                       </p>
                                       <h2>Quy hoạch tổng thể</h2>
                                       <p>
                                          <!-- /wp:heading --> <!-- wp:image {"id":526} -->
                                       </p>
                                       <figure class="wp-block-image">
                                          <div id="attachment_526" style="width: 1110px" class="wp-caption aligncenter">
                                             <img class="wp-image-526" src="https://lamrealtor.com/wp-content/uploads/2018/08/booyoung-tong-the-cac-toa-0.jpg" alt="Tổng thể các tòa nhà BooYoung" width="1100" height="562" />
                                             <p class="wp-caption-text">Tổng thể các tòa nhà BooYoung</p>
                                          </div>
                                       </figure>
                                       <p>
                                          <!-- /wp:image --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Dự án Khu căn hộ cao cấp Booyoung tại Khu Đô thị Mới Mỗ Lao, Phường Mỗ Lao, Quận Hà Đông có tổng diện tích đất 43.127 ㎡, 6 khu căn hộ cao cấp từ CT2-CT7. Chung cư bao gồm nhà trẻ, cửa hàng, bãi để xe, bể bơi… với 2 tầng ngầm và 30 tầng trên mặt đất. Có 6 loại căn hộ chung cư từ 73.31 – 107.35㎡, tổng cộng gồm 3.448 căn hộ.</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:list -->
                                       </p>
                                       <ul>
                                          <li>Địa điểm: Khu Đô thị Mới Mỗ Lao, Phường Mỗ Lao, Quận Hà Đông, Việt Nam</li>
                                          <li>Số căn hộ: 3.482 căn hộ</li>
                                          <li>Diện tích căn hộ: 73.31 – 107.35㎡ (6 loại)</li>
                                          <li>Số lượng tòa nhà: 10 tòa nhà(6 phân khu)</li>
                                          <li>Diện tích đất: 43.127㎡</li>
                                          <li>Diện tích xây dựng: 16.650㎡</li>
                                          <li>Tổng diện tích sàn: 530.385㎡</li>
                                       </ul>
                                       <p>
                                          <!-- /wp:list --> <!-- wp:heading -->
                                       </p>
                                       <h2>Ý tưởng thiết kế</h2>
                                       <p>Chung cư quốc tế Booyoung được thiết kế dựa trên triết lý hòa hợp, tiện lợi cho sinh hoạt của cư dân. Các căn hộ thông cửa lớn và ban công tạo luồng thông gió tự nhiên và hiệu quả, giữ cho không khí của toàn căn hộ luôn được trong lành. Hệ thống nột thất tiện ích tích hợp của kiểu Hàn quốc sẽ mang đến cho cư dân sự tiện lợi và thoải mái nhất.</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:heading -->
                                       </p>
                                       <h2>Dự án</h2>
                                       <p>
                                          <!-- /wp:heading --> <!-- wp:image {"id":528} -->
                                       </p>
                                       <figure class="wp-block-image">
                                          <img class="wp-image-528" src="https://lamrealtor.com/wp-content/uploads/2018/08/booyoung-vi-tri-0.jpg" alt="vị trí dự án căn hộ chung cư Booyoung" />
                                          <figcaption>vị trí dự án căn hộ chung cư Booyoung</figcaption>
                                       </figure>
                                       <p>
                                          <!-- /wp:image --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Chung cư Booyoung nằm trong khu đô thị Mỗ Lao, Hà Đông. Đây là một trong những dự   án được coi là có vị trí vàng khi tọa lạc tại một trong những địa điểm đẹp nhất của quận Hà Đông – mặt tiền đường Nguyễn Trãi. Xung quanh khu đô thị Mỗ Lao là các trung tâm chính trị, kinh tế, văn hóa của ba quận Hà Đông, Thanh Xuân và  Nam Từ Liêm.</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:heading -->
                                       </p>
                                       <h2>Giao thông thuận lợi</h2>
                                       <p>
                                          <!-- /wp:heading --> <!-- wp:image {"id":530} -->
                                       </p>
                                       <figure class="wp-block-image">
                                          <img class="wp-image-530" src="https://lamrealtor.com/wp-content/uploads/2018/08/booyoung-vi-tri-2.jpg" alt="vị trí dự án căn hộ chung cư Booyoung" />
                                          <figcaption>vị trí dự án căn hộ chung cư Booyoung</figcaption>
                                       </figure>
                                       <p>
                                          <!-- /wp:image --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Cư dân tương lai của Chung cư quốc tế Booyoung được sử dụng hệ thống giao thông hiện đại của thành phố. Vô cùng thuận tiện trong việc sử dụng các phương tiện giao thông công cộng hiện đại, văn minh đang được Thành phố Hà Nội đầu tư trọng điểm như Hệ thống tàu điện trên cao Cát Linh – Hà Đông, hệ thống xe bus nhanh BRT. Dễ  dàng tiếp cận các bến xe lớn, cửa ngõ thành phố như Bến xe Yên Nghĩa, bến xe Mỹ Đình.</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:heading -->
                                       </p>
                                       <h2>Giáo dục toàn diện</h2>
                                       <p>
                                          <!-- /wp:heading --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Một hệ thống trường học các cấp từ mẫu giáo, tiểu học đến các cấp học cao hơn. Các trường công lập (Nguyễn Du, Đặng Trần Côn, Nguyễn Du, Nguyễn Trãi, Chuyên Nguyễn Huệ) hoặc các trường thuộc hệ thống dân lập như Lương Thế Vinh, Ban Mai và thậm chí là hệ thống các trường quốc tế như hệ thống trường Olympia, trường quốc tế Nhật Bản…</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:heading -->
                                       </p>
                                       <h2>Y tế tin cậy</h2>
                                       <p>
                                          <!-- /wp:heading --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Khoảng cách hợp lý để tiếp cận hệ thống Bệnh viện có uy tín như Viện Quân y 103, Bệnh viện Đa khoa Hà Đông…</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Cuộc sống tiện nghi: Không chỉ thuận tiện cho việc mua sắm sinh hoạt hàng ngày khi vì Chung cư Quốc tế Booyoung rất gần những trung tâm mua sắm văn minh, có uy tín như Metro Hà Đông, Big C Hà Đông và rất nhiều những siêu thị điện máy khác. Không những thế, đời sống tinh thần, hoạt động thể chất của cư dân cũng được đáp ứng đầy đủ với hệ thống rạp chiếu phim CGV, Lotte Cinema, các trung tâm Fitness, Gyms…</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:heading -->
                                       </p>
                                       <h2>Nhà mẫu</h2>
                                       <p>
                                          <!-- /wp:image --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Nhà mẫu dự án Chung cư Quốc tế Booyoung nằm tại tầng 2 Tòa nhà Handico, đường Phạm Hùng.</p>
                                       <p>
                                          <!-- /wp:paragraph --> <!-- wp:paragraph -->
                                       </p>
                                       <p>Kính mời Quý khách tới thăm quan nhà mẫu để có thể cảm nhận sự chăm chút trong từng chi tiết nhỏ của căn hộ bởi mỗi căn hộ của Booyoung đều được tạo nên từ Tình yêu.</p>
                                       <h2>Cơ sở pháp lý</h2>
                                       <p>
                                          <!-- /wp:heading --> <!-- wp:list -->
                                       </p>
                                       <ul>
                                          <li>Giấy chứng nhận đầu tư số 011043000485 do Ủy ban nhân dân thành phố Hà Nội cấp, chứng nhận lần đầu ngày 10/04/2009, thay đổi lần thứ nhất ngày 22/10/2012</li>
                                          <li>Giấy phép xây dựng CT7 số 117/GPXD do Sở xây dựng Hà nội cấp ngày 16/12/2016</li>
                                          <li>Giấy phép xây dựng CT4 số 80/GPXD do Sở xây dựng Hà nội cấp ngày 10/10/2016</li>
                                          <li>Công văn số 2693/SXD – QLN do Sở xây dựng Hà nội cấp ngày 10/04/2017</li>
                                       </ul>
                                       <p>
                                          <!-- /wp:paragraph -->
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-tabs fusion-tabs-1 clean nav-not-justified horizontal-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#143c57;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#b7960b;border-top-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style>
                                       <div class="nav">
                                          <ul class="nav-tabs">
                                             <li class="active">
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-tòact4" href="#tab-0a571616fc0ea16d40e">
                                                   <h4 class="fusion-tab-heading">Tòa CT4</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-tòact7" href="#tab-d5ada5c2364f9f3cdac">
                                                   <h4 class="fusion-tab-heading">Tòa CT7</h4>
                                                </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="tab-content">
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li class="active">
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-tòact4" href="#tab-0a571616fc0ea16d40e">
                                                      <h4 class="fusion-tab-heading">Tòa CT4</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix in active" id="tab-0a571616fc0ea16d40e">
                                             <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct4.jpg" width="1100" height="571" alt="bố trí mặt bằng tòa CT4 dự án chung cư Booyoung" title="booyoung-ct4" class="img-responsive wp-image-14075" srcset="https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct4-200x104.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct4-400x208.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct4-600x311.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct4-800x415.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct4.jpg 1100w" sizes="(max-width: 800px) 100vw, 1100px" /></span></div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-tòact7" href="#tab-d5ada5c2364f9f3cdac">
                                                      <h4 class="fusion-tab-heading">Tòa CT7</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-d5ada5c2364f9f3cdac">
                                             <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct7.jpg" width="1100" height="396" alt="Bố trí mặt bằng tòa CT7 dự án chung cư Booyoung" title="booyoung-ct7" class="img-responsive wp-image-14081" srcset="https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct7-200x72.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct7-400x144.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct7-600x216.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct7-800x288.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2018/08/booyoung-ct7.jpg 1100w" sizes="(max-width: 800px) 100vw, 1100px" /></span></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
         
        
      <!-- CONTENT -->
      
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   </body>
</html>
