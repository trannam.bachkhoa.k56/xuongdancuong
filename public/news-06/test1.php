 <section class="question pdtop40">
         <div class="container">
            <div class="row justify-content-lg-center">
               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="row mgbottom20">
                     <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="questionLeft">
                           <ul class="listquestLeft">
                           	@foreach (\App\Entity\Menu::showWithLocation('footer-second') as $Main_menu_footer2)
			                   @foreach (\App\Entity\MenuElement::showMenuPageArray($Main_menu_footer2->slug) as $id=>$menu_footer2)
			                   			@php 
	                                        $arrayslug = array();
	                                        $urlslug = isset($menu_footer2['url']) ? $menu_footer2['url'] : '';
	                                        $arrayslug = explode("/",$urlslug);
	                                        $posts =(\App\Entity\Post::getPostDetail($arrayslug[2]));
	                                    @endphp		
			                        <li class="ds-block  
			                        <?php if(isset($category) && $arrayslug[2] == $category->slug)
				                        {
				                        	echo 'activeQues';
				                        }
				                        else
				                        {

				                        }
			                        ?>
			                        pd-15 pdleft20"><a href="#{{ $menu_footer2['menu_id'] }}" class="f13">{{ $menu_footer2['title_show'] }}</a></li>
			                    @endforeach
			                @endforeach
                           </ul>
                        </div>
                       <script>
                           $(document).ready(function () {
                        
                              $('ul.listquestLeft>li').click(function () {
                                 if($('li').hasClass('activeQues')) {
                                    $('li').removeClass('activeQues');
                                    $(this).addClass('activeQues');
                                alert(id);
                                 }
                                 else
                                 {    
                                    $(this).addClass('activeQues');
                                 }
                              });
                            });
                           
                       </script>
                     </div>
                        <script>
                          $(document).ready(function () {
                              var element = $('.questionLeft');
                              var originalY = element.offset().top;
                           
                              // Space between element and top of screen (when scrolling)
                              var topMargin = 20;
                              
                              // Should probably be set in CSS; but here just for emphasis
                              element.css('position', 'relative');
                              
                              $(window).on('scroll', function(event) {
                                  var scrollTop = $(window).scrollTop();
                                  var topContent = $('.questionRight').height();
								  var widthside = $( window ).width();
                                  if (scrollTop < (topContent - 0) && widthside >= 100) {
                                      element.stop(false, false).animate({
                                          top:
                                              scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
                                      }, 300);
                                  } 	 									
                           });
                        });
                          </script>
                     <div class="col-lg-9 col-md-9 col-sm-12 col-12 pgbottom30">
                        <div class="questionRight">
                        	@foreach (\App\Entity\Menu::showWithLocation('footer-second') as $Main_menu_footer2)
			                   @foreach (\App\Entity\MenuElement::showMenuPageArray($Main_menu_footer2->slug) as $id=>$menu_footer2)
		                           	<div class="itemQues mgtop20">
		                               <h2 id="{{ $menu_footer2['menu_id'] }}" class="f14 text-b700 text-up mgbotom15">{{ $menu_footer2['title_show'] }}</h2>
		                               <ul class="listquest">
		                               	@php 
	                                        $arrayslug = array();
	                                        $urlslug = isset($menu_footer2['url']) ? $menu_footer2['url'] : '';
	                                        $arrayslug = explode("/",$urlslug);
	                                        $posts =(\App\Entity\Post::categoryShow($arrayslug[2]));
	                                    @endphp		
	                                    	@foreach ($posts as $post)	                                     
			                                 	<li class="mgbotom10 pd20 bgwhite mg-5 ds-block {{ $menu_footer2['menu_id'] }} "><a class="f16 clhomeques ds-inline ">{{ isset($post->title) ?$post->title : '' }}<!--  <i class="fas fa-angle-up" class="clhomeques f30 text-rt"></i> --></a> <i class="fas fa-angle-up clhomeques f30 text-rt fl-rt" ></i>
			                                      <div class="contentli pdtop20">
			                                         <p class="clblack">{!! isset($post->content) ? $post->content : '' !!}</p>
			                                      </div> 
			                                   </li>
		                                   @endforeach
		                               </ul>
		                           </div>
	                       		@endforeach
			                @endforeach  
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('ul.listquest>li').click(function () {
                                    if($(this).hasClass('active')) {
                                       $(this).find('.contentli').slideUp();
                                        
                                        $(this).find('i').css('transform','rotate(360deg)');

                                       
                                        $(this).find('i').css('transition', 'all 1s ease');
                                        $(this).removeClass('active');
                                        // $(this).addClass('color');
                                        // $(this).find('span').html('<i class="fas fa-plus-square"></i>');
                                    }
                                    else {
                                         $(this).find('.contentli').slideDown();
                                        $(this).addClass('active');
                                          $(this).find('i').css('transform','rotate(180deg)');
                                           $(this).find('i').css('transition', 'all 1s ease');
                                    
                                        // $(this).find('i').addClass('fa-rotate-180');
                                        //$(this).find('i').css('transform','rotate(180deg)');
                                        // $(this).find('i').css('transition', 'all 1s ease');
                                        // $(this).find('span').html('<i class="fas fa-minus-square"></i>');
                                        // $(this).find('span').addClass('fa-minus-square');
                                    }
                                });
                            });
                        </script>
                     </div>
                  </div>
               </div>
              
            </div>
         </div>
      </section>