<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {	
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*		
         #text-slider-controls .prev {	
         float: right;
         }
         #text-slider-controls .next {	
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;	
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->

  <?php include('slider/slider.php')?>
      <!-- CONTENT -->
      <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-11799" class="post-11799 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Trang Chủ</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-09-28T15:26:52+00:00</span>                                  
                     <div class="post-content">
                        <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAYO6RivqRJx8E7-G18gH3SZhFcX0CR4Yg&#038;language=vi&#038;ver=1'></script>
                        <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/infobox_packed.js?ver=1'></script>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">VỀ CHÚNG TÔI</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong>Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát &#8211; HAIPHAT LAND</strong></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-1 element-bottomshadow fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="wp-content/uploads/2016/11/ve-chung-toi-hai-phat-land.png" width="" height="" alt="Ve chung toi Hai Phat Land" title="" class="img-responsive"></span></div>
                                    </div>
                                    <div class="fusion-text">
                                       <h3><span style="color: #e86108;">Chúng tôi là ai</span></h3>
                                       <div>
                                          <p><strong>Hải Phát</strong> có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo không ngừng của những người đam mê thành công.</p>
                                          <p>Với môi trường làm việc đa dạng, khuyến khích nhân viên nâng cao trình độ chuyên môn, chúng tôi luôn hướng tới giá trị sau cùng là phục vụ cho quý khách hàng những dịch vụ tốt nhất về Bất động sản.</p>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-2 element-bottomshadow fusion-animated" data-animationType="fadeInDown" data-animationDuration="1.0" data-animationOffset="100%"><img src="wp-content/uploads/2016/11/tam-nhin-su-menh-hai-phat-land.jpg" width="" height="" alt="Tam nhin su menh Hai Phat Land" title="" class="img-responsive"></span></div>
                                    </div>
                                    <div class="fusion-text">
                                       <h3><span style="color: #e86108;">Tầm nhìn và sứ mệnh</span></h3>
                                       <div>
                                          <p><strong>Hải Phát</strong> nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu Việt Nam trong lĩnh vực đầu tư, kinh doanh và quản lý bất động sản.</p>
                                          <p>Hải Phát sẽ là nơi khách hàng trao gửi niềm tin trọn vẹn, là nơi người lao động cống hiến hết mình, cổ đông hoàn toàn hài lòng, và cộng đồng được hưởng nhiều lợi ích.</p>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-3 element-bottomshadow fusion-animated" data-animationType="slideInRight" data-animationDuration="1.0" data-animationOffset="100%"><img src="wp-content/uploads/2016/11/chung-toi-lam-viec-hai-phat-land.jpg" width="" height="" alt="Chung toi lam viec Hai Phat Land" title="" class="img-responsive"></span></div>
                                    </div>
                                    <div class="fusion-text">
                                       <h3><span style="color: #e86108;">Chúng tôi làm việc như thế nào</span></h3>
                                       <div>
                                          <p>Đội ngũ <strong>Hải Phát</strong> khác biệt nhờ nỗ lực và kinh nghiệm sâu rộng, đây cũng là cơ hội để chúng tôi tận dụng kiến thức đem lại lợi ích cho khách hàng.</p>
                                          <p>Siêng năng và không ngừng học hỏi, chúng tôi cùng nhau đoàn kết để làm hài lòng khách hàng nhiều nhất, từ đó đem lại thu nhập và sự thành công cho chúng tôi.</p>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("wp-content/uploads/2016/11/linh-vuc-hoat-dong-background-hai-phat-land.jpg");background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #ffffff;">LĨNH VỰC HOẠT ĐỘNG</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#ffffff;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#ffffff;background-color:#e86108;"><i class=" fa fa-star" style="color:#ffffff;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong><span style="color: #ffffff;">Lĩnh vực hoạt động chuyên nghiệp tích luỹ qua nhiều năm kinh nghiệm.</span></strong></p>
                                    </div>
                                    <div class="fusion-content-boxes content-boxes columns row fusion-columns-3 fusion-columns-total-6 fusion-content-boxes-1 content-boxes-icon-with-title content-left fusion-delayed-animation" data-animation-delay="400" data-animationOffset="100%" style="margin-top:0px;margin-bottom:40px;">
                                       <style type="text/css" scoped="scoped">.fusion-content-boxes-1 .heading .content-box-heading {color:#d2bd56;}
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
                                          .fusion-content-boxes-1 .heading .heading-link:hover .content-box-heading {
                                          color: #d2bd56;
                                          }
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
                                          color: #d2bd56 !important;
                                          }.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #876f08;color: rgba(255,255,255,.9);}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: rgba(255,255,255,.9);}
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
                                          background-color: #d2bd56 !important;
                                          }
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
                                          border-color: #d2bd56 !important;
                                          }
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-wrapper-hover-animation-pulsate .icon span:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-wrapper-hover-animation-pulsate .icon span:after {
                                          -webkit-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #d2bd56, 0 0 0 10px rgba(255,255,255,0.5);
                                          -moz-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #d2bd56, 0 0 0 10px rgba(255,255,255,0.5);
                                          box-shadow: 0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #d2bd56, 0 0 0 10px rgba(255,255,255,0.5);
                                          }
                                       </style>
                                       <div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-4 col-md-4 col-sm-4 fusion-content-box-hover  content-box-column-first-in-row">
                                          <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInRight" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="heading heading-with-icon icon-left">
                                                <div class="icon" style="-webkit-animation-duration: 400ms;animation-duration: 400ms;"><span style="height:44px;width:44px;line-height:23px;border-color:#ffffff;border-width:1;border-style:solid;background-color:#fab524;box-sizing:content-box;border-radius:50%;"><i style="border-color:#fab524;border-width:1px;background-color:#ffffff;box-sizing:content-box;height:42px;width:42px;line-height:42px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e86108;font-size:21px;" class="fontawesome-icon  fa fa-university circle-yes"></i></span></div>
                                                <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">CHỦ ĐẦU TƯ BẤT ĐỘNG SẢN</h2>
                                             </div>
                                             <div class="fusion-clearfix"></div>
                                             <div class="content-container" style="color:#ffffff;">
                                                <p>Hải Phát được đánh giá là CĐT uy tín trong lĩnh vực đầu tư xây dựng hiện nay, đã thành công với một số dự án bất động sản như: KĐT Tân Tây Đô, Tổ hợp The Pride, KĐT Phú Lương…</p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-4 col-md-4 col-sm-4 fusion-content-box-hover ">
                                          <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInRight" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="heading heading-with-icon icon-left">
                                                <div class="icon" style="-webkit-animation-duration: 400ms;animation-duration: 400ms;"><span style="height:44px;width:44px;line-height:23px;border-color:#ffffff;border-width:1;border-style:solid;background-color:#fab524;box-sizing:content-box;border-radius:50%;"><i style="border-color:#fab524;border-width:1px;background-color:#ffffff;box-sizing:content-box;height:42px;width:42px;line-height:42px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e86108;font-size:21px;" class="fontawesome-icon  fa fa-address-book-o circle-yes"></i></span></div>
                                                <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">MÔI GIỚI BẤT ĐỘNG SẢN</h2>
                                             </div>
                                             <div class="fusion-clearfix"></div>
                                             <div class="content-container" style="color:#ffffff;">
                                                <p>Dịch vụ môi giới và tư vấn bất động sản của chúng tôi luôn luôn thấu hiểu nhu cầu, lựa chọn của khách hàng có năng lực phù hợp nhất tương ứng với mỗi loại đất bất động sản.</p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-4 col-md-4 col-sm-4 fusion-content-box-hover ">
                                          <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInRight" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="heading heading-with-icon icon-left">
                                                <div class="icon" style="-webkit-animation-duration: 400ms;animation-duration: 400ms;"><span style="height:44px;width:44px;line-height:23px;border-color:#ffffff;border-width:1;border-style:solid;background-color:#fab524;box-sizing:content-box;border-radius:50%;"><i style="border-color:#fab524;border-width:1px;background-color:#ffffff;box-sizing:content-box;height:42px;width:42px;line-height:42px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e86108;font-size:21px;" class="fontawesome-icon  fa fa-cubes circle-yes"></i></span></div>
                                                <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">CHO THUÊ MẶT BẰNG CĂN HỘ</h2>
                                             </div>
                                             <div class="fusion-clearfix"></div>
                                             <div class="content-container" style="color:#ffffff;">
                                                <p>Với một quy trình tư vấn chuyên nghiệp, chúng tôi sẽ giúp khách hàng cho thuê và thuê được bất đông sản đáp ứng mọi nhu cầu về nhà ở của khách hàng một cách nhanh nhất.</p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-column content-box-column content-box-column content-box-column-4 col-lg-4 col-md-4 col-sm-4 fusion-content-box-hover  content-box-column-first-in-row">
                                          <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInRight" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="heading heading-with-icon icon-left">
                                                <div class="icon" style="-webkit-animation-duration: 400ms;animation-duration: 400ms;"><span style="height:44px;width:44px;line-height:23px;border-color:#ffffff;border-width:1;border-style:solid;background-color:#fab524;box-sizing:content-box;border-radius:50%;"><i style="border-color:#fab524;border-width:1px;background-color:#ffffff;box-sizing:content-box;height:42px;width:42px;line-height:42px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e86108;font-size:21px;" class="fontawesome-icon  fa fa-id-card-o circle-yes"></i></span></div>
                                                <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">THỦ TỤC PHÁP LÝ NHÀ ĐẤT</h2>
                                             </div>
                                             <div class="fusion-clearfix"></div>
                                             <div class="content-container" style="color:#ffffff;">
                                                <p>Chúng tôi hỗ trợ &amp; tư vấn miễn phí về nhà đất, nhà ở, giấy phép, xây dựng, hoàn công, tranh chấp, lệ phí trước bạ nhà – đất, đáp ứng cho mọi nhu cầu và mọi quy mô bất động sản.</p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-column content-box-column content-box-column content-box-column-5 col-lg-4 col-md-4 col-sm-4 fusion-content-box-hover ">
                                          <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInRight" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="heading heading-with-icon icon-left">
                                                <div class="icon" style="-webkit-animation-duration: 400ms;animation-duration: 400ms;"><span style="height:44px;width:44px;line-height:23px;border-color:#ffffff;border-width:1;border-style:solid;background-color:#fab524;box-sizing:content-box;border-radius:50%;"><i style="border-color:#fab524;border-width:1px;background-color:#ffffff;box-sizing:content-box;height:42px;width:42px;line-height:42px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e86108;font-size:21px;" class="fontawesome-icon  fa fa-user-circle-o circle-yes"></i></span></div>
                                                <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">THẨM ĐỊNH GIÁ</h2>
                                             </div>
                                             <div class="fusion-clearfix"></div>
                                             <div class="content-container" style="color:#ffffff;">
                                                <p>Với nguồn nhân lực với trình độ kinh nghiệm chuyên môn cao, Hải Phát chúng tôi sẽ cung cấp cho khách hàng những thông tin về giá trị của các bất động sản với độ tin cậy cao.</p>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="fusion-column content-box-column content-box-column content-box-column-6 col-lg-4 col-md-4 col-sm-4 fusion-content-box-hover  content-box-column-last">
                                          <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInRight" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="heading heading-with-icon icon-left">
                                                <div class="icon" style="-webkit-animation-duration: 400ms;animation-duration: 400ms;"><span style="height:44px;width:44px;line-height:23px;border-color:#ffffff;border-width:1;border-style:solid;background-color:#fab524;box-sizing:content-box;border-radius:50%;"><i style="border-color:#fab524;border-width:1px;background-color:#ffffff;box-sizing:content-box;height:42px;width:42px;line-height:42px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e86108;font-size:21px;" class="fontawesome-icon  fa fa-sitemap circle-yes"></i></span></div>
                                                <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">QUẢN LÝ BẤT ĐỘNG SẢN</h2>
                                             </div>
                                             <div class="fusion-clearfix"></div>
                                             <div class="content-container" style="color:#ffffff;">
                                                <p>Hải Phát chúng tôi có đội ngũ chuyên viên tư vấn tận tâm, giàu kinh nghiệm sẽ giúp quý khách quản lý và giao dịch bất động sản phù hợp với mọi nhu cầu của quý khách hàng.</p>
                                             </div>
                                          </div>
                                       </div>
                                       <style type="text/css" scoped="scoped">
                                          .fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
                                          background-color: #d2bd56 !important;
                                          border-color: #d2bd56 !important;
                                          }
                                       </style>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">DỰ ÁN BẤT ĐỘNG SẢN</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong>Chúng tôi hân hạnh mang đến cho quý khách những sản phẩm đa dạng</strong></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInDown data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 20px 0px 20px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <Style>
                                       @media only screen and (max-width: 800px){
                                       .projects .portfolio-items .portfolio-item{width:100%!important;}
                                       .projects .portfolio-items .portfolio-item {
                                       margin-bottom: 2%;    float: left;
                                       width: 31.233333333%;margin-right: 1%;
                                       margin-left: 0%!important;
                                       }
                                       }
                                       .projects {
                                       padding: 0 0 0 0;
                                       }.avia-team-member.avia-builder-el-no-sibling {
                                       margin: 0px;
                                       text-align: center;
                                       }
                                       .projects .portfolio-items .portfolio-item {
                                       margin-bottom: 2%;    float: left;
                                       width: 31.333333333%;margin-right: 1%;
                                       margin-left: 1%;  
                                       }
                                       .projects .portfolio-items .portfolio-item.span-3 {
                                       width: 23.125%;
                                       margin-right: 2%;
                                       }
                                       .projects .portfolio-items .portfolio-item.span-3:nth-child( 4n+4 ) {
                                       margin-right: 0px;
                                       }
                                       .projects .portfolio-items .portfolio-item.span-3.column-flush {
                                       width: 25%;
                                       margin-right: 0px !important;
                                       margin-bottom: 0px;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image {
                                       margin-bottom: 0px;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image:hover .portfolio-lightbox {
                                       display: block;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-hover {
                                       background-color: rgba(41, 41, 41, 0.5);    height: 100%;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-hover .hover-buttons {
                                       display: none !important;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox {
                                       position: absolute;
                                       z-index: 100;
                                       width: 100%;
                                       top: 0px;
                                       bottom: 0px;
                                       text-align: left;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox .heading {
                                       padding: 0px 10% 0 10%;
                                       color: #fff;
                                       font-weight: normal;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox p {
                                       padding: 0 10%;
                                       color: #fff;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox .button {border-radius: 50px;
                                       color: #fff;
                                       background: none;
                                       border: 2px solid #fff;
                                       position: absolute;
                                       bottom: 10%;
                                       left: 10%;
                                       transition: all 0.5s linear 0s;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox .button:hover {
                                       background-color: #fab524;
                                       color: #fff !important;
                                       border: 2px solid #fab524;
                                       transition: all 0.5s linear 0s;
                                       }
                                       .projects .portfolio-items.list-masonry {
                                       height: auto !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item {
                                       position: static !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item.span-3 {
                                       width: 23.125%;
                                       margin-right: 2.5% !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item.span-3:nth-child( 4n+4 ) {
                                       margin-right: 0px !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item.span-3.no-gutter {
                                       width: 25%;
                                       margin-right: 0 !important;
                                       margin-bottom: 0 !important;
                                       }
                                       .portfolio-item .portfolio-lightbox {
                                       display: none;
                                       }
                                       .mix.portfolio-item{
                                       display:none;
                                       }
                                       .portfolio-items-wrapper {
                                       overflow:hidden;
                                       }
                                       .portfolio-items-wrapper .portfolio-item{
                                       margin-bottom:10px;
                                       }
                                       .portfolio-item .featured-image{
                                       position:relative;
                                       overflow:hidden;
                                       }
                                       .sc-portfolio-grid .portfolio-item .featured-image{
                                       overflow:visible;
                                       }
                                       .portfolio-item .featured-image:after,
                                       .portfolio-item .featured-image:before{
                                       content:'';
                                       display:table;
                                       }
                                       .portfolio-item .featured-image:after{
                                       clear:both;
                                       }
                                       .portfolio-item .featured-image{
                                       zoom:1;
                                       margin-bottom: -6px;
                                       }
                                       .portfolio-item .featured-image img{
                                       width:100%;    height: 100%;
                                       }
                                       .portfolio-hover{
                                       display:none;
                                       z-index:10;
                                       }
                                       .portfolio-hover.style2{
                                       position:absolute;
                                       top:0;
                                       left:0;
                                       bottom:0;
                                       right:0;
                                       text-align:center;
                                       background:#292929;
                                       background:rgba(41,41,41,.95);
                                       display:none;
                                       }
                                       .portfolio-hover.style2{
                                       display:block;
                                       transform:scale(0);
                                       -webkit-transform:scale(0);
                                       -moz-transform:scale(0);
                                       -ms-transform:scale(0);
                                       -o-transform:scale(0);
                                       opacity:0;
                                       transition: transform .3s, opacity .3s;
                                       -webkit-transition: -webkit-transform .3s, opacity .3s;
                                       -moz-transition: -moz-transform .3s, opacity .3s;
                                       }
                                       .portfolio-item .featured-image:hover .portfolio-hover{
                                       display:block;
                                       transform:scale(1);
                                       -webkit-transform:scale(1);
                                       -moz-transform:scale(1);
                                       -ms-transform:scale(1);
                                       -o-transform:scale(1);
                                       opacity:1;
                                       }
                                       .portfolio-item .featured-image .portfolio-hover h6,
                                       .portfolio-item .featured-image .portfolio-hover span,
                                       .portfolio-item .featured-image .portfolio-hover p,
                                       .portfolio-item .featured-image .portfolio-hover .hover-buttons{
                                       opacity:0;
                                       display:block;
                                       transform: translateY(40px);
                                       -webkit-transform: translateY(40px);
                                       -moz-transform: translateY(40px);
                                       -ms-transform: translateY(40px);
                                       -o-transform: translateY(40px);
                                       transition: opacity .3s, transform .3s;
                                       -webkit-transition: opacity .3s, transform .3s;
                                       -moz-transition: opacity .3s, transform .3s;
                                       transition-delay: .2s, .2s;
                                       -webkit-transition-delay: .2s, .2s;
                                       -moz-transition-delay: .2s, .2s;
                                       }
                                       .portfolio-item .featured-image:hover .portfolio-hover h6,
                                       .portfolio-item .featured-image:hover .portfolio-hover span,
                                       .portfolio-item .featured-image:hover .portfolio-hover p,
                                       .portfolio-item .featured-image:hover .portfolio-hover .hover-buttons{
                                       opacity:1;
                                       transform: translateY(0);
                                       -webkit-transform: translateY(0);
                                       -moz-transform: translateY(0);
                                       -ms-transform: translateY(0);
                                       -o-transform: translateY(0);
                                       }
                                       .portfolio-hover.style2>div{
                                       display:table;
                                       width:100%;
                                       height:100%;
                                       }
                                       .portfolio-hover.style2>div>div{
                                       display: table-cell;
                                       vertical-align: middle;
                                       padding:20px 18%;
                                       }
                                       .portfolio-hover.style2 h6{
                                       margin:20px 0 5px;
                                       }
                                       .portfolio-hover.style2 h6 a{
                                       color:#fff;
                                       }
                                       .portfolio-hover.style2 .categories{
                                       color:#fff;
                                       color:rgba(255,255,255,.4);
                                       font-size:12px;
                                       }
                                       .portfolio-hover.style2 p{
                                       color:#fff;
                                       color:rgba(255,255,255,.5);
                                       }
                                       .portfolio-hover .hover-buttons a{
                                       width:45px;
                                       height:45px;
                                       display:inline-block;
                                       border-radius:50%;
                                       -webkit-border-radius:50%;
                                       -moz-border-radius:50%;
                                       background:#eb5858;
                                       text-align:center;
                                       color:#fff;
                                       font-size:24px;
                                       padding-top:5px;
                                       transition: background .3s;
                                       -webkit-transition: background .3s;
                                       -moz-transition: background .3s;
                                       }
                                       .portfolio-hover .hover-buttons a:hover {
                                       background: #d74242;
                                       }
                                       .details-table a {
                                       color: #FF6347;
                                       }
                                    </style>
                                    <section class="widget row portfoliolightbox-vertical-massive projects " id="layers-widget-layers_plus_column_portfoliolightbox-5">
                                       <div class="row portfolio-grid portfolio-items container1 list-grid">
                                          <div id="layers-widget-layers_plus_column_portfoliolightbox-5-415" class="layers-masonry-column clinix-portfoliolightbox portfolio-item span-4 last  column portfolio-item">
                                             <div class="featured-image">
                                                <img src="wp-content/uploads/2017/01/avatar-du-an-thanh-xuan-complex.jpg" width="611" height="330">
                                                <div class="portfolio-hover style2">
                                                   <div>
                                                      <div>
                                                         <div class="hover-buttons">
                                                            <a class="solarLightBox" data-gallery="portfolioGallery" href="wp-content/uploads/2017/01/avatar-du-an-thanh-xuan-complex.jpg"><i class="fa fa-eye"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="portfolio-lightbox">
                                                   <h5 class="heading">DỰ ÁN THANH XUÂN COMPLEX</h5>
                                                   <p>
                                                   <p>Nơi tận hưởng cuộc sống</p>
                                                   </p>
                                                   <a href="chung-cu-cao-cap-thanh-xuan-complex" class="button btn-medium">Xem chi tiết</a>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="layers-widget-layers_plus_column_portfoliolightbox-5-146" class="layers-masonry-column clinix-portfoliolightbox portfolio-item span-4  column portfolio-item">
                                             <div class="featured-image">
                                                <img src="wp-content/uploads/2017/02/avatar-the-vesta.jpg" width="611" height="330">
                                                <div class="portfolio-hover style2">
                                                   <div>
                                                      <div>
                                                         <div class="hover-buttons">
                                                            <a class="solarLightBox" data-gallery="portfolioGallery" href="wp-content/uploads/2017/02/avatar-the-vesta.jpg"><i class="fa fa-eye"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="portfolio-lightbox">
                                                   <h5 class="heading">DỰ ÁN THE VESTA</h5>
                                                   <p>
                                                   <p>An cư nơi đất lành</p>
                                                   </p>
                                                   <a href="nha-o-xa-hoi-the-vesta" class="button btn-medium">Xem chi tiết</a>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="layers-widget-layers_plus_column_portfoliolightbox-5-658" class="layers-masonry-column clinix-portfoliolightbox portfolio-item span-4 last  column portfolio-item">
                                             <div class="featured-image">
                                                <img src="wp-content/uploads/2017/02/avatar-du-an-roman-plaza.jpg" width="611" height="330">
                                                <div class="portfolio-hover style2">
                                                   <div>
                                                      <div>
                                                         <div class="hover-buttons">
                                                            <a class="solarLightBox" data-gallery="portfolioGallery" href="wp-content/uploads/2017/02/avatar-du-an-roman-plaza.jpg"><i class="fa fa-eye"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="portfolio-lightbox">
                                                   <h5 class="heading">DỰ ÁN ROMAN PLAZA</h5>
                                                   <p>
                                                   <p>Khơi nguồn mạch sống thịnh vượng</p>
                                                   </p>
                                                   <a href="du-an-roman-plaza" class="button btn-medium">Xem chi tiết</a>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="layers-widget-layers_plus_column_portfoliolightbox-5-349" class="layers-masonry-column clinix-portfoliolightbox portfolio-item span-4  column portfolio-item">
                                             <div class="featured-image">
                                                <img src="wp-content/uploads/2017/02/avatar-du-an-176-dinh-cong.jpg" width="611" height="330">
                                                <div class="portfolio-hover style2">
                                                   <div>
                                                      <div>
                                                         <div class="hover-buttons">
                                                            <a class="solarLightBox" data-gallery="portfolioGallery" href="wp-content/uploads/2017/02/avatar-du-an-176-dinh-cong.jpg"><i class="fa fa-eye"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="portfolio-lightbox">
                                                   <h5 class="heading">DỰ ÁN SKY CENTRAL</h5>
                                                   <p>
                                                   <p>Chung cư đạt chuẩn tam cận</p>
                                                   </p>
                                                   <a href="chung-cu-cao-cap-sky-central" class="button btn-medium">Xem chi tiết</a>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="layers-widget-layers_plus_column_portfoliolightbox-5-843" class="layers-masonry-column clinix-portfoliolightbox portfolio-item span-4  column portfolio-item">
                                             <div class="featured-image">
                                                <img src="wp-content/uploads/2017/02/avatar-du-an-dream-center-home.jpg" width="611" height="330">
                                                <div class="portfolio-hover style2">
                                                   <div>
                                                      <div>
                                                         <div class="hover-buttons">
                                                            <a class="solarLightBox" data-gallery="portfolioGallery" href="wp-content/uploads/2017/02/avatar-du-an-dream-center-home.jpg"><i class="fa fa-eye"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="portfolio-lightbox">
                                                   <h5 class="heading">DỰ ÁN DREAM CENTER HOME</h5>
                                                   <p>
                                                   <p>Đêm yên tĩnh cho ngày tràn năng lượng</p>
                                                   </p>
                                                   <a href="chung-cu-dream-center-home" class="button btn-medium">Xem chi tiết</a>
                                                </div>
                                             </div>
                                          </div>
                                          <div id="layers-widget-layers_plus_column_portfoliolightbox-5-886" class="layers-masonry-column clinix-portfoliolightbox portfolio-item span-4  column portfolio-item">
                                             <div class="featured-image">
                                                <img src="wp-content/uploads/2017/02/avatar-du-an-nha-pho-thuong-mai-24h.jpg" width="611" height="330">
                                                <div class="portfolio-hover style2">
                                                   <div>
                                                      <div>
                                                         <div class="hover-buttons">
                                                            <a class="solarLightBox" data-gallery="portfolioGallery" href="wp-content/uploads/2017/02/avatar-du-an-nha-pho-thuong-mai-24h.jpg"><i class="fa fa-eye"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="portfolio-lightbox">
                                                   <h5 class="heading">NHÀ PHỐ THƯƠNG MAI 24H</h5>
                                                   <p>
                                                   <p>Đất quý như ngọc, thế mạnh như rồng</p>
                                                   </p><a href="coming-soon" class="button btn-medium">Xem chi tiết</a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </section>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-wifi" style="color:#b7960b;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("wp-content/uploads/2016/11/background-container-hai-phat-land.jpg");background-position: left center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">TIN TỨC &amp; SỰ KIỆN</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong>Chúng tôi luôn mang đến cho bạn những tin tức mới nhất từ thị trường Bất động sản.</strong></p>
                                    </div>
                                    <div class="fusion-recent-posts fusion-recent-posts-1 avada-container layout-thumbnails-on-side layout-columns-3">
                                       <section class="fusion-columns columns fusion-columns-3 columns-3">
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html" class="hover-type-zoomin" aria-label="Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!"><img src="wp-content/uploads/2017/09/canh-bao-nha-dau-tu-bds-gia-nha-chung-cu-dang-giam-1-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-09-29T14:39:57+00:00</span>
                                                <h4 class="entry-title"><a href="tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html">Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-09-29T14:39:57+00:00</span><span>29, Tháng Chín, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html" class="hover-type-zoomin" aria-label="Mẹo tự chọn căn hộ chung cư hợp phong thủy"><img src="wp-content/uploads/2017/09/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy-3-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-09-29T12:00:01+00:00</span>
                                                <h4 class="entry-title"><a href="tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html">Mẹo tự chọn căn hộ chung cư hợp phong thủy</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-09-29T12:00:01+00:00</span><span>29, Tháng Chín, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html" class="hover-type-zoomin" aria-label="Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta"><img src="wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:25:19+00:00</span>
                                                <h4 class="entry-title"><a href="the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html">Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-08T21:25:19+00:00</span><span>8, Tháng Tám, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="tin-tuc/co-hoi-cuoi-cung-de-mua-nha-xa-hoi-voi-lai-suat-co-dinh-vesta.html" class="hover-type-zoomin" aria-label="Cơ hội cuối cùng để mua nhà xã hội với lãi suất cố định &#8211; The Vesta"><img src="wp-content/uploads/2017/03/nha-o-xa-hoi-the-vesta-da-xay-xong-va-dang-hoan-thien-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-04T17:12:28+00:00</span>
                                                <h4 class="entry-title"><a href="tin-tuc/co-hoi-cuoi-cung-de-mua-nha-xa-hoi-voi-lai-suat-co-dinh-vesta.html">Cơ hội cuối cùng để mua nhà xã hội với lãi suất cố định &#8211; The Vesta</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-08-04T17:12:28+00:00</span><span>29, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="tin-tuc/co-hoi-cuoi-cung-de-mua-nha-xa-hoi-voi-lai-suat-co-dinh-vesta.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="tin-tuc/nhung-loi-cua-du-thanh-xuan-complex.html" class="hover-type-zoomin" aria-label="Những lợi thế của dự án Thanh Xuân Complex"><img src="wp-content/uploads/2017/03/nhung-loi-the-du-an-thanh-xuan-complex-01-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-25T10:45:04+00:00</span>
                                                <h4 class="entry-title"><a href="tin-tuc/nhung-loi-cua-du-thanh-xuan-complex.html">Những lợi thế của dự án Thanh Xuân Complex</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-25T10:45:04+00:00</span><span>25, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="tin-tuc/nhung-loi-cua-du-thanh-xuan-complex.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="roman-plaza/du-roman-plaza-co-loai-hinh-nha-pho-shophouse-khong.html" class="hover-type-zoomin" aria-label="Dự án Roman Plaza có loại hình nhà phố shophouse không?"><img src="wp-content/uploads/2017/03/shopphouse-roman-plaza-07-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-24T22:09:25+00:00</span>
                                                <h4 class="entry-title"><a href="roman-plaza/du-roman-plaza-co-loai-hinh-nha-pho-shophouse-khong.html">Dự án Roman Plaza có loại hình nhà phố shophouse không?</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-24T22:09:25+00:00</span><span>24, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="roman-plaza/du-roman-plaza-co-loai-hinh-nha-pho-shophouse-khong.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="tin-tuc/nhung-chung-cu-dau-tien-tai-ha-noi-co-thang-may-chong-chay.html" class="hover-type-zoomin" aria-label="Những chung cư đầu tiên tại Hà Nội có thang máy chống cháy"><img src="wp-content/uploads/2017/03/tien-do-chung-cu-ct4-vimeco-11-11-2016-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-23T16:29:56+00:00</span>
                                                <h4 class="entry-title"><a href="tin-tuc/nhung-chung-cu-dau-tien-tai-ha-noi-co-thang-may-chong-chay.html">Những chung cư đầu tiên tại Hà Nội có thang máy chống cháy</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-23T16:29:56+00:00</span><span>23, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="tin-tuc/nhung-chung-cu-dau-tien-tai-ha-noi-co-thang-may-chong-chay.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="tin-tuc/can-ho-3-phong-ngu-gia-2-ty-dong-giua-trung-tam-thanh-xuan.html" class="hover-type-zoomin" aria-label="Căn hộ 3 phòng ngủ giá 2 tỷ đồng giữa trung tâm Thanh Xuân"><img src="wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-01-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-23T13:59:31+00:00</span>
                                                <h4 class="entry-title"><a href="tin-tuc/can-ho-3-phong-ngu-gia-2-ty-dong-giua-trung-tam-thanh-xuan.html">Căn hộ 3 phòng ngủ giá 2 tỷ đồng giữa trung tâm Thanh Xuân</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-23T13:59:31+00:00</span><span>23, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="tin-tuc/can-ho-3-phong-ngu-gia-2-ty-dong-giua-trung-tam-thanh-xuan.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="tin-tuc/tap-doan-nao-dang-tao-cuc-dien-moi-tren-duong-huu.html" class="hover-type-zoomin" aria-label="Tập đoàn nào đang tạo cục diện mới trên đường Tố Hữu?"><img src="wp-content/uploads/2017/03/quy-hoach-du-an-hai-phat-duong-to-huu-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-24T15:37:38+00:00</span>
                                                <h4 class="entry-title"><a href="tin-tuc/tap-doan-nao-dang-tao-cuc-dien-moi-tren-duong-huu.html">Tập đoàn nào đang tạo cục diện mới trên đường Tố Hữu?</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-24T15:37:38+00:00</span><span>23, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="tin-tuc/tap-doan-nao-dang-tao-cuc-dien-moi-tren-duong-huu.html#respond">0 Comments</a></span></p>
                                             </div>
                                          </article>
                                       </section>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  data-animationType=zoomIn data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-no-small-visibility fusion-animated 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">ĐỐI TÁC CỦA CHÚNG TÔI</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="text-align: center;">
                                       <div class="fusion-image-carousel fusion-image-carousel-fixed fusion-carousel-border">
                                          <div class="fusion-carousel" data-autoplay="yes" data-columns="5" data-itemmargin="13" data-itemwidth="180" data-touchscroll="no" data-imagesize="fixed" data-scrollitems="1">
                                             <div class="fusion-carousel-positioner">
                                                <ul class="fusion-carousel-holder">
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="153" height="72" src="wp-content/uploads/2017/09/Logo-PMC.png" class="attachment-blog-medium size-blog-medium" alt="Logo PMC" srcset="wp-content/uploads/2017/09/Logo-PMC-150x72.png 150w, wp-content/uploads/2017/09/Logo-PMC.png 153w" sizes="(max-width: 153px) 100vw, 153px" /></div>
                                                      </div>
                                                   </li>
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="132" height="72" src="wp-content/uploads/2017/09/logo-techcombank.png" class="attachment-blog-medium size-blog-medium" alt="Logo Techcombank" /></div>
                                                      </div>
                                                   </li>
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="111" height="72" src="wp-content/uploads/2017/09/logo-mb.png" class="attachment-blog-medium size-blog-medium" alt="Logo MB" /></div>
                                                      </div>
                                                   </li>
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="205" height="96" src="wp-content/uploads/2017/09/logo-tsq.png" class="attachment-blog-medium size-blog-medium" alt="Logo TSQ" srcset="wp-content/uploads/2017/09/logo-tsq-200x94.png 200w, wp-content/uploads/2017/09/logo-tsq.png 205w" sizes="(max-width: 205px) 100vw, 205px" /></div>
                                                      </div>
                                                   </li>
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="202" height="72" src="wp-content/uploads/2017/09/logo-vietcombank.png" class="attachment-blog-medium size-blog-medium" alt="Logo Vietcombank" srcset="wp-content/uploads/2017/09/logo-vietcombank-200x71.png 200w, wp-content/uploads/2017/09/logo-vietcombank.png 202w" sizes="(max-width: 202px) 100vw, 202px" /></div>
                                                      </div>
                                                   </li>
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="179" height="72" src="wp-content/uploads/2017/09/logo-vietinbank.png" class="attachment-blog-medium size-blog-medium" alt="Logo Vietinbank" srcset="wp-content/uploads/2017/09/logo-vietinbank-177x72.png 177w, wp-content/uploads/2017/09/logo-vietinbank.png 179w" sizes="(max-width: 179px) 100vw, 179px" /></div>
                                                      </div>
                                                   </li>
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="223" height="72" src="wp-content/uploads/2016/12/logo-bidv.png" class="attachment-blog-medium size-blog-medium" alt="Logo BIDV" srcset="wp-content/uploads/2016/12/logo-bidv-200x65.png 200w, wp-content/uploads/2016/12/logo-bidv.png 223w" sizes="(max-width: 223px) 100vw, 223px" /></div>
                                                      </div>
                                                   </li>
                                                </ul>
                                                <div class="fusion-carousel-nav"><span class="fusion-nav-prev"></span><span class="fusion-nav-next"></span></div>
                                             </div>
                                          </div>
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  data-animationType=slideInDown data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                   
                                    <div class="shortcode-map fusion-google-map fusion-maps-js-type" id="fusion_map_5b888a1e946e8" style="height:300px;width:100%;"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <a href="tel:0986 205 333" class="hotline-sticky">Hotline<br><strong id="txtHotline">0986 205 333</strong></a>
                                    <style>
                                       .hotline-sticky {
                                       background: url(wp-content/uploads/2016/12/background-hotline-thanh-xuan-complex-hapulico-24t3.png) no-repeat scroll 0 0;
                                       top: 100px;
                                       height: 70px;
                                       position: fixed;
                                       right: 0;
                                       width: 189px;
                                       text-transform: uppercase;
                                       color: #fff;
                                       font-weight: 500;
                                       z-index: 2000;
                                       padding: 5px 0 0 10px;
                                       }
                                       .hotline-sticky strong {
                                       font-size: 18px;
                                       margin-left: 10px;
                                       }
                                    </style>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   	 <!--  <script type="text/javascript" src="js/jquery3.3.1.js"></script> -->
 
   </body>
</html>
