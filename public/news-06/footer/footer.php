
         <!-- #main -->

         <div class="fusion-footer">
            <footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
               <div class="fusion-row">
                  <div class="fusion-columns fusion-columns-4 fusion-widget-area">
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-2" class="fusion-footer-widget-column widget widget_text">
                           <div class="textwidget"><img src="wp-content/uploads/2016/12/footer-logo-hai-phat-land.png" alt="Logo Footer Hai Phat Land" /><br><br/>
                              <span style="font-family: UTM-Wedding; font-size: 25px; color: #d2bd56;">Chúng tôi luôn đặt trí lực, tâm lực vào mỗi sản phẩm bởi chất lượng là cốt lõi, là sự hài lòng của khách hàng và  là thành công của chúng tôi.</span>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-4" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Thông tin liên hệ</h4>
                           <div class="textwidget">
                              <p><i class="fontawesome-icon  fa fa-map-marker circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Địa chỉ: Tầng 1&2 CT4, Tổ hợp TMDV và Căn hộ The Pride, KĐT An Hưng, P. La Khê, Q. Hà Đông, TP. Hà Nội.<br><br/>
                                 <i class="fontawesome-icon  fa fa-phone circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i>Số điện thoại: 0422 001 999<br><br/>
                                 <i class="fontawesome-icon  fa fa-envelope circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Email: info@haiphatland.vn
                              </p>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="recent-posts-6" class="fusion-footer-widget-column widget widget_recent_entries">
                           <h4 class="widget-title">Tin tức mới nhất</h4>
                           <ul>
                              <li>
                                 <a href="tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html">Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!</a>
                              </li>
                              <li>
                                 <a href="tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html">Mẹo tự chọn căn hộ chung cư hợp phong thủy</a>
                              </li>
                              <li>
                                 <a href="du-an-chung-cu-goldseason/nhan-ngay-chuyen-du-lich-khi-mua-chung-cu-goldseason-47-nguyen-tuan.html">Nhận ngay chuyến du lịch Mỹ khi mua chung cư Goldseason 47 Nguyễn Tuân</a>
                              </li>
                           </ul>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
                        <section id="text-5" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Lịch làm việc</h4>
                           <div class="textwidget">
                              <p>Chúng tôi luôn hỗ trợ Quý khách 24/24<br>
                                 <i class="fontawesome-icon  fa fa-phone-square circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Hotline: 0986 205 333<br>
                                 <i class="fontawesome-icon  fa fa-fax circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> CSKH: 0938 072 999
                              </p>
                              <h5 style="margin-top: 1em; margin-bottom: 0.5em;"><span style="font-weight: 400; color: #ddd;">GIỜ MỞ CỬA</span></h5>
                              <ul>
                                 <li><strong><span style="color: #d2bd56;">Thứ 2-Thứ 6:</span> 	</strong>8h00' đến 17h30'</li>
                                 <li><strong><span style="color: #d2bd56;">Thứ 7:</span> 	</strong>8h00' đến 17h30'</li>
                                 <li><strong><span style="color: #d2bd56;">Chủ nhật:</span> 	</strong>8h00' đến 17h30'</li>
                              </ul>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-clearfix"></div>
                  </div>
                  <!-- fusion-columns -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- fusion-footer-widget-area -->
            <footer id="footer" class="fusion-footer-copyright-area">
               <div class="fusion-row">
                  <div class="fusion-copyright-content">
                     <div class="fusion-copyright-notice">
                        <div>
                           © Copyright <script>document.write(new Date().getFullYear());</script>   |   Website by <a href=“” target="_blank">Hải Phát Land</a>   |   All Rights Reserved   |   Powered by <a href='' t>Hải Phát</a>	
                        </div>
                     </div>
                  </div>
                  <!-- fusion-fusion-copyright-content -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- #footer -->
         </div>