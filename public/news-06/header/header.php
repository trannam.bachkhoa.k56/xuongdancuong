<header class="fusion-header-wrapper fusion-header-shadow">
            <div class="fusion-header-v1 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-1  fusion-mobile-menu-design-modern">
               <div class="fusion-header-sticky-height"></div>
               <div class="fusion-header">
                  <div class="fusion-row">
                     <div class="fusion-logo" data-margin-top="8px" data-margin-bottom="8px" data-margin-left="0px" data-margin-right="0px">
                        <a class="fusion-logo-link"  href="" >
                           <!-- standard logo -->
                           <img src="wp-content/uploads/2016/11/logo-hai-phat-land.png" srcset="wp-content/uploads/2016/11/logo-hai-phat-land.png 1x" width="70" height="70" alt="Công ty CP BĐS Hải Phát Logo" retina_logo_url="" class="fusion-standard-logo" />
                           <!-- mobile logo -->
                           <img src="wp-content/uploads/2016/11/mobile-logo-hai-phat-land.png" srcset="wp-content/uploads/2016/11/mobile-logo-hai-phat-land.png 1x" width="24" height="24" alt="Công ty CP BĐS Hải Phát Logo" retina_logo_url="" class="fusion-mobile-logo" />
                        </a>
                     </div>
                     <nav class="fusion-main-menu" aria-label="Main Menu">
                        <ul role="menubar" id="menu-main" class="fusion-menu">
                           <li role="menuitem"  id="menu-item-13257"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-11799 current_page_item menu-item-13257 fusion-menu-item-button"  ><a  href="" class="fusion-bar-highlight"><span class="menu-text fusion-button button-default button-medium"><span class="button-icon-divider-left"><i class="glyphicon  fa fa-home"></i></span><span class="fusion-button-text-left">TRANG CHỦ</span></span></a></li>
                           <li role="menuitem"  id="menu-item-12595"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-12595 fusion-dropdown-menu"  >
                              <a  href="gioi-thieu" class="fusion-bar-highlight"><span class="menu-text">GIỚI THIỆU</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-11971"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11971 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Phương châm hoạt động</span></a></li>
                                 <li role="menuitem"  id="menu-item-11972"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11972 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Tầm nhìn &#038; Sứ mệnh</span></a></li>
                                 <li role="menuitem"  id="menu-item-11973"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11973 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Giá trị cốt lõi</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-11974"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11974 fusion-dropdown-menu"  >
                              <a  href="#" class="fusion-bar-highlight"><span class="menu-text">DỰ ÁN</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-14099"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14099 fusion-dropdown-submenu"  ><a  href="chung-cu-cao-cap-booyoung" class="fusion-bar-highlight"><span>Chung cư cao cấp Booyoung</span></a></li>
                                 <li role="menuitem"  id="menu-item-13311"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13311 fusion-dropdown-submenu"  ><a  href="du-roman-plaza" class="fusion-bar-highlight"><span>Dự án Roman Plaza</span></a></li>
                                 <li role="menuitem"  id="menu-item-13805"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13805 fusion-dropdown-submenu"  >
                                    <a  href="du-goldseason-47-nguyen-tuan" class="fusion-bar-highlight"><span>Dự án Goldseason</span></a>
                                    <ul role="menu" class="sub-menu">
                                       <li role="menuitem"  id="menu-item-13928"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-13928"  ><a  href="chuyen-muc/du-an-chung-cu-goldseason" class="fusion-bar-highlight"><span>Dự án chung cư goldseason</span></a></li>
                                    </ul>
                                 </li>
                                 <li role="menuitem"  id="menu-item-12071"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12071 fusion-dropdown-submenu"  ><a  href="chung-cu-cao-cap-thanh-xuan-complex" class="fusion-bar-highlight"><span>Dự án Thanh Xuân Complex</span></a></li>
                                 <li role="menuitem"  id="menu-item-12918"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12918 fusion-dropdown-submenu"  ><a  href="nha-o-xa-hoi-the-vesta" class="fusion-bar-highlight"><span>Nhà ở xã hội The Vesta</span></a></li>
                                 <li role="menuitem"  id="menu-item-12819"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12819 fusion-dropdown-submenu"  ><a  href="chung-cu-dream-center-home" class="fusion-bar-highlight"><span>Dự án Dream Center Home</span></a></li>
                                 <li role="menuitem"  id="menu-item-12038"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12038 fusion-dropdown-submenu"  ><a  href="chung-cu-cao-cap-sky-central" class="fusion-bar-highlight"><span>Dự án Sky Central</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12039"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-12039 fusion-dropdown-menu"  >
                              <a  href="chuyen-muc/tin-tuc" class="fusion-bar-highlight"><span class="menu-text">TIN TỨC &#038; SỰ KIỆN</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-12040"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12040 fusion-dropdown-submenu"  ><a  href="chuyen-muc/tin-hai-phat" class="fusion-bar-highlight"><span>Tin tức Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12041"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12041 fusion-dropdown-submenu"  ><a  href="chuyen-muc/tin-bds" class="fusion-bar-highlight"><span>Tin tức Bất động sản</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12815"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-12815 fusion-dropdown-menu"  >
                              <a  href="chuyen-muc/goc-hai-phat" class="fusion-bar-highlight"><span class="menu-text">GÓC HẢI PHÁT</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-12816"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12816 fusion-dropdown-submenu"  ><a  href="chuyen-muc/dien-dan" class="fusion-bar-highlight"><span>Diễn đàn Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12817"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12817 fusion-dropdown-submenu"  ><a  href="chuyen-muc/van-hoa" class="fusion-bar-highlight"><span>Văn hóa Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12630"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12630 fusion-dropdown-submenu"  ><a  href="http://#" class="fusion-bar-highlight"><span>Thư viện Ảnh</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12627"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12627"  ><a  href="chuyen-muc/tuyen-dung" class="fusion-bar-highlight"><span class="menu-text">TUYỂN DỤNG</span></a></li>
                           <li role="menuitem"  id="menu-item-12574"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12574"  ><a  href="lien-he" class="fusion-bar-highlight"><span class="menu-text">LIÊN HỆ</span></a></li>
                           <li class="fusion-custom-menu-item fusion-main-menu-search">
                              <a class="fusion-main-menu-icon fusion-bar-highlight" href="#" aria-label="Search" data-title="Search" title="Search"></a>
                              <div class="fusion-custom-menu-item-contents">
                                 <form role="search" class="searchform fusion-search-form" method="get" action="">
                                    <div class="fusion-search-form-content">
                                       <div class="fusion-search-field search-field">
                                          <label class="screen-reader-text" for="s">Search for:</label>
                                          <input type="text" value="" name="s" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ...">
                                       </div>
                                       <div class="fusion-search-button search-button">
                                          <input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </li>
                        </ul>
                     </nav>
                     <div class="fusion-mobile-menu-icons">
                        <a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu" aria-expanded="false"></a>
                     </div>
                     <nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left"></nav>
                  </div>
               </div>
            </div>
            <div class="fusion-clearfix"></div>
         </header>