<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->
         <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-center">
            <div class="fusion-page-title-row">
               <div class="fusion-page-title-wrapper">
                  <div class="fusion-page-title-captions">
                     <h1 class="entry-title">Tin Hải Phát</h1>
                     <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs"><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="https://www.haiphatland.vn"><span itemprop="title">Home</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Tin Hải Phát</span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <main id="main" role="main" class="clearfix " style="">
            <div class="fusion-row" style="">
               <section id="content" class="" style="float: left;">
                  <div id="posts-container" class="fusion-blog-archive fusion-blog-layout-medium-alternate-wrapper fusion-clearfix">
                     <div class="fusion-posts-container fusion-blog-layout-medium-alternate fusion-blog-pagination  fusion-blog-rollover" data-pages="1">
                        <article id="post-13940" class="fusion-post-medium-alternate  post fusion-clearfix post-13940 type-post status-publish format-standard has-post-thumbnail hentry category-the-vesta category-tin-hai-phat category-tin-tuc tag-du-an-the-vesta tag-du-an-vesta tag-mo-ban-the-vesta tag-nha-o-xa-hoi tag-nha-o-xa-hoi-the-vesta tag-the-vesta tag-the-vesta-phu-lam tag-v3-prime">
                           <div class="fusion-date-and-formats">
                              <div class="fusion-date-box">
                                 <span class="fusion-date">
                                 8  </span>
                                 <span class="fusion-month-year">
                                 08, 2017 </span>
                              </div>
                              <div class="fusion-format-box">
                                 <i class="fusion-icon-pen"></i>
                              </div>
                           </div>
                           <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                              <ul class="slides">
                                 <li>
                                    <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                       <img width="320" height="202" src="https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-320x202.jpg" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-320x202.jpg 320w, https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-700x441.jpg 700w" sizes="(max-width: 320px) 100vw, 320px" />
                                       <div class="fusion-rollover">
                                          <div class="fusion-rollover-content">
                                             <a class="fusion-rollover-link" href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html">Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta</a>
                                             <div class="fusion-rollover-sep"></div>
                                             <a class="fusion-rollover-gallery" href="https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta.jpg" data-id="13940" data-rel="iLightbox[gallery]" data-title="mo-ban-v3-prime-the-vesta" data-caption="">
                                             Gallery              </a>
                                             <h4 class="fusion-rollover-title">
                                                <a href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html">
                                                Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta               </a>
                                             </h4>
                                             <div class="fusion-rollover-categories"><a href="https://www.haiphatland.vn/chuyen-muc/the-vesta" rel="tag">Nhà ở xã hội The Vesta</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="tag">Tin tức</a></div>
                                             <a class="fusion-link-wrapper" href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html" aria-label="Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta"></a>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="fusion-post-content post-content">
                              <h2 class="entry-title fusion-post-title"><a href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html">Nhiều giao dịch đã thực hiện thành công tại lễ mở bán The Vesta</a></h2>
                              <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-08-08T21:25:19+00:00</span><span>8, Tháng Tám, 2017</span><span class="fusion-inline-sep">|</span>Categories: <a href="https://www.haiphatland.vn/chuyen-muc/the-vesta" rel="category tag">Nhà ở xã hội The Vesta</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="category tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="category tag">Tin tức</a><span class="fusion-inline-sep">|</span><span class="meta-tags">Tags: <a href="https://www.haiphatland.vn/the/du-an-the-vesta" rel="tag">dự án the vesta</a>, <a href="https://www.haiphatland.vn/the/du-an-vesta" rel="tag">dự án vesta</a>, <a href="https://www.haiphatland.vn/the/mo-ban-the-vesta" rel="tag">mở bán the vesta</a>, <a href="https://www.haiphatland.vn/the/nha-o-xa-hoi" rel="tag">nhà ở xã hội</a>, <a href="https://www.haiphatland.vn/the/nha-o-xa-hoi-the-vesta" rel="tag">nhà ở xã hội the vesta</a>, <a href="https://www.haiphatland.vn/the/the-vesta" rel="tag">the vesta</a>, <a href="https://www.haiphatland.vn/the/the-vesta-phu-lam" rel="tag">the vesta phú lãm</a>, <a href="https://www.haiphatland.vn/the/v3-prime" rel="tag">v3 prime</a></span><span class="fusion-inline-sep">|</span></p>
                              <div class="fusion-post-content-container">
                                 <p>Tiếp nối thành công từ đợt mở bán tháng 10, ngày 13/11 vừa qua, công ty cổ phần bất động sản Hải Phát đã tổ chức mở bán các căn hộ &#91;...&#93;</p>
                              </div>
                           </div>
                           <div class="fusion-clearfix"></div>
                           <div class="fusion-meta-info">
                              <div class="fusion-alignright">
                                 <a href="https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html" class="fusion-read-more">
                                 Read More                           </a>
                              </div>
                           </div>
                        </article>
                        <article id="post-11949" class="fusion-post-medium-alternate  post fusion-clearfix post-11949 type-post status-publish format-standard has-post-thumbnail hentry category-tin-bds category-tin-hai-phat category-tin-tuc">
                           <div class="fusion-date-and-formats">
                              <div class="fusion-date-box">
                                 <span class="fusion-date">
                                 24 </span>
                                 <span class="fusion-month-year">
                                 12, 2016 </span>
                              </div>
                              <div class="fusion-format-box">
                                 <i class="fusion-icon-pen"></i>
                              </div>
                           </div>
                           <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                              <ul class="slides">
                                 <li>
                                    <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                       <img width="320" height="202" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/dai-dien-hai-phat-land-tham-du-hoi-nghi-320x202.jpg" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/dai-dien-hai-phat-land-tham-du-hoi-nghi-320x202.jpg 320w, https://www.haiphatland.vn/wp-content/uploads/2016/12/dai-dien-hai-phat-land-tham-du-hoi-nghi-700x441.jpg 700w" sizes="(max-width: 320px) 100vw, 320px" />
                                       <div class="fusion-rollover">
                                          <div class="fusion-rollover-content">
                                             <a class="fusion-rollover-link" href="https://www.haiphatland.vn/tin-tuc/hai-phat-land-tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san.html">Hải Phát Land tham dự hội nghị các Sàn giao dịch Bất động sản</a>
                                             <div class="fusion-rollover-sep"></div>
                                             <a class="fusion-rollover-gallery" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/dai-dien-hai-phat-land-tham-du-hoi-nghi.jpg" data-id="11949" data-rel="iLightbox[gallery]" data-title="dai-dien-hai-phat-land-tham-du-hoi-nghi" data-caption="">
                                             Gallery              </a>
                                             <h4 class="fusion-rollover-title">
                                                <a href="https://www.haiphatland.vn/tin-tuc/hai-phat-land-tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san.html">
                                                Hải Phát Land tham dự hội nghị các Sàn giao dịch Bất động sản              </a>
                                             </h4>
                                             <div class="fusion-rollover-categories"><a href="https://www.haiphatland.vn/chuyen-muc/tin-bds" rel="tag">Tin Bất động sản</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="tag">Tin tức</a></div>
                                             <a class="fusion-link-wrapper" href="https://www.haiphatland.vn/tin-tuc/hai-phat-land-tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san.html" aria-label="Hải Phát Land tham dự hội nghị các Sàn giao dịch Bất động sản"></a>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="fusion-post-content post-content">
                              <h2 class="entry-title fusion-post-title"><a href="https://www.haiphatland.vn/tin-tuc/hai-phat-land-tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san.html">Hải Phát Land tham dự hội nghị các Sàn giao dịch Bất động sản</a></h2>
                              <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-03-11T17:27:46+00:00</span><span>24, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span>Categories: <a href="https://www.haiphatland.vn/chuyen-muc/tin-bds" rel="category tag">Tin Bất động sản</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="category tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="category tag">Tin tức</a><span class="fusion-inline-sep">|</span></p>
                              <div class="fusion-post-content-container">
                                 <p>Ngày 16/12 vừa qua, Công ty Cổ phần bất động sản Hải Phát đã tham dự hội nghị “Đánh giá thị trường giao dịch bất động sản 2016, xu thế 2017 &#91;...&#93;</p>
                              </div>
                           </div>
                           <div class="fusion-clearfix"></div>
                           <div class="fusion-meta-info">
                              <div class="fusion-alignright">
                                 <a href="https://www.haiphatland.vn/tin-tuc/hai-phat-land-tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san.html" class="fusion-read-more">
                                 Read More                           </a>
                              </div>
                           </div>
                        </article>
                        <article id="post-11941" class="fusion-post-medium-alternate  post fusion-clearfix post-11941 type-post status-publish format-standard has-post-thumbnail hentry category-tin-hai-phat category-tin-tuc">
                           <div class="fusion-date-and-formats">
                              <div class="fusion-date-box">
                                 <span class="fusion-date">
                                 23 </span>
                                 <span class="fusion-month-year">
                                 12, 2016 </span>
                              </div>
                              <div class="fusion-format-box">
                                 <i class="fusion-icon-pen"></i>
                              </div>
                           </div>
                           <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                              <ul class="slides">
                                 <li>
                                    <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                       <img width="320" height="202" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu-320x202.jpg" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu-300x189.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu-320x202.jpg 320w, https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu-600x378.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu-700x441.jpg 700w, https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu.jpg 723w" sizes="(max-width: 320px) 100vw, 320px" />
                                       <div class="fusion-rollover">
                                          <div class="fusion-rollover-content">
                                             <a class="fusion-rollover-link" href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-thuong-hieu-hang-dau-2016.html">Hải Phát Invest: Vinh dự nhận giải thưởng “Thương hiệu hàng đầu 2016”</a>
                                             <div class="fusion-rollover-sep"></div>
                                             <a class="fusion-rollover-gallery" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu.jpg" data-id="11941" data-rel="iLightbox[gallery]" data-title="ong-dao-tien-dung-pho-tgd-cong-ty-co-phan-dau-tu-hai-phat-nhan-danh-hieu" data-caption="">
                                             Gallery              </a>
                                             <h4 class="fusion-rollover-title">
                                                <a href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-thuong-hieu-hang-dau-2016.html">
                                                Hải Phát Invest: Vinh dự nhận giải thưởng “Thương hiệu hàng đầu 2016”               </a>
                                             </h4>
                                             <div class="fusion-rollover-categories"><a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="tag">Tin tức</a></div>
                                             <a class="fusion-link-wrapper" href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-thuong-hieu-hang-dau-2016.html" aria-label="Hải Phát Invest: Vinh dự nhận giải thưởng “Thương hiệu hàng đầu 2016”"></a>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="fusion-post-content post-content">
                              <h2 class="entry-title fusion-post-title"><a href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-thuong-hieu-hang-dau-2016.html">Hải Phát Invest: Vinh dự nhận giải thưởng “Thương hiệu hàng đầu 2016”</a></h2>
                              <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-03-11T17:28:34+00:00</span><span>23, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span>Categories: <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="category tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="category tag">Tin tức</a><span class="fusion-inline-sep">|</span></p>
                              <div class="fusion-post-content-container">
                                 <p>Ngày 29/10/2016, Công ty cổ phần Đầu tư Hải Phát đã được vinh danh ở 2 giải thưởng “Thương hiệu hàng đầu Việt Nam - Top Brands 2016” và “Doanh nghiệp &#91;...&#93;</p>
                              </div>
                           </div>
                           <div class="fusion-clearfix"></div>
                           <div class="fusion-meta-info">
                              <div class="fusion-alignright">
                                 <a href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-thuong-hieu-hang-dau-2016.html" class="fusion-read-more">
                                 Read More                           </a>
                              </div>
                           </div>
                        </article>
                        <article id="post-11937" class="fusion-post-medium-alternate  post fusion-clearfix post-11937 type-post status-publish format-standard has-post-thumbnail hentry category-roman-plaza category-tin-hai-phat category-tin-tuc">
                           <div class="fusion-date-and-formats">
                              <div class="fusion-date-box">
                                 <span class="fusion-date">
                                 22 </span>
                                 <span class="fusion-month-year">
                                 12, 2016 </span>
                              </div>
                              <div class="fusion-format-box">
                                 <i class="fusion-icon-pen"></i>
                              </div>
                           </div>
                           <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                              <ul class="slides">
                                 <li>
                                    <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                       <img width="320" height="202" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien-320x202.jpg" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien-300x189.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien-320x202.jpg 320w, https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien-600x378.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien-700x441.jpg 700w, https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien.jpg 723w" sizes="(max-width: 320px) 100vw, 320px" />
                                       <div class="fusion-rollover">
                                          <div class="fusion-rollover-content">
                                             <a class="fusion-rollover-link" href="https://www.haiphatland.vn/tin-hai-phat/roman-plaza-hon-ca-su-tinh-te.html">Roman Plaza &#8211; Hơn cả sự tinh tế</a>
                                             <div class="fusion-rollover-sep"></div>
                                             <a class="fusion-rollover-gallery" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien.jpg" data-id="11937" data-rel="iLightbox[gallery]" data-title="roman-plaza-thiet-ke-theo-phong-cach-kien-truc-tan-co-dien" data-caption="">
                                             Gallery              </a>
                                             <h4 class="fusion-rollover-title">
                                                <a href="https://www.haiphatland.vn/tin-hai-phat/roman-plaza-hon-ca-su-tinh-te.html">
                                                Roman Plaza &#8211; Hơn cả sự tinh tế              </a>
                                             </h4>
                                             <div class="fusion-rollover-categories"><a href="https://www.haiphatland.vn/chuyen-muc/roman-plaza" rel="tag">Roman Plaza</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="tag">Tin tức</a></div>
                                             <a class="fusion-link-wrapper" href="https://www.haiphatland.vn/tin-hai-phat/roman-plaza-hon-ca-su-tinh-te.html" aria-label="Roman Plaza &#8211; Hơn cả sự tinh tế"></a>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="fusion-post-content post-content">
                              <h2 class="entry-title fusion-post-title"><a href="https://www.haiphatland.vn/tin-hai-phat/roman-plaza-hon-ca-su-tinh-te.html">Roman Plaza &#8211; Hơn cả sự tinh tế</a></h2>
                              <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-03-12T22:05:25+00:00</span><span>22, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span>Categories: <a href="https://www.haiphatland.vn/chuyen-muc/roman-plaza" rel="category tag">Roman Plaza</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="category tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="category tag">Tin tức</a><span class="fusion-inline-sep">|</span></p>
                              <div class="fusion-post-content-container">
                                 <p>ROMAN PLAZA là tổ hợp nhà ở khép kín, sang trọng được đầu tư xây dựng theo phong cách kiến trúc Tân cổ điển Châu Âu. Đến với ROMAN PLAZA, bạn &#91;...&#93;</p>
                              </div>
                           </div>
                           <div class="fusion-clearfix"></div>
                           <div class="fusion-meta-info">
                              <div class="fusion-alignright">
                                 <a href="https://www.haiphatland.vn/tin-hai-phat/roman-plaza-hon-ca-su-tinh-te.html" class="fusion-read-more">
                                 Read More                           </a>
                              </div>
                           </div>
                        </article>
                        <article id="post-11931" class="fusion-post-medium-alternate  post fusion-clearfix post-11931 type-post status-publish format-standard has-post-thumbnail hentry category-tin-hai-phat category-tin-tuc">
                           <div class="fusion-date-and-formats">
                              <div class="fusion-date-box">
                                 <span class="fusion-date">
                                 21 </span>
                                 <span class="fusion-month-year">
                                 12, 2016 </span>
                              </div>
                              <div class="fusion-format-box">
                                 <i class="fusion-icon-pen"></i>
                              </div>
                           </div>
                           <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                              <ul class="slides">
                                 <li>
                                    <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                       <img width="320" height="202" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thuong-hieu-lon-duoc-vinh-danh-tai-le-trao-giai-320x202.jpg" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thuong-hieu-lon-duoc-vinh-danh-tai-le-trao-giai-320x202.jpg 320w, https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thuong-hieu-lon-duoc-vinh-danh-tai-le-trao-giai-700x441.jpg 700w" sizes="(max-width: 320px) 100vw, 320px" />
                                       <div class="fusion-rollover">
                                          <div class="fusion-rollover-content">
                                             <a class="fusion-rollover-link" href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-top-20-nhan-hieu-hang-dau.html">Hải Phát Invest: Vinh dự nhận giải thưởng Top 20 nhãn hiệu hàng đầu</a>
                                             <div class="fusion-rollover-sep"></div>
                                             <a class="fusion-rollover-gallery" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thuong-hieu-lon-duoc-vinh-danh-tai-le-trao-giai.jpg" data-id="11931" data-rel="iLightbox[gallery]" data-title="cac-thuong-hieu-lon-duoc-vinh-danh-tai-le-trao-giai" data-caption="">
                                             Gallery              </a>
                                             <h4 class="fusion-rollover-title">
                                                <a href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-top-20-nhan-hieu-hang-dau.html">
                                                Hải Phát Invest: Vinh dự nhận giải thưởng Top 20 nhãn hiệu hàng đầu              </a>
                                             </h4>
                                             <div class="fusion-rollover-categories"><a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="tag">Tin tức</a></div>
                                             <a class="fusion-link-wrapper" href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-top-20-nhan-hieu-hang-dau.html" aria-label="Hải Phát Invest: Vinh dự nhận giải thưởng Top 20 nhãn hiệu hàng đầu"></a>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="fusion-post-content post-content">
                              <h2 class="entry-title fusion-post-title"><a href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-top-20-nhan-hieu-hang-dau.html">Hải Phát Invest: Vinh dự nhận giải thưởng Top 20 nhãn hiệu hàng đầu</a></h2>
                              <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-03-11T17:29:48+00:00</span><span>21, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span>Categories: <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="category tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="category tag">Tin tức</a><span class="fusion-inline-sep">|</span></p>
                              <div class="fusion-post-content-container">
                                 <p>Ngày 26/11/2016, tại Hà Nội, Công ty cổ phần Đầu tư Hải Phát đã được vinh danh TOP 20 nhãn hiệu hàng đầu Việt Nam trong khuôn khổ chương trình "Tôn &#91;...&#93;</p>
                              </div>
                           </div>
                           <div class="fusion-clearfix"></div>
                           <div class="fusion-meta-info">
                              <div class="fusion-alignright">
                                 <a href="https://www.haiphatland.vn/tin-tuc/hai-phat-invest-vinh-du-nhan-giai-thuong-top-20-nhan-hieu-hang-dau.html" class="fusion-read-more">
                                 Read More                           </a>
                              </div>
                           </div>
                        </article>
                        <article id="post-11921" class="fusion-post-medium-alternate  post fusion-clearfix post-11921 type-post status-publish format-standard has-post-thumbnail hentry category-tin-hai-phat category-tin-tuc">
                           <div class="fusion-date-and-formats">
                              <div class="fusion-date-box">
                                 <span class="fusion-date">
                                 20 </span>
                                 <span class="fusion-month-year">
                                 12, 2016 </span>
                              </div>
                              <div class="fusion-format-box">
                                 <i class="fusion-icon-pen"></i>
                              </div>
                           </div>
                           <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                              <ul class="slides">
                                 <li>
                                    <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                       <img width="320" height="202" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thi-sinh-dat-giai-nhat-nhi-ba-gameshow-rinh-chia-khoa-vang-320x202.jpg" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thi-sinh-dat-giai-nhat-nhi-ba-gameshow-rinh-chia-khoa-vang-320x202.jpg 320w, https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thi-sinh-dat-giai-nhat-nhi-ba-gameshow-rinh-chia-khoa-vang-700x441.jpg 700w" sizes="(max-width: 320px) 100vw, 320px" />
                                       <div class="fusion-rollover">
                                          <div class="fusion-rollover-content">
                                             <a class="fusion-rollover-link" href="https://www.haiphatland.vn/tin-tuc/le-ky-niem-13-nam-thanh-lap-hai-phat-invest.html">Lễ kỷ niệm 13 năm thành lập Hải Phát Invest</a>
                                             <div class="fusion-rollover-sep"></div>
                                             <a class="fusion-rollover-gallery" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/cac-thi-sinh-dat-giai-nhat-nhi-ba-gameshow-rinh-chia-khoa-vang.jpg" data-id="11921" data-rel="iLightbox[gallery]" data-title="cac-thi-sinh-dat-giai-nhat-nhi-ba-gameshow-rinh-chia-khoa-vang" data-caption="">
                                             Gallery              </a>
                                             <h4 class="fusion-rollover-title">
                                                <a href="https://www.haiphatland.vn/tin-tuc/le-ky-niem-13-nam-thanh-lap-hai-phat-invest.html">
                                                Lễ kỷ niệm 13 năm thành lập Hải Phát Invest              </a>
                                             </h4>
                                             <div class="fusion-rollover-categories"><a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="tag">Tin tức</a></div>
                                             <a class="fusion-link-wrapper" href="https://www.haiphatland.vn/tin-tuc/le-ky-niem-13-nam-thanh-lap-hai-phat-invest.html" aria-label="Lễ kỷ niệm 13 năm thành lập Hải Phát Invest"></a>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="fusion-post-content post-content">
                              <h2 class="entry-title fusion-post-title"><a href="https://www.haiphatland.vn/tin-tuc/le-ky-niem-13-nam-thanh-lap-hai-phat-invest.html">Lễ kỷ niệm 13 năm thành lập Hải Phát Invest</a></h2>
                              <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-03-11T17:30:29+00:00</span><span>20, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span>Categories: <a href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" rel="category tag">Tin Hải Phát</a>, <a href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" rel="category tag">Tin tức</a><span class="fusion-inline-sep">|</span></p>
                              <div class="fusion-post-content-container">
                                 <p>Tối ngày 15/12/2016, tại nhà hàng Vạn Hoa, Hà Đông, Công ty cổ phần đầu tư Hải Phát - Hải Phát Invest đã long trọng tổ chức lễ kỷ niệm 13 &#91;...&#93;</p>
                              </div>
                           </div>
                           <div class="fusion-clearfix"></div>
                           <div class="fusion-meta-info">
                              <div class="fusion-alignright">
                                 <a href="https://www.haiphatland.vn/tin-tuc/le-ky-niem-13-nam-thanh-lap-hai-phat-invest.html" class="fusion-read-more">
                                 Read More                           </a>
                              </div>
                           </div>
                        </article>
                     </div>
                  </div>
               </section>
               <aside id="sidebar" role="complementary" class="sidebar fusion-widget-area fusion-content-widget-area fusion-sidebar-right fusion-blogsidebar fusion-sticky-sidebar" style="float: right;" >
                  <div class="fusion-sidebar-inner-content">
                     <section id="search-2" class="widget widget_search">
                        <form role="search" class="searchform fusion-search-form" method="get" action="https://www.haiphatland.vn/">
                           <div class="fusion-search-form-content">
                              <div class="fusion-search-field search-field">
                                 <label class="screen-reader-text" for="s">Search for:</label>
                                 <input type="text" value="" name="s" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ..."/>
                              </div>
                              <div class="fusion-search-button search-button">
                                 <input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
                              </div>
                           </div>
                        </form>
                     </section>
                     <section id="text-6" class="widget widget_text">
                        <div class="textwidget">
                           <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/hotline-hai-phat-land.svg" width="" height="" alt="Hotline Hai Phat Land" title="" class="img-responsive"/></span></div>
                        </div>
                     </section>
                     <section id="pyre_tabs-widget-6" class="widget fusion-tabs-widget">
                        <div class="fusion-tabs-widget-wrapper fusion-tabs-widget-2 fusion-tabs-classic fusion-tabs-image-default tab-holder">
                           <nav class="fusion-tabs-nav">
                              <ul class="tabset tabs">
                                 <li class="active"><a href="#" data-link="fusion-tab-popular">Popular</a></li>
                                 <li><a href="#" data-link="fusion-tab-recent">Recent</a></li>
                              </ul>
                           </nav>
                           <div class="fusion-tabs-widget-content tab-box tabs-container">
                              <div class="fusion-tab-popular fusion-tab-content tab tab_content" data-name="fusion-tab-popular">
                                 <ul class="fusion-tabs-widget-items news-list">
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html" aria-label="Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2017/09/canh-bao-nha-dau-tu-bds-gia-nha-chung-cu-dang-giam-1-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/09/canh-bao-nha-dau-tu-bds-gia-nha-chung-cu-dang-giam-1-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2017/09/canh-bao-nha-dau-tu-bds-gia-nha-chung-cu-dang-giam-1-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html">Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!</a>
                                          <div class="fusion-meta">
                                             29, Tháng Chín, 2017                               
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/tin-tuc/thanh-cong-cua-hapulico-co-duoc-tai-hien-voi-thanh-xuan-complex.html" aria-label="Thành công của Hapulico có được tái hiện với Thanh Xuân Complex?"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/goc-san-vuon-02-thanh-xuan-complex-hapulico-24t3-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/goc-san-vuon-02-thanh-xuan-complex-hapulico-24t3-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2016/12/goc-san-vuon-02-thanh-xuan-complex-hapulico-24t3-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/tin-tuc/thanh-cong-cua-hapulico-co-duoc-tai-hien-voi-thanh-xuan-complex.html">Thành công của Hapulico có được tái hiện với Thanh Xuân Complex?</a>
                                          <div class="fusion-meta">
                                             5, Tháng Mười Hai, 2016                               
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/tin-tuc/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat.html" aria-label="Chủ đầu tư Thanh Xuân Complex nộp 500 tỉ tiền sử dụng đất"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat-1-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat-1-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat-1-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/tin-tuc/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat.html">Chủ đầu tư Thanh Xuân Complex nộp 500 tỉ tiền sử dụng đất</a>
                                          <div class="fusion-meta">
                                             6, Tháng Mười Hai, 2016                               
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/thanh-xuan-complex/vi-sao-nen-lua-chon-chung-cu-thanh-xuan-complex-hapulico-24t3.html" aria-label="Vì sao nên lựa chọn chung cư Thanh Xuân Complex – Hapulico 24T3?"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/dau-tu-sinh-loi-voi-thanh-xuan-complex-hapulico-24t3-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/dau-tu-sinh-loi-voi-thanh-xuan-complex-hapulico-24t3-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2016/12/dau-tu-sinh-loi-voi-thanh-xuan-complex-hapulico-24t3-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/thanh-xuan-complex/vi-sao-nen-lua-chon-chung-cu-thanh-xuan-complex-hapulico-24t3.html">Vì sao nên lựa chọn chung cư Thanh Xuân Complex – Hapulico 24T3?</a>
                                          <div class="fusion-meta">
                                             9, Tháng Mười Hai, 2016                               
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html" aria-label="Tổ hợp tiện ích khiến Thanh Xuân Complex được xem là dự án đáng mua nhất"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-01-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-01-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-01-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">Tổ hợp tiện ích khiến Thanh Xuân Complex được xem là dự án đáng mua nhất</a>
                                          <div class="fusion-meta">
                                             10, Tháng Mười Hai, 2016                                 
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                              <div class="fusion-tab-recent fusion-tab-content tab tab_content" data-name="fusion-tab-recent" style="display: none;">
                                 <ul class="fusion-tabs-widget-items news-list">
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html" aria-label="Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2017/09/canh-bao-nha-dau-tu-bds-gia-nha-chung-cu-dang-giam-1-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/09/canh-bao-nha-dau-tu-bds-gia-nha-chung-cu-dang-giam-1-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2017/09/canh-bao-nha-dau-tu-bds-gia-nha-chung-cu-dang-giam-1-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html">Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!</a>
                                          <div class="fusion-meta">
                                             29, Tháng Chín, 2017                               
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html" aria-label="Mẹo tự chọn căn hộ chung cư hợp phong thủy"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2017/09/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy-3-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/09/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy-3-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2017/09/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy-3-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html">Mẹo tự chọn căn hộ chung cư hợp phong thủy</a>
                                          <div class="fusion-meta">
                                             29, Tháng Chín, 2017                               
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/nhan-ngay-chuyen-du-lich-khi-mua-chung-cu-goldseason-47-nguyen-tuan.html" aria-label="Nhận ngay chuyến du lịch Mỹ khi mua chung cư Goldseason 47 Nguyễn Tuân"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2017/05/hpc-landmark-105-pc1-1-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/hpc-landmark-105-pc1-1-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2017/05/hpc-landmark-105-pc1-1-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/nhan-ngay-chuyen-du-lich-khi-mua-chung-cu-goldseason-47-nguyen-tuan.html">Nhận ngay chuyến du lịch Mỹ khi mua chung cư Goldseason 47 Nguyễn Tuân</a>
                                          <div class="fusion-meta">
                                             16, Tháng Tám, 2017                                
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/chung-cu-goldseason-47-nguyen-tuan-chinh-sach-uu-dai-thang-82016.html" aria-label="Chung cư Goldseason 47 Nguyễn Tuân &#8211; Chính sách ưu đãi tháng 8/2017"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2017/08/Gioi-thieu-GOLD-SEASON-in8-page-008-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/Gioi-thieu-GOLD-SEASON-in8-page-008-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2017/08/Gioi-thieu-GOLD-SEASON-in8-page-008-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/chung-cu-goldseason-47-nguyen-tuan-chinh-sach-uu-dai-thang-82016.html">Chung cư Goldseason 47 Nguyễn Tuân &#8211; Chính sách ưu đãi tháng 8/2017</a>
                                          <div class="fusion-meta">
                                             16, Tháng Tám, 2017                                
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="image">
                                          <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/huu-ngay-can-ho-vang-golseason-47-nguyen-tuan.html" aria-label="Sở hữu ngay căn hộ vàng Golseason 47 Nguyễn Tuân"><img width="66" height="66" src="https://www.haiphatland.vn/wp-content/uploads/2017/08/hinh-anh-chung-cu-goldseason-47-nguyen-tuan-2-66x66.jpg" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="chính-sách-ưu-đãi-goldseason" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/hinh-anh-chung-cu-goldseason-47-nguyen-tuan-2-66x66.jpg 66w, https://www.haiphatland.vn/wp-content/uploads/2017/08/hinh-anh-chung-cu-goldseason-47-nguyen-tuan-2-150x150.jpg 150w" sizes="(max-width: 66px) 100vw, 66px" /></a>
                                       </div>
                                       <div class="post-holder">
                                          <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/huu-ngay-can-ho-vang-golseason-47-nguyen-tuan.html">Sở hữu ngay căn hộ vàng Golseason 47 Nguyễn Tuân</a>
                                          <div class="fusion-meta">
                                             15, Tháng Tám, 2017                                
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </section>
                     <section id="rev-slider-widget-2" class="widget widget_revslider">
                        <div id="rev_slider_13_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:#E9E9E9;padding:0px;margin-top:0px;margin-bottom:0px;max-width:278px;">
                           <!-- START REVOLUTION SLIDER 5.4.8 auto mode -->
                           <div id="rev_slider_13_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8">
                              <ul>
                                 <!-- SLIDE  -->
                                 <li data-index="rs-43" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1500"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 1" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/dream-center-home-282-nguyen-huy-tuong-banner-sidebar.jpg"  alt="" title="dream-center-home-282-nguyen-huy-tuong-banner-sidebar"  width="278" height="626" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->
                                 </li>
                                 <!-- SLIDE  -->
                                 <li data-index="rs-42" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 2" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/roman-plaza-banner-sidebar.jpg"  alt="" title="roman-plaza-banner-sidebar"  width="278" height="626" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->
                                 </li>
                                 <!-- SLIDE  -->
                                 <li data-index="rs-44" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 3" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/thanh-xuan-complex-banner-sidebar.jpg"  alt="" title="thanh-xuan-complex-banner-sidebar"  width="278" height="626" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->
                                 </li>
                                 <!-- SLIDE  -->
                                 <li data-index="rs-45" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 4" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/sky-cen-tral-176-dinh-cong-banner-sidebar.jpg"  alt="" title="sky-cen-tral-176-dinh-cong-banner-sidebar"  width="278" height="627" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->
                                 </li>
                                 <!-- SLIDE  -->
                                 <li data-index="rs-46" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 5" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/shophouse-24h-van-phuc-banner-sidebar.jpg"  alt="" title="shophouse-24h-van-phuc-banner-sidebar"  width="278" height="626" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                    <!-- LAYERS -->
                                 </li>
                              </ul>
                              <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                           </div>
                           <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                              if(htmlDiv) {
                                 htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                              }else{
                                 var htmlDiv = document.createElement("div");
                                 htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                 document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                              }
                           </script>
                           <script type="text/javascript">
                              if (setREVStartSize!==undefined) setREVStartSize(
                                 {c: '#rev_slider_13_1', gridwidth: [278], gridheight: [626], sliderLayout: 'auto', minHeight:'626'});
                                       
                              var revapi13,
                                 tpj;  
                              (function() {        
                                 if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();  
                                 function onLoad() {           
                                    if (tpj===undefined) { tpj = jQuery; if("on" == "on") tpj.noConflict();}
                                 if(tpj("#rev_slider_13_1").revolution == undefined){
                                    revslider_showDoubleJqueryError("#rev_slider_13_1");
                                 }else{
                                    revapi13 = tpj("#rev_slider_13_1").show().revolution({
                                       sliderType:"standard",
                                       jsFileLocation:"//www.haiphatland.vn/wp-content/plugins/revslider/public/assets/js/",
                                       sliderLayout:"auto",
                                       dottedOverlay:"none",
                                       delay:4000,
                                       navigation: {
                                          keyboardNavigation:"off",
                                          keyboard_direction: "horizontal",
                                          mouseScrollNavigation:"off",
                                                   mouseScrollReverse:"default",
                                          onHoverStop:"off",
                                          touch:{
                                             touchenabled:"on",
                                             touchOnDesktop:"off",
                                             swipe_threshold: 75,
                                             swipe_min_touches: 1,
                                             swipe_direction: "horizontal",
                                             drag_block_vertical: false
                                          }
                                       },
                                       visibilityLevels:[1240,1024,778,480],
                                       gridwidth:278,
                                       gridheight:626,
                                       lazyType:"none",
                                       minHeight:"626",
                                       shadow:0,
                                       spinner:"spinner0",
                                       stopLoop:"off",
                                       stopAfterLoops:-1,
                                       stopAtSlide:-1,
                                       shuffle:"off",
                                       autoHeight:"on",
                                       disableProgressBar:"on",
                                       hideThumbsOnMobile:"off",
                                       hideSliderAtLimit:0,
                                       hideCaptionAtLimit:768,
                                       hideAllCaptionAtLilmit:0,
                                       debugMode:false,
                                       fallbacks: {
                                          simplifyAll:"off",
                                          nextSlideOnWindowFocus:"off",
                                          disableFocusListener:false,
                                       }
                                    });
                                 }; /* END OF revapi call */
                                 
                               }; /* END OF ON LOAD FUNCTION */
                              }()); /* END OF WRAPPING FUNCTION */
                           </script>
                        </div>
                        <!-- END REVOLUTION SLIDER -->
                     </section>
                     <section id="categories-2" class="widget widget_categories">
                        <div class="heading">
                           <h4 class="widget-title">Chuyên mục</h4>
                        </div>
                        <form action="https://www.haiphatland.vn" method="get">
                           <label class="screen-reader-text" for="cat">Chuyên mục</label>
                           <select  name='cat' id='cat' class='postform' >
                              <option value='-1'>Chọn chuyên mục</option>
                              <option class="level-0" value="48">Diễn đàn Hải Phát&nbsp;&nbsp;(3)</option>
                              <option class="level-0" value="51">Dream Center Home&nbsp;&nbsp;(5)</option>
                              <option class="level-0" value="1595">Dự án chung cư goldseason&nbsp;&nbsp;(11)</option>
                              <option class="level-0" value="39">Góc Hải Phát&nbsp;&nbsp;(4)</option>
                              <option class="level-0" value="1594">Nhà ở xã hội The Vesta&nbsp;&nbsp;(4)</option>
                              <option class="level-0" value="1552">Roman Plaza&nbsp;&nbsp;(2)</option>
                              <option class="level-0" value="1593">Sky Central&nbsp;&nbsp;(1)</option>
                              <option class="level-0" value="44">Thanh Xuân Complex&nbsp;&nbsp;(5)</option>
                              <option class="level-0" value="1556">Tiến độ Dự án&nbsp;&nbsp;(1)</option>
                              <option class="level-0" value="4">Tin Bất động sản&nbsp;&nbsp;(15)</option>
                              <option class="level-0" value="38" selected="selected">Tin Hải Phát&nbsp;&nbsp;(6)</option>
                              <option class="level-0" value="1">Tin tức&nbsp;&nbsp;(25)</option>
                              <option class="level-0" value="40">Tuyển dụng&nbsp;&nbsp;(4)</option>
                              <option class="level-0" value="49">Văn hóa Hải Phát&nbsp;&nbsp;(2)</option>
                           </select>
                        </form>
                        <script type='text/javascript'>
                           /* <![CDATA[ */
                           (function() {
                              var dropdown = document.getElementById( "cat" );
                              function onCatChange() {
                                 if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
                                    dropdown.parentNode.submit();
                                 }
                              }
                              dropdown.onchange = onCatChange;
                           })();
                           /* ]]> */
                        </script>
                     </section>
                     <section id="text-7" class="widget widget_text">
                        <div class="heading">
                           <h4 class="widget-title">Liên hệ Chủ đầu tư</h4>
                        </div>
                        <div class="textwidget">
                           <p><i class="fontawesome-icon  fa fa-map-marker circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> Tầng 1&2 CT4, Tổ hợp TMDV và Căn hộ The Pride, Q. Hà Đông, TP. Hà Nội.<br><br/>
                              <i class="fontawesome-icon  fa fa-phone circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> Hotline: 0986 205 333<br><br/>
                              <i class="fontawesome-icon  fa fa-fax circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> CSKH: 0906 571 288<br><br/>
                              <i class="fontawesome-icon  fa fa-envelope circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> Email: info@haiphatland.vn<br>
                           <div role="form" class="wpcf7" id="wpcf7-f11814-o1" lang="vi" dir="ltr">
                              <div class="screen-reader-response"></div>
                              <form action="/chuyen-muc/tin-hai-phat#wpcf7-f11814-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                 <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="11814" />
                                    <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                    <input type="hidden" name="_wpcf7_locale" value="vi" />
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f11814-o1" />
                                    <input type="hidden" name="_wpcf7_container_post" value="0" />
                                 </div>
                                 <p><span class="wpcf7-form-control-wrap text-833"><input type="text" name="text-833" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span></p>
                                 <p><span class="wpcf7-form-control-wrap email-20"><input type="email" name="email-20" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email*" /></span></p>
                                 <p><span class="wpcf7-form-control-wrap tel-146"><input type="tel" name="tel-146" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span></p>
                                 <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                 <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </section>
                  </div>
               </aside>
            </div>
            <!-- fusion-row -->
         </main>
        
      <!-- CONTENT -->
      
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   </body>
</html>
