<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Chung cư cao cấp Thanh Xuân Complex 24T3 KĐT HAPULICO</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Chung cư Thanh Xuân Complex – Hapulico 24T3 được xây dựng ở vị trí số 6 Lê Văn Thiêm, Thanh Xuân, Hà Nội nằm trong quần thể khu đô thị HAPULICO."/>
      <link rel="canonical" href="https://www.haiphatland.vn/chung-cu-cao-cap-thanh-xuan-complex" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="Chung cư cao cấp Thanh Xuân Complex 24T3 HAPULICO" />
      <meta property="og:description" content="Chung cư Thanh Xuân Complex – Hapulico 24T3 được xây dựng ở vị trí số 6 Lê Văn Thiêm, Thanh Xuân, Hà Nội nằm trong quần thể khu đô thị HAPULICO." />
      <meta property="og:url" content="https://www.haiphatland.vn/chung-cu-cao-cap-thanh-xuan-complex" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="article:publisher" content="https://www.facebook.com/Dreamcenterhome282NHT/" />
      <meta property="og:image" content="https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-03.jpg" />
      <meta property="og:image:secure_url" content="https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-03.jpg" />
      <meta property="og:image:width" content="1920" />
      <meta property="og:image:height" content="1097" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Chung cư Thanh Xuân Complex – Hapulico 24T3 được xây dựng ở vị trí số 6 Lê Văn Thiêm, Thanh Xuân, Hà Nội nằm trong quần thể khu đô thị HAPULICO." />
      <meta name="twitter:title" content="Chung cư cao cấp Thanh Xuân Complex 24T3 KĐT HAPULICO" />
      <meta name="twitter:image" content="https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-03.jpg" />
      <script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Person","url":"https:\/\/www.haiphatland.vn\/","sameAs":["https:\/\/www.facebook.com\/Dreamcenterhome282NHT\/"],"@id":"#person","name":"Tinh Bui"}</script>
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Chung cư cao cấp Thanh Xuân Complex"/>
      <meta property="og:type" content="article"/>
      <meta property="og:url" content="https://www.haiphatland.vn/chung-cu-cao-cap-thanh-xuan-complex"/>
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát"/>
      <meta property="og:description" content="Thanh Xuân Complex 
         Diện tích KĐT HAPULICO: 6ha 
         Diện tích Thanh Xuân Complex: 1.5ha 
         Công ty CP Đầu tư BĐS HAPULICO 
         Số 6 Lê Văn Thiêm, Q. Thanh Xuân, Hà Nội 
         Trung tâm thương mại, dịch vụ, tiện ích 
         Khu căn hộ cao cấp 
         Sàn giao dịch Bất động sản"/>
      <meta property="og:image" content="https://www.haiphatland.vn/wp-content/uploads/2017/01/thanh-xuan-complex-slider-01.jpg"/>
      <link rel='stylesheet' id='contact-form-7-css'  href='https://www.haiphatland.vn/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='https://www.haiphatland.vn/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='https://www.haiphatland.vn/wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='https://www.haiphatland.vn/wp-content/tablepress-combined.min.css?ver=19' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='https://www.haiphatland.vn/wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(https://www.haiphatland.vn/wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2aWV3Qm94PSIwIDAgNjAgNjAiPjxwYXRoIGQ9Ik03LjEwNCAxNC4wMzJsMTUuNTg2IDEuOTg0YzAgMC0wLjAxOSAwLjUgMCAwLjk1M2MwLjAyOSAwLjc1Ni0wLjI2IDEuNTM0LTAuODA5IDIuMSBsLTQuNzQgNC43NDJjMi4zNjEgMy4zIDE2LjUgMTcuNCAxOS44IDE5LjhsMTYuODEzIDEuMTQxYzAgMCAwIDAuNCAwIDEuMSBjLTAuMDAyIDAuNDc5LTAuMTc2IDAuOTUzLTAuNTQ5IDEuMzI3bC02LjUwNCA2LjUwNWMwIDAtMTEuMjYxIDAuOTg4LTI1LjkyNS0xMy42NzRDNi4xMTcgMjUuMyA3LjEgMTQgNy4xIDE0IiBmaWxsPSIjMDA2NzAwIi8+PHBhdGggZD0iTTcuMTA0IDEzLjAzMmw2LjUwNC02LjUwNWMwLjg5Ni0wLjg5NSAyLjMzNC0wLjY3OCAzLjEgMC4zNWw1LjU2MyA3LjggYzAuNzM4IDEgMC41IDIuNTMxLTAuMzYgMy40MjZsLTQuNzQgNC43NDJjMi4zNjEgMy4zIDUuMyA2LjkgOS4xIDEwLjY5OWMzLjg0MiAzLjggNy40IDYuNyAxMC43IDkuMSBsNC43NC00Ljc0MmMwLjg5Ny0wLjg5NSAyLjQ3MS0xLjAyNiAzLjQ5OC0wLjI4OWw3LjY0NiA1LjQ1NWMxLjAyNSAwLjcgMS4zIDIuMiAwLjQgMy4xMDVsLTYuNTA0IDYuNSBjMCAwLTExLjI2MiAwLjk4OC0yNS45MjUtMTMuNjc0QzYuMTE3IDI0LjMgNy4xIDEzIDcuMSAxMyIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==) center/50px 50px no-repeat #009900;}}</style>
      <script type="text/javascript">
         var ajaxRevslider;
         
         jQuery(document).ready(function() {
            // CUSTOM AJAX CONTENT LOADING FUNCTION
            ajaxRevslider = function(obj) {
            
                  // obj.type : Post Type
                  // obj.id : ID of Content to Load
                  // obj.aspectratio : The Aspect Ratio of the Container / Media
                  // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
                  
                  var content = "";
         
                  data = {};
                  
                  data.action = 'revslider_ajax_call_front';
                  data.client_action = 'get_slider_html';
                  data.token = 'f5c0856a50';
                  data.type = obj.type;
                  data.id = obj.id;
                  data.aspectratio = obj.aspectratio;
                  
                  // SYNC AJAX REQUEST
                  jQuery.ajax({
                        type:"post",
                        url:"https://www.haiphatland.vn/wp-admin/admin-ajax.php",
                        dataType: 'json',
                        data:data,
                        async:false,
                        success: function(ret, textStatus, XMLHttpRequest) {
                              if(ret.success == true)
                                    content = ret.data;                                               
                        },
                        error: function(e) {
                              console.log(e);
                        }
                  });
                  
                   // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
                   return content;                                 
            };
            
            // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
            var ajaxRemoveRevslider = function(obj) {
                  return jQuery(obj.selector+" .rev_slider").revkill();
            };
         
            // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
            var extendessential = setInterval(function() {
                  if (jQuery.fn.tpessential != undefined) {
                        clearInterval(extendessential);
                        if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
                              jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
                              // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
                              // func: the Function Name which is Called once the Item with the Post Type has been clicked
                              // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
                              // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
                        }
                  }
            },30);
         });
      </script>
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*       
         #text-slider-controls .prev {    
         float: right;
         }
         #text-slider-controls .next {    
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
      <script type="text/javascript">function setREVStartSize(e){                                                 
         try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
            if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})                              
         }catch(d){console.log("Failure at Presize of Slider:"+d)}                                    
         };
      </script>
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
      <script type="text/javascript">
         jQuery(document).ready(function() {
         /*<![CDATA[*/
         jQuery('.related-posts > div > h3').html('Bài viết liên quan');
         /*]]>*/
         });
      </script><!-- Facebook Pixel Code -->
      <script>
         !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         document,'script','https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '533271836850356'); // Insert your pixel ID here.
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="page-template page-template-100-width page-template-100-width-php page page-id-12012 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         <header class="fusion-header-wrapper fusion-header-shadow">
            <div class="fusion-header-v1 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-1  fusion-mobile-menu-design-modern">
               <div class="fusion-header-sticky-height"></div>
               <div class="fusion-header">
                  <div class="fusion-row">
                     <div class="fusion-logo" data-margin-top="8px" data-margin-bottom="8px" data-margin-left="0px" data-margin-right="0px">
                        <a class="fusion-logo-link"  href="https://www.haiphatland.vn/" >
                           <!-- standard logo -->
                           <img src="https://www.haiphatland.vn/wp-content/uploads/2016/11/logo-hai-phat-land.png" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/11/logo-hai-phat-land.png 1x" width="70" height="70" alt="Công ty CP BĐS Hải Phát Logo" retina_logo_url="" class="fusion-standard-logo" />
                           <!-- mobile logo -->
                           <img src="https://www.haiphatland.vn/wp-content/uploads/2016/11/mobile-logo-hai-phat-land.png" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/11/mobile-logo-hai-phat-land.png 1x" width="24" height="24" alt="Công ty CP BĐS Hải Phát Logo" retina_logo_url="" class="fusion-mobile-logo" />
                        </a>
                     </div>
                     <nav class="fusion-main-menu" aria-label="Main Menu">
                        <ul role="menubar" id="menu-main" class="fusion-menu">
                           <li role="menuitem"  id="menu-item-13257"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-13257 fusion-menu-item-button"  ><a  href="https://www.haiphatland.vn/" class="fusion-bar-highlight"><span class="menu-text fusion-button button-default button-medium"><span class="button-icon-divider-left"><i class="glyphicon  fa fa-home"></i></span><span class="fusion-button-text-left">TRANG CHỦ</span></span></a></li>
                           <li role="menuitem"  id="menu-item-12595"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-12595 fusion-dropdown-menu"  >
                              <a  href="https://www.haiphatland.vn/gioi-thieu" class="fusion-bar-highlight"><span class="menu-text">GIỚI THIỆU</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-11971"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11971 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Phương châm hoạt động</span></a></li>
                                 <li role="menuitem"  id="menu-item-11972"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11972 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Tầm nhìn &#038; Sứ mệnh</span></a></li>
                                 <li role="menuitem"  id="menu-item-11973"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11973 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Giá trị cốt lõi</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-11974"  class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-11974 fusion-dropdown-menu"  >
                              <a  href="#" class="fusion-bar-highlight"><span class="menu-text">DỰ ÁN</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-14114"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14114 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/du-an-khu-do-thi-thuan-thanh-3-bac-ninh" class="fusion-bar-highlight"><span>Dự án khu đô thị Thuận Thành 3 Bắc Ninh</span></a></li>
                                 <li role="menuitem"  id="menu-item-14099"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14099 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-cao-cap-booyoung" class="fusion-bar-highlight"><span>Chung cư cao cấp Booyoung</span></a></li>
                                 <li role="menuitem"  id="menu-item-13311"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13311 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/du-roman-plaza" class="fusion-bar-highlight"><span>Dự án Roman Plaza</span></a></li>
                                 <li role="menuitem"  id="menu-item-13805"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13805 fusion-dropdown-submenu"  >
                                    <a  href="https://www.haiphatland.vn/du-goldseason-47-nguyen-tuan" class="fusion-bar-highlight"><span>Dự án Goldseason</span></a>
                                    <ul role="menu" class="sub-menu">
                                       <li role="menuitem"  id="menu-item-13928"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-13928"  ><a  href="https://www.haiphatland.vn/chuyen-muc/du-an-chung-cu-goldseason" class="fusion-bar-highlight"><span>Dự án chung cư goldseason</span></a></li>
                                    </ul>
                                 </li>
                                 <li role="menuitem"  id="menu-item-12071"  class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-12012 current_page_item menu-item-12071 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-cao-cap-thanh-xuan-complex" class="fusion-bar-highlight"><span>Dự án Thanh Xuân Complex</span></a></li>
                                 <li role="menuitem"  id="menu-item-12918"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12918 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta" class="fusion-bar-highlight"><span>Nhà ở xã hội The Vesta</span></a></li>
                                 <li role="menuitem"  id="menu-item-12819"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12819 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-dream-center-home" class="fusion-bar-highlight"><span>Dự án Dream Center Home</span></a></li>
                                 <li role="menuitem"  id="menu-item-12038"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12038 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-cao-cap-sky-central" class="fusion-bar-highlight"><span>Dự án Sky Central</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12039"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-12039 fusion-dropdown-menu"  >
                              <a  href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" class="fusion-bar-highlight"><span class="menu-text">TIN TỨC &#038; SỰ KIỆN</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-12040"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12040 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" class="fusion-bar-highlight"><span>Tin tức Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12041"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12041 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/tin-bds" class="fusion-bar-highlight"><span>Tin tức Bất động sản</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12815"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-12815 fusion-dropdown-menu"  >
                              <a  href="https://www.haiphatland.vn/chuyen-muc/goc-hai-phat" class="fusion-bar-highlight"><span class="menu-text">GÓC HẢI PHÁT</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-12816"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12816 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/dien-dan" class="fusion-bar-highlight"><span>Diễn đàn Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12817"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12817 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/van-hoa" class="fusion-bar-highlight"><span>Văn hóa Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12630"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12630 fusion-dropdown-submenu"  ><a  href="http://#" class="fusion-bar-highlight"><span>Thư viện Ảnh</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12627"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12627"  ><a  href="https://www.haiphatland.vn/chuyen-muc/tuyen-dung" class="fusion-bar-highlight"><span class="menu-text">TUYỂN DỤNG</span></a></li>
                           <li role="menuitem"  id="menu-item-12574"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12574"  ><a  href="https://www.haiphatland.vn/lien-he" class="fusion-bar-highlight"><span class="menu-text">LIÊN HỆ</span></a></li>
                           <li class="fusion-custom-menu-item fusion-main-menu-search">
                              <a class="fusion-main-menu-icon fusion-bar-highlight" href="#" aria-label="Search" data-title="Search" title="Search"></a>
                              <div class="fusion-custom-menu-item-contents">
                                 <form role="search" class="searchform fusion-search-form" method="get" action="https://www.haiphatland.vn/">
                                    <div class="fusion-search-form-content">
                                       <div class="fusion-search-field search-field">
                                          <label class="screen-reader-text" for="s">Search for:</label>
                                          <input type="text" value="" name="s" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ..."/>
                                       </div>
                                       <div class="fusion-search-button search-button">
                                          <input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </li>
                        </ul>
                     </nav>
                     <div class="fusion-mobile-menu-icons">
                        <a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu" aria-expanded="false"></a>
                     </div>
                     <nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left"></nav>
                  </div>
               </div>
            </div>
            <div class="fusion-clearfix"></div>
         </header>
         <div id="sliders-container">
            <div id="fusion-slider-46" class="fusion-slider-container fusion-slider-12012" style="height:500px;max-width:100%;">
               <style type="text/css" scoped="scoped">
                  #fusion-slider-46 .flex-direction-nav a {width:63px;height:63px;line-height:63px;font-size:25px;}     
               </style>
               <div class="fusion-slider-loading">Loading...</div>
               <div class="tfs-slider flexslider main-flex" style="max-width:100%;" data-slider_width="100%" data-slider_height="500px" data-slider_content_width="" data-full_screen="1" data-parallax="1" data-nav_arrows="1" data-nav_box_width="63px" data-nav_box_height="63px" data-nav_arrow_size="25px" data-pagination_circles="0" data-autoplay="1" data-loop="0" data-animation="fade" data-slideshow_speed="5000" data-animation_speed="600" data-typo_sensitivity="1" data-typo_factor="1.5" data-slider_id="46" data-orderby="date" data-order="DESC" data-slider_indicator="" data-slider_indicator_color="#ffffff" >
                  <ul class="slides" style="max-width:100%;">
                     <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                        <div class="slide-content-container slide-content-right" style="display: none;">
                           <div class="slide-content" style="">
                              <div class="heading with-bg">
                                 <div class="fusion-title-sc-wrapper" style="background-color: rgba(0,0,0, 0.4);">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-two" style="margin-top:0px;margin-bottom:0px;">
                                       <h2 class="title-heading-right" style="color:#fff;font-size:40px;line-height:48px;">Thanh Xuân Complex</h2>
                                    </div>
                                 </div>
                              </div>
                              <div class="caption with-bg">
                                 <div class="fusion-title-sc-wrapper" style="background-color: rgba(0, 0, 0, 0.4);">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:0px;">
                                       <h3 class="title-heading-right" style="color:#fff;font-size:16px;line-height:19.2px;">Chung cư duy nhất có bể bơi nước mặn
                                          đầu tiên tại Hà Nội
                                       </h3>
                                    </div>
                                 </div>
                              </div>
                              <div class="buttons" >
                                 <div class="tfs-button-1">
                                    <div class="fusion-button-wrapper">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:rgba(255,255,255,.7);}.fusion-button.button-1 {border-width:1px;border-color:rgba(255,255,255,.7);}.fusion-button.button-1 .fusion-button-icon-divider{border-color:rgba(255,255,255,.7);}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:#fff;}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:1px;border-color:#fff;}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:#fff;}.fusion-button.button-1{background: rgba(23,77,105,.5);}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: rgba(0,0,0,.7);}.fusion-button.button-1{width:auto;}</style>
                                       <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-1" target="_self" href="#tong-quan-txc"><span class="fusion-button-text">Xem chi tiết</span></a>
                                    </div>
                                 </div>
                                 <div class="tfs-button-2">
                                    <div class="fusion-button-wrapper">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-2 .fusion-button-text, .fusion-button.button-2 i {color:rgba(255,255,255,.8);}.fusion-button.button-2 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-2 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-2:hover .fusion-button-text, .fusion-button.button-2:hover i,.fusion-button.button-2:focus .fusion-button-text, .fusion-button.button-2:focus i,.fusion-button.button-2:active .fusion-button-text, .fusion-button.button-2:active{color:rgba(255,255,255,.9);}.fusion-button.button-2:hover, .fusion-button.button-2:focus, .fusion-button.button-2:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-2{background: #b7960b;}.fusion-button.button-2:hover,.button-2:focus,.fusion-button.button-2:active{background: #876f08;}.fusion-button.button-2{width:auto;}</style>
                                       <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-2" target="_self" href="#lien-he-txc"><span class="fusion-button-text">Đăng ký tư vấn</span></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2017/01/thanh-xuan-complex-slider-02.jpg);max-width:100%;height:500px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/01/thanh-xuan-complex-slider-02.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/01/thanh-xuan-complex-slider-02.jpg', sizingMethod='scale')';" data-imgwidth="1920">
                        </div>
                     </li>
                     <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                        <div class="slide-content-container slide-content-left" style="display: none;">
                           <div class="slide-content" style="">
                              <div class="heading with-bg">
                                 <div class="fusion-title-sc-wrapper" style="background-color: rgba(0,0,0, 0.4);">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-two" style="margin-top:0px;margin-bottom:0px;">
                                       <h2 class="title-heading-left" style="color:#fff;font-size:40px;line-height:48px;">Thanh Xuân Complex</h2>
                                    </div>
                                 </div>
                              </div>
                              <div class="caption with-bg">
                                 <div class="fusion-title-sc-wrapper" style="background-color: rgba(0, 0, 0, 0.4);">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:0px;">
                                       <h3 class="title-heading-left" style="color:#fff;font-size:16px;line-height:19.2px;">Chung cư cao cấp nằm trong
                                          Quần thể KĐT HAPULICO
                                       </h3>
                                    </div>
                                 </div>
                              </div>
                              <div class="buttons" >
                                 <div class="tfs-button-1">
                                    <div class="fusion-button-wrapper">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-3 .fusion-button-text, .fusion-button.button-3 i {color:rgba(255,255,255,.7);}.fusion-button.button-3 {border-width:1px;border-color:rgba(255,255,255,.7);}.fusion-button.button-3 .fusion-button-icon-divider{border-color:rgba(255,255,255,.7);}.fusion-button.button-3:hover .fusion-button-text, .fusion-button.button-3:hover i,.fusion-button.button-3:focus .fusion-button-text, .fusion-button.button-3:focus i,.fusion-button.button-3:active .fusion-button-text, .fusion-button.button-3:active{color:#fff;}.fusion-button.button-3:hover, .fusion-button.button-3:focus, .fusion-button.button-3:active{border-width:1px;border-color:#fff;}.fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:active .fusion-button-icon-divider{border-color:#fff;}.fusion-button.button-3{background: rgba(23,77,105,.5);}.fusion-button.button-3:hover,.button-3:focus,.fusion-button.button-3:active{background: rgba(0,0,0,.7);}.fusion-button.button-3{width:auto;}</style>
                                       <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-3" target="_self" href="#tong-quan-txc"><span class="fusion-button-text">Xem chi tiết</span></a>
                                    </div>
                                 </div>
                                 <div class="tfs-button-2">
                                    <div class="fusion-button-wrapper">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-4 .fusion-button-text, .fusion-button.button-4 i {color:rgba(255,255,255,.8);}.fusion-button.button-4 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-4 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-4:hover .fusion-button-text, .fusion-button.button-4:hover i,.fusion-button.button-4:focus .fusion-button-text, .fusion-button.button-4:focus i,.fusion-button.button-4:active .fusion-button-text, .fusion-button.button-4:active{color:rgba(255,255,255,.9);}.fusion-button.button-4:hover, .fusion-button.button-4:focus, .fusion-button.button-4:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-4:hover .fusion-button-icon-divider, .fusion-button.button-4:hover .fusion-button-icon-divider, .fusion-button.button-4:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-4{background: #b7960b;}.fusion-button.button-4:hover,.button-4:focus,.fusion-button.button-4:active{background: #876f08;}.fusion-button.button-4{width:auto;}</style>
                                       <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-4" target="_self" href="#lien-he-txc"><span class="fusion-button-text">Đăng ký tư vấn</span></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-03.jpg);max-width:100%;height:500px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-03.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-03.jpg', sizingMethod='scale')';" data-imgwidth="1920">
                        </div>
                     </li>
                     <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                        <div class="slide-content-container slide-content-left" style="display: none;">
                           <div class="slide-content" style="">
                              <div class="heading with-bg">
                                 <div class="fusion-title-sc-wrapper" style="background-color: rgba(0,0,0, 0.4);">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-two" style="margin-top:0px;margin-bottom:0px;">
                                       <h2 class="title-heading-left" style="color:#fff;font-size:40px;line-height:48px;">Thanh Xuân Complex</h2>
                                    </div>
                                 </div>
                              </div>
                              <div class="caption with-bg">
                                 <div class="fusion-title-sc-wrapper" style="background-color: rgba(0, 0, 0, 0.4);">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-three" style="margin-top:0px;margin-bottom:0px;">
                                       <h3 class="title-heading-left" style="color:#fff;font-size:18px;line-height:21.6px;">Nơi tận hưởng cuộc sống</h3>
                                    </div>
                                 </div>
                              </div>
                              <div class="buttons" >
                                 <div class="tfs-button-1">
                                    <div class="fusion-button-wrapper">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-5 .fusion-button-text, .fusion-button.button-5 i {color:rgba(255,255,255,.7);}.fusion-button.button-5 {border-width:1px;border-color:rgba(255,255,255,.7);}.fusion-button.button-5 .fusion-button-icon-divider{border-color:rgba(255,255,255,.7);}.fusion-button.button-5:hover .fusion-button-text, .fusion-button.button-5:hover i,.fusion-button.button-5:focus .fusion-button-text, .fusion-button.button-5:focus i,.fusion-button.button-5:active .fusion-button-text, .fusion-button.button-5:active{color:#fff;}.fusion-button.button-5:hover, .fusion-button.button-5:focus, .fusion-button.button-5:active{border-width:1px;border-color:#fff;}.fusion-button.button-5:hover .fusion-button-icon-divider, .fusion-button.button-5:hover .fusion-button-icon-divider, .fusion-button.button-5:active .fusion-button-icon-divider{border-color:#fff;}.fusion-button.button-5{background: rgba(20,60,86,.5);}.fusion-button.button-5:hover,.button-5:focus,.fusion-button.button-5:active{background: rgba(0,0,0,.7);}.fusion-button.button-5{width:auto;}</style>
                                       <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-5" target="_self" href="#tong-quan-txc"><span class="fusion-button-text">Xem chi tiết</span></a>
                                    </div>
                                 </div>
                                 <div class="tfs-button-2">
                                    <div class="fusion-button-wrapper">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-6 .fusion-button-text, .fusion-button.button-6 i {color:rgba(255,255,255,.8);}.fusion-button.button-6 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-6 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-6:hover .fusion-button-text, .fusion-button.button-6:hover i,.fusion-button.button-6:focus .fusion-button-text, .fusion-button.button-6:focus i,.fusion-button.button-6:active .fusion-button-text, .fusion-button.button-6:active{color:rgba(255,255,255,.9);}.fusion-button.button-6:hover, .fusion-button.button-6:focus, .fusion-button.button-6:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-6:hover .fusion-button-icon-divider, .fusion-button.button-6:hover .fusion-button-icon-divider, .fusion-button.button-6:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-6{background: #b7960b;}.fusion-button.button-6:hover,.button-6:focus,.fusion-button.button-6:active{background: #876f08;}.fusion-button.button-6{width:auto;}</style>
                                       <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-6" target="_self" href="#lien-he-txc"><span class="fusion-button-text">Đăng ký tư vấn</span></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2017/01/thanh-xuan-complex-slider-01.jpg);max-width:100%;height:500px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/01/thanh-xuan-complex-slider-01.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/01/thanh-xuan-complex-slider-01.jpg', sizingMethod='scale')';" data-imgwidth="1920">
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-12012" class="post-12012 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Chung cư cao cấp Thanh Xuân Complex</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-09-29T15:37:56+00:00</span>                                   
                     <div class="post-content">
                        <div id="tong-quan-txc">
                           <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2016/12/background-container-tong-quan-thanh-xuan-complex.jpg");background-position: center top;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                              <div class="fusion-builder-row fusion-row ">
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"  style='margin-top:0px;margin-bottom:0px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-two" style="margin-top:0px;margin-bottom:30px;">
                                          <h2 class="title-heading-left">
                                             <h1><span style="font-family: UVF-Candlescript-Pro; font-size: 38px; color: #143c57;">Thanh Xuân Complex</span></h1>
                                          </h2>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-dotted" style="border-color:#b7960b;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-content-boxes content-boxes columns row fusion-columns-4 fusion-columns-total-8 fusion-content-boxes-1 content-boxes-icon-on-top content-left fusion-delayed-animation" data-animation-delay="400" data-animationOffset="100%" style="margin-top:0px;margin-bottom:40px;">
                                          <style type="text/css" scoped="scoped">.fusion-content-boxes-1 .heading .content-box-heading {color:#143c57;}
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
                                             .fusion-content-boxes-1 .heading .heading-link:hover .content-box-heading {
                                             color: #b7960b;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
                                             color: #b7960b !important;
                                             }.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #876f08;color: rgba(255,255,255,.9);}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: rgba(255,255,255,.9);}
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
                                             background-color: #b7960b !important;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
                                             border-color: #b7960b !important;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-wrapper-hover-animation-pulsate .icon span:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-wrapper-hover-animation-pulsate .icon span:after {
                                             -webkit-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             -moz-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             box-shadow: 0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             }
                                          </style>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-first-in-row">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-tong-dien-tich.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Diện tích KĐT</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Diện tích KĐT HAPULICO: 6ha</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-dien-tich-chung-cu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Diện tích TXC</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Diện tích Thanh Xuân Complex: 1.5ha</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-chu-dau-tu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Chủ đầu tư</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Công ty CP Đầu tư BĐS HAPULICO</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-4 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-vi-tri.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Vị trí</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Số 6 Lê Văn Thiêm, Q. Thanh Xuân, Hà Nội</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-5 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-first-in-row">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-tang-dich-vu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tầng 1, 2, 3</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Trung tâm thương mại, dịch vụ, tiện ích</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-6 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-khu-can-ho.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tầng 4-24</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Khu căn hộ cao cấp</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-7 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/3.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Đơn vị phân phối</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Sàn giao dịch Bất động sản Hải Phát</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-8 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-last">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/8.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tư vấn thiết kế</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>ACI Việt Nam &amp; Archivina</p>
                                                </div>
                                             </div>
                                          </div>
                                          <style type="text/css" scoped="scoped">
                                             .fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
                                             background-color: #b7960b !important;
                                             border-color: #b7960b !important;
                                             }
                                          </style>
                                          <div class="fusion-clearfix"></div>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:20px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none fusion-animated" data-animationType="fadeInRight" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-thanh-xuan-complex-hapulico-24t3.png" width="" height="" alt="Phoi canh Thanh Xuan Complex Hapulico 24T3" title="" class="img-responsive"/></span></div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="font-family: UVF-Candlescript-Pro; font-size: 38px; text-align: center;"><span style="color: #143c57;">Vị trí Vàng, kết nối đa chiều</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-text">
                                       <p style="text-align: center;"><strong>Chung cư Thanh Xuân Complex &#8211; <a href="https://haiphatland.vn/chung-cu-cao-cap-thanh-xuan-complex" target="_blank" rel="noopener">Hapulico 24t3</a></strong> nằm trên trục đường Lê Văn Thiêm nơi kết nối nhiều con đường lớn trong khu vực tạo nên 1 hệ thống giao thông đồng bộ xuyên suốt.</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first lien-ket-vung-txc fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-column-content-centered">
                                       <div class="fusion-column-content">
                                          <div class="wrap-overview">
                                             <div class="overview-info">
                                                <div class="page-branch-title">
                                                   <h1 style="text-align: center;"><span style="font-family: UTM-Wedding; color: #143c57;">Liên kết vùng <span style="color: #b7960b;">Thanh Xuân Complex</span></span></h1>
                                                </div>
                                                <div class="row gutter-0">
                                                   <div class="col-xs-3 col-lg-2 small-title">Vị trí:</div>
                                                   <div class="col-xs-9 col-lg-10">Số 6 Lê Văn Thiêm, P. Thanh Xuân Trung, Q. Thanh Xuân, TP. Hà Nội.</div>
                                                   <ul class="col-xs-12 ls-overview">
                                                      <li class="item item1"><i class="fa fa-star" style="color: #b7960b;"></i> Trung Hòa Nhân Chính<br><span>~ 500 m <small>(5 phút)</small></span></li>
                                                      <li class="item item2"><i class="fa fa-star" style="color: #b7960b;"></i> THPT Amsterdam<br><span>~ 1 km <small>(10 phút)</small></span></li>
                                                      <li class="item item3"><i class="fa fa-star" style="color: #b7960b;"></i> Làng SV Hacinco<br><span>~ 200 m <small>(2 phút)</small></span></li>
                                                      <li class="item item4"><i class="fa fa-star" style="color: #b7960b;"></i> BigC Thăng Long<br><span>~ 1.2 km <small>(12 phút)</small></span></li>
                                                      <li class="item item5"><i class="fa fa-star" style="color: #b7960b;"></i> Đại học KHTN<br><span>~ 1 km <small>(10 phút)</small></span></li>
                                                      <li class="item item6"><i class="fa fa-star" style="color: #b7960b;"></i> Royal City<br><span>~ 1.5km <small>(15 phút)</small></span></li>
                                                      <li class="item item7"><i class="fa fa-star" style="color: #b7960b;"></i> TT Hội nghị Quốc Gia<br><span>~ 1.5 km <small>(15 phút)</small></span></li>
                                                      <li class="item item8"><i class="fa fa-star" style="color: #b7960b;"></i> Mardarin Garden<br><span>~ 500 m <small>(5 phút)</small></span></li>
                                                      <li class="item item9"><i class="fa fa-star" style="color: #b7960b;"></i> Công viên Nhân Chính<br><span>~ 800 m <small>(8 phút)</small></span></li>
                                                      <li class="item item10"><i class="fa fa-star" style="color: #b7960b;"></i> Rạp chiếu phim QG<br><span>~ 1.8 km <small>(18 phút)</small></span></li>
                                                   </ul>
                                                   <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                             </div>
                                             <div class="wrap-bottom-right">
                                                <a href="/thanh-xuan-complex/vi-sao-nen-lua-chon-chung-cu-thanh-xuan-complex-hapulico-24t3.html" class="bt-control bt-detail">Xem thêm</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-last vi-tri-txc 2_3"  style='margin-top:0px;margin-bottom:20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-column-content-centered">
                                       <div class="fusion-column-content">
                                          <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/vi-tri-thanh-xuan-complex-hapulico-24t3.png" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="home-intro" class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling fusion-equal-height-columns"  style='background-color: #ffffff;background-position: center bottom;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="font-family: UVF-Candlescript-Pro; font-size: 38px; text-align: center;"><span style="color: #143c57;">Khu đô thị Hapulico</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                    </div>
                                    <div class="vongtron">
                                       <div id="text-slider">
                                          <div class="text-slideshow">
                                             <article>Khu đô thị HAPULICO là tổ hợp các công trình khu thương mại, văn phòng, nhà ở cao cấp, trường học được xây dựng trên tổng diện tích đất 6ha, ở vị trí cực kỳ đắc địa. KĐT gồm 6 khu chính: Khu hỗn hợp gồm 2 tháp cao 24 tầng; khu A có 2 khối 21 tầng, khu B có 4 khối 17 tầng, Thanh Xuân Complex (24T3) và khu nhà thấp tầng gồm 28 biệt thự nhà vườn & trường học.<span></span></article>
                                             <article>Khu đô thị HAPULICO hiện là một trong những tổ hợp nhà ở cao cấp, trung tâm thương mại, văn phòng cho thuê hiện đại và tiện ích nhất Thủ đô. Tại đây, cư dân tương lai có thể lựa chọn cho mình cũng như gia đình những ngôi nhà ước mơ của bản thân. Chung cư Thanh Xuân Complex nằm trong quần thể KĐT HAPULICO nên được thừa hưởng tất cả các tiện ích của KĐT này.<span></span></article>
                                             <article>Khu đô thị HAPULICO được xây dựng theo tiêu chuẩn quốc tế nhằm làm hài lòng những chủ nhân khó tính nhất. Điểm đặc biệt là ở chỗ, bất cứ công trình nào cũng được thiết kế với ý tưởng xuyên suốt là sự kết hợp hài hòa giữa con người và thiên nhiên. Điều đó sẽ mang lại cho bạn những giây phút thoải mái khi ở bên cạnh người thân hay bạn bè của mình.<span></span></article>
                                          </div>
                                          <div id="text-slider-controls"><a href="#" class="prev"><i class="icon-chevron-left nav-color"></i></a> <a href="#" class="next"><i class="icon-chevron-right nav-color"></i></a></div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="home-features" class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://foresa-villas.com/wp-content/uploads/2014/11/bg-features.jpg");background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #b7960b;">Tiện ích Dự án</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #ffffff;"><em>Dự án Thanh Xuân Complex – Hapulico 24T3 </em> được xây dựng ở vị trí đắc địa số 6 Lê Văn Thiêm, Thanh Xuân, Hà Nội nằm trong quần thể khu đô thị HAPULICO – được coi một trong các vị trí vàng tại Hà Nội. Sở dĩ được coi như vậy bởi khu đô thị Hapulico nằm ngay sát với khu đô thị Trung Hòa – Nhân Chính, nơi có đầy đủ các cơ sở hạ tầng kỹ thuật như : Trường học, bệnh viện, siêu thị, khu chăm sóc sức khỏe, khu vui chơi giải trí&#8230;</span></p>
                                    </div>
                                    <div class="project-features">
                                       <ul class="list-features">
                                          <li class="feature odd">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/be-boi-thanh-xuan-complex-hapulico-24t3-1.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Be boi Thanh Xuan Complex 24T3" />      
                                                <h3><img alt="Icon Be boi Thanh Xuan Complex 24T3"  src="https://www.haiphatland.vn/wp-content/uploads/2017/01/icon-be-boi-thanh-xuan-complex-hapulico-24t3.png" /><span>Bể bơi</span></h3>
                                             </a>
                                          </li>
                                          <li class="feature even">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <h3><img alt="Icon Gym Spa Thanh Xuan Complex 24T3"  src="https://www.haiphatland.vn/wp-content/uploads/2016/12/icon-gym-spa.png" /><span>Gym & Spa</span></h3>
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/gym-spa-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Gym Spa Thanh Xuan Complex 24T3" />        
                                             </a>
                                          </li>
                                          <li class="feature odd">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/khong-gian-xanh-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Khong gian xanh Thanh Xuan Complex 24T3" />          
                                                <h3>
                                                   <img alt="Icon Khong gian xanh Thanh Xuan Complex 24T3"  src="https://www.haiphatland.vn/wp-content/uploads/2017/01/icon-khong-gian-xanh-thanh-xuan-complex-hapulico-24t3.png" /><span>Không gian xanh</span>
                                                </h3>
                                             </a>
                                          </li>
                                          <li class="feature even">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <h3><img alt="Icon Benh vien Thanh Xuan Complex 24T3"  src="https://www.haiphatland.vn/wp-content/uploads/2016/12/icon-benh-vien.png" /><span>Bệnh viện</span></h3>
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/benh-vien-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Benh vien Thanh Xuan Complex 24T3" />        
                                             </a>
                                          </li>
                                          <li class="feature odd">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/truong-hoc-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Truong hoc Thanh Xuan Complex 24T3" />          
                                                <h3>
                                                   <img alt="Icon Truong hoc Thanh Xuan Complex 24T3"  src="https://www.haiphatland.vn/wp-content/uploads/2017/01/icon-truong-hoc-thanh-xuan-complex-hapulico-24t3.png" /><span>Trường học</span>
                                                </h3>
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Sơ đồ Mặt Bằng</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-tabs fusion-tabs-1 clean nav-not-justified horizontal-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#143c57;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#b7960b;border-top-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style>
                                       <div class="nav">
                                          <ul class="nav-tabs">
                                             <li class="active">
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngĐiỂnhÌnh" href="#tab-8e5365075acd594b9f2">
                                                   <h4 class="fusion-tab-heading">MẶT BẰNG ĐIỂN HÌNH</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngkĐthapulico" href="#tab-84e2f05a80da37245ef">
                                                   <h4 class="fusion-tab-heading">MẶT BẰNG KĐT HAPULICO</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-nỘithẤtcĂnhỘ" href="#tab-db510fb7f7f70c73ca4">
                                                   <h4 class="fusion-tab-heading">NỘI THẤT CĂN HỘ</h4>
                                                </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="tab-content">
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li class="active">
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngĐiỂnhÌnh" href="#tab-8e5365075acd594b9f2">
                                                      <h4 class="fusion-tab-heading">MẶT BẰNG ĐIỂN HÌNH</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix in active" id="tab-8e5365075acd594b9f2">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="fusion-text">
                                                         <div class="hotspots-container  layout-left event-click" id="hotspot-12546">
                                                            <div class="hotspots-interaction">
                                                               <div class="hotspots-placeholder" id="content-hotspot-12546">
                                                                  <div class="hotspot-initial">
                                                                     <h2 class="hotspot-title"></h2>
                                                                     <div class="hostspot-content">
                                                                        <p>Để xem chi tiết, Quý khách click vào từng căn hộ trong MB điển hình.</p>
                                                                        <table id="tablepress-1" class="tablepress tablepress-id-1">
                                                                           <thead>
                                                                              <tr class="row-1 odd">
                                                                                 <th class="column-1">Căn hộ 2 PN</th>
                                                                                 <th class="column-2">Căn hộ 3 PN</th>
                                                                              </tr>
                                                                           </thead>
                                                                           <tbody class="row-hover">
                                                                              <tr class="row-2 even">
                                                                                 <td class="column-1">A02: 85.70 m2</td>
                                                                                 <td class="column-2">A01: 122.60 m2</td>
                                                                              </tr>
                                                                              <tr class="row-3 odd">
                                                                                 <td class="column-1">A04: 90.90 m2</td>
                                                                                 <td class="column-2">A03: 118.00 m2</td>
                                                                              </tr>
                                                                              <tr class="row-4 even">
                                                                                 <td class="column-1">A05: 87.80 m2</td>
                                                                                 <td class="column-2">A07: 118.10 m2</td>
                                                                              </tr>
                                                                              <tr class="row-5 odd">
                                                                                 <td class="column-1">A06: 83.20 m2</td>
                                                                                 <td class="column-2">A09: 107.10 m2</td>
                                                                              </tr>
                                                                              <tr class="row-6 even">
                                                                                 <td class="column-1">A08: 89.20 m2</td>
                                                                                 <td class="column-2">A10: 106.90 m2</td>
                                                                              </tr>
                                                                              <tr class="row-7 odd">
                                                                                 <td class="column-1">A11:  89.20 m2</td>
                                                                                 <td class="column-2">A12: 118.00 m2</td>
                                                                              </tr>
                                                                              <tr class="row-8 even">
                                                                                 <td class="column-1">A13: 88.80 m2</td>
                                                                                 <td class="column-2">A16: 117.90 m2</td>
                                                                              </tr>
                                                                              <tr class="row-9 odd">
                                                                                 <td class="column-1">A14: 86.80 m2</td>
                                                                                 <td class="column-2">A18: 122.40 m2</td>
                                                                              </tr>
                                                                              <tr class="row-10 even">
                                                                                 <td class="column-1">A15: 88.80 m2</td>
                                                                                 <td class="column-2"></td>
                                                                              </tr>
                                                                              <tr class="row-11 odd">
                                                                                 <td class="column-1">A17: 87.50 m2</td>
                                                                                 <td class="column-2"></td>
                                                                              </tr>
                                                                           </tbody>
                                                                        </table>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <div class="hotspots-image-container"><img width="2339" height= "1654" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/mat-bang-dien-hinh-thanh-xuan-complex-hapulico-24t3.jpg" class="hotspots-image skip-lazy" usemap="#hotspots-image-12546" data-event-trigger="click" data-highlight-color="#3ca2a2" data-highlight-opacity="0.71" data-highlight-border-color="#235b6e" data-highlight-border-width="2" data-highlight-border-opacity="1.01" data-no-lazy="1" data-lazy="false" /></div>
                                                            </div>
                                                            <map name="hotspots-image-12546" class="hotspots-map">
                                                               <area shape="poly" coords="269,771,620,770,620,568,478,569,479,490,302,490,304,672,270,674" href="#hotspot-hotspot-12546-0" title="Căn hộ A01" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="478,289,578,287,577,255,676,255,677,524,621,526,620,569,479,569" href="#hotspot-hotspot-12546-1" title="Căn hộ A02" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="678,257,776,259,777,287,878,292,878,654,675,658" href="#hotspot-hotspot-12546-2" title="Căn hộ A03" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="924,474,1015,472,1015,440,1116,442,1115,742,878,742,878,597,923,597" href="#hotspot-hotspot-12546-3" title="Căn hộ A04" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1117,528,1153,527,1154,475,1232,475,1233,441,1324,441,1325,743,1116,742" href="#hotspot-hotspot-12546-4" title="Căn hộ A05" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1326,443,1424,442,1427,473,1517,476,1516,742,1323,743" href="#hotspot-hotspot-12546-5" title="Căn hộ A06" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1560,288,1666,287,1666,256,1765,258,1764,655,1562,657" href="#hotspot-hotspot-12546-6" title="Căn hộ A07" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1766,255,1864,257,1864,290,1961,289,1963,490,1998,492,1997,569,1819,568,1821,526,1765,524" href="#hotspot-hotspot-12546-7" title="Căn hộ A08" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1819,570,1999,567,1998,493,2137,490,2136,571,2173,571,2171,664,2139,667,2138,747,1820,746" href="#hotspot-hotspot-12546-8" title="Căn hộ A09" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1819,795,2138,796,2136,877,2172,878,2173,972,2139,974,2138,1052,1998,1052,1995,974,1820,976" href="#hotspot-hotspot-12546-9" title="Căn hộ A10" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1820,974,1997,976,1996,1051,1963,1051,1962,1255,1864,1255,1864,1288,1765,1286,1764,1018,1820,1019" href="#hotspot-hotspot-12546-10" title="Căn hộ A11" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1561,889,1764,886,1765,1286,1666,1287,1665,1255,1561,1255" href="#hotspot-hotspot-12546-11" title="Căn hộ A12" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1325,800,1561,801,1563,924,1514,925,1515,1067,1425,1070,1426,1100,1324,1102" href="#hotspot-hotspot-12546-12" title="Căn hộ A13" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="1116,798,1326,801,1325,991,1288,992,1288,1067,1208,1067,1209,1102,1114,1103" href="#hotspot-hotspot-12546-13" title="Căn hộ A14" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="924,802,1115,801,1115,1102,1014,1102,1014,1068,923,1067" href="#hotspot-hotspot-12546-14" title="Căn hộ A15" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="877,890,877,1254,773,1256,775,1286,677,1286,677,889" href="#hotspot-hotspot-12546-15" title="Căn hộ A16" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="676,1018,677,1287,580,1288,579,1254,478,1254,479,973,620,973,620,1020" href="#hotspot-hotspot-12546-16" title="Căn hộ A17" data-action="" target="" class="more-info-area">
                                                               <area shape="poly" coords="620,771,621,972,481,975,479,1052,303,1049,301,868,269,869,269,772" href="#hotspot-hotspot-12546-17" title="Căn hộ A18" data-action="" target="" class="more-info-area">
                                                            </map>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-0">
                                                               <h2 class="hotspot-title">Căn hộ A01</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 131.90 m2</li>
                                                                     <li>Diện tích thông thủy: 122.60 m2</li>
                                                                     <li>Ban công Tây Bắc &amp; Đông Bắc, hướng cửa vào Đông Nam.</li>
                                                                     <li>Phòng khách : 46.40 m2</li>
                                                                     <li>Phòng ngủ 01 : 18.30 m2</li>
                                                                     <li>Phòng ngủ 02 : 13.50 m2</li>
                                                                     <li>Phòng ngủ 03 : 12.30 m2</li>
                                                                     <li>Vệ sinh 01 : 3.80 m2</li>
                                                                     <li>Vệ sinh 02 : 5.00 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-1">
                                                               <h2 class="hotspot-title">Căn hộ A02</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 92.10 m2</li>
                                                                     <li>Diện tích thông thủy: 86.70 m2</li>
                                                                     <li>Ban công Đông Bắc, hướng cửa vào Đông Nam.</li>
                                                                     <li>Phòng khách : 33.60 m2</li>
                                                                     <li>Phòng ngủ 01 : 11.80 m2</li>
                                                                     <li>Phòng ngủ 02 : 17.30 m2</li>
                                                                     <li>Vệ sinh 01 : 4.00 m2</li>
                                                                     <li>Vệ sinh 02 : 3.70 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-2">
                                                               <h2 class="hotspot-title">Căn hộ A03</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg 1024w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 126.10 m2</li>
                                                                     <li>Diện tích thông thủy: 118.00 m2</li>
                                                                     <li>Ban công Đông Bắc, hướng cửa vào Tây Bắc.</li>
                                                                     <li>Phòng khách : 44.80 m2</li>
                                                                     <li>Phòng ngủ 01 : 18.00 m2</li>
                                                                     <li>Phòng ngủ 02 : 13.00 m2</li>
                                                                     <li>Phòng ngủ 03 : 18.20 m2</li>
                                                                     <li>Vệ sinh 01 : 3.50 m2</li>
                                                                     <li>Vệ sinh 02 : 5.60 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-3">
                                                               <h2 class="hotspot-title">Căn hộ A04</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 98.20 m2</li>
                                                                     <li>Diện tích thông thủy: 90.90 m2</li>
                                                                     <li>Ban công Đông Bắc, hướng cửa vào Tây Nam.</li>
                                                                     <li>Phòng khách : 32.50 m2</li>
                                                                     <li>Phòng ngủ 01 : 17.10 m2</li>
                                                                     <li>Phòng ngủ 02 : 17.20 m2</li>
                                                                     <li>Vệ sinh 01 : 3.80 m2</li>
                                                                     <li>Vệ sinh 02 : 3.20 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-4">
                                                               <h2 class="hotspot-title">Căn hộ A05</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 94.00 m2</li>
                                                                     <li>Diện tích thông thủy: 87.80 m2</li>
                                                                     <li>Ban công Đông Bắc, hướng cửa vào Tây Nam.</li>
                                                                     <li>Phòng khách : 33.60 m2</li>
                                                                     <li>Phòng ngủ 01 : 13.30 m2</li>
                                                                     <li>Phòng ngủ 02 : 12.10 m2</li>
                                                                     <li>Vệ sinh 01 : 5.20 m2</li>
                                                                     <li>Vệ sinh 02 : 4.20 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-5">
                                                               <h2 class="hotspot-title">Căn hộ A06</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 89.90 m2</li>
                                                                     <li>Diện tích thông thủy: 83.20 m2</li>
                                                                     <li>Ban công Đông Bắc, hướng cửa vào Tây Nam.</li>
                                                                     <li>Phòng khách : 34.60 m2</li>
                                                                     <li>Phòng ngủ 01 : 11.30 m2</li>
                                                                     <li>Phòng ngủ 02 : 15.90 m2</li>
                                                                     <li>Vệ sinh 01 : 3.50 m2</li>
                                                                     <li>Vệ sinh 02 : 3.80 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-6">
                                                               <h2 class="hotspot-title">Căn hộ A07</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 126.30 m2</li>
                                                                     <li>Diện tích thông thủy: 118.10 m2</li>
                                                                     <li>Ban công Đông Bắc, hướng cửa vào Đông Nam.</li>
                                                                     <li>Phòng khách : 44.30 m2</li>
                                                                     <li>Phòng ngủ 01 : 15.00 m2</li>
                                                                     <li>Phòng ngủ 02 : 13.80 m2</li>
                                                                     <li>Phòng ngủ 03 : 17.80 m2</li>
                                                                     <li>Vệ sinh 01 : 3.50 m2</li>
                                                                     <li>Vệ sinh 02 : 3.80 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-7">
                                                               <h2 class="hotspot-title">Căn hộ A08</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 96.30 m2</li>
                                                                     <li>Diện tích thông thủy: 89.20 m2</li>
                                                                     <li>Ban công Đông Nam, hướng cửa vào Tây Bắc.</li>
                                                                     <li>Phòng khách : 34.10 m2</li>
                                                                     <li>Phòng ngủ 01 : 14.60 m2</li>
                                                                     <li>Phòng ngủ 02 : 17.10 m2</li>
                                                                     <li>Vệ sinh 01 : 4.10 m2</li>
                                                                     <li>Vệ sinh 02 : 3.90 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-8">
                                                               <h2 class="hotspot-title">Căn hộ A09</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 116.50 m2</li>
                                                                     <li>Diện tích thông thủy: 107.10 m2</li>
                                                                     <li>Ban công Đông Nam, hướng cửa vào Tây Bắc.</li>
                                                                     <li>Phòng khách : 38.50 m2</li>
                                                                     <li>Phòng ngủ 01 : 14.80 m2</li>
                                                                     <li>Phòng ngủ 02 : 13.60 m2</li>
                                                                     <li>Phòng ngủ 03 : 12.40 m2</li>
                                                                     <li>Vệ sinh 01 : 3.50 m2</li>
                                                                     <li>Vệ sinh 02 : 5.60 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-9">
                                                               <h2 class="hotspot-title">Căn hộ A10</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 116.50 m2</li>
                                                                     <li>Diện tích thông thủy: 106.90 m2</li>
                                                                     <li>Ban công Đông Nam, hướng cửa vào Tây Bắc.</li>
                                                                     <li>Phòng khách : 38.70 m2</li>
                                                                     <li>Phòng ngủ 01 : 14.90 m2</li>
                                                                     <li>Phòng ngủ 02 : 12.50 m2</li>
                                                                     <li>Phòng ngủ 03 : 13.50 m2</li>
                                                                     <li>Vệ sinh 01 : 4.00 m2</li>
                                                                     <li>Vệ sinh 02 : 3.70 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-10">
                                                               <h2 class="hotspot-title">Căn hộ A11</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 96.30 m2</li>
                                                                     <li>Diện tích thông thủy: 89.20 m2</li>
                                                                     <li>Ban công Tây Nam, hướng cửa vào Đông Bắc.</li>
                                                                     <li>Phòng khách : 34.20 m2</li>
                                                                     <li>Phòng ngủ 01 : 14.70 m2</li>
                                                                     <li>Phòng ngủ 02 : 17.30 m2</li>
                                                                     <li>Vệ sinh 01 : 3.90 m2</li>
                                                                     <li>Vệ sinh 02 : 4.20 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-11">
                                                               <h2 class="hotspot-title">Căn hộ A12</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 126.10 m2</li>
                                                                     <li>Diện tích thông thủy: 118.00 m2</li>
                                                                     <li>Ban công Tây Nam &amp; Tây Bắc, hướng cửa vào Đông Nam.</li>
                                                                     <li>Phòng khách : 44.70 m2</li>
                                                                     <li>Phòng ngủ 01 : 18.40 m2</li>
                                                                     <li>Phòng ngủ 02 : 13.80 m2</li>
                                                                     <li>Phòng ngủ 03 : 17.60 m2</li>
                                                                     <li>Vệ sinh 01 : 3.90 m2</li>
                                                                     <li>Vệ sinh 02 : 4.30 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-12">
                                                               <h2 class="hotspot-title">Căn hộ A13</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 97.30 m2</li>
                                                                     <li>Diện tích thông thủy: 88.80 m2</li>
                                                                     <li>Ban công Tây Nam, hướng cửa vào Đông Bắc.</li>
                                                                     <li>Phòng khách : 30.80 m2</li>
                                                                     <li>Phòng ngủ 01 : 16.60 m2</li>
                                                                     <li>Phòng ngủ 02 : 15.80 m2</li>
                                                                     <li>Vệ sinh 01 : 4.30 m2</li>
                                                                     <li>Vệ sinh 02 : 4.50 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-13">
                                                               <h2 class="hotspot-title">Căn hộ A14</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 93.00 m2</li>
                                                                     <li>Diện tích thông thủy: 86.80 m2</li>
                                                                     <li>Ban công Tây Nam, hướng cửa vào Đông Bắc.</li>
                                                                     <li>Phòng khách : 31.60 m2</li>
                                                                     <li>Phòng ngủ 01 : 14.80 m2</li>
                                                                     <li>Phòng ngủ 02 : 13.40 m2</li>
                                                                     <li>Vệ sinh 01 : 4.60 m2</li>
                                                                     <li>Vệ sinh 02 : 4.20 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-14">
                                                               <h2 class="hotspot-title">Căn hộ A15</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 89.50 m2</li>
                                                                     <li>Diện tích thông thủy: 82.80 m2</li>
                                                                     <li>Ban công Tây Nam, hướng cửa vào Đông Bắc.</li>
                                                                     <li>Phòng khách : 33.80 m2</li>
                                                                     <li>Phòng ngủ 01 : 12.70 m2</li>
                                                                     <li>Phòng ngủ 02 : 14.60 m2</li>
                                                                     <li>Vệ sinh 01 : 4.30 m2</li>
                                                                     <li>Vệ sinh 02 : 3.70 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-15">
                                                               <h2 class="hotspot-title">Căn hộ A16</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 126.30 m2</li>
                                                                     <li>Diện tích thông thủy: 117.90 m2</li>
                                                                     <li>Ban công Tây Bắc &amp; Tây Nam, hướng cửa vào Tây Bắc.</li>
                                                                     <li>Phòng khách : 44.50 m2</li>
                                                                     <li>Phòng ngủ 01 : 18.40 m2</li>
                                                                     <li>Phòng ngủ 02 : 17.70 m2</li>
                                                                     <li>Phòng ngủ 03 : 14.60 m2</li>
                                                                     <li>Vệ sinh 01 : 3.20 m2</li>
                                                                     <li>Vệ sinh 02 : 4.00 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-16">
                                                               <h2 class="hotspot-title">Căn hộ A17</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>2 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 92.10 m2</li>
                                                                     <li>Diện tích thông thủy: 85.70 m2</li>
                                                                     <li>Ban công Tây Nam, hướng cửa vào Đông Nam.</li>
                                                                     <li>Phòng khách : 33.60 m2</li>
                                                                     <li>Phòng ngủ 01 : 11.40 m2</li>
                                                                     <li>Phòng ngủ 02 : 17.10 m2</li>
                                                                     <li>Vệ sinh 01 : 3.70 m2</li>
                                                                     <li>Vệ sinh 02 : 4.10 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <div class="hotspot-info" id="hotspot-hotspot-12546-17">
                                                               <h2 class="hotspot-title">Căn hộ A18</h2>
                                                               <div class="hotspot-thumb"><img width="300" height="212" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg" class="attachment-medium size-medium" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-300x212.jpg 300w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-768x543.jpg 768w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-1024x724.jpg 1024w, https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3-1200x849.jpg 1200w" sizes="(max-width: 300px) 100vw, 300px" /></div>
                                                               <div class="hotspot-content">
                                                                  <ul>
                                                                     <li>3 Phòng ngủ - 2 Vệ sinh</li>
                                                                     <li>Diện tích tim tường: 131.90 m2</li>
                                                                     <li>Diện tích thông thủy: 122.40 m2</li>
                                                                     <li>Ban công Tây Bắc &amp; Tây Nam, hướng cửa vào Đông Nam.</li>
                                                                     <li>Phòng khách : 43.80 m2</li>
                                                                     <li>Phòng ngủ 01 : 18.00 m2</li>
                                                                     <li>Phòng ngủ 02 : 16.00 m2</li>
                                                                     <li>Phòng ngủ 03 : 12.30 m2</li>
                                                                     <li>Vệ sinh 01 : 4.10 m2</li>
                                                                     <li>Vệ sinh 02 : 4.20 m2</li>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngkĐthapulico" href="#tab-84e2f05a80da37245ef">
                                                      <h4 class="fusion-tab-heading">MẶT BẰNG KĐT HAPULICO</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-84e2f05a80da37245ef">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"  style='margin-top: 0px;margin-bottom: 20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-3 hover-type-none fusion-animated" data-animationType="zoomInLeft" data-animationDuration="1.0" data-animationOffset="100%"><a href="https://haiphatland.vn/wp-content/uploads/2016/12/mat-bang-kdt-hapulico-thanh-xuan-complex.jpg" class="fusion-lightbox" data-rel="iLightbox[a50675d2969ce75f8ac]"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/mat-bang-kdt-hapulico-thanh-xuan-complex.jpg" width="" height="" alt="Song lap tang 1" class="img-responsive"/></a></span></div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="fusion-text">
                                                         <div class="floorplan-summary">
                                                            <p><span style="color: #143c57;"><strong>Khu đô thị HAPULICO</strong>  có quy mô 6 ha, Quần thể của Khu đô thị bao gồm 9 Khối cao tầng &amp; Biệt thự nhà vườn. Trong đó: <span style="color: #d2bd56;"><strong><a style="color: #d2bd56;" href="http://foresa-villas.com/product/biet-thu-song-lap.html">Xem chi tiết</a></strong></span></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối tháp Văn phòng &#8211; <span style="color: #ff0000;">24T1</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">24T2</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">21T1</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">21T2</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">17T1</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">17T2</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">17T3</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">17T4</span></strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>&#8211; Khối Chung cư cao cấp &#8211; <span style="color: #ff0000;">24T3 (Thanh Xuân Complex)</span></strong></span></p>
                                                         </div>
                                                         <p style="text-align: center;">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-nỘithẤtcĂnhỘ" href="#tab-db510fb7f7f70c73ca4">
                                                      <h4 class="fusion-tab-heading">NỘI THẤT CĂN HỘ</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-db510fb7f7f70c73ca4">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"  style='margin-top: 0px;margin-bottom: 20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="fusion-slider-container fusion-slider-45 full-width-slider " id="fusion-slider-sc-thanh-xuan-complex" style="height:400px; max-width:100%;">
                                                         <style type="text/css" scoped="scoped">
                                                            .fusion-slider-45 .flex-direction-nav a {
                                                            width:63px;height:63px;line-height:63px;font-size:25px;                                   }
                                                         </style>
                                                         <div class="fusion-slider-loading">Loading...</div>
                                                         <div class="tfs-slider flexslider main-flex full-width-slider" data-slider_width="100%" data-slider_height="400px" data-full_screen="0" data-parallax="0" data-nav_arrows="1" data-nav_box_width="63px" data-nav_box_height="63px" data-nav_arrow_size="25px" data-pagination_circles="0" data-autoplay="1" data-loop="1" data-animation="fade" data-slideshow_speed="4000" data-animation_speed="500" data-typo_sensitivity="1" data-typo_factor="1.5" style="max-width:100%;">
                                                            <ul class="slides">
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-khach-can-ho-2-phong-ngu-thanh-xuan-complex.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-khach-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-khach-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-master-can-ho-2-phong-ngu-thanh-xuan-complex.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-master-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-master-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-tre-em-thanh-xuan-complex-hapulico-24t3.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-tre-em-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-tre-em-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/bep-can-ho-2-phong-ngu-thanh-xuan-complex-hapulico-24t3.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/bep-can-ho-2-phong-ngu-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/bep-can-ho-2-phong-ngu-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                           <div class="tfs-button-2">
                                                                              <div class="fusion-button-wrapper">
                                                                                 <style type="text/css" scoped="scoped">.fusion-button.button-7 .fusion-button-text, .fusion-button.button-7 i {color:rgba(255,255,255,.8);}.fusion-button.button-7 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-7 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-7:hover .fusion-button-text, .fusion-button.button-7:hover i,.fusion-button.button-7:focus .fusion-button-text, .fusion-button.button-7:focus i,.fusion-button.button-7:active .fusion-button-text, .fusion-button.button-7:active{color:rgba(255,255,255,.9);}.fusion-button.button-7:hover, .fusion-button.button-7:focus, .fusion-button.button-7:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-7:hover .fusion-button-icon-divider, .fusion-button.button-7:hover .fusion-button-icon-divider, .fusion-button.button-7:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-7{background: #a0ce4e;}.fusion-button.button-7:hover,.button-7:focus,.fusion-button.button-7:active{background: #87be22;}.fusion-button.button-7{width:auto;}</style>
                                                                                 <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-7" target="_self" href="http://themeforest.net/item/avada-responsive-multipurpose-theme/2833226?ref=ThemeFusion"><span class="fusion-button-text">Đăng ký tư vấn</span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ve-sinh-thanh-xuan-complex-hapulico-24t3.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ve-sinh-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ve-sinh-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="fusion-text">
                                                         <div class="floorplan-summary">
                                                            <p><span style="color: #143c57;">Chung cư Hapulico 24T3 (Thanh Xuân Complex) được trang bị c&#8230; <span style="color: #d2bd56;"><a style="color: #d2bd56;" href="http://foresa-villas.com/product/nha-lien-ke.html"><strong>Xem chi tiết</strong></a></span></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng khách: Sàn, Tường, Trần,</strong></span><strong> </strong><span style="color: #143c57;"><strong>Cửa đi chính vào căn hộ, Cửa sổ, cửa ra vào logia, Thiết bị điện, Điều hoà nhiệt độ, Loa thông báo.</strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng ngủ điển hình: </strong><strong>Sàn, Tường, Trần, Cửa đi thông phòng, Cửa sổ, cửa ra vào logia, Thiết bị điện, Điều hoà nhiệt độ.</strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng vệ sinh điển hình: </strong><strong>Sàn, Tường, Thiết bị vệ sinh, Trần, Cửa đi vào vệ sinh, Thiết bị điện.</strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng bếp: </strong><strong>Sàn, Tường, Trần, Tủ bếp, Thiết bị điện.</strong></span></p>
                                                            <p><strong>+) <span style="color: #143c57;">Logia: Sàn, Cửa sổ, Cửa đi.</span></strong></p>
                                                            <p><span style="color: #143c57;"><strong>+) Sảnh chính, hành lang &amp; cầu thang: </strong><strong>Sàn hành lang, sảnh chung, Tường, Trần, Cầu thang bộ.</strong></span></p>
                                                         </div>
                                                         <p style="text-align: center;">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"><span class="icon-wrapper" style="border-color:#d2bd56;background-color:rgba(255,255,255,0);"><i class=" fa fa-cubes" style="color:#d2bd56;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2014/06/bkgd13.jpg");background-position: left top;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Căn hộ 2 Phòng Ngủ</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                       <p style="text-align: center; font-size: 15px; font-weight: bold;">Căn hộ 2 Phòng ngủ của Chung cư Thanh Xuân Complex với 10 căn hộ (1 sàn), mỗi căn hộ tại chung cư Hapulico 24T3 có diện tích 80 m2- 90 m2.</p>
                                    </div>
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                          <style type="text/css">
                                             a.eg-henryharrison-element-1,a.eg-henryharrison-element-2{-webkit-transition:all .4s linear;   -moz-transition:all .4s linear;   -o-transition:all .4s linear;   -ms-transition:all .4s linear;   transition:all .4s linear}.eg-jimmy-carter-element-11 i:before{margin-left:0px; margin-right:0px}.eg-harding-element-17{letter-spacing:1px}.eg-harding-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-harding-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-ulysses-s-grant-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-ulysses-s-grant-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-richard-nixon-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-richard-nixon-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-herbert-hoover-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-herbert-hoover-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.eg-lyndon-johnson-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-lyndon-johnson-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.esg-overlay.eg-ronald-reagan-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-georgebush-wrapper .esg-entry-cover{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-jefferson-wrapper{-webkit-border-radius:5px !important; -moz-border-radius:5px !important; border-radius:5px !important; -webkit-mask-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC) !important}.eg-monroe-element-1{text-shadow:0px 1px 3px rgba(0,0,0,0.1)}.eg-lyndon-johnson-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-media img{-webkit-transition:0.4s ease-in-out;  -moz-transition:0.4s ease-in-out;  -o-transition:0.4s ease-in-out;  transition:0.4s ease-in-out;  filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-wilbert-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.eg-phillie-element-3:after{content:" ";width:0px;height:0px;border-style:solid;border-width:5px 5px 0 5px;border-color:#000 transparent transparent transparent;left:50%;margin-left:-5px; bottom:-5px; position:absolute}.eg-howardtaft-wrapper .esg-entry-media img,.eg-howardtaft-wrapper .esg-media-poster{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.eg-howardtaft-wrapper:hover .esg-entry-media img,.eg-howardtaft-wrapper:hover .esg-media-poster{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.myportfolio-container .added_to_cart.wc-forward{font-family:"Open Sans"; font-size:13px; color:#fff; margin-top:10px}.esgbox-title.esgbox-title-outside-wrap{font-size:15px; font-weight:700; text-align:center}.esgbox-title.esgbox-title-inside-wrap{padding-bottom:10px; font-size:15px; font-weight:700; text-align:center}.esg-content.eg-twitterstream-element-33-a{display:inline-block}.eg-twitterstream-element-35{word-break:break-all}.esg-overlay.eg-twitterstream-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.esg-content.eg-facebookstream-element-33-a{display:inline-block}.eg-facebookstream-element-0{word-break:break-all}.esg-overlay.eg-flickrstream-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}
                                          </style>
                                          <!-- CACHE CREATED FOR: 1 -->
                                          <style type="text/css">.minimal-light .navigationbuttons,.minimal-light .esg-pagination,.minimal-light .esg-filters{text-align:center}.minimal-light .esg-filterbutton,.minimal-light .esg-navigationbutton,.minimal-light .esg-sortbutton,.minimal-light .esg-cartbutton a{color:#999; margin-right:5px; cursor:pointer; padding:0px 16px; border:1px solid #e5e5e5; line-height:38px; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; font-size:12px; font-weight:700; font-family:"Open Sans",sans-serif; display:inline-block; background:#fff; margin-bottom:5px}.minimal-light .esg-navigationbutton *{color:#999}.minimal-light .esg-navigationbutton{padding:0px 16px}.minimal-light .esg-pagination-button:last-child{margin-right:0}.minimal-light .esg-left,.minimal-light .esg-right{padding:0px 11px}.minimal-light .esg-sortbutton-wrapper,.minimal-light .esg-cartbutton-wrapper{display:inline-block}.minimal-light .esg-sortbutton-order,.minimal-light .esg-cartbutton-order{display:inline-block;  vertical-align:top;  border:1px solid #e5e5e5;  width:40px;  line-height:38px;  border-radius:0px 5px 5px 0px;  -moz-border-radius:0px 5px 5px 0px;  -webkit-border-radius:0px 5px 5px 0px;  font-size:12px;  font-weight:700;  color:#999;  cursor:pointer;  background:#fff}.minimal-light .esg-cartbutton{color:#333; cursor:default !important}.minimal-light .esg-cartbutton .esgicon-basket{color:#333;   font-size:15px;   line-height:15px;   margin-right:10px}.minimal-light .esg-cartbutton-wrapper{cursor:default !important}.minimal-light .esg-sortbutton,.minimal-light .esg-cartbutton{display:inline-block; position:relative; cursor:pointer; margin-right:0px; border-right:none; border-radius:5px 0px 0px 5px; -moz-border-radius:5px 0px 0px 5px; -webkit-border-radius:5px 0px 0px 5px}.minimal-light .esg-navigationbutton:hover,.minimal-light .esg-filterbutton:hover,.minimal-light .esg-sortbutton:hover,.minimal-light .esg-sortbutton-order:hover,.minimal-light .esg-cartbutton a:hover,.minimal-light .esg-filterbutton.selected{background-color:#fff;   border-color:#bbb;   color:#333;   box-shadow:0px 3px 5px 0px rgba(0,0,0,0.13)}.minimal-light .esg-navigationbutton:hover *{color:#333}.minimal-light .esg-sortbutton-order.tp-desc:hover{border-color:#bbb; color:#333; box-shadow:0px -3px 5px 0px rgba(0,0,0,0.13) !important}.minimal-light .esg-filter-checked{padding:1px 3px;  color:#cbcbcb;  background:#cbcbcb;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}.minimal-light .esg-filterbutton.selected .esg-filter-checked,.minimal-light .esg-filterbutton:hover .esg-filter-checked{padding:1px 3px 1px 3px;  color:#fff;  background:#000;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}</style>
                                          <style type="text/css">.eg-washington-element-0{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; padding:17px 17px 17px 17px !important; border-radius:60px 60px 60px 60px !important; background:rgba(255,255,255,0.15) !important; z-index:2 !important; display:block; font-family:"Open Sans" !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}.eg-washington-element-1{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; padding:17px 17px 17px 17px !important; border-radius:60px 60px 60px 60px !important; background:rgba(255,255,255,0.15) !important; z-index:2 !important; display:block; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}.eg-washington-element-3{font-size:13px; line-height:20px; color:#ffffff; font-weight:700; display:inline-block; float:none; clear:both; margin:15px 0px 0px 0px ; padding:5px 10px 5px 10px ; border-radius:0px 0px 0px 0px ; background:rgba(255,255,255,0.15); position:relative; z-index:2 !important; font-family:"Open Sans"; text-transform:uppercase}</style>
                                          <style type="text/css">.eg-washington-element-0:hover{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; border-radius:60px 60px 60px 60px !important; background:rgba(0,0,0,0.50) !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}.eg-washington-element-1:hover{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; border-radius:60px 60px 60px 60px !important; background:rgba(0,0,0,0.50) !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}</style>
                                          <style type="text/css">.eg-washington-element-0-a{display:inline-block !important; float:none !important; clear:none !important; margin:0px 10px 0px 0px !important; position:relative !important}</style>
                                          <style type="text/css">.eg-washington-element-1-a{display:inline-block !important; float:none !important; clear:none !important; margin:0px 10px 0px 0px !important; position:relative !important}</style>
                                          <style type="text/css">.eg-washington-container{background:rgba(0,0,0,0.65)}</style>
                                          <style type="text/css">.eg-washington-content{background:#ffffff; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:double; text-align:left}</style>
                                          <style type="text/css">.esg-grid .mainul li.eg-washington-wrapper{background:#3f424a; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:none}</style>
                                          <style type="text/css">.esg-grid .mainul li.eg-washington-wrapper .esg-media-poster{background-size:cover; background-position:center center; background-repeat:no-repeat}</style>
                                          <!-- THE ESSENTIAL GRID 2.1.6.1 POST -->
                                       <article class="myportfolio-container minimal-light" id="esg-grid-1-1-wrap">
                                          <div id="esg-grid-1-1" class="esg-grid" style="background: transparent;padding: 0px 25px 25px 25px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;">
                                             <ul>
                                                <li id="eg-1-post-id-12100" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12100" data-date="1482482933">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12100 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12100 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a2-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A2 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12100 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12100" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a2-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12100 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A2 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12102" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12102" data-date="1482483764">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12102 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12102 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a4-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A4 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12102 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12102" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a4-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12102 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A4 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12103" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12103" data-date="1482483806">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12103 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12103 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a5-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A5 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12103 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12103" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a5-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12103 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A5 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12104" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12104" data-date="1482483839">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12104 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12104 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a6-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A6 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12104 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12104" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a6-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12104 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A6 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12105" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12105" data-date="1482483861">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12105 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12105 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a8-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A8 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12105 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12105" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a8-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12105 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A8 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12106" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12106" data-date="1482483917">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12106 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12106 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a11-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A11 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12106 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12106" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a11-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12106 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A11 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12107" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12107" data-date="1482483958">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12107 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12107 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a13-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A13 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12107 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12107" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a13-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12107 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A13 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12108" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12108" data-date="1482483988">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12108 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12108 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a14-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A14 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12108 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12108" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a14-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12108 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A14 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12109" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12109" data-date="1482484015">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12109 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12109 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a15-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn A15 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12109 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12109" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-a15-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12109 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn A15 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-1-post-id-12110" class="filterall filter-can-2-ngu eg-washington-wrapper eg-post-id-12110" data-date="1482484044">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12110 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12110 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a17-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A17 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12110 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12110" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a17-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12110 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A17 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                             </ul>
                                          </div>
                                       </article>
                                       <div class="clear"></div>
                                       <script type="text/javascript">
                                          function eggbfc(winw,resultoption) {
                                                var lasttop = winw,
                                                lastbottom = 0,
                                                smallest =9999,
                                                largest = 0,
                                                samount = 0,
                                                lamoung = 0,
                                                lastamount = 0,
                                                resultid = 0,
                                                resultidb = 0,
                                                responsiveEntries = [
                                                                              { width:1400,amount:5,mmheight:0},
                                                                              { width:1170,amount:5,mmheight:0},
                                                                              { width:1024,amount:4,mmheight:0},
                                                                              { width:960,amount:3,mmheight:0},
                                                                              { width:778,amount:3,mmheight:0},
                                                                              { width:640,amount:3,mmheight:0},
                                                                              { width:480,amount:1,mmheight:0}
                                                                              ];
                                                if (responsiveEntries!=undefined && responsiveEntries.length>0)
                                                      jQuery.each(responsiveEntries, function(index,obj) {
                                                            var curw = obj.width != undefined ? obj.width : 0,
                                                                  cura = obj.amount != undefined ? obj.amount : 0;
                                                            if (smallest>curw) {
                                                                  smallest = curw;
                                                                  samount = cura;
                                                                  resultidb = index;
                                                            }
                                                            if (largest<curw) {
                                                                  largest = curw;
                                                                  lamount = cura;
                                                            }
                                                            if (curw>lastbottom && curw<=lasttop) {
                                                                  lastbottom = curw;
                                                                  lastamount = cura;
                                                                  resultid = index;
                                                            }
                                                      });
                                                      if (smallest>winw) {
                                                            lastamount = samount;
                                                            resultid = resultidb;
                                                      }
                                                      var obj = new Object;
                                                      obj.index = resultid;
                                                      obj.column = lastamount;
                                                      if (resultoption=="id")
                                                            return obj;
                                                      else
                                                            return lastamount;
                                                }
                                          if ("even"=="even") {
                                                var coh=0,
                                                      container = jQuery("#esg-grid-1-1");
                                                var   cwidth = container.width(),
                                                      ar = "2339:1654",
                                                      gbfc = eggbfc(jQuery(window).width(),"id"),
                                                row = 3;
                                          ar = ar.split(":");
                                          aratio=parseInt(ar[0],0) / parseInt(ar[1],0);
                                          coh = cwidth / aratio;
                                          coh = coh/gbfc.column*row;
                                                var ul = container.find("ul").first();
                                                ul.css({display:"block",height:coh+"px"});
                                          }
                                          var essapi_1;
                                          jQuery(document).ready(function() {
                                                essapi_1 = jQuery("#esg-grid-1-1").tpessential({
                                                  gridID:1,
                                                  layout:"even",
                                                  forceFullWidth:"off",
                                                  lazyLoad:"off",
                                                  row:3,
                                                  loadMoreAjaxToken:"7e070fec30",
                                                  loadMoreAjaxUrl:"https://www.haiphatland.vn/wp-admin/admin-ajax.php",
                                                  loadMoreAjaxAction:"Essential_Grid_Front_request_ajax",
                                                  ajaxContentTarget:"ess-grid-ajax-container-",
                                                  ajaxScrollToOffset:"0",
                                                  ajaxCloseButton:"off",
                                                  ajaxContentSliding:"on",
                                                  ajaxScrollToOnLoad:"on",
                                                  ajaxCallbackArgument:"off",
                                                  ajaxNavButton:"off",
                                                  ajaxCloseType:"type1",
                                                  ajaxCloseInner:"false",
                                                  ajaxCloseStyle:"light",
                                                  ajaxClosePosition:"tr",
                                                  space:20,
                                                  pageAnimation:"fade",
                                                  paginationScrollToTop:"off",
                                                  spinner:"spinner0",
                                                  forceFullWidth:"on",
                                                  evenGridMasonrySkinPusher:"off",
                                                  lightBoxMode:"single",
                                                  lightboxSpinner:"off",
                                                  lightBoxFeaturedImg:"off",
                                                  lightBoxPostTitle:"off",
                                                  lightBoxPostTitleTag:"h2",
                                                  animSpeed:1000,
                                                  delayBasic:1,
                                                  mainhoverdelay:1,
                                                  filterType:"single",
                                                  showDropFilter:"hover",
                                                  filterGroupClass:"esg-fgc-1",
                                                  googleFonts:['Open+Sans:300,400,600,700,800','Raleway:100,200,300,400,500,600,700,800,900','Droid+Serif:400,700'],
                                                  aspectratio:"2339:1654",
                                                  responsiveEntries: [
                                                                              { width:1400,amount:5,mmheight:0},
                                                                              { width:1170,amount:5,mmheight:0},
                                                                              { width:1024,amount:4,mmheight:0},
                                                                              { width:960,amount:3,mmheight:0},
                                                                              { width:778,amount:3,mmheight:0},
                                                                              { width:640,amount:3,mmheight:0},
                                                                              { width:480,amount:1,mmheight:0}
                                                                              ]
                                                });
                                          
                                                try{
                                                jQuery("#esg-grid-1-1 .esgbox").esgbox({
                                                      padding : [0,0,0,0],
                                                      width:"960",
                                                      height:"540",
                                                      minWidth:"100",
                                                      minHeight:"100",
                                                      maxWidth:"9999",
                                                      maxHeight:"9999",
                                                      autoPlay:false,
                                                      playSpeed:3000,
                                                      preload:3,
                                                beforeLoad:function(t) {
                                                    return essapi_1.eslightboxpost(t);
                                                       },
                                                afterLoad:function() { 
                                                      if (this.element.hasClass("esgboxhtml5")) {
                                                            this.type ="html5";
                                                         var mp = this.element.data("mp4"),
                                                            ogv = this.element.data("ogv"),
                                                            webm = this.element.data("webm");
                                                            ratio = this.element.data("ratio");
                                                            ratio = ratio==="16:9" ? "56.25%" : "75%"
                                                   this.content ='<div class="esg-lb-video-wrapper" style="width:100%"><video autoplay="true" loop=""  poster="" width="100%" height="auto" controls><source src="'+mp+'" type="video/mp4"><source src="'+webm+'" type="video/webm"><source src="'+ogv+'" type="video/ogg"></video></div>';
                                                         };
                                                       },
                                                      beforeShow : function () { 
                                                            this.title = jQuery(this.element).attr('lgtitle');
                                                            if (this.title) {
                                                                  this.title="";
                                                      this.title =  '<div style="padding:0px 0px 0px 0px">'+this.title+'</div>';
                                                            }
                                                      },
                                                      afterShow : function() {
                                                      },
                                                      openEffect : 'fade',
                                                      closeEffect : 'fade',
                                                      nextEffect : 'fade',
                                                      prevEffect : 'fade',
                                                      openSpeed : 'normal',
                                                      closeSpeed : 'normal',
                                                      nextSpeed : 'normal',
                                                      prevSpeed : 'normal',
                                                      helpers:{overlay:{locked:false}},
                                                      helpers : {
                                                            media : {},
                                                            overlay: {
                                                                  locked: false
                                                            },
                                                          title : {
                                                                  type:""
                                                            }
                                                      }
                                          });
                                          
                                           } catch (e) {}
                                          
                                          });
                                       </script>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Căn hộ 3 Phòng Ngủ</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                       <p style="text-align: center; font-size: 15px; font-weight: bold;">Căn hộ 3 Phòng ngủ của Chung cư Thanh Xuân Complex với 8 căn hộ (1 sàn), mỗi căn hộ tại chung cư Hapulico 24T3 có diện tích từ 105 m2- 122 m2.</p>
                                    </div>
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                          <style type="text/css">
                                             a.eg-henryharrison-element-1,a.eg-henryharrison-element-2{-webkit-transition:all .4s linear;   -moz-transition:all .4s linear;   -o-transition:all .4s linear;   -ms-transition:all .4s linear;   transition:all .4s linear}.eg-jimmy-carter-element-11 i:before{margin-left:0px; margin-right:0px}.eg-harding-element-17{letter-spacing:1px}.eg-harding-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-harding-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-ulysses-s-grant-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-ulysses-s-grant-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-richard-nixon-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-richard-nixon-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-herbert-hoover-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-herbert-hoover-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.eg-lyndon-johnson-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-lyndon-johnson-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.esg-overlay.eg-ronald-reagan-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-georgebush-wrapper .esg-entry-cover{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-jefferson-wrapper{-webkit-border-radius:5px !important; -moz-border-radius:5px !important; border-radius:5px !important; -webkit-mask-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC) !important}.eg-monroe-element-1{text-shadow:0px 1px 3px rgba(0,0,0,0.1)}.eg-lyndon-johnson-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-media img{-webkit-transition:0.4s ease-in-out;  -moz-transition:0.4s ease-in-out;  -o-transition:0.4s ease-in-out;  transition:0.4s ease-in-out;  filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-wilbert-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.eg-phillie-element-3:after{content:" ";width:0px;height:0px;border-style:solid;border-width:5px 5px 0 5px;border-color:#000 transparent transparent transparent;left:50%;margin-left:-5px; bottom:-5px; position:absolute}.eg-howardtaft-wrapper .esg-entry-media img,.eg-howardtaft-wrapper .esg-media-poster{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");  -webkit-filter:grayscale(0%)}.eg-howardtaft-wrapper:hover .esg-entry-media img,.eg-howardtaft-wrapper:hover .esg-media-poster{filter:url("data:image/svg+xml;utf8,
                                             <svg xmlns='http://www.w3.org/2000/svg'>
                                                <filter id='grayscale'>
                                                   <feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/>
                                                </filter>
                                             </svg>
                                             #grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.myportfolio-container .added_to_cart.wc-forward{font-family:"Open Sans"; font-size:13px; color:#fff; margin-top:10px}.esgbox-title.esgbox-title-outside-wrap{font-size:15px; font-weight:700; text-align:center}.esgbox-title.esgbox-title-inside-wrap{padding-bottom:10px; font-size:15px; font-weight:700; text-align:center}.esg-content.eg-twitterstream-element-33-a{display:inline-block}.eg-twitterstream-element-35{word-break:break-all}.esg-overlay.eg-twitterstream-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.esg-content.eg-facebookstream-element-33-a{display:inline-block}.eg-facebookstream-element-0{word-break:break-all}.esg-overlay.eg-flickrstream-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}
                                          </style>
                                          <!-- CACHE CREATED FOR: 2 -->
                                          <style type="text/css">.minimal-light .navigationbuttons,.minimal-light .esg-pagination,.minimal-light .esg-filters{text-align:center}.minimal-light .esg-filterbutton,.minimal-light .esg-navigationbutton,.minimal-light .esg-sortbutton,.minimal-light .esg-cartbutton a{color:#999; margin-right:5px; cursor:pointer; padding:0px 16px; border:1px solid #e5e5e5; line-height:38px; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; font-size:12px; font-weight:700; font-family:"Open Sans",sans-serif; display:inline-block; background:#fff; margin-bottom:5px}.minimal-light .esg-navigationbutton *{color:#999}.minimal-light .esg-navigationbutton{padding:0px 16px}.minimal-light .esg-pagination-button:last-child{margin-right:0}.minimal-light .esg-left,.minimal-light .esg-right{padding:0px 11px}.minimal-light .esg-sortbutton-wrapper,.minimal-light .esg-cartbutton-wrapper{display:inline-block}.minimal-light .esg-sortbutton-order,.minimal-light .esg-cartbutton-order{display:inline-block;  vertical-align:top;  border:1px solid #e5e5e5;  width:40px;  line-height:38px;  border-radius:0px 5px 5px 0px;  -moz-border-radius:0px 5px 5px 0px;  -webkit-border-radius:0px 5px 5px 0px;  font-size:12px;  font-weight:700;  color:#999;  cursor:pointer;  background:#fff}.minimal-light .esg-cartbutton{color:#333; cursor:default !important}.minimal-light .esg-cartbutton .esgicon-basket{color:#333;   font-size:15px;   line-height:15px;   margin-right:10px}.minimal-light .esg-cartbutton-wrapper{cursor:default !important}.minimal-light .esg-sortbutton,.minimal-light .esg-cartbutton{display:inline-block; position:relative; cursor:pointer; margin-right:0px; border-right:none; border-radius:5px 0px 0px 5px; -moz-border-radius:5px 0px 0px 5px; -webkit-border-radius:5px 0px 0px 5px}.minimal-light .esg-navigationbutton:hover,.minimal-light .esg-filterbutton:hover,.minimal-light .esg-sortbutton:hover,.minimal-light .esg-sortbutton-order:hover,.minimal-light .esg-cartbutton a:hover,.minimal-light .esg-filterbutton.selected{background-color:#fff;   border-color:#bbb;   color:#333;   box-shadow:0px 3px 5px 0px rgba(0,0,0,0.13)}.minimal-light .esg-navigationbutton:hover *{color:#333}.minimal-light .esg-sortbutton-order.tp-desc:hover{border-color:#bbb; color:#333; box-shadow:0px -3px 5px 0px rgba(0,0,0,0.13) !important}.minimal-light .esg-filter-checked{padding:1px 3px;  color:#cbcbcb;  background:#cbcbcb;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}.minimal-light .esg-filterbutton.selected .esg-filter-checked,.minimal-light .esg-filterbutton:hover .esg-filter-checked{padding:1px 3px 1px 3px;  color:#fff;  background:#000;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}</style>
                                          <style type="text/css">.eg-washington-element-0{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; padding:17px 17px 17px 17px !important; border-radius:60px 60px 60px 60px !important; background:rgba(255,255,255,0.15) !important; z-index:2 !important; display:block; font-family:"Open Sans" !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}.eg-washington-element-1{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; padding:17px 17px 17px 17px !important; border-radius:60px 60px 60px 60px !important; background:rgba(255,255,255,0.15) !important; z-index:2 !important; display:block; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}.eg-washington-element-3{font-size:13px; line-height:20px; color:#ffffff; font-weight:700; display:inline-block; float:none; clear:both; margin:15px 0px 0px 0px ; padding:5px 10px 5px 10px ; border-radius:0px 0px 0px 0px ; background:rgba(255,255,255,0.15); position:relative; z-index:2 !important; font-family:"Open Sans"; text-transform:uppercase}</style>
                                          <style type="text/css">.eg-washington-element-0:hover{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; border-radius:60px 60px 60px 60px !important; background:rgba(0,0,0,0.50) !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}.eg-washington-element-1:hover{font-size:16px !important; line-height:22px !important; color:#ffffff !important; font-weight:400 !important; border-radius:60px 60px 60px 60px !important; background:rgba(0,0,0,0.50) !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}</style>
                                          <style type="text/css">.eg-washington-element-0-a{display:inline-block !important; float:none !important; clear:none !important; margin:0px 10px 0px 0px !important; position:relative !important}</style>
                                          <style type="text/css">.eg-washington-element-1-a{display:inline-block !important; float:none !important; clear:none !important; margin:0px 10px 0px 0px !important; position:relative !important}</style>
                                          <style type="text/css">.eg-washington-container{background:rgba(0,0,0,0.65)}</style>
                                          <style type="text/css">.eg-washington-content{background:#ffffff; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:double; text-align:left}</style>
                                          <style type="text/css">.esg-grid .mainul li.eg-washington-wrapper{background:#3f424a; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:none}</style>
                                          <style type="text/css">.esg-grid .mainul li.eg-washington-wrapper .esg-media-poster{background-size:cover; background-position:center center; background-repeat:no-repeat}</style>
                                          <!-- THE ESSENTIAL GRID 2.1.6.1 POST -->
                                       <article class="myportfolio-container minimal-light" id="esg-grid-2-2-wrap">
                                          <div id="esg-grid-2-2" class="esg-grid" style="background: transparent;padding: 0px 25px 25px 25px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;">
                                             <ul>
                                                <li id="eg-2-post-id-12112" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12112" data-date="1482484504">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12112 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12112 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a1-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A01 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12112 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12112" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a01-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12112 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A01 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-2-post-id-12114" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12114" data-date="1482484756">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="1024" height="724"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12114 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12114 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a3-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A03 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12114 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12114" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a03-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12114 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A03 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-2-post-id-12115" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12115" data-date="1482484876">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12115 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12115 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a7-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A07 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12115 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12115" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a07-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12115 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A07 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-2-post-id-12116" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12116" data-date="1482485013">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12116 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12116 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a9-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A09 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12116 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12116" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a09-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12116 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A09 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-2-post-id-12117" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12117" data-date="1482485146">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12117 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12117 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a10-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A10 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12117 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12117" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a10-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12117 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A10 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-2-post-id-12118" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12118" data-date="1482485248">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12118 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12118 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a12-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A12 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12118 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12118" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a12-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12118 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A12 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-2-post-id-12119" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12119" data-date="1482485334">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12119 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12119 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a16-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A16 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12119 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12119" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a16-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12119 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A16 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                                <li id="eg-2-post-id-12120" class="filterall filter-can-3-ngu eg-washington-wrapper eg-post-id-12120" data-date="1482485449">
                                                   <div class="esg-media-cover-wrapper">
                                                      <div class="esg-entry-media"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" alt="" width="2339" height="1654"></div>
                                                      <div class="esg-entry-cover esg-fade" data-delay="0">
                                                         <div class="esg-overlay esg-fade eg-washington-container" data-delay="0"></div>
                                                         <div class="esg-center eg-post-12120 eg-washington-element-0-a esg-falldown" data-delay="0.1"><a class="eg-washington-element-0 eg-post-12120 esgbox" href="https://www.haiphatland.vn/wp-content/uploads/2016/12/can-a18-chung-cu-thanh-xuan-complex-hapulico-24t3.jpg" lgtitle="Thiết kế căn hộ A18 Thanh Xuân Complex"><i class="eg-icon-search"></i></a></div>
                                                         <div class="esg-center eg-post-12120 eg-washington-element-1-a esg-falldown" data-delay="0.2"><a class="eg-washington-element-1 eg-post-12120" href="https://www.haiphatland.vn/essential_grid/thiet-ke-can-ho-a18-thanh-xuan-complex" target="_self"><i class="eg-icon-link"></i></a></div>
                                                         <div class="esg-center eg-washington-element-8 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                         <div class="esg-center eg-post-12120 eg-washington-element-3 esg-flipup" data-delay="0.1">Thiết kế căn hộ A18 Thanh Xuân Complex</div>
                                                         <div class="esg-center eg-washington-element-9 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                      </div>
                                                   </div>
                                                </li>
                                             </ul>
                                          </div>
                                       </article>
                                       <div class="clear"></div>
                                       <script type="text/javascript">
                                          function eggbfc(winw,resultoption) {
                                                var lasttop = winw,
                                                lastbottom = 0,
                                                smallest =9999,
                                                largest = 0,
                                                samount = 0,
                                                lamoung = 0,
                                                lastamount = 0,
                                                resultid = 0,
                                                resultidb = 0,
                                                responsiveEntries = [
                                                                              { width:1400,amount:4,mmheight:0},
                                                                              { width:1170,amount:4,mmheight:0},
                                                                              { width:1024,amount:4,mmheight:0},
                                                                              { width:960,amount:3,mmheight:0},
                                                                              { width:778,amount:3,mmheight:0},
                                                                              { width:640,amount:3,mmheight:0},
                                                                              { width:480,amount:1,mmheight:0}
                                                                              ];
                                                if (responsiveEntries!=undefined && responsiveEntries.length>0)
                                                      jQuery.each(responsiveEntries, function(index,obj) {
                                                            var curw = obj.width != undefined ? obj.width : 0,
                                                                  cura = obj.amount != undefined ? obj.amount : 0;
                                                            if (smallest>curw) {
                                                                  smallest = curw;
                                                                  samount = cura;
                                                                  resultidb = index;
                                                            }
                                                            if (largest<curw) {
                                                                  largest = curw;
                                                                  lamount = cura;
                                                            }
                                                            if (curw>lastbottom && curw<=lasttop) {
                                                                  lastbottom = curw;
                                                                  lastamount = cura;
                                                                  resultid = index;
                                                            }
                                                      });
                                                      if (smallest>winw) {
                                                            lastamount = samount;
                                                            resultid = resultidb;
                                                      }
                                                      var obj = new Object;
                                                      obj.index = resultid;
                                                      obj.column = lastamount;
                                                      if (resultoption=="id")
                                                            return obj;
                                                      else
                                                            return lastamount;
                                                }
                                          if ("even"=="even") {
                                                var coh=0,
                                                      container = jQuery("#esg-grid-2-2");
                                                var   cwidth = container.width(),
                                                      ar = "2339:1654",
                                                      gbfc = eggbfc(jQuery(window).width(),"id"),
                                                row = 3;
                                          ar = ar.split(":");
                                          aratio=parseInt(ar[0],0) / parseInt(ar[1],0);
                                          coh = cwidth / aratio;
                                          coh = coh/gbfc.column*row;
                                                var ul = container.find("ul").first();
                                                ul.css({display:"block",height:coh+"px"});
                                          }
                                          var essapi_2;
                                          jQuery(document).ready(function() {
                                                essapi_2 = jQuery("#esg-grid-2-2").tpessential({
                                                  gridID:2,
                                                  layout:"even",
                                                  forceFullWidth:"off",
                                                  lazyLoad:"off",
                                                  row:3,
                                                  loadMoreAjaxToken:"7e070fec30",
                                                  loadMoreAjaxUrl:"https://www.haiphatland.vn/wp-admin/admin-ajax.php",
                                                  loadMoreAjaxAction:"Essential_Grid_Front_request_ajax",
                                                  ajaxContentTarget:"ess-grid-ajax-container-",
                                                  ajaxScrollToOffset:"0",
                                                  ajaxCloseButton:"off",
                                                  ajaxContentSliding:"on",
                                                  ajaxScrollToOnLoad:"on",
                                                  ajaxCallbackArgument:"off",
                                                  ajaxNavButton:"off",
                                                  ajaxCloseType:"type1",
                                                  ajaxCloseInner:"false",
                                                  ajaxCloseStyle:"light",
                                                  ajaxClosePosition:"tr",
                                                  space:20,
                                                  pageAnimation:"fade",
                                                  paginationScrollToTop:"off",
                                                  spinner:"spinner0",
                                                  forceFullWidth:"on",
                                                  evenGridMasonrySkinPusher:"off",
                                                  lightBoxMode:"single",
                                                  lightboxSpinner:"off",
                                                  lightBoxFeaturedImg:"off",
                                                  lightBoxPostTitle:"off",
                                                  lightBoxPostTitleTag:"h2",
                                                  animSpeed:1000,
                                                  delayBasic:1,
                                                  mainhoverdelay:1,
                                                  filterType:"single",
                                                  showDropFilter:"hover",
                                                  filterGroupClass:"esg-fgc-2",
                                                  googleFonts:['Open+Sans:300,400,600,700,800','Raleway:100,200,300,400,500,600,700,800,900','Droid+Serif:400,700'],
                                                  aspectratio:"2339:1654",
                                                  responsiveEntries: [
                                                                              { width:1400,amount:4,mmheight:0},
                                                                              { width:1170,amount:4,mmheight:0},
                                                                              { width:1024,amount:4,mmheight:0},
                                                                              { width:960,amount:3,mmheight:0},
                                                                              { width:778,amount:3,mmheight:0},
                                                                              { width:640,amount:3,mmheight:0},
                                                                              { width:480,amount:1,mmheight:0}
                                                                              ]
                                                });
                                          
                                                try{
                                                jQuery("#esg-grid-2-2 .esgbox").esgbox({
                                                      padding : [0,0,0,0],
                                                      width:"960",
                                                      height:"540",
                                                      minWidth:"100",
                                                      minHeight:"100",
                                                      maxWidth:"9999",
                                                      maxHeight:"9999",
                                                      autoPlay:false,
                                                      playSpeed:3000,
                                                      preload:3,
                                                beforeLoad:function(t) {
                                                    return essapi_2.eslightboxpost(t);
                                                       },
                                                afterLoad:function() { 
                                                      if (this.element.hasClass("esgboxhtml5")) {
                                                            this.type ="html5";
                                                         var mp = this.element.data("mp4"),
                                                            ogv = this.element.data("ogv"),
                                                            webm = this.element.data("webm");
                                                            ratio = this.element.data("ratio");
                                                            ratio = ratio==="16:9" ? "56.25%" : "75%"
                                                   this.content ='<div class="esg-lb-video-wrapper" style="width:100%"><video autoplay="true" loop=""  poster="" width="100%" height="auto" controls><source src="'+mp+'" type="video/mp4"><source src="'+webm+'" type="video/webm"><source src="'+ogv+'" type="video/ogg"></video></div>';
                                                         };
                                                       },
                                                      beforeShow : function () { 
                                                            this.title = jQuery(this.element).attr('lgtitle');
                                                            if (this.title) {
                                                                  this.title="";
                                                      this.title =  '<div style="padding:0px 0px 0px 0px">'+this.title+'</div>';
                                                            }
                                                      },
                                                      afterShow : function() {
                                                      },
                                                      openEffect : 'fade',
                                                      closeEffect : 'fade',
                                                      nextEffect : 'fade',
                                                      prevEffect : 'fade',
                                                      openSpeed : 'normal',
                                                      closeSpeed : 'normal',
                                                      nextSpeed : 'normal',
                                                      prevSpeed : 'normal',
                                                      helpers:{overlay:{locked:false}},
                                                      helpers : {
                                                            media : {},
                                                            overlay: {
                                                                  locked: false
                                                            },
                                                          title : {
                                                                  type:""
                                                            }
                                                      }
                                          });
                                          
                                           } catch (e) {}
                                          
                                          });
                                       </script>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none txc-ly-do nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2016/12/background-container-thanh-xuan-complex-hapulico-24t3.svg");background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:20px;padding-bottom:95px;padding-left:20px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last anh-9-ly-do 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none fusion-no-small-visibility"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/ly-do-khach-hang-chon-thanh-xuan-complex-hapulico-24t3.svg" width="" height="" alt="Ly do Khach hang chon Thanh Xuan Complex Hapulico 24T3" title="" class="img-responsive"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-5 hover-type-none fusion-no-medium-visibility fusion-no-large-visibility"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/mobile-ly-do-khach-hang-thanh-xuan-complex-hapulico-24t3.svg" width="" height="" alt="Ly do Khach hang Thanh Xuan Complex Hapulico 24T3 Mobile" title="" class="img-responsive"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"><span class="icon-wrapper" style="border-color:#d2bd56;background-color:rgba(255,255,255,0);"><i class=" fa fa-wifi" style="color:#d2bd56;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://foresa-villas.com/wp-content/uploads/2014/11/HA24-News-New-BG.jpg");background-position: left center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Tin tức &amp; Sự kiện</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                       <p style="text-align: center; font-family: UTM-Avo; font-weight: 600;">Chúng tôi luôn mang đến những thông tin và chính sách mới nhất về dự án <strong><a href="https://haiphatland.vn">Thanh Xuân Complex</a></strong>.</p>
                                    </div>
                                    <div class="fusion-recent-posts fusion-recent-posts-1 avada-container layout-default layout-columns-4">
                                       <section class="fusion-columns columns fusion-columns-4 columns-4">
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/tin-tuc/nhung-loi-cua-du-thanh-xuan-complex.html" class="hover-type-zoomin" aria-label="Những lợi thế của dự án Thanh Xuân Complex"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/nhung-loi-the-du-an-thanh-xuan-complex-01-500x441.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-25T10:45:04+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/tin-tuc/nhung-loi-cua-du-thanh-xuan-complex.html">Những lợi thế của dự án Thanh Xuân Complex</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-25T10:45:04+00:00</span><span>25, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/tin-tuc/nhung-loi-cua-du-thanh-xuan-complex.html#respond">0 Comments</a></span></p>
                                                <p>Chủ đầu tư uy tín, vị trí đắc địa, tiện ích đồng bộ… là những yếu tố giúp [...]</p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html" class="hover-type-zoomin" aria-label="Tổ hợp tiện ích khiến Thanh Xuân Complex được xem là dự án đáng mua nhất"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/thanh-xuan-complex-hapulico-24t3-slider-01-700x441.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-11T17:33:34+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">Tổ hợp tiện ích khiến Thanh Xuân Complex được xem là dự án đáng mua nhất</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-11T17:33:34+00:00</span><span>10, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html#respond">0 Comments</a></span></p>
                                                <p>Tiếp nối sự thành công của Hapulico Complex, Thanh Xuân Complex vừa ra mắt đã tạo nên một [...]</p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/thanh-xuan-complex/vi-sao-nen-lua-chon-chung-cu-thanh-xuan-complex-hapulico-24t3.html" class="hover-type-zoomin" aria-label="Vì sao nên lựa chọn chung cư Thanh Xuân Complex – Hapulico 24T3?"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/dau-tu-sinh-loi-voi-thanh-xuan-complex-hapulico-24t3-700x441.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-11T17:34:17+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/thanh-xuan-complex/vi-sao-nen-lua-chon-chung-cu-thanh-xuan-complex-hapulico-24t3.html">Vì sao nên lựa chọn chung cư Thanh Xuân Complex – Hapulico 24T3?</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-11T17:34:17+00:00</span><span>9, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/thanh-xuan-complex/vi-sao-nen-lua-chon-chung-cu-thanh-xuan-complex-hapulico-24t3.html#respond">0 Comments</a></span></p>
                                                <p>Chung cư Thanh Xuân Complex – Hapulico 24T3 nằm gần quần thể dự án Hapulico. Đây là tâm [...]</p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-3 col-md-3 col-sm-3">
                                             <div class="fusion-flexslider flexslider flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/tin-tuc/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat.html" class="hover-type-zoomin" aria-label="Chủ đầu tư Thanh Xuân Complex nộp 500 tỉ tiền sử dụng đất"><img src="https://www.haiphatland.vn/wp-content/uploads/2016/12/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat-1-700x441.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-11T17:35:05+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/tin-tuc/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat.html">Chủ đầu tư Thanh Xuân Complex nộp 500 tỉ tiền sử dụng đất</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-11T17:35:05+00:00</span><span>6, Tháng Mười Hai, 2016</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/tin-tuc/chu-dau-tu-thanh-xuan-complex-nop-500-ti-tien-su-dung-dat.html#respond">0 Comments</a></span></p>
                                                <p>Ngày 3/8, ông Phạm Đình Việt, Tổng Giám đốc Công ty cổ phần phát triển Thanh Xuân, chủ [...]</p>
                                             </div>
                                          </article>
                                       </section>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="lien-he-txc">
                           <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                              <div class="fusion-builder-row fusion-row ">
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Liên hệ với Chúng tôi</span></h2>
                                          <p style="text-align: center;">
                                          <div class="fusion-sep-clear"></div>
                                          <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                          </p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                          <h3 class="title-heading-left">VIDEO DỰ ÁN THANH XUÂN COMPLEX</p></h3>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-video fusion-youtube" style="max-width:768px;max-height:448px;">
                                          <div class="video-shortcode">
                                             <div class="rll-youtube-player" data-id="F9cVMpHkiDM"></div>
                                             <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/F9cVMpHkiDM?wmode=transparent&autoplay=0" width="768" height="448" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                          </div>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                          <h3 class="title-heading-left">ĐĂNG KÝ NHẬN BẢNG GIÁ</h3>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-text">
                                          <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của Dự án Thanh Xuân Complex tới Quý khách!</p>
                                       </div>
                                       <div class="fusion-text">
                                          <p style="text-align: center;">
                                          <div role="form" class="wpcf7" id="wpcf7-f11814-p12012-o1" lang="vi" dir="ltr">
                                             <div class="screen-reader-response"></div>
                                             <form action="/chung-cu-cao-cap-thanh-xuan-complex#wpcf7-f11814-p12012-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                <div style="display: none;">
                                                   <input type="hidden" name="_wpcf7" value="11814" />
                                                   <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                   <input type="hidden" name="_wpcf7_locale" value="vi" />
                                                   <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f11814-p12012-o1" />
                                                   <input type="hidden" name="_wpcf7_container_post" value="12012" />
                                                </div>
                                                <p><span class="wpcf7-form-control-wrap text-833"><input type="text" name="text-833" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span></p>
                                                <p><span class="wpcf7-form-control-wrap email-20"><input type="email" name="email-20" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email*" /></span></p>
                                                <p><span class="wpcf7-form-control-wrap tel-146"><input type="tel" name="tel-146" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span></p>
                                                <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                   <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                   <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                                </div>
                                             </form>
                                          </div>
                                          </p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                    <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <a href="tel:0986 205 333" class="hotline-sticky">Hotline<br><strong id="txtHotline">0986 205 333</strong></a>
                                       <style>
                                          .hotline-sticky {
                                          background: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/background-hotline-thanh-xuan-complex-hapulico-24t3.png) no-repeat scroll 0 0;
                                          top: 100px;
                                          height: 70px;
                                          position: fixed;
                                          right: 0;
                                          width: 189px;
                                          text-transform: uppercase;
                                          color: #fff;
                                          font-weight: 500;
                                          z-index: 2000;
                                          padding: 5px 0 0 10px;
                                          }
                                          .hotline-sticky strong {
                                          font-size: 18px;
                                          margin-left: 10px;
                                          }
                                       </style>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
         <!-- #main -->
         <div class="fusion-footer">
            <footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
               <div class="fusion-row">
                  <div class="fusion-columns fusion-columns-4 fusion-widget-area">
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-2" class="fusion-footer-widget-column widget widget_text">
                           <div class="textwidget"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/footer-logo-hai-phat-land.png" alt="Logo Footer Hai Phat Land" /><br><br/>
                              <span style="font-family: UTM-Wedding; font-size: 25px; color: #d2bd56;">Chúng tôi luôn đặt trí lực, tâm lực vào mỗi sản phẩm bởi chất lượng là cốt lõi, là sự hài lòng của khách hàng và  là thành công của chúng tôi.</span>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-4" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Thông tin liên hệ</h4>
                           <div class="textwidget">
                              <p><i class="fontawesome-icon  fa fa-map-marker circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Địa chỉ: Tầng 1&2 CT4, Tổ hợp TMDV và Căn hộ The Pride, KĐT An Hưng, P. La Khê, Q. Hà Đông, TP. Hà Nội.<br><br/>
                                 <i class="fontawesome-icon  fa fa-phone circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i>Số điện thoại: 0422 001 999<br><br/>
                                 <i class="fontawesome-icon  fa fa-envelope circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Email: info@haiphatland.vn
                              </p>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="recent-posts-6" class="fusion-footer-widget-column widget widget_recent_entries">
                           <h4 class="widget-title">Tin tức mới nhất</h4>
                           <ul>
                              <li>
                                 <a href="https://www.haiphatland.vn/tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html">Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!</a>
                              </li>
                              <li>
                                 <a href="https://www.haiphatland.vn/tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html">Mẹo tự chọn căn hộ chung cư hợp phong thủy</a>
                              </li>
                              <li>
                                 <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/nhan-ngay-chuyen-du-lich-khi-mua-chung-cu-goldseason-47-nguyen-tuan.html">Nhận ngay chuyến du lịch Mỹ khi mua chung cư Goldseason 47 Nguyễn Tuân</a>
                              </li>
                           </ul>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
                        <section id="text-5" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Lịch làm việc</h4>
                           <div class="textwidget">
                              <p>Chúng tôi luôn hỗ trợ Quý khách 24/24<br>
                                 <i class="fontawesome-icon  fa fa-phone-square circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Hotline: 0986 205 333<br>
                                 <i class="fontawesome-icon  fa fa-fax circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> CSKH: 0938 072 999
                              </p>
                              <h5 style="margin-top: 1em; margin-bottom: 0.5em;"><span style="font-weight: 400; color: #ddd;">GIỜ MỞ CỬA</span></h5>
                              <ul>
                                 <li><strong><span style="color: #d2bd56;">Thứ 2-Thứ 6:</span>  </strong>8h00' đến 17h30'</li>
                                 <li><strong><span style="color: #d2bd56;">Thứ 7:</span>  </strong>8h00' đến 17h30'</li>
                                 <li><strong><span style="color: #d2bd56;">Chủ nhật:</span>     </strong>8h00' đến 17h30'</li>
                              </ul>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-clearfix"></div>
                  </div>
                  <!-- fusion-columns -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- fusion-footer-widget-area -->
            <footer id="footer" class="fusion-footer-copyright-area">
               <div class="fusion-row">
                  <div class="fusion-copyright-content">
                     <div class="fusion-copyright-notice">
                        <div>
                           © Copyright <script>document.write(new Date().getFullYear());</script>   |   Website by <a href=“https://haiphatland.vn” target="_blank">Hải Phát Land</a>   |   All Rights Reserved   |   Powered by <a href='https://haiphatland.vn' t>Hải Phát</a>    
                        </div>
                     </div>
                  </div>
                  <!-- fusion-fusion-copyright-content -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- #footer -->
         </div>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>                       <script type="text/javascript">
         jQuery( document ).ready( function() {
            var ajaxurl = 'https://www.haiphatland.vn/wp-admin/admin-ajax.php';
            if ( 0 < jQuery( '.fusion-login-nonce' ).length ) {
                  jQuery.get( ajaxurl, { 'action': 'fusion_login_nonce' }, function( response ) {
                        jQuery( '.fusion-login-nonce' ).html( response );
                  });
            }
         });
      </script>
      <link rel='stylesheet' id='drawattention-plugin-styles-css'  href='https://www.haiphatland.vn/wp-content/plugins/draw-attention/public/assets/css/public.css?ver=1.7.3' type='text/css' media='all' />
      <style id='drawattention-plugin-styles-inline-css' type='text/css'>
         #hotspot-12546 .hotspots-image-container {
         background: #efefef;
         }
         #hotspot-12546 .hotspots-placeholder,
         .featherlight .featherlight-content.lightbox12546 {
         background: #143c57;
         color: #ffffff;
         }
         #hotspot-12546 .hotspot-title,
         .featherlight .featherlight-content.lightbox12546 .hotspot-title {
         color: #d2bd56;
         }
      </style>
      <link rel='stylesheet' id='themepunchboxextcss-css'  href='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/css/lightbox.css?ver=2.1.6.1' type='text/css' media='all' />
      <script type='text/javascript'>
         /* <![CDATA[ */
         var wpcf7 = {"apiSettings":{"root":"https:\/\/www.haiphatland.vn\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var spuvar = {"is_admin":"","disable_style":"","ajax_mode":"1","ajax_url":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php","ajax_mode_url":"https:\/\/www.haiphatland.vn\/?spu_action=spu_load&lang=","pid":"12012","is_front_page":"","is_category":"","site_url":"https:\/\/www.haiphatland.vn","is_archive":"","is_search":"","is_preview":"","seconds_confirmation_close":"5"};
         var spuvar_social = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [{"element":"#text-slider","settings":{"intervalTime":13000,"duration":0,"definedHeight":""}}];
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var toTopscreenReaderText = {"label":"Go to Top"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaToTopVars = {"status_totop_mobile":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaRevVars = {"avada_rev_styles":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaFusionSliderVars = {"side_header_break_point":"1100","slider_position":"below","header_transparency":"0","header_position":"Top","content_break_point":"800","status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/draw-attention/public/assets/js/jquery.responsilight.js?ver=1.7.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/draw-attention/public/assets/js/featherlight.min.js?ver=1.7.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/draw-attention/public/assets/js/public.js?ver=1.7.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/tablepress/js/jquery.datatables.min.js?ver=1.9.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.essential.min.js?ver=2.1.6.1'></script>
      <script type="text/javascript">
         jQuery(document).ready(function($){
         var DataTables_language={};
         DataTables_language["vi"]={"info":"Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục","infoEmpty":"Đang xem 0 đến 0 trong tổng số 0 mục","infoFiltered":"(được lọc từ _MAX_ mục)","infoPostFix":"","lengthMenu":"Xem _MENU_ mục","processing":"Đang xử lý...","search":"Tìm:","zeroRecords":"Không tìm thấy dòng nào phù hợp","paginate": {"first":"Đầu","previous":"Trước","next":"Tiếp","last":"Cuối"},"decimal":",","thousands":"."};
         $('#tablepress-1').dataTable({"language":DataTables_language["vi"],"order":[],"orderClasses":false,"stripeClasses":["even","odd"],"paging":false,"searching":false,"info":false});
         });
      </script><!--Start of Tawk.to Script-->
      <script type="text/javascript">
         var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
         (function(){
         var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
         s1.async=true;
         s1.src='https://embed.tawk.to/58d2940b5b89e2149e1dc573/default';
         s1.charset='UTF-8';
         s1.setAttribute('crossorigin','*');
         s0.parentNode.insertBefore(s1,s0);
         })();
      </script>
      <!--End of Tawk.to Script--><script>(function(w, d){
         var b = d.getElementsByTagName("body")[0];
         var s = d.createElement("script"); s.async = true;
         var v = !("IntersectionObserver" in w) ? "8.5.2" : "10.3.5";
         s.src = "https://www.haiphatland.vn/wp-content/plugins/wp-rocket/inc/front/js/lazyload-" + v + ".min.js";
         w.lazyLoadOptions = {
            elements_selector: "img, iframe",
            data_src: "lazy-src",
            data_srcset: "lazy-srcset",
            skip_invisible: false,
            class_loading: "lazyloading",
            class_loaded: "lazyloaded",
            threshold: 300,
            callback_load: function(element) {
                  if ( element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible" ) {
                        if (element.classList.contains("lazyloaded") ) {
                              if (typeof window.jQuery != "undefined") {
                                    if (jQuery.fn.fitVids) {
                                          jQuery(element).parent().fitVids();
                                    }
                              }
                        }
                  }
            }
         }; // Your options here. See "recipes" for more information about async.
         b.appendChild(s);
         }(window, document));
         
         // Listen to the Initialized event
         window.addEventListener('LazyLoad::Initialized', function (e) {
            // Get the instance and puts it in the lazyLoadInstance variable
         var lazyLoadInstance = e.detail.instance;
         
         var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                  lazyLoadInstance.update();
            } );
         } );
         
         var b      = document.getElementsByTagName("body")[0];
         var config = { childList: true, subtree: true };
         
         observer.observe(b, config);
         }, false);
      </script>         <script>function lazyLoadThumb(e){var t='<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',a='<div class="play"></div>';return t.replace("ID",e)+a}function lazyLoadYoutubeIframe(){var e=document.createElement("iframe"),t="https://www.youtube.com/embed/ID?autoplay=1";e.setAttribute("src",t.replace("ID",this.dataset.id)),e.setAttribute("frameborder","0"),e.setAttribute("allowfullscreen","1"),this.parentNode.replaceChild(e,this)}document.addEventListener("DOMContentLoaded",function(){var e,t,a=document.getElementsByClassName("rll-youtube-player");for(t=0;t<a.length;t++)e=document.createElement("div"),e.setAttribute("data-id",a[t].dataset.id),e.innerHTML=lazyLoadThumb(a[t].dataset.id),e.onclick=lazyLoadYoutubeIframe,a[t].appendChild(e)});</script>      
   </body>
</html>
<!-- This website is like a Rocket, isn't it? Performance optimized by WP Rocket. Learn more: https://wp-rocket.me - Debug: cached@1536635388 -->