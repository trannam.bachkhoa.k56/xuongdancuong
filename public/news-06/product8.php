<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Dự án chung cư cao cấp Sky Central - 176 Định Công - Hoàng Mai</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Chung cư 176 Định Công - Sky Central. Dự án tổ hợp cao tầng 176 Định Công, Hoàng Mai, Hà Nội. Website bán hàng chính thức của Chủ đầu tư."/>
      <link rel="canonical" href="https://www.haiphatland.vn/chung-cu-cao-cap-sky-central" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="Dự án chung cư cao cấp Sky Central - 176 Định Công - Hoàng Mai" />
      <meta property="og:description" content="Chung cư 176 Định Công - Sky Central. Dự án tổ hợp cao tầng 176 Định Công, Hoàng Mai, Hà Nội. Website bán hàng chính thức của Chủ đầu tư." />
      <meta property="og:url" content="https://www.haiphatland.vn/chung-cu-cao-cap-sky-central" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="article:publisher" content="https://www.facebook.com/Dreamcenterhome282NHT/" />
      <meta property="og:image" content="https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong.jpg" />
      <meta property="og:image:secure_url" content="https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong.jpg" />
      <meta property="og:image:width" content="1271" />
      <meta property="og:image:height" content="871" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Chung cư 176 Định Công - Sky Central. Dự án tổ hợp cao tầng 176 Định Công, Hoàng Mai, Hà Nội. Website bán hàng chính thức của Chủ đầu tư." />
      <meta name="twitter:title" content="Dự án chung cư cao cấp Sky Central - 176 Định Công - Hoàng Mai" />
      <meta name="twitter:image" content="https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong.jpg" />
      <script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Person","url":"https:\/\/www.haiphatland.vn\/","sameAs":["https:\/\/www.facebook.com\/Dreamcenterhome282NHT\/"],"@id":"#person","name":"Tinh Bui"}</script>
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="https://www.haiphatland.vn/wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Chung cư cao cấp Sky Central"/>
      <meta property="og:type" content="article"/>
      <meta property="og:url" content="https://www.haiphatland.vn/chung-cu-cao-cap-sky-central"/>
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát"/>
      <meta property="og:description" content="Chung cư 176 Định Công sở hữu những tiện ích 5 sao thời thượng cho cuộc sống đẳng cấp của giới thượng lưu. Sống ở vị trí trung tâm thành phố Hà Nội, hòa nhịp cùng sự phát triển của Khu đô thị, cư"/>
      <meta property="og:image" content="https://www.haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-chung-cu-sky-central-176-dinh-cong.jpg"/>
      <link rel='stylesheet' id='contact-form-7-css'  href='https://www.haiphatland.vn/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='https://www.haiphatland.vn/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='https://www.haiphatland.vn/wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='https://www.haiphatland.vn/wp-content/tablepress-combined.min.css?ver=19' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='https://www.haiphatland.vn/wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(https://www.haiphatland.vn/wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2aWV3Qm94PSIwIDAgNjAgNjAiPjxwYXRoIGQ9Ik03LjEwNCAxNC4wMzJsMTUuNTg2IDEuOTg0YzAgMC0wLjAxOSAwLjUgMCAwLjk1M2MwLjAyOSAwLjc1Ni0wLjI2IDEuNTM0LTAuODA5IDIuMSBsLTQuNzQgNC43NDJjMi4zNjEgMy4zIDE2LjUgMTcuNCAxOS44IDE5LjhsMTYuODEzIDEuMTQxYzAgMCAwIDAuNCAwIDEuMSBjLTAuMDAyIDAuNDc5LTAuMTc2IDAuOTUzLTAuNTQ5IDEuMzI3bC02LjUwNCA2LjUwNWMwIDAtMTEuMjYxIDAuOTg4LTI1LjkyNS0xMy42NzRDNi4xMTcgMjUuMyA3LjEgMTQgNy4xIDE0IiBmaWxsPSIjMDA2NzAwIi8+PHBhdGggZD0iTTcuMTA0IDEzLjAzMmw2LjUwNC02LjUwNWMwLjg5Ni0wLjg5NSAyLjMzNC0wLjY3OCAzLjEgMC4zNWw1LjU2MyA3LjggYzAuNzM4IDEgMC41IDIuNTMxLTAuMzYgMy40MjZsLTQuNzQgNC43NDJjMi4zNjEgMy4zIDUuMyA2LjkgOS4xIDEwLjY5OWMzLjg0MiAzLjggNy40IDYuNyAxMC43IDkuMSBsNC43NC00Ljc0MmMwLjg5Ny0wLjg5NSAyLjQ3MS0xLjAyNiAzLjQ5OC0wLjI4OWw3LjY0NiA1LjQ1NWMxLjAyNSAwLjcgMS4zIDIuMiAwLjQgMy4xMDVsLTYuNTA0IDYuNSBjMCAwLTExLjI2MiAwLjk4OC0yNS45MjUtMTMuNjc0QzYuMTE3IDI0LjMgNy4xIDEzIDcuMSAxMyIgZmlsbD0iI2ZmZiIvPjwvc3ZnPg==) center/50px 50px no-repeat #009900;}}</style>
      <script type="text/javascript">
         var ajaxRevslider;
         
         jQuery(document).ready(function() {
            // CUSTOM AJAX CONTENT LOADING FUNCTION
            ajaxRevslider = function(obj) {
            
               // obj.type : Post Type
               // obj.id : ID of Content to Load
               // obj.aspectratio : The Aspect Ratio of the Container / Media
               // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
               
               var content = "";
         
               data = {};
               
               data.action = 'revslider_ajax_call_front';
               data.client_action = 'get_slider_html';
               data.token = '5796bf54e3';
               data.type = obj.type;
               data.id = obj.id;
               data.aspectratio = obj.aspectratio;
               
               // SYNC AJAX REQUEST
               jQuery.ajax({
                  type:"post",
                  url:"https://www.haiphatland.vn/wp-admin/admin-ajax.php",
                  dataType: 'json',
                  data:data,
                  async:false,
                  success: function(ret, textStatus, XMLHttpRequest) {
                     if(ret.success == true)
                        content = ret.data;                       
                  },
                  error: function(e) {
                     console.log(e);
                  }
               });
               
                // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
                return content;                  
            };
            
            // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
            var ajaxRemoveRevslider = function(obj) {
               return jQuery(obj.selector+" .rev_slider").revkill();
            };
         
            // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
            var extendessential = setInterval(function() {
               if (jQuery.fn.tpessential != undefined) {
                  clearInterval(extendessential);
                  if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
                     jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
                     // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
                     // func: the Function Name which is Called once the Item with the Post Type has been clicked
                     // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
                     // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
                  }
               }
            },30);
         });
      </script>
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
      <script type="text/javascript">function setREVStartSize(e){                         
         try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
            if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})               
         }catch(d){console.log("Failure at Presize of Slider:"+d)}                  
         };
      </script>
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
      <script type="text/javascript">
         jQuery(document).ready(function() {
         /*<![CDATA[*/
         jQuery('.related-posts > div > h3').html('Bài viết liên quan');
         /*]]>*/
         });
      </script><!-- Facebook Pixel Code -->
      <script>
         !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         document,'script','https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '533271836850356'); // Insert your pixel ID here.
         fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="page-template page-template-100-width page-template-100-width-php page page-id-12020 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         <header class="fusion-header-wrapper fusion-header-shadow">
            <div class="fusion-header-v1 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-1  fusion-mobile-menu-design-modern">
               <div class="fusion-header-sticky-height"></div>
               <div class="fusion-header">
                  <div class="fusion-row">
                     <div class="fusion-logo" data-margin-top="8px" data-margin-bottom="8px" data-margin-left="0px" data-margin-right="0px">
                        <a class="fusion-logo-link"  href="https://www.haiphatland.vn/" >
                           <!-- standard logo -->
                           <img src="https://www.haiphatland.vn/wp-content/uploads/2016/11/logo-hai-phat-land.png" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/11/logo-hai-phat-land.png 1x" width="70" height="70" alt="Công ty CP BĐS Hải Phát Logo" retina_logo_url="" class="fusion-standard-logo" />
                           <!-- mobile logo -->
                           <img src="https://www.haiphatland.vn/wp-content/uploads/2016/11/mobile-logo-hai-phat-land.png" srcset="https://www.haiphatland.vn/wp-content/uploads/2016/11/mobile-logo-hai-phat-land.png 1x" width="24" height="24" alt="Công ty CP BĐS Hải Phát Logo" retina_logo_url="" class="fusion-mobile-logo" />
                        </a>
                     </div>
                     <nav class="fusion-main-menu" aria-label="Main Menu">
                        <ul role="menubar" id="menu-main" class="fusion-menu">
                           <li role="menuitem"  id="menu-item-13257"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-13257 fusion-menu-item-button"  ><a  href="https://www.haiphatland.vn/" class="fusion-bar-highlight"><span class="menu-text fusion-button button-default button-medium"><span class="button-icon-divider-left"><i class="glyphicon  fa fa-home"></i></span><span class="fusion-button-text-left">TRANG CHỦ</span></span></a></li>
                           <li role="menuitem"  id="menu-item-12595"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-12595 fusion-dropdown-menu"  >
                              <a  href="https://www.haiphatland.vn/gioi-thieu" class="fusion-bar-highlight"><span class="menu-text">GIỚI THIỆU</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-11971"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11971 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Phương châm hoạt động</span></a></li>
                                 <li role="menuitem"  id="menu-item-11972"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11972 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Tầm nhìn &#038; Sứ mệnh</span></a></li>
                                 <li role="menuitem"  id="menu-item-11973"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-11973 fusion-dropdown-submenu"  ><a  href="#" class="fusion-bar-highlight"><span>Giá trị cốt lõi</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-11974"  class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-11974 fusion-dropdown-menu"  >
                              <a  href="#" class="fusion-bar-highlight"><span class="menu-text">DỰ ÁN</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-14114"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14114 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/du-an-khu-do-thi-thuan-thanh-3-bac-ninh" class="fusion-bar-highlight"><span>Dự án khu đô thị Thuận Thành 3 Bắc Ninh</span></a></li>
                                 <li role="menuitem"  id="menu-item-14099"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14099 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-cao-cap-booyoung" class="fusion-bar-highlight"><span>Chung cư cao cấp Booyoung</span></a></li>
                                 <li role="menuitem"  id="menu-item-13311"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13311 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/du-roman-plaza" class="fusion-bar-highlight"><span>Dự án Roman Plaza</span></a></li>
                                 <li role="menuitem"  id="menu-item-13805"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13805 fusion-dropdown-submenu"  >
                                    <a  href="https://www.haiphatland.vn/du-goldseason-47-nguyen-tuan" class="fusion-bar-highlight"><span>Dự án Goldseason</span></a>
                                    <ul role="menu" class="sub-menu">
                                       <li role="menuitem"  id="menu-item-13928"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-13928"  ><a  href="https://www.haiphatland.vn/chuyen-muc/du-an-chung-cu-goldseason" class="fusion-bar-highlight"><span>Dự án chung cư goldseason</span></a></li>
                                    </ul>
                                 </li>
                                 <li role="menuitem"  id="menu-item-12071"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12071 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-cao-cap-thanh-xuan-complex" class="fusion-bar-highlight"><span>Dự án Thanh Xuân Complex</span></a></li>
                                 <li role="menuitem"  id="menu-item-12918"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12918 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/nha-o-xa-hoi-the-vesta" class="fusion-bar-highlight"><span>Nhà ở xã hội The Vesta</span></a></li>
                                 <li role="menuitem"  id="menu-item-12819"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12819 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-dream-center-home" class="fusion-bar-highlight"><span>Dự án Dream Center Home</span></a></li>
                                 <li role="menuitem"  id="menu-item-12038"  class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-12020 current_page_item menu-item-12038 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chung-cu-cao-cap-sky-central" class="fusion-bar-highlight"><span>Dự án Sky Central</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12039"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-12039 fusion-dropdown-menu"  >
                              <a  href="https://www.haiphatland.vn/chuyen-muc/tin-tuc" class="fusion-bar-highlight"><span class="menu-text">TIN TỨC &#038; SỰ KIỆN</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-12040"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12040 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/tin-hai-phat" class="fusion-bar-highlight"><span>Tin tức Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12041"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12041 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/tin-bds" class="fusion-bar-highlight"><span>Tin tức Bất động sản</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12815"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children menu-item-12815 fusion-dropdown-menu"  >
                              <a  href="https://www.haiphatland.vn/chuyen-muc/goc-hai-phat" class="fusion-bar-highlight"><span class="menu-text">GÓC HẢI PHÁT</span></a>
                              <ul role="menu" class="sub-menu">
                                 <li role="menuitem"  id="menu-item-12816"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12816 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/dien-dan" class="fusion-bar-highlight"><span>Diễn đàn Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12817"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12817 fusion-dropdown-submenu"  ><a  href="https://www.haiphatland.vn/chuyen-muc/van-hoa" class="fusion-bar-highlight"><span>Văn hóa Hải Phát</span></a></li>
                                 <li role="menuitem"  id="menu-item-12630"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12630 fusion-dropdown-submenu"  ><a  href="http://#" class="fusion-bar-highlight"><span>Thư viện Ảnh</span></a></li>
                              </ul>
                           </li>
                           <li role="menuitem"  id="menu-item-12627"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-12627"  ><a  href="https://www.haiphatland.vn/chuyen-muc/tuyen-dung" class="fusion-bar-highlight"><span class="menu-text">TUYỂN DỤNG</span></a></li>
                           <li role="menuitem"  id="menu-item-12574"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12574"  ><a  href="https://www.haiphatland.vn/lien-he" class="fusion-bar-highlight"><span class="menu-text">LIÊN HỆ</span></a></li>
                           <li class="fusion-custom-menu-item fusion-main-menu-search">
                              <a class="fusion-main-menu-icon fusion-bar-highlight" href="#" aria-label="Search" data-title="Search" title="Search"></a>
                              <div class="fusion-custom-menu-item-contents">
                                 <form role="search" class="searchform fusion-search-form" method="get" action="https://www.haiphatland.vn/">
                                    <div class="fusion-search-form-content">
                                       <div class="fusion-search-field search-field">
                                          <label class="screen-reader-text" for="s">Search for:</label>
                                          <input type="text" value="" name="s" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ..."/>
                                       </div>
                                       <div class="fusion-search-button search-button">
                                          <input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
                                       </div>
                                    </div>
                                 </form>
                              </div>
                           </li>
                        </ul>
                     </nav>
                     <div class="fusion-mobile-menu-icons">
                        <a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu" aria-expanded="false"></a>
                     </div>
                     <nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left"></nav>
                  </div>
               </div>
            </div>
            <div class="fusion-clearfix"></div>
         </header>
         <div id="sliders-container">
            <div id="rev_slider_9_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
               <!-- START REVOLUTION SLIDER 5.4.8 fullwidth mode -->
               <div id="rev_slider_9_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8">
                  <ul>
                     <!-- SLIDE  -->
                     <li data-index="rs-27" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="https://www.haiphatland.vn/wp-content/uploads/2017/02/sky-central-dinh-cong-slider-01-100x50.jpg"  data-rotate="0"  data-saveperformance="on"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="https://www.haiphatland.vn/wp-content/plugins/revslider/admin/assets/images/dummy.png"  alt="" title="sky-central-dinh-cong-slider-01"  width="1640" height="520" data-lazyload="https://www.haiphatland.vn/wp-content/uploads/2017/02/sky-central-dinh-cong-slider-01.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                     </li>
                     <!-- SLIDE  -->
                     <li data-index="rs-28" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="https://www.haiphatland.vn/wp-content/uploads/revslider/176-Dinh-Cong/slide-02-chung-cu-176-dinh-cong-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="https://www.haiphatland.vn/wp-content/plugins/revslider/admin/assets/images/dummy.png"  alt="" title="slide-02-chung-cu-176-dinh-cong.jpg"  width="1920" height="609" data-lazyload="https://www.haiphatland.vn/wp-content/uploads/revslider/176-Dinh-Cong/slide-02-chung-cu-176-dinh-cong.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                     </li>
                     <!-- SLIDE  -->
                     <li data-index="rs-29" data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="https://www.haiphatland.vn/wp-content/uploads/revslider/176-Dinh-Cong/slide-03-chung-cu-176-dinh-cong-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="https://www.haiphatland.vn/wp-content/plugins/revslider/admin/assets/images/dummy.png"  alt="" title="slide-03-chung-cu-176-dinh-cong.jpg"  width="1920" height="610" data-lazyload="https://www.haiphatland.vn/wp-content/uploads/revslider/176-Dinh-Cong/slide-03-chung-cu-176-dinh-cong.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                     </li>
                  </ul>
                  <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                     if(htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                     }else{
                        var htmlDiv = document.createElement("div");
                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                     }
                  </script>
                  <div class="tp-bannertimer" style="height: 5px; background: rgba(0,0,0,0.15);"></div>
               </div>
               <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                  if(htmlDiv) {
                     htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                  }else{
                     var htmlDiv = document.createElement("div");
                     htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                     document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                  }
               </script>
               <script type="text/javascript">
                  if (setREVStartSize!==undefined) setREVStartSize(
                     {c: '#rev_slider_9_1', gridwidth: [1640], gridheight: [520], sliderLayout: 'fullwidth'});
                           
                  var revapi9,
                     tpj;  
                  (function() {        
                     if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();  
                     function onLoad() {           
                        if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
                     if(tpj("#rev_slider_9_1").revolution == undefined){
                        revslider_showDoubleJqueryError("#rev_slider_9_1");
                     }else{
                        revapi9 = tpj("#rev_slider_9_1").show().revolution({
                           sliderType:"standard",
                           jsFileLocation:"//www.haiphatland.vn/wp-content/plugins/revslider/public/assets/js/",
                           sliderLayout:"fullwidth",
                           dottedOverlay:"none",
                           delay:4500,
                           navigation: {
                              keyboardNavigation:"off",
                              keyboard_direction: "horizontal",
                              mouseScrollNavigation:"off",
                                       mouseScrollReverse:"default",
                              onHoverStop:"off",
                              touch:{
                                 touchenabled:"on",
                                 touchOnDesktop:"off",
                                 swipe_threshold: 75,
                                 swipe_min_touches: 50,
                                 swipe_direction: "horizontal",
                                 drag_block_vertical: false
                              }
                              ,
                              arrows: {
                                 style:"hesperiden",
                                 enable:true,
                                 hide_onmobile:true,
                                 hide_under:600,
                                 hide_onleave:true,
                                 hide_delay:200,
                                 hide_delay_mobile:1200,
                                 tmp:'',
                                 left: {
                                    h_align:"left",
                                    v_align:"center",
                                    h_offset:30,
                                    v_offset:0
                                 },
                                 right: {
                                    h_align:"right",
                                    v_align:"center",
                                    h_offset:30,
                                    v_offset:0
                                 }
                              }
                              ,
                              bullets: {
                                 enable:true,
                                 hide_onmobile:true,
                                 hide_under:600,
                                 style:"ares",
                                 hide_onleave:true,
                                 hide_delay:200,
                                 hide_delay_mobile:1200,
                                 direction:"horizontal",
                                 h_align:"center",
                                 v_align:"bottom",
                                 h_offset:0,
                                 v_offset:30,
                                 space:5,
                                 tmp:'<span class="tp-bullet-title">{{title}}</span>'
                              }
                           },
                           visibilityLevels:[1240,1024,778,480],
                           gridwidth:1640,
                           gridheight:520,
                           lazyType:"smart",
                           parallax: {
                              type:"mouse",
                              origo:"slidercenter",
                              speed:2000,
                              speedbg:0,
                              speedls:0,
                              levels:[2,3,4,5,6,7,12,16,10,50,47,48,49,50,51,55],
                           },
                           shadow:0,
                           spinner:"off",
                           stopLoop:"off",
                           stopAfterLoops:-1,
                           stopAtSlide:-1,
                           shuffle:"off",
                           autoHeight:"on",
                           hideThumbsOnMobile:"off",
                           hideSliderAtLimit:0,
                           hideCaptionAtLimit:961,
                           hideAllCaptionAtLilmit:0,
                           debugMode:false,
                           fallbacks: {
                              simplifyAll:"off",
                              nextSlideOnWindowFocus:"off",
                              disableFocusListener:false,
                           }
                        });
                     }; /* END OF revapi call */
                     
                   }; /* END OF ON LOAD FUNCTION */
                  }()); /* END OF WRAPPING FUNCTION */
               </script>
               <script>
                  var htmlDivCss = unescape(".hesperiden.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A40px%3B%0A%09height%3A40px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%20%20%20%20border-radius%3A%2050%25%3B%0A%7D%0A.hesperiden.tparrows%3Ahover%20%7B%0A%09background%3Argba%280%2C%200%2C%200%2C%201%29%3B%0A%7D%0A.hesperiden.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A20px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%2040px%3B%0A%09text-align%3A%20center%3B%0A%7D%0A.hesperiden.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce82c%22%3B%0A%20%20%20%20margin-left%3A-3px%3B%0A%7D%0A.hesperiden.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce82d%22%3B%0A%20%20%20%20margin-right%3A-3px%3B%0A%7D%0A.ares.tp-bullets%20%7B%0A%7D%0A.ares.tp-bullets%3Abefore%20%7B%0A%09content%3A%22%20%22%3B%0A%09position%3Aabsolute%3B%0A%09width%3A100%25%3B%0A%09height%3A100%25%3B%0A%09background%3Atransparent%3B%0A%09padding%3A10px%3B%0A%09margin-left%3A-10px%3Bmargin-top%3A-10px%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.ares%20.tp-bullet%20%7B%0A%09width%3A13px%3B%0A%09height%3A13px%3B%0A%09position%3Aabsolute%3B%0A%09background%3Argba%28229%2C%20229%2C%20229%2C%201%29%3B%0A%09border-radius%3A50%25%3B%0A%09cursor%3A%20pointer%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A.ares%20.tp-bullet%3Ahover%2C%0A.ares%20.tp-bullet.selected%20%7B%0A%09background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%7D%0A.ares%20.tp-bullet-title%20%7B%0A%20%20position%3Aabsolute%3B%0A%20%20color%3A136%2C%20136%2C%20136%3B%0A%20%20font-size%3A12px%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20font-weight%3A600%3B%0A%20%20right%3A27px%3B%0A%20%20top%3A-4px%3B%20%20%0A%20%20background%3Argba%28255%2C255%2C255%2C0.75%29%3B%0A%20%20visibility%3Ahidden%3B%0A%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20transition%3Atransform%200.3s%3B%0A%20%20-webkit-transition%3Atransform%200.3s%3B%0A%20%20line-height%3A20px%3B%0A%20%20white-space%3Anowrap%3B%0A%7D%20%20%20%20%20%0A%0A.ares%20.tp-bullet-title%3Aafter%20%7B%0A%20%20%20%20width%3A%200px%3B%0A%09height%3A%200px%3B%0A%09border-style%3A%20solid%3B%0A%09border-width%3A%2010px%200%2010px%2010px%3B%0A%09border-color%3A%20transparent%20transparent%20transparent%20rgba%28255%2C255%2C255%2C0.75%29%3B%0A%09content%3A%22%20%22%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20right%3A-10px%3B%0A%09top%3A0px%3B%0A%7D%0A%20%20%20%20%0A.ares%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20visibility%3Avisible%3B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%7D%0A%0A.ares%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%20%7B%0A%20%20%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%7D%0A.ares%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3Atransparent%20transparent%20transparent%20rgba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%7D%0A.ares.tp-bullets%3Ahover%20.tp-bullet-title%20%7B%0A%20%20visibility%3Ahidden%3B%0A%20%20%0A%7D%0A.ares.tp-bullets%3Ahover%20.tp-bullet%3Ahover%20.tp-bullet-title%20%7B%0A%20%20%20%20visibility%3Avisible%3B%0A%20%20%20%20transform%3AtranslateX%280px%29%20translatey%280px%29%3B%0A%20%20-webkit-transform%3AtranslateX%280px%29%20translatey%280px%29%3B%0A%7D%0A%0A%0A%2F%2A%20VERTICAL%20%2A%2F%0A.ares.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%20%7B%20right%3Aauto%3B%20left%3A27px%3B%20%20transform%3Atranslatex%2820px%29%3B%20-webkit-transform%3Atranslatex%2820px%29%3B%7D%20%20%0A.ares.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%2010px%2010px%2010px%200%20%21important%3B%0A%20%20border-color%3A%20transparent%20rgba%28255%2C255%2C255%2C0.75%29%20transparent%20transparent%3B%0A%20%20right%3Aauto%20%21important%3B%0A%20%20left%3A-10px%20%21important%3B%20%20%20%0A%7D%0A.ares.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3A%20%20transparent%20rgba%28255%2C%20255%2C%20255%2C%201%29%20transparent%20transparent%20%21important%3B%0A%7D%0A%0A%0A%0A%2F%2A%20HORIZONTAL%20BOTTOM%20%26%26%20CENTER%20%2A%2F%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet-title%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet-title%20%7B%20top%3A-35px%3B%20left%3A50%25%3B%20right%3Aauto%3B%20transform%3A%20translateX%28-50%25%29%20translateY%28-10px%29%3B-webkit-transform%3A%20translateX%28-50%25%29%20translateY%28-10px%29%3B%20%7D%20%20%0A%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet-title%3Aafter%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%2010px%2010px%200px%2010px%3B%0A%20%20border-color%3A%20rgba%28255%2C255%2C255%2C0.75%29%20transparent%20transparent%20transparent%3B%0A%20%20right%3Aauto%3B%0A%20%20left%3A50%25%3B%0A%20%20margin-left%3A-10px%3B%0A%20%20top%3Aauto%3B%0A%20%20bottom%3A-10px%3B%0A%20%20%20%20%0A%7D%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3A%20%20rgba%28255%2C%20255%2C%20255%2C%201%29%20transparent%20transparent%20transparent%3B%0A%7D%0A%0A.ares.nav-dir-horizontal.nav-pos-ver-center%20.tp-bullet%3Ahover%20.tp-bullet-title%2C%0A.ares.nav-dir-horizontal.nav-pos-ver-bottom%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20%20transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%20%20-webkit-transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%7D%0A%0A%0A%2F%2A%20HORIZONTAL%20TOP%20%2A%2F%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%20%7B%20top%3A25px%3B%20left%3A50%25%3B%20right%3Aauto%3B%20transform%3A%20translateX%28-50%25%29%20translateY%2810px%29%3B-webkit-transform%3A%20translateX%28-50%25%29%20translateY%2810px%29%3B%20%7D%20%20%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%3Aafter%20%7B%20%0A%20%20border-width%3A%200%2010px%2010px%2010px%3B%0A%20%20border-color%3A%20%20transparent%20transparent%20rgba%28255%2C255%2C255%2C0.75%29%20transparent%3B%0A%20%20right%3Aauto%3B%0A%20%20left%3A50%25%3B%0A%20%20margin-left%3A-10px%3B%0A%20%20bottom%3Aauto%3B%0A%20%20top%3A-10px%3B%0A%20%20%20%20%0A%7D%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet.selected%3Ahover%20.tp-bullet-title%3Aafter%20%7B%0A%20%20border-color%3A%20%20transparent%20transparent%20%20rgba%28255%2C%20255%2C%20255%2C%201%29%20transparent%3B%0A%7D%0A%0A.ares.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet%3Ahover%20.tp-bullet-title%7B%0A%20%20%20transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%20%20-webkit-transform%3AtranslateX%28-50%25%29%20translatey%280px%29%3B%0A%7D%0A%0A%0A");
                  var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                  if(htmlDiv) {
                     htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                  }
                  else{
                     var htmlDiv = document.createElement('div');
                     htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                     document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                  }
                   
               </script>
            </div>
            <!-- END REVOLUTION SLIDER -->      
         </div>
         
         <!-- #main -->
         <div class="fusion-footer">
            <footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
               <div class="fusion-row">
                  <div class="fusion-columns fusion-columns-4 fusion-widget-area">
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-2" class="fusion-footer-widget-column widget widget_text">
                           <div class="textwidget"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/footer-logo-hai-phat-land.png" alt="Logo Footer Hai Phat Land" /><br><br/>
                              <span style="font-family: UTM-Wedding; font-size: 25px; color: #d2bd56;">Chúng tôi luôn đặt trí lực, tâm lực vào mỗi sản phẩm bởi chất lượng là cốt lõi, là sự hài lòng của khách hàng và  là thành công của chúng tôi.</span>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-4" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Thông tin liên hệ</h4>
                           <div class="textwidget">
                              <p><i class="fontawesome-icon  fa fa-map-marker circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Địa chỉ: Tầng 1&2 CT4, Tổ hợp TMDV và Căn hộ The Pride, KĐT An Hưng, P. La Khê, Q. Hà Đông, TP. Hà Nội.<br><br/>
                                 <i class="fontawesome-icon  fa fa-phone circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i>Số điện thoại: 0422 001 999<br><br/>
                                 <i class="fontawesome-icon  fa fa-envelope circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Email: info@haiphatland.vn
                              </p>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="recent-posts-6" class="fusion-footer-widget-column widget widget_recent_entries">
                           <h4 class="widget-title">Tin tức mới nhất</h4>
                           <ul>
                              <li>
                                 <a href="https://www.haiphatland.vn/tin-tuc/canh-bao-nha-dau-tu-bds-ha-noi-gia-chung-cu-dang-giam.html">Cảnh báo nhà đầu tư BĐS Hà Nội: Giá chung cư đang giảm!</a>
                              </li>
                              <li>
                                 <a href="https://www.haiphatland.vn/tin-bds/meo-tu-chon-can-ho-chung-cu-hop-phong-thuy.html">Mẹo tự chọn căn hộ chung cư hợp phong thủy</a>
                              </li>
                              <li>
                                 <a href="https://www.haiphatland.vn/du-an-chung-cu-goldseason/nhan-ngay-chuyen-du-lich-khi-mua-chung-cu-goldseason-47-nguyen-tuan.html">Nhận ngay chuyến du lịch Mỹ khi mua chung cư Goldseason 47 Nguyễn Tuân</a>
                              </li>
                           </ul>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
                        <section id="text-5" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Lịch làm việc</h4>
                           <div class="textwidget">
                              <p>Chúng tôi luôn hỗ trợ Quý khách 24/24<br>
                                 <i class="fontawesome-icon  fa fa-phone-square circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Hotline: 0986 205 333<br>
                                 <i class="fontawesome-icon  fa fa-fax circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> CSKH: 0938 072 999
                              </p>
                              <h5 style="margin-top: 1em; margin-bottom: 0.5em;"><span style="font-weight: 400; color: #ddd;">GIỜ MỞ CỬA</span></h5>
                              <ul>
                                 <li><strong><span style="color: #d2bd56;">Thứ 2-Thứ 6:</span>  </strong>8h00' đến 17h30'</li>
                                 <li><strong><span style="color: #d2bd56;">Thứ 7:</span>  </strong>8h00' đến 17h30'</li>
                                 <li><strong><span style="color: #d2bd56;">Chủ nhật:</span>  </strong>8h00' đến 17h30'</li>
                              </ul>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-clearfix"></div>
                  </div>
                  <!-- fusion-columns -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- fusion-footer-widget-area -->
            <footer id="footer" class="fusion-footer-copyright-area">
               <div class="fusion-row">
                  <div class="fusion-copyright-content">
                     <div class="fusion-copyright-notice">
                        <div>
                           © Copyright <script>document.write(new Date().getFullYear());</script>   |   Website by <a href=“https://haiphatland.vn” target="_blank">Hải Phát Land</a>   |   All Rights Reserved   |   Powered by <a href='https://haiphatland.vn' t>Hải Phát</a> 
                        </div>
                     </div>
                  </div>
                  <!-- fusion-fusion-copyright-content -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- #footer -->
         </div>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>           <script type="text/javascript">
         jQuery( document ).ready( function() {
            var ajaxurl = 'https://www.haiphatland.vn/wp-admin/admin-ajax.php';
            if ( 0 < jQuery( '.fusion-login-nonce' ).length ) {
               jQuery.get( ajaxurl, { 'action': 'fusion_login_nonce' }, function( response ) {
                  jQuery( '.fusion-login-nonce' ).html( response );
               });
            }
         });
      </script>
      <script type="text/javascript">
         function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
               jQuery(sliderID).show().html(errorMessage);
         }
      </script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var wpcf7 = {"apiSettings":{"root":"https:\/\/www.haiphatland.vn\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var spuvar = {"is_admin":"","disable_style":"","ajax_mode":"1","ajax_url":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php","ajax_mode_url":"https:\/\/www.haiphatland.vn\/?spu_action=spu_load&lang=","pid":"12020","is_front_page":"","is_category":"","site_url":"https:\/\/www.haiphatland.vn","is_archive":"","is_search":"","is_preview":"","seconds_confirmation_close":"5"};
         var spuvar_social = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var toTopscreenReaderText = {"label":"Go to Top"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaToTopVars = {"status_totop_mobile":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaRevVars = {"avada_rev_styles":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaFusionSliderVars = {"side_header_break_point":"1100","slider_position":"below","header_transparency":"0","header_position":"Top","content_break_point":"800","status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <script type='text/javascript' src='https://www.haiphatland.vn/wp-content/plugins/tablepress/js/jquery.datatables.min.js?ver=1.9.1'></script>
      <script type="text/javascript">
         jQuery(document).ready(function($){
         var DataTables_language={};
         DataTables_language["vi"]={"info":"Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục","infoEmpty":"Đang xem 0 đến 0 trong tổng số 0 mục","infoFiltered":"(được lọc từ _MAX_ mục)","infoPostFix":"","lengthMenu":"Xem _MENU_ mục","processing":"Đang xử lý...","search":"Tìm:","zeroRecords":"Không tìm thấy dòng nào phù hợp","paginate": {"first":"Đầu","previous":"Trước","next":"Tiếp","last":"Cuối"},"decimal":",","thousands":"."};
         $('#tablepress-4').dataTable({"language":DataTables_language["vi"],"order":[],"orderClasses":false,"stripeClasses":["even","odd"],"paging":false,"searching":false,"info":false});
         });
      </script><!--Start of Tawk.to Script-->
      <script type="text/javascript">
         var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
         (function(){
         var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
         s1.async=true;
         s1.src='https://embed.tawk.to/58d2940b5b89e2149e1dc573/default';
         s1.charset='UTF-8';
         s1.setAttribute('crossorigin','*');
         s0.parentNode.insertBefore(s1,s0);
         })();
      </script>
      <!--End of Tawk.to Script--><script>(function(w, d){
         var b = d.getElementsByTagName("body")[0];
         var s = d.createElement("script"); s.async = true;
         var v = !("IntersectionObserver" in w) ? "8.5.2" : "10.3.5";
         s.src = "https://www.haiphatland.vn/wp-content/plugins/wp-rocket/inc/front/js/lazyload-" + v + ".min.js";
         w.lazyLoadOptions = {
            elements_selector: "img, iframe",
            data_src: "lazy-src",
            data_srcset: "lazy-srcset",
            skip_invisible: false,
            class_loading: "lazyloading",
            class_loaded: "lazyloaded",
            threshold: 300,
            callback_load: function(element) {
               if ( element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible" ) {
                  if (element.classList.contains("lazyloaded") ) {
                     if (typeof window.jQuery != "undefined") {
                        if (jQuery.fn.fitVids) {
                           jQuery(element).parent().fitVids();
                        }
                     }
                  }
               }
            }
         }; // Your options here. See "recipes" for more information about async.
         b.appendChild(s);
         }(window, document));
         
         // Listen to the Initialized event
         window.addEventListener('LazyLoad::Initialized', function (e) {
            // Get the instance and puts it in the lazyLoadInstance variable
         var lazyLoadInstance = e.detail.instance;
         
         var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
               lazyLoadInstance.update();
            } );
         } );
         
         var b      = document.getElementsByTagName("body")[0];
         var config = { childList: true, subtree: true };
         
         observer.observe(b, config);
         }, false);
      </script>      <script>function lazyLoadThumb(e){var t='<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',a='<div class="play"></div>';return t.replace("ID",e)+a}function lazyLoadYoutubeIframe(){var e=document.createElement("iframe"),t="https://www.youtube.com/embed/ID?autoplay=1";e.setAttribute("src",t.replace("ID",this.dataset.id)),e.setAttribute("frameborder","0"),e.setAttribute("allowfullscreen","1"),this.parentNode.replaceChild(e,this)}document.addEventListener("DOMContentLoaded",function(){var e,t,a=document.getElementsByClassName("rll-youtube-player");for(t=0;t<a.length;t++)e=document.createElement("div"),e.setAttribute("data-id",a[t].dataset.id),e.innerHTML=lazyLoadThumb(a[t].dataset.id),e.onclick=lazyLoadYoutubeIframe,a[t].appendChild(e)});</script>   
   </body>
</html>
<!-- This website is like a Rocket, isn't it? Performance optimized by WP Rocket. Learn more: https://wp-rocket.me - Debug: cached@1536042827 -->