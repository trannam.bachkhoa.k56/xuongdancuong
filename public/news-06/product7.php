<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->
<main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-12682" class="post-12682 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Chung cư Dream Center Home</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-04-04T09:32:05+00:00</span>                 
                     <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h1 style="text-align: center;">GIỚI THIỆU DREAM CENTER HOME</h1>
                                    </div>
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                          <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top: 0px;margin-bottom: 20px;'>
                                             <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                <div class="fusion-slider-container fusion-slider-50 full-width-slider " id="fusion-slider-sc-dream-center-home" style="height:400px; max-width:100%;">
                                                   <style type="text/css" scoped="scoped">
                                                      .fusion-slider-50 .flex-direction-nav a {
                                                      width:63px;height:63px;line-height:63px;font-size:25px;                 }
                                                   </style>
                                                   <div class="fusion-slider-loading">Loading...</div>
                                                   <div class="tfs-slider flexslider main-flex full-width-slider" data-slider_width="100%" data-slider_height="400px" data-full_screen="0" data-parallax="0" data-nav_arrows="1" data-nav_box_width="63px" data-nav_box_height="63px" data-nav_arrow_size="25px" data-pagination_circles="0" data-autoplay="1" data-loop="1" data-animation="fade" data-slideshow_speed="4000" data-animation_speed="500" data-typo_sensitivity="1" data-typo_factor="1.5" style="max-width:100%;">
                                                      <ul class="slides">
                                                         <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                            <div class="slide-content-container slide-content-left" style="display: none;">
                                                               <div class="slide-content" style="">
                                                                  <div class="buttons" >
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-01.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-01.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-01.jpg', sizingMethod='scale')';" data-imgwidth="700">
                                                            </div>
                                                         </li>
                                                         <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                            <div class="slide-content-container slide-content-left" style="display: none;">
                                                               <div class="slide-content" style="">
                                                                  <div class="buttons" >
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-02.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-02.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-02.jpg', sizingMethod='scale')';" data-imgwidth="700">
                                                            </div>
                                                         </li>
                                                         <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                            <div class="slide-content-container slide-content-left" style="display: none;">
                                                               <div class="slide-content" style="">
                                                                  <div class="buttons" >
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-03.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-03.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-03.jpg', sizingMethod='scale')';" data-imgwidth="700">
                                                            </div>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-sep-none fusion-title-size-four" style="margin-top:0px;margin-bottom:10px;">
                                       <h4 class="title-heading-left">
                                          <h2><span style="font-family: UVF-CandleScript-Pro; font-size: 25px;"><strong>Dream Center Home &#8211; Đêm yên tĩnh cho ngày tràn năng lượng</strong></span></h2>
                                       </h4>
                                    </div>
                                    <div class="fusion-text">
                                       <p class="rtejustify">Nằm giữa khu vực trung tâm quận Thanh Xuân, chung cư Dream Center Home hướng đến những giá trị chuẩn mực về một cuộc sống cân bằng, nơi mà không gian ở được tập trung tối ưu, nơi mà mọi nhu cầu về vật chất và tinh thần của con người đều được đáp ứng.</p>
                                       <p class="rtejustify">Với thông điệp “Đêm yên tĩnh cho ngày tràn năng lượng”, dự án Dream Center Home thể hiện mong muốn của chủ đầu tư nhằm hiện thực hóa giấc mơ của người dân về một nơi an cư lý tưởng “ở trung tâm giá vừa tầm” ngay giữa nội thành Hà Nội. Sau một ngày làm việc mệt nhọc, cư dân Dream Center Home được giải tỏa căng thẳng và tái tạo năng lượng mới trong một không gian riêng tư, yên tĩnh để bắt đầu một ngày mới năng động và tràn đầy sinh lực</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2017/02/bg-location-hai-phat-land.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;">TỔNG QUAN DỰ ÁN</h2>
                                       <div class="fusion-text"></div>
                                       <p style="text-align: center;">
                                    </div>
                                    <span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span>
                                    <div class="fusion-text">
                                       <p>Tọa lạc tại 282 Nguyễn Huy Tưởng giữa trung tâm quận Thanh Xuân, Dream Center Home có vị trí giao thông vô cùng tiện lợi, nằm gần đường vành đai 3 và đường cao tốc trên cao kết nối với các tuyến đường huyết mạch của Thủ đô như: đường Nguyễn Trãi, đường Lê Văn Lương, đường Trần Duy Hưng, đường vành đai 3… Từ Dream Center Home, cư dân nhanh chóng và dễ dàng kết nối tới trung tâm Thủ đô và các khu vực lân cận.</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #b7960b;" class="fusion-imageframe imageframe-none imageframe-3 hover-type-none fusion-animated" data-animationType="slideInRight" data-animationDuration="1.0" data-animationOffset="100%"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/02/ban-do-du-an-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[956f2429ec8b272a970]" data-title="ban-do-du-an-dream-center-home" title="ban-do-du-an-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/ban-do-du-an-dream-center-home.jpg" width="800" height="753" alt="Ban do Du an Dream Center Home" class="img-responsive wp-image-12973" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/ban-do-du-an-dream-center-home-200x188.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/ban-do-du-an-dream-center-home-400x377.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/ban-do-du-an-dream-center-home-600x565.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/ban-do-du-an-dream-center-home.jpg 800w" sizes="(max-width: 800px) 100vw, 600px" /></a></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:20px;">
                                       <h3 class="title-heading-left"><span style="font-family: UVF-CandleScript-Pro; font-size: 30px;">Tổng quan Dream Center Home</span></h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <ul class="fusion-checklist fusion-checklist-1" style="font-size:13px;line-height:22.1px;">
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-university" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Chủ đầu tư: Tập đoàn BRG</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-user" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Đơn vị phân phối: Công ty Cổ phần Bất động sản Hải Phát</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Quy mô: 17 tầng nổi, 1 tầng hầm, 3 tầng thương mại dịch vụ, 14 tầng căn hộ, 2 thang máy</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Diện tích khu đất: 1906m2</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Số lượng căn hộ: 98 căn, 7 căn hộ/tầng</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Mật độ thang máy: 49 căn hộ/thang máy</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Diện tích căn hộ: từ 70 – 102m2</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Thiết kế: từ 2 – 3 phòng ngủ</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Hình thức sở hữu: vĩnh viễn</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                          <li class="fusion-li-item">
                                             <span style="background-color:#e86108;font-size:11.44px;height:22.1px;width:22.1px;margin-right:9.1px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon  fa fa-star-o" style="color:#ffffff;"></i></span>
                                             <div class="fusion-li-item-content" style="margin-left:31.2px;">
                                                </p>
                                                <p><span style="font-size: 15px;"><strong>Dự kiến bàn giao: Quý II/2017</strong></span></p>
                                                <p>
                                             </div>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;">MẶT BẰNG TỔNG THỂ</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></p>
                                    </div>
                                    <div class="fusion-tabs fusion-tabs-1 clean vertical-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#143c57;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#b7960b;border-top-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style>
                                       <div class="nav">
                                          <ul class="nav-tabs">
                                             <li class="active">
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngĐiỂnhÌnh" href="#tab-0e97645243bb8dfd4fe">
                                                   <h4 class="fusion-tab-heading">MẶT BẰNG ĐIỂN HÌNH</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtẦng1" href="#tab-81efa31c24c792fb1c3">
                                                   <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG 1</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtẦng2" href="#tab-75461cedacc095d99c7">
                                                   <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG 2</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtẦng3" href="#tab-89e4e975d307bf848fd">
                                                   <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG 3</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-mẶtbẰngtẦnghẦm" href="#tab-1f5685d7f3655041343">
                                                   <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG HẦM</h4>
                                                </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="tab-content">
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li class="active">
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngĐiỂnhÌnh" href="#tab-0e97645243bb8dfd4fe">
                                                      <h4 class="fusion-tab-heading">MẶT BẰNG ĐIỂN HÌNH</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix in active" id="tab-0e97645243bb8dfd4fe">
                                             <div class="imageframe-align-center">
                                                <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-5">
                                                   <style scoped="scoped">.element-bottomshadow.image-frame-shadow-5{display:inline-block}</style>
                                                   <span class="fusion-imageframe imageframe-bottomshadow imageframe-5 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[c738ee7087039d85dcc]" data-title="mat-bang-tong-the-dream-center-home" title="mat-bang-tong-the-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home.jpg" width="2921" height="1500" alt="Mat bang dien hinh Dream Center Home" class="img-responsive wp-image-12987" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tong-the-dream-center-home.jpg 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtẦng1" href="#tab-81efa31c24c792fb1c3">
                                                      <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG 1</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-81efa31c24c792fb1c3">
                                             <div class="imageframe-align-center">
                                                <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-6">
                                                   <style scoped="scoped">.element-bottomshadow.image-frame-shadow-6{display:inline-block}</style>
                                                   <span class="fusion-imageframe imageframe-bottomshadow imageframe-6 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[06bd85f80b5d4e2b36a]" data-title="mat-bang-tang-1-dream-center-home" title="mat-bang-tang-1-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home.jpg" width="2921" height="1500" alt="Mat bang tang 1 Dream Center Home" class="img-responsive wp-image-12997" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-1-dream-center-home.jpg 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtẦng2" href="#tab-75461cedacc095d99c7">
                                                      <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG 2</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-75461cedacc095d99c7">
                                             <div class="imageframe-align-center">
                                                <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-7">
                                                   <style scoped="scoped">.element-bottomshadow.image-frame-shadow-7{display:inline-block}</style>
                                                   <span class="fusion-imageframe imageframe-bottomshadow imageframe-7 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[d0ee1e53e3991886dd3]" data-title="mat-bang-tang-2-dream-center-home" title="mat-bang-tang-2-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home.jpg" width="2921" height="1500" alt="Mat bang tang 2 Dream Center Home" class="img-responsive wp-image-12998" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-2-dream-center-home.jpg 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtẦng3" href="#tab-89e4e975d307bf848fd">
                                                      <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG 3</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-89e4e975d307bf848fd">
                                             <div class="imageframe-align-center">
                                                <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-8">
                                                   <style scoped="scoped">.element-bottomshadow.image-frame-shadow-8{display:inline-block}</style>
                                                   <span class="fusion-imageframe imageframe-bottomshadow imageframe-8 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[86818e3aff5b23218c4]" data-title="mat-bang-tang-3-dream-center-home" title="mat-bang-tang-3-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home.jpg" width="2921" height="1500" alt="Mat bang tang 3 Dream Center Home" class="img-responsive wp-image-12999" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-3-dream-center-home.jpg 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-mẶtbẰngtẦnghẦm" href="#tab-1f5685d7f3655041343">
                                                      <h4 class="fusion-tab-heading">MẶT BẰNG TẦNG HẦM</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-1f5685d7f3655041343">
                                             <div class="imageframe-align-center">
                                                <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-9">
                                                   <style scoped="scoped">.element-bottomshadow.image-frame-shadow-9{display:inline-block}</style>
                                                   <span class="fusion-imageframe imageframe-bottomshadow imageframe-9 element-bottomshadow hover-type-none"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[927eecac1c2beba2d01]" data-title="mat-bang-tang-ham-dream-center-home" title="mat-bang-tang-ham-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home.jpg" width="2921" height="1500" alt="Mat bang tang ham Dream Center Home" class="img-responsive wp-image-13000" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home-200x103.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home-400x205.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home-600x308.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home-800x411.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home-1200x616.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/mat-bang-tang-ham-dream-center-home.jpg 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2017/02/gallery-background-dream-center-home.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;"><span style="color: #ffffff;">HÌNH ẢNH CĂN HỘ</span></h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-10 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></p>
                                    </div>
                                    <div class="fusion-text">
                                       <p style="text-align: center;"><span style="font-size: 15px; color: #ffffff;">7 căn hộ/ 1 tầng</span><br />
                                          <span style="font-size: 15px; color: #ffffff;"> Diện tích các căn hộ từ 70 &#8211; 102m2</span><br />
                                          <span style="font-size: 15px; color: #ffffff;"> Mật độ thang máy thấp, chỉ 49 căn hộ/thang máy</span><br />
                                          <span style="font-size: 15px; color: #ffffff;"> 90% các căn hộ là căn góc và có 2 logia</span>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-image-carousel fusion-image-carousel-auto lightbox-enabled fusion-carousel-border">
                                       <div class="fusion-carousel" data-autoplay="yes" data-columns="3" data-itemmargin="13" data-itemwidth="180" data-touchscroll="yes" data-imagesize="auto" data-scrollitems="1">
                                          <div class="fusion-carousel-positioner">
                                             <ul class="fusion-carousel-holder">
                                                <li class="fusion-carousel-item">
                                                   <div class="fusion-carousel-item-wrapper">
                                                      <div class="fusion-image-wrapper hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/phong-ngu-con-view-01-compressed.jpg" alt=""/></div>
                                                   </div>
                                                </li>
                                                <li class="fusion-carousel-item">
                                                   <div class="fusion-carousel-item-wrapper">
                                                      <div class="fusion-image-wrapper hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/cam81-compressed.jpg" alt=""/></div>
                                                   </div>
                                                </li>
                                                <li class="fusion-carousel-item">
                                                   <div class="fusion-carousel-item-wrapper">
                                                      <div class="fusion-image-wrapper hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/cam71-compressed.jpg" alt=""/></div>
                                                   </div>
                                                </li>
                                                <li class="fusion-carousel-item">
                                                   <div class="fusion-carousel-item-wrapper">
                                                      <div class="fusion-image-wrapper hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/9-compressed.jpg" alt=""/></div>
                                                   </div>
                                                </li>
                                                <li class="fusion-carousel-item">
                                                   <div class="fusion-carousel-item-wrapper">
                                                      <div class="fusion-image-wrapper hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/1-compressed.jpg" alt=""/></div>
                                                   </div>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;">MẶT BẰNG CĂN HỘ</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-11 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top:0px;margin-bottom:0px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-12 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-07-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[2a78c67af5822ef7ab9]" data-title="can-07-dream-center-home" title="can-07-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-07-dream-center-home.jpg" width="1000" height="630" alt="Can 07 Dream Center Home 282 Nguyen Huy Tuong" class="img-responsive wp-image-12965" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-07-dream-center-home-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-07-dream-center-home-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-07-dream-center-home-600x378.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-07-dream-center-home-800x504.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-07-dream-center-home.jpg 1000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span></div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-four" style="margin-top:20px;margin-bottom:30px;">
                                       <h4 class="title-heading-center">
                                          MẶT BẰNG CĂN HỘ 07</p>
                                          <p style="text-align: center;">
                                       </h4>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top:0px;margin-bottom:0px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-13 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-06-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[e8b94d353f40cac068a]" data-title="can-06-dream-center-home" title="can-06-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-06-dream-center-home.jpg" width="1000" height="631" alt="Can 06 Dream Center Home 282 Nguyen Huy Tuong" class="img-responsive wp-image-12964" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-06-dream-center-home-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-06-dream-center-home-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-06-dream-center-home-600x379.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-06-dream-center-home-800x505.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-06-dream-center-home.jpg 1000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span></div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-four" style="margin-top:20px;margin-bottom:30px;">
                                       <h4 class="title-heading-center">
                                          MẶT BẰNG CĂN HỘ 06</p>
                                          <p style="text-align: center;">
                                       </h4>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:0px;margin-bottom:0px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-14 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-05-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[ef6e478af397a27439f]" data-title="can-05-dream-center-home" title="can-05-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-05-dream-center-home.jpg" width="1000" height="631" alt="Can 05 Dream Center Home 282 Nguyen Huy Tuong" class="img-responsive wp-image-12963" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-05-dream-center-home-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-05-dream-center-home-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-05-dream-center-home-600x379.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-05-dream-center-home-800x505.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-05-dream-center-home.jpg 1000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span></div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-four" style="margin-top:20px;margin-bottom:30px;">
                                       <h4 class="title-heading-center">
                                          MẶT BẰNG CĂN HỘ 05</p>
                                          <p style="text-align: center;">
                                       </h4>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-15 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-04-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[d774c5c6b04d0e2b00f]" data-title="can-04-dream-center-home" title="can-04-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-04-dream-center-home.jpg" width="1000" height="631" alt="Can 04 Dream Center Home 282 Nguyen Huy Tuong" class="img-responsive wp-image-12962" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-04-dream-center-home-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-04-dream-center-home-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-04-dream-center-home-600x379.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-04-dream-center-home-800x505.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-04-dream-center-home.jpg 1000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span></div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-four" style="margin-top:20px;margin-bottom:30px;">
                                       <h4 class="title-heading-center">
                                          MẶT BẰNG CĂN HỘ 04</p>
                                          <p style="text-align: center;">
                                       </h4>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-16 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-03-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[8ad2ca9416e4689105e]" data-title="can-03-dream-center-home" title="can-03-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-03-dream-center-home.jpg" width="1000" height="630" alt="Can 03 Dream Center Home 282 Nguyen Huy Tuong" class="img-responsive wp-image-12961" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-03-dream-center-home-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-03-dream-center-home-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-03-dream-center-home-600x378.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-03-dream-center-home-800x504.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-03-dream-center-home.jpg 1000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span></div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-four" style="margin-top:20px;margin-bottom:30px;">
                                       <h4 class="title-heading-center">
                                          MẶT BẰNG CĂN HỘ 03</p>
                                          <p style="text-align: center;">
                                       </h4>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-17 hover-type-zoomin"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-02-dream-center-home.jpg" class="fusion-lightbox" data-rel="iLightbox[c0f564fe533793396bf]" data-title="can-02-dream-center-home" title="can-02-dream-center-home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-02-dream-center-home.jpg" width="1000" height="631" alt="Can 02 Dream Center Home 282 Nguyen Huy Tuong" class="img-responsive wp-image-12967" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/can-02-dream-center-home-200x126.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-02-dream-center-home-400x252.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-02-dream-center-home-600x379.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-02-dream-center-home-800x505.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/03/can-02-dream-center-home.jpg 1000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span></div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-four" style="margin-top:20px;margin-bottom:30px;">
                                       <h4 class="title-heading-center">
                                          MẶT BẰNG CĂN HỘ 02</p>
                                          <p style="text-align: center;">
                                       </h4>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #e9eaee;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2017/03/background-container-hai-phat-land.png");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;">NHÀ PHÂN PHỐI ĐỘC QUYỀN</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-18 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></p>
                                    </div>
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-19 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land-150x150.png" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">NHÀ PHÂN PHỐI DREAM CENTER HOME</p></h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#b7960b;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <p class="rtejustify"><strong>Hải Phát Land</strong> <strong>Công ty Cổ phần BĐS Hải Phát</strong> (Hải Phát Land) là công ty chuyên kinh doanh, môi giới các sản phẩm bất động sản do <strong>Công ty Cổ phần Đầu tư Hải Phát</strong> làm Chủ đầu tư, đồng thời tham gia kinh doanh, môi giới sản phẩm của các dự án bất động sản khác trên toàn quốc.</p>
                                       <p class="rtejustify">Thời gian qua, Hải phát Land đã phân phối thành công liên tiếp các dự án bất động sản quy mô như khu đô thị Tân Tây Đô, Phú Lương; Tổ hợp The Pride, V3 Prime &#8211; The Vesta, Thăng Long Victory, chung cư CT4 Vimeco, Shophouse 24h&#8230; Hải Phát Land cũng đang đẩy mạnh hoạt động hợp tác đầu tư các dự án như: Dream Center Home, Oriental Westlake&#8230; để tạo những bước tiến lớn trên con đường trở thành doanh nghiệp bất động sản hàng đầu Việt Nam.</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">TIN TỨC &amp; SỰ KIỆN</h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#b7960b;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-recent-posts fusion-recent-posts-1 avada-container layout-thumbnails-on-side layout-columns-1">
                                       <section class="fusion-columns columns fusion-columns-1 columns-1">
                                          <article class="post fusion-column column col col-lg-12 col-md-12 col-sm-12 fusion-animated" data-animationType="slideInRight" data-animationDuration="1.0" data-animationOffset="100%">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/tin-tuc/can-ho-3-phong-ngu-gia-2-ty-dong-giua-trung-tam-thanh-xuan.html" class="hover-type-zoomin" aria-label="Căn hộ 3 phòng ngủ giá 2 tỷ đồng giữa trung tâm Thanh Xuân"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/gioi-thieu-dream-center-home-slider-01-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-23T13:59:31+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/tin-tuc/can-ho-3-phong-ngu-gia-2-ty-dong-giua-trung-tam-thanh-xuan.html">Căn hộ 3 phòng ngủ giá 2 tỷ đồng giữa trung tâm Thanh Xuân</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-23T13:59:31+00:00</span><span>23, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/tin-tuc/can-ho-3-phong-ngu-gia-2-ty-dong-giua-trung-tam-thanh-xuan.html#respond">0 Comments</a></span></p>
                                                <p>Ngày 18/3 tới, Công ty Cổ phần Bất động sản [...]</p>
                                             </div>
                                          </article>
                                          <article class="post fusion-column column col col-lg-12 col-md-12 col-sm-12 fusion-animated" data-animationType="slideInRight" data-animationDuration="1.0" data-animationOffset="100%">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="https://www.haiphatland.vn/dream-center-home/hon-200-khach-hang-toi-tham-quan-can-ho-mau-dream-center-home.html" class="hover-type-zoomin" aria-label="Hơn 200 khách hàng tới tham quan căn hộ mẫu Dream Center Home"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/tham-quan-can-ho-mau-dream-center-home-02-177x142.jpg" alt="" /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-10T15:18:52+00:00</span>
                                                <h4 class="entry-title"><a href="https://www.haiphatland.vn/dream-center-home/hon-200-khach-hang-toi-tham-quan-can-ho-mau-dream-center-home.html">Hơn 200 khách hàng tới tham quan căn hộ mẫu Dream Center Home</a></h4>
                                                <p class="meta"><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated" style="display:none;">2017-03-10T15:18:52+00:00</span><span>9, Tháng Ba, 2017</span><span class="fusion-inline-sep">|</span><span class="fusion-comments"><a href="https://www.haiphatland.vn/dream-center-home/hon-200-khach-hang-toi-tham-quan-can-ho-mau-dream-center-home.html#respond">0 Comments</a></span></p>
                                                <p>Dự án căn hộ Dream Center Home một lần nữa thể [...]</p>
                                             </div>
                                          </article>
                                       </section>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-modal modal fade modal-1 modalprice282" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
                                       <style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
                                       <div class="modal-dialog modal-lg">
                                          <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                                             <div class="modal-header">
                                                <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Đăng ký nhận Bảng giá Dự án Dream Center Home</h3>
                                             </div>
                                             <div class="modal-body fusion-clearfix">
                                                <p style="text-align: center;">
                                                <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                   <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right:4%;'>
                                                      <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                         <div class="imageframe-align-center"><span style="border:1px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-20 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong.jpg" width="610" height="418" alt="Popup Dream Center Home 282 Nguyen Huy Tuong" title="popup-dream-center-home-282-nguyen-huy-tuong" class="img-responsive wp-image-12823" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong-200x137.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong-400x274.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong-600x411.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong.jpg 610w" sizes="(max-width: 800px) 100vw, 610px" /></span></div>
                                                      </div>
                                                   </div>
                                                   <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                                      <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                         <div class="fusion-text">
                                                            <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của Dự án Dream Center Home tới Quý khách!</p>
                                                            <div role="form" class="wpcf7" id="wpcf7-f4180-p12682-o1" lang="en-US" dir="ltr">
                                                               <div class="screen-reader-response"></div>
                                                               <form action="/chung-cu-dream-center-home#wpcf7-f4180-p12682-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                                  <div style="display: none;">
                                                                     <input type="hidden" name="_wpcf7" value="4180" />
                                                                     <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                                     <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                                     <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4180-p12682-o1" />
                                                                     <input type="hidden" name="_wpcf7_container_post" value="12682" />
                                                                  </div>
                                                                  <p><span class="wpcf7-form-control-wrap text-702"><input type="text" name="text-702" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên" /></span></p>
                                                                  <p><span class="wpcf7-form-control-wrap email-177"><input type="email" name="email-177" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span></p>
                                                                  <p><span class="wpcf7-form-control-wrap tel-832"><input type="tel" name="tel-832" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span></p>
                                                                  <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                                                  <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                                     <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                     <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                                                  </div>
                                                               </form>
                                                            </div>
                                                         </div>
                                                         <p style="text-align: center;">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <style type='text/css'>.reading-box-container-1 .element-bottomshadow:before,.reading-box-container-1 .element-bottomshadow:after{opacity:0.7;}</style>
                                    <div class="fusion-reading-box-container reading-box-container-1" style="margin-top:0px;margin-bottom:84px;">
                                       <div class="reading-box element-bottomshadow" style="background-color:#f6f6f6;border-width:1px;border-color:#b7960b;border-right-width:3px;border-right-color:#b7960b;border-style:solid;">
                                          <a class="button fusion-button button-default button-round fusion-button-large button-large button-flat fusion-desktop-button fusion-tagline-button continue fusion-desktop-button-margin continue-right" style="-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;" href="#" target="_self" data-toggle="modal" data-target=".modalprice282"><span>ĐĂNG KÝ NGAY</span></a>
                                          <h2>Nhận ngay Bảng giá gốc Chủ đầu tư</h2>
                                          <div class="reading-box-description fusion-reading-box-additional">Giá chỉ từ 24.1 - 25.8 Triệu /M2. Ngân hàng TP Bank hỗ trợ cho vay 70% Giá trị căn hộ.</div>
                                          <div class="fusion-clearfix"></div>
                                          <a class="button fusion-button button-default button-round fusion-button-large button-large button-flat fusion-mobile-button continue-right" style="-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;" href="#" target="_self" data-toggle="modal" data-target=".modalprice282"><span>ĐĂNG KÝ NGAY</span></a>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="lien-he-txc">
                           <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:10px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                              <div class="fusion-builder-row fusion-row ">
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h2 style="text-align: center;">LIÊN HỆ VỚI CHÚNG TÔI</h2>
                                          <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-21 hover-type-none"><img src="https://haiphatland.vn/wp-content/uploads/2017/02/ornament-hai-phat-land.png" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  data-animationType=fadeInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                          <h3 class="title-heading-left">VIDEO DỰ ÁN DREAM CENTER HOME</p></h3>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-video fusion-youtube" style="max-width:768px;max-height:448px;">
                                          <div class="video-shortcode">
                                             <div class="rll-youtube-player" data-id="7aRGWG7X90A"></div>
                                             <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/7aRGWG7X90A?wmode=transparent&autoplay=0" width="768" height="448" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                          </div>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  data-animationType=fadeInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                          <h3 class="title-heading-left">
                                             ĐĂNG KÝ NHẬN BẢNG GIÁ</p>
                                             <p style="text-align: center;">
                                          </h3>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-text">
                                          <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của Dự án Dream Center Home tới Quý khách!</p>
                                          <p style="text-align: center;">
                                       </div>
                                       <div class="fusion-text">
                                          <p style="text-align: center;">
                                          <div role="form" class="wpcf7" id="wpcf7-f11814-p12682-o2" lang="vi" dir="ltr">
                                             <div class="screen-reader-response"></div>
                                             <form action="/chung-cu-dream-center-home#wpcf7-f11814-p12682-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                                                <div style="display: none;">
                                                   <input type="hidden" name="_wpcf7" value="11814" />
                                                   <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                   <input type="hidden" name="_wpcf7_locale" value="vi" />
                                                   <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f11814-p12682-o2" />
                                                   <input type="hidden" name="_wpcf7_container_post" value="12682" />
                                                </div>
                                                <p><span class="wpcf7-form-control-wrap text-833"><input type="text" name="text-833" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" /></span></p>
                                                <p><span class="wpcf7-form-control-wrap email-20"><input type="email" name="email-20" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email*" /></span></p>
                                                <p><span class="wpcf7-form-control-wrap tel-146"><input type="tel" name="tel-146" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span></p>
                                                <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                   <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                   <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                                </div>
                                             </form>
                                          </div>
                                          </p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                    <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <a href="tel:0986 205 333" class="hotline-sticky">Hotline<br><strong id="txtHotline">0986 205 333</strong></a>
                                       <style>
                                          .hotline-sticky {
                                          background: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/background-hotline-thanh-xuan-complex-hapulico-24t3.png) no-repeat scroll 0 0;
                                          top: 100px;
                                          height: 70px;
                                          position: fixed;
                                          right: 0;
                                          width: 189px;
                                          text-transform: uppercase;
                                          color: #fff;
                                          font-weight: 500;
                                          z-index: 2000;
                                          padding: 5px 0 0 10px;
                                          }
                                          .hotline-sticky strong {
                                          font-size: 18px;
                                          margin-left: 10px;
                                          }
                                       </style>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
        
      <!-- CONTENT -->
      
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   </body>
</html>
