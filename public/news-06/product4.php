<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="vi" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# og: http://ogp.me/ns#">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v7.8.1 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN.">
      <link rel="canonical" href="" />
      <meta property="og:locale" content="vi_VN" />
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta property="og:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta property="og:url" content="" />
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát" />
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:secure_url" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="400" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Website chính thức của Công ty CP BĐS Hải Phát - Hải Phát Land. Chúng tôi nỗ lực phấn đấu phát triển bền vững, trở thành doanh nghiệp hàng đầu VN." />
      <meta name="twitter:title" content="Công ty Cổ phần Bất động sản Hải Phát - Trang Chủ" />
      <meta name="twitter:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png" />
      
      
      <!-- / Yoast SEO Premium plugin. -->
      <link rel='dns-prefetch' href='//fonts.googleapis.com' />
      <link rel='dns-prefetch' href='//s.w.org' />
      <link rel="shortcut icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land.jpg" type="image/x-icon" />
      <!-- For iPhone -->
      <link rel="apple-touch-icon" href="wp-content/uploads/2016/11/favicon-hai-phat-land-57x57.jpg">
      <!-- For iPhone Retina display -->
      <link rel="apple-touch-icon" sizes="114x114" href="wp-content/uploads/2016/11/favicon-hai-phat-land-114x114.jpg">
      <!-- For iPad -->
      <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2016/11/favicon-hai-phat-land-72x72.jpg">
      <meta property="og:title" content="Trang Chủ">
      <meta property="og:type" content="article">
      <meta property="og:url" content="">
      <meta property="og:site_name" content="Công ty CP BĐS Hải Phát">
      <meta property="og:description" content="VỀ CHÚNG TÔI
         Chúng tôi là Công ty Cổ phần Bất động sản Hải Phát - HAIPHAT LAND  
         Chúng tôi là ai
         Hải Phát có đội ngũ cán bộ mang trong mình trọn vẹn nhiệt huyết của tuổi thanh xuân cùng sự sáng tạo">
      <meta property="og:image" content="wp-content/uploads/2017/02/dream-center-home-logo-hai-phat-land.png">
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
      <link rel='stylesheet' id='essential-grid-plugin-settings-css'  href='wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=2.1.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='rs-plugin-settings-css'  href='wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
      <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>
      <link rel='stylesheet' id='spu-public-css-css'  href='wp-content/plugins/popups/public/assets/css/public.css?ver=1.9.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='font-awesome-ie7-css-css'  href='wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='text-slider-plugin-styles-css'  href='wp-content/plugins/text-slider/public/assets/css/public.css?ver=1.0.0' type='text/css' media='all' />
      <link rel='stylesheet' id='avada-stylesheet-css'  href='wp-content/themes/Tinh/assets/css/style.min.css?ver=5.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='child-style-css'  href='wp-content/themes/Tinh-Child-Theme/style.css?ver=4.9.8' type='text/css' media='all' />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel='stylesheet' id='tablepress-default-css'  href='wp-content/tablepress-combined.min.css?ver=18' type='text/css' media='all' />
      <link rel='stylesheet' id='fusion-dynamic-css-css'  href='wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css?ver=4.9.8' type='text/css' media='all' />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/lightbox.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js?ver=2.1.6.1'></script>
      <script type='text/javascript' src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/text-slider.min.js?ver=1.0.0'></script>
      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*    
         #text-slider-controls .prev { 
         float: right;
         }
         #text-slider-controls .next { 
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee;   
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
      </style>
      <meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
      <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=533271836850356&ev=PageView&noscript=1"
         /></noscript>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
   </head>
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         

      <!-- HEADER -->
         <?php include('header/header.php')?>
      <!-- SLIDER  -->
     <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-13764" class="post-13764 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Dự án Goldseason 47 Nguyễn Tuân</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="https://www.haiphatland.vn/author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-07-28T09:51:42+00:00</span>                                 
                     <div class="post-content">
                        <div id="tong-quan-txc">
                           <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2016/12/background-container-tong-quan-thanh-xuan-complex.jpg");background-position: center top;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                              <div class="fusion-builder-row fusion-row ">
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"  style='margin-top:0px;margin-bottom:0px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-two" style="margin-top:0px;margin-bottom:30px;">
                                          <h2 class="title-heading-left">
                                             <h1><span style="font-family: UVF-Candlescript-Pro; font-size: 38px; color: #143c57;">Goldseason 47 Nguyễn Tuân</span></h1>
                                          </h2>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-dotted" style="border-color:#b7960b;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-content-boxes content-boxes columns row fusion-columns-4 fusion-columns-total-8 fusion-content-boxes-1 content-boxes-icon-on-top content-left fusion-delayed-animation" data-animation-delay="400" data-animationOffset="100%" style="margin-top:0px;margin-bottom:40px;">
                                          <style type="text/css" scoped="scoped">.fusion-content-boxes-1 .heading .content-box-heading {color:#143c57;}
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
                                             .fusion-content-boxes-1 .heading .heading-link:hover .content-box-heading {
                                             color: #b7960b;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
                                             color: #b7960b !important;
                                             }.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #876f08;color: rgba(255,255,255,.9);}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: rgba(255,255,255,.9);}
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
                                             background-color: #b7960b !important;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
                                             border-color: #b7960b !important;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-wrapper-hover-animation-pulsate .icon span:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-wrapper-hover-animation-pulsate .icon span:after {
                                             -webkit-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             -moz-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             box-shadow: 0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             }
                                          </style>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-first-in-row">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-tong-dien-tich.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Diện tích Golseason</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Diện tích Dự án: 22.371 m2</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-dien-tich-chung-cu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Diện tích căn hộ</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>2- 3 phòng ngủ: 65m2-110m2</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-chu-dau-tu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Chủ đầu tư</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Công ty TNR HOLDING VIET NAM</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-4 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-vi-tri.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Vị trí</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Số 47 Nguyễn Tuân, Q. Thanh Xuân, Hà Nội</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-5 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-first-in-row">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-tang-dich-vu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tầng 1, 2, 3</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Trung tâm thương mại, dịch vụ, tiện ích</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-6 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-khu-can-ho.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tầng 4-24</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Khu căn hộ cao cấp</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-7 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/3.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Đơn vị phân phối</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Sàn giao dịch Bất động sản Hải Phát</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-8 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-last">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/8.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tư vấn thiết kế</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Moore Ruble Yudell</p>
                                                </div>
                                             </div>
                                          </div>
                                          <style type="text/css" scoped="scoped">
                                             .fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
                                             background-color: #b7960b !important;
                                             border-color: #b7960b !important;
                                             }
                                          </style>
                                          <div class="fusion-clearfix"></div>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:20px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none fusion-animated" data-animationType="fadeInRight" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/phoi-canh-thanh-xuan-complex-hapulico-24t3.png" width="" height="" alt="Phoi canh Thanh Xuan Complex Hapulico 24T3" title="" class="img-responsive"/></span></div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="font-family: UVF-Candlescript-Pro; font-size: 38px; text-align: center;"><span style="color: #143c57;">Vị trí Vàng, kết nối đa chiều</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-text">
                                       <p style="text-align: center;"><a href="https://www.haiphatland.vn/du-goldseason-47-nguyen-tuan"><strong>Dự án Goldseason 47 Nguyễn Tuân</strong></a> tọa lạc tại vị trí VÀNG trong lòng thủ đô Hà Nội, nơi được đánh giá cao về cơ sở hạ tầng tiện ích đồng bộ, liên kế vùng giao thông thuận tiện, dân trí cao.</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first lien-ket-vung-txc fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-column-content-centered">
                                       <div class="fusion-column-content">
                                          <div class="wrap-overview">
                                             <div class="overview-info">
                                                <div class="page-branch-title">
                                                   <h1 style="text-align: center;"><span style="font-family: UTM-Fleur; color: #143c57;">Liên kết vùng <span style="color: #b7960b;">Goldseason</span></span></h1>
                                                </div>
                                                <div class="row gutter-0">
                                                   <div class="col-xs-3 col-lg-2 small-title">Vị trí:</div>
                                                   <div class="col-xs-9 col-lg-10">Số 47 Nguyễn Tuân, P. Nhân Chính, Q. Thanh Xuân, TP. Hà Nội.</div>
                                                   <ul class="col-xs-12 ls-overview">
                                                      <li class="item item1"><i class="fa fa-star" style="color: #b7960b;"></i> Trung Hòa Nhân Chính<br><span>~ 500 m <small>(5 phút)</small></span></li>
                                                      <li class="item item2"><i class="fa fa-star" style="color: #b7960b;"></i> THPT Amsterdam<br><span>~ 1 km <small>(10 phút)</small></span></li>
                                                      <li class="item item3"><i class="fa fa-star" style="color: #b7960b;"></i> Làng SV Hacinco<br><span>~ 200 m <small>(2 phút)</small></span></li>
                                                      <li class="item item4"><i class="fa fa-star" style="color: #b7960b;"></i> BigC Thăng Long<br><span>~ 1.2 km <small>(12 phút)</small></span></li>
                                                      <li class="item item5"><i class="fa fa-star" style="color: #b7960b;"></i> Đại học KHTN<br><span>~ 1 km <small>(10 phút)</small></span></li>
                                                      <li class="item item6"><i class="fa fa-star" style="color: #b7960b;"></i> Royal City<br><span>~ 1.5km <small>(15 phút)</small></span></li>
                                                      <li class="item item7"><i class="fa fa-star" style="color: #b7960b;"></i> TT Hội nghị Quốc Gia<br><span>~ 1.5 km <small>(15 phút)</small></span></li>
                                                      <li class="item item8"><i class="fa fa-star" style="color: #b7960b;"></i> Mardarin Garden<br><span>~ 500 m <small>(5 phút)</small></span></li>
                                                      <li class="item item9"><i class="fa fa-star" style="color: #b7960b;"></i> Công viên Nhân Chính<br><span>~ 800 m <small>(8 phút)</small></span></li>
                                                      <li class="item item10"><i class="fa fa-star" style="color: #b7960b;"></i> Rạp chiếu phim QG<br><span>~ 1.8 km <small>(18 phút)</small></span></li>
                                                   </ul>
                                                   <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                             </div>
                                             <div class="wrap-bottom-right">
                                                <a href="/thanh-xuan-complex/vi-sao-nen-lua-chon-chung-cu-thanh-xuan-complex-hapulico-24t3.html" class="bt-control bt-detail">Xem thêm</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-last vi-tri-txc 2_3"  style='margin-top:0px;margin-bottom:20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-column-content-centered">
                                       <div class="fusion-column-content">
                                          <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/map-du-an-goldseason.png" width="1161" height="789" alt="" title="map-du-an-goldseason" class="img-responsive wp-image-13821" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/map-du-an-goldseason-200x136.png 200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/map-du-an-goldseason-400x272.png 400w, https://www.haiphatland.vn/wp-content/uploads/2017/05/map-du-an-goldseason-600x408.png 600w, https://www.haiphatland.vn/wp-content/uploads/2017/05/map-du-an-goldseason-800x544.png 800w, https://www.haiphatland.vn/wp-content/uploads/2017/05/map-du-an-goldseason.png 1161w" sizes="(max-width: 800px) 100vw, 1161px" /></span></div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2017/05/bg-contact-form-goldseason.jpg");background-position: center bottom;background-repeat: no-repeat;padding-top:30px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #b7960b; font-size: 18pt;"><strong>GOLDSEASON 47 NGUYỄN TUÂN &#8211; DỰ ÁN ĐẲNG CẤP BẬC NHẤT HN</strong></span></h2>
                                       <h4 style="text-align: center;"><span style="font-size: 14pt; color: #ffffff;">CƠ HỘI CHỌN CĂN &amp; TẦNG ĐẸP NHẤT TOÀN DỰ ÁN</span></h4>
                                       <p style="text-align: center;"><span style="font-size: 16pt;"><strong><span style="color: #ffffff;">Tài liệu trọn bộ về dự án Goldseason 47 Nguyễn Tuân</span><span style="color: #ffd700;"> <span style="color: #b7960b;">&#8211;  Đăng ký ngay</span></span></strong></span></p>
                                       <p style="text-align: center;"><span style="color: #b7960b; font-size: 16pt;"><strong>HOTLINE: 0986.205.333</strong></span></p>
                                       <p style="text-align: center;">
                                       <div role="form" class="wpcf7" id="wpcf7-f4-p13764-o1" lang="vi" dir="ltr">
                                          <div class="screen-reader-response"></div>
                                          <form action="/du-goldseason-47-nguyen-tuan#wpcf7-f4-p13764-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                             <div style="display: none;">
                                                <input type="hidden" name="_wpcf7" value="4" />
                                                <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                <input type="hidden" name="_wpcf7_locale" value="vi" />
                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p13764-o1" />
                                                <input type="hidden" name="_wpcf7_container_post" value="13764" />
                                             </div>
                                             <div class="col-sm-6 col-md-3"><label class="name"><span class="wpcf7-form-control-wrap text-858"><input type="text" name="text-858" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nhập Họ tên..." /></span></label></div>
                                             <div class="col-sm-6 col-md-3"><label class="mail"><span class="wpcf7-form-control-wrap email-695"><input type="email" name="email-695" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Nhập Email..." /></span></label></div>
                                             <div class="col-sm-6 col-md-3"><label class="phone"><span class="wpcf7-form-control-wrap tel-82"><input type="tel" name="tel-82" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Nhập SĐT..." /></span></label></div>
                                             <div class="col-sm-6 col-md-3"><button class="uppercase" type="submit"><span class="fa fa-paper-plane"></span>ĐĂNG KÝ</button></div>
                                             <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                             </div>
                                          </form>
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="home-features" class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://foresa-villas.com/wp-content/uploads/2014/11/bg-features.jpg");background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #b7960b;">Tiện ích Dự án</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="color: #ffffff;"><em>Chung cư cao cấp GoldSeason được thiết kế với đầy đủ tiện ích để phục vụ cho đời sống của những cư dân nơi đây một cách tốt nhất. Đến với GoldSeason là các bạn đã đến với một không gian hoàn toàn Xanh – Sạch – Đẹp – Chất lương tốt nhất. Ở đây, chúng tôi đề cao đến việc đáp ứng được đầy đủ mọi tiện ích sống của khách hàng nhưng vẫn không làm mất đi vẻ thanh lịch và sang trọng của khu chung cư GoldSeason mang đậm phong cách Mỹ.</em></span></p>
                                    </div>
                                    <div class="project-features">
                                       <ul class="list-features">
                                          <li class="feature odd">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/be-boi-thanh-xuan-complex-hapulico-24t3-1.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Be boi Thanh Xuan Complex 24T3" />      
                                                <h3><img alt="Icon Be boi Thanh Xuan Complex 24T3"  src="https://goldseason-tnrholdings.com/wp-content/uploads/2017/06/icon-be-boi-goldseason.png" /><span>Bể bơi</span></h3>
                                             </a>
                                          </li>
                                          <li class="feature even">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <h3><img alt="Icon Gym Spa Thanh Xuan Complex 24T3"  src="https://goldseason-tnrholdings.com/wp-content/uploads/2017/06/icon-gym-spa-goldseason.png" /><span>Gym & Spa</span></h3>
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/gym-spa-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Gym Spa Thanh Xuan Complex 24T3" />        
                                             </a>
                                          </li>
                                          <li class="feature odd">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/khong-gian-xanh-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Khong gian xanh Thanh Xuan Complex 24T3" />          
                                                <h3>
                                                   <img alt="Icon Khong gian xanh Thanh Xuan Complex 24T3"  src="https://goldseason-tnrholdings.com/wp-content/uploads/2017/06/icon-khong-gian-xanh-goldseason.png" /><span>Không gian xanh</span>
                                                </h3>
                                             </a>
                                          </li>
                                          <li class="feature even">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <h3><img alt="Icon Benh vien Thanh Xuan Complex 24T3"  src="https://goldseason-tnrholdings.com/wp-content/uploads/2017/06/icon-benh-vien-goldseason.png" /><span>Bệnh viện</span></h3>
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/benh-vien-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Benh vien Thanh Xuan Complex 24T3" />        
                                             </a>
                                          </li>
                                          <li class="feature odd">
                                             <a href="https://www.haiphatland.vn/tin-tuc/to-hop-tien-ich-khien-thanh-xuan-complex-duoc-xem-la-du-an-dang-mua-nhat.html">
                                                <img width="218" height="218" src="https://www.haiphatland.vn/wp-content/uploads/2016/12/truong-hoc-thanh-xuan-complex-hapulico-24t3.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Truong hoc Thanh Xuan Complex 24T3" />          
                                                <h3>
                                                   <img alt="Icon Truong hoc Thanh Xuan Complex 24T3"  src="https://goldseason-tnrholdings.com/wp-content/uploads/2017/06/icon-truong-hoc-goldseason.png" /><span>Trường học</span>
                                                </h3>
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Sơ đồ Mặt Bằng</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-tabs fusion-tabs-1 clean nav-not-justified horizontal-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#143c57;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#b7960b;border-top-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style>
                                       <div class="nav">
                                          <ul class="nav-tabs">
                                             <li class="active">
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-cĂnhỘtÒaautumn" href="#tab-adb2e9b9897a8a16efd">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-th-list" style="font-size:13px;"></i>CĂN HỘ TÒA AUTUMN</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-cĂnhỘtÒaspring" href="#tab-2445f480968dddb092c">
                                                   <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-th-list" style="font-size:13px;"></i>CĂN HỘ TÒA SPRING</h4>
                                                </a>
                                             </li>
                                             <li>
                                                <a class="tab-link" data-toggle="tab" id="fusion-tab-nỘithẤtcĂnhỘ" href="#tab-737188ff880937888c0">
                                                   <h4 class="fusion-tab-heading">NỘI THẤT CĂN HỘ</h4>
                                                </a>
                                             </li>
                                          </ul>
                                       </div>
                                       <div class="tab-content">
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li class="active">
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-cĂnhỘtÒaautumn" href="#tab-adb2e9b9897a8a16efd">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-th-list" style="font-size:13px;"></i>CĂN HỘ TÒA AUTUMN</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix in active" id="tab-adb2e9b9897a8a16efd">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-3"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01.jpg" class="fusion-lightbox" data-rel="iLightbox[0d9009778f1e60b4c31]" data-title="can-ho-goldseason-01" title="can-ho-goldseason-01"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01.jpg" width="1280" height="905" alt="" class="img-responsive wp-image-13832" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01-1200x848.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-01.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-4"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02.jpg" class="fusion-lightbox" data-rel="iLightbox[eabed0a4629f173f13a]" data-title="can-ho-goldseason-02" title="can-ho-goldseason-02"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02.jpg" width="1280" height="905" alt="" class="img-responsive wp-image-13833" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02-1200x848.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-02.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-5"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03.jpg" class="fusion-lightbox" data-rel="iLightbox[e2167907cb741c422c3]" data-title="can-ho-goldseason-03" title="can-ho-goldseason-03"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03.jpg" width="1280" height="905" alt="" class="img-responsive wp-image-13834" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03-1200x848.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-03.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-6"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04.jpg" class="fusion-lightbox" data-rel="iLightbox[e96a9b356ea738472d0]" data-title="can-ho-goldseason-04" title="can-ho-goldseason-04"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04.jpg" width="1280" height="905" alt="" class="img-responsive wp-image-13835" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04-1200x848.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-04.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-7"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05.jpg" class="fusion-lightbox" data-rel="iLightbox[3b43c733d608a5020f7]" data-title="can-ho-goldseason-05" title="can-ho-goldseason-05"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05.jpg" width="1280" height="905" alt="" class="img-responsive wp-image-13836" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05-1200x848.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-05.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-8"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06.jpg" class="fusion-lightbox" data-rel="iLightbox[001f4a9e74969a38289]" data-title="can-ho-goldseason-06" title="can-ho-goldseason-06"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06.jpg" width="1280" height="905" alt="" class="img-responsive wp-image-13837" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06-200x141.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06-400x283.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06-600x424.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06-800x566.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06-1200x848.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/05/can-ho-goldseason-06.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-cĂnhỘtÒaspring" href="#tab-2445f480968dddb092c">
                                                      <h4 class="fusion-tab-heading"><i class="fontawesome-icon  fa fa-th-list" style="font-size:13px;"></i>CĂN HỘ TÒA SPRING</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-2445f480968dddb092c">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-9"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07.jpg" class="fusion-lightbox" data-rel="iLightbox[a86c5b5ecc311971857]" data-title="can-ho-goldseason-07" title="can-ho-goldseason-07"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07.jpg" width="1280" height="796" alt="" class="img-responsive wp-image-13842" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07-200x124.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07-400x249.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07-600x373.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07-800x498.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07-1200x746.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-07.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-10"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-09.jpg" class="fusion-lightbox" data-rel="iLightbox[dd6df488aab924abb95]" data-title="can-ho-goldseason-09" title="can-ho-goldseason-09"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-09.jpg" width="1085" height="769" alt="" class="img-responsive wp-image-13844" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-09-200x142.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-09-400x284.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-09-600x425.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-09-800x567.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-09.jpg 1085w" sizes="(max-width: 800px) 100vw, 1085px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-11"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08.jpg" class="fusion-lightbox" data-rel="iLightbox[b8715e4baa253a2442a]" data-title="can-ho-goldseason-08" title="can-ho-goldseason-08"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08.jpg" width="1280" height="720" alt="" class="img-responsive wp-image-13843" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08-800x450.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08-1200x675.jpg 1200w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-08.jpg 1280w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center">
                                                         <div class="imageframe-liftup"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-12"><a href="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-10.jpg" class="fusion-lightbox" data-rel="iLightbox[d1cb6d5d51d37afe5e3]" data-title="can-ho-goldseason-10" title="can-ho-goldseason-10"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-10.jpg" width="900" height="507" alt="" class="img-responsive wp-image-13845" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-10-200x113.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-10-400x225.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-10-600x338.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-10-800x451.jpg 800w, https://www.haiphatland.vn/wp-content/uploads/2017/06/can-ho-goldseason-10.jpg 900w" sizes="(max-width: 800px) 100vw, 900px" /></a></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="nav fusion-mobile-tab-nav">
                                             <ul class="nav-tabs">
                                                <li>
                                                   <a class="tab-link" data-toggle="tab" id="mobile-fusion-tab-nỘithẤtcĂnhỘ" href="#tab-737188ff880937888c0">
                                                      <h4 class="fusion-tab-heading">NỘI THẤT CĂN HỘ</h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="tab-pane fade fusion-clearfix" id="tab-737188ff880937888c0">
                                             <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"  style='margin-top: 0px;margin-bottom: 20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right:4%;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="fusion-slider-container fusion-slider-45 full-width-slider " id="fusion-slider-sc-thanh-xuan-complex" style="height:400px; max-width:100%;">
                                                         <style type="text/css" scoped="scoped">
                                                            .fusion-slider-45 .flex-direction-nav a {
                                                            width:63px;height:63px;line-height:63px;font-size:25px;                                   }
                                                         </style>
                                                         <div class="fusion-slider-loading">Loading...</div>
                                                         <div class="tfs-slider flexslider main-flex full-width-slider" data-slider_width="100%" data-slider_height="400px" data-full_screen="0" data-parallax="0" data-nav_arrows="1" data-nav_box_width="63px" data-nav_box_height="63px" data-nav_arrow_size="25px" data-pagination_circles="0" data-autoplay="1" data-loop="1" data-animation="fade" data-slideshow_speed="4000" data-animation_speed="500" data-typo_sensitivity="1" data-typo_factor="1.5" style="max-width:100%;">
                                                            <ul class="slides">
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-khach-can-ho-2-phong-ngu-thanh-xuan-complex.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-khach-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-khach-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-master-can-ho-2-phong-ngu-thanh-xuan-complex.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-master-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-master-can-ho-2-phong-ngu-thanh-xuan-complex.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-tre-em-thanh-xuan-complex-hapulico-24t3.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-tre-em-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ngu-tre-em-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/bep-can-ho-2-phong-ngu-thanh-xuan-complex-hapulico-24t3.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/bep-can-ho-2-phong-ngu-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/bep-can-ho-2-phong-ngu-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                               <li data-mute="yes" data-loop="yes" data-autoplay="yes">
                                                                  <div class="slide-content-container slide-content-left" style="display: none;">
                                                                     <div class="slide-content" style="">
                                                                        <div class="buttons" >
                                                                           <div class="tfs-button-2">
                                                                              <div class="fusion-button-wrapper">
                                                                                 <style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:rgba(255,255,255,.8);}.fusion-button.button-1 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-1 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:rgba(255,255,255,.9);}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-1{background: #a0ce4e;}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #87be22;}.fusion-button.button-1{width:auto;}</style>
                                                                                 <a class="fusion-button button-flat fusion-button-round button-medium button-custom button-1" target="_self" href="http://themeforest.net/item/avada-responsive-multipurpose-theme/2833226?ref=ThemeFusion"><span class="fusion-button-text">Đăng ký tư vấn</span></a>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <div class="background background-image" style="background-image: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ve-sinh-thanh-xuan-complex-hapulico-24t3.jpg);max-width:100%;height:400px;filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ve-sinh-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale');-ms-filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='https://www.haiphatland.vn/wp-content/uploads/2016/12/phong-ve-sinh-thanh-xuan-complex-hapulico-24t3.jpg', sizingMethod='scale')';" data-imgwidth="800">
                                                                  </div>
                                                               </li>
                                                            </ul>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top: 0px;margin-bottom: 20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="fusion-text">
                                                         <div class="floorplan-summary">
                                                            <p><span style="color: #143c57;">Chung cư Hapulico 24T3 (Thanh Xuân Complex) được trang bị c&#8230; <span style="color: #d2bd56;"><a style="color: #d2bd56;" href="http://foresa-villas.com/product/nha-lien-ke.html"><strong>Xem chi tiết</strong></a></span></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng khách: Sàn, Tường, Trần,</strong></span><strong> </strong><span style="color: #143c57;"><strong>Cửa đi chính vào căn hộ, Cửa sổ, cửa ra vào logia, Thiết bị điện, Điều hoà nhiệt độ, Loa thông báo.</strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng ngủ điển hình: </strong><strong>Sàn, Tường, Trần, Cửa đi thông phòng, Cửa sổ, cửa ra vào logia, Thiết bị điện, Điều hoà nhiệt độ.</strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng vệ sinh điển hình: </strong><strong>Sàn, Tường, Thiết bị vệ sinh, Trần, Cửa đi vào vệ sinh, Thiết bị điện.</strong></span></p>
                                                            <p><span style="color: #143c57;"><strong>+) Phòng bếp: </strong><strong>Sàn, Tường, Trần, Tủ bếp, Thiết bị điện.</strong></span></p>
                                                            <p><strong>+) <span style="color: #143c57;">Logia: Sàn, Cửa sổ, Cửa đi.</span></strong></p>
                                                            <p><span style="color: #143c57;"><strong>+) Sảnh chính, hành lang &amp; cầu thang: </strong><strong>Sàn hành lang, sảnh chung, Tường, Trần, Cầu thang bộ.</strong></span></p>
                                                         </div>
                                                         <p style="text-align: center;">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none txc-ly-do nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2016/12/background-container-thanh-xuan-complex-hapulico-24t3.svg");background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:20px;padding-bottom:95px;padding-left:20px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last anh-9-ly-do 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-13 hover-type-none fusion-no-small-visibility"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/05/ly-do-khach-hang-chon-goldseason.svg" width="" height="" alt="Ly do Khach hang chon Thanh Xuan Complex Hapulico 24T3" title="ly-do-khach-hang-chon-goldseason" class="img-responsive wp-image-13824"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-14 hover-type-none fusion-no-medium-visibility fusion-no-large-visibility"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/mobile-ly-do-khach-hang-thanh-xuan-complex-hapulico-24t3.svg" width="" height="" alt="Ly do Khach hang Thanh Xuan Complex Hapulico 24T3 Mobile" title="" class="img-responsive"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"><span class="icon-wrapper" style="border-color:#d2bd56;background-color:rgba(255,255,255,0);"><i class=" fa fa-wifi" style="color:#d2bd56;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="lien-he-txc">
                           <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                              <div class="fusion-builder-row fusion-row ">
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">Liên hệ với Chúng tôi</span></h2>
                                          <p style="text-align: center;">
                                          <div class="fusion-sep-clear"></div>
                                          <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                          </p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                          <h3 class="title-heading-left">VIDEO DỰ ÁN GOLDSEASON</p></h3>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-video fusion-youtube" style="max-width:768px;max-height:432px;">
                                          <div class="video-shortcode">
                                             <div class="rll-youtube-player" data-id="wqW_7NbalTg"></div>
                                             <noscript><iframe title="YouTube video player" src="https://www.youtube.com/embed/wqW_7NbalTg?wmode=transparent&autoplay=0" width="768" height="432" allowfullscreen allow="autoplay; fullscreen"></iframe></noscript>
                                          </div>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                          <h3 class="title-heading-left">ĐĂNG KÝ NHẬN BẢNG GIÁ</h3>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-text">
                                          <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của Dự án Thanh Xuân Complex tới Quý khách!</p>
                                       </div>
                                       <div class="fusion-text">
                                          <p style="text-align: center;">
                                          <div role="form" class="wpcf7" id="wpcf7-f13809-p13764-o2" lang="vi" dir="ltr">
                                             <div class="screen-reader-response"></div>
                                             <form action="/du-goldseason-47-nguyen-tuan#wpcf7-f13809-p13764-o2" method="post" class="wpcf7-form" novalidate="novalidate">
                                                <div style="display: none;">
                                                   <input type="hidden" name="_wpcf7" value="13809" />
                                                   <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                   <input type="hidden" name="_wpcf7_locale" value="vi" />
                                                   <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f13809-p13764-o2" />
                                                   <input type="hidden" name="_wpcf7_container_post" value="13764" />
                                                </div>
                                                <p><span class="wpcf7-form-control-wrap text-629"><input type="text" name="text-629" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên" /></span></p>
                                                <p><span class="wpcf7-form-control-wrap email-752"><input type="email" name="email-752" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span></p>
                                                <p><span class="wpcf7-form-control-wrap tel-437"><input type="tel" name="tel-437" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span></p>
                                                <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                   <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                   <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                                </div>
                                             </form>
                                          </div>
                                          </p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                    <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <a href="tel:0986 205 333" class="hotline-sticky">Hotline<br><strong id="txtHotline">0986 205 333</strong></a>
                                       <style>
                                          .hotline-sticky {
                                          background: url(https://www.haiphatland.vn/wp-content/uploads/2016/12/background-hotline-thanh-xuan-complex-hapulico-24t3.png) no-repeat scroll 0 0;
                                          top: 100px;
                                          height: 70px;
                                          position: fixed;
                                          right: 0;
                                          width: 189px;
                                          text-transform: uppercase;
                                          color: #fff;
                                          font-weight: 500;
                                          z-index: 2000;
                                          padding: 5px 0 0 10px;
                                          }
                                          .hotline-sticky strong {
                                          font-size: 18px;
                                          margin-left: 10px;
                                          }
                                       </style>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
         <!-- #main -->
         
         
        
      <!-- CONTENT -->
      
      <!-- FOOTER -->
      <?php include('footer/footer.php')?>
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
      <a class="fusion-one-page-text-link fusion-page-load-link"></a>
      <a href="tel:0986205333" id="callnowbutton" ></a>

      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.3'></script>

      <script type='text/javascript' src='wp-content/plugins/popups/public/assets/js/public.js?ver=1.9.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/text-slider/public/assets/js/public.js?ver=1.0.0'></script>
      <!--[if IE 9]>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
      <![endif]-->
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js?ver=3.3.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js?ver=1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js?ver=2.2.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js?ver=3.0.6'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js?ver=3.1.8'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js?ver=3.0.4'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js?ver=2.0.0'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-portfolio.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js?ver=2.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-faqs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js?ver=3.0.3'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js?ver=2.1.28'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js?ver=2.0.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js?ver=2.0'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js?ver=1.0'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js?ver=2.1.7'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js?ver=2.7.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js?ver=3.3.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js?ver=3.1.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js?ver=3.1.1'></script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js?ver=3.3.5'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js?ver=6.2.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js?ver=1.3'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js?ver=2.2.2'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js?ver=2.0.7'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js?ver=1.6.6'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-header.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-menu.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js?ver=1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js?ver=1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js?ver=3.3.2'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-comments.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-select.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js?ver=5.6.1'></script>
  
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js?ver=1.2'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js?ver=5.6.1'></script>
    
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js?ver=5.6.1'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js?ver=5.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js?ver=5.6.1'></script>
   
      <script type='text/javascript' src='wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js?ver=1'></script>
      <!--Start of Tawk.to Script-->
      
   </body>
</html>
