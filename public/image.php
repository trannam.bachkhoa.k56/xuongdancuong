<?php
error_reporting(0);
Header("Content-Type: image/jpeg");
date_default_timezone_set('Asia/Ho_Chi_Minh');
$dayOpen = date('d/m/Y');
$hourOpen = date('H:i:s');
$author = '';
$receiver = '';
if(isset($_GET['receiver']) && !empty($_GET['receiver'])){
    $receiver = $_GET['receiver'];
}

if(isset($_GET['author']) && !empty($_GET['author'])){
    $author = $_GET['author'];
}
$file = fopen('log.txt', 'a+');
$stringData = $receiver . ';'  . $author . ';' . $hourOpen . '-' . $dayOpen .  ';'  . date('Y-m-d') . '\r\n';
fwrite($file, $stringData);
fclose($file);

$newImage = ImageCreate(1,1);
ImageJPEG($newImage);
ImageDestroy($newImage);