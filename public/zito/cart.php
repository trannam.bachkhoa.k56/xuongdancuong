<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js">
   <!--<![endif]-->
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <title>Nội Thất ZITO | Showroom Nội Thất Gỗ Cao Cấp Tại Hà Nội</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Nội thất ZITO tiên phong cung cấp các sản phẩm Sofa gỗ, da, nỉ theo kích thước tùy chỉnh, xu hướng mới nhất Showroom số 15 Dương Đình Nghệ Cầu Giấy Hà Nội"/>
      <meta name="robots" content="noodp"/>
      <base href="http://vn3c.net/zito/">
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' type='text/css' media='all' />
      <link rel='stylesheet' id='yith_wcas_frontend-css'  href='css/yith_wcas_ajax_search.css' type='text/css' media='all' />
      <style id='yith_wcas_frontend-inline-css' type='text/css'>
         .autocomplete-suggestion{
         padding-right: 20px;
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
         .autocomplete-suggestion  span.yith_wcas_result_on_sale{
         background: #7eb742;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
         .autocomplete-suggestion  span.yith_wcas_result_outofstock{
         background: #7a7a7a;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
         .autocomplete-suggestion  span.yith_wcas_result_featured{
         background: #c0392b;
         color: #ffffff
         }
         .autocomplete-suggestion img{
         width: 50px;
         }
         .autocomplete-suggestion .yith_wcas_result_content .title{
         color: #004b91;
         }
         .autocomplete-suggestion{
         min-height: 60px;
         }
      </style>
      <link rel='stylesheet' id='flatsome-main-css'  href='css/flatsome.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-shop-css'  href='css/flatsome-shop.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-style-css'  href='css/style1.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/jquery.js'></script>
      <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
      <script type='text/javascript' src='js/advanced-cf7-db-public.js'></script>
      <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>
   </head>
   <body>
   </body>
   <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
   <noscript>
      <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
   </noscript>
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
   <link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
   <meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
   <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>
   </head>
   <body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
      <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
      <div id="wrapper">
         <?php include('header/header.php')?>
       <main id="main" class="">
<div id="content" class="content-area page-wrapper" role="main">
   <div class="row row-main">
      <div class="large-12 col">
         <div class="col-inner">
            
            
                                          
                  <div class="woocommerce"><div class="woocommerce row row-large row-divided">
<div class="col large-7 pb-0 ">


<form action="https://zito.vn/gio-hang/" method="post" class="woocommerce-cart-form">
<div class="cart-wrapper sm-touch-scroll">


<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
   <thead>
      <tr>
         <th class="product-name" colspan="3">Sản phẩm</th>
         <th class="product-price">Giá</th>
         <th class="product-quantity">Số lượng</th>
         <th class="product-subtotal">Tổng cộng</th>
      </tr>
   </thead>
   <tbody>
      
              <tr class="woocommerce-cart-form__cart-item cart_item">

          <td class="product-remove">
            <a href="https://zito.vn/gio-hang/?remove_item=a0205b87490c847182672e8d371e9948&amp;_wpnonce=c5e0ebb11a" class="remove" aria-label="Xóa sản phẩm này" data-product_id="4742" data-product_sku="">×</a>          </td>

          <td class="product-thumbnail">
            <a href="https://zito.vn/shop/goi-tua/zp-meo-03/"><img width="130" height="80" src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image lazy-load-active" alt="Gối tựa sofa" srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-520x320.jpg 520w" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px"></a>          </td>

          <td class="product-name" data-title="Sản phẩm">
            <a href="https://zito.vn/shop/goi-tua/zp-meo-03/">Gối ZP Mèo 03</a>          </td>

           <td class="product-price" data-title="Giá">
            <span class="woocommerce-Price-amount amount">220.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span>          </td>

              <td class="product-quantity" data-title="Số lượng">
                <div class="quantity buttons_added">
    <input type="button" value="-" class="minus button is-form">    <input type="number" class="input-text qty text" step="1" min="0" max="9999" name="cart[a0205b87490c847182672e8d371e9948][qty]" value="1" title="SL" size="4" pattern="[0-9]*" inputmode="numeric">
    <input type="button" value="+" class="plus button is-form">  </div>
              </td>

                <td class="product-subtotal" data-title="Tổng cộng">
              <span class="woocommerce-Price-amount amount">220.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span>            </td>
            </tr>
                      <tr class="woocommerce-cart-form__cart-item cart_item">

          <td class="product-remove">
            <a href="https://zito.vn/gio-hang/?remove_item=8f4576ad85410442a74ee3a7683757b3&amp;_wpnonce=c5e0ebb11a" class="remove" aria-label="Xóa sản phẩm này" data-product_id="4648" data-product_sku="">×</a>          </td>

          <td class="product-thumbnail">
            <a href="https://zito.vn/shop/ban-tra/zb-007/"><img width="130" height="80" src="//zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg" data-src="//zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image lazy-load-active" alt="bàn trà gỗ vuông tại hà nội" srcset="//zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-520x320.jpg 520w" data-srcset="//zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px"></a>          </td>

          <td class="product-name" data-title="Sản phẩm">
            <a href="https://zito.vn/shop/ban-tra/zb-007/">Bàn Trà ZITO ZB 007</a>          </td>

           <td class="product-price" data-title="Giá">
            <span class="woocommerce-Price-amount amount">5.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span>          </td>

              <td class="product-quantity" data-title="Số lượng">
                <div class="quantity buttons_added">
    <input type="button" value="-" class="minus button is-form">    <input type="number" class="input-text qty text" step="1" min="0" max="9999" name="cart[8f4576ad85410442a74ee3a7683757b3][qty]" value="1" title="SL" size="4" pattern="[0-9]*" inputmode="numeric">
    <input type="button" value="+" class="plus button is-form">  </div>
              </td>

                <td class="product-subtotal" data-title="Tổng cộng">
              <span class="woocommerce-Price-amount amount">5.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span>            </td>
            </tr>
                  <tr>
         <td colspan="6" class="actions clear">

            <div class="continue-shopping pull-left text-left">
    <a class="button-continue-shopping button primary is-outline" href="https://zito.vn/shop/">
        ← Tiếp tục mua hàng    </a>
</div>

            <input type="submit" class="button primary mt-0 pull-left small" name="update_cart" value="Cập nhật giỏ hàng" disabled="">

            <input type="hidden" id="_wpnonce" name="_wpnonce" value="c5e0ebb11a"><input type="hidden" name="_wp_http_referer" value="/gio-hang/">       </td>
      </tr>

         </tbody>
</table>
</div>
</form>
</div>

<div class="cart-collaterals large-5 col pb-0">
   <div class="cart-sidebar col-inner ">
    <div class="cart_totals ">

             <table cellspacing="0">
          <thead>
              <tr>
                  <th class="product-name" colspan="2" style="border-width:3px;">Thông tin thanh toán</th>
              </tr>
          </thead>
          </table>
  
   <h2>Thông tin thanh toán</h2>

   <table cellspacing="0" class="shop_table shop_table_responsive">

      <tbody><tr class="cart-subtotal">
         <th>Tổng phụ</th>
         <td data-title="Tổng phụ"><span class="woocommerce-Price-amount amount">5.220.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span></td>
      </tr>

      
      
      
      
      
      <tr class="order-total">
         <th>Tổng cộng</th>
         <td data-title="Tổng cộng"><strong><span class="woocommerce-Price-amount amount">5.220.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span></strong> </td>
      </tr>

      
   </tbody></table>

   <div class="wc-proceed-to-checkout">
      
<a href="https://zito.vn/thanh-toan/" class="checkout-button button alt wc-forward">
   Thanh toán ngay</a>
   </div>

   
</div>
         <div class="cart-sidebar-content relative"></div>  </div>
</div>
</div>
<div class="cart-footer-content after-cart-content relative"></div></div>

                  
                                    </div><!-- .col-inner -->
      </div><!-- .large-12 -->
   </div><!-- .row -->
</div>


</main>
         <?php include('footer/footer.php')?>
         <!-- .footer-wrapper -->
      </div>
      <!-- #wrapper -->
      <!-- Mobile Sidebar -->
      <?php include('header/header-mobile.php')?>
      <!-- #mobile-menu -->
      <div id="login-form-popup" class="lightbox-content mfp-hide">
         <div class="account-container lightbox-inner">
            <div class="account-login-inner">
               <h3 class="uppercase">Đăng nhập</h3>
               <form method="post" class="login">
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                     <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                  </p>
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="password">Mật khẩu <span class="required">*</span></label>
                     <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                  </p>
                  <p class="form-row">
                     <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />            <input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                     <label for="rememberme" class="inline">
                     <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu          </label>
                  </p>
                  <p class="woocommerce-LostPassword lost_password">
                     <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                  </p>
               </form>
            </div>
            <!-- .login-inner -->
         </div>
         <!-- .account-login-container -->
      </div>
      <link rel='stylesheet' id='flatsome-effects-css'  href='css/effects.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/scripts.js'></script>
      <script type='text/javascript' src='js/add-to-cart.min.js'></script>
      <script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
      <script type='text/javascript' src='js/js.cookie.min.js'></script>     
      <script type='text/javascript' src='js/woocommerce.min.js'></script>
      <script type='text/javascript' src='js/cart-fragments.min.js'></script>
      <script type='text/javascript' src='js/yith-autocomplete.min.js'></script>
      <script type='text/javascript' src='js/flatsome-live-search.js'></script>
      <script type='text/javascript' src='js/hoverIntent.min.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
            /* ]]> */
            
      </script>
      <script type='text/javascript' src='js/flatsome.js'></script>
      <script type='text/javascript' src='js/flatsome-lazy-load.js'></script>
      <script type='text/javascript' src='js/woocommerce.js'></script>
      <script type='text/javascript' src='js/wp-embed.min.js'></script>
      <script type='text/javascript' src='js/packery.pkgd.min.js'></script>
      <script type='text/javascript' src='js/zxcvbn-async.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
   </body>
</html>