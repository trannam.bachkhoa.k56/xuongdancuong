<div class="slider-wrapper relative " id="slider-611385064" >
                     <div class="slider slider-nav-circle slider-nav-large slider-nav-light slider-style-normal"
                        data-flickity-options='{
                        "cellAlign": "center",
                        "imagesLoaded": true,
                        "lazyLoad": 1,
                        "freeScroll": false,
                        "wrapAround": true,
                        "autoPlay": 6000,
                        "pauseAutoPlayOnHover" : false,
                        "prevNextButtons": true,
                        "contain" : true,
                        "adaptiveHeight" : true,
                        "dragThreshold" : 5,
                        "percentPosition": true,
                        "pageDots": true,
                        "rightToLeft": false,
                        "draggable": true,
                        "selectedAttraction": 0.1,
                        "parallax" : 0,
                        "friction": 0.6        }'
                        >
                        <div class="banner has-hover" id="banner-1934667333">
                           <div class="banner-inner fill">
                              <div class="banner-bg fill" >
                                 <div class="bg fill bg-fill "></div>
                              </div>
                              <!-- bg-layers -->
                              <div class="banner-layers container">
                                 <div class="fill banner-link"></div>
                                 <div id="text-box-1697317300" class="text-box banner-layer x0 md-x0 lg-x0 y50 md-y50 lg-y50 res-text">
                                    <div data-animate="fadeInLeft">
                                       <div class="text box-shadow-3 box-shadow-3-hover dark text-shadow-3">
                                          <div class="is-border"
                                             style="border-color:rgb(176, 176, 176);border-width:1px 1px 1px 1;">
                                          </div>
                                          <div class="text-inner text-center">
                                             <h3><span style="font-size: 100%;"><strong>Trải nghiệm tiện nghi</strong></span></h3>
                                             <p>Một không gian sống hoàn toàn tiện nghi và đẳng cấp khi trải nghiệm sofa tại NỘI THẤT ZITO tích hợp nhiều tính năng sử dụng, không chỉ để ngồi mà còn để nghỉ ngơi, đọc sách và thư giãn.</p>
                                             <a href="/lien-he" target="_self" class="button white is-outline reveal-icon"  >
                                             <span>Liên hệ ngay</span>
                                             <i class="icon-phone" ></i></a>
                                          </div>
                                       </div>
                                       <!-- text-box-inner -->
                                    </div>
                                    <style scope="scope">
                                       #text-box-1697317300 .text {
                                       background-color: rgba(0, 0, 0, 0.76);
                                       font-size: 110%;
                                       }
                                       #text-box-1697317300 .text-inner {
                                       padding: 20px 20px 20px 20px;
                                       }
                                       #text-box-1697317300 {
                                       width: 60%;
                                       }
                                       @media (min-width:550px) {
                                       #text-box-1697317300 {
                                       width: 40%;
                                       }
                                       }
                                    </style>
                                 </div>
                                 <!-- text-box -->
                              </div>
                              <!-- .banner-layers -->
                           </div>
                           <!-- .banner-inner -->
                           <style scope="scope">
                              #banner-1934667333 {
                              padding-top: 40%;
                              }
                              #banner-1934667333 .bg.bg-loaded {
                              background-image: url(https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito-zg-105-11.jpg);
                              }
                           </style>
                        </div>
                        <!-- .banner -->
                        <div class="banner has-hover" id="banner-37270990">
                           <div class="banner-inner fill">
                              <div class="banner-bg fill" >
                                 <div class="bg fill bg-fill "></div>
                              </div>
                              <!-- bg-layers -->
                              <div class="banner-layers container">
                                 <div class="fill banner-link"></div>
                                 <div id="text-box-1340190896" class="text-box banner-layer x0 md-x0 lg-x0 y50 md-y50 lg-y50 res-text">
                                    <div data-animate="fadeInRight">
                                       <div class="text box-shadow-3 box-shadow-3-hover dark text-shadow-3">
                                          <div class="is-border"
                                             style="border-color:rgb(176, 176, 176);border-width:1px 1px 1px 1;">
                                          </div>
                                          <div class="text-inner text-center">
                                             <h3>Vẻ đẹp hiện đại</h3>
                                             <p>Bắt kịp xu hướng mới nhất Nội Thất <a href="https://zito.vn/">ZITO</a> tạo nên những bộ sofa mới mẻ, thiết kế theo nhu cầu riêng về kích thước, tông màu sẽ biến hóa căn phòng theo phong cách hoàn toàn khác biệt.</p>
                                             <a href="/lien-he" target="_self" class="button white is-outline reveal-icon"  >
                                             <span>Liên hệ ngay</span>
                                             <i class="icon-phone" ></i></a>
                                          </div>
                                       </div>
                                       <!-- text-box-inner -->
                                    </div>
                                    <style scope="scope">
                                       #text-box-1340190896 .text {
                                       background-color: rgba(0, 0, 0, 0.76);
                                       font-size: 110%;
                                       }
                                       #text-box-1340190896 .text-inner {
                                       padding: 20px 20px 20px 20px;
                                       }
                                       #text-box-1340190896 {
                                       width: 60%;
                                       }
                                       @media (min-width:550px) {
                                       #text-box-1340190896 {
                                       width: 40%;
                                       }
                                       }
                                    </style>
                                 </div>
                                 <!-- text-box -->
                              </div>
                              <!-- .banner-layers -->
                           </div>
                           <!-- .banner-inner -->
                           <style scope="scope">
                              #banner-37270990 {
                              padding-top: 40%;
                              }
                              #banner-37270990 .bg.bg-loaded {
                              background-image: url(https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito-mau-ni.jpg);
                              }
                           </style>
                        </div>
                        <!-- .banner -->
                        <div class="banner has-hover" id="banner-218781787">
                           <div class="banner-inner fill">
                              <div class="banner-bg fill" >
                                 <div class="bg fill bg-fill "></div>
                              </div>
                              <!-- bg-layers -->
                              <div class="banner-layers container">
                                 <div class="fill banner-link"></div>
                                 <div id="text-box-1767651304" class="text-box banner-layer x100 md-x100 lg-x100 y50 md-y50 lg-y50 res-text">
                                    <div data-animate="fadeInLeft">
                                       <div class="text box-shadow-3 box-shadow-3-hover dark text-shadow-3">
                                          <div class="is-border"
                                             style="border-color:rgb(176, 176, 176);border-width:1px 1px 1px 1;">
                                          </div>
                                          <div class="text-inner text-center">
                                             <h3><span style="font-size: 100%;">Chất lượng tuyệt vời</span></h3>
                                             <p>Chất lượng nguyên liệu cao cấp đa dạng được nhập khẩu từ Nhật, Mỹ, Châu Âu. Sản phẩm thiết kế theo mùa, khung gỗ 100% tự nhiên. Nội Thất ZITO đảm bảo độ bền bỉ theo thời gian sử dụng.</p>
                                             <a href="/lien-he" target="_self" class="button white is-outline reveal-icon"  >
                                             <span>Liên hệ ngay</span>
                                             <i class="icon-phone" ></i></a>
                                          </div>
                                       </div>
                                       <!-- text-box-inner -->
                                    </div>
                                    <style scope="scope">
                                       #text-box-1767651304 .text {
                                       background-color: rgba(0, 0, 0, 0.76);
                                       font-size: 110%;
                                       }
                                       #text-box-1767651304 .text-inner {
                                       padding: 20px 20px 20px 20px;
                                       }
                                       #text-box-1767651304 {
                                       width: 60%;
                                       }
                                       @media (min-width:550px) {
                                       #text-box-1767651304 {
                                       width: 40%;
                                       }
                                       }
                                    </style>
                                 </div>
                                 <!-- text-box -->
                              </div>
                              <!-- .banner-layers -->
                           </div>
                           <!-- .banner-inner -->
                           <style scope="scope">
                              #banner-218781787 {
                              padding-top: 40%;
                              }
                              #banner-218781787 .bg.bg-loaded {
                              background-image: url(https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito-chat-luong-go.jpg);
                              }
                           </style>
                        </div>
                        <!-- .banner -->
                        <div class="banner has-hover" id="banner-300012686">
                           <div class="banner-inner fill">
                              <div class="banner-bg fill" >
                                 <div class="bg fill bg-fill "></div>
                              </div>
                              <!-- bg-layers -->
                              <div class="banner-layers container">
                                 <div class="fill banner-link"></div>
                                 <div id="text-box-886402506" class="text-box banner-layer x0 md-x0 lg-x0 y50 md-y50 lg-y50 res-text">
                                    <div data-animate="fadeInRight">
                                       <div class="text box-shadow-3 box-shadow-3-hover dark text-shadow-3">
                                          <div class="is-border"
                                             style="border-color:rgb(176, 176, 176);border-width:1px 1px 1px 1;">
                                          </div>
                                          <div class="text-inner text-center">
                                             <h3>Độc - Chất - Riêng biệt</h3>
                                             <p>Được thiết kế và gia công với độ hoàn thiện cao, kết cấu chắc chắn, chuẩn chỉ đến từng chi tiết. Miễn phí vận chuyển và Cam kết bảo hành trong 02 năm.</p>
                                             <a href="/lien-he" target="_self" class="button white is-outline reveal-icon"  >
                                             <span>Liên hệ ngay</span>
                                             <i class="icon-phone" ></i></a>
                                          </div>
                                       </div>
                                       <!-- text-box-inner -->
                                    </div>
                                    <style scope="scope">
                                       #text-box-886402506 .text {
                                       background-color: rgba(0, 0, 0, 0.76);
                                       font-size: 110%;
                                       }
                                       #text-box-886402506 .text-inner {
                                       padding: 20px 20px 20px 20px;
                                       }
                                       #text-box-886402506 {
                                       width: 60%;
                                       }
                                       @media (min-width:550px) {
                                       #text-box-886402506 {
                                       width: 40%;
                                       }
                                       }
                                    </style>
                                 </div>
                                 <!-- text-box -->
                              </div>
                              <!-- .banner-layers -->
                           </div>
                           <!-- .banner-inner -->
                           <style scope="scope">
                              #banner-300012686 {
                              padding-top: 40%;
                              }
                              #banner-300012686 .bg.bg-loaded {
                              background-image: url(https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito.jpg);
                              }
                           </style>
                        </div>
                        <!-- .banner -->
                     </div>
                     <div class="loading-spin dark large centered"></div>
                     <style scope="scope">
                     </style>
                  </div>