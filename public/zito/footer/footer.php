 <footer id="footer" class="footer-wrapper">
               <section class="section vd" id="section_55129917">
                  <div class="bg section-bg fill bg-fill  bg-loaded" >
                  </div>
                  <!-- .section-bg -->
                  <div class="section-content relative">
                     <div class="row row-small align-center"  id="row-1597056707">
                        <div class="col medium-6 small-12 large-6"  >
                           <div class="col-inner"  >
                              <h3>KHUYẾN MẠI</h3>
                              <div class="banner has-hover" id="banner-422701026">
                                 <div class="banner-inner fill">
                                    <div class="banner-bg fill" >
                                       <div class="bg fill bg-fill "></div>
                                       <div class="overlay"></div>
                                       <div class="effect-snow bg-effect fill no-click"></div>
                                    </div>
                                    <!-- bg-layers -->
                                    <div class="banner-layers container">
                                       <a href="https://zito.vn/khuyen-mai/"  class="fill">
                                          <div class="fill banner-link"></div>
                                       </a>
                                       <div id="text-box-248751777" class="text-box banner-layer x100 md-x100 lg-x100 y90 md-y90 lg-y90 res-text">
                                          <div class="text ">
                                             <div class="text-inner text-left">
                                                <a href="https://zito.vn/khuyen-mai/" target="_self" class="button secondary is-larger"  >
                                                <span>Xem Chi Tiết</span>
                                                </a>
                                             </div>
                                          </div>
                                          <!-- text-box-inner -->
                                          <style scope="scope">
                                             #text-box-248751777 {
                                             width: 60%;
                                             }
                                             #text-box-248751777 .text {
                                             font-size: 100%;
                                             }
                                             @media (min-width:550px) {
                                             #text-box-248751777 {
                                             width: 40%;
                                             }
                                             }
                                          </style>
                                       </div>
                                       <!-- text-box -->
                                    </div>
                                    <!-- .banner-layers -->
                                 </div>
                                 <!-- .banner-inner -->
                                 <div class="height-fix is-invisible"><img width="960" height="540" src="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg" class="attachment-orginal size-orginal" alt="nội thất ZITO khuyến mại" srcset="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg 960w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-768x432.jpg 768w" sizes="(max-width: 960px) 100vw, 960px" /></div>
                                 <style scope="scope">
                                    #banner-422701026 .bg.bg-loaded {
                                    background-image: url(https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg);
                                    }
                                    #banner-422701026 .overlay {
                                    background-color: rgba(190, 190, 190, 0.2);
                                    }
                                 </style>
                              </div>
                              <!-- .banner -->
                              <p>Chương trình áp dụng từ 01/12/2017 đối với dòng sản phẩm Sofa Gỗ Cao cấp Tùy chỉnh cho đơn hàng đặt trên tất các kênh Online và tại Showroom Nội thất ZITO.</p>
                           </div>
                        </div>
                        <div class="col medium-6 small-12 large-6"  >
                           <div class="col-inner"  >
                              <h3>DÀNH CHO KHÁCH HÀNG</h3>
                              <div class="row large-columns-1 medium-columns-1 small-columns-1 row-small has-shadow row-box-shadow-2 row-box-shadow-3-hover">
                                 <div class="col post-item" >
                                    <div class="col-inner">
                                       <a href="https://zito.vn/khuyen-mai/" class="plain">
                                          <div class="box box-vertical box-text-bottom box-blog-post has-hover">
                                             <div class="box-image" style="width:30%;">
                                                <div class="image-cover" style="padding-top:75%;">
                                                   <img width="300" height="169" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-300x169.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="nội thất ZITO khuyến mại" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg 960w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						
                                                </div>
                                             </div>
                                             <!-- .box-image -->
                                             <div class="box-text text-left" >
                                                <div class="box-text-inner blog-post-inner">
                                                   <h5 class="post-title is-large ">Chương trình khuyến mại</h5>
                                                   <div class="is-divider"></div>
                                                   <p class="from_the_blog_excerpt ">Lựa chọn nội thất cho phòng khách là một công việc mất rất nhiều thời gian, công sức &amp; tiền ...					</p>
                                                </div>
                                                <!-- .box-text-inner -->
                                             </div>
                                             <!-- .box-text -->
                                          </div>
                                          <!-- .box -->
                                       </a>
                                       <!-- .link -->
                                    </div>
                                    <!-- .col-inner -->
                                 </div>
                                 <!-- .col -->
                                 <div class="col post-item" >
                                    <div class="col-inner">
                                       <a href="https://zito.vn/chinh-sach-bao-mat-thong-tin-khach-hang/" class="plain">
                                          <div class="box box-vertical box-text-bottom box-blog-post has-hover">
                                             <div class="box-image" style="width:30%;">
                                                <div class="image-cover" style="padding-top:75%;">
                                                   <img width="300" height="150" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito-300x150.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito-300x150.jpg 300w, https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito-768x384.jpg 768w, https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito.jpg 1001w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						
                                                </div>
                                             </div>
                                             <!-- .box-image -->
                                             <div class="box-text text-left" >
                                                <div class="box-text-inner blog-post-inner">
                                                   <h5 class="post-title is-large ">Bảo mật thông tin khách hàng</h5>
                                                   <div class="is-divider"></div>
                                                   <p class="from_the_blog_excerpt ">MỤC ĐÍCH VÀ PHẠM VI THU THẬP Để truy cập và sử dụng một số dịch vụ tại zito.vn, quý ...					</p>
                                                </div>
                                                <!-- .box-text-inner -->
                                             </div>
                                             <!-- .box-text -->
                                          </div>
                                          <!-- .box -->
                                       </a>
                                       <!-- .link -->
                                    </div>
                                    <!-- .col-inner -->
                                 </div>
                                 <!-- .col -->
                                 <div class="col post-item" >
                                    <div class="col-inner">
                                       <a href="https://zito.vn/chinh-sach-bao-hanh-luu-y-su-dung/" class="plain">
                                          <div class="box box-vertical box-text-bottom box-blog-post has-hover">
                                             <div class="box-image" style="width:30%;">
                                                <div class="image-cover" style="padding-top:75%;">
                                                   <img width="300" height="197" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="cách làm sạch ghế sofa da hiệu quả" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						
                                                </div>
                                             </div>
                                             <!-- .box-image -->
                                             <div class="box-text text-left" >
                                                <div class="box-text-inner blog-post-inner">
                                                   <h5 class="post-title is-large ">Chính sách bảo hành, lưu ý sử dụng</h5>
                                                   <div class="is-divider"></div>
                                                   <p class="from_the_blog_excerpt ">Chế độ bảo hành, cách bảo dưỡng sofa và các dịch vụ sau bán hàng được áp dụng như sau: ...					</p>
                                                </div>
                                                <!-- .box-text-inner -->
                                             </div>
                                             <!-- .box-text -->
                                          </div>
                                          <!-- .box -->
                                       </a>
                                       <!-- .link -->
                                    </div>
                                    <!-- .col-inner -->
                                 </div>
                                 <!-- .col -->
                              </div>
                           </div>
                        </div>
                        <style scope="scope">
                        </style>
                     </div>
                  </div>
                  <!-- .section-content -->
                  <style scope="scope">
                     #section_55129917 {
                     padding-top: 20px;
                     padding-bottom: 20px;
                     background-color: rgb(241, 241, 242);
                     }
                  </style>
               </section>
               <section class="section dark has-mask mask-arrow" id="section_1147578108">
                  <div class="bg section-bg fill bg-fill  " >
                     <div class="section-bg-overlay absolute fill"></div>
                  </div>
                  <!-- .section-bg -->
                  <div class="section-content relative">
                     <div class="row row-collapse align-middle"  id="row-1104168848">
                        <div class="col medium-4 small-12 large-4"  data-animate="bounceInDown">
                           <div class="col-inner text-center"  >
                              <h3>ĐỂ ĐƯỢC TƯ VẤN VÀ HỖ TRỢ TỐT NHẤT</h3>
                              <p><strong>Hãy liên hệ với chúng tôi qua số điện thoại hoặc để lại lời nhắn</strong></p>
                              <a href="tel:0931050000" target="_self" class="button secondary"  >
                              <i class="icon-phone" ></i>  <span>Tel: 0931 05 000</span>
                              </a>
                              <a href="#yeucau" target="_self" class="button secondary is-outline"  >
                              <i class="icon-checkmark" ></i>  <span>Gửi yêu cầu</span>
                              </a>
                           </div>
                        </div>
                        <div class="col medium-4 small-12 large-4"  >
                           <div class="col-inner"  >
                              <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_2138128550">
                                 <div class="img-inner image-zoom image-cover dark" style="padding-top:250px;">
                                    <img width="1020" height="726" src="https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-1024x729.jpg" class="attachment-large size-large" alt="" srcset="https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-1024x729.jpg 1024w, https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-300x214.jpg 300w, https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-768x547.jpg 768w, https://zito.vn/wp-content/uploads/2017/12/noi-that-zito.jpg 1177w" sizes="(max-width: 1020px) 100vw, 1020px" />                
                                 </div>
                                 <style scope="scope">
                                 </style>
                              </div>
                           </div>
                        </div>
                        <div class="col medium-4 small-12 large-4"  >
                           <div class="col-inner text-center"  >
                              <div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>
                              <p></p>
                           </div>
                        </div>
                        <style scope="scope">
                        </style>
                     </div>
                  </div>
                  <!-- .section-content -->
                  <style scope="scope">
                     #section_1147578108 {
                     padding-top: 30px;
                     padding-bottom: 30px;
                     min-height: 350px;
                     }
                     #section_1147578108 .section-bg-overlay {
                     background-color: rgba(0, 0, 0, 0.59);
                     }
                     #section_1147578108 .section-bg.bg-loaded {
                     background-image: url(https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito.jpg);
                     }
                  </style>
               </section>
               <section class="section kh" id="section_841504452">
                  <div class="bg section-bg fill bg-fill  bg-loaded" >
                  </div>
                  <!-- .section-bg -->
                  <div class="section-content relative">
                     <div class="row align-middle align-center"  id="row-1221361185">
                        <div class="col small-12 large-12"  >
                           <div class="col-inner"  >
                              <h3 style="text-align: center;">KHÁCH HÀNG CỦA CHÚNG TÔI ĐANG SỐNG TẠI</h3>
                              <div class="slider-wrapper relative " id="slider-811337358" >
                                 <div class="slider slider-nav-simple slider-nav-large slider-nav-dark slider-nav-outside slider-style-normal"
                                    data-flickity-options='{
                                    "cellAlign": "center",
                                    "imagesLoaded": true,
                                    "lazyLoad": 1,
                                    "freeScroll": true,
                                    "wrapAround": true,
                                    "autoPlay": 1500,
                                    "pauseAutoPlayOnHover" : false,
                                    "prevNextButtons": true,
                                    "contain" : true,
                                    "adaptiveHeight" : true,
                                    "dragThreshold" : 5,
                                    "percentPosition": true,
                                    "pageDots": false,
                                    "rightToLeft": false,
                                    "draggable": true,
                                    "selectedAttraction": 0.1,
                                    "parallax" : 0,
                                    "friction": 0.6        }'
                                    >
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 228.89502762431px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/du-an-chung-cu-goldmark-city-136-ho-tung-mau-logo.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 315px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-ecolife-4.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 326.47058823529px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/eco-green-city.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 260px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-park-hill-premium.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 214.53781512605px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/Logo-Vinhomes-Gardenia.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 229.69811320755px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logox2.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 165.83756345178px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-hd-mon-city.jpg" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 204.0932642487px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-imperia-garden.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 273.47826086957px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-mandarin-garden-2.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 207.77777777778px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-hoang-cau-tan-hoang-minh.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 222px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/homcitytrungkinh12017522123919.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 138px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/180x200.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 321.70731707317px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-flc-36-pham-hung-1.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                    <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 252.22222222222px!important">
                                       <div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-tai-ho-guom-plaza.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div>
                                    </div>
                                 </div>
                                 <div class="loading-spin dark large centered"></div>
                                 <style scope="scope">
                                 </style>
                              </div>
                              <!-- .ux-slider-wrapper -->
                           </div>
                        </div>
                        <style scope="scope">
                           #row-1221361185 > .col > .col-inner {
                           padding: 30px 0px 0px 0px;
                           }
                        </style>
                     </div>
                  </div>
                  <!-- .section-content -->
                  <style scope="scope">
                     #section_841504452 {
                     padding-top: 0px;
                     padding-bottom: 0px;
                     background-color: rgb(255, 255, 255);
                     }
                  </style>
               </section>
               <section class="section sec0f dark" id="section_216691949">
                  <div class="bg section-bg fill bg-fill  bg-loaded" >
                  </div>
                  <!-- .section-bg -->
                  <div class="section-content relative">
                     <div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>
                     <div class="row ic"  id="row-1992458976">
                        <style type="text/css" media="screen">
                        .last-reset h3
                           {
                              font-size: 17px;
                           }
                           .icon-box-img .icon i{
                               font-size: 60px;
                               color: #ffcb44;
                           }                                           
                           i.fa-truck {
                               -webkit-transform: rotateY(180deg);
                               -moz-transform: rotateY(180deg);
                               -o-transform: rotateY(180deg);
                               -ms-transform: rotateY(180deg);
                               transform: rotateY(180deg);
                           }   
                           </style>
                        <div class="col medium-4 small-12 large-4"  >
                           <div class="col-inner"  >
                              <div class="icon-box featured-box icon-box-left text-left"  >
                                 <div class="icon-box-img" style="width: 60px">
                                    <div class="icon">
                                      <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    </div>
                                 </div>
                                 <div class="icon-box-text last-reset">
                                    <h3><span style="color: #ffcb44;">Đảm bảo chất lượng</span></h3>
                                    <p>Chế bảo hành tới 02 năm</p>
                                 </div>
                              </div>
                              <!-- .icon-box -->
                           </div>
                        </div>
                        <div class="col medium-4 small-12 large-4"  >
                           <div class="col-inner"  >
                              <div class="icon-box featured-box icon-box-left text-left"  >
                                 <div class="icon-box-img" style="width: 60px">
                                    <div class="icon">

                                       <i class="fa fa-truck" aria-hidden="true"></i>

                                    </div>
                                 </div>
                                 <div class="icon-box-text last-reset">
                                    <h3><span style="color: #ffcb44;">GIAO HÀNG NHANH CHÓNG</span></h3>
                                    <p>Miễn phí giao hàng nội thành</p>
                                 </div>
                              </div>
                              <!-- .icon-box -->
                           </div>
                        </div>
                        <div class="col medium-4 small-12 large-4"  >
                           <div class="col-inner"  >
                              <div class="icon-box featured-box icon-box-left text-left"  >
                                 <div class="icon-box-img" style="width: 60px">
                                    <div class="icon">
                                       <i class="fa fa-phone" aria-hidden="true"></i>
                                    </div>
                                 </div>
                                 <div class="icon-box-text last-reset">
                                    <h3><span style="color: #ffcb44;">Tư vấn - Hỗ trợ 24/7</span></h3>
                                    <p>Liên hệ để được tư vấn miễn phí </p>
                                 </div>
                              </div>
                              <!-- .icon-box -->
                           </div>
                        </div>
                        <style scope="scope">
                        </style>
                     </div>
                     <div class="row row-small"  id="row-1161105673">
                        <div class="col col4 col41 medium-3 small-12 large-3"  >
                           <div class="col-inner"  >
                              <p style="text-align: justify;"><strong>Nội thất ZITO</strong> mong muốn đem đến những trải nghiệm cuộc sống tuyệt vời nhất cho bạn và gia đình. Phục vụ theo nhu cầu cho từng đối tượng khách hàng về thiết kế, lựa chọn kích thước, chất liệu, màu sắc với mức giá thành tốt nhất thị trường. Đảm bảo về quy trình sản xuất gia công chuẩn chỉ, chất lượng.</p>
                              <strong>Công ty Cổ phần Nội thất ZITO</strong></br>
                              <strong>Showroom:</strong> 15 Dương Đình Nghệ, Yên Hoà, Cầu Giấy, Hà Nội</br>
                              <strong>Tel:</strong> (024) 2212 4111 / 0931.05.0000</br>
                              <strong>Email:</strong> zitosofa@gmail.com</br>
                              <strong>Số ĐKKD:</strong> 0107889261 - Cấp bởi Sở kế Hoạch Đầu Tư Thành phố Hà Nội
                           </div>
                        </div>
                        <div class="col col4 col41 medium-2 small-12 large-2"  >
                           <div class="col-inner"  >
                              <h5 class="uppercase">Liên kết nhanh</h5>
                              <div class="menu-main-menu-nz-container">
                                 <ul id="menu-main-menu-nz-1" class="menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-3056 current_page_item menu-item-3944"><a href="https://zito.vn/">TRANG CHỦ</a></li>
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4098"><a href="https://zito.vn/danh-muc/sofa-go/">SOFA GỖ</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4667"><a href="https://zito.vn/danh-muc/ban-tra/">BÀN TRÀ</a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4749"><a href="https://zito.vn/danh-muc/goi-tua/">GỐI TỰA</a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3943">
                                       <a href="https://zito.vn/shop/">CỬA HÀNG</a>
                                       <ul  class="sub-menu">
                                          <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4097"><a href="https://zito.vn/danh-muc/sofa-da/">SOFA DA</a></li>
                                          <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4100"><a href="https://zito.vn/danh-muc/sofa-ni/">SOFA NỈ</a></li>
                                       </ul>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4698"><a href="https://zito.vn/khuyen-mai/">KHUYẾN MÃI</a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="col col4 col41 medium-3 small-12 large-3"  >
                           <div class="col-inner"  >
                              <h5 class="uppercase" style="text-align: center;">Đăng ký nhận khuyến mãi</h5>
                              <div role="form" class="wpcf7" id="wpcf7-f4110-o1" lang="vi" dir="ltr">
                                 <div class="screen-reader-response"></div>
                                 <form action="/#wpcf7-f4110-o1" method="post" class="wpcf7-form demo" novalidate="novalidate">
                                    <div style="display: none;">
                                       <input type="hidden" name="_wpcf7" value="4110" />
                                       <input type="hidden" name="_wpcf7_version" value="4.9.1" />
                                       <input type="hidden" name="_wpcf7_locale" value="vi" />
                                       <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4110-o1" />
                                       <input type="hidden" name="_wpcf7_container_post" value="0" />
                                    </div>
                                    <div class="nz-cf7 dkemail">
                                       <div class="nz-cf7-f1">
                                          <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name" aria-required="true" aria-invalid="false" placeholder="Tên quý khách" /></span>
                                       </div>
                                       <div class="nz-cf7-f1">
                                          <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span>
                                       </div>
                                       <div class="nz-cf7-f1">
                                          <input type="submit" value="Đăng ký" class="wpcf7-form-control wpcf7-submit c-button" />
                                       </div>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                 </form>
                              </div>
                              <div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>
                              <h5 class="uppercase" style="text-align: center;">Chia sẻ Websitei</h5>



                              <div class="social-icons share-icons share-row relative icon-style-fill full-width text-center" >
                                  <a href="//www.facebook.com/sharer.php?u=https://zito.vn/sofa-phong-khach/" data-label="Facebook" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip facebook tooltipstered"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                   <a href="//twitter.com/share?url=https://zito.vn/sofa-phong-khach/" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip twitter tooltipstered">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                 </a>
                 <a href="" rel="nofollow" class="icon primary button circle tooltip email tooltipstered">
                  <i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                  <a href="" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip pinterest tooltipstered"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
                

                  <a href="//plus.google.com/share?url=https://zito.vn/sofa-phong-khach/" target="_blank" class="icon primary button circle tooltip google-plus tooltipstered" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow"><i class="fa fa-google-plus" aria-hidden="true"></i></a>




                  <a href="//www.linkedin.com/shareArticle?mini=true&amp;url=" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip linkedin tooltipstered"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                              </div>



                              <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1693504053">
                                 <a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=38434" target="_self" class="">
                                    <div class="img-inner dark" style="margin:0px 15px 10px 15px;">
                                       <img width="395" height="149" src="https://zito.vn/wp-content/uploads/2017/12/da-thong-bao-bo-cong-thuong.png" class="attachment-large size-large" alt="" srcset="https://zito.vn/wp-content/uploads/2017/12/da-thong-bao-bo-cong-thuong.png 395w, https://zito.vn/wp-content/uploads/2017/12/da-thong-bao-bo-cong-thuong-300x113.png 300w" sizes="(max-width: 395px) 100vw, 395px" />                
                                    </div>
                                 </a>
                                 <style scope="scope">
                                 </style>
                              </div>
                           </div>
                        </div>
                        <div class="col col4 col41 medium-4 small-12 large-4"  >
                           <div class="col-inner"  >
                              <div class="fb-page" data-href="https://www.facebook.com/zito.vn/" data-tabs="timeline" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false">
                                 <blockquote class="fb-xfbml-parse-ignore" cite="https://www.facebook.com/zito.vn/" rel="nofollow">
                                    <p><a href="https://www.facebook.com/zito.vn/" rel="nofollow">Nội Thất ZITO</a></p>
                                 </blockquote>
                              </div>
                           </div>
                        </div>
                        <style scope="scope">
                        </style>
                     </div>
                     <div class="type nz-button-2">
                        <div><a href="tel:0931050000" class="nz-bt nz-bt-2"><span class="txt">0931.05.0000</span><span class="round"><i class="fa fa-phone faa-ring faa-slow animated"></i></span></a></div>
                        <style>
                           .type.nz-button-2 {display: block;position: fixed;left: 20px;bottom: 5%;text-align: center;z-index: 69696969;}
                           .type.nz-button-2 > div {margin: 5px auto;}
                           .nz-button-2 .nz-bt-1 {background-color: #C69a66;}
                           .nz-button-2 .nz-bt-1 .round {background-color: #fdcc30;}
                           .nz-button-2 .nz-bt-2 {background-color: #C69a66;}
                           .nz-button-2 .nz-bt-2 .round {background-color: #fdcc30;}
                           .nz-button-2 a { line-height: 16px; text-decoration: none;-moz-border-radius: 40px;-webkit-border-radius: 30px;border-radius: 40px;padding: 12px 53px 12px 23px;color: #fff;text-transform: uppercase;font-family: sans-serif;font-weight: bold;position: relative;-moz-transition: all 0.3s;-o-transition: all 0.3s;-webkit-transition: all 0.3s;transition: all 0.3s;display: inline-block;}
                           .nz-button-2 a span {position: relative;z-index: 3;}
                           .nz-button-2 a .round {-moz-border-radius: 50%;-webkit-border-radius: 50%;border-radius: 50%;width: 38px;height: 38px;position: absolute;right: 3px;top: 3px;-moz-transition: all 0.3s ease-out;-o-transition: all 0.3s ease-out;-webkit-transition: all 0.3s ease-out;transition: all 0.3s ease-out;z-index: 2;}
                           .nz-button-2 a .round i {position: absolute;top: 50%;margin-top: -6px;left: 50%;margin-left: -4px;-moz-transition: all 0.3s;-o-transition: all 0.3s;-webkit-transition: all 0.3s;transition: all 0.3s;}
                           .nz-button-2 .txt {font-size: 14px;line-height: 1.45;}
                           .nz-button-2.type a:hover {padding-left: 48px;padding-right: 28px;}
                           .nz-button-2.type a:hover .round {width: calc(100% - 6px);-moz-border-radius: 30px;-webkit-border-radius: 30px;border-radius: 30px;}
                           .nz-button-2.type a:hover .round i {left: 12%;}
                           #dktv form.wpcf7-form {max-width: 70%;margin: 0 auto;}
                           div#dktv .col.small-12.large-12, div#dktv .col.medium-6.small-12.large-6 {padding: 0;}
                           #dktv .nz-tensp {color: #fff;border: 0;}
                           div#dktv > div {margin: 0;}
                           div#dktv > div {background: url(/wp-content/uploads/2017/08/bg-popup.png);}
                           .bn-dktv {background: url(/wp-content/uploads/2017/08/bg-popup.jpg);background-repeat: no-repeat;background-size: cover;}</p>
                           <p>/***************MOBILE ***************/
                              @media only screen and (max-width: 48em) {
                              .nz-button-2 a {padding: 12px 53px 12px 10px;}
                              .type.nz-button-2 > div {margin: 0px auto;display: table-cell;}
                              .type.nz-button-2 {left: 0px;bottom: 0;text-align: left;margin: 0;}
                              }
                        </style>
                     </div>
                     <!-- .section-content -->
                     <style scope="scope">
                        #section_216691949 {
                        padding-top: 0px;
                        padding-bottom: 0px;
                        background-color: rgb(35, 35, 35);
                        }
                     </style>
               </section>
               <div class="absolute-footer dark medium-text-center text-center">
               <div class="container clearfix">
               <div class="footer-primary pull-left">
               <div class="copyright-footer">
               <span style="color: #ddd;">Copyright 2018 &copy; Bản quyền thuộc về <strong>ZITO</strong> </span>
               <div id="yeucau"
                  class="lightbox-by-id lightbox-content mfp-hide lightbox-white "
                  style="max-width:80% ;padding:20px">
               <section class="section dark" id="section_1217710058">
               <div class="bg section-bg fill bg-fill  " >
               <div class="section-bg-overlay absolute fill"></div>
               </div><!-- .section-bg -->
               <div class="section-content relative">
               <div class="row align-center sectuvan" style="max-width:870px" id="row-1423119082">
               <div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >
               <p class="lead" style="text-align: center;"><span style="font-size: 95%;">Nếu quý khách có bất kỳ thắc mắc nào có thể liên hệ với chúng tôi qua thông tin bên dưới hoặc để lại lời nhắn vào hộp thoại bên canh</span></p>
               <strong>Công ty Cổ phần Nội thất ZITO</strong></br>
               <strong>Showroom:</strong> 15 Dương Đình Nghệ, Yên Hoà, Cầu Giấy, Hà Nội</br>
               <strong>Tel:</strong> (024) 2212 4111 / 0931.05.0000</br>
               <strong>Email:</strong> zitosofa@gmail.com</br>
               <strong>Số ĐKKD:</strong> 0107889261 - Cấp bởi Sở kế Hoạch Đầu Tư Thành phố Hà Nội
               </div></div>
               <div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >
               <p><center></p>
               <h3>Hãy để lại lời nhắn<br />Để chúng tôi có thể tư vấn cho bạn</h3>
               <p></center></p>
               <div role="form" class="wpcf7" id="wpcf7-f3055-o2" lang="en-US" dir="ltr">
               <div class="screen-reader-response"></div>
               <form action="/#wpcf7-f3055-o2" method="post" class="wpcf7-form demo" novalidate="novalidate">
               <div style="display: none;">
               <input type="hidden" name="_wpcf7" value="3055" />
               <input type="hidden" name="_wpcf7_version" value="4.9.1" />
               <input type="hidden" name="_wpcf7_locale" value="en_US" />
               <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3055-o2" />
               <input type="hidden" name="_wpcf7_container_post" value="0" />
               </div>
               <div class="nz-cf7">
               <div class="nz-cf7-f1 nz-1-3">
               <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name" aria-required="true" aria-invalid="false" placeholder="Tên quý khách" /></span>
               </div>
               <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
               <span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="your-phone" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span>
               </div>
               <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
               <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span>
               </div>
               <div class="nz-cf7-f1">
               <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" id="your-message" aria-required="true" aria-invalid="false" placeholder="Lời nhắn thêm (có thể để trống)"></textarea></span>
               </div>
               <div class="nz-cf7-f1">
               <input type="submit" value="Gửi tin yêu cầu tư vấn" class="wpcf7-form-control wpcf7-submit c-button" />
               </div>
               </div>
               <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
               </div></div>
               <style scope="scope">
               </style>
               </div>
               </div><!-- .section-content -->
               <style scope="scope">
               #section_1217710058 {
               padding-top: 0px;
               padding-bottom: 0px;
               }
               #section_1217710058 .section-bg-overlay {
               background-color: rgba(0, 0, 0, 0.7);
               }
               #section_1217710058 .section-bg.bg-loaded {
               background-image: 3272;
               }
               </style>
               </section>
               </div>
               </div>
               </div><!-- .left -->
               </div><!-- .container -->
               </div><!-- .absolute-footer -->
               <a href="#top" class="back-to-top button invert plain is-outline hide-for-medium icon circle fixed bottom z-1" id="top-link"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
            </footer>