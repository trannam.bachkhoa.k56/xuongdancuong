<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js">
   <!--<![endif]-->
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <title>Nội Thất ZITO | Showroom Nội Thất Gỗ Cao Cấp Tại Hà Nội</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Nội thất ZITO tiên phong cung cấp các sản phẩm Sofa gỗ, da, nỉ theo kích thước tùy chỉnh, xu hướng mới nhất Showroom số 15 Dương Đình Nghệ Cầu Giấy Hà Nội"/>
      <meta name="robots" content="noodp"/>
     <base href="http://vn3c.net/zito/">
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      
      <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' type='text/css' media='all' />
      <link rel='stylesheet' id='yith_wcas_frontend-css'  href='css/yith_wcas_ajax_search.css' type='text/css' media='all' />
      <style id='yith_wcas_frontend-inline-css' type='text/css'>
         .autocomplete-suggestion{
         padding-right: 20px;
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
         .autocomplete-suggestion  span.yith_wcas_result_on_sale{
         background: #7eb742;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
         .autocomplete-suggestion  span.yith_wcas_result_outofstock{
         background: #7a7a7a;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
         .autocomplete-suggestion  span.yith_wcas_result_featured{
         background: #c0392b;
         color: #ffffff
         }
         .autocomplete-suggestion img{
         width: 50px;
         }
         .autocomplete-suggestion .yith_wcas_result_content .title{
         color: #004b91;
         }
         .autocomplete-suggestion{
         min-height: 60px;
         }
      </style>
      <link rel='stylesheet' id='flatsome-main-css'  href='css/flatsome.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-shop-css'  href='css/flatsome-shop.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-style-css'  href='css/style1.css' type='text/css' media='all' />

      <link rel="stylesheet" href="css/font-awesome.min.css">

      <script type='text/javascript' src='js/jquery.js'></script>
      <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
      <script type='text/javascript' src='js/advanced-cf7-db-public.js'></script>
      <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>
   </head>
   <body>
   </body>
   <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
   <noscript>
      <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
   </noscript>
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
   <link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
   <meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
   <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>
   </head>
   <body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
      <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
      <div id="wrapper">
         <?php include('header/header.php')?>
         <main id="main" class="">


<div class="row page-wrapper">
<div id="content" class="large-12 col" role="main">

                     <header class="entry-header text-center">
                  <h1 class="entry-title">Giới thiệu</h1>
                  <div class="is-divider medium"></div>
               </header>

               <div class="entry-content">
                  <p style="text-align: justify;">Công ty Cổ phần Nội thất ZITO thành lập từ tháng 7/2017 với thương hiệu Nội thất ZITO. Thương hiệu Nội thất hiện đại có những hoạt động tích cực trong lĩnh vực thiết kế và thi công. Hướng đến những sản phẩm theo phong cách hiện đại, chất liệu <a href="http://tanhoangminhhomes.vn">cao cấp</a>, giá cả hợp lý. Với hy vọng đem đến Quý khách hàng những món quà nghệ thuật chứ không đơn giản chỉ là một bộ sofa, bàn trà hay kệ ti vi…</p>
<p><a href="https://zito.vn/">Nội thất ZITO</a> không giới hạn về đối tượng khách hàng cụ thể. Bất kỳ ai cũng có cơ hội sở hữu những sản phẩm đạt chuẩn về chất lượng mà ZITO cung cấp.</p>
<figure id="attachment_4613" style="width: 961px" class="wp-caption aligncenter"><img class="wp-image-4613 size-full lazy-load-active" src="https://zito.vn/wp-content/uploads/2017/10/aaaa.jpg" data-src="https://zito.vn/wp-content/uploads/2017/10/aaaa.jpg" alt="Thương Hiệu Nội Thất ZITO" width="961" height="639" srcset="https://zito.vn/wp-content/uploads/2017/10/aaaa.jpg 961w, https://zito.vn/wp-content/uploads/2017/10/aaaa-300x199.jpg 300w, https://zito.vn/wp-content/uploads/2017/10/aaaa-768x511.jpg 768w" data-srcset="https://zito.vn/wp-content/uploads/2017/10/aaaa.jpg 961w, https://zito.vn/wp-content/uploads/2017/10/aaaa-300x199.jpg 300w, https://zito.vn/wp-content/uploads/2017/10/aaaa-768x511.jpg 768w" sizes="(max-width: 961px) 100vw, 961px"><figcaption class="wp-caption-text"><i>Showroom nội thất ZITO tại số 15 Dương Đình Nghệ, Cầu Giấy, Hà Nội</i></figcaption></figure>
<p style="text-align: justify;">Với sự chuyên nghiệp, tận tâm và chỉn chu trong chất lượng sản phẩm &amp; quy trình làm việc,&nbsp;Nội thất ZITO đang dần khẳng định được vị thế và chỗ đứng trên thị trường. Trở thành là lựa chọn của rất nhiều khách hàng đến từ:. Vinhomes&nbsp;Gardenia , Imperia Garden, Home City, Flc Complex, Eco Green City,…</p>
<h3><span style="font-size: 100%;"><strong>Đến với Thương Hiệu Nội Thất ZITO để được hưởng những Đặc quyền sau:</strong></span></h3>
<p>– Sản phẩm <a href="https://zito.vn/danh-muc/sofa-go/">sofa</a>, bàn trà, kệ ti vi cam kết về chất lượng, xuất xứ vật liệu rõ ràng, thiết kế cập nhật xu hướng.<br>
– Thiết kế theo yêu cầu, tùy chỉnh về chất liệu, kiểu dáng, màu sắc.<br>
– Miễn phí vận chuyển nội thành Hà Nội. Quý khách ở xa chúng tôi sẽ hỗ trợ vận chuyển.<br>
– Chính sách bảo hành 24 tháng.</p>
<p>Mọi thắc mắc quý khách hàng có thể liên hệ với chúng tôi hoặc trực tiếp đến showroom tại địa chỉ : Số 15 Dương Đình Nghệ – Cầu Giấy – Hà Nội để có được những trải nghiệm chân thực nhất về sản phẩm.</p>

                                 </div>
               

      

</div><!-- #content -->
</div><!-- .row -->


</main>
 <?php include('footer/footer.php')?>
         <!-- .footer-wrapper -->
      </div>
      <!-- #wrapper -->
      <!-- Mobile Sidebar -->
      <?php include('header/header-mobile.php')?>
      <!-- #mobile-menu -->
      <div id="login-form-popup" class="lightbox-content mfp-hide">
         <div class="account-container lightbox-inner">
            <div class="account-login-inner">
               <h3 class="uppercase">Đăng nhập</h3>
               <form method="post" class="login">
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                     <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                  </p>
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="password">Mật khẩu <span class="required">*</span></label>
                     <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                  </p>
                  <p class="form-row">
                     <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />            <input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                     <label for="rememberme" class="inline">
                     <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu          </label>
                  </p>
                  <p class="woocommerce-LostPassword lost_password">
                     <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                  </p>
               </form>
            </div>
            <!-- .login-inner -->
         </div>
         <!-- .account-login-container -->
      </div>
      <link rel='stylesheet' id='flatsome-effects-css'  href='css/effects.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/scripts.js'></script>
      <script type='text/javascript' src='js/add-to-cart.min.js'></script>
      <script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
      <script type='text/javascript' src='js/js.cookie.min.js'></script>     
      <script type='text/javascript' src='js/woocommerce.min.js'></script>
      <script type='text/javascript' src='js/cart-fragments.min.js'></script>
      <script type='text/javascript' src='js/yith-autocomplete.min.js'></script>
      <script type='text/javascript' src='js/flatsome-live-search.js'></script>
      <script type='text/javascript' src='js/hoverIntent.min.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
            /* ]]> */
            
      </script>
      <script type='text/javascript' src='js/flatsome.js'></script>
      <script type='text/javascript' src='js/flatsome-lazy-load.js'></script>
      <script type='text/javascript' src='js/woocommerce.js'></script>
      <script type='text/javascript' src='js/wp-embed.min.js'></script>
      <script type='text/javascript' src='js/packery.pkgd.min.js'></script>
      <script type='text/javascript' src='js/zxcvbn-async.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
   </body>
</html>