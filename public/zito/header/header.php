<header id="header" class="header has-sticky sticky-jump" style="">
   <div class="header-wrapper">
  <div id="top-bar" class="header-top hide-for-sticky">
    <div class="flex-row container">
      <div class="flex-col hide-for-medium flex-left">
          <ul class="nav nav-left medium-nav-center nav-small  nav-">
              <li class="html custom html_topbar_left"><a href="tel:0931050000"><i class="fa fa-phone" aria-hidden="true" style="margin-right: 5px;"></i>  Hotline 24/7: 0931.05.0000</a></li><li class="html header-social-icons ml-0">
  <div class="social-icons follow-icons "><a href="#" target="_blank" data-label="Facebook" rel="nofollow" class="icon plain facebook tooltip tooltipstered"><i class="icon-facebook"></i></a><a href="#" target="_blank" data-label="Twitter" rel="nofollow" class="icon plain  twitter tooltip tooltipstered"><i class="icon-twitter"></i></a><a href="#" target="_blank" rel="nofollow" data-label="Pinterest" class="icon plain  pinterest tooltip tooltipstered"><i class="icon-pinterest"></i></a><a href="#" target="_blank" rel="nofollow" data-label="YouTube" class="icon plain  youtube tooltip tooltipstered"><i class="icon-youtube"></i></a></div></li>          </ul>
      </div><!-- flex-col left -->

      <div class="flex-col hide-for-medium flex-center">
          <ul class="nav nav-center nav-small  nav-">
                        </ul>
      </div><!-- center -->

      <div class="flex-col hide-for-medium flex-right">
         <ul class="nav top-bar-nav nav-right nav-small  nav-">
              <li id="menu-item-4082" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-4082"><a href="https://zito.vn/gioi-thieu-zito/" class="nav-top-link">Giới thiệu</a></li>
<li id="menu-item-4636" class="menu-item menu-item-type-taxonomy menu-item-object-category  menu-item-4636"><a href="https://zito.vn/danh-cho-khach-hang/" class="nav-top-link">Hướng dẫn – Hỗ trợ</a></li>
<li id="menu-item-4310" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-4310"><a href="https://zito.vn/tin-tuc/" class="nav-top-link">Tin tức</a></li>
<li id="menu-item-4081" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-4081"><a href="https://zito.vn/lien-he/" class="nav-top-link">Liên hệ</a></li>
<li class="cart-item has-icon
">


<a href="https://zito.vn/gio-hang/" class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup" data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">

  
<span class="header-cart-title">
   Giỏ hàng     </span>

      <span class="cart-icon image-icon">
    <strong>2</strong>
  </span> 
  </a>



  <!-- Cart Sidebar Popup -->
  <div id="cart-popup" class="widget_shopping_cart mfp-hide">
  <div class="cart-popup-inner inner-padding">
      <div class="cart-popup-title text-center">
          <h4 class="uppercase">Giỏ hàng</h4>
          <div class="is-divider"></div>
      </div>
      <div class="widget_shopping_cart_content">

<ul class="cart_list product_list_widget ">

  
    
              <li class="mini_cart_item">
            <a href="https://zito.vn/gio-hang/?remove_item=a0205b87490c847182672e8d371e9948&amp;_wpnonce=c5e0ebb11a" class="remove" aria-label="Xóa sản phẩm này" data-product_id="4742" data-product_sku="">×</a>                          <a href="https://zito.vn/shop/goi-tua/zp-meo-03/">
                <img width="130" height="80" src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Gối tựa sofa" srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px">Gối ZP Mèo 03&nbsp;             </a>
                        
            <span class="quantity">1 × <span class="woocommerce-Price-amount amount">220.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span></span>         </li>
                    <li class="mini_cart_item">
            <a href="https://zito.vn/gio-hang/?remove_item=8f4576ad85410442a74ee3a7683757b3&amp;_wpnonce=c5e0ebb11a" class="remove" aria-label="Xóa sản phẩm này" data-product_id="4648" data-product_sku="">×</a>                          <a href="https://zito.vn/shop/ban-tra/zb-007/">
                <img width="130" height="80" src="//zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg" class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="bàn trà gỗ vuông tại hà nội" srcset="//zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px">Bàn Trà ZITO ZB 007&nbsp;              </a>
                        
            <span class="quantity">1 × <span class="woocommerce-Price-amount amount">5.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span></span>         </li>
          
    
  
</ul><!-- end product list -->


  <p class="total"><strong>Tổng phụ:</strong> <span class="woocommerce-Price-amount amount">5.220.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span></p>

  
  <p class="buttons">
    <a href="https://zito.vn/gio-hang/" class="button wc-forward">Xem giỏ hàng</a><a href="https://zito.vn/thanh-toan/" class="button checkout wc-forward">Thanh toán</a> </p>


</div>
            <div class="cart-sidebar-content relative"></div>  </div>
  </div>

</li>
          </ul>
      </div><!-- .flex-col right -->

            <div class="flex-col show-for-medium flex-grow">
          <ul class="nav nav-center nav-small mobile-nav  nav-">
              <li class="html custom html_topbar_left"><a href="tel:0931050000"><i class="fa fa-phone" aria-hidden="true" style="margin-right: 5px;"></i>  Hotline 24/7: 0931.05.0000</a></li>          </ul>
      </div>
      
    </div><!-- .flex-row -->
</div><!-- #header-top -->
<div id="masthead" class="header-main has-sticky-logo">
      <div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">

          <!-- Logo -->
          <div id="logo" class="flex-col logo">
            <!-- Header logo -->
<a href="https://zito.vn/" title="Siêu Thị Nội thất ZITO - Trải Nghiệm Sự Tiện Nghi" rel="home">
    <img width="241" height="116" src="https://zito.vn/wp-content/uploads/2017/11/logo-zito-.png" class="header-logo-sticky" alt="Siêu Thị Nội thất ZITO"><img width="241" height="116" src="https://zito.vn/wp-content/uploads/2017/11/logo-zito-.png" class="header_logo header-logo" alt="Siêu Thị Nội thất ZITO"><img width="241" height="116" src="https://zito.vn/wp-content/uploads/2017/11/logo-zito-.png" class="header-logo-dark" alt="Siêu Thị Nội thất ZITO"></a>
          </div>

          <!-- Mobile Left Elements -->
          <div class="flex-col show-for-medium flex-left">
            <ul class="mobile-nav nav nav-left ">
              <li class="nav-icon has-icon">
  <div class="header-button">   <a href="#" data-open="#main-menu" data-pos="left" data-bg="main-menu-overlay" data-color="dark" class="icon primary button round is-small" aria-controls="main-menu" aria-expanded="false">
    
      <i class="fa fa-bars" aria-hidden="true"></i>

      
          </a>
   </div> </li>            </ul>
          </div>

          <!-- Left Elements -->
          <div class="flex-col hide-for-medium flex-left
            flex-grow">
            <ul class="header-nav header-nav-main nav nav-left  nav-box nav-size-medium nav-spacing-large nav-uppercase">
                          </ul>
          </div>

          <!-- Right Elements -->
          <div class="flex-col hide-for-medium flex-right">
            <ul class="header-nav header-nav-main nav nav-right  nav-box nav-size-medium nav-spacing-large nav-uppercase">
              <li id="menu-item-3944" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home  menu-item-3944"><a href="https://zito.vn/" class="nav-top-link">TRANG CHỦ</a></li>
<li id="menu-item-4098" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat  menu-item-4098"><a href="https://zito.vn/danh-muc/sofa-go/" class="nav-top-link">SOFA GỖ</a></li>
<li id="menu-item-4667" class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-4667"><a href="https://zito.vn/danh-muc/ban-tra/" class="nav-top-link">BÀN TRÀ</a></li>
<li id="menu-item-4749" class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-4749"><a href="https://zito.vn/danh-muc/goi-tua/" class="nav-top-link">GỐI TỰA</a></li>
<li id="menu-item-3943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  menu-item-3943 has-dropdown"><a href="https://zito.vn/shop/" class="nav-top-link">CỬA HÀNG<i class="icon-angle-down"></i></a>
<ul class="nav-dropdown nav-dropdown-default">
  <li id="menu-item-4097" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat  menu-item-4097"><a href="https://zito.vn/danh-muc/sofa-da/">SOFA DA</a></li>
  <li id="menu-item-4100" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat  menu-item-4100"><a href="https://zito.vn/danh-muc/sofa-ni/">SOFA NỈ</a></li>
</ul>
</li>
<li id="menu-item-4698" class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-4698"><a href="https://zito.vn/khuyen-mai/" class="nav-top-link">KHUYẾN MÃI</a></li>
            </ul>
          </div>

          <!-- Mobile Right Elements -->
          <div class="flex-col show-for-medium flex-right">
            <ul class="mobile-nav nav nav-right ">
              <li class="cart-item has-icon">


<a href="https://zito.vn/gio-hang/" class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup" data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">

      <span class="cart-icon image-icon">
    <strong>2</strong>
  </span> 
  </a>

</li>
            </ul>
          </div>

      </div><!-- .header-inner -->
     
            <!-- Header divider -->
      <div class="container"><div class="top-divider full-width"></div></div>
      </div><!-- .header-main -->
<div class="header-bg-container fill"><div class="header-bg-image fill"></div><div class="header-bg-color fill"></div></div><!-- .header-bg-container -->   </div><!-- header-wrapper-->
</header>