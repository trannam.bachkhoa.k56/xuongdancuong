<div id="main-menu" class="mobile-sidebar no-scrollbar mfp-hide">
               <div class="sidebar-menu no-scrollbar ">
                  <ul class="nav nav-sidebar  nav-vertical nav-uppercase">
                     <li class="header-search-form search-form html relative has-icon">
                        <div class="header-search-form-wrapper">
                           <div class="searchform-wrapper ux-search-box relative form-flat is-normal">
                              <form method="get" class="searchform" action="https://zito.vn/" role="search">
                                 <div class="flex-row relative">
                                    <div class="flex-col flex-grow">
                                       <input type="search" class="search-field mb-0" name="s" value="" placeholder="Tìm kiếm..." />
                                       <input type="hidden" name="post_type" value="product" />
                                    </div>
                                    <!-- .flex-col -->
                                    <div class="flex-col">
                                       <button type="submit" class="ux-search-submit submit-button secondary button icon mb-0">
                                       <i class="icon-search" ></i>				</button>
                                    </div>
                                    <!-- .flex-col -->
                                 </div>
                                 <!-- .flex-row -->
                                 <div class="live-search-results text-left z-top"></div>
                              </form>
                           </div>
                        </div>
                     </li>
                     <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-3056 current_page_item menu-item-3944"><a href="https://zito.vn/" class="nav-top-link">TRANG CHỦ</a></li>
                     <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4098"><a href="https://zito.vn/danh-muc/sofa-go/" class="nav-top-link">SOFA GỖ</a></li>
                     <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4667"><a href="https://zito.vn/danh-muc/ban-tra/" class="nav-top-link">BÀN TRÀ</a></li>
                     <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4749"><a href="https://zito.vn/danh-muc/goi-tua/" class="nav-top-link">GỐI TỰA</a></li>
                     <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3943">
                        <a href="https://zito.vn/shop/" class="nav-top-link">CỬA HÀNG</a>
                        <ul class=children>
                           <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4097"><a href="https://zito.vn/danh-muc/sofa-da/">SOFA DA</a></li>
                           <li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4100"><a href="https://zito.vn/danh-muc/sofa-ni/">SOFA NỈ</a></li>
                        </ul>
                     </li>
                     <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4698"><a href="https://zito.vn/khuyen-mai/" class="nav-top-link">KHUYẾN MÃI</a></li>
                     <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4082"><a href="https://zito.vn/gioi-thieu-zito/" class="nav-top-link">Giới thiệu</a></li>
                     <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-4636"><a href="https://zito.vn/danh-cho-khach-hang/" class="nav-top-link">Hướng dẫn – Hỗ trợ</a></li>
                     <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4310"><a href="https://zito.vn/tin-tuc/" class="nav-top-link">Tin tức</a></li>
                     <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4081"><a href="https://zito.vn/lien-he/" class="nav-top-link">Liên hệ</a></li>
                  </ul>
               </div>
               <!-- inner -->
            </div>