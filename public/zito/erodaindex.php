<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/bootstrap.css">
     <link rel="stylesheet" href="css/font-awesome.css">
   <link rel="stylesheet" href="css/eroda.css">

    <script src="js/jquery3-3.js"></script>
     <script src="js/bootstrap.bundle.js"></script>
      <script src="js/bootstrap.js"></script>
   <style type="text/css" media="screen">
    .color:hover
    {
         color: #ff0000 !important;
         text-decoration: underline;
    }  
   </style>
</head>
<body>
   <header id="header" class="">
      <div class="headertop">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12">
               
                  <ul>
                     <li><a href="#" title="" class="color">Lịch sử phát triển  </a></li>

                     <li><a href="#" title="" class="color">Tầm nhìn, sứ mệnh  </a></li>
                     <li><a href="#" title="" class="color">Tuyển Dụng   </a></li>
                     <li><a href="#" title="" class="color">Góc báo Chí   </a></li>
                     <li><a href="#" title="" class="color">Tin mới nhất   </a></li>
                     <li><a href="#" title="" class="color">Lịch sử phát triển</a></li>
                  </ul> 
               </div>
            </div>
         </div>
         
      </div>
      <div class="headercontent">
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-3 col-sm-12">
                  <div class="logo">
                     <img src="img/logo.jpg" alt="" class="">

                     <div class="hotline hiddenPc showMb">
                     <img src="img/i11.gif" alt="">
                     <span><span class="hiddenTab">Hotline : </span><strong> 0976.529.529</strong></span>
                  </div>
                  </div>
                  
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="search">
                     <form action="" method="get" accept-charset="utf-8">
                        <input type="text" name="" value="" placeholder="">
                        <button type=""><i class="fa fa-search" aria-hidden="true"></i></button>
                     </form>
                  </div>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-12">
                  <div class="hotline hiddenMb">
                     <img src="img/i11.gif" alt="">
                     <span><span class="hiddenTab">Hotline : </span><strong> 0976.529.529</strong></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="menu">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12">
                   <nav class="navbar navbar-expand-lg navbar-light bg-light">
           
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                          <a class="nav-link" href="#">Trang chủ <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ghế sofa
                          </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            
                        </li>
                        
                      </ul>
                   
                    </div>
                  </nav>
                  
               </div>
            </div>
         </div>
        
      </div>
   </header><!-- /header -->

   <section class="slider">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="http://erado.vn/images/gallery/g81.jpg" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="http://erado.vn/images/gallery/g135.jpg" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="http://erado.vn/images/gallery/g81.jpg" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
   </section>
   <section class="artist">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 sologen" align="center">
               <h3>"Nghệ sỹ Quang Tèo và Á hậu Huyền My"</h3>
               <p>CÙNG HÀNG NGHÌN KHÁCH HÀNG ĐÃ CHỌN ERADO BỞI :</p>
               <div class="bordertitle">
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-3 col-md-6 itemSologen" align="center">
               <a href="#" title="" >
                  <img src="img/h1.png" alt="" class="img1">
                   <img src="img/hh1.png" alt="" class="img2">
                   </a>
                  <p class="title"><a href="" title="">cơ sơ sang trọng</a></p>
                  <p class="content">Sroom rộng lớn, thiết kế hiện đại sang trọng theo tiêu chuẩn 3S, phục vụ tốt nhất cho khách hàng</p>
               
            </div>
            <div class="col-lg-3 col-md-6 itemSologen" align="center">
               <a href="#" title="" >
                  <img src="img/h1.png" alt="" class="img1">
                   <img src="img/hh1.png" alt="" class="img2">
                   </a>
                  <p class="title"><a href="" title="">cơ sơ sang trọng</a></p>
                  <p class="content">Sroom rộng lớn, thiết kế hiện đại sang trọng theo tiêu chuẩn 3S, phục vụ tốt nhất cho khách hàng</p>
               
            </div>
            <div class="col-lg-3 col-md-6 itemSologen" align="center">
               <a href="#" title="" >
                  <img src="img/h1.png" alt="" class="img1">
                   <img src="img/hh1.png" alt="" class="img2">
                   </a>
                  <p class="title"><a href="" title="">cơ sơ sang trọng</a></p>
                  <p class="content">Sroom rộng lớn, thiết kế hiện đại sang trọng theo tiêu chuẩn 3S, phục vụ tốt nhất cho khách hàng</p>
               
            </div>
             <div class="col-lg-3 col-md-6 itemSologen" align="center">
                  <a href="#" title="" >
                     <img src="img/h1.png" alt="" class="img1">
                      <img src="img/hh1.png" alt="" class="img2">
                      </a>
                     <p class="title"><a href="" title="">cơ sơ sang trọng</a></p>
                     <p class="content">Sroom rộng lớn, thiết kế hiện đại sang trọng theo tiêu chuẩn 3S, phục vụ tốt nhất cho khách hàng</p>
                  
               </div>
      </div>
      
   </section>

   <!-- <section class="productHighlights">
      <div class="container">
         <div class="row">
            <div class="col-lg-12" align="center">
                <h2 class="title">SẢN PHẨM <strong>VIDEO THỰC TẾ</strong></h2>
            <div class="bordertitle">
            </div>
            
             </div>
         </div>
         <div class="row">
            <div class="col-lg-6">
               <div class="left">
                     <a href="" title=""><img src="http://erado.vn/images/pro/sofa-da-that-ma-552_6874.jpg" alt=""></a>
            
                     <div class="content">
                        <a href="" title="" class="color">Sofa đẹp mã 552</a>
                        <p class="price">39,000,000 <span>vnđ</span></p>
                     </div>
               </div>
            </div>
            <div class="col-lg-6">
                  <div class="row right">
                     <div class="col-lg-6">
                        <a href="" title="" class=""> <img src="http://erado.vn/images/pro/sofa-da-that-ma-551-6873.jpg" alt=""></a>
                        <div class="content">
                           <a href="" title="" class="color">Sofa đẹp mã 552</a>
                           <p class="price">39,000,000 <span>vnđ</span></p>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <a href="" title=""> <img src="http://erado.vn/images/pro/sofa-da-that-ma-551-6873.jpg" alt=""></a>
                        <div class="content">
                           <a href="" title="" class="color">Sofa đẹp mã 552</a>
                           <p class="price">39,000,000 <span>vnđ</span></p>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <a href="" title=""> <img src="http://erado.vn/images/pro/sofa-da-that-ma-551-6873.jpg" alt=""></a>
                        <div class="content">
                           <a href="" title="" class="color">Sofa đẹp mã 552</a>
                           <p class="price">39,000,000 <span>vnđ</span></p>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <a href="" title=""> <img src="http://erado.vn/images/pro/sofa-da-that-ma-551-6873.jpg" alt=""></a>
                        <div class="content">
                           <a href="" title="" class="color">Sofa đẹp mã 552</a>
                           <p class="price">39,000,000 <span>vnđ</span></p>
                        </div>
                     </div>
                  </div>
               </div>
         </div>
      
      </div>

      
   </section>
   <section class="cateHigh">
      <div class="bgcate">
         <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12">
              <h2 class="title">DÒNG SẢN PHẨM NỔI BẬT</h2>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-3 col-md-3 ">
               <div class="itemHigh">
                   <a href="" title=""> <img src="http://erado.vn/images/menu/ghe-thu-gian.png" alt=""></a>
                  <div class="content active">
                     <h3><a href="" title="">Ghế thư giãn</a></h3>
                     <p>Các dòng ghế cao cấp nhập khẩu với thiết kế mang lại sự hiện đại</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 ">
               <div class="itemHigh">
                   <a href="" title=""> <img src="http://erado.vn/images/menu/ghe-thu-gian.png" alt=""></a>
                  <div class="content">
                     <h3><a href="" title="">Ghế thư giãn</a></h3>
                     <p>Các dòng ghế cao cấp nhập khẩu với thiết kế mang lại sự hiện đại</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 ">
               <div class="itemHigh">
                   <a href="" title=""> <img src="http://erado.vn/images/menu/ghe-thu-gian.png" alt=""></a>
                  <div class="content">
                     <h3><a href="" title="">Ghế thư giãn</a></h3>
                     <p>Các dòng ghế cao cấp nhập khẩu với thiết kế mang lại sự hiện đại</p>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-md-3 ">
               <div class="itemHigh">
                   <a href="" title=""> <img src="http://erado.vn/images/menu/ghe-thu-gian.png" alt=""></a>
                  <div class="content">
                     <h3><a href="" title="">Ghế thư giãn</a></h3>
                     <p>Các dòng ghế cao cấp nhập khẩu với thiết kế mang lại sự hiện đại</p>
                  </div>
               </div>
            </div>
           
            
         </div>
   </div>
      </div>   
      
   </section>
   <sectiton class="producTomorrow">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12">
               <h3 class="title">SẢN PHẨM <strong>KHUYẾN MÃI</strong></h3>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-4 col-md-4 itemTomo">
               <a href="" title="" class="proTitle color">Bàn ăn tích hợp bếp từ mã L113B</a>
               <a href="" title="">
                  <img src="http://erado.vn/images/pro/ban-an-tich-hop-bep-tu-ma-l113-6951.jpg" alt="">
               </a>
               <div class="sale">
                  -16%
               </div>
               <div class="price">
                  <h3>Giá : 500.000 đ</h3>

                  <p><del>800.000 đ</del></p>
               </div>
            </div>
           
            <div class="col-lg-4 col-md-4 itemTomo">
               <a href="" title="" class="proTitle color">Bàn ăn tích hợp bếp từ mã L113B</a>
               <a href="" title="">
                  <img src="http://erado.vn/images/pro/ban-an-tich-hop-bep-tu-ma-l113-6951.jpg" alt="">
               </a>
               <div class="sale">
                  -16%
               </div>
               <div class="price">
                  <h3>Giá : 500.000 đ</h3>

                  <p><del>800.000 đ</del></p>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 itemTomo">
               <a href="" title="" class="proTitle color">Bàn ăn tích hợp bếp từ mã L113B</a>
               <a href="" title="">
                  <img src="http://erado.vn/images/pro/ban-an-tich-hop-bep-tu-ma-l113-6951.jpg" alt="">
               </a>
               <div class="sale">
                  -16%
               </div>
               <div class="price">
                  <h3>Giá : 500.000 đ</h3>

                  <p><del>800.000 đ</del></p>
               </div>
            </div>
         </div>
      </div>
   </sectiton>

   <section class="products">
      <div class="container">
         <div class="row">
             <div class="col-lg-12 col-md-12">
               <h3 class="title">GHẾ <strong>SOFA</strong></h3>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
             <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
             <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
             <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
         </div>
      </div>
   </section> -->
</body>
</html>