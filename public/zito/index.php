<!DOCTYPE html>

      <html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js">
         <!--<![endif]-->
         <head>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
         
            <title>Nội Thất ZITO | Showroom Nội Thất Gỗ Cao Cấp Tại Hà Nội</title>
            <!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
            <meta name="description" content="Nội thất ZITO tiên phong cung cấp các sản phẩm Sofa gỗ, da, nỉ theo kích thước tùy chỉnh, xu hướng mới nhất Showroom số 15 Dương Đình Nghệ Cầu Giấy Hà Nội"/>
            <meta name="robots" content="noodp"/>
			<base href="http://vn3c.net/zito/">
            <style type="text/css">
               img.wp-smiley,
               img.emoji {
               display: inline !important;
               border: none !important;
               box-shadow: none !important;
               height: 1em !important;
               width: 1em !important;
               margin: 0 .07em !important;
               vertical-align: -0.1em !important;
               background: none !important;
               padding: 0 !important;
               }
            </style>
    <link rel="stylesheet" href="css/font-awesome.min.css">
            <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' type='text/css' media='all' />
            <link rel='stylesheet' id='yith_wcas_frontend-css'  href='css/yith_wcas_ajax_search.css' type='text/css' media='all' />
            <style id='yith_wcas_frontend-inline-css' type='text/css'>
               .autocomplete-suggestion{
               padding-right: 20px;
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
               .autocomplete-suggestion  span.yith_wcas_result_on_sale{
               background: #7eb742;
               color: #ffffff
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
               .autocomplete-suggestion  span.yith_wcas_result_outofstock{
               background: #7a7a7a;
               color: #ffffff
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
               .autocomplete-suggestion  span.yith_wcas_result_featured{
               background: #c0392b;
               color: #ffffff
               }
               .autocomplete-suggestion img{
               width: 50px;
               }
               .autocomplete-suggestion .yith_wcas_result_content .title{
               color: #004b91;
               }
               .autocomplete-suggestion{
               min-height: 60px;
               }
            </style>
            <link rel='stylesheet' id='flatsome-main-css'  href='css/flatsome.css' type='text/css' media='all' />
            <link rel='stylesheet' id='flatsome-shop-css'  href='css/flatsome-shop.css' type='text/css' media='all' />
            <link rel='stylesheet' id='flatsome-style-css'  href='css/style1.css' type='text/css' media='all' />
            <script type='text/javascript' src='js/jquery.js'></script>
            <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
            <script type='text/javascript' src='js/advanced-cf7-db-public.js'></script>
         
           
            <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>
       
       
            
 
         </head>
         <body>
     
         </body>
         <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
         <noscript>
            <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
         </noscript>
         <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
         <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
         <link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
         <meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
         <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>
         </head>
         <body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
            <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
            <div id="wrapper">
            <?php include('header/header.php')?>
			
            <main id="main" class="">
               <div id="content" role="main" class="content-area">
			   
			   
                  <?php include('slider/slider.php')?>
				  
				  
                  <!-- .ux-slider-wrapper -->
                  <section class="section" id="section_601109630">
                     <div class="bg section-bg fill bg-fill  bg-loaded" >
                        <div class="is-border"
                           style="border-width:0px 0px 0px 0px;">
                        </div>
                     </div>
                     <!-- .section-bg -->
                     <div class="section-content relative">
                        <div class="row"  id="row-193621860">
                           <div class="col medium-4 small-12 large-4"  >
                              <div class="col-inner"  >
                                 <div class="banner has-hover" id="banner-776979661">
                                    <div class="banner-inner fill">
                                       <div class="banner-bg fill" >
                                          <div class="bg fill bg-fill "></div>
                                          <div class="overlay"></div>
                                       </div>
                                       <!-- bg-layers -->
                                       <div class="banner-layers container">
                                          <div class="fill banner-link"></div>
                                          <div id="text-box-724877082" class="text-box banner-layer x50 md-x50 lg-x50 y50 md-y50 lg-y50 res-text">
                                             <div class="hover-bounce">
                                                <div class="text dark">
                                                   <div class="text-inner text-center">
                                                      <h3 class="uppercase">LỰA CHỌN CHẤT LIỆU</h3>
                                                      <p><span style="font-size: 170%;">Chọn chất liệu gỗ Sồi Nga, Sồi Mỹ, Xoan đào, Óc chó,&#8230; Nệm Da thật, Da công nghiệp, Nỉ cao cấp theo nhu cầu</span></p>
                                                   </div>
                                                </div>
                                                <!-- text-box-inner -->
                                             </div>
                                             <style scope="scope">
                                                #text-box-724877082 {
                                                width: 80%;
                                                }
                                                #text-box-724877082 .text {
                                                font-size: 100%;
                                                }
                                             </style>
                                          </div>
                                          <!-- text-box -->
                                       </div>
                                       <!-- .banner-layers -->
                                    </div>
                                    <!-- .banner-inner -->
                                    <style scope="scope">
                                       #banner-776979661 {
                                       padding-top: 300px;
                                       }
                                       #banner-776979661 .bg.bg-loaded {
                                       background-image: url(https://zito.vn/wp-content/uploads/2017/11/sofa-zito-tuy-chinh-zg-118-3-1024x575.jpg);
                                       }
                                       #banner-776979661 .overlay {
                                       background-color: rgba(0, 0, 0, 0.37);
                                       }
                                    </style>
                                 </div>
                                 <!-- .banner -->
                              </div>
                           </div>
                           <div class="col medium-4 small-12 large-4"  >
                              <div class="col-inner"  >
                                 <div class="banner has-hover" id="banner-855417071">
                                    <div class="banner-inner fill">
                                       <div class="banner-bg fill" >
                                          <div class="bg fill bg-fill "></div>
                                          <div class="overlay"></div>
                                       </div>
                                       <!-- bg-layers -->
                                       <div class="banner-layers container">
                                          <div class="fill banner-link"></div>
                                          <div id="text-box-1911575830" class="text-box banner-layer x50 md-x50 lg-x50 y50 md-y50 lg-y50 res-text">
                                             <div class="hover-bounce">
                                                <div class="text dark">
                                                   <div class="text-inner text-center">
                                                      <h3 class="uppercase">THAY ĐỔI MÀU SẮC</h3>
                                                      <p><span style="font-size: 170%;">Tone màu sơn thay đổi để phù hợp với phong thủy Gia chủ, các vật dung sẳn có; màu da, nỉ theo sở thích riêng</span></p>
                                                   </div>
                                                </div>
                                                <!-- text-box-inner -->
                                             </div>
                                             <style scope="scope">
                                                #text-box-1911575830 {
                                                width: 80%;
                                                }
                                                #text-box-1911575830 .text {
                                                font-size: 100%;
                                                }
                                                @media (min-width:550px) {
                                                #text-box-1911575830 {
                                                width: 80%;
                                                }
                                                }
                                             </style>
                                          </div>
                                          <!-- text-box -->
                                       </div>
                                       <!-- .banner-layers -->
                                    </div>
                                    <!-- .banner-inner -->
                                    <style scope="scope">
                                       #banner-855417071 {
                                       padding-top: 300px;
                                       }
                                       #banner-855417071 .bg.bg-loaded {
                                       background-image: url(https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito-mau-ni-1-1024x576.jpg);
                                       }
                                       #banner-855417071 .overlay {
                                       background-color: rgba(0, 0, 0, 0.37);
                                       }
                                    </style>
                                 </div>
                                 <!-- .banner -->
                              </div>
                           </div>
                           <div class="col medium-4 small-12 large-4"  >
                              <div class="col-inner"  >
                                 <div class="banner has-hover" id="banner-1841409992">
                                    <div class="banner-inner fill">
                                       <div class="banner-bg fill" >
                                          <div class="bg fill bg-fill "></div>
                                          <div class="overlay"></div>
                                       </div>
                                       <!-- bg-layers -->
                                       <div class="banner-layers container">
                                          <div class="fill banner-link"></div>
                                          <div id="text-box-117192336" class="text-box banner-layer x50 md-x50 lg-x50 y50 md-y50 lg-y50 res-text">
                                             <div class="hover-bounce">
                                                <div class="text dark">
                                                   <div class="text-inner text-center">
                                                      <h3 class="uppercase">tùy chỉnh kích thước</h3>
                                                      <p><span style="font-size: 170%;">Kiểu dáng Văng, Góc chữ L,&#8230; kích thước được đo đạc, tư vấn cho phù hợp nhất với phòng khách &amp; căn hộ của bạn</span></p>
                                                   </div>
                                                </div>
                                                <!-- text-box-inner -->
                                             </div>
                                             <style scope="scope">
                                                #text-box-117192336 {
                                                width: 80%;
                                                }
                                                #text-box-117192336 .text {
                                                font-size: 100%;
                                                }
                                             </style>
                                          </div>
                                          <!-- text-box -->
                                       </div>
                                       <!-- .banner-layers -->
                                    </div>
                                    <!-- .banner-inner -->
                                    <style scope="scope">
                                       #banner-1841409992 {
                                       padding-top: 300px;
                                       }
                                       #banner-1841409992 .bg.bg-loaded {
                                       background-image: url(https://zito.vn/wp-content/uploads/2017/11/sofa-zito-tuy-chinh-zg-118-12-1024x575.jpg);
                                       }
                                       #banner-1841409992 .overlay {
                                       background-color: rgba(0, 0, 0, 0.37);
                                       }
                                    </style>
                                 </div>
                                 <!-- .banner -->
                              </div>
                           </div>
                        </div>
                        <div class="row"  id="row-820692455">
                           <div class="col medium-6 small-12 large-6"  data-animate="fadeInRight">
                              <div class="col-inner" style="padding:0px 15px 0px 15px;margin:0px 0px 0px 0px;" >
                                 <div class="row"  id="row-1522671144">
                                    <div class="col hide-for-small small-12 large-12"  >
                                       <div class="col-inner"  >
                                          <div class="gap-element" style="display:block; height:auto; padding-top:100px" class="clearfix"></div>
                                       </div>
                                    </div>
                                 </div>
                                 <h2 style="text-align: right;">Đổi mới, sáng tạo &#8211; Nội thất ZITO</h2>
                                 <p style="text-align: justify;">Vượt lên nhưng khái niệm thông thường, giúp bạn có được định nghĩa mới của từ “SÁNG TẠO”, thay đổi tư duy về NỘI THẤT để hòa mình vào một không gian sống và làm việc HIỆN ĐẠI với những chức năng được tích hợp đột phá chưa từng có!</p>
                                 <p style="text-align: right;"><em>&#8221; Trải nghiệm thực tế ngay bây giờ cùng ZITO !&#8221;</em></p>
                              </div>
                           </div>
                           <div class="col medium-6 small-12 large-6"  >
                              <div class="col-inner"  >
                                 <div class="banner-grid-wrapper">
                                    <div id="banner-grid-771097791" class="banner-grid row row-grid row-xsmall" data-packery-options="">
                                       <div class="col grid-col large-6 grid-col-2-3" >
                                          <div class="col-inner">
                                             <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_918092633">
                                                <a class="image-lightbox lightbox-gallery" href="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-5-1024x575.jpg" title="">
                                                   <div class="img-inner image-cover dark" style="padding-top:100%;">
                                                      <img width="1020" height="573" src="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-5-1024x575.jpg" class="attachment-large size-large" alt="Sofa Gỗ ZG 102" srcset="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-5-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-5-300x168.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-5-768x431.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-5.jpg 1366w" sizes="(max-width: 1020px) 100vw, 1020px" />                
                                                   </div>
                                                </a>
                                                <style scope="scope">
                                                </style>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col grid-col large-6 grid-col-1-2" >
                                          <div class="col-inner">
                                             <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_156127786">
                                                <a class="image-lightbox lightbox-gallery" href="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-tuy-chinh-zg-102-1-1024x576-1024x576.jpg" title="">
                                                   <div class="img-inner image-cover dark" style="padding-top:100%;">
                                                      <img width="1020" height="574" src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-tuy-chinh-zg-102-1-1024x576-1024x576.jpg" class="attachment-large size-large" alt="Sofa gỗ ZG102" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-tuy-chinh-zg-102-1-1024x576.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-tuy-chinh-zg-102-1-1024x576-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-tuy-chinh-zg-102-1-1024x576-768x432.jpg 768w" sizes="(max-width: 1020px) 100vw, 1020px" />                
                                                   </div>
                                                </a>
                                                <style scope="scope">
                                                </style>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col grid-col large-3 grid-col-1-2" >
                                          <div class="col-inner">
                                             <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1316758441">
                                                <a class="image-lightbox lightbox-gallery" href="https://zito.vn/wp-content/uploads/2017/12/zito-sofa-go-tuy-chinh-zg-102-4-1024x576.jpg" title="">
                                                   <div class="img-inner image-cover dark" style="padding-top:100%;">
                                                      <img width="1020" height="574" src="https://zito.vn/wp-content/uploads/2017/12/zito-sofa-go-tuy-chinh-zg-102-4-1024x576.jpg" class="attachment-large size-large" alt="sofa gỗ" srcset="https://zito.vn/wp-content/uploads/2017/12/zito-sofa-go-tuy-chinh-zg-102-4-1024x576.jpg 1024w, https://zito.vn/wp-content/uploads/2017/12/zito-sofa-go-tuy-chinh-zg-102-4-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/12/zito-sofa-go-tuy-chinh-zg-102-4-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2017/12/zito-sofa-go-tuy-chinh-zg-102-4.jpg 1366w" sizes="(max-width: 1020px) 100vw, 1020px" />                
                                                   </div>
                                                </a>
                                                <style scope="scope">
                                                </style>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col grid-col large-3 grid-col-1-2" >
                                          <div class="col-inner">
                                             <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1579159330">
                                                <a class="image-lightbox lightbox-gallery" href="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-6-1024x575-1024x575.jpg" title="">
                                                   <div class="img-inner image-cover dark" style="padding-top:100%;">
                                                      <img width="1020" height="573" src="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-6-1024x575-1024x575.jpg" class="attachment-large size-large" alt="nội thất zito" srcset="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-6-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-6-1024x575-300x168.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-6-1024x575-768x431.jpg 768w" sizes="(max-width: 1020px) 100vw, 1020px" />                
                                                   </div>
                                                </a>
                                                <style scope="scope">
                                                </style>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col grid-col large-6 grid-col-1-3" >
                                          <div class="col-inner">
                                             <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1320684318">
                                                <a class="image-lightbox lightbox-gallery" href="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-1024x575-1024x575.jpg" title="">
                                                   <div class="img-inner image-cover dark" style="padding-top:100%;">
                                                      <img width="1020" height="573" src="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-1024x575-1024x575.jpg" class="attachment-large size-large" alt="Sofa gỗ đẹp giá rẻ" srcset="https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-1024x575-300x168.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/zito-sofa-go-tuy-chinh-zg-102-1024x575-768x431.jpg 768w" sizes="(max-width: 1020px) 100vw, 1020px" />                
                                                   </div>
                                                </a>
                                                <style scope="scope">
                                                </style>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- .banner-grid .row .grid -->
                                    <style scope="scope">
                                       #banner-grid-771097791 .grid-col-1{height: 380px}
                                       #banner-grid-771097791 .grid-col-1-2{height: 190px}
                                       #banner-grid-771097791 .grid-col-1-3{height:126.66666666667px}
                                       #banner-grid-771097791 .grid-col-2-3{height: 253.33333333333px}
                                       #banner-grid-771097791 .grid-col-1-4{height: 95px}
                                       #banner-grid-771097791 .grid-col-3-4{height: 285px}
                                       /* Mobile */
                                       @media (max-width: 550px){
                                       #banner-grid-771097791 .grid-col-1{height: 300px}
                                       #banner-grid-771097791 .grid-col-1-2{height: 150px}
                                       #banner-grid-771097791 .grid-col-1-3{height:100px}
                                       #banner-grid-771097791 .grid-col-2-3{height: 200px}
                                       #banner-grid-771097791 .grid-col-1-4{height: 75px}
                                       #banner-grid-771097791 .grid-col-3-4{height: 225px}
                                       }
                                    </style>
                                 </div>
                                 <!-- .banner-grid-wrapper -->
                              </div>
                           </div>
                           <style scope="scope">
                              #row-820692455 > .col > .col-inner {
                              padding: 0px 0px 0px 0px;
                              }
                           </style>
                        </div>
                     </div>
                     <!-- .section-content -->
                     <style scope="scope">
                        #section_601109630 {
                        padding-top: 30px;
                        padding-bottom: 30px;
                        background-color: rgb(237, 239, 237);
                        }
                     </style>
                  </section>
                  <section class="section" id="section_866624665">
                     <div class="bg section-bg fill bg-fill  bg-loaded" >
                     </div>
                     <!-- .section-bg -->
                     <div class="section-content relative">
                        <div class="row row-collapse row-full-width align-middle"  id="row-56604385">
                           <div class="col medium-6 small-12 large-6"  >
                              <div class="col-inner"  >
                                 <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1877098676">
                                    <div class="img-inner image-zoom dark" >
                                       <img width="1020" height="576" src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-1024x578.jpg" class="attachment-large size-large" alt="Sofa gỗ tự nhiên" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-1024x578.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-768x433.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg 1600w" sizes="(max-width: 1020px) 100vw, 1020px" />                
                                    </div>
                                    <style scope="scope">
                                    </style>
                                 </div>
                              </div>
                           </div>
                           <div class="col medium-6 small-12 large-6"  >
                              <div class="col-inner text-left" style="max-width:520px;padding:5% 5% 5% 5%;" >
                                 <h2>Sofa 100% Gỗ tự nhiên</h2>
                                 <p style="text-align: justify;">Gỗ tự nhiên (Sồi Nga, Sồi Mỹ, Xoan đào, Óc chó,&#8230;) được tẩm sấy, BỀN BỈ với chế độ bảo hành lên tới 02 năm. Sản phẩm được gia công TỈ MỈ, kết cấu CHẮC CHẮN theo kiểu dáng, kích thước, màu sắc và đệm Da hoặc Nỉ theo nhu cầu của Quý khách!</p>
                                 <a href="/danh-muc/sofa-go/" target="_self" class="button secondary"  >
                                 <span>Lựa chọn kiểu dáng</span>
                                 </a>
                              </div>
                           </div>
                           <style scope="scope">
                           </style>
                        </div>
                     </div>
                     <!-- .section-content -->
                     <style scope="scope">
                        #section_866624665 {
                        padding-top: 0px;
                        padding-bottom: 0px;
                        }
                     </style>
                  </section>
                  <section class="section sphome" id="section_1700251332">
                     <div class="bg section-bg fill bg-fill  bg-loaded" >
                     </div>
                     <!-- .section-bg -->
                     <div class="section-content relative">
                        <div class="row align-center"  id="row-775226297">
                           <div class="col small-12 large-12"  >
                              <div class="col-inner text-center"  >
                                 <div class="container section-title-container" >
                                    <h3 class="section-title section-title-normal"><b></b><span class="section-title-main" >LỰA CHỌN MẪU SOFA GỖ TÙY CHỈNH</span><b></b></h3>
                                 </div>
                                 <!-- .section-title -->
                                 <div class="row large-columns-4 medium-columns- small-columns-2 row-small has-shadow row-box-shadow-1 row-box-shadow-3-hover">
                                    <div class="product-small col has-hover post-139 product type-product status-publish has-post-thumbnail product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l first instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-101/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-101-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa gỗ ZG 101" title="Sofa gỗ ZG 101 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-101-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-101-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-101-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-101/">Sofa gỗ ZG 101</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">19.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-165 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-vang product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l product_tag-sofa-phong-khach-nho  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-102/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-102-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 102" title="Sofa Gỗ ZG 102 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-102-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-102-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-102-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-102/">Sofa Gỗ ZG 102</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">12.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-180 product type-product status-publish has-post-thumbnail product_cat-sofa-go product_cat-sofa-vang product_tag-sofa product_tag-sofa-go-dem-ni product_tag-sofa-phong-khach-nho  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-103/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-103-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 103" title="Sofa Gỗ ZG 103 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-103-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-103-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-103-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-103/">Sofa Gỗ ZG 103</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">13.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-166 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l last instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-104/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 104" title="Sofa Gỗ ZG 104 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-104/">Sofa Gỗ ZG 104</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">21.300.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-167 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l first instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-105/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 105" title="Sofa Gỗ ZG 105 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-105/">Sofa Gỗ ZG 105</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">21.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-183 product type-product status-publish has-post-thumbnail product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-106/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 106" title="Sofa Gỗ ZG 106 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-106/">Sofa Gỗ ZG 106</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">20.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-187 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-107/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 107" title="Sofa Gỗ ZG 107 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-107/">Sofa Gỗ ZG 107</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">19.400.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-191 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-chu-u product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-phong-khach-nho last instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/sofa-go/zg-108/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-108-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 108" title="Sofa Gỗ ZG 108 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-108-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-108-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-108-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-108/">Sofa Gỗ ZG 108</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">26.300.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                 </div>
                                 <a href="/danh-muc/sofa-go/" target="_self" class="button secondary box-shadow-3 reveal-icon"  >
                                 <span>Xem thêm sản phẩm khác</span>
                                 <i class="icon-expand" ></i></a>
                                 <div class="container section-title-container" >
                                    <h3 class="section-title section-title-normal"><b></b><span class="section-title-main" >CÁC MẪU BÀN TRÀ, GỐI TỰA LƯNG, GỐI ÔM SOFA KHÁCH HÀNG YÊU THÍCH NHẤT</span><b></b></h3>
                                 </div>
                                 <!-- .section-title -->
                                 <div class="row large-columns-6 medium-columns-3 small-columns-2 row-small has-shadow row-box-shadow-1 row-box-shadow-3-hover slider row-slider slider-nav-reveal slider-nav-push"  data-flickity-options='{"imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : false}'>
                                    <div class="product-small col has-hover post-4653 product type-product status-publish has-post-thumbnail product_cat-ban-tra first instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-008/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ kiểu nhật" title="BAN TRA GO ZITO ZB 008" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-008/">Bàn Trà ZITO ZB 008</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4648 product type-product status-publish has-post-thumbnail product_cat-ban-tra  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-007/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ vuông tại hà nội" title="BAN TRA GO ZITO ZB 007 2" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-007/">Bàn Trà ZITO ZB 007</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">5.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4644 product type-product status-publish has-post-thumbnail product_cat-ban-tra  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-006/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà sofa" title="BAN TRA GO ZITO ZB 006 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-006/">Bàn Trà ZITO ZB 006</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4641 product type-product status-publish has-post-thumbnail product_cat-ban-tra last instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-005/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-005-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ thông minh tại ZITO" title="BAN TRA GO ZITO 005 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-005-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-005-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-005-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-005/">Bàn Trà ZITO ZB 005</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4637 product type-product status-publish has-post-thumbnail product_cat-ban-tra first instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-004/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-004-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ sồi nga phòng khách tại ZITO" title="BAN TRA GO ZITO ZB 004 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-004-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-004-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-004-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-004/">Bàn Trà ZITO ZB 004</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4631 product type-product status-publish has-post-thumbnail product_cat-ban-tra  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-003/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-003-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ tự nhiên" title="BAN TRA GO ZITO ZB 003 2" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-003-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-003-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-003-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-003/">Bàn Trà ZITO ZB 003</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4626 product type-product status-publish has-post-thumbnail product_cat-ban-tra  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-002/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-002-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ phòng khách mới 2018" title="BAN TRA GO ZITO 002 2" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-002-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-002-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-002-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-002/">Bàn trà ZITO ZB 002</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4618 product type-product status-publish has-post-thumbnail product_cat-ban-tra product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat last instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/san-pham-noi-bat/zb-001/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-001-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ phòng khách tại hà nội ZB 001" title="BAN TRA GO ZITO 001" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-001-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-001-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-001-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/san-pham-noi-bat/zb-001/">Bàn Trà ZITO ZB 001</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                 </div>
                                 <div class="row large-columns-6 medium-columns-3 small-columns-2 row-small has-shadow row-box-shadow-1 row-box-shadow-3-hover slider row-slider slider-nav-reveal slider-nav-push"  data-flickity-options='{"imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : false}'>
                                    <div class="product-small col has-hover post-4742 product type-product status-publish has-post-thumbnail product_cat-goi-tua product_tag-goi-hinh-meo product_tag-goi-sofa product_tag-goi-trang-tri product_tag-goi-tua-lung product_tag-goi-vuong first instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/goi-tua/zp-meo-03/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Gối tựa sofa" title="ZP MEO 03 goi trang tri hinh meo tai ZITO" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/goi-tua/zp-meo-03/">Gối ZP Mèo 03</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">220.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4739 product type-product status-publish has-post-thumbnail product_cat-goi-tua product_tag-goi-hinh-meo product_tag-goi-sofa product_tag-goi-trang-tri product_tag-goi-tua-lung product_tag-goi-vuong  instock shipping-taxable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/goi-tua/zp-meo-02/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="gối tựa lưng văn phòng" title="ZP MEO 02 goi tua lung hinh meo tai ZITO" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/goi-tua/zp-meo-02/">Gối ZP Mèo 02</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4737 product type-product status-publish has-post-thumbnail product_cat-goi-tua product_tag-goi-hinh-meo product_tag-goi-sofa product_tag-goi-trang-tri product_tag-goi-tua-lung product_tag-goi-vuong  instock shipping-taxable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/goi-tua/zp-meo-01/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="gối tựa lưng" title="ZP MEO 01 goi trang tri hinh meo tai ZITO" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/goi-tua/zp-meo-01/">Gối ZP Mèo 01</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4736 product type-product status-publish has-post-thumbnail product_cat-goi-tua product_tag-goi-hinh-gau product_tag-goi-sofa product_tag-goi-trang-tri product_tag-goi-tua-lung product_tag-goi-vuong last instock shipping-taxable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/goi-tua/zp-gau-01/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="gối tựa lưng dễ thương" title="ZP GAU 01 goi tua lung hinh gau tai ZITO" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/goi-tua/zp-gau-01/">Gối ZP Gấu 01</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4734 product type-product status-publish has-post-thumbnail product_cat-goi-tua product_tag-goi-hinh-buom product_tag-goi-sofa product_tag-goi-trang-tri product_tag-goi-tua-lung product_tag-goi-vuong first instock shipping-taxable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/goi-tua/zp-buom-01/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Gối ôm gối tựa lưng giá rẻ tại hà nội" title="ZP BUOM 04 goi tua lung gia re tai ha noi" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/goi-tua/zp-buom-01/">Gối ZP Bướm 01</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4653 product type-product status-publish has-post-thumbnail product_cat-ban-tra  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-008/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ kiểu nhật" title="BAN TRA GO ZITO ZB 008" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-008-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-008/">Bàn Trà ZITO ZB 008</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4648 product type-product status-publish has-post-thumbnail product_cat-ban-tra  instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-007/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà gỗ vuông tại hà nội" title="BAN TRA GO ZITO ZB 007 2" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-007-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-007/">Bàn Trà ZITO ZB 007</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">5.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                    <div class="product-small col has-hover post-4644 product type-product status-publish has-post-thumbnail product_cat-ban-tra last instock shipping-taxable purchasable product-type-simple">
                                       <div class="col-inner">
                                          <div class="badge-container absolute left top z-1"></div>
                                          <div class="product-small box ">
                                             <div class="box-image">
                                                <div class="image-none">
                                                   <a href="https://zito.vn/shop/ban-tra/zb-006/">
                                                   <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="bàn trà sofa" title="BAN TRA GO ZITO ZB 006 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2018/01/BAN-TRA-GO-ZITO-ZB-006-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                                </div>
                                                <div class="image-tools is-small top right show-on-hover">
                                                </div>
                                                <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                                </div>
                                                <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                </div>
                                             </div>
                                             <!-- box-image -->
                                             <div class="box-text box-text-products text-center grid-style-2">
                                                <div class="title-wrapper">
                                                   <p class="name product-title"><a href="https://zito.vn/shop/ban-tra/zb-006/">Bàn Trà ZITO ZB 006</a></p>
                                                </div>
                                                <div class="price-wrapper">
                                                   <span class="price"><span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                                </div>
                                             </div>
                                             <!-- box-text -->
                                          </div>
                                          <!-- box -->
                                       </div>
                                       <!-- .col-inner -->
                                    </div>
                                    <!-- col -->
                                 </div>
                              </div>
                           </div>
                           <style scope="scope">
                              #row-775226297 > .col > .col-inner {
                              padding: 31px 0px 10px 0px;
                              }
                           </style>
                        </div>
                     </div>
                     <!-- .section-content -->
                     <style scope="scope">
                        #section_1700251332 {
                        padding-top: 0px;
                        padding-bottom: 0px;
                        background-color: rgb(237, 239, 237);
                        }
                     </style>
                  </section>
               </div>
            </main>
            <!-- #main -->
			
			
           
			<?php include('footer/footer.php')?>
			
            <!-- .footer-wrapper -->
            </div>
			<!-- #wrapper -->
			
			
            <!-- Mobile Sidebar -->
            <?php include('header/header-mobile.php')?>
            <!-- #mobile-menu -->
            
      
			
            <div id="login-form-popup" class="lightbox-content mfp-hide">
               <div class="account-container lightbox-inner">
                  <div class="account-login-inner">
                     <h3 class="uppercase">Đăng nhập</h3>
                     <form method="post" class="login">
                        <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                           <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                           <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                        </p>
                        <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                           <label for="password">Mật khẩu <span class="required">*</span></label>
                           <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                        </p>
                        <p class="form-row">
                           <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />				<input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                           <label for="rememberme" class="inline">
                           <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu				</label>
                        </p>
                        <p class="woocommerce-LostPassword lost_password">
                           <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                        </p>
                     </form>
                  </div>
                  <!-- .login-inner -->
               </div>
               <!-- .account-login-container -->
            </div>
            <link rel='stylesheet' id='flatsome-effects-css'  href='css/effects.css' type='text/css' media='all' />
   
            <script type='text/javascript' src='js/scripts.js'></script>
            <script type='text/javascript' src='js/add-to-cart.min.js'></script>
            <script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
            <script type='text/javascript' src='js/js.cookie.min.js'></script>     
            <script type='text/javascript' src='js/woocommerce.min.js'></script>
            <script type='text/javascript' src='js/cart-fragments.min.js'></script>
            <script type='text/javascript' src='js/yith-autocomplete.min.js'></script>
            <script type='text/javascript' src='js/flatsome-live-search.js'></script>
            <script type='text/javascript' src='js/hoverIntent.min.js'></script>
            <script type='text/javascript'>
               /* <![CDATA[ */
               var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
               /* ]]> */
            </script>
            <script type='text/javascript' src='js/flatsome.js'></script>
            <script type='text/javascript' src='js/flatsome-lazy-load.js'></script>
            <script type='text/javascript' src='js/woocommerce.js'></script>
            <script type='text/javascript' src='js/wp-embed.min.js'></script>
            <script type='text/javascript' src='js/packery.pkgd.min.js'></script>
            <script type='text/javascript' src='js/zxcvbn-async.min.js'></script>
            
            <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
          
            <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
         </body>
      </html>
     