<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/bootstrap.css">
     <link rel="stylesheet" href="css/font-awesome.css">
   <link rel="stylesheet" href="css/eroda.css">

    <script src="js/jquery3-3.js"></script>
     <script src="js/bootstrap.bundle.js"></script>
      <script src="js/bootstrap.js"></script>
   <style type="text/css" media="screen">
    .color:hover
    {
         color: #ff0000 !important;
         text-decoration: underline !important;
    }  
   </style>
</head>
<body>
   <header id="header" class="">
      <div class="headertop">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12">
               
                  <ul>
                     <li><a href="#" title="" class="color">Lịch sử phát triển  </a></li>

                     <li><a href="#" title="" class="color">Tầm nhìn, sứ mệnh  </a></li>
                     <li><a href="#" title="" class="color">Tuyển Dụng   </a></li>
                     <li><a href="#" title="" class="color">Góc báo Chí   </a></li>
                     <li><a href="#" title="" class="color">Tin mới nhất   </a></li>
                     <li><a href="#" title="" class="color">Lịch sử phát triển</a></li>
                  </ul> 
               </div>
            </div>
         </div>
         
      </div>
      <div class="headercontent">
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-3 col-sm-12">
                  <div class="logo">
                     <img src="img/logo.jpg" alt="" class="">

                     <div class="hotline hiddenPc showMb">
                     <img src="img/i11.gif" alt="">
                     <span><span class="hiddenTab">Hotline : </span><strong> 0976.529.529</strong></span>
                  </div>
                  </div>
                  
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12">
                  <div class="search">
                     <form action="" method="get" accept-charset="utf-8">
                        <input type="text" name="" value="" placeholder="">
                        <button type=""><i class="fa fa-search" aria-hidden="true"></i></button>
                     </form>
                  </div>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-12">
                  <div class="hotline hiddenMb">
                     <img src="img/i11.gif" alt="">
                     <span><span class="hiddenTab">Hotline : </span><strong> 0976.529.529</strong></span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="menu">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12">
                   <nav class="navbar navbar-expand-lg navbar-light bg-light">
           
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                          <a class="nav-link" href="#">Trang chủ <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ghế sofa
                          </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            
                        </li>
                        
                      </ul>
                   
                    </div>
                  </nav>
                  
               </div>
            </div>
         </div>
        
      </div>
   </header><!-- /header -->

   <section class="detailProduct">
     <div class="container">
       <div class="row">
         <div class="col-lg-6 col-md-6 left">
           <img src="http://erado.vn/images/pro/ghe-thu-gian-imola-ma-05-7213.jpg" alt="">
         </div>
         <div class="col-lg-6 col-md-6 rightitem">
           <div class="right">
             <h1>Sofa da mã 501</h1>
             <p class="price">Giá : <span>25,000,000 đ</span></p>
             <div class="des">
               Giá thị trường: 30,000,000 đ
                Hoặc mua với giá: 38,000,000 đ
                Hoặc mua với giá: 22,300,000 đ
                Kích thước: 2800 * 1800 * 1000
                Bảo hành: 6 năm
                Tình trạng: Còn hàng
             </div>
             <div class="cart">
                <a href="" title="" class="send">Mua ngay</a>
                <a href="" title="" class="guide">Hướng dẫn mua hàng</a>
             </div>
             <div class="ship">
              <img src="img/d1.png" alt="">
               <a href="" rel="nofollow">Ship hàng trên toàn quốc</a>
             </div>
           </div>
         </div>
       </div>
     </div>
   </section>
   <section class="productInvolve">
        <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12">
               <h3 class="title">SẢN PHẨM CÙNG LOẠI</h3>
            </div>
         </div>
         <div class="row">
           <div class="col-lg-3 col-md-6 col-sm-12">
             <div class="itempro">
              <a href="" title=""> <img src="http://erado.vn/images/pro/ghe-thu-gian-imola-ma-05-7213.jpg" alt=""></a>
              <a class="title color">
                Ghế thư giãn Imola mã 05
              </a>
              <p><span>14,000,000</span> vnđ</p>

             </div>
           </div>
           <div class="col-lg-3 col-md-6 col-sm-12">
             <div class="itempro">
              <a href="" title=""> <img src="http://erado.vn/images/pro/ghe-thu-gian-imola-ma-05-7213.jpg" alt=""></a>
              <a class="title color">
                Ghế thư giãn Imola mã 05
              </a>
              <p><span>14,000,000</span> vnđ</p>

             </div>
           </div>
           <div class="col-lg-3 col-md-6 col-sm-12">
             <div class="itempro">
              <a href="" title=""> <img src="http://erado.vn/images/pro/ghe-thu-gian-imola-ma-05-7213.jpg" alt=""></a>
              <a class="title color">
                Ghế thư giãn Imola mã 05
              </a>
              <p><span>14,000,000</span> vnđ</p>

             </div>
           </div>
           <div class="col-lg-3 col-md-6 col-sm-12">
             <div class="itempro">
              <a href="" title=""> <img src="http://erado.vn/images/pro/ghe-thu-gian-imola-ma-05-7213.jpg" alt=""></a>
              <a class="title color">
                Ghế thư giãn Imola mã 05
              </a>
              <p><span>14,000,000</span> vnđ</p>

             </div>
           </div>
           <div class="col-lg-3 col-md-6 col-sm-12">
             <div class="itempro">
              <a href="" title=""> <img src="http://erado.vn/images/pro/ghe-thu-gian-imola-ma-05-7213.jpg" alt=""></a>
              <a class="title color">
                Ghế thư giãn Imola mã 05
              </a>
              <p><span>14,000,000</span> vnđ</p>

             </div>
           </div>
         </div>
        
      </div>
   </section>
   <sectiton class="producTomorrow">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12">
               <h3 class="title">SẢN PHẨM <strong>KHUYẾN MÃI</strong></h3>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12 itemTomo">
               <a href="" title="" class="proTitle color">Bàn ăn tích hợp bếp từ mã L113B</a>
               <a href="" title="">
                  <img src="http://erado.vn/images/pro/ban-an-tich-hop-bep-tu-ma-l113-6951.jpg" alt="">
               </a>
               <div class="sale">
                  -16%
               </div>
               <div class="price">
                  <h3>Giá : 500.000 đ</h3>

                  <p><del>800.000 đ</del></p>
               </div>
            </div>
           
            <div class="col-lg-4 col-md-6 col-sm-12 itemTomo">
               <a href="" title="" class="proTitle color">Bàn ăn tích hợp bếp từ mã L113B</a>
               <a href="" title="">
                  <img src="http://erado.vn/images/pro/ban-an-tich-hop-bep-tu-ma-l113-6951.jpg" alt="">
               </a>
               <div class="sale">
                  -16%
               </div>
               <div class="price">
                  <h3>Giá : 500.000 đ</h3>

                  <p><del>800.000 đ</del></p>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 itemTomo">
               <a href="" title="" class="proTitle color">Bàn ăn tích hợp bếp từ mã L113B</a>
               <a href="" title="">
                  <img src="http://erado.vn/images/pro/ban-an-tich-hop-bep-tu-ma-l113-6951.jpg" alt="">
               </a>
               <div class="sale">
                  -16%
               </div>
               <div class="price">
                  <h3>Giá : 500.000 đ</h3>

                  <p><del>800.000 đ</del></p>
               </div>
            </div>
         </div>
      </div>
   </sectiton>

   <!-- <section class="products">
      <div class="container">
         <div class="row">
             <div class="col-lg-12 col-md-12">
               <h3 class="title">GHẾ <strong>SOFA</strong></h3>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
             <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
             <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
             <div class="col-lg-3 col-md-3">
                <div class="itemPro" align="center">
                   <a href="" title=""><img src="http://erado.vn/images/pro/ghe-thu-gian-metropolitan-09-7020.jpg" alt=""></a>

                  <a href="" title="" class="color titlepro">Ghế thư giãn Metropolitan 09</a>
                  <p>Giá : <span>39,000,000 VNĐ</span></p>
                </div>  
            </div>
         </div>
      </div>
   </section> -->
</body>
</html>