<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js">
   <!--<![endif]-->
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <title>Nội Thất ZITO | Showroom Nội Thất Gỗ Cao Cấp Tại Hà Nội</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Nội thất ZITO tiên phong cung cấp các sản phẩm Sofa gỗ, da, nỉ theo kích thước tùy chỉnh, xu hướng mới nhất Showroom số 15 Dương Đình Nghệ Cầu Giấy Hà Nội"/>
      <meta name="robots" content="noodp"/>
      <base href="http://vn3c.net/zito/">
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' type='text/css' media='all' />
      <link rel='stylesheet' id='yith_wcas_frontend-css'  href='css/yith_wcas_ajax_search.css' type='text/css' media='all' />
      <style id='yith_wcas_frontend-inline-css' type='text/css'>
         .autocomplete-suggestion{
         padding-right: 20px;
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
         .autocomplete-suggestion  span.yith_wcas_result_on_sale{
         background: #7eb742;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
         .autocomplete-suggestion  span.yith_wcas_result_outofstock{
         background: #7a7a7a;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
         .autocomplete-suggestion  span.yith_wcas_result_featured{
         background: #c0392b;
         color: #ffffff
         }
         .autocomplete-suggestion img{
         width: 50px;
         }
         .autocomplete-suggestion .yith_wcas_result_content .title{
         color: #004b91;
         }
         .autocomplete-suggestion{
         min-height: 60px;
         }
      </style>
      <link rel='stylesheet' id='flatsome-main-css'  href='css/flatsome.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-shop-css'  href='css/flatsome-shop.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-style-css'  href='css/style1.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/jquery.js'></script>
      <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
      <script type='text/javascript' src='js/advanced-cf7-db-public.js'></script>
      <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>
   </head>
   <body>
   </body>
   <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
   <noscript>
      <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
   </noscript>
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
   <link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
   <meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
   <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>
   </head>
   <body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
      <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
      <div id="wrapper">
         <?php include('header/header.php')?>
          <main id="main" class="">
<div id="content" class="content-area page-wrapper" role="main">
  <div class="row row-main">
    <div class="large-12 col">
      <div class="col-inner">
        
        
                            
            <div class="woocommerce">

  <div class="woocommerce-info message-wrapper">
     <div class="message-container container medium-text-center">
       Bạn đã có tài khoản? <a class="showlogin">Ấn vào đây để đăng nhập</a>     </div>
  </div>
  <style>
    .login-toggle
    {
      display: none;
    }
  </style>
<script src="js/jquery.min.js"></script>
  <script>
  $(document).ready(function(){
      $( ".showlogin" ).click(function() {
      $( ".login-toggle" ).slideToggle( "slow" );
      });


      $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                $('.create-account').css('display','block');
            }
            else if($(this).prop("checked") == false){
                $('.create-account').css('display','none');
                $('.woocommerce-validated').css('display','block');
            }
        });
  });
  </script>
<form class="woocomerce-form woocommerce-form-login login login-toggle" method="post" style="">

  
  <p>Nếu trước đây bạn đã mua hàng của chúng tôi, vui lòng ghi đầy đủ thông tin trong các hộp dưới đây. Nếu bạn là khách hàng mới, vui lòng chuyển tới phần Đơn hàng &amp; Giao hàng.</p>

  <p class="form-row form-row-first">
    <label for="username">Tên đăng nhập hoặc email <span class="required">*</span></label>
    <input type="text" class="input-text" name="username" id="username">
  </p>
  <p class="form-row form-row-last">
    <label for="password">Mật khẩu <span class="required">*</span></label>
    <input class="input-text" type="password" name="password" id="password">
  </p>
  <div class="clear"></div>

  
  <p class="form-row">
    <input type="hidden" id="_wpnonce" name="_wpnonce" value="750d78bc48"><input type="hidden" name="_wp_http_referer" value="/thanh-toan/lost-password/">    <input type="submit" class="button" name="login" value="Đăng nhập">
    <input type="hidden" name="redirect" value="https://zito.vn/thanh-toan/">
    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
      <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever"> <span>Ghi nhớ mật khẩu</span>
    </label>
  </p>
  <p class="lost_password">
    <a href="https://zito.vn/thanh-toan/lost-password/">Quên mật khẩu?</a>
  </p>

  <div class="clear"></div>

  
</form>


<form name="checkout" method="post" class="checkout woocommerce-checkout" action="https://zito.vn/thanh-toan/" enctype="multipart/form-data" novalidate="novalidate">

  <div class="row pt-0 ">
    <div class="large-7 col  ">
    
      
      <div id="customer_details">
        <div class="clear">
          <div class="woocommerce-billing-fields">
  
    <h3>Thông tin đặt hàng</h3>

  
  
  <div class="woocommerce-billing-fields__field-wrapper">
          <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field" data-priority="10"><label for="billing_first_name" class="">Họ và Tên <abbr class="required" title="bắt buộc">*</abbr></label><input type="text" class="input-text " name="billing_first_name" id="billing_first_name" placeholder="" value="" autocomplete="given-name" autofocus="autofocus"></p>         <p class="form-row form-row-wide address-field validate-required" id="billing_address_1_field" data-priority="50"><label for="billing_address_1" class="">Địa chỉ <abbr class="required" title="bắt buộc">*</abbr></label><input type="text" class="input-text " name="billing_address_1" id="billing_address_1" placeholder="Số nhà - Tên đường - Quận/Huyện..." value="" autocomplete="address-line1"></p>          <p class="form-row form-row-wide address-field validate-required" id="billing_city_field" data-priority="70"><label for="billing_city" class="">Tỉnh / Thành phố <abbr class="required" title="bắt buộc">*</abbr></label><input type="text" class="input-text " name="billing_city" id="billing_city" placeholder="" value="" autocomplete="address-level2"></p>          <p class="form-row form-row-first validate-required validate-phone" id="billing_phone_field" data-priority="100"><label for="billing_phone" class="">Số điện thoại <abbr class="required" title="bắt buộc">*</abbr></label><input type="tel" class="input-text " name="billing_phone" id="billing_phone" placeholder="" value="" autocomplete="tel"></p>          <p class="form-row form-row-last validate-required validate-email" id="billing_email_field" data-priority="110"><label for="billing_email" class="">Địa chỉ email <abbr class="required" title="bắt buộc">*</abbr></label><input type="email" class="input-text " name="billing_email" id="billing_email" placeholder="" value="" autocomplete="email username"></p>      </div>

  </div>

  <div class="woocommerce-account-fields">
    
      <p class="form-row form-row-wide create-account woocommerce-validated">
        <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
          <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox check-pass" id="createaccount" type="checkbox" name="createaccount" value="1"> <span>Tạo tài khoản mới?</span>
        </label>
      </p>

    
    
    
      <div class="create-account" style="display: none;">
                  <p class="form-row validate-required" id="account_password_field" data-priority=""><label for="account_password" class="">Mật khẩu tài khoản <abbr class="required" title="bắt buộc">*</abbr></label><input type="password" class="input-text " name="account_password" id="account_password" placeholder="Mật khẩu" value=""></p>                <div class="clear"></div>
      </div>

    
      </div>
        </div>
        <div class="clear">
          <div class="woocommerce-shipping-fields">
  </div>
<div class="woocommerce-additional-fields">
  
  
    
      <h3>Thông tin thêm</h3>

    
    <div class="woocommerce-additional-fields__field-wrapper">
              <p class="form-row notes" id="order_comments_field" data-priority=""><label for="order_comments" class="">Ghi chú thêm về đơn hàng</label><textarea name="order_comments" class="input-text " id="order_comments" placeholder="Ví dụ thời gian giao hàng, địa chỉ giao hàng..." rows="2" cols="5"></textarea></p>         </div>

  
  </div>
        </div>
      </div>

      
        </div><!-- large-7 -->

    <div class="large-5 col">
      <div class="col-inner has-border">
        <div class="checkout-sidebar sm-touch-scroll">
          <h3 id="order_review_heading">Đơn hàng của bạn</h3>
          
          <div id="order_review" class="woocommerce-checkout-review-order">
            <table class="shop_table woocommerce-checkout-review-order-table">
  <thead>
    <tr>
      <th class="product-name">Sản phẩm</th>
      <th class="product-total">Tổng cộng</th>
    </tr>
  </thead>
  <tbody>
              <tr class="cart_item">
            <td class="product-name">
              Bàn Trà ZITO ZB 007&nbsp;              <strong class="product-quantity">× 1</strong>                          </td>
            <td class="product-total">
              <span class="woocommerce-Price-amount amount">5.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span>           </td>
          </tr>
                    <tr class="cart_item">
            <td class="product-name">
              Gối ZP Mèo 03&nbsp;              <strong class="product-quantity">× 1</strong>                          </td>
            <td class="product-total">
              <span class="woocommerce-Price-amount amount">220.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span>           </td>
          </tr>
                    <tr class="cart_item">
            <td class="product-name">
              Bàn Trà ZITO ZB 003&nbsp;              <strong class="product-quantity">× 1</strong>                          </td>
            <td class="product-total">
              <span class="woocommerce-Price-amount amount">3.800.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span>           </td>
          </tr>
            </tbody>
  <tfoot>

    <tr class="cart-subtotal">
      <th>Tổng phụ</th>
      <td><span class="woocommerce-Price-amount amount">9.020.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span></td>
    </tr>

    
    
    
    
    
    <tr class="order-total">
      <th>Tổng cộng</th>
      <td><strong><span class="woocommerce-Price-amount amount">9.020.000&nbsp;<span class="woocommerce-Price-currencySymbol">₫</span></span></strong> </td>
    </tr>

    
  </tfoot>
</table>

<div id="payment" class="woocommerce-checkout-payment">
      <ul class="wc_payment_methods payment_methods methods">
      <li class="wc_payment_method payment_method_bacs">
  <input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="bacs" checked="checked" data-order_button_text="">

  <label for="payment_method_bacs">
    Chuyển khoản ngân hàng  </label>
      <div class="payment_box payment_method_bacs">
      <p>Thực hiện thanh toán vào ngay tài khoản ngân hàng của chúng tôi. Vui lòng sử dụng ID Đơn hàng của bạn như một phần để tham khảo khi thanh toán. Đơn hàng của bạn sẽ không được vận chuyển cho tới khi tiền được gửi vào tài khoản của chúng tôi.</p>
    </div>
  </li>
<li class="wc_payment_method payment_method_cod">
  <input id="payment_method_cod" type="radio" class="input-radio" name="payment_method" value="cod" data-order_button_text="">

  <label for="payment_method_cod">
    Thanh toán khi nhận hàng COD  </label>
      <div class="payment_box payment_method_cod" style="display:none;">
      <p>Quý khách sẽ phải trả $ sau khi nhận hàng</p>
    </div>
  </li>
    </ul>
    <div class="form-row place-order">
    <noscript>
      Trình duyệt của bạn không hỗ trợ JavaScript, hoặc nó bị vô hiệu hóa, hãy đảm bảo bạn nhấp vào &lt;em&gt; Cập nhật giỏ hàng &lt;/ em&gt; trước khi bạn thanh toán. Bạn có thể phải trả nhiều hơn số tiền đã nói ở trên, nếu bạn không làm như vậy.     &lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Cập nhật tổng" /&gt;
    </noscript>

    
    
    <input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Đặt hàng" data-value="Đặt hàng">
    
    <input type="hidden" id="_wpnonce" name="_wpnonce" value="66f404cb12"><input type="hidden" name="_wp_http_referer" value="/thanh-toan/lost-password/?wc-ajax=update_order_review">  </div>
</div>

          </div>
          <div class="html-checkout-sidebar pt-half"></div>       </div>
      </div>
    </div><!-- large-5 -->

  </div><!-- row -->
</form>

</div>

            
                        </div><!-- .col-inner -->
    </div><!-- .large-12 -->
  </div><!-- .row -->
</div>


</main>
         <?php include('footer/footer.php')?>
         <!-- .footer-wrapper -->
      </div>
      <!-- #wrapper -->
      <!-- Mobile Sidebar -->
      <?php include('header/header-mobile.php')?>
      <!-- #mobile-menu -->
      <div id="login-form-popup" class="lightbox-content mfp-hide">
         <div class="account-container lightbox-inner">
            <div class="account-login-inner">
               <h3 class="uppercase">Đăng nhập</h3>
               <form method="post" class="login">
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                     <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                  </p>
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="password">Mật khẩu <span class="required">*</span></label>
                     <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                  </p>
                  <p class="form-row">
                     <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />            <input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                     <label for="rememberme" class="inline">
                     <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu          </label>
                  </p>
                  <p class="woocommerce-LostPassword lost_password">
                     <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                  </p>
               </form>
            </div>
            <!-- .login-inner -->
         </div>
         <!-- .account-login-container -->
      </div>
      <link rel='stylesheet' id='flatsome-effects-css'  href='css/effects.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/scripts.js'></script>
      <script type='text/javascript' src='js/add-to-cart.min.js'></script>
      <script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
      <script type='text/javascript' src='js/js.cookie.min.js'></script>     
      <script type='text/javascript' src='js/woocommerce.min.js'></script>
      <script type='text/javascript' src='js/cart-fragments.min.js'></script>
      <script type='text/javascript' src='js/yith-autocomplete.min.js'></script>
      <script type='text/javascript' src='js/flatsome-live-search.js'></script>
      <script type='text/javascript' src='js/hoverIntent.min.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
            /* ]]> */
            
      </script>
      <script type='text/javascript' src='js/flatsome.js'></script>
      <script type='text/javascript' src='js/flatsome-lazy-load.js'></script>
      <script type='text/javascript' src='js/woocommerce.js'></script>
      <script type='text/javascript' src='js/wp-embed.min.js'></script>
      <script type='text/javascript' src='js/packery.pkgd.min.js'></script>
      <script type='text/javascript' src='js/zxcvbn-async.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
   </body>
</html>