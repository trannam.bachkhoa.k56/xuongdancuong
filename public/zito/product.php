<!DOCTYPE html>

      <html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js">
         <!--<![endif]-->
         <head>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
         
            <title>Nội Thất ZITO | Showroom Nội Thất Gỗ Cao Cấp Tại Hà Nội</title>
            <!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
            <meta name="description" content="Nội thất ZITO tiên phong cung cấp các sản phẩm Sofa gỗ, da, nỉ theo kích thước tùy chỉnh, xu hướng mới nhất Showroom số 15 Dương Đình Nghệ Cầu Giấy Hà Nội"/>
            <meta name="robots" content="noodp"/>
			<base href="http://vn3c.net/zito/">
            <style type="text/css">
               img.wp-smiley,
               img.emoji {
               display: inline !important;
               border: none !important;
               box-shadow: none !important;
               height: 1em !important;
               width: 1em !important;
               margin: 0 .07em !important;
               vertical-align: -0.1em !important;
               background: none !important;
               padding: 0 !important;
               }
            </style>
			<link rel="stylesheet" href="css/font-awesome.min.css">

            <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' type='text/css' media='all' />
            <link rel='stylesheet' id='yith_wcas_frontend-css'  href='css/yith_wcas_ajax_search.css' type='text/css' media='all' />
            <style id='yith_wcas_frontend-inline-css' type='text/css'>
               .autocomplete-suggestion{
               padding-right: 20px;
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
               .autocomplete-suggestion  span.yith_wcas_result_on_sale{
               background: #7eb742;
               color: #ffffff
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
               .autocomplete-suggestion  span.yith_wcas_result_outofstock{
               background: #7a7a7a;
               color: #ffffff
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
               .autocomplete-suggestion  span.yith_wcas_result_featured{
               background: #c0392b;
               color: #ffffff
               }
               .autocomplete-suggestion img{
               width: 50px;
               }
               .autocomplete-suggestion .yith_wcas_result_content .title{
               color: #004b91;
               }
               .autocomplete-suggestion{
               min-height: 60px;
               }
            </style>
            <link rel='stylesheet' id='flatsome-main-css'  href='css/flatsome.css' type='text/css' media='all' />
            <link rel='stylesheet' id='flatsome-shop-css'  href='css/flatsome-shop.css' type='text/css' media='all' />
            <link rel='stylesheet' id='flatsome-style-css'  href='css/style1.css' type='text/css' media='all' />
            <script type='text/javascript' src='js/jquery.js'></script>
            <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
            <script type='text/javascript' src='js/advanced-cf7-db-public.js'></script>
         
           
            <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>
       
       
            
 
         </head>
         <body>
     
         </body>
         <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
         <noscript>
            <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
         </noscript>
         <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
         <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
         <link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
         <meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
         <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>
         </head>
         <body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
            <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
            <div id="wrapper">
            <?php include('header/header.php')?>
			
            
				  
			 <div class="shop-page-title category-page-title page-title featured-title dark ">
               <div class="page-title-bg fill">
                  <div class="title-bg fill bg-fill" data-parallax-fade="true" data-parallax="-2" data-parallax-background data-parallax-container=".page-title"></div>
                  <div class="title-overlay fill"></div>
               </div>
               <div class="page-title-inner flex-row  medium-flex-wrap container">
                  <div class="flex-col flex-grow medium-text-center">
                     <div class="is-large">
                        <nav class="woocommerce-breadcrumb breadcrumbs"><a href="https://zito.vn">Trang chủ</a> <span class="divider">&#47;</span> <a href="https://zito.vn/shop/">Cửa hàng</a> <span class="divider">&#47;</span> Sofa Gỗ</nav>
                     </div>
                     <div class="category-filtering category-filter-row show-for-medium">
                        <a href="#" data-open="#shop-sidebar" data-visible-after="true" data-pos="left" class="filter-button uppercase plain">
                        <i class="icon-menu"></i>
                        <strong>Lọc</strong>
                        </a>
                        <div class="inline-block">
                        </div>
                     </div>
                  </div>
                  <!-- .flex-left -->
                  <div class="flex-col medium-text-center  form-flat">
                     <p class="woocommerce-result-count hide-for-medium">
                        Showing 1&ndash;12 of 21 results
                     </p>
                     <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby">
                           <option value="popularity" >Thứ tự theo mức độ phổ biến</option>
                           <option value="rating" >Thứ tự theo điểm đánh giá</option>
                           <option value="date"  selected='selected'>Thứ tự theo sản phẩm mới</option>
                           <option value="price" >Thứ tự theo giá: thấp đến cao</option>
                           <option value="price-desc" >Thứ tự theo giá: cao xuống thấp</option>
                        </select>
                     </form>
                  </div>
                  <!-- .flex-right -->
               </div>
               <!-- flex-row -->
            </div>
            <!-- .page-title -->
            <main id="main" class="">
               <div class="row category-page-row">
			   <?php include('block/sidebar-ct.php')?>
			    <!-- #shop-sidebar -->
				<div class="col large-9">
		<div class="row">
			<div class="large-6 col">
				
<div class="product-images relative mb-half has-hover woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4">

  <div class="badge-container is-larger absolute left top z-1">
</div>
  <div class="image-tools absolute top show-on-hover right z-3">
      </div>

  <figure class="woocommerce-product-gallery__wrapper product-gallery-slider slider slider-nav-small mb-half has-image-zoom"
        data-flickity-options='{
                "cellAlign": "center",
                "wrapAround": true,
                "autoPlay": false,
                "prevNextButtons":true,
                "adaptiveHeight": true,
                "imagesLoaded": true,
                "lazyLoad": 1,
                "dragThreshold" : 15,
                "pageDots": false,
                "rightToLeft": false       }'>
    <div data-thumb="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg" class="first slide woocommerce-product-gallery__image"><a href="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg"><img width="520" height="320" src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-520x320.jpg" class="attachment-shop_single size-shop_single wp-post-image" alt="Sofa gỗ tự nhiên" title="sofa go ZITO ZG121 1" data-caption="" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg" data-large_image="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg" data-large_image_width="1600" data-large_image_height="903" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-520x320.jpg 520w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-260x160.jpg 260w" sizes="(max-width: 520px) 100vw, 520px" /></a></div><div data-thumb="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg" class="woocommerce-product-gallery__image slide"><a href="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg"><img width="520" height="320" src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-520x320.jpg" class="attachment-shop_single size-shop_single" alt="Sofa gỗ tự nhiên" title="sofa go ZITO ZG121 1" data-caption="" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg" data-large_image="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg" data-large_image_width="1600" data-large_image_height="903" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-520x320.jpg 520w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-260x160.jpg 260w" sizes="(max-width: 520px) 100vw, 520px" /></a></div><div data-thumb="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-130x80.jpg" class="woocommerce-product-gallery__image slide"><a href="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121.jpg"><img width="520" height="320" src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-520x320.jpg" class="attachment-shop_single size-shop_single" alt="sofa gỗ đẹp tự nhiên" title="sofa go ZITO ZG121" data-caption="" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121.jpg" data-large_image="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121.jpg" data-large_image_width="1600" data-large_image_height="903" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-520x320.jpg 520w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-260x160.jpg 260w" sizes="(max-width: 520px) 100vw, 520px" /></a></div>  </figure>

  <div class="image-tools absolute bottom left z-3">
        <a href="#product-zoom" class="zoom-button button is-outline circle icon tooltip hide-for-small" title="Zoom">
      <i class="icon-expand" ></i>    </a>
   </div>
</div>


  <div class="product-thumbnails thumbnails slider-no-arrows slider row row-small row-slider slider-nav-small small-columns-4"
    data-flickity-options='{
              "cellAlign": "left",
              "wrapAround": false,
              "autoPlay": false,
              "prevNextButtons":true,
              "asNavFor": ".product-gallery-slider",
              "percentPosition": true,
              "imagesLoaded": true,
              "pageDots": false,
              "rightToLeft": false,
              "contain": true
          }'
    >      <div class="col is-nav-selected first"><a><img width="130" height="80" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Sofa gỗ tự nhiên" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" /></a></div>
    <div class="col"><a class="" title="" ><img width="130" height="80" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail" alt="sofa go ZITO ZG121 1" title="sofa go ZITO ZG121 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" /></a></div><div class="col"><a class="" title="" ><img width="130" height="80" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail" alt="sofa go ZITO ZG121" title="sofa go ZITO ZG121" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" /></a></div>  </div><!-- .product-thumbnails -->
  
			</div>


			<div class="product-info summary entry-summary col col-fit product-summary">
				<nav class="woocommerce-breadcrumb breadcrumbs"><a href="https://zito.vn">Trang chủ</a> <span class="divider">&#47;</span> <a href="https://zito.vn/shop/">Cửa hàng</a> <span class="divider">&#47;</span> <a href="https://zito.vn/danh-muc/san-pham-noi-bat/">Sản Phẩm Nổi Bật</a></nav><h1 class="product-title entry-title">
	Sofa gỗ ZG121</h1>

	<div class="is-divider small"></div>

	<div class="woocommerce-product-rating">
		<div class="star-rating">
			<span style="width:100%">
				<strong class="rating">5.00</strong> out of <span>5</span>				dựa trên <span class="rating">3</span> bình chọn của khách hàng			</span>
		</div>
		<a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<span class="count">3</span> đánh giá của khách hàng)</a>	</div>

<div class="price-wrapper">
	<p class="price product-page-price ">
  <span class="woocommerce-Price-amount amount">23.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></p>
</div>
<div class="product-short-description">
	<ul>
<li>Kiểu dáng            :  Gồm 1 văng ghế dài và 2 ghế đơn</li>
<li>Kích thước           : Tùy chỉnh</li>
<li>Chất liệu khung   : Gỗ sồi Nga</li>
<li>Chất liệu bọc       : Da Hàn Quốc</li>
<li>Ưu điểm nổi bật  : Tay gỗ lớn rộng, tận dụng như một giá sách hay trang trí những vật nhỏ xinh .</li>
<li>Miễn phí vận chuyển nội thành</li>
<li>Bảo hành 2 năm</li>
<li><em>Quý khách hàng có thể thay đổi chất liệu gỗ, chất liệu bọc hay màu sắc theo ý thích</em></li>
</ul>
</div>
 
	
	<form class="cart" method="post" enctype='multipart/form-data'>
		  <div class="quantity buttons_added">
    <input type="button" value="-" class="minus button is-form">    <input type="number" class="input-text qty text" step="1" min="1" max="9999" name="quantity" value="1" title="SL" size="4" pattern="[0-9]*" inputmode="numeric" />
    <input type="button" value="+" class="plus button is-form">  </div>
  
		<button type="submit" name="add-to-cart" value="4201" class="single_add_to_cart_button button alt">Thêm vào giỏ</button>

			</form>

	
<div class="buttonlhe"><a class="button primary" href="#dathang" target="_self"><i class="fa fa-commenting-o" aria-hidden="true"></i>	Yêu cầu tư vấn</a></div>
<div class="buttonlhe2"><a class="button alert" href="tel:0931050000"><i class="fa fa-phone faa-ring faa-slow animated"></i>0931.05.0000</a></div>


<p><div id="dathang"
    class="lightbox-by-id lightbox-content mfp-hide lightbox-white "
    style="max-width:800px ;padding:20px">
    <div class="nz-dathang nz-dathang-1"><div class="nz-tensp">Sofa gỗ ZG121</div><div class="nz-anhsp"><img width="300" height="169" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Sofa gỗ ZG121" title="Sofa gỗ ZG121" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-768x433.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-1024x578.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg 1600w" sizes="(max-width: 300px) 100vw, 300px" /></div><div class="nz-giasp">Giá: 23.600.000</div><div class="nz-motasp"><ul>
<li>Kiểu dáng            :  Gồm 1 văng ghế dài và 2 ghế đơn</li>
<li>Kích thước           : Tùy chỉnh</li>
<li>Chất liệu khung   : Gỗ sồi Nga</li>
<li>Chất liệu bọc       : Da Hàn Quốc</li>
<li>Ưu điểm nổi bật  : Tay gỗ lớn rộng, tận dụng như một giá sách hay trang trí những vật nhỏ xinh .</li>
<li>Miễn phí vận chuyển nội thành</li>
<li>Bảo hành 2 năm</li>
<li><em>Quý khách hàng có thể thay đổi chất liệu gỗ, chất liệu bọc hay màu sắc theo ý thích</em></li>
</ul>
</div></div><br />


<div class="nz-dathang nz-dathang-2">
<div class="nz-tensp">Thông tin của quý khách</div>
<div role="form" class="wpcf7" id="wpcf7-f8-p4201-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/shop/sofa-go/zg-121/#wpcf7-f8-p4201-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="8" />
<input type="hidden" name="_wpcf7_version" value="4.9.1" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f8-p4201-o1" />
<input type="hidden" name="_wpcf7_container_post" value="4201" />
</div>
<div class="nz-cf7">
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name" aria-required="true" aria-invalid="false" placeholder="Tên quý khách" /></span>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span>
</div>
<div class="nz-cf7-f1 hidden">
<span class="wpcf7-form-control-wrap giasp"><input type="text" name="giasp" value="23600000" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span><br />
<span class="wpcf7-form-control-wrap linksp"><input type="text" name="linksp" value="https://zito.vn:443/shop/sofa-go/zg-121/" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span><br />
<span class="wpcf7-form-control-wrap tde"><input type="text" name="tde" value="Sofa gỗ ZG121" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span><br />
<span class="wpcf7-form-control-wrap ttin"><input type="text" name="ttin" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span></p>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="your-phone" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" id="your-message" aria-invalid="false" placeholder="Lời nhắn thêm (có thể để trống)"></textarea></span>
</div>
<div class="nz-cf7-f1">
<input type="submit" value="Gửi tin tư vấn" class="wpcf7-form-control wpcf7-submit c-button" />
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
<div class="nz-tensp">Hoặc liên hệ với chúng tôi</div>
<strong>Công ty Cổ phần Nội thất ZITO</strong></br>
<strong>Showroom:</strong> 15 Dương Đình Nghệ, Yên Hoà, Cầu Giấy, Hà Nội</br>
<strong>Tel:</strong> (024) 2212 4111 / 0931.05.0000</br>
<strong>Email:</strong> zitosofa@gmail.com</br>
<strong>Số ĐKKD:</strong> 0107889261 - Cấp bởi Sở kế Hoạch Đầu Tư Thành phố Hà Nội
</div>
</div>

</p><div class="product_meta">

	
	
	<span class="posted_in">Danh mục: <a href="https://zito.vn/danh-muc/san-pham-noi-bat/" rel="tag">Sản Phẩm Nổi Bật</a>, <a href="https://zito.vn/danh-muc/sofa-go/" rel="tag">Sofa Gỗ</a></span>
	<span class="tagged_as">Từ khóa: <a href="https://zito.vn/tu-khoa/sofa-go-dem-ni/" rel="tag">sofa gỗ đệm nỉ</a>, <a href="https://zito.vn/tu-khoa/sofa-go-goc-chu-l/" rel="tag">sofa gỗ góc chữ L</a></span>
	
</div>

<div class="social-icons share-icons share-row relative icon-style-fill " ><a href="//www.facebook.com/sharer.php?u=https://zito.vn/shop/sofa-go/zg-121/" data-label="Facebook" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip facebook" title="Share on Facebook"><i class="icon-facebook" ></i></a><a href="//twitter.com/share?url=https://zito.vn/shop/sofa-go/zg-121/" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip twitter" title="Share on Twitter"><i class="icon-twitter" ></i></a><a href="mailto:enteryour@addresshere.com?subject=Sofa%20g%E1%BB%97%20ZG121&amp;body=Check%20this%20out:%20https://zito.vn/shop/sofa-go/zg-121/" rel="nofollow" class="icon primary button circle tooltip email" title="Email to a Friend"><i class="icon-envelop" ></i></a><a href="//pinterest.com/pin/create/button/?url=https://zito.vn/shop/sofa-go/zg-121/&amp;media=https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-1024x578.jpg&amp;description=Sofa%20g%E1%BB%97%20ZG121" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip pinterest" title="Pin on Pinterest"><i class="icon-pinterest" ></i></a><a href="//plus.google.com/share?url=https://zito.vn/shop/sofa-go/zg-121/" target="_blank" class="icon primary button circle tooltip google-plus" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" title="Share on Google+"><i class="icon-google-plus" ></i></a><a href="//www.linkedin.com/shareArticle?mini=true&url=https://zito.vn/shop/sofa-go/zg-121/&title=Sofa%20g%E1%BB%97%20ZG121" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;"  rel="nofollow" target="_blank" class="icon primary button circle tooltip linkedin" title="Share on LinkedIn"><i class="icon-linkedin" ></i></a></div>
			</div><!-- .summary -->


			</div><!-- .row -->
		
            
                  
                        
                        <!-- row -->
						<div class="product-footer">
			<div class="product-page-sections">
		<div class="product-section">
	<div class="row">
		<div class="large-2 col pb-0 mb-0">
			 <h5 class="uppercase mt">Mô tả</h5>
		</div>

		<div class="large-10 col pb-0 mb-0">
			<div class="panel entry-content">
				

<p><em>Mỗi mẫu sofa đều mang một vẻ đẹp khác nhau, mẫu <strong>Sofa gỗ tự nhiên</strong> ZG121 dựa trên sự đơn giản tạo ra những điểm khác biệt về không gian. Màu gốc của gỗ là màu vàng với ý tưởng tự nhiên, phù hợp với không gian thiên nhiên tươi mới. Màu vàng còn là màu sắc cực kỳ dễ kết hợp với các loại phụ kiện decor như: bàn  trà, thảm hay đơn giản là những chậu cây&#8230; Bên cạnh đó màu vàng của <strong><a href="https://zito.vn/danh-muc/sofa-go/">Sofa Gỗ</a> tự nhiên cao cấp</strong> <strong>ZG121</strong> còn là màu tuyệt vời trong việc áp dụng thuật Phong Thủy vào bất kỳ nơi nào trong nhà của bạn. Mẫu Sofa gỗ tự nhiên ZG121 được thiết kế với phong cách hiện đại trẻ trung cực kỳ phù hợp với các gia đình trẻ năng động.</em></p>
<p><img class="lazy-load aligncenter wp-image-4202" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg" alt="Sofa gỗ tự nhiên" width="750" height="423" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg 1600w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-768x433.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-1024x578.jpg 1024w" sizes="(max-width: 750px) 100vw, 750px" /></p>
<h2>Chi tiết sofa gỗ tự nhiên <a href="https://zito.vn/shop/sofa-go/zg-121/">ZG121</a></h2>
<ul>
<li>Được <a href="http://tanhoangminhhomes.vn/toa-c6-d-capitale-tran-duy-hung.html">thiết kế</a> theo phong cách sofa đối diện, gồm 1 văng ghế dài và 2 văng ghế đơn. ZG121  mang lại khoảnh khắc quây quần bên gia đình hay những lần tụ họp bạn bè.</li>
<li>ZG121 được làm từ gỗ sồi Nga với đường vân gỗ sáng, sọc tự nhiên cực kỳ đẹp mắt. Cùng với đó là mùi thơm dịu dàng , nhẹ nhàng vốn có, đủ sức để mê hoặc người đối diện.</li>
<li>Đệm mút Vạn Thành bọc da Hàn Quốc cao cấp . Quý khách hàng có thể thay đổi chất liệu bọc nỉ hay vải, thay đổi màu sắc cho phù hợp với không gian chung.</li>
<li>Chân ghế  thiết kế vuông vức, mang lại độ chắc chắn cho người sử dụng</li>
<li>Tay vịn to, tỷ lệ vàng với cả bộ sofa</li>
</ul>
<h2>Ưu điểm nổi bật của sofa gỗ tự nhiên ZG121:</h2>
<ul>
<li>Toàn bộ sofa gỗ đẹp tự nhiên tại <a href="https://zito.vn/">ZITO</a> đều được thi công thiết kế đặc biệt. Mỗi mẫu đều mang những nét đặc sắc riêng. Tay Sofa Gỗ SG121 là kiểu tay tựa to, vuông vức , mang lại cảm giác hào phóng , thoái mái khi sử dụng. Đặc biệt tay còn kết hợp cả hộc để sách tiện lợi khi sử dụng.</li>
<li>Điểm linh hoặt của bộ sofa gỗ tự nhiên ZG121, gia chủ có thể sắp xếp sofa theo rất nhiều phong cách khác nhau.<a href="https://zito.vn/bat-mi-05-cach-bo-tri-sofa-cho-phong-khach/"> Tham khảo cách sắp xếp sofa</a></li>
<li>Lớp đệm dễ dàng tháo rời. Sofa gỗ được ưa chuộng bởi nó có thể sử dụng cho cả mùa đông và mùa hè.  Phù hợp với khí hậu gió mùa nóng ẩm thất thường của Việt Nam.</li>
</ul>
<p><img class="lazy-load aligncenter wp-image-4203" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121.jpg" alt="sofa gỗ đẹp tự nhiên" width="750" height="423" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121.jpg 1600w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-768x433.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1024x578.jpg 1024w" sizes="(max-width: 750px) 100vw, 750px" /></p>
<p>&gt;&gt;Xem thêm : <a href="https://zito.vn/chon-mau-sac-sofa-phu-hop-voi-khong-gian-song/">Lựa chọn màu sắc cho sofa phù hợp nhất </a></p><details class="nz-xemthem">

<summary>Đọc thêm</summary>

<p>&gt;&gt;Xem thêm :</p>
<blockquote><p>Quý khách hàng thắc mắc hãy liên hệ với ZITO theo hotline 093.105.0000 để được tư  vấn miễn phí 24/7 . Hoặc đến showroom tại số 15 Dương Đình Nghệ, Cầu Giấy, Hà Nội để chiêm ngưỡng và trải nghiệm sản phẩm thực thụ. ZITO cam kết sẽ mang lại cho quý khách những lợi ích tốt đẹp nhất.</p></blockquote>
</p>			</div>
		</div>
	</div>
	</div>
		<div class="product-section">
	<div class="row">
		<div class="large-2 col pb-0 mb-0">
			 <h5 class="uppercase mt">Tư vấn sản phẩm</h5>
		</div>

		<div class="large-10 col pb-0 mb-0">
			<div class="panel entry-content">
				<div class="nz-tu-van"><div class="nz-anhsp"><img width="300" height="169" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Sofa gỗ tự nhiên" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-768x433.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-1024x578.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg 1600w" sizes="(max-width: 300px) 100vw, 300px" /></div><div class="nz-sp2"><div class="nz-tensp2">Sofa gỗ ZG121</div><div class="nz-giasp2">23.600.000đ </div></div><div class="buttonlhe"><a class="button primary" href="#dathang" target="_self"><i class="fa fa-commenting-o" aria-hidden="true"></i> Yêu cầu tư vấn</a></div>
<div class="buttonlhe2"><a class="button alert" href="tel:0931050000"><i class="fa fa-phone faa-ring faa-slow animated"></i>0931.05.0000</a></div>


<p><div id="dathang"
    class="lightbox-by-id lightbox-content mfp-hide lightbox-white "
    style="max-width:800px ;padding:20px">
    <div class="nz-dathang nz-dathang-1"><div class="nz-tensp">Sofa gỗ ZG121</div><div class="nz-anhsp"><img width="300" height="169" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Sofa gỗ ZG121" title="Sofa gỗ ZG121" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-768x433.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-1024x578.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1.jpg 1600w" sizes="(max-width: 300px) 100vw, 300px" /></div><div class="nz-giasp">Giá: 23.600.000</div><div class="nz-motasp"><ul>
<li>Kiểu dáng            :  Gồm 1 văng ghế dài và 2 ghế đơn</li>
<li>Kích thước           : Tùy chỉnh</li>
<li>Chất liệu khung   : Gỗ sồi Nga</li>
<li>Chất liệu bọc       : Da Hàn Quốc</li>
<li>Ưu điểm nổi bật  : Tay gỗ lớn rộng, tận dụng như một giá sách hay trang trí những vật nhỏ xinh .</li>
<li>Miễn phí vận chuyển nội thành</li>
<li>Bảo hành 2 năm</li>
<li><em>Quý khách hàng có thể thay đổi chất liệu gỗ, chất liệu bọc hay màu sắc theo ý thích</em></li>
</ul>
</div></div><br />


<div class="nz-dathang nz-dathang-2">
<div class="nz-tensp">Thông tin của quý khách</div>
<div role="form" class="wpcf7" id="wpcf7-f8-p4201-o2" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/shop/sofa-go/zg-121/#wpcf7-f8-p4201-o2" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="8" />
<input type="hidden" name="_wpcf7_version" value="4.9.1" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f8-p4201-o2" />
<input type="hidden" name="_wpcf7_container_post" value="4201" />
</div>
<div class="nz-cf7">
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name" aria-required="true" aria-invalid="false" placeholder="Tên quý khách" /></span>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span>
</div>
<div class="nz-cf7-f1 hidden">
<span class="wpcf7-form-control-wrap giasp"><input type="text" name="giasp" value="23600000" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span><br />
<span class="wpcf7-form-control-wrap linksp"><input type="text" name="linksp" value="https://zito.vn:443/shop/sofa-go/zg-121/" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span><br />
<span class="wpcf7-form-control-wrap tde"><input type="text" name="tde" value="Sofa gỗ ZG121" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span><br />
<span class="wpcf7-form-control-wrap ttin"><input type="text" name="ttin" value="" size="40" class="wpcf7-form-control wpcf7dtx-dynamictext wpcf7-dynamictext" aria-invalid="false" /></span></p>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="your-phone" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" id="your-message" aria-invalid="false" placeholder="Lời nhắn thêm (có thể để trống)"></textarea></span>
</div>
<div class="nz-cf7-f1">
<input type="submit" value="Gửi tin tư vấn" class="wpcf7-form-control wpcf7-submit c-button" />
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
<div class="nz-tensp">Hoặc liên hệ với chúng tôi</div>
<strong>Công ty Cổ phần Nội thất ZITO</strong></br>
<strong>Showroom:</strong> 15 Dương Đình Nghệ, Yên Hoà, Cầu Giấy, Hà Nội</br>
<strong>Tel:</strong> (024) 2212 4111 / 0931.05.0000</br>
<strong>Email:</strong> zitosofa@gmail.com</br>
<strong>Số ĐKKD:</strong> 0107889261 - Cấp bởi Sở kế Hoạch Đầu Tư Thành phố Hà Nội
</div>
</div>

</p></div><div class="fb-like" data-href=" https://zito.vn/shop/sofa-go/zg-121/" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div>			</div>
		</div>
	</div>
	</div>
		<div class="product-section">
	<div class="row">
		<div class="large-2 col pb-0 mb-0">
			 <h5 class="uppercase mt">Đánh giá (3)</h5>
		</div>

		<div class="large-10 col pb-0 mb-0">
			<div class="panel entry-content">
				<div class="row" id="reviews">
	<div class="col large-12" id="comments">
		<h3 class="normal">3 reviews for <span>Sofa gỗ ZG121</span></h3>

		
			<ol class="commentlist">
				<li class="comment byuser comment-author-trongdung even thread-even depth-1" id="li-comment-15">
<div id="comment-15" class="comment_container review-item flex-row align-top">

	<div class="flex-col">
	<img alt='' src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src='https://secure.gravatar.com/avatar/4525c54d30390b2b026fdd017b98a56b?s=60&#038;d=mm&#038;r=g' srcset="" data-srcset='https://secure.gravatar.com/avatar/4525c54d30390b2b026fdd017b98a56b?s=120&amp;d=mm&amp;r=g 2x' class='lazy-load avatar avatar-60 photo' height='60' width='60' />	</div>

	<div class="comment-text flex-col flex-grow">

		
	<div class="star" style="color: #FDCC30; ">
		<i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i>
	</div>


	<p class="meta">
		<strong class="woocommerce-review__author" itemprop="author">trongdung</strong> <span class="woocommerce-review__dash">&ndash;</span> <time class="woocommerce-review__published-date" itemprop="datePublished" datetime="2017-12-01T10:48:54+00:00">1 Tháng Mười Hai, 2017</time>
	</p>

<div class="description"><p>Đẹp</p>
</div>	</div>
</div>
</li><!-- #comment-## -->
<li class="comment byuser comment-author-dungvpn odd alt thread-odd thread-alt depth-1" id="li-comment-16">
<div id="comment-16" class="comment_container review-item flex-row align-top">

	<div class="flex-col">
	<img alt='' src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src='https://secure.gravatar.com/avatar/e3ba4abe85948783c329d043a1bcbb35?s=60&#038;d=mm&#038;r=g' srcset="" data-srcset='https://secure.gravatar.com/avatar/e3ba4abe85948783c329d043a1bcbb35?s=120&amp;d=mm&amp;r=g 2x' class='lazy-load avatar avatar-60 photo' height='60' width='60' />	</div>

	<div class="comment-text flex-col flex-grow">

		
	<div class="star" style="color: #FDCC30; ">
		<i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i>
	</div>


	<p class="meta">
		<strong class="woocommerce-review__author" itemprop="author">Dungvpn</strong> <span class="woocommerce-review__dash">&ndash;</span> <time class="woocommerce-review__published-date" itemprop="datePublished" datetime="2017-12-01T14:31:50+00:00">1 Tháng Mười Hai, 2017</time>
	</p>

<div class="description"><p>Tuyệt vời</p>
</div>	</div>
</div>
</li><!-- #comment-## -->
<li class="comment byuser comment-author-maihoa even thread-even depth-1" id="li-comment-17">
<div id="comment-17" class="comment_container review-item flex-row align-top">

	<div class="flex-col">
	<img alt='' src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src='https://secure.gravatar.com/avatar/307ffa7b958e5659e7afcc5daba55093?s=60&#038;d=mm&#038;r=g' srcset="" data-srcset='https://secure.gravatar.com/avatar/307ffa7b958e5659e7afcc5daba55093?s=120&amp;d=mm&amp;r=g 2x' class='lazy-load avatar avatar-60 photo' height='60' width='60' />	</div>

	<div class="comment-text flex-col flex-grow">

		
	<div class="star" style="color: #FDCC30; ">
		<i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i>
	</div>


	<p class="meta">
		<strong class="woocommerce-review__author" itemprop="author">Mai Hoa</strong> <span class="woocommerce-review__dash">&ndash;</span> <time class="woocommerce-review__published-date" itemprop="datePublished" datetime="2017-12-01T23:49:30+00:00">1 Tháng Mười Hai, 2017</time>
	</p>

<div class="description"><p>mẫu mã sang trọng quá</p>
</div>	</div>
</div>
</li><!-- #comment-## -->
			</ol>

						</div>

			<div id="review_form_wrapper" class="large-12 col">
			<div id="review_form" class="col-inner">
			  <div class="review-form-inner has-border">
					<div id="respond" class="comment-respond">
		<h3 id="reply-title" class="comment-reply-title">Thêm đánh giá <small><a rel="nofollow" id="cancel-comment-reply-link" href="/shop/sofa-go/zg-121/#respond" style="display:none;">Hủy</a></small></h3>			<form action="https://zito.vn/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate>
				<p class="comment-form-rating"><label for="rating">Your rating</label><select name="rating" id="rating">
							<option value="">Xếp hạng&hellip;</option>
							<option value="5">Rất tốt</option>
							<option value="4">Tốt</option>
							<option value="3">Trung bình</option>
							<option value="2">Không tệ</option>
							<option value="1">Rất Tệ</option>
						</select></p><p class="comment-form-comment"><label for="comment">Your review</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p><p class="comment-form-author"><label for="author">Tên <span class="required">*</span></label> <input id="author" name="author" type="text" value="" size="30" aria-required="true" /></p>
<p class="comment-form-email"><label for="email">Email <span class="required">*</span></label> <input id="email" name="email" type="text" value="" size="30" aria-required="true" /></p>
<p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Gửi đi" /> <input type='hidden' name='comment_post_ID' value='4201' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p><p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="10568bee51" /></p><p style="display: none;"><input type="hidden" id="ak_js" name="ak_js" value="141"/></p>			</form>
			</div><!-- #respond -->
				  </div>
			</div>
		</div>

	
</div>
			</div>
		</div>
	</div>
	</div>
	</div>

                     
                     </div>
					  <div class="related related-products-wrapper product-section">

    <h3 class="product-section-title product-section-title-related pt-half pb-half uppercase">
      Sản phẩm liên quan    </h3>

      
  
    <div class="row large-columns-6 medium-columns-3 small-columns-2 row-small">
  
      
        
<div class="product-small col has-hover post-4196 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-none">
				<a href="https://zito.vn/shop/sofa-go/zg-120/">
					<img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="sofa gỗ phòng khách" title="sofa go ZITO zg 120 3" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-120/">Sofa gỗ ZG 120</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">19.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->

      
        
<div class="product-small col has-hover post-183 product type-product status-publish has-post-thumbnail product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-none">
				<a href="https://zito.vn/shop/sofa-go/zg-106/">
					<img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 106" title="Sofa Gỗ ZG 106 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-106/">Sofa Gỗ ZG 106</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">20.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->

      
        
<div class="product-small col has-hover post-166 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l last instock shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-none">
				<a href="https://zito.vn/shop/sofa-go/zg-104/">
					<img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 104" title="Sofa Gỗ ZG 104 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-104-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-104/">Sofa Gỗ ZG 104</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">21.300.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->

      
        
<div class="product-small col has-hover post-219 product type-product status-publish has-post-thumbnail product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-vang product_tag-sofa-go-dem-ni product_tag-sofa-phong-khach-nho first instock shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-none">
				<a href="https://zito.vn/shop/sofa-go/zg-115/">
					<img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="giá sofa dẹp" title="Sofa Gỗ ZG 115 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-115/">Sofa Gỗ ZG 115</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">25.300.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->

      
        
<div class="product-small col has-hover post-187 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-none">
				<a href="https://zito.vn/shop/sofa-go/zg-107/">
					<img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 107" title="Sofa Gỗ ZG 107 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-107-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-107/">Sofa Gỗ ZG 107</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">19.400.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->

      
        
<div class="product-small col has-hover post-167 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
	<div class="col-inner">
	
<div class="badge-container absolute left top z-1">
</div>
	<div class="product-small box ">
		<div class="box-image">
			<div class="image-none">
				<a href="https://zito.vn/shop/sofa-go/zg-105/">
					<img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 105" title="Sofa Gỗ ZG 105 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/ZG-105-3-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
							</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
							</div>
			<div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
							</div>
					</div><!-- box-image -->

		<div class="box-text box-text-products text-center grid-style-2">
			<div class="title-wrapper"><p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-105/">Sofa Gỗ ZG 105</a></p></div><div class="price-wrapper">
	<span class="price"><span class="woocommerce-Price-amount amount">21.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
</div>		</div><!-- box-text -->
	</div><!-- box -->
		</div><!-- .col-inner -->
</div><!-- col -->

      
      </div>
  </div>

                     <!-- shop container -->
                  </div>
               
				
			   </div>
            </main>
            <!-- #main -->	  
            <!-- #main -->
			
			
           
			<?php include('footer/footer.php')?>
			
            <!-- .footer-wrapper -->
            </div>
			<!-- #wrapper -->
			
			
         
            
      
			
            <div id="login-form-popup" class="lightbox-content mfp-hide">
               <div class="account-container lightbox-inner">
                  <div class="account-login-inner">
                     <h3 class="uppercase">Đăng nhập</h3>
                     <form method="post" class="login">
                        <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                           <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                           <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                        </p>
                        <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                           <label for="password">Mật khẩu <span class="required">*</span></label>
                           <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                        </p>
                        <p class="form-row">
                           <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />				<input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                           <label for="rememberme" class="inline">
                           <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu				</label>
                        </p>
                        <p class="woocommerce-LostPassword lost_password">
                           <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                        </p>
                     </form>
                  </div>
                  <!-- .login-inner -->
               </div>
               <!-- .account-login-container -->
            </div>
            <link rel='stylesheet' id='flatsome-effects-css'  href='css/effects.css' type='text/css' media='all' />
   
            <script type='text/javascript' src='js/scripts.js'></script>
            <script type='text/javascript' src='js/add-to-cart.min.js'></script>
            <script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
            <script type='text/javascript' src='js/js.cookie.min.js'></script>     
            <script type='text/javascript' src='js/woocommerce.min.js'></script>
            <script type='text/javascript' src='js/cart-fragments.min.js'></script>
            <script type='text/javascript' src='js/yith-autocomplete.min.js'></script>
            <script type='text/javascript' src='js/flatsome-live-search.js'></script>
            <script type='text/javascript' src='js/hoverIntent.min.js'></script>
            <script type='text/javascript'>
               /* <![CDATA[ */
               var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
               /* ]]> */
            </script>
            <script type='text/javascript' src='js/flatsome.js'></script>
            <script type='text/javascript' src='js/flatsome-lazy-load.js'></script>
            <script type='text/javascript' src='js/woocommerce.js'></script>
            <script type='text/javascript' src='js/wp-embed.min.js'></script>
            <script type='text/javascript' src='js/packery.pkgd.min.js'></script>
            <script type='text/javascript' src='js/zxcvbn-async.min.js'></script>
            
            <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
          
            <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
         </body>
      </html>
     