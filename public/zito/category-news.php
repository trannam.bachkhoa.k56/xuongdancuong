<!DOCTYPE html>
<html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js">
   <!--<![endif]-->
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <title>Nội Thất ZITO | Showroom Nội Thất Gỗ Cao Cấp Tại Hà Nội</title>
      <!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
      <meta name="description" content="Nội thất ZITO tiên phong cung cấp các sản phẩm Sofa gỗ, da, nỉ theo kích thước tùy chỉnh, xu hướng mới nhất Showroom số 15 Dương Đình Nghệ Cầu Giấy Hà Nội"/>
      <meta name="robots" content="noodp"/>
      <base href="http://vn3c.net/zito/">
      <style type="text/css">
         img.wp-smiley,
         img.emoji {
         display: inline !important;
         border: none !important;
         box-shadow: none !important;
         height: 1em !important;
         width: 1em !important;
         margin: 0 .07em !important;
         vertical-align: -0.1em !important;
         background: none !important;
         padding: 0 !important;
         }
      </style>
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' type='text/css' media='all' />
      <link rel='stylesheet' id='yith_wcas_frontend-css'  href='css/yith_wcas_ajax_search.css' type='text/css' media='all' />
      <style id='yith_wcas_frontend-inline-css' type='text/css'>
         .autocomplete-suggestion{
         padding-right: 20px;
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
         .autocomplete-suggestion  span.yith_wcas_result_on_sale{
         background: #7eb742;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
         .autocomplete-suggestion  span.yith_wcas_result_outofstock{
         background: #7a7a7a;
         color: #ffffff
         }
         .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
         .autocomplete-suggestion  span.yith_wcas_result_featured{
         background: #c0392b;
         color: #ffffff
         }
         .autocomplete-suggestion img{
         width: 50px;
         }
         .autocomplete-suggestion .yith_wcas_result_content .title{
         color: #004b91;
         }
         .autocomplete-suggestion{
         min-height: 60px;
         }
      </style>
      <link rel='stylesheet' id='flatsome-main-css'  href='css/flatsome.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-shop-css'  href='css/flatsome-shop.css' type='text/css' media='all' />
      <link rel='stylesheet' id='flatsome-style-css'  href='css/style1.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/jquery.js'></script>
      <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
      <script type='text/javascript' src='js/advanced-cf7-db-public.js'></script>
      <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>
   </head>
   <body>
   </body>
   <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
   <noscript>
      <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
   </noscript>
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
   <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
   <link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
   <meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
   <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>
   </head>
   <body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
      <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
      <div id="wrapper">
         <?php include('header/header.php')?>
         <main id="main" class="">
            <div id="content" class="blog-wrapper blog-archive page-wrapper">
               <header class="archive-page-header">
                  <div class="row">
                     <div class="large-12 text-center col">
                        <h1 class="page-title is-large uppercase">
                           Chuyên mục: <span>Tin tức</span> 
                        </h1>
                        <div class="taxonomy-description">
                           <p style="text-align: justify;">Chuyên mục Tin tức <a href="https://zito.vn/">Nội thất ZITO</a> cung cấp cho Quý khách hàng những thông tin hữu ích về những vấn đề xoay quanh việc chọn mua, sử dụng và bảo quản nội thất, sofa tốt nhất. Bao gồm tin tức sofa, bàn trà…; việc chọn lựa kiểu dáng thiết kế, chất liệu <a href="https://zito.vn/danh-muc/sofa-go/">sofa gỗ</a>, sofa nỉ hay da; Màu sắc đệm mút hợp thời, hợp phong thủy; Các chi phí bỏ ra để chọn được sản phẩm phù hợp với <a href="http://imova.vn/">ngôi nhà</a>, đảm bảo về cả độ thẩm mỹ và bền theo thời gian… Sẽ không còn mất quá nhiều thời gian, công sức cho việc tìm kiếm thông tin, nghiên cứu khi bạn đến với chúng tôi – Công ty Cổ phần Nội thất ZITO, showroom số 15 Dương Đình Nghê, Cầu Giấy, Hà Nội.</p>
                           <h2 style="text-align: justify;"><span style="font-size: 85%;">Mọi thắc mắc xin đừng ngần ngại, hãy liên hệ với đội ngũ nhân viên để được hỗ trợ và tư vấn. Trân trọng !</span></h2>
                        </div>
                     </div>
                  </div>
               </header>
               <!-- .page-header -->
               <div class="row row-large ">
                  <div class="large-9 col">
                     <div id="row-1322045788" class="row large-columns-2 medium-columns- small-columns-1 has-shadow row-box-shadow-1 row-box-shadow-3-hover row-masonry" data-packery-options="{&quot;itemSelector&quot;: &quot;.col&quot;, &quot;gutter&quot;: 0, &quot;presentageWidth&quot; : true}" style="position: relative; height: 2204.43px;">
                        <div class="col post-item" style="position: absolute; left: 0px; top: 0px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/sofa-vang-cao-cap/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="180" src="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg" data-src="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg" class="attachment-medium size-medium wp-post-image lazy-load-active" alt="" srcset="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-768x461.jpg 768w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg 800w" data-srcset="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-768x461.jpg 768w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg 800w" sizes="(max-width: 300px) 100vw, 300px">                                                              
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Góp mặt vào sự đa dạng của bộ sưu tập ghế sofa ZITO, sofa văng cao cấp luôn khiến các tín đồ sofa phải say lòng bởi nét nhẹ nhàng, thanh lịch, sang trọng giúp khẳng định phong cách và cá tính của chủ ...              </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">23</span><br>
                                          <span class="post-date-month is-xsmall">Th1</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 446px; top: 0px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/lam-sach-ghe-sofa-da/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="197" src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg" data-src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg" class="attachment-medium size-medium wp-post-image lazy-load-active" alt="cách làm sạch ghế sofa da hiệu quả" srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w" data-srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w" sizes="(max-width: 300px) 100vw, 300px">                                                              
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Sofa da được biết đến không chỉ bởi vẻ sang trọng vốn có của lớp da. Mà còn có công nặng vượt trội hẳn so với sofa gỗ hay nỉ bởi dễ dàng vệ sinh lau chùi. Nhưng không vì thế mà việc vệ ...               </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">29</span><br>
                                          <span class="post-date-month is-xsmall">Th11</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 0px; top: 436px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/chon-ghe-sofa-cho-phong-khach-nho/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="200" src="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg" class="attachment-medium size-medium wp-post-image lazy-load-active" alt="Sofa Nỉ ZN 401" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-768x512.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg 800w" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-768x512.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg 800w" sizes="(max-width: 300px) 100vw, 300px">                                                            
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Chọn ghế sofa cho phòng khách nhỏ</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Hiện nay, trên thị trường nội thất ghế sofa được bày bán với nhiều kiểu dáng, mẫu mã đa dạng. Phù hợp với nhiều không gian, căn phòng khác nhau cho bạn thỏa sức lựa chọ. Để lựa chọn ghế sofa cho phòng khách ...              </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">29</span><br>
                                          <span class="post-date-month is-xsmall">Th11</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 446px; top: 436px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/sofa-go-nem/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="169" src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg" class="attachment-medium size-medium wp-post-image lazy-load-active" alt="sofa gỗ nệm" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7.jpg 1365w" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7.jpg 1365w" sizes="(max-width: 300px) 100vw, 300px">                                                              
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Sofa gỗ nệm – Giải pháp thông minh cho phòng khách đẹp</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Sofa gỗ thiết kế theo phong cách đơn giản, kết hợp khéo léo với đệm tựa êm ái. Không những khiến cho  phòng khách đẹp mà còn ấn tượng và thu hút. Với không gian phòng khách hiện đại thì lựa chọn bộ sofa ...               </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">29</span><br>
                                          <span class="post-date-month is-xsmall">Th11</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 0px; top: 873px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/chon-mau-sac-sofa/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="225" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768-300x225.png" class="lazy-load attachment-medium size-medium wp-post-image" alt="sofa giá rẻ" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768-300x225.png 300w, https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768-768x576.png 768w, https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768.png 1024w" sizes="(max-width: 300px) 100vw, 300px">                                                            
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Chọn màu sắc cho SOFA phù hợp với không gian sống</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Bạn đang muốn tân trang cho căn phòng của mình? Bạn băn khoăn không biết phối màu sao cho không gian hài hòa nhất? Bạn lại không có nhiều chi phí để thuê nhân viên thiết kế nội thất? Và bạn muốn không gian ...               </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">06</span><br>
                                          <span class="post-date-month is-xsmall">Th10</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 446px; top: 873px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/chat-lieu-go-sofa-zito/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="185" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1-300x185.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="sofa gỗ óc chó Hà Nội" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1-260x160.jpg 260w" sizes="(max-width: 300px) 100vw, 300px">                                                             
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Chất liệu gỗ sofa phòng khách có tại ZITO</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Gia đình bạn đang có nhu cầu mua 1 bộ sofa gỗ cho phòng khách của mình. Bạn phân vân không biết phải lựa chọn chất liệu gỗ sofa gì, phối màu thế nào sao cho đáng số tiền bỏ ra ? Trên thị ...             </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">14</span><br>
                                          <span class="post-date-month is-xsmall">Th9</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 0px; top: 1310px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/sofa-gia-re-ha-noi/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="170" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581-300x170.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="nội thất ZITO" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581-300x170.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581-768x436.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581.jpg 1024w" sizes="(max-width: 300px) 100vw, 300px">                                                            
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Nội thất Zito – Địa chỉ mua sofa giá rẻ Hà Nội</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Bạn đang phân vân mua Sofa giá rẻ Hà Nội bộ bàn ghế bếp, bàn ghế ăn, phòng khách cho căn hộ, nhà ở của mình với chi phí hợp lý nhưng phải đẹp, độc đáo, hiện đại bạn chưa biết tìm ở đâu? ...              </p>
                                          <p class="from_the_blog_comments uppercase is-xsmall">
                                             1 Comment                        
                                          </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">13</span><br>
                                          <span class="post-date-month is-xsmall">Th7</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 446px; top: 1310px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/khai-truong-thuong-hieu-noi-that-zito-khuyen-mai-20-30/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="199" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit-300x199.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Nội thất Zito" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit-300x199.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit-768x511.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit.jpg 961w" sizes="(max-width: 300px) 100vw, 300px">                                                              
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Khai trương thương hiệu nội thất Zito khuyến mãi 20 – 30%</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Trong tháng lễ khai trương từ 9/7 đến ngày 31/7/2017, Quý khách hàng mua sản phẩm tại Zito sẽ được nhận nhiều khuyến mại như:  Giảm giá 30%, tặng kèm bàn trà khi mua bộ sofa và giao hàng miễn phí trong nội thành. ...              </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">10</span><br>
                                          <span class="post-date-month is-xsmall">Th7</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 446px; top: 1747px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/xu-huong-lua-chon-sofa-go-he-2017/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="162" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-1-300x162-1-300x162.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Xu hướng lựa chọn sofa gỗ hè 2017">                                                             
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Xu hướng lựa chọn sofa gỗ</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">Xu hướng lựa chọn sofa gỗ Đáp ứng nhu cầu thẩm mỹ ngày càng cao của gia chủ, những chiếc sofa không còn đóng vai trò đơn giản là vật trang trí mà cần phải có công năng sử dụng lớn. Sử dụng các mẫu ...               </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">29</span><br>
                                          <span class="post-date-month is-xsmall">Th6</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                        <div class="col post-item" style="position: absolute; left: 0px; top: 1767px;">
                           <div class="col-inner">
                              <a href="https://zito.vn/xu-huong-lua-chon-sofa-phong-khach/" class="plain">
                                 <div class="box box-text-bottom box-blog-post has-hover">
                                    <div class="box-image">
                                       <div class="image-cover" style="padding-top:56%;">
                                          <img width="300" height="186" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1-300x186.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="chọn sofa phòng khách năm 2017" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1-260x160.jpg 260w" sizes="(max-width: 300px) 100vw, 300px">                                                            
                                       </div>
                                    </div>
                                    <!-- .box-image -->
                                    <div class="box-text text-left">
                                       <div class="box-text-inner blog-post-inner">
                                          <h5 class="post-title is-large ">Bắt kịp xu hướng lựa chọn sofa phòng khách năm 2018</h5>
                                          <div class="is-divider"></div>
                                          <p class="from_the_blog_excerpt ">“Chọn sofa phòng khách năm 2018”. Thật thiếu sót nếu bây giờ bạn vẫn loay hoay không biết cách lựa chọn sofa phòng khách. Ngoài các tiêu chí về sở thích việc bắt kịp xu hướng mới nhất sẽ đem lại không gian nhà ...              </p>
                                       </div>
                                       <!-- .box-text-inner -->
                                    </div>
                                    <!-- .box-text -->
                                    <div class="badge absolute top post-date badge-square">
                                       <div class="badge-inner">
                                          <span class="post-date-day">29</span><br>
                                          <span class="post-date-month is-xsmall">Th6</span>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- .box -->
                              </a>
                              <!-- .link -->
                           </div>
                           <!-- .col-inner -->
                        </div>
                        <!-- .col -->
                     </div>
                     <ul class="page-numbers nav-pagination links text-center">
                        <li><span class="page-number current">1</span></li>
                        <li><a class="page-number" href="https://zito.vn/tin-tuc/page/2/">2</a></li>
                        <li><a class="next page-number" href="https://zito.vn/tin-tuc/page/2/"><i class="icon-angle-right"></i></a></li>
                     </ul>
                  </div>
                  <!-- .large-9 -->
                  <?php include('block/sidebar-new.php')?>
                  <!-- .post-sidebar -->
               </div>
               <!-- .row -->
               <div class="nz-bvlq-main">
                  <div class="nz-bvlq">
                     <div class="row large-columns-4 medium-columns-2 small-columns-1 row-small">
                        <h3>Bài viết liên quan</h3>
                        <div class="nz-bvlq-content col post-item">
                           <div class="col-inner">
                              <a href="https://zito.vn/sofa-vang-cao-cap/" title="Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất">
                                 <div class="nz-bvlq-img"><img width="800" height="480" src="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg" data-src="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg" class="attachment-large size-large wp-post-image lazy-load-active" alt="" title="Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất" srcset="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg 800w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-768x461.jpg 768w" data-srcset="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg 800w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-768x461.jpg 768w" sizes="(max-width: 800px) 100vw, 800px"></div>
                                 <div class="nz-bvlq-td">Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất</div>
                              </a>
                           </div>
                        </div>
                        <div class="nz-bvlq-content col post-item">
                           <div class="col-inner">
                              <a href="https://zito.vn/lam-sach-ghe-sofa-da/" title="Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ">
                                 <div class="nz-bvlq-img"><img width="740" height="486" src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg" data-src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg" class="attachment-large size-large wp-post-image lazy-load-active" alt="cách làm sạch ghế sofa da hiệu quả" title="Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ" srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w" data-srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w" sizes="(max-width: 740px) 100vw, 740px"></div>
                                 <div class="nz-bvlq-td">Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ</div>
                              </a>
                           </div>
                        </div>
                        <div class="nz-bvlq-content col post-item">
                           <div class="col-inner">
                              <a href="https://zito.vn/chon-ghe-sofa-cho-phong-khach-nho/" title="Chọn ghế sofa cho phòng khách nhỏ">
                                 <div class="nz-bvlq-img"><img width="800" height="533" src="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg" class="attachment-large size-large wp-post-image lazy-load-active" alt="Sofa Nỉ ZN 401" title="Chọn ghế sofa cho phòng khách nhỏ" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg 800w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-768x512.jpg 768w" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg 800w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-768x512.jpg 768w" sizes="(max-width: 800px) 100vw, 800px"></div>
                                 <div class="nz-bvlq-td">Chọn ghế sofa cho phòng khách nhỏ</div>
                              </a>
                           </div>
                        </div>
                        <div class="nz-bvlq-content col post-item">
                           <div class="col-inner">
                              <a href="https://zito.vn/sofa-go-nem/" title="Sofa gỗ nệm – Giải pháp thông minh cho phòng khách đẹp">
                                 <div class="nz-bvlq-img"><img width="1020" height="573" src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg" class="attachment-large size-large wp-post-image lazy-load-active" alt="sofa gỗ nệm" title="Sofa gỗ nệm – Giải pháp thông minh cho phòng khách đẹp" srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7.jpg 1365w" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7.jpg 1365w" sizes="(max-width: 1020px) 100vw, 1020px"></div>
                                 <div class="nz-bvlq-td">Sofa gỗ nệm – Giải pháp thông minh cho phòng khách đẹp</div>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .page-wrapper .blog-wrapper -->
            </div>
         </main>
         <?php include('footer/footer.php')?>
         <!-- .footer-wrapper -->
      </div>
      <!-- #wrapper -->
      <!-- Mobile Sidebar -->
      <?php include('header/header-mobile.php')?>
      <!-- #mobile-menu -->
      <div id="login-form-popup" class="lightbox-content mfp-hide">
         <div class="account-container lightbox-inner">
            <div class="account-login-inner">
               <h3 class="uppercase">Đăng nhập</h3>
               <form method="post" class="login">
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                     <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                  </p>
                  <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                     <label for="password">Mật khẩu <span class="required">*</span></label>
                     <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                  </p>
                  <p class="form-row">
                     <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />            <input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                     <label for="rememberme" class="inline">
                     <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu          </label>
                  </p>
                  <p class="woocommerce-LostPassword lost_password">
                     <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                  </p>
               </form>
            </div>
            <!-- .login-inner -->
         </div>
         <!-- .account-login-container -->
      </div>
      <link rel='stylesheet' id='flatsome-effects-css'  href='css/effects.css' type='text/css' media='all' />
      <script type='text/javascript' src='js/scripts.js'></script>
      <script type='text/javascript' src='js/add-to-cart.min.js'></script>
      <script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
      <script type='text/javascript' src='js/js.cookie.min.js'></script>     
      <script type='text/javascript' src='js/woocommerce.min.js'></script>
      <script type='text/javascript' src='js/cart-fragments.min.js'></script>
      <script type='text/javascript' src='js/yith-autocomplete.min.js'></script>
      <script type='text/javascript' src='js/flatsome-live-search.js'></script>
      <script type='text/javascript' src='js/hoverIntent.min.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
            var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
            /* ]]> */
            
      </script>
      <script type='text/javascript' src='js/flatsome.js'></script>
      <script type='text/javascript' src='js/flatsome-lazy-load.js'></script>
      <script type='text/javascript' src='js/woocommerce.js'></script>
      <script type='text/javascript' src='js/wp-embed.min.js'></script>
      <script type='text/javascript' src='js/packery.pkgd.min.js'></script>
      <script type='text/javascript' src='js/zxcvbn-async.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
      <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
   </body>
</html>