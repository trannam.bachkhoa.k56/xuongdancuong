<!DOCTYPE html>

      <html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js">
         <!--<![endif]-->
         <head>
            <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
         
            <title>Nội Thất ZITO | Showroom Nội Thất Gỗ Cao Cấp Tại Hà Nội</title>
            <!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
            <meta name="description" content="Nội thất ZITO tiên phong cung cấp các sản phẩm Sofa gỗ, da, nỉ theo kích thước tùy chỉnh, xu hướng mới nhất Showroom số 15 Dương Đình Nghệ Cầu Giấy Hà Nội"/>
            <meta name="robots" content="noodp"/>
		<base href="http://vn3c.net/zito/">
            <style type="text/css">
               img.wp-smiley,
               img.emoji {
               display: inline !important;
               border: none !important;
               box-shadow: none !important;
               height: 1em !important;
               width: 1em !important;
               margin: 0 .07em !important;
               vertical-align: -0.1em !important;
               background: none !important;
               padding: 0 !important;
               }
            </style>
			<link rel="stylesheet" href="css/font-awesome.min.css">
            <link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' type='text/css' media='all' />
            <link rel='stylesheet' id='yith_wcas_frontend-css'  href='css/yith_wcas_ajax_search.css' type='text/css' media='all' />
            <style id='yith_wcas_frontend-inline-css' type='text/css'>
               .autocomplete-suggestion{
               padding-right: 20px;
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
               .autocomplete-suggestion  span.yith_wcas_result_on_sale{
               background: #7eb742;
               color: #ffffff
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
               .autocomplete-suggestion  span.yith_wcas_result_outofstock{
               background: #7a7a7a;
               color: #ffffff
               }
               .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
               .autocomplete-suggestion  span.yith_wcas_result_featured{
               background: #c0392b;
               color: #ffffff
               }
               .autocomplete-suggestion img{
               width: 50px;
               }
               .autocomplete-suggestion .yith_wcas_result_content .title{
               color: #004b91;
               }
               .autocomplete-suggestion{
               min-height: 60px;
               }
            </style>
            <link rel='stylesheet' id='flatsome-main-css'  href='css/flatsome.css' type='text/css' media='all' />
            <link rel='stylesheet' id='flatsome-shop-css'  href='css/flatsome-shop.css' type='text/css' media='all' />
            <link rel='stylesheet' id='flatsome-style-css'  href='css/style1.css' type='text/css' media='all' />
            <script type='text/javascript' src='js/jquery.js'></script>
            <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
            <script type='text/javascript' src='js/advanced-cf7-db-public.js'></script>
         
           
            <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>
       
       
            
 
         </head>
         <body>
     
         </body>
         <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
         <noscript>
            <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
         </noscript>
         <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
         <link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
         <link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
         <meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
         <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>
         </head>
         <body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
            <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
            <div id="wrapper">
            <?php include('header/header.php')?>
			
            
				  
			 <div class="shop-page-title category-page-title page-title featured-title dark ">
               <div class="page-title-bg fill">
                  <div class="title-bg fill bg-fill" data-parallax-fade="true" data-parallax="-2" data-parallax-background data-parallax-container=".page-title"></div>
                  <div class="title-overlay fill"></div>
               </div>
               <div class="page-title-inner flex-row  medium-flex-wrap container">
                  <div class="flex-col flex-grow medium-text-center">
                     <div class="is-large">
                        <nav class="woocommerce-breadcrumb breadcrumbs"><a href="https://zito.vn">Trang chủ</a> <span class="divider">&#47;</span> <a href="https://zito.vn/shop/">Cửa hàng</a> <span class="divider">&#47;</span> Sofa Gỗ</nav>
                     </div>
                     <div class="category-filtering category-filter-row show-for-medium">
                        <a href="#" data-open="#shop-sidebar" data-visible-after="true" data-pos="left" class="filter-button uppercase plain">
                        <i class="icon-menu"></i>
                        <strong>Lọc</strong>
                        </a>
                        <div class="inline-block">
                        </div>
                     </div>
                  </div>
                  <!-- .flex-left -->
                  <div class="flex-col medium-text-center  form-flat">
                     <p class="woocommerce-result-count hide-for-medium">
                        Showing 1&ndash;12 of 21 results
                     </p>
                     <form class="woocommerce-ordering" method="get">
                        <select name="orderby" class="orderby">
                           <option value="popularity" >Thứ tự theo mức độ phổ biến</option>
                           <option value="rating" >Thứ tự theo điểm đánh giá</option>
                           <option value="date"  selected='selected'>Thứ tự theo sản phẩm mới</option>
                           <option value="price" >Thứ tự theo giá: thấp đến cao</option>
                           <option value="price-desc" >Thứ tự theo giá: cao xuống thấp</option>
                        </select>
                     </form>
                  </div>
                  <!-- .flex-right -->
               </div>
               <!-- flex-row -->
            </div>
            <!-- .page-title -->
            <main id="main" class="">
               <div class="row category-page-row">
			   <?php include('block/sidebar-ct.php')?>
			    <!-- #shop-sidebar -->
                  <div class="col large-9">
                     <div class="shop-container">
                        <div class="term-description">
                           <p>
                           
						   <?php include('slider/slider-ct.php')?>
                           <!-- .ux-slider-wrapper -->
                           <br />
                           <strong>Sofa gỗ Cao Cấp ZITO – 05 Chất:</strong></p>
                           <p>+ Chất với KIỂU DÁNG mới nhất hiện nay<br />
                              + Chất lượng NGUYÊN LIỆU cao cấp NHẬP KHẨU từ Nhật, Mỹ, Châu Âu<br />
                              + Chất GỖ TỰ NHIÊN 100% tẩm sấy, BỀN BỈ với thời gian<br />
                              + Chất ở độ gia công TỈ MỈ, kết cấu CHẮC CHẮN
                           </p>
                           <h2>Sofa Gỗ Phòng Khách ZITO thể hiện CHẤT RIÊNG CỦA BẠN:</h2>
                           <p>+ Sofa gỗ có vẻ đẹp hiện đại!<br />
                              + Sofa gỗ đảm bảo bảo yếu tố bền đẹp!<br />
                              + <a href="https://zito.vn/danh-muc/sofa-go/">Sofa gỗ tự nhiên</a> tiết kiệm diện tích!<br />
                              + Sofa gỗ có giá thành rẻ, phù hợp với tầm chính của người Việt!<br />
                              + Chất liệu gỗ: Sồi Nga, Sồi Mỹ, Xoan đào, Óc chó  …. theo yêu cầu của bạn<br />
                              + Màu sắc sơn gỗ, nổi vân, bóng… theo sở thích, phong thủy của gia chủ<br />
                              + Kiểu dáng bao gồm sofa văng , Góc Chữ L, Chữ U kích thước được đo đạc và tư vấn phù hợp với căn hộ, vị trí đặt<br />
                              + Nệm da thật, da công nghiệp cao cấp nhập khẩu Hàn Quốc,Italia, Malaysia,…<br />
                              + Nệm nỉ chất liệu mềm mại, đứng form dẽ dàng tháo lắp, vệ sinh, thay thế
                           </p>
                           <h3><strong>MUA NGAY SOFA GỖ tại NỘI THẤT ZITO để hưởng những Đặc quyền sau:</strong></h3>
                           <p>&#8211; Sản phẩm sofa cam kết về chất lượng, xuất xứ vật liệu rõ ràng, thiết kế cập nhật xu hướng.<br />
                              &#8211; Thiết kế theo yêu cầu, tùy chỉnh về chất liệu, kiểu dáng, màu sắc.<br />
                              &#8211; Miễn phí vận chuyển nội thành Hà Nội. Quý khách ở xa chúng tôi sẽ hỗ trợ vận chuyển.<br />
                              &#8211; Chính sách bảo hành 24 tháng.<br />
                              <em>Cùng tham khảo những <a href="http://tanhoangminhhomes.vn/chinh-thuc-khai-truong-nha-mau-chung-cu-quang-an-du-an-d-le-roi-soleil.html">mẫu</a> sofa Phòng khách đang bày bán tại <a href="https://zito.vn/">showroom Nội Thất ZITO</a> ngay dưới đây:</em>
                           </p>
                        </div>
                        <div class="products row row-small large-columns-3 medium-columns-3 small-columns-2 has-shadow row-box-shadow-1 row-box-shadow-3-hover">
                           <div class="product-small col has-hover post-4201 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l first instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-121/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa gỗ tự nhiên" title="sofa go ZITO ZG121 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG121-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-121/">Sofa gỗ ZG121</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">23.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-4196 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-120/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="sofa gỗ phòng khách" title="sofa go ZITO zg 120 3" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-zg-120-3-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-120/">Sofa gỗ ZG 120</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">19.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-632 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-da product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-119/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-cao-cap-ZITO-ZG-119-15-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa gỗ ZG119" title="Sofa gỗ ZG119 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-cao-cap-ZITO-ZG-119-15-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-cao-cap-ZITO-ZG-119-15-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-cao-cap-ZITO-ZG-119-15-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-119/">Sofa gỗ ZG 119</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">30.500.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-596 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-da product_tag-sofa-go-goc-chu-l last instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-118/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-zito-cao-cap-zg-118-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa gỗ ZG 118" title="Sofa gỗ ZG 118 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-zito-cao-cap-zg-118-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-zito-cao-cap-zg-118-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-zito-cao-cap-zg-118-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-118/">Sofa gỗ ZG 118</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">29.500.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-227 product type-product status-publish has-post-thumbnail product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa product_tag-sofa-go-dem-ni first instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-117/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-117-3-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 117" title="Sofa Gỗ ZG 117 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-117-3-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-117-3-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-117-3-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-117/">Sofa Gỗ ZG 117</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">30.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-223 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-116/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-116-1-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="mẫu sofa cao cấp" title="Sofa Gỗ ZG 116 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-116-1-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-116-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-116-1-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-116/">Sofa Gỗ ZG 116</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">19.500.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-219 product type-product status-publish has-post-thumbnail product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-vang product_tag-sofa-go-dem-ni product_tag-sofa-phong-khach-nho  instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-115/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="giá sofa dẹp" title="Sofa Gỗ ZG 115 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-115-3-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-115/">Sofa Gỗ ZG 115</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">25.300.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-215 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l last instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-114/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-114-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 114" title="Sofa Gỗ ZG 114 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-114-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-114-2-300x183.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-114-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-114-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-114/">Sofa Gỗ ZG 114</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">20.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-210 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l product_tag-sofa-phong-khach-nho first instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-113/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-113-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 113" title="Sofa Gỗ ZG 113 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-113-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-113-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-113-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-113/">Sofa Gỗ ZG 113</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">18.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-206 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-da product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-112/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="sofa gỗ bọc nệm" title="Sofa Gỗ ZG 112 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-768x472.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-1024x630.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-520x320.jpg 520w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3.jpg 1302w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-112/">Sofa Gỗ ZG 112</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">19.300.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-202 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l  instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-111/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-111-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="sofa gỗ tự nhiên góc" title="Sofa Gỗ ZG 111 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-111-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-111-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-111-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-111/">Sofa Gỗ ZG 111</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">21.600.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                           <div class="product-small col has-hover post-198 product type-product status-publish has-post-thumbnail product_cat-phong-khach product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l last instock shipping-taxable purchasable product-type-simple">
                              <div class="col-inner">
                                 <div class="badge-container absolute left top z-1"></div>
                                 <div class="product-small box ">
                                    <div class="box-image">
                                       <div class="image-none">
                                          <a href="https://zito.vn/shop/sofa-go/zg-110/">
                                          <img width="260" height="160" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-110-2-260x160.jpg" class="lazy-load attachment-shop_catalog size-shop_catalog wp-post-image" alt="Sofa Gỗ ZG 110" title="Sofa Gỗ ZG 110 1" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-110-2-260x160.jpg 260w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-110-2-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-110-2-520x320.jpg 520w" sizes="(max-width: 260px) 100vw, 260px" />				</a>
                                       </div>
                                       <div class="image-tools is-small top right show-on-hover">
                                       </div>
                                       <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                       </div>
                                       <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                       </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                       <div class="title-wrapper">
                                          <p class="name product-title"><a href="https://zito.vn/shop/sofa-go/zg-110/">Sofa Gỗ ZG 110</a></p>
                                       </div>
                                       <div class="price-wrapper">
                                          <span class="price"><span class="woocommerce-Price-amount amount">28.000.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></span>
                                       </div>
                                    </div>
                                    <!-- box-text -->
                                 </div>
                                 <!-- box -->
                              </div>
                              <!-- .col-inner -->
                           </div>
                           <!-- col -->
                        </div>
                        <!-- row -->
                        <div class="container">
                           <nav class="woocommerce-pagination">
                              <ul class="page-numbers nav-pagination links text-center">
                                 <li><span class='page-number current'>1</span></li>
                                 <li><a class='page-number' href='https://zito.vn/danh-muc/sofa-go/page/2/'>2</a></li>
                                 <li><a class="next page-number" href="https://zito.vn/danh-muc/sofa-go/page/2/"><i class="icon-angle-right"></i></a></li>
                              </ul>
                           </nav>
                        </div>
                     </div>
                     <!-- shop container -->
                  </div>
               </div>
            </main>
            <!-- #main -->	  
            <!-- #main -->
			
			
           
			<?php include('footer/footer.php')?>
			
            <!-- .footer-wrapper -->
            </div>
			<!-- #wrapper -->
			
			
         
            
      
			
            <div id="login-form-popup" class="lightbox-content mfp-hide">
               <div class="account-container lightbox-inner">
                  <div class="account-login-inner">
                     <h3 class="uppercase">Đăng nhập</h3>
                     <form method="post" class="login">
                        <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                           <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                           <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                        </p>
                        <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                           <label for="password">Mật khẩu <span class="required">*</span></label>
                           <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                        </p>
                        <p class="form-row">
                           <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />				<input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                           <label for="rememberme" class="inline">
                           <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu				</label>
                        </p>
                        <p class="woocommerce-LostPassword lost_password">
                           <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                        </p>
                     </form>
                  </div>
                  <!-- .login-inner -->
               </div>
               <!-- .account-login-container -->
            </div>
            <link rel='stylesheet' id='flatsome-effects-css'  href='css/effects.css' type='text/css' media='all' />
   
            <script type='text/javascript' src='js/scripts.js'></script>
            <script type='text/javascript' src='js/add-to-cart.min.js'></script>
            <script type='text/javascript' src='js/jquery.blockUI.min.js'></script>
            <script type='text/javascript' src='js/js.cookie.min.js'></script>     
            <script type='text/javascript' src='js/woocommerce.min.js'></script>
            <script type='text/javascript' src='js/cart-fragments.min.js'></script>
            <script type='text/javascript' src='js/yith-autocomplete.min.js'></script>
            <script type='text/javascript' src='js/flatsome-live-search.js'></script>
            <script type='text/javascript' src='js/hoverIntent.min.js'></script>
            <script type='text/javascript'>
               /* <![CDATA[ */
               var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
               /* ]]> */
            </script>
            <script type='text/javascript' src='js/flatsome.js'></script>
            <script type='text/javascript' src='js/flatsome-lazy-load.js'></script>
            <script type='text/javascript' src='js/woocommerce.js'></script>
            <script type='text/javascript' src='js/wp-embed.min.js'></script>
            <script type='text/javascript' src='js/packery.pkgd.min.js'></script>
            <script type='text/javascript' src='js/zxcvbn-async.min.js'></script>
            
            <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
          
            <script type='text/javascript' src='js/password-strength-meter.min.js'></script>
         </body>
      </html>
     