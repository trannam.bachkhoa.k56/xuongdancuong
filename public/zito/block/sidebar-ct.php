

                  <div class="col large-3 hide-for-medium ">
                     <div id="shop-sidebar" class="sidebar-inner col-inner">
                        <aside id="search-6" class="widget widget_search">
                           <span class="widget-title shop-sidebar">Tìm kiếm</span>
                           <div class="is-divider small"></div>
                           <form method="get" class="searchform" action="https://zito.vn/" role="search">
                              <div class="flex-row relative">
                                 <div class="flex-col flex-grow">
                                    <input type="search" class="search-field mb-0" name="s" value="" id="s" placeholder="Tìm kiếm..." />
                                 </div>
                                 <!-- .flex-col -->
                                 <div class="flex-col">
                                    <button type="submit" class="ux-search-submit submit-button secondary button icon mb-0">
                                    <i class="icon-search" ></i>				</button>
                                 </div>
                                 <!-- .flex-col -->
                              </div>
                              <!-- .flex-row -->
                              <div class="live-search-results text-left z-top"></div>
                           </form>
                        </aside>
                        <aside id="woocommerce_product_categories-2" class="widget woocommerce widget_product_categories">
                           <span class="widget-title shop-sidebar">Danh mục sản phẩm</span>
                           <div class="is-divider small"></div>
                           <ul class="product-categories">
                              <li class="cat-item cat-item-33"><a href="https://zito.vn/danh-muc/ban-tra/">Bàn Trà</a></li>
                              <li class="cat-item cat-item-42"><a href="https://zito.vn/danh-muc/goi-tua/">Gối Tựa</a></li>
                              <li class="cat-item cat-item-34"><a href="https://zito.vn/danh-muc/ke-tivi/">Kệ Tivi</a></li>
                              <li class="cat-item cat-item-16"><a href="https://zito.vn/danh-muc/phong-khach/">Phòng Khách</a></li>
                              <li class="cat-item cat-item-24"><a href="https://zito.vn/danh-muc/san-pham-ban-chay/">Sản Phẩm Bán Chạy</a></li>
                              <li class="cat-item cat-item-23"><a href="https://zito.vn/danh-muc/san-pham-noi-bat/">Sản Phẩm Nổi Bật</a></li>
                              <li class="cat-item cat-item-20"><a href="https://zito.vn/danh-muc/sofa-chu-u/">Sofa chữ U</a></li>
                              <li class="cat-item cat-item-21"><a href="https://zito.vn/danh-muc/sofa-da/">Sofa Da</a></li>
                              <li class="cat-item cat-item-15 current-cat"><a href="https://zito.vn/danh-muc/sofa-go/">Sofa Gỗ</a></li>
                              <li class="cat-item cat-item-18"><a href="https://zito.vn/danh-muc/sofa-goc/">Sofa Góc</a></li>
                              <li class="cat-item cat-item-22"><a href="https://zito.vn/danh-muc/sofa-ni/">Sofa Nỉ</a></li>
                              <li class="cat-item cat-item-19"><a href="https://zito.vn/danh-muc/sofa-vang/">Sofa Văng</a></li>
                           </ul>
                        </aside>
                        <aside id="woocommerce_price_filter-2" class="widget woocommerce widget_price_filter">
                           <span class="widget-title shop-sidebar">Giá Bán</span>
                           <div class="is-divider small"></div>
                           <form method="get" action="https://zito.vn/danh-muc/sofa-go/">
                              <div class="price_slider_wrapper">
                                 <div class="price_slider" style="display:none;"></div>
                                 <div class="price_slider_amount">
                                    <input type="text" id="min_price" name="min_price" value="" data-min="12600000" placeholder="Giá thấp nhất" />
                                    <input type="text" id="max_price" name="max_price" value="" data-max="30600000" placeholder="Giá cao nhất" />
                                    <button type="submit" class="button">Lọc</button>
                                    <div class="price_label" style="display:none;">
                                       Giá <span class="from"></span> &mdash; <span class="to"></span>
                                    </div>
                                    <div class="clear"></div>
                                 </div>
                              </div>
                           </form>
                        </aside>
                        <aside id="text-2" class="widget widget_text">
                           <div class="textwidget">
                              <p><img class="aligncenter size-full wp-image-4246" src="https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito-khuyen-mai-12-17.png" alt="" width="300" height="783" /></p>
                           </div>
                        </aside>
                        <aside id="woocommerce_products-2" class="widget woocommerce widget_products">
                           <span class="widget-title shop-sidebar">Sản phẩm mới</span>
                           <div class="is-divider small"></div>
                           <ul class="product_list_widget">
                              <li>
                                 <a href="https://zito.vn/shop/goi-tua/zp-meo-03/">
                                 <img width="130" height="80" src="//zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Gối tựa sofa" srcset="" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-03-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" />		<span class="product-title">Gối ZP Mèo 03</span>
                                 </a>
                                 <span class="woocommerce-Price-amount amount">220.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span>
                              </li>
                              <li>
                                 <a href="https://zito.vn/shop/goi-tua/zp-meo-02/">
                                 <img width="130" height="80" src="//zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="gối tựa lưng văn phòng" srcset="" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" />		<span class="product-title">Gối ZP Mèo 02</span>
                                 </a>
                                 <ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins>
                              </li>
                              <li>
                                 <a href="https://zito.vn/shop/goi-tua/zp-meo-01/">
                                 <img width="130" height="80" src="//zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="gối tựa lưng" srcset="" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-01-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" />		<span class="product-title">Gối ZP Mèo 01</span>
                                 </a>
                                 <ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins>
                              </li>
                              <li>
                                 <a href="https://zito.vn/shop/goi-tua/zp-gau-01/">
                                 <img width="130" height="80" src="//zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="gối tựa lưng dễ thương" srcset="" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-GAU-01-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" />		<span class="product-title">Gối ZP Gấu 01</span>
                                 </a>
                                 <ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins>
                              </li>
                              <li>
                                 <a href="https://zito.vn/shop/goi-tua/zp-buom-01/">
                                 <img width="130" height="80" src="//zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="Gối ôm gối tựa lưng giá rẻ tại hà nội" srcset="" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-BUOM-04-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" />		<span class="product-title">Gối ZP Bướm 01</span>
                                 </a>
                                 <ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins>
                              </li>
                           </ul>
                        </aside>
                        <style>.rpwe-block ul{
                           background: white;
                           list-style: none !important;
                           margin-left: 0 !important;
                           padding-left: 0 !important;
                           }
                           .rpwe-block li{    margin-left: 0 !important;
                           text-align: left;
                           border: 1px solid #eee;
                           margin-bottom: 0px;
                           padding: 5px 10px;
                           list-style-type: none;
                           }
                           .rpwe-block a{
                           text-decoration: none;
                           font-size: 14px;
                           font-weight: normal;
                           font-family: "Roboto Condensed", sans-serif;
                           }
                           .rpwe-block h3{
                           background: none !important;
                           clear: none;
                           margin-bottom: 0 !important;
                           margin-top: 0 !important;
                           font-weight: 400;
                           font-size: 12px !important;
                           line-height: 1.5em;
                           }
                           .rpwe-thumb{
                           border-radius:3px;
                           box-shadow: none !important;
                           margin: 2px 10px 2px 0;
                           }
                           .rpwe-summary {
                           font-size: 20px;
                           color: red;
                           }
                           .rpwe-summary:before {
                           font-family: FontAwesome;
                           content: "\f095 ";
                           padding-right: 5px;
                           }
                           .rpwe-time{
                           color: #909090;
                           font-size: 11px;
                           }
                           .rpwe-comment{
                           color: #bbb;
                           font-size: 11px;
                           padding-left: 5px;
                           }
                           .rpwe-alignleft{
                           display: inline;
                           float: left;
                           }
                           .rpwe-alignright{
                           display: inline;
                           float: right;
                           }
                           .rpwe-aligncenter{
                           display: block;
                           margin-left: auto;
                           margin-right: auto;
                           }
                           .rpwe-clearfix:before,
                           .rpwe-clearfix:after{
                           content: "";
                           display: table !important;
                           }
                           .rpwe-clearfix:after{
                           clear: both;
                           }
                           .rpwe-clearfix{
                           zoom: 1;
                           }
                        </style>
                        <aside id="rpwe_widget-2" class="widget rpwe_widget recent-posts-extended">
                           <span class="widget-title shop-sidebar">Tin tức mới</span>
                           <div class="is-divider small"></div>
                           <div  class="rpwe-block ">
                              <ul class="rpwe-ul">
                                 <li class="rpwe-li rpwe-clearfix">
                                    <a class="rpwe-img" href="https://zito.vn/sofa-vang-cao-cap/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-100x80.jpg" alt="Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất"></a>
                                    <h3 class="rpwe-title"><a href="https://zito.vn/sofa-vang-cao-cap/" title="Permalink to Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất" rel="bookmark">Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất</a></h3>
                                    <time class="rpwe-time published" datetime="2018-01-23T09:10:50+00:00">23 Tháng Một, 2018</time>
                                 </li>
                                 <li class="rpwe-li rpwe-clearfix">
                                    <a class="rpwe-img" href="https://zito.vn/khuyen-mai/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-100x80.jpg" alt="Chương trình khuyến mại"></a>
                                    <h3 class="rpwe-title"><a href="https://zito.vn/khuyen-mai/" title="Permalink to Chương trình khuyến mại" rel="bookmark">Chương trình khuyến mại</a></h3>
                                    <time class="rpwe-time published" datetime="2018-01-08T09:51:41+00:00">8 Tháng Một, 2018</time>
                                 </li>
                                 <li class="rpwe-li rpwe-clearfix">
                                    <a class="rpwe-img" href="https://zito.vn/chinh-sach-bao-mat-thong-tin-khach-hang/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito-100x80.jpg" alt="Bảo mật thông tin khách hàng"></a>
                                    <h3 class="rpwe-title"><a href="https://zito.vn/chinh-sach-bao-mat-thong-tin-khach-hang/" title="Permalink to Bảo mật thông tin khách hàng" rel="bookmark">Bảo mật thông tin khách hàng</a></h3>
                                    <time class="rpwe-time published" datetime="2017-12-07T22:08:35+00:00">7 Tháng Mười Hai, 2017</time>
                                 </li>
                                 <li class="rpwe-li rpwe-clearfix">
                                    <a class="rpwe-img" href="https://zito.vn/chinh-sach-bao-hanh-luu-y-su-dung/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-100x80.jpg" alt="Chính sách bảo hành, lưu ý sử dụng"></a>
                                    <h3 class="rpwe-title"><a href="https://zito.vn/chinh-sach-bao-hanh-luu-y-su-dung/" title="Permalink to Chính sách bảo hành, lưu ý sử dụng" rel="bookmark">Chính sách bảo hành, lưu ý sử dụng</a></h3>
                                    <time class="rpwe-time published" datetime="2017-12-07T20:33:48+00:00">7 Tháng Mười Hai, 2017</time>
                                 </li>
                                 <li class="rpwe-li rpwe-clearfix">
                                    <a class="rpwe-img" href="https://zito.vn/mua-hang-van-chuyen-thanh-toan/"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://zito.vn/wp-content/uploads/2017/12/Thanh-toan-mua-hang-tai-noi-that-zito-100x80.jpg" alt="Hướng dẫn đặt mua, thanh toán, vận chuyển"></a>
                                    <h3 class="rpwe-title"><a href="https://zito.vn/mua-hang-van-chuyen-thanh-toan/" title="Permalink to Hướng dẫn đặt mua, thanh toán, vận chuyển" rel="bookmark">Hướng dẫn đặt mua, thanh toán, vận chuyển</a></h3>
                                    <time class="rpwe-time published" datetime="2017-12-07T19:29:37+00:00">7 Tháng Mười Hai, 2017</time>
                                 </li>
                              </ul>
                           </div>
                           <!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ -->
                        </aside>
                        <aside id="media_video-2" class="widget widget_media_video">
                           <span class="widget-title shop-sidebar">VIDEO</span>
                           <div class="is-divider small"></div>
                           <div style="width:100%;" class="wp-video">
                              <!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
                              <video class="wp-video-shortcode" id="video-4201-1" preload="metadata" controls="controls">
                                 <source type="video/youtube" src="https://youtu.be/1dcFaoMsHNY?_=1" />
                                 <a href="https://youtu.be/1dcFaoMsHNY">https://youtu.be/1dcFaoMsHNY</a>
                              </video>
                           </div>
                        </aside>
                     </div>
                     <!-- .sidebar-inner -->
                  </div>
                 