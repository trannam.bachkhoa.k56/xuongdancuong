<div class="post-sidebar large-3 col">
                     <div id="secondary" class="widget-area " role="complementary">
                        <aside id="woocommerce_product_categories-3" class="widget woocommerce widget_product_categories">
                           <span class="widget-title "><span>Danh mục sản phẩm</span></span>
                           <div class="is-divider small"></div>
                           <ul class="product-categories">
                              <li class="cat-item cat-item-33"><a href="https://zito.vn/danh-muc/ban-tra/">Bàn Trà</a></li>
                              <li class="cat-item cat-item-42"><a href="https://zito.vn/danh-muc/goi-tua/">Gối Tựa</a></li>
                              <li class="cat-item cat-item-34"><a href="https://zito.vn/danh-muc/ke-tivi/">Kệ Tivi</a></li>
                              <li class="cat-item cat-item-16"><a href="https://zito.vn/danh-muc/phong-khach/">Phòng Khách</a></li>
                              <li class="cat-item cat-item-24"><a href="https://zito.vn/danh-muc/san-pham-ban-chay/">Sản Phẩm Bán Chạy</a></li>
                              <li class="cat-item cat-item-23"><a href="https://zito.vn/danh-muc/san-pham-noi-bat/">Sản Phẩm Nổi Bật</a></li>
                              <li class="cat-item cat-item-20"><a href="https://zito.vn/danh-muc/sofa-chu-u/">Sofa chữ U</a></li>
                              <li class="cat-item cat-item-21"><a href="https://zito.vn/danh-muc/sofa-da/">Sofa Da</a></li>
                              <li class="cat-item cat-item-15"><a href="https://zito.vn/danh-muc/sofa-go/">Sofa Gỗ</a></li>
                              <li class="cat-item cat-item-18"><a href="https://zito.vn/danh-muc/sofa-goc/">Sofa Góc</a></li>
                              <li class="cat-item cat-item-22"><a href="https://zito.vn/danh-muc/sofa-ni/">Sofa Nỉ</a></li>
                              <li class="cat-item cat-item-19"><a href="https://zito.vn/danh-muc/sofa-vang/">Sofa Văng</a></li>
                           </ul>
                        </aside>
                        <aside id="text-3" class="widget widget_text">
                           <div class="textwidget">
                              <p><img class="aligncenter size-full wp-image-4246" src="https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito-khuyen-mai-12-17.png" alt="" width="300" height="783"></p>
                           </div>
                        </aside>
                        <aside id="woocommerce_recently_viewed_products-3" class="widget woocommerce widget_recently_viewed_products">
                           <span class="widget-title "><span>Sản phẩm đã xem</span></span>
                           <div class="is-divider small"></div>
                           <ul class="product_list_widget">
                              <li>
                                 <a href="https://zito.vn/shop/goi-tua/zp-meo-02/">
                                 <img width="130" height="80" src="//zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="gối tựa lưng văn phòng" srcset="" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px">    <span class="product-title">Gối ZP Mèo 02</span>
                                 </a>
                                 <ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins>
                              </li>
                           </ul>
                        </aside>
                        <aside id="media_video-3" class="widget widget_media_video">
                           <span class="widget-title "><span>VIDEO</span></span>
                           <div class="is-divider small"></div>
                           <div style="width:100%;" class="wp-video">
                              <!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
                              <span class="mejs-offscreen">Trình chơi Video</span>
                              <div id="mep_0" class="mejs-container svg wp-video-shortcode mejs-video" tabindex="0" role="application" aria-label="Trình chơi Video" style="width: 278px; height: 156px;">
                                 <div class="mejs-inner">
                                    <div class="mejs-mediaelement">
                                       <iframe class="me-plugin" id="me_youtube_0_container" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="278" height="156" src="https://www.youtube.com/embed/1dcFaoMsHNY?controls=0&amp;wmode=transparent&amp;enablejsapi=1&amp;origin=http%3A%2F%2Flocalhost&amp;widgetid=1" style="width: 278px; height: 156px;"></iframe>
                                       <video class="wp-video-shortcode" id="video-4744-1" preload="metadata" style="width: 100%; height: 100%; display: none;">
                                          <source type="video/youtube" src="https://youtu.be/1dcFaoMsHNY?_=1">
                                          <a href="https://youtu.be/1dcFaoMsHNY">https://youtu.be/1dcFaoMsHNY</a>
                                       </video>
                                    </div>
                                    <div class="mejs-layers">
                                       <div class="mejs-poster mejs-layer" style="display: none; width: 100%; height: 100%;"></div>
                                       <div class="mejs-overlay mejs-layer" style="display: none; width: 100%; height: 100%;">
                                          <div class="mejs-overlay-loading"><span></span></div>
                                       </div>
                                       <div class="mejs-overlay mejs-layer" style="display: none; width: 100%; height: 100%;">
                                          <div class="mejs-overlay-error"></div>
                                       </div>
                                       <div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height: 100%;">
                                          <div class="mejs-overlay-button"></div>
                                       </div>
                                    </div>
                                    <div class="mejs-controls">
                                       <div class="mejs-button mejs-playpause-button mejs-play" style=""><button type="button" aria-controls="mep_0" title="Chạy" aria-label="Chạy"></button></div>
                                       <div class="mejs-time mejs-currenttime-container" role="timer" aria-live="off" style=""><span class="mejs-currenttime">00:00</span></div>
                                       <div class="mejs-time-rail" style="width: 132px;"><span class="mejs-time-total mejs-time-slider" style="width: 122px;" aria-label="Thời gian trình chiếu" aria-valuemin="0" aria-valuemax="78" aria-valuenow="0" aria-valuetext="00:00" role="slider" tabindex="0"><span class="mejs-time-buffering" style="display: none;"></span><span class="mejs-time-loaded" style="width: 0px;"></span><span class="mejs-time-current" style="width: 0px;"></span><span class="mejs-time-handle" style="left: -5px;"></span><span class="mejs-time-float"><span class="mejs-time-float-current">00:00</span><span class="mejs-time-float-corner"></span></span></span></div>
                                       <div class="mejs-time mejs-duration-container" style=""><span class="mejs-duration">01:18</span></div>
                                       <div class="mejs-button mejs-volume-button mejs-mute" style="">
                                          <button type="button" aria-controls="mep_0" title="Tắt tiếng" aria-label="Tắt tiếng"></button>
                                          <a href="javascript:void(0);" class="mejs-volume-slider" style="display: none;">
                                             <span class="mejs-offscreen">Sử dụng các phím mũi tên Lên/Xuống để tăng hoặc giảm âm lượng.</span>
                                             <div class="mejs-volume-total" style=""></div>
                                             <div class="mejs-volume-current" style="height: 100px; top: 7.98828px;"></div>
                                             <div class="mejs-volume-handle" style="top: 5px;"></div>
                                          </a>
                                       </div>
                                       <div class="mejs-button mejs-fullscreen-button" style=""><button type="button" aria-controls="mep_0" title="Toàn màn hình" aria-label="Toàn màn hình"></button></div>
                                    </div>
                                    <div class="mejs-clear"></div>
                                 </div>
                              </div>
                           </div>
                        </aside>
                     </div>
                     <!-- #secondary -->
                  </div>