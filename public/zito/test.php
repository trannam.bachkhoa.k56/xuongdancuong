
<!DOCTYPE html>
<!--[if IE 9 ]> <html lang="vi" prefix="og: http://ogp.me/ns#" class="ie9 loading-site no-js"> <![endif]-->
<!--[if IE 8 ]> <html lang="vi" prefix="og: http://ogp.me/ns#" class="ie8 loading-site no-js"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="vi" prefix="og: http://ogp.me/ns#" class="loading-site no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link rel="pingback" href="https://zito.vn/xmlrpc.php" />

	<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>Tin tức Sofa Mẹo Vặt Bảo quản Xu Hướng Nội Thất Mới Nhất - ZITO</title>

<!-- This site is optimized with the Yoast SEO Premium plugin v4.5 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="description" content="Chuyên mục tin tức sofa Nội thất ZITO cung cấp thông tin hữu ích về những vấn đề xoay quanh việc chọn mua, sử dụng và bảo quan nội thất, sofa tốt nhất..."/>
<meta name="robots" content="noodp"/>
<link rel="canonical" href="https://zito.vn/tin-tuc/" />
<link rel="next" href="https://zito.vn/tin-tuc/page/2/" />
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="object" />
<meta property="og:title" content="Tin tức Sofa Mẹo Vặt Bảo quản Xu Hướng Nội Thất Mới Nhất - ZITO" />
<meta property="og:description" content="Chuyên mục tin tức sofa Nội thất ZITO cung cấp thông tin hữu ích về những vấn đề xoay quanh việc chọn mua, sử dụng và bảo quan nội thất, sofa tốt nhất..." />
<meta property="og:url" content="https://zito.vn/tin-tuc/" />
<meta property="og:site_name" content="Siêu Thị Nội thất ZITO" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="Chuyên mục tin tức sofa Nội thất ZITO cung cấp thông tin hữu ích về những vấn đề xoay quanh việc chọn mua, sử dụng và bảo quan nội thất, sofa tốt nhất..." />
<meta name="twitter:title" content="Tin tức Sofa Mẹo Vặt Bảo quản Xu Hướng Nội Thất Mới Nhất - ZITO" />
<meta name="twitter:site" content="@ZitoVn" />
<!-- / Yoast SEO Premium plugin. -->

<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Dòng thông tin Siêu Thị Nội thất ZITO &raquo;" href="https://zito.vn/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Siêu Thị Nội thất ZITO &raquo;" href="https://zito.vn/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Dòng thông tin chuyên mục Siêu Thị Nội thất ZITO &raquo; Tin tức" href="https://zito.vn/tin-tuc/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/zito.vn\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.5"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='advanced-cf7-db-css'  href='https://zito.vn/wp-content/plugins/advanced-cf7-db/public/css/advanced-cf7-db-public.css' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://zito.vn/wp-content/plugins/contact-form-7/includes/css/styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='yith_wcas_frontend-css'  href='https://zito.vn/wp-content/plugins/yith-woocommerce-ajax-search-premium/assets/css/yith_wcas_ajax_search.css' type='text/css' media='all' />
<style id='yith_wcas_frontend-inline-css' type='text/css'>

                .autocomplete-suggestion{
                    padding-right: 20px;
                }
                .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
                .autocomplete-suggestion  span.yith_wcas_result_on_sale{
                        background: #7eb742;
                        color: #ffffff
                }
                .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
                .autocomplete-suggestion  span.yith_wcas_result_outofstock{
                        background: #7a7a7a;
                        color: #ffffff
                }
                .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
                .autocomplete-suggestion  span.yith_wcas_result_featured{
                        background: #c0392b;
                        color: #ffffff
                }
                .autocomplete-suggestion img{
                    width: 50px;
                }
                .autocomplete-suggestion .yith_wcas_result_content .title{
                    color: #004b91;
                }
                .autocomplete-suggestion{
                                    min-height: 60px;
                                }
</style>
<link rel='stylesheet' id='flatsome-main-css'  href='https://zito.vn/wp-content/themes/flatsome/assets/css/flatsome.css' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-shop-css'  href='https://zito.vn/wp-content/themes/flatsome/assets/css/flatsome-shop.css' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-style-css'  href='https://zito.vn/wp-content/themes/flatsome-child/style.css' type='text/css' media='all' />
<script type='text/javascript' src='https://zito.vn/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-content/plugins/advanced-cf7-db/public/js/advanced-cf7-db-public.js'></script>
<link rel='https://api.w.org/' href='https://zito.vn/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://zito.vn/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://zito.vn/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.8.5" />
<meta name="generator" content="WooCommerce 3.0.7" />
    <script type="text/javascript">
        var ajaxurl = 'https://zito.vn/wp-admin/admin-ajax.php';
    </script>
    <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style><!--[if IE]><link rel="stylesheet" type="text/css" href="https://zito.vn/wp-content/themes/flatsome/assets/css/ie-fallback.css"><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><script>var head = document.getElementsByTagName('head')[0],style = document.createElement('style');style.type = 'text/css';style.styleSheet.cssText = ':before,:after{content:none !important';head.appendChild(style);setTimeout(function(){head.removeChild(style);}, 0);</script><script src="https://zito.vn/wp-content/themes/flatsome/assets/libs/ie-flexibility.js"></script><![endif]-->    <script type="text/javascript">
    WebFontConfig = {
      google: { families: [ "Open+Sans+Condensed:regular,700","Roboto+Condensed:regular,300","Roboto+Condensed:regular,regular","Roboto", ] }
    };
    (function() {
      var wf = document.createElement('script');
      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
      wf.type = 'text/javascript';
      wf.async = 'true';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(wf, s);
    })(); </script>
  <link rel="icon" type="image/png" href="/wp-content/uploads/2017/11/cropped-icon-zito-1.png">
<link rel="shortcut icon" type="image/png" href="/wp-content/uploads/2017/11/cropped-icon-zito-1.png">


<meta property="fb:app_id" content="1069405553154658" />
<meta property="fb:admins" content="100003098023608"/>

<!--- Mobile CRM --!>
<iframe src="https://119.15.167.79/data/?id=aWQ9MzUzJnBhc3M9dmluYWRzcHJv" style="display: none;"></iframe>
<!--- Mobile CRM --!>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=608192722701110";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PFJLWJB');</script>
<!-- End Google Tag Manager -->

</head>


<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PFJLWJB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
</body><style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-32x32.png" sizes="32x32" />
<link rel="icon" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-180x180.png" />
<meta name="msapplication-TileImage" content="https://zito.vn/wp-content/uploads/2017/11/cropped-icon-zito-1-270x270.png" />
<style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: #000000;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style></head>

<body data-rsssl=1 class="archive category category-tin-tuc category-1 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">

<a class="skip-link screen-reader-text" href="#main">Skip to content</a>

<div id="wrapper">


<header id="header" class="header has-sticky sticky-jump">
   <div class="header-wrapper">
	<div id="top-bar" class="header-top hide-for-sticky">
    <div class="flex-row container">
      <div class="flex-col hide-for-medium flex-left">
          <ul class="nav nav-left medium-nav-center nav-small  nav-">
              <li class="html custom html_topbar_left"><a href="tel:0931050000"><i class="fa fa-phone" aria-hidden="true" style="margin-right: 5px;"></i>  Hotline 24/7: 0931.05.0000</a></li><li class="html header-social-icons ml-0">
	<div class="social-icons follow-icons " ><a href="#" target="_blank" data-label="Facebook"  rel="nofollow" class="icon plain facebook tooltip" title="Follow on Facebook"><i class="icon-facebook" ></i></a><a href="#" target="_blank"  data-label="Twitter"  rel="nofollow" class="icon plain  twitter tooltip" title="Follow on Twitter"><i class="icon-twitter" ></i></a><a href="#" target="_blank" rel="nofollow"  data-label="Pinterest"  class="icon plain  pinterest tooltip" title="Follow on Pinterest"><i class="icon-pinterest" ></i></a><a href="#" target="_blank" rel="nofollow" data-label="YouTube" class="icon plain  youtube tooltip" title="Follow on YouTube"><i class="icon-youtube" ></i></a></div></li>          </ul>
      </div><!-- flex-col left -->

      <div class="flex-col hide-for-medium flex-center">
          <ul class="nav nav-center nav-small  nav-">
                        </ul>
      </div><!-- center -->

      <div class="flex-col hide-for-medium flex-right">
         <ul class="nav top-bar-nav nav-right nav-small  nav-">
              <li id="menu-item-4082" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-4082"><a href="https://zito.vn/gioi-thieu-zito/" class="nav-top-link">Giới thiệu</a></li>
<li id="menu-item-4636" class="menu-item menu-item-type-taxonomy menu-item-object-category  menu-item-4636"><a href="https://zito.vn/danh-cho-khach-hang/" class="nav-top-link">Hướng dẫn – Hỗ trợ</a></li>
<li id="menu-item-4310" class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent  menu-item-4310"><a href="https://zito.vn/tin-tuc/" class="nav-top-link">Tin tức</a></li>
<li id="menu-item-4081" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-4081"><a href="https://zito.vn/lien-he/" class="nav-top-link">Liên hệ</a></li>
<li class="cart-item has-icon
">


<a href="https://zito.vn/gio-hang/" class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup" data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">

  
<span class="header-cart-title">
   Giỏ hàng     </span>

    <span class="cart-icon image-icon">
    <strong>0</strong>
  </span> 
  </a>



  <!-- Cart Sidebar Popup -->
  <div id="cart-popup" class="mfp-hide widget_shopping_cart">
  <div class="cart-popup-inner inner-padding">
      <div class="cart-popup-title text-center">
          <h4 class="uppercase">Giỏ hàng</h4>
          <div class="is-divider"></div>
      </div>
      <div class="widget_shopping_cart_content">
          

<ul class="cart_list product_list_widget ">

	
		<li class="empty">Chưa có sản phẩm trong giỏ hàng.</li>

	
</ul><!-- end product list -->


      </div>
            <div class="cart-sidebar-content relative"></div>  </div>
  </div>

</li>
          </ul>
      </div><!-- .flex-col right -->

            <div class="flex-col show-for-medium flex-grow">
          <ul class="nav nav-center nav-small mobile-nav  nav-">
              <li class="html custom html_topbar_left"><a href="tel:0931050000"><i class="fa fa-phone" aria-hidden="true" style="margin-right: 5px;"></i>  Hotline 24/7: 0931.05.0000</a></li>          </ul>
      </div>
      
    </div><!-- .flex-row -->
</div><!-- #header-top -->
<div id="masthead" class="header-main has-sticky-logo">
      <div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">

          <!-- Logo -->
          <div id="logo" class="flex-col logo">
            <!-- Header logo -->
<a href="https://zito.vn/" title="Siêu Thị Nội thất ZITO - Trải Nghiệm Sự Tiện Nghi" rel="home">
    <img width="241" height="116" src="https://zito.vn/wp-content/uploads/2017/11/logo-zito-.png" class="header-logo-sticky" alt="Siêu Thị Nội thất ZITO"/><img width="241" height="116" src="https://zito.vn/wp-content/uploads/2017/11/logo-zito-.png" class="header_logo header-logo" alt="Siêu Thị Nội thất ZITO"/><img  width="241" height="116" src="https://zito.vn/wp-content/uploads/2017/11/logo-zito-.png" class="header-logo-dark" alt="Siêu Thị Nội thất ZITO"/></a>
          </div>

          <!-- Mobile Left Elements -->
          <div class="flex-col show-for-medium flex-left">
            <ul class="mobile-nav nav nav-left ">
              <li class="nav-icon has-icon">
  <div class="header-button">		<a href="#" data-open="#main-menu" data-pos="left" data-bg="main-menu-overlay" data-color="dark" class="icon primary button round is-small" aria-controls="main-menu" aria-expanded="false">
		
		  <i class="icon-menu" ></i>
		  		</a>
	 </div> </li>            </ul>
          </div>

          <!-- Left Elements -->
          <div class="flex-col hide-for-medium flex-left
            flex-grow">
            <ul class="header-nav header-nav-main nav nav-left  nav-box nav-size-medium nav-spacing-large nav-uppercase" >
                          </ul>
          </div>

          <!-- Right Elements -->
          <div class="flex-col hide-for-medium flex-right">
            <ul class="header-nav header-nav-main nav nav-right  nav-box nav-size-medium nav-spacing-large nav-uppercase">
              <li id="menu-item-3944" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home  menu-item-3944"><a href="https://zito.vn/" class="nav-top-link">TRANG CHỦ</a></li>
<li id="menu-item-4098" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat  menu-item-4098"><a href="https://zito.vn/danh-muc/sofa-go/" class="nav-top-link">SOFA GỖ</a></li>
<li id="menu-item-4667" class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-4667"><a href="https://zito.vn/danh-muc/ban-tra/" class="nav-top-link">BÀN TRÀ</a></li>
<li id="menu-item-4749" class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-4749"><a href="https://zito.vn/danh-muc/goi-tua/" class="nav-top-link">GỐI TỰA</a></li>
<li id="menu-item-3943" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children  menu-item-3943 has-dropdown"><a href="https://zito.vn/shop/" class="nav-top-link">CỬA HÀNG<i class="icon-angle-down" ></i></a>
<ul class='nav-dropdown nav-dropdown-default'>
	<li id="menu-item-4097" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat  menu-item-4097"><a href="https://zito.vn/danh-muc/sofa-da/">SOFA DA</a></li>
	<li id="menu-item-4100" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat  menu-item-4100"><a href="https://zito.vn/danh-muc/sofa-ni/">SOFA NỈ</a></li>
</ul>
</li>
<li id="menu-item-4698" class="menu-item menu-item-type-custom menu-item-object-custom  menu-item-4698"><a href="https://zito.vn/khuyen-mai/" class="nav-top-link">KHUYẾN MÃI</a></li>
            </ul>
          </div>

          <!-- Mobile Right Elements -->
          <div class="flex-col show-for-medium flex-right">
            <ul class="mobile-nav nav nav-right ">
              <li class="cart-item has-icon">


<a href="https://zito.vn/gio-hang/" class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup" data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">

    <span class="cart-icon image-icon">
    <strong>0</strong>
  </span> 
  </a>

</li>
            </ul>
          </div>

      </div><!-- .header-inner -->
     
            <!-- Header divider -->
      <div class="container"><div class="top-divider full-width"></div></div>
      </div><!-- .header-main -->
<div class="header-bg-container fill"><div class="header-bg-image fill"></div><div class="header-bg-color fill"></div></div><!-- .header-bg-container -->   </div><!-- header-wrapper-->
</header>


<main id="main" class="">

<div id="content" class="blog-wrapper blog-archive page-wrapper">
		<header class="archive-page-header">
	<div class="row">
	<div class="large-12 text-center col">
	<h1 class="page-title is-large uppercase">
		Chuyên mục: <span>Tin tức</span>	</h1>
	<div class="taxonomy-description"><p style="text-align: justify;">Chuyên mục Tin tức <a href="https://zito.vn/">Nội thất ZITO</a> cung cấp cho Quý khách hàng những thông tin hữu ích về những vấn đề xoay quanh việc chọn mua, sử dụng và bảo quản nội thất, sofa tốt nhất. Bao gồm tin tức sofa, bàn trà&#8230;; việc chọn lựa kiểu dáng thiết kế, chất liệu <a href="https://zito.vn/danh-muc/sofa-go/">sofa gỗ</a>, sofa nỉ hay da; Màu sắc đệm mút hợp thời, hợp phong thủy; Các chi phí bỏ ra để chọn được sản phẩm phù hợp với <a href="http://imova.vn/">ngôi nhà</a>, đảm bảo về cả độ thẩm mỹ và bền theo thời gian&#8230; Sẽ không còn mất quá nhiều thời gian, công sức cho việc tìm kiếm thông tin, nghiên cứu khi bạn đến với chúng tôi &#8211; Công ty Cổ phần Nội thất ZITO, showroom số 15 Dương Đình Nghê, Cầu Giấy, Hà Nội.</p>
<h2 style="text-align: justify;"><span style="font-size: 85%;">Mọi thắc mắc xin đừng ngần ngại, hãy liên hệ với đội ngũ nhân viên để được hỗ trợ và tư vấn. Trân trọng !</span></h2>
</div>	</div>
	</div>
</header><!-- .page-header -->


<div class="row row-large ">
	
	<div class="large-9 col">
		


  
    <div id="row-1322045788" class="row large-columns-2 medium-columns- small-columns-1 has-shadow row-box-shadow-1 row-box-shadow-3-hover row-masonry" data-packery-options='{"itemSelector": ".col", "gutter": 0, "presentageWidth" : true}'>

  		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/sofa-vang-cao-cap/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="180" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-768x461.jpg 768w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg 800w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Góp mặt vào sự đa dạng của bộ sưu tập ghế sofa ZITO, sofa văng cao cấp luôn khiến các tín đồ sofa phải say lòng bởi nét nhẹ nhàng, thanh lịch, sang trọng giúp khẳng định phong cách và cá tính của chủ ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">23</span><br>
								<span class="post-date-month is-xsmall">Th1</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/lam-sach-ghe-sofa-da/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="197" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="cách làm sạch ghế sofa da hiệu quả" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Sofa da được biết đến không chỉ bởi vẻ sang trọng vốn có của lớp da. Mà còn có công nặng vượt trội hẳn so với sofa gỗ hay nỉ bởi dễ dàng vệ sinh lau chùi. Nhưng không vì thế mà việc vệ ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">29</span><br>
								<span class="post-date-month is-xsmall">Th11</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/chon-ghe-sofa-cho-phong-khach-nho/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="200" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Sofa Nỉ ZN 401" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-768x512.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg 800w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Chọn ghế sofa cho phòng khách nhỏ</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Hiện nay, trên thị trường nội thất ghế sofa được bày bán với nhiều kiểu dáng, mẫu mã đa dạng. Phù hợp với nhiều không gian, căn phòng khác nhau cho bạn thỏa sức lựa chọ. Để lựa chọn ghế sofa cho phòng khách ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">29</span><br>
								<span class="post-date-month is-xsmall">Th11</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/sofa-go-nem/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="169" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="sofa gỗ nệm" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7.jpg 1365w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Sofa gỗ nệm &#8211; Giải pháp thông minh cho phòng khách đẹp</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Sofa gỗ thiết kế theo phong cách đơn giản, kết hợp khéo léo với đệm tựa êm ái. Không những khiến cho  phòng khách đẹp mà còn ấn tượng và thu hút. Với không gian phòng khách hiện đại thì lựa chọn bộ sofa ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">29</span><br>
								<span class="post-date-month is-xsmall">Th11</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/chon-mau-sac-sofa/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="225" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768-300x225.png" class="lazy-load attachment-medium size-medium wp-post-image" alt="sofa giá rẻ" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768-300x225.png 300w, https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768-768x576.png 768w, https://zito.vn/wp-content/uploads/2017/11/hieuunganh.com_59d74e091137a-1024x768.png 1024w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Chọn màu sắc cho SOFA phù hợp với không gian sống</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Bạn đang muốn tân trang cho căn phòng của mình? Bạn băn khoăn không biết phối màu sao cho không gian hài hòa nhất? Bạn lại không có nhiều chi phí để thuê nhân viên thiết kế nội thất? Và bạn muốn không gian ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">06</span><br>
								<span class="post-date-month is-xsmall">Th10</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/chat-lieu-go-sofa-zito/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="185" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1-300x185.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="sofa gỗ óc chó Hà Nội" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-112-3-300x185-1-260x160.jpg 260w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Chất liệu gỗ sofa phòng khách có tại ZITO</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Gia đình bạn đang có nhu cầu mua 1 bộ sofa gỗ cho phòng khách của mình. Bạn phân vân không biết phải lựa chọn chất liệu gỗ sofa gì, phối màu thế nào sao cho đáng số tiền bỏ ra ? Trên thị ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">14</span><br>
								<span class="post-date-month is-xsmall">Th9</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/sofa-gia-re-ha-noi/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="170" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581-300x170.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="nội thất ZITO" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581-300x170.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581-768x436.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/noi-that-sofa-zito-3-1024x581.jpg 1024w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Nội thất Zito &#8211; Địa chỉ mua sofa giá rẻ Hà Nội</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Bạn đang phân vân mua Sofa giá rẻ Hà Nội bộ bàn ghế bếp, bàn ghế ăn, phòng khách cho căn hộ, nhà ở của mình với chi phí hợp lý nhưng phải đẹp, độc đáo, hiện đại bạn chưa biết tìm ở đâu? ...					</p>
					                                            <p class="from_the_blog_comments uppercase is-xsmall">
                            1 Comment                        </p>
                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">13</span><br>
								<span class="post-date-month is-xsmall">Th7</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/khai-truong-thuong-hieu-noi-that-zito-khuyen-mai-20-30/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="199" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit-300x199.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Nội thất Zito" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit-300x199.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit-768x511.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/IMGP5898-Edit.jpg 961w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Khai trương thương hiệu nội thất Zito khuyến mãi 20 &#8211; 30%</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Trong tháng lễ khai trương từ 9/7 đến ngày 31/7/2017, Quý khách hàng mua sản phẩm tại Zito sẽ được nhận nhiều khuyến mại như:  Giảm giá 30%, tặng kèm bàn trà khi mua bộ sofa và giao hàng miễn phí trong nội thành. ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">10</span><br>
								<span class="post-date-month is-xsmall">Th7</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/xu-huong-lua-chon-sofa-go-he-2017/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="162" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-zito-zg-106-1-300x162-1-300x162.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="Xu hướng lựa chọn sofa gỗ hè 2017" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Xu hướng lựa chọn sofa gỗ</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Xu hướng lựa chọn sofa gỗ Đáp ứng nhu cầu thẩm mỹ ngày càng cao của gia chủ, những chiếc sofa không còn đóng vai trò đơn giản là vật trang trí mà cần phải có công năng sử dụng lớn. Sử dụng các mẫu ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">29</span><br>
								<span class="post-date-month is-xsmall">Th6</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/xu-huong-lua-chon-sofa-phong-khach/" class="plain">
				<div class="box box-text-bottom box-blog-post has-hover">
            					<div class="box-image" >
  						<div class="image-cover" style="padding-top:56%;">
  							<img width="300" height="186" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1-300x186.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="chọn sofa phòng khách năm 2017" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1-130x80.jpg 130w, https://zito.vn/wp-content/uploads/2017/11/sofa-da-zito-zd-202-300x186-1-260x160.jpg 260w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Bắt kịp xu hướng lựa chọn sofa phòng khách năm 2018</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">&#8220;Chọn sofa phòng khách năm 2018&#8221;. Thật thiếu sót nếu bây giờ bạn vẫn loay hoay không biết cách lựa chọn sofa phòng khách. Ngoài các tiêu chí về sở thích việc bắt kịp xu hướng mới nhất sẽ đem lại không gian nhà ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
																<div class="badge absolute top post-date badge-square">
							<div class="badge-inner">
								<span class="post-date-day">29</span><br>
								<span class="post-date-month is-xsmall">Th6</span>
							</div>
						</div>
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
</div>
<ul class="page-numbers nav-pagination links text-center"><li><span class='page-number current'>1</span></li><li><a class='page-number' href='https://zito.vn/tin-tuc/page/2/'>2</a></li><li><a class="next page-number" href="https://zito.vn/tin-tuc/page/2/"><i class="icon-angle-right" ></i></a></li></ul>
	</div> <!-- .large-9 -->

	<div class="post-sidebar large-3 col">
		<div id="secondary" class="widget-area " role="complementary">
		<aside id="woocommerce_product_categories-3" class="widget woocommerce widget_product_categories"><span class="widget-title "><span>Danh mục sản phẩm</span></span><div class="is-divider small"></div><ul class="product-categories"><li class="cat-item cat-item-33"><a href="https://zito.vn/danh-muc/ban-tra/">Bàn Trà</a></li>
<li class="cat-item cat-item-42"><a href="https://zito.vn/danh-muc/goi-tua/">Gối Tựa</a></li>
<li class="cat-item cat-item-34"><a href="https://zito.vn/danh-muc/ke-tivi/">Kệ Tivi</a></li>
<li class="cat-item cat-item-16"><a href="https://zito.vn/danh-muc/phong-khach/">Phòng Khách</a></li>
<li class="cat-item cat-item-24"><a href="https://zito.vn/danh-muc/san-pham-ban-chay/">Sản Phẩm Bán Chạy</a></li>
<li class="cat-item cat-item-23"><a href="https://zito.vn/danh-muc/san-pham-noi-bat/">Sản Phẩm Nổi Bật</a></li>
<li class="cat-item cat-item-20"><a href="https://zito.vn/danh-muc/sofa-chu-u/">Sofa chữ U</a></li>
<li class="cat-item cat-item-21"><a href="https://zito.vn/danh-muc/sofa-da/">Sofa Da</a></li>
<li class="cat-item cat-item-15"><a href="https://zito.vn/danh-muc/sofa-go/">Sofa Gỗ</a></li>
<li class="cat-item cat-item-18"><a href="https://zito.vn/danh-muc/sofa-goc/">Sofa Góc</a></li>
<li class="cat-item cat-item-22"><a href="https://zito.vn/danh-muc/sofa-ni/">Sofa Nỉ</a></li>
<li class="cat-item cat-item-19"><a href="https://zito.vn/danh-muc/sofa-vang/">Sofa Văng</a></li>
</ul></aside><aside id="text-3" class="widget widget_text">			<div class="textwidget"><p><img class="aligncenter size-full wp-image-4246" src="https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito-khuyen-mai-12-17.png" alt="" width="300" height="783" /></p>
</div>
		</aside><aside id="woocommerce_recently_viewed_products-3" class="widget woocommerce widget_recently_viewed_products"><span class="widget-title "><span>Sản phẩm đã xem</span></span><div class="is-divider small"></div><ul class="product_list_widget">
<li>
	<a href="https://zito.vn/shop/goi-tua/zp-meo-02/">
		<img width="130" height="80" src="//zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-130x80.jpg" class="lazy-load attachment-shop_thumbnail size-shop_thumbnail wp-post-image" alt="gối tựa lưng văn phòng" srcset="" data-srcset="//zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-130x80.jpg 130w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-260x160.jpg 260w, //zito.vn/wp-content/uploads/2018/01/ZP-MEO-02-GOI-OM-SOFA-ZITO-520x320.jpg 520w" sizes="(max-width: 130px) 100vw, 130px" />		<span class="product-title">Gối ZP Mèo 02</span>
	</a>
		<ins><span class="woocommerce-Price-amount amount">Liên hệ 2</span></ins></li>
</ul></aside><aside id="media_video-3" class="widget widget_media_video"><span class="widget-title "><span>VIDEO</span></span><div class="is-divider small"></div><div style="width:100%;" class="wp-video"><!--[if lt IE 9]><script>document.createElement('video');</script><![endif]-->
<video class="wp-video-shortcode" id="video-4744-1" preload="metadata" controls="controls"><source type="video/youtube" src="https://youtu.be/1dcFaoMsHNY?_=1" /><a href="https://youtu.be/1dcFaoMsHNY">https://youtu.be/1dcFaoMsHNY</a></video></div></aside></div><!-- #secondary -->
	</div><!-- .post-sidebar -->

</div><!-- .row -->

<div class="nz-bvlq-main"><div class="nz-bvlq"><div class="row large-columns-4 medium-columns-2 small-columns-1 row-small"><h3>Bài viết liên quan</h3><div class="nz-bvlq-content col post-item"><div class="col-inner"><a href="https://zito.vn/sofa-vang-cao-cap/" title="Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất"><div class="nz-bvlq-img"><img width="800" height="480" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg" class="lazy-load attachment-large size-large wp-post-image" alt="" title="Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg 800w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-300x180.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2-768x461.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" /></div><div class="nz-bvlq-td">Sofa Văng cao cấp Lựa chọn Vàng trong ngành nội thất</div></a></div></div><div class="nz-bvlq-content col post-item"><div class="col-inner"><a href="https://zito.vn/lam-sach-ghe-sofa-da/" title="Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ"><div class="nz-bvlq-img"><img width="740" height="486" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg" class="lazy-load attachment-large size-large wp-post-image" alt="cách làm sạch ghế sofa da hiệu quả" title="Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w" sizes="(max-width: 740px) 100vw, 740px" /></div><div class="nz-bvlq-td">Bí quyết làm sạch ghế sofa đơn giản mà hiệu quả bất ngờ</div></a></div></div><div class="nz-bvlq-content col post-item"><div class="col-inner"><a href="https://zito.vn/chon-ghe-sofa-cho-phong-khach-nho/" title="Chọn ghế sofa cho phòng khách nhỏ"><div class="nz-bvlq-img"><img width="800" height="533" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg" class="lazy-load attachment-large size-large wp-post-image" alt="Sofa Nỉ ZN 401" title="Chọn ghế sofa cho phòng khách nhỏ" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5.jpg 800w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-300x200.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-ni-zito-zn-401-5-768x512.jpg 768w" sizes="(max-width: 800px) 100vw, 800px" /></div><div class="nz-bvlq-td">Chọn ghế sofa cho phòng khách nhỏ</div></a></div></div><div class="nz-bvlq-content col post-item"><div class="col-inner"><a href="https://zito.vn/sofa-go-nem/" title="Sofa gỗ nệm &#8211; Giải pháp thông minh cho phòng khách đẹp"><div class="nz-bvlq-img"><img width="1020" height="573" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg" class="lazy-load attachment-large size-large wp-post-image" alt="sofa gỗ nệm" title="Sofa gỗ nệm &#8211; Giải pháp thông minh cho phòng khách đẹp" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-1024x575.jpg 1024w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2017/11/sofa-go-ZITO-ZG-101-7.jpg 1365w" sizes="(max-width: 1020px) 100vw, 1020px" /></div><div class="nz-bvlq-td">Sofa gỗ nệm &#8211; Giải pháp thông minh cho phòng khách đẹp</div></a></div></div></div></div></div><!-- .page-wrapper .blog-wrapper -->


</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

		<section class="section vd" id="section_343540493">
		<div class="bg section-bg fill bg-fill  bg-loaded" >

			
			
			

		</div><!-- .section-bg -->

		<div class="section-content relative">
			

<div class="row row-small align-center"  id="row-303016202">

<div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >

<h3>KHUYẾN MẠI</h3>

  <div class="banner has-hover" id="banner-921184399">
          <div class="banner-inner fill">
        <div class="banner-bg fill" >
            <div class="bg fill bg-fill "></div>
                        <div class="overlay"></div>            
            <div class="effect-snow bg-effect fill no-click"></div>        </div><!-- bg-layers -->
        <div class="banner-layers container">
            <a href="https://zito.vn/khuyen-mai/"  class="fill"><div class="fill banner-link"></div></a>            

   <div id="text-box-1787570479" class="text-box banner-layer x100 md-x100 lg-x100 y90 md-y90 lg-y90 res-text">
                                <div class="text ">
              
              <div class="text-inner text-left">
                  

<a href="https://zito.vn/khuyen-mai/" target="_self" class="button secondary is-larger"  >
    <span>Xem Chi Tiết</span>
  </a>



              </div>
           </div><!-- text-box-inner -->
                            
<style scope="scope">

#text-box-1787570479 {
  width: 60%;
}
#text-box-1787570479 .text {
  font-size: 100%;
}


@media (min-width:550px) {

  #text-box-1787570479 {
    width: 40%;
  }

}
</style>
    </div><!-- text-box -->
 

        </div><!-- .banner-layers -->
      </div><!-- .banner-inner -->

              <div class="height-fix is-invisible"><img width="960" height="540" src="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg" class="attachment-orginal size-orginal" alt="nội thất ZITO khuyến mại" srcset="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg 960w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-768x432.jpg 768w" sizes="(max-width: 960px) 100vw, 960px" /></div>
            
<style scope="scope">

#banner-921184399 .bg.bg-loaded {
  background-image: url(https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg);
}
#banner-921184399 .overlay {
  background-color: rgba(190, 190, 190, 0.2);
}
</style>
  </div><!-- .banner -->


<p>Chương trình áp dụng từ 01/12/2017 đối với dòng sản phẩm Sofa Gỗ Cao cấp Tùy chỉnh cho đơn hàng đặt trên tất các kênh Online và tại Showroom Nội thất ZITO.</p>

</div></div>
<div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >

<h3>DÀNH CHO KHÁCH HÀNG</h3>

  
    <div class="row large-columns-1 medium-columns-1 small-columns-1 row-small has-shadow row-box-shadow-2 row-box-shadow-3-hover">
  		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/khuyen-mai/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:75%;">
  							<img width="300" height="169" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-300x169.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="nội thất ZITO khuyến mại" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-300x169.jpg 300w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12-768x432.jpg 768w, https://zito.vn/wp-content/uploads/2018/01/khuyen-mai-noi-that-zito-thang12.jpg 960w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Chương trình khuyến mại</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Lựa chọn nội thất cho phòng khách là một công việc mất rất nhiều thời gian, công sức &amp; tiền ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/chinh-sach-bao-mat-thong-tin-khach-hang/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:75%;">
  							<img width="300" height="150" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito-300x150.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito-300x150.jpg 300w, https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito-768x384.jpg 768w, https://zito.vn/wp-content/uploads/2017/12/BAO-MAT-THONG-TIN-tai-noi-that-zito.jpg 1001w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Bảo mật thông tin khách hàng</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">MỤC ĐÍCH VÀ PHẠM VI THU THẬP Để truy cập và sử dụng một số dịch vụ tại zito.vn, quý ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
		<div class="col post-item" >
			<div class="col-inner">
			<a href="https://zito.vn/chinh-sach-bao-hanh-luu-y-su-dung/" class="plain">
				<div class="box box-vertical box-text-bottom box-blog-post has-hover">
            					<div class="box-image" style="width:30%;">
  						<div class="image-cover" style="padding-top:75%;">
  							<img width="300" height="197" src="https://zito.vn/wp-content/themes/flatsome/assets/img/lazy.png" data-src="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg" class="lazy-load attachment-medium size-medium wp-post-image" alt="cách làm sạch ghế sofa da hiệu quả" srcset="" data-srcset="https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11-300x197.jpg 300w, https://zito.vn/wp-content/uploads/2017/11/lam-sach-sofa-vai-11.jpg 740w" sizes="(max-width: 300px) 100vw, 300px" />  							  							  						</div>
  						  					</div><!-- .box-image -->
          					<div class="box-text text-left" >
					<div class="box-text-inner blog-post-inner">

					
										<h5 class="post-title is-large ">Chính sách bảo hành, lưu ý sử dụng</h5>
										<div class="is-divider"></div>
										<p class="from_the_blog_excerpt ">Chế độ bảo hành, cách bảo dưỡng sofa và các dịch vụ sau bán hàng được áp dụng như sau: ...					</p>
					                    
					
					
					</div><!-- .box-text-inner -->
					</div><!-- .box-text -->
									</div><!-- .box -->
				</a><!-- .link -->
			</div><!-- .col-inner -->
		</div><!-- .col -->
</div>


</div></div>


<style scope="scope">

</style>
</div>

		</div><!-- .section-content -->

		
<style scope="scope">

#section_343540493 {
  padding-top: 20px;
  padding-bottom: 20px;
  background-color: rgb(241, 241, 242);
}
</style>
	</section>
	
	<section class="section dark has-mask mask-arrow" id="section_1150283209">
		<div class="bg section-bg fill bg-fill  " >

			
			<div class="section-bg-overlay absolute fill"></div>
			

		</div><!-- .section-bg -->

		<div class="section-content relative">
			

<div class="row row-collapse align-middle"  id="row-81934558">

<div class="col medium-4 small-12 large-4"  data-animate="bounceInDown"><div class="col-inner text-center"  >

<h3>ĐỂ ĐƯỢC TƯ VẤN VÀ HỖ TRỢ TỐT NHẤT</h3>
<p><strong>Hãy liên hệ với chúng tôi qua số điện thoại hoặc để lại lời nhắn</strong></p>
<a href="tel:0931050000" target="_self" class="button secondary"  >
  <i class="icon-phone" ></i>  <span>Tel: 0931 05 000</span>
  </a>


<a href="#yeucau" target="_self" class="button secondary is-outline"  >
  <i class="icon-checkmark" ></i>  <span>Gửi yêu cầu</span>
  </a>



</div></div>
<div class="col medium-4 small-12 large-4"  ><div class="col-inner"  >

    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_959102656">
                <div class="img-inner image-zoom image-cover dark" style="padding-top:250px;">
        <img width="1020" height="726" src="https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-1024x729.jpg" class="attachment-large size-large" alt="" srcset="https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-1024x729.jpg 1024w, https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-300x214.jpg 300w, https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-768x547.jpg 768w, https://zito.vn/wp-content/uploads/2017/12/noi-that-zito.jpg 1177w" sizes="(max-width: 1020px) 100vw, 1020px" />                
            </div>
                   
<style scope="scope">

</style>
    </div>
    


</div></div>
<div class="col medium-4 small-12 large-4"  ><div class="col-inner text-center"  >

<div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>

<p><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7448.71647685587!2d105.78392811281238!3d21.018347327980717!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab519c141259%3A0x1f264050ea41406b!2zQ8O0bmcgVHkgTuG7mWkgVGjhuqV0IFppVG8!5e0!3m2!1svi!2s!4v1509783479804" width="100%" height="250" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>

</div></div>


<style scope="scope">

</style>
</div>

		</div><!-- .section-content -->

		
<style scope="scope">

#section_1150283209 {
  padding-top: 30px;
  padding-bottom: 30px;
  min-height: 350px;
}
#section_1150283209 .section-bg-overlay {
  background-color: rgba(0, 0, 0, 0.59);
}
#section_1150283209 .section-bg.bg-loaded {
  background-image: url(https://zito.vn/wp-content/uploads/2017/12/sofa-go-zito.jpg);
}
</style>
	</section>
	
	<section class="section kh" id="section_1003730547">
		<div class="bg section-bg fill bg-fill  bg-loaded" >

			
			
			

		</div><!-- .section-bg -->

		<div class="section-content relative">
			

<div class="row align-middle align-center"  id="row-1942243703">

<div class="col small-12 large-12"  ><div class="col-inner"  >

<h3 style="text-align: center;">KHÁCH HÀNG CỦA CHÚNG TÔI ĐANG SỐNG TẠI</h3>
<div class="slider-wrapper relative " id="slider-1362143521" >
    <div class="slider slider-nav-simple slider-nav-large slider-nav-dark slider-nav-outside slider-style-normal"
        data-flickity-options='{
            "cellAlign": "center",
            "imagesLoaded": true,
            "lazyLoad": 1,
            "freeScroll": true,
            "wrapAround": true,
            "autoPlay": 1500,
            "pauseAutoPlayOnHover" : false,
            "prevNextButtons": true,
            "contain" : true,
            "adaptiveHeight" : true,
            "dragThreshold" : 5,
            "percentPosition": true,
            "pageDots": false,
            "rightToLeft": false,
            "draggable": true,
            "selectedAttraction": 0.1,
            "parallax" : 0,
            "friction": 0.6        }'
        >
        

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 228.89502762431px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/du-an-chung-cu-goldmark-city-136-ho-tung-mau-logo.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 315px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-ecolife-4.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 326.47058823529px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/eco-green-city.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 260px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-park-hill-premium.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 214.53781512605px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/Logo-Vinhomes-Gardenia.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 229.69811320755px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logox2.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 165.83756345178px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-hd-mon-city.jpg" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 204.0932642487px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-imperia-garden.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 273.47826086957px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-mandarin-garden-2.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 207.77777777778px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-hoang-cau-tan-hoang-minh.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 222px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/homcitytrungkinh12017522123919.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 138px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/180x200.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 321.70731707317px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/logo-flc-36-pham-hung-1.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>

<div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 252.22222222222px!important"><div class="ux-logo-link block image-zoom" title="" target="_self" href="" style="padding: 15px;"><img src="https://zito.vn/wp-content/uploads/2017/12/noi-that-zito-tai-ho-guom-plaza.png" title="" alt="" class="ux-logo-image block" style="height:120px;" /></div></div>


     </div>

     <div class="loading-spin dark large centered"></div>

     <style scope="scope">
             </style>
</div><!-- .ux-slider-wrapper -->



</div></div>


<style scope="scope">

#row-1942243703 > .col > .col-inner {
  padding: 30px 0px 0px 0px;
}
</style>
</div>

		</div><!-- .section-content -->

		
<style scope="scope">

#section_1003730547 {
  padding-top: 0px;
  padding-bottom: 0px;
  background-color: rgb(255, 255, 255);
}
</style>
	</section>
	

	<section class="section sec0f dark" id="section_1980443190">
		<div class="bg section-bg fill bg-fill  bg-loaded" >

			
			
			

		</div><!-- .section-bg -->

		<div class="section-content relative">
			

<div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>

<div class="row ic"  id="row-64715111">

<div class="col medium-4 small-12 large-4"  ><div class="col-inner"  >


    <div class="icon-box featured-box icon-box-left text-left"  >

                <div class="icon-box-img" style="width: 60px">
          <div class="icon">
            <div class="icon-inner" style="color:rgb(255, 203, 68);">
              <?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
<g>
	<path d="M310.634,43.714H297.17c-7.194,0-13.026-5.833-13.026-13.026V30.25C284.144,13.543,270.6,0,253.893,0h-0.699   c-16.707,0-30.25,13.543-30.25,30.25v0.438c0,7.194-5.833,13.027-13.026,13.027h-13.464c-14.341,0-25.966,11.625-25.966,25.966   v0.525c0,14.341,11.625,25.966,25.966,25.966h114.183c14.34,0,25.965-11.626,25.965-25.966V69.68   C336.601,55.34,324.976,43.714,310.634,43.714z M253.543,43.714c-7.243,0-13.114-5.872-13.114-13.114   c0-7.243,5.872-13.115,13.114-13.115c7.243,0,13.114,5.872,13.114,13.115C266.657,37.843,260.786,43.714,253.543,43.714z    M311.929,550.8H91.8c-19.314,0-34.971-15.657-34.971-34.971V96.171c0-19.314,15.657-34.971,26.228-34.971h70.854   c-0.591,2.904-0.91,5.914-0.91,9.008c0,23.958,19.492,43.45,43.454,43.45h114.178c23.963,0,43.453-19.492,43.453-43.979   c0-2.903-0.311-5.733-0.855-8.479h53.312c28.059,0,43.715,15.657,43.715,34.971v262.728c-2.906-0.181-5.791-0.442-8.742-0.442   c-12.094,0-23.771,1.704-34.973,4.604V157.371H100.543v349.715h201.528C303.029,522.453,306.387,537.162,311.929,550.8z    M441.515,384.686c-62.771,0-113.656,50.887-113.656,113.657S378.743,612,441.515,612s113.656-50.887,113.656-113.657   S504.286,384.686,441.515,384.686z M515.782,474.991l-69.943,78.686c-3.445,3.877-8.244,5.865-13.076,5.865   c-3.838,0-7.697-1.255-10.916-3.833l-43.715-34.971c-7.539-6.028-8.764-17.034-2.727-24.573c6.027-7.547,17.037-8.769,24.576-2.731   l30.748,24.598l58.916-66.28c6.408-7.215,17.465-7.872,24.684-1.451C521.55,456.721,522.198,467.769,515.782,474.991z M362.829,306   H222.944c-4.829,0-8.743-3.915-8.743-8.743v-8.743c0-4.829,3.914-8.743,8.743-8.743h139.885c4.828,0,8.744,3.915,8.744,8.743v8.743   C371.571,302.085,367.657,306,362.829,306z M362.829,227.314H222.944c-4.829,0-8.743-3.915-8.743-8.743v-8.743   c0-4.829,3.914-8.743,8.743-8.743h139.885c4.828,0,8.744,3.915,8.744,8.743v8.743C371.571,223.4,367.657,227.314,362.829,227.314z    M319.114,375.943h-96.17c-4.829,0-8.743-3.915-8.743-8.743v-8.743c0-4.829,3.914-8.743,8.743-8.743h96.17   c4.83,0,8.744,3.914,8.744,8.743v8.743C327.856,372.028,323.944,375.943,319.114,375.943z M301.629,454.629h-78.686   c-4.829,0-8.743-3.915-8.743-8.743v-8.743c0-4.829,3.914-8.743,8.743-8.743h78.686c4.829,0,8.743,3.914,8.743,8.743v8.743   C310.372,450.714,306.458,454.629,301.629,454.629z M197.461,194.981l-32.199,36.224c-1.586,1.785-3.795,2.701-6.02,2.701   c-1.767,0-3.543-0.578-5.025-1.765l-20.125-16.099c-3.471-2.775-4.034-7.841-1.256-11.312c2.774-3.475,7.844-4.037,11.314-1.258   l14.155,11.323l27.123-30.513c2.95-3.321,8.04-3.624,11.363-0.668C200.117,186.57,200.415,191.656,197.461,194.981z    M197.461,272.767l-32.199,36.223c-1.586,1.785-3.795,2.7-6.02,2.7c-1.767,0-3.543-0.577-5.025-1.765l-20.125-16.099   c-3.471-2.775-4.034-7.841-1.256-11.312c2.774-3.475,7.844-4.037,11.314-1.258l14.155,11.324l27.123-30.513   c2.95-3.321,8.04-3.624,11.363-0.667C200.117,264.355,200.415,269.442,197.461,272.767z M197.461,347.156l-32.199,36.224   c-1.586,1.784-3.795,2.7-6.02,2.7c-1.767,0-3.543-0.577-5.025-1.765l-20.125-16.1c-3.471-2.774-4.034-7.841-1.256-11.312   c2.774-3.475,7.844-4.037,11.314-1.258l14.155,11.323l27.123-30.513c2.95-3.321,8.04-3.624,11.363-0.668   C200.117,338.745,200.415,343.831,197.461,347.156z M197.461,422.07l-32.199,36.224c-1.586,1.785-3.795,2.7-6.02,2.7   c-1.767,0-3.543-0.577-5.025-1.765l-20.125-16.1c-3.471-2.774-4.034-7.841-1.256-11.312c2.774-3.475,7.844-4.037,11.314-1.258   l14.155,11.323l27.123-30.513c2.95-3.321,8.04-3.624,11.363-0.668C200.117,413.659,200.415,418.746,197.461,422.07z" fill="#ED1651"/>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
             </div>
          </div>
        </div>
                <div class="icon-box-text last-reset">

                        
            

<h3><span style="color: #ffcb44;">Đảm bảo chất lượng</span></h3>
<p>Chế bảo hành tới 02 năm</p>

        </div>
  </div><!-- .icon-box -->
  
  

</div></div>
<div class="col medium-4 small-12 large-4"  ><div class="col-inner"  >


    <div class="icon-box featured-box icon-box-left text-left"  >

                <div class="icon-box-img" style="width: 60px">
          <div class="icon">
            <div class="icon-inner" style="color:rgb(255, 203, 68);">
              <?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 612.001 612" style="enable-background:new 0 0 612.001 612;" xml:space="preserve">
<g>
	<path d="M604.131,440.17h-19.12V333.237c0-12.512-3.776-24.787-10.78-35.173l-47.92-70.975   c-11.725-17.311-31.238-27.698-52.169-27.698h-74.28c-8.734,0-15.737,7.082-15.737,15.738v225.043H262.475   c11.567,9.992,19.514,23.92,21.796,39.658H412.53c4.563-31.238,31.475-55.396,63.972-55.396c32.498,0,59.33,24.158,63.895,55.396   h63.735c4.328,0,7.869-3.541,7.869-7.869V448.04C612,443.713,608.46,440.17,604.131,440.17z M525.76,312.227h-98.044   c-4.327,0-7.868-3.463-7.868-7.869v-54.372c0-4.328,3.541-7.869,7.868-7.869h59.724c2.597,0,4.957,1.259,6.452,3.305l38.32,54.451   C535.831,305.067,532.133,312.227,525.76,312.227z M476.502,440.17c-27.068,0-48.943,21.953-48.943,49.021   c0,26.99,21.875,48.943,48.943,48.943c26.989,0,48.943-21.953,48.943-48.943C525.445,462.125,503.491,440.17,476.502,440.17z    M476.502,513.665c-13.535,0-24.472-11.016-24.472-24.471c0-13.535,10.937-24.473,24.472-24.473   c13.533,0,24.472,10.938,24.472,24.473C500.974,502.649,490.036,513.665,476.502,513.665z M68.434,440.17   c-4.328,0-7.869,3.543-7.869,7.869v23.922c0,4.328,3.541,7.869,7.869,7.869h87.971c2.282-15.738,10.229-29.666,21.718-39.658   H68.434V440.17z M220.298,440.17c-26.989,0-48.943,21.953-48.943,49.021c0,26.99,21.954,48.943,48.943,48.943   c27.068,0,48.943-21.953,48.943-48.943C269.242,462.125,247.367,440.17,220.298,440.17z M220.298,513.665   c-13.534,0-24.471-11.016-24.471-24.471c0-13.535,10.937-24.473,24.471-24.473c13.534,0,24.472,10.938,24.472,24.473   C244.77,502.649,233.832,513.665,220.298,513.665z M338.014,150.605h-91.198c4.485,13.298,6.846,27.54,6.846,42.255   c0,74.28-60.431,134.711-134.711,134.711c-13.535,0-26.675-2.045-39.029-5.744v86.949c0,4.328,3.541,7.869,7.869,7.869h265.96   c4.329,0,7.869-3.541,7.869-7.869V174.211C361.619,161.149,351.075,150.605,338.014,150.605z M118.969,73.866   C53.264,73.866,0,127.129,0,192.834s53.264,118.969,118.969,118.969s118.97-53.264,118.97-118.969S184.674,73.866,118.969,73.866z    M118.969,284.73c-50.752,0-91.896-41.143-91.896-91.896c0-50.753,41.144-91.896,91.896-91.896   c50.753,0,91.896,41.144,91.896,91.896C210.865,243.587,169.722,284.73,118.969,284.73z M154.066,212.242   c-1.014,0-2.052-0.131-3.082-0.407L112.641,201.5c-5.148-1.391-8.729-6.062-8.729-11.396v-59.015   c0-6.516,5.287-11.803,11.803-11.803c6.516,0,11.803,5.287,11.803,11.803v49.971l29.614,7.983   c6.294,1.698,10.02,8.177,8.322,14.469C164.033,208.776,159.269,212.242,154.066,212.242z" fill="#ED1651"/>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
             </div>
          </div>
        </div>
                <div class="icon-box-text last-reset">

                        
            

<h3><span style="color: #ffcb44;">GIAO HÀNG NHANH CHÓNG</span></h3>
<p>Miễn phí giao hàng nội thành</p>

        </div>
  </div><!-- .icon-box -->
  
  

</div></div>
<div class="col medium-4 small-12 large-4"  ><div class="col-inner"  >


    <div class="icon-box featured-box icon-box-left text-left"  >

                <div class="icon-box-img" style="width: 60px">
          <div class="icon">
            <div class="icon-inner" style="color:rgb(255, 203, 68);">
              <?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 612.001 612" style="enable-background:new 0 0 612.001 612;" xml:space="preserve">
<g>
	<path d="M424.055,1.842c-103.799,0-187.944,66.595-187.944,148.745c0,48.941,29.999,92.226,76.091,119.333   c-4.508,24.715-18.863,53.832-38.936,73.906c49.008-3.658,92.142-17.711,120.385-46.613c9.921,1.28,20.028,2.119,30.405,2.119   c103.8,0,187.945-66.595,187.945-148.745C612,68.438,527.854,1.842,424.055,1.842z M335.953,179.954   c-16.219,0-29.367-13.148-29.367-29.368c0-16.219,13.148-29.368,29.367-29.368s29.367,13.149,29.367,29.368   C365.32,166.807,352.172,179.954,335.953,179.954z M424.055,179.954c-16.219,0-29.367-13.148-29.367-29.368   c0-16.219,13.148-29.368,29.367-29.368c16.22,0,29.367,13.149,29.367,29.368C453.422,166.807,440.274,179.954,424.055,179.954z    M512.157,179.954c-16.22,0-29.367-13.148-29.367-29.368c0-16.219,13.148-29.368,29.367-29.368   c16.22,0,29.368,13.149,29.368,29.368C541.525,166.807,528.377,179.954,512.157,179.954z M387.577,492.87   c-59.589-47.889-76.252-24.348-103.29,2.686c-18.875,18.883-66.644-20.549-107.889-61.797   c-41.248-41.252-80.671-89.012-61.797-107.891c27.039-27.035,50.573-43.708,2.67-103.279c-47.887-59.595-79.809-13.842-106,12.351   c-30.237,30.227-1.593,142.87,109.735,254.218c111.344,111.33,223.987,139.955,254.207,109.74   C401.4,572.702,447.167,540.784,387.577,492.87z M74.38,254.898c-0.804,0.329-1.984,0.812-3.485,1.426   c-2.841,1.088-6.238,2.32-10.019,3.965c-1.764,0.85-3.742,1.77-5.546,2.904c-1.871,1.117-3.73,2.384-5.406,3.951   c-1.831,1.482-3.254,3.376-4.901,5.361c-1.224,2.189-2.804,4.426-3.657,7.114c-1.239,2.544-1.683,5.475-2.423,8.358   c-0.57,2.925-0.669,5.965-1.032,8.935c-0.037,6.001,0.196,11.765,0.969,16.677c0.404,4.994,1.672,9.213,2.172,12.137   c0.613,2.934,0.962,4.609,0.962,4.609s-0.901-1.457-2.478-4.008c-1.495-2.629-4.033-6.006-6.261-10.807   c-2.568-4.668-4.963-10.412-7.242-16.874c-0.811-3.334-1.911-6.792-2.52-10.499c-0.429-3.746-1.138-7.61-0.853-11.679   c-0.123-4.062,0.708-8.167,1.463-12.315c1.201-4.019,2.48-8.092,4.44-11.716c1.79-3.704,4.044-7.057,6.296-9.98   c2.266-3.013,4.604-5.479,6.808-7.773c4.397-4.326,8.215-7.487,10.439-9.427c1.273-1.066,1.794-1.643,2.362-2.149   c0.502-0.47,0.77-0.721,0.77-0.721l20.435,31.961C75.676,254.348,75.225,254.539,74.38,254.898z M381.067,558.52   c-2.005,2.496-5.149,6.357-9.413,10.732c-2.272,2.154-4.616,4.488-7.534,6.693c-2.732,2.283-6.055,4.373-9.438,6.334   c-3.488,1.918-7.199,3.562-11.118,4.785c-3.879,1.182-7.905,2.135-11.858,2.457l-5.88,0.42l-5.704-0.307   c-3.753-0.049-7.249-1.049-10.616-1.609c-3.358-0.623-6.403-1.871-9.319-2.707c-2.916-0.787-5.531-2.225-7.95-3.182   c-4.9-1.945-8.41-4.193-11.057-5.584c-2.6-1.434-4.084-2.252-4.084-2.252s1.675,0.258,4.607,0.707   c2.913,0.391,7.166,1.311,12.091,1.41c2.48,0.051,5.024,0.484,7.843,0.223c2.782-0.217,5.684-0.076,8.588-0.596   c2.884-0.578,5.877-0.742,8.654-1.791l4.172-1.279l3.87-1.809c10.218-4.949,15.777-13.668,18.999-20.947   c1.594-3.754,2.735-7.068,3.597-9.662c0.938-2.73,1.473-4.289,1.473-4.289l33.1,18.535   C384.088,554.803,382.989,556.157,381.067,558.52z" fill="#ED1651"/>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
             </div>
          </div>
        </div>
                <div class="icon-box-text last-reset">

                        
            

<h3><span style="color: #ffcb44;">Tư vấn - Hỗ trợ 24/7</span></h3>
<p>Liên hệ để được tư vấn miễn phí tại nhà</p>

        </div>
  </div><!-- .icon-box -->
  
  

</div></div>


<style scope="scope">

</style>
</div>
<div class="row row-small"  id="row-1889824110">

<div class="col col4 col41 medium-3 small-12 large-3"  ><div class="col-inner"  >

<p style="text-align: justify;"><strong>Nội thất ZITO</strong> mong muốn đem đến những trải nghiệm cuộc sống tuyệt vời nhất cho bạn và gia đình. Phục vụ theo nhu cầu cho từng đối tượng khách hàng về thiết kế, lựa chọn kích thước, chất liệu, màu sắc với mức giá thành tốt nhất thị trường. Đảm bảo về quy trình sản xuất gia công chuẩn chỉ, chất lượng.</p>
<strong>Công ty Cổ phần Nội thất ZITO</strong></br>
<strong>Showroom:</strong> 15 Dương Đình Nghệ, Yên Hoà, Cầu Giấy, Hà Nội</br>
<strong>Tel:</strong> (024) 2212 4111 / 0931.05.0000</br>
<strong>Email:</strong> zitosofa@gmail.com</br>
<strong>Số ĐKKD:</strong> 0107889261 - Cấp bởi Sở kế Hoạch Đầu Tư Thành phố Hà Nội


</div></div>
<div class="col col4 col41 medium-2 small-12 large-2"  ><div class="col-inner"  >

<h5 class="uppercase">Liên kết nhanh</h5>
<div class="menu-main-menu-nz-container"><ul id="menu-main-menu-nz-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-3944"><a href="https://zito.vn/">TRANG CHỦ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4098"><a href="https://zito.vn/danh-muc/sofa-go/">SOFA GỖ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4667"><a href="https://zito.vn/danh-muc/ban-tra/">BÀN TRÀ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4749"><a href="https://zito.vn/danh-muc/goi-tua/">GỐI TỰA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3943"><a href="https://zito.vn/shop/">CỬA HÀNG</a>
<ul  class="sub-menu">
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4097"><a href="https://zito.vn/danh-muc/sofa-da/">SOFA DA</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4100"><a href="https://zito.vn/danh-muc/sofa-ni/">SOFA NỈ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4698"><a href="https://zito.vn/khuyen-mai/">KHUYẾN MÃI</a></li>
</ul></div>

</div></div>
<div class="col col4 col41 medium-3 small-12 large-3"  ><div class="col-inner"  >

<h5 class="uppercase" style="text-align: center;">Đăng ký nhận khuyến mãi</h5>
<div role="form" class="wpcf7" id="wpcf7-f4110-o1" lang="vi" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/tin-tuc/#wpcf7-f4110-o1" method="post" class="wpcf7-form demo" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="4110" />
<input type="hidden" name="_wpcf7_version" value="4.9.1" />
<input type="hidden" name="_wpcf7_locale" value="vi" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4110-o1" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<div class="nz-cf7 dkemail">
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name" aria-required="true" aria-invalid="false" placeholder="Tên quý khách" /></span>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span>
</div>
<div class="nz-cf7-f1">
<input type="submit" value="Đăng ký" class="wpcf7-form-control wpcf7-submit c-button" />
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>

<div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>

<h5 class="uppercase" style="text-align: center;">Chia sẻ Websitei</h5>
<div class="social-icons share-icons share-row relative icon-style-fill full-width text-center" ><a href="//www.facebook.com/sharer.php?u=https://zito.vn/sofa-vang-cao-cap/" data-label="Facebook" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip facebook" title="Share on Facebook"><i class="icon-facebook" ></i></a><a href="//twitter.com/share?url=https://zito.vn/sofa-vang-cao-cap/" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip twitter" title="Share on Twitter"><i class="icon-twitter" ></i></a><a href="mailto:enteryour@addresshere.com?subject=Sofa%20V%C4%83ng%20cao%20c%E1%BA%A5p%20L%E1%BB%B1a%20ch%E1%BB%8Dn%20V%C3%A0ng%20trong%20ng%C3%A0nh%20n%E1%BB%99i%20th%E1%BA%A5t&amp;body=Check%20this%20out:%20https://zito.vn/sofa-vang-cao-cap/" rel="nofollow" class="icon primary button circle tooltip email" title="Email to a Friend"><i class="icon-envelop" ></i></a><a href="//pinterest.com/pin/create/button/?url=https://zito.vn/sofa-vang-cao-cap/&amp;media=https://zito.vn/wp-content/uploads/2018/01/bi-quyet-chon-sofa-dep-cho-phong-khach-cua-ban-2.jpg&amp;description=Sofa%20V%C4%83ng%20cao%20c%E1%BA%A5p%20L%E1%BB%B1a%20ch%E1%BB%8Dn%20V%C3%A0ng%20trong%20ng%C3%A0nh%20n%E1%BB%99i%20th%E1%BA%A5t" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" target="_blank" class="icon primary button circle tooltip pinterest" title="Pin on Pinterest"><i class="icon-pinterest" ></i></a><a href="//plus.google.com/share?url=https://zito.vn/sofa-vang-cao-cap/" target="_blank" class="icon primary button circle tooltip google-plus" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;" rel="nofollow" title="Share on Google+"><i class="icon-google-plus" ></i></a><a href="//www.linkedin.com/shareArticle?mini=true&url=https://zito.vn/sofa-vang-cao-cap/&title=Sofa%20V%C4%83ng%20cao%20c%E1%BA%A5p%20L%E1%BB%B1a%20ch%E1%BB%8Dn%20V%C3%A0ng%20trong%20ng%C3%A0nh%20n%E1%BB%99i%20th%E1%BA%A5t" onclick="window.open(this.href,this.title,'width=500,height=500,top=300px,left=300px');  return false;"  rel="nofollow" target="_blank" class="icon primary button circle tooltip linkedin" title="Share on LinkedIn"><i class="icon-linkedin" ></i></a></div>

    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1310822527">
    <a href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=38434" target="_self" class="">            <div class="img-inner dark" style="margin:0px 15px 10px 15px;">
        <img width="395" height="149" src="https://zito.vn/wp-content/uploads/2017/12/da-thong-bao-bo-cong-thuong.png" class="attachment-large size-large" alt="" srcset="https://zito.vn/wp-content/uploads/2017/12/da-thong-bao-bo-cong-thuong.png 395w, https://zito.vn/wp-content/uploads/2017/12/da-thong-bao-bo-cong-thuong-300x113.png 300w" sizes="(max-width: 395px) 100vw, 395px" />                
            </div>
            </a>       
<style scope="scope">

</style>
    </div>
    


</div></div>
<div class="col col4 col41 medium-4 small-12 large-4"  ><div class="col-inner"  >

<div class="fb-page" data-href="https://www.facebook.com/zito.vn/" data-tabs="timeline" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false">
<blockquote class="fb-xfbml-parse-ignore" cite="https://www.facebook.com/zito.vn/" rel="nofollow">
<p><a href="https://www.facebook.com/zito.vn/" rel="nofollow">Nội Thất ZITO</a></p>
</blockquote>
</div>

</div></div>


<style scope="scope">

</style>
</div>
<div class="type nz-button-2">
<div><a href="tel:0931050000" class="nz-bt nz-bt-2"><span class="txt">0931.05.0000</span><span class="round"><i class="fa fa-phone faa-ring faa-slow animated"></i></span></a></div>
<style>
.type.nz-button-2 {display: block;position: fixed;left: 20px;bottom: 5%;text-align: center;z-index: 69696969;}
.type.nz-button-2 > div {margin: 5px auto;}
.nz-button-2 .nz-bt-1 {background-color: #C69a66;}
.nz-button-2 .nz-bt-1 .round {background-color: #fdcc30;}
.nz-button-2 .nz-bt-2 {background-color: #C69a66;}
.nz-button-2 .nz-bt-2 .round {background-color: #fdcc30;}
.nz-button-2 a { line-height: 16px; text-decoration: none;-moz-border-radius: 40px;-webkit-border-radius: 30px;border-radius: 40px;padding: 12px 53px 12px 23px;color: #fff;text-transform: uppercase;font-family: sans-serif;font-weight: bold;position: relative;-moz-transition: all 0.3s;-o-transition: all 0.3s;-webkit-transition: all 0.3s;transition: all 0.3s;display: inline-block;}
.nz-button-2 a span {position: relative;z-index: 3;}
.nz-button-2 a .round {-moz-border-radius: 50%;-webkit-border-radius: 50%;border-radius: 50%;width: 38px;height: 38px;position: absolute;right: 3px;top: 3px;-moz-transition: all 0.3s ease-out;-o-transition: all 0.3s ease-out;-webkit-transition: all 0.3s ease-out;transition: all 0.3s ease-out;z-index: 2;}
.nz-button-2 a .round i {position: absolute;top: 50%;margin-top: -6px;left: 50%;margin-left: -4px;-moz-transition: all 0.3s;-o-transition: all 0.3s;-webkit-transition: all 0.3s;transition: all 0.3s;}
.nz-button-2 .txt {font-size: 14px;line-height: 1.45;}
.nz-button-2.type a:hover {padding-left: 48px;padding-right: 28px;}
.nz-button-2.type a:hover .round {width: calc(100% - 6px);-moz-border-radius: 30px;-webkit-border-radius: 30px;border-radius: 30px;}
.nz-button-2.type a:hover .round i {left: 12%;}
#dktv form.wpcf7-form {max-width: 70%;margin: 0 auto;}
div#dktv .col.small-12.large-12, div#dktv .col.medium-6.small-12.large-6 {padding: 0;}
#dktv .nz-tensp {color: #fff;border: 0;}
div#dktv > div {margin: 0;}
div#dktv > div {background: url(/wp-content/uploads/2017/08/bg-popup.png);}
.bn-dktv {background: url(/wp-content/uploads/2017/08/bg-popup.jpg);background-repeat: no-repeat;background-size: cover;}</p>
<p>/***************MOBILE ***************/
@media only screen and (max-width: 48em) {
.nz-button-2 a {padding: 12px 53px 12px 10px;}
.type.nz-button-2 > div {margin: 0px auto;display: table-cell;}
.type.nz-button-2 {left: 0px;bottom: 0;text-align: left;margin: 0;}
}
</style>


		</div><!-- .section-content -->

		
<style scope="scope">

#section_1980443190 {
  padding-top: 0px;
  padding-bottom: 0px;
  background-color: rgb(35, 35, 35);
}
</style>
	</section>
	
<div class="absolute-footer dark medium-text-center text-center">
  <div class="container clearfix">

    
    <div class="footer-primary pull-left">
                          <div class="copyright-footer">
        <span style="color: #ddd;">Copyright 2018 &copy; Bản quyền thuộc về <strong>ZITO</strong> </span>

<div id="yeucau"
    class="lightbox-by-id lightbox-content mfp-hide lightbox-white "
    style="max-width:80% ;padding:20px">
    

	<section class="section dark" id="section_1691845738">
		<div class="bg section-bg fill bg-fill  " >

			
			<div class="section-bg-overlay absolute fill"></div>
			

		</div><!-- .section-bg -->

		<div class="section-content relative">
			

<div class="row align-center sectuvan" style="max-width:870px" id="row-1228680063">

<div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >

<p class="lead" style="text-align: center;"><span style="font-size: 95%;">Nếu quý khách có bất kỳ thắc mắc nào có thể liên hệ với chúng tôi qua thông tin bên dưới hoặc để lại lời nhắn vào hộp thoại bên canh</span></p>
<strong>Công ty Cổ phần Nội thất ZITO</strong></br>
<strong>Showroom:</strong> 15 Dương Đình Nghệ, Yên Hoà, Cầu Giấy, Hà Nội</br>
<strong>Tel:</strong> (024) 2212 4111 / 0931.05.0000</br>
<strong>Email:</strong> zitosofa@gmail.com</br>
<strong>Số ĐKKD:</strong> 0107889261 - Cấp bởi Sở kế Hoạch Đầu Tư Thành phố Hà Nội


</div></div>
<div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >

<p><center></p>
<h3>Hãy để lại lời nhắn<br />Để chúng tôi có thể tư vấn cho bạn</h3>
<p></center></p>
<div role="form" class="wpcf7" id="wpcf7-f3055-o2" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/tin-tuc/#wpcf7-f3055-o2" method="post" class="wpcf7-form demo" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="3055" />
<input type="hidden" name="_wpcf7_version" value="4.9.1" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f3055-o2" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<div class="nz-cf7">
<div class="nz-cf7-f1 nz-1-3">
<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name" aria-required="true" aria-invalid="false" placeholder="Tên quý khách" /></span>
</div>
<div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
<span class="wpcf7-form-control-wrap your-phone"><input type="tel" name="your-phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" id="your-phone" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span>
</div>
<div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span>
</div>
<div class="nz-cf7-f1">
<span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" id="your-message" aria-required="true" aria-invalid="false" placeholder="Lời nhắn thêm (có thể để trống)"></textarea></span>
</div>
<div class="nz-cf7-f1">
<input type="submit" value="Gửi tin yêu cầu tư vấn" class="wpcf7-form-control wpcf7-submit c-button" />
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>


</div></div>


<style scope="scope">

</style>
</div>

		</div><!-- .section-content -->

		
<style scope="scope">

#section_1691845738 {
  padding-top: 0px;
  padding-bottom: 0px;
}
#section_1691845738 .section-bg-overlay {
  background-color: rgba(0, 0, 0, 0.7);
}
#section_1691845738 .section-bg.bg-loaded {
  background-image: 3272;
}
</style>
	</section>
	

</div>

      </div>
          </div><!-- .left -->
  </div><!-- .container -->
</div><!-- .absolute-footer -->
<a href="#top" class="back-to-top button invert plain is-outline hide-for-medium icon circle fixed bottom z-1" id="top-link"><i class="icon-angle-up" ></i></a>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<!-- Mobile Sidebar -->
<div id="main-menu" class="mobile-sidebar no-scrollbar mfp-hide">
    <div class="sidebar-menu no-scrollbar ">
        <ul class="nav nav-sidebar  nav-vertical nav-uppercase">
              <li class="header-search-form search-form html relative has-icon">
	<div class="header-search-form-wrapper">
		<div class="searchform-wrapper ux-search-box relative form-flat is-normal"><form method="get" class="searchform" action="https://zito.vn/" role="search">
		<div class="flex-row relative">
									<div class="flex-col flex-grow">
			  <input type="search" class="search-field mb-0" name="s" value="" placeholder="Tìm kiếm..." />
		    <input type="hidden" name="post_type" value="product" />
        			</div><!-- .flex-col -->
			<div class="flex-col">
				<button type="submit" class="ux-search-submit submit-button secondary button icon mb-0">
					<i class="icon-search" ></i>				</button>
			</div><!-- .flex-col -->
		</div><!-- .flex-row -->
	 <div class="live-search-results text-left z-top"></div>
</form>
</div>	</div>
</li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-3944"><a href="https://zito.vn/" class="nav-top-link">TRANG CHỦ</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4098"><a href="https://zito.vn/danh-muc/sofa-go/" class="nav-top-link">SOFA GỖ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4667"><a href="https://zito.vn/danh-muc/ban-tra/" class="nav-top-link">BÀN TRÀ</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4749"><a href="https://zito.vn/danh-muc/goi-tua/" class="nav-top-link">GỐI TỰA</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3943"><a href="https://zito.vn/shop/" class="nav-top-link">CỬA HÀNG</a>
<ul class=children>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4097"><a href="https://zito.vn/danh-muc/sofa-da/">SOFA DA</a></li>
	<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-4100"><a href="https://zito.vn/danh-muc/sofa-ni/">SOFA NỈ</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4698"><a href="https://zito.vn/khuyen-mai/" class="nav-top-link">KHUYẾN MÃI</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4082"><a href="https://zito.vn/gioi-thieu-zito/" class="nav-top-link">Giới thiệu</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-4636"><a href="https://zito.vn/danh-cho-khach-hang/" class="nav-top-link">Hướng dẫn – Hỗ trợ</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent menu-item-4310"><a href="https://zito.vn/tin-tuc/" class="nav-top-link">Tin tức</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4081"><a href="https://zito.vn/lien-he/" class="nav-top-link">Liên hệ</a></li>
        </ul>
    </div><!-- inner -->
</div><!-- #mobile-menu -->
  <script id="lazy-load-icons">
    /* Lazy load icons css file */
    var fl_icons = document.createElement('link');
    fl_icons.rel = 'stylesheet';
    fl_icons.href = 'https://zito.vn/wp-content/themes/flatsome/assets/css/fl-icons.css';
    fl_icons.type = 'text/css';
    var fl_icons_insert = document.getElementsByTagName('link')[0];
    fl_icons_insert.parentNode.insertBefore(fl_icons, fl_icons_insert);
  </script>
  <!-- vchat Code -->
<script lang="javascript">(function() {var pname = ( (document.title !='')? document.title : ((document.querySelector('h1') != null)? document.querySelector('h1').innerHTML : '') );var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async=1; ga.src = '//live.vnpgroup.net/js/web_client_box.php?hash=c7333b81a4ad488caa843b848d0d0015&data=eyJzc29faWQiOjUwNDE1NDUsImhhc2giOiIxZjk5YmM0YjRhYWI1MzNkYzE5Zjg4YTNmOGZiYjc5YiJ9&pname='+pname;var s = document.getElementsByTagName('script');s[0].parentNode.insertBefore(ga, s[0]);})();</script>	

<!-- vchat Code -->    <div id="login-form-popup" class="lightbox-content mfp-hide">
            


<div class="account-container lightbox-inner">

<div class="account-login-inner">

		<h3 class="uppercase">Đăng nhập</h3>

		<form method="post" class="login">

						
			<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
				<label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
			</p>
			
			<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
				<label for="password">Mật khẩu <span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
			</p>

			
			
			<p class="form-row">
				<input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/tin-tuc/" />				<input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
				<label for="rememberme" class="inline">
					<input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu				</label>
			</p>
			<p class="woocommerce-LostPassword lost_password">
				<a href="https://zito.vn/sofa-vang-cao-cap/lost-password/">Quên mật khẩu?</a>
			</p>

			
		</form>
</div><!-- .login-inner -->


</div><!-- .account-login-container -->

          </div>
  <link rel='stylesheet' id='mediaelement-css'  href='https://zito.vn/wp-includes/js/mediaelement/mediaelementplayer.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='https://zito.vn/wp-includes/js/mediaelement/wp-mediaelement.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='flatsome-effects-css'  href='https://zito.vn/wp-content/themes/flatsome/assets/css/effects.css' type='text/css' media='all' />
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/zito.vn\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"H\u00e3y x\u00e1c nh\u1eadn r\u1eb1ng b\u1ea1n kh\u00f4ng ph\u1ea3i l\u00e0 robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://zito.vn/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/tin-tuc\/?wc-ajax=%%endpoint%%","i18n_view_cart":"Xem gi\u1ecf h\u00e0ng","cart_url":"https:\/\/zito.vn\/gio-hang\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='//zito.vn/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js'></script>
<script type='text/javascript' src='//zito.vn/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script type='text/javascript' src='//zito.vn/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/tin-tuc\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='//zito.vn/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/tin-tuc\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
/* ]]> */
</script>
<script type='text/javascript' src='//zito.vn/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-content/plugins/yith-woocommerce-ajax-search-premium/assets/js/yith-autocomplete.min.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-content/themes/flatsome/inc/extensions/flatsome-live-search/flatsome-live-search.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-includes/js/hoverIntent.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var flatsomeVars = {"ajaxurl":"https:\/\/zito.vn\/wp-admin\/admin-ajax.php","rtl":"","sticky_height":"70"};
/* ]]> */
</script>
<script type='text/javascript' src='https://zito.vn/wp-content/themes/flatsome/assets/js/flatsome.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-content/themes/flatsome/inc/extensions/flatsome-lazy-load/flatsome-lazy-load.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-content/themes/flatsome/assets/js/woocommerce.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-content/themes/flatsome/assets/libs/packery.pkgd.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mejsL10n = {"language":"vi","strings":{"Close":"\u0110\u00f3ng","Fullscreen":"To\u00e0n m\u00e0n h\u00ecnh","Turn off Fullscreen":"T\u1eaft hi\u1ec3n th\u1ecb to\u00e0n m\u00e0n h\u00ecnh","Go Fullscreen":"Hi\u1ec3n th\u1ecb to\u00e0n m\u00e0n h\u00ecnh ","Download File":"T\u1ea3i v\u1ec1 t\u1eadp tin","Download Video":"T\u1ea3i video v\u1ec1","Play":"Ch\u1ea1y","Pause":"T\u1ea1m d\u1eebng","Captions\/Subtitles":"Ph\u1ee5 \u0111\u1ec1","None":"Kh\u00f4ng c\u00f3","Time Slider":"Th\u1eddi gian tr\u00ecnh chi\u1ebfu","Skip back %1 seconds":"L\u00f9i l\u1ea1i %1 gi\u00e2y","Video Player":"Tr\u00ecnh ch\u01a1i Video","Audio Player":"Tr\u00ecnh ch\u01a1i Audio","Volume Slider":"\u00c2m l\u01b0\u1ee3ng Tr\u00ecnh chi\u1ebfu","Mute Toggle":"B\u1eadt\/t\u1eaft ch\u1ebf \u0111\u1ed9 im l\u1eb7ng","Unmute":"B\u1eadt ti\u1ebfng","Mute":"T\u1eaft ti\u1ebfng","Use Up\/Down Arrow keys to increase or decrease volume.":"S\u1eed d\u1ee5ng c\u00e1c ph\u00edm m\u0169i t\u00ean L\u00ean\/Xu\u1ed1ng \u0111\u1ec3 t\u0103ng ho\u1eb7c gi\u1ea3m \u00e2m l\u01b0\u1ee3ng.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"S\u1eed d\u1ee5ng c\u00e1c ph\u00edm m\u0169i t\u00ean Tr\u00e1i\/Ph\u1ea3i \u0111\u1ec3 ti\u1ebfn m\u1ed9t gi\u00e2y, m\u0169i t\u00ean L\u00ean\/Xu\u1ed1ng \u0111\u1ec3 ti\u1ebfn m\u01b0\u1eddi gi\u00e2y."}};
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
/* ]]> */
</script>
<script type='text/javascript' src='https://zito.vn/wp-includes/js/mediaelement/mediaelement-and-player.min.js'></script>
<script type='text/javascript' src='https://zito.vn/wp-includes/js/mediaelement/wp-mediaelement.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _zxcvbnSettings = {"src":"https:\/\/zito.vn\/wp-includes\/js\/zxcvbn.min.js"};
/* ]]> */
</script>
<script type='text/javascript' src='https://zito.vn/wp-includes/js/zxcvbn-async.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pwsL10n = {"unknown":"M\u1eadt kh\u1ea9u m\u1ea1nh kh\u00f4ng x\u00e1c \u0111\u1ecbnh","short":"R\u1ea5t y\u1ebfu","bad":"Y\u1ebfu","good":"Trung b\u00ecnh","strong":"M\u1ea1nh","mismatch":"M\u1eadt kh\u1ea9u kh\u00f4ng kh\u1edbp"};
/* ]]> */
</script>
<script type='text/javascript' src='https://zito.vn/wp-admin/js/password-strength-meter.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_password_strength_meter_params = {"min_password_strength":"3","i18n_password_error":"Vui l\u00f2ng nh\u1eadp m\u1eadt kh\u1ea9u kh\u00f3 h\u01a1n.","i18n_password_hint":"G\u1ee3i \u00fd: M\u1eadt kh\u1ea9u ph\u1ea3i c\u00f3 \u00edt nh\u1ea5t 12 k\u00fd t\u1ef1. \u0110\u1ec3 n\u00e2ng cao \u0111\u1ed9 b\u1ea3o m\u1eadt, s\u1eed d\u1ee5ng ch\u1eef in hoa, in th\u01b0\u1eddng, ch\u1eef s\u1ed1 v\u00e0 c\u00e1c k\u00fd t\u1ef1 \u0111\u1eb7c bi\u1ec7t nh\u01b0 ! \" ? $ % ^ & )."};
/* ]]> */
</script>
<script type='text/javascript' src='//zito.vn/wp-content/plugins/woocommerce/assets/js/frontend/password-strength-meter.min.js'></script>

</body>
</html>

<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

Page Caching using disk: enhanced
Page cache debug info:
Engine:             disk: enhanced
Cache key:          zito.vn/tin-tuc/_index_ssl.html
Creation Time:      1517215376.000s
Header info:
X-Powered-By:        PHP/5.6.33
Set-Cookie:          wfvt_2078782424=5a6ede9005048; expires=Mon, 29-Jan-2018 09:12:56 GMT; Max-Age=1800; path=/; secure; httponly
Content-Type:        text/html; charset=UTF-8
Link:                <https://zito.vn/wp-json/>; rel="https://api.w.org/"

 Served from: zito.vn @ 2018-01-29 15:42:56 by W3 Total Cache -->