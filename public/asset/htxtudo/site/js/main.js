$(document).ready(function() {
    $("#zoom_03").ezPlus({
        gallery: 'gallery_01',
        cursor: 'pointer',
        galleryActiveClass: "active",
        imageCrossfade: true,
        zoomWindowWidth: 300,
        zoomWindowHeight: 350,
        zoomWindowOffsetX: 10,
    });
});