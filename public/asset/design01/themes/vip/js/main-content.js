jQuery(document).ready(function($) {

    var $scrollpane = $('[data-block="article-scrollpanel"]');
    var $scrollContent = $('[data-block="scroll-content"]');

    if ($scrollpane.length) {

        var $main_close_button = $('[data-main="close"]');
        var $main_block = $('[data-main="block"]');
        var $content_toggle = $('[data-bar="content-toggle"]');
        var $content_bar = $content_toggle.parent();
        var $primary_content = $('[data-main="primary"]');
        var isIOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

        var api = null;
        var isContentShowing = false;

        $main_close_button.on('touchstart click', function (e) {
            e.stopPropagation();

            if (!isContentShowing) {
                $isContentShowing = true;

                $main_block.addClass('-fadeOut');

                setTimeout(function() {
                    $isContentShowing = false;
                    $main_block.addClass('-hide');
                    $content_toggle.removeClass('-hide');
                    $scrollpane.attr('style');
                    $scrollpane.removeAttr('style');
                    $main_block.removeClass('-fadeIn')
                }, 350);
            }
                
        });

        $content_toggle.on('touchstart click', function (e) {
            e.stopPropagation();

            if (!isContentShowing && !$content_toggle.hasClass('-hide')) {
                $isContentShowing = true;
                $content_toggle.addClass('-hide');
                $main_block
                    .removeClass('-hide')
                    .addClass('-fadeIn');

                setTimeout(function() {
                    $isContentShowing = false;
                    $main_block.removeClass('-fadeOut')
                    remakeScrollpanel();
                }, 350);
            }

        });

        if (!isIOS) {
            
            remakeScrollpanel();

            function remakeScrollpanel () {
                var scrollpane_top = $scrollpane.offset().top;
                var scrollcontent_height = $scrollContent.outerHeight();
                var scrollpane_bottom = scrollpane_top + scrollcontent_height;
                var windowHeight = $(window).height();
                var element = null;
                var content_bar_top = $content_bar.offset().top;

                if (api) {
                    api.reinitialise();
                }

                if ( scrollpane_bottom > windowHeight ) {
                    $scrollpane.height(content_bar_top - scrollpane_top);
                    element = $scrollpane.jScrollPane({
                        verticalGutter: 0,
                        contentWidth: false
                    });

                    api = element.data('jsp');
                }
            }

            $(window).bind('resize', function () {
                $scrollpane.attr('style');
                $scrollpane.removeAttr('style');
                remakeScrollpanel();
            });

        } else {
            $main_block.addClass('-ios');
        }
    }
  
});