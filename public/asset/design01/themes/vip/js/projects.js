jQuery(document).ready(function($) {

    if ( isTouchDevice() ) {
        var $project_items = $('.project._items');
        var $project_links = $('.project._link');

        $project_links.on('click', function (e) {

            var $this = $(this);
            var $parent = $this.parent();

            if ( $parent.hasClass('-touched') ) {
                return true;
            } else {
                e.preventDefault();
                $parent
                    .addClass('-touched')
                    .siblings()
                    .removeClass('-touched');
            }
            
        });
    }

});