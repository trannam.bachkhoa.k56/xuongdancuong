jQuery(document).ready(function($) {
    var $scrollpane = $('[data-block="article-scrollpanel"]');
    var $news_list = $('[data-news="list"]');
    var $news_loader = $('[data-news="loader"]');

    var news_loading = 0;
    var news_paging = 1;
    var url = window.location.href;
    url += (url.indexOf('?') != -1 ? '&' : '?') + 'ajax=1&p='

    $scrollpane.bind(
		'jsp-scroll-y',
		function(event, scrollPositionY, isAtTop, isAtBottom)
		{
            if (isAtBottom == true && !news_loading && news_paging) {
                news_loading = 1;
                $news_loader.show();
                $.post(url + news_paging,function(d){
                    if (d) {
                        $news_list.append(d);
                        news_paging++;
                    }
                    else {
                        news_paging = 0;
                    }
                    $news_loader.hide();
                    news_loading = 0;
                });
            }
		}
	);
});