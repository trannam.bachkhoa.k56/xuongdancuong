! function(window, undefined) {
    function createOptions(options) {
        var object = optionsCache[options] = {};
        return jQuery.each(options.split(core_rspace), function(_, flag) { object[flag] = !0 }), object }

    function dataAttr(elem, key, data) {
        if (data === undefined && 1 === elem.nodeType) {
            var name = "data-" + key.replace(rmultiDash, "-$1").toLowerCase();
            if (data = elem.getAttribute(name), "string" == typeof data) {
                try { data = "true" === data ? !0 : "false" === data ? !1 : "null" === data ? null : +data + "" === data ? +data : rbrace.test(data) ? jQuery.parseJSON(data) : data } catch (e) {}
                jQuery.data(elem, key, data) } else data = undefined }
        return data }

    function isEmptyDataObject(obj) {
        var name;
        for (name in obj)
            if (("data" !== name || !jQuery.isEmptyObject(obj[name])) && "toJSON" !== name) return !1;
        return !0 }

    function returnFalse() {
        return !1 }

    function returnTrue() {
        return !0 }

    function isDisconnected(node) {
        return !node || !node.parentNode || 11 === node.parentNode.nodeType }

    function sibling(cur, dir) { do cur = cur[dir]; while (cur && 1 !== cur.nodeType);
        return cur }

    function winnow(elements, qualifier, keep) {
        if (qualifier = qualifier || 0, jQuery.isFunction(qualifier)) return jQuery.grep(elements, function(elem, i) {
            var retVal = !!qualifier.call(elem, i, elem);
            return retVal === keep });
        if (qualifier.nodeType) return jQuery.grep(elements, function(elem, i) {
            return elem === qualifier === keep });
        if ("string" == typeof qualifier) {
            var filtered = jQuery.grep(elements, function(elem) {
                return 1 === elem.nodeType });
            if (isSimple.test(qualifier)) return jQuery.filter(qualifier, filtered, !keep);
            qualifier = jQuery.filter(qualifier, filtered) }
        return jQuery.grep(elements, function(elem, i) {
            return jQuery.inArray(elem, qualifier) >= 0 === keep }) }

    function createSafeFragment(document) {
        var list = nodeNames.split("|"),
            safeFrag = document.createDocumentFragment();
        if (safeFrag.createElement)
            for (; list.length;) safeFrag.createElement(list.pop());
        return safeFrag }

    function findOrAppend(elem, tag) {
        return elem.getElementsByTagName(tag)[0] || elem.appendChild(elem.ownerDocument.createElement(tag)) }

    function cloneCopyEvent(src, dest) {
        if (1 === dest.nodeType && jQuery.hasData(src)) {
            var type, i, l, oldData = jQuery._data(src),
                curData = jQuery._data(dest, oldData),
                events = oldData.events;
            if (events) { delete curData.handle, curData.events = {};
                for (type in events)
                    for (i = 0, l = events[type].length; l > i; i++) jQuery.event.add(dest, type, events[type][i]) }
            curData.data && (curData.data = jQuery.extend({}, curData.data)) } }

    function cloneFixAttributes(src, dest) {
        var nodeName;
        1 === dest.nodeType && (dest.clearAttributes && dest.clearAttributes(), dest.mergeAttributes && dest.mergeAttributes(src), nodeName = dest.nodeName.toLowerCase(), "object" === nodeName ? (dest.parentNode && (dest.outerHTML = src.outerHTML), jQuery.support.html5Clone && src.innerHTML && !jQuery.trim(dest.innerHTML) && (dest.innerHTML = src.innerHTML)) : "input" === nodeName && rcheckableType.test(src.type) ? (dest.defaultChecked = dest.checked = src.checked, dest.value !== src.value && (dest.value = src.value)) : "option" === nodeName ? dest.selected = src.defaultSelected : "input" === nodeName || "textarea" === nodeName ? dest.defaultValue = src.defaultValue : "script" === nodeName && dest.text !== src.text && (dest.text = src.text), dest.removeAttribute(jQuery.expando)) }

    function getAll(elem) {
        return "undefined" != typeof elem.getElementsByTagName ? elem.getElementsByTagName("*") : "undefined" != typeof elem.querySelectorAll ? elem.querySelectorAll("*") : [] }

    function fixDefaultChecked(elem) { rcheckableType.test(elem.type) && (elem.defaultChecked = elem.checked) }

    function vendorPropName(style, name) {
        if (name in style) return name;
        for (var capName = name.charAt(0).toUpperCase() + name.slice(1), origName = name, i = cssPrefixes.length; i--;)
            if (name = cssPrefixes[i] + capName, name in style) return name;
        return origName }

    function isHidden(elem, el) {
        return elem = el || elem, "none" === jQuery.css(elem, "display") || !jQuery.contains(elem.ownerDocument, elem) }

    function showHide(elements, show) {
        for (var elem, display, values = [], index = 0, length = elements.length; length > index; index++) elem = elements[index], elem.style && (values[index] = jQuery._data(elem, "olddisplay"), show ? (values[index] || "none" !== elem.style.display || (elem.style.display = ""), "" === elem.style.display && isHidden(elem) && (values[index] = jQuery._data(elem, "olddisplay", css_defaultDisplay(elem.nodeName)))) : (display = curCSS(elem, "display"), values[index] || "none" === display || jQuery._data(elem, "olddisplay", display)));
        for (index = 0; length > index; index++) elem = elements[index], elem.style && (show && "none" !== elem.style.display && "" !== elem.style.display || (elem.style.display = show ? values[index] || "" : "none"));
        return elements }

    function setPositiveNumber(elem, value, subtract) {
        var matches = rnumsplit.exec(value);
        return matches ? Math.max(0, matches[1] - (subtract || 0)) + (matches[2] || "px") : value }

    function augmentWidthOrHeight(elem, name, extra, isBorderBox) {
        for (var i = extra === (isBorderBox ? "border" : "content") ? 4 : "width" === name ? 1 : 0, val = 0; 4 > i; i += 2) "margin" === extra && (val += jQuery.css(elem, extra + cssExpand[i], !0)), isBorderBox ? ("content" === extra && (val -= parseFloat(curCSS(elem, "padding" + cssExpand[i])) || 0), "margin" !== extra && (val -= parseFloat(curCSS(elem, "border" + cssExpand[i] + "Width")) || 0)) : (val += parseFloat(curCSS(elem, "padding" + cssExpand[i])) || 0, "padding" !== extra && (val += parseFloat(curCSS(elem, "border" + cssExpand[i] + "Width")) || 0));
        return val }

    function getWidthOrHeight(elem, name, extra) {
        var val = "width" === name ? elem.offsetWidth : elem.offsetHeight,
            valueIsBorderBox = !0,
            isBorderBox = jQuery.support.boxSizing && "border-box" === jQuery.css(elem, "boxSizing");
        if (0 >= val) {
            if (val = curCSS(elem, name), (0 > val || null == val) && (val = elem.style[name]), rnumnonpx.test(val)) return val;
            valueIsBorderBox = isBorderBox && (jQuery.support.boxSizingReliable || val === elem.style[name]), val = parseFloat(val) || 0 }
        return val + augmentWidthOrHeight(elem, name, extra || (isBorderBox ? "border" : "content"), valueIsBorderBox) + "px" }

    function css_defaultDisplay(nodeName) {
        if (elemdisplay[nodeName]) return elemdisplay[nodeName];
        var elem = jQuery("<" + nodeName + ">").appendTo(document.body),
            display = elem.css("display");
        return elem.remove(), ("none" === display || "" === display) && (iframe = document.body.appendChild(iframe || jQuery.extend(document.createElement("iframe"), { frameBorder: 0, width: 0, height: 0 })), iframeDoc && iframe.createElement || (iframeDoc = (iframe.contentWindow || iframe.contentDocument).document, iframeDoc.write("<!doctype html><html><body>"), iframeDoc.close()), elem = iframeDoc.body.appendChild(iframeDoc.createElement(nodeName)), display = curCSS(elem, "display"), document.body.removeChild(iframe)), elemdisplay[nodeName] = display, display }

    function buildParams(prefix, obj, traditional, add) {
        var name;
        if (jQuery.isArray(obj)) jQuery.each(obj, function(i, v) { traditional || rbracket.test(prefix) ? add(prefix, v) : buildParams(prefix + "[" + ("object" == typeof v ? i : "") + "]", v, traditional, add) });
        else if (traditional || "object" !== jQuery.type(obj)) add(prefix, obj);
        else
            for (name in obj) buildParams(prefix + "[" + name + "]", obj[name], traditional, add) }

    function addToPrefiltersOrTransports(structure) {
        return function(dataTypeExpression, func) { "string" != typeof dataTypeExpression && (func = dataTypeExpression, dataTypeExpression = "*");
            var dataType, list, placeBefore, dataTypes = dataTypeExpression.toLowerCase().split(core_rspace),
                i = 0,
                length = dataTypes.length;
            if (jQuery.isFunction(func))
                for (; length > i; i++) dataType = dataTypes[i], placeBefore = /^\+/.test(dataType), placeBefore && (dataType = dataType.substr(1) || "*"), list = structure[dataType] = structure[dataType] || [], list[placeBefore ? "unshift" : "push"](func) } }

    function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR, dataType, inspected) { dataType = dataType || options.dataTypes[0], inspected = inspected || {}, inspected[dataType] = !0;
        for (var selection, list = structure[dataType], i = 0, length = list ? list.length : 0, executeOnly = structure === prefilters; length > i && (executeOnly || !selection); i++) selection = list[i](options, originalOptions, jqXHR), "string" == typeof selection && (!executeOnly || inspected[selection] ? selection = undefined : (options.dataTypes.unshift(selection), selection = inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR, selection, inspected)));
        return !executeOnly && selection || inspected["*"] || (selection = inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR, "*", inspected)), selection }

    function ajaxExtend(target, src) {
        var key, deep, flatOptions = jQuery.ajaxSettings.flatOptions || {};
        for (key in src) src[key] !== undefined && ((flatOptions[key] ? target : deep || (deep = {}))[key] = src[key]);
        deep && jQuery.extend(!0, target, deep) }

    function ajaxHandleResponses(s, jqXHR, responses) {
        var ct, type, finalDataType, firstDataType, contents = s.contents,
            dataTypes = s.dataTypes,
            responseFields = s.responseFields;
        for (type in responseFields) type in responses && (jqXHR[responseFields[type]] = responses[type]);
        for (;
            "*" === dataTypes[0];) dataTypes.shift(), ct === undefined && (ct = s.mimeType || jqXHR.getResponseHeader("content-type"));
        if (ct)
            for (type in contents)
                if (contents[type] && contents[type].test(ct)) { dataTypes.unshift(type);
                    break }
        if (dataTypes[0] in responses) finalDataType = dataTypes[0];
        else {
            for (type in responses) {
                if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) { finalDataType = type;
                    break }
                firstDataType || (firstDataType = type) }
            finalDataType = finalDataType || firstDataType }
        return finalDataType ? (finalDataType !== dataTypes[0] && dataTypes.unshift(finalDataType), responses[finalDataType]) : void 0 }

    function ajaxConvert(s, response) {
        var conv, conv2, current, tmp, dataTypes = s.dataTypes.slice(),
            prev = dataTypes[0],
            converters = {},
            i = 0;
        if (s.dataFilter && (response = s.dataFilter(response, s.dataType)), dataTypes[1])
            for (conv in s.converters) converters[conv.toLowerCase()] = s.converters[conv];
        for (; current = dataTypes[++i];)
            if ("*" !== current) {
                if ("*" !== prev && prev !== current) {
                    if (conv = converters[prev + " " + current] || converters["* " + current], !conv)
                        for (conv2 in converters)
                            if (tmp = conv2.split(" "), tmp[1] === current && (conv = converters[prev + " " + tmp[0]] || converters["* " + tmp[0]])) { conv === !0 ? conv = converters[conv2] : converters[conv2] !== !0 && (current = tmp[0], dataTypes.splice(i--, 0, current));
                                break }
                    if (conv !== !0)
                        if (conv && s["throws"]) response = conv(response);
                        else try { response = conv(response) } catch (e) {
                            return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current } } }
                prev = current }
        return { state: "success", data: response } }

    function createStandardXHR() {
        try {
            return new window.XMLHttpRequest } catch (e) {} }

    function createActiveXHR() {
        try {
            return new window.ActiveXObject("Microsoft.XMLHTTP") } catch (e) {} }

    function createFxNow() {
        return setTimeout(function() { fxNow = undefined }, 0), fxNow = jQuery.now() }

    function createTweens(animation, props) { jQuery.each(props, function(prop, value) {
            for (var collection = (tweeners[prop] || []).concat(tweeners["*"]), index = 0, length = collection.length; length > index; index++)
                if (collection[index].call(animation, prop, value)) return }) }

    function Animation(elem, properties, options) {
        var result, index = 0,
            length = animationPrefilters.length,
            deferred = jQuery.Deferred().always(function() { delete tick.elem }),
            tick = function() {
                for (var currentTime = fxNow || createFxNow(), remaining = Math.max(0, animation.startTime + animation.duration - currentTime), percent = 1 - (remaining / animation.duration || 0), index = 0, length = animation.tweens.length; length > index; index++) animation.tweens[index].run(percent);
                return deferred.notifyWith(elem, [animation, percent, remaining]), 1 > percent && length ? remaining : (deferred.resolveWith(elem, [animation]), !1) },
            animation = deferred.promise({ elem: elem, props: jQuery.extend({}, properties), opts: jQuery.extend(!0, { specialEasing: {} }, options), originalProperties: properties, originalOptions: options, startTime: fxNow || createFxNow(), duration: options.duration, tweens: [], createTween: function(prop, end, easing) {
                    var tween = jQuery.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
                    return animation.tweens.push(tween), tween }, stop: function(gotoEnd) {
                    for (var index = 0, length = gotoEnd ? animation.tweens.length : 0; length > index; index++) animation.tweens[index].run(1);
                    return gotoEnd ? deferred.resolveWith(elem, [animation, gotoEnd]) : deferred.rejectWith(elem, [animation, gotoEnd]), this } }),
            props = animation.props;
        for (propFilter(props, animation.opts.specialEasing); length > index; index++)
            if (result = animationPrefilters[index].call(animation, elem, props, animation.opts)) return result;
        return createTweens(animation, props), jQuery.isFunction(animation.opts.start) && animation.opts.start.call(elem, animation), jQuery.fx.timer(jQuery.extend(tick, { anim: animation, queue: animation.opts.queue, elem: elem })), animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always) }

    function propFilter(props, specialEasing) {
        var index, name, easing, value, hooks;
        for (index in props)
            if (name = jQuery.camelCase(index), easing = specialEasing[name], value = props[index], jQuery.isArray(value) && (easing = value[1], value = props[index] = value[0]), index !== name && (props[name] = value, delete props[index]), hooks = jQuery.cssHooks[name], hooks && "expand" in hooks) { value = hooks.expand(value), delete props[name];
                for (index in value) index in props || (props[index] = value[index], specialEasing[index] = easing) } else specialEasing[name] = easing }

    function defaultPrefilter(elem, props, opts) {
        var index, prop, value, length, dataShow, tween, hooks, oldfire, anim = this,
            style = elem.style,
            orig = {},
            handled = [],
            hidden = elem.nodeType && isHidden(elem);
        opts.queue || (hooks = jQuery._queueHooks(elem, "fx"), null == hooks.unqueued && (hooks.unqueued = 0, oldfire = hooks.empty.fire, hooks.empty.fire = function() { hooks.unqueued || oldfire() }), hooks.unqueued++, anim.always(function() { anim.always(function() { hooks.unqueued--, jQuery.queue(elem, "fx").length || hooks.empty.fire() }) })), 1 === elem.nodeType && ("height" in props || "width" in props) && (opts.overflow = [style.overflow, style.overflowX, style.overflowY], "inline" === jQuery.css(elem, "display") && "none" === jQuery.css(elem, "float") && (jQuery.support.inlineBlockNeedsLayout && "inline" !== css_defaultDisplay(elem.nodeName) ? style.zoom = 1 : style.display = "inline-block")), opts.overflow && (style.overflow = "hidden", jQuery.support.shrinkWrapBlocks || anim.done(function() { style.overflow = opts.overflow[0], style.overflowX = opts.overflow[1], style.overflowY = opts.overflow[2] }));
        for (index in props)
            if (value = props[index], rfxtypes.exec(value)) {
                if (delete props[index], value === (hidden ? "hide" : "show")) continue;
                handled.push(index) }
        if (length = handled.length)
            for (dataShow = jQuery._data(elem, "fxshow") || jQuery._data(elem, "fxshow", {}), hidden ? jQuery(elem).show() : anim.done(function() { jQuery(elem).hide() }), anim.done(function() {
                    var prop;
                    jQuery.removeData(elem, "fxshow", !0);
                    for (prop in orig) jQuery.style(elem, prop, orig[prop]) }), index = 0; length > index; index++) prop = handled[index], tween = anim.createTween(prop, hidden ? dataShow[prop] : 0), orig[prop] = dataShow[prop] || jQuery.style(elem, prop), prop in dataShow || (dataShow[prop] = tween.start, hidden && (tween.end = tween.start, tween.start = "width" === prop || "height" === prop ? 1 : 0)) }

    function Tween(elem, options, prop, end, easing) {
        return new Tween.prototype.init(elem, options, prop, end, easing) }

    function genFx(type, includeWidth) {
        for (var which, attrs = { height: type }, i = 0; 4 > i; i += 2 - includeWidth) which = cssExpand[i], attrs["margin" + which] = attrs["padding" + which] = type;
        return includeWidth && (attrs.opacity = attrs.width = type), attrs }

    function getWindow(elem) {
        return jQuery.isWindow(elem) ? elem : 9 === elem.nodeType ? elem.defaultView || elem.parentWindow : !1 }
    var rootjQuery, readyList, document = window.document,
        location = window.location,
        navigator = window.navigator,
        _jQuery = window.jQuery,
        _$ = window.$,
        core_push = Array.prototype.push,
        core_slice = Array.prototype.slice,
        core_indexOf = Array.prototype.indexOf,
        core_toString = Object.prototype.toString,
        core_hasOwn = Object.prototype.hasOwnProperty,
        core_trim = String.prototype.trim,
        jQuery = function(selector, context) {
            return new jQuery.fn.init(selector, context, rootjQuery) },
        core_pnum = /[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source,
        core_rnotwhite = /\S/,
        core_rspace = /\s+/,
        rtrim = core_rnotwhite.test(" ") ? /^[\s\xA0]+|[\s\xA0]+$/g : /^\s+|\s+$/g,
        rquickExpr = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
        rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        rvalidchars = /^[\],:{}\s]*$/,
        rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g,
        rvalidescape = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        rvalidtokens = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,
        rmsPrefix = /^-ms-/,
        rdashAlpha = /-([\da-z])/gi,
        fcamelCase = function(all, letter) {
            return (letter + "").toUpperCase() },
        DOMContentLoaded = function() { document.addEventListener ? (document.removeEventListener("DOMContentLoaded", DOMContentLoaded, !1), jQuery.ready()) : "complete" === document.readyState && (document.detachEvent("onreadystatechange", DOMContentLoaded), jQuery.ready()) },
        class2type = {};
    jQuery.fn = jQuery.prototype = { constructor: jQuery, init: function(selector, context, rootjQuery) {
            var match, elem, doc;
            if (!selector) return this;
            if (selector.nodeType) return this.context = this[0] = selector, this.length = 1, this;
            if ("string" == typeof selector) {
                if (match = "<" === selector.charAt(0) && ">" === selector.charAt(selector.length - 1) && selector.length >= 3 ? [null, selector, null] : rquickExpr.exec(selector), !match || !match[1] && context) return !context || context.jquery ? (context || rootjQuery).find(selector) : this.constructor(context).find(selector);
                if (match[1]) return context = context instanceof jQuery ? context[0] : context, doc = context && context.nodeType ? context.ownerDocument || context : document, selector = jQuery.parseHTML(match[1], doc, !0), rsingleTag.test(match[1]) && jQuery.isPlainObject(context) && this.attr.call(selector, context, !0), jQuery.merge(this, selector);
                if (elem = document.getElementById(match[2]), elem && elem.parentNode) {
                    if (elem.id !== match[2]) return rootjQuery.find(selector);
                    this.length = 1, this[0] = elem }
                return this.context = document, this.selector = selector, this }
            return jQuery.isFunction(selector) ? rootjQuery.ready(selector) : (selector.selector !== undefined && (this.selector = selector.selector, this.context = selector.context), jQuery.makeArray(selector, this)) }, selector: "", jquery: "1.8.0", length: 0, size: function() {
            return this.length }, toArray: function() {
            return core_slice.call(this) }, get: function(num) {
            return null == num ? this.toArray() : 0 > num ? this[this.length + num] : this[num] }, pushStack: function(elems, name, selector) {
            var ret = jQuery.merge(this.constructor(), elems);
            return ret.prevObject = this, ret.context = this.context, "find" === name ? ret.selector = this.selector + (this.selector ? " " : "") + selector : name && (ret.selector = this.selector + "." + name + "(" + selector + ")"), ret }, each: function(callback, args) {
            return jQuery.each(this, callback, args) }, ready: function(fn) {
            return jQuery.ready.promise().done(fn), this }, eq: function(i) {
            return i = +i, -1 === i ? this.slice(i) : this.slice(i, i + 1) }, first: function() {
            return this.eq(0) }, last: function() {
            return this.eq(-1) }, slice: function() {
            return this.pushStack(core_slice.apply(this, arguments), "slice", core_slice.call(arguments).join(",")) }, map: function(callback) {
            return this.pushStack(jQuery.map(this, function(elem, i) {
                return callback.call(elem, i, elem) })) }, end: function() {
            return this.prevObject || this.constructor(null) }, push: core_push, sort: [].sort, splice: [].splice }, jQuery.fn.init.prototype = jQuery.fn, jQuery.extend = jQuery.fn.extend = function() {
        var options, name, src, copy, copyIsArray, clone, target = arguments[0] || {},
            i = 1,
            length = arguments.length,
            deep = !1;
        for ("boolean" == typeof target && (deep = target, target = arguments[1] || {}, i = 2), "object" == typeof target || jQuery.isFunction(target) || (target = {}), length === i && (target = this, --i); length > i; i++)
            if (null != (options = arguments[i]))
                for (name in options) src = target[name], copy = options[name], target !== copy && (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy))) ? (copyIsArray ? (copyIsArray = !1, clone = src && jQuery.isArray(src) ? src : []) : clone = src && jQuery.isPlainObject(src) ? src : {}, target[name] = jQuery.extend(deep, clone, copy)) : copy !== undefined && (target[name] = copy));
        return target }, jQuery.extend({ noConflict: function(deep) {
            return window.$ === jQuery && (window.$ = _$), deep && window.jQuery === jQuery && (window.jQuery = _jQuery), jQuery }, isReady: !1, readyWait: 1, holdReady: function(hold) { hold ? jQuery.readyWait++ : jQuery.ready(!0) }, ready: function(wait) {
            if (wait === !0 ? !--jQuery.readyWait : !jQuery.isReady) {
                if (!document.body) return setTimeout(jQuery.ready, 1);
                jQuery.isReady = !0, wait !== !0 && --jQuery.readyWait > 0 || (readyList.resolveWith(document, [jQuery]), jQuery.fn.trigger && jQuery(document).trigger("ready").off("ready")) } }, isFunction: function(obj) {
            return "function" === jQuery.type(obj) }, isArray: Array.isArray || function(obj) {
            return "array" === jQuery.type(obj) }, isWindow: function(obj) {
            return null != obj && obj == obj.window }, isNumeric: function(obj) {
            return !isNaN(parseFloat(obj)) && isFinite(obj) }, type: function(obj) {
            return null == obj ? String(obj) : class2type[core_toString.call(obj)] || "object" }, isPlainObject: function(obj) {
            if (!obj || "object" !== jQuery.type(obj) || obj.nodeType || jQuery.isWindow(obj)) return !1;
            try {
                if (obj.constructor && !core_hasOwn.call(obj, "constructor") && !core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) return !1 } catch (e) {
                return !1 }
            var key;
            for (key in obj);
            return key === undefined || core_hasOwn.call(obj, key) }, isEmptyObject: function(obj) {
            var name;
            for (name in obj) return !1;
            return !0 }, error: function(msg) {
            throw new Error(msg) }, parseHTML: function(data, context, scripts) {
            var parsed;
            return data && "string" == typeof data ? ("boolean" == typeof context && (scripts = context, context = 0), context = context || document, (parsed = rsingleTag.exec(data)) ? [context.createElement(parsed[1])] : (parsed = jQuery.buildFragment([data], context, scripts ? null : []), jQuery.merge([], (parsed.cacheable ? jQuery.clone(parsed.fragment) : parsed.fragment).childNodes))) : null }, parseJSON: function(data) {
            return data && "string" == typeof data ? (data = jQuery.trim(data), window.JSON && window.JSON.parse ? window.JSON.parse(data) : rvalidchars.test(data.replace(rvalidescape, "@").replace(rvalidtokens, "]").replace(rvalidbraces, "")) ? new Function("return " + data)() : void jQuery.error("Invalid JSON: " + data)) : null }, parseXML: function(data) {
            var xml, tmp;
            if (!data || "string" != typeof data) return null;
            try { window.DOMParser ? (tmp = new DOMParser, xml = tmp.parseFromString(data, "text/xml")) : (xml = new ActiveXObject("Microsoft.XMLDOM"), xml.async = "false", xml.loadXML(data)) } catch (e) { xml = undefined }
            return xml && xml.documentElement && !xml.getElementsByTagName("parsererror").length || jQuery.error("Invalid XML: " + data), xml }, noop: function() {}, globalEval: function(data) { data && core_rnotwhite.test(data) && (window.execScript || function(data) { window.eval.call(window, data) })(data) }, camelCase: function(string) {
            return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase) }, nodeName: function(elem, name) {
            return elem.nodeName && elem.nodeName.toUpperCase() === name.toUpperCase() }, each: function(obj, callback, args) {
            var name, i = 0,
                length = obj.length,
                isObj = length === undefined || jQuery.isFunction(obj);
            if (args)
                if (isObj) {
                    for (name in obj)
                        if (callback.apply(obj[name], args) === !1) break } else
                    for (; length > i && callback.apply(obj[i++], args) !== !1;);
            else if (isObj) {
                for (name in obj)
                    if (callback.call(obj[name], name, obj[name]) === !1) break } else
                for (; length > i && callback.call(obj[i], i, obj[i++]) !== !1;);
            return obj }, trim: core_trim ? function(text) {
            return null == text ? "" : core_trim.call(text) } : function(text) {
            return null == text ? "" : text.toString().replace(rtrim, "") }, makeArray: function(arr, results) {
            var type, ret = results || [];
            return null != arr && (type = jQuery.type(arr), null == arr.length || "string" === type || "function" === type || "regexp" === type || jQuery.isWindow(arr) ? core_push.call(ret, arr) : jQuery.merge(ret, arr)), ret }, inArray: function(elem, arr, i) {
            var len;
            if (arr) {
                if (core_indexOf) return core_indexOf.call(arr, elem, i);
                for (len = arr.length, i = i ? 0 > i ? Math.max(0, len + i) : i : 0; len > i; i++)
                    if (i in arr && arr[i] === elem) return i }
            return -1 }, merge: function(first, second) {
            var l = second.length,
                i = first.length,
                j = 0;
            if ("number" == typeof l)
                for (; l > j; j++) first[i++] = second[j];
            else
                for (; second[j] !== undefined;) first[i++] = second[j++];
            return first.length = i, first }, grep: function(elems, callback, inv) {
            var retVal, ret = [],
                i = 0,
                length = elems.length;
            for (inv = !!inv; length > i; i++) retVal = !!callback(elems[i], i), inv !== retVal && ret.push(elems[i]);
            return ret }, map: function(elems, callback, arg) {
            var value, key, ret = [],
                i = 0,
                length = elems.length,
                isArray = elems instanceof jQuery || length !== undefined && "number" == typeof length && (length > 0 && elems[0] && elems[length - 1] || 0 === length || jQuery.isArray(elems));
            if (isArray)
                for (; length > i; i++) value = callback(elems[i], i, arg), null != value && (ret[ret.length] = value);
            else
                for (key in elems) value = callback(elems[key], key, arg), null != value && (ret[ret.length] = value);
            return ret.concat.apply([], ret) }, guid: 1, proxy: function(fn, context) {
            var tmp, args, proxy;
            return "string" == typeof context && (tmp = fn[context], context = fn, fn = tmp), jQuery.isFunction(fn) ? (args = core_slice.call(arguments, 2), proxy = function() {
                return fn.apply(context, args.concat(core_slice.call(arguments))) }, proxy.guid = fn.guid = fn.guid || proxy.guid || jQuery.guid++, proxy) : undefined }, access: function(elems, fn, key, value, chainable, emptyGet, pass) {
            var exec, bulk = null == key,
                i = 0,
                length = elems.length;
            if (key && "object" == typeof key) {
                for (i in key) jQuery.access(elems, fn, i, key[i], 1, emptyGet, value);
                chainable = 1 } else if (value !== undefined) {
                if (exec = pass === undefined && jQuery.isFunction(value), bulk && (exec ? (exec = fn, fn = function(elem, key, value) {
                        return exec.call(jQuery(elem), value) }) : (fn.call(elems, value), fn = null)), fn)
                    for (; length > i; i++) fn(elems[i], key, exec ? value.call(elems[i], i, fn(elems[i], key)) : value, pass);
                chainable = 1 }
            return chainable ? elems : bulk ? fn.call(elems) : length ? fn(elems[0], key) : emptyGet }, now: function() {
            return (new Date).getTime() } }), jQuery.ready.promise = function(obj) {
        if (!readyList)
            if (readyList = jQuery.Deferred(), "complete" === document.readyState || "loading" !== document.readyState && document.addEventListener) setTimeout(jQuery.ready, 1);
            else if (document.addEventListener) document.addEventListener("DOMContentLoaded", DOMContentLoaded, !1), window.addEventListener("load", jQuery.ready, !1);
        else { document.attachEvent("onreadystatechange", DOMContentLoaded), window.attachEvent("onload", jQuery.ready);
            var top = !1;
            try { top = null == window.frameElement && document.documentElement } catch (e) {}
            top && top.doScroll && ! function doScrollCheck() {
                if (!jQuery.isReady) {
                    try { top.doScroll("left") } catch (e) {
                        return setTimeout(doScrollCheck, 50) }
                    jQuery.ready() } }() }
        return readyList.promise(obj) }, jQuery.each("Boolean Number String Function Array Date RegExp Object".split(" "), function(i, name) { class2type["[object " + name + "]"] = name.toLowerCase() }), rootjQuery = jQuery(document);
    var optionsCache = {};
    jQuery.Callbacks = function(options) { options = "string" == typeof options ? optionsCache[options] || createOptions(options) : jQuery.extend({}, options);
        var memory, fired, firing, firingStart, firingLength, firingIndex, list = [],
            stack = !options.once && [],
            fire = function(data) {
                for (memory = options.memory && data, fired = !0, firingIndex = firingStart || 0, firingStart = 0, firingLength = list.length, firing = !0; list && firingLength > firingIndex; firingIndex++)
                    if (list[firingIndex].apply(data[0], data[1]) === !1 && options.stopOnFalse) { memory = !1;
                        break }
                firing = !1, list && (stack ? stack.length && fire(stack.shift()) : memory ? list = [] : self.disable()) },
            self = { add: function() {
                    if (list) {
                        var start = list.length;! function add(args) { jQuery.each(args, function(_, arg) {!jQuery.isFunction(arg) || options.unique && self.has(arg) ? arg && arg.length && add(arg) : list.push(arg) }) }(arguments), firing ? firingLength = list.length : memory && (firingStart = start, fire(memory)) }
                    return this }, remove: function() {
                    return list && jQuery.each(arguments, function(_, arg) {
                        for (var index;
                            (index = jQuery.inArray(arg, list, index)) > -1;) list.splice(index, 1), firing && (firingLength >= index && firingLength--, firingIndex >= index && firingIndex--) }), this }, has: function(fn) {
                    return jQuery.inArray(fn, list) > -1 }, empty: function() {
                    return list = [], this }, disable: function() {
                    return list = stack = memory = undefined, this }, disabled: function() {
                    return !list }, lock: function() {
                    return stack = undefined, memory || self.disable(), this }, locked: function() {
                    return !stack }, fireWith: function(context, args) {
                    return args = args || [], args = [context, args.slice ? args.slice() : args], !list || fired && !stack || (firing ? stack.push(args) : fire(args)), this }, fire: function() {
                    return self.fireWith(this, arguments), this }, fired: function() {
                    return !!fired } };
        return self }, jQuery.extend({ Deferred: function(func) {
            var tuples = [
                    ["resolve", "done", jQuery.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", jQuery.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", jQuery.Callbacks("memory")]
                ],
                state = "pending",
                promise = { state: function() {
                        return state }, always: function() {
                        return deferred.done(arguments).fail(arguments), this }, then: function() {
                        var fns = arguments;
                        return jQuery.Deferred(function(newDefer) { jQuery.each(tuples, function(i, tuple) {
                                var action = tuple[0],
                                    fn = fns[i];
                                deferred[tuple[1]](jQuery.isFunction(fn) ? function() {
                                    var returned = fn.apply(this, arguments);
                                    returned && jQuery.isFunction(returned.promise) ? returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify) : newDefer[action + "With"](this === deferred ? newDefer : this, [returned]) } : newDefer[action]) }), fns = null }).promise() }, promise: function(obj) {
                        return "object" == typeof obj ? jQuery.extend(obj, promise) : promise } },
                deferred = {};
            return promise.pipe = promise.then, jQuery.each(tuples, function(i, tuple) {
                var list = tuple[2],
                    stateString = tuple[3];
                promise[tuple[1]] = list.add, stateString && list.add(function() { state = stateString }, tuples[1 ^ i][2].disable, tuples[2][2].lock), deferred[tuple[0]] = list.fire, deferred[tuple[0] + "With"] = list.fireWith }), promise.promise(deferred), func && func.call(deferred, deferred), deferred }, when: function(subordinate) {
            var progressValues, progressContexts, resolveContexts, i = 0,
                resolveValues = core_slice.call(arguments),
                length = resolveValues.length,
                remaining = 1 !== length || subordinate && jQuery.isFunction(subordinate.promise) ? length : 0,
                deferred = 1 === remaining ? subordinate : jQuery.Deferred(),
                updateFunc = function(i, contexts, values) {
                    return function(value) { contexts[i] = this, values[i] = arguments.length > 1 ? core_slice.call(arguments) : value, values === progressValues ? deferred.notifyWith(contexts, values) : --remaining || deferred.resolveWith(contexts, values) } };
            if (length > 1)
                for (progressValues = new Array(length), progressContexts = new Array(length), resolveContexts = new Array(length); length > i; i++) resolveValues[i] && jQuery.isFunction(resolveValues[i].promise) ? resolveValues[i].promise().done(updateFunc(i, resolveContexts, resolveValues)).fail(deferred.reject).progress(updateFunc(i, progressContexts, progressValues)) : --remaining;
            return remaining || deferred.resolveWith(resolveContexts, resolveValues), deferred.promise() } }), jQuery.support = function() {
        var support, all, a, select, opt, input, fragment, eventName, i, isSupported, clickFn, div = document.createElement("div");
        if (div.setAttribute("className", "t"), div.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", all = div.getElementsByTagName("*"), a = div.getElementsByTagName("a")[0], a.style.cssText = "top:1px;float:left;opacity:.5", !all || !all.length || !a) return {};
        select = document.createElement("select"), opt = select.appendChild(document.createElement("option")), input = div.getElementsByTagName("input")[0], support = { leadingWhitespace: 3 === div.firstChild.nodeType, tbody: !div.getElementsByTagName("tbody").length, htmlSerialize: !!div.getElementsByTagName("link").length, style: /top/.test(a.getAttribute("style")), hrefNormalized: "/a" === a.getAttribute("href"), opacity: /^0.5/.test(a.style.opacity), cssFloat: !!a.style.cssFloat, checkOn: "on" === input.value, optSelected: opt.selected, getSetAttribute: "t" !== div.className, enctype: !!document.createElement("form").enctype, html5Clone: "<:nav></:nav>" !== document.createElement("nav").cloneNode(!0).outerHTML, boxModel: "CSS1Compat" === document.compatMode, submitBubbles: !0, changeBubbles: !0, focusinBubbles: !1, deleteExpando: !0, noCloneEvent: !0, inlineBlockNeedsLayout: !1, shrinkWrapBlocks: !1, reliableMarginRight: !0, boxSizingReliable: !0, pixelPosition: !1 }, input.checked = !0, support.noCloneChecked = input.cloneNode(!0).checked, select.disabled = !0, support.optDisabled = !opt.disabled;
        try { delete div.test } catch (e) { support.deleteExpando = !1 }
        if (!div.addEventListener && div.attachEvent && div.fireEvent && (div.attachEvent("onclick", clickFn = function() { support.noCloneEvent = !1 }), div.cloneNode(!0).fireEvent("onclick"), div.detachEvent("onclick", clickFn)), input = document.createElement("input"), input.value = "t", input.setAttribute("type", "radio"), support.radioValue = "t" === input.value, input.setAttribute("checked", "checked"), input.setAttribute("name", "t"), div.appendChild(input), fragment = document.createDocumentFragment(), fragment.appendChild(div.lastChild), support.checkClone = fragment.cloneNode(!0).cloneNode(!0).lastChild.checked, support.appendChecked = input.checked, fragment.removeChild(input), fragment.appendChild(div), div.attachEvent)
            for (i in { submit: !0, change: !0, focusin: !0 }) eventName = "on" + i, isSupported = eventName in div,
                isSupported || (div.setAttribute(eventName, "return;"), isSupported = "function" == typeof div[eventName]), support[i + "Bubbles"] = isSupported;
        return jQuery(function() {
            var container, div, tds, marginDiv, divReset = "padding:0;margin:0;border:0;display:block;overflow:hidden;",
                body = document.getElementsByTagName("body")[0];
            body && (container = document.createElement("div"), container.style.cssText = "visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px", body.insertBefore(container, body.firstChild), div = document.createElement("div"), container.appendChild(div), div.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", tds = div.getElementsByTagName("td"), tds[0].style.cssText = "padding:0;margin:0;border:0;display:none", isSupported = 0 === tds[0].offsetHeight, tds[0].style.display = "", tds[1].style.display = "none", support.reliableHiddenOffsets = isSupported && 0 === tds[0].offsetHeight, div.innerHTML = "", div.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", support.boxSizing = 4 === div.offsetWidth, support.doesNotIncludeMarginInBodyOffset = 1 !== body.offsetTop, window.getComputedStyle && (support.pixelPosition = "1%" !== (window.getComputedStyle(div, null) || {}).top, support.boxSizingReliable = "4px" === (window.getComputedStyle(div, null) || { width: "4px" }).width, marginDiv = document.createElement("div"), marginDiv.style.cssText = div.style.cssText = divReset, marginDiv.style.marginRight = marginDiv.style.width = "0", div.style.width = "1px", div.appendChild(marginDiv), support.reliableMarginRight = !parseFloat((window.getComputedStyle(marginDiv, null) || {}).marginRight)), "undefined" != typeof div.style.zoom && (div.innerHTML = "", div.style.cssText = divReset + "width:1px;padding:1px;display:inline;zoom:1", support.inlineBlockNeedsLayout = 3 === div.offsetWidth, div.style.display = "block", div.style.overflow = "visible", div.innerHTML = "<div></div>", div.firstChild.style.width = "5px", support.shrinkWrapBlocks = 3 !== div.offsetWidth, container.style.zoom = 1), body.removeChild(container), container = div = tds = marginDiv = null) }), fragment.removeChild(div), all = a = select = opt = input = fragment = div = null, support
    }();
    var rbrace = /^(?:\{.*\}|\[.*\])$/,
        rmultiDash = /([A-Z])/g;
    jQuery.extend({ cache: {}, deletedIds: [], uuid: 0, expando: "jQuery" + (jQuery.fn.jquery + Math.random()).replace(/\D/g, ""), noData: { embed: !0, object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000", applet: !0 }, hasData: function(elem) {
            return elem = elem.nodeType ? jQuery.cache[elem[jQuery.expando]] : elem[jQuery.expando], !!elem && !isEmptyDataObject(elem) }, data: function(elem, name, data, pvt) {
            if (jQuery.acceptData(elem)) {
                var thisCache, ret, internalKey = jQuery.expando,
                    getByName = "string" == typeof name,
                    isNode = elem.nodeType,
                    cache = isNode ? jQuery.cache : elem,
                    id = isNode ? elem[internalKey] : elem[internalKey] && internalKey;
                if (id && cache[id] && (pvt || cache[id].data) || !getByName || data !== undefined) return id || (isNode ? elem[internalKey] = id = jQuery.deletedIds.pop() || ++jQuery.uuid : id = internalKey), cache[id] || (cache[id] = {}, isNode || (cache[id].toJSON = jQuery.noop)), ("object" == typeof name || "function" == typeof name) && (pvt ? cache[id] = jQuery.extend(cache[id], name) : cache[id].data = jQuery.extend(cache[id].data, name)), thisCache = cache[id], pvt || (thisCache.data || (thisCache.data = {}), thisCache = thisCache.data), data !== undefined && (thisCache[jQuery.camelCase(name)] = data), getByName ? (ret = thisCache[name], null == ret && (ret = thisCache[jQuery.camelCase(name)])) : ret = thisCache, ret } }, removeData: function(elem, name, pvt) {
            if (jQuery.acceptData(elem)) {
                var thisCache, i, l, isNode = elem.nodeType,
                    cache = isNode ? jQuery.cache : elem,
                    id = isNode ? elem[jQuery.expando] : jQuery.expando;
                if (cache[id]) {
                    if (name && (thisCache = pvt ? cache[id] : cache[id].data)) { jQuery.isArray(name) || (name in thisCache ? name = [name] : (name = jQuery.camelCase(name), name = name in thisCache ? [name] : name.split(" ")));
                        for (i = 0, l = name.length; l > i; i++) delete thisCache[name[i]];
                        if (!(pvt ? isEmptyDataObject : jQuery.isEmptyObject)(thisCache)) return }(pvt || (delete cache[id].data, isEmptyDataObject(cache[id]))) && (isNode ? jQuery.cleanData([elem], !0) : jQuery.support.deleteExpando || cache != cache.window ? delete cache[id] : cache[id] = null) } } }, _data: function(elem, name, data) {
            return jQuery.data(elem, name, data, !0) }, acceptData: function(elem) {
            var noData = elem.nodeName && jQuery.noData[elem.nodeName.toLowerCase()];
            return !noData || noData !== !0 && elem.getAttribute("classid") === noData } }), jQuery.fn.extend({ data: function(key, value) {
            var parts, part, attr, name, l, elem = this[0],
                i = 0,
                data = null;
            if (key === undefined) {
                if (this.length && (data = jQuery.data(elem), 1 === elem.nodeType && !jQuery._data(elem, "parsedAttrs"))) {
                    for (attr = elem.attributes, l = attr.length; l > i; i++) name = attr[i].name, 0 === name.indexOf("data-") && (name = jQuery.camelCase(name.substring(5)), dataAttr(elem, name, data[name]));
                    jQuery._data(elem, "parsedAttrs", !0) }
                return data }
            return "object" == typeof key ? this.each(function() { jQuery.data(this, key) }) : (parts = key.split(".", 2), parts[1] = parts[1] ? "." + parts[1] : "", part = parts[1] + "!", jQuery.access(this, function(value) {
                return value === undefined ? (data = this.triggerHandler("getData" + part, [parts[0]]), data === undefined && elem && (data = jQuery.data(elem, key), data = dataAttr(elem, key, data)), data === undefined && parts[1] ? this.data(parts[0]) : data) : (parts[1] = value, void this.each(function() {
                    var self = jQuery(this);
                    self.triggerHandler("setData" + part, parts), jQuery.data(this, key, value), self.triggerHandler("changeData" + part, parts) })) }, null, value, arguments.length > 1, null, !1)) }, removeData: function(key) {
            return this.each(function() { jQuery.removeData(this, key) }) } }), jQuery.extend({ queue: function(elem, type, data) {
            var queue;
            return elem ? (type = (type || "fx") + "queue", queue = jQuery._data(elem, type), data && (!queue || jQuery.isArray(data) ? queue = jQuery._data(elem, type, jQuery.makeArray(data)) : queue.push(data)), queue || []) : void 0 }, dequeue: function(elem, type) { type = type || "fx";
            var queue = jQuery.queue(elem, type),
                fn = queue.shift(),
                hooks = jQuery._queueHooks(elem, type),
                next = function() { jQuery.dequeue(elem, type) }; "inprogress" === fn && (fn = queue.shift()), fn && ("fx" === type && queue.unshift("inprogress"), delete hooks.stop, fn.call(elem, next, hooks)), !queue.length && hooks && hooks.empty.fire() }, _queueHooks: function(elem, type) {
            var key = type + "queueHooks";
            return jQuery._data(elem, key) || jQuery._data(elem, key, { empty: jQuery.Callbacks("once memory").add(function() { jQuery.removeData(elem, type + "queue", !0), jQuery.removeData(elem, key, !0) }) }) } }), jQuery.fn.extend({ queue: function(type, data) {
            var setter = 2;
            return "string" != typeof type && (data = type, type = "fx", setter--), arguments.length < setter ? jQuery.queue(this[0], type) : data === undefined ? this : this.each(function() {
                var queue = jQuery.queue(this, type, data);
                jQuery._queueHooks(this, type), "fx" === type && "inprogress" !== queue[0] && jQuery.dequeue(this, type) }) }, dequeue: function(type) {
            return this.each(function() { jQuery.dequeue(this, type) }) }, delay: function(time, type) {
            return time = jQuery.fx ? jQuery.fx.speeds[time] || time : time, type = type || "fx", this.queue(type, function(next, hooks) {
                var timeout = setTimeout(next, time);
                hooks.stop = function() { clearTimeout(timeout) } }) }, clearQueue: function(type) {
            return this.queue(type || "fx", []) }, promise: function(type, obj) {
            var tmp, count = 1,
                defer = jQuery.Deferred(),
                elements = this,
                i = this.length,
                resolve = function() {--count || defer.resolveWith(elements, [elements]) };
            for ("string" != typeof type && (obj = type, type = undefined), type = type || "fx"; i--;)(tmp = jQuery._data(elements[i], type + "queueHooks")) && tmp.empty && (count++, tmp.empty.add(resolve));
            return resolve(), defer.promise(obj) } });
    var nodeHook, boolHook, fixSpecified, rclass = /[\t\r\n]/g,
        rreturn = /\r/g,
        rtype = /^(?:button|input)$/i,
        rfocusable = /^(?:button|input|object|select|textarea)$/i,
        rclickable = /^a(?:rea|)$/i,
        rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
        getSetAttribute = jQuery.support.getSetAttribute;
    jQuery.fn.extend({ attr: function(name, value) {
            return jQuery.access(this, jQuery.attr, name, value, arguments.length > 1) }, removeAttr: function(name) {
            return this.each(function() { jQuery.removeAttr(this, name) }) }, prop: function(name, value) {
            return jQuery.access(this, jQuery.prop, name, value, arguments.length > 1) }, removeProp: function(name) {
            return name = jQuery.propFix[name] || name, this.each(function() {
                try { this[name] = undefined, delete this[name] } catch (e) {} }) }, addClass: function(value) {
            var classNames, i, l, elem, setClass, c, cl;
            if (jQuery.isFunction(value)) return this.each(function(j) { jQuery(this).addClass(value.call(this, j, this.className)) });
            if (value && "string" == typeof value)
                for (classNames = value.split(core_rspace), i = 0, l = this.length; l > i; i++)
                    if (elem = this[i], 1 === elem.nodeType)
                        if (elem.className || 1 !== classNames.length) {
                            for (setClass = " " + elem.className + " ", c = 0, cl = classNames.length; cl > c; c++) ~setClass.indexOf(" " + classNames[c] + " ") || (setClass += classNames[c] + " ");
                            elem.className = jQuery.trim(setClass) } else elem.className = value;
            return this }, removeClass: function(value) {
            var removes, className, elem, c, cl, i, l;
            if (jQuery.isFunction(value)) return this.each(function(j) { jQuery(this).removeClass(value.call(this, j, this.className)) });
            if (value && "string" == typeof value || value === undefined)
                for (removes = (value || "").split(core_rspace), i = 0, l = this.length; l > i; i++)
                    if (elem = this[i], 1 === elem.nodeType && elem.className) {
                        for (className = (" " + elem.className + " ").replace(rclass, " "), c = 0, cl = removes.length; cl > c; c++)
                            for (; className.indexOf(" " + removes[c] + " ") > -1;) className = className.replace(" " + removes[c] + " ", " ");
                        elem.className = value ? jQuery.trim(className) : "" }
            return this }, toggleClass: function(value, stateVal) {
            var type = typeof value,
                isBool = "boolean" == typeof stateVal;
            return this.each(jQuery.isFunction(value) ? function(i) { jQuery(this).toggleClass(value.call(this, i, this.className, stateVal), stateVal) } : function() {
                if ("string" === type)
                    for (var className, i = 0, self = jQuery(this), state = stateVal, classNames = value.split(core_rspace); className = classNames[i++];) state = isBool ? state : !self.hasClass(className), self[state ? "addClass" : "removeClass"](className);
                else("undefined" === type || "boolean" === type) && (this.className && jQuery._data(this, "__className__", this.className), this.className = this.className || value === !1 ? "" : jQuery._data(this, "__className__") || "") }) }, hasClass: function(selector) {
            for (var className = " " + selector + " ", i = 0, l = this.length; l > i; i++)
                if (1 === this[i].nodeType && (" " + this[i].className + " ").replace(rclass, " ").indexOf(className) > -1) return !0;
            return !1 }, val: function(value) {
            var hooks, ret, isFunction, elem = this[0]; {
                if (arguments.length) return isFunction = jQuery.isFunction(value), this.each(function(i) {
                    var val, self = jQuery(this);
                    1 === this.nodeType && (val = isFunction ? value.call(this, i, self.val()) : value, null == val ? val = "" : "number" == typeof val ? val += "" : jQuery.isArray(val) && (val = jQuery.map(val, function(value) {
                        return null == value ? "" : value + "" })), hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()], hooks && "set" in hooks && hooks.set(this, val, "value") !== undefined || (this.value = val)) });
                if (elem) return hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()], hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined ? ret : (ret = elem.value, "string" == typeof ret ? ret.replace(rreturn, "") : null == ret ? "" : ret) } } }), jQuery.extend({ valHooks: { option: { get: function(elem) {
                    var val = elem.attributes.value;
                    return !val || val.specified ? elem.value : elem.text } }, select: { get: function(elem) {
                    var value, i, max, option, index = elem.selectedIndex,
                        values = [],
                        options = elem.options,
                        one = "select-one" === elem.type;
                    if (0 > index) return null;
                    for (i = one ? index : 0, max = one ? index + 1 : options.length; max > i; i++)
                        if (option = options[i], !(!option.selected || (jQuery.support.optDisabled ? option.disabled : null !== option.getAttribute("disabled")) || option.parentNode.disabled && jQuery.nodeName(option.parentNode, "optgroup"))) {
                            if (value = jQuery(option).val(), one) return value;
                            values.push(value) }
                    return one && !values.length && options.length ? jQuery(options[index]).val() : values }, set: function(elem, value) {
                    var values = jQuery.makeArray(value);
                    return jQuery(elem).find("option").each(function() { this.selected = jQuery.inArray(jQuery(this).val(), values) >= 0 }), values.length || (elem.selectedIndex = -1), values } } }, attrFn: {}, attr: function(elem, name, value, pass) {
            var ret, hooks, notxml, nType = elem.nodeType;
            if (elem && 3 !== nType && 8 !== nType && 2 !== nType) return pass && jQuery.isFunction(jQuery.fn[name]) ? jQuery(elem)[name](value) : "undefined" == typeof elem.getAttribute ? jQuery.prop(elem, name, value) : (notxml = 1 !== nType || !jQuery.isXMLDoc(elem), notxml && (name = name.toLowerCase(), hooks = jQuery.attrHooks[name] || (rboolean.test(name) ? boolHook : nodeHook)), value !== undefined ? null === value ? void jQuery.removeAttr(elem, name) : hooks && "set" in hooks && notxml && (ret = hooks.set(elem, value, name)) !== undefined ? ret : (elem.setAttribute(name, "" + value), value) : hooks && "get" in hooks && notxml && null !== (ret = hooks.get(elem, name)) ? ret : (ret = elem.getAttribute(name), null === ret ? undefined : ret)) }, removeAttr: function(elem, value) {
            var propName, attrNames, name, isBool, i = 0;
            if (value && 1 === elem.nodeType)
                for (attrNames = value.split(core_rspace); i < attrNames.length; i++) name = attrNames[i], name && (propName = jQuery.propFix[name] || name, isBool = rboolean.test(name), isBool || jQuery.attr(elem, name, ""), elem.removeAttribute(getSetAttribute ? name : propName), isBool && propName in elem && (elem[propName] = !1)) }, attrHooks: { type: { set: function(elem, value) {
                    if (rtype.test(elem.nodeName) && elem.parentNode) jQuery.error("type property can't be changed");
                    else if (!jQuery.support.radioValue && "radio" === value && jQuery.nodeName(elem, "input")) {
                        var val = elem.value;
                        return elem.setAttribute("type", value), val && (elem.value = val), value } } }, value: { get: function(elem, name) {
                    return nodeHook && jQuery.nodeName(elem, "button") ? nodeHook.get(elem, name) : name in elem ? elem.value : null }, set: function(elem, value, name) {
                    return nodeHook && jQuery.nodeName(elem, "button") ? nodeHook.set(elem, value, name) : void(elem.value = value) } } }, propFix: { tabindex: "tabIndex", readonly: "readOnly", "for": "htmlFor", "class": "className", maxlength: "maxLength", cellspacing: "cellSpacing", cellpadding: "cellPadding", rowspan: "rowSpan", colspan: "colSpan", usemap: "useMap", frameborder: "frameBorder", contenteditable: "contentEditable" }, prop: function(elem, name, value) {
            var ret, hooks, notxml, nType = elem.nodeType;
            if (elem && 3 !== nType && 8 !== nType && 2 !== nType) return notxml = 1 !== nType || !jQuery.isXMLDoc(elem), notxml && (name = jQuery.propFix[name] || name, hooks = jQuery.propHooks[name]), value !== undefined ? hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined ? ret : elem[name] = value : hooks && "get" in hooks && null !== (ret = hooks.get(elem, name)) ? ret : elem[name] }, propHooks: { tabIndex: { get: function(elem) {
                    var attributeNode = elem.getAttributeNode("tabindex");
                    return attributeNode && attributeNode.specified ? parseInt(attributeNode.value, 10) : rfocusable.test(elem.nodeName) || rclickable.test(elem.nodeName) && elem.href ? 0 : undefined } } } }), boolHook = { get: function(elem, name) {
            var attrNode, property = jQuery.prop(elem, name);
            return property === !0 || "boolean" != typeof property && (attrNode = elem.getAttributeNode(name)) && attrNode.nodeValue !== !1 ? name.toLowerCase() : undefined }, set: function(elem, value, name) {
            var propName;
            return value === !1 ? jQuery.removeAttr(elem, name) : (propName = jQuery.propFix[name] || name, propName in elem && (elem[propName] = !0), elem.setAttribute(name, name.toLowerCase())), name } }, getSetAttribute || (fixSpecified = { name: !0, id: !0, coords: !0 }, nodeHook = jQuery.valHooks.button = { get: function(elem, name) {
            var ret;
            return ret = elem.getAttributeNode(name), ret && (fixSpecified[name] ? "" !== ret.value : ret.specified) ? ret.value : undefined }, set: function(elem, value, name) {
            var ret = elem.getAttributeNode(name);
            return ret || (ret = document.createAttribute(name), elem.setAttributeNode(ret)), ret.value = value + "" } }, jQuery.each(["width", "height"], function(i, name) { jQuery.attrHooks[name] = jQuery.extend(jQuery.attrHooks[name], { set: function(elem, value) {
                return "" === value ? (elem.setAttribute(name, "auto"), value) : void 0 } }) }), jQuery.attrHooks.contenteditable = { get: nodeHook.get, set: function(elem, value, name) { "" === value && (value = "false"), nodeHook.set(elem, value, name) } }), jQuery.support.hrefNormalized || jQuery.each(["href", "src", "width", "height"], function(i, name) { jQuery.attrHooks[name] = jQuery.extend(jQuery.attrHooks[name], { get: function(elem) {
                var ret = elem.getAttribute(name, 2);
                return null === ret ? undefined : ret } }) }), jQuery.support.style || (jQuery.attrHooks.style = { get: function(elem) {
            return elem.style.cssText.toLowerCase() || undefined }, set: function(elem, value) {
            return elem.style.cssText = "" + value } }), jQuery.support.optSelected || (jQuery.propHooks.selected = jQuery.extend(jQuery.propHooks.selected, { get: function(elem) {
            var parent = elem.parentNode;
            return parent && (parent.selectedIndex, parent.parentNode && parent.parentNode.selectedIndex), null } })), jQuery.support.enctype || (jQuery.propFix.enctype = "encoding"), jQuery.support.checkOn || jQuery.each(["radio", "checkbox"], function() { jQuery.valHooks[this] = { get: function(elem) {
                return null === elem.getAttribute("value") ? "on" : elem.value } } }), jQuery.each(["radio", "checkbox"], function() { jQuery.valHooks[this] = jQuery.extend(jQuery.valHooks[this], { set: function(elem, value) {
                return jQuery.isArray(value) ? elem.checked = jQuery.inArray(jQuery(elem).val(), value) >= 0 : void 0 } }) });
    var rformElems = /^(?:textarea|input|select)$/i,
        rtypenamespace = /^([^\.]*|)(?:\.(.+)|)$/,
        rhoverHack = /(?:^|\s)hover(\.\S+|)\b/,
        rkeyEvent = /^key/,
        rmouseEvent = /^(?:mouse|contextmenu)|click/,
        rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
        hoverHack = function(events) {
            return jQuery.event.special.hover ? events : events.replace(rhoverHack, "mouseenter$1 mouseleave$1") };
    jQuery.event = { add: function(elem, types, handler, data, selector) {
                var elemData, eventHandle, events, t, tns, type, namespaces, handleObj, handleObjIn, handlers, special;
                if (3 !== elem.nodeType && 8 !== elem.nodeType && types && handler && (elemData = jQuery._data(elem))) {
                    for (handler.handler && (handleObjIn = handler, handler = handleObjIn.handler, selector = handleObjIn.selector), handler.guid || (handler.guid = jQuery.guid++), events = elemData.events, events || (elemData.events = events = {}), eventHandle = elemData.handle, eventHandle || (elemData.handle = eventHandle = function(e) {
                            return "undefined" == typeof jQuery || e && jQuery.event.triggered === e.type ? undefined : jQuery.event.dispatch.apply(eventHandle.elem, arguments) }, eventHandle.elem = elem), types = jQuery.trim(hoverHack(types)).split(" "), t = 0; t < types.length; t++) tns = rtypenamespace.exec(types[t]) || [], type = tns[1], namespaces = (tns[2] || "").split(".").sort(), special = jQuery.event.special[type] || {}, type = (selector ? special.delegateType : special.bindType) || type, special = jQuery.event.special[type] || {}, handleObj = jQuery.extend({ type: type, origType: tns[1], data: data, handler: handler, guid: handler.guid, selector: selector, namespace: namespaces.join(".") }, handleObjIn), handlers = events[type], handlers || (handlers = events[type] = [], handlers.delegateCount = 0, special.setup && special.setup.call(elem, data, namespaces, eventHandle) !== !1 || (elem.addEventListener ? elem.addEventListener(type, eventHandle, !1) : elem.attachEvent && elem.attachEvent("on" + type, eventHandle))), special.add && (special.add.call(elem, handleObj), handleObj.handler.guid || (handleObj.handler.guid = handler.guid)), selector ? handlers.splice(handlers.delegateCount++, 0, handleObj) : handlers.push(handleObj), jQuery.event.global[type] = !0;
                    elem = null } }, global: {}, remove: function(elem, types, handler, selector, mappedTypes) {
                var t, tns, type, origType, namespaces, origCount, j, events, special, eventType, handleObj, elemData = jQuery.hasData(elem) && jQuery._data(elem);
                if (elemData && (events = elemData.events)) {
                    for (types = jQuery.trim(hoverHack(types || "")).split(" "), t = 0; t < types.length; t++)
                        if (tns = rtypenamespace.exec(types[t]) || [], type = origType = tns[1], namespaces = tns[2], type) {
                            for (special = jQuery.event.special[type] || {}, type = (selector ? special.delegateType : special.bindType) || type, eventType = events[type] || [], origCount = eventType.length, namespaces = namespaces ? new RegExp("(^|\\.)" + namespaces.split(".").sort().join("\\.(?:.*\\.|)") + "(\\.|$)") : null, j = 0; j < eventType.length; j++) handleObj = eventType[j], !mappedTypes && origType !== handleObj.origType || handler && handler.guid !== handleObj.guid || namespaces && !namespaces.test(handleObj.namespace) || selector && selector !== handleObj.selector && ("**" !== selector || !handleObj.selector) || (eventType.splice(j--, 1), handleObj.selector && eventType.delegateCount--, special.remove && special.remove.call(elem, handleObj));
                            0 === eventType.length && origCount !== eventType.length && (special.teardown && special.teardown.call(elem, namespaces, elemData.handle) !== !1 || jQuery.removeEvent(elem, type, elemData.handle), delete events[type]) } else
                            for (type in events) jQuery.event.remove(elem, type + types[t], handler, selector, !0);
                    jQuery.isEmptyObject(events) && (delete elemData.handle, jQuery.removeData(elem, "events", !0)) } }, customEvent: { getData: !0, setData: !0, changeData: !0 }, trigger: function(event, data, elem, onlyHandlers) {
                if (!elem || 3 !== elem.nodeType && 8 !== elem.nodeType) {
                    var cache, exclusive, i, cur, old, ontype, special, handle, eventPath, bubbleType, type = event.type || event,
                        namespaces = [];
                    if (!rfocusMorph.test(type + jQuery.event.triggered) && (type.indexOf("!") >= 0 && (type = type.slice(0, -1), exclusive = !0), type.indexOf(".") >= 0 && (namespaces = type.split("."), type = namespaces.shift(), namespaces.sort()), elem && !jQuery.event.customEvent[type] || jQuery.event.global[type]))
                        if (event = "object" == typeof event ? event[jQuery.expando] ? event : new jQuery.Event(type, event) : new jQuery.Event(type), event.type = type, event.isTrigger = !0, event.exclusive = exclusive, event.namespace = namespaces.join("."), event.namespace_re = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, ontype = type.indexOf(":") < 0 ? "on" + type : "", elem) {
                            if (event.result = undefined, event.target || (event.target = elem), data = null != data ? jQuery.makeArray(data) : [], data.unshift(event), special = jQuery.event.special[type] || {}, !special.trigger || special.trigger.apply(elem, data) !== !1) {
                                if (eventPath = [
                                        [elem, special.bindType || type]
                                    ], !onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {
                                    for (bubbleType = special.delegateType || type, cur = rfocusMorph.test(bubbleType + type) ? elem : elem.parentNode, old = elem; cur; cur = cur.parentNode) eventPath.push([cur, bubbleType]), old = cur;
                                    old === (elem.ownerDocument || document) && eventPath.push([old.defaultView || old.parentWindow || window, bubbleType]) }
                                for (i = 0; i < eventPath.length && !event.isPropagationStopped(); i++) cur = eventPath[i][0], event.type = eventPath[i][1], handle = (jQuery._data(cur, "events") || {})[event.type] && jQuery._data(cur, "handle"), handle && handle.apply(cur, data), handle = ontype && cur[ontype], handle && jQuery.acceptData(cur) && handle.apply(cur, data) === !1 && event.preventDefault();
                                return event.type = type, onlyHandlers || event.isDefaultPrevented() || special._default && special._default.apply(elem.ownerDocument, data) !== !1 || "click" === type && jQuery.nodeName(elem, "a") || !jQuery.acceptData(elem) || ontype && elem[type] && ("focus" !== type && "blur" !== type || 0 !== event.target.offsetWidth) && !jQuery.isWindow(elem) && (old = elem[ontype], old && (elem[ontype] = null), jQuery.event.triggered = type, elem[type](), jQuery.event.triggered = undefined, old && (elem[ontype] = old)), event.result } } else { cache = jQuery.cache;
                            for (i in cache) cache[i].events && cache[i].events[type] && jQuery.event.trigger(event, data, cache[i].handle.elem, !0) } } }, dispatch: function(event) { event = jQuery.event.fix(event || window.event);
                var i, j, cur, jqcur, ret, selMatch, matched, matches, handleObj, sel, handlers = (jQuery._data(this, "events") || {})[event.type] || [],
                    delegateCount = handlers.delegateCount,
                    args = [].slice.call(arguments),
                    run_all = !event.exclusive && !event.namespace,
                    special = jQuery.event.special[event.type] || {},
                    handlerQueue = [];
                if (args[0] = event, event.delegateTarget = this, !special.preDispatch || special.preDispatch.call(this, event) !== !1) {
                    if (delegateCount && (!event.button || "click" !== event.type))
                        for (jqcur = jQuery(this), jqcur.context = this, cur = event.target; cur != this; cur = cur.parentNode || this)
                            if (cur.disabled !== !0 || "click" !== event.type) {
                                for (selMatch = {}, matches = [], jqcur[0] = cur, i = 0; delegateCount > i; i++) handleObj = handlers[i], sel = handleObj.selector, selMatch[sel] === undefined && (selMatch[sel] = jqcur.is(sel)), selMatch[sel] && matches.push(handleObj);
                                matches.length && handlerQueue.push({ elem: cur, matches: matches }) }
                    for (handlers.length > delegateCount && handlerQueue.push({ elem: this, matches: handlers.slice(delegateCount) }), i = 0; i < handlerQueue.length && !event.isPropagationStopped(); i++)
                        for (matched = handlerQueue[i], event.currentTarget = matched.elem, j = 0; j < matched.matches.length && !event.isImmediatePropagationStopped(); j++) handleObj = matched.matches[j], (run_all || !event.namespace && !handleObj.namespace || event.namespace_re && event.namespace_re.test(handleObj.namespace)) && (event.data = handleObj.data, event.handleObj = handleObj, ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args), ret !== undefined && (event.result = ret, ret === !1 && (event.preventDefault(), event.stopPropagation())));
                    return special.postDispatch && special.postDispatch.call(this, event), event.result } }, props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "), fixHooks: {}, keyHooks: { props: "char charCode key keyCode".split(" "), filter: function(event, original) {
                    return null == event.which && (event.which = null != original.charCode ? original.charCode : original.keyCode), event } }, mouseHooks: { props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter: function(event, original) {
                    var eventDoc, doc, body, button = original.button,
                        fromElement = original.fromElement;
                    return null == event.pageX && null != original.clientX && (eventDoc = event.target.ownerDocument || document, doc = eventDoc.documentElement, body = eventDoc.body, event.pageX = original.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0), event.pageY = original.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0)), !event.relatedTarget && fromElement && (event.relatedTarget = fromElement === event.target ? original.toElement : fromElement), event.which || button === undefined || (event.which = 1 & button ? 1 : 2 & button ? 3 : 4 & button ? 2 : 0), event } }, fix: function(event) {
                if (event[jQuery.expando]) return event;
                var i, prop, originalEvent = event,
                    fixHook = jQuery.event.fixHooks[event.type] || {},
                    copy = fixHook.props ? this.props.concat(fixHook.props) : this.props;
                for (event = jQuery.Event(originalEvent), i = copy.length; i;) prop = copy[--i], event[prop] = originalEvent[prop];
                return event.target || (event.target = originalEvent.srcElement || document), 3 === event.target.nodeType && (event.target = event.target.parentNode), event.metaKey = !!event.metaKey, fixHook.filter ? fixHook.filter(event, originalEvent) : event }, special: { ready: { setup: jQuery.bindReady }, load: { noBubble: !0 }, focus: { delegateType: "focusin" }, blur: { delegateType: "focusout" }, beforeunload: { setup: function(data, namespaces, eventHandle) { jQuery.isWindow(this) && (this.onbeforeunload = eventHandle) }, teardown: function(namespaces, eventHandle) { this.onbeforeunload === eventHandle && (this.onbeforeunload = null) } } }, simulate: function(type, elem, event, bubble) {
                var e = jQuery.extend(new jQuery.Event, event, { type: type, isSimulated: !0, originalEvent: {} });
                bubble ? jQuery.event.trigger(e, null, elem) : jQuery.event.dispatch.call(elem, e), e.isDefaultPrevented() && event.preventDefault() } }, jQuery.event.handle = jQuery.event.dispatch, jQuery.removeEvent = document.removeEventListener ? function(elem, type, handle) { elem.removeEventListener && elem.removeEventListener(type, handle, !1) } : function(elem, type, handle) {
            var name = "on" + type;
            elem.detachEvent && ("undefined" == typeof elem[name] && (elem[name] = null), elem.detachEvent(name, handle)) }, jQuery.Event = function(src, props) {
            return this instanceof jQuery.Event ? (src && src.type ? (this.originalEvent = src, this.type = src.type, this.isDefaultPrevented = src.defaultPrevented || src.returnValue === !1 || src.getPreventDefault && src.getPreventDefault() ? returnTrue : returnFalse) : this.type = src, props && jQuery.extend(this, props), this.timeStamp = src && src.timeStamp || jQuery.now(), void(this[jQuery.expando] = !0)) : new jQuery.Event(src, props) }, jQuery.Event.prototype = { preventDefault: function() { this.isDefaultPrevented = returnTrue;
                var e = this.originalEvent;
                e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1) }, stopPropagation: function() { this.isPropagationStopped = returnTrue;
                var e = this.originalEvent;
                e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0) }, stopImmediatePropagation: function() { this.isImmediatePropagationStopped = returnTrue, this.stopPropagation() }, isDefaultPrevented: returnFalse, isPropagationStopped: returnFalse, isImmediatePropagationStopped: returnFalse }, jQuery.each({ mouseenter: "mouseover", mouseleave: "mouseout" }, function(orig, fix) { jQuery.event.special[orig] = { delegateType: fix, bindType: fix, handle: function(event) {
                    {
                        var ret, target = this,
                            related = event.relatedTarget,
                            handleObj = event.handleObj;
                        handleObj.selector }
                    return (!related || related !== target && !jQuery.contains(target, related)) && (event.type = handleObj.origType, ret = handleObj.handler.apply(this, arguments), event.type = fix), ret } } }), jQuery.support.submitBubbles || (jQuery.event.special.submit = { setup: function() {
                return jQuery.nodeName(this, "form") ? !1 : void jQuery.event.add(this, "click._submit keypress._submit", function(e) {
                    var elem = e.target,
                        form = jQuery.nodeName(elem, "input") || jQuery.nodeName(elem, "button") ? elem.form : undefined;
                    form && !jQuery._data(form, "_submit_attached") && (jQuery.event.add(form, "submit._submit", function(event) { event._submit_bubble = !0 }), jQuery._data(form, "_submit_attached", !0)) }) }, postDispatch: function(event) { event._submit_bubble && (delete event._submit_bubble, this.parentNode && !event.isTrigger && jQuery.event.simulate("submit", this.parentNode, event, !0)) }, teardown: function() {
                return jQuery.nodeName(this, "form") ? !1 : void jQuery.event.remove(this, "._submit") } }), jQuery.support.changeBubbles || (jQuery.event.special.change = { setup: function() {
                return rformElems.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (jQuery.event.add(this, "propertychange._change", function(event) { "checked" === event.originalEvent.propertyName && (this._just_changed = !0) }), jQuery.event.add(this, "click._change", function(event) { this._just_changed && !event.isTrigger && (this._just_changed = !1), jQuery.event.simulate("change", this, event, !0) })), !1) : void jQuery.event.add(this, "beforeactivate._change", function(e) {
                    var elem = e.target;
                    rformElems.test(elem.nodeName) && !jQuery._data(elem, "_change_attached") && (jQuery.event.add(elem, "change._change", function(event) {!this.parentNode || event.isSimulated || event.isTrigger || jQuery.event.simulate("change", this.parentNode, event, !0) }), jQuery._data(elem, "_change_attached", !0)) }) }, handle: function(event) {
                var elem = event.target;
                return this !== elem || event.isSimulated || event.isTrigger || "radio" !== elem.type && "checkbox" !== elem.type ? event.handleObj.handler.apply(this, arguments) : void 0 }, teardown: function() {
                return jQuery.event.remove(this, "._change"), rformElems.test(this.nodeName) } }), jQuery.support.focusinBubbles || jQuery.each({ focus: "focusin", blur: "focusout" }, function(orig, fix) {
            var attaches = 0,
                handler = function(event) { jQuery.event.simulate(fix, event.target, jQuery.event.fix(event), !0) };
            jQuery.event.special[fix] = { setup: function() { 0 === attaches++ && document.addEventListener(orig, handler, !0) }, teardown: function() { 0 === --attaches && document.removeEventListener(orig, handler, !0) } } }), jQuery.fn.extend({
            on: function(types, selector, data, fn, one) {
                var origFn, type;
                if ("object" == typeof types) { "string" != typeof selector && (data = data || selector, selector = undefined);
                    for (type in types) this.on(type, selector, data, types[type], one);
                    return this }
                if (null == data && null == fn ? (fn = selector, data = selector = undefined) : null == fn && ("string" == typeof selector ? (fn = data, data = undefined) : (fn = data, data = selector, selector = undefined)), fn === !1) fn = returnFalse;
                else if (!fn) return this;
                return 1 === one && (origFn = fn, fn = function(event) {
                    return jQuery().off(event), origFn.apply(this, arguments) }, fn.guid = origFn.guid || (origFn.guid = jQuery.guid++)), this.each(function() { jQuery.event.add(this, types, fn, data, selector) }) },
            one: function(types, selector, data, fn) {
                return this.on(types, selector, data, fn, 1) },
            off: function(types, selector, fn) {
                var handleObj, type;
                if (types && types.preventDefault && types.handleObj) return handleObj = types.handleObj, jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler), this;
                if ("object" == typeof types) {
                    for (type in types) this.off(type, selector, types[type]);
                    return this }
                return (selector === !1 || "function" == typeof selector) && (fn = selector, selector = undefined),
                    fn === !1 && (fn = returnFalse), this.each(function() { jQuery.event.remove(this, types, fn, selector) })
            },
            bind: function(types, data, fn) {
                return this.on(types, null, data, fn) },
            unbind: function(types, fn) {
                return this.off(types, null, fn) },
            live: function(types, data, fn) {
                return jQuery(this.context).on(types, this.selector, data, fn), this },
            die: function(types, fn) {
                return jQuery(this.context).off(types, this.selector || "**", fn), this },
            delegate: function(selector, types, data, fn) {
                return this.on(types, selector, data, fn) },
            undelegate: function(selector, types, fn) {
                return 1 == arguments.length ? this.off(selector, "**") : this.off(types, selector || "**", fn) },
            trigger: function(type, data) {
                return this.each(function() { jQuery.event.trigger(type, data, this) }) },
            triggerHandler: function(type, data) {
                return this[0] ? jQuery.event.trigger(type, data, this[0], !0) : void 0 },
            toggle: function(fn) {
                var args = arguments,
                    guid = fn.guid || jQuery.guid++,
                    i = 0,
                    toggler = function(event) {
                        var lastToggle = (jQuery._data(this, "lastToggle" + fn.guid) || 0) % i;
                        return jQuery._data(this, "lastToggle" + fn.guid, lastToggle + 1), event.preventDefault(), args[lastToggle].apply(this, arguments) || !1 };
                for (toggler.guid = guid; i < args.length;) args[i++].guid = guid;
                return this.click(toggler) },
            hover: function(fnOver, fnOut) {
                return this.mouseenter(fnOver).mouseleave(fnOut || fnOver) }
        }), jQuery.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(i, name) { jQuery.fn[name] = function(data, fn) {
                return null == fn && (fn = data, data = null), arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name) }, rkeyEvent.test(name) && (jQuery.event.fixHooks[name] = jQuery.event.keyHooks), rmouseEvent.test(name) && (jQuery.event.fixHooks[name] = jQuery.event.mouseHooks) }),
        function(window, undefined) {
            function multipleContexts(selector, contexts, results, seed) {
                for (var i = 0, len = contexts.length; len > i; i++) Sizzle(selector, contexts[i], results, seed) }

            function handlePOSGroup(selector, posfilter, argument, contexts, seed, not) {
                var results, fn = Expr.setFilters[posfilter.toLowerCase()];
                return fn || Sizzle.error(posfilter), (selector || !(results = seed)) && multipleContexts(selector || "*", contexts, results = [], seed), results.length > 0 ? fn(results, argument, not) : [] }

            function handlePOS(selector, context, results, seed, groups) {
                for (var match, not, anchor, ret, elements, currentContexts, part, lastIndex, i = 0, len = groups.length, rpos = matchExpr.POS, rposgroups = new RegExp("^" + rpos.source + "(?!" + whitespace + ")", "i"), setUndefined = function() {
                        for (var i = 1, len = arguments.length - 2; len > i; i++) arguments[i] === undefined && (match[i] = undefined) }; len > i; i++) {
                    for (rpos.exec(""), selector = groups[i], ret = [], anchor = 0, elements = seed; match = rpos.exec(selector);) lastIndex = rpos.lastIndex = match.index + match[0].length, lastIndex > anchor && (part = selector.slice(anchor, match.index), anchor = lastIndex, currentContexts = [context], rcombinators.test(part) && (elements && (currentContexts = elements), elements = seed), (not = rendsWithNot.test(part)) && (part = part.slice(0, -5).replace(rcombinators, "$&*")), match.length > 1 && match[0].replace(rposgroups, setUndefined), elements = handlePOSGroup(part, match[1], match[2], currentContexts, elements, not));
                    elements ? (ret = ret.concat(elements), (part = selector.slice(anchor)) && ")" !== part ? rcombinators.test(part) ? multipleContexts(part, ret, results, seed) : Sizzle(part, context, results, seed ? seed.concat(elements) : elements) : push.apply(results, ret)) : Sizzle(selector, context, results, seed) }
                return 1 === len ? results : Sizzle.uniqueSort(results) }

            function tokenize(selector, context, xml) {
                for (var tokens, soFar, type, groups = [], i = 0, match = rselector.exec(selector), matched = !match.pop() && !match.pop(), selectorGroups = matched && selector.match(rgroups) || [""], preFilters = Expr.preFilter, filters = Expr.filter, checkContext = !xml && context !== document; null != (soFar = selectorGroups[i]) && matched; i++)
                    for (groups.push(tokens = []), checkContext && (soFar = " " + soFar); soFar;) { matched = !1, (match = rcombinators.exec(soFar)) && (soFar = soFar.slice(match[0].length), matched = tokens.push({ part: match.pop().replace(rtrim, " "), captures: match }));
                        for (type in filters) !(match = matchExpr[type].exec(soFar)) || preFilters[type] && !(match = preFilters[type](match, context, xml)) || (soFar = soFar.slice(match.shift().length), matched = tokens.push({ part: type, captures: match }));
                        if (!matched) break }
                return matched || Sizzle.error(selector), groups }

            function addCombinator(matcher, combinator, context) {
                var dir = combinator.dir,
                    doneName = done++;
                return matcher || (matcher = function(elem) {
                    return elem === context }), combinator.first ? function(elem, context) {
                    for (; elem = elem[dir];)
                        if (1 === elem.nodeType) return matcher(elem, context) && elem } : function(elem, context) {
                    for (var cache, dirkey = doneName + "." + dirruns, cachedkey = dirkey + "." + cachedruns; elem = elem[dir];)
                        if (1 === elem.nodeType) {
                            if ((cache = elem[expando]) === cachedkey) return elem.sizset;
                            if ("string" == typeof cache && 0 === cache.indexOf(dirkey)) {
                                if (elem.sizset) return elem } else {
                                if (elem[expando] = cachedkey, matcher(elem, context)) return elem.sizset = !0, elem;
                                elem.sizset = !1 } } } }

            function addMatcher(higher, deeper) {
                return higher ? function(elem, context) {
                    var result = deeper(elem, context);
                    return result && higher(result === !0 ? elem : result, context) } : deeper }

            function matcherFromTokens(tokens, context, xml) {
                for (var token, matcher, i = 0; token = tokens[i]; i++) Expr.relative[token.part] ? matcher = addCombinator(matcher, Expr.relative[token.part], context) : (token.captures.push(context, xml), matcher = addMatcher(matcher, Expr.filter[token.part].apply(null, token.captures)));
                return matcher }

            function matcherFromGroupMatchers(matchers) {
                return function(elem, context) {
                    for (var matcher, j = 0; matcher = matchers[j]; j++)
                        if (matcher(elem, context)) return !0;
                    return !1 } }
            var cachedruns, dirruns, sortOrder, siblingCheck, assertGetIdNotName, document = window.document,
                docElem = document.documentElement,
                strundefined = "undefined",
                hasDuplicate = !1,
                baseHasDuplicate = !0,
                done = 0,
                slice = [].slice,
                push = [].push,
                expando = ("sizcache" + Math.random()).replace(".", ""),
                whitespace = "[\\x20\\t\\r\\n\\f]",
                characterEncoding = "(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+",
                identifier = characterEncoding.replace("w", "w#"),
                operators = "([*^$|!~]?=)",
                attributes = "\\[" + whitespace + "*(" + characterEncoding + ")" + whitespace + "*(?:" + operators + whitespace + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + identifier + ")|)|)" + whitespace + "*\\]",
                pseudos = ":(" + characterEncoding + ")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|((?:[^,]|\\\\,|(?:,(?=[^\\[]*\\]))|(?:,(?=[^\\(]*\\))))*))\\)|)",
                pos = ":(nth|eq|gt|lt|first|last|even|odd)(?:\\((\\d*)\\)|)(?=[^-]|$)",
                combinators = whitespace + "*([\\x20\\t\\r\\n\\f>+~])" + whitespace + "*",
                groups = "(?=[^\\x20\\t\\r\\n\\f])(?:\\\\.|" + attributes + "|" + pseudos.replace(2, 7) + "|[^\\\\(),])+",
                rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"),
                rcombinators = new RegExp("^" + combinators),
                rgroups = new RegExp(groups + "?(?=" + whitespace + "*,|$)", "g"),
                rselector = new RegExp("^(?:(?!,)(?:(?:^|,)" + whitespace + "*" + groups + ")*?|" + whitespace + "*(.*?))(\\)|$)"),
                rtokens = new RegExp(groups.slice(19, -6) + "\\x20\\t\\r\\n\\f>+~])+|" + combinators, "g"),
                rquickExpr = /^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/,
                rsibling = /[\x20\t\r\n\f]*[+~]/,
                rendsWithNot = /:not\($/,
                rheader = /h\d/i,
                rinputs = /input|select|textarea|button/i,
                rbackslash = /\\(?!\\)/g,
                matchExpr = { ID: new RegExp("^#(" + characterEncoding + ")"), CLASS: new RegExp("^\\.(" + characterEncoding + ")"), NAME: new RegExp("^\\[name=['\"]?(" + characterEncoding + ")['\"]?\\]"), TAG: new RegExp("^(" + characterEncoding.replace("[-", "[-\\*") + ")"), ATTR: new RegExp("^" + attributes), PSEUDO: new RegExp("^" + pseudos), CHILD: new RegExp("^:(only|nth|last|first)-child(?:\\(" + whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i"), POS: new RegExp(pos, "ig"), needsContext: new RegExp("^" + whitespace + "*[>+~]|" + pos, "i") },
                classCache = {},
                cachedClasses = [],
                compilerCache = {},
                cachedSelectors = [],
                markFunction = function(fn) {
                    return fn.sizzleFilter = !0, fn },
                createInputFunction = function(type) {
                    return function(elem) {
                        return "input" === elem.nodeName.toLowerCase() && elem.type === type } },
                createButtonFunction = function(type) {
                    return function(elem) {
                        var name = elem.nodeName.toLowerCase();
                        return ("input" === name || "button" === name) && elem.type === type } },
                assert = function(fn) {
                    var pass = !1,
                        div = document.createElement("div");
                    try { pass = fn(div) } catch (e) {}
                    return div = null, pass },
                assertAttributes = assert(function(div) { div.innerHTML = "<select></select>";
                    var type = typeof div.lastChild.getAttribute("multiple");
                    return "boolean" !== type && "string" !== type }),
                assertUsableName = assert(function(div) { div.id = expando + 0, div.innerHTML = "<a name='" + expando + "'></a><div name='" + expando + "'></div>", docElem.insertBefore(div, docElem.firstChild);
                    var pass = document.getElementsByName && document.getElementsByName(expando).length === 2 + document.getElementsByName(expando + 0).length;
                    return assertGetIdNotName = !document.getElementById(expando), docElem.removeChild(div), pass }),
                assertTagNameNoComments = assert(function(div) {
                    return div.appendChild(document.createComment("")), 0 === div.getElementsByTagName("*").length }),
                assertHrefNotNormalized = assert(function(div) {
                    return div.innerHTML = "<a href='#'></a>", div.firstChild && typeof div.firstChild.getAttribute !== strundefined && "#" === div.firstChild.getAttribute("href") }),
                assertUsableClassName = assert(function(div) {
                    return div.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>", div.getElementsByClassName && 0 !== div.getElementsByClassName("e").length ? (div.lastChild.className = "e", 1 !== div.getElementsByClassName("e").length) : !1 }),
                Sizzle = function(selector, context, results, seed) { results = results || [], context = context || document;
                    var match, elem, xml, m, nodeType = context.nodeType;
                    if (1 !== nodeType && 9 !== nodeType) return [];
                    if (!selector || "string" != typeof selector) return results;
                    if (xml = isXML(context), !xml && !seed && (match = rquickExpr.exec(selector)))
                        if (m = match[1]) {
                            if (9 === nodeType) {
                                if (elem = context.getElementById(m), !elem || !elem.parentNode) return results;
                                if (elem.id === m) return results.push(elem), results } else if (context.ownerDocument && (elem = context.ownerDocument.getElementById(m)) && contains(context, elem) && elem.id === m) return results.push(elem), results } else {
                            if (match[2]) return push.apply(results, slice.call(context.getElementsByTagName(selector), 0)), results;
                            if ((m = match[3]) && assertUsableClassName && context.getElementsByClassName) return push.apply(results, slice.call(context.getElementsByClassName(m), 0)), results }
                    return select(selector, context, results, seed, xml) },
                Expr = Sizzle.selectors = { cacheLength: 50, match: matchExpr, order: ["ID", "TAG"], attrHandle: {}, createPseudo: markFunction, find: { ID: assertGetIdNotName ? function(id, context, xml) {
                            if (typeof context.getElementById !== strundefined && !xml) {
                                var m = context.getElementById(id);
                                return m && m.parentNode ? [m] : [] } } : function(id, context, xml) {
                            if (typeof context.getElementById !== strundefined && !xml) {
                                var m = context.getElementById(id);
                                return m ? m.id === id || typeof m.getAttributeNode !== strundefined && m.getAttributeNode("id").value === id ? [m] : undefined : [] } }, TAG: assertTagNameNoComments ? function(tag, context) {
                            return typeof context.getElementsByTagName !== strundefined ? context.getElementsByTagName(tag) : void 0 } : function(tag, context) {
                            var results = context.getElementsByTagName(tag);
                            if ("*" === tag) {
                                for (var elem, tmp = [], i = 0; elem = results[i]; i++) 1 === elem.nodeType && tmp.push(elem);
                                return tmp }
                            return results } }, relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } }, preFilter: { ATTR: function(match) {
                            return match[1] = match[1].replace(rbackslash, ""), match[3] = (match[4] || match[5] || "").replace(rbackslash, ""), "~=" === match[2] && (match[3] = " " + match[3] + " "), match.slice(0, 4) }, CHILD: function(match) {
                            return match[1] = match[1].toLowerCase(), "nth" === match[1] ? (match[2] || Sizzle.error(match[0]), match[3] = +(match[3] ? match[4] + (match[5] || 1) : 2 * ("even" === match[2] || "odd" === match[2])), match[4] = +(match[6] + match[7] || "odd" === match[2])) : match[2] && Sizzle.error(match[0]), match }, PSEUDO: function(match) {
                            var argument, unquoted = match[4];
                            return matchExpr.CHILD.test(match[0]) ? null : (unquoted && (argument = rselector.exec(unquoted)) && argument.pop() && (match[0] = match[0].slice(0, argument[0].length - unquoted.length - 1), unquoted = argument[0].slice(0, -1)), match.splice(2, 3, unquoted || match[3]), match) } }, filter: { ID: assertGetIdNotName ? function(id) {
                            return id = id.replace(rbackslash, ""),
                                function(elem) {
                                    return elem.getAttribute("id") === id } } : function(id) {
                            return id = id.replace(rbackslash, ""),
                                function(elem) {
                                    var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode("id");
                                    return node && node.value === id } }, TAG: function(nodeName) {
                            return "*" === nodeName ? function() {
                                return !0 } : (nodeName = nodeName.replace(rbackslash, "").toLowerCase(), function(elem) {
                                return elem.nodeName && elem.nodeName.toLowerCase() === nodeName }) }, CLASS: function(className) {
                            var pattern = classCache[className];
                            return pattern || (pattern = classCache[className] = new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)"), cachedClasses.push(className), cachedClasses.length > Expr.cacheLength && delete classCache[cachedClasses.shift()]),
                                function(elem) {
                                    return pattern.test(elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute("class") || "") } }, ATTR: function(name, operator, check) {
                            return operator ? function(elem) {
                                var result = Sizzle.attr(elem, name),
                                    value = result + "";
                                if (null == result) return "!=" === operator;
                                switch (operator) {
                                    case "=":
                                        return value === check;
                                    case "!=":
                                        return value !== check;
                                    case "^=":
                                        return check && 0 === value.indexOf(check);
                                    case "*=":
                                        return check && value.indexOf(check) > -1;
                                    case "$=":
                                        return check && value.substr(value.length - check.length) === check;
                                    case "~=":
                                        return (" " + value + " ").indexOf(check) > -1;
                                    case "|=":
                                        return value === check || value.substr(0, check.length + 1) === check + "-" } } : function(elem) {
                                return null != Sizzle.attr(elem, name) } }, CHILD: function(type, argument, first, last) {
                            if ("nth" === type) {
                                var doneName = done++;
                                return function(elem) {
                                    var parent, diff, count = 0,
                                        node = elem;
                                    if (1 === first && 0 === last) return !0;
                                    if (parent = elem.parentNode, parent && (parent[expando] !== doneName || !elem.sizset)) {
                                        for (node = parent.firstChild; node && (1 !== node.nodeType || (node.sizset = ++count, node !== elem)); node = node.nextSibling);
                                        parent[expando] = doneName }
                                    return diff = elem.sizset - last, 0 === first ? 0 === diff : diff % first === 0 && diff / first >= 0 } }
                            return function(elem) {
                                var node = elem;
                                switch (type) {
                                    case "only":
                                    case "first":
                                        for (; node = node.previousSibling;)
                                            if (1 === node.nodeType) return !1;
                                        if ("first" === type) return !0;
                                        node = elem;
                                    case "last":
                                        for (; node = node.nextSibling;)
                                            if (1 === node.nodeType) return !1;
                                        return !0 } } }, PSEUDO: function(pseudo, argument, context, xml) {
                            var fn = Expr.pseudos[pseudo] || Expr.pseudos[pseudo.toLowerCase()];
                            return fn || Sizzle.error("unsupported pseudo: " + pseudo), fn.sizzleFilter ? fn(argument, context, xml) : fn } }, pseudos: { not: markFunction(function(selector, context, xml) {
                            var matcher = compile(selector.replace(rtrim, "$1"), context, xml);
                            return function(elem) {
                                return !matcher(elem) } }), enabled: function(elem) {
                            return elem.disabled === !1 }, disabled: function(elem) {
                            return elem.disabled === !0 }, checked: function(elem) {
                            var nodeName = elem.nodeName.toLowerCase();
                            return "input" === nodeName && !!elem.checked || "option" === nodeName && !!elem.selected }, selected: function(elem) {
                            return elem.parentNode && elem.parentNode.selectedIndex, elem.selected === !0 }, parent: function(elem) {
                            return !Expr.pseudos.empty(elem) }, empty: function(elem) {
                            var nodeType;
                            for (elem = elem.firstChild; elem;) {
                                if (elem.nodeName > "@" || 3 === (nodeType = elem.nodeType) || 4 === nodeType) return !1;
                                elem = elem.nextSibling }
                            return !0 }, contains: markFunction(function(text) {
                            return function(elem) {
                                return (elem.textContent || elem.innerText || getText(elem)).indexOf(text) > -1 } }), has: markFunction(function(selector) {
                            return function(elem) {
                                return Sizzle(selector, elem).length > 0 } }), header: function(elem) {
                            return rheader.test(elem.nodeName) }, text: function(elem) {
                            var type, attr;
                            return "input" === elem.nodeName.toLowerCase() && "text" === (type = elem.type) && (null == (attr = elem.getAttribute("type")) || attr.toLowerCase() === type) }, radio: createInputFunction("radio"), checkbox: createInputFunction("checkbox"), file: createInputFunction("file"), password: createInputFunction("password"), image: createInputFunction("image.php"), submit: createButtonFunction("submit"), reset: createButtonFunction("reset"), button: function(elem) {
                            var name = elem.nodeName.toLowerCase();
                            return "input" === name && "button" === elem.type || "button" === name }, input: function(elem) {
                            return rinputs.test(elem.nodeName) }, focus: function(elem) {
                            var doc = elem.ownerDocument;
                            return !(elem !== doc.activeElement || doc.hasFocus && !doc.hasFocus() || !elem.type && !elem.href) }, active: function(elem) {
                            return elem === elem.ownerDocument.activeElement } }, setFilters: { first: function(elements, argument, not) {
                            return not ? elements.slice(1) : [elements[0]] }, last: function(elements, argument, not) {
                            var elem = elements.pop();
                            return not ? elements : [elem] }, even: function(elements, argument, not) {
                            for (var results = [], i = not ? 1 : 0, len = elements.length; len > i; i += 2) results.push(elements[i]);
                            return results }, odd: function(elements, argument, not) {
                            for (var results = [], i = not ? 0 : 1, len = elements.length; len > i; i += 2) results.push(elements[i]);
                            return results }, lt: function(elements, argument, not) {
                            return not ? elements.slice(+argument) : elements.slice(0, +argument) }, gt: function(elements, argument, not) {
                            return not ? elements.slice(0, +argument + 1) : elements.slice(+argument + 1) }, eq: function(elements, argument, not) {
                            var elem = elements.splice(+argument, 1);
                            return not ? elements : elem } } };
            Expr.setFilters.nth = Expr.setFilters.eq, Expr.filters = Expr.pseudos, assertHrefNotNormalized || (Expr.attrHandle = { href: function(elem) {
                    return elem.getAttribute("href", 2) }, type: function(elem) {
                    return elem.getAttribute("type") } }), assertUsableName && (Expr.order.push("NAME"), Expr.find.NAME = function(name, context) {
                return typeof context.getElementsByName !== strundefined ? context.getElementsByName(name) : void 0 }), assertUsableClassName && (Expr.order.splice(1, 0, "CLASS"), Expr.find.CLASS = function(className, context, xml) {
                return typeof context.getElementsByClassName === strundefined || xml ? void 0 : context.getElementsByClassName(className) });
            try { slice.call(docElem.childNodes, 0)[0].nodeType } catch (e) { slice = function(i) {
                    for (var elem, results = []; elem = this[i]; i++) results.push(elem);
                    return results } }
            var isXML = Sizzle.isXML = function(elem) {
                    var documentElement = elem && (elem.ownerDocument || elem).documentElement;
                    return documentElement ? "HTML" !== documentElement.nodeName : !1 },
                contains = Sizzle.contains = docElem.compareDocumentPosition ? function(a, b) {
                    return !!(16 & a.compareDocumentPosition(b)) } : docElem.contains ? function(a, b) {
                    var adown = 9 === a.nodeType ? a.documentElement : a,
                        bup = b.parentNode;
                    return a === bup || !!(bup && 1 === bup.nodeType && adown.contains && adown.contains(bup)) } : function(a, b) {
                    for (; b = b.parentNode;)
                        if (b === a) return !0;
                    return !1 },
                getText = Sizzle.getText = function(elem) {
                    var node, ret = "",
                        i = 0,
                        nodeType = elem.nodeType;
                    if (nodeType) {
                        if (1 === nodeType || 9 === nodeType || 11 === nodeType) {
                            if ("string" == typeof elem.textContent) return elem.textContent;
                            for (elem = elem.firstChild; elem; elem = elem.nextSibling) ret += getText(elem) } else if (3 === nodeType || 4 === nodeType) return elem.nodeValue } else
                        for (; node = elem[i]; i++) ret += getText(node);
                    return ret };
            Sizzle.attr = function(elem, name) {
                var attr, xml = isXML(elem);
                return xml || (name = name.toLowerCase()), Expr.attrHandle[name] ? Expr.attrHandle[name](elem) : assertAttributes || xml ? elem.getAttribute(name) : (attr = elem.getAttributeNode(name), attr ? "boolean" == typeof elem[name] ? elem[name] ? name : null : attr.specified ? attr.value : null : null) }, Sizzle.error = function(msg) {
                throw new Error("Syntax error, unrecognized expression: " + msg) }, [0, 0].sort(function() {
                return baseHasDuplicate = 0 }), docElem.compareDocumentPosition ? sortOrder = function(a, b) {
                return a === b ? (hasDuplicate = !0, 0) : (a.compareDocumentPosition && b.compareDocumentPosition ? 4 & a.compareDocumentPosition(b) : a.compareDocumentPosition) ? -1 : 1 } : (sortOrder = function(a, b) {
                if (a === b) return hasDuplicate = !0, 0;
                if (a.sourceIndex && b.sourceIndex) return a.sourceIndex - b.sourceIndex;
                var al, bl, ap = [],
                    bp = [],
                    aup = a.parentNode,
                    bup = b.parentNode,
                    cur = aup;
                if (aup === bup) return siblingCheck(a, b);
                if (!aup) return -1;
                if (!bup) return 1;
                for (; cur;) ap.unshift(cur), cur = cur.parentNode;
                for (cur = bup; cur;) bp.unshift(cur), cur = cur.parentNode;
                al = ap.length, bl = bp.length;
                for (var i = 0; al > i && bl > i; i++)
                    if (ap[i] !== bp[i]) return siblingCheck(ap[i], bp[i]);
                return i === al ? siblingCheck(a, bp[i], -1) : siblingCheck(ap[i], b, 1) }, siblingCheck = function(a, b, ret) {
                if (a === b) return ret;
                for (var cur = a.nextSibling; cur;) {
                    if (cur === b) return -1;
                    cur = cur.nextSibling }
                return 1 }), Sizzle.uniqueSort = function(results) {
                var elem, i = 1;
                if (sortOrder && (hasDuplicate = baseHasDuplicate, results.sort(sortOrder), hasDuplicate))
                    for (; elem = results[i]; i++) elem === results[i - 1] && results.splice(i--, 1);
                return results };
            var compile = Sizzle.compile = function(selector, context, xml) {
                var tokens, group, i, cached = compilerCache[selector];
                if (cached && cached.context === context) return cached;
                for (group = tokenize(selector, context, xml), i = 0; tokens = group[i]; i++) group[i] = matcherFromTokens(tokens, context, xml);
                return cached = compilerCache[selector] = matcherFromGroupMatchers(group), cached.context = context, cached.runs = cached.dirruns = 0, cachedSelectors.push(selector), cachedSelectors.length > Expr.cacheLength && delete compilerCache[cachedSelectors.shift()], cached };
            Sizzle.matches = function(expr, elements) {
                return Sizzle(expr, null, null, elements) }, Sizzle.matchesSelector = function(elem, expr) {
                return Sizzle(expr, null, null, [elem]).length > 0 };
            var select = function(selector, context, results, seed, xml) { selector = selector.replace(rtrim, "$1");
                var elements, matcher, i, len, elem, token, type, findContext, notTokens, match = selector.match(rgroups),
                    tokens = selector.match(rtokens),
                    contextNodeType = context.nodeType;
                if (matchExpr.POS.test(selector)) return handlePOS(selector, context, results, seed, match);
                if (seed) elements = slice.call(seed, 0);
                else if (match && 1 === match.length) {
                    if (tokens.length > 1 && 9 === contextNodeType && !xml && (match = matchExpr.ID.exec(tokens[0]))) {
                        if (context = Expr.find.ID(match[1], context, xml)[0], !context) return results;
                        selector = selector.slice(tokens.shift().length) }
                    for (findContext = (match = rsibling.exec(tokens[0])) && !match.index && context.parentNode || context, notTokens = tokens.pop(), token = notTokens.split(":not")[0], i = 0, len = Expr.order.length; len > i; i++)
                        if (type = Expr.order[i], match = matchExpr[type].exec(token)) {
                            if (elements = Expr.find[type]((match[1] || "").replace(rbackslash, ""), findContext, xml), null == elements) continue;
                            token === notTokens && (selector = selector.slice(0, selector.length - notTokens.length) + token.replace(matchExpr[type], ""), selector || push.apply(results, slice.call(elements, 0)));
                            break } }
                if (selector)
                    for (matcher = compile(selector, context, xml), dirruns = matcher.dirruns++, null == elements && (elements = Expr.find.TAG("*", rsibling.test(selector) && context.parentNode || context)), i = 0; elem = elements[i]; i++) cachedruns = matcher.runs++, matcher(elem, context) && results.push(elem);
                return results };
            document.querySelectorAll && ! function() {
                var disconnectedMatch, oldSelect = select,
                    rescape = /'|\\/g,
                    rattributeQuotes = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
                    rbuggyQSA = [],
                    rbuggyMatches = [":active"],
                    matches = docElem.matchesSelector || docElem.mozMatchesSelector || docElem.webkitMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector;
                assert(function(div) { div.innerHTML = "<select><option selected></option></select>", div.querySelectorAll("[selected]").length || rbuggyQSA.push("\\[" + whitespace + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"), div.querySelectorAll(":checked").length || rbuggyQSA.push(":checked") }), assert(function(div) { div.innerHTML = "<p test=''></p>", div.querySelectorAll("[test^='']").length && rbuggyQSA.push("[*^$]=" + whitespace + "*(?:\"\"|'')"), div.innerHTML = "<input type='hidden'>", div.querySelectorAll(":enabled").length || rbuggyQSA.push(":enabled", ":disabled") }), rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join("|")), select = function(selector, context, results, seed, xml) {
                    if (!(seed || xml || rbuggyQSA && rbuggyQSA.test(selector)))
                        if (9 === context.nodeType) try {
                            return push.apply(results, slice.call(context.querySelectorAll(selector), 0)), results } catch (qsaError) {} else if (1 === context.nodeType && "object" !== context.nodeName.toLowerCase()) {
                            var old = context.getAttribute("id"),
                                nid = old || expando,
                                newContext = rsibling.test(selector) && context.parentNode || context;
                            old ? nid = nid.replace(rescape, "\\$&") : context.setAttribute("id", nid);
                            try {
                                return push.apply(results, slice.call(newContext.querySelectorAll(selector.replace(rgroups, "[id='" + nid + "'] $&")), 0)), results } catch (qsaError) {} finally { old || context.removeAttribute("id") } }
                    return oldSelect(selector, context, results, seed, xml) }, matches && (assert(function(div) { disconnectedMatch = matches.call(div, "div");
                    try { matches.call(div, "[test!='']:sizzle"), rbuggyMatches.push(Expr.match.PSEUDO) } catch (e) {} }), rbuggyMatches = new RegExp(rbuggyMatches.join("|")), Sizzle.matchesSelector = function(elem, expr) {
                    if (expr = expr.replace(rattributeQuotes, "='$1']"), !(isXML(elem) || rbuggyMatches.test(expr) || rbuggyQSA && rbuggyQSA.test(expr))) try {
                        var ret = matches.call(elem, expr);
                        if (ret || disconnectedMatch || elem.document && 11 !== elem.document.nodeType) return ret } catch (e) {}
                    return Sizzle(expr, null, null, [elem]).length > 0 }) }(), Sizzle.attr = jQuery.attr, jQuery.find = Sizzle, jQuery.expr = Sizzle.selectors, jQuery.expr[":"] = jQuery.expr.pseudos, jQuery.unique = Sizzle.uniqueSort, jQuery.text = Sizzle.getText, jQuery.isXMLDoc = Sizzle.isXML, jQuery.contains = Sizzle.contains }(window);
    var runtil = /Until$/,
        rparentsprev = /^(?:parents|prev(?:Until|All))/,
        isSimple = /^.[^:#\[\.,]*$/,
        rneedsContext = jQuery.expr.match.needsContext,
        guaranteedUnique = { children: !0, contents: !0, next: !0, prev: !0 };
    jQuery.fn.extend({ find: function(selector) {
            var i, l, length, n, r, ret, self = this;
            if ("string" != typeof selector) return jQuery(selector).filter(function() {
                for (i = 0, l = self.length; l > i; i++)
                    if (jQuery.contains(self[i], this)) return !0 });
            for (ret = this.pushStack("", "find", selector), i = 0, l = this.length; l > i; i++)
                if (length = ret.length, jQuery.find(selector, this[i], ret), i > 0)
                    for (n = length; n < ret.length; n++)
                        for (r = 0; length > r; r++)
                            if (ret[r] === ret[n]) { ret.splice(n--, 1);
                                break }
            return ret }, has: function(target) {
            var i, targets = jQuery(target, this),
                len = targets.length;
            return this.filter(function() {
                for (i = 0; len > i; i++)
                    if (jQuery.contains(this, targets[i])) return !0 }) }, not: function(selector) {
            return this.pushStack(winnow(this, selector, !1), "not", selector) }, filter: function(selector) {
            return this.pushStack(winnow(this, selector, !0), "filter", selector) }, is: function(selector) {
            return !!selector && ("string" == typeof selector ? rneedsContext.test(selector) ? jQuery(selector, this.context).index(this[0]) >= 0 : jQuery.filter(selector, this).length > 0 : this.filter(selector).length > 0) }, closest: function(selectors, context) {
            for (var cur, i = 0, l = this.length, ret = [], pos = rneedsContext.test(selectors) || "string" != typeof selectors ? jQuery(selectors, context || this.context) : 0; l > i; i++)
                for (cur = this[i]; cur && cur.ownerDocument && cur !== context && 11 !== cur.nodeType;) {
                    if (pos ? pos.index(cur) > -1 : jQuery.find.matchesSelector(cur, selectors)) { ret.push(cur);
                        break }
                    cur = cur.parentNode }
            return ret = ret.length > 1 ? jQuery.unique(ret) : ret, this.pushStack(ret, "closest", selectors) }, index: function(elem) {
            return elem ? "string" == typeof elem ? jQuery.inArray(this[0], jQuery(elem)) : jQuery.inArray(elem.jquery ? elem[0] : elem, this) : this[0] && this[0].parentNode ? this.prevAll().length : -1 }, add: function(selector, context) {
            var set = "string" == typeof selector ? jQuery(selector, context) : jQuery.makeArray(selector && selector.nodeType ? [selector] : selector),
                all = jQuery.merge(this.get(), set);
            return this.pushStack(isDisconnected(set[0]) || isDisconnected(all[0]) ? all : jQuery.unique(all)) }, addBack: function(selector) {
            return this.add(null == selector ? this.prevObject : this.prevObject.filter(selector)) } }), jQuery.fn.andSelf = jQuery.fn.addBack, jQuery.each({ parent: function(elem) {
            var parent = elem.parentNode;
            return parent && 11 !== parent.nodeType ? parent : null }, parents: function(elem) {
            return jQuery.dir(elem, "parentNode") }, parentsUntil: function(elem, i, until) {
            return jQuery.dir(elem, "parentNode", until) }, next: function(elem) {
            return sibling(elem, "nextSibling") }, prev: function(elem) {
            return sibling(elem, "previousSibling") }, nextAll: function(elem) {
            return jQuery.dir(elem, "nextSibling") }, prevAll: function(elem) {
            return jQuery.dir(elem, "previousSibling") }, nextUntil: function(elem, i, until) {
            return jQuery.dir(elem, "nextSibling", until) }, prevUntil: function(elem, i, until) {
            return jQuery.dir(elem, "previousSibling", until) }, siblings: function(elem) {
            return jQuery.sibling((elem.parentNode || {}).firstChild, elem) }, children: function(elem) {
            return jQuery.sibling(elem.firstChild) }, contents: function(elem) {
            return jQuery.nodeName(elem, "iframe") ? elem.contentDocument || elem.contentWindow.document : jQuery.merge([], elem.childNodes) } }, function(name, fn) { jQuery.fn[name] = function(until, selector) {
            var ret = jQuery.map(this, fn, until);
            return runtil.test(name) || (selector = until), selector && "string" == typeof selector && (ret = jQuery.filter(selector, ret)), ret = this.length > 1 && !guaranteedUnique[name] ? jQuery.unique(ret) : ret, this.length > 1 && rparentsprev.test(name) && (ret = ret.reverse()), this.pushStack(ret, name, core_slice.call(arguments).join(",")) } }), jQuery.extend({ filter: function(expr, elems, not) {
            return not && (expr = ":not(" + expr + ")"), 1 === elems.length ? jQuery.find.matchesSelector(elems[0], expr) ? [elems[0]] : [] : jQuery.find.matches(expr, elems) }, dir: function(elem, dir, until) {
            for (var matched = [], cur = elem[dir]; cur && 9 !== cur.nodeType && (until === undefined || 1 !== cur.nodeType || !jQuery(cur).is(until));) 1 === cur.nodeType && matched.push(cur), cur = cur[dir];
            return matched }, sibling: function(n, elem) {
            for (var r = []; n; n = n.nextSibling) 1 === n.nodeType && n !== elem && r.push(n);
            return r } });
    var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        rinlinejQuery = / jQuery\d+="(?:null|\d+)"/g,
        rleadingWhitespace = /^\s+/,
        rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        rtagName = /<([\w:]+)/,
        rtbody = /<tbody/i,
        rhtml = /<|&#?\w+;/,
        rnoInnerhtml = /<(?:script|style|link)/i,
        rnocache = /<(?:script|object|embed|option|style)/i,
        rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
        rcheckableType = /^(?:checkbox|radio)$/,
        rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
        rscriptType = /\/(java|ecma)script/i,
        rcleanScript = /^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g,
        wrapMap = { option: [1, "<select multiple='multiple'>", "</select>"], legend: [1, "<fieldset>", "</fieldset>"], thead: [1, "<table>", "</table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"], area: [1, "<map>", "</map>"], _default: [0, "", ""] },
        safeFragment = createSafeFragment(document),
        fragmentDiv = safeFragment.appendChild(document.createElement("div"));
    wrapMap.optgroup = wrapMap.option, wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead, wrapMap.th = wrapMap.td, jQuery.support.htmlSerialize || (wrapMap._default = [1, "X<div>", "</div>"]), jQuery.fn.extend({
            text: function(value) {
                return jQuery.access(this, function(value) {
                    return value === undefined ? jQuery.text(this) : this.empty().append((this[0] && this[0].ownerDocument || document).createTextNode(value)) }, null, value, arguments.length) },
            wrapAll: function(html) {
                if (jQuery.isFunction(html)) return this.each(function(i) { jQuery(this).wrapAll(html.call(this, i)) });
                if (this[0]) {
                    var wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(!0);
                    this[0].parentNode && wrap.insertBefore(this[0]), wrap.map(function() {
                        for (var elem = this; elem.firstChild && 1 === elem.firstChild.nodeType;) elem = elem.firstChild;
                        return elem }).append(this) }
                return this },
            wrapInner: function(html) {
                return this.each(jQuery.isFunction(html) ? function(i) { jQuery(this).wrapInner(html.call(this, i)) } : function() {
                    var self = jQuery(this),
                        contents = self.contents();
                    contents.length ? contents.wrapAll(html) : self.append(html) }) },
            wrap: function(html) {
                var isFunction = jQuery.isFunction(html);
                return this.each(function(i) { jQuery(this).wrapAll(isFunction ? html.call(this, i) : html) }) },
            unwrap: function() {
                return this.parent().each(function() { jQuery.nodeName(this, "body") || jQuery(this).replaceWith(this.childNodes) }).end() },
            append: function() {
                return this.domManip(arguments, !0, function(elem) {
                    (1 === this.nodeType || 11 === this.nodeType) && this.appendChild(elem) }) },
            prepend: function() {
                return this.domManip(arguments, !0, function(elem) {
                    (1 === this.nodeType || 11 === this.nodeType) && this.insertBefore(elem, this.firstChild) }) },
            before: function() {
                if (!isDisconnected(this[0])) return this.domManip(arguments, !1, function(elem) { this.parentNode.insertBefore(elem, this) });
                if (arguments.length) {
                    var set = jQuery.clean(arguments);
                    return this.pushStack(jQuery.merge(set, this), "before", this.selector);

                }
            },
            after: function() {
                if (!isDisconnected(this[0])) return this.domManip(arguments, !1, function(elem) { this.parentNode.insertBefore(elem, this.nextSibling) });
                if (arguments.length) {
                    var set = jQuery.clean(arguments);
                    return this.pushStack(jQuery.merge(this, set), "after", this.selector) } },
            remove: function(selector, keepData) {
                for (var elem, i = 0; null != (elem = this[i]); i++)(!selector || jQuery.filter(selector, [elem]).length) && (keepData || 1 !== elem.nodeType || (jQuery.cleanData(elem.getElementsByTagName("*")), jQuery.cleanData([elem])), elem.parentNode && elem.parentNode.removeChild(elem));
                return this },
            empty: function() {
                for (var elem, i = 0; null != (elem = this[i]); i++)
                    for (1 === elem.nodeType && jQuery.cleanData(elem.getElementsByTagName("*")); elem.firstChild;) elem.removeChild(elem.firstChild);
                return this },
            clone: function(dataAndEvents, deepDataAndEvents) {
                return dataAndEvents = null == dataAndEvents ? !1 : dataAndEvents, deepDataAndEvents = null == deepDataAndEvents ? dataAndEvents : deepDataAndEvents, this.map(function() {
                    return jQuery.clone(this, dataAndEvents, deepDataAndEvents) }) },
            html: function(value) {
                return jQuery.access(this, function(value) {
                    var elem = this[0] || {},
                        i = 0,
                        l = this.length;
                    if (value === undefined) return 1 === elem.nodeType ? elem.innerHTML.replace(rinlinejQuery, "") : undefined;
                    if (!("string" != typeof value || rnoInnerhtml.test(value) || !jQuery.support.htmlSerialize && rnoshimcache.test(value) || !jQuery.support.leadingWhitespace && rleadingWhitespace.test(value) || wrapMap[(rtagName.exec(value) || ["", ""])[1].toLowerCase()])) { value = value.replace(rxhtmlTag, "<$1></$2>");
                        try {
                            for (; l > i; i++) elem = this[i] || {}, 1 === elem.nodeType && (jQuery.cleanData(elem.getElementsByTagName("*")), elem.innerHTML = value);
                            elem = 0 } catch (e) {} }
                    elem && this.empty().append(value) }, null, value, arguments.length) },
            replaceWith: function(value) {
                return isDisconnected(this[0]) ? this.length ? this.pushStack(jQuery(jQuery.isFunction(value) ? value() : value), "replaceWith", value) : this : jQuery.isFunction(value) ? this.each(function(i) {
                    var self = jQuery(this),
                        old = self.html();
                    self.replaceWith(value.call(this, i, old)) }) : ("string" != typeof value && (value = jQuery(value).detach()), this.each(function() {
                    var next = this.nextSibling,
                        parent = this.parentNode;
                    jQuery(this).remove(), next ? jQuery(next).before(value) : jQuery(parent).append(value) })) },
            detach: function(selector) {
                return this.remove(selector, !0) },
            domManip: function(args, table, callback) { args = [].concat.apply([], args);
                var results, first, fragment, iNoClone, i = 0,
                    value = args[0],
                    scripts = [],
                    l = this.length;
                if (!jQuery.support.checkClone && l > 1 && "string" == typeof value && rchecked.test(value)) return this.each(function() { jQuery(this).domManip(args, table, callback) });
                if (jQuery.isFunction(value)) return this.each(function(i) {
                    var self = jQuery(this);
                    args[0] = value.call(this, i, table ? self.html() : undefined), self.domManip(args, table, callback) });
                if (this[0]) {
                    if (results = jQuery.buildFragment(args, this, scripts), fragment = results.fragment, first = fragment.firstChild, 1 === fragment.childNodes.length && (fragment = first), first)
                        for (table = table && jQuery.nodeName(first, "tr"), iNoClone = results.cacheable || l - 1; l > i; i++) callback.call(table && jQuery.nodeName(this[i], "table") ? findOrAppend(this[i], "tbody") : this[i], i === iNoClone ? fragment : jQuery.clone(fragment, !0, !0));
                    fragment = first = null, scripts.length && jQuery.each(scripts, function(i, elem) { elem.src ? jQuery.ajax ? jQuery.ajax({ url: elem.src, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0 }) : jQuery.error("no ajax") : jQuery.globalEval((elem.text || elem.textContent || elem.innerHTML || "").replace(rcleanScript, "")), elem.parentNode && elem.parentNode.removeChild(elem) }) }
                return this }
        }), jQuery.buildFragment = function(args, context, scripts) {
            var fragment, cacheable, cachehit, first = args[0];
            return context = context || document, context = (context[0] || context).ownerDocument || context[0] || context, "undefined" == typeof context.createDocumentFragment && (context = document), !(1 === args.length && "string" == typeof first && first.length < 512 && context === document && "<" === first.charAt(0)) || rnocache.test(first) || !jQuery.support.checkClone && rchecked.test(first) || !jQuery.support.html5Clone && rnoshimcache.test(first) || (cacheable = !0, fragment = jQuery.fragments[first], cachehit = fragment !== undefined), fragment || (fragment = context.createDocumentFragment(), jQuery.clean(args, context, fragment, scripts), cacheable && (jQuery.fragments[first] = cachehit && fragment)), { fragment: fragment, cacheable: cacheable } }, jQuery.fragments = {}, jQuery.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function(name, original) { jQuery.fn[name] = function(selector) {
                var elems, i = 0,
                    ret = [],
                    insert = jQuery(selector),
                    l = insert.length,
                    parent = 1 === this.length && this[0].parentNode;
                if ((null == parent || parent && 11 === parent.nodeType && 1 === parent.childNodes.length) && 1 === l) return insert[original](this[0]), this;
                for (; l > i; i++) elems = (i > 0 ? this.clone(!0) : this).get(), jQuery(insert[i])[original](elems), ret = ret.concat(elems);
                return this.pushStack(ret, name, insert.selector) } }), jQuery.extend({ clone: function(elem, dataAndEvents, deepDataAndEvents) {
                var srcElements, destElements, i, clone;
                if (jQuery.support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test("<" + elem.nodeName + ">") ? clone = elem.cloneNode(!0) : (fragmentDiv.innerHTML = elem.outerHTML, fragmentDiv.removeChild(clone = fragmentDiv.firstChild)), !(jQuery.support.noCloneEvent && jQuery.support.noCloneChecked || 1 !== elem.nodeType && 11 !== elem.nodeType || jQuery.isXMLDoc(elem)))
                    for (cloneFixAttributes(elem, clone), srcElements = getAll(elem), destElements = getAll(clone), i = 0; srcElements[i]; ++i) destElements[i] && cloneFixAttributes(srcElements[i], destElements[i]);
                if (dataAndEvents && (cloneCopyEvent(elem, clone), deepDataAndEvents))
                    for (srcElements = getAll(elem), destElements = getAll(clone), i = 0; srcElements[i]; ++i) cloneCopyEvent(srcElements[i], destElements[i]);
                return srcElements = destElements = null, clone }, clean: function(elems, context, fragment, scripts) {
                var j, safe, elem, tag, wrap, depth, div, hasBody, tbody, handleScript, jsTags, i = 0,
                    ret = [];
                for (context && "undefined" != typeof context.createDocumentFragment || (context = document), safe = context === document && safeFragment; null != (elem = elems[i]); i++)
                    if ("number" == typeof elem && (elem += ""), elem) {
                        if ("string" == typeof elem)
                            if (rhtml.test(elem)) {
                                for (safe = safe || createSafeFragment(context), div = div || safe.appendChild(context.createElement("div")), elem = elem.replace(rxhtmlTag, "<$1></$2>"), tag = (rtagName.exec(elem) || ["", ""])[1].toLowerCase(), wrap = wrapMap[tag] || wrapMap._default, depth = wrap[0], div.innerHTML = wrap[1] + elem + wrap[2]; depth--;) div = div.lastChild;
                                if (!jQuery.support.tbody)
                                    for (hasBody = rtbody.test(elem), tbody = "table" !== tag || hasBody ? "<table>" !== wrap[1] || hasBody ? [] : div.childNodes : div.firstChild && div.firstChild.childNodes, j = tbody.length - 1; j >= 0; --j) jQuery.nodeName(tbody[j], "tbody") && !tbody[j].childNodes.length && tbody[j].parentNode.removeChild(tbody[j]);!jQuery.support.leadingWhitespace && rleadingWhitespace.test(elem) && div.insertBefore(context.createTextNode(rleadingWhitespace.exec(elem)[0]), div.firstChild), elem = div.childNodes, div = safe.lastChild } else elem = context.createTextNode(elem);
                        elem.nodeType ? ret.push(elem) : ret = jQuery.merge(ret, elem) }
                if (div && (safe.removeChild(div), elem = div = safe = null), !jQuery.support.appendChecked)
                    for (i = 0; null != (elem = ret[i]); i++) jQuery.nodeName(elem, "input") ? fixDefaultChecked(elem) : "undefined" != typeof elem.getElementsByTagName && jQuery.grep(elem.getElementsByTagName("input"), fixDefaultChecked);
                if (fragment)
                    for (handleScript = function(elem) {
                            return !elem.type || rscriptType.test(elem.type) ? scripts ? scripts.push(elem.parentNode ? elem.parentNode.removeChild(elem) : elem) : fragment.appendChild(elem) : void 0 }, i = 0; null != (elem = ret[i]); i++) jQuery.nodeName(elem, "script") && handleScript(elem) || (fragment.appendChild(elem), "undefined" != typeof elem.getElementsByTagName && (jsTags = jQuery.grep(jQuery.merge([], elem.getElementsByTagName("script")), handleScript), ret.splice.apply(ret, [i + 1, 0].concat(jsTags)), i += jsTags.length));
                return ret }, cleanData: function(elems, acceptData) {
                for (var data, id, elem, type, i = 0, internalKey = jQuery.expando, cache = jQuery.cache, deleteExpando = jQuery.support.deleteExpando, special = jQuery.event.special; null != (elem = elems[i]); i++)
                    if ((acceptData || jQuery.acceptData(elem)) && (id = elem[internalKey], data = id && cache[id])) {
                        if (data.events)
                            for (type in data.events) special[type] ? jQuery.event.remove(elem, type) : jQuery.removeEvent(elem, type, data.handle);
                        cache[id] && (delete cache[id], deleteExpando ? delete elem[internalKey] : elem.removeAttribute ? elem.removeAttribute(internalKey) : elem[internalKey] = null, jQuery.deletedIds.push(id)) } } }),
        function() {
            var matched, browser;
            jQuery.uaMatch = function(ua) { ua = ua.toLowerCase();
                var match = /(chrome)[ \/]([\w.]+)/.exec(ua) || /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) || /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];
                return { browser: match[1] || "", version: match[2] || "0" } }, matched = jQuery.uaMatch(navigator.userAgent), browser = {}, matched.browser && (browser[matched.browser] = !0, browser.version = matched.version), browser.webkit && (browser.safari = !0), jQuery.browser = browser, jQuery.sub = function() {
                function jQuerySub(selector, context) {
                    return new jQuerySub.fn.init(selector, context) }
                jQuery.extend(!0, jQuerySub, this), jQuerySub.superclass = this, jQuerySub.fn = jQuerySub.prototype = this(), jQuerySub.fn.constructor = jQuerySub, jQuerySub.sub = this.sub, jQuerySub.fn.init = function(selector, context) {
                    return context && context instanceof jQuery && !(context instanceof jQuerySub) && (context = jQuerySub(context)), jQuery.fn.init.call(this, selector, context, rootjQuerySub) }, jQuerySub.fn.init.prototype = jQuerySub.fn;
                var rootjQuerySub = jQuerySub(document);
                return jQuerySub } }();
    var curCSS, iframe, iframeDoc, ralpha = /alpha\([^)]*\)/i,
        ropacity = /opacity=([^)]*)/,
        rposition = /^(top|right|bottom|left)$/,
        rmargin = /^margin/,
        rnumsplit = new RegExp("^(" + core_pnum + ")(.*)$", "i"),
        rnumnonpx = new RegExp("^(" + core_pnum + ")(?!px)[a-z%]+$", "i"),
        rrelNum = new RegExp("^([-+])=(" + core_pnum + ")", "i"),
        elemdisplay = {},
        cssShow = { position: "absolute", visibility: "hidden", display: "block" },
        cssNormalTransform = { letterSpacing: 0, fontWeight: 400, lineHeight: 1 },
        cssExpand = ["Top", "Right", "Bottom", "Left"],
        cssPrefixes = ["Webkit", "O", "Moz", "ms"],
        eventsToggle = jQuery.fn.toggle;
    jQuery.fn.extend({ css: function(name, value) {
            return jQuery.access(this, function(elem, name, value) {
                return value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name) }, name, value, arguments.length > 1) }, show: function() {
            return showHide(this, !0) }, hide: function() {
            return showHide(this) }, toggle: function(state, fn2) {
            var bool = "boolean" == typeof state;
            return jQuery.isFunction(state) && jQuery.isFunction(fn2) ? eventsToggle.apply(this, arguments) : this.each(function() {
                (bool ? state : isHidden(this)) ? jQuery(this).show(): jQuery(this).hide() }) } }), jQuery.extend({ cssHooks: { opacity: { get: function(elem, computed) {
                    if (computed) {
                        var ret = curCSS(elem, "opacity");
                        return "" === ret ? "1" : ret } } } }, cssNumber: { fillOpacity: !0, fontWeight: !0, lineHeight: !0, opacity: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 }, cssProps: { "float": jQuery.support.cssFloat ? "cssFloat" : "styleFloat" }, style: function(elem, name, value, extra) {
            if (elem && 3 !== elem.nodeType && 8 !== elem.nodeType && elem.style) {
                var ret, type, hooks, origName = jQuery.camelCase(name),
                    style = elem.style;
                if (name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(style, origName)), hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName], value === undefined) return hooks && "get" in hooks && (ret = hooks.get(elem, !1, extra)) !== undefined ? ret : style[name];
                if (type = typeof value, "string" === type && (ret = rrelNum.exec(value)) && (value = (ret[1] + 1) * ret[2] + parseFloat(jQuery.css(elem, name)), type = "number"), !(null == value || "number" === type && isNaN(value) || ("number" !== type || jQuery.cssNumber[origName] || (value += "px"), hooks && "set" in hooks && (value = hooks.set(elem, value, extra)) === undefined))) try { style[name] = value } catch (e) {} } }, css: function(elem, name, numeric, extra) {
            var val, num, hooks, origName = jQuery.camelCase(name);
            return name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(elem.style, origName)), hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName], hooks && "get" in hooks && (val = hooks.get(elem, !0, extra)), val === undefined && (val = curCSS(elem, name)), "normal" === val && name in cssNormalTransform && (val = cssNormalTransform[name]), numeric || extra !== undefined ? (num = parseFloat(val), numeric || jQuery.isNumeric(num) ? num || 0 : val) : val }, swap: function(elem, options, callback) {
            var ret, name, old = {};
            for (name in options) old[name] = elem.style[name], elem.style[name] = options[name];
            ret = callback.call(elem);
            for (name in options) elem.style[name] = old[name];
            return ret } }), window.getComputedStyle ? curCSS = function(elem, name) {
        var ret, width, minWidth, maxWidth, computed = getComputedStyle(elem, null),
            style = elem.style;
        return computed && (ret = computed[name], "" !== ret || jQuery.contains(elem.ownerDocument.documentElement, elem) || (ret = jQuery.style(elem, name)), rnumnonpx.test(ret) && rmargin.test(name) && (width = style.width, minWidth = style.minWidth, maxWidth = style.maxWidth, style.minWidth = style.maxWidth = style.width = ret, ret = computed.width, style.width = width, style.minWidth = minWidth, style.maxWidth = maxWidth)), ret } : document.documentElement.currentStyle && (curCSS = function(elem, name) {
        var left, rsLeft, ret = elem.currentStyle && elem.currentStyle[name],
            style = elem.style;
        return null == ret && style && style[name] && (ret = style[name]), rnumnonpx.test(ret) && !rposition.test(name) && (left = style.left, rsLeft = elem.runtimeStyle && elem.runtimeStyle.left, rsLeft && (elem.runtimeStyle.left = elem.currentStyle.left), style.left = "fontSize" === name ? "1em" : ret, ret = style.pixelLeft + "px", style.left = left, rsLeft && (elem.runtimeStyle.left = rsLeft)), "" === ret ? "auto" : ret }), jQuery.each(["height", "width"], function(i, name) { jQuery.cssHooks[name] = { get: function(elem, computed, extra) {
                return computed ? 0 !== elem.offsetWidth || "none" !== curCSS(elem, "display") ? getWidthOrHeight(elem, name, extra) : jQuery.swap(elem, cssShow, function() {
                    return getWidthOrHeight(elem, name, extra) }) : void 0 }, set: function(elem, value, extra) {
                return setPositiveNumber(elem, value, extra ? augmentWidthOrHeight(elem, name, extra, jQuery.support.boxSizing && "border-box" === jQuery.css(elem, "boxSizing")) : 0) } } }), jQuery.support.opacity || (jQuery.cssHooks.opacity = { get: function(elem, computed) {
            return ropacity.test((computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : computed ? "1" : "" }, set: function(elem, value) {
            var style = elem.style,
                currentStyle = elem.currentStyle,
                opacity = jQuery.isNumeric(value) ? "alpha(opacity=" + 100 * value + ")" : "",
                filter = currentStyle && currentStyle.filter || style.filter || "";
            style.zoom = 1, value >= 1 && "" === jQuery.trim(filter.replace(ralpha, "")) && style.removeAttribute && (style.removeAttribute("filter"), currentStyle && !currentStyle.filter) || (style.filter = ralpha.test(filter) ? filter.replace(ralpha, opacity) : filter + " " + opacity) } }), jQuery(function() { jQuery.support.reliableMarginRight || (jQuery.cssHooks.marginRight = { get: function(elem, computed) {
                return jQuery.swap(elem, { display: "inline-block" }, function() {
                    return computed ? curCSS(elem, "marginRight") : void 0 }) } }), !jQuery.support.pixelPosition && jQuery.fn.position && jQuery.each(["top", "left"], function(i, prop) { jQuery.cssHooks[prop] = { get: function(elem, computed) {
                    if (computed) {
                        var ret = curCSS(elem, prop);
                        return rnumnonpx.test(ret) ? jQuery(elem).position()[prop] + "px" : ret } } } }) }), jQuery.expr && jQuery.expr.filters && (jQuery.expr.filters.hidden = function(elem) {
        return 0 === elem.offsetWidth && 0 === elem.offsetHeight || !jQuery.support.reliableHiddenOffsets && "none" === (elem.style && elem.style.display || curCSS(elem, "display")) }, jQuery.expr.filters.visible = function(elem) {
        return !jQuery.expr.filters.hidden(elem) }), jQuery.each({ margin: "", padding: "", border: "Width" }, function(prefix, suffix) { jQuery.cssHooks[prefix + suffix] = { expand: function(value) {
                var i, parts = "string" == typeof value ? value.split(" ") : [value],
                    expanded = {};
                for (i = 0; 4 > i; i++) expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
                return expanded } }, rmargin.test(prefix) || (jQuery.cssHooks[prefix + suffix].set = setPositiveNumber) });
    var r20 = /%20/g,
        rbracket = /\[\]$/,
        rCRLF = /\r?\n/g,
        rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
        rselectTextarea = /^(?:select|textarea)/i;
    jQuery.fn.extend({ serialize: function() {
            return jQuery.param(this.serializeArray()) }, serializeArray: function() {
            return this.map(function() {
                return this.elements ? jQuery.makeArray(this.elements) : this }).filter(function() {
                return this.name && !this.disabled && (this.checked || rselectTextarea.test(this.nodeName) || rinput.test(this.type)) }).map(function(i, elem) {
                var val = jQuery(this).val();
                return null == val ? null : jQuery.isArray(val) ? jQuery.map(val, function(val, i) {
                    return { name: elem.name, value: val.replace(rCRLF, "\r\n") } }) : { name: elem.name, value: val.replace(rCRLF, "\r\n") } }).get() } }), jQuery.param = function(a, traditional) {
        var prefix, s = [],
            add = function(key, value) { value = jQuery.isFunction(value) ? value() : null == value ? "" : value, s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value) };
        if (traditional === undefined && (traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional), jQuery.isArray(a) || a.jquery && !jQuery.isPlainObject(a)) jQuery.each(a, function() { add(this.name, this.value) });
        else
            for (prefix in a) buildParams(prefix, a[prefix], traditional, add);
        return s.join("&").replace(r20, "+") };
    var ajaxLocation, ajaxLocParts, rhash = /#.*$/,
        rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        rlocalProtocol = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
        rnoContent = /^(?:GET|HEAD)$/,
        rprotocol = /^\/\//,
        rquery = /\?/,
        rscript = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
        rts = /([?&])_=[^&]*/,
        rurl = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        _load = jQuery.fn.load,
        prefilters = {},
        transports = {},
        allTypes = ["*/"] + ["*"];
    try { ajaxLocation = location.href } catch (e) { ajaxLocation = document.createElement("a"), ajaxLocation.href = "", ajaxLocation = ajaxLocation.href }
    ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [], jQuery.fn.load = function(url, params, callback) {
        if ("string" != typeof url && _load) return _load.apply(this, arguments);
        if (!this.length) return this;
        var selector, type, response, self = this,
            off = url.indexOf(" ");
        return off >= 0 && (selector = url.slice(off, url.length), url = url.slice(0, off)), jQuery.isFunction(params) ? (callback = params, params = undefined) : "object" == typeof params && (type = "POST"), jQuery.ajax({ url: url, type: type, dataType: "html", data: params, complete: function(jqXHR, status) { callback && self.each(callback, response || [jqXHR.responseText, status, jqXHR]) } }).done(function(responseText) { response = arguments, self.html(selector ? jQuery("<div>").append(responseText.replace(rscript, "")).find(selector) : responseText) }), this }, jQuery.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function(i, o) { jQuery.fn[o] = function(f) {
            return this.on(o, f) } }), jQuery.each(["get", "post"], function(i, method) { jQuery[method] = function(url, data, callback, type) {
            return jQuery.isFunction(data) && (type = type || callback, callback = data, data = undefined), jQuery.ajax({ type: method, url: url, data: data, success: callback, dataType: type }) } }), jQuery.extend({ getScript: function(url, callback) {
            return jQuery.get(url, undefined, callback, "script") }, getJSON: function(url, data, callback) {
            return jQuery.get(url, data, callback, "json") }, ajaxSetup: function(target, settings) {
            return settings ? ajaxExtend(target, jQuery.ajaxSettings) : (settings = target, target = jQuery.ajaxSettings), ajaxExtend(target, settings), target }, ajaxSettings: { url: ajaxLocation, isLocal: rlocalProtocol.test(ajaxLocParts[1]), global: !0, type: "GET", contentType: "application/x-www-form-urlencoded; charset=UTF-8", processData: !0, async: !0, accepts: { xml: "application/xml, text/xml", html: "text/html", text: "text/plain", json: "application/json, text/javascript", "*": allTypes }, contents: { xml: /xml/, html: /html/, json: /json/ }, responseFields: { xml: "responseXML", text: "responseText" }, converters: { "* text": window.String, "text html": !0, "text json": jQuery.parseJSON, "text xml": jQuery.parseXML }, flatOptions: { context: !0, url: !0 } }, ajaxPrefilter: addToPrefiltersOrTransports(prefilters), ajaxTransport: addToPrefiltersOrTransports(transports), ajax: function(url, options) {
            function done(status, nativeStatusText, responses, headers) {
                var isSuccess, success, error, response, modified, statusText = nativeStatusText;
                2 !== state && (state = 2, timeoutTimer && clearTimeout(timeoutTimer), transport = undefined, responseHeadersString = headers || "", jqXHR.readyState = status > 0 ? 4 : 0, responses && (response = ajaxHandleResponses(s, jqXHR, responses)), status >= 200 && 300 > status || 304 === status ? (s.ifModified && (modified = jqXHR.getResponseHeader("Last-Modified"), modified && (jQuery.lastModified[ifModifiedKey] = modified), modified = jqXHR.getResponseHeader("Etag"), modified && (jQuery.etag[ifModifiedKey] = modified)), 304 === status ? (statusText = "notmodified", isSuccess = !0) : (isSuccess = ajaxConvert(s, response), statusText = isSuccess.state, success = isSuccess.data, error = isSuccess.error, isSuccess = !error)) : (error = statusText, (!statusText || status) && (statusText = "error", 0 > status && (status = 0))), jqXHR.status = status, jqXHR.statusText = "" + (nativeStatusText || statusText), isSuccess ? deferred.resolveWith(callbackContext, [success, statusText, jqXHR]) : deferred.rejectWith(callbackContext, [jqXHR, statusText, error]), jqXHR.statusCode(statusCode), statusCode = undefined, fireGlobals && globalEventContext.trigger("ajax" + (isSuccess ? "Success" : "Error"), [jqXHR, s, isSuccess ? success : error]), completeDeferred.fireWith(callbackContext, [jqXHR, statusText]), fireGlobals && (globalEventContext.trigger("ajaxComplete", [jqXHR, s]), --jQuery.active || jQuery.event.trigger("ajaxStop"))) } "object" == typeof url && (options = url, url = undefined), options = options || {};
            var ifModifiedKey, responseHeadersString, responseHeaders, transport, timeoutTimer, parts, fireGlobals, i, s = jQuery.ajaxSetup({}, options),
                callbackContext = s.context || s,
                globalEventContext = callbackContext !== s && (callbackContext.nodeType || callbackContext instanceof jQuery) ? jQuery(callbackContext) : jQuery.event,
                deferred = jQuery.Deferred(),
                completeDeferred = jQuery.Callbacks("once memory"),
                statusCode = s.statusCode || {},
                requestHeaders = {},
                requestHeadersNames = {},
                state = 0,
                strAbort = "canceled",
                jqXHR = { readyState: 0, setRequestHeader: function(name, value) {
                        if (!state) {
                            var lname = name.toLowerCase();
                            name = requestHeadersNames[lname] = requestHeadersNames[lname] || name, requestHeaders[name] = value }
                        return this }, getAllResponseHeaders: function() {
                        return 2 === state ? responseHeadersString : null }, getResponseHeader: function(key) {
                        var match;
                        if (2 === state) {
                            if (!responseHeaders)
                                for (responseHeaders = {}; match = rheaders.exec(responseHeadersString);) responseHeaders[match[1].toLowerCase()] = match[2];
                            match = responseHeaders[key.toLowerCase()] }
                        return match === undefined ? null : match }, overrideMimeType: function(type) {
                        return state || (s.mimeType = type), this }, abort: function(statusText) {
                        return statusText = statusText || strAbort, transport && transport.abort(statusText), done(0, statusText), this } };
            if (deferred.promise(jqXHR), jqXHR.success = jqXHR.done, jqXHR.error = jqXHR.fail, jqXHR.complete = completeDeferred.add, jqXHR.statusCode = function(map) {
                    if (map) {
                        var tmp;
                        if (2 > state)
                            for (tmp in map) statusCode[tmp] = [statusCode[tmp], map[tmp]];
                        else tmp = map[jqXHR.status], jqXHR.always(tmp) }
                    return this }, s.url = ((url || s.url) + "").replace(rhash, "").replace(rprotocol, ajaxLocParts[1] + "//"), s.dataTypes = jQuery.trim(s.dataType || "*").toLowerCase().split(core_rspace), null == s.crossDomain && (parts = rurl.exec(s.url.toLowerCase()), s.crossDomain = !(!parts || parts[1] == ajaxLocParts[1] && parts[2] == ajaxLocParts[2] && (parts[3] || ("http:" === parts[1] ? 80 : 443)) == (ajaxLocParts[3] || ("http:" === ajaxLocParts[1] ? 80 : 443)))), s.data && s.processData && "string" != typeof s.data && (s.data = jQuery.param(s.data, s.traditional)), inspectPrefiltersOrTransports(prefilters, s, options, jqXHR), 2 === state) return jqXHR;
            if (fireGlobals = s.global, s.type = s.type.toUpperCase(), s.hasContent = !rnoContent.test(s.type), fireGlobals && 0 === jQuery.active++ && jQuery.event.trigger("ajaxStart"), !s.hasContent && (s.data && (s.url += (rquery.test(s.url) ? "&" : "?") + s.data, delete s.data), ifModifiedKey = s.url, s.cache === !1)) {
                var ts = jQuery.now(),
                    ret = s.url.replace(rts, "$1_=" + ts);
                s.url = ret + (ret === s.url ? (rquery.test(s.url) ? "&" : "?") + "_=" + ts : "") }(s.data && s.hasContent && s.contentType !== !1 || options.contentType) && jqXHR.setRequestHeader("Content-Type", s.contentType), s.ifModified && (ifModifiedKey = ifModifiedKey || s.url, jQuery.lastModified[ifModifiedKey] && jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[ifModifiedKey]), jQuery.etag[ifModifiedKey] && jqXHR.setRequestHeader("If-None-Match", jQuery.etag[ifModifiedKey])), jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + ("*" !== s.dataTypes[0] ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]);
            for (i in s.headers) jqXHR.setRequestHeader(i, s.headers[i]);
            if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === !1 || 2 === state)) return jqXHR.abort();
            strAbort = "abort";
            for (i in { success: 1, error: 1, complete: 1 }) jqXHR[i](s[i]);
            if (transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR)) { jqXHR.readyState = 1, fireGlobals && globalEventContext.trigger("ajaxSend", [jqXHR, s]), s.async && s.timeout > 0 && (timeoutTimer = setTimeout(function() { jqXHR.abort("timeout") }, s.timeout));
                try { state = 1, transport.send(requestHeaders, done) } catch (e) {
                    if (!(2 > state)) throw e;
                    done(-1, e) } } else done(-1, "No Transport");
            return jqXHR }, active: 0, lastModified: {}, etag: {} });
    var oldCallbacks = [],
        rquestion = /\?/,
        rjsonp = /(=)\?(?=&|$)|\?\?/,
        nonce = jQuery.now();
    jQuery.ajaxSetup({ jsonp: "callback", jsonpCallback: function() {
            var callback = oldCallbacks.pop() || jQuery.expando + "_" + nonce++;
            return this[callback] = !0, callback } }), jQuery.ajaxPrefilter("json jsonp", function(s, originalSettings, jqXHR) {
        var callbackName, overwritten, responseContainer, data = s.data,
            url = s.url,
            hasCallback = s.jsonp !== !1,
            replaceInUrl = hasCallback && rjsonp.test(url),
            replaceInData = hasCallback && !replaceInUrl && "string" == typeof data && !(s.contentType || "").indexOf("application/x-www-form-urlencoded") && rjsonp.test(data);
        return "jsonp" === s.dataTypes[0] || replaceInUrl || replaceInData ? (callbackName = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback, overwritten = window[callbackName], replaceInUrl ? s.url = url.replace(rjsonp, "$1" + callbackName) : replaceInData ? s.data = data.replace(rjsonp, "$1" + callbackName) : hasCallback && (s.url += (rquestion.test(url) ? "&" : "?") + s.jsonp + "=" + callbackName), s.converters["script json"] = function() {
            return responseContainer || jQuery.error(callbackName + " was not called"), responseContainer[0] }, s.dataTypes[0] = "json", window[callbackName] = function() { responseContainer = arguments }, jqXHR.always(function() { window[callbackName] = overwritten, s[callbackName] && (s.jsonpCallback = originalSettings.jsonpCallback, oldCallbacks.push(callbackName)), responseContainer && jQuery.isFunction(overwritten) && overwritten(responseContainer[0]), responseContainer = overwritten = undefined }), "script") : void 0 }), jQuery.ajaxSetup({ accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" }, contents: { script: /javascript|ecmascript/ }, converters: { "text script": function(text) {
                return jQuery.globalEval(text), text } } }), jQuery.ajaxPrefilter("script", function(s) { s.cache === undefined && (s.cache = !1), s.crossDomain && (s.type = "GET", s.global = !1) }), jQuery.ajaxTransport("script", function(s) {
        if (s.crossDomain) {
            var script, head = document.head || document.getElementsByTagName("head")[0] || document.documentElement;
            return { send: function(_, callback) { script = document.createElement("script"), script.async = "async", s.scriptCharset && (script.charset = s.scriptCharset), script.src = s.url, script.onload = script.onreadystatechange = function(_, isAbort) {
                        (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) && (script.onload = script.onreadystatechange = null, head && script.parentNode && head.removeChild(script), script = undefined, isAbort || callback(200, "success")) }, head.insertBefore(script, head.firstChild) }, abort: function() { script && script.onload(0, 1) } } } });
    var xhrCallbacks, xhrOnUnloadAbort = window.ActiveXObject ? function() {
            for (var key in xhrCallbacks) xhrCallbacks[key](0, 1) } : !1,
        xhrId = 0;
    jQuery.ajaxSettings.xhr = window.ActiveXObject ? function() {
            return !this.isLocal && createStandardXHR() || createActiveXHR() } : createStandardXHR,
        function(xhr) { jQuery.extend(jQuery.support, { ajax: !!xhr, cors: !!xhr && "withCredentials" in xhr }) }(jQuery.ajaxSettings.xhr()), jQuery.support.ajax && jQuery.ajaxTransport(function(s) {
            if (!s.crossDomain || jQuery.support.cors) {
                var callback;
                return { send: function(headers, complete) {
                        var handle, i, xhr = s.xhr();
                        if (s.username ? xhr.open(s.type, s.url, s.async, s.username, s.password) : xhr.open(s.type, s.url, s.async), s.xhrFields)
                            for (i in s.xhrFields) xhr[i] = s.xhrFields[i];
                        s.mimeType && xhr.overrideMimeType && xhr.overrideMimeType(s.mimeType), s.crossDomain || headers["X-Requested-With"] || (headers["X-Requested-With"] = "XMLHttpRequest");
                        try {
                            for (i in headers) xhr.setRequestHeader(i, headers[i]) } catch (_) {}
                        xhr.send(s.hasContent && s.data || null), callback = function(_, isAbort) {
                            var status, statusText, responseHeaders, responses, xml;
                            try {
                                if (callback && (isAbort || 4 === xhr.readyState))
                                    if (callback = undefined, handle && (xhr.onreadystatechange = jQuery.noop, xhrOnUnloadAbort && delete xhrCallbacks[handle]), isAbort) 4 !== xhr.readyState && xhr.abort();
                                    else { status = xhr.status, responseHeaders = xhr.getAllResponseHeaders(), responses = {}, xml = xhr.responseXML, xml && xml.documentElement && (responses.xml = xml);
                                        try { responses.text = xhr.responseText } catch (_) {}
                                        try { statusText = xhr.statusText } catch (e) { statusText = "" }
                                        status || !s.isLocal || s.crossDomain ? 1223 === status && (status = 204) : status = responses.text ? 200 : 404 } } catch (firefoxAccessException) { isAbort || complete(-1, firefoxAccessException) }
                            responses && complete(status, statusText, responses, responseHeaders) }, s.async ? 4 === xhr.readyState ? setTimeout(callback, 0) : (handle = ++xhrId, xhrOnUnloadAbort && (xhrCallbacks || (xhrCallbacks = {}, jQuery(window).unload(xhrOnUnloadAbort)), xhrCallbacks[handle] = callback), xhr.onreadystatechange = callback) : callback() }, abort: function() { callback && callback(0, 1) } } } });
    var fxNow, timerId, rfxtypes = /^(?:toggle|show|hide)$/,
        rfxnum = new RegExp("^(?:([-+])=|)(" + core_pnum + ")([a-z%]*)$", "i"),
        rrun = /queueHooks$/,
        animationPrefilters = [defaultPrefilter],
        tweeners = { "*": [function(prop, value) {
                var end, unit, prevScale, tween = this.createTween(prop, value),
                    parts = rfxnum.exec(value),
                    target = tween.cur(),
                    start = +target || 0,
                    scale = 1;
                if (parts) {
                    if (end = +parts[2], unit = parts[3] || (jQuery.cssNumber[prop] ? "" : "px"), "px" !== unit && start) { start = jQuery.css(tween.elem, prop, !0) || end || 1;
                        do prevScale = scale = scale || ".5", start /= scale, jQuery.style(tween.elem, prop, start + unit), scale = tween.cur() / target; while (1 !== scale && scale !== prevScale) }
                    tween.unit = unit, tween.start = start, tween.end = parts[1] ? start + (parts[1] + 1) * end : end }
                return tween }] };
    jQuery.Animation = jQuery.extend(Animation, { tweener: function(props, callback) { jQuery.isFunction(props) ? (callback = props, props = ["*"]) : props = props.split(" ");
                for (var prop, index = 0, length = props.length; length > index; index++) prop = props[index], tweeners[prop] = tweeners[prop] || [], tweeners[prop].unshift(callback) }, prefilter: function(callback, prepend) { prepend ? animationPrefilters.unshift(callback) : animationPrefilters.push(callback) } }), jQuery.Tween = Tween, Tween.prototype = { constructor: Tween, init: function(elem, options, prop, end, easing, unit) { this.elem = elem, this.prop = prop, this.easing = easing || "swing", this.options = options, this.start = this.now = this.cur(), this.end = end, this.unit = unit || (jQuery.cssNumber[prop] ? "" : "px") }, cur: function() {
                var hooks = Tween.propHooks[this.prop];
                return hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this) }, run: function(percent) {
                var eased, hooks = Tween.propHooks[this.prop];
                return this.pos = eased = jQuery.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration), this.now = (this.end - this.start) * eased + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), hooks && hooks.set ? hooks.set(this) : Tween.propHooks._default.set(this), this } }, Tween.prototype.init.prototype = Tween.prototype, Tween.propHooks = { _default: { get: function(tween) {
                    var result;
                    return null == tween.elem[tween.prop] || tween.elem.style && null != tween.elem.style[tween.prop] ? (result = jQuery.css(tween.elem, tween.prop, !1, ""), result && "auto" !== result ? result : 0) : tween.elem[tween.prop] }, set: function(tween) { jQuery.fx.step[tween.prop] ? jQuery.fx.step[tween.prop](tween) : tween.elem.style && (null != tween.elem.style[jQuery.cssProps[tween.prop]] || jQuery.cssHooks[tween.prop]) ? jQuery.style(tween.elem, tween.prop, tween.now + tween.unit) : tween.elem[tween.prop] = tween.now } } },
        Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = { set: function(tween) { tween.elem.nodeType && tween.elem.parentNode && (tween.elem[tween.prop] = tween.now) } }, jQuery.each(["toggle", "show", "hide"], function(i, name) {
            var cssFn = jQuery.fn[name];
            jQuery.fn[name] = function(speed, easing, callback) {
                return null == speed || "boolean" == typeof speed || !i && jQuery.isFunction(speed) && jQuery.isFunction(easing) ? cssFn.apply(this, arguments) : this.animate(genFx(name, !0), speed, easing, callback) } }), jQuery.fn.extend({ fadeTo: function(speed, to, easing, callback) {
                return this.filter(isHidden).css("opacity", 0).show().end().animate({ opacity: to }, speed, easing, callback) }, animate: function(prop, speed, easing, callback) {
                var empty = jQuery.isEmptyObject(prop),
                    optall = jQuery.speed(speed, easing, callback),
                    doAnimation = function() {
                        var anim = Animation(this, jQuery.extend({}, prop), optall);
                        empty && anim.stop(!0) };
                return empty || optall.queue === !1 ? this.each(doAnimation) : this.queue(optall.queue, doAnimation) }, stop: function(type, clearQueue, gotoEnd) {
                var stopQueue = function(hooks) {
                    var stop = hooks.stop;
                    delete hooks.stop, stop(gotoEnd) };
                return "string" != typeof type && (gotoEnd = clearQueue, clearQueue = type, type = undefined), clearQueue && type !== !1 && this.queue(type || "fx", []), this.each(function() {
                    var dequeue = !0,
                        index = null != type && type + "queueHooks",
                        timers = jQuery.timers,
                        data = jQuery._data(this);
                    if (index) data[index] && data[index].stop && stopQueue(data[index]);
                    else
                        for (index in data) data[index] && data[index].stop && rrun.test(index) && stopQueue(data[index]);
                    for (index = timers.length; index--;) timers[index].elem !== this || null != type && timers[index].queue !== type || (timers[index].anim.stop(gotoEnd), dequeue = !1, timers.splice(index, 1));
                    (dequeue || !gotoEnd) && jQuery.dequeue(this, type) }) } }), jQuery.each({ slideDown: genFx("show"), slideUp: genFx("hide"), slideToggle: genFx("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function(name, props) { jQuery.fn[name] = function(speed, easing, callback) {
                return this.animate(props, speed, easing, callback) } }), jQuery.speed = function(speed, easing, fn) {
            var opt = speed && "object" == typeof speed ? jQuery.extend({}, speed) : { complete: fn || !fn && easing || jQuery.isFunction(speed) && speed, duration: speed, easing: fn && easing || easing && !jQuery.isFunction(easing) && easing };
            return opt.duration = jQuery.fx.off ? 0 : "number" == typeof opt.duration ? opt.duration : opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[opt.duration] : jQuery.fx.speeds._default, (null == opt.queue || opt.queue === !0) && (opt.queue = "fx"), opt.old = opt.complete, opt.complete = function() { jQuery.isFunction(opt.old) && opt.old.call(this), opt.queue && jQuery.dequeue(this, opt.queue) }, opt }, jQuery.easing = { linear: function(p) {
                return p }, swing: function(p) {
                return .5 - Math.cos(p * Math.PI) / 2 } }, jQuery.timers = [], jQuery.fx = Tween.prototype.init, jQuery.fx.tick = function() {
            for (var timer, timers = jQuery.timers, i = 0; i < timers.length; i++) timer = timers[i], timer() || timers[i] !== timer || timers.splice(i--, 1);
            timers.length || jQuery.fx.stop() }, jQuery.fx.timer = function(timer) { timer() && jQuery.timers.push(timer) && !timerId && (timerId = setInterval(jQuery.fx.tick, jQuery.fx.interval)) }, jQuery.fx.interval = 13, jQuery.fx.stop = function() { clearInterval(timerId), timerId = null }, jQuery.fx.speeds = { slow: 600, fast: 200, _default: 400 }, jQuery.fx.step = {}, jQuery.expr && jQuery.expr.filters && (jQuery.expr.filters.animated = function(elem) {
            return jQuery.grep(jQuery.timers, function(fn) {
                return elem === fn.elem }).length });
    var rroot = /^(?:body|html)$/i;
    jQuery.fn.offset = function(options) {
        if (arguments.length) return options === undefined ? this : this.each(function(i) { jQuery.offset.setOffset(this, options, i) });
        var box, docElem, body, win, clientTop, clientLeft, scrollTop, scrollLeft, top, left, elem = this[0],
            doc = elem && elem.ownerDocument;
        if (doc) return (body = doc.body) === elem ? jQuery.offset.bodyOffset(elem) : (docElem = doc.documentElement, jQuery.contains(docElem, elem) ? (box = elem.getBoundingClientRect(), win = getWindow(doc), clientTop = docElem.clientTop || body.clientTop || 0, clientLeft = docElem.clientLeft || body.clientLeft || 0, scrollTop = win.pageYOffset || docElem.scrollTop, scrollLeft = win.pageXOffset || docElem.scrollLeft, top = box.top + scrollTop - clientTop, left = box.left + scrollLeft - clientLeft, { top: top, left: left }) : { top: 0, left: 0 }) }, jQuery.offset = { bodyOffset: function(body) {
            var top = body.offsetTop,
                left = body.offsetLeft;
            return jQuery.support.doesNotIncludeMarginInBodyOffset && (top += parseFloat(jQuery.css(body, "marginTop")) || 0, left += parseFloat(jQuery.css(body, "marginLeft")) || 0), { top: top, left: left } }, setOffset: function(elem, options, i) {
            var position = jQuery.css(elem, "position"); "static" === position && (elem.style.position = "relative");
            var curTop, curLeft, curElem = jQuery(elem),
                curOffset = curElem.offset(),
                curCSSTop = jQuery.css(elem, "top"),
                curCSSLeft = jQuery.css(elem, "left"),
                calculatePosition = ("absolute" === position || "fixed" === position) && jQuery.inArray("auto", [curCSSTop, curCSSLeft]) > -1,
                props = {},
                curPosition = {};
            calculatePosition ? (curPosition = curElem.position(), curTop = curPosition.top, curLeft = curPosition.left) : (curTop = parseFloat(curCSSTop) || 0, curLeft = parseFloat(curCSSLeft) || 0), jQuery.isFunction(options) && (options = options.call(elem, i, curOffset)), null != options.top && (props.top = options.top - curOffset.top + curTop), null != options.left && (props.left = options.left - curOffset.left + curLeft), "using" in options ? options.using.call(elem, props) : curElem.css(props) } }, jQuery.fn.extend({ position: function() {
            if (this[0]) {
                var elem = this[0],
                    offsetParent = this.offsetParent(),
                    offset = this.offset(),
                    parentOffset = rroot.test(offsetParent[0].nodeName) ? { top: 0, left: 0 } : offsetParent.offset();
                return offset.top -= parseFloat(jQuery.css(elem, "marginTop")) || 0, offset.left -= parseFloat(jQuery.css(elem, "marginLeft")) || 0, parentOffset.top += parseFloat(jQuery.css(offsetParent[0], "borderTopWidth")) || 0, parentOffset.left += parseFloat(jQuery.css(offsetParent[0], "borderLeftWidth")) || 0, { top: offset.top - parentOffset.top, left: offset.left - parentOffset.left } } }, offsetParent: function() {
            return this.map(function() {
                for (var offsetParent = this.offsetParent || document.body; offsetParent && !rroot.test(offsetParent.nodeName) && "static" === jQuery.css(offsetParent, "position");) offsetParent = offsetParent.offsetParent;
                return offsetParent || document.body }) } }), jQuery.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function(method, prop) {
        var top = /Y/.test(prop);
        jQuery.fn[method] = function(val) {
            return jQuery.access(this, function(elem, method, val) {
                var win = getWindow(elem);
                return val === undefined ? win ? prop in win ? win[prop] : win.document.documentElement[method] : elem[method] : void(win ? win.scrollTo(top ? jQuery(win).scrollLeft() : val, top ? val : jQuery(win).scrollTop()) : elem[method] = val) }, method, val, arguments.length, null) } }), jQuery.each({ Height: "height", Width: "width" }, function(name, type) { jQuery.each({ padding: "inner" + name, content: type, "": "outer" + name }, function(defaultExtra, funcName) { jQuery.fn[funcName] = function(margin, value) {
                var chainable = arguments.length && (defaultExtra || "boolean" != typeof margin),
                    extra = defaultExtra || (margin === !0 || value === !0 ? "margin" : "border");
                return jQuery.access(this, function(elem, type, value) {
                    var doc;
                    return jQuery.isWindow(elem) ? elem.document.documentElement["client" + name] : 9 === elem.nodeType ? (doc = elem.documentElement, Math.max(elem.body["scroll" + name], doc["scroll" + name], elem.body["offset" + name], doc["offset" + name], doc["client" + name])) : value === undefined ? jQuery.css(elem, type, value, extra) : jQuery.style(elem, type, value, extra) }, type, chainable ? margin : undefined, chainable) } }) }), window.jQuery = window.$ = jQuery, "function" == typeof define && define.amd && define.amd.jQuery && define("jquery", [], function() {
        return jQuery })
}(window),
function(k, f) { "function" == typeof define && define.amd ? define(f) : "object" == typeof exports ? module.exports = f() : k.Blazy = f() }(this, function() {
    function k(b) { setTimeout(function() {
            var c = b._util;
            c.elements = w(b.options.selector), c.count = c.elements.length, c.destroyed && (c.destroyed = !1, b.options.container && h(b.options.container, function(a) { l(a, "scroll", c.validateT) }), l(window, "resize", c.saveViewportOffsetT), l(window, "resize", c.validateT), l(window, "scroll", c.validateT)), f(b) }, 1) }

    function f(b) {
        for (var c = b._util, a = 0; a < c.count; a++) {
            var d = c.elements[a],
                g = d.getBoundingClientRect();
            (g.right >= e.left && g.bottom >= e.top && g.left <= e.right && g.top <= e.bottom || n(d, b.options.successClass)) && (b.load(d), c.elements.splice(a, 1), c.count--, a--) }
        0 === c.count && b.destroy() }

    function q(b, c, a) {
        if (!n(b, a.successClass) && (c || a.loadInvisible || 0 < b.offsetWidth && 0 < b.offsetHeight))
            if (c = b.getAttribute(p) || b.getAttribute(a.src)) { c = c.split(a.separator);
                var d = c[r && 1 < c.length ? 1 : 0],
                    g = "img" === b.nodeName.toLowerCase();
                g || void 0 === b.src ? (c = new Image, c.onerror = function() { a.error && a.error(b, "invalid"), b.className = b.className + " " + a.errorClass }, c.onload = function() { g ? b.src = d : b.style.backgroundImage = 'url("' + d + '")', t(b, a) }, c.src = d) : (b.src = d, t(b, a)) } else a.error && a.error(b, "missing"), n(b, a.errorClass) || (b.className = b.className + " " + a.errorClass) }

    function t(b, c) { b.className = b.className + " " + c.successClass, c.success && c.success(b), h(c.breakpoints, function(a) { b.removeAttribute(a.src) }), b.removeAttribute(c.src) }

    function n(b, c) {
        return -1 !== (" " + b.className + " ").indexOf(" " + c + " ") }

    function w(b) {
        var c = [];
        b = document.querySelectorAll(b);
        for (var a = b.length; a--; c.unshift(b[a]));
        return c }

    function u(b) { e.bottom = (window.innerHeight || document.documentElement.clientHeight) + b, e.right = (window.innerWidth || document.documentElement.clientWidth) + b }

    function l(b, c, a) { b.attachEvent ? b.attachEvent && b.attachEvent("on" + c, a) : b.addEventListener(c, a, !1) }

    function m(b, c, a) { b.detachEvent ? b.detachEvent && b.detachEvent("on" + c, a) : b.removeEventListener(c, a, !1) }

    function h(b, c) {
        if (b && c)
            for (var a = b.length, d = 0; a > d && !1 !== c(b[d], d); d++); }

    function v(b, c, a) {
        var d = 0;
        return function() {
            var g = +new Date;
            c > g - d || (d = g, b.apply(a, arguments)) } }
    var p, e, r;
    return function(b) {
        if (!document.querySelectorAll) {
            var c = document.createStyleSheet();
            document.querySelectorAll = function(a, b, d, e, f) {
                for (f = document.all, b = [], a = a.replace(/\[for\b/gi, "[htmlFor").split(","), d = a.length; d--;) {
                    for (c.addRule(a[d], "k:v"), e = f.length; e--;) f[e].currentStyle.k && b.push(f[e]);
                    c.removeRule(0) }
                return b } }
        var a = this,
            d = a._util = {};
        d.elements = [], d.destroyed = !0, a.options = b || {}, a.options.error = a.options.error || !1, a.options.offset = a.options.offset || 100, a.options.success = a.options.success || !1, a.options.selector = a.options.selector || ".b-lazy", a.options.separator = a.options.separator || "|", a.options.container = a.options.container ? document.querySelectorAll(a.options.container) : !1, a.options.errorClass = a.options.errorClass || "b-error", a.options.breakpoints = a.options.breakpoints || !1, a.options.loadInvisible = a.options.loadInvisible || !1, a.options.successClass = a.options.successClass || "b-loaded", a.options.validateDelay = a.options.validateDelay || 25, a.options.saveViewportOffsetDelay = a.options.saveViewportOffsetDelay || 50, a.options.src = p = a.options.src || "data-src", r = 1 < window.devicePixelRatio, e = {}, e.top = 0 - a.options.offset, e.left = 0 - a.options.offset, a.revalidate = function() { k(this) }, a.load = function(a, b) {
            var c = this.options;
            void 0 === a.length ? q(a, b, c) : h(a, function(a) { q(a, b, c) }) }, a.destroy = function() {
            var a = this._util;
            this.options.container && h(this.options.container, function(b) { m(b, "scroll", a.validateT) }), m(window, "scroll", a.validateT), m(window, "resize", a.validateT), m(window, "resize", a.saveViewportOffsetT), a.count = 0, a.elements.length = 0, a.destroyed = !0 }, d.validateT = v(function() { f(a) }, a.options.validateDelay, a), d.saveViewportOffsetT = v(function() { u(a.options.offset) }, a.options.saveViewportOffsetDelay, a), u(a.options.offset), h(a.options.breakpoints, function(a) {
            return a.width >= window.screen.width ? (p = a.src, !1) : void 0 }), k(a) } }),
function($, window, document, undefined) {
    function Accordion(element, options) { this.element = element, this.options = $.extend({}, defaults, options), this._defaults = defaults, this._name = pluginName, this.init() }
    var pluginName = "accordion",
        defaults = { transitionSpeed: 300, transitionEasing: "ease", controlElement: "[data-control]", contentElement: "[data-content]", groupElement: "[data-accordion-group]", singleOpen: !0 };
    Accordion.prototype.init = function() {
        function debounce(func, threshold, execAsap) {
            var timeout;
            return function() {
                function delayed() { execAsap || func.apply(obj, args), timeout = null }
                var obj = this,
                    args = arguments;
                timeout ? clearTimeout(timeout) : execAsap && func.apply(obj, args), timeout = setTimeout(delayed, threshold || 100) } }

        function supportsTransitions() {
            var b = document.body || document.documentElement,
                s = b.style,
                p = "transition";
            if ("string" == typeof s[p]) return !0;
            var v = ["Moz", "webkit", "Webkit", "Khtml", "O", "ms"];
            p = "Transition";
            for (var i = 0; i < v.length; i++)
                if ("string" == typeof s[v[i] + p]) return !0;
            return !1 }

        function requestAnimFrame(cb) {
            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame ? requestAnimationFrame(cb) || webkitRequestAnimationFrame(cb) || mozRequestAnimationFrame(cb) : setTimeout(cb, 1e3 / 60) }

        function toggleTransition($el, remove) { $content.css(remove ? { "-webkit-transition": "", transition: "" } : { "-webkit-transition": "max-height " + opts.transitionSpeed + "ms " + opts.transitionEasing, transition: "max-height " + opts.transitionSpeed + "ms " + opts.transitionEasing }) }

        function calculateHeight($el) {
            var height = 0;
            $el.children().each(function() { height += $(this).outerHeight(!0) }), $el.data("oHeight", height) }

        function updateParentHeight($parentAccordion, $currentAccordion, qty, operation) {
            var $matched, $content = $parentAccordion.filter(".open").find("> [data-content]"),
                $childs = $content.find("[data-accordion].open > [data-content]");
            opts.singleOpen || ($childs = $childs.not($currentAccordion.siblings("[data-accordion].open").find("> [data-content]"))), $matched = $content.add($childs), $parentAccordion.hasClass("open") && $matched.each(function() {
                var currentHeight = $(this).data("oHeight");
                switch (operation) {
                    case "+":
                        $(this).data("oHeight", currentHeight + qty);
                        break;
                    case "-":
                        $(this).data("oHeight", currentHeight - qty);
                        break;
                    default:
                        throw "updateParentHeight method needs an operation" }
                $(this).css("max-height", $(this).data("oHeight")) }) }

        function refreshHeight($accordion) {
            if ($accordion.hasClass("open")) {
                var $content = $accordion.find("> [data-content]"),
                    $childs = $content.find("[data-accordion].open > [data-content]"),
                    $matched = $content.add($childs);
                calculateHeight($matched), $matched.css("max-height", $matched.data("oHeight")) } }

        function closeAccordion($accordion, $content) {
            if ($accordion.trigger("accordion.close"), CSStransitions) {
                if (accordionHasParent) {
                    var $parentAccordions = $accordion.parents("[data-accordion]");
                    updateParentHeight($parentAccordions, $accordion, $content.data("oHeight"), "-") }
                $content.css(closedCSS), $accordion.removeClass("open") } else $content.css("max-height", $content.data("oHeight")), $content.animate(closedCSS, opts.transitionSpeed), $accordion.removeClass("open") }

        function openAccordion($accordion, $content) {
            if ($accordion.trigger("accordion.open"), CSStransitions) {
                if (toggleTransition($content), accordionHasParent) {
                    var $parentAccordions = $accordion.parents("[data-accordion]");
                    updateParentHeight($parentAccordions, $accordion, $content.data("oHeight"), "+") }
                requestAnimFrame(function() { $content.css("max-height", $content.data("oHeight")) }), $accordion.addClass("open") } else $content.animate({ "max-height": $content.data("oHeight") }, opts.transitionSpeed, function() { $content.css({ "max-height": "none" }) }), $accordion.addClass("open") }

        function closeSiblingAccordions($accordion) {
            var $siblings = ($accordion.closest(opts.groupElement), $accordion.siblings("[data-accordion]").filter(".open")),
                $siblingsChildren = $siblings.find("[data-accordion]").filter(".open"),
                $otherAccordions = $siblings.add($siblingsChildren);
            $otherAccordions.each(function() {
                var $accordion = $(this),
                    $content = $accordion.find(opts.contentElement);
                closeAccordion($accordion, $content) }), $otherAccordions.removeClass("open") }

        function toggleAccordion() {
            var isAccordionGroup = opts.singleOpen ? $accordion.parents(opts.groupElement).length > 0 : !1;
            calculateHeight($content), isAccordionGroup && closeSiblingAccordions($accordion), $accordion.hasClass("open") ? closeAccordion($accordion, $content) : openAccordion($accordion, $content) }

        function addEventListeners() { $controls.on("click", toggleAccordion), $controls.on("accordion.toggle", function() {
                return opts.singleOpen && $controls.length > 1 ? !1 : void toggleAccordion() }), $(window).on("resize", debounce(function() { refreshHeight($accordion) })) }

        function setup() { $content.each(function() {
                var $curr = $(this);
                0 != $curr.css("max-height") && ($curr.closest("[data-accordion]").hasClass("open") ? (toggleTransition($curr), calculateHeight($curr), $curr.css("max-height", $curr.data("oHeight"))) : $curr.css({ "max-height": 0, overflow: "hidden" })) }), $accordion.attr("data-accordion") || ($accordion.attr("data-accordion", ""), $accordion.find(opts.controlElement).attr("data-control", ""), $accordion.find(opts.contentElement).attr("data-content", "")) }
        var self = this,
            opts = self.options,
            $accordion = $(self.element),
            $controls = $accordion.find("> " + opts.controlElement),
            $content = $accordion.find("> " + opts.contentElement),
            accordionParentsQty = $accordion.parents("[data-accordion]").length,
            accordionHasParent = accordionParentsQty > 0,
            closedCSS = { "max-height": 0, overflow: "hidden" },
            CSStransitions = supportsTransitions();
        setup(), addEventListeners() }, $.fn[pluginName] = function(options) {
        return this.each(function() { $.data(this, "plugin_" + pluginName) || $.data(this, "plugin_" + pluginName, new Accordion(this, options)) }) } }(jQuery, window, document),
function(window, undefined) { "$:nomunge";
    var jq_throttle, $ = window.jQuery || window.Cowboy || (window.Cowboy = {});
    $.throttle = jq_throttle = function(delay, no_trailing, callback, debounce_mode) {
        function wrapper() {
            function exec() { last_exec = +new Date, callback.apply(that, args) }

            function clear() { timeout_id = undefined }
            var that = this,
                elapsed = +new Date - last_exec,
                args = arguments;
            debounce_mode && !timeout_id && exec(), timeout_id && clearTimeout(timeout_id), debounce_mode === undefined && elapsed > delay ? exec() : no_trailing !== !0 && (timeout_id = setTimeout(debounce_mode ? clear : exec, debounce_mode === undefined ? delay - elapsed : delay)) }
        var timeout_id, last_exec = 0;
        return "boolean" != typeof no_trailing && (debounce_mode = callback, callback = no_trailing, no_trailing = undefined), $.guid && (wrapper.guid = callback.guid = callback.guid || $.guid++), wrapper }, $.debounce = function(delay, at_begin, callback) {
        return callback === undefined ? jq_throttle(delay, at_begin, !1) : jq_throttle(delay, callback, at_begin !== !1) } }(this), jQuery.easing.jswing = jQuery.easing.swing, jQuery.extend(jQuery.easing, { def: "easeOutQuad", swing: function(x, t, b, c, d) {
            return jQuery.easing[jQuery.easing.def](x, t, b, c, d) }, easeInQuad: function(x, t, b, c, d) {
            return c * (t /= d) * t + b }, easeOutQuad: function(x, t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b }, easeInOutQuad: function(x, t, b, c, d) {
            return (t /= d / 2) < 1 ? c / 2 * t * t + b : -c / 2 * (--t * (t - 2) - 1) + b }, easeInCubic: function(x, t, b, c, d) {
            return c * (t /= d) * t * t + b }, easeOutCubic: function(x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t + 1) + b }, easeInOutCubic: function(x, t, b, c, d) {
            return (t /= d / 2) < 1 ? c / 2 * t * t * t + b : c / 2 * ((t -= 2) * t * t + 2) + b }, easeInQuart: function(x, t, b, c, d) {
            return c * (t /= d) * t * t * t + b }, easeOutQuart: function(x, t, b, c, d) {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b }, easeInOutQuart: function(x, t, b, c, d) {
            return (t /= d / 2) < 1 ? c / 2 * t * t * t * t + b : -c / 2 * ((t -= 2) * t * t * t - 2) + b }, easeInQuint: function(x, t, b, c, d) {
            return c * (t /= d) * t * t * t * t + b }, easeOutQuint: function(x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t * t * t + 1) + b }, easeInOutQuint: function(x, t, b, c, d) {
            return (t /= d / 2) < 1 ? c / 2 * t * t * t * t * t + b : c / 2 * ((t -= 2) * t * t * t * t + 2) + b }, easeInSine: function(x, t, b, c, d) {
            return -c * Math.cos(t / d * (Math.PI / 2)) + c + b }, easeOutSine: function(x, t, b, c, d) {
            return c * Math.sin(t / d * (Math.PI / 2)) + b }, easeInOutSine: function(x, t, b, c, d) {
            return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b }, easeInExpo: function(x, t, b, c, d) {
            return 0 == t ? b : c * Math.pow(2, 10 * (t / d - 1)) + b }, easeOutExpo: function(x, t, b, c, d) {
            return t == d ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b }, easeInOutExpo: function(x, t, b, c, d) {
            return 0 == t ? b : t == d ? b + c : (t /= d / 2) < 1 ? c / 2 * Math.pow(2, 10 * (t - 1)) + b : c / 2 * (-Math.pow(2, -10 * --t) + 2) + b }, easeInCirc: function(x, t, b, c, d) {
            return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b }, easeOutCirc: function(x, t, b, c, d) {
            return c * Math.sqrt(1 - (t = t / d - 1) * t) + b }, easeInOutCirc: function(x, t, b, c, d) {
            return (t /= d / 2) < 1 ? -c / 2 * (Math.sqrt(1 - t * t) - 1) + b : c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b }, easeInElastic: function(x, t, b, c, d) {
            var s = 1.70158,
                p = 0,
                a = c;
            if (0 == t) return b;
            if (1 == (t /= d)) return b + c;
            if (p || (p = .3 * d), a < Math.abs(c)) { a = c;
                var s = p / 4 } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t * d - s) * Math.PI / p)) + b }, easeOutElastic: function(x, t, b, c, d) {
            var s = 1.70158,
                p = 0,
                a = c;
            if (0 == t) return b;
            if (1 == (t /= d)) return b + c;
            if (p || (p = .3 * d), a < Math.abs(c)) { a = c;
                var s = p / 4 } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return a * Math.pow(2, -10 * t) * Math.sin(2 * (t * d - s) * Math.PI / p) + c + b }, easeInOutElastic: function(x, t, b, c, d) {
            var s = 1.70158,
                p = 0,
                a = c;
            if (0 == t) return b;
            if (2 == (t /= d / 2)) return b + c;
            if (p || (p = .3 * d * 1.5), a < Math.abs(c)) { a = c;
                var s = p / 4 } else var s = p / (2 * Math.PI) * Math.asin(c / a);
            return 1 > t ? -.5 * a * Math.pow(2, 10 * (t -= 1)) * Math.sin(2 * (t * d - s) * Math.PI / p) + b : a * Math.pow(2, -10 * (t -= 1)) * Math.sin(2 * (t * d - s) * Math.PI / p) * .5 + c + b }, easeInBack: function(x, t, b, c, d, s) {
            return void 0 == s && (s = 1.70158), c * (t /= d) * t * ((s + 1) * t - s) + b }, easeOutBack: function(x, t, b, c, d, s) {
            return void 0 == s && (s = 1.70158), c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b }, easeInOutBack: function(x, t, b, c, d, s) {
            return void 0 == s && (s = 1.70158), (t /= d / 2) < 1 ? c / 2 * t * t * (((s *= 1.525) + 1) * t - s) + b : c / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b }, easeInBounce: function(x, t, b, c, d) {
            return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b }, easeOutBounce: function(x, t, b, c, d) {
            return (t /= d) < 1 / 2.75 ? 7.5625 * c * t * t + b : 2 / 2.75 > t ? c * (7.5625 * (t -= 1.5 / 2.75) * t + .75) + b : 2.5 / 2.75 > t ? c * (7.5625 * (t -= 2.25 / 2.75) * t + .9375) + b : c * (7.5625 * (t -= 2.625 / 2.75) * t + .984375) + b }, easeInOutBounce: function(x, t, b, c, d) {
            return d / 2 > t ? .5 * jQuery.easing.easeInBounce(x, 2 * t, 0, c, d) + b : .5 * jQuery.easing.easeOutBounce(x, 2 * t - d, 0, c, d) + .5 * c + b } }),
    function($) {
        $.easytabs = function(container, options) {
            var $defaultTab, $defaultTabLink, transitions, lastHash, skipUpdateToHash, settings, plugin = this,
                $container = $(container),
                defaults = { animate: !0, panelActiveClass: "active", tabActiveClass: "active", defaultTab: "li:first-child", animationSpeed: "normal", tabs: "> ul > li", updateHash: !0, cycle: !1, collapsible: !1, collapsedClass: "collapsed", collapsedByDefault: !0, uiTabs: !1, transitionIn: "fadeIn", transitionOut: "fadeOut", transitionInEasing: "swing", transitionOutEasing: "swing", transitionCollapse: "slideUp", transitionUncollapse: "slideDown", transitionCollapseEasing: "swing", transitionUncollapseEasing: "swing", containerClass: "", tabsClass: "", tabClass: "", panelClass: "", cache: !0, event: "click", panelContext: $container },
                animationSpeeds = { fast: 200, normal: 400, slow: 600 };
            plugin.init = function() { plugin.settings = settings = $.extend({}, defaults, options), settings.bind_str = settings.event + ".easytabs", settings.uiTabs && (settings.tabActiveClass = "ui-tabs-selected", settings.containerClass = "ui-tabs ui-widget ui-widget-content ui-corner-all", settings.tabsClass = "ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all", settings.tabClass = "ui-state-default ui-corner-top", settings.panelClass = "ui-tabs-panel ui-widget-content ui-corner-bottom"), settings.collapsible && void 0 !== options.defaultTab && void 0 === options.collpasedByDefault && (settings.collapsedByDefault = !1), "string" == typeof settings.animationSpeed && (settings.animationSpeed = animationSpeeds[settings.animationSpeed]), $("a.anchor").remove().prependTo("body"), $container.data("easytabs", {}), plugin.setTransitions(), plugin.getTabs(), addClasses(), setDefaultTab(), bindToTabClicks(), initHashChange(), initCycle(), $container.attr("data-easytabs", !0) }, plugin.setTransitions = function() { transitions = settings.animate ? { show: settings.transitionIn, hide: settings.transitionOut, speed: settings.animationSpeed, collapse: settings.transitionCollapse, uncollapse: settings.transitionUncollapse, halfSpeed: settings.animationSpeed / 2 } : { show: "show", hide: "hide", speed: 0, collapse: "hide", uncollapse: "show", halfSpeed: 0 } }, plugin.getTabs = function() {
                var $matchingPanel;
                plugin.tabs = $container.find(settings.tabs), plugin.panels = $(), plugin.tabs.each(function() {
                    var $tab = $(this),
                        $a = $tab.children("a"),
                        targetId = $tab.children("a").data("target");
                    $tab.data("easytabs", {}), void 0 !== targetId && null !== targetId ? $tab.data("easytabs").ajax = $a.attr("href") : targetId = $a.attr("href"), targetId = targetId.match(/#([^\?]+)/)[1], $matchingPanel = settings.panelContext.find("#" + targetId), $matchingPanel.length ? ($matchingPanel.data("easytabs", { position: $matchingPanel.css("position"), visibility: $matchingPanel.css("visibility") }), $matchingPanel.not(settings.panelActiveClass).hide(), plugin.panels = plugin.panels.add($matchingPanel), $tab.data("easytabs").panel = $matchingPanel) : (plugin.tabs = plugin.tabs.not($tab), "console" in window && console.warn("Warning: tab without matching panel for selector '#" + targetId + "' removed from set")) }) }, plugin.selectTab = function($clicked, callback) {
                var url = window.location,
                    $targetPanel = (url.hash.match(/^[^\?]*/)[0], $clicked.parent().data("easytabs").panel),
                    ajaxUrl = $clicked.parent().data("easytabs").ajax;
                settings.collapsible && !skipUpdateToHash && ($clicked.hasClass(settings.tabActiveClass) || $clicked.hasClass(settings.collapsedClass)) ? plugin.toggleTabCollapse($clicked, $targetPanel, ajaxUrl, callback) : $clicked.hasClass(settings.tabActiveClass) && $targetPanel.hasClass(settings.panelActiveClass) ? settings.cache || activateTab($clicked, $targetPanel, ajaxUrl, callback) : activateTab($clicked, $targetPanel, ajaxUrl, callback) }, plugin.toggleTabCollapse = function($clicked, $targetPanel, ajaxUrl, callback) { plugin.panels.stop(!0, !0), fire($container, "easytabs:before", [$clicked, $targetPanel, settings]) && (plugin.tabs.filter("." + settings.tabActiveClass).removeClass(settings.tabActiveClass).children().removeClass(settings.tabActiveClass), $clicked.hasClass(settings.collapsedClass) ? (!ajaxUrl || settings.cache && $clicked.parent().data("easytabs").cached || ($container.trigger("easytabs:ajax:beforeSend", [$clicked, $targetPanel]), $targetPanel.load(ajaxUrl, function(response, status, xhr) { $clicked.parent().data("easytabs").cached = !0, $container.trigger("easytabs:ajax:complete", [$clicked, $targetPanel, response, status, xhr]) })), $clicked.parent().removeClass(settings.collapsedClass).addClass(settings.tabActiveClass).children().removeClass(settings.collapsedClass).addClass(settings.tabActiveClass), $targetPanel.addClass(settings.panelActiveClass)[transitions.uncollapse](transitions.speed, settings.transitionUncollapseEasing, function() { $container.trigger("easytabs:midTransition", [$clicked, $targetPanel, settings]), "function" == typeof callback && callback() })) : ($clicked.addClass(settings.collapsedClass).parent().addClass(settings.collapsedClass), $targetPanel.removeClass(settings.panelActiveClass)[transitions.collapse](transitions.speed, settings.transitionCollapseEasing, function() { $container.trigger("easytabs:midTransition", [$clicked, $targetPanel, settings]), "function" == typeof callback && callback() }))) }, plugin.matchTab = function(hash) {
                return plugin.tabs.find("[href='" + hash + "'],[data-target='" + hash + "']").first() }, plugin.matchInPanel = function(hash) {
                return hash && plugin.validId(hash) ? plugin.panels.filter(":has(" + hash + ")").first() : [] }, plugin.validId = function(id) {
                return id.substr(1).match(/^[A-Za-z]+[A-Za-z0-9\-_:\.].$/) }, plugin.selectTabFromHashChange = function() {
                var $panel, hash = window.location.hash.match(/^[^\?]*/)[0],
                    $tab = plugin.matchTab(hash);
                settings.updateHash && ($tab.length ? (skipUpdateToHash = !0, plugin.selectTab($tab)) : ($panel = plugin.matchInPanel(hash), $panel.length ? (hash = "#" + $panel.attr("id"), $tab = plugin.matchTab(hash), skipUpdateToHash = !0, plugin.selectTab($tab)) : $defaultTab.hasClass(settings.tabActiveClass) || settings.cycle || ("" === hash || plugin.matchTab(lastHash).length || $container.closest(hash).length) && (skipUpdateToHash = !0, plugin.selectTab($defaultTabLink)))) }, plugin.cycleTabs = function(tabNumber) { settings.cycle && (tabNumber %= plugin.tabs.length, $tab = $(plugin.tabs[tabNumber]).children("a").first(), skipUpdateToHash = !0, plugin.selectTab($tab, function() { setTimeout(function() { plugin.cycleTabs(tabNumber + 1) }, settings.cycle) })) }, plugin.publicMethods = { select: function(tabSelector) {
                    var $tab;
                    0 === ($tab = plugin.tabs.filter(tabSelector)).length ? 0 === ($tab = plugin.tabs.find("a[href='" + tabSelector + "']")).length && 0 === ($tab = plugin.tabs.find("a" + tabSelector)).length && 0 === ($tab = plugin.tabs.find("[data-target='" + tabSelector + "']")).length && 0 === ($tab = plugin.tabs.find("a[href$='" + tabSelector + "']")).length && $.error("Tab '" + tabSelector + "' does not exist in tab set") : $tab = $tab.children("a").first(), plugin.selectTab($tab) } };
            var fire = function(obj, name, data) {
                    var event = $.Event(name);
                    return obj.trigger(event, data), event.result !== !1 },
                addClasses = function() { $container.addClass(settings.containerClass), plugin.tabs.parent().addClass(settings.tabsClass), plugin.tabs.addClass(settings.tabClass), plugin.panels.addClass(settings.panelClass) },
                setDefaultTab = function() {
                    var $panel, hash = window.location.hash.match(/^[^\?]*/)[0],
                        $selectedTab = plugin.matchTab(hash).parent();
                    1 === $selectedTab.length ? ($defaultTab = $selectedTab, settings.cycle = !1) : ($panel = plugin.matchInPanel(hash), $panel.length ? (hash = "#" + $panel.attr("id"), $defaultTab = plugin.matchTab(hash).parent()) : ($defaultTab = plugin.tabs.parent().find(settings.defaultTab), 0 === $defaultTab.length && $.error("The specified default tab ('" + settings.defaultTab + "') could not be found in the tab set ('" + settings.tabs + "') out of " + plugin.tabs.length + " tabs."))), $defaultTabLink = $defaultTab.children("a").first(), activateDefaultTab($selectedTab) },
                activateDefaultTab = function($selectedTab) {
                    var defaultPanel, defaultAjaxUrl;
                    settings.collapsible && 0 === $selectedTab.length && settings.collapsedByDefault ? $defaultTab.addClass(settings.collapsedClass).children().addClass(settings.collapsedClass) : (defaultPanel = $($defaultTab.data("easytabs").panel), defaultAjaxUrl = $defaultTab.data("easytabs").ajax, !defaultAjaxUrl || settings.cache && $defaultTab.data("easytabs").cached || ($container.trigger("easytabs:ajax:beforeSend", [$defaultTabLink, defaultPanel]), defaultPanel.load(defaultAjaxUrl, function(response, status, xhr) { $defaultTab.data("easytabs").cached = !0, $container.trigger("easytabs:ajax:complete", [$defaultTabLink, defaultPanel, response, status, xhr]) })), $defaultTab.data("easytabs").panel.show().addClass(settings.panelActiveClass), $defaultTab.addClass(settings.tabActiveClass).children().addClass(settings.tabActiveClass)), $container.trigger("easytabs:initialised", [$defaultTabLink, defaultPanel]) },
                bindToTabClicks = function() { plugin.tabs.children("a").bind(settings.bind_str, function(e) { settings.cycle = !1, skipUpdateToHash = !1, plugin.selectTab($(this)), e.preventDefault ? e.preventDefault() : e.returnValue = !1 }) },
                activateTab = function($clicked, $targetPanel, ajaxUrl, callback) {
                    if (plugin.panels.stop(!0, !0), fire($container, "easytabs:before", [$clicked, $targetPanel, settings])) {
                        var targetHeight, visibleHeight, heightDifference, showPanel, $visiblePanel = plugin.panels.filter(":visible"),
                            $panelContainer = $targetPanel.parent(),
                            hash = window.location.hash.match(/^[^\?]*/)[0];
                        settings.animate && (targetHeight = getHeightForHidden($targetPanel), visibleHeight = $visiblePanel.length ? setAndReturnHeight($visiblePanel) : 0, heightDifference = targetHeight - visibleHeight), lastHash = hash, showPanel = function() { $container.trigger("easytabs:midTransition", [$clicked, $targetPanel, settings]), settings.animate && "fadeIn" == settings.transitionIn && 0 > heightDifference && $panelContainer.animate({ height: $panelContainer.height() + heightDifference }, transitions.halfSpeed).css({ "min-height": "" }), settings.updateHash && !skipUpdateToHash ? window.location.hash = "#" + $targetPanel.attr("id") : skipUpdateToHash = !1, $targetPanel[transitions.show](transitions.speed, settings.transitionInEasing, function() { $panelContainer.css({ height: "", "min-height": "" }), $container.trigger("easytabs:after", [$clicked, $targetPanel, settings]), "function" == typeof callback && callback() }) }, !ajaxUrl || settings.cache && $clicked.parent().data("easytabs").cached || ($container.trigger("easytabs:ajax:beforeSend", [$clicked, $targetPanel]), $targetPanel.load(ajaxUrl, function(response, status, xhr) { $clicked.parent().data("easytabs").cached = !0, $container.trigger("easytabs:ajax:complete", [$clicked, $targetPanel, response, status, xhr]) })), settings.animate && "fadeOut" == settings.transitionOut && (heightDifference > 0 ? $panelContainer.animate({ height: $panelContainer.height() + heightDifference }, transitions.halfSpeed) : $panelContainer.css({ "min-height": $panelContainer.height() })), plugin.tabs.filter("." + settings.tabActiveClass).removeClass(settings.tabActiveClass).children().removeClass(settings.tabActiveClass), plugin.tabs.filter("." + settings.collapsedClass).removeClass(settings.collapsedClass).children().removeClass(settings.collapsedClass),
                            $clicked.parent().addClass(settings.tabActiveClass).children().addClass(settings.tabActiveClass), plugin.panels.filter("." + settings.panelActiveClass).removeClass(settings.panelActiveClass), $targetPanel.addClass(settings.panelActiveClass), $visiblePanel.length ? $visiblePanel[transitions.hide](transitions.speed, settings.transitionOutEasing, showPanel) : $targetPanel[transitions.uncollapse](transitions.speed, settings.transitionUncollapseEasing, showPanel)
                    }
                },
                getHeightForHidden = function($targetPanel) {
                    if ($targetPanel.data("easytabs") && $targetPanel.data("easytabs").lastHeight) return $targetPanel.data("easytabs").lastHeight;
                    var outerCloak, height, display = $targetPanel.css("display");
                    try { outerCloak = $("<div></div>", { position: "absolute", visibility: "hidden", overflow: "hidden" }) } catch (e) { outerCloak = $("<div></div>", { visibility: "hidden", overflow: "hidden" }) }
                    return height = $targetPanel.wrap(outerCloak).css({ position: "relative", visibility: "hidden", display: "block" }).outerHeight(), $targetPanel.unwrap(), $targetPanel.css({ position: $targetPanel.data("easytabs").position, visibility: $targetPanel.data("easytabs").visibility, display: display }), $targetPanel.data("easytabs").lastHeight = height, height },
                setAndReturnHeight = function($visiblePanel) {
                    var height = $visiblePanel.outerHeight();
                    return $visiblePanel.data("easytabs") ? $visiblePanel.data("easytabs").lastHeight = height : $visiblePanel.data("easytabs", { lastHeight: height }), height },
                initHashChange = function() { "function" == typeof $(window).hashchange ? $(window).hashchange(function() { plugin.selectTabFromHashChange() }) : $.address && "function" == typeof $.address.change && $.address.change(function() { plugin.selectTabFromHashChange() }) },
                initCycle = function() {
                    var tabNumber;
                    settings.cycle && (tabNumber = plugin.tabs.index($defaultTab), setTimeout(function() { plugin.cycleTabs(tabNumber + 1) }, settings.cycle)) };
            plugin.init()
        }, $.fn.easytabs = function(options) {
            var args = arguments;
            return this.each(function() {
                var $this = $(this),
                    plugin = $this.data("easytabs");
                return void 0 === plugin && (plugin = new $.easytabs(this, options), $this.data("easytabs", plugin)), plugin.publicMethods[options] ? plugin.publicMethods[options](Array.prototype.slice.call(args, 1)) : void 0 }) }
    }(jQuery),
    function(e, t) { "use strict";
        var n = e.History = e.History || {},
            r = e.jQuery;
        if ("undefined" != typeof n.Adapter) throw new Error("History.js Adapter has already been loaded...");
        n.Adapter = { bind: function(e, t, n) { r(e).bind(t, n) }, trigger: function(e, t, n) { r(e).trigger(t, n) }, extractEventData: function(e, n, r) {
                var i = n && n.originalEvent && n.originalEvent[e] || r && r[e] || t;
                return i }, onDomLoad: function(e) { r(e) } }, "undefined" != typeof n.init && n.init() }(window),
    function(e, t) { "use strict";
        var n = e.console || t,
            r = e.document,
            i = e.navigator,
            s = !1,
            o = e.setTimeout,
            u = e.clearTimeout,
            a = e.setInterval,
            f = e.clearInterval,
            l = e.JSON,
            c = e.alert,
            h = e.History = e.History || {},
            p = e.history;
        try { s = e.sessionStorage, s.setItem("TEST", "1"), s.removeItem("TEST") } catch (d) { s = !1 }
        if (l.stringify = l.stringify || l.encode, l.parse = l.parse || l.decode, "undefined" != typeof h.init) throw new Error("History.js Core has already been loaded...");
        h.init = function(e) {
            return "undefined" == typeof h.Adapter ? !1 : ("undefined" != typeof h.initCore && h.initCore(), "undefined" != typeof h.initHtml4 && h.initHtml4(), !0) }, h.initCore = function(d) {
            if ("undefined" != typeof h.initCore.initialized) return !1;
            if (h.initCore.initialized = !0, h.options = h.options || {}, h.options.hashChangeInterval = h.options.hashChangeInterval || 100, h.options.safariPollInterval = h.options.safariPollInterval || 500, h.options.doubleCheckInterval = h.options.doubleCheckInterval || 500, h.options.disableSuid = h.options.disableSuid || !1, h.options.storeInterval = h.options.storeInterval || 1e3, h.options.busyDelay = h.options.busyDelay || 250, h.options.debug = h.options.debug || !1, h.options.initialTitle = h.options.initialTitle || r.title, h.options.html4Mode = h.options.html4Mode || !1, h.options.delayInit = h.options.delayInit || !1, h.intervalList = [], h.clearAllIntervals = function() {
                    var e, t = h.intervalList;
                    if ("undefined" != typeof t && null !== t) {
                        for (e = 0; e < t.length; e++) f(t[e]);
                        h.intervalList = null } }, h.debug = function() {
                    (h.options.debug || !1) && h.log.apply(h, arguments) }, h.log = function() {
                    var i, s, o, u, a, e = "undefined" != typeof n && "undefined" != typeof n.log && "undefined" != typeof n.log.apply,
                        t = r.getElementById("log");
                    for (e ? (u = Array.prototype.slice.call(arguments), i = u.shift(), "undefined" != typeof n.debug ? n.debug.apply(n, [i, u]) : n.log.apply(n, [i, u])) : i = "\n" + arguments[0] + "\n", s = 1, o = arguments.length; o > s; ++s) {
                        if (a = arguments[s], "object" == typeof a && "undefined" != typeof l) try { a = l.stringify(a) } catch (f) {}
                        i += "\n" + a + "\n" }
                    return t ? (t.value += i + "\n-----\n", t.scrollTop = t.scrollHeight - t.clientHeight) : e || c(i), !0 }, h.getInternetExplorerMajorVersion = function() {
                    var e = h.getInternetExplorerMajorVersion.cached = "undefined" != typeof h.getInternetExplorerMajorVersion.cached ? h.getInternetExplorerMajorVersion.cached : function() {
                        for (var e = 3, t = r.createElement("div"), n = t.getElementsByTagName("i");
                            (t.innerHTML = "<!--[if gt IE " + ++e + "]><i></i><![endif]-->") && n[0];);
                        return e > 4 ? e : !1 }();
                    return e }, h.isInternetExplorer = function() {
                    var e = h.isInternetExplorer.cached = "undefined" != typeof h.isInternetExplorer.cached ? h.isInternetExplorer.cached : Boolean(h.getInternetExplorerMajorVersion());
                    return e }, h.emulated = h.options.html4Mode ? { pushState: !0, hashChange: !0 } : { pushState: !Boolean(e.history && e.history.pushState && e.history.replaceState && !/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(i.userAgent) && !/AppleWebKit\/5([0-2]|3[0-2])/i.test(i.userAgent)), hashChange: Boolean(!("onhashchange" in e || "onhashchange" in r) || h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8) }, h.enabled = !h.emulated.pushState, h.bugs = { setHash: Boolean(!h.emulated.pushState && "Apple Computer, Inc." === i.vendor && /AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)), safariPoll: Boolean(!h.emulated.pushState && "Apple Computer, Inc." === i.vendor && /AppleWebKit\/5([0-2]|3[0-3])/.test(i.userAgent)), ieDoubleCheck: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8), hashEscape: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 7) }, h.isEmptyObject = function(e) {
                    for (var t in e)
                        if (e.hasOwnProperty(t)) return !1;
                    return !0 }, h.cloneObject = function(e) {
                    var t, n;
                    return e ? (t = l.stringify(e), n = l.parse(t)) : n = {}, n }, h.getRootUrl = function() {
                    var e = r.location.protocol + "//" + (r.location.hostname || r.location.host);
                    return r.location.port && (e += ":" + r.location.port), e += "/" }, h.getBaseHref = function() {
                    var e = r.getElementsByTagName("base"),
                        t = null,
                        n = "";
                    return 1 === e.length && (t = e[0], n = t.href.replace(/[^\/]+$/, "")), n = n.replace(/\/+$/, ""), n && (n += "/"), n }, h.getBaseUrl = function() {
                    var e = h.getBaseHref() || h.getBasePageUrl() || h.getRootUrl();
                    return e }, h.getPageUrl = function() {
                    var n, e = h.getState(!1, !1),
                        t = (e || {}).url || h.getLocationHref();
                    return n = t.replace(/\/+$/, "").replace(/[^\/]+$/, function(e, t, n) {
                        return /\./.test(e) ? e : e + "/" }) }, h.getBasePageUrl = function() {
                    var e = h.getLocationHref().replace(/[#\?].*/, "").replace(/[^\/]+$/, function(e, t, n) {
                        return /[^\/]$/.test(e) ? "" : e }).replace(/\/+$/, "") + "/";
                    return e }, h.getFullUrl = function(e, t) {
                    var n = e,
                        r = e.substring(0, 1);
                    return t = "undefined" == typeof t ? !0 : t, /[a-z]+\:\/\//.test(e) || (n = "/" === r ? h.getRootUrl() + e.replace(/^\/+/, "") : "#" === r ? h.getPageUrl().replace(/#.*/, "") + e : "?" === r ? h.getPageUrl().replace(/[\?#].*/, "") + e : t ? h.getBaseUrl() + e.replace(/^(\.\/)+/, "") : h.getBasePageUrl() + e.replace(/^(\.\/)+/, "")), n.replace(/\#$/, "") }, h.getShortUrl = function(e) {
                    var t = e,
                        n = h.getBaseUrl(),
                        r = h.getRootUrl();
                    return h.emulated.pushState && (t = t.replace(n, "")), t = t.replace(r, "/"), h.isTraditionalAnchor(t) && (t = "./" + t), t = t.replace(/^(\.\/)+/g, "./").replace(/\#$/, "") }, h.getLocationHref = function(e) {
                    return e = e || r, e.URL === e.location.href ? e.location.href : e.location.href === decodeURIComponent(e.URL) ? e.URL : e.location.hash && decodeURIComponent(e.location.href.replace(/^[^#]+/, "")) === e.location.hash ? e.location.href : -1 == e.URL.indexOf("#") && -1 != e.location.href.indexOf("#") ? e.location.href : e.URL || e.location.href }, h.store = {}, h.idToState = h.idToState || {}, h.stateToId = h.stateToId || {}, h.urlToId = h.urlToId || {}, h.storedStates = h.storedStates || [], h.savedStates = h.savedStates || [], h.normalizeStore = function() { h.store.idToState = h.store.idToState || {}, h.store.urlToId = h.store.urlToId || {}, h.store.stateToId = h.store.stateToId || {} }, h.getState = function(e, t) { "undefined" == typeof e && (e = !0), "undefined" == typeof t && (t = !0);
                    var n = h.getLastSavedState();
                    return !n && t && (n = h.createStateObject()), e && (n = h.cloneObject(n), n.url = n.cleanUrl || n.url), n }, h.getIdByState = function(e) {
                    var n, t = h.extractId(e.url);
                    if (!t)
                        if (n = h.getStateString(e), "undefined" != typeof h.stateToId[n]) t = h.stateToId[n];
                        else if ("undefined" != typeof h.store.stateToId[n]) t = h.store.stateToId[n];
                    else {
                        for (; t = (new Date).getTime() + String(Math.random()).replace(/\D/g, ""), "undefined" != typeof h.idToState[t] || "undefined" != typeof h.store.idToState[t];);
                        h.stateToId[n] = t, h.idToState[t] = e }
                    return t }, h.normalizeState = function(e) {
                    var t, n;
                    return e && "object" == typeof e || (e = {}), "undefined" != typeof e.normalized ? e : (e.data && "object" == typeof e.data || (e.data = {}), t = {}, t.normalized = !0, t.title = e.title || "", t.url = h.getFullUrl(e.url ? e.url : h.getLocationHref()), t.hash = h.getShortUrl(t.url), t.data = h.cloneObject(e.data), t.id = h.getIdByState(t), t.cleanUrl = t.url.replace(/\??\&_suid.*/, ""), t.url = t.cleanUrl, n = !h.isEmptyObject(t.data), (t.title || n) && h.options.disableSuid !== !0 && (t.hash = h.getShortUrl(t.url).replace(/\??\&_suid.*/, ""), /\?/.test(t.hash) || (t.hash += "?"), t.hash += "&_suid=" + t.id), t.hashedUrl = h.getFullUrl(t.hash), (h.emulated.pushState || h.bugs.safariPoll) && h.hasUrlDuplicate(t) && (t.url = t.hashedUrl), t) }, h.createStateObject = function(e, t, n) {
                    var r = { data: e, title: t, url: n };
                    return r = h.normalizeState(r) }, h.getStateById = function(e) { e = String(e);
                    var n = h.idToState[e] || h.store.idToState[e] || t;
                    return n }, h.getStateString = function(e) {
                    var t, n, r;
                    return t = h.normalizeState(e), n = { data: t.data, title: e.title, url: e.url }, r = l.stringify(n) }, h.getStateId = function(e) {
                    var t, n;
                    return t = h.normalizeState(e), n = t.id }, h.getHashByState = function(e) {
                    var t, n;
                    return t = h.normalizeState(e), n = t.hash }, h.extractId = function(e) {
                    var t, n, r, i;
                    return i = -1 != e.indexOf("#") ? e.split("#")[0] : e, n = /(.*)\&_suid=([0-9]+)$/.exec(i), r = n ? n[1] || e : e, t = n ? String(n[2] || "") : "", t || !1 }, h.isTraditionalAnchor = function(e) {
                    var t = !/[\/\?\.]/.test(e);
                    return t }, h.extractState = function(e, t) {
                    var r, i, n = null;
                    return t = t || !1, r = h.extractId(e), r && (n = h.getStateById(r)), n || (i = h.getFullUrl(e), r = h.getIdByUrl(i) || !1, r && (n = h.getStateById(r)), !n && t && !h.isTraditionalAnchor(e) && (n = h.createStateObject(null, null, i))), n }, h.getIdByUrl = function(e) {
                    var n = h.urlToId[e] || h.store.urlToId[e] || t;
                    return n }, h.getLastSavedState = function() {
                    return h.savedStates[h.savedStates.length - 1] || t }, h.getLastStoredState = function() {
                    return h.storedStates[h.storedStates.length - 1] || t }, h.hasUrlDuplicate = function(e) {
                    var n, t = !1;
                    return n = h.extractState(e.url), t = n && n.id !== e.id }, h.storeState = function(e) {
                    return h.urlToId[e.url] = e.id, h.storedStates.push(h.cloneObject(e)), e }, h.isLastSavedState = function(e) {
                    var n, r, i, t = !1;
                    return h.savedStates.length && (n = e.id, r = h.getLastSavedState(), i = r.id, t = n === i), t }, h.saveState = function(e) {
                    return h.isLastSavedState(e) ? !1 : (h.savedStates.push(h.cloneObject(e)), !0) }, h.getStateByIndex = function(e) {
                    var t = null;
                    return t = "undefined" == typeof e ? h.savedStates[h.savedStates.length - 1] : 0 > e ? h.savedStates[h.savedStates.length + e] : h.savedStates[e] }, h.getCurrentIndex = function() {
                    var e = null;
                    return e = h.savedStates.length < 1 ? 0 : h.savedStates.length - 1 }, h.getHash = function(e) {
                    var n, t = h.getLocationHref(e);
                    return n = h.getHashByUrl(t) }, h.unescapeHash = function(e) {
                    var t = h.normalizeHash(e);
                    return t = decodeURIComponent(t) }, h.normalizeHash = function(e) {
                    var t = e.replace(/[^#]*#/, "").replace(/#.*/, "");
                    return t }, h.setHash = function(e, t) {
                    var n, i;
                    return t !== !1 && h.busy() ? (h.pushQueue({ scope: h, callback: h.setHash, args: arguments, queue: t }), !1) : (h.busy(!0), n = h.extractState(e, !0), n && !h.emulated.pushState ? h.pushState(n.data, n.title, n.url, !1) : h.getHash() !== e && (h.bugs.setHash ? (i = h.getPageUrl(), h.pushState(null, null, i + "#" + e, !1)) : r.location.hash = e), h) }, h.escapeHash = function(t) {
                    var n = h.normalizeHash(t);
                    return n = e.encodeURIComponent(n), h.bugs.hashEscape || (n = n.replace(/\%21/g, "!").replace(/\%26/g, "&").replace(/\%3D/g, "=").replace(/\%3F/g, "?")), n }, h.getHashByUrl = function(e) {
                    var t = String(e).replace(/([^#]*)#?([^#]*)#?(.*)/, "$2");
                    return t = h.unescapeHash(t) }, h.setTitle = function(e) {
                    var n, t = e.title;
                    t || (n = h.getStateByIndex(0), n && n.url === e.url && (t = n.title || h.options.initialTitle));
                    try { r.getElementsByTagName("title")[0].innerHTML = t.replace("<", "&lt;").replace(">", "&gt;").replace(" & ", " &amp; ") } catch (i) {}
                    return r.title = t, h }, h.queues = [], h.busy = function(e) {
                    if ("undefined" != typeof e ? h.busy.flag = e : "undefined" == typeof h.busy.flag && (h.busy.flag = !1), !h.busy.flag) { u(h.busy.timeout);
                        var t = function() {
                            var e, n, r;
                            if (!h.busy.flag)
                                for (e = h.queues.length - 1; e >= 0; --e) n = h.queues[e], 0 !== n.length && (r = n.shift(), h.fireQueueItem(r), h.busy.timeout = o(t, h.options.busyDelay)) };
                        h.busy.timeout = o(t, h.options.busyDelay) }
                    return h.busy.flag }, h.busy.flag = !1, h.fireQueueItem = function(e) {
                    return e.callback.apply(e.scope || h, e.args || []) }, h.pushQueue = function(e) {
                    return h.queues[e.queue || 0] = h.queues[e.queue || 0] || [], h.queues[e.queue || 0].push(e), h }, h.queue = function(e, t) {
                    return "function" == typeof e && (e = { callback: e }), "undefined" != typeof t && (e.queue = t), h.busy() ? h.pushQueue(e) : h.fireQueueItem(e), h }, h.clearQueue = function() {
                    return h.busy.flag = !1, h.queues = [], h }, h.stateChanged = !1, h.doubleChecker = !1, h.doubleCheckComplete = function() {
                    return h.stateChanged = !0, h.doubleCheckClear(), h }, h.doubleCheckClear = function() {
                    return h.doubleChecker && (u(h.doubleChecker), h.doubleChecker = !1), h }, h.doubleCheck = function(e) {
                    return h.stateChanged = !1, h.doubleCheckClear(), h.bugs.ieDoubleCheck && (h.doubleChecker = o(function() {
                        return h.doubleCheckClear(), h.stateChanged || e(), !0 }, h.options.doubleCheckInterval)), h }, h.safariStatePoll = function() {
                    var n, t = h.extractState(h.getLocationHref());
                    return h.isLastSavedState(t) ? void 0 : (n = t, n || (n = h.createStateObject()), h.Adapter.trigger(e, "popstate"), h) }, h.back = function(e) {
                    return e !== !1 && h.busy() ? (h.pushQueue({ scope: h, callback: h.back, args: arguments, queue: e }), !1) : (h.busy(!0), h.doubleCheck(function() { h.back(!1) }), p.go(-1), !0) }, h.forward = function(e) {
                    return e !== !1 && h.busy() ? (h.pushQueue({ scope: h, callback: h.forward, args: arguments, queue: e }), !1) : (h.busy(!0), h.doubleCheck(function() { h.forward(!1) }), p.go(1), !0) }, h.go = function(e, t) {
                    var n;
                    if (e > 0)
                        for (n = 1; e >= n; ++n) h.forward(t);
                    else {
                        if (!(0 > e)) throw new Error("History.go: History.go requires a positive or negative integer passed.");
                        for (n = -1; n >= e; --n) h.back(t) }
                    return h }, h.emulated.pushState) {
                var v = function() {};
                h.pushState = h.pushState || v, h.replaceState = h.replaceState || v } else h.onPopState = function(t, n) {
                var s, o, r = !1,
                    i = !1;
                return h.doubleCheckComplete(), s = h.getHash(), s ? (o = h.extractState(s || h.getLocationHref(), !0), o ? h.replaceState(o.data, o.title, o.url, !1) : (h.Adapter.trigger(e, "anchorchange"), h.busy(!1)), h.expectedStateId = !1, !1) : (r = h.Adapter.extractEventData("state", t, n) || !1, i = r ? h.getStateById(r) : h.expectedStateId ? h.getStateById(h.expectedStateId) : h.extractState(h.getLocationHref()), i || (i = h.createStateObject(null, null, h.getLocationHref())), h.expectedStateId = !1, h.isLastSavedState(i) ? (h.busy(!1), !1) : (h.storeState(i), h.saveState(i), h.setTitle(i), h.Adapter.trigger(e, "statechange"), h.busy(!1), !0)) }, h.Adapter.bind(e, "popstate", h.onPopState), h.pushState = function(t, n, r, i) {
                if (h.getHashByUrl(r) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
                if (i !== !1 && h.busy()) return h.pushQueue({ scope: h, callback: h.pushState, args: arguments, queue: i }), !1;
                h.busy(!0);
                var s = h.createStateObject(t, n, r);
                return h.isLastSavedState(s) ? h.busy(!1) : (h.storeState(s), h.expectedStateId = s.id, p.pushState(s.id, s.title, s.url), h.Adapter.trigger(e, "popstate")), !0 }, h.replaceState = function(t, n, r, i) {
                if (h.getHashByUrl(r) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
                if (i !== !1 && h.busy()) return h.pushQueue({ scope: h, callback: h.replaceState, args: arguments, queue: i }), !1;
                h.busy(!0);
                var s = h.createStateObject(t, n, r);
                return h.isLastSavedState(s) ? h.busy(!1) : (h.storeState(s), h.expectedStateId = s.id, p.replaceState(s.id, s.title, s.url), h.Adapter.trigger(e, "popstate")), !0 };
            if (s) {
                try { h.store = l.parse(s.getItem("History.store")) || {} } catch (m) { h.store = {} }
                h.normalizeStore() } else h.store = {}, h.normalizeStore();
            h.Adapter.bind(e, "unload", h.clearAllIntervals), h.saveState(h.storeState(h.extractState(h.getLocationHref(), !0))), s && (h.onUnload = function() {
                var e, t, n;
                try { e = l.parse(s.getItem("History.store")) || {} } catch (r) { e = {} }
                e.idToState = e.idToState || {}, e.urlToId = e.urlToId || {}, e.stateToId = e.stateToId || {};
                for (t in h.idToState) h.idToState.hasOwnProperty(t) && (e.idToState[t] = h.idToState[t]);
                for (t in h.urlToId) h.urlToId.hasOwnProperty(t) && (e.urlToId[t] = h.urlToId[t]);
                for (t in h.stateToId) h.stateToId.hasOwnProperty(t) && (e.stateToId[t] = h.stateToId[t]);
                h.store = e, h.normalizeStore(), n = l.stringify(e);
                try { s.setItem("History.store", n) } catch (i) {
                    if (i.code !== DOMException.QUOTA_EXCEEDED_ERR) throw i;
                    s.length && (s.removeItem("History.store"), s.setItem("History.store", n)) } }, h.intervalList.push(a(h.onUnload, h.options.storeInterval)), h.Adapter.bind(e, "beforeunload", h.onUnload), h.Adapter.bind(e, "unload", h.onUnload)), h.emulated.pushState || (h.bugs.safariPoll && h.intervalList.push(a(h.safariStatePoll, h.options.safariPollInterval)), ("Apple Computer, Inc." === i.vendor || "Mozilla" === (i.appCodeName || "")) && (h.Adapter.bind(e, "hashchange", function() { h.Adapter.trigger(e, "popstate") }), h.getHash() && h.Adapter.onDomLoad(function() { h.Adapter.trigger(e, "hashchange") }))) }, (!h.options || !h.options.delayInit) && h.init() }(window),
    function() {
        function EventEmitter() {}

        function indexOfListener(listeners, listener) {
            for (var i = listeners.length; i--;)
                if (listeners[i].listener === listener) return i;
            return -1 }

        function alias(name) {
            return function() {
                return this[name].apply(this, arguments) } }
        var proto = EventEmitter.prototype,
            exports = this,
            originalGlobalValue = exports.EventEmitter;
        proto.getListeners = function(evt) {
            var response, key, events = this._getEvents();
            if ("object" == typeof evt) { response = {};
                for (key in events) events.hasOwnProperty(key) && evt.test(key) && (response[key] = events[key]) } else response = events[evt] || (events[evt] = []);
            return response }, proto.flattenListeners = function(listeners) {
            var i, flatListeners = [];
            for (i = 0; i < listeners.length; i += 1) flatListeners.push(listeners[i].listener);
            return flatListeners }, proto.getListenersAsObject = function(evt) {
            var response, listeners = this.getListeners(evt);
            return listeners instanceof Array && (response = {}, response[evt] = listeners), response || listeners }, proto.addListener = function(evt, listener) {
            var key, listeners = this.getListenersAsObject(evt),
                listenerIsWrapped = "object" == typeof listener;
            for (key in listeners) listeners.hasOwnProperty(key) && -1 === indexOfListener(listeners[key], listener) && listeners[key].push(listenerIsWrapped ? listener : { listener: listener, once: !1 });
            return this }, proto.on = alias("addListener"), proto.addOnceListener = function(evt, listener) {
            return this.addListener(evt, { listener: listener, once: !0 }) }, proto.once = alias("addOnceListener"), proto.defineEvent = function(evt) {
            return this.getListeners(evt), this }, proto.defineEvents = function(evts) {
            for (var i = 0; i < evts.length; i += 1) this.defineEvent(evts[i]);
            return this }, proto.removeListener = function(evt, listener) {
            var index, key, listeners = this.getListenersAsObject(evt);
            for (key in listeners) listeners.hasOwnProperty(key) && (index = indexOfListener(listeners[key], listener), -1 !== index && listeners[key].splice(index, 1));
            return this }, proto.off = alias("removeListener"), proto.addListeners = function(evt, listeners) {
            return this.manipulateListeners(!1, evt, listeners) }, proto.removeListeners = function(evt, listeners) {
            return this.manipulateListeners(!0, evt, listeners) }, proto.manipulateListeners = function(remove, evt, listeners) {
            var i, value, single = remove ? this.removeListener : this.addListener,
                multiple = remove ? this.removeListeners : this.addListeners;
            if ("object" != typeof evt || evt instanceof RegExp)
                for (i = listeners.length; i--;) single.call(this, evt, listeners[i]);
            else
                for (i in evt) evt.hasOwnProperty(i) && (value = evt[i]) && ("function" == typeof value ? single.call(this, i, value) : multiple.call(this, i, value));
            return this }, proto.removeEvent = function(evt) {
            var key, type = typeof evt,
                events = this._getEvents();
            if ("string" === type) delete events[evt];
            else if ("object" === type)
                for (key in events) events.hasOwnProperty(key) && evt.test(key) && delete events[key];
            else delete this._events;
            return this }, proto.removeAllListeners = alias("removeEvent"), proto.emitEvent = function(evt, args) {
            var listener, i, key, response, listeners = this.getListenersAsObject(evt);
            for (key in listeners)
                if (listeners.hasOwnProperty(key))
                    for (i = listeners[key].length; i--;) listener = listeners[key][i], listener.once === !0 && this.removeListener(evt, listener.listener), response = listener.listener.apply(this, args || []), response === this._getOnceReturnValue() && this.removeListener(evt, listener.listener);
            return this }, proto.trigger = alias("emitEvent"), proto.emit = function(evt) {
            var args = Array.prototype.slice.call(arguments, 1);
            return this.emitEvent(evt, args) }, proto.setOnceReturnValue = function(value) {
            return this._onceReturnValue = value, this }, proto._getOnceReturnValue = function() {
            return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0 }, proto._getEvents = function() {
            return this._events || (this._events = {}) }, EventEmitter.noConflict = function() {
            return exports.EventEmitter = originalGlobalValue, EventEmitter }, "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
            return EventEmitter }) : "object" == typeof module && module.exports ? module.exports = EventEmitter : this.EventEmitter = EventEmitter }.call(this),
    function(window) {
        function getIEEvent(obj) {
            var event = window.event;
            return event.target = event.target || event.srcElement || obj, event }
        var docElem = document.documentElement,
            bind = function() {};
        docElem.addEventListener ? bind = function(obj, type, fn) { obj.addEventListener(type, fn, !1) } : docElem.attachEvent && (bind = function(obj, type, fn) { obj[type + fn] = fn.handleEvent ? function() {
                var event = getIEEvent(obj);
                fn.handleEvent.call(fn, event) } : function() {
                var event = getIEEvent(obj);
                fn.call(obj, event) }, obj.attachEvent("on" + type, obj[type + fn]) });
        var unbind = function() {};
        docElem.removeEventListener ? unbind = function(obj, type, fn) { obj.removeEventListener(type, fn, !1) } : docElem.detachEvent && (unbind = function(obj, type, fn) { obj.detachEvent("on" + type, obj[type + fn]);
            try { delete obj[type + fn] } catch (err) { obj[type + fn] = void 0 } });
        var eventie = { bind: bind, unbind: unbind }; "function" == typeof define && define.amd ? define("eventie/eventie", eventie) : window.eventie = eventie }(this),
    function(window, factory) { "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], function(EventEmitter, eventie) {
            return factory(window, EventEmitter, eventie) }) : "object" == typeof exports ? module.exports = factory(window, require("wolfy87-eventemitter"), require("eventie")) : window.imagesLoaded = factory(window, window.EventEmitter, window.eventie) }(window, function(window, EventEmitter, eventie) {
        function extend(a, b) {
            for (var prop in b) a[prop] = b[prop];
            return a }

        function isArray(obj) {
            return "[object Array]" === objToString.call(obj) }

        function makeArray(obj) {
            var ary = [];
            if (isArray(obj)) ary = obj;
            else if ("number" == typeof obj.length)
                for (var i = 0, len = obj.length; len > i; i++) ary.push(obj[i]);
            else ary.push(obj);
            return ary }

        function ImagesLoaded(elem, options, onAlways) {
            if (!(this instanceof ImagesLoaded)) return new ImagesLoaded(elem, options); "string" == typeof elem && (elem = document.querySelectorAll(elem)), this.elements = makeArray(elem), this.options = extend({}, this.options), "function" == typeof options ? onAlways = options : extend(this.options, options), onAlways && this.on("always", onAlways), this.getImages(), $ && (this.jqDeferred = new $.Deferred);
            var _this = this;
            setTimeout(function() { _this.check() }) }

        function LoadingImage(img) { this.img = img }

        function Resource(src) { this.src = src, cache[src] = this }
        var $ = window.jQuery,
            console = window.console,
            hasConsole = "undefined" != typeof console,
            objToString = Object.prototype.toString;
        ImagesLoaded.prototype = new EventEmitter, ImagesLoaded.prototype.options = {}, ImagesLoaded.prototype.getImages = function() { this.images = [];
            for (var i = 0, len = this.elements.length; len > i; i++) {
                var elem = this.elements[i]; "IMG" === elem.nodeName && this.addImage(elem);
                var nodeType = elem.nodeType;
                if (nodeType && (1 === nodeType || 9 === nodeType || 11 === nodeType))
                    for (var childElems = elem.querySelectorAll("img"), j = 0, jLen = childElems.length; jLen > j; j++) {
                        var img = childElems[j];
                        this.addImage(img) } } }, ImagesLoaded.prototype.addImage = function(img) {
            var loadingImage = new LoadingImage(img);
            this.images.push(loadingImage) }, ImagesLoaded.prototype.check = function() {
            function onConfirm(image, message) {
                return _this.options.debug && hasConsole && console.log("confirm", image, message), _this.progress(image), checkedCount++, checkedCount === length && _this.complete(), !0 }
            var _this = this,
                checkedCount = 0,
                length = this.images.length;
            if (this.hasAnyBroken = !1, !length) return void this.complete();
            for (var i = 0; length > i; i++) {
                var loadingImage = this.images[i];
                loadingImage.on("confirm", onConfirm), loadingImage.check() } }, ImagesLoaded.prototype.progress = function(image) { this.hasAnyBroken = this.hasAnyBroken || !image.isLoaded;
            var _this = this;
            setTimeout(function() { _this.emit("progress", _this, image), _this.jqDeferred && _this.jqDeferred.notify && _this.jqDeferred.notify(_this, image) }) }, ImagesLoaded.prototype.complete = function() {
            var eventName = this.hasAnyBroken ? "fail" : "done";
            this.isComplete = !0;
            var _this = this;
            setTimeout(function() {
                if (_this.emit(eventName, _this), _this.emit("always", _this), _this.jqDeferred) {
                    var jqMethod = _this.hasAnyBroken ? "reject" : "resolve";
                    _this.jqDeferred[jqMethod](_this) } }) }, $ && ($.fn.imagesLoaded = function(options, callback) {
            var instance = new ImagesLoaded(this, options, callback);
            return instance.jqDeferred.promise($(this)) }), LoadingImage.prototype = new EventEmitter, LoadingImage.prototype.check = function() {
            var resource = cache[this.img.src] || new Resource(this.img.src);
            if (resource.isConfirmed) return void this.confirm(resource.isLoaded, "cached was confirmed");
            if (this.img.complete && void 0 !== this.img.naturalWidth) return void this.confirm(0 !== this.img.naturalWidth, "naturalWidth");
            var _this = this;
            resource.on("confirm", function(resrc, message) {
                return _this.confirm(resrc.isLoaded, message), !0 }), resource.check() }, LoadingImage.prototype.confirm = function(isLoaded, message) { this.isLoaded = isLoaded, this.emit("confirm", this, message) };
        var cache = {};
        return Resource.prototype = new EventEmitter, Resource.prototype.check = function() {
            if (!this.isChecked) {
                var proxyImage = new Image;
                eventie.bind(proxyImage, "load", this), eventie.bind(proxyImage, "error", this), proxyImage.src = this.src, this.isChecked = !0 } }, Resource.prototype.handleEvent = function(event) {
            var method = "on" + event.type;
            this[method] && this[method](event) }, Resource.prototype.onload = function(event) { this.confirm(!0, "onload"), this.unbindProxyEvents(event) }, Resource.prototype.onerror = function(event) { this.confirm(!1, "onerror"), this.unbindProxyEvents(event) }, Resource.prototype.confirm = function(isLoaded, message) { this.isConfirmed = !0, this.isLoaded = isLoaded, this.emit("confirm", this, message) }, Resource.prototype.unbindProxyEvents = function(event) { eventie.unbind(event.target, "load", this), eventie.unbind(event.target, "error", this) }, ImagesLoaded }),
    function(b) { b.jscroll = { defaults: { debug: !1, autoTrigger: !0, autoTriggerUntil: !1, loadingHtml: "<small>Loading...</small>", padding: 0, nextSelector: "a:last", contentSelector: "", pagingSelector: "", callback: !1 } };
        var a = function(e, g) {
            function k() {
                var x = b(p.loadingHtml).filter("img").attr("src");
                if (x) {
                    var w = new Image;
                    w.src = x } }

            function r() { e.find(".jscroll-inner").length || e.contents().wrapAll('<div class="jscroll-inner" />') }

            function d(w) {
                if (p.pagingSelector) var x = w.closest(p.pagingSelector).hide();
                else {
                    var x = w.parent().not(".jscroll-inner,.jscroll-added").addClass("jscroll-next-parent").hide();
                    x.length || w.wrap('<div class="jscroll-next-parent" />').parent().hide() } }

            function j() {
                return q.unbind(".jscroll").removeData("jscroll").find(".jscroll-inner").children().unwrap().filter(".jscroll-added").children().unwrap() }

            function i() { r();
                var D = e.find("div.jscroll-inner").first(),
                    B = e.data("jscroll"),
                    C = parseInt(e.css("borderTopWidth")),
                    y = isNaN(C) ? 0 : C,
                    x = parseInt(e.css("paddingTop")) + y,
                    A = c ? q.scrollTop() : e.offset().top,
                    z = D.length ? D.offset().top : 0,
                    w = Math.ceil(A - z + q.height() + x);
                return !B.waiting && w + p.padding >= D.outerHeight() ? (f("info", "jScroll:", D.outerHeight() - w, "from bottom. Loading next request..."), u()) : void 0 }

            function s(w) {
                return w = w || e.data("jscroll"), w && w.nextHref ? (t(), !0) : (f("warn", "jScroll: nextSelector not found - destroying"), j(), !1) }

            function t() {
                var w = e.find(p.nextSelector).first();
                p.autoTrigger && (p.autoTriggerUntil === !1 || p.autoTriggerUntil > 0) ? (d(w), h.height() <= v.height() && i(), q.unbind(".jscroll").bind("scroll.jscroll", function() {
                    return i() }), p.autoTriggerUntil > 0 && p.autoTriggerUntil--) : (q.unbind(".jscroll"), w.bind("click.jscroll", function() {
                    return d(w), u(), !1 })) }

            function u() {
                var x = e.find("div.jscroll-inner").first(),
                    w = e.data("jscroll");
                return w.waiting = !0, x.append('<div class="jscroll-added" />').children(".jscroll-added").last().html('<div class="jscroll-loading">' + p.loadingHtml + "</div>"), e.animate({ scrollTop: x.outerHeight() }, 0, function() { x.find("div.jscroll-added").last().load(w.nextHref, function(A, z, B) {
                        if ("error" === z) return j();
                        var y = b(this).find(p.nextSelector).first();
                        w.waiting = !1, w.nextHref = y.attr("href") ? b.trim(y.attr("href") + " " + p.contentSelector) : !1, b(".jscroll-next-parent", e).remove(), s(), p.callback && p.callback.call(this), f("dir", w) }) }) }

            function f(w) {
                if (p.debug && "object" == typeof console && ("object" == typeof w || "function" == typeof console[w]))
                    if ("object" == typeof w) {
                        var y = [];
                        for (var x in w) "function" == typeof console[x] ? (y = w[x].length ? w[x] : [w[x]], console[x].apply(console, y)) : console.log.apply(console, y) } else console[w].apply(console, Array.prototype.slice.call(arguments, 1)) }
            var o = e.data("jscroll"),
                n = "function" == typeof g ? { callback: g } : g,
                p = b.extend({}, b.jscroll.defaults, n, o || {}),
                c = "visible" === e.css("overflow-y"),
                l = e.find(p.nextSelector).first(),
                v = b(window),
                h = b("body"),
                q = c ? v : e,
                m = b.trim(l.attr("href") + " " + p.contentSelector);
            return e.data("jscroll", b.extend({}, o, { initialized: !0, waiting: !1, nextHref: m })), r(), k(), t(), b.extend(e.jscroll, { destroy: j }), e };
        b.fn.jscroll = function(c) {
            return this.each(function() {
                var f = b(this),
                    e = f.data("jscroll");
                if (!e || !e.initialized) { new a(f, c) } }) } }(jQuery), ! function(a, b, c, d) {
        var e = a(b);
        a.fn.lazyload = function(f) {
            function g() {
                var b = 0;
                i.each(function() {
                    var c = a(this);
                    if (!j.skip_invisible || c.is(":visible"))
                        if (a.abovethetop(this, j) || a.leftofbegin(this, j));
                        else if (a.belowthefold(this, j) || a.rightoffold(this, j)) {
                        if (++b > j.failure_limit) return !1 } else c.trigger("appear"), b = 0 }) }
            var h, i = this,
                j = { threshold: 0, failure_limit: 0, event: "scroll", effect: "show", container: b, data_attribute: "original", skip_invisible: !1, appear: null, load: null, placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" };
            return f && (d !== f.failurelimit && (f.failure_limit = f.failurelimit, delete f.failurelimit), d !== f.effectspeed && (f.effect_speed = f.effectspeed, delete f.effectspeed), a.extend(j, f)), h = j.container === d || j.container === b ? e : a(j.container), 0 === j.event.indexOf("scroll") && h.bind(j.event, function() {
                return g() }), this.each(function() {
                var b = this,
                    c = a(b);
                b.loaded = !1, (c.attr("src") === d || c.attr("src") === !1) && c.is("img") && c.attr("src", j.placeholder), c.one("appear", function() {
                    if (!this.loaded) {
                        if (j.appear) {
                            var d = i.length;
                            j.appear.call(b, d, j) }
                        a("<img />").bind("load", function() {
                            var d = c.attr("data-" + j.data_attribute);
                            c.hide(), c.is("img") ? c.attr("src", d) : c.css("background-image", "url('" + d + "')"), c[j.effect](j.effect_speed), b.loaded = !0;
                            var e = a.grep(i, function(a) {
                                return !a.loaded });
                            if (i = a(e), j.load) {
                                var f = i.length;
                                j.load.call(b, f, j) } }).attr("src", c.attr("data-" + j.data_attribute)) } }), 0 !== j.event.indexOf("scroll") && c.bind(j.event, function() { b.loaded || c.trigger("appear") }) }), e.bind("resize", function() { g() }), /(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion) && e.bind("pageshow", function(b) { b.originalEvent && b.originalEvent.persisted && i.each(function() { a(this).trigger("appear") }) }), a(c).ready(function() {
                g()
            }), this
        }, a.belowthefold = function(c, f) {
            var g;
            return g = f.container === d || f.container === b ? (b.innerHeight ? b.innerHeight : e.height()) + e.scrollTop() : a(f.container).offset().top + a(f.container).height(), g <= a(c).offset().top - f.threshold }, a.rightoffold = function(c, f) {
            var g;
            return g = f.container === d || f.container === b ? e.width() + e.scrollLeft() : a(f.container).offset().left + a(f.container).width(), g <= a(c).offset().left - f.threshold }, a.abovethetop = function(c, f) {
            var g;
            return g = f.container === d || f.container === b ? e.scrollTop() : a(f.container).offset().top, g >= a(c).offset().top + f.threshold + a(c).height() }, a.leftofbegin = function(c, f) {
            var g;
            return g = f.container === d || f.container === b ? e.scrollLeft() : a(f.container).offset().left, g >= a(c).offset().left + f.threshold + a(c).width() }, a.inviewport = function(b, c) {
            return !(a.rightoffold(b, c) || a.leftofbegin(b, c) || a.belowthefold(b, c) || a.abovethetop(b, c)) }, a.extend(a.expr[":"], { "below-the-fold": function(b) {
                return a.belowthefold(b, { threshold: 0 }) }, "above-the-top": function(b) {
                return !a.belowthefold(b, { threshold: 0 }) }, "right-of-screen": function(b) {
                return a.rightoffold(b, { threshold: 0 }) }, "left-of-screen": function(b) {
                return !a.rightoffold(b, { threshold: 0 }) }, "in-viewport": function(b) {
                return a.inviewport(b, { threshold: 0 }) }, "above-the-fold": function(b) {
                return !a.belowthefold(b, { threshold: 0 }) }, "right-of-fold": function(b) {
                return a.rightoffold(b, { threshold: 0 }) }, "left-of-fold": function(b) {
                return !a.rightoffold(b, { threshold: 0 }) } })
    }(jQuery, window, document),
    function(factory) { "use strict"; "function" == typeof define && define.amd ? define(["jquery"], factory) : "undefined" != typeof module && module.exports ? module.exports = factory(require("jquery")) : factory(jQuery) }(function($) {
        var _previousResizeWidth = -1,
            _updateTimeout = -1,
            _parse = function(value) {
                return parseFloat(value) || 0 },
            _rows = function(elements) {
                var tolerance = 1,
                    $elements = $(elements),
                    lastTop = null,
                    rows = [];
                return $elements.each(function() {
                    var $that = $(this),
                        top = $that.offset().top - _parse($that.css("margin-top")),
                        lastRow = rows.length > 0 ? rows[rows.length - 1] : null;
                    null === lastRow ? rows.push($that) : Math.floor(Math.abs(lastTop - top)) <= tolerance ? rows[rows.length - 1] = lastRow.add($that) : rows.push($that), lastTop = top }), rows },
            _parseOptions = function(options) {
                var opts = { byRow: !0, property: "height", target: null, remove: !1 };
                return "object" == typeof options ? $.extend(opts, options) : ("boolean" == typeof options ? opts.byRow = options : "remove" === options && (opts.remove = !0), opts) },
            matchHeight = $.fn.matchHeight = function(options) {
                var opts = _parseOptions(options);
                if (opts.remove) {
                    var that = this;
                    return this.css(opts.property, ""), $.each(matchHeight._groups, function(key, group) { group.elements = group.elements.not(that) }), this }
                return this.length <= 1 && !opts.target ? this : (matchHeight._groups.push({ elements: this, options: opts }), matchHeight._apply(this, opts), this) };
        matchHeight.version = "master", matchHeight._groups = [], matchHeight._throttle = 80, matchHeight._maintainScroll = !1, matchHeight._beforeUpdate = null, matchHeight._afterUpdate = null, matchHeight._rows = _rows, matchHeight._parse = _parse, matchHeight._parseOptions = _parseOptions, matchHeight._apply = function(elements, options) {
            var opts = _parseOptions(options),
                $elements = $(elements),
                rows = [$elements],
                scrollTop = $(window).scrollTop(),
                htmlHeight = $("html").outerHeight(!0),
                $hiddenParents = $elements.parents().filter(":hidden");
            return $hiddenParents.each(function() {
                var $that = $(this);
                $that.data("style-cache", $that.attr("style")) }), $hiddenParents.css("display", "block"), opts.byRow && !opts.target && ($elements.each(function() {
                var $that = $(this),
                    display = $that.css("display"); "inline-block" !== display && "flex" !== display && "inline-flex" !== display && (display = "block"), $that.data("style-cache", $that.attr("style")), $that.css({ display: display, "padding-top": "0", "padding-bottom": "0", "margin-top": "0", "margin-bottom": "0", "border-top-width": "0", "border-bottom-width": "0", height: "100px", overflow: "hidden" }) }), rows = _rows($elements), $elements.each(function() {
                var $that = $(this);
                $that.attr("style", $that.data("style-cache") || "") })), $.each(rows, function(key, row) {
                var $row = $(row),
                    targetHeight = 0;
                if (opts.target) targetHeight = opts.target.outerHeight(!1);
                else {
                    if (opts.byRow && $row.length <= 1) return void $row.css(opts.property, "");
                    $row.each(function() {
                        var $that = $(this),
                            style = $that.attr("style"),
                            display = $that.css("display"); "inline-block" !== display && "flex" !== display && "inline-flex" !== display && (display = "block");
                        var css = { display: display };
                        css[opts.property] = "", $that.css(css), $that.outerHeight(!1) > targetHeight && (targetHeight = $that.outerHeight(!1)), style ? $that.attr("style", style) : $that.css("display", "") }) }
                $row.each(function() {
                    var $that = $(this),
                        verticalPadding = 0;
                    opts.target && $that.is(opts.target) || ("border-box" !== $that.css("box-sizing") && (verticalPadding += _parse($that.css("border-top-width")) + _parse($that.css("border-bottom-width")), verticalPadding += _parse($that.css("padding-top")) + _parse($that.css("padding-bottom"))), $that.css(opts.property, targetHeight - verticalPadding + "px")) }) }), $hiddenParents.each(function() {
                var $that = $(this);
                $that.attr("style", $that.data("style-cache") || null) }), matchHeight._maintainScroll && $(window).scrollTop(scrollTop / htmlHeight * $("html").outerHeight(!0)), this }, matchHeight._applyDataApi = function() {
            var groups = {};
            $("[data-match-height], [data-mh]").each(function() {
                var $this = $(this),
                    groupId = $this.attr("data-mh") || $this.attr("data-match-height");
                groups[groupId] = groupId in groups ? groups[groupId].add($this) : $this }), $.each(groups, function() { this.matchHeight(!0) }) };
        var _update = function(event) { matchHeight._beforeUpdate && matchHeight._beforeUpdate(event, matchHeight._groups), $.each(matchHeight._groups, function() { matchHeight._apply(this.elements, this.options) }), matchHeight._afterUpdate && matchHeight._afterUpdate(event, matchHeight._groups) };
        matchHeight._update = function(throttle, event) {
            if (event && "resize" === event.type) {
                var windowWidth = $(window).width();
                if (windowWidth === _previousResizeWidth) return;
                _previousResizeWidth = windowWidth }
            throttle ? -1 === _updateTimeout && (_updateTimeout = setTimeout(function() { _update(event), _updateTimeout = -1 }, matchHeight._throttle)) : _update(event) }, $(matchHeight._applyDataApi), $(window).bind("load", function(event) { matchHeight._update(!1, event) }), $(window).bind("resize orientationchange", function(event) { matchHeight._update(!0, event) }) }),
    function() {
        var b, f;
        b = this.jQuery || window.jQuery, f = b(window), b.fn.stick_in_parent = function(d) {
            var A, w, J, n, B, K, p, q, k, E, t;
            for (null == d && (d = {}), t = d.sticky_class, B = d.inner_scrolling, E = d.recalc_every, k = d.parent, q = d.offset_top, p = d.spacer, w = d.bottoming, null == q && (q = 0), null == k && (k = void 0), null == B && (B = !0), null == t && (t = "is_stuck"), A = b(document), null == w && (w = !0), J = function(a, d, n, C, F, u, r, G) {
                    var v, H, m, D, I, c, g, x, y, z, h, l;
                    if (!a.data("sticky_kit")) {
                        if (a.data("sticky_kit", !0), I = A.height(), g = a.parent(), null != k && (g = g.closest(k)), !g.length) throw "failed to find stick parent";
                        if (v = m = !1, (h = null != p ? p && a.closest(p) : b("<div />")) && h.css("position", a.css("position")), x = function() {
                                var c, f, e;
                                return !G && (I = A.height(), c = parseInt(g.css("border-top-width"), 10), f = parseInt(g.css("padding-top"), 10), d = parseInt(g.css("padding-bottom"), 10), n = g.offset().top + c + f, C = g.height(), m && (v = m = !1, null == p && (a.insertAfter(h), h.detach()), a.css({ position: "", top: "", width: "", bottom: "" }).removeClass(t), e = !0), F = a.offset().top - (parseInt(a.css("margin-top"), 10) || 0) - q, u = a.outerHeight(!0), r = a.css("float"), h && h.css({ width: a.outerWidth(!0), height: u, display: a.css("display"), "vertical-align": a.css("vertical-align"), "float": r }), e) ? l() : void 0 }, x(), u !== C) return D = void 0, c = q, z = E, l = function() {
                            var b, l, e, k;
                            return !G && (e = !1, null != z && (--z, 0 >= z && (z = E, x(), e = !0)), e || A.height() === I || x(), e = f.scrollTop(), null != D && (l = e - D), D = e, m ? (w && (k = e + u + c > C + n, v && !k && (v = !1, a.css({ position: "fixed", bottom: "", top: c }).trigger("sticky_kit:unbottom"))), F > e && (m = !1, c = q, null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.detach()), b = { position: "", width: "", top: "" }, a.css(b).removeClass(t).trigger("sticky_kit:unstick")), B && (b = f.height(), u + q > b && !v && (c -= l, c = Math.max(b - u, c), c = Math.min(q, c), m && a.css({ top: c + "px" })))) : e > F && (m = !0, b = { position: "fixed", top: c }, b.width = "border-box" === a.css("box-sizing") ? a.outerWidth() + "px" : a.width() + "px", a.css(b).addClass(t), null == p && (a.after(h), "left" !== r && "right" !== r || h.append(a)), a.trigger("sticky_kit:stick")), m && w && (null == k && (k = e + u + c > C + n), !v && k)) ? (v = !0, "static" === g.css("position") && g.css({ position: "relative" }), a.css({ position: "absolute", bottom: d, top: "auto" }).trigger("sticky_kit:bottom")) : void 0 }, y = function() {
                            return x(), l() }, H = function() {
                            return G = !0, f.off("touchmove", l), f.off("scroll", l), f.off("resize", y), b(document.body).off("sticky_kit:recalc", y), a.off("sticky_kit:detach", H), a.removeData("sticky_kit"), a.css({ position: "", bottom: "", top: "", width: "" }), g.position("position", ""), m ? (null == p && ("left" !== r && "right" !== r || a.insertAfter(h), h.remove()), a.removeClass(t)) : void 0 }, f.on("touchmove", l), f.on("scroll", l), f.on("resize", y), b(document.body).on("sticky_kit:recalc", y), a.on("sticky_kit:detach", H), setTimeout(l, 0) } }, n = 0, K = this.length; K > n; n++) d = this[n], J(b(d));
            return this } }.call(this),
    function($, window, undefined) { "use strict";
        $.fn.tabslet = function(options) {
            var defaults = { mouseevent: "click", attribute: "href", animation: !1, autorotate: !1, deeplinking: !1, pauseonhover: !0, delay: 2e3, active: 1, container: !1, controls: { prev: ".prev", next: ".next" } },
                options = $.extend(defaults, options);
            return this.each(function() {
                var $this = $(this),
                    _cache_li = [],
                    _cache_div = [],
                    _container = options.container ? $(options.container) : $this,
                    _tabs = _container.find("> div");
                _tabs.each(function() { _cache_div.push($(this).css("display")) });
                var elements = $this.find("> ul > li"),
                    i = options.active - 1;
                if (!$this.data("tabslet-init")) { $this.data("tabslet-init", !0), $this.opts = [], $.map(["mouseevent", "attribute", "animation", "autorotate", "deeplinking", "pauseonhover", "delay", "container"], function(val, i) { $this.opts[val] = $this.data(val) || options[val] }), $this.opts.active = $this.opts.deeplinking ? deep_link() : $this.data("active") || options.active, _tabs.hide(), $this.opts.active && (_tabs.eq($this.opts.active - 1).show(), elements.eq($this.opts.active - 1).addClass("active"));
                    var fn = eval(function(e, tab) {
                            var _this = tab ? elements.find("a[" + $this.opts.attribute + "=" + tab + "]").parent() : $(this);
                            _this.trigger("_before"), elements.removeClass("active"), _this.addClass("active"), _tabs.hide(), i = elements.index(_this);
                            var currentTab = tab || _this.find("a").attr($this.opts.attribute);
                            return $this.opts.deeplinking && (location.hash = currentTab), $this.opts.animation ? _container.find(currentTab).animate({ opacity: "show" }, "slow", function() { _this.trigger("_after") }) : (_container.find(currentTab).show(), _this.trigger("_after")), !1 }),
                        init = eval("elements." + $this.opts.mouseevent + "(fn)"),
                        t, forward = function() { i = ++i % elements.length, "hover" == $this.opts.mouseevent ? elements.eq(i).trigger("mouseover") : elements.eq(i).click(), $this.opts.autorotate && (clearTimeout(t), t = setTimeout(forward, $this.opts.delay), $this.mouseover(function() { $this.opts.pauseonhover && clearTimeout(t) })) };
                    $this.opts.autorotate && (t = setTimeout(forward, $this.opts.delay), $this.hover(function() { $this.opts.pauseonhover && clearTimeout(t) }, function() { t = setTimeout(forward, $this.opts.delay) }), $this.opts.pauseonhover && $this.on("mouseleave", function() { clearTimeout(t), t = setTimeout(forward, $this.opts.delay) }));
                    var deep_link = function() {
                            var ids = [];
                            elements.find("a").each(function() { ids.push($(this).attr($this.opts.attribute)) });
                            var index = $.inArray(location.hash, ids);
                            return index > -1 ? index + 1 : $this.data("active") || options.active },
                        move = function(direction) { "forward" == direction && (i = ++i % elements.length), "backward" == direction && (i = --i % elements.length), elements.eq(i).click() };
                    $this.find(options.controls.next).click(function() { move("forward") }), $this.find(options.controls.prev).click(function() { move("backward") }), $this.on("show", function(e, tab) { fn(e, tab) }), $this.on("next", function() { move("forward") }), $this.on("prev", function() { move("backward") }), $this.on("destroy", function() { $(this).removeData().find("> ul li").each(function(i) { $(this).removeClass("active") }), _tabs.each(function(i) { $(this).removeAttr("style").css("display", _cache_div[i]) }) }) } }) }, $(document).ready(function() { $('[data-toggle="tabslet"]').tabslet() }) }(jQuery),
    function($, window, document, undefined) {
        function Owl(element, options) { this.settings = null, this.options = $.extend({}, Owl.Defaults, options), this.$element = $(element), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = { time: null, target: null, pointer: null, stage: { start: null, current: null }, direction: null }, this._states = { current: {}, tags: { initializing: ["busy"], animating: ["busy"], dragging: ["interacting"] } }, $.each(["onResize", "onThrottledResize"], $.proxy(function(i, handler) { this._handlers[handler] = $.proxy(this[handler], this) }, this)), $.each(Owl.Plugins, $.proxy(function(key, plugin) { this._plugins[key.charAt(0).toLowerCase() + key.slice(1)] = new plugin(this) }, this)), $.each(Owl.Workers, $.proxy(function(priority, worker) { this._pipe.push({ filter: worker.filter, run: $.proxy(worker.run, this) }) }, this)), this.setup(), this.initialize() }
        Owl.Defaults = { items: 3, loop: !1, center: !1, rewind: !1, mouseDrag: !0, touchDrag: !0, pullDrag: !0, freeDrag: !1, margin: 0, stagePadding: 0, merge: !1, mergeFit: !0, autoWidth: !1, startPosition: 0, rtl: !1, smartSpeed: 250, fluidSpeed: !1, dragEndSpeed: !1, responsive: {}, responsiveRefreshRate: 200, responsiveBaseElement: window, fallbackEasing: "swing", info: !1, nestedItemSelector: !1, itemElement: "div", stageElement: "div", refreshClass: "owl-refresh", loadedClass: "owl-loaded", loadingClass: "owl-loading", rtlClass: "owl-rtl", responsiveClass: "owl-responsive", dragClass: "owl-drag", itemClass: "owl-item", stageClass: "owl-stage", stageOuterClass: "owl-stage-outer", grabClass: "owl-grab" }, Owl.Width = { Default: "default", Inner: "inner", Outer: "outer" }, Owl.Type = { Event: "event", State: "state" }, Owl.Plugins = {}, Owl.Workers = [{ filter: ["width", "settings"], run: function() { this._width = this.$element.width() } }, { filter: ["width", "items", "settings"], run: function(cache) { cache.current = this._items && this._items[this.relative(this._current)] } }, { filter: ["items", "settings"], run: function() { this.$stage.children(".cloned").remove() } }, { filter: ["width", "items", "settings"], run: function(cache) {
                var margin = this.settings.margin || "",
                    grid = !this.settings.autoWidth,
                    rtl = this.settings.rtl,
                    css = { width: "auto", "margin-left": rtl ? margin : "", "margin-right": rtl ? "" : margin };!grid && this.$stage.children().css(css), cache.css = css } }, { filter: ["width", "items", "settings"], run: function(cache) {
                var width = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
                    merge = null,
                    iterator = this._items.length,
                    grid = !this.settings.autoWidth,
                    widths = [];
                for (cache.items = { merge: !1, width: width }; iterator--;) merge = this._mergers[iterator], merge = this.settings.mergeFit && Math.min(merge, this.settings.items) || merge, cache.items.merge = merge > 1 || cache.items.merge, widths[iterator] = grid ? width * merge : this._items[iterator].width();
                this._widths = widths } }, { filter: ["items", "settings"], run: function() {
                var clones = [],
                    items = this._items,
                    settings = this.settings,
                    view = Math.max(2 * settings.items, 4),
                    size = 2 * Math.ceil(items.length / 2),
                    repeat = settings.loop && items.length ? settings.rewind ? view : Math.max(view, size) : 0,
                    append = "",
                    prepend = "";
                for (repeat /= 2; repeat--;) clones.push(this.normalize(clones.length / 2, !0)), append += items[clones[clones.length - 1]][0].outerHTML, clones.push(this.normalize(items.length - 1 - (clones.length - 1) / 2, !0)), prepend = items[clones[clones.length - 1]][0].outerHTML + prepend;
                this._clones = clones, $(append).addClass("cloned").appendTo(this.$stage), $(prepend).addClass("cloned").prependTo(this.$stage) } }, { filter: ["width", "items", "settings"], run: function() {
                for (var rtl = this.settings.rtl ? 1 : -1, size = this._clones.length + this._items.length, iterator = -1, previous = 0, current = 0, coordinates = []; ++iterator < size;) previous = coordinates[iterator - 1] || 0, current = this._widths[this.relative(iterator)] + this.settings.margin, coordinates.push(previous + current * rtl);
                this._coordinates = coordinates } }, { filter: ["width", "items", "settings"], run: function() {
                var padding = this.settings.stagePadding,
                    coordinates = this._coordinates,
                    css = { width: Math.ceil(Math.abs(coordinates[coordinates.length - 1])) + 2 * padding, "padding-left": padding || "", "padding-right": padding || "" };
                this.$stage.css(css) } }, { filter: ["width", "items", "settings"], run: function(cache) {
                var iterator = this._coordinates.length,
                    grid = !this.settings.autoWidth,
                    items = this.$stage.children();
                if (grid && cache.items.merge)
                    for (; iterator--;) cache.css.width = this._widths[this.relative(iterator)], items.eq(iterator).css(cache.css);
                else grid && (cache.css.width = cache.items.width, items.css(cache.css)) } }, { filter: ["items"], run: function() { this._coordinates.length < 1 && this.$stage.removeAttr("style") } }, { filter: ["width", "items", "settings"], run: function(cache) { cache.current = cache.current ? this.$stage.children().index(cache.current) : 0, cache.current = Math.max(this.minimum(), Math.min(this.maximum(), cache.current)), this.reset(cache.current) } }, { filter: ["position"], run: function() { this.animate(this.coordinates(this._current)) } }, { filter: ["width", "position", "items", "settings"], run: function() {
                var inner, outer, i, n, rtl = this.settings.rtl ? 1 : -1,
                    padding = 2 * this.settings.stagePadding,
                    begin = this.coordinates(this.current()) + padding,
                    end = begin + this.width() * rtl,
                    matches = [];
                for (i = 0, n = this._coordinates.length; n > i; i++) inner = this._coordinates[i - 1] || 0, outer = Math.abs(this._coordinates[i]) + padding * rtl, (this.op(inner, "<=", begin) && this.op(inner, ">", end) || this.op(outer, "<", begin) && this.op(outer, ">", end)) && matches.push(i);
                this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + matches.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center")) } }], Owl.prototype.initialize = function() {
            if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
                var imgs, nestedSelector, width;
                imgs = this.$element.find("img"), nestedSelector = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : undefined, width = this.$element.children(nestedSelector).width(), imgs.length && 0 >= width && this.preloadAutoWidthImages(imgs) }
            this.$element.addClass(this.options.loadingClass), this.$stage = $("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized") }, Owl.prototype.setup = function() {
            var viewport = this.viewport(),
                overwrites = this.options.responsive,
                match = -1,
                settings = null;
            overwrites ? ($.each(overwrites, function(breakpoint) { viewport >= breakpoint && breakpoint > match && (match = Number(breakpoint)) }), settings = $.extend({}, this.options, overwrites[match]), delete settings.responsive, settings.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + match))) : settings = $.extend({}, this.options), (null === this.settings || this._breakpoint !== match) && (this.trigger("change", { property: { name: "settings", value: settings } }), this._breakpoint = match, this.settings = settings, this.invalidate("settings"), this.trigger("changed", { property: { name: "settings", value: this.settings } })) }, Owl.prototype.optionsLogic = function() { this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1) }, Owl.prototype.prepare = function(item) {
            var event = this.trigger("prepare", { content: item });
            return event.data || (event.data = $("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(item)), this.trigger("prepared", { content: event.data }), event.data }, Owl.prototype.update = function() {
            for (var i = 0, n = this._pipe.length, filter = $.proxy(function(p) {
                    return this[p] }, this._invalidated), cache = {}; n > i;)(this._invalidated.all || $.grep(this._pipe[i].filter, filter).length > 0) && this._pipe[i].run(cache), i++;
            this._invalidated = {}, !this.is("valid") && this.enter("valid") }, Owl.prototype.width = function(dimension) {
            switch (dimension = dimension || Owl.Width.Default) {
                case Owl.Width.Inner:
                case Owl.Width.Outer:
                    return this._width;
                default:
                    return this._width - 2 * this.settings.stagePadding + this.settings.margin } }, Owl.prototype.refresh = function() { this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed") }, Owl.prototype.onThrottledResize = function() { window.clearTimeout(this.resizeTimer), this.resizeTimer = window.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate) }, Owl.prototype.onResize = function() {
            return this._items.length ? this._width === this.$element.width() ? !1 : this.$element.is(":visible") ? (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized"))) : !1 : !1 }, Owl.prototype.registerEventHandlers = function() { $.support.transition && this.$stage.on($.support.transition.end + ".owl.core", $.proxy(this.onTransitionEnd, this)), this.settings.responsive !== !1 && this.on(window, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", $.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
                return !1 })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", $.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", $.proxy(this.onDragEnd, this))) }, Owl.prototype.onDragStart = function(event) {
            var stage = null;
            3 !== event.which && ($.support.transform ? (stage = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), stage = { x: stage[16 === stage.length ? 12 : 4], y: stage[16 === stage.length ? 13 : 5] }) : (stage = this.$stage.position(), stage = { x: this.settings.rtl ? stage.left + this.$stage.width() - this.width() + this.settings.margin : stage.left, y: stage.top }), this.is("animating") && ($.support.transform ? this.animate(stage.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === event.type), this.speed(0), this._drag.time = (new Date).getTime(), this._drag.target = $(event.target), this._drag.stage.start = stage, this._drag.stage.current = stage, this._drag.pointer = this.pointer(event), $(document).on("mouseup.owl.core touchend.owl.core", $.proxy(this.onDragEnd, this)), $(document).one("mousemove.owl.core touchmove.owl.core", $.proxy(function(event) {
                var delta = this.difference(this._drag.pointer, this.pointer(event));
                $(document).on("mousemove.owl.core touchmove.owl.core", $.proxy(this.onDragMove, this)), Math.abs(delta.x) < Math.abs(delta.y) && this.is("valid") || (event.preventDefault(), this.enter("dragging"), this.trigger("drag")) }, this))) }, Owl.prototype.onDragMove = function(event) {
            var minimum = null,
                maximum = null,
                pull = null,
                delta = this.difference(this._drag.pointer, this.pointer(event)),
                stage = this.difference(this._drag.stage.start, delta);
            this.is("dragging") && (event.preventDefault(), this.settings.loop ? (minimum = this.coordinates(this.minimum()), maximum = this.coordinates(this.maximum() + 1) - minimum, stage.x = ((stage.x - minimum) % maximum + maximum) % maximum + minimum) : (minimum = this.coordinates(this.settings.rtl ? this.maximum() : this.minimum()), maximum = this.coordinates(this.settings.rtl ? this.minimum() : this.maximum()), pull = this.settings.pullDrag ? -1 * delta.x / 5 : 0, stage.x = Math.max(Math.min(stage.x, minimum + pull), maximum + pull)), this._drag.stage.current = stage, this.animate(stage.x)) }, Owl.prototype.onDragEnd = function(event) {
            var delta = this.difference(this._drag.pointer, this.pointer(event)),
                stage = this._drag.stage.current,
                direction = delta.x > 0 ^ this.settings.rtl ? "left" : "right";
            $(document).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== delta.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(stage.x, 0 !== delta.x ? direction : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = direction, (Math.abs(delta.x) > 3 || (new Date).getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function() {
                return !1 })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged")) }, Owl.prototype.closest = function(coordinate, direction) {
            var position = -1,
                pull = 30,
                width = this.width(),
                coordinates = this.coordinates();
            return this.settings.freeDrag || $.each(coordinates, $.proxy(function(index, value) {
                return coordinate > value - pull && value + pull > coordinate ? position = index : this.op(coordinate, "<", value) && this.op(coordinate, ">", coordinates[index + 1] || value - width) && (position = "left" === direction ? index + 1 : index), -1 === position }, this)), this.settings.loop || (this.op(coordinate, ">", coordinates[this.minimum()]) ? position = coordinate = this.minimum() : this.op(coordinate, "<", coordinates[this.maximum()]) && (position = coordinate = this.maximum())), position }, Owl.prototype.animate = function(coordinate) {
            var animate = this.speed() > 0;
            this.is("animating") && this.onTransitionEnd(), animate && (this.enter("animating"), this.trigger("translate")), $.support.transform3d && $.support.transition ? this.$stage.css({ transform: "translate3d(" + coordinate + "px,0px,0px)", transition: this.speed() / 1e3 + "s" }) : animate ? this.$stage.animate({ left: coordinate + "px" }, this.speed(), this.settings.fallbackEasing, $.proxy(this.onTransitionEnd, this)) : this.$stage.css({ left: coordinate + "px" }) }, Owl.prototype.is = function(state) {
            return this._states.current[state] && this._states.current[state] > 0 }, Owl.prototype.current = function(position) {
            if (position === undefined) return this._current;
            if (0 === this._items.length) return undefined;
            if (position = this.normalize(position), this._current !== position) {
                var event = this.trigger("change", { property: { name: "position", value: position } });
                event.data !== undefined && (position = this.normalize(event.data)), this._current = position, this.invalidate("position"), this.trigger("changed", { property: { name: "position", value: this._current } }) }
            return this._current }, Owl.prototype.invalidate = function(part) {
            return "string" === $.type(part) && (this._invalidated[part] = !0, this.is("valid") && this.leave("valid")), $.map(this._invalidated, function(v, i) {
                return i }) }, Owl.prototype.reset = function(position) { position = this.normalize(position), position !== undefined && (this._speed = 0, this._current = position, this.suppress(["translate", "translated"]), this.animate(this.coordinates(position)), this.release(["translate", "translated"])) }, Owl.prototype.normalize = function(position, relative) {
            var n = this._items.length,
                m = relative ? 0 : this._clones.length;
            return !$.isNumeric(position) || 1 > n ? position = undefined : (0 > position || position >= n + m) && (position = ((position - m / 2) % n + n) % n + m / 2), position }, Owl.prototype.relative = function(position) {
            return position -= this._clones.length / 2, this.normalize(position, !0) }, Owl.prototype.maximum = function(relative) {
            var j, settings = this.settings,
                maximum = this._coordinates.length,
                boundary = Math.abs(this._coordinates[maximum - 1]) - this._width,
                i = -1;
            if (settings.loop) maximum = this._clones.length / 2 + this._items.length - 1;
            else if (settings.autoWidth || settings.merge)
                for (; maximum - i > 1;) Math.abs(this._coordinates[j = maximum + i >> 1]) < boundary ? i = j : maximum = j;
            else maximum = settings.center ? this._items.length - 1 : this._items.length - settings.items;
            return relative && (maximum -= this._clones.length / 2), Math.max(maximum, 0) }, Owl.prototype.minimum = function(relative) {
            return relative ? 0 : this._clones.length / 2 }, Owl.prototype.items = function(position) {
            return position === undefined ? this._items.slice() : (position = this.normalize(position, !0), this._items[position]) }, Owl.prototype.mergers = function(position) {
            return position === undefined ? this._mergers.slice() : (position = this.normalize(position, !0), this._mergers[position]) }, Owl.prototype.clones = function(position) {
            var odd = this._clones.length / 2,
                even = odd + this._items.length,
                map = function(index) {
                    return index % 2 === 0 ? even + index / 2 : odd - (index + 1) / 2 };
            return position === undefined ? $.map(this._clones, function(v, i) {
                return map(i) }) : $.map(this._clones, function(v, i) {
                return v === position ? map(i) : null }) }, Owl.prototype.speed = function(speed) {
            return speed !== undefined && (this._speed = speed), this._speed }, Owl.prototype.coordinates = function(position) {
            var coordinate = null;
            return position === undefined ? $.map(this._coordinates, $.proxy(function(coordinate, index) {
                return this.coordinates(index) }, this)) : (this.settings.center ? (coordinate = this._coordinates[position], coordinate += (this.width() - coordinate + (this._coordinates[position - 1] || 0)) / 2 * (this.settings.rtl ? -1 : 1)) : coordinate = this._coordinates[position - 1] || 0, coordinate) }, Owl.prototype.duration = function(from, to, factor) {
            return Math.min(Math.max(Math.abs(to - from), 1), 6) * Math.abs(factor || this.settings.smartSpeed) }, Owl.prototype.to = function(position, speed) {
            var current = this.current(),
                revert = null,
                distance = position - this.relative(current),
                direction = (distance > 0) - (0 > distance),
                items = this._items.length,
                minimum = this.minimum(),
                maximum = this.maximum();
            this.settings.loop ? (!this.settings.rewind && Math.abs(distance) > items / 2 && (distance += -1 * direction * items), position = current + distance, revert = ((position - minimum) % items + items) % items + minimum, revert !== position && maximum >= revert - distance && revert - distance > 0 && (current = revert - distance, position = revert, this.reset(current))) : this.settings.rewind ? (maximum += 1, position = (position % maximum + maximum) % maximum) : position = Math.max(minimum, Math.min(maximum, position)), this.speed(this.duration(current, position, speed)), this.current(position), this.$element.is(":visible") && this.update() }, Owl.prototype.next = function(speed) { speed = speed || !1, this.to(this.relative(this.current()) + 1, speed) }, Owl.prototype.prev = function(speed) { speed = speed || !1, this.to(this.relative(this.current()) - 1, speed) }, Owl.prototype.onTransitionEnd = function(event) {
            return event !== undefined && (event.stopPropagation(), (event.target || event.srcElement || event.originalTarget) !== this.$stage.get(0)) ? !1 : (this.leave("animating"), void this.trigger("translated")) }, Owl.prototype.viewport = function() {
            var width;
            if (this.options.responsiveBaseElement !== window) width = $(this.options.responsiveBaseElement).width();
            else if (window.innerWidth) width = window.innerWidth;
            else {
                if (!document.documentElement || !document.documentElement.clientWidth) throw "Can not detect viewport width.";
                width = document.documentElement.clientWidth }
            return width }, Owl.prototype.replace = function(content) { this.$stage.empty(), this._items = [], content && (content = content instanceof jQuery ? content : $(content)), this.settings.nestedItemSelector && (content = content.find("." + this.settings.nestedItemSelector)), content.filter(function() {
                return 1 === this.nodeType }).each($.proxy(function(index, item) { item = this.prepare(item), this.$stage.append(item), this._items.push(item), this._mergers.push(1 * item.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1) }, this)), this.reset($.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items") }, Owl.prototype.add = function(content, position) {
            var current = this.relative(this._current);
            position = position === undefined ? this._items.length : this.normalize(position, !0), content = content instanceof jQuery ? content : $(content), this.trigger("add", { content: content, position: position }), content = this.prepare(content), 0 === this._items.length || position === this._items.length ? (0 === this._items.length && this.$stage.append(content), 0 !== this._items.length && this._items[position - 1].after(content), this._items.push(content), this._mergers.push(1 * content.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1)) : (this._items[position].before(content), this._items.splice(position, 0, content), this._mergers.splice(position, 0, 1 * content.find("[data-merge]").andSelf("[data-merge]").attr("data-merge") || 1)), this._items[current] && this.reset(this._items[current].index()), this.invalidate("items"), this.trigger("added", { content: content, position: position }) }, Owl.prototype.remove = function(position) {
            position = this.normalize(position, !0), position !== undefined && (this.trigger("remove", { content: this._items[position], position: position }), this._items[position].remove(), this._items.splice(position, 1), this._mergers.splice(position, 1), this.invalidate("items"), this.trigger("removed", { content: null, position: position }))
        }, Owl.prototype.preloadAutoWidthImages = function(images) { images.each($.proxy(function(i, element) { this.enter("pre-loading"), element = $(element), $(new Image).one("load", $.proxy(function(e) { element.attr("src", e.target.src), element.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh() }, this)).attr("src", element.attr("src") || element.attr("data-src") || element.attr("data-src-retina")) }, this)) }, Owl.prototype.destroy = function() { this.$element.off(".owl.core"), this.$stage.off(".owl.core"), $(document).off(".owl.core"), this.settings.responsive !== !1 && (window.clearTimeout(this.resizeTimer), this.off(window, "resize", this._handlers.onThrottledResize));
            for (var i in this._plugins) this._plugins[i].destroy();
            this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel") }, Owl.prototype.op = function(a, o, b) {
            var rtl = this.settings.rtl;
            switch (o) {
                case "<":
                    return rtl ? a > b : b > a;
                case ">":
                    return rtl ? b > a : a > b;
                case ">=":
                    return rtl ? b >= a : a >= b;
                case "<=":
                    return rtl ? a >= b : b >= a } }, Owl.prototype.on = function(element, event, listener, capture) { element.addEventListener ? element.addEventListener(event, listener, capture) : element.attachEvent && element.attachEvent("on" + event, listener) }, Owl.prototype.off = function(element, event, listener, capture) { element.removeEventListener ? element.removeEventListener(event, listener, capture) : element.detachEvent && element.detachEvent("on" + event, listener) }, Owl.prototype.trigger = function(name, data, namespace, state, enter) {
            var status = { item: { count: this._items.length, index: this.current() } },
                handler = $.camelCase($.grep(["on", name, namespace], function(v) {
                    return v }).join("-").toLowerCase()),
                event = $.Event([name, "owl", namespace || "carousel"].join(".").toLowerCase(), $.extend({ relatedTarget: this }, status, data));
            return this._supress[name] || ($.each(this._plugins, function(name, plugin) { plugin.onTrigger && plugin.onTrigger(event) }), this.register({ type: Owl.Type.Event, name: name }), this.$element.trigger(event), this.settings && "function" == typeof this.settings[handler] && this.settings[handler].call(this, event)), event }, Owl.prototype.enter = function(name) { $.each([name].concat(this._states.tags[name] || []), $.proxy(function(i, name) { this._states.current[name] === undefined && (this._states.current[name] = 0), this._states.current[name]++ }, this)) }, Owl.prototype.leave = function(name) { $.each([name].concat(this._states.tags[name] || []), $.proxy(function(i, name) { this._states.current[name]-- }, this)) }, Owl.prototype.register = function(object) {
            if (object.type === Owl.Type.Event) {
                if ($.event.special[object.name] || ($.event.special[object.name] = {}), !$.event.special[object.name].owl) {
                    var _default = $.event.special[object.name]._default;
                    $.event.special[object.name]._default = function(e) {
                        return !_default || !_default.apply || e.namespace && -1 !== e.namespace.indexOf("owl") ? e.namespace && e.namespace.indexOf("owl") > -1 : _default.apply(this, arguments) }, $.event.special[object.name].owl = !0 } } else object.type === Owl.Type.State && (this._states.tags[object.name] = this._states.tags[object.name] ? this._states.tags[object.name].concat(object.tags) : object.tags, this._states.tags[object.name] = $.grep(this._states.tags[object.name], $.proxy(function(tag, i) {
                return $.inArray(tag, this._states.tags[object.name]) === i }, this))) }, Owl.prototype.suppress = function(events) { $.each(events, $.proxy(function(index, event) { this._supress[event] = !0 }, this)) }, Owl.prototype.release = function(events) { $.each(events, $.proxy(function(index, event) { delete this._supress[event] }, this)) }, Owl.prototype.pointer = function(event) {
            var result = { x: null, y: null };
            return event = event.originalEvent || event || window.event, event = event.touches && event.touches.length ? event.touches[0] : event.changedTouches && event.changedTouches.length ? event.changedTouches[0] : event, event.pageX ? (result.x = event.pageX, result.y = event.pageY) : (result.x = event.clientX, result.y = event.clientY), result }, Owl.prototype.difference = function(first, second) {
            return { x: first.x - second.x, y: first.y - second.y } }, $.fn.owlCarousel = function(option) {
            var args = Array.prototype.slice.call(arguments, 1);
            return this.each(function() {
                var $this = $(this),
                    data = $this.data("owl.carousel");
                data || (data = new Owl(this, "object" == typeof option && option), $this.data("owl.carousel", data), $.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function(i, event) { data.register({ type: Owl.Type.Event, name: event }), data.$element.on(event + ".owl.carousel.core", $.proxy(function(e) { e.namespace && e.relatedTarget !== this && (this.suppress([event]), data[event].apply(this, [].slice.call(arguments, 1)), this.release([event])) }, data)) })), "string" == typeof option && "_" !== option.charAt(0) && data[option].apply(data, args) }) }, $.fn.owlCarousel.Constructor = Owl
    }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        var AutoRefresh = function(carousel) { this._core = carousel, this._interval = null, this._visible = null, this._handlers = { "initialized.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.autoRefresh && this.watch() }, this) }, this._core.options = $.extend({}, AutoRefresh.Defaults, this._core.options), this._core.$element.on(this._handlers) };
        AutoRefresh.Defaults = { autoRefresh: !0, autoRefreshInterval: 500 }, AutoRefresh.prototype.watch = function() { this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = window.setInterval($.proxy(this.refresh, this), this._core.settings.autoRefreshInterval)) }, AutoRefresh.prototype.refresh = function() { this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh()) }, AutoRefresh.prototype.destroy = function() {
            var handler, property;
            window.clearInterval(this._interval);
            for (handler in this._handlers) this._core.$element.off(handler, this._handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.AutoRefresh = AutoRefresh }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        var Lazy = function(carousel) { this._core = carousel, this._loaded = [], this._handlers = { "initialized.owl.carousel change.owl.carousel": $.proxy(function(e) {
                    if (e.namespace && this._core.settings && this._core.settings.lazyLoad && (e.property && "position" == e.property.name || "initialized" == e.type)) {
                        var settings = this._core.settings,
                            n = settings.center && Math.ceil(settings.items / 2) || settings.items,
                            i = settings.center && -1 * n || 0,
                            position = (e.property && e.property.value || this._core.current()) + i,
                            clones = this._core.clones().length,
                            load = $.proxy(function(i, v) { this.load(v) }, this);
                        for (settings.lazyLoadEager && (n += settings.lazyLoadEager + 1), settings.lazyLoadEager && (position -= settings.lazyLoadEager); i++ < n;) this.load(clones / 2 + this._core.relative(position)), clones && $.each(this._core.clones(this._core.relative(position)), load), position++ } }, this) }, this._core.options = $.extend({}, Lazy.Defaults, this._core.options), this._core.$element.on(this._handlers) };
        Lazy.Defaults = { lazyLoad: !1, lazyLoadEager: 1 }, Lazy.prototype.load = function(position) {
            var $item = this._core.$stage.children().eq(position),
                $elements = $item && $item.find(".owl-lazy");!$elements || $.inArray($item.get(0), this._loaded) > -1 || ($elements.each($.proxy(function(index, element) {
                var image, $element = $(element),
                    url = window.devicePixelRatio > 1 && $element.attr("data-src-retina") || $element.attr("data-src");
                this._core.trigger("load", { element: $element, url: url }, "lazy"), $element.is("img") ? $element.one("load.owl.lazy", $.proxy(function() { $element.css("opacity", 1), this._core.trigger("loaded", { element: $element, url: url }, "lazy") }, this)).attr("src", url) : (image = new Image, image.onload = $.proxy(function() { $element.css({ "background-image": "url(" + url + ")", opacity: "1" }), this._core.trigger("loaded", { element: $element, url: url }, "lazy") }, this), image.src = url) }, this)), this._loaded.push($item.get(0))) }, Lazy.prototype.destroy = function() {
            var handler, property;
            for (handler in this.handlers) this._core.$element.off(handler, this.handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.Lazy = Lazy }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        var AutoHeight = function(carousel) { this._core = carousel, this._handlers = { "initialized.owl.carousel refreshed.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.autoHeight && this.update() }, this), "changed.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.autoHeight && "position" == e.property.name && this.update() }, this), "loaded.owl.lazy": $.proxy(function(e) { e.namespace && this._core.settings.autoHeight && e.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update() }, this) }, this._core.options = $.extend({}, AutoHeight.Defaults, this._core.options), this._core.$element.on(this._handlers) };
        AutoHeight.Defaults = { autoHeight: !1, autoHeightClass: "owl-height" }, AutoHeight.prototype.update = function() {
            var start = this._core._current,
                end = start + this._core.settings.items,
                visible = this._core.$stage.children().toArray().slice(start, end);
            heights = [], maxheight = 0, $.each(visible, function(index, item) { heights.push($(item).height()) }), maxheight = Math.max.apply(null, heights), this._core.$stage.parent().height(maxheight).addClass(this._core.settings.autoHeightClass) }, AutoHeight.prototype.destroy = function() {
            var handler, property;
            for (handler in this._handlers) this._core.$element.off(handler, this._handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.AutoHeight = AutoHeight }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        var Video = function(carousel) { this._core = carousel, this._videos = {}, this._playing = null, this._handlers = { "initialized.owl.carousel": $.proxy(function(e) { e.namespace && this._core.register({ type: "state", name: "playing", tags: ["interacting"] }) }, this), "resize.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.video && this.isInFullScreen() && e.preventDefault() }, this), "refreshed.owl.carousel": $.proxy(function(e) { e.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove() }, this), "changed.owl.carousel": $.proxy(function(e) { e.namespace && "position" === e.property.name && this._playing && this.stop() }, this), "prepared.owl.carousel": $.proxy(function(e) {
                    if (e.namespace) {
                        var $element = $(e.content).find(".owl-video");
                        $element.length && ($element.css("display", "none"), this.fetch($element, $(e.content))) } }, this) }, this._core.options = $.extend({}, Video.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", $.proxy(function(e) { this.play(e) }, this)) };
        Video.Defaults = { video: !1, videoHeight: !1, videoWidth: !1 }, Video.prototype.fetch = function(target, item) {
            var type = function() {
                    return target.attr("data-vimeo-id") ? "vimeo" : target.attr("data-vzaar-id") ? "vzaar" : "youtube" }(),
                id = target.attr("data-vimeo-id") || target.attr("data-youtube-id") || target.attr("data-vzaar-id"),
                width = target.attr("data-width") || this._core.settings.videoWidth,
                height = target.attr("data-height") || this._core.settings.videoHeight,
                url = target.attr("href");
            if (!url) throw new Error("Missing video URL.");
            if (id = url.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), id[3].indexOf("youtu") > -1) type = "youtube";
            else if (id[3].indexOf("vimeo") > -1) type = "vimeo";
            else {
                if (!(id[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");
                type = "vzaar" }
            id = id[6], this._videos[url] = { type: type, id: id, width: width, height: height }, item.attr("data-video", url), this.thumbnail(target, this._videos[url]) }, Video.prototype.thumbnail = function(target, video) {
            var tnLink, icon, path, dimensions = video.width && video.height ? 'style="width:' + video.width + "px;height:" + video.height + 'px;"' : "",
                customTn = target.find("img"),
                srcType = "src",
                lazyClass = "",
                create = (this._core.settings, function(path) { icon = '<div class="owl-video-play-icon"></div>', tnLink = '<div class="owl-video-tn" style="opacity:1;background-image:url(' + path + ')"></div>', target.after(tnLink), target.after(icon) });
            return target.wrap('<div class="owl-video-wrapper"' + dimensions + "></div>"), this._core.settings.lazyLoad && (srcType = "data-src", lazyClass = "owl-lazy"), customTn.length ? (create(customTn.attr(srcType)), customTn.remove(), !1) : void("youtube" === video.type ? (path = "//img.youtube.com/vi/" + video.id + "/hqdefault.jpg", create(path)) : "vimeo" === video.type ? $.ajax({ type: "GET", url: "//vimeo.com/api/v2/video/" + video.id + ".json", jsonp: "callback", dataType: "jsonp", success: function(data) { path = data[0].thumbnail_large, create(path) } }) : "vzaar" === video.type && $.ajax({ type: "GET", url: "//vzaar.com/api/videos/" + video.id + ".json", jsonp: "callback", dataType: "jsonp", success: function(data) { path = data.framegrab_url, create(path) } })) }, Video.prototype.stop = function() { this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video") }, Video.prototype.play = function(event) {
            var html, target = $(event.target),
                item = target.closest("." + this._core.settings.itemClass),
                video = this._videos[item.attr("data-video")],
                width = video.width || "100%",
                height = video.height || this._core.$stage.height();
            this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), item = this._core.items(this._core.relative(item.index())), this._core.reset(item.index()), "youtube" === video.type ? html = '<iframe width="' + width + '" height="' + height + '" src="//www.youtube.com/embed/' + video.id + "?autoplay=1&v=" + video.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === video.type ? html = '<iframe src="//player.vimeo.com/video/' + video.id + '?autoplay=1" width="' + width + '" height="' + height + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === video.type && (html = '<iframe frameborder="0"height="' + height + '"width="' + width + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + video.id + '/player?autoplay=true"></iframe>'), $('<div class="owl-video-frame">' + html + "</div>").insertAfter(item.find(".owl-video")), this._playing = item.addClass("owl-video-playing")) }, Video.prototype.isInFullScreen = function() {
            var element = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
            return element && $(element).parent().hasClass("owl-video-frame") }, Video.prototype.destroy = function() {
            var handler, property;
            this._core.$element.off("click.owl.video");
            for (handler in this._handlers) this._core.$element.off(handler, this._handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.Video = Video }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        var Animate = function(scope) { this.core = scope, this.core.options = $.extend({}, Animate.Defaults, this.core.options), this.swapping = !0, this.previous = undefined, this.next = undefined, this.handlers = { "change.owl.carousel": $.proxy(function(e) { e.namespace && "position" == e.property.name && (this.previous = this.core.current(), this.next = e.property.value) }, this), "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": $.proxy(function(e) { e.namespace && (this.swapping = "translated" == e.type) }, this), "translate.owl.carousel": $.proxy(function(e) { e.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap() }, this) }, this.core.$element.on(this.handlers) };
        Animate.Defaults = { animateOut: !1, animateIn: !1 }, Animate.prototype.swap = function() {
            if (1 === this.core.settings.items && $.support.animation && $.support.transition) { this.core.speed(0);
                var left, clear = $.proxy(this.clear, this),
                    previous = this.core.$stage.children().eq(this.previous),
                    next = this.core.$stage.children().eq(this.next),
                    incoming = this.core.settings.animateIn,
                    outgoing = this.core.settings.animateOut;
                this.core.current() !== this.previous && (outgoing && (left = this.core.coordinates(this.previous) - this.core.coordinates(this.next), previous.one($.support.animation.end, clear).css({ left: left + "px" }).addClass("animated owl-animated-out").addClass(outgoing)), incoming && next.one($.support.animation.end, clear).addClass("animated owl-animated-in").addClass(incoming)) } }, Animate.prototype.clear = function(e) { $(e.target).css({ left: "" }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd() }, Animate.prototype.destroy = function() {
            var handler, property;
            for (handler in this.handlers) this.core.$element.off(handler, this.handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.Animate = Animate }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        var Autoplay = function(carousel) { this._core = carousel, this._interval = null, this._paused = !1, this._handlers = { "changed.owl.carousel": $.proxy(function(e) { e.namespace && "settings" === e.property.name && (this._core.settings.autoplay ? this.play() : this.stop()) }, this), "initialized.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.autoplay && this.play() }, this), "play.owl.autoplay": $.proxy(function(e, t, s) { e.namespace && this.play(t, s) }, this), "stop.owl.autoplay": $.proxy(function(e) { e.namespace && this.stop() }, this), "mouseover.owl.autoplay": $.proxy(function() { this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause() }, this), "mouseleave.owl.autoplay": $.proxy(function() { this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play() }, this) }, this._core.$element.on(this._handlers), this._core.options = $.extend({}, Autoplay.Defaults, this._core.options) };
        Autoplay.Defaults = { autoplay: !1, autoplayTimeout: 5e3, autoplayHoverPause: !1, autoplaySpeed: !1 }, Autoplay.prototype.play = function(timeout, speed) { this._paused = !1, this._core.is("rotating") || (this._core.enter("rotating"), this._interval = window.setInterval($.proxy(function() { this._paused || this._core.is("busy") || this._core.is("interacting") || document.hidden || this._core.next(speed || this._core.settings.autoplaySpeed) }, this), timeout || this._core.settings.autoplayTimeout)) }, Autoplay.prototype.stop = function() { this._core.is("rotating") && (window.clearInterval(this._interval), this._core.leave("rotating")) }, Autoplay.prototype.pause = function() { this._core.is("rotating") && (this._paused = !0) }, Autoplay.prototype.destroy = function() {
            var handler, property;
            this.stop();
            for (handler in this._handlers) this._core.$element.off(handler, this._handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.autoplay = Autoplay }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) { "use strict";
        var Navigation = function(carousel) { this._core = carousel, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = { next: this._core.next, prev: this._core.prev, to: this._core.to }, this._handlers = { "prepared.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + $(e.content).find("[data-dot]").andSelf("[data-dot]").attr("data-dot") + "</div>") }, this), "added.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 0, this._templates.pop()) }, this), "remove.owl.carousel": $.proxy(function(e) { e.namespace && this._core.settings.dotsData && this._templates.splice(e.position, 1) }, this), "changed.owl.carousel": $.proxy(function(e) { e.namespace && "position" == e.property.name && this.draw() }, this), "initialized.owl.carousel": $.proxy(function(e) { e.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation")) }, this), "refreshed.owl.carousel": $.proxy(function(e) { e.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation")) }, this) }, this._core.options = $.extend({}, Navigation.Defaults, this._core.options), this.$element.on(this._handlers) };
        Navigation.Defaults = { nav: !1, navText: ["prev", "next"], navSpeed: !1, navElement: "div", navContainer: !1, navContainerClass: "owl-nav", navClass: ["owl-prev", "owl-next"], slideBy: 1, dotClass: "owl-dot", dotsClass: "owl-dots", dots: !0, dotsEach: !1, dotsData: !1, dotsSpeed: !1, dotsContainer: !1 }, Navigation.prototype.initialize = function() {
            var override, settings = this._core.settings;
            this._controls.$relative = (settings.navContainer ? $(settings.navContainer) : $("<div>").addClass(settings.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = $("<" + settings.navElement + ">").addClass(settings.navClass[0]).html(settings.navText[0]).prependTo(this._controls.$relative).on("click", $.proxy(function(e) { this.prev(settings.navSpeed) }, this)), this._controls.$next = $("<" + settings.navElement + ">").addClass(settings.navClass[1]).html(settings.navText[1]).appendTo(this._controls.$relative).on("click", $.proxy(function(e) { this.next(settings.navSpeed) }, this)), settings.dotsData || (this._templates = [$("<div>").addClass(settings.dotClass).append($("<span>")).prop("outerHTML")]), this._controls.$absolute = (settings.dotsContainer ? $(settings.dotsContainer) : $("<div>").addClass(settings.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "div", $.proxy(function(e) {
                var index = $(e.target).parent().is(this._controls.$absolute) ? $(e.target).index() : $(e.target).parent().index();
                e.preventDefault(), this.to(index, settings.dotsSpeed) }, this));
            for (override in this._overrides) this._core[override] = $.proxy(this[override], this) }, Navigation.prototype.destroy = function() {
            var handler, control, property, override;
            for (handler in this._handlers) this.$element.off(handler, this._handlers[handler]);
            for (control in this._controls) this._controls[control].remove();
            for (override in this.overides) this._core[override] = this._overrides[override];
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, Navigation.prototype.update = function() {
            var i, j, k, lower = this._core.clones().length / 2,
                upper = lower + this._core.items().length,
                maximum = this._core.maximum(!0),
                settings = this._core.settings,
                size = settings.center || settings.autoWidth || settings.dotsData ? 1 : settings.dotsEach || settings.items;
            if ("page" !== settings.slideBy && (settings.slideBy = Math.min(settings.slideBy, settings.items)), settings.dots || "page" == settings.slideBy)
                for (this._pages = [], i = lower, j = 0, k = 0; upper > i; i++) {
                    if (j >= size || 0 === j) {
                        if (this._pages.push({ start: Math.min(maximum, i - lower), end: i - lower + size - 1 }), Math.min(maximum, i - lower) === maximum) break;
                        j = 0, ++k }
                    j += this._core.mergers(this._core.relative(i)) } }, Navigation.prototype.draw = function() {
            var difference, settings = this._core.settings,
                disabled = this._core.items().length <= settings.items,
                index = this._core.relative(this._core.current()),
                loop = settings.loop || settings.rewind;
            this._controls.$relative.toggleClass("disabled", !settings.nav || disabled), settings.nav && (this._controls.$previous.toggleClass("disabled", !loop && index <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !loop && index >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !settings.dots || disabled), settings.dots && (difference = this._pages.length - this._controls.$absolute.children().length, settings.dotsData && 0 !== difference ? this._controls.$absolute.html(this._templates.join("")) : difference > 0 ? this._controls.$absolute.append(new Array(difference + 1).join(this._templates[0])) : 0 > difference && this._controls.$absolute.children().slice(difference).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq($.inArray(this.current(), this._pages)).addClass("active")) }, Navigation.prototype.onTrigger = function(event) {
            var settings = this._core.settings;
            event.page = { index: $.inArray(this.current(), this._pages), count: this._pages.length, size: settings && (settings.center || settings.autoWidth || settings.dotsData ? 1 : settings.dotsEach || settings.items) } }, Navigation.prototype.current = function() {
            var current = this._core.relative(this._core.current());
            return $.grep(this._pages, $.proxy(function(page, index) {
                return page.start <= current && page.end >= current }, this)).pop() }, Navigation.prototype.getPosition = function(successor) {
            var position, length, settings = this._core.settings;
            return "page" == settings.slideBy ? (position = $.inArray(this.current(), this._pages), length = this._pages.length, successor ? ++position : --position, position = this._pages[(position % length + length) % length].start) : (position = this._core.relative(this._core.current()), length = this._core.items().length, successor ? position += settings.slideBy : position -= settings.slideBy), position }, Navigation.prototype.next = function(speed) { $.proxy(this._overrides.to, this._core)(this.getPosition(!0), speed) }, Navigation.prototype.prev = function(speed) { $.proxy(this._overrides.to, this._core)(this.getPosition(!1), speed) }, Navigation.prototype.to = function(position, speed, standard) {
            var length;
            standard ? $.proxy(this._overrides.to, this._core)(position, speed) : (length = this._pages.length, $.proxy(this._overrides.to, this._core)(this._pages[(position % length + length) % length].start, speed)) }, $.fn.owlCarousel.Constructor.Plugins.Navigation = Navigation }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) { "use strict";
        var Hash = function(carousel) { this._core = carousel, this._hashes = {}, this.$element = this._core.$element, this._handlers = { "initialized.owl.carousel": $.proxy(function(e) { e.namespace && "URLHash" === this._core.settings.startPosition && $(window).trigger("hashchange.owl.navigation") }, this), "prepared.owl.carousel": $.proxy(function(e) {
                    if (e.namespace) {
                        var hash = $(e.content).find("[data-hash]").andSelf("[data-hash]").attr("data-hash");
                        if (!hash) return;
                        this._hashes[hash] = e.content } }, this), "changed.owl.carousel": $.proxy(function(e) {
                    if (e.namespace && "position" === e.property.name) {
                        var current = this._core.items(this._core.relative(this._core.current())),
                            hash = $.map(this._hashes, function(item, hash) {
                                return item === current ? hash : null }).join();
                        if (!hash || window.location.hash.slice(1) === hash) return;
                        window.location.hash = hash } }, this) }, this._core.options = $.extend({}, Hash.Defaults, this._core.options), this.$element.on(this._handlers), $(window).on("hashchange.owl.navigation", $.proxy(function(e) {
                var hash = window.location.hash.substring(1),
                    items = this._core.$stage.children(),
                    position = this._hashes[hash] && items.index(this._hashes[hash]);
                position !== undefined && position !== this._core.current() && this._core.to(this._core.relative(position), !1, !0) }, this)) };
        Hash.Defaults = { URLhashListener: !1 }, Hash.prototype.destroy = function() {
            var handler, property;
            $(window).off("hashchange.owl.navigation");
            for (handler in this._handlers) this._core.$element.off(handler, this._handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.Hash = Hash }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        function test(property, prefixed) {
            var result = !1,
                upper = property.charAt(0).toUpperCase() + property.slice(1);
            return $.each((property + " " + prefixes.join(upper + " ") + upper).split(" "), function(i, property) {
                return style[property] !== undefined ? (result = prefixed ? property : !0, !1) : void 0 }), result }

        function prefixed(property) {
            return test(property, !0) }
        var style = $("<support>").get(0).style,
            prefixes = "Webkit Moz O ms".split(" "),
            events = { transition: { end: { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd", transition: "transitionend" } }, animation: { end: { WebkitAnimation: "webkitAnimationEnd", MozAnimation: "animationend", OAnimation: "oAnimationEnd", animation: "animationend" } } },
            tests = { csstransforms: function() {
                    return !!test("transform") }, csstransforms3d: function() {
                    return !!test("perspective") }, csstransitions: function() {
                    return !!test("transition") }, cssanimations: function() {
                    return !!test("animation") } };
        tests.csstransitions() && ($.support.transition = new String(prefixed("transition")), $.support.transition.end = events.transition.end[$.support.transition]), tests.cssanimations() && ($.support.animation = new String(prefixed("animation")), $.support.animation.end = events.animation.end[$.support.animation]), tests.csstransforms() && ($.support.transform = new String(prefixed("transform")), $.support.transform3d = tests.csstransforms3d()) }(window.Zepto || window.jQuery, window, document),
    function($, window, document, undefined) {
        "use strict";
        var Thumbs = function(carousel) { this.owl = carousel, this._thumbcontent = [], this._identifier = 0, this.owl_currentitem = this.owl.options.startPosition, this.$element = this.owl.$element, this._handlers = { "prepared.owl.carousel": $.proxy(function(e) {
                    if (!e.namespace || !this.owl.options.thumbs || this.owl.options.thumbImage || this.owl.options.thumbsPrerendered || this.owl.options.thumbImage) {
                        if (e.namespace && this.owl.options.thumbs && this.owl.options.thumbImage) {
                            var innerImage = $(e.content).find("img");
                            this._thumbcontent.push(innerImage) } } else $(e.content).find("[data-thumb]").attr("data-thumb") !== undefined && this._thumbcontent.push($(e.content).find("[data-thumb]").attr("data-thumb")) }, this), "initialized.owl.carousel": $.proxy(function(e) { e.namespace && this.owl.options.thumbs && (this.render(), this.listen(), this._identifier = this.owl.$element.data("slider-id"), this.setActive()) }, this), "changed.owl.carousel": $.proxy(function(e) { e.namespace && "position" === e.property.name && this.owl.options.thumbs && (this._identifier = this.owl.$element.data("slider-id"), this.setActive()) }, this) }, this.owl.options = $.extend(Thumbs.Defaults, this.owl.options), this.owl.$element.on(this._handlers) };
        Thumbs.Defaults = { thumbs: !0, thumbImage: !1, thumbContainerClass: "owl-thumbs", thumbItemClass: "owl-thumb-item", moveThumbsInside: !1 }, Thumbs.prototype.listen = function() {
            var options = this.owl.options;
            options.thumbsPrerendered && (this._thumbcontent._thumbcontainer = $("." + options.thumbContainerClass)), $(this._thumbcontent._thumbcontainer).on("click", this._thumbcontent._thumbcontainer.children(), $.proxy(function(e) { this._identifier = $(e.target).closest("." + options.thumbContainerClass).data("slider-id");
                var index = $(e.target).parent().is(this._thumbcontent._thumbcontainer) ? $(e.target).index() : $(e.target).closest("." + options.thumbItemClass).index();
                options.thumbsPrerendered ? $("[data-slider-id=" + this._identifier + "]").trigger("to.owl.carousel", [index, options.dotsSpeed, !0]) : this.owl.to(index, options.dotsSpeed), e.preventDefault() }, this)) }, Thumbs.prototype.render = function() {
            var options = this.owl.options;
            options.thumbsPrerendered ? (this._thumbcontent._thumbcontainer = $("." + options.thumbContainerClass), options.moveThumbsInside && this._thumbcontent._thumbcontainer.appendTo(this.$element)) : this._thumbcontent._thumbcontainer = $("<div>").addClass(options.thumbContainerClass).appendTo(this.$element);
            var i;
            if (options.thumbImage)
                for (i = 0; i < this._thumbcontent.length; ++i) this._thumbcontent._thumbcontainer.append("<button class=" + options.thumbItemClass + '><img src="' + this._thumbcontent[i].attr("src") + '" alt="' + this._thumbcontent[i].attr("alt") + '" /></button>');
            else
                for (i = 0; i < this._thumbcontent.length; ++i) this._thumbcontent._thumbcontainer.append("<button class=" + options.thumbItemClass + ">" + this._thumbcontent[i] + "</button>")
        }, Thumbs.prototype.setActive = function() { this.owl_currentitem = this.owl._current - this.owl._clones.length / 2, this.owl_currentitem === this.owl._items.length && (this.owl_currentitem = 0);
            var options = this.owl.options,
                thumbContainer = options.thumbsPrerendered ? $("." + options.thumbContainerClass + '[data-slider-id="' + this._identifier + '"]') : this._thumbcontent._thumbcontainer;
            thumbContainer.children().filter(".active").removeClass("active"), thumbContainer.children().eq(this.owl_currentitem).addClass("active") }, Thumbs.prototype.destroy = function() {
            var handler, property;
            for (handler in this._handlers) this.owl.$element.off(handler, this._handlers[handler]);
            for (property in Object.getOwnPropertyNames(this)) "function" != typeof this[property] && (this[property] = null) }, $.fn.owlCarousel.Constructor.Plugins.Thumbs = Thumbs
    }(window.Zepto || window.jQuery, window, document),
    function(root, factory) { "function" == typeof define && define.amd ? define(factory) : "object" == typeof exports ? module.exports = factory() : root.PhotoSwipeUI_Default = factory() }(this, function() { "use strict";
        var PhotoSwipeUI_Default = function(pswp, framework) {
            var _fullscrenAPI, _controls, _captionContainer, _fakeCaptionContainer, _indexIndicator, _shareButton, _shareModal, _initalCloseOnScrollValue, _isIdle, _listen, _loadingIndicator, _loadingIndicatorHidden, _loadingIndicatorTimeout, _galleryHasOneSlide, _options, _blockControlsTap, _blockControlsTapTimeout, _idleInterval, _idleTimer, ui = this,
                _overlayUIUpdated = !1,
                _controlsVisible = !0,
                _shareModalHidden = !0,
                _defaultUIOptions = { barsSize: { top: 44, bottom: "auto" }, closeElClasses: ["item", "caption", "zoom-wrap", "ui", "top-bar"], timeToIdle: 4e3, timeToIdleOutside: 1e3, loadingIndicatorDelay: 1e3, addCaptionHTMLFn: function(item, captionEl) {
                        return item.title ? (captionEl.children[0].innerHTML = item.title, !0) : (captionEl.children[0].innerHTML = "", !1) }, closeEl: !0, captionEl: !0, fullscreenEl: !0, zoomEl: !0, shareEl: !0, counterEl: !0, arrowEl: !0, preloaderEl: !0, tapToClose: !1, tapToToggleControls: !0, clickToCloseNonZoomable: !0, shareButtons: [{ id: "facebook", label: "Share on Facebook", url: "https://www.facebook.com/sharer/sharer.php?u={{url}}" }, { id: "twitter", label: "Tweet", url: "https://twitter.com/intent/tweet?text={{text}}&url={{url}}" }, { id: "pinterest", label: "Pin it", url: "http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}" }, { id: "download", label: "Download image", url: "{{raw_image_url}}", download: !0 }], getImageURLForShare: function() {
                        return pswp.currItem.src || "" }, getPageURLForShare: function() {
                        return window.location.href }, getTextForShare: function() {
                        return pswp.currItem.title || "" }, indexIndicatorSep: " / ", fitControlsWidth: 1200 },
                _onControlsTap = function(e) {
                    if (_blockControlsTap) return !0;
                    e = e || window.event, _options.timeToIdle && _options.mouseUsed && !_isIdle && _onIdleMouseMove();
                    for (var uiElement, found, target = e.target || e.srcElement, clickedClass = target.getAttribute("class") || "", i = 0; i < _uiElements.length; i++) uiElement = _uiElements[i], uiElement.onTap && clickedClass.indexOf("pswp__" + uiElement.name) > -1 && (uiElement.onTap(), found = !0);
                    if (found) { e.stopPropagation && e.stopPropagation(), _blockControlsTap = !0;
                        var tapDelay = framework.features.isOldAndroid ? 600 : 30;
                        _blockControlsTapTimeout = setTimeout(function() { _blockControlsTap = !1 }, tapDelay) } },
                _fitControlsInViewport = function() {
                    return !pswp.likelyTouchDevice || _options.mouseUsed || screen.width > _options.fitControlsWidth },
                _togglePswpClass = function(el, cName, add) { framework[(add ? "add" : "remove") + "Class"](el, "pswp__" + cName) },
                _countNumItems = function() {
                    var hasOneSlide = 1 === _options.getNumItemsFn();
                    hasOneSlide !== _galleryHasOneSlide && (_togglePswpClass(_controls, "ui--one-slide", hasOneSlide), _galleryHasOneSlide = hasOneSlide) },
                _toggleShareModalClass = function() { _togglePswpClass(_shareModal, "share-modal--hidden", _shareModalHidden) },
                _toggleShareModal = function() {
                    return _shareModalHidden = !_shareModalHidden, _shareModalHidden ? (framework.removeClass(_shareModal, "pswp__share-modal--fade-in"), setTimeout(function() { _shareModalHidden && _toggleShareModalClass() }, 300)) : (_toggleShareModalClass(), setTimeout(function() { _shareModalHidden || framework.addClass(_shareModal, "pswp__share-modal--fade-in") }, 30)), _shareModalHidden || _updateShareURLs(), !1 },
                _openWindowPopup = function(e) { e = e || window.event;
                    var target = e.target || e.srcElement;
                    return pswp.shout("shareLinkClick", e, target), target.href ? target.hasAttribute("download") ? !0 : (window.open(target.href, "pswp_share", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=550,height=420,top=100,left=" + (window.screen ? Math.round(screen.width / 2 - 275) : 100)), _shareModalHidden || _toggleShareModal(), !1) : !1 },
                _updateShareURLs = function() {
                    for (var shareButtonData, shareURL, image_url, page_url, share_text, shareButtonOut = "", i = 0; i < _options.shareButtons.length; i++) shareButtonData = _options.shareButtons[i], image_url = _options.getImageURLForShare(shareButtonData), page_url = _options.getPageURLForShare(shareButtonData), share_text = _options.getTextForShare(shareButtonData), shareURL = shareButtonData.url.replace("{{url}}", encodeURIComponent(page_url)).replace("{{image_url}}", encodeURIComponent(image_url)).replace("{{raw_image_url}}", image_url).replace("{{text}}", encodeURIComponent(share_text)), shareButtonOut += '<a href="' + shareURL + '" target="_blank" class="pswp__share--' + shareButtonData.id + '"' + (shareButtonData.download ? "download" : "") + ">" + shareButtonData.label + "</a>", _options.parseShareButtonOut && (shareButtonOut = _options.parseShareButtonOut(shareButtonData, shareButtonOut));
                    _shareModal.children[0].innerHTML = shareButtonOut, _shareModal.children[0].onclick = _openWindowPopup },
                _hasCloseClass = function(target) {
                    for (var i = 0; i < _options.closeElClasses.length; i++)
                        if (framework.hasClass(target, "pswp__" + _options.closeElClasses[i])) return !0 },
                _idleIncrement = 0,
                _onIdleMouseMove = function() { clearTimeout(_idleTimer), _idleIncrement = 0, _isIdle && ui.setIdle(!1) },
                _onMouseLeaveWindow = function(e) { e = e ? e : window.event;
                    var from = e.relatedTarget || e.toElement;
                    from && "HTML" !== from.nodeName || (clearTimeout(_idleTimer), _idleTimer = setTimeout(function() { ui.setIdle(!0) }, _options.timeToIdleOutside)) },
                _setupFullscreenAPI = function() { _options.fullscreenEl && !framework.features.isOldAndroid && (_fullscrenAPI || (_fullscrenAPI = ui.getFullscreenAPI()), _fullscrenAPI ? (framework.bind(document, _fullscrenAPI.eventK, ui.updateFullscreen), ui.updateFullscreen(), framework.addClass(pswp.template, "pswp--supports-fs")) : framework.removeClass(pswp.template, "pswp--supports-fs")) },
                _setupLoadingIndicator = function() { _options.preloaderEl && (_toggleLoadingIndicator(!0), _listen("beforeChange", function() { clearTimeout(_loadingIndicatorTimeout), _loadingIndicatorTimeout = setTimeout(function() { pswp.currItem && pswp.currItem.loading ? (!pswp.allowProgressiveImg() || pswp.currItem.img && !pswp.currItem.img.naturalWidth) && _toggleLoadingIndicator(!1) : _toggleLoadingIndicator(!0) }, _options.loadingIndicatorDelay) }), _listen("imageLoadComplete", function(index, item) { pswp.currItem === item && _toggleLoadingIndicator(!0) })) },
                _toggleLoadingIndicator = function(hide) { _loadingIndicatorHidden !== hide && (_togglePswpClass(_loadingIndicator, "preloader--active", !hide), _loadingIndicatorHidden = hide) },
                _applyNavBarGaps = function(item) {
                    var gap = item.vGap;
                    if (_fitControlsInViewport()) {
                        var bars = _options.barsSize;
                        if (_options.captionEl && "auto" === bars.bottom)
                            if (_fakeCaptionContainer || (_fakeCaptionContainer = framework.createEl("pswp__caption pswp__caption--fake"), _fakeCaptionContainer.appendChild(framework.createEl("pswp__caption__center")), _controls.insertBefore(_fakeCaptionContainer, _captionContainer), framework.addClass(_controls, "pswp__ui--fit")), _options.addCaptionHTMLFn(item, _fakeCaptionContainer, !0)) {
                                var captionSize = _fakeCaptionContainer.clientHeight;
                                gap.bottom = parseInt(captionSize, 10) || 44 } else gap.bottom = bars.top;
                        else gap.bottom = "auto" === bars.bottom ? 0 : bars.bottom;
                        gap.top = bars.top } else gap.top = gap.bottom = 0 },
                _setupIdle = function() { _options.timeToIdle && _listen("mouseUsed", function() { framework.bind(document, "mousemove", _onIdleMouseMove), framework.bind(document, "mouseout", _onMouseLeaveWindow), _idleInterval = setInterval(function() { _idleIncrement++, 2 === _idleIncrement && ui.setIdle(!0) }, _options.timeToIdle / 2) }) },
                _setupHidingControlsDuringGestures = function() { _listen("onVerticalDrag", function(now) { _controlsVisible && .95 > now ? ui.hideControls() : !_controlsVisible && now >= .95 && ui.showControls() });
                    var pinchControlsHidden;
                    _listen("onPinchClose", function(now) { _controlsVisible && .9 > now ? (ui.hideControls(), pinchControlsHidden = !0) : pinchControlsHidden && !_controlsVisible && now > .9 && ui.showControls() }), _listen("zoomGestureEnded", function() { pinchControlsHidden = !1, pinchControlsHidden && !_controlsVisible && ui.showControls() }) },
                _uiElements = [{ name: "caption", option: "captionEl", onInit: function(el) { _captionContainer = el } }, { name: "share-modal", option: "shareEl", onInit: function(el) { _shareModal = el }, onTap: function() { _toggleShareModal() } }, { name: "button--share", option: "shareEl", onInit: function(el) { _shareButton = el }, onTap: function() { _toggleShareModal() } }, { name: "button--zoom", option: "zoomEl", onTap: pswp.toggleDesktopZoom }, { name: "counter", option: "counterEl", onInit: function(el) { _indexIndicator = el } }, { name: "button--close", option: "closeEl", onTap: pswp.close }, { name: "button--arrow--left", option: "arrowEl", onTap: pswp.prev }, { name: "button--arrow--right", option: "arrowEl", onTap: pswp.next }, { name: "button--fs", option: "fullscreenEl", onTap: function() { _fullscrenAPI.isFullscreen() ? _fullscrenAPI.exit() : _fullscrenAPI.enter() } }, { name: "preloader", option: "preloaderEl", onInit: function(el) { _loadingIndicator = el } }],
                _setupUIElements = function() {
                    var item, classAttr, uiElement, loopThroughChildElements = function(sChildren) {
                        if (sChildren)
                            for (var l = sChildren.length, i = 0; l > i; i++) { item = sChildren[i], classAttr = item.className;
                                for (var a = 0; a < _uiElements.length; a++) uiElement = _uiElements[a], classAttr.indexOf("pswp__" + uiElement.name) > -1 && (_options[uiElement.option] ? (framework.removeClass(item, "pswp__element--disabled"), uiElement.onInit && uiElement.onInit(item)) : framework.addClass(item, "pswp__element--disabled")) } };
                    loopThroughChildElements(_controls.children);
                    var topBar = framework.getChildByClass(_controls, "pswp__top-bar");
                    topBar && loopThroughChildElements(topBar.children) };
            ui.init = function() { framework.extend(pswp.options, _defaultUIOptions, !0), _options = pswp.options, _controls = framework.getChildByClass(pswp.scrollWrap, "pswp__ui"), _listen = pswp.listen, _setupHidingControlsDuringGestures(), _listen("beforeChange", ui.update), _listen("doubleTap", function(point) {
                    var initialZoomLevel = pswp.currItem.initialZoomLevel;
                    pswp.getZoomLevel() !== initialZoomLevel ? pswp.zoomTo(initialZoomLevel, point, 333) : pswp.zoomTo(_options.getDoubleTapZoom(!1, pswp.currItem), point, 333) }), _listen("preventDragEvent", function(e, isDown, preventObj) {
                    var t = e.target || e.srcElement;
                    t && t.getAttribute("class") && e.type.indexOf("mouse") > -1 && (t.getAttribute("class").indexOf("__caption") > 0 || /(SMALL|STRONG|EM)/i.test(t.tagName)) && (preventObj.prevent = !1) }), _listen("bindEvents", function() { framework.bind(_controls, "pswpTap click", _onControlsTap), framework.bind(pswp.scrollWrap, "pswpTap", ui.onGlobalTap), pswp.likelyTouchDevice || framework.bind(pswp.scrollWrap, "mouseover", ui.onMouseOver) }), _listen("unbindEvents", function() { _shareModalHidden || _toggleShareModal(), _idleInterval && clearInterval(_idleInterval), framework.unbind(document, "mouseout", _onMouseLeaveWindow), framework.unbind(document, "mousemove", _onIdleMouseMove), framework.unbind(_controls, "pswpTap click", _onControlsTap), framework.unbind(pswp.scrollWrap, "pswpTap", ui.onGlobalTap), framework.unbind(pswp.scrollWrap, "mouseover", ui.onMouseOver), _fullscrenAPI && (framework.unbind(document, _fullscrenAPI.eventK, ui.updateFullscreen), _fullscrenAPI.isFullscreen() && (_options.hideAnimationDuration = 0, _fullscrenAPI.exit()), _fullscrenAPI = null) }), _listen("destroy", function() { _options.captionEl && (_fakeCaptionContainer && _controls.removeChild(_fakeCaptionContainer), framework.removeClass(_captionContainer, "pswp__caption--empty")), _shareModal && (_shareModal.children[0].onclick = null), framework.removeClass(_controls, "pswp__ui--over-close"), framework.addClass(_controls, "pswp__ui--hidden"), ui.setIdle(!1) }), _options.showAnimationDuration || framework.removeClass(_controls, "pswp__ui--hidden"), _listen("initialZoomIn", function() { _options.showAnimationDuration && framework.removeClass(_controls, "pswp__ui--hidden") }), _listen("initialZoomOut", function() { framework.addClass(_controls, "pswp__ui--hidden") }), _listen("parseVerticalMargin", _applyNavBarGaps), _setupUIElements(), _options.shareEl && _shareButton && _shareModal && (_shareModalHidden = !0), _countNumItems(), _setupIdle(), _setupFullscreenAPI(), _setupLoadingIndicator() }, ui.setIdle = function(isIdle) { _isIdle = isIdle, _togglePswpClass(_controls, "ui--idle", isIdle) }, ui.update = function() { _controlsVisible && pswp.currItem ? (ui.updateIndexIndicator(), _options.captionEl && (_options.addCaptionHTMLFn(pswp.currItem, _captionContainer), _togglePswpClass(_captionContainer, "caption--empty", !pswp.currItem.title)), _overlayUIUpdated = !0) : _overlayUIUpdated = !1, _shareModalHidden || _toggleShareModal(), _countNumItems() }, ui.updateFullscreen = function(e) { e && setTimeout(function() { pswp.setScrollOffset(0, framework.getScrollY()) }, 50), framework[(_fullscrenAPI.isFullscreen() ? "add" : "remove") + "Class"](pswp.template, "pswp--fs") }, ui.updateIndexIndicator = function() { _options.counterEl && (_indexIndicator.innerHTML = pswp.getCurrentIndex() + 1 + _options.indexIndicatorSep + _options.getNumItemsFn()) }, ui.doToggleExpand = function() {
                var caption = $(_captionContainer),
                    slideshow = caption.find(".slideshow"),
                    inner = caption.find(".inner"),
                    toggle = caption.find(".toggle"),
                    open = slideshow.data("expanded") || !1;
                open ? (toggle.text("MORE"), caption.removeClass("expanded"), slideshow.animate({ height: 60 }, 200, function() { slideshow.css({ maxHeight: "60px", height: "auto" }) }), slideshow.data("expanded", !1)) : (toggle.text("LESS"), caption.addClass("expanded"), slideshow.height(slideshow.height()).css({ maxHeight: "none" }), slideshow.animate({ height: inner.innerHeight() }, 200), slideshow.data("expanded", !0)) }, ui.onGlobalTap = function(e) { e = e || window.event;
                var target = e.target || e.srcElement;
                if (!_blockControlsTap) {
                    if ($(target).hasClass("toggle")) return void ui.doToggleExpand();
                    if (e.detail && "mouse" === e.detail.pointerType) {
                        if (_hasCloseClass(target)) return void pswp.close();
                        framework.hasClass(target, "pswp__img") && (1 === pswp.getZoomLevel() && pswp.getZoomLevel() <= pswp.currItem.fitRatio ? _options.clickToCloseNonZoomable && pswp.close() : pswp.toggleDesktopZoom(e.detail.releasePoint)) } else if (_options.tapToToggleControls && (_controlsVisible ? ui.hideControls() : ui.showControls()), _options.tapToClose && (framework.hasClass(target, "pswp__img") || _hasCloseClass(target))) return void pswp.close() } }, ui.onMouseOver = function(e) { e = e || window.event;
                var target = e.target || e.srcElement;
                _togglePswpClass(_controls, "ui--over-close", _hasCloseClass(target)) }, ui.hideControls = function() { framework.addClass(_controls, "pswp__ui--hidden"), _controlsVisible = !1 }, ui.showControls = function() { _controlsVisible = !0, _overlayUIUpdated || ui.update(), framework.removeClass(_controls, "pswp__ui--hidden") }, ui.supportsFullscreen = function() {
                var d = document;
                return !!(d.exitFullscreen || d.mozCancelFullScreen || d.webkitExitFullscreen || d.msExitFullscreen) }, ui.getFullscreenAPI = function() {
                var api, dE = document.documentElement,
                    tF = "fullscreenchange";
                return dE.requestFullscreen ? api = { enterK: "requestFullscreen", exitK: "exitFullscreen", elementK: "fullscreenElement", eventK: tF } : dE.mozRequestFullScreen ? api = { enterK: "mozRequestFullScreen", exitK: "mozCancelFullScreen", elementK: "mozFullScreenElement", eventK: "moz" + tF } : dE.webkitRequestFullscreen ? api = { enterK: "webkitRequestFullscreen", exitK: "webkitExitFullscreen", elementK: "webkitFullscreenElement", eventK: "webkit" + tF } : dE.msRequestFullscreen && (api = { enterK: "msRequestFullscreen", exitK: "msExitFullscreen", elementK: "msFullscreenElement", eventK: "MSFullscreenChange" }), api && (api.enter = function() {
                    return _initalCloseOnScrollValue = _options.closeOnScroll, _options.closeOnScroll = !1, "webkitRequestFullscreen" !== this.enterK ? pswp.template[this.enterK]() : void pswp.template[this.enterK](Element.ALLOW_KEYBOARD_INPUT) }, api.exit = function() {
                    return _options.closeOnScroll = _initalCloseOnScrollValue, document[this.exitK]() }, api.isFullscreen = function() {
                    return document[this.elementK] }), api } };
        return PhotoSwipeUI_Default }),
    function(root, factory) { "function" == typeof define && define.amd ? define(factory) : "object" == typeof exports ? module.exports = factory() : root.PhotoSwipe = factory() }(this, function() {
        "use strict";
        var PhotoSwipe = function(template, UiClass, items, options) {
            var framework = { features: null, bind: function(target, type, listener, unbind) {
                    var methodName = (unbind ? "remove" : "add") + "EventListener";
                    type = type.split(" ");
                    for (var i = 0; i < type.length; i++) type[i] && target[methodName](type[i], listener, !1) }, isArray: function(obj) {
                    return obj instanceof Array }, createEl: function(classes, tag) {
                    var el = document.createElement(tag || "div");
                    return classes && (el.className = classes), el }, getScrollY: function() {
                    var yOffset = window.pageYOffset;
                    return void 0 !== yOffset ? yOffset : document.documentElement.scrollTop }, unbind: function(target, type, listener) { framework.bind(target, type, listener, !0) }, removeClass: function(el, className) {
                    var reg = new RegExp("(\\s|^)" + className + "(\\s|$)");
                    el.className = el.className.replace(reg, " ").replace(/^\s\s*/, "").replace(/\s\s*$/, "") }, addClass: function(el, className) { framework.hasClass(el, className) || (el.className += (el.className ? " " : "") + className) }, hasClass: function(el, className) {
                    return el.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(el.className) }, getChildByClass: function(parentEl, childClassName) {
                    for (var node = parentEl.firstChild; node;) {
                        if (framework.hasClass(node, childClassName)) return node;
                        node = node.nextSibling } }, arraySearch: function(array, value, key) {
                    for (var i = array.length; i--;)
                        if (array[i][key] === value) return i;
                    return -1 }, extend: function(o1, o2, preventOverwrite) {
                    for (var prop in o2)
                        if (o2.hasOwnProperty(prop)) {
                            if (preventOverwrite && o1.hasOwnProperty(prop)) continue;
                            o1[prop] = o2[prop] } }, easing: { sine: { out: function(k) {
                            return Math.sin(k * (Math.PI / 2)) }, inOut: function(k) {
                            return -(Math.cos(Math.PI * k) - 1) / 2 } }, cubic: { out: function(k) {
                            return --k * k * k + 1 } } }, detectFeatures: function() {
                    if (framework.features) return framework.features;
                    var helperEl = framework.createEl(),
                        helperStyle = helperEl.style,
                        vendor = "",
                        features = {};
                    if (features.oldIE = document.all && !document.addEventListener, features.touch = "ontouchstart" in window, window.requestAnimationFrame && (features.raf = window.requestAnimationFrame, features.caf = window.cancelAnimationFrame), features.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled, !features.pointerEvent) {
                        var ua = navigator.userAgent;
                        if (/iP(hone|od)/.test(navigator.platform)) {
                            var v = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
                            v && v.length > 0 && (v = parseInt(v[1], 10), v >= 1 && 8 > v && (features.isOldIOSPhone = !0)) }
                        var match = ua.match(/Android\s([0-9\.]*)/),
                            androidversion = match ? match[1] : 0;
                        androidversion = parseFloat(androidversion), androidversion >= 1 && (4.4 > androidversion && (features.isOldAndroid = !0), features.androidVersion = androidversion), features.isMobileOpera = /opera mini|opera mobi/i.test(ua) }
                    for (var styleCheckItem, styleName, styleChecks = ["transform", "perspective", "animationName"], vendors = ["", "webkit", "Moz", "ms", "O"], i = 0; 4 > i; i++) { vendor = vendors[i];
                        for (var a = 0; 3 > a; a++) styleCheckItem = styleChecks[a], styleName = vendor + (vendor ? styleCheckItem.charAt(0).toUpperCase() + styleCheckItem.slice(1) : styleCheckItem), !features[styleCheckItem] && styleName in helperStyle && (features[styleCheckItem] = styleName);
                        vendor && !features.raf && (vendor = vendor.toLowerCase(), features.raf = window[vendor + "RequestAnimationFrame"], features.raf && (features.caf = window[vendor + "CancelAnimationFrame"] || window[vendor + "CancelRequestAnimationFrame"])) }
                    if (!features.raf) {
                        var lastTime = 0;
                        features.raf = function(fn) {
                            var currTime = (new Date).getTime(),
                                timeToCall = Math.max(0, 16 - (currTime - lastTime)),
                                id = window.setTimeout(function() { fn(currTime + timeToCall) }, timeToCall);
                            return lastTime = currTime + timeToCall, id }, features.caf = function(id) { clearTimeout(id) } }
                    return features.svg = !!document.createElementNS && !!document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect, framework.features = features, features } };
            framework.detectFeatures(), framework.features.oldIE && (framework.bind = function(target, type, listener, unbind) { type = type.split(" ");
                for (var evName, methodName = (unbind ? "detach" : "attach") + "Event", _handleEv = function() { listener.handleEvent.call(listener) }, i = 0; i < type.length; i++)
                    if (evName = type[i])
                        if ("object" == typeof listener && listener.handleEvent) {
                            if (unbind) {
                                if (!listener["oldIE" + evName]) return !1 } else listener["oldIE" + evName] = _handleEv;
                            target[methodName]("on" + evName, listener["oldIE" + evName]) } else target[methodName]("on" + evName, listener) });
            var self = this,
                DOUBLE_TAP_RADIUS = 25,
                NUM_HOLDERS = 3,
                _options = { allowPanToNext: !0, spacing: .12, bgOpacity: 1, mouseUsed: !1, loop: !0, pinchToClose: !0, closeOnScroll: !0, closeOnVerticalDrag: !0, verticalDragRange: .75, hideAnimationDuration: 333, showAnimationDuration: 333, showHideOpacity: !1, focus: !0, escKey: !0, arrowKeys: !0, mainScrollEndFriction: .35, panEndFriction: .35, isClickableElement: function(el) {
                        return "A" === el.tagName }, getDoubleTapZoom: function(isMouseClick, item) {
                        return isMouseClick ? 1 : item.initialZoomLevel < .7 ? 1 : 1.33 }, maxSpreadZoom: 1.33, modal: !0, scaleMode: "fit" };
            framework.extend(_options, options);
            var _isOpen, _isDestroying, _closedByScroll, _currentItemIndex, _containerStyle, _containerShiftIndex, _upMoveEvents, _downEvents, _globalEventHandlers, _currZoomLevel, _startZoomLevel, _translatePrefix, _translateSufix, _updateSizeInterval, _itemsNeedUpdate, _itemHolders, _prevItemIndex, _dragStartEvent, _dragMoveEvent, _dragEndEvent, _dragCancelEvent, _transformKey, _pointerEventEnabled, _likelyTouchDevice, _requestAF, _cancelAF, _initalClassName, _initalWindowScrollY, _oldIE, _currentWindowScrollY, _features, _gestureStartTime, _gestureCheckSpeedTime, _releaseAnimData, _isZoomingIn, _verticalDragInitiated, _oldAndroidTouchEndTimeout, _isDragging, _isMultitouch, _zoomStarted, _moved, _dragAnimFrame, _mainScrollShifted, _currentPoints, _isZooming, _currPointsDistance, _startPointsDistance, _currPanBounds, _currZoomElementStyle, _mainScrollAnimating, _direction, _isFirstMove, _opacityChanged, _bgOpacity, _wasOverInitialZoom, _tempCounter, _getEmptyPoint = function() {
                    return { x: 0, y: 0 } },
                _currPanDist = _getEmptyPoint(),
                _startPanOffset = _getEmptyPoint(),
                _panOffset = _getEmptyPoint(),
                _viewportSize = {},
                _currPositionIndex = 0,
                _offset = {},
                _slideSize = _getEmptyPoint(),
                _indexDiff = 0,
                _isFixedPosition = !0,
                _modules = [],
                _windowVisibleSize = {},
                _renderMaxResolution = !1,
                _registerModule = function(name, module) { framework.extend(self, module.publicMethods), _modules.push(name) },
                _getLoopedId = function(index) {
                    var numSlides = _getNumItems();
                    return index > numSlides - 1 ? index - numSlides : 0 > index ? numSlides + index : index },
                _listeners = {},
                _listen = function(name, fn) {
                    return _listeners[name] || (_listeners[name] = []), _listeners[name].push(fn) },
                _shout = function(name) {
                    var listeners = _listeners[name];
                    if (listeners) {
                        var args = Array.prototype.slice.call(arguments);
                        args.shift();
                        for (var i = 0; i < listeners.length; i++) listeners[i].apply(self, args) } },
                _getCurrentTime = function() {
                    return (new Date).getTime() },
                _applyBgOpacity = function(opacity) { _bgOpacity = opacity, self.bg.style.opacity = opacity * _options.bgOpacity },
                _applyZoomTransform = function(styleObj, x, y, zoom, item) {
                    (!_renderMaxResolution || item && item !== self.currItem) && (zoom /= item ? item.fitRatio : self.currItem.fitRatio), styleObj[_transformKey] = _translatePrefix + x + "px, " + y + "px" + _translateSufix + " scale(" + zoom + ")" },
                _applyCurrentZoomPan = function(allowRenderResolution) { _currZoomElementStyle && (allowRenderResolution && (_currZoomLevel > self.currItem.fitRatio ? _renderMaxResolution || (_setImageSize(self.currItem, !1, !0), _renderMaxResolution = !0) : _renderMaxResolution && (_setImageSize(self.currItem), _renderMaxResolution = !1)), _applyZoomTransform(_currZoomElementStyle, _panOffset.x, _panOffset.y, _currZoomLevel)) },
                _applyZoomPanToItem = function(item) { item.container && _applyZoomTransform(item.container.style, item.initialPosition.x, item.initialPosition.y, item.initialZoomLevel, item) },
                _setTranslateX = function(x, elStyle) { elStyle[_transformKey] = _translatePrefix + x + "px, 0px" + _translateSufix },
                _moveMainScroll = function(x, dragging) {
                    if (!_options.loop && dragging) {
                        var newSlideIndexOffset = _currentItemIndex + (_slideSize.x * _currPositionIndex - x) / _slideSize.x,
                            delta = Math.round(x - _mainScrollPos.x);
                        (0 > newSlideIndexOffset && delta > 0 || newSlideIndexOffset >= _getNumItems() - 1 && 0 > delta) && (x = _mainScrollPos.x + delta * _options.mainScrollEndFriction) }
                    _mainScrollPos.x = x, _setTranslateX(x, _containerStyle) },
                _calculatePanOffset = function(axis, zoomLevel) {
                    var m = _midZoomPoint[axis] - _offset[axis];
                    return _startPanOffset[axis] + _currPanDist[axis] + m - m * (zoomLevel / _startZoomLevel) },
                _equalizePoints = function(p1, p2) { p1.x = p2.x, p1.y = p2.y, p2.id && (p1.id = p2.id) },
                _roundPoint = function(p) { p.x = Math.round(p.x), p.y = Math.round(p.y) },
                _mouseMoveTimeout = null,
                _onFirstMouseMove = function() { _mouseMoveTimeout && (framework.unbind(document, "mousemove", _onFirstMouseMove), framework.addClass(template, "pswp--has_mouse"), _options.mouseUsed = !0, _shout("mouseUsed")), _mouseMoveTimeout = setTimeout(function() { _mouseMoveTimeout = null }, 100) },
                _bindEvents = function() { framework.bind(document, "keydown", self), _features.transform && framework.bind(self.scrollWrap, "click", self), _options.mouseUsed || framework.bind(document, "mousemove", _onFirstMouseMove), framework.bind(window, "resize scroll", self), _shout("bindEvents") },
                _unbindEvents = function() { framework.unbind(window, "resize", self), framework.unbind(window, "scroll", _globalEventHandlers.scroll), framework.unbind(document, "keydown", self), framework.unbind(document, "mousemove", _onFirstMouseMove), _features.transform && framework.unbind(self.scrollWrap, "click", self), _isDragging && framework.unbind(window, _upMoveEvents, self), _shout("unbindEvents") },
                _calculatePanBounds = function(zoomLevel, update) {
                    var bounds = _calculateItemSize(self.currItem, _viewportSize, zoomLevel);
                    return update && (_currPanBounds = bounds), bounds },
                _getMinZoomLevel = function(item) {
                    return item || (item = self.currItem), item.initialZoomLevel },
                _getMaxZoomLevel = function(item) {
                    return item || (item = self.currItem), item.w > 0 ? _options.maxSpreadZoom : 1 },
                _modifyDestPanOffset = function(axis, destPanBounds, destPanOffset, destZoomLevel) {
                    return destZoomLevel === self.currItem.initialZoomLevel ? (destPanOffset[axis] = self.currItem.initialPosition[axis], !0) : (destPanOffset[axis] = _calculatePanOffset(axis, destZoomLevel), destPanOffset[axis] > destPanBounds.min[axis] ? (destPanOffset[axis] = destPanBounds.min[axis], !0) : destPanOffset[axis] < destPanBounds.max[axis] ? (destPanOffset[axis] = destPanBounds.max[axis], !0) : !1) },
                _setupTransforms = function() {
                    if (_transformKey) {
                        var allow3dTransform = _features.perspective && !_likelyTouchDevice;
                        return _translatePrefix = "translate" + (allow3dTransform ? "3d(" : "("), void(_translateSufix = _features.perspective ? ", 0px)" : ")") }
                    _transformKey = "left", framework.addClass(template, "pswp--ie"), _setTranslateX = function(x, elStyle) { elStyle.left = x + "px" }, _applyZoomPanToItem = function(item) {
                        var zoomRatio = item.fitRatio > 1 ? 1 : item.fitRatio,
                            s = item.container.style,
                            w = zoomRatio * item.w,
                            h = zoomRatio * item.h;
                        s.width = w + "px", s.height = h + "px", s.left = item.initialPosition.x + "px", s.top = item.initialPosition.y + "px" }, _applyCurrentZoomPan = function() {
                        if (_currZoomElementStyle) {
                            var s = _currZoomElementStyle,
                                item = self.currItem,
                                zoomRatio = item.fitRatio > 1 ? 1 : item.fitRatio,
                                w = zoomRatio * item.w,
                                h = zoomRatio * item.h;
                            s.width = w + "px", s.height = h + "px", s.left = _panOffset.x + "px", s.top = _panOffset.y + "px" } } },
                _onKeyDown = function(e) {
                    var keydownAction = "";
                    _options.escKey && 27 === e.keyCode ? keydownAction = "close" : _options.arrowKeys && (37 === e.keyCode ? keydownAction = "prev" : 39 === e.keyCode && (keydownAction = "next")), keydownAction && (e.ctrlKey || e.altKey || e.shiftKey || e.metaKey || (e.preventDefault ? e.preventDefault() : e.returnValue = !1, self[keydownAction]())) },
                _onGlobalClick = function(e) { e && (_moved || _zoomStarted || _mainScrollAnimating || _verticalDragInitiated) && (e.preventDefault(), e.stopPropagation()) },
                _updatePageScrollOffset = function() { self.setScrollOffset(0, framework.getScrollY()) },
                _animations = {},
                _numAnimations = 0,
                _stopAnimation = function(name) { _animations[name] && (_animations[name].raf && _cancelAF(_animations[name].raf), _numAnimations--, delete _animations[name]) },
                _registerStartAnimation = function(name) { _animations[name] && _stopAnimation(name), _animations[name] || (_numAnimations++, _animations[name] = {}) },
                _stopAllAnimations = function() {
                    for (var prop in _animations) _animations.hasOwnProperty(prop) && _stopAnimation(prop) },
                _animateProp = function(name, b, endProp, d, easingFn, onUpdate, onComplete) {
                    var t, startAnimTime = _getCurrentTime();
                    _registerStartAnimation(name);
                    var animloop = function() {
                        if (_animations[name]) {
                            if (t = _getCurrentTime() - startAnimTime, t >= d) return _stopAnimation(name), onUpdate(endProp), void(onComplete && onComplete());
                            onUpdate((endProp - b) * easingFn(t / d) + b), _animations[name].raf = _requestAF(animloop) } };
                    animloop() },
                publicMethods = {
                    shout: _shout,
                    listen: _listen,
                    viewportSize: _viewportSize,
                    options: _options,
                    isMainScrollAnimating: function() {
                        return _mainScrollAnimating },
                    getZoomLevel: function() {
                        return _currZoomLevel },
                    getCurrentIndex: function() {
                        return _currentItemIndex },
                    isDragging: function() {
                        return _isDragging },
                    isZooming: function() {
                        return _isZooming },
                    setScrollOffset: function(x, y) { _offset.x = x, _currentWindowScrollY = _offset.y = y, _shout("updateScrollOffset", _offset) },
                    applyZoomPan: function(zoomLevel, panX, panY, allowRenderResolution) { _panOffset.x = panX, _panOffset.y = panY, _currZoomLevel = zoomLevel, _applyCurrentZoomPan(allowRenderResolution) },
                    init: function() {
                        if (!_isOpen && !_isDestroying) {
                            var i;
                            self.framework = framework, self.template = template, self.bg = framework.getChildByClass(template, "pswp__bg"), _initalClassName = template.className, _isOpen = !0, _features = framework.detectFeatures(), _requestAF = _features.raf, _cancelAF = _features.caf, _transformKey = _features.transform, _oldIE = _features.oldIE, self.scrollWrap = framework.getChildByClass(template, "pswp__scroll-wrap"), self.container = framework.getChildByClass(self.scrollWrap, "pswp__container"), _containerStyle = self.container.style, self.itemHolders = _itemHolders = [{ el: self.container.children[0], wrap: 0, index: -1 }, { el: self.container.children[1], wrap: 0, index: -1 }, { el: self.container.children[2], wrap: 0, index: -1 }], _itemHolders[0].el.style.display = _itemHolders[2].el.style.display = "none", _setupTransforms(), _globalEventHandlers = { resize: self.updateSize, scroll: _updatePageScrollOffset, keydown: _onKeyDown, click: _onGlobalClick };
                            var oldPhone = _features.isOldIOSPhone || _features.isOldAndroid || _features.isMobileOpera;
                            for (_features.animationName && _features.transform && !oldPhone || (_options.showAnimationDuration = _options.hideAnimationDuration = 0), i = 0; i < _modules.length; i++) self["init" + _modules[i]]();
                            if (UiClass) {
                                var ui = self.ui = new UiClass(self, framework);
                                ui.init() }
                            _shout("firstUpdate"), _currentItemIndex = _currentItemIndex || _options.index || 0, (isNaN(_currentItemIndex) || 0 > _currentItemIndex || _currentItemIndex >= _getNumItems()) && (_currentItemIndex = 0), self.currItem = _getItemAt(_currentItemIndex), (_features.isOldIOSPhone || _features.isOldAndroid) && (_isFixedPosition = !1), template.setAttribute("aria-hidden", "false"), _options.modal && (_isFixedPosition ? template.style.position = "fixed" : (template.style.position = "absolute", template.style.top = framework.getScrollY() + "px")), void 0 === _currentWindowScrollY && (_shout("initialLayout"), _currentWindowScrollY = _initalWindowScrollY = framework.getScrollY());
                            var rootClasses = "pswp--open ";
                            for (_options.mainClass && (rootClasses += _options.mainClass + " "), _options.showHideOpacity && (rootClasses += "pswp--animate_opacity "), rootClasses += _likelyTouchDevice ? "pswp--touch" : "pswp--notouch", rootClasses += _features.animationName ? " pswp--css_animation" : "", rootClasses += _features.svg ? " pswp--svg" : "", framework.addClass(template, rootClasses), self.updateSize(), _containerShiftIndex = -1, _indexDiff = null, i = 0; NUM_HOLDERS > i; i++) _setTranslateX((i + _containerShiftIndex) * _slideSize.x, _itemHolders[i].el.style);
                            _oldIE || framework.bind(self.scrollWrap, _downEvents, self), _listen("initialZoomInEnd", function() {
                                self.setContent(_itemHolders[0], _currentItemIndex - 1), self.setContent(_itemHolders[2], _currentItemIndex + 1), _itemHolders[0].el.style.display = _itemHolders[2].el.style.display = "block",
                                    _options.focus && template.focus(), _bindEvents()
                            }), self.setContent(_itemHolders[1], _currentItemIndex), self.updateCurrItem(), _shout("afterInit"), _isFixedPosition || (_updateSizeInterval = setInterval(function() { _numAnimations || _isDragging || _isZooming || _currZoomLevel !== self.currItem.initialZoomLevel || self.updateSize() }, 1e3)), framework.addClass(template, "pswp--visible")
                        }
                    },
                    close: function() { _isOpen && (_isOpen = !1, _isDestroying = !0, _shout("close"), _unbindEvents(), _showOrHide(self.currItem, null, !0, self.destroy)) },
                    destroy: function() { _shout("destroy"), _showOrHideTimeout && clearTimeout(_showOrHideTimeout), template.setAttribute("aria-hidden", "true"), template.className = _initalClassName, _updateSizeInterval && clearInterval(_updateSizeInterval), framework.unbind(self.scrollWrap, _downEvents, self), framework.unbind(window, "scroll", self), _stopDragUpdateLoop(), _stopAllAnimations(), _listeners = null },
                    panTo: function(x, y, force) { force || (x > _currPanBounds.min.x ? x = _currPanBounds.min.x : x < _currPanBounds.max.x && (x = _currPanBounds.max.x), y > _currPanBounds.min.y ? y = _currPanBounds.min.y : y < _currPanBounds.max.y && (y = _currPanBounds.max.y)), _panOffset.x = x, _panOffset.y = y, _applyCurrentZoomPan() },
                    handleEvent: function(e) { e = e || window.event, _globalEventHandlers[e.type] && _globalEventHandlers[e.type](e) },
                    goTo: function(index) { index = _getLoopedId(index);
                        var diff = index - _currentItemIndex;
                        _indexDiff = diff, _currentItemIndex = index, self.currItem = _getItemAt(_currentItemIndex), _currPositionIndex -= diff, _moveMainScroll(_slideSize.x * _currPositionIndex), _stopAllAnimations(), _mainScrollAnimating = !1, self.updateCurrItem() },
                    next: function() { self.goTo(_currentItemIndex + 1) },
                    prev: function() { self.goTo(_currentItemIndex - 1) },
                    updateCurrZoomItem: function(emulateSetContent) {
                        if (emulateSetContent && _shout("beforeChange", 0), _itemHolders[1].el.children.length) {
                            var zoomElement = _itemHolders[1].el.children[0];
                            _currZoomElementStyle = framework.hasClass(zoomElement, "pswp__zoom-wrap") ? zoomElement.style : null } else _currZoomElementStyle = null;
                        _currPanBounds = self.currItem.bounds, _startZoomLevel = _currZoomLevel = self.currItem.initialZoomLevel, _panOffset.x = _currPanBounds.center.x, _panOffset.y = _currPanBounds.center.y, emulateSetContent && _shout("afterChange") },
                    invalidateCurrItems: function() { _itemsNeedUpdate = !0;
                        for (var i = 0; NUM_HOLDERS > i; i++) _itemHolders[i].item && (_itemHolders[i].item.needsUpdate = !0) },
                    updateCurrItem: function(beforeAnimation) {
                        if (0 !== _indexDiff) {
                            var tempHolder, diffAbs = Math.abs(_indexDiff);
                            if (!(beforeAnimation && 2 > diffAbs)) { self.currItem = _getItemAt(_currentItemIndex), _renderMaxResolution = !1, _shout("beforeChange", _indexDiff), diffAbs >= NUM_HOLDERS && (_containerShiftIndex += _indexDiff + (_indexDiff > 0 ? -NUM_HOLDERS : NUM_HOLDERS), diffAbs = NUM_HOLDERS);
                                for (var i = 0; diffAbs > i; i++) _indexDiff > 0 ? (tempHolder = _itemHolders.shift(), _itemHolders[NUM_HOLDERS - 1] = tempHolder, _containerShiftIndex++, _setTranslateX((_containerShiftIndex + 2) * _slideSize.x, tempHolder.el.style), self.setContent(tempHolder, _currentItemIndex - diffAbs + i + 1 + 1)) : (tempHolder = _itemHolders.pop(), _itemHolders.unshift(tempHolder), _containerShiftIndex--, _setTranslateX(_containerShiftIndex * _slideSize.x, tempHolder.el.style), self.setContent(tempHolder, _currentItemIndex + diffAbs - i - 1 - 1));
                                if (_currZoomElementStyle && 1 === Math.abs(_indexDiff)) {
                                    var prevItem = _getItemAt(_prevItemIndex);
                                    prevItem.initialZoomLevel !== _currZoomLevel && (_calculateItemSize(prevItem, _viewportSize), _setImageSize(prevItem), _applyZoomPanToItem(prevItem)) }
                                _indexDiff = 0, self.updateCurrZoomItem(), _prevItemIndex = _currentItemIndex, _shout("afterChange") } } },
                    updateSize: function(force) {
                        if (!_isFixedPosition && _options.modal) {
                            var windowScrollY = framework.getScrollY();
                            if (_currentWindowScrollY !== windowScrollY && (template.style.top = windowScrollY + "px", _currentWindowScrollY = windowScrollY), !force && _windowVisibleSize.x === window.innerWidth && _windowVisibleSize.y === window.innerHeight) return;
                            _windowVisibleSize.x = window.innerWidth, _windowVisibleSize.y = window.innerHeight, template.style.height = _windowVisibleSize.y + "px" }
                        if (_viewportSize.x = self.scrollWrap.clientWidth, _viewportSize.y = self.scrollWrap.clientHeight, _updatePageScrollOffset(), _slideSize.x = _viewportSize.x + Math.round(_viewportSize.x * _options.spacing), _slideSize.y = _viewportSize.y, _moveMainScroll(_slideSize.x * _currPositionIndex), _shout("beforeResize"), void 0 !== _containerShiftIndex) {
                            for (var holder, item, hIndex, i = 0; NUM_HOLDERS > i; i++) holder = _itemHolders[i], _setTranslateX((i + _containerShiftIndex) * _slideSize.x, holder.el.style), hIndex = _currentItemIndex + i - 1, _options.loop && _getNumItems() > 2 && (hIndex = _getLoopedId(hIndex)), item = _getItemAt(hIndex), item && (_itemsNeedUpdate || item.needsUpdate || !item.bounds) ? (self.cleanSlide(item), self.setContent(holder, hIndex), 1 === i && (self.currItem = item, self.updateCurrZoomItem(!0)), item.needsUpdate = !1) : -1 === holder.index && hIndex >= 0 && self.setContent(holder, hIndex), item && item.container && (_calculateItemSize(item, _viewportSize), _setImageSize(item), _applyZoomPanToItem(item));
                            _itemsNeedUpdate = !1 }
                        _startZoomLevel = _currZoomLevel = self.currItem.initialZoomLevel, _currPanBounds = self.currItem.bounds, _currPanBounds && (_panOffset.x = _currPanBounds.center.x, _panOffset.y = _currPanBounds.center.y, _applyCurrentZoomPan(!0)), _shout("resize") },
                    zoomTo: function(destZoomLevel, centerPoint, speed, easingFn, updateFn) { centerPoint && (_startZoomLevel = _currZoomLevel, _midZoomPoint.x = Math.abs(centerPoint.x) - _panOffset.x, _midZoomPoint.y = Math.abs(centerPoint.y) - _panOffset.y, _equalizePoints(_startPanOffset, _panOffset));
                        var destPanBounds = _calculatePanBounds(destZoomLevel, !1),
                            destPanOffset = {};
                        _modifyDestPanOffset("x", destPanBounds, destPanOffset, destZoomLevel), _modifyDestPanOffset("y", destPanBounds, destPanOffset, destZoomLevel);
                        var initialZoomLevel = _currZoomLevel,
                            initialPanOffset = { x: _panOffset.x, y: _panOffset.y };
                        _roundPoint(destPanOffset);
                        var onUpdate = function(now) { 1 === now ? (_currZoomLevel = destZoomLevel, _panOffset.x = destPanOffset.x, _panOffset.y = destPanOffset.y) : (_currZoomLevel = (destZoomLevel - initialZoomLevel) * now + initialZoomLevel, _panOffset.x = (destPanOffset.x - initialPanOffset.x) * now + initialPanOffset.x, _panOffset.y = (destPanOffset.y - initialPanOffset.y) * now + initialPanOffset.y), updateFn && updateFn(now), _applyCurrentZoomPan(1 === now) };
                        speed ? _animateProp("customZoomTo", 0, 1, speed, easingFn || framework.easing.sine.inOut, onUpdate) : onUpdate(1) }
                },
                MIN_SWIPE_DISTANCE = 30,
                DIRECTION_CHECK_OFFSET = 10,
                p = {},
                p2 = {},
                delta = {},
                _currPoint = {},
                _startPoint = {},
                _currPointers = [],
                _startMainScrollPos = {},
                _posPoints = [],
                _tempPoint = {},
                _currZoomedItemIndex = 0,
                _centerPoint = _getEmptyPoint(),
                _lastReleaseTime = 0,
                _mainScrollPos = _getEmptyPoint(),
                _midZoomPoint = _getEmptyPoint(),
                _currCenterPoint = _getEmptyPoint(),
                _isEqualPoints = function(p1, p2) {
                    return p1.x === p2.x && p1.y === p2.y },
                _isNearbyPoints = function(touch0, touch1) {
                    return Math.abs(touch0.x - touch1.x) < DOUBLE_TAP_RADIUS && Math.abs(touch0.y - touch1.y) < DOUBLE_TAP_RADIUS },
                _calculatePointsDistance = function(p1, p2) {
                    return _tempPoint.x = Math.abs(p1.x - p2.x), _tempPoint.y = Math.abs(p1.y - p2.y), Math.sqrt(_tempPoint.x * _tempPoint.x + _tempPoint.y * _tempPoint.y) },
                _stopDragUpdateLoop = function() { _dragAnimFrame && (_cancelAF(_dragAnimFrame), _dragAnimFrame = null) },
                _dragUpdateLoop = function() { _isDragging && (_dragAnimFrame = _requestAF(_dragUpdateLoop), _renderMovement()) },
                _canPan = function() {
                    return !("fit" === _options.scaleMode && _currZoomLevel === self.currItem.initialZoomLevel) },
                _closestElement = function(el, fn) {
                    return el && el !== document ? el.getAttribute("class") && el.getAttribute("class").indexOf("pswp__scroll-wrap") > -1 ? !1 : fn(el) ? el : _closestElement(el.parentNode, fn) : !1 },
                _preventObj = {},
                _preventDefaultEventBehaviour = function(e, isDown) {
                    return _preventObj.prevent = !_closestElement(e.target, _options.isClickableElement), _shout("preventDragEvent", e, isDown, _preventObj), _preventObj.prevent },
                _convertTouchToPoint = function(touch, p) {
                    return p.x = touch.pageX, p.y = touch.pageY, p.id = touch.identifier, p },
                _findCenterOfPoints = function(p1, p2, pCenter) { pCenter.x = .5 * (p1.x + p2.x), pCenter.y = .5 * (p1.y + p2.y) },
                _pushPosPoint = function(time, x, y) {
                    if (time - _gestureCheckSpeedTime > 50) {
                        var o = _posPoints.length > 2 ? _posPoints.shift() : {};
                        o.x = x, o.y = y, _posPoints.push(o), _gestureCheckSpeedTime = time } },
                _calculateVerticalDragOpacityRatio = function() {
                    var yOffset = _panOffset.y - self.currItem.initialPosition.y;
                    return 1 - Math.abs(yOffset / (_viewportSize.y / 2)) },
                _ePoint1 = {},
                _ePoint2 = {},
                _tempPointsArr = [],
                _getTouchPoints = function(e) {
                    for (; _tempPointsArr.length > 0;) _tempPointsArr.pop();
                    return _pointerEventEnabled ? (_tempCounter = 0, _currPointers.forEach(function(p) { 0 === _tempCounter ? _tempPointsArr[0] = p : 1 === _tempCounter && (_tempPointsArr[1] = p), _tempCounter++ })) : e.type.indexOf("touch") > -1 ? e.touches && e.touches.length > 0 && (_tempPointsArr[0] = _convertTouchToPoint(e.touches[0], _ePoint1), e.touches.length > 1 && (_tempPointsArr[1] = _convertTouchToPoint(e.touches[1], _ePoint2))) : (_ePoint1.x = e.pageX, _ePoint1.y = e.pageY, _ePoint1.id = "", _tempPointsArr[0] = _ePoint1), _tempPointsArr },
                _panOrMoveMainScroll = function(axis, delta) {
                    var panFriction, startOverDiff, newPanPos, newMainScrollPos, overDiff = 0,
                        newOffset = _panOffset[axis] + delta[axis],
                        dir = delta[axis] > 0,
                        newMainScrollPosition = _mainScrollPos.x + delta.x,
                        mainScrollDiff = _mainScrollPos.x - _startMainScrollPos.x;
                    return panFriction = newOffset > _currPanBounds.min[axis] || newOffset < _currPanBounds.max[axis] ? _options.panEndFriction : 1, newOffset = _panOffset[axis] + delta[axis] * panFriction, !_options.allowPanToNext && _currZoomLevel !== self.currItem.initialZoomLevel || (_currZoomElementStyle ? "h" !== _direction || "x" !== axis || _zoomStarted || (dir ? (newOffset > _currPanBounds.min[axis] && (panFriction = _options.panEndFriction, overDiff = _currPanBounds.min[axis] - newOffset, startOverDiff = _currPanBounds.min[axis] - _startPanOffset[axis]), (0 >= startOverDiff || 0 > mainScrollDiff) && _getNumItems() > 1 ? (newMainScrollPos = newMainScrollPosition, 0 > mainScrollDiff && newMainScrollPosition > _startMainScrollPos.x && (newMainScrollPos = _startMainScrollPos.x)) : _currPanBounds.min.x !== _currPanBounds.max.x && (newPanPos = newOffset)) : (newOffset < _currPanBounds.max[axis] && (panFriction = _options.panEndFriction, overDiff = newOffset - _currPanBounds.max[axis], startOverDiff = _startPanOffset[axis] - _currPanBounds.max[axis]), (0 >= startOverDiff || mainScrollDiff > 0) && _getNumItems() > 1 ? (newMainScrollPos = newMainScrollPosition, mainScrollDiff > 0 && newMainScrollPosition < _startMainScrollPos.x && (newMainScrollPos = _startMainScrollPos.x)) : _currPanBounds.min.x !== _currPanBounds.max.x && (newPanPos = newOffset))) : newMainScrollPos = newMainScrollPosition, "x" !== axis) ? void(_mainScrollAnimating || _mainScrollShifted || _currZoomLevel > self.currItem.fitRatio && (_panOffset[axis] += delta[axis] * panFriction)) : (void 0 !== newMainScrollPos && (_moveMainScroll(newMainScrollPos, !0), _mainScrollShifted = newMainScrollPos === _startMainScrollPos.x ? !1 : !0), _currPanBounds.min.x !== _currPanBounds.max.x && (void 0 !== newPanPos ? _panOffset.x = newPanPos : _mainScrollShifted || (_panOffset.x += delta.x * panFriction)), void 0 !== newMainScrollPos) },
                _onDragStart = function(e) {
                    if (!("mousedown" === e.type && e.button > 0)) {
                        if (_initialZoomRunning) return void e.preventDefault();
                        if (!_oldAndroidTouchEndTimeout || "mousedown" !== e.type) {
                            if (_preventDefaultEventBehaviour(e, !0) && e.preventDefault(), _shout("pointerDown"), _pointerEventEnabled) {
                                var pointerIndex = framework.arraySearch(_currPointers, e.pointerId, "id");
                                0 > pointerIndex && (pointerIndex = _currPointers.length), _currPointers[pointerIndex] = { x: e.pageX, y: e.pageY, id: e.pointerId } }
                            var startPointsList = _getTouchPoints(e),
                                numPoints = startPointsList.length;
                            _currentPoints = null, _stopAllAnimations(), _isDragging && 1 !== numPoints || (_isDragging = _isFirstMove = !0, framework.bind(window, _upMoveEvents, self), _isZoomingIn = _wasOverInitialZoom = _opacityChanged = _verticalDragInitiated = _mainScrollShifted = _moved = _isMultitouch = _zoomStarted = !1, _direction = null, _shout("firstTouchStart", startPointsList), _equalizePoints(_startPanOffset, _panOffset), _currPanDist.x = _currPanDist.y = 0, _equalizePoints(_currPoint, startPointsList[0]), _equalizePoints(_startPoint, _currPoint), _startMainScrollPos.x = _slideSize.x * _currPositionIndex, _posPoints = [{ x: _currPoint.x, y: _currPoint.y }], _gestureCheckSpeedTime = _gestureStartTime = _getCurrentTime(), _calculatePanBounds(_currZoomLevel, !0), _stopDragUpdateLoop(), _dragUpdateLoop()), !_isZooming && numPoints > 1 && !_mainScrollAnimating && !_mainScrollShifted && (_startZoomLevel = _currZoomLevel, _zoomStarted = !1, _isZooming = _isMultitouch = !0, _currPanDist.y = _currPanDist.x = 0, _equalizePoints(_startPanOffset, _panOffset), _equalizePoints(p, startPointsList[0]), _equalizePoints(p2, startPointsList[1]), _findCenterOfPoints(p, p2, _currCenterPoint), _midZoomPoint.x = Math.abs(_currCenterPoint.x) - _panOffset.x, _midZoomPoint.y = Math.abs(_currCenterPoint.y) - _panOffset.y, _currPointsDistance = _startPointsDistance = _calculatePointsDistance(p, p2)) } } },
                _onDragMove = function(e) {
                    if (e.preventDefault(), _pointerEventEnabled) {
                        var pointerIndex = framework.arraySearch(_currPointers, e.pointerId, "id");
                        if (pointerIndex > -1) {
                            var p = _currPointers[pointerIndex];
                            p.x = e.pageX, p.y = e.pageY } }
                    if (_isDragging) {
                        var touchesList = _getTouchPoints(e);
                        if (_direction || _moved || _isZooming) _currentPoints = touchesList;
                        else if (_mainScrollPos.x !== _slideSize.x * _currPositionIndex) _direction = "h";
                        else {
                            var diff = Math.abs(touchesList[0].x - _currPoint.x) - Math.abs(touchesList[0].y - _currPoint.y);
                            Math.abs(diff) >= DIRECTION_CHECK_OFFSET && (_direction = diff > 0 ? "h" : "v", _currentPoints = touchesList) } } },
                _renderMovement = function() {
                    if (_currentPoints) {
                        var numPoints = _currentPoints.length;
                        if (0 !== numPoints)
                            if (_equalizePoints(p, _currentPoints[0]), delta.x = p.x - _currPoint.x, delta.y = p.y - _currPoint.y, _isZooming && numPoints > 1) {
                                if (_currPoint.x = p.x, _currPoint.y = p.y, !delta.x && !delta.y && _isEqualPoints(_currentPoints[1], p2)) return;
                                _equalizePoints(p2, _currentPoints[1]), _zoomStarted || (_zoomStarted = !0, _shout("zoomGestureStarted"));
                                var pointsDistance = _calculatePointsDistance(p, p2),
                                    zoomLevel = _calculateZoomLevel(pointsDistance);
                                zoomLevel > self.currItem.initialZoomLevel + self.currItem.initialZoomLevel / 15 && (_wasOverInitialZoom = !0);
                                var zoomFriction = 1,
                                    minZoomLevel = _getMinZoomLevel(),
                                    maxZoomLevel = _getMaxZoomLevel();
                                if (minZoomLevel > zoomLevel)
                                    if (_options.pinchToClose && !_wasOverInitialZoom && _startZoomLevel <= self.currItem.initialZoomLevel) {
                                        var minusDiff = minZoomLevel - zoomLevel,
                                            percent = 1 - minusDiff / (minZoomLevel / 1.2);
                                        _applyBgOpacity(percent), _shout("onPinchClose", percent), _opacityChanged = !0 } else zoomFriction = (minZoomLevel - zoomLevel) / minZoomLevel, zoomFriction > 1 && (zoomFriction = 1), zoomLevel = minZoomLevel - zoomFriction * (minZoomLevel / 3);
                                else zoomLevel > maxZoomLevel && (zoomFriction = (zoomLevel - maxZoomLevel) / (6 * minZoomLevel), zoomFriction > 1 && (zoomFriction = 1), zoomLevel = maxZoomLevel + zoomFriction * minZoomLevel);
                                0 > zoomFriction && (zoomFriction = 0), _currPointsDistance = pointsDistance, _findCenterOfPoints(p, p2, _centerPoint), _currPanDist.x += _centerPoint.x - _currCenterPoint.x, _currPanDist.y += _centerPoint.y - _currCenterPoint.y, _equalizePoints(_currCenterPoint, _centerPoint), _panOffset.x = _calculatePanOffset("x", zoomLevel), _panOffset.y = _calculatePanOffset("y", zoomLevel), _isZoomingIn = zoomLevel > _currZoomLevel, _currZoomLevel = zoomLevel, _applyCurrentZoomPan() } else {
                                if (!_direction) return;
                                if (_isFirstMove && (_isFirstMove = !1, Math.abs(delta.x) >= DIRECTION_CHECK_OFFSET && (delta.x -= _currentPoints[0].x - _startPoint.x), Math.abs(delta.y) >= DIRECTION_CHECK_OFFSET && (delta.y -= _currentPoints[0].y - _startPoint.y)), _currPoint.x = p.x, _currPoint.y = p.y, 0 === delta.x && 0 === delta.y) return;
                                if ("v" === _direction && _options.closeOnVerticalDrag && !_canPan()) { _currPanDist.y += delta.y, _panOffset.y += delta.y;
                                    var opacityRatio = _calculateVerticalDragOpacityRatio();
                                    return _verticalDragInitiated = !0, _shout("onVerticalDrag", opacityRatio), _applyBgOpacity(opacityRatio), void _applyCurrentZoomPan() }
                                _pushPosPoint(_getCurrentTime(), p.x, p.y), _moved = !0, _currPanBounds = self.currItem.bounds;
                                var mainScrollChanged = _panOrMoveMainScroll("x", delta);
                                mainScrollChanged || (_panOrMoveMainScroll("y", delta), _roundPoint(_panOffset), _applyCurrentZoomPan()) } } },
                _onDragRelease = function(e) {
                    if (_features.isOldAndroid) {
                        if (_oldAndroidTouchEndTimeout && "mouseup" === e.type) return;
                        e.type.indexOf("touch") > -1 && (clearTimeout(_oldAndroidTouchEndTimeout), _oldAndroidTouchEndTimeout = setTimeout(function() { _oldAndroidTouchEndTimeout = 0 }, 600)) }
                    _shout("pointerUp"), _preventDefaultEventBehaviour(e, !1) && e.preventDefault();
                    var releasePoint;
                    if (_pointerEventEnabled) {
                        var pointerIndex = framework.arraySearch(_currPointers, e.pointerId, "id");
                        if (pointerIndex > -1)
                            if (releasePoint = _currPointers.splice(pointerIndex, 1)[0], navigator.pointerEnabled) releasePoint.type = e.pointerType || "mouse";
                            else {
                                var MSPOINTER_TYPES = { 4: "mouse", 2: "touch", 3: "pen" };
                                releasePoint.type = MSPOINTER_TYPES[e.pointerType], releasePoint.type || (releasePoint.type = e.pointerType || "mouse") } }
                    var gestureType, touchList = _getTouchPoints(e),
                        numPoints = touchList.length;
                    if ("mouseup" === e.type && (numPoints = 0), 2 === numPoints) return _currentPoints = null, !0;
                    1 === numPoints && _equalizePoints(_startPoint, touchList[0]), 0 !== numPoints || _direction || _mainScrollAnimating || (releasePoint || ("mouseup" === e.type ? releasePoint = { x: e.pageX, y: e.pageY, type: "mouse" } : e.changedTouches && e.changedTouches[0] && (releasePoint = { x: e.changedTouches[0].pageX, y: e.changedTouches[0].pageY, type: "touch" })), _shout("touchRelease", e, releasePoint));
                    var releaseTimeDiff = -1;
                    if (0 === numPoints && (_isDragging = !1, framework.unbind(window, _upMoveEvents, self), _stopDragUpdateLoop(), _isZooming ? releaseTimeDiff = 0 : -1 !== _lastReleaseTime && (releaseTimeDiff = _getCurrentTime() - _lastReleaseTime)), _lastReleaseTime = 1 === numPoints ? _getCurrentTime() : -1, gestureType = -1 !== releaseTimeDiff && 150 > releaseTimeDiff ? "zoom" : "swipe", _isZooming && 2 > numPoints && (_isZooming = !1, 1 === numPoints && (gestureType = "zoomPointerUp"), _shout("zoomGestureEnded")), _currentPoints = null, _moved || _zoomStarted || _mainScrollAnimating || _verticalDragInitiated)
                        if (_stopAllAnimations(), _releaseAnimData || (_releaseAnimData = _initDragReleaseAnimationData()), _releaseAnimData.calculateSwipeSpeed("x"), _verticalDragInitiated) {
                            var opacityRatio = _calculateVerticalDragOpacityRatio();
                            if (opacityRatio < _options.verticalDragRange) self.close();
                            else {
                                var initalPanY = _panOffset.y,
                                    initialBgOpacity = _bgOpacity;
                                _animateProp("verticalDrag", 0, 1, 300, framework.easing.cubic.out, function(now) { _panOffset.y = (self.currItem.initialPosition.y - initalPanY) * now + initalPanY, _applyBgOpacity((1 - initialBgOpacity) * now + initialBgOpacity), _applyCurrentZoomPan() }), _shout("onVerticalDrag", 1) } } else {
                            if ((_mainScrollShifted || _mainScrollAnimating) && 0 === numPoints) {
                                var itemChanged = _finishSwipeMainScrollGesture(gestureType, _releaseAnimData);
                                if (itemChanged) return;
                                gestureType = "zoomPointerUp" }
                            if (!_mainScrollAnimating) return "swipe" !== gestureType ? void _completeZoomGesture() : void(!_mainScrollShifted && _currZoomLevel > self.currItem.fitRatio && _completePanGesture(_releaseAnimData)) } },
                _initDragReleaseAnimationData = function() {
                    var lastFlickDuration, tempReleasePos, s = { lastFlickOffset: {}, lastFlickDist: {}, lastFlickSpeed: {}, slowDownRatio: {}, slowDownRatioReverse: {}, speedDecelerationRatio: {}, speedDecelerationRatioAbs: {}, distanceOffset: {}, backAnimDestination: {}, backAnimStarted: {}, calculateSwipeSpeed: function(axis) { _posPoints.length > 1 ? (lastFlickDuration = _getCurrentTime() - _gestureCheckSpeedTime + 50, tempReleasePos = _posPoints[_posPoints.length - 2][axis]) : (lastFlickDuration = _getCurrentTime() - _gestureStartTime, tempReleasePos = _startPoint[axis]), s.lastFlickOffset[axis] = _currPoint[axis] - tempReleasePos, s.lastFlickDist[axis] = Math.abs(s.lastFlickOffset[axis]), s.lastFlickSpeed[axis] = s.lastFlickDist[axis] > 20 ? s.lastFlickOffset[axis] / lastFlickDuration : 0, Math.abs(s.lastFlickSpeed[axis]) < .1 && (s.lastFlickSpeed[axis] = 0), s.slowDownRatio[axis] = .95, s.slowDownRatioReverse[axis] = 1 - s.slowDownRatio[axis], s.speedDecelerationRatio[axis] = 1 }, calculateOverBoundsAnimOffset: function(axis, speed) { s.backAnimStarted[axis] || (_panOffset[axis] > _currPanBounds.min[axis] ? s.backAnimDestination[axis] = _currPanBounds.min[axis] : _panOffset[axis] < _currPanBounds.max[axis] && (s.backAnimDestination[axis] = _currPanBounds.max[axis]), void 0 !== s.backAnimDestination[axis] && (s.slowDownRatio[axis] = .7, s.slowDownRatioReverse[axis] = 1 - s.slowDownRatio[axis], s.speedDecelerationRatioAbs[axis] < .05 && (s.lastFlickSpeed[axis] = 0, s.backAnimStarted[axis] = !0, _animateProp("bounceZoomPan" + axis, _panOffset[axis], s.backAnimDestination[axis], speed || 300, framework.easing.sine.out, function(pos) { _panOffset[axis] = pos, _applyCurrentZoomPan() })))) }, calculateAnimOffset: function(axis) { s.backAnimStarted[axis] || (s.speedDecelerationRatio[axis] = s.speedDecelerationRatio[axis] * (s.slowDownRatio[axis] + s.slowDownRatioReverse[axis] - s.slowDownRatioReverse[axis] * s.timeDiff / 10), s.speedDecelerationRatioAbs[axis] = Math.abs(s.lastFlickSpeed[axis] * s.speedDecelerationRatio[axis]), s.distanceOffset[axis] = s.lastFlickSpeed[axis] * s.speedDecelerationRatio[axis] * s.timeDiff, _panOffset[axis] += s.distanceOffset[axis]) }, panAnimLoop: function() {
                            return _animations.zoomPan && (_animations.zoomPan.raf = _requestAF(s.panAnimLoop), s.now = _getCurrentTime(), s.timeDiff = s.now - s.lastNow, s.lastNow = s.now, s.calculateAnimOffset("x"), s.calculateAnimOffset("y"), _applyCurrentZoomPan(), s.calculateOverBoundsAnimOffset("x"), s.calculateOverBoundsAnimOffset("y"), s.speedDecelerationRatioAbs.x < .05 && s.speedDecelerationRatioAbs.y < .05) ? (_panOffset.x = Math.round(_panOffset.x), _panOffset.y = Math.round(_panOffset.y), _applyCurrentZoomPan(), void _stopAnimation("zoomPan")) : void 0 } };
                    return s },
                _completePanGesture = function(animData) {
                    return animData.calculateSwipeSpeed("y"), _currPanBounds = self.currItem.bounds, animData.backAnimDestination = {}, animData.backAnimStarted = {}, Math.abs(animData.lastFlickSpeed.x) <= .05 && Math.abs(animData.lastFlickSpeed.y) <= .05 ? (animData.speedDecelerationRatioAbs.x = animData.speedDecelerationRatioAbs.y = 0, animData.calculateOverBoundsAnimOffset("x"), animData.calculateOverBoundsAnimOffset("y"), !0) : (_registerStartAnimation("zoomPan"), animData.lastNow = _getCurrentTime(), void animData.panAnimLoop()) },
                _finishSwipeMainScrollGesture = function(gestureType, _releaseAnimData) {
                    var itemChanged;
                    _mainScrollAnimating || (_currZoomedItemIndex = _currentItemIndex);
                    var itemsDiff;
                    if ("swipe" === gestureType) {
                        var totalShiftDist = _currPoint.x - _startPoint.x,
                            isFastLastFlick = _releaseAnimData.lastFlickDist.x < 10;
                        totalShiftDist > MIN_SWIPE_DISTANCE && (isFastLastFlick || _releaseAnimData.lastFlickOffset.x > 20) ? itemsDiff = -1 : -MIN_SWIPE_DISTANCE > totalShiftDist && (isFastLastFlick || _releaseAnimData.lastFlickOffset.x < -20) && (itemsDiff = 1) }
                    var nextCircle;
                    itemsDiff && (_currentItemIndex += itemsDiff, 0 > _currentItemIndex ? (_currentItemIndex = _options.loop ? _getNumItems() - 1 : 0, nextCircle = !0) : _currentItemIndex >= _getNumItems() && (_currentItemIndex = _options.loop ? 0 : _getNumItems() - 1, nextCircle = !0), (!nextCircle || _options.loop) && (_indexDiff += itemsDiff, _currPositionIndex -= itemsDiff, itemChanged = !0));
                    var finishAnimDuration, animateToX = _slideSize.x * _currPositionIndex,
                        animateToDist = Math.abs(animateToX - _mainScrollPos.x);
                    return itemChanged || animateToX > _mainScrollPos.x == _releaseAnimData.lastFlickSpeed.x > 0 ? (finishAnimDuration = Math.abs(_releaseAnimData.lastFlickSpeed.x) > 0 ? animateToDist / Math.abs(_releaseAnimData.lastFlickSpeed.x) : 333, finishAnimDuration = Math.min(finishAnimDuration, 400), finishAnimDuration = Math.max(finishAnimDuration, 250)) : finishAnimDuration = 333, _currZoomedItemIndex === _currentItemIndex && (itemChanged = !1), _mainScrollAnimating = !0, _shout("mainScrollAnimStart"), _animateProp("mainScroll", _mainScrollPos.x, animateToX, finishAnimDuration, framework.easing.cubic.out, _moveMainScroll, function() { _stopAllAnimations(), _mainScrollAnimating = !1, _currZoomedItemIndex = -1, (itemChanged || _currZoomedItemIndex !== _currentItemIndex) && self.updateCurrItem(), _shout("mainScrollAnimComplete") }), itemChanged && self.updateCurrItem(!0), itemChanged },
                _calculateZoomLevel = function(touchesDistance) {
                    return 1 / _startPointsDistance * touchesDistance * _startZoomLevel },
                _completeZoomGesture = function() {
                    var destZoomLevel = _currZoomLevel,
                        minZoomLevel = _getMinZoomLevel(),
                        maxZoomLevel = _getMaxZoomLevel();
                    minZoomLevel > _currZoomLevel ? destZoomLevel = minZoomLevel : _currZoomLevel > maxZoomLevel && (destZoomLevel = maxZoomLevel);
                    var onUpdate, destOpacity = 1,
                        initialOpacity = _bgOpacity;
                    return _opacityChanged && !_isZoomingIn && !_wasOverInitialZoom && minZoomLevel > _currZoomLevel ? (self.close(), !0) : (_opacityChanged && (onUpdate = function(now) { _applyBgOpacity((destOpacity - initialOpacity) * now + initialOpacity) }), self.zoomTo(destZoomLevel, 0, 200, framework.easing.cubic.out, onUpdate), !0) };
            _registerModule("Gestures", { publicMethods: { initGestures: function() {
                        var addEventNames = function(pref, down, move, up, cancel) { _dragStartEvent = pref + down, _dragMoveEvent = pref + move, _dragEndEvent = pref + up, _dragCancelEvent = cancel ? pref + cancel : "" };
                        _pointerEventEnabled = _features.pointerEvent, _pointerEventEnabled && _features.touch && (_features.touch = !1), _pointerEventEnabled ? navigator.pointerEnabled ? addEventNames("pointer", "down", "move", "up", "cancel") : addEventNames("MSPointer", "Down", "Move", "Up", "Cancel") : _features.touch ? (addEventNames("touch", "start", "move", "end", "cancel"), _likelyTouchDevice = !0) : addEventNames("mouse", "down", "move", "up"), _upMoveEvents = _dragMoveEvent + " " + _dragEndEvent + " " + _dragCancelEvent, _downEvents = _dragStartEvent, _pointerEventEnabled && !_likelyTouchDevice && (_likelyTouchDevice = navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1), self.likelyTouchDevice = _likelyTouchDevice, _globalEventHandlers[_dragStartEvent] = _onDragStart, _globalEventHandlers[_dragMoveEvent] = _onDragMove, _globalEventHandlers[_dragEndEvent] = _onDragRelease, _dragCancelEvent && (_globalEventHandlers[_dragCancelEvent] = _globalEventHandlers[_dragEndEvent]), _features.touch && (_downEvents += " mousedown", _upMoveEvents += " mousemove mouseup", _globalEventHandlers.mousedown = _globalEventHandlers[_dragStartEvent], _globalEventHandlers.mousemove = _globalEventHandlers[_dragMoveEvent], _globalEventHandlers.mouseup = _globalEventHandlers[_dragEndEvent]), _likelyTouchDevice || (_options.allowPanToNext = !1) } } });
            var _showOrHideTimeout, _items, _initialContentSet, _initialZoomRunning, _getItemAt, _getNumItems, _initialIsLoop, _showOrHide = function(item, img, out, completeFn) { _showOrHideTimeout && clearTimeout(_showOrHideTimeout), _initialZoomRunning = !0, _initialContentSet = !0;
                    var thumbBounds;
                    item.initialLayout ? (thumbBounds = item.initialLayout, item.initialLayout = null) : thumbBounds = _options.getThumbBoundsFn && _options.getThumbBoundsFn(_currentItemIndex);
                    var duration = out ? _options.hideAnimationDuration : _options.showAnimationDuration,
                        onComplete = function() { _stopAnimation("initialZoom"), out ? (self.template.removeAttribute("style"), self.bg.removeAttribute("style")) : (_applyBgOpacity(1), img && (img.style.display = "block"), framework.addClass(template, "pswp--animated-in"), _shout("initialZoom" + (out ? "OutEnd" : "InEnd"))), completeFn && completeFn(), _initialZoomRunning = !1 };
                    if (!duration || !thumbBounds || void 0 === thumbBounds.x) return _shout("initialZoom" + (out ? "Out" : "In")), _currZoomLevel = item.initialZoomLevel, _equalizePoints(_panOffset, item.initialPosition), _applyCurrentZoomPan(), template.style.opacity = out ? 0 : 1, _applyBgOpacity(1), void(duration ? setTimeout(function() { onComplete() }, duration) : onComplete());
                    var startAnimation = function() {
                        var closeWithRaf = _closedByScroll,
                            fadeEverything = !self.currItem.src || self.currItem.loadError || _options.showHideOpacity;
                        item.miniImg && (item.miniImg.style.webkitBackfaceVisibility = "hidden"), out || (_currZoomLevel = thumbBounds.w / item.w, _panOffset.x = thumbBounds.x, _panOffset.y = thumbBounds.y - _initalWindowScrollY, self[fadeEverything ? "template" : "bg"].style.opacity = .001, _applyCurrentZoomPan()), _registerStartAnimation("initialZoom"), out && !closeWithRaf && framework.removeClass(template, "pswp--animated-in"), fadeEverything && (out ? framework[(closeWithRaf ? "remove" : "add") + "Class"](template, "pswp--animate_opacity") : setTimeout(function() { framework.addClass(template, "pswp--animate_opacity") }, 30)), _showOrHideTimeout = setTimeout(function() {
                            if (_shout("initialZoom" + (out ? "Out" : "In")), out) {
                                var destZoomLevel = thumbBounds.w / item.w,
                                    initialPanOffset = { x: _panOffset.x, y: _panOffset.y },
                                    initialZoomLevel = _currZoomLevel,
                                    initalBgOpacity = _bgOpacity,
                                    onUpdate = function(now) { 1 === now ? (_currZoomLevel = destZoomLevel, _panOffset.x = thumbBounds.x, _panOffset.y = thumbBounds.y - _currentWindowScrollY) : (_currZoomLevel = (destZoomLevel - initialZoomLevel) * now + initialZoomLevel, _panOffset.x = (thumbBounds.x - initialPanOffset.x) * now + initialPanOffset.x, _panOffset.y = (thumbBounds.y - _currentWindowScrollY - initialPanOffset.y) * now + initialPanOffset.y), _applyCurrentZoomPan(), fadeEverything ? template.style.opacity = 1 - now : _applyBgOpacity(initalBgOpacity - now * initalBgOpacity) };
                                closeWithRaf ? _animateProp("initialZoom", 0, 1, duration, framework.easing.cubic.out, onUpdate, onComplete) : (onUpdate(1), _showOrHideTimeout = setTimeout(onComplete, duration + 20)) } else _currZoomLevel = item.initialZoomLevel, _equalizePoints(_panOffset, item.initialPosition), _applyCurrentZoomPan(), _applyBgOpacity(1), fadeEverything ? template.style.opacity = 1 : _applyBgOpacity(1), _showOrHideTimeout = setTimeout(onComplete, duration + 20) }, out ? 25 : 90) };
                    startAnimation() },
                _tempPanAreaSize = {},
                _imagesToAppendPool = [],
                _controllerDefaultOptions = { index: 0, errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>', forceProgressiveLoading: !1, preload: [1, 1], getNumItemsFn: function() {
                        return _items.length } },
                _getZeroBounds = function() {
                    return { center: { x: 0, y: 0 }, max: { x: 0, y: 0 }, min: { x: 0, y: 0 } } },
                _calculateSingleItemPanBounds = function(item, realPanElementW, realPanElementH) {
                    var bounds = item.bounds;
                    bounds.center.x = Math.round((_tempPanAreaSize.x - realPanElementW) / 2), bounds.center.y = Math.round((_tempPanAreaSize.y - realPanElementH) / 2) + item.vGap.top, bounds.max.x = realPanElementW > _tempPanAreaSize.x ? Math.round(_tempPanAreaSize.x - realPanElementW) : bounds.center.x, bounds.max.y = realPanElementH > _tempPanAreaSize.y ? Math.round(_tempPanAreaSize.y - realPanElementH) + item.vGap.top : bounds.center.y, bounds.min.x = realPanElementW > _tempPanAreaSize.x ? 0 : bounds.center.x, bounds.min.y = realPanElementH > _tempPanAreaSize.y ? item.vGap.top : bounds.center.y },
                _calculateItemSize = function(item, viewportSize, zoomLevel) {
                    if (item.src && !item.loadError) {
                        var isInitial = !zoomLevel;
                        if (isInitial && (item.vGap || (item.vGap = { top: 0, bottom: 0 }), _shout("parseVerticalMargin", item)), _tempPanAreaSize.x = viewportSize.x, _tempPanAreaSize.y = viewportSize.y - item.vGap.top - item.vGap.bottom, isInitial) {
                            var hRatio = _tempPanAreaSize.x / item.w,
                                vRatio = _tempPanAreaSize.y / item.h;
                            item.fitRatio = vRatio > hRatio ? hRatio : vRatio;
                            var scaleMode = _options.scaleMode; "orig" === scaleMode ? zoomLevel = 1 : "fit" === scaleMode && (zoomLevel = item.fitRatio), zoomLevel > 1 && (zoomLevel = 1), item.initialZoomLevel = zoomLevel, item.bounds || (item.bounds = _getZeroBounds()) }
                        if (!zoomLevel) return;
                        return _calculateSingleItemPanBounds(item, item.w * zoomLevel, item.h * zoomLevel), isInitial && zoomLevel === item.initialZoomLevel && (item.initialPosition = item.bounds.center), item.bounds }
                    return item.w = item.h = 0, item.initialZoomLevel = item.fitRatio = 1, item.bounds = _getZeroBounds(), item.initialPosition = item.bounds.center, item.bounds },
                _appendImage = function(index, item, baseDiv, img, preventAnimation, keepPlaceholder) { item.loadError || img && (item.imageAppended = !0, _setImageSize(item, img, item === self.currItem && _renderMaxResolution), baseDiv.appendChild(img), keepPlaceholder && setTimeout(function() { item && item.loaded && item.placeholder && (item.placeholder.style.display = "none", item.placeholder = null) }, 500)) },
                _preloadImage = function(item) { item.loading = !0, item.loaded = !1;
                    var img = item.img = framework.createEl("pswp__img", "img"),
                        onComplete = function() { item.loading = !1, item.loaded = !0, item.loadComplete ? item.loadComplete(item) : item.img = null, img.onload = img.onerror = null, img = null };
                    return img.onload = onComplete, img.onerror = function() { item.loadError = !0, onComplete() }, img.src = item.src, img },
                _checkForError = function(item, cleanUp) {
                    return item.src && item.loadError && item.container ? (cleanUp && (item.container.innerHTML = ""), item.container.innerHTML = _options.errorMsg.replace("%url%", item.src), !0) : void 0 },
                _setImageSize = function(item, img, maxRes) {
                    if (item.src) { img || (img = item.container.lastChild);
                        var w = maxRes ? item.w : Math.round(item.w * item.fitRatio),
                            h = maxRes ? item.h : Math.round(item.h * item.fitRatio);
                        item.placeholder && !item.loaded && (item.placeholder.style.width = w + "px", item.placeholder.style.height = h + "px"), img.style.width = w + "px", img.style.height = h + "px" } },
                _appendImagesPool = function() {
                    if (_imagesToAppendPool.length) {
                        for (var poolItem, i = 0; i < _imagesToAppendPool.length; i++) poolItem = _imagesToAppendPool[i], poolItem.holder.index === poolItem.index && _appendImage(poolItem.index, poolItem.item, poolItem.baseDiv, poolItem.img, !1, poolItem.clearPlaceholder);

                        _imagesToAppendPool = []
                    }
                };
            _registerModule("Controller", { publicMethods: { lazyLoadItem: function(index) { index = _getLoopedId(index);
                        var item = _getItemAt(index);
                        item && (!item.loaded && !item.loading || _itemsNeedUpdate) && (_shout("gettingData", index, item), item.src && _preloadImage(item)) }, initController: function() { framework.extend(_options, _controllerDefaultOptions, !0), self.items = _items = items, _getItemAt = self.getItemAt, _getNumItems = _options.getNumItemsFn, _initialIsLoop = _options.loop, _getNumItems() < 3 && (_options.loop = !1), _listen("beforeChange", function(diff) {
                            var i, p = _options.preload,
                                isNext = null === diff ? !0 : diff >= 0,
                                preloadBefore = Math.min(p[0], _getNumItems()),
                                preloadAfter = Math.min(p[1], _getNumItems());
                            for (i = 1;
                                (isNext ? preloadAfter : preloadBefore) >= i; i++) self.lazyLoadItem(_currentItemIndex + i);
                            for (i = 1;
                                (isNext ? preloadBefore : preloadAfter) >= i; i++) self.lazyLoadItem(_currentItemIndex - i) }), _listen("initialLayout", function() { self.currItem.initialLayout = _options.getThumbBoundsFn && _options.getThumbBoundsFn(_currentItemIndex) }), _listen("mainScrollAnimComplete", _appendImagesPool), _listen("initialZoomInEnd", _appendImagesPool), _listen("destroy", function() {
                            for (var item, i = 0; i < _items.length; i++) item = _items[i], item.container && (item.container = null), item.placeholder && (item.placeholder = null), item.img && (item.img = null), item.preloader && (item.preloader = null), item.loadError && (item.loaded = item.loadError = !1);
                            _imagesToAppendPool = null }) }, getItemAt: function(index) {
                        return index >= 0 && void 0 !== _items[index] ? _items[index] : !1 }, allowProgressiveImg: function() {
                        return _options.forceProgressiveLoading || !_likelyTouchDevice || _options.mouseUsed || screen.width > 1200 }, setContent: function(holder, index) { _options.loop && (index = _getLoopedId(index));
                        var prevItem = self.getItemAt(holder.index);
                        prevItem && (prevItem.container = null);
                        var img, item = self.getItemAt(index);
                        if (!item) return void(holder.el.innerHTML = "");
                        _shout("gettingData", index, item), holder.index = index, holder.item = item;
                        var baseDiv = item.container = framework.createEl("pswp__zoom-wrap");
                        if (!item.src && item.html && (item.html.tagName ? baseDiv.appendChild(item.html) : baseDiv.innerHTML = item.html), _checkForError(item), _calculateItemSize(item, _viewportSize), !item.src || item.loadError || item.loaded) item.src && !item.loadError && (img = framework.createEl("pswp__img", "img"), img.style.opacity = 1, img.src = item.src, _setImageSize(item, img), _appendImage(index, item, baseDiv, img, !0));
                        else {
                            if (item.loadComplete = function(item) {
                                    if (_isOpen) {
                                        if (holder && holder.index === index) {
                                            if (_checkForError(item, !0)) return item.loadComplete = item.img = null, _calculateItemSize(item, _viewportSize), _applyZoomPanToItem(item), void(holder.index === _currentItemIndex && self.updateCurrZoomItem());
                                            item.imageAppended ? !_initialZoomRunning && item.placeholder && (item.placeholder.style.display = "none", item.placeholder = null) : _features.transform && (_mainScrollAnimating || _initialZoomRunning) ? _imagesToAppendPool.push({ item: item, baseDiv: baseDiv, img: item.img, index: index, holder: holder, clearPlaceholder: !0 }) : _appendImage(index, item, baseDiv, item.img, _mainScrollAnimating || _initialZoomRunning, !0) }
                                        item.loadComplete = null, item.img = null, _shout("imageLoadComplete", index, item) } }, framework.features.transform) {
                                var placeholderClassName = "pswp__img pswp__img--placeholder";
                                placeholderClassName += item.msrc ? "" : " pswp__img--placeholder--blank";
                                var placeholder = framework.createEl(placeholderClassName, item.msrc ? "img" : "");
                                item.msrc && (placeholder.src = item.msrc), _setImageSize(item, placeholder), baseDiv.appendChild(placeholder), item.placeholder = placeholder }
                            item.loading || _preloadImage(item), self.allowProgressiveImg() && (!_initialContentSet && _features.transform ? _imagesToAppendPool.push({ item: item, baseDiv: baseDiv, img: item.img, index: index, holder: holder }) : _appendImage(index, item, baseDiv, item.img, !0, !0)) }
                        _initialContentSet || index !== _currentItemIndex ? _applyZoomPanToItem(item) : (_currZoomElementStyle = baseDiv.style, _showOrHide(item, img || item.img)), holder.el.innerHTML = "", holder.el.appendChild(baseDiv) }, cleanSlide: function(item) { item.img && (item.img.onload = item.img.onerror = null), item.loaded = item.loading = item.img = item.imageAppended = !1 } } });
            var tapTimer, tapReleasePoint = {},
                _dispatchTapEvent = function(origEvent, releasePoint, pointerType) {
                    var e = document.createEvent("CustomEvent"),
                        eDetail = { origEvent: origEvent, target: origEvent.target, releasePoint: releasePoint, pointerType: pointerType || "touch" };
                    e.initCustomEvent("pswpTap", !0, !0, eDetail), origEvent.target.dispatchEvent(e) };
            _registerModule("Tap", { publicMethods: { initTap: function() { _listen("firstTouchStart", self.onTapStart), _listen("touchRelease", self.onTapRelease), _listen("destroy", function() { tapReleasePoint = {}, tapTimer = null }) }, onTapStart: function(touchList) { touchList.length > 1 && (clearTimeout(tapTimer), tapTimer = null) }, onTapRelease: function(e, releasePoint) {
                        if (releasePoint && !_moved && !_isMultitouch && !_numAnimations) {
                            var p0 = releasePoint;
                            if (tapTimer && (clearTimeout(tapTimer), tapTimer = null, _isNearbyPoints(p0, tapReleasePoint))) return void _shout("doubleTap", p0);
                            if ("mouse" === releasePoint.type) return void _dispatchTapEvent(e, releasePoint, "mouse");
                            var clickedTagName = e.target.tagName.toUpperCase();
                            if ("BUTTON" === clickedTagName || framework.hasClass(e.target, "pswp__single-tap")) return void _dispatchTapEvent(e, releasePoint);
                            _equalizePoints(tapReleasePoint, p0), tapTimer = setTimeout(function() { _dispatchTapEvent(e, releasePoint), tapTimer = null }, 300) } } } });
            var _wheelDelta;
            _registerModule("DesktopZoom", { publicMethods: { initDesktopZoom: function() { _oldIE || (_likelyTouchDevice ? _listen("mouseUsed", function() { self.setupDesktopZoom() }) : self.setupDesktopZoom(!0)) }, setupDesktopZoom: function(onInit) { _wheelDelta = {};
                        var events = "wheel mousewheel DOMMouseScroll";
                        _listen("bindEvents", function() { framework.bind(template, events, self.handleMouseWheel) }), _listen("unbindEvents", function() { _wheelDelta && framework.unbind(template, events, self.handleMouseWheel) }), self.mouseZoomedIn = !1;
                        var hasDraggingClass, updateZoomable = function() { self.mouseZoomedIn && (framework.removeClass(template, "pswp--zoomed-in"), self.mouseZoomedIn = !1), 1 > _currZoomLevel ? framework.addClass(template, "pswp--zoom-allowed") : framework.removeClass(template, "pswp--zoom-allowed"), removeDraggingClass() },
                            removeDraggingClass = function() { hasDraggingClass && (framework.removeClass(template, "pswp--dragging"), hasDraggingClass = !1) };
                        _listen("resize", updateZoomable), _listen("afterChange", updateZoomable), _listen("pointerDown", function() { self.mouseZoomedIn && (hasDraggingClass = !0, framework.addClass(template, "pswp--dragging")) }), _listen("pointerUp", removeDraggingClass), onInit || updateZoomable() }, handleMouseWheel: function(e) {
                        if (_currZoomLevel <= self.currItem.fitRatio) return _options.modal && (!_options.closeOnScroll || _numAnimations || _isDragging ? e.preventDefault() : _transformKey && Math.abs(e.deltaY) > 2 && (_closedByScroll = !0, self.close())), !0;
                        if (e.stopPropagation(), _wheelDelta.x = 0, "deltaX" in e) 1 === e.deltaMode ? (_wheelDelta.x = 18 * e.deltaX, _wheelDelta.y = 18 * e.deltaY) : (_wheelDelta.x = e.deltaX, _wheelDelta.y = e.deltaY);
                        else if ("wheelDelta" in e) e.wheelDeltaX && (_wheelDelta.x = -.16 * e.wheelDeltaX), _wheelDelta.y = e.wheelDeltaY ? -.16 * e.wheelDeltaY : -.16 * e.wheelDelta;
                        else {
                            if (!("detail" in e)) return;
                            _wheelDelta.y = e.detail }
                        _calculatePanBounds(_currZoomLevel, !0);
                        var newPanX = _panOffset.x - _wheelDelta.x,
                            newPanY = _panOffset.y - _wheelDelta.y;
                        (_options.modal || newPanX <= _currPanBounds.min.x && newPanX >= _currPanBounds.max.x && newPanY <= _currPanBounds.min.y && newPanY >= _currPanBounds.max.y) && e.preventDefault(), self.panTo(newPanX, newPanY) }, toggleDesktopZoom: function(centerPoint) { centerPoint = centerPoint || { x: _viewportSize.x / 2 + _offset.x, y: _viewportSize.y / 2 + _offset.y };
                        var doubleTapZoomLevel = _options.getDoubleTapZoom(!0, self.currItem),
                            zoomOut = _currZoomLevel === doubleTapZoomLevel;
                        self.mouseZoomedIn = !zoomOut, self.zoomTo(zoomOut ? self.currItem.initialZoomLevel : doubleTapZoomLevel, centerPoint, 333), framework[(zoomOut ? "remove" : "add") + "Class"](template, "pswp--zoomed-in") } } });
            var _historyUpdateTimeout, _hashChangeTimeout, _hashAnimCheckTimeout, _hashChangedByScript, _hashChangedByHistory, _hashReseted, _initialHash, _historyChanged, _closedFromURL, _urlChangedOnce, _windowLoc, _supportsPushState, _historyDefaultOptions = { history: !0, galleryUID: 1 },
                _getHash = function() {
                    return _windowLoc.hash.substring(1) },
                _cleanHistoryTimeouts = function() { _historyUpdateTimeout && clearTimeout(_historyUpdateTimeout), _hashAnimCheckTimeout && clearTimeout(_hashAnimCheckTimeout) },
                _parseItemIndexFromURL = function() {
                    var hash = _getHash(),
                        params = {};
                    if (hash.length < 5) return params;
                    var i, vars = hash.split("&");
                    for (i = 0; i < vars.length; i++)
                        if (vars[i]) {
                            var pair = vars[i].split("=");
                            pair.length < 2 || (params[pair[0]] = pair[1]) }
                    if (_options.galleryPIDs) {
                        var searchfor = params.pid;
                        for (params.pid = 0, i = 0; i < _items.length; i++)
                            if (_items[i].pid === searchfor) { params.pid = i;
                                break } } else params.pid = parseInt(params.pid, 10) - 1;
                    return params.pid < 0 && (params.pid = 0), params },
                _updateHash = function() {
                    if (_hashAnimCheckTimeout && clearTimeout(_hashAnimCheckTimeout), _numAnimations || _isDragging) return void(_hashAnimCheckTimeout = setTimeout(_updateHash, 500));
                    _hashChangedByScript ? clearTimeout(_hashChangeTimeout) : _hashChangedByScript = !0;
                    var pid = _currentItemIndex + 1,
                        item = _getItemAt(_currentItemIndex);
                    item.hasOwnProperty("pid") && (pid = item.pid);
                    var newHash = _initialHash + "&gid=" + _options.galleryUID + "&pid=" + pid;
                    _historyChanged || -1 === _windowLoc.hash.indexOf(newHash) && (_urlChangedOnce = !0);
                    var newURL = _windowLoc.href.split("#")[0] + "#" + newHash;
                    _supportsPushState ? "#" + newHash !== window.location.hash && history[_historyChanged ? "replaceState" : "pushState"]("", document.title, newURL) : _historyChanged ? _windowLoc.replace(newURL) : _windowLoc.hash = newHash, _historyChanged = !0, _hashChangeTimeout = setTimeout(function() { _hashChangedByScript = !1 }, 60) };
            _registerModule("History", { publicMethods: { initHistory: function() {
                        if (framework.extend(_options, _historyDefaultOptions, !0), _options.history) { _windowLoc = window.location, _urlChangedOnce = !1, _closedFromURL = !1, _historyChanged = !1, _initialHash = _getHash(), _supportsPushState = "pushState" in history, _initialHash.indexOf("gid=") > -1 && (_initialHash = _initialHash.split("&gid=")[0], _initialHash = _initialHash.split("?gid=")[0]), _listen("afterChange", self.updateURL), _listen("unbindEvents", function() { framework.unbind(window, "hashchange", self.onHashChange) });
                            var returnToOriginal = function() { _hashReseted = !0, _closedFromURL || (_urlChangedOnce ? history.back() : _initialHash ? _windowLoc.hash = _initialHash : _supportsPushState ? history.pushState("", document.title, _windowLoc.pathname + _windowLoc.search) : _windowLoc.hash = ""), _cleanHistoryTimeouts() };
                            _listen("unbindEvents", function() { _closedByScroll && returnToOriginal() }), _listen("destroy", function() { _hashReseted || returnToOriginal() }), _listen("firstUpdate", function() { _currentItemIndex = _parseItemIndexFromURL().pid });
                            var index = _initialHash.indexOf("pid=");
                            index > -1 && (_initialHash = _initialHash.substring(0, index), "&" === _initialHash.slice(-1) && (_initialHash = _initialHash.slice(0, -1))), setTimeout(function() { _isOpen && framework.bind(window, "hashchange", self.onHashChange) }, 40) } }, onHashChange: function() {
                        return _getHash() === _initialHash ? (_closedFromURL = !0, void self.close()) : void(_hashChangedByScript || (_hashChangedByHistory = !0, self.goTo(_parseItemIndexFromURL().pid), _hashChangedByHistory = !1)) }, updateURL: function() { _cleanHistoryTimeouts(), _hashChangedByHistory || (_historyChanged ? _historyUpdateTimeout = setTimeout(_updateHash, 800) : _updateHash()) } } }), framework.extend(self, publicMethods)
        };
        return PhotoSwipe
    });

/*! loadCSS. [c]2017 Filament Group, Inc. MIT License */
(function(w){"use strict";var loadCSS=function(href,before,media){var doc=w.document;var ss=doc.createElement("link");var ref;if(before){ref=before}
else{var refs=(doc.body||doc.getElementsByTagName("head")[0]).childNodes;ref=refs[refs.length-1]}
var sheets=doc.styleSheets;ss.rel="stylesheet";ss.href=href;ss.media="only x";function ready(cb){if(doc.body){return cb()}
setTimeout(function(){ready(cb)})}
ready(function(){ref.parentNode.insertBefore(ss,(before?ref:ref.nextSibling))});var onloadcssdefined=function(cb){var resolvedHref=ss.href;var i=sheets.length;while(i--){if(sheets[i].href===resolvedHref){return cb()}}
setTimeout(function(){onloadcssdefined(cb)})};function loadCB(){if(ss.addEventListener){ss.removeEventListener("load",loadCB)}
ss.media=media||"all"}
if(ss.addEventListener){ss.addEventListener("load",loadCB)}
ss.onloadcssdefined=onloadcssdefined;onloadcssdefined(loadCB);return ss};if(typeof exports!=="undefined"){exports.loadCSS=loadCSS}
else{w.loadCSS=loadCSS}}(typeof global!=="undefined"?global:this))
/*! loadCSS rel=preload polyfill. [c]2017 Filament Group, Inc. MIT License */
(function(w){if(!w.loadCSS){return}
var rp=loadCSS.relpreload={};rp.support=function(){try{return w.document.createElement("link").relList.supports("preload")}catch(e){return!1}};rp.poly=function(){var links=w.document.getElementsByTagName("link");for(var i=0;i<links.length;i++){var link=links[i];if(link.rel==="preload"&&link.getAttribute("as")==="style"){w.loadCSS(link.href,link,link.getAttribute("media"));link.rel=null}}};if(!rp.support()){rp.poly();var run=w.setInterval(rp.poly,300);if(w.addEventListener){w.addEventListener("load",function(){rp.poly();w.clearInterval(run)})}
if(w.attachEvent){w.attachEvent("onload",function(){w.clearInterval(run)})}}}(this))