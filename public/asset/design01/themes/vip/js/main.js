jQuery(document).ready(function($) {

    var $bannerSlider = $('[data-banner="carousel"]');
    var isIOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

    if ( $bannerSlider.length ) {
        var owl = $bannerSlider;
        var $owlItems = null;
        var $thumbnails = $('[data-bar="thumbnail-item"]');
        var $bannerItems = $('[data-banner="item"]');
        var isStopAutoplay = !0;    // STOP AUTOPLAY AFTER ANY EVENT ON SLIDER

        $bannerSlider.owlCarousel({
            items: 1,
            autoplay: !0,
            loop: !0,
            nav: !0,
            navSpeed: 1200,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !1,
            autoplaySpeed: 1200,
            center: !1,
            lazyLoad: !0,
            dotsContainer: '[data-bar="thumbnail"]',
            onInitialized: function () {
                owl.find('.owl-prev').bind('click', stopAutoplayHandler);
                owl.find('.owl-next').bind('click', stopAutoplayHandler);
                owl.find('.owl-item').bind('click', stopAutoplayHandler);
            },
            onDragged: stopAutoplayHandler
        });

        owl.on('change.owl.carousel', function(event) {
            var $owlItems = $bannerSlider.find('.owl-item');
            var currentIndex = event.item.index;
            var $currentOwlItem = $owlItems.eq(currentIndex);
            var $video = $currentOwlItem.find('video');
            var videoControl = null;

            if ($video.length) {
                videoControl = $video.get(0);
                videoControl.pause();
            }
        });

        owl.on('changed.owl.carousel', function(event) {
            var $owlItems = $bannerSlider.find('.owl-item');
            var currentIndex = event.item.index;
            var $currentOwlItem = $owlItems.eq(currentIndex);
            var $video = $currentOwlItem.find('video');
            var videoControl = null;

            if ($video.length) {
                videoControl = $video.get(0);
                videoControl.play();
                
                $video.off('ended').on('ended', function () {
                    owl.trigger('to.owl.carousel', [currentIndex++, 300, true]);
                    owl.trigger('play.owl.autoplay', 300);
                });

                owl.trigger('stop.owl.autoplay');
            }
        });

        owl.on('dragge.owl.carousel', function(event) {
            owl.trigger('stop.owl.autoplay');
        });

        owl.on('dragged.owl.carousel', function(event) {
            owl.trigger('stop.owl.autoplay');
        });

        function stopAutoplayHandler () {
            console.log(owl);
            owl.trigger('stop.owl.autoplay');
        }
    }
        
    var $nav_toggle = $('[data-bar="nav-toggle"]');
    var nav_toggle_expanded_class = '-expanded';
    var $nav = $('[data-bar="nav"]');
    var is_nav_sliding = false;
    var is_nav_expanded = false;

    var isMobile = function () {
        return $nav_toggle.is(":visible");
    };

    $nav_toggle.bind('click', function(e) {
        e.stopPropagation();

        if (isMobile) {
            is_nav_sliding = true;

            if (is_nav_expanded) {
                // close nav
                $nav_toggle.removeClass(nav_toggle_expanded_class);
                $nav.slideUp(350, function () {
                    is_nav_expanded = false;
                    is_nav_sliding = false;
                });

            } else {
                // expand nav
                $nav_toggle.addClass(nav_toggle_expanded_class);
                $nav.slideDown(350, function () {
                    is_nav_expanded = true;
                    is_nav_sliding = false;
                });
            }
        }
    });

    var $sub_menu_open_button = $('[data-menu="open-sub"]');
    var $sub_menu_close_button = $('[data-menu="close-sub"]');
    var sub_menu_expanded_class = '-mobile-expanded';

    $sub_menu_open_button.bind('click', function (e) {
        var $this = $(this);
        var $nav_item = $this.parents('[data-menu="item"]');
        var $sub_nav = $nav_item.find('ul');

        e.preventDefault();
        e.stopPropagation();

        if (isMobile() && !$sub_nav.hasClass(sub_menu_expanded_class)) {

            $nav.animate(
                {
                    right: -192
                },
                350,
                function() {
                    $sub_nav.addClass(sub_menu_expanded_class);
                    $sub_nav.animate(
                        {
                            right: 192
                        },
                        350,
                        function() {
                            
                        }
                    );
                }
            );
        }
    });

    $sub_menu_close_button.bind('click', function (e) {
        var $this = $(this);
        var $nav_item = $this.parents('[data-menu="item"]');
        var $sub_nav = $nav_item.find('ul');

        if (isMobile() && $sub_nav.hasClass(sub_menu_expanded_class)) {

            $sub_nav.animate(
                {
                    right: 0
                },
                350,
                function() {
                    $sub_nav
                        .removeClass(sub_menu_expanded_class)
                        .css('right', '');
                    $nav.animate(
                        {
                            right: 0
                        },
                        350,
                        function() {

                        }
                    );
                    
                }
            );
        }
    });

    $(document).click(function(e) {
        if (
           !$(e.target).is($nav_toggle)
        ) {

        } 
    });

    var $search_toggle = $('[data-search="toggle"]');
    var $search_form = $('[data-search="form"]');
    var $search_text = $('[data-search="text"]');
    var is_search_transition = false;
    var is_search_showed = false;

    $search_toggle.bind('click', function () {

        if ( isDesktop() && is_search_transition === false ) {
            is_search_transition = true;
            $search_toggle.hide();
            $search_form.animate({
                right: 0},
                500, function() {
                    $search_text.focus();
                    is_search_transition = false;
                    is_search_showed = true;
            });
            
        }
    });

    $search_form.bind('mouseleave', function () {

        if ( isDesktop() && is_search_transition === false ) {
            is_search_transition = true;
            $(':focus').blur();
            $search_form.animate({
                right: -200},
                500, function() {
                    is_search_transition = false;
                    $search_toggle.show();
                    is_search_showed = false;
            });
        }
    });

    var $scrollpane = $('[data-block="scrollpanel"]');

    if (!isIOS) {
        if ($scrollpane.length) {
            $scrollpane.jScrollPane({
                verticalGutter: 0
            });
        }
    }

    $(window).bind('resize', function () {

        if ( is_search_showed && !is_search_transition) {
            $search_form.animate({
                right: -200},
                0, function() {
                    $search_toggle.show();
                    is_search_showed = false;
            });
        }


        if ($scrollpane.length && !isIOS) {
            $scrollpane.jScrollPane({
                verticalGutter: 0
            });
        }

    });

});

function isTouchDevice () {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch (e) {
        return false;
    }
}

function isDesktop () {
    return $('.detect-desktop').is(':visible');
}