<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DevVape</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="title" content="DevVape">
    <meta name="description" content="Cửa hàng thuốc lá điện tử, vape, pod chính hãng tại Việt Nam. Chung cung cấp thân máy vape, pod, tinh dầu và phụ kiện với giá thành và chất lượng ưu việt">
    <meta name="keywords" content="từ khóa trên google 1, từ khóa trên google 2">
    <meta name="theme-color" content="#07a0d2">
    <meta property="og:url" content="">
    <meta property="og:type" content="Website">
    <meta property="og:title" content="DevVape">
    <meta property="og:description" content="Cửa hàng thuốc lá điện tử, vape, pod chính hãng tại Việt Nam. Chung cung cấp thân máy vape, pod, tinh dầu và phụ kiện với giá thành và chất lượng ưu việt">
    <meta property="og:image" content="">

    <link rel="canonical" href="">

    <link rel="icon" type="image/x-icon" href="http://devvape.comhttp://devvape.com/libraries/libraryxhome-842/images/av.png">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/extra.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/animate.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/hover.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/all.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/slick.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/slick-theme.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/fontawesome.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/drawer.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/css/hover-min.css">
    <script src="../../assets/js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../assets/js/slick.min.js"></script>
    <!--script src="../../assets/js/wow.js"></script-->
    <script src="../../assets/js/iscroll.min.js"></script>
    <script src="../../assets/js/drawer.min.js"></script>
    <script src="../../assets/js/jquery.sticky-kit.js"></script>
    <script src="../../assets/js/jquery.matchHeight-min.js"></script>
</head>

<body class="f3 f1" style="">
<header data-vvveb-disabled>
        <div class="headerTop bgmt" id="hideHeader" style="background-color: rgb(128, 0, 0);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-3 site-branding col-xs-12">
                        <div class="logoHeader pdt10 pdb10">
                            <a href="/" title="logo">
                                <img src="http://devvape.com/libraries/libraryxhome-842/images/av.png" alt=""/>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 white" style="background-color: rgb(128, 0, 0);">
                        <div class="row">
                            <div class="contactHeaders inBlock col-md-3">
                                <div class="contactHeader">
                                    <i class="fas fa-phone-alt"></i>
                                </div>
                                <div class="header-info">
                                    <span class="phone">0852892811</span>
                                </div>
                            </div>
                            <div class="contactHeaders inBlock col-md-9">
                                <div class="contactHeader">
                                    <i class="fas fa-home"></i>
                                </div>
                                <div class="header-info">
                                    <span class="phone">307 Đường Trâu Quỳ, Tt Trâu Quỳ, Gia Lâm, Hà Nội</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navigation gray-bg" id="myHeader">
            <div class="container">
                <div class="row">
                    <ul class="nav col-md-10">
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/">Trang chủ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/cua-hang/vapekit">VAPEKIT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/cua-hang/pod-system">POD SYSTEM</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/cua-hang/tinh-dau-salt-nic">TINH DẦU SALT NIC</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/cua-hang/tinh-dau-freebase-2899">TINH DẦU FREEBASE</a>
                            <ul class="subMenu">
                                <li class="nav-item">
                                    <a class="nav-link textUpper black fw7 colorhv menuPosition" href="/cua-hang/tinh-dau-my-2909">tinh dầu mỹ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link textUpper black fw7 colorhv menuPosition" href="/cua-hang/tinh-dau-malaysia-2910">tinh dầu malaysia</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/cua-hang/phu-kien">PHỤ KIỆN</a>
                            <ul class="subMenu">
                                <li class="nav-item">
                                    <a class="nav-link textUpper black fw7 colorhv menuPosition" href="/cua-hang/phu-kien-vape-2911">phụ kiện vape</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link textUpper black fw7 colorhv menuPosition" href="/cua-hang/phu-kien-pod-system-2912">phụ kiện pod system</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/danh-muc/tin-tuc">Tin tức</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link textUpper black fw7 colorhv" href="/lien-he">Liên hệ</a>
                        </li>
                    </ul>
                    <div class="col-md-2 nav-item " id="search-form">
                        <a style="cursor: pointer;">
                            <form method="get" action="/tim-kiem">
                                <input id="search_input" value="" type="text" name="word" placeholder="Tìm kiếm...">
                            </form>
                        </a>
                    </div>
                </div>
            </div>
        </nav>
    </header>