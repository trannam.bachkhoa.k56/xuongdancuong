<!DOCTYPE html>
<html lang="en">

<head>

    </script>

    <meta name="p:domain_verify" content="0bc5d7433f35424aad6d2134ebaeba2b" />

    <meta name="p:domain_verify" content="0bc5d7433f35424aad6d2134ebaeba2b" />

    <!--Start of Zendesk Chat Script-->

    <!--End of Zendesk Chat Script-->
    <meta name="google-site-verification" content="HLwyfTHUOzO6FlduzK-YQhE4gw2-gPj4XtxnfkweM6g" />
    <meta charset="utf-8" />

    <!-- Basic Page Needs
================================================== -->

    <title>
        Your Shopping Cart &ndash; The Print Emporium
    </title>

    <?php include('head.php'); ?>

    <!-- CSS
================================================== -->

    <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>

<body class="gridlock  cart">
     <?php include('header/header_mobile.php')?>

    <div class="page-wrap">

        <!-- HEADER -->
        <?php include('header/header.php')?>

        <div class="content-wrapper">

            <div id="content" class="row">

                <div id="shopping-cart" class="desktop-12 tablet-6 mobile-3">

                    <h2>My Cart</h2>

                    <form action="/cart" method="post" id="cartform">

                        <table>
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th style="text-align: center;">Qty</th>
                                    <th style="text-align: center;">Remove</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>

                                <!-- product feather-canvas set-price -->
                                <!-- product.price 16900: wh_price 16900-->
                                <!-- product.price_min 16900 : wh_price_min 16900 -->
                                <!-- product.price_max 89900 : wh_price_max 89900 -->

                                <!-- wh_discount_value 1 -->
                                <!-- compare_at_price : wh_compare_at_price  -->
                                <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                                <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                                <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                <!-- variant.price 16900 : wh_v_price 16900 -->

                                <tr>
                                    <td class="cart-item">
                                        <div class="cart-image">
                                            <a href="/products/feather-canvas?variant=6950973931580" title="Feather Canvas">
                                                <img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_compact.jpg?v=1522150047" alt="Feather Canvas - 45 x 60cm / (NONE)" />
                                            </a>
                                        </div>
                                        <div class="cart-title">

                                            Feather Canvas - 45 x 60cm / (NONE)

                                            <span class="Bold-theme-hook-DO-NOT-DELETE bold_cart_item_properties" style="display:none !important;"></span>

                                        </div>
                                    </td>

                                    <td class="cart-price">
                                        <span class="Bold-theme-hook-DO-NOT-DELETE bold_cart_item_price" style="display:none !important;"></span>$169.00
                                    </td>

                                    <td class="cart-quantity">
                                        <input type="text" class="cart-qty" size="4" name="updates[6950973931580]" id="updates_6950973931580" max='999' value="5" onfocus="this.select();" />
                                    </td>
                                    <td class="cart-remove">
                                        <a href="#" onclick="remove_item(6950973931580); return false;"><i class="icon-trash icon-2x"></i></a>
                                    </td>

                                    <td class="cart-total">
                                        <span class="Bold-theme-hook-DO-NOT-DELETE bold_cart_item_total" style="display:none !important;"></span>$845.00
                                    </td>

                                </tr>

                            </tbody>
                        </table>
                        <div class="notice" style="float: none; text-align: left; clear: both; margin: 10px 0;">
                            <!--         <input style="float:none; vertical-align: middle;" type="checkbox" id="agree" /> -->
                            <label style="display:inline; float:none" for="agree">
                                For Australian customers - we ship framed and unframed prints and canvases. For customers who live outside of Australia, we can only ship unframed items. Please see more info on our FAQ page: <a target="_blank" href="/pages/faq">HERE</a>
                                <!--           Please note, for customers outside of Australia we cannot ship framed products. We can however ship all unframed prints and rolled canvases (rolled in mailer tube) Thanks!.  <a target="_blank" href="/pages/faq">See our FAQ HERE for more info</a>. -->
                            </label>
                        </div>
                        

                        

                    </form>

                  
                </div>

            </div>

        </div>

    </div>
    <!-- End page wrap for sticky footer -->

    <div id="footer-wrapper">
        <div id="footer" class="row">

            <div class="desktop-3 tablet-half mobile-half">
                <h4>RESOURCES</h4>
                <ul>

                    <li><a href="/pages/faq" title="">FAQ / INFO</a></li>

                    <li><a href="/pages/about-our-process" title="">ABOUT OUR PROCESS</a></li>

                    <li><a href="/pages/about-us" title="">ABOUT US</a></li>

                    <li><a href="/pages/contact" title="">CONTACT</a></li>

                    <li><a href="http://www.instagram.com/theprintemporium" title="">INSTAGRAM</a></li>

                    <li><a href="/pages/privacy-policy" title="">PRIVACY POLICY</a></li>

                    <li><a href="/pages/press" title="">PRESS / AS SEEN IN </a></li>

                </ul>
            </div>

            <div class="desktop-3 tablet-half mobile-half">

                <h4>ACCOUNT</h4>

                <ul>

                    <li><a href="https://www.theprintemporium.com.au/account/login" title="">CREATE ACCOUNT</a></li>

                    <li><a href="https://www.theprintemporium.com.au/account/login" title="">SIGN IN TO ACCOUNT</a></li>

                    <li><a href="/pages/enquiries" title="">WHOLESALE / TRADE</a></li>

                    <li><a href="/pages/payment-info" title="">PAYMENT INFO</a></li>

                    <li><a href="/pages/shipping-info" title="">SHIPPING INFO</a></li>

                    <li><a href="/pages/zippay" title="">PAY LATER WITH ZIPPAY</a></li>

                    <li><a href="/products/gift-card" title="">GIFT CARD</a></li>

                    <li><a href="/pages/contact" title="">CONTACT US</a></li>

                </ul>
            </div>

            <div class="desktop-3 tablet-half mobile-half">

                <h4>SHOP</h4>

                <ul>

                    <li><a href="/collections/all" title="">SHOP ALL ART</a></li>

                    <li><a href="/collections/canvas-art" title="">CANVAS ART</a></li>

                    <li><a href="/collections/art-prints" title="">ART PRINTS</a></li>

                    <li><a href="/search" title="">SEARCH</a></li>

                </ul>
            </div>

            <div class="desktop-3 tablet-half mobile-3">
                <h4>Connect</h4>
                <div id="footer_signup">
                    <p>Subscribe To Receive A Coupon Code For $10 Off Your First Order & News Updates:</p>
                    <form action="//theprintemporium.us15.list-manage.com/subscribe/post?u=84f35a8ad4dc5547f6e9b4b62&amp;id=e3d0f09f99" method="post" id="footer-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
                        <input value="" name="EMAIL" class="email" id="footer-EMAIL" placeholder="Enter Email Address" required="" type="email">
                        <input value="Join" name="subscribe" id="footer-subscribe" class="button" type="submit">
                    </form>
                </div>
            </div>

            <div class="clear"></div>

            <ul id="footer-icons" class="desktop-12 tablet-6 mobile-3">
                <li><a href="https://www.facebook.com/THEPRINTEMPORIUM1" target="_blank"><i class="icon-facebook icon-2x"></i></a></li>
                <li><a href="https://twitter.com/ThePrintEmporiu" target="_blank"><i class="icon-twitter icon-2x"></i></a></li>
                <li><a href="https://www.pinterest.com/theprintemporium" target="_blank"><i class="icon-pinterest icon-2x"></i></a></li>

                <li><a href="//instagram.com/THEPRINTEMPORIUM" target="_blank"><i class="icon-instagram icon-2x"></i></a></li>
                <li><a href="https://theprintemporium.com.au/blogs/news.atom" target="_blank"><i class="icon-rss icon-2x"></i></a></li>
            </ul>

            <div class="clear"></div>

            <div class="credit desktop-12 tablet-6 mobile-3">
                <p>
                    Copyright &copy; 2018 <a href="/" title="">The Print Emporium </a>
                    <br>ALL prices are in AUD
                </p>
            </div>
        </div>
    </div>

    <div style='display:none'>
        <div id='search_popup' style='padding:30px;'>
            <p class="box-title">Search our store
                <p>
                    <!-- BEGIN #subs-container -->
                    <div id="subs-container" class="clearfix">
                        <div id="search">
                            <form action="/search" method="get">
                                <input type="text" name="q" id="q" placeholder="Enter your search terms" />
                            </form>
                        </div>
                    </div>
        </div>
    </div>

    <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
            if ($(window).width() >= 741) {

                $(document).ready(function() {
                    //enabling stickUp on the '.navbar-wrapper' class
                    $('nav').stickUp();
                });
            }

        });
    </script>

    <script id="cartTemplate" type="text/x-handlebars-template">

        {{#each items}}
        <div class="quick-cart-item">
            <div class="quick-cart-image">
                <a href="{{ this.url }}" title="{{ this.title }}">
                    <img src="{{ this.image }}" alt="{{ this.title }}" />
                </a>
            </div>
            <div class="quick-cart-details">
                <p>
                    <a href="{{ this.url }}">{{ this.title }}</a>
                </p>
                <p>{{ this.price }}</p>
                <p>
                    <a class="remove_item" href="#" data-id="{{ this.id }}">Remove</a>
                </p>
            </div>
        </div>
        {{/each}}
        <a class="checkout-link" href="/cart">Checkout</a>

    </script>

    <a href="#" class="scrollup"><i class="icon-angle-up icon-2x"></i></a>

    <!-- Begin Recently Viewed Products -->

</body>

</html>