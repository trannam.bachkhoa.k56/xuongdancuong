<div id="footer-wrapper"> 
    <div id="footer" class="row">

      
      
      
       
              
      
      <div class="desktop-3 tablet-half mobile-half">
        <h4>RESOURCES</h4>
        <ul>
          
          <li><a href="/pages/faq" title="">FAQ / INFO</a></li>
          
          <li><a href="/pages/about-our-process" title="">ABOUT OUR PROCESS</a></li>
          
          <li><a href="/pages/about-us" title="">ABOUT US</a></li>
          
          <li><a href="/pages/contact" title="">CONTACT</a></li>
          
          <li><a href="http://www.instagram.com/theprintemporium" title="">INSTAGRAM</a></li>
          
          <li><a href="/pages/privacy-policy" title="">PRIVACY POLICY</a></li>
          
          <li><a href="/pages/press" title="">PRESS / AS SEEN IN </a></li>
          
        </ul>
      </div>

      
      <div class="desktop-3 tablet-half mobile-half">
        
        <h4>ACCOUNT</h4>
        
        <ul>
          
          <li><a href="https://www.theprintemporium.com.au/account/login" title="">CREATE ACCOUNT</a></li>
          
          <li><a href="https://www.theprintemporium.com.au/account/login" title="">SIGN IN TO ACCOUNT</a></li>
          
          <li><a href="/pages/enquiries" title="">WHOLESALE / TRADE</a></li>
          
          <li><a href="/pages/payment-info" title="">PAYMENT INFO</a></li>
          
          <li><a href="/pages/shipping-info" title="">SHIPPING INFO</a></li>
          
          <li><a href="/pages/zippay" title="">PAY LATER WITH ZIPPAY</a></li>
          
          <li><a href="/products/gift-card" title="">GIFT CARD</a></li>
          
          <li><a href="/pages/contact" title="">CONTACT US</a></li>
          
        </ul>
      </div>
      

      
      <div class="desktop-3 tablet-half mobile-half">
                
        <h4>SHOP</h4>
        
        <ul>
          
          <li><a href="/collections/all" title="">SHOP ALL ART</a></li>
          
          <li><a href="/collections/canvas-art" title="">CANVAS ART</a></li>
          
          <li><a href="/collections/art-prints" title="">ART PRINTS</a></li>
          
          <li><a href="/search" title="">SEARCH</a></li>
          
        </ul>
      </div>
      

      <div class="desktop-3 tablet-half mobile-3">
        <h4>Connect</h4>
        <div id="footer_signup">
          <p>Subscribe To Receive A Coupon Code For $10 Off Your First Order & News Updates:</p>
          <form action="//theprintemporium.us15.list-manage.com/subscribe/post?u=84f35a8ad4dc5547f6e9b4b62&amp;id=e3d0f09f99" method="post" id="footer-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
            <input value="" name="EMAIL" class="email" id="footer-EMAIL" placeholder="Enter Email Address" required="" type="email">
            <input value="Join" name="subscribe" id="footer-subscribe" class="button" type="submit">
          </form>
        </div> 
      </div>      
      
      <div class="clear"></div>

      <ul id="footer-icons" class="desktop-12 tablet-6 mobile-3">
        <li><a href="https://www.facebook.com/THEPRINTEMPORIUM1" target="_blank"><i class="icon-facebook icon-2x"></i></a></li>
        <li><a href="https://twitter.com/ThePrintEmporiu" target="_blank"><i class="icon-twitter icon-2x"></i></a></li>
        <li><a href="https://www.pinterest.com/theprintemporium" target="_blank"><i class="icon-pinterest icon-2x"></i></a></li>
        
        
        
        <li><a href="//instagram.com/THEPRINTEMPORIUM" target="_blank"><i class="icon-instagram icon-2x"></i></a></li>
        <li><a href="https://theprintemporium.com.au/blogs/news.atom" target="_blank"><i class="icon-rss icon-2x"></i></a></li>
      </ul>      

      <div class="clear"></div>

      <div class="credit desktop-12 tablet-6 mobile-3">
        <p>
          Copyright &copy; 2018 <a href="/" title="">The Print Emporium </a>
          <br>ALL prices are in AUD
        </p>
      </div>
    </div> 
  </div>