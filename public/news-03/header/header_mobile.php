<div id="dl-menu" class="dl-menuwrapper">
        <button class="dl-trigger"><i class="icon-align-justify"></i></button>
        <ul class="dl-menu">

            <li>
                <a href="/" title="">HOME</a>

            </li>

            <li>
                <a href="/collections/all" title="">SHOP ALL ART</a>

            </li>

            <li>
                <a href="/collections/canvas-art" title="">CANVAS ART</a>

            </li>

            <li>
                <a href="/collections/art-prints" title="">ART PRINTS</a>

            </li>

            <li>
                <a href="/collections/photography-series" title="">PHOTO ART</a>

            </li>

            <li>
                <a href="/pages/faq" title="">FAQ / INFO</a>

            </li>

            <li>
                <a href="/pages/about-us" title="">ABOUT US</a>

            </li>

            <li>
                <a href="/products/gift-card" title="">GIFT CARD</a>

            </li>

            <li>
                <a href="/pages/press" title="">PRESS</a>

            </li>

        </ul>
    </div>
    <!-- /dl-menuwrapper -->

    <script>
        $(function() {
            $('#dl-menu').dlmenu({
                animationClasses: {
                    classin: 'dl-animate-in-2',
                    classout: 'dl-animate-out-2'
                }
            });
        });
    </script>