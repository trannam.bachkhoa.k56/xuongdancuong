 <div id="hello" class="desktop-12 tablet-12 mobile-12">
            <p>FREE SHIPPING ON ALL ORDERS OVER $150 AUD - ALSO WE NOW SELL GIFT CARDS TOO</p>
            <a href="#"><i class="fa fa-close"></i></a>
        </div>

        <header>
            <div class="row">

                <ul id="social-icons" class="desktop-6 tablet-6 mobile-hide">
                    <li><a href="https://www.facebook.com/THEPRINTEMPORIUM1" target="_blank"><i class="icon-facebook icon-2x"></i></a></li>
                    <li><a href="https://twitter.com/ThePrintEmporiu" target="_blank"><i class="icon-twitter icon-2x"></i></a></li>
                    <li><a href="https://www.pinterest.com/theprintemporium" target="_blank"><i class="icon-pinterest icon-2x"></i></a></li>

                    <li><a href="//instagram.com/THEPRINTEMPORIUM" target="_blank"><i class="icon-instagram icon-2x"></i></a></li>
                    <li><a href="https://theprintemporium.com.au/blogs/news.atom" target="_blank"><i class="icon-rss icon-2x"></i></a></li>
                </ul>

                <ul id="cart" class="desktop-6 tablet-6 mobile-3">

                    <li><a href="https://theprintemporium.com.au"><i class="icon-home icon-2x"></i></a>
                        <li class="seeks"><a id="inline" href="#search_popup" class="open_popup"><i class="icon-search icon-2x"></i></a></li>
                        <li class="seeks-mobile"><a href="/search"><i class="icon-search icon-2x"></i></a></li>

                        <li class="cust"><a href="/account/login"><i class="icon-user icon-2x"></i></a></li>

                        <li class="cart-overview"><a href="/cart">MY CART&nbsp; <i class="icon-shopping-cart icon-2x"></i>&nbsp; <span id="item_count">5</span></a>

                            <div id="crt">

                                <div class="quick-cart-item">
                                    <div class="quick-cart-image">
                                        <a href="/products/feather-canvas?variant=6950973931580" title="Feather Canvas">

                                            <img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_small.jpg?v=1522150047" alt="Feather Canvas - 45 x 60cm / (NONE)" />

                                        </a>
                                    </div>
                                    <div class="quick-cart-details">
                                        <p><a href="/products/feather-canvas?variant=6950973931580">Feather Canvas - 45 x 60cm / (NONE)</a></p>
                                        <p>$169.00</p>
                                        <a class="remove_item" href="#" data-id="6950973931580">Remove</a>
                                    </div>
                                </div>

                                <a class="checkout-link" href="/cart">Checkout</a>

                            </div>
                        </li>

                </ul>

            </div>
        </header>
        <div class="header-wrapper">
            <div class="row">
                <div id="logo" class="desktop-12 tablet-6 mobile-2">

                    <a href="/"><img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/logo.png?17897852212690934772" alt="The Print Emporium " style="border: 0;" /></a>

                </div>
            </div>

            <div class="clear"></div>

            <nav>
                <ul id="main-nav" role="navigation" class="row">

                    <li><a href="/" title="">HOME</a></li>

                    <li><a href="/collections/all" title="">SHOP ALL ART</a></li>

                    <li><a href="/collections/canvas-art" title="">CANVAS ART</a></li>

                    <li><a href="/collections/art-prints" title="">ART PRINTS</a></li>

                    <li><a href="/collections/photography-series" title="">PHOTO ART</a></li>

                    <li><a href="/pages/faq" title="">FAQ / INFO</a></li>

                    <li><a href="/pages/about-us" title="">ABOUT US</a></li>

                    <li><a href="/products/gift-card" title="">GIFT CARD</a></li>

                    <li><a href="/pages/press" title="">PRESS</a></li>

                </ul>
            </nav>

            <div class="clear"></div>
        </div>