<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Open+Sans+Condensed:300,600' rel='stylesheet' type='text/css'>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/queries.css" rel="stylesheet" type="text/css" media="all" />
      <link href="css/styles.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/option_selection-ea4f4a242e299f2227b2b8038152223f741e90780c0c766883939e8902542bda.js" type="text/javascript"></script>
    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <script src="js/handlebars.js" type="text/javascript"></script>
    <script src="js/api.js" type="text/javascript"></script>
    <script src="js/select.js" type="text/javascript"></script>
    <script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
    <script src="js/jquery.placeholder.js" type="text/javascript"></script>
    <script src="js/modernizr.custom.js" type="text/javascript"></script>
    <script src="js/jquery.fancybox.js" type="text/javascript"></script>
    <script src="js/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script>
    <script src="js/jquery.dlmenu.js" type="text/javascript"></script>
    <script src="js/jquery.flexslider.js" type="text/javascript"></script>
    <script src="js/wow.js" type="text/javascript"></script>
    <script src="js/jquery.nice-select.min.js" type="text/javascript"></script>
    <script src="js/fastclick.js" type="text/javascript"></script>
    <script src="js/theme.js" type="text/javascript"></script>
    <script>
        new WOW().init();
    </script>
    <script src="js/jquery.bxslider.js" type="text/javascript"></script>
    <script src="js/jquery.elevateZoom-2.5.5.min.js" type="text/javascript"></script>
    <script src="js/instafeed.js" type="text/javascript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
    <link rel="shortcut icon" href="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/favicon.png?17897852212690934772">
    <meta id="shopify-digital-wallet" name="shopify-digital-wallet" content="/12446908/digital_wallets/dialog">
    <meta name="shopify-checkout-api-token" content="9f32b3a2a3c74a85783b7c3246d2d14f">
    <meta id="in-context-paypal-metadata" data-shop-id="12446908" data-environment="production" data-locale="en_US" data-merchant-id="J3L5G9JP6WC7C" data-redirect-url="">

    <script defer="defer" integrity="sha256-2/0AMx/qQYiOWAQ4bnrweBUvInpiqgnB+jz8/0gG8X8=" crossorigin="anonymous" src="js/express_buttons-dbfd00331fea41888e5804386e7af078152f227a62aa09c1fa3cfcff4806f17f.js"></script>
    <script defer="defer" src="js/ga_urchin_forms-68ca1924c495cfc55dac65f4853e0c9a395387ffedc8fe58e0f2e677f95d7f23.js"></script>
    <link href="css/bold-options.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/options.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).load(function() {
            $('.prod-image').matchHeight();

        });
    </script>
    <link rel="next" href="/next" />
    <link rel="prev" href="/prev" />
     <script src="js/jquery.tmpl.min.js" type="text/javascript"></script>
    <script src="js/jquery.products.min.js" type="text/javascript"></script>
    <script src="js/zipmoney-widgets-v1.min.js" type="text/javascript"></script>