   <section class="homepage-section animate wow fadeIn">

                <div class="flexslider">
                    <ul class="slides">

                        <li>
                            <a href="/collections/all">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/slide1.jpg?17897852212690934772" alt="" />
                            </a>
                            <div class="flex-caption slide1">

                            </div>
                        </li>

                        <li>
                            <a href="https://theprintemporium.com.au/products/gift-card">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/slide2.jpg?17897852212690934772" alt="" />
                            </a>
                            <div class="flex-caption slide2">

                            </div>
                        </li>

                        <li>
                            <a href="/collections/all">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/slide3.jpg?17897852212690934772" alt="" />
                            </a>
                            <div class="flex-caption slide3">

                            </div>
                        </li>

                        <li>
                            <a href="/collections/all">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/slide4.jpg?17897852212690934772" alt="" />
                            </a>
                            <div class="flex-caption slide4">

                            </div>
                        </li>

                        <li>
                            <a href="/collections/all">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/slide5.jpg?17897852212690934772" alt="" />
                            </a>
                            <div class="flex-caption slide5">

                            </div>
                        </li>

                    </ul>
                </div>

                <div class="clear"></div>
            </section>