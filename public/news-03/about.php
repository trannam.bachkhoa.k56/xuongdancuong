<!DOCTYPE html>

         <html lang="en">
            <!--<![endif]-->
            <head>
               <meta name="p:domain_verify" content="0bc5d7433f35424aad6d2134ebaeba2b"/>
               <meta name="p:domain_verify" content="0bc5d7433f35424aad6d2134ebaeba2b"/>
               <!--Start of Zendesk Chat Script-->
               <!--End of Zendesk Chat Script-->
               <meta name="google-site-verification" content="HLwyfTHUOzO6FlduzK-YQhE4gw2-gPj4XtxnfkweM6g" />
               <meta charset="utf-8" />
               <!-- Basic Page Needs
                  ================================================== -->
               <title>
                  About Us | Designer Contemporary Wall Art Prints | The Print Emporium &ndash; The Print Emporium 
               </title>
               <?php include('head.php'); ?>
            </head>
            <body class="gridlock  page">
               <?php include('header/header_mobile.php')?> 
               <div class="page-wrap">
                  <?php include('header/header.php')?>
                  <div class="content-wrapper">
                     <div id="content" class="row">
                        <div id="page" class="desktop-12 tablet-6 mobile-3">
                           <div id="page-content"">
                              <h1 class="page-title">ABOUT US</h1>
                              <div class="rte">
                                 <p style="text-align: center;"><img alt="About Us" src="//cdn.shopify.com/s/files/1/1244/6908/files/About-Us_banner_2048x2048.jpg?v=1490873382" style="float: none;"></p>
                                 <p> <img src="//cdn.shopify.com/s/files/1/1244/6908/files/Me_large.jpg?v=1502339960" alt="About Us"></p>
                                 <p>The Print Emporium is a newly launched Melbourne based contemporary art brand – Our aim is to deliver an ever-growing range of on trend, unique and affordable designer artwork.</p>
                                 <p>Founder and Creative Director Zoe Bristow, comes from a background in textile, fashion, homewares design &amp; buying in Melbourne, Sydney, and the United Kingdom. Zoe now leads a team of artists to create the curated range of unique artwork – the core of which has been originally hand-painted in oil or acrylic paints. After hand painting designs are then digitised using superior reproduction techniques, then printed as either a stretched canvas, or as a giclée art print (unframed or framed option). The incredible detail of the original brush strokes is clearly visible, almost as if it were the original painting. A photo series is also available. The collection is proudly made in Australia using high quality finishes and genuine oak framing – and are available in a variety of size options.</p>
                                 <p>See more <strong><a href="https://theprintemporium.com.au/pages/about-our-process" title="About Our Process">ABOUT OUR PROCESS HERE.</a></strong></p>
                                 <p><span>We will keep adding to our collection with new and inspiring designs, so to keep in the loop we would love you to join our mailing list to hear of new releases and news.</span></p>
                                 <p>Got a question for our team? Don't hesitate to get in touch! <strong><a href="mailto:hello@theprintemporium.com.au">hello@theprintemporium.com.au</a> </strong>or use our <a href="https://www.theprintemporium.com.au/pages/contact" title="CONTACT US">CONTACT US</a> page</p>
                                 <p> </p>
                                 <p>Happy browsing!</p>
                                 <p> </p>
                                 <p>The Print Emporium Team </p>
                                 <p>Melbourne, Australia</p>
                                 <p> </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- End page wrap for sticky footer -->
              
               <script type="text/javascript">
                  //initiating jQuery
                  jQuery(function($) {
                    if ($(window).width() >= 741) {
                  
                      $(document).ready( function() {
                        //enabling stickUp on the '.navbar-wrapper' class
                        $('nav').stickUp();
                      });
                    }
                  
                  });
               </script> 
              
               <a href="#" class="scrollup"><i class="icon-angle-up icon-2x"></i></a>
            </body>
         </html>