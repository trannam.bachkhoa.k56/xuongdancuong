<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="google-site-verification" content="HLwyfTHUOzO6FlduzK-YQhE4gw2-gPj4XtxnfkweM6g" />
    <meta charset="utf-8" />

    <!-- Basic Page Needs
================================================== -->

    <title>
        The Print Emporium | Contemporary Canvases &amp; Art Prints
    </title>
    <meta name="description" content="Shop Our Designer Range of Art Prints &amp; Canvas Artwork. We Ship Worldwide." />
    <link rel="canonical" href="https://theprintemporium.com.au/" />
    <!-- CSS
================================================== -->
    <?php include('head.php'); ?>

</head>

<body class="gridlock  index">
     <?php include('header/header_mobile.php')?>
    <div class="page-wrap">
        <!-- HEADER -->
        <?php include('header/header.php')?>

        <div class="content-wrapper">

        <!--  SLIDER -->
         <?php include('slider/slider.php')?>
            <div class="single-image">

                <section class="homepage-section animate wow fadeIn no-fouc">
                    <div class="row">
                        <div class="section-title lines desktop-12 tablet-6 mobile-3">
                            <h2>AS SEEN IN</h2></div>
                        <div class="homepage-partners">

                            <div class="partner-item">
                                <a href="#">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/logo01.jpg?17897852212690934772" alt="#">
                                </a>
                            </div>

                            <div class="partner-item">
                                <a href="#">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/logo02.jpg?17897852212690934772" alt="#">
                                </a>
                            </div>

                            <div class="partner-item">
                                <a href="#">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/logo03.jpg?17897852212690934772" alt="#">
                                </a>
                            </div>

                            <div class="partner-item">
                                <a href="#">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/logo05.jpg?17897852212690934772" alt="#">
                                </a>
                            </div>

                            <div class="partner-item">
                                <a href="#">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/logo06.jpg?17897852212690934772" alt="#">
                                </a>
                            </div>

                            <div class="partner-item">
                                <a href="#">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/logo07.jpg?17897852212690934772" alt="#">
                                </a>
                            </div>

                        </div>
                    </div>
                </section>

            </div>

            <div class="single-image">

                <section class="homepage-section animate wow fadeIn no-fouc">
                    <div class="row">

                        <div class="homepage-promo desktop-6 mobile-2">

                            <div class="promo-inner">
                                <a href="https://www.theprintemporium.com.au/pages/about-our-process">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo21.jpg?17897852212690934772" alt="">
                                    <div class="caption">
                                        <h3></h3>
                                        <p></p>
                                    </div>
                                </a>
                            </div>

                        </div>

                        <div class="homepage-promo desktop-6 mobile-2">

                            <div class="promo-inner">
                                <a href="https://www.theprintemporium.com.au/collections/coastal-tropical">
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo22.jpg?17897852212690934772" alt="">
                                    <div class="caption">
                                        <h3></h3>
                                        <p></p>
                                    </div>
                                </a>
                            </div>

                        </div>

                    </div>
                </section>

            </div>

            <section class="homepage-section animate wow fadeIn no-fouc">
                <div class="row">

                    <div class="homepage-promo desktop-12 mobile-1">

                        <div class="promo-inner">
                            <a href="">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo5.jpg?17897852212690934772" alt="">
                                <div class="caption">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </a>
                        </div>

                    </div>

                </div>
            </section>

            <section class="homepage-section animate wow fadeIn no-fouc">
                <div class="row">

                    <div class="homepage-promo desktop-6 mobile-2">

                        <div class="promo-inner">
                            <a href="https://the-print-emporium.myshopify.com/collections/florals-botanicals">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo25.jpg?17897852212690934772" alt="">
                                <div class="caption">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </a>
                        </div>

                    </div>

                    <div class="homepage-promo desktop-6 mobile-2">

                        <div class="promo-inner">
                            <a href="https://the-print-emporium.myshopify.com/collections/scandinavian-winter">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo26.jpg?17897852212690934772" alt="">
                                <div class="caption">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </a>
                        </div>

                    </div>

                </div>
            </section>

            <section class="homepage-section animate wow fadeIn no-fouc">
                <div class="row">

                    <div class="homepage-promo desktop-4 mobile-3">

                        <div class="promo-inner">
                            <a href="https://www.theprintemporium.com.au/collections/canvas-art">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo1.jpg?17897852212690934772" alt="">
                                <div class="caption">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </a>
                        </div>

                    </div>

                    <div class="homepage-promo desktop-4 mobile-3">

                        <div class="promo-inner">
                            <a href="https://www.theprintemporium.com.au/collections/art-prints">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo2.jpg?17897852212690934772" alt="">
                                <div class="caption">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </a>
                        </div>

                    </div>

                    <div class="homepage-promo desktop-4 mobile-3">

                        <div class="promo-inner">
                            <a href="https://www.theprintemporium.com.au/pages/faq">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo3.jpg?17897852212690934772" alt="">
                                <div class="caption">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </a>
                        </div>

                    </div>

                </div>
            </section>
            <link href="css/stylesheet.css" rel="stylesheet" type="text/css" media="all" />
            <section class="homepage-section animate wow fadeIn no-fouc best-sellers">
                <div class="row">
                    <div class="section-title lines desktop-12 tablet-6 mobile-3">
                        <h2>FEATURED WALL ART</h2></div>

                    <div class="collection-carousel desktop-12 tablet-6 mobile-3">

                        <div class="lazyOwl" id="product-listing-9519862605" data-alpha="Black Cockatoo Canvas Art" data-price="16900">

                            <div class="product-index-inner">

                            </div>

                            <div class="prod-image">
                                <a class="product-link" href="/collections/best-sellers/products/black-cockatoo-canvas" title="Black Cockatoo Canvas Art">
                                    <div class="hover-element">
                                    </div>
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/products/black-cockatoo-canvas-art-by-the-print-emporium_large.jpg?v=1522089066" alt="" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/black-cockatoo-canvas-art-by-the-print-emporium_large.jpg?v=1522089066" data-alt-image="//cdn.shopify.com/s/files/1/1244/6908/products/black-cockatoo-canvas-art-by-the-print-emporium-2_large.jpg?v=1522089066" />
                                </a>

                            </div>

                            <div class="product-info">
                                <div class="product-info-inner">
                                    <a href="/collections/best-sellers/products/black-cockatoo-canvas">

                                        <h3>Black Cockatoo Canvas Art</h3>
                                    </a>

                                    <div class="price">

                                        <div class="prod-price">

                                            From $169.00
                                            <!-- - $899.00 -->
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="lazyOwl" id="product-listing-9571135565" data-alpha="Elk Canvas" data-price="16900">

                            <div class="product-index-inner">

                            </div>

                            <div class="prod-image">
                                <a class="product-link" href="/collections/best-sellers/products/elk-canvas" title="Elk Canvas">
                                    <div class="hover-element">
                                    </div>
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/products/elk-canvas-by-the-print-emporium_large.jpg?v=1522090254" alt="" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/elk-canvas-by-the-print-emporium_large.jpg?v=1522090254" data-alt-image="//cdn.shopify.com/s/files/1/1244/6908/products/elk-canvas-by-the-print-emporium-2_large.jpg?v=1522090254" />
                                </a>

                            </div>

                            <div class="product-info">
                                <div class="product-info-inner">
                                    <a href="/collections/best-sellers/products/elk-canvas">

                                        <h3>Elk Canvas</h3>
                                    </a>

                                    <div class="price">

                                        <div class="prod-price">

                                            From $169.00
                                            <!-- - $899.00 -->
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="lazyOwl" id="product-listing-9530235021" data-alpha="White Cockatoo Canvas" data-price="16900">

                            <div class="product-index-inner">
                            </div>

                            <div class="prod-image">
                                <a class="product-link" href="/collections/best-sellers/products/white-cockatoo-canvas" title="White Cockatoo Canvas">
                                    <div class="hover-element">
                                    </div>
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/products/white-cockatoo-canvas-by-the-print-emporium_large.jpg?v=1522089282" alt="" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/white-cockatoo-canvas-by-the-print-emporium_large.jpg?v=1522089282" data-alt-image="//cdn.shopify.com/s/files/1/1244/6908/products/white-cockatoo-canvas-by-the-print-emporium-2_large.jpg?v=1522089282" />
                                </a>
                            </div>

                            <div class="product-info">
                                <div class="product-info-inner">
                                    <a href="/collections/best-sellers/products/white-cockatoo-canvas">

                                        <h3>White Cockatoo Canvas</h3>
                                    </a>
                                    <div class="price">

                                        <div class="prod-price">

                                            From $169.00
                                            <!-- - $899.00 -->
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="lazyOwl" id="product-listing-9453177997" data-alpha="European Cove Canvas" data-price="16900">

                            <div class="product-index-inner">

                            </div>

                            <div class="prod-image">
                                <a class="product-link" href="/collections/best-sellers/products/european-cove-canvas" title="European Cove Canvas">
                                    <div class="hover-element">
                                    </div>
                                    <img src="//cdn.shopify.com/s/files/1/1244/6908/products/european-cove-canvas-by-the-print-emporium_large.jpg?v=1522088941" alt="" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/european-cove-canvas-by-the-print-emporium_large.jpg?v=1522088941" data-alt-image="//cdn.shopify.com/s/files/1/1244/6908/products/european-cove-canvas-by-the-print-emporium-2_large.jpg?v=1522088941" />
                                </a>

                            </div>

                            <div class="product-info">
                                <div class="product-info-inner">
                                    <a href="/collections/best-sellers/products/european-cove-canvas">

                                        <h3>European Cove Canvas</h3>
                                    </a>

                                    <div class="price">

                                        <div class="prod-price">

                                            From $169.00
                                            <!-- - $899.00 -->
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

            <section class="homepage-section animate wow fadeIn no-fouc">
                <div class="row">
                    <div class="homepage-promo desktop-12 mobile-3">

                        <div class="promo-inner">
                            <a href="https://www.theprintemporium.com.au/pages/enquiries">
                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/promo29.jpg?17897852212690934772" alt="">
                                <div class="caption">
                                    <h3></h3>
                                    <p></p>
                                </div>
                            </a>
                        </div>

                    </div>

                </div>
            </section>
            <section class="homepage-section animate wow fadeIn no-fouc">
                <div id="instagram-card" class="row">
                    <div class="section-title lines">
                        <h2><a href="http://instagram.com/THEPRINTEMPORIUM" target="_blank">Follow @THEPRINTEMPORIUM</a> On Instagram</h2></div>
                    <div id="instafeed"></div>
                </div>
            </section>
            <div class="load-wait">
                <i class="icon-spinner icon-spin icon-large"></i>
            </div>
        </div>
    </div>
    <!-- End page wrap for sticky footer -->
  <!--   FOOTER -->
   <?php include('footer/footer.php')?>
  

    <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
            if ($(window).width() >= 741) {

                $(document).ready(function() {
                    //enabling stickUp on the '.navbar-wrapper' class
                    $('nav').stickUp();
                });
            }
        });
    </script>
   
</body>

</html>