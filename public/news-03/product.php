<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <script>
        window.wh_metafields = {};

        window.wh_discount_value = 1;
    </script>

    <meta name="p:domain_verify" content="0bc5d7433f35424aad6d2134ebaeba2b" />

    <meta name="p:domain_verify" content="0bc5d7433f35424aad6d2134ebaeba2b" />

    <!--Start of Zendesk Chat Script-->
    <meta name="google-site-verification" content="HLwyfTHUOzO6FlduzK-YQhE4gw2-gPj4XtxnfkweM6g" />
    <meta charset="utf-8" />

    <!-- Basic Page Needs
================================================== -->

    <title>
        Feather Canvas | Scandinavian Artwork | The Print Emporium &ndash; The Print Emporium
    </title>

    <meta name="description" content="Featuring a detailed speckled feather in greyscale, this canvas print was originally hand painted by our in-house artist team, and now available as a reproduction stretched and ready-to-hang canvas art piece. If you look up close you can see the original and incredible brush stroke detailing from our artist&#39;s brush. Re" />

    <link rel="canonical" href="https://theprintemporium.com.au/products/feather-canvas" />

    <?php include('head.php')?>

</head>

<body class="gridlock  product">
   <?php include('header/header_mobile.php')?>

    <div class="page-wrap">

       <?php include('header/header.php')?>

        <div class="content-wrapper">

            <div id="content" class="row">

                <!-- product feather-canvas set-price -->
                <!-- product.price 16900: wh_price 16900-->
                <!-- product.price_min 16900 : wh_price_min 16900 -->
                <!-- product.price_max 89900 : wh_price_max 89900 -->

                <!-- wh_discount_value 1 -->
                <!-- compare_at_price : wh_compare_at_price  -->
                <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                <div itemscope itemtype="http://schema.org/Product" id="product-520402141244">

                    <meta itemprop="url" content="https://theprintemporium.com.au/products/feather-canvas">
                    <meta itemprop="image" content="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium_grande.jpg?v=1522150038">

                    <!-- For Mobile -->
                    <div id="mobile-product" class="desktop-12 tablet-6 mobile-3">
                        <ul class="bxslider">

                            <li><img class="featured" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7.jpg?v=1522150047" data-image-id="" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_1024x1024.jpg?v=1522150047" alt=""></li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium.jpg?v=1522150038" class="fancybox">
                                    <img data-image-id="2149691457596" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium.jpg?v=1522150038" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium_1024x1024.jpg?v=1522150038" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-2.jpg?v=1522150040" class="fancybox">
                                    <img data-image-id="2149691490364" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-2.jpg?v=1522150040" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-2_1024x1024.jpg?v=1522150040" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-3.jpg?v=1522150041" class="fancybox">
                                    <img data-image-id="2149691523132" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-3.jpg?v=1522150041" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-3_1024x1024.jpg?v=1522150041" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-4.jpg?v=1522150043" class="fancybox">
                                    <img data-image-id="2149691555900" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-4.jpg?v=1522150043" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-4_1024x1024.jpg?v=1522150043" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5.jpg?v=1522150044" class="fancybox">
                                    <img data-image-id="2149691719740" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5.jpg?v=1522150044" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5_1024x1024.jpg?v=1522150044" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-6.jpg?v=1522150046" class="fancybox">
                                    <img data-image-id="2149691818044" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-6.jpg?v=1522150046" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-6_1024x1024.jpg?v=1522150046" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7.jpg?v=1522150047" class="fancybox">
                                    <img data-image-id="2149692112956" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7.jpg?v=1522150047" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_1024x1024.jpg?v=1522150047" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-8.jpg?v=1522150049" class="fancybox">
                                    <img data-image-id="2149692309564" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-8.jpg?v=1522150049" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-8_1024x1024.jpg?v=1522150049" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-9.jpg?v=1522150050" class="fancybox">
                                    <img data-image-id="2149692375100" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-9.jpg?v=1522150050" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-9_1024x1024.jpg?v=1522150050" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-10.jpg?v=1522150052" class="fancybox">
                                    <img data-image-id="2149692473404" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-10.jpg?v=1522150052" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-10_1024x1024.jpg?v=1522150052" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                            <li>
                                <a href="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-11.jpg?v=1522150053" class="fancybox">
                                    <img data-image-id="2149692604476" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-11.jpg?v=1522150053" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-11_1024x1024.jpg?v=1522150053" alt="Feather Canvas-The Print Emporium">
                                </a>
                            </li>

                        </ul>

                        <div id="bx-pager" style="display:none">

                            <a class="thumbnail" data-slide-index="1" data-image-id="2149691457596" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium_compact.jpg?v=1522150038" /></a>

                            <a class="thumbnail" data-slide-index="2" data-image-id="2149691490364" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-2_compact.jpg?v=1522150040" /></a>

                            <a class="thumbnail" data-slide-index="3" data-image-id="2149691523132" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-3_compact.jpg?v=1522150041" /></a>

                            <a class="thumbnail" data-slide-index="4" data-image-id="2149691555900" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-4_compact.jpg?v=1522150043" /></a>

                            <a class="thumbnail" data-slide-index="5" data-image-id="2149691719740" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5_compact.jpg?v=1522150044" /></a>

                            <a class="thumbnail" data-slide-index="6" data-image-id="2149691818044" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-6_compact.jpg?v=1522150046" /></a>

                            <a class="thumbnail" data-slide-index="7" data-image-id="2149692112956" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_compact.jpg?v=1522150047" /></a>

                            <a class="thumbnail" data-slide-index="8" data-image-id="2149692309564" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-8_compact.jpg?v=1522150049" /></a>

                            <a class="thumbnail" data-slide-index="9" data-image-id="2149692375100" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-9_compact.jpg?v=1522150050" /></a>

                            <a class="thumbnail" data-slide-index="10" data-image-id="2149692473404" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-10_compact.jpg?v=1522150052" /></a>

                            <a class="thumbnail" data-slide-index="11" data-image-id="2149692604476" href=""><img src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-11_compact.jpg?v=1522150053" /></a>

                        </div>

                    </div>

                    <!-- For Desktop -->

                    <div id="product-photos" class="desktop-7 tablet-3 mobile-3">

                        <div class="bigimage desktop-12 tablet-5">
                            <img id="520402141244" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_1024x1024.jpg?v=1522150047" data-image-id="" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7.jpg?v=1522150047" alt='' title="Feather Canvas" />
                        </div>

                        <div id="520402141244-gallery" class="desktop-12 tablet-1">
                            <div class="thumbnail-slider">

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium_1024x1024.jpg?v=1522150038" data-image-id="2149691457596" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium.jpg?v=1522150038">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium_compact.jpg?v=1522150038" data-image-id="2149691457596" alt="Feather Canvas-The Print Emporium" data-image-id="2149691457596" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-2_1024x1024.jpg?v=1522150040" data-image-id="2149691490364" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-2.jpg?v=1522150040">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-2_compact.jpg?v=1522150040" data-image-id="2149691490364" alt="Feather Canvas-The Print Emporium" data-image-id="2149691490364" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-3_1024x1024.jpg?v=1522150041" data-image-id="2149691523132" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-3.jpg?v=1522150041">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-3_compact.jpg?v=1522150041" data-image-id="2149691523132" alt="Feather Canvas-The Print Emporium" data-image-id="2149691523132" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-4_1024x1024.jpg?v=1522150043" data-image-id="2149691555900" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-4.jpg?v=1522150043">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-4_compact.jpg?v=1522150043" data-image-id="2149691555900" alt="Feather Canvas-The Print Emporium" data-image-id="2149691555900" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5_1024x1024.jpg?v=1522150044" data-image-id="2149691719740" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5.jpg?v=1522150044">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5_compact.jpg?v=1522150044" data-image-id="2149691719740" alt="Feather Canvas-The Print Emporium" data-image-id="2149691719740" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-6_1024x1024.jpg?v=1522150046" data-image-id="2149691818044" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-6.jpg?v=1522150046">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-6_compact.jpg?v=1522150046" data-image-id="2149691818044" alt="Feather Canvas-The Print Emporium" data-image-id="2149691818044" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_1024x1024.jpg?v=1522150047" data-image-id="2149692112956" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7.jpg?v=1522150047">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_compact.jpg?v=1522150047" data-image-id="2149692112956" alt="Feather Canvas-The Print Emporium" data-image-id="2149692112956" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-8_1024x1024.jpg?v=1522150049" data-image-id="2149692309564" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-8.jpg?v=1522150049">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-8_compact.jpg?v=1522150049" data-image-id="2149692309564" alt="Feather Canvas-The Print Emporium" data-image-id="2149692309564" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-9_1024x1024.jpg?v=1522150050" data-image-id="2149692375100" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-9.jpg?v=1522150050">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-9_compact.jpg?v=1522150050" data-image-id="2149692375100" alt="Feather Canvas-The Print Emporium" data-image-id="2149692375100" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-10_1024x1024.jpg?v=1522150052" data-image-id="2149692473404" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-10.jpg?v=1522150052">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-10_compact.jpg?v=1522150052" data-image-id="2149692473404" alt="Feather Canvas-The Print Emporium" data-image-id="2149692473404" />
                                    </a>
                                </div>

                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-11_1024x1024.jpg?v=1522150053" data-image-id="2149692604476" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-11.jpg?v=1522150053">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-11_compact.jpg?v=1522150053" data-image-id="2149692604476" alt="Feather Canvas-The Print Emporium" data-image-id="2149692604476" />
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {

                            $('.bxslider').bxSlider({
                                pagerCustom: '#bx-pager',
                                prevText: "",
                                nextText: "",
                            });
                            pager: false

                            $('.thumbnail-slider').bxSlider({
                                mode: 'horizontal',
                                minSlides: 2,
                                maxSlides: 5,
                                slideMargin: 5,
                                infiniteLoop: true,
                                pager: false,
                                prevText: "",
                                nextText: "",
                                slideWidth: 128,
                                hideControlOnEnd: true
                            });

                            //initiate the plugin and pass the id of the div containing gallery images
                            $("#520402141244").elevateZoom({
                                gallery: '520402141244-gallery',
                                cursor: 'pointer',
                                galleryActiveClass: 'active',
                                borderColour: '#eee',
                                borderSize: '1',
                                zoomWindowWidth: 605,
                                zoomWindowHeight: 548,
                                zoomWindowOffety: 0,
                                responsive: false,
                                zoomWindowPosition: 1
                            });

                            //pass the images to Fancybox
                            $("#520402141244").bind("click", function(e) {
                                var ez = $('#520402141244').data('elevateZoom');
                                $.fancybox(ez.getGalleryList());
                                return false;
                            });

                        });
                    </script>

                    <div id="product-right" class="desktop-5 tablet-3 mobile-3">
                        <div id="product-description">

                            <h1 itemprop="name">Feather Canvas</h1>

                            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <p id="product-price">

                                    <span class="product-price price" itemprop="price">169.00</span>

                                </p>

                                <meta itemprop="priceCurrency" content="AUD">
                                <link itemprop="availability" href="http://schema.org/InStock">

                                <img src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/canvas-print-icon.jpg?17897852212690934772" alt="Feather Canvas" class="product-type-icon">

                                <!-- product feather-canvas set-price -->
                                <!-- product.price 16900: wh_price 16900-->
                                <!-- product.price_min 16900 : wh_price_min 16900 -->
                                <!-- product.price_max 89900 : wh_price_max 89900 -->

                                <!-- wh_discount_value 1 -->
                                <!-- compare_at_price : wh_compare_at_price  -->
                                <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                                <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                                <form action="/cart/add" method="post" data-money-format="${{amount}}" id="product-form-520402141244">

                                    <div class="select">
                                        <select id="product-select-520402141244" name='id'>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 16900 : wh_v_price 16900 -->
                                            <option selected="selected" value="6950973931580" data-sku="TPE-211-CA">45 x 60cm / (NONE) - $169.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 29900 : wh_v_price 29900 -->
                                            <option value="6950973964348" data-sku="TPE-211-CA">45 x 60cm / ROLLED - $299.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 29900 : wh_v_price 29900 -->
                                            <option value="6950973997116" data-sku="TPE-211-CA">45 x 60cm / WHITE - $299.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 29900 : wh_v_price 29900 -->
                                            <option value="6950974029884" data-sku="TPE-211-CA">45 x 60cm / NATURAL OAK - $299.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 29900 : wh_v_price 29900 -->
                                            <option value="6950974062652" data-sku="TPE-211-CA">45 x 60cm / BLACK - $299.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 26900 : wh_v_price 26900 -->
                                            <option value="6950974095420" data-sku="TPE-211-CA">60 x 80cm / (NONE) - $269.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 39900 : wh_v_price 39900 -->
                                            <option value="6950974128188" data-sku="TPE-211-CA">60 x 80cm / ROLLED - $399.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 39900 : wh_v_price 39900 -->
                                            <option value="6950974160956" data-sku="TPE-211-CA">60 x 80cm / WHITE - $399.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 39900 : wh_v_price 39900 -->
                                            <option value="6950974193724" data-sku="TPE-211-CA">60 x 80cm / NATURAL OAK - $399.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 39900 : wh_v_price 39900 -->
                                            <option value="6950974226492" data-sku="TPE-211-CA">60 x 80cm / BLACK - $399.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 29900 : wh_v_price 29900 -->
                                            <option value="6950974259260" data-sku="TPE-211-CA">75 x 100cm / (NONE) - $299.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 49900 : wh_v_price 49900 -->
                                            <option value="6950974292028" data-sku="TPE-211-CA">75 x 100cm / ROLLED - $499.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 49900 : wh_v_price 49900 -->
                                            <option value="6950974324796" data-sku="TPE-211-CA">75 x 100cm / WHITE - $499.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 49900 : wh_v_price 49900 -->
                                            <option value="6950974357564" data-sku="TPE-211-CA">75 x 100cm / NATURAL OAK - $499.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 49900 : wh_v_price 49900 -->
                                            <option value="6950974390332" data-sku="TPE-211-CA">75 x 100cm / BLACK - $499.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 39900 : wh_v_price 39900 -->
                                            <option value="6950974423100" data-sku="TPE-211-CA">90 x 120cm / (NONE) - $399.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 59900 : wh_v_price 59900 -->
                                            <option value="6950974455868" data-sku="TPE-211-CA">90 x 120cm / ROLLED - $599.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 59900 : wh_v_price 59900 -->
                                            <option value="6950974488636" data-sku="TPE-211-CA">90 x 120cm / WHITE - $599.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 59900 : wh_v_price 59900 -->
                                            <option value="6950974521404" data-sku="TPE-211-CA">90 x 120cm / NATURAL OAK - $599.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 59900 : wh_v_price 59900 -->
                                            <option value="6950974554172" data-sku="TPE-211-CA">90 x 120cm / BLACK - $599.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 54900 : wh_v_price 54900 -->
                                            <option value="6950974586940" data-sku="TPE-211-CA">105 x 140cm / (NONE) - $549.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 69900 : wh_v_price 69900 -->
                                            <option value="6950974619708" data-sku="TPE-211-CA">120 x 160cm / (NONE) - $699.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 74900 : wh_v_price 74900 -->
                                            <option value="6950974652476" data-sku="TPE-211-CA">105 x 140cm / ROLLED - $749.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 89900 : wh_v_price 89900 -->
                                            <option value="6950974685244" data-sku="TPE-211-CA">120 x 160cm / ROLLED - $899.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 74900 : wh_v_price 74900 -->
                                            <option value="6950974718012" data-sku="TPE-211-CA">105 x 140cm / WHITE - $749.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 89900 : wh_v_price 89900 -->
                                            <option value="6950974750780" data-sku="TPE-211-CA">120 x 160cm / WHITE - $899.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 74900 : wh_v_price 74900 -->
                                            <option value="6950974783548" data-sku="TPE-211-CA">105 x 140cm / NATURAL OAK - $749.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 89900 : wh_v_price 89900 -->
                                            <option value="6950974816316" data-sku="TPE-211-CA">120 x 160cm / NATURAL OAK - $899.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 74900 : wh_v_price 74900 -->
                                            <option value="6950974849084" data-sku="TPE-211-CA">105 x 140cm / BLACK - $749.00</option>

                                            <!-- variant.compare_at_price : wh_v_compare_at_price  : set_v_price -->
                                            <!-- variant.price 89900 : wh_v_price 89900 -->
                                            <option value="6950974914620" data-sku="TPE-211-CA">120 x 160cm / BLACK - $899.00</option>

                                        </select>
                                    </div>

                                    <div class="swatch clearfix" data-option-index="0">
                                        <h5>SELECT CANVAS SIZE:</h5>

                                        <div data-value="45 x 60cm" class="swatch-element 45-x-60cm available">

                                            <input id="swatch-0-45-x-60cm" type="radio" name="option-0" value="45 x 60cm" checked />

                                            <label for="swatch-0-45-x-60cm">
                                                45 x 60cm
                                                <img class="crossed-out" src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/soldout.png?17897852212690934772" />
                                            </label>

                                        </div>

                                       

                                        <div data-value="60 x 80cm" class="swatch-element 60-x-80cm available">

                                            <input id="swatch-0-60-x-80cm" type="radio" name="option-0" value="60 x 80cm" />

                                            <label for="swatch-0-60-x-80cm">
                                                60 x 80cm
                                                <img class="crossed-out" src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/soldout.png?17897852212690934772" />
                                            </label>

                                        </div>

                                        

                                        <div data-value="90 x 120cm" class="swatch-element 90-x-120cm available">

                                            <input id="swatch-0-90-x-120cm" type="radio" name="option-0" value="90 x 120cm" />

                                            <label for="swatch-0-90-x-120cm">
                                                90 x 120cm
                                                <img class="crossed-out" src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/soldout.png?17897852212690934772" />
                                            </label>

                                        </div>

                                      

                                        <div data-value="105 x 140cm" class="swatch-element 105-x-140cm available">

                                            <input id="swatch-0-105-x-140cm" type="radio" name="option-0" value="105 x 140cm" />

                                            <label for="swatch-0-105-x-140cm">
                                                105 x 140cm
                                                <img class="crossed-out" src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/soldout.png?17897852212690934772" />
                                            </label>

                                        </div>


                                        <div data-value="120 x 160cm" class="swatch-element 120-x-160cm available">

                                            <input id="swatch-0-120-x-160cm" type="radio" name="option-0" value="120 x 160cm" />

                                            <label for="swatch-0-120-x-160cm">
                                                120 x 160cm
                                                <img class="crossed-out" src="//cdn.shopify.com/s/files/1/1244/6908/t/8/assets/soldout.png?17897852212690934772" />
                                            </label>

                                        </div>

                                        

                                    </div>

                        

                                 

                                    <br/>

                                    <div class="product-add">

                                        <div class="qty-selection">
                                            <h5>Quantity</h5>
                                            <a class="down" field="quantity"><i class="icon-minus"></i></a>
                                            <input min="1" type="text" name="quantity" class="quantity" value="1" />
                                            <a class="up" field="quantity"><i class="icon-plus"></i></a>
                                        </div>

                                        <input type="submit" name="button" class="add" value="Add to Cart" />

                                    </div>
                                    <p class="add-to-cart-msg"></p>

                                    <!-- zipmoney widgets -->
                                    <style>
                                        #shopify-zip-cart-widget > iframe {
                                            height: 75px!important;
                                            width: 213px !important;
                                        }
                                    </style>
                                    <span style="cursor:pointer" id="shopify-zip-cart-widget" data-zm-asset="cartwidget" data-zm-widget="popup" data-zm-popup-asset="termsdialog"></span>
                                    <!-- end zipmoney widgets -->

                                    <!-- Begin Afterpay Code asa -->

                                    <p class="afterpay-paragraph" style="display:block;" data-product-id="520402141244">
                                        Make 4 interest-free payments of
                                        <span class="afterpay-instalments">$42.25 AUD</span> fortnightly with
                                        <a style="display:inline-block; margin-bottom: 10px;" target="_blank" href="https://www.afterpay.com.au/terms">
                                            <img style="vertical-align:bottom" width="100" alt="Afterpay" src="https://www.afterpay.com.au/wp-content/themes/afterpay/assets/img/logo_scroll.png" />
                                            <span style="font-size:12px"> <u>More info</u> </span>
                                        </a>
                                    </p>

                                

                                    <!-- End Afterpay Code -->
                                </form>




                            </div>
                            <div class="desc">

                                <div class="share-icons">
                                    <a title="Share on Facebook" href="//www.facebook.com/sharer.php?u=https://theprintemporium.com.au/products/feather-canvas" class="facebook" target="_blank"><i class="icon-facebook icon-2x"></i></a>
                                    <a title="Share on Twitter" href="//twitter.com/home?status=Feather Canvas, https://theprintemporium.com.au/products/feather-canvas via @ThePrintEmporiu" title="Share on Twitter" target="_blank" class="twitter"><i class="icon-twitter icon-2x"></i></a>

                                    <a title="Share on Pinterest" target="blank" href="//pinterest.com/pin/create/button/?url=https://theprintemporium.com.au/products/feather-canvas&amp;media=http://cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium_1024x1024.jpg?v=1522150038" title="Pin This Product" class="pinterest"><i class="icon-pinterest icon-2x"></i></a>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="clear"></div>

                    <div id="lower-description" class="desktop-8 desktop-push-2 tablet-6 mobile-3">
                        <div class="section-title lines">
                            <h2>Details</h2></div>
                        <div class="rte" itemprop="description">
                            <meta charset="utf-8">
                            <meta charset="utf-8">
                            <meta charset="utf-8">
                            <meta charset="utf-8">
                            <p class="subtitle" style="text-align: left;"><span>Featuring a detailed speckled feather in greyscale,</span> <span>this canvas print was originally hand painted by our in-house artist team, and now available as a reproduction stretched and ready-to-hang canvas art piece. I</span>f you look up close you can see the original and incredible brush stroke detailing from our artist's brush. Read more about our unique process (<a href="https://www.theprintemporium.com.au/pages/about-our-process" target="_blank" rel="noopener noreferrer">HERE</a>). This artwork design is also available as a paper Art Print (framed or unframed). All stretched canvases are designed and made in Australia, and come ready to hang. We offer a rolled option for our international customers (shipped rolled in a tube). There is also an option to add a timber shadowbox floating wood frame (three colour options) for a more deluxe gallery look.</p>
                            <p style="text-align: left;"><strong><em>All prices are in Australian Dollars AUD$</em></strong></p>
                            <div style="text-align: left;"></div>
                            <div style="text-align: left;"><span style="text-decoration: underline;"><strong><span lang="EN-US">ABOUT OUR CANVASES:</span></strong>
                                </span>
                            </div>
                            <div style="text-align: left;">
                                <ul>
                                    <li>Proudly designed and made to order in Australia</li>
                                    <li>Premium poly/cotton artist's canvas</li>
                                    <li>Printed with quality UV fade resistant eco-friendly giclee inks</li>
                                    <li>Stretched onto a 4cm thick kiln-dried craft wood timber frame</li>
                                    <li>Arrives ready to hang on your wall</li>
                                    <li>Gallery-quality finish backing wire metal d-ring hooks</li>
                                    <li>All canvas edges have mirrored artwork, for a full wrap around effect of the design</li>
                                    <li>An option to add a 5cm deep oak shadowbox floating frame (natural oak, white or black timber)</li>
                                    <li>Care instructions: wipe down with a wet cloth</li>
                                    <li>On most sizes of our canvases the use of removable hooks eg 3M brand instead of traditional hooks is fine. This means no damage to walls, which is especially great for renters.</li>
                                    <li>All framed orders arrive safely wrapped within a protective box carton</li>
                                    <li>For International customers, we offer un-stretched (rolled canvas) options. Shipped in tube.</li>
                                </ul>
                            </div>
                            <div class="ProductDescriptionContainer prodAccordionContent" style="text-align: left;"><strong> </strong></div>
                            <div class="ProductDescriptionContainer prodAccordionContent" style="text-align: left;">
                                <div>
                                    <p><strong>For Australian based customers: </strong>As all our products are custom made to order, please allow 2-4 weeks for production and delivery of your canvases Australia wide. for just $10 Flat Rate Shipping per order, nationwide, on all bulky framed items. Don't forget to sign up to our newsletter and receive a $10 discount code. <strong>**Free Shipping On All Orders Over $150</strong></p>
                                </div>
                                <div><strong></strong></div>
                            </div>
                            <div class="ProductDescriptionContainer prodAccordionContent" style="text-align: left;"><strong></strong></div>
                            <div class="ProductDescriptionContainer prodAccordionContent" style="text-align: left;"><strong></strong></div>
                            <div class="ProductDescriptionContainer prodAccordionContent" style="text-align: left;">
                                <strong>For International Customers (outside of Australia): </strong><span>At this stage due to the bulkiness and high cost of shipping framed items internationally, we just sell unframed rolled canvases (rolled and shipped worldwide in a protective mailer tube) to our international customers. You will just need to find a framer or art shop whom can stretch to a timber frame. Please select the 'ROLLED" canvas option for checkout. Shipping is just $15AUD postage per order for unframed prints or rolled canvases, anywhere in the world. All canvases bought rolled will still have the mirrored artwork edges, so that when you have your canvas professionally stretched to wood frame, it has the artwork 'wrapped' around all four sides, for a seamless look from any angle. Any q's or concerns please email us, and we will assist you. <strong><a href="mailto:hello@theprintemporium.com.au">hello@theprintemporium.com.au</a> </strong></span><strong>**Free Shipping On All Orders Over $150</strong>
                            </div>
                            <div class="ProductDescriptionContainer prodAccordionContent">
                                <br>
                                <p style="text-align: left;">For more information on our products and FAQ, see (<a href="https://www.theprintemporium.com.au/pages/faq" target="_blank" rel="noopener noreferrer">HERE</a>)</p>
                            </div>
                            <div class="ProductDescriptionContainer prodAccordionContent"></div>
                            Please note all artwork and imagery are Copyright protected of The Print Emporium, all rights reserved</div>
                    </div>

                    <div class="clear"></div>

                    <!-- product feather-canvas set-price -->
                    <!-- product.price 16900: wh_price 16900-->
                    <!-- product.price_min 16900 : wh_price_min 16900 -->
                    <!-- product.price_max 89900 : wh_price_max 89900 -->

                    <!-- wh_discount_value 1 -->
                    <!-- compare_at_price : wh_compare_at_price  -->
                    <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                    <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                    <div class="desktop-12 tablet-6 mobile-3" id="related">

                        <div class="section-title lines tablet-6 desktop-12 mobile-3">
                            <h2>More in this Collection</h2></div>

                        <div class="collection-carousel desktop-12 tablet-6 mobile-3">

                            <!-- product spring-bouquet-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Spring Bouquet (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/spring-bouquet-square-canvas-art" title="Spring Bouquet (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/spring-bouquet-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148123" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/spring-bouquet-square-canvas-art">

                                            <h3>Spring Bouquet (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product vintage-blooms-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Vintage Blooms (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/vintage-blooms-square-canvas-art" title="Vintage Blooms (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/vintage-blooms-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148756" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/vintage-blooms-square-canvas-art">

                                            <h3>Vintage Blooms (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product pink-blooms-square-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Pink Blooms Canvas (Square)" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/pink-blooms-square-canvas" title="Pink Blooms Canvas (Square)">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/pink-blooms-canvas-square-by-the-print-emporium_large.jpg?v=1522090472" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/pink-blooms-square-canvas">

                                            <h3>Pink Blooms Canvas (Square)</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product charlie-the-cockatoo-ice-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Charlie The Cockatoo (Ice) Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/charlie-the-cockatoo-ice-canvas" title="Charlie The Cockatoo (Ice) Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/charlie-the-cockatoo-ice-canvas-by-the-print-emporium_large.jpg?v=1522150405" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/charlie-the-cockatoo-ice-canvas">

                                            <h3>Charlie The Cockatoo (Ice) Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product george-the-black-cockatoo-white-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="George The Black Cockatoo (White) Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/george-the-black-cockatoo-white-canvas" title="George The Black Cockatoo (White) Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/george-the-black-cockatoo-white-canvas-by-the-print-emporium_large.jpg?v=1522150508" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/george-the-black-cockatoo-white-canvas">

                                            <h3>George The Black Cockatoo (White) Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product milly-the-umbrella-cockatoo-pink-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Milly The Umbrella Cockatoo - Pink Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/milly-the-umbrella-cockatoo-pink-canvas-art" title="Milly The Umbrella Cockatoo - Pink Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/milly-the-umbrella-cockatoo-pink-canvas-art-by-the-print-emporium_large.jpg?v=1522150537" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/milly-the-umbrella-cockatoo-pink-canvas-art">

                                            <h3>Milly The Umbrella Cockatoo - Pink Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

        

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Dusty Pink Roses Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/dusty-pink-roses-canvas" title="Dusty Pink Roses Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/dusty-pink-roses-canvas-by-the-print-emporium_large.jpg?v=1522089512" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/dusty-pink-roses-canvas">

                                            <h3>Dusty Pink Roses Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $749.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product midnight-garden-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Midnight Garden Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/midnight-garden-canvas" title="Midnight Garden Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/midnight-garden-canvas-by-the-print-emporium_large.jpg?v=1522089384" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/midnight-garden-canvas">

                                            <h3>Midnight Garden Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product romantic-floral-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Romantic Floral Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/romantic-floral-canvas" title="Romantic Floral Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/romantic-floral-canvas-by-the-print-emporium_large.jpg?v=1522089663" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/romantic-floral-canvas">

                                            <h3>Romantic Floral Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product divine-dahlia-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Divine Dahlia (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/divine-dahlia-canvas-art" title="Divine Dahlia (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/divine-dahlia-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148338" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/divine-dahlia-canvas-art">

                                            <h3>Divine Dahlia (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product oriental-garden-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Oriental Garden Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/oriental-garden-canvas" title="Oriental Garden Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/oriental-garden-canvas-by-the-print-emporium_large.jpg?v=1522148151" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/oriental-garden-canvas">

                                            <h3>Oriental Garden Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product vintage-blooms-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Vintage Blooms Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/vintage-blooms-canvas" title="Vintage Blooms Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/vintage-blooms-canvas-by-the-print-emporium_large.jpg?v=1522149928" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/vintage-blooms-canvas">

                                            <h3>Vintage Blooms Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product pink-roses-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Pink Roses Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/pink-roses-canvas" title="Pink Roses Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/pink-roses-canvas-by-the-print-emporium_large.jpg?v=1522089006" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/pink-roses-canvas">

                                            <h3>Pink Roses Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product marfa-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Marfa Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/marfa-canvas" title="Marfa Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/marfa-canvas-by-the-print-emporium_large.jpg?v=1522090054" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/marfa-canvas">

                                            <h3>Marfa Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product black-white-marfa-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Black & White Marfa Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/black-white-marfa-canvas" title="Black &amp; White Marfa Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/black-white-marfa-canvas-by-the-print-emporium_large.jpg?v=1522090089" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/black-white-marfa-canvas">

                                            <h3>Black & White Marfa Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product palm-springs-doorway-iii-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Palm Springs Doorway 3 Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/palm-springs-doorway-iii-canvas" title="Palm Springs Doorway 3 Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/palm-springs-doorway-3-canvas-by-the-print-emporium_large.jpg?v=1522149638" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/palm-springs-doorway-iii-canvas">

                                            <h3>Palm Springs Doorway 3 Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product palm-springs-doorway-ii-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Palm Springs Doorway 2 Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/palm-springs-doorway-ii-canvas" title="Palm Springs Doorway 2 Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/palm-springs-doorway-2-canvas-by-the-print-emporium_large.jpg?v=1522149553" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/palm-springs-doorway-ii-canvas">

                                            <h3>Palm Springs Doorway 2 Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product palm-springs-doorway-i-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Palm Springs Doorway 1 Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/palm-springs-doorway-i-canvas" title="Palm Springs Doorway 1 Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/palm-springs-doorway-1-canvas-by-the-print-emporium_large.jpg?v=1522149476" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/palm-springs-doorway-i-canvas">

                                            <h3>Palm Springs Doorway 1 Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product palm-springs-doorway-4-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Palm Springs Doorway 4 Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/palm-springs-doorway-4-canvas" title="Palm Springs Doorway 4 Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/palm-springs-doorway-4-canvas-by-the-print-emporium_large.jpg?v=1522148400" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/palm-springs-doorway-4-canvas">

                                            <h3>Palm Springs Doorway 4 Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product palm-springs-doorway-5-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Palm Springs Doorway 5 Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/palm-springs-doorway-5-canvas-art" title="Palm Springs Doorway 5 Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/palm-springs-doorway-5-canvas-art-by-the-print-emporium_large.jpg?v=1522149306" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/palm-springs-doorway-5-canvas-art">

                                            <h3>Palm Springs Doorway 5 Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product european-cove-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="European Cove Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/european-cove-canvas" title="European Cove Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/european-cove-canvas-by-the-print-emporium_large.jpg?v=1522088941" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/european-cove-canvas">

                                            <h3>European Cove Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product palm-springs-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Palm Springs Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/palm-springs-canvas" title="Palm Springs Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/palm-springs-canvas-by-the-print-emporium_large.jpg?v=1522090999" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/palm-springs-canvas">

                                            <h3>Palm Springs Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product california-palms-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="California Palms Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/california-palms-canvas" title="California Palms Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/california-palms-canvas-by-the-print-emporium_large.jpg?v=1522090314" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/california-palms-canvas">

                                            <h3>California Palms Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product cactus-view-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Cactus View Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/cactus-view-canvas" title="Cactus View Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/cactus-view-canvas-by-the-print-emporium_large.jpg?v=1522091221" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/cactus-view-canvas">

                                            <h3>Cactus View Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product day-at-the-beach-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Day At The Beach Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/day-at-the-beach-canvas" title="Day At The Beach Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/day-at-the-beach-canvas-by-the-print-emporium_large.jpg?v=1522090018" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/day-at-the-beach-canvas">

                                            <h3>Day At The Beach Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbathers-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbathers Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbathers-canvas" title="Sunbathers Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbathers-canvas-by-the-print-emporium_large.jpg?v=1522090163" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbathers-canvas">

                                            <h3>Sunbathers Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbathers-light-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbathers II Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbathers-light-canvas" title="Sunbathers II Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbathers-ii-canvas-by-the-print-emporium_large.jpg?v=1522090127" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbathers-light-canvas">

                                            <h3>Sunbathers II Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbaking-i-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbaking I (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbaking-i-square-canvas-art" title="Sunbaking I (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbaking-i-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148611" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbaking-i-square-canvas-art">

                                            <h3>Sunbaking I (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbaking-iii-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbaking III (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbaking-iii-square-canvas-art" title="Sunbaking III (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbaking-iii-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148366" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbaking-iii-square-canvas-art">

                                            <h3>Sunbaking III (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbaking-i-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbaking I Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbaking-i-canvas" title="Sunbaking I Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbaking-i-canvas-by-the-print-emporium_large.jpg?v=1522151054" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbaking-i-canvas">

                                            <h3>Sunbaking I Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbaking-ii-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbaking II (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbaking-ii-square-canvas-art" title="Sunbaking II (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbaking-ii-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148513" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbaking-ii-square-canvas-art">

                                            <h3>Sunbaking II (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbaking-iv-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbaking IV (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbaking-iv-square-canvas-art" title="Sunbaking IV (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbaking-iv-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148272" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbaking-iv-square-canvas-art">

                                            <h3>Sunbaking IV (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product sunbaking-ii-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Sunbaking II Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/sunbaking-ii-canvas-art" title="Sunbaking II Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/sunbaking-ii-canvas-art-by-the-print-emporium_large.jpg?v=1522148665" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/sunbaking-ii-canvas-art">

                                            <h3>Sunbaking II Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product runway-reads-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Runway Reads (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/runway-reads-square-canvas-art" title="Runway Reads (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/runway-reads-square-canvas-art-by-the-print-emporium_large.jpg?v=1522149429" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/runway-reads-square-canvas-art">

                                            <h3>Runway Reads (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product 70s-girl-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="70s Girl Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/70s-girl-canvas" title="70s Girl Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/70s-girl-canvas-art-by-the-print-emporium_large.jpg?v=1522150975" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/70s-girl-canvas">

                                            <h3>70s Girl Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product runway-reads-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Runway Reads Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/runway-reads-canvas" title="Runway Reads Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/runway-reads-canvas-art-by-the-print-emporium_large.jpg?v=1522150933" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/runway-reads-canvas">

                                            <h3>Runway Reads Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product feather-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <!-- product seduction-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Seduction (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/seduction-square-canvas-art" title="Seduction (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/seduction-square-canvas-art-by-the-print-emporium_large.jpg?v=1522148773" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/seduction-square-canvas-art">

                                            <h3>Seduction (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product pia-the-white-cockatiel-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Pia The White Cockatiel Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/pia-the-white-cockatiel-canvas" title="Pia The White Cockatiel Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/pia-the-white-cockatiel-canvas-by-the-print-emporium_large.jpg?v=1522150331" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/pia-the-white-cockatiel-canvas">

                                            <h3>Pia The White Cockatiel Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product snow-owl-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Snow Owl Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/snow-owl-canvas" title="Snow Owl Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/snow-owl-canvas-by-the-print-emporium_large.jpg?v=1522148289" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/snow-owl-canvas">

                                            <h3>Snow Owl Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product misty-forest-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Misty Forest Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/misty-forest-canvas" title="Misty Forest Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/misty-forest-canvas-by-the-print-emporium_large.jpg?v=1522148648" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/misty-forest-canvas">

                                            <h3>Misty Forest Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product winter-elk-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Winter Elk Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/winter-elk-canvas" title="Winter Elk Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/winter-elk-canvas-by-the-print-emporium_large.jpg?v=1522148681" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/winter-elk-canvas">

                                            <h3>Winter Elk Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product peeking-pineapples-square-canvas set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Peeking Pineapples (Square) Canvas" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/peeking-pineapples-square-canvas" title="Peeking Pineapples (Square) Canvas">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/peeking-pineapples-square-canvas-by-the-print-emporium_large.jpg?v=1522091139" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/peeking-pineapples-square-canvas">

                                            <h3>Peeking Pineapples (Square) Canvas</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product cloudscape-ii-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Cloudscape II Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/cloudscape-ii-canvas-art" title="Cloudscape II Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/cloudscape-ii-canvas-art-by-the-print-emporium_large.jpg?v=1522149261" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/cloudscape-ii-canvas-art">

                                            <h3>Cloudscape II Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product cloudscape-iii-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Cloudscape III Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/cloudscape-iii-canvas-art" title="Cloudscape III Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/cloudscape-iii-canvas-art-by-the-print-emporium_large.jpg?v=1522148878" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/cloudscape-iii-canvas-art">

                                            <h3>Cloudscape III Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product romantic-floral-square-i-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Romantic Floral I (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/romantic-floral-square-i-canvas-art" title="Romantic Floral I (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/romantic-floral-i-square-canvas-art-by-the-print-emporium_large.jpg?v=1522151082" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/romantic-floral-square-i-canvas-art">

                                            <h3>Romantic Floral I (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product romantic-floral-square-ii-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Romantic Floral II (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/romantic-floral-square-ii-canvas-art" title="Romantic Floral II (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/romantic-floral-ii-square-canvas-art-by-the-print-emporium_large.jpg?v=1522151110" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/romantic-floral-square-ii-canvas-art">

                                            <h3>Romantic Floral II (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product beautiful-blooms-ii-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Beautiful Blooms II (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/beautiful-blooms-ii-square-canvas-art" title="Beautiful Blooms II (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/beautiful-blooms-ii-square-canvas-art-by-the-print-emporium_large.jpg?v=1522150242" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/beautiful-blooms-ii-square-canvas-art">

                                            <h3>Beautiful Blooms II (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product pretty-in-pink-ii-square-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Pretty In Pink II (Square) Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/pretty-in-pink-ii-square-canvas-art" title="Pretty In Pink II (Square) Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/pretty-in-pink-ii-square-canvas-art-by-the-print-emporium_large.jpg?v=1522149125" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/pretty-in-pink-ii-square-canvas-art">

                                            <h3>Pretty In Pink II (Square) Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- product beautiful-blooms-ii-canvas-art set-price -->
                            <!-- product.price 16900: wh_price 16900-->
                            <!-- product.price_min 16900 : wh_price_min 16900 -->
                            <!-- product.price_max 89900 : wh_price_max 89900 -->

                            <!-- wh_discount_value 1 -->
                            <!-- compare_at_price : wh_compare_at_price  -->
                            <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                            <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->

                            <div class="lazyOwl" id="prod-520402141244" data-alpha="Beautiful Blooms II Canvas Art" data-price="16900">
                                <div class="prod-image">
                                    <a class="product-link" href="/collections/canvas-art/products/beautiful-blooms-ii-canvas-art" title="Beautiful Blooms II Canvas Art">
                                        <div class="hover-element"></div>
                                        <img src="//cdn.shopify.com/s/files/1/1244/6908/products/beautiful-blooms-ii-canvas-art-by-the-print-emporium_large.jpg?v=1522150142" alt="Feather Canvas" />
                                    </a>

                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="/collections/canvas-art/products/beautiful-blooms-ii-canvas-art">

                                            <h3>Beautiful Blooms II Canvas Art</h3>
                                        </a>
                                        <div class="price">

                                            <div class="prod-price"> From $169.00 - $899.00 </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="clear"></div>

                    <div id="product-navigation" class="desktop-12 tablet-6 mobile-3">
                        <span class="backto">
      Back To <a href="/collections/canvas-art" title="">CANVAS ART</a>
    </span>
                        <div id="back-forth">

                            <span class="prev-prod"> 
        <img id="previous-product-image" />
        <a href="/collections/canvas-art/products/runway-reads-canvas#content" title="">&larr; Previous Product</a>

      </span>

                            <span class="next-prod">
        <img id="next-product-image" />
        <a href="/collections/canvas-art/products/seduction-square-canvas-art#content" title="">Next Product &rarr;</a>
      </span>

                        </div>
                    </div>

                    <script>
                        jQuery.getJSON('/collections/canvas-art/products/seduction-square-canvas-art.js', function(product) {
                            jQuery('#next-product-image').attr('src', product.images[0].replace(/(\.jpg|\.png|\.jpeg|\.gif)/, '_small$1'));
                        });
                        jQuery.getJSON('/collections/canvas-art/products/runway-reads-canvas.js', function(product) {
                            jQuery('#previous-product-image').attr('src', product.images[0].replace(/(\.jpg|\.png|\.jpeg|\.gif)/, '_small$1'));
                        });
                    </script>

                </div>
            </div>

        </div>

    </div>
    <!-- End page wrap for sticky footer -->

    <div id="footer-wrapper">
        <div id="footer" class="row">

            <div class="desktop-3 tablet-half mobile-half">
                <h4>RESOURCES</h4>
                <ul>

                    <li><a href="/pages/faq" title="">FAQ / INFO</a></li>

                    <li><a href="/pages/about-our-process" title="">ABOUT OUR PROCESS</a></li>

                    <li><a href="/pages/about-us" title="">ABOUT US</a></li>

                    <li><a href="/pages/contact" title="">CONTACT</a></li>

                    <li><a href="http://www.instagram.com/theprintemporium" title="">INSTAGRAM</a></li>

                    <li><a href="/pages/privacy-policy" title="">PRIVACY POLICY</a></li>

                    <li><a href="/pages/press" title="">PRESS / AS SEEN IN </a></li>

                </ul>
            </div>

            <div class="desktop-3 tablet-half mobile-half">

                <h4>ACCOUNT</h4>

                <ul>

                    <li><a href="https://www.theprintemporium.com.au/account/login" title="">CREATE ACCOUNT</a></li>

                    <li><a href="https://www.theprintemporium.com.au/account/login" title="">SIGN IN TO ACCOUNT</a></li>

                    <li><a href="/pages/enquiries" title="">WHOLESALE / TRADE</a></li>

                    <li><a href="/pages/payment-info" title="">PAYMENT INFO</a></li>

                    <li><a href="/pages/shipping-info" title="">SHIPPING INFO</a></li>

                    <li><a href="/pages/zippay" title="">PAY LATER WITH ZIPPAY</a></li>

                    <li><a href="/products/gift-card" title="">GIFT CARD</a></li>

                    <li><a href="/pages/contact" title="">CONTACT US</a></li>

                </ul>
            </div>

            <div class="desktop-3 tablet-half mobile-half">

                <h4>SHOP</h4>

                <ul>

                    <li><a href="/collections/all" title="">SHOP ALL ART</a></li>

                    <li><a href="/collections/canvas-art" title="">CANVAS ART</a></li>

                    <li><a href="/collections/art-prints" title="">ART PRINTS</a></li>

                    <li><a href="/search" title="">SEARCH</a></li>

                </ul>
            </div>

            <div class="desktop-3 tablet-half mobile-3">
                <h4>Connect</h4>
                <div id="footer_signup">
                    <p>Subscribe To Receive A Coupon Code For $10 Off Your First Order & News Updates:</p>
                    <form action="//theprintemporium.us15.list-manage.com/subscribe/post?u=84f35a8ad4dc5547f6e9b4b62&amp;id=e3d0f09f99" method="post" id="footer-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">
                        <input value="" name="EMAIL" class="email" id="footer-EMAIL" placeholder="Enter Email Address" required="" type="email">
                        <input value="Join" name="subscribe" id="footer-subscribe" class="button" type="submit">
                    </form>
                </div>
            </div>

            <div class="clear"></div>

            <ul id="footer-icons" class="desktop-12 tablet-6 mobile-3">
                <li><a href="https://www.facebook.com/THEPRINTEMPORIUM1" target="_blank"><i class="icon-facebook icon-2x"></i></a></li>
                <li><a href="https://twitter.com/ThePrintEmporiu" target="_blank"><i class="icon-twitter icon-2x"></i></a></li>
                <li><a href="https://www.pinterest.com/theprintemporium" target="_blank"><i class="icon-pinterest icon-2x"></i></a></li>

                <li><a href="//instagram.com/THEPRINTEMPORIUM" target="_blank"><i class="icon-instagram icon-2x"></i></a></li>
                <li><a href="https://theprintemporium.com.au/blogs/news.atom" target="_blank"><i class="icon-rss icon-2x"></i></a></li>
            </ul>

            <div class="clear"></div>

            <div class="credit desktop-12 tablet-6 mobile-3">
                <p>
                    Copyright &copy; 2018 <a href="/" title="">The Print Emporium </a>
                    <br>ALL prices are in AUD
                </p>
            </div>
        </div>
    </div>

    <div style='display:none'>
        <div id='search_popup' style='padding:30px;'>
            <p class="box-title">Search our store
                <p>
                    <!-- BEGIN #subs-container -->
                    <div id="subs-container" class="clearfix">
                        <div id="search">
                            <form action="/search" method="get">
                                <input type="text" name="q" id="q" placeholder="Enter your search terms" />
                            </form>
                        </div>
                    </div>
        </div>
    </div>

    <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
            if ($(window).width() >= 741) {

                $(document).ready(function() {
                    //enabling stickUp on the '.navbar-wrapper' class
                    $('nav').stickUp();
                });
            }

        });
    </script>

    <a href="#" class="scrollup"><i class="icon-angle-up icon-2x"></i></a>

    <!-- Wholesale Hero End -->

</body>

</html>