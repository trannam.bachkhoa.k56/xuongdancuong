@extends('moma.layout.site')

@section('title', isset($category->title) ?$category->title : '')
@section('meta_description',  isset($category->description) ?$category->description : '')
@section('keywords', '')

@section('content')
<div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>BLOG</h1>
                                <p><a href="#">Home </a> > Blog</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="blog-area inner-padding7">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="blog-masonary">
                        @foreach(\App\Entity\Post::categoryShow('tin-tuc',8) as $post) 
                        <div class="post-grid2">
                            <div class="post-row foo" data-sr='enter'>
                                <div class="post-header">
                                    <div class="post-feature">
                                        <img src="{{ isset($post['image']) ?  $post['image'] : '' }}" alt="responsive img">
                                    </div>
                                </div>
                                <div class="post-body">
                                    <div class="post-caption">
                                        <h2 class="post-heading"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">{{ isset($post['title']) ?  $post['title'] : '' }}</a></h2>
                                        <div class="post-meta">{{ isset($post['nguoi-dang']) ?  $post['nguoi-dang'] : '' }} / {{ isset($post['tags']) ?  $post['tags'] : '' }}, at {{ isset($post['thoi-gian']) ?  $post['thoi-gian'] : '' }}</div>
                                        <div class="post-sticker"><small>{{ isset($post['ngay']) ?  $post['ngay'] : '' }}</small>
                                            <p class="month">{{ isset($post['thang']) ?  $post['thang'] : '' }}</p>
                                        </div>
                                    </div>
                                    <p class="post-text">{{ isset($post['description']) ?  $post['description'] : '' }}</p>
                                </div>
                                <div class="post-footer">
                                    <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="btn btn-default btn-post" role="button">READ MORE<i class="fa fa-angle-double-right"></i></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="pagination pl15">
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
				<div class="col-sm-12 col-md-3">
                    <div class="sidebar">
                        <div class="widget-area foo" data-sr='enter'>
                            <div class="widget-header">
                                <h3 class="widget-title">Categories</h3>
                            </div>
                            <div class="sidebar-list">
                                <ul class="categories-list">
                                    @foreach(\App\Entity\Post::categoryShow('dich-vu',8) as $post2) 
                                    <li><a href="#">{{ isset($post2['title']) ?  $post2['title'] : '' }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="widget-area foo" data-sr='enter'>
                            <div class="widget-header">
                                <h3 class="widget-title">Recent Post</h3>
                            </div>
                            <div class="sidebar-list">
                                <ul>
                                    @foreach(\App\Entity\Post::categoryShow('tin-tuc',8) as $post) 
                                    <li class="post-item">
                                        <div class="recent-post-feature">
                                            <img src="{{ isset($post['image']) ?  $post['image'] : '' }}" class="img-responsive" alt="responsive item">
                                        </div>
                                        <div class="post-contant">
                                            <h2 class="recent-post-title"><a href="#">{{ isset($post['title']) ?  $post['title'] : '' }}</a></h2>
                                            <div class="widget-post-meta"><a href="#">{{ isset($post['nguoi-dang']) ?  $post['nguoi-dang'] : '' }} / {{ isset($post['ngay']) ?  $post['ngay'] : '' }} {{ isset($post['thang']) ?  $post['thang'] : '' }}</a></div>
                                            <p>{{ isset($post['description']) ?  $post['description'] : '' }} </p>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="widget-area foo" data-sr='enter'>
                            <div class="widget-header">
                                <h3 class="widget-title">Latest Tweets</h3>
                            </div>
                            <div class="sidebar-list">
                                <ul class="tweets-list">
                                    @foreach(\App\Entity\Post::categoryShow('tweets',8) as $post4) 
                                    <li><a href="#">@ {{ isset($post['nguoi-dang']) ?  $post['nguoi-dang'] : '' }} </a>
                                        {!! isset($post4['content']) ?  $post4['content'] : '' !!}
                                        <div class="tweets-meta">{{ isset($post4['thoi-gian']) ?  $post4['thoi-gian'] : '' }}</div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-widget foo" data-sr='enter'>
                            <div class="widget-header">
                                <h3 class="widget-title">Tag</h3>
                            </div>
                            <div class="sidebar-list">
                                <ul class="tag-cloud">
                                    @foreach(\App\Entity\Post::categoryShow('dich-vu',8) as $post3) 
                                    <li><a href="#">{{ isset($post3['title']) ?  $post3['title'] : '' }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
