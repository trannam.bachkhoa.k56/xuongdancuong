@extends('moma.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )


@section('content')
    <div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>{{ isset($post['title']) ?  $post['title'] : '' }}</h1>
                                <p><a href="/">Home </a> > {{ isset($post['title']) ?  $post['title'] : '' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area inner-padding7">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="priceTable">
					{!! $post['content'] !!}
					</div>
                </div>
               
            </div>
        </div>
    </div>
@endsection


