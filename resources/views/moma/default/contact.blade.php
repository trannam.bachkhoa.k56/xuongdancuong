@extends('moma.layout.site')

@section('title',isset($category->title) ? $category->title : '')
@section('meta_description','')
@section('keywords', '')

@section('content')
<div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>CONTACT</h1>
                                <p><a href="/">Home </a> > Contact</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="contact-area inner-padding6">
        <!-- Map area Section -->
        <div class="map-area">
            <div class="google-map">
                <div id="gmap">
                    {!! isset($information['map']) ?  $information['map'] : '' !!}
                </div>
            </div>
        </div>
        <!-- End Map area Section -->
        <!-- Contact Form Section -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="address-widget foo" data-sr='enter'>
                        <div class="contact-icon"><i class="fa fa-map-marker"></i></div>
                        <h5>Address</h5>
                        <p>{{ isset($information['dia-chi']) ?  $information['dia-chi'] : '' }}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="address-widget foo" data-sr='enter'>
                        <div class="contact-icon"><i class="fa fa-phone"></i></div>
                        <h5>Phone Number</h5>
                        <p>{{ isset($information['dien-thoai']) ?  $information['dien-thoai'] : '' }}</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="address-widget foo" data-sr='enter'>
                        <div class="contact-icon"><i class="fa fa-globe"></i></div>
                        <h5>Emai & Web</h5>
                        <p>{{ isset($information['email']) ?  $information['email'] : '' }}</p>
                        <p>{{ isset($information['web']) ?  $information['web'] : '' }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2">
                    <div class="form-area-row foo" data-sr='enter'>
                        <div class="form-area-title">
                            <h2>Leave a Message</h2>
                        </div>
                        <div class="form-area">
                            <div class="cf-msg"></div>
                            <form class="form-inline" action="#" method="post" id="cf">
                                <div class="row">
                                    <ul class="contact-form">
                                        <li class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="fname">Your name</label>
                                                <div class="input-group">
                                                    <input type="text" id="fname" name="fname" class="form-control2" placeholder="Your name">
                                                </div>
                                            </div>
                                        </li>
                                        <li class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="fname">Email here</label>
                                                <div class="input-group">
                                                    <input type="email" class="form-control2" id="email" name="email" placeholder="Email here">
                                                </div>
                                            </div>
                                        </li>
                                        <li class="col-xs-12">
                                            <div class="form-group">
                                                <label for="subject">Subject</label>
                                                <div class="input-group">
                                                    <input type="text" id="subject" class="form-control2" placeholder="Subject" name="subject">
                                                </div>
                                            </div>
                                        </li>
                                        <li class="col-xs-12">
                                            <div class="form-group">
                                                <label for="fname">Write here</label>
                                                <div class="input-group">
                                                    <textarea rows="7" class="form-control2 form-message" placeholder="Write here" id="msg" name="msg"></textarea>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="col-xs-12 text-center">
                                        <button type="submit" id="submit" name="submit" class="btn btn-default btn-form">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Contact Form Section -->
    </div>
@endsection
