@extends('moma.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
 <div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>{{ isset($post['title']) ?  $post['title'] : '' }}</h1>
                                <p><a href="/">Home </a> > {{ isset($post['title']) ?  $post['title'] : '' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-area inner-padding7">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <div class="single-post-row " data-sr='enter'>
                        <div class="single-post-header">
                            <div class="single-post-feature">
                                <img src="{{ isset($post['image']) ?  $post['image'] : '' }}" alt="responsive img">
                            </div>
                        </div>
                        <div class="single-post-body">
                            <div class="single-post-caption">
                                <h2 class="single-post-heading"><a href="#">{{ isset($post['title']) ?  $post['title'] : '' }}</a></h2>
                                <div class="single-post-meta">{{ isset($post['nguoi-dang']) ?  $post['nguoi-dang'] : '' }} / {{ isset($post['tags']) ?  $post['tags'] : '' }}, at {{ isset($post['thoi-gian']) ?  $post['thoi-gian'] : '' }}</div>
                                <div class="single-post-sticker"><small>{{ isset($post['ngay']) ?  $post['ngay'] : '' }}</small>
                                    <p class="month">{{ isset($post['thang']) ?  $post['thang'] : '' }}</p>
                                </div>
                            </div>
                            {!! isset($post['content']) ?  $post['content'] : '' !!}
							{!! isset($post['ofin-form']) ? $post['ofin-form'] : '' !!}
                        </div>
                        
                    </div>
                </div>
                <div class="col-sm-12 col-md-3">

                </div>
            </div>
        </div>
    </div>
@endsection



