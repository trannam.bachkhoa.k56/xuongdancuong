@extends('news-03.layout.site')

@section('title', isset($information['meta-title']) ? $information['meta-title'] : '')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '')

@section('content')
    <section class="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <li class="home">
                            <a itemprop="url"  href="/" ><span itemprop="title">Trang chủ</span></a>
                            <span><i class="fa fa-angle-right"></i> </span>
                        </li>
                        <li><strong ><span itemprop="title">{{isset($category->title) ? $category->title : ''}}</span></strong></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="content-wrapper">
        <div id="content" class="row">
            <h1 class="page-title">
                {{isset($category->title) ? $category->title : ''}}
            </h1>

           

          @include('news-03.partials.sidebar')


            <!-- End Sidebar -->
            <div class="desktop-9 tablet-6 mobile-3">
                <div id="product-loop">

                    @if(empty($products))
                        <p>Không tồn tại danh mục này</p>
                    @else
                        @foreach ($products as $id => $product)
                            <div id="product-listing-520099332156" class="product-index desktop-4 tablet-half mobile-half" data-alpha="{{ $product->title }}" data-price="{{ number_format($product->price) }}đ">
                                <div class="product-index-inner">
                                </div>
                                <div class="prod-image">
                                    <a class="product-link" href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title="{{ $product->title }}">
                                        <div class="hover-element">
                                        </div>
                                        <img src="{{ !empty($product->image) ?  asset($product->image) : '' }}"
                                             alt="{{ $product->title }}"
                                             data-image="{{ !empty($product->image) ?  asset($product->image) : '' }}"
                                             data-alt-image="{{ !empty($product['anh-lon']) ?  asset($product['anh-lon']) : '' }}" />
                                    </a>
                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                                            <h3>{{ $product->title }}</h3>
                                        </a>
                                         <div class="price">
											<div class="prod-price">
											@if($product->discount > 0 )
												 <del>{{ number_format($product->price) }} đ </del>  
												<span>{{ number_format($product->discount) }}đ</span>	
											@else
												<span>{{ number_format($product->price) }}đ</span>	
											@endif
										   
												<!-- - $899.00 -->
											</div>
										</div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @endif
                </div>
            </div>
            <div id="pagination" class="desktop-12 tablet-6 mobile-3">
                @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{ $products->links() }}
                @endif
				<style>
				#pagination ul li
				{
					display: inline-block;
				}
					#pagination ul li a, #pagination ul li span
				{
					    font-size: 16px;
				}
				#pagination ul li span
				{
					color:red;
					    font-weight: bold;
					
				}
				</style>
            </div>
            <script>
                //load anh
                $(document).ready(function() {
                    function preload(arrayOfImages) {
                        $(arrayOfImages).each(function() {
                            (new Image()).src = this;
                        });
                    }

                    var images = [];

                    $('.prod-image').each(function() {
                        var img = $(this).find('img').data('alt-image');
                        images.push(img);
                    });
                    preload(images);

                    $('.product-index').hover( //When the ProductImage is hovered show the alt image.
                        function() {
                            var image = ($(this).find('.product-link img'));

                            if (image.attr('data-alt-image')) {
                                image.attr('src', image.attr('data-alt-image'));
                            };
                        },
                        function() {
                            var image = ($(this).find('.product-link img'));
                            image.attr('src', image.attr('data-image')); //Show normal image for non-hover state.
                        }
                    );

                });
            </script>
        </div>
    </div>
@endsection
