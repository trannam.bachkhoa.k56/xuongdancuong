<header>
        <nav class="navbar navbar-default navbar-fixed-top nav-area" id="scroll-menu">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" alt="responsive img"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                                @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id => $menuelement)
                                <li class="dropdown {{ ($id == 0) ? 'active' : ''  }}">
                                    <a href="{{ $menuelement['url'] }}" @if (!empty($menuelement['children']))
                                        class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
                                        @endif >
                                        {{ $menuelement['title_show'] }}
                                    </a>
                                    @if (!empty($menuelement['children']))
                                        <ul class="dropdown-menu">
                                            @foreach ($menuelement['children'] as $menuelementpr)
                                                <li><a href="{{ $menuelementpr['url'] }}">{{ $menuelementpr['title_show'] }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                    <div class="nav navbar-nav navbar-right hidden-xs hidden-sm">
                        <form action="#" method="POST">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" placeholder="Search...." class="form-control placeholder" id="text-mail2">
                                    <span class="input-group-btn">
                                                    <a href="#" class="btn btn-default btn-search"><i class="fa fa-search"></i></a>
                                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>
    </header>