<footer>
        <div class="footer-area foo" data-sr='bottom'>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="footer-content">
                            <img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" alt="responsive img">
                            <ul class="address-list">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <div class="address-content">
                                        <p>{{ isset($information['dia-chi']) ?  $information['dia-chi'] : '' }}</p>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <div class="address-content">
                                        <p>{{ isset($information['dien-thoai']) ?  $information['dien-thoai'] : '' }}</p>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-globe"></i>
                                    <div class="address-content">
                                        <p>{{ isset($information['email']) ?  $information['email'] : '' }}</p>
                                        <p>{{ isset($information['web']) ?  $information['web'] : '' }}</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-2">
                        <div class="footer-content">
                            <h5>OUR FEATURES</h5>
                            <ul class="feature-list">
                                @foreach(\App\Entity\SubPost::showSubPost('features', 8) as $id => $feature)
                                <li>
                                    <a href="#">{{ isset($feature['title']) ?  $feature['title'] : '' }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="footer-content">
                            <h5>Fanpage Facebook</h5>
                            <ul class="instagram-list">
                               <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fvn3ctran%2F&tabs=timeline&width=340&height=350px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1875296572729968" width="100%" height="300px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="footer-content">
                            <h5>Liên Hệ</h5>
                            <div class="footer-form"> 
                                <div id="getfly-optin-form-iframe-1561516648793"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var regex = /(https?:\/\/.*?)\//g; var furl = regex.exec(r); r = furl ? furl[0] : r; var f = document.createElement("iframe"); const url_string = new URLSearchParams(window.location.search); var utm_source, utm_campaign, utm_medium, utm_content, utm_term; if((!url_string.has('utm_source') || url_string.get('utm_source') == '') && document.cookie.match(new RegExp('utm_source' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_source' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_source') != null ? "&utm_source=" + url_string.get('utm_source') : "";} if((!url_string.has('utm_campaign') || url_string.get('utm_campaign') == '') && document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_campaign') != null ? "&utm_campaign=" + url_string.get('utm_campaign') : "";} if((!url_string.has('utm_medium') || url_string.get('utm_medium') == '') && document.cookie.match(new RegExp('utm_medium' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_medium' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_medium') != null ? "&utm_medium=" + url_string.get('utm_medium') : "";} if((!url_string.has('utm_content') || url_string.get('utm_content') == '') && document.cookie.match(new RegExp('utm_content' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_content' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_content') != null ? "&utm_content=" + url_string.get('utm_content') : "";} if((!url_string.has('utm_term') || url_string.get('utm_term') == '') && document.cookie.match(new RegExp('utm_term' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_term' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_term') != null ? "&utm_term=" + url_string.get('utm_term') : "";} f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=qUohlAOkfFBneZTdxvqngwyciOGEkwbFpNYdZEpnsaVOdKjK7x&referrer="+r); f.style.width = "100%";f.style.height = "500px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1561516648793");s.appendChild(f); })(); </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	
	
	
	<div class="embedCode" style="">
		<div class="messagerBot">
			<style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff; width: 45px; height: 45px; text-align: center; bottom: 75px; right: 10px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{width: 120px; left: -140px; top: 5px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style><div class="fb-livechat"> <div class="ctrlq fb-overlay"></div><div class="fb-widget"> <div class="ctrlq fb-close"></div><div class="fb-page" data-href="https://www.facebook.com/vn3ctran" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div><div class="fb-credit"> <a href="https://www.facebook.com/vn3ctran" target="_blank"></a> </div><div id="fb-root"></div></div><a href="https://m.me/vn3ctran" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button"> <div class="bubble">1</div></a></div><script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><script>jQuery(document).ready(function($){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){return true;}else{return false;}}var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")}; setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>
		</div>
		<div class="zaloBot">
			<div class="zalo-chat-widget" data-oaid="579745863508352884" data-welcome-message="Rất vui khi được hỗ trợ bạn!" 
						data-autopopup="5" data-width="350" data-height="420""></div>
					<script src="https://sp.zalo.me/plugins/sdk.js"></script> 
			</div>
		</div> 
		<div class="hotlineBot">
			<div class="fix_tel">
				<div class="ring-alo-phone ring-alo-green ring-alo-show" id="ring-alo-phoneIcon" style="right: 0; bottom: 130px;">
					<div class="ring-alo-ph-circle"></div>
					<div class="ring-alo-ph-circle-fill"></div>
					<div class="ring-alo-ph-img-circle">

					<a href="tel:0946673322"><img class="lazy" src="https://khomaythegioi.com/icon/goi.png" alt="G"></a>

					</div>
					</div>
					<div class="tel">
					<a href="tel:0946673322"><p class="fone"></p></a>
				</div>
			</div> 
		</div>
	</div>
	
	<style>
	

.fone {
font-size: 19px; /* chữ cạnh nút gọi */
color: #f00;
line-height: 40px;
font-weight: bold;
padding-left: 48px; /* cách bên trái cho chữ */
margin: 0 0;
}
.fix_tel { position:fixed; bottom:15px; right:0; z-index:999;} /* left 18px là cách bên trái 18px. nếu muốn cho nút gọi sang phải thay là right */
.fix_tel a {text-decoration: none; display:block;}
.tel {width:205px; height:40px; position:relative; overflow:hidden;background-size:40px;border-radius:28px;border:none}
.ring-alo-phone {
background-color: transparent;
cursor: pointer;
height: 65px;
position: absolute;
transition: visibility 0.5s ease 0s;
visibility: hidden;
width: 80px;
z-index: 200000 !important;
}
.ring-alo-phone.ring-alo-show {
visibility: visible;
}
.ring-alo-phone.ring-alo-hover, .ring-alo-phone:hover {
opacity: 1;
}
.ring-alo-ph-circle {
animation: 1.2s ease-in-out 0s normal none infinite running ring-alo-circle-anim;
background-color: transparent;
border: 2px solid rgba(30, 30, 30, 0.4);
border-radius: 100%;
height: 70px;
left: 10px;
opacity: 0.1;
position: absolute;
top: 12px;
transform-origin: 50% 50% 0;
transition: all 0.5s ease 0s;
width: 70px;
}
.ring-alo-phone.ring-alo-active .ring-alo-ph-circle {
animation: 1.1s ease-in-out 0s normal none infinite running ring-alo-circle-anim !important;
}
.ring-alo-phone.ring-alo-static .ring-alo-ph-circle {
animation: 2.2s ease-in-out 0s normal none infinite running ring-alo-circle-anim !important;
}
.ring-alo-phone.ring-alo-hover .ring-alo-ph-circle, .ring-alo-phone:hover .ring-alo-ph-circle {
border-color: #009900;
opacity: 0.5;
}
.ring-alo-phone.ring-alo-green.ring-alo-hover .ring-alo-ph-circle, .ring-alo-phone.ring-alo-green:hover .ring-alo-ph-circle {
border-color: #baf5a7;
opacity: 0.5;
}
.ring-alo-phone.ring-alo-green .ring-alo-ph-circle {
border-color: #009900;
opacity: 0.5;
}
.ring-alo-ph-circle-fill {
animation: 2.3s ease-in-out 0s normal none infinite running ring-alo-circle-fill-anim;
background-color: #000;
border: 2px solid transparent;
border-radius: 100%;
height: 30px;
left: 30px;
opacity: 0.1;
position: absolute;
top: 33px;
transform-origin: 50% 50% 0;
transition: all 0.5s ease 0s;
width: 30px;
}
.ring-alo-phone.ring-alo-hover .ring-alo-ph-circle-fill, .ring-alo-phone:hover .ring-alo-ph-circle-fill {
background-color: rgba(0, 175, 242, 0.5);
opacity: 0.75 !important;
}
.ring-alo-phone.ring-alo-green.ring-alo-hover .ring-alo-ph-circle-fill, .ring-alo-phone.ring-alo-green:hover .ring-alo-ph-circle-fill {
background-color: rgba(117, 235, 80, 0.5);
opacity: 0.75 !important;
}
.ring-alo-phone.ring-alo-green .ring-alo-ph-circle-fill {
background-color: rgba(0, 175, 242, 0.5);
opacity: 0.75 !important;
}

 

.ring-alo-ph-img-circle {
animation: 1s ease-in-out 0s normal none infinite running ring-alo-circle-img-anim;
border: 2px solid transparent;
border-radius: 100%;
height: 30px;
left: 30px; 
opacity: 1;
position: absolute;
top: 33px;
transform-origin: 50% 50% 0;
width: 30px;
}

.ring-alo-phone.ring-alo-hover .ring-alo-ph-img-circle, .ring-alo-phone:hover .ring-alo-ph-img-circle {
background-color: #009900;
}
.ring-alo-phone.ring-alo-green.ring-alo-hover .ring-alo-ph-img-circle, .ring-alo-phone.ring-alo-green:hover .ring-alo-ph-img-circle {
background-color: #75eb50;
}
.ring-alo-phone.ring-alo-green .ring-alo-ph-img-circle {
background-color: #009900;
}
@keyframes ring-alo-circle-anim {
0% {
opacity: 0.1;
transform: rotate(0deg) scale(0.5) skew(1deg);
}
30% {
opacity: 0.5;
transform: rotate(0deg) scale(0.7) skew(1deg);
}
100% {
opacity: 0.6;
transform: rotate(0deg) scale(1) skew(1deg);
}
}

 

@keyframes ring-alo-circle-img-anim {
0% {
transform: rotate(0deg) scale(1) skew(1deg);
}
10% {
transform: rotate(-25deg) scale(1) skew(1deg);
}
20% {
transform: rotate(25deg) scale(1) skew(1deg);
}
30% {
transform: rotate(-25deg) scale(1) skew(1deg);
}
40% {
transform: rotate(25deg) scale(1) skew(1deg);
}
50% {
transform: rotate(0deg) scale(1) skew(1deg);
}
100% {
transform: rotate(0deg) scale(1) skew(1deg);
}
}
@keyframes ring-alo-circle-fill-anim {
0% {
opacity: 0.2;
transform: rotate(0deg) scale(0.7) skew(1deg);
}
50% {
opacity: 0.2;
transform: rotate(0deg) scale(1) skew(1deg);
}
100% {
opacity: 0.2;
transform: rotate(0deg) scale(0.7) skew(1deg);
}
}
.ring-alo-ph-img-circle a img {
padding: 1px 0 12px 1px;
width: 30px;
position: relative;
top: -1px;
}
}


	
	</style>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    <!-- Ends Footer Section -->
    <!-- Copyright Section -->
    <div class="copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="footer-copyright">{{ isset($information['copyright']) ?  $information['copyright'] : '' }}</p>
                </div>
            </div>
        </div>
    </div>




