<div class="section-title  desktop-3 tablet-6 mobile-12 sidebarIndex">
					<form action="" method="get" id="filterProduct">
						<div class="show-desktop">
							<a class="show mobile-3" href="#"><i class="icon-align-justify icon-2x"></i></a>
							<div class="desktop-12 tablet-12 tablet-push-12 mobile-12" id="sidebar">
								<ul>
								</ul>
								<h4>Danh mục sản phẩm</h4>
								<ul>
									@foreach (\App\Entity\Menu::showWithLocation('side-left-menu') as $Mainmenu)
										@foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
											<li style="height: 30px;">
												<a href="{{ $menuelement['url'] }}" title="{{ $menuelement['title_show'] }}">{{ $menuelement['title_show'] }}</a>
											</li>
										@endforeach
									@endforeach
								</ul>

								<!-- Start Filtering -->

								<h4>Lọc sản phẩm</h4>
								@foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
									<ul>
										<li>
											<b style="font-size: 14px">{{ $filterGroup->group_name }}</b>
											<ul class="filter">
												@foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
													<li>
														<a onclick="return selectColor(this);" style="display: -webkit-inline-box;">
															<input type="checkbox" name="filter[]" class="inputColor" value="{{ $filter->name_filter }}"
																   @if(!empty($_GET['filter']) && in_array($filter->name_filter, $_GET['filter']) >0) checked @endif
																   onChange="return changeFilter(this);"
															/>
															<a href="#" style="font-size: 15px">{{ $filter->name_filter }}</a>
														</a>
													</li>
												@endforeach
												<script>
													function changeFilter(e) {
														$('#filterProduct').submit();
													}
												</script>
											</ul>
										</li>
									</ul>
								@endforeach
							</div>

							{{--filter--}}
						</div>
					</form>
				
					<div class="show-mobile">
						<div class="mobile-menu" id="sidebar-mobile">
							<select id="mobile-collection-menu">
								<optgroup label="">
								</optgroup>
								<optgroup label="SHOP BY THEME:">
									@foreach (\App\Entity\Menu::showWithLocation('side-left-menu') as $Mainmenu)
										@foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
											<li style="height: 30px;">
												<option value="{{ $menuelement['url'] }}">{{ $menuelement['title_show'] }}</option>
											</li>
										@endforeach
									@endforeach

								</optgroup>

							</select>
							<!-- Start Filtering -->
							<div class="filterPro">
								<h4 class="clichshow">Lọc sản phẩm <i class="fa fa-angle-down" aria-hidden="true"></i></h4>
								<div class="showmenuHiddenMB">
									@foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
										<ul>
											<li>
												<b style="font-size: 14px">{{ $filterGroup->group_name }}</b>
												<ul class="filter">
													@foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
														<li>
															<a onclick="return selectColor(this);" style="display: -webkit-inline-box;">
																<input type="checkbox" name="filter[]" class="inputColor" value="{{ $filter->name_filter }}"
																	   @if(!empty($_GET['filter']) && in_array($filter->name_filter, $_GET['filter']) >0) checked @endif
																	   onChange="return changeFilter(this);"
																/>
																<a href="#" style="font-size: 15px">{{ $filter->name_filter }}</a>
															</a>
														</li>
													@endforeach
													<script>
														function changeFilter(e) {
															$('#filterProduct').submit();
														}
														$( document ).ready(function() {
															$('.clichshow').click(function(){
																$('.showmenuHiddenMB').toggle();
															});
														});
													</script>
												</ul>
											</li>
										</ul>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
