<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />   
	<meta name="yandex-verification" content="6edad8a8e0020432" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <link rel="icon" href="" type="image/x-icon" />

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
    <link rel="shortcut icon" type="image/png" href="{{ isset($information['fav']) ?  $information['fav'] : '' }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Raleway:400,600,700" rel="stylesheet">
 	<link rel="stylesheet" href="/moma/css/bootstrap.min.css">
    <link rel="stylesheet" href="/moma/css/font-awesome.min.css">
    <link rel="stylesheet" href="/moma/css/animate.min.css">
    <link rel="stylesheet" href="/moma/css/prettyPhoto.css">
    <link rel="stylesheet" href="/moma/css/default.css">
    <link rel="stylesheet" href="/moma/css/typography.css">
    <link rel="stylesheet" href="/moma/css/style.css">
    <link rel="stylesheet" href="/moma/css/responsive.css">
    <link rel="stylesheet" href="/moma/css/owl.carousel.min.css">

	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}
	
	{!! isset($information['google-analynic']) ? $information['google-analynic'] : '' !!}
	


    <style type="text/css">
        .fw6{
            font-weight: 600;
        }
        .fw7{
            font-weight: 700;
        }
        .textUpper{
            text-transform: uppercase;
        }
        .textCenter{
            text-align: center;
        }
        .nobdTop{
            border-top: 0 !important;
        }
        .nobdLeft{
            border-left: 0 !important;
        }
        .bdTopWhite{
            border-top: 1px solid white !important;
        }
        .bdLeftWhite{
            border-left: 1px solid white !important;
        }
        .bdBlack{
            border: 1px solid black !important;
        }
        .bgrBlueMoma{
            background: #0db1ad;
        }
        .blueMoma{
            color: #0db1ad;
        }
        .blue{
            color: #10519F;
        }
        .red{
            color: #f13c3c;
        }
        .f16{
            font-size: 16px;
        }
        .f18{
            font-size: 18px;
        }
        .f20{
            font-size: 20px;
        }
        .f23{
            font-size: 23px;
        }
        .mgb20{
            margin-bottom: 20px;
        }
        .bgrf5{
            background: #f5f5f5;
        }
        .pd10{
            padding: 10px;
        }
        .radius5{
            border-radius: 5px;
        }
        .white, .hvWhite:hover{
            color: white !important;
        }
        .block{
            display: block;
        }
        .w20p{
            width: 20%;
        }
        .w25p{
            width: 25%;
        }
        .mgb0{
            margin-bottom: 0;
        }
        .pdl20{
            padding-left: 20px
        }
        .green{
            color: green;
        }
        .middle{
            vertical-align: middle !important;
        }
    </style>
</head>
<body data-spy="scroll" data-target="#scroll-menu" data-offset="65">
   <!-- Preloader -->
    <div class="preloader-wrap">
        <div class="preloader-inside">
            <div class="spinner spinner-1">
                <img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" alt="responsive img">
            </div>
        </div>
    </div>
    <!-- End Preloader -->
    <!-- Scroll Top Button -->
    <a href="#home" class="smoothscroll">
        <div class="scroll-top"><i class="fa fa-angle-up"></i></div>
    </a>
    <!-- End Scroll Top Button -->
    <!-- Nav Section -->
    @include('moma.common.header')
    @yield('content')
    @include('moma.common.footer')
	<script src="/moma/js/jquery-2.1.1.min.js"></script>
    <script src="/moma/js/bootstrap.min.js"></script>
    <script src="/moma/js/scrollreveal.min.js"></script>
    <script src="/moma/js/jquery.waypoints.min.js"></script>
    <script src="/moma/js/jquery.counterup.min.js"></script>
    <script src="/moma/js/owl.carousel.min.js"></script>
    <!-- Isotope Js -->
    <script src="/moma/js/isotope.pkgd.min.js"></script>
    <script src="/moma/js/isotope_custom.js"></script>
    <!-- Masonary Js -->
    <script src="/moma/js/masonry.pkgd.min.js"></script>
    <!-- Prettyphoto js -->
    <script src="/moma/js/jquery.prettyPhoto.js"></script>
    <script src="/moma/js/theme.js"></script>
	<!-- ManyChat -->

    <script>
  function contact(e) {
    var $btn = $(e).find('button').button('loading');
    var data = $(e).serialize();
    
    $.ajax({
            type: "POST",
            url: '{!! route('sub_contact') !!}',
            data: data,
            success: function(result){
                var obj = jQuery.parseJSON( result);
        // gửi thành công
                if (obj.status == 200) {
          alert(obj.message);
          $btn.button('reset');
          
          return;
        }
        
        // gửi thất bại
        if (obj.status == 500) {
          alert(obj.message);
          $btn.button('reset');

          
          return;
        }
            },
            error: function(error) {
                //alert('Lỗi gì đó đã xảy ra!')
            }

        });
    
    
    return false;
  }
</script>
</body>
</html>
