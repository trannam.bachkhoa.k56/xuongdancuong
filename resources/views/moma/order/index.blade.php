@extends('news-03.layout.site')

@section('title','Đặt hàng')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '' )

@section('content')
    <section class="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <li class="home">
                            <a itemprop="url"  href="/" ><span itemprop="title">Trang chủ</span></a>
                            <span><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li><strong itemprop="title">Giỏ hàng</strong></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <style>
        .bread-crumb .breadcrumb
        {
            list-style: none;
        }
        .bread-crumb .breadcrumb li
        {
            display: inline-block;
        }
    </style>

    <div class="content-wrapper">
        <div id="content" class="row">
            <div id="shopping-cart" class="desktop-12 tablet-6 mobile-3">
                <h2>Giỏ hàng</h2>
                <form action="{{ route('send') }}" method="post" id="cartform">
                    {{ csrf_field() }}
                    <table>
                        <thead>
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Giá</th>
                            <th style="text-align: center;">Số lượng</th>
                            <th style="text-align: center;">Xóa</th>
                            <th>Tổng tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $sumPrice = 0;?>
                        @foreach($orderItems as $id => $orderItem)
                            <tr>
                                <td class="cart-item">
                                    <div class="cart-image">
                                        <a href="/products/feather-canvas?variant=6950973931580" title="{{ $orderItem->title }}">
                                            <img src="{{ $orderItem->image }}" alt="{{ $orderItem->title }}" width="100px"  />
                                        </a>
                                    </div>
                                    <div class="cart-title">
                                        {{ $orderItem->title }}
                                        <span class="Bold-theme-hook-DO-NOT-DELETE bold_cart_item_properties" style="display:none !important;"></span>
                                    </div>
                                </td>

                                <td class="cart-price">
                                    <div class="price">
                                        Giá:
                                        @if (!empty($orderItem->price_deal)
                                         && !empty($orderItem->discount_end) && ( time() < strtotime($orderItem->discount_end))
                                         && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                        )
                                            <span class="priceOld">{{ number_format($orderItem->price , 0)}}</span>
                                            <span class="priceDiscount">{{ number_format($orderItem->price_deal , 0) }}
                                                đ</span>
                                            <input type="hidden" class="unitPrice"
                                                   value="{{ $orderItem->price_deal }}">
                                        @elseif (!empty($orderItem->discount))
                                            <span class="priceOld">{{ number_format($orderItem->price , 0)}}</span>
                                            <span class="priceDiscount">{{ number_format($orderItem->discount , 0) }}
                                                 đ</span>
                                            <input type="hidden" class="unitPrice"
                                                   value="{{ $orderItem->discount }}">
                                        @else
                                            <input type="hidden" class="unitPrice"
                                                   value="{{ $orderItem->price }}">
                                            <span class="priceDiscount">{{ number_format($orderItem->price , 0)}}
                                                đ</span>
                                        @endif

                                        @if (!empty($orderItem->price_deal)
                                        && !empty($orderItem->discount_end) && ( time() < strtotime($orderItem->discount_end))
                                        && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                        )
                                            Tiết kiệm: <span class="priceDiscount">{{ round(($orderItem->price - $orderItem->price_deal) / $orderItem->price * 100) }}
                                                %</span>
                                        @elseif (!empty($orderItem->discount))
                                            Tiết kiệm: <span class="priceDiscount">{{ round(($orderItem->price - $orderItem->discount) / $orderItem->price * 100) }}
                                                %</span>
                                        @endif
                                    </div>
                                </td>

                                <td class="cart-quantity">
                                    <input type="hidden" name="product_id[]"
                                           value="{{ $orderItem->product_id }}"/>
                                    {{--min = 1--}}
                                    <input type="number" name="quantity[]" style="width:80px;"
                                           value="{{ $orderItem->quantity }}"
                                           onchange="return changeQuantity(this);" min="0"/>
                                </td>


                                <td class="cart-remove">
                                    <a href="#" onclick="remove_item(); return false;"><i class="icon-trash icon-2x"></i></a>
                                </td>
                                <td class="cart-total totalPrice">
                                    <font color="red"><?php
                                        if (!empty($orderItem->price_deal)
                                            && !empty($orderItem->discount_end) && (time() < strtotime($orderItem->discount_end))
                                            && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                        ) {
                                            $sumPrice += ($orderItem->price_deal * $orderItem->quantity);
                                            echo number_format(($orderItem->price_deal * $orderItem->quantity), 0);
                                        } elseif (!empty($orderItem->discount)) {
                                            $sumPrice += ($orderItem->discount * $orderItem->quantity);
                                            echo number_format(($orderItem->discount * $orderItem->quantity), 0);
                                        } else {
                                            $sumPrice += ($orderItem->price * $orderItem->quantity);
                                            echo number_format(($orderItem->price * $orderItem->quantity), 0);
                                        } ?></font>đ
                                </td>
                            </tr>
                        @endforeach
                            <tr>
                                <td colspan="5" class="totalMobile">
                                    <div class="inline-block" style="float: right; margin-right: 30px;">
                                        <span>
                                              <a href="/" class="btn btn-default">Tiếp tục mua hàng </a> ||
                                        </span>
                                        <span>Tổng tiền:</span>
                                        <strong><span class="totals_price price sumPrice">{{ number_format($sumPrice , 0) }}</span></strong>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                    <div class="InformationPerson informationOrder clearfix">
                        <div class="mainTitle lineorange"><h3 class="titleV bgorange"><i class="fa fa-newspaper-o"
                                                                                         aria-hidden="true"></i>Thông
                                tin thanh toán</h3></div>
                        <div class="col-xs-12 col-xs-offset-0 col-md-9 col-md-offset-3 pb20">
                            <p class="titlePayment">Vui lòng điền đầy đủ thông tin nhận hàng bên dưới, các mục có
                                dấu <font color="red">(*)</font> là bắt buộc </p>
                            <div class="form-group">
                                <label>Họ và tên<span><font color="red">*</font></span>: </label>
                                <input type="text" class="form-control" name="ship_name" placeholder=""
                                       value="{{ !empty(old('ship_name')) ? old('ship_name') : '' }}" required/>
                                @if ($errors->has('ship_name'))
                                    <span class="red">
                                        <strong>{{ $errors->first('ship_name') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Điện thoại<span><font color="red">*</font></span>: </label>
                                <input type="text" class="form-control" name="ship_phone" placeholder=""
                                       value="{{ !empty(old('ship_phone')) ? old('ship_phone') : '' }}" required/>
                                @if ($errors->has('ship_phone'))
                                    <span class="red">
                                        <strong>{{ $errors->first('ship_phone') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Email<span></span>: </label>
                                <input type="email" class="form-control" name="ship_email" placeholder=""
                                       value="{{ !empty(old('ship_email')) ? old('ship_email') : '' }}" required/>
                                @if ($errors->has('ship_email'))
                                    <span class="red">
                                        <strong>{{ $errors->first('ship_email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ nhận hàng<span><font color="red">*</font></span>: </label>
                                <input type="text" class="form-control" name="ship_address" placeholder=""
                                       value="{{ !empty(old('ship_address')) ? old('ship_address') : '' }}"
                                       required/>
                                @if ($errors->has('ship_address'))
                                    <span class="red">
                                        <strong>{{ $errors->first('ship_address') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="btnSubmit">
                                <button type="submit" class="btn btn-danger">Thanh toán</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <script>
        function changeQuantity(e) {
            var unitPrice = $(e).parent().parent().find('.unitPrice').val();
            var quantity = $(e).val();
            var totalPrice = unitPrice * quantity;
            var sum = 0;
            $(e).parent().parent().find('.totalPrice').find('font').empty();
            $(e).parent().parent().find('.totalPrice').find('font').html(numeral(totalPrice).format('0,0'));

            $('.totalPrice').each(function () {
                var totalPrice = $(this).find('font').html();
                sum += parseInt(numeral(totalPrice).value());
            });
            $('.sumPrice').empty();
            $('.sumPrice').html(numeral(sum).format('0,0'));
        }
    </script>
@endsection
