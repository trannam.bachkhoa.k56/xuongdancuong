@extends('moma.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )


@section('content')
    <div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>{{ isset($post['title']) ?  $post['title'] : '' }}</h1>
                                <p><a href="/">Home </a> > {{ isset($post['title']) ?  $post['title'] : '' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 50px"></div>
    <section class="webCSKH">
        <div class="container">
            <p class="f18 fw7 textUpper red">WEBSITE CHĂM SÓC KHÁCH HÀNG TÍCH HỢP VỚI HỆ THỐNG CRM GỒM CÓ:</p>
            <ul class="pdl20 f16">
                <li>Giải pháp Chăm sóc khách hàng</li>
                <li>Giải pháp Quản lý đội kinh doanh</li>
                <li>Giải pháp Giao việc tự động</li>
                <li>Giải pháp SMS marketing</li>
                <li>Giải pháp Email marketing</li>
                <li>Giải pháp kênh marketing tự động</li>
                <li>Giải pháp marketing truyển miệng 3.0</li>
                <li>Giải pháp đo lường KPI</li>
                <li>Hỗ trợ App</li>
                <li>Bộ tài liệu đào tạo</li>
            </ul>
            <p class="f23 fw7 textUpper textCenter mgb20 blueMoma">BẢNG GIÁ DỊCH VỤ WEBSITE CHĂM SÓC KHÁCH HÀNG</p>
            <div class="table-responsive">
                <table class="table table-bordered fw6">
                    <thead>
                    <tr class="">
                        <th scope="row" class="textCenter bdBlack white bgrBlueMoma f18 w25p"></th>
                        <th class="textCenter bdBlack white bgrBlueMoma f18 w20p">
                            <p>GÓI CSKH01</p>
                            <p>500.000 đ / THÁNG</p>
                        </th>
                        <th class="textCenter bdBlack white bgrBlueMoma f18 w20p">
                            <p>GÓI CSKH02</p>
                            <p>1.568.000 đ /THÁNG</p>
                        </th>
                        <th class="textCenter bdBlack white bgrBlueMoma f18 w20p">
                            <p>GÓI CSKH03</p>
                            <p>2.068.000 đ / THÁNG</p>
                        </th>
                        <th class="textCenter bdBlack white bgrBlueMoma f18 w20p">
                            <p>GÓI CSKH04</p>
                            <p>LIÊN HỆ</p>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row" class="bdBlack blue">Số lượng người dùng</th>
                        <td class="textCenter bdBlack">1</td>
                        <td class="textCenter bdBlack">30</td>
                        <td class="textCenter bdBlack">50</td>
                        <td class="textCenter bdBlack">Hơn 50</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bdBlack bgrf5 blue">Số lượng khách hàng</th>
                        <td class="textCenter bdBlack bgrf5">10.000</td>
                        <td class="textCenter bdBlack bgrf5">30.000</td>
                        <td class="textCenter bdBlack bgrf5">50.000</td>
                        <td class="textCenter bdBlack bgrf5">Hơn 50.000</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bdBlack blue">Thời gian</th>
                        <td class="textCenter bdBlack">1 tháng</td>
                        <td class="textCenter bdBlack">1 tháng</td>
                        <td class="textCenter bdBlack">1 tháng</td>
                        <td class="textCenter bdBlack">1 tháng</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bdBlack  blue"></th>
                        <td class="textCenter bdBlack "><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                        <td class="textCenter bdBlack "><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                        <td class="textCenter bdBlack "><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                        <td class="textCenter bdBlack "><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <div style="height: 50px"></div>
@endsection


