@extends('moma.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )


@section('content')
    <div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>{{ isset($post['title']) ?  $post['title'] : '' }}</h1>
                                <p><a href="/">Home </a> > {{ isset($post['title']) ?  $post['title'] : '' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 50px"></div>
    <section class="supportMoma">
        <div class="container">
            <p class="f23 fw7 textUpper textCenter mgb20 blueMoma">BẢNG GIÁ THIẾT KẾ VÀ HỖ TRỢ WEBSITE MOMA</p>
            <div class="table-responsive">
                <table class="table table-bordered fw6">
                    <thead>
                    <tr class="bgrf5">
                        <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Tên gói</th>
                        <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Mô tả</th>
                        <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Số lượng</th>
                        <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Giá thành</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row" class="bdBlack blue">Logo</th>
                        <td class="textCenter bdBlack ">Thiết kế theo yêu cầu</td>
                        <td class="textCenter bdBlack ">1 ảnh</td>
                        <td class="textCenter bdBlack red fw7">1.200.000 đ</td>
                    </tr>
                    <tr class="bgrf5">
                        <th scope="row" class="bdBlack blue">Banner Facebook</th>
                        <td class="textCenter bdBlack ">Thiết kế theo yêu cầu</td>
                        <td class="textCenter bdBlack ">1 ảnh</td>
                        <td class="textCenter bdBlack red fw7">500.000 đ</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bdBlack blue">Ảnh bài viết Facebook</th>
                        <td class="textCenter bdBlack ">Thiết kế theo yêu cầu</td>
                        <td class="textCenter bdBlack ">5 ảnh</td>
                        <td class="textCenter bdBlack red fw7">2.000.000 đ</td>
                    </tr>
                    <tr class="bgrf5">
                        <th scope="row" class="bdBlack blue">Banner Website</th>
                        <td class="textCenter bdBlack ">Thiết kế theo yêu cầu</td>
                        <td class="textCenter bdBlack ">5 ảnh</td>
                        <td class="textCenter bdBlack red fw7">2.000.000 đ</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bdBlack blue">Ảnh bài viết Facebook</th>
                        <td class="textCenter bdBlack ">Thiết kế theo yêu cầu</td>
                        <td class="textCenter bdBlack ">5 ảnh</td>
                        <td class="textCenter bdBlack red fw7">2.000.000 đ</td>
                    </tr>
                    <tr class="bgrf5">
                        <th scope="row" class="bdBlack blue">Bài viết Facebook</th>
                        <td class="textCenter bdBlack ">Bài viết chuẩn SEO</td>
                        <td class="textCenter bdBlack ">10 bài</td>
                        <td class="textCenter bdBlack red fw7">850.000 đ</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bdBlack blue">Bài viết Website</th>
                        <td class="textCenter bdBlack ">Bài viết chuẩn SEO</td>
                        <td class="textCenter bdBlack ">10 bài</td>
                        <td class="textCenter bdBlack red fw7">850.000 đ</td>
                    </tr>
                    <tr class="bgrf5">
                        <th scope="row" class="bdBlack blue"></th>
                        <td class="textCenter bdBlack"><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                        <td class="textCenter bdBlack"><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                        <td class="textCenter bdBlack"><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <div style="height: 50px"></div>
    <section class="supportMoma">
    <div class="container">
        <p class="f23 fw7 textUpper textCenter mgb20 blueMoma">BẢNG GIÁ HỖ TRỢ MARKETING</p>
        <div class="table-responsive">
            <table class="table table-bordered fw6">
                <thead>
                <tr class="bgrf5">
                    <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Tên gói</th>
                    <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Mô tả</th>
                    <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Số lượng</th>
                    <th class="textCenter  bdBlack white bgrBlueMoma f18 w25p">Giá thành</th>
                </tr>
                </thead>
                <tbody>
                <tr class="middle">
                    <th scope="row" class="bdBlack blue middle">GOOGLEKEYWORD</th>
                    <td class="bdBlack middle">Phân tích 50 từ khóa của google hiệu quả nhất có thống kê chi tiết</td>
                    <td class="textCenter bdBlack middle">1 lần</td>
                    <td class="textCenter bdBlack red fw7 middle">500.000 đ</td>
                </tr>
                <tr class="bgrf5 middle">
                    <th scope="row" class="bdBlack blue middle">GOOGLESETTING</th>
                    <td class=" bdBlack middle">Lên chiến dịch quảng cáo google cho khách (yêu cầu đã có từ khóa)</td>
                    <td class="textCenter bdBlack middle">1 lần</td>
                    <td class="textCenter bdBlack red fw7 middle">690.000 đ</td>
                </tr>
                <tr class="bgrf5 middle">
                    <th scope="row" class="bdBlack blue middle">CHATBOTSETTING</th>
                    <td class="bdBlack middle">
                        <p class="mgb0">- Cài đặt Chatbot cho trang bán hàng của bạn trên Facebook. Tự động chuyển comment của khách hàng thành inbox trả lời tự động cho khách hàng ngay khi khách comment</p>
                        <p class="mgb0">-  Tạo kịch bản chào mừng khi có khách bắt đầu cuộc trò chuyện, hệ thống sẽ tự động trả lời gắn thẻ, phân loại khách hàng. Tạo kịch bản CSKH tự động cho từng nhóm khách hàng</p>
                    </td>
                    <td class="textCenter bdBlack middle">1 lần</td>
                    <td class="textCenter bdBlack red fw7 middle">2.00.000 đ</td>
                </tr>
                <tr class="middle">
                    <th scope="row" class="bdBlack blue"></th>
                    <td class="textCenter bdBlack"><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                    <td class="textCenter bdBlack"><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                    <td class="textCenter bdBlack"><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    </section>
    <div style="height: 50px"></div>
@endsection


