@extends('moma.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )


@section('content')
   <div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
   <div class="header-caption">
      <div class="header-caption-contant">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div class="header-caption-inner">
                     <h1>{{ isset($post['title']) ?  $post['title'] : '' }}</h1>
                     <p><a href="/">Home </a> > {{ isset($post['title']) ?  $post['title'] : '' }}</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="blog-area" style="padding: 80px 0px">
   <div class="container">
    <div class="priceTable">
      <h2>1. Bảng giá website Moma</h2>
      <br>
      <table border="1" cellpadding="1" cellspacing="1" >
         <thead>
            <tr>
               <th scope="col">Tên gói</th>
               <th scope="col"style="width: 50%">Mô tả</th>
               <th scope="col">Thời gian  sử dụng</th>
               <th scope="col">Thời gian hoàn thành</th>
               <th scope="col">Tặng kèm</th>
               <th scope="col">Giá thành</th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td><strong>web01</strong></td>
               <td>
                  Miễn phí tạo website tại website-mienphi.com. Số sản phẩm: 30, Số tin tức: 60, Số liên hệ: 30, Số đơn hàng: 30.
               </td>
               <td>trọn đời</td>
               <td></td>
               <td></td>
               <td>Miễn phí</td>
            </tr>
            <tr>
               <td><strong>web02</strong></td>
               <td> nâng cấp thành gói doanh nghiệp trong website miễn phí: sản phẩm:150, Bài viết:200, Đơn hàng tối đa: 500, Liên hệ tối đa: 500</td>
               <td>1 năm</td>
               <td></td>
               <td></td>
               <td>1.200.000 đ</td>
            </tr>
            <tr>
               <td><strong>web03</strong></td>
               <td>Nâng cấp thành gói nâng cao trong website miễn phí: sản phẩm: 1000, Bài viết:2000, Đơn hàng tối đa: không giới hạn, Liên hệ tối đa: không giới hạn.</td>
               <td>1 năm</td>
               <td></td>
               <td></td>
               <td>6.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>web04</strong></td>
               <td>Thiết kế website hoàn toàn mới</td>
               <td>1 lần</td>
               <td>liên hệ</td>
               <td></td>
               <td>5000.000 đ - 50.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>web05</strong></td>
               <td>Website chăm sóc khách hàng dạng doanh nghiệp: automation chăm sóc khách hàng, quản lý chiến dịch, quản lý khách hàng, email marketing, sms marketing, sản phẩm:150, Bài viết:200, Đơn hàng tối đa: 500, Liên hệ tối đa: 500.</td>
               <td>3 ngày</td>
               <td></td>
               <td></td>
               <td>6.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>web06</strong></td>
               <td>Website chăm sóc khách hàng dạng doanh nghiệp: automation chăm sóc khách hàng, quản lý chiến dịch, quản lý khách hàng, email marketing, sms marketing, sản phẩm: 1000, Bài viết:2000, Đơn hàng tối đa: không giới hạn, Liên hệ tối đa: không giới hạn.</td>
               <td>1 năm</td>
               <td>3 ngày</td>
               <td></td>
               <td>12.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>theme01</strong></td>
               <td>website bán sẵn, lựa chọn theme tại vn3c.net</td>
               <td>1 lần</td>
               <td>2 ngày</td>
               <td></td>
               <td>1.500.000 đ</td>
            </tr>
            <tr>
               <td><strong>domain01</strong></td>
               <td>trỏ tên miền cấu hình tên miền trên website miễn phí</td>
               <td>1 lần</td>
               <td></td>
               <td></td>
               <td>200.000 đ</td>
            </tr>
         </tbody>
      </table>
      <h2>&nbsp;</h2>
      <h2>&nbsp;</h2>
      <h2>2. Bảng giá thiết kế và hỗ trợ website Moma</h2>
      <br>
      <table border="1" cellpadding="1" cellspacing="1" >
         <thead>
            <tr>
               <th scope="col">Tên gói</th>
               <th scope="col"style="width: 50%">Mô tả</th>
               <th scope="col">Thời gian sử dụng</th>
               <th scope="col">Thời gian hoàn thành</th>
               <th scope="col">Tặng kèm</th>
               <th scope="col">Giá thành</th>
            </tr>
            <tr>
               <td><strong>Logo</strong></td>
               <td>
                  Thiết kế logo mới theo yêu cầu
               </td>
               <td>1 lần</td>
               <td></td>
               <td></td>
               <td>1.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>banner facebook</strong></td>
               <td>Thiết kế banner trên facebook theo yêu cầu</td>
               <td>1 lần</td>
               <td></td>
               <td></td>
               <td>800.000 đ</td>
            </tr>
            <tr>
               <td><strong>Ảnh bài viết facebook</strong></td>
               <td>Thiết kế hình ảnh bài viết quảng cáo facebook theo yêu cầu.</td>
               <td>5 cái ảnh</td>
               <td>liên hệ</td>
               <td></td>
               <td>2.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>Banner website</strong></td>
               <td>Thiết kế 1 banner trên website theo yêu cầu</td>
               <td>5 lần</td>
               <td>liên hệ</td>
               <td></td>
               <td>2.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>Trỏ tên miền</strong></td>
               <td> Thay đổi tên miền trên website miễn phí</td>
               <td>1 lần</td>
               <td>3 ngày</td>
               <td></td>
               <td>
                  200.000 đ
               </td>
            </tr>
         </thead>
      </table>
      <h2>&nbsp;</h2>
      <h2>&nbsp;</h2>
      <h2>3. Bảng giá marketing Moma</h2>
      <br>
      <table  border="1" cellpadding="1" cellspacing="1" >
         <thead>
            <tr>
               <th scope="col">Tên gói</th>
               <th scope="col"style="width: 50%">Mô tả</th>
               <th scope="col">Thời gian sử dụng</th>
               <th scope="col">Thời gian hoàn thành</th>
               <th scope="col">Tặng kèm</th>
               <th scope="col">Giá thành</th>
            </tr>
            <tr>
               <td><strong>Từ khóa google</strong></td>
               <td>
                  Phân tích ra 50 từ khóa của google với số lượng tìm kiếm là tối thiểu là 300 lượt search / tháng, mức độ cạnh tranh.
               </td>
               <td>1 lần</td>
               <td></td>
               <td></td>
               <td>500.000 đ</td>
            </tr>
            <tr>
               <td><strong>Cài đặt quảng cáo google</strong></td>
               <td>Lên 1 chiến dịch quảng cáo google cho khách hàng. yêu cầu: đã có từ khóa, và website đích. Nếu không có website đích có thể sử dụng website miễn phí của moma.</td>
               <td>1 lần</td>
               <td></td>
               <td></td>
               <td>500.000 đ</td>
            </tr>
            <tr>
               <td><strong>Theo dõi quảng cáo google</strong></td>
               <td>Theo dõi hộ quảng cáo google: thứ hạng từ khóa trên top google, theo dõi tương tác, điều chỉnh mức tiền quảng cáo</td>
               <td>3 tháng</td>
               <td>3 tháng</td>
               <td></td>
               <td>6.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>Tìm kiếm target facebook</strong></td>
               <td>target đối tượng, tạo nhóm chiến dịch quảng cáo, test thử chiến dịch và đưa ra được nhóm đối tượng target hiệu quả với mỗi comment, inbox < 100k / 1 người khách hàng quan tâm</td>
               <td>1 lần</td>
               <td>2 tuần</td>
               <td></td>
               <td>10.000.000 đ</td>
            </tr>
            <tr>
               <td><strong>Theo dõi facebook ads</strong></td>
               <td>Theo dõi chiến dịch quảng cáo facebook. Khi mà vào thời điểm thuận lợi thì tăng tiền quảng cáo, không thuận lợi giảm tiền quảng cáo</td>
               <td>3 tháng</td>
               <td>3 tháng</td>
               <td></td>
               <td>
                  6.000.000 đ
               </td>
            </tr>
            <tr>
               <td><strong>Cài đặt chatbot</strong></td>
               <td>Cài đặt chatbot cho trang bán hàng của bạn trên facebook. Tự động chuyển comment của khách hàng thành inbox trả lời tự động cho khách hàng ngay khi khách comment (tránh bị lỡ mất bất kỳ một lượt tương tác mới nào) Tạo kịch bản chào mừng: khi có khách bắt đầu cuộc trò chuyện, hệ thống sẽ tự động trả lời Gắn thẻ, phân loại khách hàng -->tạo kịch bản chăm sóc khách hàng tự động cho từng nhóm khách hàng (khách đã chốt đơn mua hàng, khách chưa mua hàng)</td>
               <td>1 lần</td>
               <td>2 tuần</td>
               <td></td>
               <td>2.000.000 đ</td>
            </tr>
         </thead>
      </table>
   </div>
   </div>
</div>
@endsection


