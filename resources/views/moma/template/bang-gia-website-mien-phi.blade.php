@extends('moma.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )


@section('content')
    <div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>{{ isset($post['title']) ?  $post['title'] : '' }}</h1>
                                <p><a href="/">Home </a> > {{ isset($post['title']) ?  $post['title'] : '' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <section class="servicePriceList">
        <div class="container">
            <div class="fw7 textUpper textCenter blueMoma f23 mgb20">BẢNG GIÁ WEBSITE MOMA</div>
            <div class="listService">
                <div class="table-responsive">
                    <table class="table table-bordered fw6">
                        <thead>
                        <tr class="">
                            <th scope="row" class="textCenter bdBlack white bgrBlueMoma f18 w25p"></th>
                            <th class="textCenter bdBlack white bgrBlueMoma f18 w25p">
                                <p class="white">GÓI WEB01</p>
                                <p class="white">MIỄN PHÍ</p>
                            </th>
                            <th class="textCenter bdBlack white bgrBlueMoma f18 w25p">
                                <p class="white">GÓI WEB02</p>
                                <p class="white">1.200.000 đ / NĂM</p>
                            </th>
                            <th class="textCenter bdBlack white bgrBlueMoma f18 w25p">
                                <p class="white">GÓI WEB03</p>
                                <p class="white">6.000.000 đ / NĂM</p>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row" class="bdBlack blue">Số lượng sản phẩm</th>
                            <td class="textCenter bdBlack">30</td>
                            <td class="textCenter bdBlack">150</td>
                            <td class="textCenter bdBlack">1.000</td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack bgrf5 blue">Số lượng tin tức</th>
                            <td class="textCenter bdBlack bgrf5">60</td>
                            <td class="textCenter bdBlack bgrf5">200</td>
                            <td class="textCenter bdBlack bgrf5">2.000</td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Số lượng đơn hàng</th>
                            <td class="textCenter bdBlack">30</td>
                            <td class="textCenter bdBlack">500</td>
                            <td class="textCenter bdBlack">Không giới hạn</td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack bgrf5 blue">Số liên hệ</th>
                            <td class="textCenter bdBlack bgrf5">30</td>
                            <td class="textCenter bdBlack bgrf5">500</td>
                            <td class="textCenter bdBlack bgrf5">Không giới hạn</td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack  blue"></th>
                            <td class="textCenter bdBlack "><a href="http://website-mienphi.com/trang/tao-moi-website" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                            <td class="textCenter bdBlack "><a href="http://website-mienphi.com/trang/tao-moi-website" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                            <td class="textCenter bdBlack "><a href="http://website-mienphi.com/trang/tao-moi-website" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="note">
                    <p class="fw7 f20 red textUpper">Dịch vụ khác</p>
                    <p class="mgb0 f18">Gói Website thiết kế mới hoàn toàn: <span class="red fw7"> Liên hệ</span></p>
                    <p class="mgb0 f18">Thay đổi tên miền trên website: <span class="red fw7"> 100.000 đ/lần</span></p>
                </div>
            </div>
        </div>
    </section>  -->
	<section class="productPriceList pd3p">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">BẢNG GIÁ WEBSITE MOMA</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10"><i class="fa fa-handshake-o orange f20" aria-hidden="true"></i></div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
    <div class="contentProductPriceList">
        <div class="row">
            <div class="col-md-4 col-12 sm-pdb20">
				<div class="pdb10 pdt10 textCenter">
					&nbsp;
				</div>
                <div class="packed bdLightGray textCenter">
                    <div class="bgrGray textCenter pdt10 pdb10 bdBottomGray">
                        <p class="black mgb0 f23 md-f18">GÓI MIỄN PHÍ</p>
                        <p class="black mgb0 f16 mgt10 orange">Miễn phí</p>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Đăng ký Website lên các công cụ tìm kiếm
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ mọi phiên bản màn hình <span class="red">SMARTPHONE</span> và <span class="red">MÁY TÍNH BẢNG</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Website 1 ngôn ngữ
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Khởi tạo, tích hợp <span class="red">Google Analytics</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        <span class="red">Chuẩn SEO</span>, đầy đủ các công cụ <span class="red">hỗ trợ SEO</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Tích hợp <span class="red">Fanpage</span>, <span class="red">Google map</span> trên website
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Quản lý khách hàng: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Nhắc việc tự động: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Quản lý đội kinh doanh: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Giao việc tự động: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        SMS marketing: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Email marketing: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Đo lường KPI: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ App: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Bộ tài liệu đào tạo: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ sử dụng: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng sản phẩm đăng tải: <span class="red">30 sản phẩm</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng tin tức đăng tải: <span class="red">60 tin</span>

                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng khách hàng: <span class="red">30 khách hàng</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Không được thay tên miền website.
                    </div>
                    <div class="bgrOrange pd10 fw7 mg10">
                        <a href="/trang/tao-moi-website" class="block white hvWhite">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12 sm-pdb20">
				<div class="pdb10 pdt10 bgrBlueHeader white textCenter">
					Gói tốt nhất dành cho bán hàng
				</div>
                <div class="packed bdLightGray textCenter">
                    <div class="bgrGray textCenter pdb10 pdt10 bdBottomGray">
                        <p class="black mgb0 f23 md-f18">GÓI DOANH NGHIỆP</p>
                        <p class="black mgb0 f16 mgt10 red">99.000 VNĐ/THÁNG</p>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Đăng ký Website lên các công cụ tìm kiếm
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ mọi phiên bản màn hình <span class="red">SMARTPHONE</span> và <span class="red">MÁY TÍNH BẢNG</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Website 1 ngôn ngữ
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Khởi tạo, tích hợp <span class="red">Google Analytics</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        <span class="red">Chuẩn SEO</span>, đầy đủ các công cụ <span class="red">hỗ trợ SEO</span>
                    </div>
                    <div class="bgrWhite pd10 pdb10 fw7">
                        Tích hợp <span class="red">Fanpage</span>, <span class="red">Google map</span> trên website
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Quản lý khách hàng: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Nhắc việc tự động: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Quản lý đội kinh doanh: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Giao việc tự động: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        SMS marketing: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Email marketing: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Đo lường KPI: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ App: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Bộ tài liệu đào tạo: <span class="gray">Không</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ sử dụng: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng sản phẩm đăng tải: <span class="red">Không giới hạn</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng tin tức đăng tải: <span class="red">Không giới hạn</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng khách hàng: <span class="red">Không giới hạn</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Thay tên miền website <span class="red">miễn phí</span>
                    </div>
                    <div class="bgrOrange pd10 fw7 mg10">
                        <a href="/trang/tao-moi-website" class="block white hvWhite">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
				<div class="pdb10 pdt10 textCenter">
					&nbsp;
				</div>	
				<div class="packed bdLightGray textCenter">
                    <div class="bgrGray textCenter pdt10 pdb10 bdBottomGray">
                        <p class="black mgb0 f23 md-f18 textUpper">gói thương mại điện tử</p>
                        <p class="black mgb0 f16 mgt10 red">500.000 VNĐ/THÁNG</p>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Đăng ký Website lên các công cụ tìm kiếm
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ mọi phiên bản màn hình <span class="red">SMARTPHONE</span> và <span class="red">MÁY TÍNH BẢNG</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Website 1 ngôn ngữ
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Khởi tạo, tích hợp <span class="red">Google Analytics</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        <span class="red">Chuẩn SEO</span>, đầy đủ các công cụ <span class="red">hỗ trợ SEO</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Tích hợp <span class="red">Fanpage</span>, <span class="red">Google map</span> trên website
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Quản lý khách hàng: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Nhắc việc tự động: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Quản lý đội kinh doanh: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Giao việc tự động: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        SMS marketing: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Email marketing: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Đo lường KPI: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ App: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Bộ tài liệu đào tạo: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Hỗ trợ sử dụng: <span class="red">Có</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng sản phẩm đăng tải: <span class="red">Không giới hạn</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng tin tức đăng tải: <span class="red">Không giới hạn</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Số lượng khách hàng: <span class="red">Không giới hạn</span>
                    </div>
                    <div class="bgrWhite pd10 fw7">
                        Thay tên miền website: <span class="red">Miễn phí</span>
                    </div>
                    <div class="bgrOrange pd10 fw7">
                        <a href="/trang/tao-moi-website" class="block white hvWhite ">ĐĂNG KÝ NGAY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


