@extends('moma.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )


@section('content')
    <div class="page-header" id="home" style="background-image: url('{{ isset($information['page-header']) ?  $information['page-header'] : '' }}');">
        <div class="header-caption">
            <div class="header-caption-contant">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="header-caption-inner">
                                <h1>{{ isset($post['title']) ?  $post['title'] : '' }}</h1>
                                <p><a href="/">Home </a> > {{ isset($post['title']) ?  $post['title'] : '' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 50px"></div>
    <section class="marketingMoma">
        <div class="container">
            <p class="f23 fw7 textUpper textCenter mgb20 blueMoma">BẢNG GIÁ GÓI MARKETING MOMA</p>
            <div class="table-responsive">
                <table class="table table-bordered fw6">
                    <thead>
                    <tr class="">
                        <th scope="row" class="textCenter bdBlack white bgrBlueMoma f18 w25p"></th>
                        <th class="textCenter bdBlack white bgrBlueMoma f18 w25p">
                            <p>GÓI MOMA1</p>
                            <p>4.000.000 đ / THÁNG</p>
                        </th>
                        <th class="textCenter bdBlack white bgrBlueMoma f18 w25p">
                            <p>GÓI MOMA2</p>
                            <p>6.000.000 đ /THÁNG</p>
                        </th>
                        <th class="textCenter bdBlack white bgrBlueMoma f18 w25p">
                            <p>GÓI MOMA3</p>
                            <p>8.000.000 đ / THÁNG</p>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" class="bdBlack blue">Tạo website miễn phí</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr class="bgrf5">
                            <th scope="row" class="bdBlack blue">Tạo Fanpage</th>
                            <td class="textCenter bdBlack red"><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Tạo Sitemap.xml</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr class="bgrf5">
                            <th scope="row" class="bdBlack blue">Tạo Robot.txt</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Cài đặt Facebook Fixel</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr class="bgrf5">
                            <th scope="row" class="bdBlack blue">Remarketing Google</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Cài đặt Chatbot</th>
                            <td class="textCenter bdBlack red"><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr class="bgrf5">
                            <th scope="row" class="bdBlack blue">Cài đặt Automation CSKH bằng Email</th>
                            <td class="textCenter bdBlack red"><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Bài viết chuẩn SEO</th>
                            <td class="textCenter bdBlack">10</td>
                            <td class="textCenter bdBlack">20</td>
                            <td class="textCenter bdBlack">30</td>
                        </tr>
                        <tr class="bgrf5">
                            <th scope="row" class="bdBlack bgrf5 blue">Bài phát triển Fanpage</th>
                            <td class="textCenter bdBlack bgrf5">không</td>
                            <td class="textCenter bdBlack bgrf5">10</td>
                            <td class="textCenter bdBlack bgrf5">20</td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Cài đặt Google Analynic</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Quảng cáo google</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Quảng cáo remarketing google</th>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Quảng cáo facebook</th>
                            <td class="textCenter bdBlack red"><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack red"><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue">Quảng cáo remarketing face</th>
                            <td class="textCenter bdBlack red"><i class="fa fa-times" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                            <td class="textCenter bdBlack green"><i class="fa fa-check" aria-hidden="true"></i></td>
                        </tr>
                        <tr class="bgrf5">
                            <th scope="row" class="bdBlack blue middle">Tặng kèm</th>
                            <td class="textCenter bdBlack middle">1 năm sử dụng website</td>
                            <td class="textCenter bdBlack middle">
                                <p class="mgb0">1 năm sử dụng website</p>
                                <p class="mgb0">1 bản getfly CRM cá nhân (nếu ký hợp đồng 1 năm)</p>
                            </td>
                            <td class="textCenter bdBlack middle">
                                <p class="mgb0">1 năm sử dụng website</p>
                                <p class="mgb0">1 bản getfly CRM cá nhân (nếu ký hợp đồng 1 năm)</p>
                                <p class="mgb0">Chatbot ManyChat</p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="bdBlack blue"></th>
                            <td class="textCenter bdBlack "><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                            <td class="textCenter bdBlack "><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                            <td class="textCenter bdBlack "><a href="" class="bgrBlueMoma pd10 radius5 white hvWhite block">Đăng ký</a></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <div style="height: 50px"></div>
@endsection


