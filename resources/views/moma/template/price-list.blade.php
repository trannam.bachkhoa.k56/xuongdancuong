@extends('moma.layout.site')

@section('title', 'Bảng giá website')
@section('content')
  
	<section class="productPriceList pd3p">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">BẢNG GIÁ WEBSITE MOMA</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10"><i class="fa fa-handshake-o orange f20" aria-hidden="true"></i></div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
  
    <section class="content">
        <div class="row">
            <!-- form start -->
                <div class="col-xs-12 col-md-12">
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <!-- /.box-header -->
                            <div class="col-xs-12 col-md-3 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-blue">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 1</b></h3>
                                        <p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
                                        <h4 class="center"><b>2.400.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Không</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-3 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-yellow">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 2</b></h3>
                                        <p class="center"><i>(Mua 2 năm tặng 1 năm)</i></p>
                                        <h4 class="center"><b>4.800.000 VNĐ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">3 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-3 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-green">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 3</b></h3>
                                        <p class="center"><i>(Mua 3 năm tặng 2 năm)</i></p>
                                        <h4 class="center"><b>7.200.000 VNĐ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">5 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip4" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-3 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-red">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-diamond" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 4</b></h3>
                                        <p class="center"><i>(Dành cho doanh nghiệp cỡ lớn)</i></p>
                                        <h4 class="center"><b>Liên hệ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
                                            <li><button style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#"><b>Liên hệ: 097 456 1735 </b></button></li>
                                     <!--       <li><button data-toggle="modal" data-target="#modal-vip3" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>   -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-header -->
                    </div>
                    <!-- /.box -->
                    <!-- Nội dung thêm mới -->
                </div>
        </div>

        <div class="modal fade" id="modal-vip1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 1</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
                            <input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2400000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-vip2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 2</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="36" value="36" required>
                            </div>
                            <input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="4800000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-vip4">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 3</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="60" value="60" required>
                            </div>
                            <input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="7200000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-vip3">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 4</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="6000000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    
    <section class="content-header">
        <h1>
            GIA HẠN THỜI GIAN SỬ DỤNG GÓI EMAIL MARKETING
        </h1>
    </section>
        <section class="content">
        <div class="row">
            <!-- form start -->
                <div class="col-xs-12 col-md-12">
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <!-- /.box-header -->
                            <div class="col-xs-12 col-md-6 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-blue">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI DOANH NGHIỆP VỪA VÀ NHỎ </b></h3>
                                        <h4 class="center"><b>2.400.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Số lượng mail/tháng: <span class="pull-right badge bg-red">5.000</span></a></li>
                                            <li><a href="#">Đặt lịch gửi mail chăm sóc khách hàng</a></li>
                                            <li><a href="#">Gửi mail tới khách hàng</a></li>
                                            <li><button data-toggle="modal" data-target="#modal-email1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6 mgt10">
                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-yellow">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI DOANH NGHIỆP LỚN</b></h3>
                                        <h4 class="center"><b>6.000.000 VNĐ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Số lượng mail/tháng: <span class="pull-right badge bg-red">5.000</span></a></li>
                                            <li><a href="#">Đặt lịch gửi mail chăm sóc khách hàng</a></li>
                                            <li><a href="#">Gửi mail tới khách hàng</a></li>
                                            <li><button data-toggle="modal" data-target="#modal-email2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        <!-- /.box-header -->
                    </div>
                    <!-- /.box -->
                    <!-- Nội dung thêm mới -->
                </div>
        </div>

        <div class="modal fade" id="modal-email1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
                            <input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2400000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-email2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="36" value="36" required>
                            </div>
                            <input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="6000000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
</section>
@endsection


