@extends('news-01.layout.site')
@section('content')

    <section class="textBox">
        <div class="mask"></div>
        <div class="container">
            <div class="infoText">
                <div class="cont">
                    <h2 class="title"><strong>{{ isset($post->title) ? $post->title : "" }}</strong><br>{{ isset($post->description) ? $post->description : "" }}<span></span></h2>
                    <div class="ContentNews">
                        {!! isset($post->content) ? $post->content : "" !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection