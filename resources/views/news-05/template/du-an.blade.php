@extends('news-05.layout.site')
@section('content')

    <section class="viewProject wow fadeInUp" data-wow-offset="300">
        <div class="mask"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="infoProject">
                        <div class="cont">
                            <h2><span>{{ isset($post->title) ? $post->title : "" }}</span></h2>
                            <div class="contentNews">
                                <div class="tc mb20 video"><img src="thumbs/video1.png"/></div>
                                {!! $post->content !!}
                            </div>
                        </div>
                    </div>
                    <div class="relateProject">
                        <h2><span>Dịch vụ tiện ích</span></h2>
                        <div class="row">
                            @foreach (\App\Entity\Product::showProduct('dich-vu-tien-ich', 5) as $id => $product)
                            <div class="itemm">
                                <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                                    <img src="{{ $product->image }}"/>
                                    <h3>{{ $product->title }}</h3>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection