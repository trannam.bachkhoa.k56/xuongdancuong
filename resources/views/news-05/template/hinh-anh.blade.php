@extends('news-05.layout.site')

@section('content')
     <!-- #Header_bg -->
         <div id="Header_wrapper"  style="" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
            <header id="Header">
                <!-- HEADER -->
                @include('news-05.common.header')
                @include('news-05.partials.slider')
            </header>
         </div>
         <!-- mfn_hook_content_before --><!-- mfn_hook_content_before -->  
         <!-- #Content -->
         <div id="Content">
            <div class="content_wrapper clearfix">
               <!-- .sections_group -->
               <div class="sections_group">
                  <div class="entry-content" itemprop="mainContentOfPage">
                     
                     <div class="section mcb-section full-width  "  style="padding-top:60px; padding-bottom:30px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <div class="animate" data-anim-type="fadeIn">
                                          <h2 class="title"><i class="icon-right-dir"></i>Hình ảnh<i class="icon-left-dir"></i></h2>
                                       </div>
                                    </div>
                                 </div>
								 @foreach(\App\Entity\Post::categoryShow('du-an',20) as $id => $intro)


                                <div class="column mcb-column one-fourth column_story_box ">
                                    <div class="story_box ">
                                       <a href="{{ isset($intro['image']) ? asset($intro['image']) : ''}}" rel="prettyphoto">
                                          <div class="photo_wrapper"><img class="scale-with-grid" src="{{ isset($intro['image']) ? asset($intro['image']) : ''}}" alt="{{ isset($intro['title']) ?$intro['title'] : ''}}" width="800" height="600"/>
                                          </div>
                                          </a>
                                           
                                          </div>
                                      
                                    </div>
								 @endforeach 
                                 </div>


                              
                              

                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- DOI TAC -->
                     @include('news-05.partials.doitac')
                     <div class="section the_content no_content">
                        <div class="section_wrapper">
                           <div class="the_content_wrapper"></div>
                        </div>
                     </div>
                     <div class="section section-page-footer">
                        <div class="section_wrapper clearfix">
                           <div class="column one page-pager">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
            </div>
         </div>
         <!-- mfn_hook_content_after --><!-- mfn_hook_content_after -->
   
@endsection

