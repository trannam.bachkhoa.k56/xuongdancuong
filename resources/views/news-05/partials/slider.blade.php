 <div class="row">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
     
     <div class="carousel-inner">
      @foreach(\App\Entity\SubPost::showSubPost('slider', 8) as $id => $slide)
         <div class="carousel-item {{ $id == 0 ?'active' : ''}}">
           <img src="{{ $slide->image }}" alt="{{ $slide->title }}">
           <div class="carousel-caption d-none d-md-block">
           <!--   <h5>...</h5>
             <p>...</p> -->
           </div>
         </div>
      @endforeach   
     </div>
     <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
       <span class="carousel-control-prev-icon" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
       <span class="carousel-control-next-icon" aria-hidden="true"></span>
       <span class="sr-only">Next</span>
     </a>
   </div>
</div>