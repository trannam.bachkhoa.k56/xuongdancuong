@extends('news-05.layout.site')

@section('title', isset($post->title) ? $post->title : ''  )
@section('meta_description',  isset($post->description) ? $post->description : '')
@section('keywords', '' )
@section('content')
<div id="Header_wrapper"  style="background-image:url({{ isset($information['backgruod-menu-header']) ? asset($information['backgruod-menu-header']) : '' }});" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
            <style type="text/css">
            #Header {
               min-height: 200px !important;
            }
            </style>
    <header id="Header">
        <!-- HEADER -->
        @include('news-05.common.header')
      
        
    </header>
       
</div>
<div id="Content" class="container">
            <style type="text/css">
               @media only screen and (max-width: 767px)
               {
               #Content .section_wrapper, .container, .four.columns .widget-area {
                   max-width: 100% !important;
               }
            </style>
            <div class="content_wrapper row clearfix ">
               <!-- .sections_group -->
               <div class="sections_group col-lg-9 col-md-8 col-sm-12 col-12">
                  <div id="post-3899" class="no-share post-3899 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-chuyen-nganh category-tin-tuc">
                     
                     <div class="section section-post-header">
                        <div class="section_wrapper clearfix">
                           
                           <div class="column one post-header">
                             
                              <div class="title_wrapper" style="margin-left: 0">
                                 <h1 class="entry-title" itemprop="headline" style="margin:15px 0">{{ isset($post->title) ? $post->title : ''}}</h1>
                                 <div class="post-meta clearfix">
                                    <div class="author-date">
                                       <span class="date">
                                          {{ isset($post->updated_at) ? $post->updated_at : ''}}
                                       </span>  
                                    </div>
                                   
                                 
                                 </div>
                              </div>
                           </div>

                           <div class="column one single-photo-wrapper image">
                              <div class="image_frame scale-with-grid ">
                                 <div class="image_wrapper">
                                    <a href="{{ isset($post->image) ? asset($post->image) : ''}}" rel="prettyphoto">
                                       <div class="mask"></div>
                                       <img width="600" height="400" src="{{ isset($post->image) ? asset($post->image) : ''}}" class="scale-with-grid wp-post-image" alt="" itemprop="image" srcset="{{ isset($post->image) ? asset($post->image) : ''}} 600w, {{ isset($post->image) ? asset($post->image) : ''}} 768w, {{ isset($post->image) ? asset($post->image) : ''}} 219w, {{ isset($post->image) ? asset($post->image) : ''}} 50w, {{ isset($post->image) ? asset($post->image) : ''}} 113w, {{ isset($post->image) ? asset($post->image) : ''}} 900w" sizes="(max-width: 600px) 100vw, 600px" />
                                    </a>
                                    <div class="image_links"><a href="{{ isset($post->image) ? asset($post->image) : ''}}" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a></div>
                                 </div>
                              </div>
                           </div>

                        </div>
                     </div>
                     <div class="post-wrapper-content">
                        <div class="section the_content has_content">
                           <div class="section_wrapper">
                              <div class="the_content_wrapper">
                                 <!-- CONTENT -->
                                  {!! isset($post->content) ? $post->content : '' !!}
                              </div>
                           </div>
                        </div>
                        <div class="section section-post-footer">
                           <div class="section_wrapper clearfix">
                              <div class="column one post-pager">
                              </div>
                           </div>
                        </div>
                        <div class="section section-post-about">
                           <div class="section_wrapper clearfix">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
               <div class="sidebar sidebar-1 col-lg-3 col-md-4 col-sm-12 col-12">
                  <div class="widget-area clearfix ">
                     
                     <aside id="widget_mfn_recent_posts-3" class="widget widget_mfn_recent_posts">
                        <h3 style="margin-left:15px">TIN MỚI NHẤT</h3>
                        <div class="Recent_posts">
                           <ul>
                              @foreach(\App\Entity\Post::categoryShow('tin-moi-nhat',8) as $introduction)
                              <li class="post format-">
                                 <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introduction->slug]) }}">
                                    <div class="photo"><img width="80" height="80" src="{{ isset($introduction->image) ? asset($introduction->image) : ''}}" class="scale-with-grid wp-post-image" alt="" srcset="{{ isset($introduction->image) ? asset($introduction->image) : ''}} 80w, {{ isset($introduction->image) ? asset($introduction->image) : ''}} 85w" sizes="(max-width: 80px) 100vw, 80px" /></div>
                                    <div class="desc">
                                       <h6>{{ isset($introduction->title) ? $introduction->title : ''}}</h6>
                                       <span class="date"><i class="icon-clock"></i>   <?php 
                                          $date=date_create($introduction->updated_at);
                                          echo date_format($date,"Y/m/d");

                                          ?>  
                                       </span>
                                    </div>
                                 </a>
                              </li>
                             @endforeach
                           </ul>
                        </div>
                     </aside>
                  </div>
               </div>
            </div>
         </div>         
@endsection
