@extends('news-05.layout.site')


@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title)
@section('meta_description',  !empty($category->meta_description) ? $category->meta_description : $category->description)
@section('keywords', $category->meta_keyword )
@section('content')
<div id="Header_wrapper"  style="background-image:url({{ isset($information['backgruod-menu-header']) ? asset($information['backgruod-menu-header']) : '' }});" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
    <header id="Header">
        <!-- HEADER -->
        @include('news-05.common.header')
        @include('news-05.partials.slider')
        
    </header>
     <div id="Content">
            <div class="content_wrapper clearfix">
               <!-- .sections_group -->
               <div class="sections_group">
                  <div class="extra_content">
                  </div>
                  <div class="section ">
                     <div class="section_wrapper clearfix">
                        <div class="column one column_blog">
                           <div class="blog_wrapper isotope_wrapper">

                            <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <div class="animate zoomIn" data-anim-type="zoomIn">
                                          <h1 class="title" style="margin:15px 0"><i class="icon-right-dir"></i>{{ !empty($category->meta_title) ? $category->meta_title : $category->title }}<i class="icon-left-dir"></i></h1>
                                       </div>
                                    </div>
                                 </div>
								<?php 
								/*print_r($posts)*/
								// echo count($posts)
								?>
								
                              @foreach($posts as $post)
                              <div class="posts_group lm_wrapper classic col-12">
                                 <div class="post-item isotope-item clearfix author-webadmin post-3899 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-chuyen-nganh category-tin-tuc" >
                                 
                                    <div class="image_frame post-photo-wrapper scale-with-grid image">
                                       <div class="image_wrapper CropImg">
									   <div class="thumbs">
									   
                                          <a class="" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">
                                             <div class="mask"></div>
                                             <img width="600" height="400" src="{{ isset($post->image) ? asset($post->image) : ''}}" class="scale-with-grid wp-post-image" alt="" itemprop="image" srcset="{{ isset($post->image) ? asset($post->image) : ''}} 600w, {{ isset($post->image) ? asset($post->image) : ''}} 768w, {{ isset($post->image) ? asset($post->image) : ''}} 219w, {{ isset($post->image) ? asset($post->image) : ''}} 50w, {{ isset($post->image) ? asset($post->image) : ''}} 113w, {{ isset($post->image) ? asset($post->image) : ''}} 900w" sizes="(max-width: 600px) 100vw, 600px" />
                                          </a>
										  </div>
										  
                                          <div class="image_links double"><a href="{{ isset($post->image) ? asset($post->image) : ''}}" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="link"><i class="icon-link"></i></a></div>
                                       </div>
                                    </div>
                                    <div class="post-desc-wrapper">
                                       <div class="post-desc">
                                         
                                          <div class="post-title">
                                             <h2 class="entry-title" itemprop="headline"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">{{
                                              isset($post->title) ? $post->title : ''
                                             }}</a></h2>
                                          </div>
                                          <div class="post-excerpt">{{
                                              isset($post->description) ? $post->description : 'Đang cập nhật'
                                             }}</div>
                                          <div class="post-footer">
                                             

                                             <div class="post-links"><i class="icon-doc-text"></i> <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="post-more">Xem thêm</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              @endforeach


                              <div class="column one pager_wrapper">
                                 <div class="pager">
                                    <div class="pagination-ct">
                                      {{ $posts->links() }}
                                  </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
            </div>
         </div>

</div>
         
@endsection
