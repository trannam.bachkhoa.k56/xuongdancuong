@extends('news-05.layout.site')


@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('content')
<div id="Header_wrapper"  style="background-image:url({{ isset($information['backgruod-menu-header']) ? asset($information['backgruod-menu-header']) : '' }});" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
            <style type="text/css">
            #Header {
               min-height: 200px !important;
            }
            </style>
    <header id="Header">
        <!-- HEADER -->
        @include('news-05.common.header')
    </header>
       
</div>
<div id="Content" class="container">
            <style type="text/css">
               @media only screen and (max-width: 767px)
               {
               #Content .section_wrapper, .container, .four.columns .widget-area {
                   max-width: 100% !important;
               }
            </style>
			<div class="row viewPro">
				
			</div>
            <div class="content_wrapper row clearfix mgbottom30">
               <!-- .sections_group -->
               <div class="sections_group col-lg-9 col-md-8 col-sm-12 col-12">
			   <div class="row">
					<div class="col-md-6" style="margin-bottom:20px;">
					<img src="{{ isset($product->image) ? $product->image : "" }}"/>
					</div>
					<div class="col-md-6">
						<div class="column one post-header">
						  <div class="title_wrapper" style="margin-left: 0">
							 <h1 class="entry-title" itemprop="headline" style="margin:15px 0">{{ isset($product->title) ? $product->title : "" }}</h1>
							 <div class="post-meta clearfix">
								<div class="author-date">
								   <span class="date">
									 <?php $date=date_create($product->created_at); ?>
								   </span>  
								</div>
							   
							 {{ isset($product->description) ? $product->description : "" }}
							 </div>
						  </div>
					   </div>
					</div>
			   </div>
                  <div id="post-3899" class="no-share post-3899 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-chuyen-nganh category-tin-tuc">
                     
                     <div class="section section-post-header">
                        <div class="section_wrapper clearfix">
						{{--
                           <div class="column one single-photo-wrapper image">
                              <div class="image_frame scale-with-grid ">
                                 <div class="image_wrapper">
                                    <a href="{{ isset($product->image) ? asset($product->image) : ''}}" rel="prettyphoto">
                                       <div class="mask"></div>
                                       <img width="600" height="400" src="{{ isset($product->image) ? asset($product->image) : ''}}" class="scale-with-grid wp-post-image" alt="" itemprop="image" srcset="{{ isset($product->image) ? asset($product->image) : ''}} 600w, {{ isset($product->image) ? asset($product->image) : ''}} 768w, {{ isset($product->image) ? asset($product->image) : ''}} 219w, {{ isset($product->image) ? asset($product->image) : ''}} 50w, {{ isset($product->image) ? asset($product->image) : ''}} 113w, {{ isset($product->image) ? asset($product->image) : ''}} 900w" sizes="(max-width: 600px) 100vw, 600px" />
                                    </a>
                                    <div class="image_links"><a href="{{ isset($product->image) ? asset($product->image) : ''}}" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a></div>
                                 </div>
                              </div>
                           </div>
							--}}

                        </div>
                     </div>
                     <div class="post-wrapper-content">
                        <div class="section the_content has_content">
                           <div class="section_wrapper">
                              <div class="the_content_wrapper">
                                 <!-- CONTENT -->
                                  {!! isset($product->content) ? $product->content : "" !!}
                              </div>
                           </div>
                        </div>
						<div class="Tags">
							<i class="fa fa-tag" aria-hidden="true"></i>Tags
							<?php $tags = explode(',', $product->tags)?>
							@foreach($tags as $tag)
								<a href="">{!! $tag !!}</a>
							@endforeach
						</div>
                        <div class="section section-post-footer">
                           <div class="section_wrapper clearfix">
                              <div class="column one post-pager">
                              </div>
                           </div>
                        </div>
                        <div class="section section-post-about">
                           <div class="section_wrapper clearfix">
                           </div>
                        </div>
                     </div>
					 
					 
					 <div class="section section-post-related">
                        <div class="section_wrapper clearfix">
                           <div class="section-related-adjustment ">
                              <h4 style="margin-left: 15px">Sản phẩm cùng chuyên mục</h4>
                              <div class="section-related-ul col-12">
								@foreach(\App\Entity\Product::relativeProduct($product->slug, $product->product_id, 6) as $id => $postRelative)
                                 
                                 <div class="column post-related post-3892 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-chuyen-nganh category-tin-hoat-dong-lien-quan category-tin-tuc" style="height: 350px">
                                    <div class="image_frame scale-with-grid">
                                       <div class="image_wrapper">
                                          <a href="{{ route('product',['cate_slug' => $postRelative->slug]) }}">
                                             <div class="mask"></div>
                                             <img width="600" height="400" src="{{ isset($postRelative->image) ? asset($postRelative->image) : ''}}" class="scale-with-grid wp-post-image" alt="" itemprop="image" srcset="{{ isset($postRelative->image) ? asset($postRelative->image) : ''}} 600w, {{ isset($postRelative->image) ? asset($postRelative->image) : ''}} 768w, {{ isset($postRelative->image) ? asset($postRelative->image) : ''}} 219w, {{ isset($postRelative->image) ? asset($postRelative->image) : ''}} 50w, {{ isset($postRelative->image) ? asset($postRelative->image) : ''}} 113w, {{ isset($postRelative->image) ? asset($postRelative->image) : ''}} 900w" sizes="(max-width: 600px) 100vw, 600px"  style="width: 230px;height: 140px;"/>
                                          </a>
                                          <div class="image_links double"><a href="{{ isset($postRelative->image) ? asset($postRelative->image) : ''}}" class="zoom" rel="prettyphoto"><i class="icon-search"></i></a><a href="{{ route('product',['cate_slug' => $postRelative->slug]) }}" class="link"><i class="icon-link"></i></a></div>
                                       </div>
                                    </div>
                                   
                                    <div class="desc" style="padding-left: 30px">
                                       <h4><a href="{{ route('product',['cate_slug' => $postRelative->slug]) }}">{{ isset($postRelative->title) ? $postRelative->title : ''}}</a></h4>
                                       <hr class="hr_color" />
                                       <a href="{{ route('product',['cate_slug' => $postRelative->slug]) }}" class="button button_left button_js"><span class="button_icon"><i class="icon-layout"></i></span><span class="button_label"> xem thêm</span></a>
                                    </div>
                                 </div>
                                 @endforeach
                              </div>
                           </div>
                        </div>
                     </div>
					 
                 
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
			   <div class="sidebar sidebar-1 col-lg-3 col-md-4 col-sm-12 col-12">
                  <div class="widget-area clearfix ">
                     
                     <aside id="widget_mfn_recent_posts-3" class="widget widget_mfn_recent_posts">
                        <h3 style="margin-left:15px">TIN MỚI NHẤT</h3>
                        <div class="Recent_posts">
                           <ul>
								
                              @foreach(\App\Entity\Post::categoryShow('tin-moi-nhat',8) as $introduction)
                              <li class="post format-">
                                 <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introduction->slug]) }}">
                                    <div class="photo"><img width="80" height="80" src="{{ isset($introduction->image) ? asset($introduction->image) : ''}}" class="scale-with-grid wp-post-image" alt="" srcset="{{ isset($introduction->image) ? asset($introduction->image) : ''}} 80w, {{ isset($introduction->image) ? asset($introduction->image) : ''}} 85w" sizes="(max-width: 80px) 100vw, 80px" /></div>
                                    <div class="desc">
                                       <h6>{{ isset($introduction->title) ? $introduction->title : ''}}</h6>
                                       <span class="date"><i class="icon-clock"></i>   <?php 
                                          $date=date_create($introduction->updated_at);
                                          echo date_format($date,"Y/m/d");

                                          ?>  
                                       </span>
                                    </div>
                                 </a>
                              </li>
                             @endforeach
                           </ul>
                        </div>
                     </aside>
                     <aside id="text-6" class="widget widget_text">
                        <h3>LIÊN KẾT NHANH</h3>
                        <div class="textwidget">
                           <ul class="footer_links">
                              @foreach(\App\Entity\Post::categoryShow('tin-tuc',8) as $intronew)
                              <li><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $intronew->slug]) }}">{{ isset($intronew->title) ? $intronew->title : ''}}</a></li>
                              @endforeach
                             
                           </ul>
                        </div>
                     </aside>
                  </div>
               </div>
			   
            
			</div>
         </div>  
@endsection
