
@extends('news-05.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : "")
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : "")
    
@section('content')
     <!-- #Header_bg -->
         <div id="Header_wrapper"  style="" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
            <header id="Header">
                <!-- HEADER -->
                @include('news-05.common.header')
                @include('news-05.partials.slider')
            </header>
         </div>
         <!-- mfn_hook_content_before --><!-- mfn_hook_content_before -->  
         <!-- #Content -->
         <div id="Content">
            <div class="content_wrapper clearfix">
               <!-- .sections_group -->
               <div class="sections_group">
                  <div class="entry-content" itemprop="mainContentOfPage">
                     <div class="section mcb-section bgsologan "  style="padding-top:20px; padding-bottom:20px" >
                        <div class="section-divider triangle down"></div>
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <div class="animate" data-anim-type="zoomIn">
                                          <h2 class="title"><i class="icon-right-dir"></i> {{ isset($information['tieu-de-sologan']) ? $information['tieu-de-sologan']: '' }}<i class="icon-left-dir"></i></h2>
                                          <div class="inside">{!! isset($information['   mo-ta-sologan']) ? $information['   mo-ta-sologan']: '' !!}</div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section mcb-section full-width "  style="padding-top:50px; padding-bottom:0px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  column-margin-10px valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <div class="animate" data-anim-type="zoomIn">
                                          <h2 class="title"><i class="icon-right-dir"></i>SẢN PHẨM CHÍNH<i class="icon-left-dir"></i></h2>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                            <div class="mcb-wrap-inner">
                                @foreach (\App\Entity\Menu::showWithLocation('show-category-index') as $Mainmenu_index)
                                    @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu_index->slug) as $id=>$menu_category)
                                    <div class="column mcb-column one-fourth column_flat_box ">
                                        <div class="flat_box">
                                           <div class="animate" data-anim-type="fadeIn">
                                              <a href="{{ $menu_category['url'] }}" >
                                                 <div class="photo_wrapper">
                                                    <div class="icon themebg" ><i class="icon-doc-text"></i></div>
                                                    <img class="scale-with-grid" src="{{ $menu_category['image'] }}" alt="" width="" height=""/>
                                                 </div>
                                                 <div class="desc_wrapper" >
                                                    <h4>{{ $menu_category['title_show'] }}</h4>
                                                 </div>
                                              </a>
                                           </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endforeach
                            </div>
                           </div>
                        </div>
                     </div>
                     <div class="section mcb-section dark  "  style="padding-top:30px; padding-bottom:10px; background-color:" data-parallax="3d">
                        <img class="mfn-parallax" src="{{ isset($information['backgruod-menu-header']) ? asset($information['backgruod-menu-header']) : '' }}" alt=""/>
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <div class="animate" data-anim-type="fadeIn">
                                          <h2 class="title" style="color: #fff !important;"><i class="icon-right-dir"></i>{{ isset($information['tieu-de-sologan2']) ? $information['tieu-de-sologan2'] :'' }}<i class="icon-left-dir"></i></h2>
                                          <div class="inside">
                                             <h4>{!! isset($information['mo-ta-sologan2']) ? $information['mo-ta-sologan2'] :'' !!}</h4>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section mcb-section full-width  "  style="padding-top:60px; padding-bottom:30px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <div class="animate" data-anim-type="fadeIn">
                                          <h2 class="title"><i class="icon-right-dir"></i>DỰ ÁN ĐÃ/ĐANG TRIỂN KHAI<i class="icon-left-dir"></i></h2>
                                       </div>
                                    </div>
                                 </div>

                                @foreach(\App\Entity\Post::categoryShow('du-an',4) as $id=>$intro)


                                <div class="column mcb-column one-fourth column_story_box ">
                                    <div class="story_box">
                                       <a href="{{ isset($intro['image']) ? asset($intro['image']) : ''}}" rel="prettyphoto">
                                          <div class="CropImg mgbottom10" class="photo_wrapper"><div class="thumbs"><img class="scale-with-grid" src="{{ isset($intro['image']) ? asset($intro['image']) : ''}}" alt="{{ isset($intro['title']) ?$intro['title'] : ''}}" width="800" height="600"/></div></div>
                                          <div class="desc_wrapper" style="margin-right: 0">
                                             <div class="desc">{{ isset($intro['title']) ?$intro['title'] : ''}}</div>
                                          </div>
                                       </a>
                                    </div>
                                 </div>


                                <!--  <div class="column mcb-column one-fourth column_story_box ">
                                    <div class="story_box ">
                                       <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $intro->slug]) }}" rel="prettyphoto">
                                          <div class="photo_wrapper"><img class="" src="{{ isset($intro['image']) ? asset($intro['image']) : ''}}" alt="{ isset($intro['title']) ? $intro['title'] : ''}}" width="800" height="600"/></div>
                                          <div class="desc_wrapper" style="margin-right: 0">
                                             <div class="desc">{{ isset($intro['title']) ?$intro['title'] : ''}}</div>
                                          </div>
                                       </a>
                                    </div>
                                 </div> -->
                                @endforeach

                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- DOI TAC -->
                     @include('news-05.partials.doitac')
                     <div class="section the_content no_content">
                        <div class="section_wrapper">
                           <div class="the_content_wrapper"></div>
                        </div>
                     </div>
                     <div class="section section-page-footer">
                        <div class="section_wrapper clearfix">
                           <div class="column one page-pager">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
            </div>
         </div>
         <!-- mfn_hook_content_after --><!-- mfn_hook_content_after -->
   
@endsection


