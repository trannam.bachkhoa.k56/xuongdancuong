@extends('news-05.layout.site')


@section('title', 'liên hệ')
@section('meta_description',  isset($information['mo-ta-lien-he']) ? $information['mo-ta-lien-he'] : '' )
@section('keywords', '' )
@section('content')
<div id="Header_wrapper"  style="background-image:url({{ isset($information['backgruod-menu-header']) ? asset($information['backgruod-menu-header']) : '' }});" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
            <style type="text/css">
            #Header {
               min-height: 200px !important;
            }
            </style>
    <header id="Header">
        <!-- HEADER -->
        @include('news-05.common.header')
      
        
    </header>
     <div id="Content">
            <div class="content_wrapper clearfix">
               <!-- .sections_group -->
               <div class="sections_group">
                  <div class="entry-content" itemprop="mainContentOfPage">
                     <div class="section mcb-section   "  style="padding-top:0px; padding-bottom:0px; background-color:" >
 {{-- print_r($post) --}}
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <h2 class="title" style="margin: 15px 0"><i class="icon-right-dir"></i>LIÊN HỆ<i class="icon-left-dir"></i></h2>
                                       <div class="inside">
                                        

                                          <h3> {!! isset($information['mo-ta-lien-he']) ? $information['mo-ta-lien-he'] : '' !!}</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section mcb-section   "  style="padding-top:0px; padding-bottom:0px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column column_visual ">
                                    {!! isset($information['lien-he']) ? $information['lien-he'] : '' !!}
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section mcb-section   "  style="padding-top:0px; padding-bottom:0px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <h2 class="title"><i class="icon-right-dir"></i>BẢN ĐỒ ĐẾN VĂN PHÒNG <i class="icon-left-dir"></i></h2>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one column_visual ">
                                    <p>{!! isset($information['google-map']) ? $information['google-map'] : '' !!}</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="section the_content no_content">
                        <div class="section_wrapper">
                           <div class="the_content_wrapper"></div>
                        </div>
                     </div>
                     <div class="section section-page-footer">
                        <div class="section_wrapper clearfix">
                           <div class="column one page-pager">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
            </div>
         </div>
</div>
         
@endsection
