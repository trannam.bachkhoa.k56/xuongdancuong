<div class="header_placeholder"></div>
   <div id="Top_bar" class="loading">
      <div class="container">
         <div class="column one">
            <div class="top_bar_left clearfix">
               <!-- .logo -->
               <div class="logo">
                  <a id="logo" href="/" title="logo"><img class="logo-main scale-with-grid" src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}" alt="logo" /><img class="logo-sticky scale-with-grid" src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}" alt="metaldoor-vietnam" /><img class="logo-mobile scale-with-grid" src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}" alt="logo" /></a>       
               </div>
               <div class="menu_wrapper">
                  <nav id="menu" class="menu-main-menu-container">
                     <ul id="menu-main-menu" class="menu">
                        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                           @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                        <!-- <li id="menu-item-3835" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2324 current_page_item">
                           <a href="http://metaldoor.vn/"><span>TRANG CHỦ</span></a>
                        </li> -->
                           <li id="menu-item-3484" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                              <a href="{{ $menuelement['url'] }}"><span style="text-transform: uppercase;">{{ $menuelement['title_show'] }}</span></a>
                              @if (!empty($menuelement['children']))
                              <ul class="sub-menu">
                                 @foreach ($menuelement['children'] as $elenmentparent)
                                 <li id="menu-item-2717" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children">
                                    <a href="{{ $elenmentparent['url'] }}"><span>{{ $elenmentparent['title_show']}} </span></a>
                                    @if (!empty($elenmentparent['children']))
                                    <ul class="sub-menu">
                                       @foreach ($elenmentparent['children'] as $elenmentparent2)
                                       <li id="menu-item-3587" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ $elenmentparent2['title_show']}}"><span>{{ $elenmentparent2['title_show']}}</span></a>
                                       </li>
                                       @endforeach
                                    </ul>
                                    @endif
                                 </li>
                                 @endforeach
                              </ul>
                              @endif
                           </li>
                           @endforeach
                        @endforeach

                        <!-- <li id="menu-item-2363" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                           <a href="http://metaldoor.vn/tin-tuc/"><span>TIN TỨC</span></a>
                           <ul class="sub-menu">
                              <li id="menu-item-2457" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/tin-tuc/tin-tuc-va-su-kien/"><span>Tin tức và Sự kiện</span></a></li>
                              <li id="menu-item-2456" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/tin-tuc/tin-chuyen-nganh/"><span>Tin chuyên ngành</span></a></li>
                              <li id="menu-item-3372" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/tin-tuc/tin-hoat-dong-lien-quan/"><span>Tin hoạt động liên quan</span></a></li>
                           </ul>
                        </li>
                        <li id="menu-item-2942" class="menu-item menu-item-type-taxonomy menu-item-object-portfolio-types menu-item-has-children">
                           <a href="http://metaldoor.vn/portfolio-types/cong-trinh/"><span>DỰ ÁN</span></a>
                           <ul class="sub-menu">
                              <li id="menu-item-2943" class="menu-item menu-item-type-taxonomy menu-item-object-portfolio-types"><a href="http://metaldoor.vn/portfolio-types/cong-trinh-da-trien-khai/"><span>Công trình đã triển khai</span></a></li>
                              <li id="menu-item-2944" class="menu-item menu-item-type-taxonomy menu-item-object-portfolio-types"><a href="http://metaldoor.vn/portfolio-types/cong-trinh-dang-trien-khai/"><span>Công trình đang triển khai</span></a></li>
                           </ul>
                        </li>
                        <li id="menu-item-3488" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                           <a href="http://metaldoor.vn/cong-nghe/"><span>CÔNG NGHỆ</span></a>
                           <ul class="sub-menu">
                              <li id="menu-item-3519" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/kinh/"><span>Kính</span></a></li>
                              <li id="menu-item-3520" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/nhom/"><span>Nhôm</span></a></li>
                              <li id="menu-item-3521" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/phu-kien/"><span>Phụ kiện</span></a></li>
                              <li id="menu-item-3522" class="menu-item menu-item-type-taxonomy menu-item-object-category"><a href="http://metaldoor.vn/cong-nghe/video-clips-cong-nghe/"><span>Video Clips</span></a></li>
                           </ul>
                        </li>
                        <li id="menu-item-3870" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/hinh-anh/"><span>HÌNH ẢNH</span></a></li>
                        <li id="menu-item-2368" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="http://metaldoor.vn/lien-he/"><span>LIÊN HỆ</span></a></li> -->
                     </ul>
                  </nav>
                  <a class="responsive-menu-toggle " href="#"><i class="icon-menu"></i></a>         
               </div>
               <div class="secondary_menu_wrapper">
                  <!-- #secondary-menu -->
               </div>
               <div class="banner_wrapper">
               </div>
               <div class="search_wrapper">
                  <!-- #searchform -->
              
                  <form method="get" id="searchform" action="{{ route('search_product') }}">
                     <i class="icon_search icon-search"></i>
                     <a href="#" class="icon_close"><i class="icon-cancel"></i></a>
                     <input type="text" class="field" name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}"  placeholder="Tìm kiếm..." />      
                     <input type="submit" class="submit" value="" style="display:none;" />
                  </form>
               </div>
            </div>
            <div class="top_bar_right">
               <div class="top_bar_right_wrapper"><a id="search_button" href="#"><i class="icon-search"></i></a></div>
            </div>
         </div>
      </div>
    </div>