<footer id="Footer" class="clearfix">
   <div class="widgets_wrapper" style="padding:15px;">
      <div class="container">
         <div class="column one-third">
            <aside id="text-5" class="widget widget_text">
               <h4>CHÍNH SÁCH &#038; QUY ĐỊNH</h4>
               <div class="textwidget">
                  <ul class="footer_links">
                     {!! isset($information['chinh-sach-quy-dinh'])  ? $information['chinh-sach-quy-dinh'] : ''  !!}
                  </ul>
               </div>
            </aside>
         </div>
         <div class="column one-third">
            <aside id="text-7" class="widget widget_text">
               <h4>TƯ VẤN KHÁCH HÀNG</h4>
               <div class="textwidget">
                  <ul class="footer_links">
                     {!! isset($information['tu-van-khach-hang'])  ? $information['tu-van-khach-hang'] : ''  !!}
                  </ul>
               </div>
            </aside>
         </div>
         <div class="column one-third">
            <aside id="text-2" class="widget widget_text">
               <h4>THÔNG TIN LIÊN HỆ</h4>
               <div class="textwidget">
                  <ul class="footer_links">
                     {!! isset($information['thong-tin-lien-he'])  ? $information['thong-tin-lien-he'] : ''  !!}
                  </ul>
               </div>
            </aside>
         </div>
      </div>
   </div>
   <div class="footer_copy">
      <div class="container">
         <div class="column one">
            <a id="back_to_top" class="button button_left button_js" href=""><span class="button_icon"><i class="icon-up-open-big"></i></span></a>
            <!-- Copyrights -->
            <div class="copyright">
               &copy; {{ isset($information['ten-cong-ty'])  ? $information['ten-cong-ty'] : ''  }}
            </div>
            <ul class="social"></ul>
         </div>
      </div>
   </div>
</footer>