@extends('xhome.layout.site')

@section('title','Đặt hàng')
@section('content')
    <style>
header{
    position: static !important;
    box-shadow: 0 0 6px rgba(0,0,0,0.3);
}
</style>

<style>
    h3.titleV 
    {
            font-size: 25px;
    }
    h3.titleV i
    {
            margin-right: 15px;
    }
    section.col1-layout
    {
        margin-bottom: 60px; 
    }
    section.col1-layout .pb20
    {
        padding: 0 200px;
    }
    @media all and (max-width: 500px) {
        section.col1-layout .pb20
        {
            padding: 0;

        }
        section.col1-layout .table img
        {
                width: 70px;
        }
        section.col1-layout .table td .price
        {
            font-size: 12px;
        }
    }
</style>
 <section class="main-cart-page main-container col1-layout">
        <div class="main container -xs">
            <div class="col-main cart_desktop_page cart-page">
                <div class="cart page_cart ">
                    <h1>Giỏ hàng</h1>
                    <form action="{{ route('send') }}" method="post">
                        {{ csrf_field() }}
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Sản phẩm</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Tổng tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sumPrice = 0;?>
                            @foreach($orderItems as $id => $orderItem)
                                <tr>
                                    <td>
                                        <img src="{{ $orderItem->image }}" alt="{{ $orderItem->title }}"
                                             title="{{ $orderItem->title }}" width="100"/>
                                        <p>{{ $orderItem->title }}</p>
                                    </td>
                                    <td>
                                        <div class="price" style="font-size: 14px;">
                                            Giá:
                                            @if (!empty($orderItem->price_deal)
                                             && !empty($orderItem->discount_end) && ( time() < strtotime($orderItem->discount_end))
                                             && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                            )
                                                <span class="priceOld"><del>{{ number_format($orderItem->price , 0)}}</del></span>

                                                <span class="priceDiscount">{{ number_format($orderItem->price_deal , 0) }}
                                                    đ</span>
                                                <input type="hidden" class="unitPrice"
                                                       value="{{ $orderItem->price_deal }}">
                                            @elseif (!empty($orderItem->discount))
                                                <span class="priceOld"><del>{{ number_format($orderItem->price , 0)}}</del></span>

                                                <span class="priceDiscount">{{ number_format($orderItem->discount , 0) }}
                                                    đ</span>
                                                <input type=""hidden class="unitPrice"
                                                       value="{{ $orderItem->discount }}">
                                            @else
                                                <input type="hidden" class="unitPrice"
                                                       value="{{ $orderItem->price }}">

                                                <span class="priceDiscount">{{ number_format($orderItem->price , 0)}}
                                                    đ</span>
                                            @endif
                                           
                                        </div>
                                    </td>
                                    <td>
                                        <input type="hidden" name="product_id[]"
                                               value="{{ $orderItem->product_id }}"/>
                                        {{--min = 1--}}
                                        <input type="number" name="quantity[]" style="width:80px;"
                                               value="{{ $orderItem->quantity }}"
                                               onchange="return changeQuantity(this);" min="0"/>
                                    </td>
                                    <td class="totalPrice tr bold">
                                        <font color="red"><?php
                                            if (!empty($orderItem->price_deal)
                                                && !empty($orderItem->discount_end) && (time() < strtotime($orderItem->discount_end))
                                                && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                            ) {
                                                $sumPrice += ($orderItem->price_deal * $orderItem->quantity);
                                                echo number_format(($orderItem->price_deal * $orderItem->quantity), 0);
                                            } elseif (!empty($orderItem->discount)) {
                                                $sumPrice += ($orderItem->discount * $orderItem->quantity);
                                                echo number_format(($orderItem->discount * $orderItem->quantity), 0);
                                            } else {
                                                $sumPrice += ($orderItem->price * $orderItem->quantity);
                                                echo number_format(($orderItem->price * $orderItem->quantity), 0);
                                            } ?></font>

                                    </td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td colspan="4" rowspan="" headers="">
                                        <div class="inline-block" style="float: right"><span>Tổng tiền:</span>
                                                <strong><span class="totals_price price sumPrice">{{ number_format($sumPrice , 0) }}</span></strong>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <script>
                            function changeQuantity(e) {
                                var unitPrice = $(e).parent().parent().find('.unitPrice').val();
                                var quantity = $(e).val();
                                var totalPrice = unitPrice * quantity;
                                var sum = 0;
                                $(e).parent().parent().find('.totalPrice').find('font').empty();
                                $(e).parent().parent().find('.totalPrice').find('font').html(numeral(totalPrice).format('0,0'));

                                $('.totalPrice').each(function () {
                                    var totalPrice = $(this).find('font').html();
                                    sum += parseInt(numeral(totalPrice).value());
                                });
                                $('.sumPrice').empty();
                                $('.sumPrice').html(numeral(sum).format('0,0'));
                            }
                        </script>

                        <div class="InformationPerson informationOrder clearfix">
                          
                            <div class="col-xs-12 col-xs-offset-0 col-md-12 pb20">
                                <div class="mainTitle lineorange"><h3 class="titleV bgorange"><i class="fa fa-newspaper-o"
                                                                                             aria-="true"></i>Thông
                                    tin khách hàng</h3></div>
                                <p class="titlePayment">Vui lòng điền đầy đủ thông tin nhận hàng bên dưới, các mục có
                                    dấu <font color="red">(*)</font> là bắt buộc </p>
                                <div class="form-group">
                                    <label>Họ và tên<span><font color="red">*</font></span>: </label>
                                    <input type="text" class="form-control" name="ship_name" placeholder=""
                                           value="{{ !empty(old('ship_name')) ? old('ship_name') : '' }}" required/>
                                    @if ($errors->has('ship_name'))
                                        <span class="red">
                                        <strong>{{ $errors->first('ship_name') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Điện thoại<span><font color="red">*</font></span>: </label>
                                    <input type="text" class="form-control" name="ship_phone" placeholder=""
                                           value="{{ !empty(old('ship_phone')) ? old('ship_phone') : '' }}" required/>
                                    @if ($errors->has('ship_phone'))
                                        <span class="red">
                                        <strong>{{ $errors->first('ship_phone') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Email<span></span>: </label>
                                    <input type="email" class="form-control" name="ship_email" placeholder=""
                                           value="{{ !empty(old('ship_email')) ? old('ship_email') : '' }}" required/>
                                    @if ($errors->has('ship_email'))
                                        <span class="red">
                                        <strong>{{ $errors->first('ship_email') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Địa chỉ nhận hàng<span><font color="red">*</font></span>: </label>
                                    <input type="text" class="form-control" name="ship_address" placeholder=""
                                           value="{{ !empty(old('ship_address')) ? old('ship_address') : '' }}"
                                           required/>
                                    @if ($errors->has('ship_address'))
                                        <span class="red">
                                        <strong>{{ $errors->first('ship_address') }}</strong>
                                </span>
                                    @endif
                                </div>
                                <div class="btnSubmit">
                                
                                    <button type="submit" class="btn btn-danger">Tiến hàng thanh toán</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
        
@endsection