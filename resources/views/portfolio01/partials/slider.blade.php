<!--Home Section Start-->
<section id="home" class="banner" data-stellar-background-ratio=".7">
	<div id="slideHome" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">
		@foreach(\App\Entity\SubPost::showSubPost('slider', 8) as $id => $slide)
		<div class="carousel-item {{ ($id == 0)? 'active' : ''}}">
			<div class="carousel-caption d-md-block">
			  <h5>{{ isset($slide['title']) ?  $slide['title'] : '' }}</h5>
			  <p>{!! isset($slide['content']) ?  $slide['content'] : '' !!}</p>
			</div>
			<div class="mask"></div>
			<img src="{{ isset($slide['image']) ?  $slide['image'] : '' }}" class="w-100 hideMobile" alt="{{ isset($slide['title']) ?  $slide['title'] : '' }}">
			<img src="{{ isset($slide['hinh-mobile']) ?  $slide['hinh-mobile'] : '' }}" class="w-100 hidePC" alt="{{ isset($slide['title']) ?  $slide['title'] : '' }}">
		</div>
		@endforeach
	  </div>
	  <a class="carousel-control-prev" href="#slideHome" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#slideHome" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	  </a>
		<div class="arrow bounce">
			<a class="fa fa-chevron-down fa-2x" href="#" data-scroll-nav="1"></a>
		</div>
	</div>
	<!-- <svg id="home-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1400 300" preserveAspectRatio="none">
		<path class="p-curve" d="M0,96.1c109.9,67.5,145.1,201.1,329.6,202.5S1043.2,99.5,1400,0v300H0V96.1z"/>
	</svg> -->
</section>
<!--Home Section End-->
<script src="{{ asset('portfolio01/js/particles.min.js') }}"></script>
<script>
	$(document).ready(function(){
		$('.carousel').carousel({
		  interval: 5000
		})
	});
</script>