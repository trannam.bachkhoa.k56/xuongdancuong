<!--Footer Start-->
<footer class="pt-50 pb-50">
	<div class="container">
		<div class="row text-left">
			<div class="col-md-4 col-sm-6">
				<!--Contant Item-->
				<div class="contact-info">
					<h5>COMPANY</h5>
					<p>{{ isset($information['ten-cong-ty']) ?  $information['ten-cong-ty'] : '' }}</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6 ">
				<!--Contant Item-->
				<div class="contact-info text-left">
					<h5>CONTACT US</h5>
					<p>MOBILE: {{ isset($information['so-dien-thoai']) ?  $information['so-dien-thoai'] : '' }}<br>
					TEL: {{ isset($information['telephone']) ?  $information['telephone'] : '' }}<br>
					EMAIL: {{ isset($information['email']) ?  $information['email'] : '' }}</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<!--Contant Item-->
				<div class="contact-info">
					<h5>ADDRESS</h5>
					<p>{{ isset($information['dia-chi-dau-trang']) ?  $information['dia-chi-dau-trang'] : '' }}</p>
				</div>
			</div>
		</div>
		<!--<div class="row text-center">
			<div class="col-md-12">
				<hr>
				<p class="copy pt-30">
					{{ isset($information['copyright']) ?  $information['copyright'] : '' }}
				</p>
			</div>
		</div>-->
	</div>
</footer>
<!--Footer End-->