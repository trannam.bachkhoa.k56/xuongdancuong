<!--Preloader-->
        <div class="preloader">
            <div class="loader "></div>
        </div>
        <!--Preloader-->

        <!--Navbar Start-->
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <!-- LOGO -->
                <a class="navbar-brand logo" href="/">
                    <img src="{{ isset($information['logo']) ? $information['logo'] : '' }}"/>
                </a>

                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar-collapse collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
						@foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
							@foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
								<li class="nav-item">
									<a class="nav-link" href="{{ $menuElement['url'] }}">{{ $menuElement['title_show'] }}</a>
								</li>
							@endforeach
						@endforeach
						<li class="nav-item">
							<a class="nav-link facebookIcon" target="_blank" href="{!! isset($information['nhung-fb']) ?  $information['nhung-fb'] : '' !!}"><i class="fa fa-facebook"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link facebookIcon" target="_blank" href="{!! isset($information['instagram']) ?  $information['instagram'] : '' !!}"><i class="fa fa-instagram"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link youtubeIcon" target="_blank" href="{!! isset($information['link-youtube']) ?  $information['link-youtube'] : '' !!}"><i class="fa fa-youtube"></i></a>
						</li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--Navbar End-->