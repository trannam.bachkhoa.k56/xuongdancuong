@extends('portfolio01.layout.site')

@section('title', isset($post->title) ? $post->title : '')
@section('meta_description', !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', '') @section('dynx_itemid', '')
@section('dynx_pagetype', 'other')
@section('dynx_totalvalue', '0')

@section('content')
<section class="banner-blog hideMobile" style="background: url({{ isset($information['banner-gioi-thieu']) ?  $information['banner-gioi-thieu'] : '' }});background-size: cover;background-position: center center !important;">
		<div class="mask"></div>
            <!--Banner Caption-->
            <div class="banner-caption text-center">
                <h1>{{ $post->title }}</h1>
				<div class="desCription">{!! isset($information['mo-ta-gioi-thieu']) ?  $information['mo-ta-gioi-thieu'] : '' !!}</div> 
                <!--<div class="bread-crumb mt-10">
                    <a href="/">Home</a>
                    <a href="#">Contact</a>
                </div> -->
            </div>
        </section>
		<section class="banner-blog fullheight hidePC" style="background: url({{ isset($information['banner-gioi-thieu-mobile']) ?  $information['banner-gioi-thieu-mobile'] : '' }});background-size: cover;background-position: center center !important;">
			<div class="mask"></div>
            <!--Banner Caption-->
            <div class="banner-caption text-center">
                <h1>{{ $post->title }}</h1>
				<div class="desCription">{!! isset($information['mo-ta-gioi-thieu']) ?  $information['mo-ta-gioi-thieu'] : '' !!}</div> 
                <!--<div class="bread-crumb mt-10">
                    <a href="/">Home</a>
                    <a href="#">Contact</a>
                </div> -->
            </div>
            <div class="arrow bounce">
                <a class="fa fa-chevron-down fa-2x" href="#" data-scroll-nav="4"></a>
            </div>
        </section>
		<!--About Section Start-->
        <section class="about pt-40 pb-40" data-scroll-index="4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 hideMobile">
                        <!--About Image-->
                        <div class="about-img">
                            <img src="{{ isset($information['hinh-anh-gioi-thieu']) ?  $information['hinh-anh-gioi-thieu'] : '' }}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <!--About Content-->
                        <div class="about-content">
                            <div class="about-heading">
                                <h2>{!! isset($information['ten-cong-ty']) ?  $information['ten-cong-ty'] : '' !!}</h2>
                            </div>
                            <div>{!! isset($information['gioi-thieu-ngoai-trang-chu']) ?  $information['gioi-thieu-ngoai-trang-chu'] : '' !!}</div>
                            <!--About Social Icons-->
                            <div class="social-icons">
                                <a href="{!! isset($information['nhung-fb']) ?  $information['nhung-fb'] : '' !!}"><i class="fa fa-facebook"></i></a>
                                <a href="{!! isset($information['twitter']) ?  $information['twitter'] : '' !!}"><i class="fa fa-twitter"></i></a>
                                <a href="{!! isset($information['google-plus']) ?  $information['google-plus'] : '' !!}"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--About Section End-->
		
        <!--<section class="blog-detail pt-20 pb-30">
            <div class="container">
                <div class="row">
                    Blog Content
                    <div class="col-lg-12">
						<div class="exCept mb-20">{{$post->description}}</div>
                        <!--Blog Content-->
                        <!--<div class="blog-content">
                            {!!$post->content!!}
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
		
		<!-- Staff -->
		<section class="staff pt-40 pb-50">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="heading text-center">
							<h2>OUR TEAM</h2>
						</div>
					</div>
				</div>
				<div class="row">
					@foreach(\App\Entity\SubPost::showSubPost('thanh-vien', 8) as $id => $customer)
					<div class="col-md-3">
						<!--Service Item-->
						<div class="staff-item">
							<!-- <div class="CropImg mb-10">
							<span class="member thumbs">
								<img src="{{ isset($customer['image']) ? asset($customer['image']) : '' }}" alt="{{ isset($customer['title']) ?  $customer['title'] : '' }}" class="img-responsive" />
							</span>
							</div>-->
							<h3>{{ isset($customer['title']) ?  $customer['title'] : '' }}</h3>
							<p>{{ isset($customer['description']) ?  $customer['description'] : '' }}</p>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</section>
		<!-- end Staff -->
@endsection
