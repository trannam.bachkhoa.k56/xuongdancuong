@extends('portfolio01.layout.site')

@section('title', isset($post->title) ? $post->title : '')
@section('meta_description', !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', '') @section('dynx_itemid', '')
@section('dynx_pagetype', 'other')
@section('dynx_totalvalue', '0')

@section('content')
<section class="banner-blog bannerContact hideMobile" style="background: url({{ isset($information['banner-trang-lien-he']) ?  $information['banner-trang-lien-he'] : '' }});background-size: cover;">
            <!--Banner Caption-->
			<div class="mask"></div>
            <div class="banner-caption text-center">
                <h1>{{ $post->title }}</h1>
				<div class="desCription">{!! isset($information['mo-ta-lien-he']) ?  $information['mo-ta-lien-he'] : '' !!}</div> 
                <!--<div class="bread-crumb mt-10">
                    <a href="/">Home</a>
                    <a href="#">Contact</a>
                </div>-->
            </div>
            <div class="arrow bounce">
				<a class="fa fa-chevron-down fa-2x" href="#" data-scroll-nav="3"></a>
			</div>
        </section>
		<section class="banner-blog fullheight hidePC" style="background: url({{ isset($information['banner-contact-mobile']) ?  $information['banner-contact-mobile'] : '' }});background-size: cover;background-position: center center !important;">
		<div class="mask"></div>
            <!--Banner Caption-->
            <div class="banner-caption text-center">
                <h1>{{ $post->title }}</h1>
				<div class="desCription">{!! isset($information['mo-ta-lien-he']) ?  $information['mo-ta-lien-he'] : '' !!}</div> 
                <!--<div class="bread-crumb mt-10">
                    <a href="/">Home</a>
                    <a href="#">Contact</a>
                </div>-->
            </div>
            <div class="arrow bounce">
                <a class="fa fa-chevron-down fa-2x" href="#" data-scroll-nav="4"></a>
            </div>
        </section>
        <section class="blog-detail pt-30 pb-100" data-scroll-index="3">
            <div class="container">
                <div class="row">
                    <!--Blog Content-->
                    <div class="col-lg-12">
						<div class="row">
							<div class="col-lg-6">
							  <div class="location">
								<div class="contactMain">
									{!!$post->content!!}
								</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div id="map">{!! isset($information['map']) ?  $information['map'] : '' !!}</div>
							</div>
						</div>
                        <!--Blog Comment Form-->
						<div class="row">
							<form class="col-lg-8 offset-lg-2 contact-form" action="{{route('sub_contact')}}" method="post" onSubmit="return contact(this);">
								<h3 class="comment-title">Contact form</h3>
								<div class="row">
									<!--Name-->
									<div class="col-md-6">
										<input class="form-inpt" type="text" name="name" required="required" placeholder="Name*">
									</div>
									<!--Email-->
									<div class="col-md-6">
										<input class="form-inpt" type="text" name="address" placeholder="Address*" required="required">
									</div>
									<!--Email-->
									<div class="col-md-6">
										<input class="form-inpt" type="text" name="email" placeholder="Email*" required="required">
									</div>
									<!--phone-->
									<div class="col-md-6">
										<input class="form-inpt" type="text" name="phone" placeholder="Phone*" required="required">
									</div>
									<!--Message-->
									<div class="col-md-12">
										<textarea name="form-message" name="message" placeholder="Message*" rows="8"></textarea>
									</div>
									<div class="col-md-12 text-center pt-30">
										<input id="submit" class="main-btn" type="submit" value="Send a Message">
									</div>
								</div>
							</form>
						</div>
                    </div>
                </div>
            </div>
        </section>
@endsection
