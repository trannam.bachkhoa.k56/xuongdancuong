<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />   
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <link rel="icon" href="{{ !empty($information['icon']) ?  asset($information['icon']) : '' }}" type="image/x-icon" />

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
	
	<link rel="stylesheet" href="{{ asset('portfolio01/css/font-awesome.min.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('portfolio01/css/bootstrap.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('portfolio01/css/owl.carousel.min.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('portfolio01/css/owl.theme.default.min.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('portfolio01/css/magnific-popup.css') }}" media="all" type="text/css" />
	
	<link rel="stylesheet" href="{{ asset('portfolio01/css/style.css') }}" media="all" type="text/css" />
	
	<!--Jquery js-->
	<script src="{{ asset('portfolio01/js/jquery-3.0.0.min.js') }}"></script>
	<script src="{{ asset('portfolio01/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('portfolio01/js/jquery.stellar.js') }}"></script>
	<script src="{{ asset('portfolio01/js/animated.headline.js') }}"></script>
	<script src="{{ asset('portfolio01/js/scrollIt.min.js') }}"></script>
	
	<script src="{{ asset('portfolio01/js/isotope.pkgd.min.js') }}"></script>
	<script src="{{ asset('portfolio01/js/jquery.magnific-popup.min.js') }}"></script>
	
	<script src="{{ asset('portfolio01/js/main.js') }}"></script>

	{!! isset($information['google-anylic']) ? $information['google-anylic'] : '' !!}
	
	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}

    <script>
        var dataLayer = [];
        dataLayer.push({
            'dynx_itemid': '@yield('dynx_itemid')',
            'dynx_pagetype' : '@yield('dynx_pagetype')',
            'dynx_totalvalue' : '@yield('dynx_totalvalue')'
        });
    </script>
</head>

<body>
	@include('portfolio01.common.header')
    @yield('content')
	@include('portfolio01.common.footer')
	
	
</body>
</html>
