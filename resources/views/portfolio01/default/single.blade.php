@extends('momastore01.layout.site')

@section('title', isset($post->title) ? $post->title : '')
@section('meta_description', !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', '') @section('dynx_itemid', '')
@section('dynx_pagetype', 'other')
@section('dynx_totalvalue', '0')

@section('content')
<!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
          <div class="breadcrumb ptb_20">
            <h1>{{ $post->title }}</h1>
            <ul>
              <li><a href="/">Trang chủ</a></li>
              <li><a href="{{ route('site_category_post', ['cate_slug' => isset($category->slug) ? $category->slug : 'tin-tuc' ]) }}">{{ isset($category->title) ? $category->title : 'Tin tức' }}</a></li>
              <li class="active">{{ $post->title }}</li>
            </ul>
          </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
		@include('momastore01.partials.sidebar')
        <div class="col-sm-8 col-lg-9 mtb_20">
          <div class="row">
            <div class="blog-item listing-effect col-md-12 mb_50">
              <div class="post-info mtb_20 ">
                <h2 class="mb_10 title"> <a href="single_blog.html">{{ $post->title }}</a> </h2>
				<div class="details mtb_20">
					<?php $date=date_create($post->created_at); ?>
					<div class="date"> <i class="fa fa-calendar" aria-hidden="true"></i>{{ date_format($date,"d/m/Y H:i") }}</div>
				  </div>
                <p>{{ $post->description }}</p>
              </div>
              
			  <div class="content">
				<?=  isset($post->content ) ?$post->content  : 'đang cập nhật tin tức'?>
			  </div>
			  <!-- Related Posts -->
			  <div class="Related">
			<h4 class="headline mgTop30">BÀI VIẾT LIÊN QUAN</h4>
			<div class="row blog-grid-flex">
				 @foreach (\App\Entity\Post::relativeProduct($post->slug, 3) as $post)	
				<?php $date=date_create($post->created_at); ?>
				<div class="blog-item col-md-6 mb_30">
                <div class="post-format">
                  <div class="thumb post-img"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}"> <img src="{{ !empty($post['image']) ? asset($post['image']) : '' }}"  alt="{{ $post['title'] }}"></a></div>
                </div>
                <div class="post-info mtb_20 ">
                  <h3 class="mb_10"> <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">{{ $post['title'] }}</a> </h3>
                  <p>{{ $post['description'] }}</p>
                  <div class="details mtb_20">
                    <div class="date pull-left"> <i class="fa fa-calendar" aria-hidden="true"></i>{{ date_format($date,"d/m/Y H:i") }}</div>
                    <div class="more pull-right"> <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">Xem thêm <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
                  </div>
                </div>
				</div>
				@endforeach
			</div>
			</div>
			<!-- Related Posts / End -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Single Blog  -->
  <!-- End Blog   -->
  <!-- =====  CONTAINER END  ===== -->
@endsection