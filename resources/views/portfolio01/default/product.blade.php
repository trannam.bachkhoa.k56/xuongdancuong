@extends('portfolio01.layout.site')

@section('title', isset($product->title) ? $product->title : '' )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)

@section('dynx_itemid',  $product->product_id )
@section('dynx_pagetype', 'offerdetail')
@php
    $totalValue = 0;
    if ( !empty($product->price_deal)
        && !empty($product->discount_end)
        && ( time() < strtotime($product->discount_end))
        && !empty($product->discount_start) && (time() > strtotime($product ->discount_start))) {
            $totalValue = $product->price_deal;
        }

        elseif (!empty($product->discount)) {
            $totalValue =  $product->discount; }
        else
            $totalValue = $product->price;
@endphp

@section('dynx_totalvalue', $totalValue)

@section('content')   
		<section class="banner-project banner-blog">
            <!--Banner Caption-->
           <!--  <div class="banner-caption text-center">
                <h1>{{$product->title}}</h1>
				<p class="clw italic">
					<?php $showCategory = false; ?>  
					@foreach ($categories as $id => $category) 
					@if ($category->title != 'Sản phẩm khuyến mãi' && !$showCategory)
					{{ $category->title }}
					@endif    
					@endforeach
				</p>
               <div class="bread-crumb mt-10">
					<a href="/">Home</a>
					<?php $showCategory = false; ?>  
					@foreach ($categories as $id => $category) 
					@if ($category->title != 'Sản phẩm khuyến mãi' && !$showCategory)
                    <a href="{{ route('site_category_product', [ 'cate_slug' => $category->slug]) }}">{{ $category->title }}</a>
					@endif    
					@endforeach
                    <a href="#">{{$product->title}}</a>
                </div>
            </div>-->
        </section>
        <section class="blog-detail pt-20 pb-50">
            <div class="container">
                <div class="row">
					<div class="col-md-12">
						<div class="headingl">
						 <h1>{{$product->title}}</h1>
						<p class="clw italic">
							<?php $showCategory = false; ?>  
							@foreach ($categories as $id => $category) 
							@if ($category->title != 'Sản phẩm khuyến mãi' && !$showCategory)
							{{ $category->title }}
							@endif    
							@endforeach
						</p>
						</div>
					   <div class="imagesProduct mb-10">
					   @if (!empty($product->image_list))
						  @foreach (explode(',', $product->image_list) as $imageProduct)

						  <div class="pd10">
							 <div class="CropImg CropImg70 CropImgPro">
								<a class="thumbs" id="ZoomIn" href="{{ $imageProduct }}" data-fancybox="images" data-srcset="{{ $imageProduct }} 1600w, {{ $imageProduct }} 1200w, {{ $imageProduct }} 640w">
								<img src="{{$imageProduct}}" alt="" width="100%" >
								<!--  href="{{ $imageProduct }}" -->
							 </a>
							 </div>
						  </div>
						  @endforeach
					   @else
							 <div class="pd10">
								<div class="CropImg CropImg70 CropImgPro">
								   <a class="thumbs">
									  <img src="{{ $product->image }}" alt="" width="100%" >
								   </a>
								</div>
							 </div>
					   @endif
					   </div>
					   <div class="imagesProducts">
						  @if (!empty($product->image_list))
					   @foreach (explode(',', $product->image_list) as $imageProduct)
					   <div class="pd10">
						  <div class="CropImg CropImg70 CropImgFull">
							 <a class="thumbs">
							 <img src="{{ $imageProduct }}" alt="" width="100%" >
						  </a>
						  </div>
					   </div>
						  @endforeach
					   @else
							 <div class="pd10">
								<div class="CropImg CropImg70 CropImgFull">
								   <a class="thumbs">
									  <img src="{{$product->image }}" alt="" width="100%" >
								   </a>
								</div>
							 </div>
					   @endif
					   </div>
					</div>
                    <!--Blog Content-->
                    <!--<div class="col-lg-8 offset-lg-2">
                        <!--Blog Heading-->
                        <!--<div class="blog-heading">
                            <h2>{{$product->title}}</h2>
                        </div>
						<div class="exCept mb-20">{{$product->description}}</div>
                        <!--Blog Content-->
                        <!--<div class="blog-content">
                            {!!$product->content!!}
							@foreach (explode(',', $product->image_list) as $imageProduct)
								<div class="mt-20 tc">
							  <img src="{{ $imageProduct }}" alt="" />
							  </div>
							@endforeach
                        </div>
                    </div>-->
                </div>
            </div>
        </section>
		<link rel="stylesheet" type="text/css" media="screen" href="/public/xhome1/css/slick.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/public/xhome1/css/slick-theme.css"/ >
		<script src="/public/xhome1/js/slick.min.js"></script>
		
		<script>
		
$('.imagesProduct').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.imagesProducts'
    });
    $('.imagesProducts').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.imagesProduct',
      focusOnSelect: true
    });

         $(document).ready(function(){
             $(window).scroll(function () {
                var w = window.innerWidth;
                if (w > 767) {
                    $("#scroller").stick_in_parent({offset_top: 75});
                }
            });
         
         });
         
      </script>
		<script src="{{ asset('portfolio01/js/fancybox/jquery.mousewheel-3.0.4.pack.js') }}"></script>
		<script src="{{ asset('portfolio01/js/fancybox/jquery.fancybox-1.3.4.js') }}"></script>
		<link rel="stylesheet" href="{{ asset('portfolio01/js/fancybox/jquery.fancybox-1.3.4.css') }}" media="all" type="text/css" />
		<script>
		$("a#ZoomIn").fancybox();
      </script>
@endsection
