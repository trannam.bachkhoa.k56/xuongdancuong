@extends('momastore01.layout.site')
@section('title', 'Tin tức')
@section('meta_description',  isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'searchresults')
@section('dynx_totalvalue', '0')

@section('content')
	<!-- =====  CONTAINER START  ===== -->
    <div class="container">
      <div class="row ">
        <!-- =====  BANNER STRAT  ===== -->
        <div class="col-sm-12">
          <div class="breadcrumb ptb_20">
            <h1>{{ isset($category->title) ?$category->title : 'Tin tức' }}</h1>
            <ul>
              <li><a href="/">Trang chủ</a></li>
              <li class="active">{{ isset($category->title) ?$category->title : 'Tin tức' }}</li>
            </ul>
          </div>
        </div>
        <!-- =====  BREADCRUMB END===== -->
		@include('momastore01.partials.sidebar')
        <div class="col-sm-8 col-lg-9 mtb_20">
          <div class="row">
            <div class="three-col-blog text-left">
				@if(empty($posts))
					<p>Không tồn tại danh mục này</p>
				@else
					@foreach ($posts as $id => $post)
				<?php $date=date_create($post->created_at); ?>
				<div class="blog-item col-md-6 mb_30">
                <div class="post-format">
                  <div class="thumb post-img CropImg CropImg70"><a class="thumbs" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}"> <img src="{{ !empty($post['image']) ? asset($post['image']) : '' }}"  alt="{{ $post['title'] }}"></a></div>
                </div>
                <div class="post-info mtb_20 ">
                  <h3 class="mb_10"> <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">{{ $post['title'] }}</a> </h3>
                  <p class="CutText3">{{ $post['description'] }}</p>
                  <div class="details mtb_20">
                    <div class="date pull-left"> <i class="fa fa-calendar" aria-hidden="true"></i>{{ date_format($date,"d/m/Y H:i") }}</div>
                    <div class="more pull-right"> <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">Xem thêm <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
                  </div>
                </div>
				</div>
				@endforeach
				@endif
            </div>
          </div>
          <div class="pagination-nav text-center mtb_20">
            {{ $posts->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- =====  CONTAINER END  ===== -->
  
  
    
@endsection
