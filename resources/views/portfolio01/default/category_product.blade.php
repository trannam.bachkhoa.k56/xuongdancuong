@extends('portfolio01.layout.site')
@section('title', isset($category->title) ? $category->title : '')
@section('meta_description',  isset($category->description) ? $category->description : '' )
@section('keywords', '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'searchresults')
@section('dynx_totalvalue', '0')

@section('content')
		<section class="banner-blog hideMobile" style="background: url({{ isset($information['banner-trang-san-pham']) ?  $information['banner-trang-san-pham'] : '' }});background-size: cover;background-position: center center !important;">
			<div class="mask"></div>
            <!--Banner Caption-->
            <div class="banner-caption text-center">
                <h1>{{$category->title}}</h1>
				<div class="desCription">{!! isset($information['mo-ta-du-an']) ?  $information['mo-ta-du-an'] : '' !!}</div> 
                <!-- <div class="bread-crumb mt-10">
                    <a href="/">Home</a>
                    <a href="">{{$category->title}}</a>
                </div> -->
            </div>

        </section>
		<section class="banner-blog fullheight hidePC" style="background: url({{ isset($information['banner-project-mobile']) ?  $information['banner-project-mobile'] : '' }});background-size: cover;background-position: center center !important;">
			<div class="mask"></div>
            <!--Banner Caption-->
            <div class="banner-caption text-center">
                <h1>{{$category->title}}</h1>
				<div class="desCription">{!! isset($information['mo-ta-du-an']) ?  $information['mo-ta-du-an'] : '' !!}</div> 
                <!--<div class="bread-crumb mt-10">
                    <a href="/">Home</a>
                    <a href="">{{$category->title}}</a>
                </div> -->
            </div>
            <div class="arrow bounce">
				<a class="fa fa-chevron-down fa-2x" href="#" data-scroll-nav="2"></a>
			</div>
        </section>
        <!--Portfolio Section Start-->
        <section class="portfolio mt-30 pb-70" data-scroll-index="2">
            <div class="container">
				<div class="row headingh">
					{!!$category->description!!}
				</div>
				<div class="row portfolio-items">
				@foreach (\App\Entity\Product::newProductID(20) as $id => $product)
					@if (!empty($product))
						<div class="col-lg-4 col-md-6 item h-{{ $product->category_id }}">
							<a href="{{ route('product',['cate_slug' => $product->slug]) }}">
								<div class="item-content">
									<div class="CropImg CropImg70">
										<span class="thumbs">
											<img src="{{ isset($product['image']) ?  $product['image'] : '' }}" alt="">
										</span>
									</div>
									<h3>{{ isset($product['title']) ?  $product['title'] : '' }}</h3>
									<div class="except cate">{{ isset($product['description']) ?  $product['description'] : '' }}</div>
								</div>
							</a>
						</div>
					@endif
				 @endforeach
				</div>
				<div class="row pagination mt-30">
					@if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
						{{ $products->links() }}
					@endif
				</div>
            </div>
        </section>
        <!--Portfolio Section End-->
@endsection
