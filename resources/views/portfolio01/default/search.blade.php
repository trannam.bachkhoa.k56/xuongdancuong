@extends('xhome.layout.site')
@section('title', isset($category->title) ? $category->title : '')
@section('meta_description',  isset($category->description) ? $category->description : '' )
@section('keywords', '')

@section('content')
    <style>
         header{
         position: static !important;
         box-shadow: 0 0 6px rgba(0,0,0,0.3);
         }
    </style>
   <section class="v2_bnc_inside_page">
         <div class="v2_bnc_content_top"></div>
         <script type="text/javascript">
            var urlNow=document.URL;
         </script>
         <section id="products-main" class="v2_bnc_products_page">
            <!-- Breadcrumbs -->
            <!-- Breadcrumbs -->
            <div class="clearfix"></div>
            <div class="v2_breadcrumb_main padding-top-10">
               <div class="container">
                  <ul class="breadcrumb">
                     <li ><a href="/">Trang chủ</a></li>
                     <li ><a href="#">Tìm kiếm sản phẩm</a></li>
                  </ul>
               </div>
            </div>
            <!-- End Breadcrumbs --> 
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="v2_bnc_title_main">
                        <h1>Sản phẩm phù hợp với từ khóa : <?php echo $_GET['word'];?></h1>
                     </div>
                     <div class="v2_bnc_products_page_body col-xs-12 no-padding">
                        @if(empty($products))
                                <p>Không tồn tại danh mục này</p>
                        @else
                            @foreach ($products as $id => $product)
                                <div class="v2_bnc_pr_item_box col-ld-3 col-md-3 col-sm-6 col-xs-12">
									<div class="v2_bnc_pr_item">
										<figure class="v2_bnc_pr_item_img">
										 <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title="{{ $product->title }}">
										 <img alt="{{ $product->title }}" id="f-pr-image-zoom-id-tab-home-742729" src="{{ !empty($product->image) ?  asset($product->image) : '' }}" 
											class="BNC-image-add-cart-742729 img-responsive"/>
										 </a>
											<figcaption class="v2_bnc_pr_item_boxdetails">
												<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="links_fixed">
												   <div class="hvr-shutter-in-vertical">
													  <div class="v2_bnc_pr_item_boxdetails_border">
														 <div class="v2_bnc_pr_item_boxdetails_content">
															<!-- Products Name -->  
												<h3 class="v2_bnc_pr_item_name">
													<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title=" {{ $product->title }}">
													{{ $product->title }}
													</a>
												</h3>
												<!-- End Products Name --> 
												<!-- Details -->
												<div class="v2_bnc_pr_item_short_info">
												<p>Giá :
												@if(empty($product->price))
													<span>liên hệ</span>
												@elseif($product->discount > 0)
												<span><del>{{ number_format($product->price) }}</del> đ</span> -
												<span>{{ number_format($product->discount) }} đ</span>
												@else
												<span>{{ number_format($product->price) }} đ</span>
												@endif
												</p>   
												<p>Liên hê : {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai']  : '' }}</p>                             </div>
												<!-- End Details -->
												</div>
												</div>
												</div>    
												</a>  
											</figcaption>
										</figure>
										<div class="content" style="padding-top: 10px;">
											<h3 class="v2_bnc_pr_item_name">
												<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title=" {{ $product->title }}">
													{{ $product->title }}
												</a>
											</h3>
											<p class="star"> Chất lượng : 
											<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
											</p>
											<p style="color:red">Giá :
												@if(empty($product->price))
													<span>liên hệ</span>
												@elseif($product->discount > 0)
												<span><del>{{ number_format($product->price) }}</del> đ</span> -
												<span>{{ number_format($product->discount) }} đ</span>
												@else
												<span>{{ number_format($product->price) }} đ</span>
												@endif
											</p>
											<p class="">
											<i class="fa fa-commenting-o" aria-hidden="true"></i> <span>6 bình luận<//span>
											</p>		
										</div>
								   </div>
								</div>
                            @endforeach
                        @endif

                       
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </section>
@endsection
