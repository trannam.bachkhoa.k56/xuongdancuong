@extends('momastore01.layout.site')

@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('content')
<!-- =====  CONTAINER START  ===== -->
    <div class="container mt_30">
      <div class="row ">
		@include('momastore01.partials.sidebar')
        <div class="col-sm-8 col-lg-9 mtb_20">
          <!-- contact  -->
          <div class="row">
            <div class="col-md-6 col-xs-12 contact">
              <div class="location mb_50">
                <h5 class="capitalize mb_20"><strong>THÔNG TIN LIÊN HỆ</strong></h5>
                <div class="contactMain">
				{!! isset($information['thong-tin-lien-he']) ?  $information['thong-tin-lien-he'] : '' !!}
				</div>
				</div>
            </div>
            <div class="col-md-6 col-xs-12 contact-form mb_50">
              <!-- Contact FORM -->
              <div id="contact_form">
                <form id="contact_body" onSubmit="return contact(this);" method="post" action="{{route('sub_contact')}}">
                  <input class="full-with-form " type="text" name="name" placeholder="Tên của bạn" data-required="true" />
                  <input class="full-with-form  mt_30" type="email" name="email" placeholder="Email của bạn Address" data-required="true" />
                  <input class="full-with-form  mt_30" type="text" name="phone" placeholder="Số diện thoại của bạn Number" maxlength="15" data-required="true" />
                  <textarea class="full-with-form  mt_30" name="message" placeholder="Nội dung ghi chú" data-required="true"></textarea>
                  <button class="btn mt_30" type="submit">ĐĂNG KÝ TƯ VẤN</button>
                </form>
                <div id="contact_results"></div>
              </div>
              <!-- END Contact FORM -->
            </div>
          </div>
          <!-- map  -->
          <div class="row">
            <div class="col-xs-12 map mb_80">
              <div id="map">{!! isset($information['map']) ?  $information['map'] : '' !!}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Single Blog  -->
  <!-- End Blog   -->
  <!-- =====  CONTAINER END  ===== -->
@endsection