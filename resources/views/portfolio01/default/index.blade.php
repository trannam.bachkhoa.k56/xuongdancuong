@extends('portfolio01.layout.site')

@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'home')
@section('dynx_totalvalue', '0')

@section('content')
	@include('portfolio01.partials.slider')
	
        <!--Portfolio Section Start-->
        <section class="portfolio mt-40 pb-70" data-scroll-index="1">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="heading text-center">
                            <h6>Projects</h6>
                            <h2>{{ isset($information['tieu-de-san-pham-hoac-dich-vu']) ?  $information['tieu-de-san-pham-hoac-dich-vu'] : '' }}</h2>
                        </div>
                        <div class="portfolio-filter text-center">
                            <ul>
								<li class="sel-item" data-filter="*">All</li>
								@foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
									<li data-filter=".h-{{ $cateProduct->category_id }}">{{ $cateProduct->title  }}</li>
								@endforeach
                               <!-- <li data-filter=".design">Web Design</li>
                                <li data-filter=".application">Applications</li>
                                <li data-filter=".development">Development</li> -->
                            </ul>
                        </div>
                    </div>
                </div>
				<div class="row portfolio-items">
				@foreach (\App\Entity\Product::newProductID(20) as $id => $product)
					@if (!empty($product))
						<div class="col-lg-4 col-md-6 item h-{{ $product->category_id }}">
							<a href="{{ route('product',['cate_slug' => $product->slug]) }}">
								<div class="item-content">
									<div class="CropImg CropImg70">
										<span class="thumbs">
											<img src="{{ isset($product['image']) ?  $product['image'] : '' }}" alt="">
										</span>
									</div>
									<h3>{{ isset($product['title']) ?  $product['title'] : '' }}</h3>
									<div class="except cate">{{ isset($product['description']) ?  $product['description'] : '' }}</div>
								</div>
							</a>
						</div>
					@endif
				 @endforeach

				</div>
            </div>
        </section>
        <!--Portfolio Section End-->

@endsection