@extends('admin.layout.admin')

@section('title', 'Quản trị website')

@section('content')
    <section class="Statis">
        @if (empty( Auth::user()->bussiness))
        <div class="row">
            <div class="col-md-12 col-12">
                <h3 class="btn btn-danger form-control"><a href="{{ route('users.edit', ['id' => Auth::user()->id]) }}" style="font-size: 16px; color: white;">Hoàn thiện lĩnh vực kinh doanh của bạn, để chúng tôi giúp bạn tìm đúng khách hàng mục tiêu!</a></h3>
                <hr/>
            </div>
        </div>
        @endif
        <!--<div class="row">
            <div class="col-md-12 col-12">
                <button class="btn btn-primary form-control" onclick="return loginFacebook(this); " ><i class="fa fa-facebook-square" aria-hidden="true"></i> Đăng nhập facebook</button>
                <hr/>
            </div>
        </div>
        <script>
            function loginFacebook(e) {
                window.location.replace('http://website.local/dang-nhap-facebook?user_email={!! $emailUser !!}&current_url={!! \App\Ultility\Ultility::getCurrentDomain() !!}')
            }
        </script> -->
          <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-users" aria-hidden="true"></i></span>

                    <div class="info-box-content">
                    <span class="info-box-text">Khách ghé thăm</span>
                    <span class="info-box-number">{{ $countViewsWebsite }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-user" aria-hidden="true"></i></i></span>

                    <div class="info-box-content">
                    <span class="info-box-text">Khách hàng</span>
                    <span class="info-box-number">{{ $countContactNews }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>

                    <div class="info-box-content">
                    <span class="info-box-text">Top 10 google</span>
                    <span class="info-box-number">{!! $countPost !!} bài viết</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-product-hunt" aria-hidden="true"></i></span>

                    <div class="info-box-content">
                    <span class="info-box-text">Top 10 google</span>
                    <span class="info-box-number">{!! $countProduct !!} sản phẩm</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-6 col-12">
                <div class="boxt">
                    <figure class="highcharts-figure">
                        <div id="pieChart"></div>
                        </p>
                    </figure>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="boxt">
                    <figure class="highcharts-figure">
                        <div id="funnelChart"></div>
                    </figure>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="boxt">
                    <h3><span>Tổng số khách hàng</span></h3>
                    <div class="number">
                        <p>khách hàng: {{ $countContactNews }}</p>
                    </div>
                    <div class="stat">
                        <?php
                        $countCustomer = [];
                        $customerStatus = [];
                        ?>
                        @foreach ($customerNews as $customerNew)
                            <p>{{ !empty($customerNew->utm_source) ? $customerNew->utm_source : 'không có nguồn'  }} ({{ $customerNew->count_customer_new }})</p>
                            <?php
                            $countCustomer[] = $customerNew->count_customer_new;
                            $customerStatus[] = !empty($customerNew->utm_source) ? $customerNew->utm_source : 'không có nguồn'
                            ?>
                        @endforeach
                        @if($countContactNews == 0)
                            <p>Google</p>
                            <p>Youtube</p>
                            <p>Facebook</p>
                            <p>intagram</p>
                            <p>printer</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxt">
                    <h3><span>Tương tác</span></h3>
                    <div class="number">
                        <p>Tổng số tương tác: {{ ($noteContacts + $remindContacts + $orderContacts) }}</p>
                    </div>
                    <div class="stat">
                        <p>Số trao đổi: {{ $noteContacts }}</p>
                        <p>Số nhắc nhở: {{ $remindContacts }}</p>
                        <p>Số đơn hàng: {{ $orderContacts }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxt">
                    <h3><span>Doanh thu {{ !empty($sumPriceContactOrders) ? number_format($sumPriceContactOrders, 2).' đ' : '' }}</span></h3>
                    <div class="number">
                        <p>Tỉ lệ chuyển đổi đơn hàng: {{ !empty($countContactNews) && $countContactNews != 0 ? round($countContactOrders/$countContactNews *100, 2).'%' : '' }}</p>
                    </div>
                    <div class="stat">
                        @foreach ($customerOrders as $customerOrder)
                            <p>{{ !empty($customerOrder->utm_source) ? $customerOrder->utm_source : 'không có nguồn'  }} ({{ $customerOrder->count_customer_new }})</p>
                        @endforeach
                        @if($countContactOrders == 0)
                            <p>Website moma</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content dragCustomer">  
        <div class="box">
            <div class="box-header with-border">
                <h3>Học Kinh Doanh Online Hiệu Quả Hoàn Toàn Miễn Phí</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6">
                        <p>Bạn đã biết gì về kinh doanh online hiệu quả phải làm thế nào chưa?</p>
                        <p>Nếu bạn muốn được đào tạo thường xuyên và liên tục thì Kết Công Nghệ tôi sẽ chia sẻ cho bạn về khóa đào tạo của tôi diễn ra đều đặn vào 14h giờ chiều các ngày trong tuần.</p>
                        <p>Gửi mọi người link đào tạo 14 giờ qua meet của google tại đây ID cuộc họp
                            <a href="https://meet.google.com/vas-qvnj-fnz"></a>
                            Số điện thoại
                            (‪US‬)
                            ‪+1 503-917-5259‬
                            Mã PIN: ‪499 787 844#‬ chủ đề hôm nay tiết kiệm ngân sách quảng cáo facebook bằng tiện ích moma</p>
                        <p>Để nhận các kênh thông báo với moma </p>
                        <p>Nhóm zalo: <a href="https://chat.zalo.me/?g=sglvfu883">https://chat.zalo.me/?g=sglvfu883</a></p>
                        <p>Group <a href="https://www.facebook.com/groups/513213432665245/">https://www.facebook.com/groups/513213432665245/</a></p>
                        <p>Youtube <a href="https://www.youtube.com/channel/UCHRyZ_2BVR24SQ5kwy5j6hg?view_as=subscriber">kênh youtube kết công nghệ</a></p>
                    </div>
                    <div class="col-xs-6">
                        <iframe width="100%" height="350" src="https://www.youtube.com/embed/Wz8ECqqk2Jw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>


        @if (\Illuminate\Support\Facades\Auth::user()->is_help == 0)
        $( document ).ready(function() {
            $('#modal-welcome').modal('show');
        });
        @endif

        function showHelp(e) {
            $('#modal-welcome').modal('hide');
            var intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: document.querySelector('#stepHome1'),
                        intro: "Đây là mục quản lý thông tin sản phẩm gồm có: quản lý sản phẩm của bạn, danh mục (nhóm) sản phẩm của bạn."
                    },
                    {
                        element: document.querySelector('#stepHome2'),
                        intro: "Quản lý đon hàng và công nợ của website!"
                    },
                    {
                        element: document.querySelector('#stepHome3'),
                        intro: "Quản lý khách hàng trong hệ thống website!"
                    },
                    {
                        element: document.querySelector('#stepHome4'),
                        intro: "Chăm sóc khách hàng bằng email."
                    },
                    {
                        element: document.querySelector('#stepHome5'),
                        intro: "Phát triển marketing của bạn nhờ vào bộ công cụ của moma.vn"
                    },
                    {
                        element: document.querySelector('#stepHome6'),
                        intro: "Quản lý tiện ích trên website, gồm các dịch vụ đi kèm hỗ trợ bạn kinh doanh online tốt hơn."
                    },
                    {
                    element: document.querySelector('#stepHome7'),
                        intro: "Quản lý toàn bộ thông tin website của bạn, gồm số điện thoại, fanpage, zalo,..."
                    },
                    /*{
                    element: document.querySelector('#stepHome8'),
                        intro: "Các video đào tọa để có thể sử dụng website tốt hơn và đồng thời giúp bạn kinh doanh hiệu quả hơn."
                    }, */
                    {
                        element: document.querySelector('#stepHome9'),
                        intro: "Xem các báo cáo kinh doanh của bạn trên website tại đây."
                    }
                    /*{
                     element: document.querySelector('#stepHome7'),
                     intro: "Theo dõi khách hàng trên các mạng xã hội facebook, twister, lotus, intagram,... Đồng bộ khách hàng về website."
                     },*/

                ]
            });

            intro.setOption('doneLabel', 'Đến quản lý sản phẩm').start().oncomplete(function() {
                window.location.href = '/admin/products?multipage=true';
            });


        }
    </script>

    <div class="modal fade" id="modal-welcome">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">CHÀO MỪNG BẠN ĐẾN VỚI WEBSITE MIỄN PHÍ CỦA Moma!</h4>
                </div>

                <div class="modal-body">
                    <p>Hãy xem qua một vài tác vụ thiết yếu để giúp bạn thành công. Hãy ấn bắt đầu để hiểu các tác vụ. </p>
                    <p>Cảm ơn bạn đã tin tưởng sử dụng website Moma!</p>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('update_status_help') }}" class="btn btn-default pull-left" >Đã hiểu</a>
                    <button type="button" class="btn btn-primary" href="javascript:void(0);" onclick="return showHelp(this);">Bắt đầu</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script>
        var countData = <?php echo json_encode($countCustomer) ?>;
        var statusCustomerArray = <?php echo json_encode($customerStatus) ?>;
        var dataArray = [];
        for (var i = 0 ; i < countData.length; i++) {
            dataArray.push(statusCustomerArray[i] + ' (' + countData[i] + ')');
        }
        var data = {
            datasets: [{
                data: <?php echo json_encode($countCustomer) ?>,
                backgroundColor: [
                    "#FF6384",
                    "#4BC0C0",
                    "#FFCE56",
                    "#E7E9ED",
                    "#36A2EB"
                ],
                label: 'Thống kê nguồn khách hàng vào website'
            }],
            labels: dataArray
        };

        var pieOptions = {
            events: false,
            animation: {
                duration: 500,
                easing: "easeOutQuart",
                onComplete: function () {
                    var ctx = this.chart.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'inner';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset) {

                        for (var i = 0; i < dataset.data.length; i++) {
                            var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
                                    total = dataset._meta[Object.keys(dataset._meta)[0]].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;

                            var x = mid_radius * Math.cos(mid_angle);
                            var y = mid_radius * Math.sin(mid_angle);

                            ctx.fillStyle = '#fff';
                            if (i == 3){
                                ctx.fillStyle = '#444';
                            }
                            var percent = String(Math.round(dataset.data[i]/total*100)) + "%";
                            if(dataset.data[i] != 0 && dataset._meta[0].data[i].hidden != true) {

                                ctx.fillText(percent, model.x + x, model.y + y + 15);
                            }
                        }
                    });
                }
            }
        };

        var pieChartCanvas = $("#pieChart");
        var pieChart = new Chart(pieChartCanvas, {
            type: 'pie',
            data: data,
            options: pieOptions
        });

    </script>

    <script>
        var countData = <?php echo json_encode($countCustomer) ?>;
        var statusCustomerArray = <?php echo json_encode($customerStatus) ?>;
        var dataArray = [];
        for(var i=0; i<countData.length; i++){
            dataArray.push(
                    {
                        name: statusCustomerArray[i],
                        y: countData[i]
                    },
            );
        }
        Highcharts.chart('pieChart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Thống kế nguồn khách hàng vào Website của bạn'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: dataArray
            }]
        });
    </script>

    <script>
        var dataArray = [];
        if (<?php echo $countNewContact?> != 0) {
            dataArray.push(['Khách hàng mới', {{ $countNewContact }}]);
        }

        if (<?php echo $countApproachContact?> != 0) {
            dataArray.push(['Khách hàng tiếp cận', {{ $countApproachContact }}]);
        }

        if (<?php echo $countOrderContact?> != 0) {
            dataArray.push(['Khách hàng mua hàng', {{ $countOrderContact }}]);
        }

        if (<?php echo $countCompletedContact?> != 0) {
            dataArray.push(['Khách hàng hoàn đơn', {{ $countCompletedContact }}]);
        }

        @if ( $countCompletedContact == 0 && $countNewContact == 0 && $countApproachContact == 0 && $countOrderContact == 0)
            dataArray.push(['Khách hàng mới', 1000]);
        dataArray.push(['Khách hàng tiếp cận', 500]);
        dataArray.push(['Khách hàng mua hàng', 200]);
        dataArray.push(['Khách hàng hoàn đơn', 150]);
        @endif
        Highcharts.chart('funnelChart', {
            chart: {
                type: 'funnel3d',
                options3d: {
                    enabled: true,
                    alpha: 10,
                    depth: 50,
                    viewDistance: 50
                }
            },
            title: {
                text: 'Thống kê khách hàng của bạn'
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b> ({point.y:,.0f})',
                        allowOverlap: true,
                        y: 10
                    },
                    neckWidth: '30%',
                    neckHeight: '25%',
                    width: '80%',
                    height: '80%'
                }
            },
            series: [{
                name: 'Khách hàng của bạn',
                data: dataArray
            }]
        });
    </script>

    @push('scripts')
    <script>
        $(function () {
            'use strict';

            var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
            var salesChart = new Chart(salesChartCanvas);

            var salesChartData = {
                labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                    {
                        label               : 'Electronics',
                        fillColor           : 'rgb(210, 214, 222)',
                        strokeColor         : 'rgb(210, 214, 222)',
                        pointColor          : 'rgb(210, 214, 222)',
                        pointStrokeColor    : '#c1c7d1',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgb(220,220,220)',
                        data                : [65, 59, 80, 81, 56, 55, 40]
                    },
                    {
                        label               : 'Digital Goods',
                        fillColor           : 'rgba(60,141,188,0.9)',
                        strokeColor         : 'rgba(60,141,188,0.8)',
                        pointColor          : '#3b8bba',
                        pointStrokeColor    : 'rgba(60,141,188,1)',
                        pointHighlightFill  : '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data                : [28, 48, 40, 19, 86, 27, 90]
                    }
                ]
            };

            var salesChartOptions = {
                showScale               : true,
                scaleShowGridLines      : false,
                scaleGridLineColor      : 'rgba(0,0,0,.05)',
                scaleGridLineWidth      : 1,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines  : true,
                bezierCurve             : true,
                bezierCurveTension      : 0.3,
                pointDot                : false,
                pointDotRadius          : 4,
                pointDotStrokeWidth     : 1,
                pointHitDetectionRadius : 20,
                datasetStroke           : true,
                datasetStrokeWidth      : 2,
                datasetFill             : true,
                legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',

                    maintainAspectRatio     : true,

                    responsive              : true
                  };

                  salesChart.Line(salesChartData, salesChartOptions);


            });
        </script>
    @endpush
@endsection
