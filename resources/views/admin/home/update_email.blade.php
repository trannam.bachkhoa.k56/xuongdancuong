@extends('admin.layout.admin')

@section('title', 'nâng cấp để sử dụng email marketing')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <!-- small box -->
                <section class="content dragCustomer">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header with-border">
									<h1>Sử dụng email để xây dựng mối quan hệ bền vững với khách hàng.</h1>
								</div>
								<div class="box-body">
									<p>Khách hàng sau khi đã mua sản phẩm và dịch vụ của bạn.</p>
									<p>Họ có sẵn lòng ra ngoài kia và giới thiệu sản phẩm và dịch vụ của bạn đến người thân hoặc bạn bè của họ không?</p>
									<p>Câu trả lời là: "không".</p>
									<p>Khách hàng cũ sẽ chỉ giới thiệu khách hàng mới cho bạn dựa trên 3 điều: </p>
									<p>1. Họ biết rõ về sản phẩm và dịch vụ của bạn.</p>
									<p>2. Họ yêu mến bạn.</p>
									<p>3. Họ tin tưởng bạn.</p>
									<p>Khi khách hàng giới thiệu bạn bè (người thân) đến mua sản phẩm(dịch vụ) của bạn, họ phải mang danh tiếng của mình ra để đặt cược. Chính vì vậy khi bạn nhận được lời giới thiệu từ khách hàng cũ, thì có nghĩa rằng họ đang yêu quý bạn, rất tin tưởng bạn, và tin rằng bạn sẽ cung cấp sản phẩm dịch vụ tốt nhất dành cho bạn bè (người thân) của họ.</p>
									<p>Nếu chỉ đơn thuần là những cuộc điện thoại, hoặc bữa ăn sẽ không hiệu quả - để nhiều khách hàng cũ giới thiệu khách hàng cho bạn.</p>
									<p>Đó chính là lý do vì sao khi sử dụng email hàng tuần: </p>
									<p>1. Để khách hàng hiểu rõ về ngành nghề kinh doanh của bạn.</p>
									<p>2. Để xây dựng mối quan hệ kinh doanh bền vững và lâu dài.</p>
									<p>3. Để phát triển niềm tin vào sản phẩm (dịch vụ) của bạn.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="content-header">
					<h1>
						GIA HẠN THỜI GIAN SỬ DỤNG GÓI EMAIL MARKETING
					</h1>
				</section>
				<section class="content">
			 
					<div class="row">
						<!-- form start -->
						<div class="col-xs-12 col-md-12 mgt10">

							<div class="box box-widget widget-user-2">
								<!-- Add the bg color to the header using any of the bg-* classes -->
								<div class="widget-user-header bg-blue">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png">
									</div>
									<!-- /.widget-user-image -->
									<h3 class="center"><b>GÓI DOANH NGHIỆP VỪA VÀ NHỎ </b></h3>
									<h4 class="center"><b>5.000 Email trong 1 tháng</b></h4>
								</div>
								
								<div class="box-footer no-padding bg-footer-blue">
									<table class="table" style="overflow-x: scroll;">
										  <thead class="vip2">
											<tr>
											  <th scope="col">Gói Email 1</th>
											  <th scope="col">Gói Email 2</th>
											  <th scope="col">Gói Email 3</th>
											</tr>
										  </thead>
										  <tbody>
											<tr>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
											 </tr>
											<tr>
											  <td>Giá tiền : 300.000 vnđ</td>
											  <td>Giá tiền : 806.000 vnđ</td>
											  <td>Giá tiền : 2.400.000 vnđ</td>
											</tr>
											<tr>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											</tr>
											<tr>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											</tr>
											<tr>
											  <td><button data-toggle="modal" data-target="#modal-email1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email3" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											</tr>
										  </tbody>
										</table>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-md-12 mgt10">
							<div class="box box-widget widget-user-2">
								<!-- Add the bg color to the header using any of the bg-* classes -->
								<div class="widget-user-header bg-yellow">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png">
									</div>
									<!-- /.widget-user-image -->
									<h3 class="center"><b>GÓI DOANH NGHIỆP LỚN</b></h3>
									<h4 class="center"><b>20.000 Email trong 1 tháng </b></h4>
								</div>
								<div class="box-footer bg-footer-yellow no-padding">
									<table class="table" style="overflow-x: scroll;">
										  <thead class="bg-vip1" style="color:#fff">
											<tr>
											  <th scope="col">Gói Email 1</th>
											  <th scope="col">Gói Email 2</th>
											  <th scope="col">Gói Email 3</th>
											</tr>
										  </thead>
										  <tbody>
											<tr>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
											 </tr>
											<tr>
											  <td>Giá tiền : 800.000 vnđ</td>
											  <td>Giá tiền : 2.000.000 vnđ</td>
											  <td>Giá tiền : 6.000.000 vnđ</td>
											</tr>
											<tr>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											</tr>
											<tr>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											</tr>
											
											<tr>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											</tr>
											<tr>
											  <td><button data-toggle="modal" data-target="#modal-email4" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email5" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email6" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											</tr>
										  </tbody>
										</table>
								</div>
							</div>
						</div>
					</div>
				</section>
				<style>
							.table thead th{
								text-align:center;
							}
							.table thead th,td{
								    border-left: 1px solid #fff
							}
							.bg-yellow{
								background: #ffc107;
							}
							.bg-footer-yellow{
								background:#ffc10714;
							}
							.bg-blue{
								background: #03a9f4;
							}
							.bg-footer-blue{
								background:#03a9f417;
							}
							.bg-vip1{
								background: #fcaf42;
							}
							.widget-user-header{
								text-align:center;
								padding-bottom: 10px;
							}
							h3.center{
								padding: 10px;	
							}
							.vip1{
								background: #f5cb4dc2;
								color: #FFF;
							}
							.box-footer1{
								background:#f2eddfc2;
							}
							.vip2{
								background: #429bcf;
								color: #FFF;
							}
							.bg-vip2{
								background:#2189c7;
							}
							.box-footer2{
								background:#deedf7;
							}
							
							.vip3{
								background: #70cd73;
								color: #FFF;
							}
							.bg-vip3{
								background:#4caf50;
							}
							.box-footer3{
								background:#cfdecb7a;
							}
							
							.vip{
								background: #dedddd;
								color: #333 !important;
							}
							.bg-vip{
								background : #a0a0a0 ;
							}
							.box-footer0{
								background:#d1d0d045;
							}
							
							.box-footer ul li {
								padding: 5px;
								width: 100%;
								border-bottom: 1px solid #3333331a;
							}
							.box-footer ul li a {
									color:#000;
							}
							.color-blue{
								color:#0086ff;
							}
							.color-red{
								color:red;
							}
							.center{
								color:white;
							}
							.no-padding{
								padding: 0;
							}
							input:hover{
								text-indent: 15px;
								border :1px solid #11d6f0;
								border-bottom: 1px solid #11d6f0;
							}
						</style>
				
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
		   <div class="modal fade" id="modal-email1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="1" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="300000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="3" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="806000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email3">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2400000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email4">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="1" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="800000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email5">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="3" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2000000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email6">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="6000000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- Main row -->
        <!-- /.row (main row) -->

    </section>
@endsection

