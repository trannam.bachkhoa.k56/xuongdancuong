`@extends('admin.layout.admin')

@section('title', 'Thống kê từ quảng cáo moma')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                 <div class="box box-info" >
                    <div class="box-header with-border">
                        <ul class="filte_time">
                            <li><a href="/admin/bao-cao-quang-cao-moma?start_date={!! date("Y/m/d") !!}&end_date={!! date("Y/m/d") !!}" class="btn_choose_time">Hôm nay </a></li>
                            <li><a href="/admin/bao-cao-quang-cao-moma?start_date={!! date('Y/m/d',strtotime("-1 days")); !!}&end_date={!! date('Y/m/d',strtotime("-1 days")); !!}" class="btn_choose_time">Hôm qua</a></li>
                            <li><a href="/admin/bao-cao-quang-cao-moma?start_date={!! date('Y/m/d',strtotime("-29 days")); !!}&end_date={!! date('Y/m/d',strtotime("-1 days")); !!}" class="btn_choose_time">30 ngày qua</a></li>
                            <li>
                                <a data-time="TIME_REQUEST" onclick="return showModalTime();">
                                    Thời gian khác
                                </a>
                                <div class="add-drop add-d-right" id="frm_others_date" style="left: auto; right: 0px; display:  none;position: absolute;">
                                    <form action="/admin/bao-cao-quang-cao-moma" method="get">
                                        <s class="gf-icon-neotop"></s>
                                        <div class="padding">
                                            <p>Ngày bắt đầu</p>
                                            <input placeholder="Ngày bắt đầu" name="start_date" type="date" class="datepick form-control hasDatepicker"> </div>
                                        <div class="padding">
                                            <p>Ngày kết thúc</p>
                                            <input placeholder="Ngày kết thúc" name="end_date" type="date" class="datepick form-control hasDatepicker"> </div>
                                        <div class="padding tl">
                                            <button type="submit" class="btn btn-info btn_choose_time" data-time="TIME_REQUEST" >Áp dụng</button>

                                            <button class="btn btn-default"  onclick="return showModalTime();" id="closeFilterOthersTime">Đóng</button>
                                        </div>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info" style="height:310px">
                    <div class="box-header with-border">
                        <h3 class="box-title">Chỉ số đo lường thông qua phân tích phễu</h3>
                        @if (isset($_GET['start_date']) && isset($_GET['end_date']))
                            <hr>
                            <p>Từ ngày: {!!  date_format(date_create($_GET['start_date']),"d-m-Y") !!} đến ngày {!! date_format(date_create($_GET['end_date']),"d-m-Y")  !!}</p>
                        @endif
                    </div>
                    <div class="box-body">
                        <p>Ngân sách đã sử dụng: <span style="color:red; float:right;">0 vnđ</span></p>
                        <p>Số lần hiển thị: <span style="color:red; float:right;">{!! number_format($showWebsites) !!}</span></p>
                         <p>Lượt truy cập: <span style="color:red; float:right;">{!! number_format($viewWebsites) !!}</span></p>
                        <p>khách hàng: <span style="color:red; float:right;">{!! number_format($countContacts) !!}</span></p>
                        <p>Doanh thu: <span style="color:red; float:right;">{!! number_format($sumOrderCustomer) !!} vnđ</span></p>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div class="box box-info" style="height:310px">
             <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="boxt">
                            <h3><span>Tổng lượt xem</span></h3>
                            <div class="number">
                            
                            </div>
                            <div class="stat">
                                <p>Ngân sách đã sử dụng: <span style="color:red;">0 vnđ</span></p>
                                <p>Số lần hiển thị: <span style="color:red;">{!! number_format($showWebsites) !!}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="boxt">
                            <h3><span>Tương tác</span></h3>
                            <div class="number">
                            
                            </div>
                            <div class="stat">
                                <p>Lượt truy cập: <span style="color:red">{!! number_format($viewWebsites) !!}</span></p>
                                <p>khách hàng: <span style="color:red;">{!! number_format($countContacts) !!}</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="boxt">
                            <h3><span>Doanh thu</span></h3>
                            <div class="number">
                            
                            </div>
                            <div class="stat">
                                <p>Doanh thu: <span style="color:red;">{!! number_format($sumOrderCustomer) !!} vnđ</span></p>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </section>  
    @push('scripts')      
    <script>
    
        function showModalTime(){
            $('#frm_others_date').toggle(400);
            return false;
        }
    </script>
    @endpush
    
@endsection
`
