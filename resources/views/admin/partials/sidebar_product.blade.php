<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
		
        @include('admin.partials.info')


		@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))

        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Dịch vụ (sản phẩm)</li>
                <li class="{{ Request::is('admin/products', 'admin/products/create') ? 'active' : null }} " id="stepProductGeneral1" >
                    <a href="{{ route('products.index') }}">
                        <i class="fa fa-product-hunt" aria-hidden="true"></i> <span>Dịch vụ (sản phẩm)</span>

                    </a>
                </li>

                <li class="{{ Request::is('admin/category-products', 'admin/category-products/create') ? 'active' : null }} " id="stepProductGeneral2" >
                    <a href="{{ route('category-products.index') }}">
                        <i class="fa fa-product-hunt" aria-hidden="true"></i> <span>Danh mục dịch vụ (sản phẩm)</span>
                    </a>
                </li>

            </ul>

			@endif

    </section>
    <!-- /.sidebar -->

</aside>
