<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
    @include('admin.partials.info')
    @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Menu Chính</li>
                <li class="{{ Request::is('admin/posts', 'admin/posts/create') ? 'active' : null }}">
                    <a href="{{ route('posts.index') }}">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Tin tức</span>                       
                    </a>
                </li>
				<li class="{{ Request::is('admin/categories') ? 'active' : null }}">
					<a href="{{ route('categories.index') }}"><i class="fa fa-circle-o"></i>Danh mục tin tức</a>
				</li>

                <li class="{{ Request::is('admin/pages', 'admin/pages/create') ? 'active' : null }} treeview">
                    @if ($themeCode != 'xhome')
					<a href="{{ route('pages.index') }}">
                        <i class="fa fa-file-o" aria-hidden="true"></i> <span>Trang</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
					@endif
                    <ul class="treeview-menu">
                        <li class="{{ Request::is('admin/pages') ? 'active' : null }}">
                            <a href="{{ route('pages.index') }}"><i class="fa fa-circle-o"></i>Tất cả trang</a>
                        </li>
                        <li class="{{ Request::is('admin/pages/create') ? 'active' : null }}">
                            <a href="{{ route('pages.create') }}"><i class="fa fa-circle-o"></i>Thêm mới trang</a>
                        </li>
                    </ul>
                </li>

                <li class="header">Bổ sung</li>
                @foreach($typeSubPostsAdmin as $typeSubPost)
                    <li class="{{ Request::is('admin/'.$typeSubPost->slug.'/sub-posts', 'admin/'.$typeSubPost->slug.'/sub-posts/create') ? 'active' : null }} ">
                        <a href="{{ route('sub-posts.index', ['typePost' => $typeSubPost->slug]) }}">
                            <i class="fa fa-th-list" aria-hidden="true"></i><span>{{ $typeSubPost->title }}</span>
                        </a>          
                    </li>
                @endforeach
                
                @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
				@endif

				@if ($themeCode != 'xhome')
                <li class="{{ Request::is( 'admin/menus' ) ? 'active' : null }}">
                    <a href="{{ route('menus.index') }}"><i class="fa fa-circle-o"></i>Tất cả menu</a>
                </li>
				@endif
               
                <li class="{{ Request::is( 'admin/information' ) ? 'active' : null }}">
                    <a href="{{ route('information.index') }}"><i class="fa fa-circle-o"></i>Nội dung website</a>
                </li>

                <li class="{{ Request::is('admin/hinh-thuc-thanh-toan') ? 'active' : null }} ">
                    <a href="{{ route('method_payment') }}">
                        <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Cấu hình</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/show-add-user') ? 'active' : null }} ">
                    <a href="{{ route('show_add_user') }}">
                        <i class="fa fa-user" aria-hidden="true"></i> <span>Thêm thành viên </span>
                    </a>
                </li>
                <!--<li class="{{ Request::is('admin/cau-hinh-giao-dien') ? 'active' : null }} ">
                    <a href="{{ route('config_template') }}">
                        <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Giao diện</span>
                    </a>
                </li> -->
                <li class="{{ Request::is('admin/active-theme') ? 'active' : null }} ">
                    <a href="/admin/active-theme">
                        <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Giao diện</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/cau-hinh-thong-tin-tu-van') ? 'active' : null }} ">
                    <a href="/admin/cau-hinh-thong-tin-tu-van">
                        <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Cấu hình form tư vấn</span>
                    </a>
                </li>
                
                <li class="{{ Request::is('admin/advertisement') ? 'active' : null }} ">
                    <a href="{{ route('advertisement.index') }}">
                        <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Quảng cáo </span>
                    </a>
                </li>

            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
