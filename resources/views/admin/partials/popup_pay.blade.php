<div class="modal fade" id="schedule">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title tx-center bg-teal text-fuchsia"><b>BẢNG TIẾN ĐỘ TRẢ GÓP</b></h3>
            </div>
            <div class="modal-body modal-body-table">
                <div class="result">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đơn vị tính: <b class="text-maroon">VNĐ</b> </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-content{
        height: calc(100vh - 150px);
        overflow: auto;
        width: 130%;
    }

    .tx-center{
        text-align: center;
    }
</style>