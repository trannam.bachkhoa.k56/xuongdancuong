<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
         @include('admin.partials.info')
        @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <!-- <li class="header">Kênh vận chuyển </li>

                <li class="treeview" id=""  >
                    <a target="" href="{{ route('transport.index') }}"><i class="fa fa-truck"></i>  Vận chuyển 
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                        <!--<ul class="treeview-menu" data-widget="tree">
                            <li class="{{ Request::is('admin/kenh-giao-hang-tiet-kiem') ? 'active' : null }} " id="stepProductGeneral1" >
                                <a href="{{ route('transport.giaohangtietkiem') }}">
                                    <i class="fa fa-motorcycle" aria-hidden="true"></i> <span>Giao hàng tiết kiệm</span>
                                </a>
                            </li>
                            <li class="{{ Request::is('admin/kenh-viettel-port') ? 'active' : null }} " id="stepProductGeneral1" >
                                <a href="{{ route('transport.viettel') }}">
                                    <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng viettel</span>

                                </a>
                            </li>
                            <li class="{{ Request::is('admin/kenh-giao-hang-nhanh') ? 'active' : null }} " id="stepProductGeneral1" >
                                <a href="{{ route('transport.giaohangnhanh') }}">
                                    <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng nhanh</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                </li> -->
                
                <li class="header">Hỗ trợ</li>
                <li class="">
                    <a href="https://tiva.vn/trang/ntd" style="color:red;" target="_blank">
                        1. <span > Tuyển dụng miễn phí</span>
                    </a>
                </li>
                <li class="">
                    <a href="https://printgo.vn/" target="_blank">
                        2. <span> Dịch vụ in ấn.</span>
                    </a>
                </li>
                <li class="">
                    <a href="https://bima.vn" target="_blank">
                        3. <span> Đào tạo.</span>
                    </a>
                </li>
                <li class="">
                    <a href="https://aha.chat/HZQQ4F">
                        4. <span> Chatbot.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        5. <span> Văn phòng phẩm.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        6. <span> Hoa.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        7. <span> Ăn uống.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        8. <span> Du lịch.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        9. <span> Văn phòng - bất động sản.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        10. <span> Công chứng.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        11. <span> Domains.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        12. <span> Dịch vụ quảng cáo.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        13. <span> Khóa học đào tạo.</span>
                    </a>
                </li>
				<li class="">
                    <a href="#">
                        14. <span> Vay vốn ngân hàng.</span>
                    </a>
                </li>
            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
