<div class="modal fade" id="modalActiveTheme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content_1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Thay đổi theme</h4>
            </div>
            <form method="post" onsubmit="return changeTheme(this);">
            <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" value="" id="themeId" />
                     <div class="form-group">
                        <p>Bạn có chắc chắn muốn thay đổi theme không ?</p>
                        <p>
                            Bạn sẽ phải chi trả 1 khoản phí để có thể thay đổi theme là: <span id="priceTheme"></span> đ
                        </p>
                    </div>
                    <div class="form-group errorDomain" style="color: red;">

                    </div>
                    <div class="form-group">
                        
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Thay đổi</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
            </form>
        </div>
    </div>
</div>
<script>
    function createNewDomain(e) {
        console.log(1);
        if ($(e).prop('checked')) {
            $('.selectDomain').hide();
            $('.createNewDomain').show();

            return true;
        }

        $('.selectDomain').show();
        $('.createNewDomain').hide();

        return true;
    }
    function changeTheme(e) {
        $('.errorDomain').empty();
        var token =  $('input[name=_token]').val();
        var themeId =  $('#themeId').val();

        $.ajax({
            type: "POST",
            url: '{!! route('change_theme') !!}',
            data: {
                _token: token,
                theme_id : themeId
            },
            success: function(result){
                console.log(1);
                var obj = $.parseJSON(result);
                console.log(obj.status);
                if (obj.status == 500) {
                    $('.errorDomain').append('<p>'+obj.message+'</p>');
                }
                if (obj.status == 200) {
                    alert(obj.message);
                    location.reload();
                }

                return false;
            },
            error: function(error) {
                $('.errorDomain').append('<p>Có một lỗi xảy ra. </p>');

                return false;
            }

        });


        return false;
    }
    function addTheme(e) {
        var themeId = $(e).attr('idTheme');
        var price = $(e).attr('price');

        $('#themeId').val(themeId);
        $('priceTheme').val(price);

        $('#modalActiveTheme').modal('show');
    }
</script>
<style>
    .modal-content_1 {
        background: #fff;
    }
    .createNewDomain {
        display: none;
    }
    label {
        cursor: pointer;
    }
</style>
