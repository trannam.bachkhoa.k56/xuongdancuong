<table class="table table-bordered">
    <thead class="fixed">
    <tr>
        <th class="bg-black tx-center">Số tháng</th>
        <th class="bg-black tx-center">Dư nợ đầu kỳ</th>
        <th class="bg-black tx-center">Trả gốc hàng tháng</th>
        <th class="bg-black tx-center">Trả lãi hàng tháng</th>
        <th class="bg-black tx-center">Gốc + lãi</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>0</td>
            <td class="tx-right">{{ number_format($money)  }}</td>
            <td class="tx-right">0</td>
            <td class="tx-right">0</td>
            <td class="tx-right">0</td>
        </tr>
        @php{
            $original = round($money / $month);
            $sumOriginal = 0;
            $sumInterest = 0;
            $sumMoney = 0;
        }
        @endphp
        @for($i = 1; $i <= $month; $i ++)
            <tr>
                <td>{{ $i }}</td>
                @php{
                    $interestMoney = round($money * $interest / 100 / $month);
                    $sumInterest += $interestMoney;
                    $money = round($money - $original) ;
                    $sumMoney += $original + $interestMoney;
                }
                @endphp
                <td class="tx-right">{{ number_format($money) }}</td>
                @php{
                    $sumOriginal += $original;
                }
                @endphp
                <td class="tx-right">{{ number_format($original) }}</td>
                <td class="tx-right">{{ number_format($interestMoney) }}</td>
                <td class="tx-right">{{ number_format($original + $interestMoney) }}</td>
            </tr>
        @endfor
        <tr>
            <td class="bg-black tx-center">Tổng số</td>
            <td></td>
            <td class="tx-right"><strong>{{ number_format($sumOriginal) }}</strong></td>
            <td class="tx-right"><strong>{{ number_format($sumInterest) }}</strong></td>
            <td class="tx-right"><strong>{{ number_format($sumMoney) }}</strong></td>
        </tr>
    </tbody>
</table>

<style>
    .tx-center{
        text-align: center;
        position: -webkit-sticky;
        position: sticky;
        top: 0;
        z-index: 10;
        background-color: #333;
        color: #fff;
    }

    .tx-right{
        text-align: right;
    }
</style>