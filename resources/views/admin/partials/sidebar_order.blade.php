<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        @include('admin.partials.info')

        @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="{{ Request::is('/order-customer') ? 'active' : null }} ">
                    <a href="{{ route('order-customer.index') }}">
                        <i class="fa fa-shopping-basket" aria-hidden="true"></i> <span>Đơn hàng</span>
                    </a>
                </li>

            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
