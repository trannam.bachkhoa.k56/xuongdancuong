<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
    @include('admin.partials.info')
		
        @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                @if(\Illuminate\Support\Facades\Auth::user()->role )
                    <li class="{{ Request::is('admin/') ? 'active' : null }}  treeview">
                        <a><i class="fa fa-free-code-camp" aria-hidden="true"></i> <span>Chiến dịch khách hàng</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="">
                                <a href="{{route('campaign-customer.create')}}"><i class="fa fa-circle-o">
                                </i>Thêm mới Chiến dịch</a>
                            </li>
                            <li class="">
                                <a href="{{route('campaign-customer.index')}}"><i class="fa fa-circle-o"></i>
                                    Danh sách Chiến dịch
                                </a>
                            </li>
                           <!--  <li class="">
                                <a href="{{route('list_mail_automation')}}"><i class="fa fa-circle-o"></i>
                                    Email chiến dịch khách hàng
                                </a>
                            </li> -->
                        </ul>
                    </li>

                   <!--  <li class="{{ Request::is('admin/') ? 'active' : null }}  treeview">
                        <a href=""><i class="fa fa-handshake-o" aria-hidden="true"></i> <span>Cơ hội khách hàng</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="">
                                <a href="{{route('opportunity.create')}}"><i class="fa fa-circle-o">
                                </i>Thêm mới cơ hội</a>
                            </li>
                            <li class="">
                                <a href="{{route('opportunity.index')}}"><i class="fa fa-circle-o"></i>
                                    Danh sách cơ hội
                                </a>
                            </li>
                        </ul>

                    </li> -->
                @endif


                @if(\App\Entity\User::isManager(\Illuminate\Support\Facades\Auth::user()->role))
                <li class="header">CSKH</li>

                <li class="{{ Request::is('admin/quan-ly-email') ? 'active' : null }} ">
                    <a href="{{ route('manager-email') }}">
                        <i class="fa fa-envelope-open-o" aria-hidden="true"></i> <span>Thống kê email của bạn</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/sendEmail') ? 'active' : null }} ">
                    <a href="{{ route('sendEmail') }}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Gửi Email</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/settingSendEmail') ? 'active' : null }} ">
                    <a href="{{ route('settingSendEmail') }}">
                        <i class="fa fa-calendar" aria-hidden="true"></i> <span>Đặt lịch gửi Email</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/SettingCampaign') ? 'active' : null }} ">
                    <a href="{{ route('SettingCampaign') }}">
                        <i class="fa fa-cog" aria-hidden="true"></i> <span>Cấu hình Chiến dịch </span>
                    </a>
                </li> 

                <li class="{{ Request::is('admin/settingCustomer') ? 'active' : null }} ">
                    <a href="{{ route('settingCustomer') }}">
                       <i class="fa fa-user-plus" aria-hidden="true"></i> <span>Cấu hình khách hàng mới</span>
                    </a>
                </li>

                    @if(\Illuminate\Support\Facades\Auth::user()->role >=4 )
                        <li class="{{ Request::is('admin/settingCustomer') ? 'active' : null }}  treeview ">
                            <a href="{{ route('settingCustomer') }}">
                             <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                 <span>Mẫu email</span>
                               <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="">
                                    <a href="{{route('group-template.index')}}"><i class="fa fa-circle-o"></i>Tất cả Mẫu </a>
                                </li>
                                <li class="">
                                    <a href="{{route('group-template.create')}}"><i class="fa fa-circle-o"></i>Thêm mới Mẫu Email</a>
                                </li>
                            </ul>
                        </li>
                    @endif

                @endif

                <li class="header">Quản lý Khách hàng giới thiệu</li>
                @if(\Illuminate\Support\Facades\Auth::user()->role != 4 )
                    <li class="{{ Request::is('admin/list-customer-introduce') ? 'active' : null }} ">
                        <a href="{{ route('detail_customer') }}">
                            <i class="fa fa-address-book-o" aria-hidden="true"></i>
                            <span>Danh sách Giới thiệu của tôi </span>
                        </a>
                    </li>
                @else
                    <li class="{{ Request::is('admin/list-customer-introduce') ? 'active' : null }} ">
                        <a href="{{ route('list_customer_introduce') }}">
                            <i class="fa fa-address-book-o" aria-hidden="true"></i>
                            <span>Danh sách Giới thiệu</span>
                        </a>
                    </li>

                    <li class="{{ Request::is('admin/list-customer-introduce') ? 'active' : null }} ">
                        <a href="{{ route('request_money') }}">
                            <i class="fa fa-address-book-o" aria-hidden="true"></i>
                            <span>Yêu cầu rút tiền</span>
                        </a>
                    </li>
                @endif
            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
