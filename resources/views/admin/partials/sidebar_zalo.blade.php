<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
    @include('admin.partials.info')
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">nick zalo facebook</li>
				
				<li class="{{ Request::is('admin/cskhzalo') ? 'active' : null }}">
                    <a href="/admin/cskhzalo">
                        <i class="fa fa-star-o" aria-hidden="true"></i> <span>Mặc định</span>
                    </a>
                </li>
				
				@foreach ($zaloManagers as $zaloManager)
				<li class="{{ Request::is('admin/cskhzalo/'.$zaloManager->zalo_manager_id) ? 'active' : null }}">
                    <a href="/admin/cskhzalo/{{ $zaloManager->zalo_manager_id }}" >
                        <i class="fa fa-star-o" aria-hidden="true"></i> <span>{{ $zaloManager->name_zalo }}</span> 
                    </a>
                </li>
				@endforeach
				
				<li class="{{ Request::is('admin/cskhzalo/create') ? 'active' : null }}">
                    <a href="{{ route('cskhzalo.create') }}" >
                        <i class="fa fa-plus" aria-hidden="true"></i> <span>Thêm mới zalo</span>
                    </a>
                </li>
				
				
				
            </ul>
    </section>
    <!-- /.sidebar -->

</aside>
