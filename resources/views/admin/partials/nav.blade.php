<header class="main-header">
	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
		<span class="sr-only"></span>
	</a>
    <!-- Logo -->
    <a href="/" class="logo hideMB" target="_blank">
       Xem website
    </a>
    <a href="{{ url()->previous() }}" class="logo hiddenPC">
        <i class="fa fa-hand-o-left" aria-hidden="true" style="float:left; margin-top: 15px; margin-left: 21px;"></i>  @yield('title')
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" id="navMb">
        <!-- Sidebar toggle button-->
        <ul class="nav navbar-nav">
            <li class="{{ ($menuTop == 'products') ? 'active' : '' }}" id="stepHome1" >
                <a target="" href="{{ route('products.index') }}">Dịch vụ (sản phẩm)</a>
            </li>
			@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role) && \Illuminate\Support\Facades\Auth::user()->role == 3)
			<li class="{{ ($menuTop == 'orders') ? 'active' : '' }}"  id="stepHome2" >
				<a target="" href="{{ route('order-customer.index') }}">ĐƠN HÀNG</a>
			</li>
			@endif
			@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role) && \Illuminate\Support\Facades\Auth::user()->role == 4)
				<li class="{{ ($menuTop == 'orders') ? 'active' : '' }}"  id="stepHome2" >
					<a target="" href="/admin/don-hang">ĐƠN HÀNG</a>
				</li>
			@endif
            <li class="{{ ($menuTop == 'customers') ? 'active' : '' }}"  id="stepHome3" >
                <a target="" href="{{ route('contact_list') }}">Khách hàng</a>
            </li>
            <li class="{{ ($menuTop == 'cskh') ? 'active' : '' }}"  id="stepHome4" >
                <a target="" href="{{ route('manager-email') }}">CSKH</a>
            </li>
			<li class="{{ ($menuTop == 'communicate') ? 'active' : '' }}" id="stepHome5" >
                <a target="" href="{{ route('campaign-customer.index') }}">Marketing</a>
            </li>
			<li class="{{ ($menuTop == 'utilities') ? 'active' : '' }}" id="stepHome6" >
                <a target="" href="{{ route('utilities.index') }}">Tiện ích</a>
            </li>
			<li class="{{ ($menuTop == 'websites') ? 'active' : '' }}" id="stepHome7" >
                <a target="" href="/admin/information">WEBSITE</a>
            </li>
			<li class="{{ ($menuTop == 'help') ? 'active' : '' }}"  id="stepHome8" >
                <a target="" href="/admin/help">Đào tạo</a>
            </li>
            <li class="{{ ($menuTop == 'statistic') ? 'active' : '' }}" style="background-color: #f39c12" id="stepHome9">
                <a target="" href="/admin/home">Báo Cáo</a>
            </li>
            @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
            <li class="{{ ($menuTop == 'setting') ? 'active' : '' }}" >
                <a target="" href="{{ route('domains.index') }}">CÀI ĐẶT</a>
            </li>
			@endif
           
           <!--  <li class="{{ ($menuTop == 'facebook') ? 'active' : '' }}" id="stepHome7" >
                <a target="" href="{{ route('facebook_manager.index') }}">M. Xã hội</a>
            </li> -->

        </ul>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (!empty($domainUser))
                    <?php
                    $datetime1 = new DateTime();
                    $datetime2 = new DateTime($domainUser->end_at);
                    $interval = $datetime1->diff($datetime2);
                    ?>
                    @if ($interval->format('%a') <= 7 && \Illuminate\Support\Facades\Auth::user()->vip > 0)
                    <li style="color: red; background: #fff; padding: 0 50px;">
                        <a style="color: red; font-size: 24px; ">Theme của bạn chỉ còn {{ $interval->format('%a ngày') }} sử dụng</a>
                        <a href="/admin/nang-cap-tai-khoan" class="btn btn-danger">Gia Hạn</a>
                    </li>
                    @endif
                @endif
                {{--notification--}}
                

				<li class="btn btn-success" style="padding: 0;" id="" >

                    <a href="{{route('show_gift')}}" ><i class="fa fa-gift"aria-hidden="true"></i> Affiliate</a>
                </li>

				<!-- @if (\Illuminate\Support\Facades\Auth::user()->vip == 0)
    				<li >
                        <a  href="/admin/nang-cap-tai-khoan" class="btn btn-warning" > <i class="fa fa-arrow-up" aria-hidden="true"></i> Thay tên miền</a>
                    </li>
				@else
				<li>
                    <a  href="/admin/mua-them-dung-luong" class="btn btn-warning"> <i class="fa fa-arrow-up" aria-hidden="true"></i> GIA HẠN</a>
                </li>
				@endif -->
                <li class="" id="stepInformation3" >
                   <a target="_blank" title="Xem website" alt="Xem website" style="font-size: 20px;" href="{{ !empty($domainUser->domain) ? $domainUser->domain : !empty($domainUser->url) ? $domainUser->url : '/' }}">
				   <i class="fa fa-globe" aria-hidden="true"></i>
				   </a>
                </li>

				 <li class="" id="stepInformation4" >
                    <a  href="{{ route('update_enable_help') }}"> <i class="fa fa-question-circle" aria-hidden="true"></i></a>
                </li>
				<!-- Notifications: style can be found in dropdown.less -->
				  <li class="dropdown notifications-menu">
					<script>
                        function seenNotification(e) {
                            /* Khi click đọc thông báo thì cho số lượng thông báo về 0 */
                            $('#ajax_countRp').empty();
                            /*  Gọi ajax để db xử lý dữ liệu cho status về 1 */
                            $.ajax({
                                url: '{!! route('seenNotification') !!}',
                                method: 'get',
                                success: function(data){
                                },
                                error: function(){},
                            });

                            return true;
                        }
                    </script>
					<a href="#" alt="Thông báo" title="Thông báo" class="dropdown-toggle" data-toggle="dropdown">
					  <i class="fa fa-bell-o"></i>
						@if (!empty($countRp) || \App\Entity\RemindContact::remindContact()->count() > 0)
						<span id="ajax_countRp" class="label label-danger">
                            {!! ($countRp + \App\Entity\RemindContact::remindContact()->count()) !!}</span>
						@endif
					</a>

					<ul class="dropdown-menu">
						@if (!empty($countRp) || \App\Entity\RemindContact::remindContact()->count() > 0)
						<li class="header">
                            Bạn có {!! ($countRp + \App\Entity\RemindContact::remindContact()->count()) !!} thông báo
                        </li>
						@endif
					  <li>
                        
						<!-- inner menu: contains the actual data -->
						<ul class="menu">
							@foreach (\App\Entity\RemindContact::remindContact() as $remindContact)

                                <!--@if($remindContact->contact_idm == null)
                                <li>
                                    <a href="{{route('settingCustomer')}}">
                                      <i style="margin-top: 6px;" class="fa fa-users text-aqua"></i> {!! $remindContact->content !!}
                                    </a>
                                  </li>
                                @endif -->

								  <li>
									<a href="{{ route('contact.show', ['contact_id' => $remindContact->contact_id, 'remind' => 1]) }}">
									  <i style="margin-top: 6px;" class="fa fa-users text-aqua"></i> {!! $remindContact->content !!}
									</a>
								  </li>
							@endforeach
							<!-- 
							@if (!empty($notifications))
								@foreach($notifications as $ntf)
									<li class="@if($ntf->status == 0 || $ntf->status == 1) blue @else white @endif ">
										<a id="{{ $ntf->notify_id }}" href="{{$ntf->URL}}" onclick="return readNotification(this)" >{!! '<b>'. $ntf->title.'</b>'. " : " . $ntf->content . "<br/>" !!}</a>
									</li>
								@endforeach
							@endif -->
							{{--Bắt sự kiện click vào thông báo--}}
							<script>
								function readNotification(e) {
									var id = $(e).attr('id');
									/* Gọi ajax để db xử lý dữ liệu cho status về 2 */
									$.ajax({
										url: '{!! route('readNotification') !!}',
										method: 'get',
										data: {
											id: id
										},
										success: function(data){
										},
										error: function(){},
									});

									return true;
								}
							</script>
						</ul>
					  </li>
					  <li class="footer"><a href="{{ route('report') }}">Xem tất cả</a></li>
					</ul>
				  </li>
                {{--endreport--}}
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ (isset(Auth::user()->image) && !empty(Auth::user()->image)) ? asset(Auth::user()->image) : asset('image/avatar_admin.png') }}" class="user-image" alt="User Image">

                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ (isset(Auth::user()->image) && !empty(Auth::user()->image)) ? asset(Auth::user()->image) : asset('image/avatar_admin.png') }}" class="img-circle" alt="User Image">
                            <p>
                                {{Auth::user()->name}}
                                <small>{{Auth::user()->email}}</small>
                                <small>{{Auth::user()->phone}}</small>
                            </p>
                         
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('users.edit', ['id' => Auth::user()->id]) }}" class="btn btn-default btn-flat">Thông tin</a>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Thoát</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>

                        <li class="list_website"> 
                            <ul class="list-group list-group-flush">                                
                            @foreach(App\Entity\DomainUser::getDomain(Auth::user()->id) as $id => $domain)  
                            <?php
                                $encrypt = Crypt::encrypt([
                                    'random' => rand(1000000, 10000000),
                                    'domainId' => $domain->domain_id,
                                    'userId' =>  Auth::user()->id,
                                ]);
                             ?>     
                                <li class="list-group-item">                                       
                                    <div class="title">
                                        <a style="cursor: pointer;" href="{{ $domain->url }}/check-user-redirect/{{ $encrypt }}">
                                            <div class="name"><b>{{$domain->name}}</b>
                                            </div>
                                        </a>
                                        <div class="powerful">{{$domain->url}}</div>
                                      
                                    </div>                                        
                                </li>
                            @endforeach
                           
                            </ul>               
                         
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>

</header>


<aside class="SideBar hiddenPC main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview" id="" >
                <a target="" href="{{ route('products.index') }}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i> <span>Sản phẩm</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                <ul class="treeview-menu">
                    <li class="{{ Request::is('admin/products', 'admin/products/create') ? 'active' : null }} " id="" >
                        <a href="{{ route('products.index') }}">
                            <i class="fa fa-product-hunt" aria-hidden="true"></i> <span>Dich vụ hoặc Sản phẩm</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/category-products', 'admin/category-products/create') ? 'active' : null }} " id="" >
                        <a href="{{ route('category-products.index') }}">
                            <i class="fa fa-product-hunt" aria-hidden="true"></i> <span>Danh mục dịch vụ hoặc sản phẩm</span>
                        </a>
                    </li>
                </ul>
                @endif
            </li>
            <li class="treeview"  id="" >
                <a target="" href="{{ route('order-customer.index') }}">ĐƠN HÀNG
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a> 
            </li>
            <li class="treeview"  id="" >
                <a target="" href="{{ route('contact.index') }}">Khách hàng
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="treeview-menu" data-widget="tree">
                        <li class="{{ Request::is('admin/contact') ? 'active' : null }} ">
                            <a href="{{ route('contact.index') }}">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span>Quản lý khách hàng</span>
                            </a>
                        </li>
                        <!--<li class="{{ Request::is('admin/ipclients') ? 'active' : null }} ">
                            <a href="{{ '/admin/ipclients' }}">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i> <span>Quản lý ip truy cập</span>
                            </a>
                        </li>

                        <li class="{{ Request::is('admin/subcribe-email') ? 'active' : null }} ">
                            <a href="{{ route('subcribe-email.index') }}">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Đăng ký nhận email</span>
                            </a>
                        </li> -->
                        <li class="{{ Request::is('admin/comments') ? 'active' : null }} ">
                            <a href="{{ route('comments.index') }}">
                                <i class="fa fa-comments" aria-hidden="true"></i> <span>Quản lý bình luận</span>
                            </a>
                        </li>
                        
                        <li class="{{ Request::is('admin/do-luong-quang-cao-facebook') ? 'active' : null }} ">
                            <a href="/admin/do-luong-quang-cao-facebook">
                                <i class="fa fa-comments" aria-hidden="true"></i> <span>Phân tích facebook ads</span>
                            </a>
                        </li>

                        @if(\App\Entity\User::isManager(\Illuminate\Support\Facades\Auth::user()->role))
                            @if(\Illuminate\Support\Facades\Auth::user()->vip >= 1)
                        <li class="header">CSKH</li>
                        
                        <li class="{{ Request::is('admin/sendEmail') ? 'active' : null }} ">
                            <a href="{{ route('sendEmail') }}">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Gửi Email</span>
                            </a>
                        </li>

                        <li class="{{ Request::is('admin/settingSendEmail') ? 'active' : null }} ">
                            <a href="{{ route('settingSendEmail') }}">
                                <i class="fa fa-calendar" aria-hidden="true"></i> <span>Đặt lịch gửi Email</span>
                            </a>
                        </li>

                        <li class="{{ Request::is('admin/SettingCampaign') ? 'active' : null }} ">
                            <a href="{{ route('SettingCampaign') }}">
                                <i class="fa fa-cog" aria-hidden="true"></i> <span>Cấu hình Chiến dịch </span>
                            </a>
                        </li> 

                        <li class="{{ Request::is('admin/settingCustomer') ? 'active' : null }} ">
                            <a href="{{ route('settingCustomer') }}">
                               <i class="fa fa-user-plus" aria-hidden="true"></i> <span>Cấu hình khách hàng mới</span>
                            </a>
                        </li>
                            @endif
                        @endif

                        @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                            <li class="header">Thành viên</li>
                            <li class="{{ Request::is('admin/users', 'admin/users/create') ? 'active' : null }}">
                                <a href="{{ route('users.index') }}">
                                    <i class="fa fa-users" aria-hidden="true"></i> <span>Quản lý thành viên</span>
                                </a>
                            </li>
                        @endif
                    </ul>
                @endif
            </li>
            @if (!empty(\App\Entity\SettingGetfly::getBaseUrl()))
            <li class="treeview" >
                <a target="" href="{{ route('show_getfly') }}">CRM</a>
            </li>
            @endif
            <li class="treeview" id="">
                <a target="" href="{{ route('show_zalo') }}">Zalo</a>
            </li>

            <li class="treeview" id="" >
                <a target="" href="{{ route('posts.index') }}">WEBSITE
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="treeview-menu" data-widget="tree">
                        <li class="header">Menu Chính</li>
                        <li class="{{ Request::is('admin/posts', 'admin/posts/create') ? 'active' : null }}">
                            <a href="{{ route('posts.index') }}">
                                <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Tin tức</span>
                            </a>
                        </li>

                        <li class="{{ Request::is('admin/pages', 'admin/pages/create') ? 'active' : null }}">
                            @if ($themeCode != 'xhome')
                            <a href="{{ route('pages.index') }}">
                                <i class="fa fa-file-o" aria-hidden="true"></i> <span>Trang</span>
                            </a>
                            @endif
                        </li>

                        <li class="header">Bổ sung</li>
                        @foreach($typeSubPostsAdmin as $typeSubPost)
                            <li class="{{ Request::is('admin/'.$typeSubPost->slug.'/sub-posts', 'admin/'.$typeSubPost->slug.'/sub-posts/create') ? 'active' : null }}">
                                <a href="{{ route('sub-posts.index', ['typePost' => $typeSubPost->slug]) }}">
                                    <i class="fa fa-th-list" aria-hidden="true"></i><span>{{ $typeSubPost->title }}</span>
                                </a>
                            </li>
                        @endforeach
                        @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                        @endif
                        @if ($themeCode != 'xhome')
                        <li class="{{ Request::is( 'admin/menus' ) ? 'active' : null }}">
                            <a href="{{ route('menus.index') }}"><i class="fa fa-circle-o"></i>Tất cả menu</a>
                        </li>
                        @endif
                       
                        <li class="{{ Request::is( 'admin/information' ) ? 'active' : null }}">
                            <a href="{{ route('information.index') }}"><i class="fa fa-circle-o"></i>Nội dung website</a>
                        </li>

                        <li class="{{ Request::is('admin/hinh-thuc-thanh-toan') ? 'active' : null }} ">
                            <a href="{{ route('method_payment') }}">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Cấu hình</span>
                            </a>
                        </li>
                        <li class="{{ Request::is('admin/cau-hinh-giao-dien') ? 'active' : null }} ">
                            <a href="{{ route('config_template') }}">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Giao diện</span>
                            </a>
                        </li>
                    </ul>
                @endif
            </li>
            @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
            <li class="treeview" >
                <a target="" href="{{ route('domains.index') }}">CÀI ĐẶT
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="treeview-menu">

                        @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                            <li class="header" title="Dạng bài viết, trường dữ liệu, trường thông tin, template">Cài Đặt Website</li>
                            <li class="{{ Request::is( 'admin/type-sub-post' ) ? 'active' : null }}">
                                <a href="{{ route('type-sub-post.index') }}"><i class="fa fa-clipboard" aria-hidden="true"></i> Dạng bài viết</a>
                            </li>
                            <li class="{{ Request::is( 'admin/type-input' ) ? 'active' : null }}">
                                <a href="{{ route('type-input.index') }}"><i class="fa fa-keyboard-o" aria-hidden="true"></i> Trường dữ liệu</a>
                            </li>
                            <li class="{{ Request::is( 'admin/type-information' ) ? 'active' : null }}">
                                <a href="{{ route('type-information.index') }}"><i class="fa fa-info-circle" aria-hidden="true"></i> Trường Thông tin</a>
                            </li>
                            <li class="{{ Request::is( 'admin/templates' ) ? 'active' : null }}">
                                <a href="{{ route('templates.index') }}"><i class="fa fa-desktop" aria-hidden="true"></i> Template</a>
                            </li>

                            <li class="header" title="Dạng bài viết, trường dữ liệu, trường thông tin, template">Quản lý website hệ thống</li>
                            <li class="{{ Request::is( 'admin/domains' ) ? 'active' : null }}">
                                <a href="{{ route('domains.index') }}"><i class="fa fa-globe" aria-hidden="true"></i> Domains</a>
                            </li>
                            <li class="{{ Request::is( 'admin/group-theme' ) ? 'active' : null }}">
                                <a href="{{ route('group-theme.index') }}"><i class="fa fa-object-group" aria-hidden="true"></i> Nhóm theme</a>
                            </li>
                            <li class="{{ Request::is( 'admin/themes' ) ? 'active' : null }}">
                                <a href="{{ route('themes.index') }}"><i class="fa fa-snowflake-o" aria-hidden="true"></i> Theme</a>
                            </li>
                            <li class="{{ Request::is( 'admin/group-help-video' ) ? 'active' : null }}">
                                <a href="{{ route('group-help-video.index') }}"><i class="fa fa-object-group" aria-hidden="true"></i> Nhóm hướng dẫn</a>
                            </li>
                            <li class="{{ Request::is( 'admin/help-video' ) ? 'active' : null }}">
                                <a href="{{ route('help-video.index') }}"><i class="fa fa-object-group" aria-hidden="true"></i> Hướng dẫn</a>
                            </li>
                            <li class="{{ Request::is('admin/thay-doi-domain') ? 'active' : null }} ">
                                <a href="{{ route('change-domain') }}">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Thay đổi tên miền</span>
                                </a>
                            </li>
                            
                           @if(\Illuminate\Support\Facades\Auth::user()->role >= 4)
                            <li class="{{ Request::is('admin/settingCustomer') ? 'active' : null }} " >
                                <a href="{{ route('mail_user')}}">
                                    <i class="fa fa-envelope-open" aria-hidden="true"></i><span>Gia hạn Gói Email</span>
                                </a>
                            </li>
                            @endif
                        @endif
                    </ul>
                @endif
            </li>
            @endif
            
            <li class="treeview" id="" >
                <a target="" href="{{ route('communicate.index') }}">Marketing
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="treeview-menu" data-widget="tree">

                        <li class="header">Phân tích đối thủ</li>
                        <li class="{{ Request::is('admin/phan-tich-doi-thu') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.analytics') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Phân tích đối thủ</span>

                            </a>
                        </li>
                        <li class="header">CỘNG ĐỒNG MUA BÁN</li>
                        <li class="{{ Request::is('admin/google-ads') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.google_ads') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Google ads</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-shopee') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.shopee') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Shopee</span>

                            </a>
                        </li>
                        
                        <li class="{{ Request::is('admin/kenh-ban-hang-lazada') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.lazada') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>lazada</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-sendo') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.sendo') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Sendo</span>

                            </a>
                        </li>
                        
                        <li class="{{ Request::is('admin/kenh-ban-hang-tiki') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.tiki') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Tiki</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-voso') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.voso') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Vỏ sò</span>
                            </a>
                        </li>
                        
                        <li class="{{ Request::is('admin/kenh-ban-hang-muare') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.muare') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Muare</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-muaban') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.muaban') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Muaban</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-enbac') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.enbac') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Én bạc</span>

                            </a>
                        </li>
						
                        <li class="{{ Request::is('admin/kenh-ban-hang-raovatvietnamnet') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.raovatvietnamnet') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Rao Vặt Vietnamnet</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-chophien') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.chophien') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Chợ phiên</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-ndfloodinfo') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.ndfloodinfo') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>NDfloodinfo</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-muabanraovat') ? 'active' : null }} " id="" >
                            <a href="{{ route('communicate.muabanraovat') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>muabanraovat</span>

                            </a>
                        </li>
                    </ul>
                @endif
            </li>
            <li class="treeview" id="" >
                <a target="" href="{{ route('transport.index') }}">Vận chuyển
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="treeview-menu" data-widget="tree">
                        <li class="header">Các kênh vận chuyển</li>
                        <li class="{{ Request::is('admin/kenh-giao-hang-tiet-kiem') ? 'active' : null }} " id="" >
                            <a href="{{ route('transport.giaohangtietkiem') }}">
                                <i class="fa fa-motorcycle" aria-hidden="true"></i> <span>Giao hàng tiết kiệm</span>
                            </a>
                        </li>
                        <li class="{{ Request::is('admin/kenh-viettel-port') ? 'active' : null }} " id="" >
                            <a href="{{ route('transport.viettel') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng viettel</span>

                            </a>
                        </li>
                        <li class="{{ Request::is('admin/kenh-giao-hang-nhanh') ? 'active' : null }} " id="" >
                            <a href="{{ route('transport.giaohangnhanh') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng nhanh</span>

                            </a>
                        </li>
                    </ul>
                @endif
            </li>
            <li class="treeview" id="" >
                <a target="" href="{{ route('facebook_manager.index') }}">M. Xã hội</a>
            </li>
        </ul>
    </section>
</aside>