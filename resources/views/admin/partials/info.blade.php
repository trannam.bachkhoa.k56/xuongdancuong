<!-- Sidebar user panel -->
<div class="website-panel">
	<h3>Thông số website</h3>
	<p>{{Auth::user()->name}}</p>
	<p>Lượng truy cập: {{ $countViewsWebsite }}</p>
	<p>Khả năng thành công:
		@php
			$countTaskMain = \App\Entity\TaskHelp::where('user_id', \Illuminate\Support\Facades\Auth::id())->where('type', 'main')->count();
			$countTaskDay = \App\Entity\TaskHelp::where('user_id', \Illuminate\Support\Facades\Auth::id())->where('type', 'day')->count();

		echo $countTaskMain*3 + $countTaskDay*5;
		@endphp
	</p>
	@if (\Illuminate\Support\Facades\Auth::user()->vip < 1)
		<p><a href="/admin/nang-cap-tai-khoan" class="btn btn-danger" style="width: 100%; color: white;">Đột Phá Doanh Số!</a></p>
		@else
		<p><a href="" class="btn btn-success" style="width: 100%; color: white;">Nâng cấp <img src="https://tiva.vn/site/images/tich_xanh.png" width="20"></a></p>
	@endif
</div>
