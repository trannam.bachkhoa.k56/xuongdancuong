<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        @include('admin.partials.info')

        @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">

                <li class="{{ Request::is('admin/contact') ? 'active' : null }} ">
                    <a href="{{ route('contact_list') }}">
                        <i class="fa fa-users" aria-hidden="true"></i>
                        <span>Quản lý khách hàng</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/contact') ? 'active' : null }} ">
                   <a href="" data-toggle="modal" data-target="#modalQrCode">
                        <i class="fa fa-credit-card" aria-hidden="true"></i> <span>Tạo Thẻ Thành Viên</span>
                    </a>
                </li>
               
                <!--<li class="{{ Request::is('admin/ipclients') ? 'active' : null }} ">
                    <a href="{{ '/admin/ipclients' }}">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i> <span>Quản lý ip truy cập</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/subcribe-email') ? 'active' : null }} ">
                    <a href="{{ route('subcribe-email.index') }}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Đăng ký nhận email</span>
                    </a>
                </li> -->
				<li class="{{ Request::is('admin/comments') ? 'active' : null }} ">
                    <a href="{{ route('comments.index') }}">
                        <i class="fa fa-comments" aria-hidden="true"></i> <span>Quản lý bình luận</span>
                    </a>
                </li>
				
				<!-- <li class="{{ Request::is('admin/do-luong-quang-cao-facebook') ? 'active' : null }} ">
                    <a href="/admin/do-luong-quang-cao-facebook">
                        <i class="fa fa-comments" aria-hidden="true"></i> <span>Phân tích facebook ads</span>
                    </a>
                </li>
				<li class="{{ Request::is('admin/get-post-facebook') ? 'active' : null }} ">
                    <a href="/admin/get-post-facebook">
                        <i class="fa fa-facebook-official" aria-hidden="true"></i> <span>Đồng bộ facebook</span>
                    </a>
                </li> -->
				
                @if(\Illuminate\Support\Facades\Auth::user()->vip >= 1 )
                
                @if(\Illuminate\Support\Facades\Auth::user()->role )
                   <!--  <li class="{{ Request::is('admin/') ? 'active' : null }}  treeview">
                        <a href=""><i class="fa fa-free-code-camp" aria-hidden="true"></i> <span>Chiến dịch khách hàng</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="">
                                <a href="{{route('campaign-customer.create')}}"><i class="fa fa-circle-o">
                                </i>Thêm mới Chiến dịch</a>
                            </li>
                            <li class="">
                                <a href="{{route('campaign-customer.index')}}"><i class="fa fa-circle-o"></i>
                                    Danh sách Chiến dịch
                                </a>
                            </li>
                        </ul>
                    </li> -->

                   <!--  <li class="{{ Request::is('admin/') ? 'active' : null }}  treeview">
                        <a href=""><i class="fa fa-handshake-o" aria-hidden="true"></i> <span>Cơ hội khách hàng</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="">
                                <a href="{{route('opportunity.create')}}"><i class="fa fa-circle-o">
                                </i>Thêm mới cơ hội</a>
                            </li>
                            <li class="">
                                <a href="{{route('opportunity.index')}}"><i class="fa fa-circle-o"></i>
                                    Danh sách cơ hội
                                </a>
                            </li>
                        </ul>

                    </li> -->
                    @endif
                @endif

                @if (!empty(\App\Entity\SettingGetfly::getBaseUrl()))
                    <li class="header">CRM</li>

                    @if (!empty(\App\Entity\SettingGetfly::getBaseUrl()))
                    <li class="{{ ($menuTop == 'getfly') ? 'active' : '' }}" >
                        <a target="" href="{{ route('show_getfly') }}">CRM</a>
                    </li>


                    @endif
                @endif
               

                @if(\App\Entity\User::isManager(\Illuminate\Support\Facades\Auth::user()->role))
					@if(\Illuminate\Support\Facades\Auth::user()->vip >= 1)
              <!--   <li class="header">CSKH</li>

                <li class="{{ Request::is('admin/quan-ly-email') ? 'active' : null }} ">
                    <a href="{{ route('manager-email') }}">
                        <i class="fa fa-envelope-open-o" aria-hidden="true"></i> <span>Thống kê email của bạn</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/sendEmail') ? 'active' : null }} ">
                    <a href="{{ route('sendEmail') }}">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Gửi Email</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/settingSendEmail') ? 'active' : null }} ">
                    <a href="{{ route('settingSendEmail') }}">
                        <i class="fa fa-calendar" aria-hidden="true"></i> <span>Đặt lịch gửi Email</span>
                    </a>
                </li>

                <li class="{{ Request::is('admin/SettingCampaign') ? 'active' : null }} ">
                    <a href="{{ route('SettingCampaign') }}">
                        <i class="fa fa-cog" aria-hidden="true"></i> <span>Cấu hình Chiến dịch </span>
                    </a>
                </li> 

                <li class="{{ Request::is('admin/settingCustomer') ? 'active' : null }} ">
                    <a href="{{ route('settingCustomer') }}">
                       <i class="fa fa-user-plus" aria-hidden="true"></i> <span>Cấu hình khách hàng mới</span>
                    </a>
                </li> -->
					@endif

                    @if(\Illuminate\Support\Facades\Auth::user()->role >=4 )
                      <!--   <li class="{{ Request::is('admin/settingCustomer') ? 'active' : null }}  treeview ">
                            <a href="{{ route('settingCustomer') }}">
                             <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                 <span>Mẫu email</span>
                               <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="">
                                    <a href="{{route('group-template.index')}}"><i class="fa fa-circle-o"></i>Tất cả Mẫu </a>
                                </li>
                                <li class="">
                                    <a href="{{route('group-template.create')}}"><i class="fa fa-circle-o"></i>Thêm mới Mẫu Email</a>
                                </li>
                            </ul>
                        </li> -->
                    @endif

                @endif

                @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                    <li class="header">Thành viên</li>
                    <li class="{{ Request::is('admin/users', 'admin/users/create') ? 'active' : null }} treeview">
                        <a href="{{ route('users.index') }}">
                            <i class="fa fa-users" aria-hidden="true"></i> <span>Quản lý thành viên</span>
                            <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ Request::is( 'admin/users' ) ? 'active' : null }}">
                                <a href="{{ route('users.index') }}"><i class="fa fa-circle-o"></i>Tất cả Thành viên</a>
                            </li>
                            <li class="{{ Request::is('admin/users/create') ? 'active' : null }}">
                                <a href="{{ route('users.create') }}"><i class="fa fa-circle-o"></i>Thêm mới thành viên</a>
                            </li>
                        </ul>
                    </li>
                @endif
                
				<li class="header">Quản lý Khách hàng giới thiệu</li>
				@if(\Illuminate\Support\Facades\Auth::user()->role != 4 )
					<li class="{{ Request::is('admin/list-customer-introduce') ? 'active' : null }} ">
						<a href="{{ route('detail_customer') }}">
						<i class="fa fa-address-book-o" aria-hidden="true"></i>
							 <span>Danh sách Giới thiệu của tôi </span>
						</a>                                                 
					</li>  
				@else
				<li class="{{ Request::is('admin/list-customer-introduce') ? 'active' : null }} ">
					<a href="{{ route('list_customer_introduce') }}">
					<i class="fa fa-address-book-o" aria-hidden="true"></i>
						 <span>Danh sách Giới thiệu</span>
					</a>                       
				</li>  
				
				<li class="{{ Request::is('admin/list-customer-introduce') ? 'active' : null }} ">
					<a href="{{ route('request_money') }}">
					<i class="fa fa-address-book-o" aria-hidden="true"></i>
						 <span>Yêu cầu rút tiền</span>
					</a>                       
				</li>  
				@endif             
            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
<!-- Modal -->
<div class="modal fade" id="modalQrCode" tabindex="-1" role="dialog" aria-labelledby="modalQrCodeShow">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalQrCodeShow">Tạo mã giảm giá</h4>
            </div>
            <form action="{!! route('create_qr_code') !!}" method="get">
                <div class="modal-body">
                    <h2>Tạo mã giảm giá</h2>
					<div class="form-group">
                        <label for="exampleInputEmail1">Tên thẻ</label>
                        <input type="text" class="form-control" name="name_card" placeholder="Tên thẻ" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Đường dẫn hướng tới</label>
                        <input type="text" class="form-control" name="link" placeholder="Đường dẫn hướng tới" required>
                    </div>

                    <!--<div class="form-group">
                        <label for="exampleInputEmail1">Hoa hồng trên toàn hệ thống (%)</label>
                        <input type="text" class="form-control" name="affiliate" value="{!! !empty(Auth::user()->affiliate_money) ? Auth::user()->affiliate_money :  '' !!}" placeholder="Hoa hồng trên toàn hệ thống" required>
                    </div> -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Tạo mã giảm giá</button>
                </div>
            </form>

        </div>
    </div>
</div>