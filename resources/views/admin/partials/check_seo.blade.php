	<div class="mm-portlet">
		<div class="mm-portlet__head">
			<div class="mm-portlet__head-label">
				<h3 class="mm-portlet__head-title">
					HỖ TRỢ SEO
				</h3>
			</div>
		</div>
		<div class="mm-portlet__body">	
			<div class="box-body" >
				 <div class="form-group">
					<label for="exampleInputEmail1">Từ khóa</label>
					<input type="text" class="form-control" name="meta_keyword" value="{{ isset($meta_keyword) ? $meta_keyword : '' }}" placeholder="Các từ khóa cách nhau bởi dấu ," />
				</div>
				<div class="form-group">
					<button class="btn btn-success" onClick="return checkSeo(this);"><i class="fa fa-check" aria-hidden="true"></i> kiểm tra chuẩn seo</button>
					<p id="checkSeo"></p>
				</div>
			</div>
		</div>
	</div>
<style>
	.red {
		color: red;
	}
	.green {
		color: green;
	}
	.yellow {
		color: orange;
	}
</style>
<script>
	function checkSeo(e) {
    $(e).attr('disabled', 'disabled');
    
    $.ajax({
            type: "POST",
            url: '/admin/check-seo',
            data: {
				title: $('#formPost').find('input[name=title]').val(),
				description: $('#formPost').find('textarea[name=description]').html(),
				content: $('#formPost').find('textarea[name=content]').html(),
				keyword: $('#formPost').find('input[name=meta_keyword]').val(),
				_token: $('#formPost').find('input[name=_token]').val(),
			},
            success: function(result){
                var obj = jQuery.parseJSON( result);
				/* gửi thành công */
				$('#checkSeo').html(obj.messages);
				$(e).removeAttr('disabled');
          },
          error: function(error) {
               $(e).removeAttr('disabled');
          }

        });

    return false;
  }
</script>
