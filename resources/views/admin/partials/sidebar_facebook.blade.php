<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        @include('admin.partials.info')
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Fanpage facebook</li>
                @foreach ($fanpages as $fanpage)
                <li class="{{ Request::is('fanpage-facebook/'.$fanpage->slug) ? 'active' : null }}">
                    <a href="{{ route('facebook_manager.detailFanpage', ['slug' => $fanpage->slug]) }}">
                        <i class="fa fa-facebook-official" aria-hidden="true"></i> <span>{{ $fanpage->name_fanpage }}</span>
                    </a>
                </li>
                @endforeach

            </ul>
    </section>
    <!-- /.sidebar -->

</aside>
