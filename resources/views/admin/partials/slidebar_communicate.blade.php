<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
    @include('admin.partials.info')
        @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <!-- <li class="header">Kênh vận chuyển </li>

                <li class="treeview" id=""  >
                    <a target="" href="{{ route('transport.index') }}"><i class="fa fa-truck"></i>  Vận chuyển 
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                        <!--<ul class="treeview-menu" data-widget="tree">
                            <li class="{{ Request::is('admin/kenh-giao-hang-tiet-kiem') ? 'active' : null }} " id="stepProductGeneral1" >
                                <a href="{{ route('transport.giaohangtietkiem') }}">
                                    <i class="fa fa-motorcycle" aria-hidden="true"></i> <span>Giao hàng tiết kiệm</span>
                                </a>
                            </li>
                            <li class="{{ Request::is('admin/kenh-viettel-port') ? 'active' : null }} " id="stepProductGeneral1" >
                                <a href="{{ route('transport.viettel') }}">
                                    <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng viettel</span>

                                </a>
                            </li>
                            <li class="{{ Request::is('admin/kenh-giao-hang-nhanh') ? 'active' : null }} " id="stepProductGeneral1" >
                                <a href="{{ route('transport.giaohangnhanh') }}">
                                    <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng nhanh</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                </li> -->
                
                <li class="header">Hỗ trợ</li>
                <li class="{{ Request::is('admin/thay-doi-url') ? 'active' : null }} ">
                    <a href="{{ route('show_change_url') }}" >
                        1. <span > Thay tên miền</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/email-marketing') ? 'active' : null }}">
                    <a href="{{ route('email_marketing') }}">
                        2. <span> Email marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/sms-marketing') ? 'active' : null }}">
                    <a href="{{ route('sms_marketing') }}">
                        3. <span> SMS marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/automation-marketing') ? 'active' : null }}">
                    <a href="{{ route('automation_marketing') }}">
                        4. <span> automation marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/telesale-marketing') ? 'active' : null }}">
                    <a href="{{ route('telesale_marketing') }}">
                        5. <span> telesale marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('/admin/optin-form', '/admin/optin-form/create') ? 'active' : null }} " id="stepProductGeneral2" >
                    <a href="{{ route('optin-form.index') }}">
                        6. <span>Form thông tin</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/google-adsword') ? 'active' : null }}">
                    <a href="{{ route('google_adsword') }}">
                        7. <span> Google adsword</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/google-shopping') ? 'active' : null }}">
                    <a href="{{ route('google_shopping') }}">
                        8. <span> Google shopping</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/seo-marketing') ? 'active' : null }}">
                    <a href="{{ route('seo_marketing') }}">
                        9. <span> seo marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/gmap-marketing') ? 'active' : null }}">
                    <a href="{{ route('gmap_marketing') }}">
                        10. <span> gmap marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/affiliate') ? 'active' : null }}">
                    <a href="{{ route('affiliate') }}">
                        11. <span> affiliate</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/moma-marketing') ? 'active' : null }}">
                    <a href="{{ route('moma_marketing') }}">
                        12. <span> Quảng cáo moma marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/facebook-marketing') ? 'active' : null }}">
                    <a href="{{ route('facebook_marketing') }}">
                        13. <span> Facebook marketing</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/app-mobile-marketing') ? 'active' : null }}">
                    <a href="{{ route('app_mobile_marketing') }}">
                        14. <span> App mobile marketing</span>
                    </a>
                </li>
				<li class="{{ Request::is('admin/crm') ? 'active' : null }}">
                    <a href="{{ route('crm') }}">
                        15. <span> CRM</span>
                    </a>
                </li>

				<li class="header">Chiến dịch bán hàng</li>
				<li class="{{ Request::is('admin/campaign-customer') ? 'active' : null }}">
					<a href="{{route('campaign-customer.index')}}">
                        <i class="fa fa-circle-o" aria-hidden="true"></i>
						Chiến dịch bán hàng
					</a>
				</li>
                <li class="{{ Request::is('/admin/optin-form', '/admin/optin-form/create') ? 'active' : null }} " id="stepProductGeneral2" >
                    <a href="{{ route('optin-form.index') }}">
                        <i class="fa fa-wpforms" aria-hidden="true"></i> <span>Form thông tin</span>
                    </a>
                </li>

                <li class="header">Báo cáo nhân viên</li>
                <li class="{{ Request::is('admin/bao-cao-nhan-vien', 'admin/bao-cao-doanh-so') ? 'active' : null }} treeview">
                    <a href="/admin/bao-cao-nhan-vien">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Báo cáo nhân viên</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::is( 'admin/bao-cao-doanh-so') ? 'active' : null }} ">
                            <a href="{{ route('report_revenue_employee') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Doanh số</span>
                            </a>
                        </li>
                        <li class="{{ Request::is( 'admin/bao-cao-nhan-vien') ? 'active' : null }}">
                            <a href="{{ route('report_employee') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Báo cáo kpi</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="header">KPI mục tiêu</li>
                <li class="{{ Request::is('admin/goals') ? 'active' : null }}" >
                    <a href="{{ route('goals.index') }}">
                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i> <sTpan>KPI mục tiêu</sTpan>

                    </a>
                </li>

                <li class="header">Chế độ nhân viên</li>
                <li class="{{ Request::is('admin/commission') ? 'active' : null }}" >
                    <a href="{{ route('commission.index') }}">
                        <i class="fa fa-money" aria-hidden="true"></i> <span>Cơ chế hoa hồng</span>

                    </a>
                </li>
                @if (empty($userIdPartner))
				<li class="header">Cộng sự</li>
				<li class="{{ Request::is('admin/show-add-user') ? 'active' : null }}">
                    <a href="{{ route('show_add_user') }}">
                        <i class="fa fa-user" aria-hidden="true"></i> <span>Cộng sự </span>
                    </a>
                </li>
                @endif
				
                <!-- <li class="header">Mạng Xã Hội</li>

                <li class="" id="stepHome7" >
                    <a target="" href="{{ route('facebook_manager.index') }}">
                        <i class="fa fa-facebook-square" aria-hidden="true"></i>  M. Xã hội 
                    </a>
                </li> 
             
                <li class="header">CỘNG ĐỒNG MUA BÁN</li>-->

                <!-- <li class="treeview menu-open " id="" >
                    <a target="" href=""><i class="fa fa-wifi"></i> Cộng đồng mua bán
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>

                    <ul class="treeview-menu" data-widget="tree">
                        <li class="{{ Request::is('admin/google-ads') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.google_ads') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Google ads</span>
                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-shopee') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.shopee') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Shopee</span>

                            </a>
                        </li>
        				
        				<li class="{{ Request::is('admin/kenh-ban-hang-lazada') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.lazada') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>lazada</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-sendo') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.sendo') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Sendo</span>

                            </a>
                        </li>
        				
        				<li class="{{ Request::is('admin/kenh-ban-hang-tiki') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.tiki') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Tiki</span>

                            </a>
                        </li>

        				<li class="{{ Request::is('admin/kenh-ban-hang-voso') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.voso') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Vỏ sò</span>
                            </a>
                        </li>
        				
                        <li class="{{ Request::is('admin/kenh-ban-hang-muare') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.muare') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Muare</span>
                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-muaban') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.muaban') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Muaban</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-enbac') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.enbac') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Én bạc</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-raovatvietnamnet') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.raovatvietnamnet') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Rao Vặt Vietnamnet</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-chophien') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.chophien') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>Chợ phiên</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-ndfloodinfo') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.ndfloodinfo') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>NDfloodinfo</span>

                            </a>
                        </li>

                        <li class="{{ Request::is('admin/kenh-ban-hang-muabanraovat') ? 'active' : null }} " id="stepProductGeneral1" >
                            <a href="{{ route('communicate.muabanraovat') }}">
                                <i class="fa fa-share" aria-hidden="true"></i> <span>muabanraovat</span>

                            </a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
