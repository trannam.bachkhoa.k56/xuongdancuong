<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
    @include('admin.partials.info')
    @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">

                @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                    <li class="header" title="Dạng bài viết, trường dữ liệu, trường thông tin, template">Cài Đặt Website</li>
                    <li class="{{ Request::is( 'admin/type-sub-post' ) ? 'active' : null }}">
                        <a href="{{ route('type-sub-post.index') }}"><i class="fa fa-clipboard" aria-hidden="true"></i> Dạng bài viết</a>
                    </li>
                    <li class="{{ Request::is( 'admin/type-input' ) ? 'active' : null }}">
                        <a href="{{ route('type-input.index') }}"><i class="fa fa-keyboard-o" aria-hidden="true"></i> Trường dữ liệu</a>
                    </li>
                    <li class="{{ Request::is( 'admin/type-information' ) ? 'active' : null }}">
                        <a href="{{ route('type-information.index') }}"><i class="fa fa-info-circle" aria-hidden="true"></i> Trường Thông tin</a>
                    </li>
                    <li class="{{ Request::is( 'admin/templates' ) ? 'active' : null }}">
                        <a href="{{ route('templates.index') }}"><i class="fa fa-desktop" aria-hidden="true"></i> Template</a>
                    </li>

                    <li class="header" title="Dạng bài viết, trường dữ liệu, trường thông tin, template">Quản lý website hệ thống</li>
                    <li class="{{ Request::is( 'admin/domains' ) ? 'active' : null }}">
                        <a href="{{ route('domains.index') }}"><i class="fa fa-globe" aria-hidden="true"></i> Domains</a>
                    </li>
                    <li class="{{ Request::is( 'admin/group-theme' ) ? 'active' : null }}">
                        <a href="{{ route('group-theme.index') }}"><i class="fa fa-object-group" aria-hidden="true"></i> Nhóm theme</a>
                    </li>
                    <li class="{{ Request::is( 'admin/themes' ) ? 'active' : null }}">
                        <a href="{{ route('themes.index') }}"><i class="fa fa-snowflake-o" aria-hidden="true"></i> Theme</a>
                    </li>
                    <li class="{{ Request::is( 'admin/group-help-video' ) ? 'active' : null }}">
                        <a href="{{ route('group-help-video.index') }}"><i class="fa fa-object-group" aria-hidden="true"></i> Nhóm hướng dẫn</a>
                    </li>
                    <li class="{{ Request::is( 'admin/help-video' ) ? 'active' : null }}">
                        <a href="{{ route('help-video.index') }}"><i class="fa fa-object-group" aria-hidden="true"></i> Hướng dẫn</a>
                    </li>
                    <li class="{{ Request::is('admin/thay-doi-domain') ? 'active' : null }} ">
                        <a href="{{ route('change-domain') }}">
                            <i class="fa fa-info-circle" aria-hidden="true"></i> <span>Thay đổi tên miền</span>
                        </a>
                    </li>
                    
                   @if(\Illuminate\Support\Facades\Auth::user()->role >= 4)
                    <li class="{{ Request::is('admin/settingCustomer') ? 'active' : null }} " >
                        <a href="{{ route('mail_user')}}">
                            <i class="fa fa-envelope-open" aria-hidden="true"></i><span>Gia hạn Gói Email</span>
                        </a>
                    </li>
                    @endif
                @endif
            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
