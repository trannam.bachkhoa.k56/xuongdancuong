<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
    @include('admin.partials.info')
    @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu Chính</li>
            <li class="{{ Request::is('admin/home') ? 'active' : null }}">
                <a href="/admin/home">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Báo cáo tổng</span>                       
                </a>
            </li>

            <li class="{{ Request::is('/admin/bao-cao-quang-cao-moma') ? 'active' : null }}">
                <a href="/admin/bao-cao-quang-cao-moma">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Báo cáo quảng cáo moma</span>                       
                </a>
            </li>
        </ul>
    @endif
    </section>
    <!-- /.sidebar -->

</aside>
