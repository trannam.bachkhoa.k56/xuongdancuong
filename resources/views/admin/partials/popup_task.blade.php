<div class="popupTask">
    <div class="popupTask-title">
        <h4>Nhiệm vụ</h4>
        <span onclick="return hideTask(this);"> Ẩn </span>
    </div>
    <div class="popupTask-body">
        <ul>
            @foreach (\App\Entity\SubPost::checkTaskMoma('nhiem-vu-chinh', 3) as $taskPopup)
            <li class="active">
                <input   type="checkbox" class="icheckbox_flat-green" onClick="return checkTask(this);" postId="{!! $taskPopup->post_id !!}" typeTask="main" /> <a href="{!! $taskPopup['link'] !!}"><span>[Chính]</span> {!! $taskPopup->title !!}</a>
            </li>
            @endforeach
            @foreach (\App\Entity\SubPost::checkTaskMoma('nhiem-vu-ngay', 3) as $taskPopup)
            <li>
                <input  type="checkbox" class="icheckbox_flat-green" onClick="return checkTask(this);" postId="{!! $taskPopup->post_id !!}" typeTask="day"/> <a href="{!! $taskPopup['link'] !!}"><span>[Ngày]</span> {!! $taskPopup->title !!}</a>
            </li>
            @endforeach
        </ul>
    </div>
</div>

<span class="iconShowHideTaskPopup"onclick="return showTask(this);"> Hiện </span>

<script>
    $(document).ready(function () {
        var showPop = localStorage.getItem("showPopup");

        if (showPop == 'hide') {
            $('.popupTask').hide();
            $('.iconShowHideTaskPopup').toggle( "slow");
        }
    });

    function hideTask() {
        $('.popupTask').hide();
        $('.iconShowHideTaskPopup').toggle( "slow");

        localStorage.setItem("showPopup", "hide");
    }
    function showTask() {
        $('.popupTask').toggle( "slow");
        $('.iconShowHideTaskPopup').hide();

        localStorage.setItem("showPopup", "show");
    }

    function checkTask (e) {
        var postId = $(e).attr('postId');
        var type = $(e).attr('typeTask');
        $.ajax({
            type: "GET",
            url: '{!! route('check_task') !!}',
            data: {
                post_id: postId,
                type: type
            },
            success: function(data){
                $(e).parent().toggle().remove();
                return true;
            }
        });
    }
</script>

<style>
    .popupTask {
        position: fixed;
        width: 250px;
        right: 0;
        top: 25%;
        z-index: 99999999;
        background: rgba(0,0,0,.8);
		display: none;
    }
    .popupTask-title {
        background: #f3c070;
        color: white;
        /* padding: 0px 15px; */
        text-align: center;
        position: relative;
        border-right: 15px solid #f38515b5;
        border-left: 15px solid #f38515b5;
    }
    .popupTask-title h4{
        margin-top: 0;
        margin-bottom: 0;
        margin-left: 2px;
        margin-right: 2px;
        border-right: 1px solid #f38515b5;
        padding: 10px 15px;
        color: #f38515b5;
        border-left: 1px solid #f38515b5;
        font-size: 30px;
    }
    .popupTask-title span{
        position: absolute;
        top: 0;
        left: -58px;
        color: white;
        background: #f38515b5;
        padding: 9px 12px;
        border: 1px solid #f38515b5;
        cursor: pointer;
    }
    .iconShowHideTaskPopup {
        background: #f38515b5;
        padding: 9px 12px;
        border: 1px solid #f38515b5;
        cursor: pointer;
        position: fixed;
        right: 0;
        top: 25%;
        color: white;
        display: none;
    }
    .popupTask-body {
        color: #e8b701;
    }
    .popupTask-body ul{
        list-style: none;
        padding: 0px 0px;
        margin-bottom: 0;
    }
    .popupTask-body ul li{
        border-bottom: 1px solid #e5e5e5;
        padding: 10px 14px;
        margin-top: 2px;
    }
    .popupTask-body ul li.active{
        border: 1px solid #f38515b5;
    }
    .popupTask-body a {
        color: #e8b701;
    }
</style>
