<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
    @include('admin.partials.info')
    @if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Các kênh vận chuyển</li>
                <li class="{{ Request::is('admin/kenh-giao-hang-tiet-kiem') ? 'active' : null }} " id="stepProductGeneral1" >
                    <a href="{{ route('transport.giaohangtietkiem') }}">
                        <i class="fa fa-motorcycle" aria-hidden="true"></i> <span>Giao hàng tiết kiệm</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/kenh-viettel-port') ? 'active' : null }} " id="stepProductGeneral1" >
                    <a href="{{ route('transport.viettel') }}">
                        <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng viettel</span>

                    </a>
                </li>
                <li class="{{ Request::is('admin/kenh-giao-hang-nhanh') ? 'active' : null }} " id="stepProductGeneral1" >
                    <a href="{{ route('transport.giaohangnhanh') }}">
                        <i class="fa fa-share" aria-hidden="true"></i> <span>Giao hàng nhanh</span>

                    </a>
                </li>
            </ul>
        @endif
    </section>
    <!-- /.sidebar -->

</aside>
