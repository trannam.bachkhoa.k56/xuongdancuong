@extends('admin.layout.admin')

@section('title', 'Ip')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Liên hệ
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Ip</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">STT</th>
                                <th>IP</th>
                                <th>Số lượt</th>
                                <th>Thời gian</th>
                                <th>urls</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ipClients as $id => $ipClient )
                                <tr>
                                    <td>{{ $ipClient->ip_client_id }}</td>
                                    <td>{{ $ipClient->ip }}</td>
                                    <td>{{ $ipClient->number }}</td>
                                    <td>{{ $ipClient->updated_at }}</td>
                                    <td>
                                        @foreach (explode(',', $ipClient->urls) as $url)
                                            <p>{{ $url }}</p>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table> 
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
	<script type="text/javascript">
    $(function() {
        var table = $('#example1').DataTable();
    });


</script>
@endsection

