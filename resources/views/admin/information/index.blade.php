@extends('admin.layout.admin')

@section('title', 'Thông tin website')

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Thông tin trang web
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Thông tin trang web</a></li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<!-- form start -->
		<form role="form" action="{{ route('information.store') }}" method="POST" id="form_info">
			{!! csrf_field() !!}
			{{ method_field('POST') }}

			<div class="col-xs-12 col-md-12">
				<!-- Nội dung thêm mới -->

				<!-- /.box-header -->
				@foreach($typeInformations as $typeinformation)
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">{{ $typeinformation->title }}</h3>
						</div>
						<div class="box-body" id="stepInformation1">
							<div class="form-group">
								<input type="hidden" value="{{ $typeinformation->slug }}" name="slug_type_input[]"/>
								<div class="row">
									<div class=col-xs-10>
										@if ($typeinformation->type_input == 'one_line')
										<input type="text" class="form-control" name="content[]" value="{{ $typeinformation->information }}"
										   placeholder="{{ $typeinformation->placeholder }}"
												/>
										@endif

										@if ($typeinformation->type_input == 'multi_line')
											<textarea rows="4" class="form-control" name="content[]" slug_type_input="{{ $typeinformation->slug }}"
													>{{ $typeinformation->information }}</textarea>
										@endif

										@if ($typeinformation->type_input == 'editor')
											<textarea class="editor" id="{{$typeinformation->slug}}" name="content[]"
													rows="10" cols="80" slug_type_input="{{ $typeinformation->slug }}"
													/>{{ $typeinformation->information }}</textarea>
										@endif

										@if ($typeinformation->type_input == 'image')
											<div>
												<input type="button" onclick="return uploadImage(this);" value="Chọn ảnh"
													size="20"/>
												<img src="{{ $typeinformation->information }}" width="80" height="70"/>
												<input name="content[]" type="hidden"
													value="{{ $typeinformation->information }}"/>
											</div>
										@endif
									</div>
									<div class=col-xs-2>
										<button type=" submit" class="btn btn-primary" onClick="return changeValue(this);" id="stepInformation2">Lưu</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach

							<!-- /.box-body -->

					<!-- /.box -->

			</div>
		</form>
	</div>
</section>
<script>
	function changeValue (e) {
		for (instance in CKEDITOR.instances) {
        	CKEDITOR.instances[instance].updateElement();
    	}
		var data = $('#form_info').serialize();
		
		 $.ajax({
            type: "POST",
            url: '{{route("information.store")}}',
            data: data,
            success: function(result){
                var obj = jQuery.parseJSON( result);
				alert('Lưu Thông tin thành công!')
              },
            error: function(error) {
            }

        });
		
	 	return false;
	}

	$(document).ready(function() {
		if (RegExp('multipage', 'gi').test(window.location.search)) {
			var intro = introJs();
			intro.setOptions({
				steps: [
					{
						element: document.querySelector('#stepInformation1'),
						intro: "Bạn có thể thay đổi thông tin như số điện thoại, email, ... "
					},
					{
						element: document.querySelector('#stepInformation2'),
						intro: "Ấn vào đây để lưu thay đổi thông tin trang."
					},
					{
						element: document.querySelector('#stepInformation3'),
						intro: "Để xem website của bạn hiển thị với khách hàng như thế nào thì ấn vào đây ..."
					},
					{
						element: document.querySelector('#stepInformation4'),
						intro: "Để xem lại hướng dẫn của chúng tôi, vui lòng ấn vào dây ..."
					},
				]
			});

			intro.start();

		}
	});
</script>
@endsection

