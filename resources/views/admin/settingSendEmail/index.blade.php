@extends('admin.layout.admin')

@section('title', 'đặt lịch gửi email')

@section('content')
    <section class="content-header">
        <h1>
            Đặt lịch gửi email
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Đặt lịch gửi email</li>
        </ol>
    </section>

    <div class="content">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box">
                    <div class="box-header">
                        <a  href="{{ route('showAddSettingSendEmail') }}">
                            <button class="btn btn-primary">Thêm mới</button> </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="emails" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tiêu đề</th>
                                <th>Nội dung </th>
                                <th>Ngày tạo</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        $(function() {
            $('#emails').DataTable({
                processing: true,
                serverSide: true,
                type: "POST",
                ajax: '{!! route('datatable_email') !!}',
                columns: [
                    { data: 'id', name: 'emailsetting.id' },

                    { data: 'subject', name: 'emailsetting.subject' },

                    { data: 'content', name: 'emailsetting.content' },

                    { data: 'updated_at', name: 'emailsetting.updated_at' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                ]
            });
        });


    </script>
@endsection
