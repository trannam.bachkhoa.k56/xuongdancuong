@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa')

@section('content')
    <section class="content-header">
        <h1>
            Chỉnh sửa lịch gửi email
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Chỉnh sửa gửi email</a></li>
            <li class="active">{{$email->subject}}</li>
        </ol>
    </section>
    <br>
    <div class="row">
        <div class="container">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary boxCateScoll">
                    <form action="{{route('update_mail',['id' => $email->id])}}" method="POST">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Chọn group</label>
                                    <select id="select_group" class="form-control" name="group">
                                        <option <?php if($email->group_id == null) { echo "selected";} ?> value="">Khách hàng mới</option>
                                        <option <?php if($email->group_id == 1) { echo "selected";} ?> value="1">Khách hàng Tiếp cận </option>
                                        <option <?php if($email->group_id == 2) { echo "selected";} ?> value="2">Khách hàng Mua hàng </option>
                                        <option <?php if($email->group_id == 3) { echo "selected";} ?> value="3">Khách hàng Hoàn đơn (Hoàn tiền)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Chọn hình thức gửi</label>
                                <div class="radio" style="padding-bottom: 15px;">

                                    <label>
                                        <input type="radio" name="type" id="optionsRadios1" value="1"  @if($email->type == 1) checked="checked" @endif >
                                        Chọn thời gian : ngày &emsp;
                                        <input type="date" name="date_send" value="{{$email->date_send}}" > &emsp;
                                        <!-- lúc &emsp;
                          <input type="text" name="time_send" value="{{$email->time_send}}" class="timepicker"> -->
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="type" id="optionsRadios2" value="2"  @if($email->type == 2) checked="checked"'@endif >

                                        Độ trễ sau &emsp;
                                        <input type="number" value="{{$email->date_delay}}" name="date_delay"> &emsp; ngày &emsp;
                                        <!--  <input type="number" value="{{$email->delay_time}}" name="delay_time">&emsp; giờ -->
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text" class="form-control" name="subject" value="{{$email->subject}}" placeholder="Tiêu đề">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Nội dung</label>
                                <textarea class="editor" id="content" name="content" rows="10" cols="80"/>{{$email->content}}</textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" id="stepProductCreate7">Chỉnh sửa</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#datepicker').datepicker({
                autoclose: true
            })
            $('.timepicker').timepicker({
                showInputs: false
            })
        })
    </script>
@endsection
