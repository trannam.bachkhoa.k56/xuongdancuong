@extends('admin.layout.admin')

@section('title', 'thêm mới')

@section('content')
    <section class="content-header">
        <h1>
            Thêm mới lịch gửi email
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Đặt lịch gửi email</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>
    <br>
    <div class="row">
        <div class="container">
            <div class="col-xs-12 col-md-8">
                <div class="box box-primary boxCateScoll">
                    <form action="{{route('save_setting_mail')}}" method="POST">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Chọn group</label>
                                    <select class="form-control" name="group">
                                        <option value="">Khách hàng mới</option>
                                        <option value="1">Khách hàng Tiếp cận </option>
                                        <option value="2">Khách hàng Mua hàng </option>
                                        <option value="3">Khách hàng Hoàn đơn (Hoàn tiền)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Hoặc tag khách hàng</label>
                                    <input type="text" class="form-control" id="tag" name="tag" onkeyup="autoComplete(this)" placeholder="Tag khách hàng">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Chọn hình thức gửi</label>
                                <div class="radio" style="padding-bottom: 15px;">
                                    <label>
                                        <input type="radio" name="type" id="optionsRadios1" value="1" checked="">
                                        Chọn thời gian : ngày &emsp;
                                        <input type="date" name="date_send" class="" > &emsp;
                                        <!--      lúc &emsp;
                                             <input type="text" name="time_send" class="timepicker"> -->
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="type" id="optionsRadios2" value="2">
                                        Độ trễ sau &emsp;
                                        <input type="number" name="date_delay"> &emsp; ngày &emsp;
                                        <!-- <input type="number" name="delay_time">&emsp; giờ -->
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text" class="form-control" name="subject" placeholder="Tiêu đề">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Nội dung</label>
                                <textarea class="editor" id="content" name="content" rows="10" cols="80"></textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" id="stepProductCreate7">Thêm mới</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#datepicker').datepicker({
                autoclose: true
            });
            $('.timepicker').timepicker({
                showInputs: false
            });
        });

        function autoComplete(e) {
            $.ajax({
                url: '{{ route('show-tag-customer') }}',
                type: 'GET',
                data:{
                    tag: $(e).val()
                },
                success: function (result) {
                    $(e).autocomplete({
                        source: result,
                        appendTo: "#someElem"
                    });
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    </script>
@endsection
