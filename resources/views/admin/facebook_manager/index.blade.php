@extends('admin.layout.admin')

@section('title', 'Cấu hình facebook')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cấu hình facebook
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cấu hình facebook</a></li>
        </ol>
    </section>
	@if(\Illuminate\Support\Facades\Auth::user()->vip < 1)
		<section class="content dragCustomer">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header with-border">
							<h1>Tìm kiếm khách hàng từ mạng xã hội</h1>
						</div>
						<div class="box-body">
							<p>Mạng xã hội là một ứng dụng giúp kết nối mọi người ở bất cứ đâu, là bất kỳ ai thông qua dịch vụ internet, giúp người dùng có thể chia sẻ những sở thích và trao đổi những thông tin cần thiết với nhau. Mạng xã hội dành cho mọi đối tượng sử dụng, không phân biệt giới tính, độ tuổi, vùng miền… Người dùng có thể liên kết với nhau dù họ ở bất cứ đâu, chỉ cần có internet. </p>
							<p>Ví dụ: facebook, google+, lotus, zalo oa, twister, intagram, youtube, ... </p>
							
						</div>
						<div class="box-footer">
							<a  href="/admin/nang-cap-tai-khoan" class="btn btn-primary">Nâng cấp đối tác vận chuyển </a>
						</div>
					</div>
				</div>
			</div>
		</section>	
	@else
		<section class="content">
			<div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('facebook_manager.config') }}" method="POST">
                {!! csrf_field() !!}

                <div class="col-xs-12 col-md-10">
                    <table id="filter" class="table table-striped table-bordered table-hover">
                        <tbody>
                        @foreach($fanpages as $fanpage)
                            <script type="text/javascript">
                                var filter_row = 0;

                                var html  = '<tr id="filter-row' + filter_row + '">';
                                html += '  <td class="text-left" style="width: 90%;">';
                                html += '  <div class="form-group">';
                                html += '<input type="text" name="name_fanpage[]" value="{{ $fanpage->name_fanpage }}" class="form-control" placeholder="Tên fanpage facebook của bạn. ví dụ: Moma website" >';
                                html += '<input type="text" name="link_fanpage[]" value="{{ $fanpage->link_fanpage }}" class="form-control" placeholder="Đường dẫn fanpage facebook của bạn. Ví dụ: https://www.facebook.com/taowebsitemienphimoma/" >';
                                html += '  </div>';
                                html += '  </td>';
                                html += '  <td ><button type="button" onclick="$(\'#filter-row' + filter_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
                                html += '</tr>';

                                $('#filter tbody').append(html);

                                filter_row++;
                            </script>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td style="background: #f4f4f4"><button type="button" onclick="addFilterRow();" data-toggle="tooltip" title="Add Filter" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                        </tr>
                        </tfoot>
                    </table>

                    <button type="submit" class="btn btn-primary" id="stepInformation2">Lưu thay đổi</button>
                </div>
            </form>
            <div class="col-xs-12 col-md-10">
            <h3><i>Chú ý: Để có thể sử dụng được module này: Vui lòng cài đặt extension của chrome: <br><a href="https://chrome.google.com/webstore/detail/ignore-x-frame-headers/gleekbfjekiniecknbkamfmkohkpodhe" target="_blank">Link cài đặt</a></i></h3>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        var filter_row = 0;

        function addFilterRow() {
            html  = '<tr id="filter-row' + filter_row + '">';
            html += '  <td class="text-left" style="width: 90%;">';
            html += '  <h3>Fanpage facebook của bạn</h3>';
            html += '  <div class="form-group">';
            html += '<input type="text" name="name_fanpage[]" value="" class="form-control" placeholder="Tên fanpage facebook của bạn. ví dụ: Moma website" required>'
            html += '<input type="text" name="link_fanpage[]" value="" class="form-control" placeholder="Đường dẫn fanpage facebook của bạn. Ví dụ: https://www.facebook.com/taowebsitemienphimoma/" required>'
            html += '  </div>';
            html += '  </td>';
            html += '  <td ><button type="button" onclick="$(\'#filter-row' + filter_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#filter tbody').append(html);

            filter_row++;
        }
    </script>	
	@endif
    
@endsection

