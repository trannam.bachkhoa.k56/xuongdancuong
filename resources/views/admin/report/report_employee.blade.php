@extends('admin.layout.admin')

@section('title', 'Báo cáo nhân viên')

@section('content')
    <style type="text/css">
        #frm_others_date{
            z-index: 99999;
            background: #eee;
            border: 1px solid #666;
            padding: 5px
        }
        #frm_others_date input{
            margin-bottom: 10px;
        }
        ul.filte_time li{
            display: inline-block;
            padding: 10px;
            cursor: pointer;
        }
        ul.filte_time {
            float: right;
        }
        .w-100{
            width: 100% !important;
        }
        .bg-light-gray {
            background: #D3D3D3;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="overflow-x: hidden;">
                <div class="box-header with-border">
                    <h1 class="box-title">Báo cáo nhân viên</h1>

                    <ul class="filte_time">
                        <li><a href="{{ route('report_employee', ['user_id' => isset($_GET['user_id']) ? $_GET['user_id'] : \Illuminate\Support\Facades\Auth::user()->id, 'month_report' => 'this_month' ]) }}" data-time="THIS_MONTH" class="btn_choose_time">Tháng này</a></li>
                        <li><a href="{{ route('report_employee', ['user_id' => isset($_GET['user_id']) ? $_GET['user_id'] : \Illuminate\Support\Facades\Auth::user()->id, 'month_report' => 'last_month' ]) }}" data-time="LAST_MONTH" class="btn_choose_time">Tháng trước</a></li>
                        <li>
                            <a data-time="TIME_REQUEST" onclick="return showModalTime();">
                                Thời gian khác
                            </a>
                            <div class="add-drop add-d-right" id="frm_others_date" style="left: auto; right: 0px; display:  none;position: absolute;">
                                <form action="{{ route('report_employee') }}" method="get">
                                    <s class="gf-icon-neotop"></s>
                                    <div class="padding">
                                        <p>Ngày bắt đầu</p>
                                        <input placeholder="Tháng tính kpi" name="month_report" type="month" class="datepick form-control hasDatepicker"> </div>

                                        <input type="hidden" value="{{ isset($_GET['user_id']) ? $_GET['user_id'] : \Illuminate\Support\Facades\Auth::user()->id }}" name="user_id"/>
                                    <div class="padding tl">
                                        <button type="submit" class="btn btn-info btn_choose_time" data-time="TIME_REQUEST" id="filterByOthersTime">Áp dụng</button>

                                        <button class="btn btn-default"  onclick="return showModalTime();" id="closeFilterOthersTime">Đóng</button>
                                    </div>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title">{{ $user->name }}</h3>

                    <div class="box-tools pull-right">
                        @if (empty($userIdPartner))
                        <div class="btn-group">
                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-wrench"></i> Lựa chọn nhân viên
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('report_employee', ['user_id' => \Illuminate\Support\Facades\Auth::user()->id ]) }}">Quản trị viên</a></li>
                                @foreach ($employees as $employee)
                                <li><a href="{{ route('report_employee', ['user_id' => $employee->id]) }}">{{ $employee->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <p class="text-center">
                                <strong>Mục tiêu KPI</strong>
                            </p>

                            <div class="progress-group">
                                <span class="progress-text">Số khách hàng</span>
                                <span class="progress-number"><b>{{ $contactCount }}</b>/{{ isset($goal->customers) ? $goal->customers : '' }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua" style="width: {{ isset($goal->customers) ? $contactCount/$goal->customers *100 : 0 }}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Số đơn hàng</span>
                                <span class="progress-number"><b>{{ $orderCustomerCountOrder }}</b>/{{ isset($goal->orders) ? $goal->orders : '' }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: {{  isset($goal->orders)  ? $orderCustomerCountOrder/$goal->orders *100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Số thanh toán</span>
                                <span class="progress-number"><b>{{ $orderCustomerCountPayment }}</b>/{{ isset($goal->purchases) ? $goal->purchases : '' }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-green" style="width: {{ isset($goal->purchases) ? $orderCustomerCountPayment/$goal->purchases *100 : 0}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Doanh số</span>
                                <span class="progress-number"><b>{{ number_format($orderCustomerRevenue, 0,",",".") }} đ</b>/{{ isset($goal->revenue) ? number_format($goal->revenue, 0,",",".") : 0  }} đ</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-yellow" style="width: {{ isset($goal->revenue) ? $orderCustomerRevenue/$goal->revenue *100 : 0 }}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <!--<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>-->
                                <h5 class="description-header">{{ number_format($orderCustomerPayment, 0,",",".") }}</h5>
                                <span class="description-text">Tổng thu</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <!-- <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span> -->
                                @php
                                    if (!empty($partner)) {
                                        $costTotal = $costOrder + $orderCustomerPayment*$partner->commission/100 + $partner->salary + $partner->sub_salary;
                                    } else {
                                        $costTotal = 0;
                                    }

                                @endphp
                                <h5 class="description-header">{{ number_format($costTotal, 0,",",".") }}</h5>
                                <span class="description-text">Tổng chi phí</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block border-right">
                                <!--<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span> -->
                                <h5 class="description-header">{{ number_format($orderCustomerPayment - $costTotal, 0,",",".") }}</h5>
                                <span class="description-text">Tổng lợi nhuận</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-3 col-xs-6">
                            <div class="description-block">
                                <h5 class="description-header"><span class="description-percentage text-red">{{ isset($goal->revenue) ? $orderCustomerPayment/$goal->revenue *100 : 0}}%</span></h5>
                                <span class="description-text">Hoàn thành mục tiêu</span>
                            </div>
                            <!-- /.description-block -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
            <div class="col-md-12">
                <div class="box">
                <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">STT</th>
                                    <th>Ngày đặt hàng</th>
                                    <th>khách hàng</th>
                                    <th>Doanh số</th>
                                    <th>Giá vốn</th>
                                    <th>phụ phí</th>
                                    <th>Đã thanh toán</th>
                                    <th>Công nợ</th>
                                    <th>Lợi nhuận</th>
                                    <th>Xóa</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orderCustomerUsers as $id => $orderCustomer)
                                    <tr>
                                        <td width="5%">{{ ($id+1) }}</td>
                                        <td>{{ date_format(date_create($orderCustomer->created_at), "d/m/Y H:i") }}</td>
                                        <td>{{ $orderCustomer->contact_name }}</td>
                                        <td>{{ number_format($orderCustomer->total_price, 0,",",".") }} đ</td>
                                        <td>{{ number_format($orderCustomer->cost, 0,",",".") }} đ</td>
                                        <td>{{ number_format($orderCustomer->surcharge, 0,",",".") }} đ</td>
                                        <td>{{ number_format($orderCustomer->payment, 0,",",".") }} đ</td>
                                        <td>{{ number_format(($orderCustomer->total_price - $orderCustomer->payment - $orderCustomer->surcharge ), 0,",",".") }} đ</td>
                                        <td>{{ number_format(($orderCustomer->total_price - $orderCustomer->cost - $orderCustomer->surcharge ), 0,",",".") }} đ</td>
                                        <td>
                                            <a  href="{{ route('order-customer.show', ['order_customer_id' => $orderCustomer->order_customer_id]) }}" class="btn btn-primary" >
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </a>
                                            <a  href="{{ route('order-customer.destroy', ['order_customer_id' => $orderCustomer->order_customer_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    @push('scripts')
    @endpush

@endsection
