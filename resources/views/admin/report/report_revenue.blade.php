@extends('admin.layout.admin')

@section('title', 'Báo cáo nhân viên')

@section('content')
    <style type="text/css">
        #frm_others_date{
            z-index: 99999;
            background: #eee;
            border: 1px solid #666;
            padding: 5px
        }
        #frm_others_date input{
            margin-bottom: 10px;
        }
        ul.filte_time li{
            display: inline-block;
            padding: 10px;
            cursor: pointer;
        }
        ul.filte_time {
            float: right;
        }
        .w-100{
            width: 100% !important;
        }
        .bg-light-gray {
            background: #D3D3D3;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <ul class="filte_time">
                        <form action="" method="get">
                            <li>Bắt đầu: <input placeholder="Tháng bắt đầu thống kê" name="date_start" type="month" class="datepick form-control hasDatepicker"></li>
                            <li>kết thúc: <input placeholder="Tháng kết thúc thống kê" name="date_end" type="month" class="datepick form-control hasDatepicker"></li>
                            <li><button type="submit" class="btn btn-success">Thống kê</button></li>
                        </form>
                    </ul>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BAR CHART -->
                            <div class="box box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Biểu đồ tăng trưởng nhân viên</h3>

                                </div>
                                <div class="box-body chart-responsive">
                                    <div class="chart" id="bar-chart" style="height: 500px;"></div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    @push('scripts')
        <script>
            $(function () {
                "use strict";
                var bar = new Morris.Bar({
                    element: 'bar-chart',
                    resize: true,
                    data: [
                    @for ($i = strtotime($dateStart); strtotime("+1 MONTH", $i) < strtotime($dateEnd); $i = strtotime("+1 MONTH", $i) )
                        {y: '{!! date("m", $i).'/'.date("Y", $i) !!}',
                        @foreach ($employees as $employee)
                        {!! $employee->id !!}: {!! \App\Entity\OrderCustomer::orderCustomerTotalOrderMonthYear($employee->id,  date("m", $i), date("Y", $i)) !!},
                        @endforeach
                        },
                    @endfor
                    ],
                    barColors: [
                        @foreach ($employees as $employee)
                                '#{!! rand(100000,999999) !!}',
                        @endforeach
                    ],
                            xkey: 'y',
                            ykeys: [
                        @foreach ($employees as $employee)
                                '{!! $employee->id !!}',
                        @endforeach
                    ],
                            labels: [
                        @foreach ($employees as $employee)
                                '{!! $employee->name !!}',
                        @endforeach
                    ],
                            hideHover: 'auto'
                });
            });

        </script>
    @endpush

@endsection
