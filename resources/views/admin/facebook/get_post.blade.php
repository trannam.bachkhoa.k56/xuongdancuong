@extends('admin.layout.admin')

@section('title', 'Đồng bộ facebook vào website của bạn')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Đồng bộ facebook vào website của bạn
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Đồng bộ facebook vào website của bạn</a></li>
        </ol>
    </section>
       
  
            

    <section class="content">
		
		<div class="box box-primary">
			<div class="box-header with-border">
				<a class="btn btn-primary form-control" style="cursor:pointer;color:#fff" href="https://moma.vn/dang-nhap-facebook?user_email={!! $emailUser !!}&current_url={!! \App\Ultility\Ultility::getCurrentDomain() !!}">Kết nối với facebook</a>
			</div>

            <div style="margin-top:10px;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#post" aria-controls="post" role="tab" data-toggle="tab">Bài viết tốt nhất về chủ đề của bạn</a>
                    </li>
                    <li role="presentation" >
                        <a href="#settingId" aria-controls="facebook" role="tab" data-toggle="tab">Trang bán hàng facebook của bạn!</a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="post">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Bài viết đang được ưu chuộng</h3>
                                </div>

                                @if (empty($feeds))
                                    <p>HIện tại chúng tôi đang không tìm thấy bất kỳ bài viết nào.</p>

                                @else
                                    <div class="row">
                                        <div class="col-xs-12 col-md-12">
                                            @foreach($feeds as $feed)
                                                <div class="box box-primary">
                                                    <div class="box-body">
                                                        <h4><img src="http://graph.facebook.com/{!! $feed['page']->id !!}/picture?type=square" width="70"> {{ $feed['page']->name }}</h4>
                                                        <p>{!! str_replace("\n","<br>", $feed['message']) !!}</p>
                                                        <p><img src="{!! $feed['picture'] !!}" style="width: 100%"/></p>
                                                        <p style="background: #e5e5e5;color: #365899;"><i class="fa fa-thumbs-up" aria-hidden="true"></i> {{ $feed['likes'] }}<br>
                                                            Có tất cả {{ $feed['comments'] }} bình luận
                                                        </p>
                                                        <p>Link: <a href="https://facebook.com/{{ $feed['id'] }}" target="_blank">https://facebook.com/{{ $feed['id'] }}</a></p>
                                                        <br>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                                <h4><i>Hướng dẫn sử dụng:</i></h4>
                                <p><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <i>Phần cấu hình: bổ sung mã truy cập, điền số lượng like tối thiểu, comment tối thiểu.</i></p>
                                <p><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <i>Phần nhóm facebook: bổ sung thêm những group mà bạn đã tham gia.</i></p>
                                <p><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> <i>Fanpage hoặc trang cá nhân: Điền thêm id, hoặc trang cá nhân của người dùng.</i></p>

                            </div>
                        </div>
                    </div>
                   
                    <div role="tabpanel" class="tab-pane " id="settingId">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Fanpage và trang cá nhân</h3>
                                <p><i>Lấy thông tin từ trang cá nhân và fanpage bạn quan tâm</i></p>
                            </div>
                            <div class="box-body">
                                <!-- <form action="{{ route('update_facebook_id') }}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="form-group col-xs-12 ">
                                        <label>Nhập vào ID facebook</label>

                                        <div class="input-group">
                                            <input type="number" class="form-control" name="face_id" placeholder="faceId" required>
                                            <span class="input-group-addon" id="basic-addon1" data-toggle="collapse" data-target="#helpGetId" aria-expanded="false" aria-controls="helpGetId">
                                            <i class="fa fa-question-circle" aria-hidden="true"></i> Hướng dẫn lấy id
                                        </span>
                                        </div>
                                        <div class="collapse" id="helpGetId">
                                            <div class="well">
                                                ...
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <button type="submit" class="btn btn-primary">Thêm ID</button>
                                    </div>
                                </form> -->
                                <div class="formGroupsFacebook" >

                                    @foreach ($faceInforByIds as $faceInforById)
                                        <div class="form-group col-xs-12">
                                            <i class="fa fa-check-square-o" aria-hidden="true"></i> <img src="http://graph.facebook.com/{!! $faceInforById['id'] !!}/picture?type=square" width="70"> {!! $faceInforById['name'] !!}
                                            <a href="{{ route('delete_facebook_id', ['face_id' => $faceInforById['id'] ]) }}" class="btn btn-danger">xóa</a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </section>
@endsection

