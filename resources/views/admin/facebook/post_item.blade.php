@extends('admin.layout.admin')

@section('title', 'Phát triển fanapge và trang cá nhân')

@section('content')
    <!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Phát triển fanapge và trang cá nhân
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Danh sách bài viết </a></li>
	</ol>
</section>
        <section class="content">
		
		<div class="box box-primary">
			<div id="post_fanpage">
				@foreach( $graphEdge as $graphNode )
					<div class="row" style="padding:10px;">
						<form method="POST" action="{{route('save_post_fanpage')}}">
						  {{ csrf_field() }}
							<div class="col-md-10">
								<a style="cursor:pointer" >
								{{ App\Ultility\Ultility::textLimit(isset($graphNode['message']) ? $graphNode['message'] : '', 35)}}
								{{ App\Ultility\Ultility::textLimit(isset($graphNode['story']) ? $graphNode['story'] : '', 35)}}
								</a>
							</div> 
							<input type="hidden" name="title" value="{{ isset($graphNode['story']) ? $graphNode['story'] : App\Ultility\Ultility::textLimit(isset($graphNode['message']) ? $graphNode['message'] : '', 20) }}">
							<input type="hidden" name="content" value="{{ isset($graphNode['message']) ? $graphNode['message'] : ''}}">
							<input type="hidden" name="slug" value="{{$graphNode['id']}}">
							<input type="hidden" name="access_token" value="{{$userToken}}">
							
							<div class="col-md-2">
							<button type="submit" class="btn btn-success">Sử dụng <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
							</div> 
						</form>
					</div> 
					<hr>
					
				@endforeach
			</div> 
		</div> 	

@endsection