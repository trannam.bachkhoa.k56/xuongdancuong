@extends('admin.layout.communicate')

@section('title', 'kênh bán hàng google ads')

@section('content')
<section class="content dragCustomer">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3>Hướng dẫn chạy quảng cáo google ads</h3>
				</div>
				<div class="box-body">
					<div class="col-12 col-lg-6">
						<iframe width="100%" height="400" src="https://www.youtube.com/embed/McreBTSfPF4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						
						<h4>1. Hướng dẫn tạo tài khoản chạy google ads</h4>
					</div>
					<div class="col-12 col-lg-6">
						<iframe width="100%" height="400" src="https://www.youtube.com/embed/QCyaSi8fZLU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						
						<h4>2. Hướng dẫn setup chiến dịch Quảng Cáo google</h4>
					</div>
				</div>
				
				@if(\Illuminate\Support\Facades\Auth::user()->vip < 1)
				<div class="box-footer">
					<a  href="/admin/nang-cap-tai-khoan" class="btn btn-primary">Nâng cấp bán hàng đa kênh </a>
				</div>
				@endif
			</div>
		</div>
	</div>
</section>

<section class="GoogleAds pd2-3p">
	<div class="titleGoogleAds textCenter">
		<p class="textUpper fw7 f24"> Tối ưu hóa quảng cáo của bạn </p>
		<p>Sử dụng nền tảng của chúng tôi sẽ giúp cải thiện hiệu quả quảng cáo của bạn.</p>
	</div>
	<div class="ContentGoogleAds mgt40">
		<div class="row">
        <div class="col-lg-6 col-xs-12">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Tối ưu Google Ads</h3>
              <p>Dịch vụ Google Ads – Quảng cáo từ khóa là dịch vụ marketing hàng đầu mà chúng tôi đang làm cho trên 100 đối tác trên khắp các tỉnh thành cả nước đem lại hiệu quả vượt trội. Gói dịch vụ bao gồm : setup quảng cáo, chạy từ khóa hiệu quả nhất, theo dõi tối ưu quảng cáo giúp cửa hàng, doanh nghiệp tiết kiệm chi phí  nhất có thể.</p>
            </div>
            <a href="#" class="small-box-footer" data-toggle="modal" data-target="#myModal">
              Xem chi tiết <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Chặn click ảo<sup style="font-size: 20px"></sup></h3>

              <p>Click ảo ư? Đừng lo vì chúng tôi cung cấp phần mềm tự động chặn click ảo. Giảm chi phí quảng cáo cho cửa hàng, doanh nghiệp  một cách hiệu quả nhất.
			 </p>
            </div>
      <!--      <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>   -->
            <a href="#" class="small-box-footer" data-toggle="modal" data-target="#myModal">
              Xem chi tiết <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Remarketing</h3>

              <p>Tiếp thị lại cho khách hàng đã vào website của bạn là hình thức Marketing hiệu quả và tiết kiệm chi phí nhất.  Bắt kịp xu hướng chúng tôi đã và đang cung cấp dịch vụ Remarketing cho cửa hàng doanh nghiệp với hiệu quả cao, tiết kiệm chi phí.</p>
            </div>
            <a href="#" class="small-box-footer" data-toggle="modal" data-target="#myModal">
              Xem chi tiết <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-light-blue">
            <div class="inner">
              <h3>Google Shopping</h3>

              <p>Chỉ với vài thao tác đơn giản, sản phẩm trên website của bạn sẽ tự động đồng bộ lên Google Merchant Center và triển khai chiến dịch quảng cáo ngay tức thì.</p>
            </div>
       <!--     <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>   -->
            <a href="#" class="small-box-footer" data-toggle="modal" data-target="#myModal">
              Xem chi tiết <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
		
		<div class="col-lg-6 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Chatbot</h3>

              <p>Sử dụng chatbot là hình cách tốt nhất để không bỏ sót khách hàng hay việc phản hồi trậm trễ với khách hàng sẽ không bao giờ xảy ra nữa. Từ đó khách hàng sẽ cảm thấy dịch vụ của công ty doanh nghiệp bạn thất tốt. Hãychokháchhàngthấysựchuyênnghiệpvànhanhchóngcủabạn bằng cách cài đặt chatbot của chúng tôi.</p>
            </div>
       <!--     <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>   -->
            <a href="#" class="small-box-footer" data-toggle="modal" data-target="#myModal">
              Xem chi tiết <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
      </div>
	</div>
</section>
<!-- MODAL -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tối Ưu Hóa Quảng Cáo Của Bạn</h4>
      </div>
      <div class="modal-body">
        <div id="getfly-optin-form-iframe-1569720945118"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var regex = /(https?:\/\/.*?)\//g; var furl = regex.exec(r); r = furl ? furl[0] : r; var f = document.createElement("iframe"); const url_string = new URLSearchParams(window.location.search); var utm_source, utm_campaign, utm_medium, utm_content, utm_term; if((!url_string.has('utm_source') || url_string.get('utm_source') == '') && document.cookie.match(new RegExp('utm_source' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_source' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_source') != null ? "&utm_source=" + url_string.get('utm_source') : "";} if((!url_string.has('utm_campaign') || url_string.get('utm_campaign') == '') && document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_campaign') != null ? "&utm_campaign=" + url_string.get('utm_campaign') : "";} if((!url_string.has('utm_medium') || url_string.get('utm_medium') == '') && document.cookie.match(new RegExp('utm_medium' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_medium' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_medium') != null ? "&utm_medium=" + url_string.get('utm_medium') : "";} if((!url_string.has('utm_content') || url_string.get('utm_content') == '') && document.cookie.match(new RegExp('utm_content' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_content' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_content') != null ? "&utm_content=" + url_string.get('utm_content') : "";} if((!url_string.has('utm_term') || url_string.get('utm_term') == '') && document.cookie.match(new RegExp('utm_term' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_term' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_term') != null ? "&utm_term=" + url_string.get('utm_term') : "";} f.setAttribute("src", "https://sachvidan.getflycrm.com/api/forms/viewform/?key=4xQoO6RayHAI4LRILH9WbQQpwjxyWrkk5c3Z8iMMCD6AGT62Kl&referrer="+r); f.style.width = "100%";f.style.height = "450px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1569720945118");s.appendChild(f); })(); </script>
      </div>
     
    </div>
  </div>
</div>




<style>
	.small-box h3{
		font-size: 24px ;
	}
	.small-box > .small-box-footer {
		padding: 5px 0;
	}
	.textCenter{
		text-align: center;
	}
	.textUpper{
		text-transform: uppercase;
	}
	.fw7{
		font-weight: 700;
	}
	.f24{
		font-size: 24px;
	}
	.mgb0{
		margin-bottom: 0;
	}
	.mgt40{
		margin-top: 40px;
	}
	.pd1-3p{
		padding: 2% 3%;
	}
</style>
@endsection

