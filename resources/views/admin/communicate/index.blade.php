@extends('admin.layout.communicate')

@section('title', 'kênh bán hàng hàng online')

@section('content')
	<section class="content dragCustomer">
        <div class="row">
            <div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
						<h1>Bán hàng đa kênh là gì? Tại sao lại cần Tiếp thị đa kênh?</h1>
					</div>
					<div class="box-body">
						<p>Lợi ích đầu tiên mà bạn sẽ nhận được khi sử dụng dịch vụ của Moma là bạn sẽ phát triển kinh doanh thông qua việc phát triển nhiều kênh bán hàng.</p>
						<p>Doanh nghiệp của bạn sẽ phát triển thế nào nếu bạn có 30 kênh bán hàng thụ động?</p>
						<p>Thế nhưng hầu hết các doanh nghiệp không dám chi trả cho 1 sản phẩm giúp bạn bán hàng 30 kênh?</p>
						<p>Tuy nhiên với Moma, chúng tôi giúp bạn thực hiện được việc này bằng cách giúp đỡ các chủ doanh nghiệp lấy thông tin khách hàng từ nhiều nơi. </p>
						<p>Moma giúp đỡ các chủ doanh nghiệp kinh doanh dựa trên việc theo dõi lịch sử khách hàng cũ.</p>
						<h3><i>Chú ý: Để có thể sử dụng được module này: Vui lòng cài đặt extension của chrome: <br><a href="https://chrome.google.com/webstore/detail/ignore-x-frame-headers/gleekbfjekiniecknbkamfmkohkpodhe" target="_blank">Link cài đặt</a></i></h3>
					</div>
					<!--@if(\Illuminate\Support\Facades\Auth::user()->vip < 1)
					<div class="box-footer">
						<a  href="/admin/nang-cap-tai-khoan" class="btn btn-primary">Nâng cấp bán hàng đa kênh </a>
					</div>
					@endif -->
				</div>
			</div>
		</div>
	</section>	
@endsection

