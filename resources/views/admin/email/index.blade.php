@extends('admin.layout.admin')

@section('title', 'Thống kê email của bạn')

@section('content')
	<style>
		#frm_others_date input{
			z-index: 99999;
			background: #eee;
			border: 1px solid #666;
			padding: 5px;
			margin-bottom: 10px;
		}
		ul.filter_time li{
			display: inline-block;
			padding: 10px;
			cursor: pointer;
		}
		ul.filter_time {
			float: right;
		}
		.bg-light-gray {
			background: #D3D3D3;
		}
	</style>

	<section class="content-header">
		<h1>
			Thống kê email của bạn
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Thống kê email của bạn </li>
		</ol>
	</section>

	<div class="content">
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="box">
					<div class="box-header with-border">
						<ul class="filter_time">
							<li><a data-time="THIS_DAY" onclick="filterByTime(this)">Hôm nay</a></li>
							<li><a data-time="THIS_WEEK" onclick="filterByTime(this)">Tuần này</a></li>
							<li><a data-time="LAST_WEEK" onclick="filterByTime(this)">Tuần trước</a></li>

							<li><a data-time="THIS_MONTH" onclick="filterByTime(this)">Tháng này</a></li>
							<li><a data-time="LAST_MONTH" onclick="filterByTime(this)">Tháng trước</a></li>

							<li><a data-time="THIS_YEAR" onclick="filterByTime(this)">Năm nay</a></li>
							<li><a data-time="LAST_YEAR" onclick="filterByTime(this)">Năm trước</a></li>
						</ul>
					</div>
					<div class="box-body">
						<table id="emails" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th> Tổng số email bạn có </th>
								<th> Tổng số email bạn đã gửi </th>
								<th> Tổng số email khách hàng đã đọc </th>
								<th> Tổng số email khách hàng chưa đọc </th>
								<th> Hạn sử dụng </th>
							</tr>
							</thead>
							<tbody>
							<tr id="data_email">
								<td> {{ $totalMail }} </td>
								<td> {{ $emailSend }} </td>
								<td> {{ $emailSeen }} </td>
								<td> {{ $emailSend - $emailSeen }}</td>
								<td> {{ $lastDayOfLastMonth}} </td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('scripts')
<script>
	function showModalTime(){
		$('#frm_others_date').toggle(400)
	}

	function filterByTime(e){
		$(e).parent().parent().find($('.bg-light-gray')).removeClass('bg-light-gray');
		$(e).parent().addClass('bg-light-gray');
		$.ajax({
			url: ' {{ route('email-statistics') }} ',
			type: 'POST',
			data: {
				time: $(e).attr('data-time'),
			},
			success: function (data) {
				$('#data_email').html(data);
			},
			error: function (errol) {
				console.log('Đã có lỗi xảy ra ' + errol);
			}
		});
	}
</script>
@endpush
