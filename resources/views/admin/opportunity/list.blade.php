@extends('admin.layout.admin')

@section('title', 'Danh sách Cơ hội')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Cơ hội
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Cơ hội</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <a href="{{ route('opportunity.create') }}" >
                                    <button class="btn btn-primary" id="stepProductGeneral3" data-step="1" data-intro="Ấn vào đây nếu bạn muốn thêm mới sản phẩm, ...">Thêm mới Cơ hội</button></a>
                            </div>                          
                        </div>
                       
                        <div class="box-body">
                            <table id="products" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên khách hàng</th>
                                    <th width="20%">Email</th>
                                    <th>SĐT</th>
                                    <th>Ghi chú</th>
                                    <th>Người phụ trách</th>
                                    <th>Trạng thái</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
    
@endsection
@push('scripts')
<script>
   
    $(function() {
        $('#products').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
			type: "POST",
            ajax: '{!! route('datatable_product') !!}',
            columns: [
                { data: 'product_id', name: 'products.product_id' },
                { data: 'post_id', name: 'posts.post_id', orderable: false,
                    render: function ( data, type, row, meta ) {
                        return '<input type="checkbox" name="chkdelete" class="chkDelete" value="'+data+'" onclick="return checkElement(this);"/>';
                    },
                    searchable: false
                },
                { data: 'title', name: 'posts.title' },
                { data: 'category_string', name: 'category_string' },
                { data: 'image', name: 'posts.image', orderable: false,
                    render: function ( data, type, row, meta ) {
                        return '<div class=""><img src="'+data+'" width="50" /></div>';
                    },
                    searchable: false  },
                { data: 'code', name: 'products.code' },
                { data: 'views', name: 'posts.views' },
                { data: 'price', name: 'products.price' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    });


</script>


@endpush
