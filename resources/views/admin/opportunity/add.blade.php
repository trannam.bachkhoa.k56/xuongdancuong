@extends('admin.layout.admin')

@section('title', 'Thêm mới Khách hàng')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới bài viết
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Sản phẩm</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <form role="form" action="{{ route('opportunity.store') }}" method="POST">
            {!! csrf_field() !!}
            {{ method_field('POST') }}
        <div>
        <div class="box-body">      
            <div class="row">
                <div class="col-xs-8 col-md-8">
                    <div class="box box-primary boxCateScoll">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên Khách hàng</label>
                                <input type="text" class="form-control" name="name" placeholder="Tên Khách hàng" required id="stepProductCreate1">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Email" id="stepProductCreate2" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại</label>
                                <input type="number" class="form-control formatPrice" name="phone" placeholder="Số điện thoại" id="stepProductCreate3" required>
                            </div>

                            <!-- <div class="form-group">
                                <label for="exampleInputEmail1">Ghi Chú</label>
                                <textarea rows="4" class="form-control" name="note"
                                  id="note-contact" placeholder="Ghi chú" required></textarea>
                            </div> -->      

                            <div class="form-group">
                                <label for="exampleInputEmail1">Người phụ trách</label>
                                <select class="form-control" name="manager">
                                    @foreach($managers as $manager)
                                        <option value="{{$manager->id}}">
                                           {{$manager->name}} -
                                           @if($manager->role == 1)
                                                Thành viên
                                           @elseif($manager->role == 2)
                                                Biên tập viên
                                           @elseif($manager->role == 3)
                                                Quản trị
                                           @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Trạng thái</label>                
                                <select class="form-control" name="status">      
                                    <option value="">Khách hàng mới</option>
                                    <option value="1">Khách hàng Tiếp cận </option>
                                    <option value="2">Khách hàng Mua hàng </option>
                                    <option value="3">Khách hàng Hoàn đơn (Hoàn tiền)</option>       
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" id="stepProductCreate7">Thêm mới</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    
@endsection

