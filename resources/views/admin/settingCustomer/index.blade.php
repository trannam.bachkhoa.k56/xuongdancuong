@extends('admin.layout.admin')

@section('title', 'Cấu hình khách hàng')

@section('content')
	<section class="content-header">
		<h1>
			Cấu hình khách hàng
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Cấu hình khách hàng</li>
		</ol>
	</section>
	<br>
	<div class="row">
		<div class="content">
			<div class="col-md-8">
				<div class="box box-primary">
					<div class="box-header ui-sortable-handle" style="cursor: move;">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Automation</h3>
						<div class="box-tools pull-right">
							{{ $emails->links() }}
						</div>
					</div>
					<style type="text/css">
						.pagination{
							margin:0;
						}
					</style>

					<!-- /.box-header -->
					<div class="box-body">
						<!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
						<ul class="todo-list ui-sortable">
							@foreach($emails as $id => $email)
								<li class="" style="">
									<!-- drag handle -->
                        <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                        </span>
                        <span class="text">{{$id +1}} :
							@if($email->type_id == 1)
								Email
							@else
								Nhắc nhở {{$email->subject}}
							@endif
							CSKH sau {{$email->time_delay}} Ngày </span>
									<div class="tools">
										<a style="cursor: pointer;font-size: 20px" title="Sửa">
											<i class="fa fa-edit" data-toggle="collapse" data-target="#collapse{{$id}}" aria-expanded="false" aria-controls="collapse1"></i></a>
										<a href="{{route('delete_mail_customer',['id' => $email->mail_id])}}" style="cursor: pointer;font-size: 20px"  title="Xóa">
											<i class="fa fa-trash-o"></i>
										</a>
									</div>
								</li>
							@endforeach

						</ul>
					</div>
					<div class="box-footer clearfix no-border">
						<button type="button" class="btn btn-primary pull-left" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse"><i class="fa fa-plus"></i> Thêm automation</button>
					</div>
					<div class="collapse" id="collapse">

						<form action="{{route('add_mail_customer')}}" method="POST">
							{!! csrf_field() !!}
							{{ method_field('POST') }}
							<div class="box-body">

								<div class="form-group">
									<label>Chọn dạng</label>
									<select class="form-control" name="type">
										<option value="1">Email</option>
										<option value="2">Nhắc nhở</option>
									</select>
								</div>
								<input type="hidden" name="group" value="">


								<div class="form-group">
									Chọn thời gian sau:
									<input type="number" name="time_delay" class="">&emsp;  ngày &emsp;

								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Tiêu đề</label>
									<input type="text" name="subject" class="form-control" placeholder="Tiêu đề">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Nội dung</label>
									<textarea class="editor" id="content" name="content" rows="10" cols="80"/></textarea>
								</div>
							</div>
							<div class="box-footer">
								<button type="submit" class="btn btn-primary" id="stepProductCreate7">Thêm mới</button>
							</div>
						</form>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box box-primary">
					@foreach($emails as $id => $email)

						<div class="collapse" id="collapse{{$id}}">
							<div class="box-header">
								<h4 style="font-weight: bold;">Sửa CSKH sau {{$email->time_delay}} Ngày </h4>
							</div>
							<a href="#"data-toggle="collapse" data-target="#collapse{{$id}}" style="font-size: 18px;color: #000;position: absolute; right: 20px;top: 20px" title="đóng" >
								<i class="fa fa-times-circle" aria-hidden="true"></i>
							</a>
							<form action="{{route('update_mail_customer',['id' => $email->mail_id])}}" method="POST">
								{!! csrf_field() !!}
								{{ method_field('POST') }}
								<div class="box-body">
									<div class="form-group">
										<div class="form-group">
											<label>Chọn dạng</label>
											<select class="form-control" name="type" disabled="disabled">
												<option value="1" <?php if($email->type_id == 1) { echo "selected";}  ?>>Email</option>
												<option value="2"  <?php if($email->type_id == 2) { echo "selected";}  ?>>Nhắc nhở</option>
											</select>
										</div>
									</div>


									<div class="form-group">
										<label>Sau bao nhiêu Ngày</label>
										<input type="number" name="time_delay" value="{{$email->time_delay}}" class="form-control" >
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Tiêu đề</label>
										<input type="text" name="subject" class="form-control" value="{{$email->subject}}" placeholder="Tiêu đề">
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Nội dung</label>
										<textarea class="editor" id="content{{$id}}" name="content" rows="10" cols="40"/>{{$email->content}}</textarea>
									</div>
								</div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary" id="stepProductCreate7">Sửa</button>
								</div>
							</form>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<script>
		$(function () {
			$('#datepicker').datepicker({
				autoclose: true
			})
			$('#datepicker2').datepicker({
				autoclose: true
			})
			$('.timepicker').timepicker({
				showInputs: false
			})
		})
	</script>

	@if (session('add'))
		<script type="text/javascript">
			alert ('{{ session('add') }}')
		</script>

	@endif
	@if (session('update'))
		<script type="text/javascript">
			alert ('{{ session('update') }}')
		</script>
	@endif
	@if (session('remove'))
		<script type="text/javascript">
			alert ('{{ session('remove') }}')
		</script>
	@endif


@endsection
