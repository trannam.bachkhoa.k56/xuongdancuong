@extends('admin.layout.admin')

@section('title', 'Phân tích quảng cáo facebook')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Phân tích quảng cáo facebook
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Phân tích quảng cáo facebook</a></li>
        </ol>
    </section>


    <section class="content">
        <div class="row">
            <div class="col-xs-12">
				<div class="box">
					 <div class="box-body">
						<div class="row">
							<div class="col-xs-12 col-lg-12">
								<form action ="" method='get'>
									<div class="form-group">
										<label class="col-xs-4" for="exampleInputEmail1">Lựa chọn tài khoản hoặc chiến dịch quảng cáo</label>
										<select class="col-xs-6 form-control" name="campaigns">
											@foreach ($accountAds as $accountAd)
											<option value="{{ $accountAd['id'] }}" 
											{{ isset($_GET['campaigns']) && $_GET['campaigns'] == $accountAd['id'] ? 'selected' : '' }}>{{ $accountAd['id'] }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label class="col-xs-4" for="exampleInputEmail1">Thời gian</label>
										<select class="col-xs-6 form-control" name="date_preset">
											<option value="today" 
											{{ isset($_GET['date_preset']) && $_GET['date_preset'] == 'today' ? 'selected' : '' }}
											>Hôm nay</option>
											<option value="yesterday" 
											{{ isset($_GET['date_preset']) && $_GET['date_preset'] == 'yesterday' ? 'selected' : '' }}>
											Hôm qua</option>
											<option value="this_month" 
											{{ isset($_GET['date_preset']) && $_GET['date_preset'] == 'this_month' ? 'selected' : '' }}>Tháng này</option>
											<option value="last_month" 
											{{ isset($_GET['date_preset']) && $_GET['date_preset'] == 'last_month' ? 'selected' : '' }}>Tháng trước</option>
											<option value="this_year" 
											{{ isset($_GET['date_preset']) && $_GET['date_preset'] == 'this_year' ? 'selected' : '' }}>Năm nay</option>
											<option value="last_year" 
											{{ isset($_GET['date_preset']) && $_GET['date_preset'] == 'last_year' ? 'selected' : '' }}>Năm ngóai</option>
										</select>
									</div>
								
									<div class="form-group">
										<button class="col-xs-2 btn btn-success">Lọc</button>
									</div>
								</form>
								<div class="form-group">
									<button class="btn btn-primary" onclick="return loginFacebook(this); " >Đăng nhập facebook</button>
								
								</div>
							
							</div>
						</div>
					 </div>
				</div>
			
                <div class="box">
                    <div class="box-header">
						
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
						<div class="row">
							<div class="col-xs-12 col-lg-8">
								<script>
									window.onload = function () {

									var options = {
										animationEnabled: true,
										theme: "light2",
										title: {
											text: "Biểu đồ chạy quảng cáo facebook"
										},
										data: [{
											type: "funnel",
											toolTipContent: "<b>{label}</b>: {y} <b>({percentage}%)</b>",
											indexLabel: "{label} ({percentage}%)",
											dataPoints: [
												@if (isset($actionApiResponseAccount[0]['actions']))
													@foreach ($actionApiResponseAccount[0]['actions'] as $actionAccount)
														@if ($actionAccount['action_type'] == 'page_engagement')
															{ y: {!! $actionAccount['value'] !!}, label: "Người truy cập" },
														@endif
									
													@endforeach
													
													@foreach ($actionApiResponseAccount[0]['actions'] as $actionAccount)
														@if ($actionAccount['action_type'] == 'video_view')
															{ y: {!! $actionAccount['value'] !!}, label: "Xem video" },
														@endif
													@endforeach
													
													@foreach ($actionApiResponseAccount[0]['actions'] as $actionAccount)
														@if ($actionAccount['action_type'] == 'link_click')
															{ y: {!! $actionAccount['value'] !!}, label: "Ấn vào link" },
														@endif
													@endforeach
													
													@foreach ($actionApiResponseAccount[0]['actions'] as $actionAccount)
														@if ($actionAccount['action_type'] == 'post_reaction')
															{ y: {!! $actionAccount['value'] !!}, label: "Biểu lộ cảm xúc" },
														@endif
													@endforeach
													
													
													
													@foreach ($actionApiResponseAccount[0]['actions'] as $actionAccount)
														@if ($actionAccount['action_type'] == 'comment')
															{ y: {!! $actionAccount['value'] !!}, label: "Bình luận" },
														@endif
													@endforeach
													
													@foreach ($actionApiResponseAccount[0]['actions'] as $actionAccount)
														@if ($actionAccount['action_type'] == 'onsite_conversion.messaging_conversation_started_7d')
															{ y: {!! $actionAccount['value'] !!}, label: "Tin nhắn" },
														@endif
													@endforeach
													
												@endif
												
											]
										}]
									};
									calculatePercentage();
									$("#chartContainer").CanvasJSChart(options);

									function calculatePercentage() {
										var dataPoint = options.data[0].dataPoints;
										var total = dataPoint[0].y;
										for (var i = 0; i < dataPoint.length; i++) {
											if (i == 0) {
												options.data[0].dataPoints[i].percentage = 100;
											} else {
												options.data[0].dataPoints[i].percentage = ((dataPoint[i].y / total) * 100).toFixed(2);
											}
										}
									}

									}
									</script>
								<div id="chartContainer" style="height: 300px; width: 100%;"></div>
							<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
							<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
								
							</div>
							<div class="col-xs-12 col-lg-4">
								<label for="exampleInputEmail1">Số tiền bạn đã chi tiêu: <span style="color: red;">{{ number_format((isset($actionApiResponseAccount[0]['spend'])) ? $actionApiResponseAccount[0]['spend'] : 0)  }} VNĐ </span></label>
								
								<p>Thời gian: Từ
								<?php
									$date=date_create(isset($actionApiResponseAccount[0]['date_start']) ? $actionApiResponseAccount[0]['date_start'] : '');
									echo date_format($date,"d/m/Y");
								?>
								Đến 
								<?php
									$date=date_create(isset($actionApiResponseAccount[0]['date_stop']) ? $actionApiResponseAccount[0]['date_stop'] : '');
									echo date_format($date,"d/m/Y");
								?>
							
								</p>
							</div>
							
						</div>
                        <table id="" class="table table-bordered table-striped">
                            <thead>
								<tr>
									<th width="5%">STT</th>
									<th>Tên chiến dịch</th>
									<th width="15%">Lượt tương tác</th>
									<th width="10%">cảm xúc</th>
									<th>link click</th>
									<th>inbox</th>
									<th>Bình luận</th>
									<th>chi phí cơ hội</th>
									<th>Tổng tiền</th>
								</tr>
                            </thead>
							<?php $comments = 0;?>
							<?php $inboxs = 0;?>
							<?php $totals = 0;?>
							@foreach ($campains as $id => $campaign)
							@if (isset($campaign[0]['campaign_name']))
								<?php $commentCampaigns = 0;?>
								<?php $inboxCampaigns = 0;?>
							<tbody>
								<tr>
									<td>{{ ($id + 1) }}</td>
									<td> {{ isset($campaign[0]['campaign_name']) ? $campaign[0]['campaign_name'] : 'không xác định' }}</td>
									<td>
										@if (isset($campaign[0]['actions']))
											@foreach ($campaign[0]['actions'] as $actionCampaign)
												@if ($actionCampaign['action_type'] == 'page_engagement')
													{{ $actionCampaign['value'] }}
												@endif
											@endforeach
										@endif
									</td>
									<td>
										@if (isset($campaign[0]['actions']))
											@foreach ($campaign[0]['actions'] as $actionCampaign)
												@if ($actionCampaign['action_type'] == 'post_reaction')
													{{ $actionCampaign['value'] }}
												@endif
											@endforeach
										@endif
									</td>
									<td>
										@if (isset($campaign[0]['actions']))
											@foreach ($campaign[0]['actions'] as $actionCampaign)
												@if ($actionCampaign['action_type'] == 'link_click')
													{{ $actionCampaign['value'] }}
												@endif
											@endforeach
										@endif
									</td>
									<td>
										@if (isset($campaign[0]['actions']))
											@foreach ($campaign[0]['actions'] as $actionCampaign)
												@if ($actionCampaign['action_type'] == 'onsite_conversion.messaging_conversation_started_7d')
													{{ $actionCampaign['value'] }}
													<?php $inboxs += $actionCampaign['value'];?>
													<?php $inboxCampaigns += $actionCampaign['value'];?>
												@endif
											@endforeach
										@endif
									</td>
									<td>
										@if (isset($campaign[0]['actions']))
											@foreach ($campaign[0]['actions'] as $actionCampaign)
												@if ($actionCampaign['action_type'] == 'comment')
													{{ $actionCampaign['value'] }}
													<?php $comments += $actionCampaign['value'];?>
													<?php $commentCampaigns += $actionCampaign['value'];?>
												@endif
											@endforeach
										@endif
									</td>
									<td>
										@if ( ($inboxCampaigns + $commentCampaigns) > 0)
											{{ number_format($campaign[0]['spend'] / ($inboxCampaigns + $commentCampaigns)) }}
										@endif
									</td>
									<td>
										<span style="color: red;">{{ number_format((isset($campaign[0]['spend'])) ? $campaign[0]['spend'] : 0)  }} VNĐ </span>
										<?php $totals += isset($campaign[0]['spend']) ? $campaign[0]['spend'] : 0 ; ?>
									</td>
								</tr>
							</tbody>
							@endif
							@endforeach
							<tbody>
								<tr>
									<td colspan="5">Tổng</td>
									<td>{{ $inboxs }}</td>
									<td>{{ $comments }}</td>
									<td>
										<span style="color: red;">
											@if ( ($inboxs + $comments) > 0)
												{{ number_format($totals / ($inboxs + $comments)) }}
											@endif
										</span>
									</td>
									<td><span style="color: red;">{{ number_format($totals) }}</span></td>
								</tr>
							</tbody>
						
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
	<script>
		function loginFacebook(e) {
			window.location.replace('https://moma.vn/dang-nhap-facebook?user_email={!! $emailUser !!}&current_url={!! \App\Ultility\Ultility::getCurrentDomain() !!}')
		}
</script>
@endsection


