@extends('admin.layout.admin')

@section('title', 'Danh sách tin tức')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Tin tức
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Danh sách tin tức</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-xs-6 col-md-2">
                        <p>
                            <a  href="{{ route('posts.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                        </p>
                    </div>
                    @if (!empty($_GET['messageErrorVip']))
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Lỗi:</span> {!! $_GET['messageErrorVip'] !!}
                        </div>
                    @endif
                     <div class="col-xs-6 col-md-3">
                        <form action="{{ route('post_delete_all') }}" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="id_delete" id="idDeletes" value=""/>
                            <button type="submit" class="btn btn-danger">Xóa các mục đã chọn</button>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="posts2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tin Tức</th>
                        </tr>
                        </thead>
                    </table>
                    <table id="posts" class="table table-bordered table-striped hideMB" style="width:1200px !important;">
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th>
                                <input type="checkbox" name="checkall" id="select_all" onClick="check_uncheck_checkbox(this.checked);" />
                            </th>
                            <th>Tiêu đề</th>
                            <th width="15%">Đường dẫn</th>
                            <th width="10%">Danh mục</th>
                            <th>Hình ảnh</th>
                            <th>Xem</th>
                            <th>link</th>
                            <th>google</th>
                            <th width="14%">Thao tác</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th width="5%">ID</th>
                            <th></th>

                            <th>Tiêu đề</th>
                            <th>Đường dẫn</th>
                            <th>Danh mục</th>
                            <th>Hình ảnh</th>
                            <th>Lượt xem</th>
                            <th>link</th>
                            <th>google</th>
                            <th>Thao tác</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@include('admin.partials.popup_delete')
@include('admin.partials.visiable')
@endsection

@push('scripts')
<script src="https://sp.zalo.me/plugins/sdk.js"></script>
<script type="text/javascript">
    $window = $(window);
    var windowsize = $window.width();
    if(windowsize > 765) {
        $(function() {
            var table = $('#posts').DataTable({
				processing: true,
				serverSide: true,
				responsive: true,
				scrollX: true,
				scrollY: 400,
				autoWidth: false,
				pageLength: 20,
				type: "GET",
                ajax: '{!! route('datatable_post') !!}',
                columns: [
                    { data: 'post_id', name: 'post_id' },
                    { data: 'post_id', name: 'post_id', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<input type="checkbox" name="chkdelete" class="chkDelete" value="'+data+'" onclick="return checkElement(this);"/>';
                        },
                        searchable: false
                    },
                    { data: 'title', name: 'title' },
                    { data: 'slug', name: 'slug' },
                    { data: 'category_string', name: 'category_string' },
                    { data: 'image', name: 'image', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<img src="'+data+'" width="100" />';
                        },
                        searchable: false  },
                    { data: 'views', name: 'views' },
                    { data: 'slug', name: 'slug', orderable: false,
                        render: function ( data, type, row, meta ) {
                            html = '<p><a href="/tin-tuc/'+ data +'" target="_blank">Xem trang</a></p>';
                            html += `<iframe src="https://www.facebook.com/plugins/share_button.php?href={!! !empty($domainUser) ? (!empty($domainUser->domain) ? $domainUser->domain : $domainUser->url ) : 'https://moma.vn' !!}/tin-tuc/${data}&layout=button_count&size=small&appId=1875296572729968&width=102&height=20" width="102" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>`;

                            html += `<div class="zalo-share-button" data-href="{!! !empty($domainUser) ? (!empty($domainUser->domain) ? $domainUser->domain : $domainUser->url ) : 'https://moma.vn' !!}/tin-tuc/${data}" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false></div>`;

                            return html;
                        },
                        searchable: false  },
                    { data: 'title', name: 'title', render: function ( data, type, row, meta ) {
                            return `<a href="https://www.google.com/search?q=${data}" target="_blank">kiểm tra</a>`;
                        },
                    },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                ]
            });
        });
    }else {
        $('#posts2').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{!! route('datatable_post') !!}',
            columns: [
                { data: 'post_id', name: 'post_id', render: function (data, type, row, meta) {
                        return `<div class="box box-warning box-solid mgBottom0">
                                <div class="box-header with-border">
                                  <h3 class="box-title">${row.title}</h3>

                                        <input type="checkbox" name="chkdelete" class="chkDelete" value="'+ row.post_id +'" onclick="return checkElement(this);"/>
                                        ${row.action}

                                    </div>
                                    <div class="box-body">
                                        <p><img src="${row.image}" width="100%"/></p>
                                         <p>Đường dẫn: ${row.slug} đ</p>
                                         <p>Danh mục: ${row.category_string === '' ? 'chưa có' :row.category_string} </p>
                                         <p>Lượt xem: ${row.views == null ? '0' : row.views}</p>
                                         <p>Link liên kết: <a href="/tin-tuc/${row.slug}" target="_blank">Xem trang</a> </p>
                                    </div>
                            </div>`;
                    }
                },
            ]
        });

    }


    function check_uncheck_checkbox(isChecked) {
        if(isChecked) {
            $('.chkDelete').each(function() {
                this.checked = true;
            });
        } else {
            $('.chkDelete').each(function() {
                this.checked = false;
            });
        }
        getIdDelete();
    }

    function checkElement(e) {
        if($('.chkDelete:checked').length == $('.chkDelete').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
        getIdDelete();
    }

    function getIdDelete() {
        var idDeletes = '';
        $('.chkDelete:checked').each(function() {
            var idDelete = $(this).val();
            idDeletes += ',' + idDelete;
        });

        $('#idDeletes').val(idDeletes);
    }
</script>
@endpush

