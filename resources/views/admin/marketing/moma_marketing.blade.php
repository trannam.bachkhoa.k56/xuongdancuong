@extends('admin.layout.admin')

@section('title', 'marketing')

@section('content')
<section class="content">
    <div class="box">
        <div class="box-content pd15">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h1 class="txtCenter clRed fntWeight fntSize25">Moma Click là gì?</h1>
                    <p class="txtCenter" >Moma Click giúp hiển thị sản phẩm của shop đến những khách hàng tiềm năng của Moma. </p>
                    <p class="txtCenter"> Chỉ cần nạp ngân sách, hệ thống sẽ tự tối ưu, tăng doanh thu cho shop. </p>
                    <p class="txtCenter"><img src="/adminstration/moma_ads/google-banner.png" /></p>

                    <!-- Dịch vụ của chúng tôi? -->
                    <h2 class="txtCenter clRed fntWeight fntSize25">Dịch vụ của chúng tôi</h2>
                    <p class="txtCenter">Moma sẽ tư vấn và phân tích những vấn đề mà quý Shop cung cấp. Đưa ra các hình thức hỗ trợ bán hàng trọn gói đạt hiệu quả cao nhất cho Shop.</p>
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <div class="boxShadow">
                                <h3 class="txtCenter fntWeight">Moma click 1</h3>
                                <hr>
                                <p class="txtCenter clRed fntSize25">500.000 đ</p>
                                <p>Tổng ngân sách: <span class="flRight">500.000 đ</span></p>
                                <p class="txtCenter"><button class="btn btn-primary" money="500000" onClick="return buyAds(this);" >Mua ngay</button></p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <div class="boxShadow"  >
                                <h3 class="txtCenter fntWeight">Moma click 2</h3>
                                <hr>
                                <p class="txtCenter  clRed fntSize25">1.000.000 đ</p>
                                <p>Tổng ngân sách: <span class="flRight">1.000.000 đ</span></p>
                                <p class="txtCenter"><button class="btn btn-primary" money="1000000" onClick="return buyAds(this);" >Mua ngay</button><p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <div class="boxShadow"  >
                                <h3 class="txtCenter fntWeight">Moma click 3</h3>
                                <hr>
                                <p class="txtCenter  clRed fntSize25">3.000.000 đ</p>
                                <p>Tổng ngân sách: <span class="flRight">3.000.000 đ</span></p>
                                <p class="txtCenter"><button class="btn btn-primary" money="3000000" onClick="return buyAds(this);" >Mua ngay</button></p>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <div class="boxShadow"  >
                                <h3 class="txtCenter fntWeight">Moma click 4</h3>
                                <hr>
                                <p class="txtCenter  clRed fntSize25">5.000.000 đ</p>
                                <p>Tổng ngân sách: <span class="flRight">5.000.000 đ</span></p>
                                <p class="txtCenter"><button class="btn btn-primary" money="5000000" onClick="return buyAds(this);">Mua ngay</button></p>
                            </div>
                        </div>
                    </div>
                    <!-- Dịch vụ của chúng tôi? -->

                    <!-- Lưu ý -->
                    <div class="boxShadow mgt30" >
                        <h3><i>Lưu ý:</i></h3>
                        <p><i>- Giá chưa bao gồm VAT.</i></p>
                        <p><i>- Shop có thể chọn số lượng khi mua để có mức ngân sách mong muốn.</i></p>
                        <p><i>- Shop cần có email và có sản phẩm để hệ thống có thể cài đặt.</i></p>
                        <p><i> Để kích hoạt ngân sách ưu đãi hoặc cần thêm thông tin, vui lòng liên hệ:</i></p>
                        <p><i>⭐️ Hồ Chí Minh: 098.284.0758</i></p>
                        <p><i>⭐️ Hà Nội: 098.284.0758 </i></p>
                        <p><i>- Xuất hóa đơn: Sau khi mua dịch vụ hỗ trợ bán hàng của Moma, nếu có nhu cầu xuất hóa đơn giá trị gia tăng, Shop vui lòng liên hệ nhân viên kinh doanh thực hiện hợp đồng dịch vụ hoặc làm bảng kê chi tiết về dịch vụ và có ký xác nhận đóng dấu bởi đại diện có thẩm quyền của Shop và Moma, hoặc liên hệ số 098.284.0758 để được hướng dẫn và hỗ trợ.</i></p>
                    </div>

                    <!-- Tại sao Shop nên sử dụng dịch vụ Sendo Click? -->
                    <div>
                        <h2 class="txtCenter clRed fntWeight fntSize25 pd15">Tại sao Shop nên sử dụng dịch vụ MOMA Click?</h2>
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/whyuse_01.png" /></p>
                                <p class="txtCenter"> Tăng khả năng hiển thị sản phẩm với hàng triệu người online Moma mỗi ngày.</p>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/whyuse_02.png" /></p>
                                <p class="txtCenter"> Shop chỉ tốn chi phí khi có khách hàng truy cập vào xem sản phẩm của Shop.</p>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/whyuse_03.png" /></p>
                                <p class="txtCenter">Hệ thống tối ưu tiếp thị bán hàng tự động.</p>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/whyuse_04.png" /></p>
                                <p class="txtCenter">Báo cáo chiến dịch tự động 24/7.</p>
                            </div>
                        </div>
                    </div>

                    <div>
                        <h2 class="txtCenter clRed fntWeight fntSize25 pd15">Quy trình sử dụng của Moma Click</h2>
                        <div class="row">
                            <div class="col-xs-6 col-md-2">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/shopadstart1.png" /></p>
                                <p class="txtCenter">Bước 1</p>
                                <p class="txtCenter">Chọn gói phù hợp</p>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/shopadstart2.png" /></p>
                                <p class="txtCenter">Bước 2</p>
                                <p class="txtCenter">Thực hiện thanh toán</p>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/shopadstart3.png" /></p>
                                <p class="txtCenter">Bước 3</p>
                                <p class="txtCenter">Chiến dịch bán hàng bắt đầu</p>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/shopadstart4.png" /></p>
                                <p class="txtCenter">Bước 4</p>
                                <p class="txtCenter">Hệ thống và chuyên gia tối ưu hoá chiến dịch</p>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <p class="txtCenter"><img src="/adminstration/moma_ads/shopadstart5.png" /></p>
                                <p class="txtCenter">Bước 5</p>
                                <p class="txtCenter">Sản phẩm hiển thị tại vị trí Hot của Sendo</p>
                            </div>

                            <div class="col-xs-12 col-md-12">
                                <p class="txtCenter"><button class="btn btn-danger">Tham gia ngay.</button></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="buyAds" tabindex="-1" role="dialog" aria-labelledby="buyAds1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="buyAds1">Thanh toán mua Ads</h4>
        </div>
        <form  onsubmit="return paymentAds(this);">
            {!! csrf_field() !!}
            <div class="modal-body">
                <input type="hidden" value="" id="moneyForm" />
                <p>Bạn có chắc chắn muốn sử dụng gói quảng cáo của moma không ?</p>
                <p>
                    Bạn sẽ phải chi trả 1 khoản phí để quảng cáo là là: <span id="priceTheme"></span> đ
                </p>
                    <div class="form-group errorDomain" style="color: red;">

                </div>
            </div>
            <div class="modal-footer">
                <span type="button" class="btn btn-default" data-dismiss="modal">Đóng</span>
                <button type="submit" class="btn btn-primary">Đồng ý</button>
            </div>
        </form>
    </div>
  </div>
</div>


<style>
    .pd15 {
        padding: 15px;
    }
    .txtCenter {
        text-align: center;
    }
    .clRed {
        color: red;
    }
    .fntWeight {
        font-weight: bold;
    }
    .fntSize25 {
        font-size: 25px;
    }
    .flRight {
        float: right;
    }
    .boxShadow {
        border-radius: 6px 6px 7px 7px;
        padding: 15px 15px 20px;
        box-shadow: 0 2px 4px -1px rgba(0,0,0,.2), 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12);
    }
    .mgt30 {
        margin-top: 30px;
    }
</style>
<script>
    function paymentAds(e) {
        $('.errorDomain').empty();
        var token =  $('input[name=_token]').val();
        var money =  $('#moneyForm').val();

        $.ajax({
            type: "POST",
            url: '{!! route('payment_moma_ads') !!}',
            data: {
                _token: token,
                money : money
            },
            success: function(result){
                console.log(1);
                var obj = $.parseJSON(result);

                if (obj.status == 500) {
                    $('.errorDomain').append('<p>'+obj.message+'</p>');
                }
                if (obj.status == 200) {
                    alert(obj.message);
                    location.reload();
                }

                return false;
            },
            error: function(error) {
                $('.errorDomain').append('<p>Có một lỗi xảy ra. </p>');

                return false;
            }

        });


        return false;
    }
    function buyAds(e) {
        console.log();
        var money = $(e).attr('money');

        $('#moneyForm').val(money);
        $('#priceTheme').html(money);

        $('#buyAds').modal('show');
    }
</script>


@endsection