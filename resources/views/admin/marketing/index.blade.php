@extends('admin.layout.admin')

@section('title', 'marketing')

@section('content')
<section class="content">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			@include('admin.partials.form_support_marketing')
		</div>
	</div>
</section>
@endsection