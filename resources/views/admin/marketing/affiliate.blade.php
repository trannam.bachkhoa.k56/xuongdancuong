@extends('admin.layout.admin')

@section('title', 'affiliate moma')

@section('content')
<section class="content-header">
    <h1>
        Tin tức
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">danh sách mã giảm giá tham gia moma</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h1>Danh Sách mã giảm giá tham gia moma!</h1>
                    <p>
                        <button class="btn btn-success form-control" data-toggle="modal" data-target="#modalAffiliateMoma"><i class="fa fa-money" aria-hidden="true"></i> Đấu giá lên vị trí top 1</button>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="posts" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="10%">Thứ tự</th>
                                <th width="10%">Website</th>
                                <th width="30%">Nội dung</th>
                                <th width="10%">Khuyến mãi</th>
                                <th width="30%">QR giảm giá</th>
                                <th width="15%">Tiền đấu thầu</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($affiliateMomas as $id => $affiliateMomaShow)
                                <tr>
                                    <td width="10%">{!! ($id+1) !!}</td>
                                    <td>{!! !empty($affiliateMomaShow->domain) ? $affiliateMomaShow->domain : $affiliateMomaShow->url !!}</td>
                                    <td width="30%">{!! $affiliateMomaShow->title !!}</td>
                                    <td width="10%">{!! $affiliateMomaShow->affiliate !!}%</td>
                                    <td width="30%"><img src="https://chart.googleapis.com/chart?chs=70x70&cht=qr&chl={!! $affiliateMomaShow->link !!}&choe=UTF-8" /></td>
                                    <td width="15%">{!! !empty($affiliateMomaShow->money) ? number_format($affiliateMomaShow->money) : '0' !!} đ</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th width="10%">Thứ tự</th>
                                <th width="10%">Website</th>
                                <th width="30%">Nội dung</th>
                                <th width="10%">Khuyến mãi</th>
                                <th width="30%">QR giảm giá</th>
                                <th width="15%">Tiền đấu thầu</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <form role="form" action="{{ route('add_affiliate_moma') }}" method="POST" id="formPost">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="col-xs-12 col-md-8">

                            <!-- Nội dung thêm mới -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Tham gia chương trình giảm giá</h3>
                                </div>
                                <!-- /.box-header -->

                                <div class="box-body">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Nội dung khuyến mãi</label>
                                    <input type="text" class="form-control" name="title" value="{!!  !empty($affiliateMoma) ? $affiliateMoma->title : ''  !!}" placeholder="Nội dung khuyến mãi" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Đường dẫn hướng tới</label>
                                    <input type="text" class="form-control" name="link" value="{!!  !empty($affiliateMoma) ? $affiliateMoma->link : ''  !!}" placeholder="Đường dẫn hướng tới" required>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nội dung giảm giá</label>
                                    <textarea name="content_deal" maxlength="130" class="form-control" rows="4" placeholder="Nội dung giảm giá!" >{!!  !empty($affiliateMoma) ? $affiliateMoma->content_deal : ''  !!}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hoa hồng trên toàn hệ thống (%)</label>
                                    <input type="text" class="form-control" name="affiliate" value="{!! !empty(Auth::user()->affiliate_money) ? Auth::user()->affiliate_money :  '' !!}" placeholder="Hoa hồng trên toàn hệ thống" value="{!! !empty($affiliateMoma) ? $affiliateMoma->affiliate : '' !!}" required>
                                </div>

                                <div class="form-group" style="color: red;">
                                        @if ($errors->has('title'))
                                            <label for="exampleInputEmail1">{{ $errors->first('title') }}</label>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group" >
                                    <button type="submit" class="btn btn-success">Tạo mã giảm giá</button>
                                </div>
                                
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->

                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Modal -->
<div class="modal fade" id="modalAffiliateMoma" tabindex="-1" role="dialog" aria-labelledby="modalAffiliateMoma">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalAffiliateMoma">Tham gia đấu giá mã giảm giá</h4>
            </div>
            <form action="/admin/thanh-toan-qua-vi-dien-tu" method="get">
                <div class="modal-body">
					<div class="form-group">
                        <label for="exampleInputEmail1">Số tiền đấu giá</label>
                        <input type="text" class="form-control" name="money" placeholder="Số tiền đấu giá" required>
                    </div>
                </div>
                <input type="hidden" value="1" name="type" />
                <input type="hidden" value="100" name="month" />
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Đấu giá</button>
                </div>
            </form>

        </div>
    </div>
</div>