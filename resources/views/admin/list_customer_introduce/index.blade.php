@extends('admin.layout.admin')

@section('title', 'Danh sách Khách hàng đã giới thiệu')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Danh sách Khách hàng đã giới thiệu
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"> Danh sách giới thiệu </a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                    <div class="box-body">
                        <table id="user" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th width="5%">STT</th>
                            <th>Họ và tên</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>                        
                            <th>Danh sách đã giới thiệu</th>
                       
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customerIntroducts as $id => $user )
                            <tr>
                                <td>{{ ($id+1) }}</td>
                                <td>{{ $user->user_name }}</td>
                                <td>{{ $user->user_email }}</td>
                                <td>{{ $user->user_phone }}</td>
                                <td>
                                    <a href="{{ route('detail_customer_introduce', ['id' => $user->id]) }}" target="_blank">
                                    <i class="fa fa-search-plus" aria-hidden="true"></i>   Lịch sử giới thiệu</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                             <th width="5%">STT</th>
                            <th>Email</th>
                            <th>Họ và tên</th>
                            <th>Số điện thoại</th>                        
                            <th>Danh sách đã giới thiệu</th>
                       
                        </tr>
                        </tfoot>
                    </table>
                  @include('admin.partials.popup_delete')
                </div>
            </div>
        </div>
    </div>
</section>

@endsection