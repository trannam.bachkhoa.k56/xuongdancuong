@extends('admin.layout.admin')

@section('title', 'Chi tiết Khách hàng  giới thiệu')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Chi tiết Khách hàng giới thiệu
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"> Chi tiết  giới thiệu </a></li>
    </ol>
</section>
  <!-- thông tin tài khoản  thanh toán của khách hàng  -->
     <?php 
     $payment =  App\Entity\PaymentInfomation::getWithUser(isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id );                                   
     ?>

  
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <section class="content">
                        <div class="row">
                          <div class="col-md-4 col-xs-12">
                           
                            <div class="tab-content">
                               <div class="box box-info" id="forGroup">
                                  <div class="box-header">
                                     <i class="fa fa-address-book"></i>
                                     <h3 class="box-title" >Thông tin Khách hàng </h3>
                                  </div>
                                  <div class="box-body">
                                    <div class="card" style="width: 30rem;position: relative;">                              
                                      <div class="card-body" >
                                        <h3 class="card-title" style="margin-top: 10px">                      {{isset($customerIntroducts->user_name) ? $customerIntroducts->user_name : $user->name }}
                                        </h3>

                                        <p class="card-text">
                                            <i class="fa fa-phone"></i>
                                            {{isset($customerIntroducts->user_phone) ? $customerIntroducts->user_phone : $user->phone}}
                                        </p>
                                        <p class="card-text">
                                            <i class="fa fa-envelope"></i>
                                            {{isset($customerIntroducts->user_email) ? $customerIntroducts->user_email : $user->email}}                                         
                                        </p>

                                        <p class="card-text">
                                            <i class="fa fa-book"></i>
                                            {{isset($customerIntroducts->shipping_address) ? $customerIntroducts->shipping_address : $user->shipping_address}}  -  {{number_format(isset($customerIntroducts->total_price) ? $customerIntroducts->total_price : $user->total_price)}}
                                            VNĐ                                 
                                        </p>

                                        <p class="card-text" >
                                       <!-- Nếu có thông tin ví thì hiển thị số tiền -->
                                        @if(!empty($payment))                                              
                                          <label>Số dư trong ví:</label>
                                          <span style="color: #ff9800;font-size: 16px;font-weight: bold;"> {{ number_format($payment->coin_total) }} <i class="fa fa-usd" aria-hidden="true"></i></span>
                                          <input type="hidden" id="coin_total" value="{{ $payment->coin_total }}">
                                         
                                          <a style="margin-left: 10px;" onclick="rutTien()"> Rút tiền </a>   
                                          <div id="money" style="display: none;">
                                              <div class="form-group">
                                                <input class="form-control" id="money_number" type="number" name="" placeholder="Số tiền ...." min="1">
                                              </div>

                                              <div class="form-group">
                                                <button class="btn btn-rutTien" id="btn-rutTien" onclick="submit_money()">Rút tiền</button>
                                              </div>
                                            </div>
                                          

                                        @endif
                                        </p>     
                                        <!-- nếu đăng nhập bằng user thành viên thì có nút đổi thông tin     -->
                                         @if(\Illuminate\Support\Facades\Auth::user()->role != 4 )
                                         <a style="position: absolute;top: 0;right: 0" href="{{ route('users.edit', ['id' => Auth::user()->id]) }}" class="btn btn-primary"> Đổi thông tin </a>
                                         @endif                                     
                                      </div>
                                    </div>
                                  </div>

                                  <!-- nếu không có ví thì hiện form tạo mới ,Nếu có rồi thì ra edit -->
                                  @if(empty($payment))                                  
                                    <div id="formPayment">
                                      <h3 class="box-title"> Thêm mới thông tin tài khoản </h3>
                                      <form method="POST" action="{{route('add_payment_infomation')}}">
                                         {!! csrf_field() !!}
                                        {{ method_field('POST') }}
                                          <input class="form-control" type="hidden" name="user_id" value="{{isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id }}">

                                          <div class="form-group">
                                              <label>Tên Chủ tài khoản</label>
                                              <input class="form-control" type="text" name="payment_name" required="">
                                          </div>
                                          <div class="form-group">
                                              <label>Số tài khoản</label>
                                              <input class="form-control" type="number" name="bank_number" required="">
                                          </div>
                                           <div class="form-group">
                                              <label>Tên Ngân hàng</label>
                                              <input class="form-control" type="text" name="payment_bank" required="">
                                          </div>
                                           <div class="form-group">
                                              <label>Tên Chi nhánh</label>
                                              <input class="form-control" type="text" name="bank_branch" required="">
                                          </div>
                                          <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Kích hoạt ví MOMA</button>
                                          </div>
                                       
                                      </form>
                                    </div>
                                  @else
                                    <div id="formPayment">
                                      <h3 class="box-title"> Thông tin ví Moma </h3>
                                      <form method="POST" action="{{route('edit_payment_infomation')}}">
                                        {!! csrf_field() !!}
                                        {{ method_field('POST') }}
                                          <div class="form-group">
                                              <label>Tên Chủ tài khoản</label>
                                              <input class="form-control" type="text" name="payment_name" value="{{ $payment->payment_name }}">
                                          </div>
                                          <input type="hidden" name="id" value="{{$payment->payment_id}}">
                                          <div class="form-group">
                                              <label>Số tài khoản</label>
                                              <input class="form-control" type="text" name="bank_number" value="{{ $payment->bank_number }}">
                                          </div>
                                           <div class="form-group">
                                              <label>Tên Ngân hàng</label>
                                              <input class="form-control" type="text" name="payment_bank" value="{{ $payment->payment_bank }}">
                                          </div>
                                           <div class="form-group">
                                              <label>Tên Chi nhánh</label>
                                              <input class="form-control" type="text" name="bank_branch" value="{{ $payment->bank_branch }}">
                                          </div>
                                          <div class="form-group">
                                            <button  type="submit" class="btn btn-primary">Lưu thông tin </button>
                                          </div>
                                       
                                      </form>
                                    </div>
                                  @endif
                               </div>
                              
                            </div>
                          </div>

                          <div class="col-md-8">
                            <div class="box box-info">
                                <div class="box-header">
                                  <h3 class="box-title" style="padding-bottom: 20px"> <i class="fa fa-list-ul"></i>  Danh sách khách hàng đã mời </h3>
                                </div>

                                   <div class="box-body">
                                        <table id="user" class="table table-bordered table-striped">
                                          <thead>
                                            <tr>
                                                <th width="5%">STT</th>
                                                <th>Tên đơn hàng</th>
                                                <th>Email</th>
                                                <th>Gói mua </th> 
                                                <th>Giá tiền </th>
                                               <!--  <th>Triết khấu (30%)</th>     -->                 
                                                <th>Mã giới thiệu</th>
                                                <th>Ngày tạo</th>                                        
                                                <!-- <th>Trạng thái</th>                                      -->
                                            </tr>
                                          </thead>                                     
                                        <tbody>

                                        @foreach(App\Entity\CustomerIntroduct::getIntroduceWithUserID(isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id ) as $id => $order  )
                                            <tr>
                                                <td>{{ ($id+1) }}</td>
                                                <td>{{ $order->shipping_name }}</td>
                                                <td>{{ $order->shipping_email }}</td>
                                                <td>{{ $order->shipping_address }}</td>
                                                <td>{{ number_format($order->total_price) }} <span style="display: none"  class="price">{{ $order->total_price }}</span>
                                                </td>
                                               
                                                <td>{{ $order->gift_code}}</td>
                                                <td>{{ $order->created_at}}</td>

                                            <!--   @if(\Illuminate\Support\Facades\Auth::user()->role != 4 )
                                                <td style="text-align: center;">
                                                  @if($order->status != 0 )
                                                    <button class="btn btn-success" disabled="disabled">
                                                      Đã Phê Được duyệt 
                                                    </button>
                                                  @else                                             
                                                    <button class="btn btn-primary" disabled="disabled">
                                                      Chưa Phê duyệt
                                                    </button>
                                                  @endif
                                                  <input type="hidden" name="introduct_id" value="{{$order->id}}">
                                                </td>
                                                @else
                                                 <td style="text-align: center;">
                                                  @if($order->status != 0 )
                                                    <button class="btn" disabled="disabled">
                                                      Đã Phê duyệt 
                                                    </button>
                                                  @else                                             
                                                    <button class="btn btn-primary" onclick="return pheDuyet(this)">
                                                      Phê duyệt
                                                    </button>
                                                  @endif
                                                  <input type="hidden" name="introduct_id" value="{{$order->id}}">
                                                </td>
                                              @endif -->
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th width="5%">STT</th>
                                            <th>Tên đơn hàng</th>
                                            <th>Email</th>
                                            <th>Gói mua </th>         
                                            <th>Giá tiền </th>
                                            <!-- <th>Triết khấu</th> -->
                                            <th>Mã giới thiệu</th>
                                            <th>Ngày tạo</th>                                           
                                            <!-- <th>Trạng thái</th> -->
                                        </tr>
                                        </tfoot>
                                    </table>                            
                             </div>

                          </div>

                          <div>
                              <p style="font-size: 20px;color: red">Tổng tiền đơn hàng đã giới thiệu : 
                                  <?php 
                                   $sumprice = App\Entity\CustomerIntroduct::getSumPriceWithUserID(isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id );
                                    echo number_format($sumprice). " VNĐ </br>";
                                    if ($sumprice < 10000000) {
                                      # code...
                                      echo 'Mức Hoa hồng 23%';
                                    }
                                    else if ($sumprice >= 10000000 || $sumprice < 20000000 ) {
                                      # code...
                                      echo 'Mức Hoa hồng 25%';
                                    }
                                    else if ($sumprice >= 20000000 || $sumprice < 50000000 ) {
                                      # code...
                                      echo 'Mức Hoa hồng 27%';
                                    }
                                    else if ($sumprice >= 50000000 || $sumprice < 70000000 ) {
                                      # code...
                                      echo 'Mức Hoa hồng 29%';
                                    }
                                    else if ($sumprice >= 70000000 || $sumprice < 100000000 ) {
                                      # code...
                                      echo 'Mức Hoa hồng 30%';
                                    }
                                    else if ($sumprice >= 100000000 || $sumprice < 200000000 ) {
                                      # code...
                                      echo 'Mức Hoa hồng 25%';
                                    }
                                    else if ($sumprice >= 20000000 ) {
                                      # code...
                                      echo 'Mức Hoa hồng 35%';
                                    }
                                   ?>
                              </p>

                                <p>
                                  Số tiền đang có: {{number_format(isset($user->coin) ? $user->coin : 0 )}} VNĐ 
                                </p>
                                @if($user->coin != 0 )
                                  @if(!empty($payment))      
                                  <div>
                                    <a onclick="pheDuyet();" class="btn btn-success">Chuyển tiền vào ví moma</a>
                                  </div>
                                  @else
                                  <p style="color: red; font-weight: bold;font-size: 20px ">
                                    Bạn cần Phải Khởi Tạo ví moma trước khi chuyển tiền vào ví  
                                  </p>
                                  @endif
                                @endif
                          </div>
                        </div>
                    </section>
                  
                    <script type="text/javascript">

                      function rutTien(){
                        $('#money').toggle(400);
                      }

                      function submit_money(){
                        if(confirm('Hãy đảm bảo số tiền trong ví còn lại không dưới 1.000.000'))
                        {
                          $('#btn-rutTien').attr('disabled',true);

                          /* số tiền rút ra */
                          var money_number = $('#money_number').val();
                          /* số tiền trong ví */
                          var coin_total = $('#coin_total').val();
                          var sub_coin = coin_total - money_number;

                          console.log(sub_coin);
                          if( sub_coin  < 1000000){
                              alert('Bạn cần giữ cho tài khoản không dưới 1.000.000');
                          }
                          else{
                            $.ajax({
                              url: '{{route("submit_money")}}',
                              type: "GET",                           
                              data: {
                                user_id : '{{$user->id }}',
                                user_name : '{{$user->name}}',
                                user_email : '{{$user->email}}',
                                money_number: money_number,
                              },
                            })
                            .done(function() {
                              console.log("success");
                              alert('Yêu cầu của bạn đã được Gửi cho quản trị viên');
                            })
                            .fail(function() {
                              console.log("error");
                            })
                            .always(function() {
                              console.log("complete");
                          });
                                                        
                          }
                        }
                      }


                      function pheDuyet(){
                        let coin = {{$user->coin}};
                        var user_id = {{isset($user->id) ? $user->id : ''}};

                        $.ajax({
                          url: '{{route("change_introduce")}}',
                          type: 'GET' ,                        
                          data: {
                            coin: coin,
                            user_id: user_id
                                },
                        })

                        .done(function() {
                          console.log("success");
                            alert('Đã chuyển tiền vào ví thành công ');
                            location.reload();
                        })
                        .fail(function() {
                          console.log("error");
                        })
                        .always(function() {
                          console.log("complete");
                        });
                      }
                      
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
