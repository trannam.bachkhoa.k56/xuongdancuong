@extends('admin.layout.admin')

@section('title', 'KPI SALE' )

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        KPI SALE
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Danh sách KPI SALE</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a  href="{{ route('goals.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">STT</th>
                                <th>Mục tiêu</th>
                                <th>Người nhận</th>
                                <th>Doanh số</th>
                                <th>khách hàng</th>
                                <th>Cuộc gọi</th>
                                <th>Hẹn gặp</th>
                                <th>Trao đổi</th>
                                <th>Đơn hàng</th>
                                <th>Số thanh toán</th>
                                <th>Tháng</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        @foreach ($goals as $id => $goal)
                        <tbody>
                            <tr>
                                <td width="5%">{{ ($id + 1) }}</td>
                                <td><a href="{{ route('goals.edit',[ 'goal_id' => $goal->goal_id]) }}">{{ $goal->title }}</a></td>
                                <td>{{ $goal->name }}</td>
                                <td>{{ number_format($goal->revenue, 0,",",".") }}</td>
                                <td>{{ $goal->customers }}</td>
                                <td>{{ $goal->calls }}</td>
                                <td>{{ $goal->reminders }}</td>
                                <td>{{ $goal->notes }}</td>
                                <td>{{ $goal->orders }}</td>
                                <td>{{ $goal->purchases }}</td>
                                <td>{{ date_format(date_create($goal->month),"m") }}</td>
                                <td>
                                    <a  href="{{ route('goals.destroy', ['goal_id' => $goal->goal_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@include('admin.partials.popup_delete')
@endsection

