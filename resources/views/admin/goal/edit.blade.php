@extends('admin.layout.admin')

@section('title', $goal->title )

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Chỉnh sửa {{ $goal->title }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Kpi mục tiêu</a></li>
        <li class="active">Chỉnh sửa</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- form start -->
        <form role="form" action="{{ route('goals.update', ['goal_id' => $goal->goal_id]) }}" method="POST">
            {!! csrf_field() !!}
            {{ method_field('PUT') }}
            <div class="col-xs-12 col-md-6">

                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mục tiêu chung</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên mục tiêu</label>
                            <input type="text" class="form-control" name="title" value="{{ $goal->title }}" required placeholder="Mục tiêu KPI" >
                        </div>

                        <div class="form-group">
                            @if (empty($userIdPartner))
                            <label for="exampleInputEmail1">Lựa chọn cộng sự</label>
                            <select name="user_id" class="form-control">
                                <option value="{{ \Illuminate\Support\Facades\Auth::user()->id }}"
                                        {{ \Illuminate\Support\Facades\Auth::user()->id  == $goal->user_id ? 'selected' : '' }}
                                >Quản trị viên</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}"
                                    {{ $employee->id == $goal->user_id ? 'selected' : '' }}
                                    >{{ $employee->name }}</option>
                                @endforeach
                            </select>
                            @else
                                <input type="hidden" value="{{ $userIdPartner }}" name="user_id"/>
                            @endif

                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Cài đặt trong tháng </label>
                            <input type="month"  value="{{ date_format(date_create($goal->month),"Y-m") }}" class="form-control" name="month" placeholder="kpi tính vào tháng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng khách hàng</label>
                            <input type="number" class="form-control " value="{{ $goal->customers }}" name="customers" placeholder="Số lượng khách hàng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Mục tiêu doanh số</label>
                            <input type="text" class="form-control formatPrice" value="{{ $goal->revenue }}" name="revenue" placeholder="Số lượng khách hàng" >
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>

            <div class="col-xs-12 col-md-6">

                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cài đặt chỉ số KPI</h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số cuộc gọi</label>
                            <input type="number" class="form-control " value="{{ $goal->calls }}" name="calls" placeholder="Số cuộc gọi" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số trao đổi</label>
                            <input type="number" class="form-control " value="{{ $goal->notes }}" name="notes" placeholder="Số trao đổi với khách hàng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số hẹn gặp</label>
                            <input type="number" class="form-control " value="{{ $goal->reminders }}" name="reminders" placeholder="Số cuộc hẹn gặp" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số đơn hàng</label>
                            <input type="number" class="form-control" name="orders" value="{{ $goal->orders }}" placeholder="Số lượng đơn hàng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng thanh toán</label>
                            <input type="number" class="form-control" name="purchases" value="{{ $goal->purchases }}" placeholder="Số lượng thanh toán" >
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </div>
                <!-- /.box -->

            </div>


        </form>
    </div>
</section>
<script>
    $('.formatPrice').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: '.'
    });
</script>
@endsection

