@extends('admin.layout.admin')

@section('title', 'Thêm mới kpi cho nhân viên' )

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Thêm mới Kpi cho nhân viên
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">KPI cho nhân viên</a></li>
        <li class="active">Thêm mới</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- form start -->
        <form role="form" action="{{ route('goals.store') }}" method="POST">
            {!! csrf_field() !!}
            {{ method_field('POST') }}
            <div class="col-xs-12 col-md-6">

                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mục tiêu chung</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên mục tiêu</label>
                            <input type="text" class="form-control" name="title" required placeholder="Mục tiêu KPI" >
                        </div>

                        <div class="form-group">
                            @if (empty($userIdPartner))
                            <label for="exampleInputEmail1">Lựa chọn cộng sự</label>
                            <select name="user_id" class="form-control">\Illuminate\Support\Facades\Auth::user()->id
                                <option value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">Quản trị viên</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                @endforeach
                            </select>
                            @else
                                <input type="hidden" value="{{ $userIdPartner }}" name="user_id"/>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Cài đặt trong tháng </label>
                            <input type="month" class="form-control" name="month" placeholder="kpi tính vào tháng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng khách hàng</label>
                            <input type="number" class="form-control " name="customers" placeholder="Số lượng khách hàng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Mục tiêu doanh số</label>
                            <input type="text" class="form-control formatPrice" name="revenue" placeholder="Số lượng khách hàng" >
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>

            <div class="col-xs-12 col-md-6">

                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cài đặt chỉ số KPI</h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số cuộc gọi</label>
                            <input type="number" class="form-control " name="calls" placeholder="Số cuộc gọi" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số trao đổi</label>
                            <input type="number" class="form-control " name="notes" placeholder="Số trao đổi với khách hàng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số hẹn gặp</label>
                            <input type="number" class="form-control " name="reminders" placeholder="Số cuộc hẹn gặp" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số đơn hàng</label>
                            <input type="number" class="form-control" name="orders" placeholder="Số lượng đơn hàng" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số lượng thanh toán</label>
                            <input type="number" class="form-control" name="purchases" placeholder="Số lượng thanh toán" >
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Thêm mới</button>
                    </div>
                </div>
                <!-- /.box -->

            </div>
        </form>
    </div>
</section>
<script>
    $('.formatPrice').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: '.'
    });
</script>
@endsection

