@extends('admin.zalo.zalo_layout')

@section('title', 'Thêm mới zalo chăm sóc khách hàng')

@section('content')
	<script src='https://cdn.rawgit.com/lrsjng/jquery-qrcode/v0.9.5/dist/jquery.qrcode.min.js'></script>
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới zalo chăm sóc khách hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">chăm sóc khách hàng</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('cskhzalo.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-6">
    
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên người dùng</label>
                                    <input type="text" class="form-control" name="name_zalo" placeholder="Tên người dùng" required>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại</label>
                                    <input type="text" class="form-control" name="phone_zalo" placeholder="Số điện thoại" required>
                                </div>
								
								<div class="form-group">
                                    <label for="exampleInputEmail1">Mã đăng nhập</label>
                                   <input type="hidden" name="link_token" class="form-control" value="" id="linkToken" required/>
                                </div>
								
								<div class="form-group">
                                    <span class="btn btn-primary" onClick="return getQrCode(this);">Lấy mã đăng nhập</span>
                                </div>
								

                                <div class="form-group" style="color: red;">
                                    @if ($errors->has('title'))
                                        <label for="exampleInputEmail1">{{ $errors->first('title') }}</label>
                                    @endif
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Thêm mới</button>
                            </div>
                    </div>
                    <!-- /.box -->

                </div>
            </form>
			<div class="col-xs-12 col-md-6">
				<div id="qrCode"></div>
			</div>
        </div>
    </section>
	<script>		
		function getQrCode () {
			$.ajax({
				type: "get",
				url: 'https://id.zalo.me/account/authen?a=qr&t=1&continue=https%3A%2F%2Fchat%2Ezalo%2Eme',
				success: function(result){
					$('#qrCode').empty();
					
					$('#qrCode').qrcode(
						{
							width: 64,
							height: 64,
							text: result.data.token
						}
					);
					//console.log(result.data.code);
					
					return scanQrCode(result.data.code);
					
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
		function scanQrCode (code) {
			$.ajax({
				type: "get",
				url: '/admin/quet-qr-code?code='+code,
				crossDomain: true,
				success: function(result){
					if (result.httpCode == 200) {
						var obj = jQuery.parseJSON(result.decode);
					 
						// console.log(obj.data.continueUrl);
						 
						 $('#linkToken').val(obj.data.continueUrl);
						 
						 alert('lấy mã đăng nhập zalo thành công');
						 
						 return;
					}
					
					alert('Không lấy được mã zalo');
					 
					
				},
				error: function(error) {
					console.log(error);
				}

			});	
		}
		
	
	</script>
@endsection