@extends('admin.zalo.zalo_layout')

@section('title', $nameZalo)

@section('content')
	<iframe style="width: 100%; height: 100vh" src="{{ $tokenLink }}"></iframe>
@endsection