@extends('admin.layout.communicate')

@section('title', 'Tiện ích')

@section('content')
<section class="content-header">
    <h1>
        Tiện ích
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tiện ích</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-3">
			<div class="box box-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
				<div class="widget-user-header bg-black" style="background: url('/image/banner_tivavn.jpg') center center;">
					<h3 class="widget-user-username">Sàn tuyển dụng việc làm tiva</h3>
					<h5 class="widget-user-desc">Tiva.vn</h5>
				</div>
				<div class="widget-user-image">
					<img  src="/image/logo_tiva.png" alt="User Avatar">
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">3,200</h5>
							<span class="description-text">Ứng viên</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">13,000</h5>
							<span class="description-text">Việc làm</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">8,000</h5>
							<span class="description-text">tuyển dụng</span>
						  </div>
						  <!-- /.description-block -->
						</div>	
						<!-- /.col -->
					</div>
					  <!-- /.row -->
				  
					<p><a class="btn btn-success" style="width: 100%; text-align: center;" href="https://tiva.vn/trang/ntd" target="_blank">Truy cập</a></p>
				</div>
			</div>
			
		</div>
		
		<!-- dịch vụ in ấn -->
		<div class="col-xs-12 col-md-3">
			<div class="box box-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
				<div class="widget-user-header bg-black" style="background: url('/image/banner_pringo.jpg') center center;">
					<h3 class="widget-user-username">Nền tảng in ấn số 1 Việt Nam</h3>
					<h5 class="widget-user-desc">printgo.vn</h5>
				</div>
				<div class="widget-user-image">
					<img  src="/image/pringo_logo.png" alt="User Avatar">
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">2,100</h5>
							<span class="description-text">Khách hàng</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">1,100</h5>
							<span class="description-text">Dịch vụ</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">1,450,000</h5>
							<span class="description-text">Xuất bản</span>
						  </div>
						  <!-- /.description-block -->
						</div>	
						<!-- /.col -->
					</div>
					  <!-- /.row -->
				  
					<p><a class="btn btn-success" style="width: 100%; text-align: center;" href="https://printgo.vn/" target="_blank">Truy cập</a></p>
				</div>
			</div>
			
		</div>
		<!-- dịch vụ in ấn -->
		
		<!-- dịch vụ in ấn -->
		<div class="col-xs-12 col-md-3">
			<div class="box box-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
				<div class="widget-user-header bg-black" style="background: url('/image/chatbot_banner.jpg') center center;">
					<h3 class="widget-user-username">Tự động thu hút khách hàng </h3>
					<h5 class="widget-user-desc">chatbot</h5>
				</div>
				<div class="widget-user-image">
					<img  src="/image/aha_logo.png" alt="User Avatar">
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">6,100</h5>
							<span class="description-text">Khách hàng</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">2,879,100</h5>
							<span class="description-text">Tin nhắn</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">200,000</h5>
							<span class="description-text">Fanpage</span>
						  </div>
						  <!-- /.description-block -->
						</div>	
						<!-- /.col -->
					</div>
					  <!-- /.row -->
				  
					<p><a class="btn btn-success" style="width: 100%; text-align: center;" href="https://ahachat.com/HZQQ4F" target="_blank">Truy cập</a></p>
				</div>
			</div>
			
		</div>
		<!-- dịch vụ in ấn -->
		
		<!-- dịch vụ phát sinh -->
		<div class="col-xs-12 col-md-3">
			<div class="box box-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
				<div class="widget-user-header bg-black" style="background: #506dad !important;center center;">
					<h3 class="widget-user-username">Tự động thu hút khách hàng </h3>
					<h5 class="widget-user-desc">vpn Chat</h5>
				</div>
				<div class="widget-user-image">
					<img  src="https://vnpsoftware.vn/dist/img/logo.png" alt="User Avatar">
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">3208</h5>
							<span class="description-text">khách hàng</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">15433</h5>
							<span class="description-text">tin nhắn</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">5432</h5>
							<span class="description-text">fanpage</span>
						  </div>
						  <!-- /.description-block -->
						</div>	
						<!-- /.col -->
					</div>
					  <!-- /.row -->
				  
					<p><a class="btn btn-success" style="width: 100%; text-align: center;" onClick="return acceptVpnChat(this);" target="_blank">Truy cập</a></p>
				</div>
			</div>
			<script>
				function acceptVpnChat(e) {
					$.ajax({
						type: "get",
						url: '{{route("login_vpn_chat")}}',
						success: function(result){
							var obj = jQuery.parseJSON( result);
							/* gửi thành công */
							if (obj.status == 200) {
								
								window.location = obj.url_redirect;
							}

						},
						error: function(error) {
						}

					});
				} 
			</script>	

		</div>
		<!-- dịch vụ phát sinh -->
		
		<!-- dịch vụ phát sinh -->
		<div class="col-xs-12 col-md-3">
			<div class="box box-widget widget-user">
			<!-- Add the bg color to the header using any of the bg-* classes -->
				<div class="widget-user-header bg-black" style="background: url('/image/banner.jpg') center center;">
					<!-- <h3 class="widget-user-username">Tự động thu hút khách hàng </h3>
					<h5 class="widget-user-desc">chatbot</h5> -->
				</div>
				<div class="widget-user-image">
					<img  src="/image/logo_moma.jpg" alt="User Avatar">
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">...</h5>
							<span class="description-text">nội dung</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4 border-right">
						  <div class="description-block">
							<h5 class="description-header">...</h5>
							<span class="description-text">nội dung</span>
						  </div>
						  <!-- /.description-block -->
						</div>
						<!-- /.col -->
						<div class="col-sm-4">
						  <div class="description-block">
							<h5 class="description-header">...</h5>
							<span class="description-text">nội dung</span>
						  </div>
						  <!-- /.description-block -->
						</div>	
						<!-- /.col -->
					</div>
					  <!-- /.row -->
				  
					<p><a class="btn btn-success" style="width: 100%; text-align: center;" href="https://ahachat.com/HZQQ4F" target="_blank">Truy cập</a></p>
				</div>
			</div>
			
		</div>
		<!-- dịch vụ phát sinh -->

	</div>
</section>
@endsection

