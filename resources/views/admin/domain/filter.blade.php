@extends('admin.layout.admin')

@section('title', 'Danh sách domain')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Domains
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách domains</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                   
                   <div class="box-header">
                        <ul id='filter'>
                           <li class="item">
                                <span class="btn-default"></span>
                              <a href="{{route('filter_domain',['sub' => 1 ])}}" sub="1" class="btn btn-default">Mới tạo website </a>
                            </li>
                            <li class="item">
                                <span class="btn-success"></span>
                              <a href="{{route('filter_domain',['sub' => 2 ])}}" class="btn btn-success">Nhập sản phẩm </a>
                            </li>
                            <li class="item">
                            <span class="btn-primary"></span>
                              <a  href="{{route('filter_domain',['sub' => 3 ])}}" class="btn btn-primary">Nhập tin tức </a>   
                            </li>
                            <li class="item">
                                <span class="btn-warning"></span>
                              <a href="{{route('filter_domain',['sub' => 4 ])}}"  class="btn btn-warning">Đã có khách hàng </a>   
                            </li>
                            <li class="item">
                                 <span class="btn-danger"></span>
                              <a href="{{route('filter_domain',['sub' => 5 ])}}" class="btn btn-danger">Đã có đơn hàng </a>   
                            </li>
                            <li class="item">
                                <span class="btn-info"></span>
                              <a href="{{route('filter_domain',['sub' => 6 ])}}"  class="btn btn-info">Đang online</a>   
                            </li>
                        </ul>
                    </div>

                    <style type="text/css">
                        #filter{
                            display: block;
                            padding-inline-start: 0px;
                        }
                        #filter .item {
                            display: inline-block;
                            list-style-type: none;
                            position: relative;
                            cursor: pointer;
                        }   
                        #filter .item span{
                            position: absolute;
                            top: -30px;
                            right: 0;
                            padding: 3px 7px;
                            font-size: 12px;
                          /*  border-radius: 10px;*/
                            color: #000;
                            font-weight: bold;
                        } 

                         #excel .item {
                            display: inline-block;
                            list-style-type: none;
                            position: relative;
                            cursor: pointer;
                        }   

                    </style>

                  
                     <div class="box-header" >
                        <a  href="{{ route('domains.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>

                        <span >
                            <button class="btn btn-success" onclick="showExport();">Xuất file excel</button>
                        </span>
                        <span class="excel" style="display: none">
                        <ul id="excel" style="display: inline-block;">
                             <li class="item">
                                <!-- <span class="btn-default">4</span> -->
                              <a href="{{route('export_domain', ['sub' => 0 ])}}" sub="1" class="btn btn-default">Tất cả <i class="fa fa-cloud-download" aria-hidden="true"></i></a>   
                            </li>
                            <li class="item">
                                <!-- <span class="btn-default">4</span> -->
                              <a href="{{route('export_domain', ['sub' => 1 ])}}" sub="1" class="btn btn-default">Mới tạo website <i class="fa fa-cloud-download" aria-hidden="true"></i></a>   
                            </li>
                            <li class="item">
                                <!-- <span class="btn-success">4</span> -->
                              <a href="{{route('export_domain', ['sub' => 2 ])}}" class="btn btn-success">Nhập sản phẩm <i class="fa fa-cloud-download" aria-hidden="true"></i></a>   
                            </li>
                            <li class="item">
                                <!-- <span class="btn-primary">4</span> -->
                              <a  href="{{route('export_domain', ['sub' => 3 ])}}" class="btn btn-primary">Nhập tin tức <i class="fa fa-cloud-download" aria-hidden="true"></i></a>   
                            </li>
                            <li class="item">
                                <!-- <span class="btn-warning">4</span> -->
                              <a href="{{route('export_domain', ['sub' => 4 ])}}"  class="btn btn-warning">Đã có khách hàng <i class="fa fa-cloud-download" aria-hidden="true"></i></a>   
                            </li>
                            <li class="item">
                                <!-- <span class="btn-danger">4</span> -->
                              <a href="{{route('export_domain', ['sub' => 5 ])}}" class="btn btn-danger">Đã có đơn hàng <i class="fa fa-cloud-download" aria-hidden="true"></i></a>   
                            </li>
                            <li class="item">
                                <!-- <span class="btn-info">4</span> -->
                              <a href="{{route('export_domain', ['sub' => 6 ])}}"  class="btn btn-info">Đang online <i class="fa fa-cloud-download" aria-hidden="true"></i></a>   
                            </li>
                        </ul>
                        </span>


                    </div>


                    <script type="text/javascript">
                        function showExport(){
                            $('.excel').toggle();
                        }
                    </script>

                    <!-- /.box-header -->
                    <div>
                        <h3 style="margin-left: 15px">
                           <?php 
                           switch ($sub) {
                               case 1:
                                   # code...
                               echo "Domain Mới tạo website";
                                   break;
                               case 2:
                                   # code...
                               echo "Domain Đã nhập sản phẩm ";
                                   break;
                               case 3:
                                   # code...
                               echo "Domain Đã nhập Tin tức ";
                                   break;
                               case 4:
                                   # code...
                               echo "Domain Đã Có khách hàng";
                                   break;
                               case 5:
                                   # code...
                               echo "Domain Đã Có đơn hàng ";
                                   break;
                               
                                case 5:
                                   # code...
                               echo "Domain Đang online ";
                                   break;
                               
                               default:
                                   # code...
                                   break;
                           }
                            ?>
                        </h3>
                    </div>
                    <div class="box-body" id="box">
                        <table class="table table-bordered table-striped" id="domain_filter">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tên domain</th>
                                <th>email</th>
                                <th>SDT</th>
                                <th>Đường dẫn</th>
                                <th>Ngày hết hạn</th>
                                <th>Theme</th>
                                <th>Status</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                           
                            <tfoot>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tên domain</th>
                                <th>email</th>
                                <th>SDT</th>
                                <th>Đường dẫn</th>
                                <th>Ngày hết hạn</th>
                                <th>Theme</th>
                                 <th>Status</th>
                                 <th>Thao tác</th>
                            </tr>
                            </tfoot>
                        </table>
                     
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
@endsection

@push('scripts')
    <script>
        $(function() {
            var table = $('#domain_filter').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_domain_filter', ['sub' => $sub]) !!}',
                columns: [
                    { data: 'domain_id', name: 'domain_id'},
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'users.email' },
                    { data: 'phone', name: 'users.phone' },
                    { data: 'url', name: 'url' },
                    { data: 'end_at', name: 'end_at' },
                    { data: 'theme_name', name: 'themes.name' },
                    { data: 'status', name: 'status'},
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                ]
            });
        });
    </script>
@endpush

