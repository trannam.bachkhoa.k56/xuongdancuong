@extends('admin.layout.admin')

@section('title', 'Danh sách domain')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Domains
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Danh sách domains</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                @php
                    {{--$countProductDomain = \App\Entity\Domain::countFilterDomain(2);--}}
                    {{--$countProductNews = \App\Entity\Domain::countFilterDomain(3);--}}
                    {{--$countProductContact = \App\Entity\Domain::countFilterDomain(4);--}}
                    {{--$countProductOrder = \App\Entity\Domain::countFilterDomain(5);--}}
                    {{--$countAllWebsite = \App\Entity\Domain::countFilterDomain(1);--}}
                    $countProductDomain =0;
                    $countProductNews =0;
                    $countProductContact =0;
                    $countProductOrder =0;
                    $countAllWebsite =0;
                @endphp
                <div class="box-header">
                    <ul id='filter'>
                        <li class="item">
                            <span class="btn-default">{{ $countAllWebsite }}</span>
                            <a href="{{route('domains.index',['type' => 1 ])}}" sub="1" class="btn btn-default">website </a>
                        </li>
                        <li class="item">
                            <span class="btn-success">{{ $countProductDomain  }}</span>
                            <a href="{{route('domains.index',['type' => 2 ])}}" class="btn btn-success">Nhập sản phẩm </a>
                        </li>
                        <li class="item">
                            <span class="btn-primary">{{ $countProductNews }}</span>
                            <a  href="{{route('domains.index',['type' => 3 ])}}" class="btn btn-primary">Nhập tin tức </a>
                        </li>
                        <li class="item">
                            <span class="btn-warning">{{ $countProductContact }}</span>
                            <a href="{{route('domains.index',['type' => 4 ])}}"  class="btn btn-warning">Đã có khách hàng </a>
                        </li>
                        <li class="item">
                            <span class="btn-danger">{{ $countProductOrder }}</span>
                            <a href="{{route('domains.index',['type' => 5 ])}}" class="btn btn-danger">Đã có đơn hàng </a>
                        </li>
                        <li class="item">
                            <span class="btn-info"></span>
                            <a href="{{route('domains.index',['type' => 6 ])}}"  class="btn btn-info">Đang online</a>
                        </li>
                    </ul>
                </div>

                <div class="box-header">
                    <ul id='filter'>
                        <li class="item">
                            <span class="btn-default">{{ \App\Entity\Domain::where('action', 0)->count() }}</span>
                            <a href="{{route('domains.index',['action' => 'news' ])}}"  class="btn btn-default">Mới </a>
                        </li>
                        <li class="item">
                            <span class="btn-success">{{  \App\Entity\Domain::where('action', 1)->count()  }}</span>
                            <a href="{{route('domains.index',['action' => 'note' ])}}" class="btn btn-success">Đã tương tác </a>
                        </li>
                        <li class="item">
                            <span class="btn-primary">{{  \App\Entity\Domain::where('change_url', 1)->count()  }}</span>
                            <a  href="{{route('domains.index',['action' => 'change_url' ])}}" class="btn btn-primary">Đã click thay tên miền</a>
                        </li>
                        <li class="item">
                            <span class="btn-info">{{  \App\Entity\Domain::join('users', 'users.id', 'domains.user_id')->where('users.vip', '>', 0)->count()  }}</span>
                            <a href="{{route('domains.index',['action' => 'payment' ])}}"  class="btn btn-info">Đã thay tên miền</a>
                        </li>
                    </ul>
                </div>

                <style type="text/css">
                    #filter{
                        display: block;
                        padding-inline-start: 0px;
                    }
                    #filter .item {
                        display: inline-block;
                        list-style-type: none;
                        position: relative;
                        cursor: pointer;
                    }
                    #filter .item span{
                        position: absolute;
                        top: -30px;
                        right: 0;
                        padding: 3px 7px;
                        font-size: 12px;
                        /*  border-radius: 10px;*/
                        color: #000;
                        font-weight: bold;
                    }

                    #excel .item {
                        display: inline-block;
                        list-style-type: none;
                        position: relative;
                        cursor: pointer;
                    }

                </style>




                <div class="box-header">
                    <a  href="{{ route('domains.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>

                         <span >
                            <button class="btn btn-success" onclick="showExport();">Xuất file excel</button>
                        </span>
                        <span class="excel" style="display: none">
                            <ul id="excel" style="display: inline-block;">
                                <li class="item">
                                    <a href="{{route('export_domain', ['sub' => 1 ])}}" sub="1" class="btn btn-default">Tất cả <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                </li>
                                <li class="item">
                                    <!-- <span class="btn-success">4</span> -->
                                    <a href="{{route('export_domain', ['sub' => 2 ])}}" class="btn btn-success">Nhập sản phẩm <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                </li>
                                <li class="item">
                                    <!-- <span class="btn-primary">4</span> -->
                                    <a  href="{{route('export_domain', ['sub' => 3 ])}}" class="btn btn-primary">Nhập tin tức <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                </li>
                                <li class="item">
                                    <!-- <span class="btn-warning">4</span> -->
                                    <a href="{{route('export_domain', ['sub' => 4 ])}}"  class="btn btn-warning">Đã có khách hàng <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                </li>
                                <li class="item">
                                    <!-- <span class="btn-danger">4</span> -->
                                    <a href="{{route('export_domain', ['sub' => 5 ])}}" class="btn btn-danger">Đã có đơn hàng <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                </li>
                                <li class="item">
                                    <!-- <span class="btn-info">4</span> -->
                                    <a href="{{route('export_domain', ['sub' => 6 ])}}"  class="btn btn-info">Đang online <i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </span>


                </div>


                <script type="text/javascript">
                    function showExport(){
                        $('.excel').toggle();
                    }
                </script>
                <form action="{{ route('domain_delete_all') }}" method="post" class="floatLeft">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id_delete" id="idDeletes" value=""/>
                    <button type="submit" class="btn btn-danger">Xóa các mục đã chọn</button>
                </form>

                <!-- /.box-header -->
                <div class="box-body" id="box">
                    <table id="domains" class="table table-bordered table-striped" style="width:1800px !important;">
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th>
                                <input type="checkbox" name="checkall" id="select_all" onClick="check_uncheck_checkbox(this.checked);" />
                            </th>
                            <th>Tên domain</th>
							<th>Tên kh</th>
                            <th>SDT</th>
                            <th width="10%">Đường dẫn</th>
                            <th>Tên miền</th>
                            <th>Người giới thiệu</th>
                            <th width="10%">LH lần cuối</th>
                            <th width="10%">TĐ lần cuối</th>
                            <th>email</th>
                            <th>Vip</th>
                            <th>Ngày tạo</th>
                            <th>Theme</th>
                            <th>Trang thai</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th width="5%">ID</th>
                            <th>
                                <input type="checkbox" name="checkall" id="select_all" onClick="check_uncheck_checkbox(this.checked);" />
                            </th>
                            <th>Tên domain</th>
                            <th>Tên kh</th>
                            <th>SDT</th>
                            <th>Đường dẫn</th>
                            <th>Tên miền</th>
                            <th>Người giới thiệu</th>
                            <th width="10%">LH lần cuối</th>
                            <th width="10%">TĐ lần cuối</th>
                            <th>email</th>
                            <th>Vip</th>
                            <th>Ngày tạo</th>
                            <th>Theme</th>
                            <th>Trang thai</th>
                            <th>Thao tác</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@include('admin.partials.popup_delete')
@endsection

@push('scripts')
<script>
    $(function() {
        var table = $('#domains').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollY: 600,
            autoWidth: false,
            pageLength: 100,
            ajax: '{!! route('datatable_domain') !!}{!! isset($_GET['type']) ? '?type='.$_GET['type'] : ''  !!}{!! isset($_GET['action']) ? '?action='.$_GET['action'] : ''  !!}',
            columns: [
                { data: 'domain_id', name: 'domain_id' },
                { data: 'domain_id', name: 'domain_id', orderable: false,
                    render: function ( data, type, row, meta ) {
                        return '<input type="checkbox" name="chkdelete" class="chkDelete" value="'+data+'" onclick="return checkElement(this);"/>';
                    },
                    searchable: false
                },
                { data: 'name', name: 'name' , render: function (data, type, row) {
                    return '<a href="/admin/domains/' + row.domain_id + '/edit" target="_blank">' + data + '</a>';
                }},
                { data: 'user_name', name: 'users.name' },
                { data: 'phone', name: 'users.phone', render(data) {
					return `<a href="tel:${data}">${data}</a>`;
					}, 
				},
                { data: 'url', name: 'url', render(data) {
					return `<a href="${data}" target="_blank">${data}</a>`;
					}, 
				},
                { data: 'domain', name: 'domain', render(data) {
                    return `<a href="${data}" target="_blank">${data !== '' ? data : 'chưa thay' }</a>`;
                     },
                },
                { data: 'user_invite_name', name: 'user_invite.name' },
                { data: 'dayConn', orderable: false, searchable: false },
                { data: 'convention', orderable: false, searchable: false },
                { data: 'email', name: 'users.email' },
                { data: 'vip', name: 'users.vip' },
                { data: 'created_at', name: 'domains.created_at' },
                { data: 'theme_name', name: 'themes.name' },
                { data: 'status', name: 'status'},
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    });

    function check_uncheck_checkbox(isChecked) {
        if(isChecked) {
            $('.chkDelete').each(function() {
                this.checked = true;
            });
        } else {
            $('.chkDelete').each(function() {
                this.checked = false;
            });
        }
        getIdDelete();
    }

    function checkElement(e) {
        if($('.chkDelete:checked').length == $('.chkDelete').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
        getIdDelete();
    }

    function getIdDelete() {
        var idDeletes = '';
        $('.chkDelete:checked').each(function() {
            var idDelete = $(this).val();
            idDeletes += ',' + idDelete;
        });

        $('#idDeletes').val(idDeletes);
    }
</script>
@endpush

