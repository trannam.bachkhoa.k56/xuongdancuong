<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="../../../">
		<meta charset="utf-8" />
		<title>ĐĂNG NHẬP QUẢN LÝ HỆ THỐNG CỦA MOMA</title>
		<meta name="description" content="Login page example">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


		<!--begin::Page Custom Styles(used by this page) -->
		<link href="{{asset('MomaManager/css/pages/login/login-1.css')}}" rel="stylesheet" type="text/css" />

		<!--end::Page Custom Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{asset('MomaManager/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('MomaManager/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="{{asset('MomaManager/img/logoMM2.png')}}" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="mm-quick-panel--right mm-panel--right mm-offcanvas-panel--right mm-header--fixed mm-header-mobile--fixed mm-subheader--fixed mm-subheader--enabled mm-subheader--solid mm-aside--enabled mm-aside--fixed mm-page--loading">

		<!-- begin:: Page -->
		<div class="mm-grid mm-grid--ver mm-grid--root mm-page">
			<div class="mm-grid mm-grid--hor mm-grid--root  mm-login mm-login--v1" id="kt_login">
				<div class="mm-grid__item mm-grid__item--fluid mm-grid mm-grid--desktop mm-grid--ver-desktop mm-grid--hor-tablet-and-mobile">

					<!--begin::Aside-->
					<div class="mm-grid__item mm-grid__item--order-tablet-and-mobile-2 mm-grid mm-grid--hor mm-login__aside" style="background-image: url({{asset('MomaManager/media/bg/bg-4.jpg')}});">
						<div class="mm-grid__item">
							<a href="#" class="mm-login__logo">
								<img src="{{asset('MomaManager/img/logoMM2.png')}}">
							</a>
						</div>
						<div class="mm-grid__item mm-grid__item--fluid mm-grid mm-grid--ver">
							<div class="mm-grid__item mm-grid__item--middle">
								<h3 class="mm-login__title">Chào mừng bạn đến với hệ thống quản lý khách hàng MOMA</h3>
								<h4 class="mm-login__subtitle">MOMA sẽ giúp bạn tăng doanh thu dễ dàng từ website</h4>
							</div>
						</div>
						<div class="mm-grid__item">
							<div class="mm-login__info">
								<div class="mm-login__copyright">
									&copy 2020 MOMA manager
								</div>
								<div class="mm-login__menu">
									<a href="#" class="mm-link">Điều khoản</a>
									<a href="/danh-muc/huong-dan" class="mm-link">Hướng dẫn</a>
									<a href="/trang/lien-he" class="mm-link">Liên hệ</a>
								</div>
							</div>
						</div>
					</div>

					<!--begin::Aside-->

					<!--begin::Content-->
					<div class="mm-grid__item mm-grid__item--fluid  mm-grid__item--order-tablet-and-mobile-1  mm-login__wrapper">

						<!--begin::Head-->
						<div class="mm-login__head">
							<span class="mm-login__signup-label">Bạn chưa có tài khoản?</span>&nbsp;&nbsp;
							<a href="/trang/tao-moi-website" class="mm-link mm-login__signup-link">Đăng ký!</a>
						</div>

						<!--end::Head-->

						<!--begin::Body-->
						<div class="mm-login__body">

							<!--begin::Signin-->
							<div class="mm-login__form">
								<div class="mm-login__title">
									<h3>ĐĂNG NHẬP</h3>
								</div>

								<!--begin::Form-->
								<form class="mm-form" action="{{ route('login') }}" method="post">
									{{ csrf_field() }}
									<div class="form-group has-feedback">
										<input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus />
										<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
										@if ($errors->has('email'))
											<span class="help-block">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
									</div>

									<div class="form-group has-feedback">
										<input type="password" class="form-control" placeholder="Mật khẩu" name="password"/>
										<span class="glyphicon glyphicon-lock form-control-feedback"></span>
										@if ($errors->has('password'))
											<span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
										@endif
									</div>
									<div class="form-group mm-margin-t-20">
										<div class="checkbox">
											<label class="mm-checkbox">
												<input type="checkbox" {{ old('remember') ? 'checked' : '' }}/> Nhớ đăng nhập
												<span></span>
											</label>
										</div>
									</div>
									<!--begin::Action-->
									<div class="mm-login__actions">
										<a href="/admin/password/reset" class="mm-link mm-login__link-forgot">
											Quên mật khẩu ?
										</a>

										<button type="submit" id="kt_login_signin_submit" class="btn btn-primary btn-elevate mm-login__btn-primary">Đăng nhập</button>
									</div>

									<!--end::Action-->
								</form>

								<!--end::Form-->
							</div>
							<!--end::Signin-->
						</div>
						<!--end::Body-->
					</div>
					<!--end::Content-->
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{asset('MomaManager/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('MomaManager/js/scripts.bundle.js')}}" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

	</body>

	<!-- end::Body -->
</html>