@extends('admin.layout.login')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href=""><b>Khôi phục mật khẩu</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Bạn vui lòng check lại email để khôi phục mật khẩu mới!</p>

        </div>
    </div>

@endsection
