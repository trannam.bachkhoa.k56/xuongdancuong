@extends('admin.layout.login')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href=""><b>Quên mật khẩu?</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Bạn nhập đủ thông tin để tạo lại mật khẩu</p>

            <form class="form-horizontal" method="POST" action="/admin/password/reset">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Email</label>

                    <div class="col-md-8">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        @if($errors->any() && $errors->has('emailFail') )
                            <div class="alert alert-danger" role="alert">
                                <strong>Email của bạn không tồn tại trong hệ thống.</strong>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Gửi password mới
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
