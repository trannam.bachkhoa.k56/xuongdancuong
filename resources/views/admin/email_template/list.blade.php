@extends('admin.layout.admin')

@section('title', 'Danh sách Mẫu email')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
         Mẫu email 
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách  Mẫu email</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <p>
                            <a  href="{{ route('group-template.create') }}">
                                <button class="btn btn-primary">Thêm mới</button>
                            </a>
                        </p>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="posts" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tiêu đề</th>
                                <th>Nội dung</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                          
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
     @include('admin.partials.popup_delete')
    @include('admin.partials.visiable')
@endsection

@push('scripts')
<script type="text/javascript">
    $(function() {
        var table = $('#posts').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('datatable_template_email') !!}',
            columns: [
                { data: 'template_id', name: 'template_id' },
                { data: 'template_title', name: 'template_title' },
                { data: 'content', name: 'content' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    });

</script>
@endpush

