@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa bài viết')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa Mẫu email
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Mẫu email</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{route('group-template.update',['id' =>$email->template_id])}}" method="POST">
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-8">
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Nhóm khách hàng</label>
                                <select  class="form-control" name="group_customer" >
                                    <option value="0" <?php if($email->group_customer == 0){echo 'selected';} ?> >Khách hàng mới</option>
                                    <option value="1" <?php if($email->group_customer == 1){echo 'selected';} ?> >Khách hàng tiếp cận </option>
                                    <option value="2" <?php if($email->group_customer == 2){echo 'selected';} ?> >Khách hàng Mua hàng </option>
                                    <option value="3" <?php if($email->group_customer == 3){echo 'selected';} ?> >Khách hàng hoàn đơn </option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">title</label>
                                <input type="text" class="form-control" name="template_title" placeholder="Tiêu đề" value="{{$email->template_title}}" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmails1">Nội dung</label>
                                <textarea class="editor" id="content" name="content" rows="10" cols="80"/>{{$email->content}}</textarea>
                            </div>

                          
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Chỉnh sửa</button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </form>

        </div>
    </section>
@endsection


