<ul class="sidebar-menu" data-widget="tree">

  <!--   <li class="{{ Request::is('admin/cau-hinh-dau-trang') ? 'active' : null }}">
        <a href="{{ route('config_header') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Đầu trang</span>
        </a>
    </li>

    <li class="{{ Request::is('admin/cau-hinh-trang-chu') ? 'active' : null }}">
        <a href="{{ route('config_home') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Trang chủ</span>
        </a>
    </li>

    <li class="{{ Request::is('admin/cau-hinh-danh-muc-san-pham') ? 'active' : null }}">
        <a href="{{ route('config_category_product') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Danh mục sản phẩm</span>
        </a>
    </li> -->

    <li class="{{ Request::is('admin/cau-hinh-chi-tiet-san-pham') ? 'active' : null }}">
        <a href="{{ route('config_detail_product') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Chi tiết sản phẩm</span>
        </a>
    </li>

  <!--   <li class="{{ Request::is('admin/cau-hinh-danh-muc-bai-viet') ? 'active' : null }}">
        <a href="{{ route('config_category') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Danh mục bài viết</span>
        </a>
    </li>
    <li class="{{ Request::is('admin/cau-hinh-chi-tiet-bai-viet') ? 'active' : null }}">
        <a href="{{ route('config_single') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Chi tiết bài viết</span>
        </a>
    </li> -->
    <li class="{{ Request::is('admin/cau-hinh-thong-tin-tu-van') ? 'active' : null }}">
        <a href="{{ route('config_advice') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Thông tin nhận tư vấn</span>
        </a>
    </li>
  <!--   <li class="{{ Request::is('admin/cau-hinh-lien-he') ? 'active' : null }}">
        <a href="{{ route('config_contact') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Liên hệ</span>
        </a>
    </li>
    <li class="{{ Request::is('admin/cau-hinh-footer') ? 'active' : null }}">
        <a href="{{ route('config_footer') }}">
            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Chân trang</span>
        </a>
    </li> -->
</ul>