@extends('admin.layout.admin')

@section('title', 'Cấu hình giao diện')

@section('content')
    <section class="content-header">
        <h1>
            Thay đổi giao diện
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Thay đổi giao diện</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <div class="col-xs-12 col-md-12">
                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-body">
                        <p>Thay đổi giao diện chỉ dành cho những tài khoản đã nâng cấp. Vui lòng ấn nâng cấp để thay đổi giao diện trên website.</p>
                        <p ><a href="/admin/nang-cap-tai-khoan" style="width: 100%;" class="btn btn-warning">Nâng cấp tài khoản</a><p>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

