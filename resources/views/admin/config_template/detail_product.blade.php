@extends('admin.layout.admin')
@section('title', 'Cấu hình giao diện chi tiết sản phẩm')
@section('content')
    <section class="content-header">
        <h1>
            Thay đổi giao diện chi tiết sản phẩm
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Thay đổi giao diện</a></li>
            <li><a href="#">Chi tiết sản phẩm</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <div class="box box-primary">
                    <div class="box-body">
                        @include('admin.config_template.general.siderbar')
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-9">
                <!-- <div class=" box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                    <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                    <img src="https://www.impulsapopular.com/wp-content/uploads/2018/03/4062-Innovar-y-emprender-%C2%BFes-lo-mismo.jpg" width="500px">
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                                    <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px">
                                </label>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="box box-primary">
                    <div class="box-body">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                Công cụ cho vay và tính lãi xuất
                            </h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <input type="checkbox" aria-label="action" checked onchange="showHideLend(this)">
                                <label for="action">Hiển thị</label>
                            </div>

                            <div class="lend">
                                <div class="form-group">
                                    <label for="price">Giá sản phẩm</label>
                                    <input type="text" id="price" aria-label="price" class="form-control" placeholder="Giá sản phẩm"
                                           onkeyup="this.value = FormatNumber(this.value); equivalency(this)">
                                </div>

                                <div class="form-group">
                                    <label for="loan">Số tiền vay</label>
                                    <select class="form-control select2" id="loan" onchange="interest_rate(this)">
                                        @for($i = 25; $i <= 75; $i++)
                                            @if($i % 5 === 0)
                                                <option value="{{$i}}" class="form-group">{{$i}} %</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="equivalent">Tương đương</label>
                                    <input type="text" id="equivalent" aria-label="price" name="equivalent" class="form-control" placeholder="Tương đương" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="time">Thời gian</label>
                                    <select class="form-control select2" id="time" name="time">
                                        @for($i = 1; $i <= 7; $i++)
                                            <option value="{{$i}}" class="form-group">{{$i}} năm</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="interest_rate">Lãi suất</label>
                                    <select class="form-control select2" name="interest_rate" id="interest_rate">
                                        @for($i = 10; $i <= 13; $i++)
                                            <option value="{{$i}}" class="form-group">{{$i}} %</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group">
                                    <button type="button" data-toggle="modal" data-target="#schedule" onclick="sendData(this)" class="btn btn-success">Bảng tiến độ trả góp</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.partials.popup_pay')
@endsection

@push('scripts')
    <script type="text/javascript">
        function showHideLend(e) {
            if ($(e).prop('checked')){
                $(e).parent().parent().find($('.lend')).show();
            }else {
                $(e).parent().parent().find($('.lend')).hide();
            }
        }
        
        function equivalency(e) {
            let moneyValue = $(e).val().replace(new RegExp(',', 'g'), '');
            let money = moneyValue * $(e).parent().parent().find($('#loan')).val() / 100;
            $(e).parent().parent().find($('#equivalent')).val(money);
            let moneyEquivalency = $(e).parent().parent().find($('#equivalent')).val();
            $(e).parent().parent().find($('#equivalent')).val(FormatNumber(moneyEquivalency));
        }

        function interest_rate(e) {
            let moneyValue = $(e).parent().parent().find($('#price')).val().replace(new RegExp(',', 'g'), '');
            let money = $(e).val() * moneyValue / 100;
            $(e).parent().parent().find($('#equivalent')).val(money);
            let moneyEquivalency = $(e).parent().parent().find($('#equivalent')).val();
            $(e).parent().parent().find($('#equivalent')).val(FormatNumber(moneyEquivalency));
        }


        let inputnumber = 'Giá trị nhập vào không phải là số';
        function FormatNumber(str) {
            var strTemp = GetNumber(str);
            if (strTemp.length <= 3)
                return strTemp;
            strResult = "";
            for (let i = 0; i < strTemp.length; i++)
                strTemp = strTemp.replace(",", "");
            var m = strTemp.lastIndexOf(".");
            if (m == -1) {
                for (var i = strTemp.length; i >= 0; i--) {
                    if (strResult.length > 0 && (strTemp.length - i - 1) % 3 == 0)
                        strResult = "," + strResult;
                    strResult = strTemp.substring(i, i + 1) + strResult;
                }
            } else {
                var strphannguyen = strTemp.substring(0, strTemp.lastIndexOf("."));
                var strphanthapphan = strTemp.substring(strTemp.lastIndexOf("."),
                    strTemp.length);
                var tam = 0;
                for (var i = strphannguyen.length; i >= 0; i--) {

                    if (strResult.length > 0 && tam == 4) {
                        strResult = "," + strResult;
                        tam = 1;
                    }

                    strResult = strphannguyen.substring(i, i + 1) + strResult;
                    tam = tam + 1;
                }
                strResult = strResult + strphanthapphan;
            }
            return strResult;
        }

        function GetNumber(str) {
            let count = 0;
            for (let i = 0; i < str.length; i++) {
                let temp = str.substring(i, i + 1);
                if (!(temp === ',' || temp === '.' || (temp >= 0 && temp <= 9))) {
                    alert(inputnumber);
                    return str.substring(0, i);
                }
                if (temp === ' ')
                    return str.substring(0, i);
                if (temp === '.') {
                    if (count > 0)
                        return str.substring(0, ipubl_date);
                    count++;
                }
            }
            return str;
        }

        function IsNumberInt(str) {
            for (let i = 0; i < str.length; i++) {
                let temp = str.substring(i, i + 1);
                if (!(temp === '.' || (temp >= 0 && temp <= 9))) {
                    alert(inputnumber);
                    return str.substring(0, i);
                }
                if (temp === ',') {
                    return str.substring(0, i);
                }
            }
            return str;
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(find, 'g'), replace);
        }
        
        function sendData(e) {
            $.ajax({
                type : 'GET',
                url : '{{ route('interest_rate') }}',
                data : {
                    'time' : $(e).parent().parent().find($('#time')).val(),
                    'money' : $(e).parent().parent().find($('#equivalent')).val(),
                    'interest_rate' : $(e).parent().parent().find($('#interest_rate')).val(),
                },
                success : function (result) {
                     $('.result').html(result);
                },
                error : function (error) {
                    console.log('Đã có lỗi xảy ra : ' + error);
                }
            });
        }
    </script>
@endpush