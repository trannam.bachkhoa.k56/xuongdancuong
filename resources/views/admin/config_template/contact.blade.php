@extends('admin.layout.admin')

@section('title', 'Cấu hình giao diện liên hệ')

@section('content')
    <section class="content-header">
        <h1>
            Thay đổi giao diện liên hệ
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Thay đổi giao diện</a></li>
            <li><a href="#">Liên hệ</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <div class="col-xs-12 col-md-3">
                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-body">
                        @include('admin.config_template.general.siderbar')
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-xs-12 col-md-9">
                <div class=" box box-primary">
            <div class="box-body">
               <div class="form-group">
                  <div class="radio">
                     <label>
                     <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                     <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px">
                     </label>
                  </div>
               </div>
               <div class="form-group">
                  <div class="radio">
                     <label>
                     <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" >
                     <img src="https://www.impulsapopular.com/wp-content/uploads/2018/03/4062-Innovar-y-emprender-%C2%BFes-lo-mismo.jpg" width="500px">
                     </label>
                  </div>
               </div>
               <div class="form-group">
                  <div class="radio">
                     <label>
                     <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                     <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px">
                     </label>
                  </div>
               </div>
            </div>
         </div>
            </div>
        </div>
    </section>
@endsection

