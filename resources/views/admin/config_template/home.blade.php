@extends('admin.layout.admin')

@section('title', 'Cấu hình giao diện trang chủ')

@section('content')
    <section class="content-header">
   <h1>
      Thay đổi giao diện trang chủ
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Thay đổi giao diện</a></li>
      <li><a href="#">Trang chủ</a></li>
   </ol>
</section>
<section class="content">
   <div class="row">
      <!-- form start -->
      <div class="col-xs-12 col-md-3">
         <!-- Nội dung thêm mới -->
         <div class="box box-primary">
            <div class="box-body">
               @include('admin.config_template.general.siderbar')
            </div>
            <!-- /.box-body -->
         </div>
         <!-- /.box -->
      </div>
      <div class="col-xs-12 col-md-9">
         <div class="box box-primary">
            <div class="box-body">
            <ul class="nav nav-tabs" role="tablist">
               <li role="presentation" class="active"><a href="#slide" aria-controls="slide" role="tab" data-toggle="tab">Slide</a></li>
               <li role="presentation"><a href="#block1" aria-controls="block1" role="tab" data-toggle="tab">khối 1</a></li>
               <li role="presentation"><a href="#block2" aria-controls="block2" role="tab" data-toggle="tab">khối 2</a></li>
               <li role="presentation"><a href="#block3" aria-controls="block3" role="tab" data-toggle="tab">khối 3</a></li>
               <li role="presentation"><a href="#block4" aria-controls="block4" role="tab" data-toggle="tab">khối 4</a></li>
               <li role="presentation"><a href="#block5" aria-controls="block5" role="tab" data-toggle="tab">khối 5</a></li>
               <li role="presentation"><a href="#block6" aria-controls="block6" role="tab" data-toggle="tab">khối 6</a></li>
               <li role="presentation"><a href="#block7" aria-controls="block7" role="tab" data-toggle="tab">khối 7</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane fade in active" id="slide">
                  <div class="form-group">
                  <div class="radio">
                     <label>
                     <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                     <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px">
                     </label>
                  </div>
               </div>
               <div class="form-group">
                  <div class="radio">
                     <label>
                     <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" >
                     <img src="https://www.impulsapopular.com/wp-content/uploads/2018/03/4062-Innovar-y-emprender-%C2%BFes-lo-mismo.jpg" width="500px">
                     </label>
                  </div>
               </div>
               <div class="form-group">
                  <div class="radio">
                     <label>
                     <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3">
                     <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px">
                     </label>
                  </div>
               </div>
               </div>
               <div role="tabpanel" class="tab-pane fade" id="block1">
                  <div class="form-group">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                      <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px"> &emsp;
                     <select class="" style="
                                  padding: 3px;
                                  border-radius: 2px;">
                    <option>Chọn danh mục</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                     </select>
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox">
                      <img src="https://blog.matcharesident.com/wp-content/uploads/2019/07/iStock-944453634-750x450.jpg" width="500px"> &emsp;
                     <select class="" style="
                                  padding: 3px;
                                  border-radius: 2px;">
                    <option>Chọn danh mục</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                     </select>
                    </label>
                  </div>
                </div>
               </div>
               <div role="tabpanel" class="tab-pane fade" id="block2">
               </div>
               <div role="tabpanel" class="tab-pane fade" id="block3">
               </div>
               <div role="tabpanel" class="tab-pane fade" id="block4">
               </div>
               <div role="tabpanel" class="tab-pane fade" id="block5">
               </div>
               <div role="tabpanel" class="tab-pane fade" id="block6">
               </div>
               <div role="tabpanel" class="tab-pane fade" id="block7">
               </div>
            </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection

