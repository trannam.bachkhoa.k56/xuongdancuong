@extends('admin.layout.admin')

@section('title', 'Cấu hình giao diện thông tin khách hàng nhận tư vấn')

@section('content')
    <section class="content-header">
        <h1>
            Thay đổi giao diện thông tin khách hàng nhận tư vấn
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class=" box box-primary">
                    <div class="box-body">
                        @if(!empty($domain))
                        <div class="form-group">
                            <span class="radio-advice">
                                <input <?php if($domain->show_advice_setting == 0 ){ echo 'checked = true'; } ?> type="radio" name="advice" class="change-advice" value="0" onchange="return change_advice(this);">
                                <label>Sử dụng giao diện mặc định</label>
                            </span>
                            <span class="radio-advice">
                                <input <?php if($domain->show_advice_setting != 0 || $domain->show_advice_setting != null){ echo 'checked = true'; } ?> type="radio" name="advice" class="change-advice" value="1" onchange="return change_advice(this);">
                                <label>Tùy biến giao diện</label>
                            </span>
                        </div>
                        @endif

                        <style type="text/css">
                            .radio-advice{
                                margin-right: 20px;
                            }
                            .radio-advice input{
                                margin: 10px;
                            }
                        </style>

                        <script type="text/javascript">
                            function change_advice(e){
                                if(confirm("Bạn có muốn thay đổi giao diện không?")){
                                    $(e).removeAttr('checked').prop('checked', true);
                                    var status = $(e).val();
                                    $.ajax({
                                        url: '{{route('change_status_adivce')}}',
                                        type: 'POST',                                    
                                        data: {
                                            status: status,
											_token: $('input[name=_token]').val()
                                           },
                                        })
                                        .done(function() {
                                            console.log("success");
                                        })
                                        .fail(function() {
                                            console.log("error");
                                        })
                                        .always(function() {
                                            console.log("complete");

                                            location.reload();
                                    });    
                                }   

                            };              

                        </script>
                        @if(!empty($domain))

                        @if($domain->show_advice_setting != 0 || $domain->show_advice_setting != null )
                                @if(!empty($customer_display))
                                     <div class="form-group">
                                        <label for="exampleEmail1">Họ và tên khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusName" name="statusName" onchange="showHideInput(this)" value="{{$customer_display->statusName}}" <?php if($customer_display->statusName != 0){ echo "checked";} ?> > Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Họ và tên khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleEmail1">Email của khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusEmail" name="statusEmail" onchange="showHideInput(this)" value="{{$customer_display->statusEmail}}" <?php if($customer_display->statusEmail != 0){ echo "checked";} ?>> Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Email của khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleEmail1">Số điện thoại của khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusPhone" name="statusPhone" onchange="showHideInput(this)" value="{{$customer_display->statusPhone}}" <?php if($customer_display->statusPhone != 0){ echo "checked";} ?> > Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Số điện thoại của khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleEmail1">Nội dung ghi chú của khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusMessage" name="statusMessage" onchange="showHideInput(this)" value="{{$customer_display->statusMessage}}" <?php if($customer_display->statusMessage != 0){ echo "checked";} ?>> Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Nội dung ghi chú của khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="button">Nội dung nút đăng ký</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-6">
                                                    <input type="text" onchange="changeContent(this)" name="buttonName" class="form-control buttonName" id="button" placeholder="Nội dung nút đăng ký" value="{{$customer_display->buttonName}}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-warning"><span id="content"> ĐĂNG KÝ TƯ VẤN </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="notification">Nội dung thông báo khi khách hàng đăng ký thành công</label>
                                        <input type="text" value="{{$customer_display->message}}" name="message" id="notification" placeholder="Nội dung thông báo" class="form-control message" required>
                                    </div>
                                    <div class="form-group">
                                        <button onclick="return editAdvice();" class="btn btn-info">Lưu thay đổi!</button>
                                    </div>

                                @else
                                     <div class="form-group">
                                        <label for="exampleEmail1">Họ và tên khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusName" value="" name="statusName" onchange="showHideInput(this)"> Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Họ và tên khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleEmail1">Email của khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusEmail" value="" name="statusEmail" onchange="showHideInput(this)"> Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Email của khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleEmail1">Số điện thoại của khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusPhone" value="" name="statusPhone" onchange="showHideInput(this)" > Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Số điện thoại của khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleEmail1">Nội dung ghi chú của khách hàng</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="showHide statusMessage" value="" name="statusMessage" onchange="showHideInput(this)"> Hiển thị
                                                </div>
                                                <div class="col-md-10">
                                                    <input type="text" value="" placeholder="Nội dung ghi chú của khách hàng" disabled class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="button">Nội dung nút đăng ký</label><br>
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0">
                                                <div class="col-md-6">
                                                    <input type="text" onchange="changeContent(this)" name="buttonName" class="form-control buttonName" id="button" placeholder="Nội dung nút đăng ký" value="ĐĂNG KÝ TƯ VẤN" />
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="button" class="btn btn-warning"><span id="content"> ĐĂNG KÝ TƯ VẤN </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="notification">Nội dung thông báo khi khách hàng đăng ký thành công</label>
                                        <input type="text" value="Nội dung thông báo" name="message" id="notification" placeholder="Nội dung thông báo" class="form-control message" required>
                                    </div>
                                    <div class="form-group">
                                        <button onclick="return submitAdvice();" class="btn btn-info">Lưu thay đổi</button>
                                    </div>
                                @endif
                        @else
                         <div class="form-group">
                                <label for="exampleEmail1">Họ và tên khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2">
                                          <!--   <input type="checkbox" class="showHide" name="name" onchange="showHideInput(this)" checked> Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Họ và tên khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleEmail1">Email của khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2">
                                         <!--    <input type="checkbox" class="showHide" onchange="showHideInput(this)" checked> Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Email của khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleEmail1">Số điện thoại của khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2">
                                            <!-- <input type="checkbox" class="showHide" onchange="showHideInput(this)" > Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Số điện thoại của khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleEmail1">Nội dung ghi chú của khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2"><!-- 
                                            <input type="checkbox" class="showHide" onchange="showHideInput(this)"> Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Nội dung ghi chú của khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="button">Nội dung nút đăng ký</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-6">
                                            <input type="text" onchange="changeContent(this)" class="form-control" id="button" placeholder="Nội dung nút đăng ký" disabled="disabled" value="ĐĂNG KÝ TƯ VẤN" />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-warning"><span id="content"> ĐĂNG KÝ TƯ VẤN </span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="notification">Nội dung thông báo khi khách hàng đăng ký thành công</label>
                                <input type="text" value="" disabled="disabled" name="notification" id="notification" placeholder="Nội dung thông báo" class="form-control">
                            </div>
                        @endif


                        @else

                            <div class="form-group">
                                <label for="exampleEmail1">Họ và tên khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2">
                                          <!--   <input type="checkbox" class="showHide" name="name" onchange="showHideInput(this)" checked> Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Họ và tên khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleEmail1">Email của khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2">
                                         <!--    <input type="checkbox" class="showHide" onchange="showHideInput(this)" checked> Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Email của khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleEmail1">Số điện thoại của khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2">
                                            <!-- <input type="checkbox" class="showHide" onchange="showHideInput(this)" > Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Số điện thoại của khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleEmail1">Nội dung ghi chú của khách hàng</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-2"><!-- 
                                            <input type="checkbox" class="showHide" onchange="showHideInput(this)"> Hiển thị -->
                                        </div>
                                        <div class="col-md-12">
                                            <input type="text" value="" placeholder="Nội dung ghi chú của khách hàng" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
							{!! csrf_field() !!}
                            <div class="form-group">
                                <label for="button">Nội dung nút đăng ký</label><br>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="col-md-6">
                                            <input type="text" onchange="changeContent(this)" class="form-control" id="button" placeholder="Nội dung nút đăng ký" disabled="disabled" value="ĐĂNG KÝ TƯ VẤN" />
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-warning"><span id="content"> ĐĂNG KÝ TƯ VẤN </span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="notification">Nội dung thông báo khi khách hàng đăng ký thành công</label>
                                <input type="text" value="" disabled="disabled" name="notification" id="notification" placeholder="Nội dung thông báo" class="form-control">
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        function changeContent(e) {
            $('#content').text($(e).val());
        }

        function showHideInput(e){
            if ($(e).prop('value') != 0) {

                $(e).val('0');
            }
            else{
                $(e).val('1');
            }
            
        }
    </script>
    <script type="text/javascript">
        function submitAdvice(){
            var statusName = $('.statusName').val();
            var statusEmail = $('.statusEmail').val();
            var statusPhone = $('.statusPhone').val();
            var statusMessage = $('.statusMessage').val();
            var buttonName = $('.buttonName').val();
            var message = $('.message').val();

            $.ajax({
                url: '{{route('add_customer_adivce')}}',
                type: 'POST',
                data: {
                    statusName: statusName,
                    statusEmail: statusEmail,
                    statusPhone: statusPhone,
                    statusMessage: statusMessage,
                    buttonName: buttonName,
                    message: message,
					_token: $('input[name=_token]').val()
                },
            })
            .done(function() {
                console.log("success");
                
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
                 location.reload();
                alert ("Lưu thành công thay đổi !") 
            });
            
        }
    </script>

     <script type="text/javascript">
        function editAdvice(){
            var statusName = $('.statusName').val();
            var statusEmail = $('.statusEmail').val();
            var statusPhone = $('.statusPhone').val();
            var statusMessage = $('.statusMessage').val();
            var buttonName = $('.buttonName').val();
            var message = $('.message').val();

            $.ajax({
                url: '{{route('edit_customer_adivce')}}',
                type: 'POST',
                data: {
                    statusName: statusName,
                    statusEmail: statusEmail,
                    statusPhone: statusPhone,
                    statusMessage: statusMessage,
                    buttonName: buttonName,
                    message: message,
					_token: $('input[name=_token]').val()
                },
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
                alert ("Lưu thành công thay đổi !") 
            });
            
        }
    </script>
@endsection

