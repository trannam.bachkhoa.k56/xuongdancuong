@extends('admin.layout.admin')

@section('title', 'Cơ chế hoa hồng' )

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Cơ chế hoa hồng
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Danh sách Cơ chế hoa hồng</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                @if (empty($userIdPartner))
                <div class="box-header">
                    <a  href="{{ route('commission.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                </div>
                @endif
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="5%">STT</th>
                                <th>Nhân viên</th>
                                <th>Lương cơ bản</th>
                                <th>Phụ cấp</th>
                                <th>Bảo hiểm</th>
                                <th>Doanh số</th>
                                <th>Hoa hồng</th>
                                <th>Hoa hồng nhận được</th>
                                <th>Tổng thu nhập / tháng</th>
                                @if (empty($userIdPartner))
                                    <th>Xóa</th>
                                @endif
                            </tr>
                        </thead>
                        @foreach ($commissions as $id => $commission)
                        <tbody>
                            <tr>
                                <td width="5%">{{ ($id + 1) }}</td>
                                @if (empty($userIdPartner))
                                    <td><a href="{{ route('commission.edit',[ 'commission_id' => $commission->commission_id]) }}">{{ $commission->position }}</a></td>
                                @else
                                    <td>{{ $commission->position }}</td>
                                @endif
                                <td>{{ number_format($commission->salary, 0,",",".") }} đ</td>
                                <td>{{ number_format($commission->sub_salary, 0,",",".") }} đ</td>
                                <td>{{ number_format($commission->security, 0,",",".") }} đ</td>
                                <td>{{ number_format($commission->revenue, 0,",",".") }} đ</td>
                                <td>{{ number_format($commission->commission, 0,",",".") }} %</td>
                                <td>{{ number_format($commission->commission * $commission->revenue / 100, 0,",",".") }} đ</td>
                                <td>{{ number_format($commission->commission * $commission->revenue / 100 + $commission->salary + $commission->sub_salary + $commission->security, 0,",",".") }} đ</td>
                                @if (empty($userIdPartner))
                                <td>
                                    <a  href="{{ route('commission.destroy', ['$commission_id' => $commission->commission_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </td>
                                @endif
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@include('admin.partials.popup_delete')
@include('admin.partials.popup_delete')
@endsection

