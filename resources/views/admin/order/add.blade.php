@extends('admin.layout.admin')

@section('title', 'Thêm mới đơn hàng')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới đơn hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Thêm mới đơn hàng</li>
        </ol>

    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box-header -->
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-lg-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Sản phẩm</h3>
                                <p><i>(Nếu bạn chưa có sản phẩm tạo đơn hàng, vui lòng truy cập phần <a href="{{ route('products.index') }}">sản phẩm</a> để tạo đơn hàng)</i></p>
                            </div>

                            <div class="box-body">
                                <table class="table table-bordered table-striped">
									<tr>
										<th>Sản phẩm</th>
										<th>Số lượng</th>
										<th>Loại hình</th>
										<th>Đơn giá</th>
										<th>Triết khấu(%)</th>
										<th>Thanh toán</th>
										<th>Vat(%)</th>
										<th>Thành tiền</th>
									</tr>
									<tr>
										<td>
											<select name="products" class="select2">
												@foreach (\App\Entity\Product::getAllProduct() as $product)
													<option value="{{ $product->code }}">
														{{ $product->title }}
													</option>
												@endforeach
											</select>
										</td>
										<td>
											<input class="form-control" value="" type="number" name="quantity"/> 
										</td>
										<td>
											<select name="sale" class="select2">
												<option value="1" >Bán buôn</option>
												<option value="2" >Bán lẻ</option>
												<option value="3" >khuyến mãi</option>
											</select>
										</td>
										<td>
											<input class="form-control" value="" type="number" name="discount"/>
										</td>
										<td>
											<input class="form-control" value="" type="number" name="payment"/>
										</td>
										<td>	
											<select name="sale" class="select2">
												<option value="1" >0%</option>
												<option value="2" >5%</option>
												<option value="3" >10%</option>
												<option value="4" >15%</option>
												<option value="5" >20%</option>
											</select>
										</td>
										<td>
											
										</td>
									</tr>
									<tr>
										<td>
											thêm mới
										</td>
									</tr>
								</table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Tạo mới đơn hàng</button>
                </div>
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
@endsection

