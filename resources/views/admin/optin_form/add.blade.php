@extends('admin.layout.admin')

@section('title', ' Thêm mới Form')

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Thêm mới Form
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">optin</a></li>
		<li class="active">Thêm mới</li>
	</ol>
</section>

<section class="content">

	<div class="main box" id="main-content" style="padding-top:20px ">
		<div class="container-fluid">
			<form method="POST" action="{{ route('optin-form.store') }}">
				{!! csrf_field() !!}
				<div class="row mb20 create_form ">

					<div class="col-md-12 pt10">
						<div class="col-md-6 pl0 form-group">
							<label>Tiêu đề Optin form&nbsp;<span class="red">(*)</span></label>
							<input type="text" class="form-control titleform" value="" name="form_title">
						</div>

						<div class="col-md-6 pr0">
							<label>Mô tả</label>
							<textarea class="form-control" rows="2" name="form_description"></textarea>
						</div>
					</div>

					<div class="col-md-12 tab-content mb30" style="padding-top:20px ">
						<div class="col-md-12">
							<div class="col-md-6 pr10">
								<div class="row">

									<lable style="font-weight: bold; margin-bottom: 20px " >
										Các thông tin cần lấy:
									</lable>

									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
											<tr>
												<th width="5%" class="tc">STT</th>
												<th>Trường dữ liệu</th>
												<th width="15%" class="tc">Sử dụng</th>

											</tr>
											</thead>

											<tbody>

											<tr>
												<td class="tc">1</td>
												<td>Tên Khách hàng</td>
												<td class="tc">
													<input type="checkbox" value="" class="chk_basic_info chk_cf_name" name="showName" onchange="return changeValue(this);" >
												</td>
											</tr>
											<tr>
												<td class="tc">2</td>
												<td>Điện thoại</td>
												<td class="tc">
													<input type="checkbox" value="" class="chk_basic_info chk_cf_name" name="showPhone" onchange="return changeValue(this);" >
												</td>

											</tr>
											<tr>
												<td class="tc">3</td>
												<td>Email</td>
												<td class="tc">
													<input type="checkbox" value="" class="chk_basic_info chk_cf_name" name="showEmail" onchange="return changeValue(this);" >
												</td>

											</tr>

											<tr>
												<td class="tc">4</td>
												<td>Địa chỉ</td>
												<td class="tc">
													<input type="checkbox" value="" class="chk_basic_info chk_cf_name" name="showAddress" onchange="return changeValue(this);" >
												</td>

											</tr>

											<tr>
												<td class="tc">5</td>
												<td>Mô tả khách hàng</td>
												<td class="tc">
													<input type="checkbox" value="" class="chk_basic_info chk_cf_name" name="showDescription" onchange="return changeValue(this);" >
												</td>
											</tr>

											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-6 pl10">

								<div class="form-group">
									<label>Địa chỉ trả về sau khi đăng ký: </label>
									<input type="text" class="form-control titleform" value="" name="formRedirect">
								</div>

								<div class="form-group">
									<label>Chiến dịch đổ về</label>
									<select class="form-control" name="campaign">
										<option value="">-- Chọn chiến dịch --</option>
										@foreach($campaigns as $campaign)
											<option value="{{$campaign->campaign_customer_id}}">{{$campaign->campaign_title}}</option>
										@endforeach
									</select>
								</div>

							</div>
							<div class="row">
								<div class="col-md-12 b-gray" style="padding-bottom:10px">
									<button class="btn btn-info" type="submit" id="save-form">Lưu</button>
								</div>
							</div>
						</div>

					</div>
				</div>



			</form>

		</div>
	</div>

	<script type="text/javascript">

		function changeValue(e){
			var value = $(e).val();
			if(value != 1){
				$(e).val('1');
			}
			else{
				$(e).val('0');
			}

		}

	</script>

</section>

@endsection


