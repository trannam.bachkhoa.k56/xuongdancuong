@extends('admin.layout.admin')

@section('title', 'Danh sách domain')

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Form thông tin
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Danh sách Form thông tin</a></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<a  href="{{ route('optin-form.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="domains" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th width="5%">ID</th>
							<th>Tên form</th>
							<th>Mô tả</th>
							<th>Truy cập</th>
							<th>Hôm Nay</th>
							<th>Tổng số</th>
							<th>Thao tác</th>
						</tr>
						</thead>

						<tbody>
						@foreach($forms as $id => $form)
							<tr>
								<td width="5%">{{$id+1}}</td>
								<td>{{isset($form->form_title) ? $form->form_title : ''}}</td>
								<td>{{isset($form->form_description) ? $form->form_description : ''}}</td>
								<td></td>
								<td></td>
								<td></td>
								<td>
									<a href="{{ route('optin-form.edit', ['form_id' => $form->form_id]) }}">
										<button class="btn btn-primary">
											<i class="fa fa-pencil" aria-hidden="true"></i>
										</button>
									</a>
									<a href="{{ route('optin-form.destroy', ['form_id' => $form->form_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
										<i class="fa fa-trash-o" aria-hidden="true"></i>
									</a>
								</td>
							</tr>
						@endforeach
						</tbody>

						<tfoot>
						<tr>
							<th width="5%">ID</th>
							<th>Tên form</th>
							<th>Mô tả</th>
							<th>Truy cập</th>
							<th>Hôm Nay</th>
							<th>Tổng số</th>
							<th>Thao tác</th>
						</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			{{ $forms->links() }}
					<!-- /.box -->
		</div>
		<div class="col-xs-12 col-md-6">
			@include('admin.partials.form_support_marketing')
		</div>
	</div>
</section>
@include('admin.partials.popup_delete')
@endsection


