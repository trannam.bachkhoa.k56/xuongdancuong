@extends('admin.layout.admin')

@section('title', 'Tóm lược thanh toán')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            TÓM LƯỢC THANH TOÁN
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Tóm lược thanh toán</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-6 ">
                        <h2>Tín dụng còn lại của bạn</h2>
                    </div>
                    <div class="col-xs-6 ">
                        <h2>{!! !empty($payment) ? number_format($payment->coin_total) : 0 !!} đ</h2>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#payment">Thanh toán</button>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-title">
                        <h2>Lịch sử giao dịch</h2>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">STT</th>
                                <th>Nội dung</th>
                                <th>Ngày sử dụng</th>
                                <th>Số tiền</th>
                            </tr>
                            </thead>
                            @foreach ($historyPayments as $id => $historyPayment)
                            <tbody>
                                <td>{{ ($id+1) }}</td>
                                <td>{{ $historyPayment->content }}</td>
                                <td>{{ $historyPayment->created_at }}</td>
                                <td>{{ number_format($historyPayment->money) }}đ</td>
                            </tbody>
                            @endforeach

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nạp tiền vào tài khoản</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{!! route('thong-tin-thanh-toan-vi-dien-tu') !!}" method="get" >
                    <div class="modal-body">

                            <div class="form-group">
                                <input type="number" class="form-control" name="money" value="" min="0" />
                            </div>
                            <input type="hidden" class="form-control" name="type" value="1" min="0" />
                            <input type="hidden" class="form-control" name="month" value="8080" min="0" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary">Thanh toán</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

