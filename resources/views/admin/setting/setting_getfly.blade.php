
<div class="row">
    <div class="col-xs-12 col-md-6">
        <!-- Nội dung thêm mới -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Cài đặt getfly</h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
                <form action="{{ route('updateSettingGetFly') }}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label>api key</label>
                        <input type="text" class="form-control" name="api_key" value="{{ \App\Entity\SettingGetfly::getApiKey() }}"
                               placeholder="Mã api_key getfly cung cấp..." />
                    </div>
                    <div class="form-group">
                        <label>Đường dẫn website</label>
                        <input type="text" class="form-control" name="base_url" value="{{ \App\Entity\SettingGetfly::getBaseUrl() }}"
                               placeholder="https://vn3c.getflycrm.com/..." />
                    </div>
                    <div class="form-group">
                        <label>Chọn chiến dịch getfly</label>
                        <select class="form-control select2" name="campaign" >
                            @if (isset($campaigns['decode']) && !empty($campaigns['decode']))
                                @foreach ($campaigns['decode'] as $campaign)
									@if (isset($campaign['token_api']))
                                    <option value="{{ $campaign['token_api'].'-'.$campaign['campaign_id'] }}"
                                            {{ (\App\Entity\SettingGetfly::getCampaign() == $campaign['token_api'] ) ? 'selected' : '' }}
                                    >{{ $campaign['campaign_name'] }}</option>
									@endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
