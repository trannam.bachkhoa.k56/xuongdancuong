<div class="row">
    <div class="col-xs-12 col-md-12">
        <!-- Nội dung thêm mới -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Màu sắc trên trang</h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body " id="smtp"   >
                <form action="{{ route('update_color') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" value="0" name="method"/>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Màu chữ toàn trang: </label>
                                <input type="color" class="" name="text_full" placeholder="Màu chữ toàn trang"  value="{{ !empty($colorWebsite->text_full) ? $colorWebsite->text_full : '' }}" />
                            </div>
                            <div class="form-group">
                                <label>Màu nền chủ đạo: </label>
                                <input type="color" class="" name="background_full" value="{{ !empty($colorWebsite->background_full) ? $colorWebsite->background_full : '' }}" placeholder="Màu nền chủ đạo" />
                            </div>
                            <div class="form-group">
                                <label>Màu chữ khi di chuột vào: </label>
                                <input type="color" class="" name="color_hover" value="{{ !empty($colorWebsite->color_hover) ? $colorWebsite->color_hover : '' }}" placeholder="Màu chữ khi di chuột vào" />
                            </div>
                            <div class="form-group">
                                <label>Màu của các thẻ tiêu đề: </label>
                                <input type="color" class="" name="color_title" value="{{ !empty($colorWebsite->color_title) ? $colorWebsite->color_title : '' }}" placeholder="Màu của các thẻ tiêu đề" />
                            </div>
                            <div class="form-group">
                                <label>Màu chữ cuối trang: </label>
                                <input type="color" class="" name="color_footer" value="{{ !empty($colorWebsite->color_footer) ? $colorWebsite->color_footer : '' }}" placeholder="Màu chữ cuối trang" />
                            </div>
                        </div>




                        <div class="box-body" id="smtp">
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <!-- /.box -->
    </div>
</div>
