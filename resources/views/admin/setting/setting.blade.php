@extends('admin.layout.admin')

@section('title', 'Cài đặt thanh toán')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cài thanh toán
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Cài đặt thanh toán</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <!-- <li role="presentation" ><a href="#payment" aria-controls="home" role="tab" data-toggle="tab">Cài đặt thanh toán</a></li> -->
                    <li role="presentation" class="active"><a href="#getfly" aria-controls="profile" role="tab" data-toggle="tab">Cài đặt getfly</a></li>
                    <li role="presentation"><a href="#email" aria-controls="profile" role="tab" data-toggle="tab">Cấu hình email</a></li>
                    <li role="presentation"><a href="#color" aria-controls="profile" role="tab" data-toggle="tab">Màu sắc</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="getfly">
                        @include('admin.setting.setting_getfly')
                    </div>

                    <div role="tabpanel" class="tab-pane" id="email">
                        @include('admin.setting.setting_email')
                    </div>

                    <div role="tabpanel" class="tab-pane" id="color">
                        @include('admin.setting.setting_color')
                    </div>
                </div>
            </div>
        </div>

        <script>
            function changeMethod(e) {
                var val = $(e).val();
                if (val == 0) {
                    $('#smtp').removeClass('hide');
                    $('#api').addClass('hide');
                } else {
                    $('#smtp').addClass('hide');
                    $('#api').removeClass('hide');
                }
            }
        </script>
    </section>
    @include('admin.partials.popup_delete')
@endsection

