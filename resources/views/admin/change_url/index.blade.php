@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa url website')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Domain thương hiệu
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Domains</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
           
                <div class="col-xs-12 col-md-6">
				<form role="form" action="{{ route('change_url') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary" style="padding: 0 10px">
						<div class="box-header with-border">
                            <p><h3 class="box-title">Cài đặt tên miền thương hiệu ( ví dụ ketcongnghe.com không phải ket.moma.vn)
</h3></p>
							<p style="">
								<strong>Bước 1: </strong> Nếu bạn chưa có tên miền thì <a href="http://hostvn.net/moma" target="_blank">mua tại đây</a>. hoặc các đơn vị trên toàn quốc.
							</p>
							<p style="">
								<strong>Bước 2: </strong> Nếu bạn đã có hặc mua xong  tên miền xin giúp moma trỏ tên miền về địa chỉ 123.30.186.20. 	</p>
							<p>	cách làm như sau: </p>
							<p>	Tại nơi mua tên miền bạn chỉ cần dùng Gmail gửi nội dung như sau: </p>
							<p>	"Chào bạn tôi có tên miên ví dụ( ketcongnghe.com) bạn giúp tôi trỏ về địa chỉ IP 123.30.186.20". </p>
							<p>	sau khoảng 1 phút thì các bạn chuyên bước 3. </p>
						
							<p style="">
								<strong>Bước 3: </strong> nhập tên miền của bạn và nhấn nâng cấp là xong( moma sẽ thu phí seo hộ bạn tượng trưng ở đây).
							</p>
							<p><i>Ghi chú:</i></p>
							<p style="">
							 Tên tên miền của bạn, không chưa https, không http, không ký hiệu (/), không ký hiệu (:) </br><strong>Ví dụ : moma.vn, nhanvang.com, namtrust.com, sukien.net, ....</strong>
							</p>
							<p>
								<strong>Bước 4: </strong> vào lại trang chính xem là tên miền chạy chưa, như vậy ko ai biết bên bạn dùng nền tảng nào.
							</p>
                        </div>     
                        <div class="form-group">
                            <input type="text" name="url" class="form-control" placeholder="Nhập tên miền của bạn" value="{{isset($domain) ? $domain : ''}}">
                        </div>
						@if (isset($_GET['error']))
							<p style="color: red">Tên miền đã được một người khác trỏ về!</p>
						@endif
						@if (isset($domain))
							<p><strong>Xem website: </strong><a href="http://{{ isset($domain) ? $domain : ''}}" target="_blank">{{isset($domain) ? $domain : ''}}</a></p>
						@endif
                        <!-- /.box-header -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Thay đổi url</button>
                            </div>
                    </div>
                    <!-- /.box -->
				</form>
                </div>
				
			
				
				<div class="col-xs-12 col-md-6">
					@include('admin.partials.form_support_marketing')
				</div>
           
        </div>
    </section>
@endsection

