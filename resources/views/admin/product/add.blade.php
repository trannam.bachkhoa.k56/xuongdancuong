@extends('admin.layout.admin')

@section('title', 'Thêm mới dịch vụ (sản phẩm)')

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Thêm mới bài viết
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Dịch vụ (sản phẩm)</a></li>
		<li class="active">Thêm mới</li>
	</ol>
</section>

<section class="content">
	<form role="form" action="{{ route('products.store') }}" method="POST" id="formPost">
		{!! csrf_field() !!}
		{{ method_field('POST') }}
		<div>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Dịch vụ (sản phẩm)</a></li>
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Nội dung dịch vụ (sản phẩm)</a></li>
				<!--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Hỗ trợ seo</a></li>-->
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="home">
					<div class="row">
						<div class="col-xs-12 col-md-8">
							<div class="box box-primary boxCateScoll">
								<div class="box-body">
									<div class="form-group">
										<label for="exampleInputEmail1">Tên dịch vụ (sản phẩm)</label>
										<input type="text" class="form-control" name="title" placeholder="Tên sản phẩm" required id="stepProductCreate1">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Mã dịch vụ (sản phẩm)</label>
										<input type="text" class="form-control" name="code" placeholder="Mã sản phẩm" id="stepProductCreate2">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Giá</label>
										<input type="text" class="form-control formatPrice" name="price" placeholder="Giá sản phẩm" min="1" id="stepProductCreate3">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Giá khuyến mãi</label>
										<input type="text" class="form-control formatPrice" name="discount" placeholder="Giá khuyến mãi" min="1" step="any" id="stepProductCreate4">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Giá bán buôn</label>
										<input type="text" class="form-control formatPrice" name="wholesale" placeholder="Giá bán buôn" min="1" step="any" id="">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Giá vốn</label>
										<input type="text" class="form-control formatPrice" name="cost" placeholder="Giá vốn" min="1" step="any" id="">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Mô tả ngắn dịch vụ (sản phẩm)</label>
                                        <textarea rows="4" class="form-control" name="description"
												  placeholder=""></textarea>
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Nội dung chi tiết dịch vụ (sản phẩm)</label>
										<textarea class="editor" id="content" name="content" rows="10" cols="80"/></textarea>
									</div>

									<script>
										$('.formatPrice').priceFormat({
											prefix: '',
											centsLimit: 0,
											thousandsSeparator: '.'
										});
									</script>




								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-4">
							@include('admin.partials.check_seo')

							<div class="box box-primary boxCateScoll"  id="stepProductCreate6">
								<div class="box-header with-border">
									<h3 class="box-title">Chọn danh mục</h3>
								</div>
								<!-- /.box-header -->

								<div class="box-body" style="height: 300px; overflow-y: auto;">

									@foreach($categories as $cate)
										<div class="form-group">
											<label>
												<input type="checkbox" name="parents[]" value="{{ $cate->category_id }}" class="flat-red">
												{{ $cate->title }}
											</label>
										</div>
										@foreach($cate['sub_children'] as $child)
											<div class="form-group">
												<label>
													<input type="checkbox" name="parents[]" value="{{ $child['category_id'] }}" class="flat-red" >
													{{ $child['title'] }}
												</label>
											</div>
										@endforeach
									@endforeach

								</div>



							</div>

							<div class="box box-primary boxCateScoll" >
								<div class="box-body">
									<div class="form-group" id="stepProductCreate5">
										<label>Ảnh dịch vụ (sản phẩm)</label>
										<input type="button" onclick="return uploadImage(this);" value="Chọn ảnh sản phẩm"
											   size="20" />
										<img src="" width="80" height="70"/>
										<input name="image" type="hidden" value=""/>
									</div>

									<div class="form-group">
										<label>Danh sách hình ảnh dịch vụ (sản phẩm)</label>
										<input type="button" onclick="return openKCFinder(this);" value="Chọn ảnh"
											   size="20"/>
										<div class="imageList">
										</div>
										<input name="image_list" type="hidden" value=""/>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="profile">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="box box-primary boxCateScoll">
								<div class="box-body">

									<!--<div class="form-group">
                                        <label for="exampleInputEmail1">Thuộc tính sản phẩm</label>
                                        <textarea class="editor" id="properties" name="properties" rows="10" cols="80"/></textarea>
                                    </div>-->

								</div>
							</div>
						</div>

						<div class="col-xs-12 col-md-8">
							<div class="box box-primary boxCateScoll">
								<div class="box-body">

									<div class="form-group">
										<label>Bộ lọc:</label>
										<select class="select2 form-control" name="filter[]" multiple="multiple">
											@foreach($filter as $filters)
												<option value="{{ $filters->name_filter }}">{{ $filters->name_filter }}</option>
											@endforeach
										</select>
									</div>

									@foreach ($typeInputs as $typeInput)
										<div class="form-group">
											<label>{{ $typeInput->title }} </label>
											@if($typeInput->type_input == 'one_line')
												<input type="text" class="form-control" name="{{$typeInput->slug}}" placeholder="{{ $typeInput->placeholder }}" />
											@endif

											@if($typeInput->type_input == 'multi_line')
												<textarea rows="4" class="form-control" name="{{$typeInput->slug}}" placeholder="{{ $typeInput->placeholder }}"></textarea>
											@endif

											@if($typeInput->type_input == 'image')
												<input type="button" onclick="return uploadImage(this);" value="Chọn ảnh"
													   size="20"/>
												<img src="" width="80" height="70"/>
												<input name="{{$typeInput->slug}}" type="hidden" value=""/>
											@endif

											@if($typeInput->type_input == 'image_list')
												<label>Danh sách hình ảnh</label>
												<input type="button" onclick="return openKCFinder(this);" value="Chọn ảnh"
													   size="20"/>
												<div class="imageList">
												</div>
												<input name="{{$typeInput->slug}}" type="hidden" value=""/>
											@endif

											@if($typeInput->type_input == 'editor')
												<textarea class="editor" id="{{$typeInput->slug}}" name="{{$typeInput->slug}}" rows="10" cols="80"/></textarea>
											@endif

											@if(!in_array($typeInput->type_input, array('one_line', 'multi_line', 'image', 'editor', 'image_list'), true) && strpos($typeInput->type_input, 'listMultil') >= 0)
												<?php $slugSubPost = str_replace('listMultil', '', $typeInput->type_input);?>
												<select name="{{$typeInput->slug}}[]" class="select2 form-control" multiple="multiple">

													@foreach(\App\Entity\SubPost::showSubPost($slugSubPost, 100) as $subPost)
														<option value="{{ $subPost->slug }}">{{ $subPost->title }}</option>
													@endforeach
												</select>
											@elseif (!in_array($typeInput->type_input, array('one_line', 'multi_line', 'image', 'editor', 'image_list'), true))
												<select name="{{$typeInput->slug}}" class="form-control">
													@foreach(\App\Entity\SubPost::showSubPost($typeInput->type_input, 100) as $subPost)
														<option value="{{ $subPost->title }}">{{ $subPost->title }}</option>
													@endforeach
												</select>
											@endif
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="messages">
					<div class="row">
						<div class="col-xs-12 col-md-8">
							<!--<div class="box box-primary">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tags (Viết tag cách nhau bởi dấu ,)</label>
                                        <input type="text" class="form-control" name="tags" placeholder="Tags" >
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Thẻ title</label>
                                        <input type="text" class="form-control" name="meta_title" placeholder="Thẻ title" />
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Thẻ description</label>
                                        <input type="text" class="form-control" name="meta_description" placeholder="Thẻ description" />
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Thẻ keyword</label>
                                        <input type="text" class="form-control" name="meta_keyword" placeholder="Thẻ keyword" />
                                    </div>
                                </div>
                            </div>-->

						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="box-footer">
			<button type="submit" class="btn btn-primary" id="stepProductCreate7">Thêm mới</button>
		</div>
	</form>

</section>
<script>
	$(document).ready(function() {
		if (RegExp('multipage', 'gi').test(window.location.search)) {
			var intro = introJs();
			intro.setOptions({
				steps: [
					{
						element: document.querySelector('#stepProductCreate1'),
						intro: "Nhập tên sản phẩm của bạn."
					},
					{
						element: document.querySelector('#stepProductCreate2'),
						intro: "Nhập mã sản phẩm của bạn."
					},
					{
						element: document.querySelector('#stepProductCreate3'),
						intro: "Nhập giá sản phẩm của bạn. Không nên bỏ trống."
					},
					{
						element: document.querySelector('#stepProductCreate4'),
						intro: "Nhập giá khuyến mãi sản phẩm của bạn. Nếu không có giá bạn có thể bỏ trống."
					},
					{
						element: document.querySelector('#stepProductCreate5'),
						intro: "Ấn vào để tải hình ảnh sản phẩm của bạn lên website."
					},
					{
						element: document.querySelector('#stepProductCreate6'),
						intro: "Tích chọn danh mục sản phẩm mà bạn muốn hiển thị"
					},
					{
						element: document.querySelector('#stepProductCreate7'),
						intro: "Ấn vào đây để lưu sản phẩm vừa thêm"
					}
				]
			});

			intro.setOption('doneLabel', 'Thay đổi thông tin trang').start().oncomplete(function() {
				window.location.href = '/admin/information?multipage=true';
			});
		}
	});
</script>
@endsection

