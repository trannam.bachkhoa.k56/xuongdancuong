@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa '.$post->title)

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Chỉnh sửa dịch vụ (sản phẩm){{$post->title}}
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Dịch vụ (sản phẩm)</a></li>
		<li class="active">Chỉnh sửa</li>
	</ol>
</section>

<section class="content">
	<form role="form" action="{{ route('products.update', ['product_id' => $product->product_id]) }}" method="POST" id="formPost">
		{!! csrf_field() !!}
		{{ method_field('PUT') }}
		<div>
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Dịch vụ (sản phẩm)</a></li>
				<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Nội dung dịch vụ (sản phẩm)</a></li>
				<!--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Hỗ trợ seo</a></li>-->
			</ul>

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="home">
					<div class="row">
						<div class="col-xs-12 col-md-8">
							<div class="box box-primary">
								<div class="box-body">
									<div class="form-group">
										<label for="exampleInputEmail1">Tên dịch vụ(sản phẩm)</label>
										<input type="text" class="form-control" name="title" placeholder="Tên dịch vụ(sản phẩm)" value="{{$post->title}}" required>
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Mã dịch vụ(sản phẩm)</label>
										<input type="text" class="form-control" name="code" placeholder="Mã dịch vụ(sản phẩm)" value="{{$product->code}}" />
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Giá</label>
										<input type="text" class="form-control formatPrice" name="price" placeholder="Giá dịch vụ(sản phẩm)"  value="{{$product->price}}" />
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Giá khuyến mãi</label>
										<input type="text" class="form-control formatPrice" name="discount" placeholder="Giá khuyến mãi"  value="{{$product->discount}}" />
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Giá bán buôn</label>
										<input type="text" class="form-control formatPrice" name="wholesale" placeholder="Giá bán buôn" min="1" step="any" value="{{$product->wholesale}}" id="">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Giá vốn</label>
										<input type="text" class="form-control formatPrice" name="cost" placeholder="Giá vốn" min="1" step="any" value="{{$product->cost}}" id="">
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Mô tả</label>
                                            <textarea rows="4" class="form-control" name="description"
													  placeholder="">{!! nl2br($post->description) !!}</textarea>
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Nội dung</label>
										<textarea class="editor" id="content" name="content" rows="10" cols="80"/>{{ $post->content }}</textarea>
									</div>



								</div>
							</div>
						</div>

						<div class="col-xs-12 col-md-4">
							@include('admin.partials.check_seo', ['meta_keyword' => $post->meta_keyword])

							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Chọn danh mục</h3>
								</div>
								<!-- /.box-header -->

								<div class="box-body" style="height: 300px; overflow-y: auto;">

									@foreach($categories as $cate)
										<div class="form-group">
											<label>
												<input type="checkbox" name="parents[]" value="{{ $cate->category_id }}" class="flat-red"
													   @if(in_array($cate->category_id, $categoryPost)) checked @endif/>
												{{ $cate->title }}
											</label>
										</div>
										@foreach($cate['sub_children'] as $child)
											<div class="form-group">
												<label>
													<input type="checkbox" name="parents[]" value="{{ $child['category_id'] }}" class="flat-red"
														   @if(in_array($child['category_id'], $categoryPost)) checked @endif/>
													{{ $child['title'] }}
												</label>
											</div>
										@endforeach
									@endforeach

								</div>
							</div>

							<div class="box box-primary">
								<div class="box-body">
									<div class="form-group">
										<label>Ảnh dịch vụ(sản phẩm)</label>
										<input type="button" onclick="return uploadImage(this);" value="Chọn ảnh dịch vụ(sản phẩm)"
											   size="20"/>
										<img src="{{ $post->image }}" width="80" height="70"/>
										<input name="image" type="hidden" value="{{ $post->image }}"/>
									</div>

									<div class="form-group">
										<label>Danh sách hình ảnh</label>
										<input type="button" onclick="return openKCFinder(this);" value="Chọn ảnh"
											   size="20"/>
										<div class="imageList">
											@if(!empty($product->image_list))
												@foreach(explode(',',$product->image_list) as $image)
													<img src="{{$image}}" width="80" height="70" style="margin-left: 5px; margin-bottom: 5px;"/>
												@endforeach
											@endif
										</div>
										<input name="image_list" type="hidden" value="{{$product->image_list}}"/>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="profile">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="box box-primary">
								<div class="box-body">




									<!--<div class="form-group">
                                            <label for="exampleInputEmail1">Thuộc tính dịch vụ(sản phẩm)</label>
                                            <textarea class="editor" id="properties" name="properties" rows="10" cols="80">{{$product->properties}}</textarea>
                                        </div>-->
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-12">

							<div class="box box-primary ">
								<div class="box-body">
									<div class="form-group">
										<label>Chọn template</label>
										<select class="form-control" name="template">
											<option value="default">Mặc định</option>
											@foreach($templates as $template)
												<option value="{{ $template->slug }}"
														@if($template->slug == $post->template) selected @endif >{{ $template->title }}</option>
											@endforeach
										</select>
									</div>

									<?php $filterProduct = explode(',', $product->filter)?>
									<div class="form-group">
										<label>Bộ lọc:</label>
										<select class="select2 form-control " name="filter[]" multiple="multiple">
											@foreach($filter as $filterItem)
												<option value="{{ $filterItem->name_filter }}" {{ (in_array($filterItem->name_filter, $filterProduct) != false) ? 'selected' : '' }}>
													{{ $filterItem->name_filter }}
												</option>
											@endforeach
										</select>
									</div>

									@foreach ($typeInputs as $typeInput)
										<div class="form-group">
											<label>{{ $typeInput->title }}</label>
											@if($typeInput->type_input == 'one_line')
												<input type="text" class="form-control" name="{{$typeInput->slug}}" placeholder="{{ $typeInput->placeholder }}"
													   value="{{ $post[$typeInput->slug] }}" />
											@endif

											@if($typeInput->type_input == 'multi_line')
												<textarea rows="4" class="form-control" name="{{$typeInput->slug}}" placeholder="{{ $typeInput->placeholder }}">{{ $post[$typeInput->slug] }}</textarea>
											@endif

											@if($typeInput->type_input == 'image')
												<input type="button" onclick="return uploadImage(this);" value="Chọn ảnh"
													   size="20"/>
												<img src="{{ $post[$typeInput->slug] }}" width="80" height="70"/>
												<input name="{{$typeInput->slug}}" type="hidden" value="{{ $post[$typeInput->slug] }}"/>
											@endif

											@if($typeInput->type_input == 'image_list')
												<input type="button" onclick="return openKCFinder(this);" value="Chọn ảnh"
													   size="20"/>
												<div class="imageList">
													@if(!empty($post[$typeInput->slug]))
														@foreach(explode(',',$post[$typeInput->slug] ) as $image)
															<img src="{{ asset($image) }}" width="80" height="70" style="margin-left: 5px; margin-bottom: 5px;"/>
														@endforeach
													@endif
												</div>
												<input name="{{$typeInput->slug}}" type="hidden" value="{{$post[$typeInput->slug]}}"/>
											@endif

											@if($typeInput->type_input == 'editor')
												<textarea class="editor" id="{{$typeInput->slug}}" name="{{$typeInput->slug}}" rows="10" cols="80"/>{{ $post[$typeInput->slug] }}</textarea>
											@endif

											@if(!in_array($typeInput->type_input, array('one_line', 'multi_line', 'image', 'editor', 'image_list'), true) && strpos($typeInput->type_input, 'listMultil') >= 0)
												<select name="{{$typeInput->slug}}[]" class="select2 form-control" multiple="multiple">
													<?php $slugSubPost = str_replace('listMultil', '', $typeInput->type_input);?>
													@foreach(\App\Entity\SubPost::showSubPost($slugSubPost, 100) as $subPost)
														<option value="{{ $subPost->slug }}"
																@if(in_array($subPost->slug, explode(',', $post[$typeInput->slug])) > 0 ) selected @endif>
															{{ $subPost->title }}</option>
													@endforeach
												</select>
											@elseif (!in_array($typeInput->type_input, array('one_line', 'multi_line', 'image', 'editor', 'image_list'), true))
												<select name="{{$typeInput->slug}}" class="form-control">
													@foreach(\App\Entity\SubPost::showSubPost($typeInput->type_input, 100) as $subPost)
														<option value="{{ $subPost->title }}"
																@if($post[$typeInput->slug] == $subPost->title) selected @endif>
															{{ $subPost->title }}</option>
													@endforeach
												</select>
											@endif
										</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="messages">
					<div class="row">
						<div class="col-xs-12 col-md-8">
							<div class="box box-primary">
								<!--<div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tags (Viết tag cách nhau bởi dấu ,)</label>
                                            <input type="text" class="form-control" name="tags" value="{{ $post->tags }}" placeholder="Tags" >
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Thẻ title</label>
                                            <input type="text" class="form-control" name="meta_title" value="{{ $post->meta_title }}" placeholder="Thẻ title" >
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Thẻ description</label>
                                            <input type="text" class="form-control" name="meta_description" value="{{ $post->meta_description }}" placeholder="Thẻ description" >
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Thẻ keyword</label>
                                            <input type="text" class="form-control" name="meta_keyword" value="{{ $post->meta_keyword }}" placeholder="Thẻ keyword" >
                                        </div>
                                    </div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="box-footer">
			<button type="submit" class="btn btn-primary">Lưu thay đổi</button>
		</div>
	</form>
</section>
@endsection

