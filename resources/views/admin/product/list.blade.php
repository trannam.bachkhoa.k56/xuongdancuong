@extends('admin.layout.admin')

@section('title', 'Danh sách sản phẩm (dịch vụ)')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Sản phẩm (dịch vụ)
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Danh sách sản phẩm (dịch vụ)</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            @if (\App\Entity\SettingGetfly::checkSettingGetfly())
                <div class="box " id="getCateProductGetFly" style="display: none;">
                    <div class="box-header">
                        <h1>Lựa chọn danh mục tải về</h1>
                    </div>
                    <form action="{{ route('getProductGetfly') }}" method="post" >
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <label>Chọn danh mục lấy về</label>
                            <div class="formCateProduct" style="overflow: scroll; height: 300px; overflow-x: hidden;">
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" data-loading-text="Đang lấy dữ liệu">Lấy dữ liệu về</button>
                        </div>
                    </form>
                </div>
            @endif

            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <a href="{{ route('products.create') }}" class="floatLeft" >
                                <button class="btn btn-primary" id="stepProductGeneral3" data-step="1" data-intro="Ấn vào đây nếu bạn muốn thêm mới sản phẩm, ...">    Thêm mới
                                </button>
                            </a>

                            <form action="{{ route('product_delete_all') }}" method="post" class="floatLeft">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id_delete" id="idDeletes" value=""/>
                                <button type="submit" class="btn btn-danger">Xóa các mục đã chọn</button>
                            </form>
                        </div>
                    </div>
                    <p></p>
                    @if (!empty($_GET['messageErrorVip']))
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Lỗi:</span> {!! $_GET['messageErrorVip'] !!}
                        </div>
                    @endif

                    @if (\App\Entity\SettingGetfly::checkSettingGetfly())
                        <button class="btn btn-primary " onclick="return getCateProductGetfly(this);" data-loading-text="Cập nhật...">Đồng bộ sản phẩm từ getfly</button>
                    @endif
                    {{--<form action="{{ route('importProducts')}}" method="POST" enctype="multipart/form-data">--}}
                    {{--{!! csrf_field() !!}--}}
                    {{--{{ method_field('POST')}}--}}
                    {{--<div class="form-group">--}}
                    {{--<br>--}}
                    {{--<input type="file" name="file" required><br>--}}
                    {{--<button type="submit" class="btn btn-info">Nhập file Excel</button>--}}
                    {{--<a href="{{ route('exportProducts') }}" class="btn btn-success">Xuất file Excel</a>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <table id="products2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Sản phẩm</th>
                        </tr>
                        </thead>
                    </table>
                    <table id="products" class="table table-bordered table-striped hideMB">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>
                                <input type="checkbox" name="checkall" id="select_all" onClick="check_uncheck_checkbox(this.checked);" />
                            </th>
                            <th>Tiêu đề</th>
                            <th width="20%">Danh mục</th>
                            <th>Hình ảnh</th>
                            <th>Mã sản phẩm</th>
                            <th>Lượt xem</th>
                            <th>Giá</th>
                            <th>link</th>
                            <th>google</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@include('admin.partials.popup_delete')
@include('admin.partials.visiable')


@endsection
@push('scripts')
<script src="https://sp.zalo.me/plugins/sdk.js"></script>
<script>
    $(document).ready(function() {
        if (RegExp('multipage', 'gi').test(window.location.search)) {
            var intro = introJs();
            intro.setOptions({
                steps: [
                    {
                        element: document.querySelector('#stepProductGeneral1'),
                        intro: "Ấn vào đây để quản lý toàn bộ sản phẩm của bạn."
                    },
                    {
                        element: document.querySelector('#stepProductGeneral2'),
                        intro: "Ấn vào đây để quản lý toàn bộ danh mục(nhóm) sản phẩm của bạn."
                    },
                    {
                        element: document.querySelector('#stepProductGeneral3'),
                        intro: "Ấn vào đây nếu bạn muốn thêm mới sản phẩm của bạn."
                    }
                ]
            });

            intro.setOption('doneLabel', 'Thêm mới danh mục sản phẩm').start().oncomplete(function() {
                window.location.href = '/admin/category-products/create?multipage=true';
            });
        }
    });


    $(function() {
        $window = $(window);
        var windowsize = $window.width();
        if (windowsize > 765) {
            $('#products').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                type: "POST",
                ajax: '{!! route('datatable_product') !!}',
                columns: [
                    { data: 'product_id', name: 'products.product_id' },
                    { data: 'post_id', name: 'posts.post_id', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<input type="checkbox" name="chkdelete" class="chkDelete" value="'+data+'" onclick="return checkElement(this);"/>';
                        },
                        searchable: false
                    },
                    { data: 'title', name: 'posts.title' },
                    { data: 'category_string', name: 'category_string' },
                    { data: 'image', name: 'posts.image', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<div class=""><img src="'+data+'" width="50" /></div>';
                        },
                        searchable: false  },
                    { data: 'code', name: 'products.code' },
                    { data: 'views', name: 'posts.views' },
                    { data: 'price', name: 'products.price' },
                    { data: 'slug', name: 'slug', orderable: false,
                        render: function ( data, type, row, meta ) {
                            html = '<p><a href="/'+ data +'" target="_blank">Xem trang</a></p>';
                            html += `<iframe src="https://www.facebook.com/plugins/share_button.php?href={!! !empty($domainUser) ? (!empty($domainUser->domain) ? $domainUser->domain : $domainUser->url ) : 'https://moma.vn' !!}/${data}&layout=button_count&size=small&appId=1875296572729968&width=102&height=20" width="102" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>`;
                            html += `<div class="zalo-share-button" data-href="{!! !empty($domainUser) ? (!empty($domainUser->domain) ? $domainUser->domain : $domainUser->url ) : 'https://moma.vn' !!}/${data}" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false></div>`;

                            return html;
                        },
                        searchable: false  },
                    { data: 'title', name: 'title', render: function ( data, type, row, meta ) {
                            return `<a href="https://www.google.com/search?q=${data}" target="_blank">kiểm tra</a>`;
                        },
                    },   
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                ]
            });
        } else {
            $('#products2').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                type: "POST",
                ajax: '{!! route('datatable_product') !!}',
                columns: [
                    { data: 'product_id', name: 'products.product_id', render: function (data, type, row, meta) {
                        return `<div class="box box-warning box-solid mgBottom0">
                                <div class="box-header with-border">
                                  <h3 class="box-title">${row.title}</h3>
                                  <div class="box-tools pull-right">
                                        <input type="checkbox" name="chkdelete" class="chkDelete" value="'+ row.post_id +'" onclick="return checkElement(this);"/>
                                        ${row.action}
                                  </div>
                                    </div>
                                    <div class="box-body">
                                        <p><img src="${row.image}" width="100%"/></p>
                                         <p>Danh mục: ${row.category_string == null ? '0' : row.category_string}</p>
                                        <p>Giá: ${numeral(row.price).format('0,0')} đ</p>
                                        <p>Lượt xem: ${row.views == null ? '0' : row.views}</p>
                                    </div>
                            </div>`;
                    }
                    },
                ]
            });
        }
    });

    function check_uncheck_checkbox(isChecked) {
        if(isChecked) {
            $('.chkDelete').each(function() {
                this.checked = true;
            });
        } else {
            $('.chkDelete').each(function() {
                this.checked = false;
            });
        }
        getIdDelete();
    }

    function checkElement(e) {
        if($('.chkDelete:checked').length == $('.chkDelete').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
        getIdDelete();
    }

    function getIdDelete() {
        var idDeletes = '';
        $('.chkDelete:checked').each(function() {
            var idDelete = $(this).val();
            idDeletes += ',' + idDelete;
        });

        $('#idDeletes').val(idDeletes);
    }

    function getCateProductGetfly(e) {
        var btn = $(e).button('loading');
        $.ajax({
            type: "get",
            url: '{!! route('getCateProductGetfly') !!}',
            success: function(result){
                $('#getCateProductGetFly').show();
                var obj = jQuery.parseJSON( result);
                if (obj.httpCode == 500) {
                    alert('Đã có lỗi xảy ra vui lòng liên hệ với chúng tôi, để chúng tôi hỗ trợ.');
                    btn.button('reset');

                    return true;
                }
                if (obj.httpCode == 200) {
                    $(e).hide();
                    btn.button('reset');
                    var categories = obj.categories;
                    $.each(categories, function(index, element) {
                        var html = '<div class="form-group col-xs-12">';
                        html += '<label style="cursor: pointer;"><input type="checkbox"  name="cate_getfly[]" value="'+element.category_id+', '+element.category_name+', '+ element.parent_id + '" class="flat-red" /> '+ element.cate_name_show +'</label>';
                        html += '</div>';

                        $('#getCateProductGetFly').find('.formCateProduct').append(html);
                    });

                    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                        checkboxClass: "icheckbox_flat-green",
                        radioClass   : "iradio_flat-green"
                    })

                }
            }
        });
    }

</script>


@endpush
