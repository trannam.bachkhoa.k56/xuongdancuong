@extends('admin.layout.communicate')

@section('title', 'Quản lý vận chuyển')

@section('content')
	<section class="content dragCustomer">
        <div class="row">
            <div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
						<h1>Cập nhật chính sách mới về đối tác vận chuyển cho khách hàng Moma</h1>
					</div>
					<div class="box-body">
						<p>Để đáp ứng nhu cầu kịp thời của các chủ shop, chủ doanh nghiệp trong quá trình vận hành và quản lý đơn hàng, Sapo cập nhật chính sách mới với hai đối tác vận chuyển Viettel Post và Giao hàng tiết kiệm, giao hàng nhanh</p>
						<h3><i>Chú ý: Để có thể sử dụng được module này: Vui lòng cài đặt extension của chrome: <br><a href="https://chrome.google.com/webstore/detail/ignore-x-frame-headers/gleekbfjekiniecknbkamfmkohkpodhe" target="_blank">Link cài đặt</a></i></h3>
					</div>
					<!-- @if(\Illuminate\Support\Facades\Auth::user()->vip < 1)
					<div class="box-footer">
						<a  href="/admin/nang-cap-tai-khoan" class="btn btn-primary">Nâng cấp đối tác vận chuyển </a>
					</div>
					@endif -->
				</div>
			</div>
		</div>
	</section>	
		
	
	
@endsection

