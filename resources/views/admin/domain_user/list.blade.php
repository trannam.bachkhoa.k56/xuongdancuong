@extends('admin.layout.admin')

@section('title', 'Danh sách Thành viên ')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Thành viên website
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Thành viên</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-header">
                          <button class="btn" data-toggle="modal" data-target="#modalAddUser">
                                          Thêm mới thành viên <i class="fa fa-plus"></i>
                                      </button>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="domains" class="table table-bordered table-striped">
                            <thead>
                             <tr>
                                <th width="5%">ID</th>
                                <th>Tên </th>
                                <th>Email</th>
                                <th>Số điện thoại</th>
                                <th>Cấp độ nhân viên</th>
                               
                            </tr>
                            </thead>

                            <tbody>
	                            @foreach(App\Entity\DomainUser::getUser(Auth::user()->id) as $id => $user)                                   	
	                            	<tr>
	                            		<td width="5%">{{$id+1}}</td>
		                                <td>
		                                	<a style=" cursor: pointer;" data-toggle="modal" data-target="#modalEditRole{{$id}}">
                                               {{$user->name}}
                                            </a>
                                        </td>
		                                <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
		                                <td> 
		                                	{{ $user->position }}
                                         </td>
		                               
	                            	</tr>
	                             @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tên </th>
                                <th>Email</th>
                                <th>Số điện thoại</th>
                                <th>Cấp độ nhân viên</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            	 
                <!-- /.box -->
            </div>
        </div>
          <div class="modal fade" id="modalAddUser" tabindex="-1" role="dialog" aria-labelledby="modalAddUser" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title" id="exampleModalLabel">Thêm Thành Viên </h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <form method="POST" action="{{route('add_user')}}" >
				 {!! csrf_field() !!}
                <div class="form-group">
                    <label>
                        Email 
                    </label>
					<input type="email" class="form-control" value="" name="email_invite"/>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Cấp độ nhân viên</label>
                    <select class="form-control" name="commision_id">
                        @foreach ($commissions as $commission)
                        <option value="{{ $commission->commission_id }}">{{ $commission->position }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                        Thêm Thành viên 
                    </button>
                </div>

            </form>

          </div>
         
        </div>
      </div>
    </div>

     @foreach(App\Entity\DomainUser::getUser(Auth::user()->id) as $id => $user)
         <div class="modal fade" id="modalEditRole{{$id}}" tabindex="-1" role="dialog" aria-labelledby="modalEditRole{{$id}}" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa quyền thành viên</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <div class="form-group">
                        <p>
                            <label>
                                Tên Thành viên : 
                            </label>
                            {{$user->name}}
                        </p>
                        <p>
                            <label>
                                Email : 
                            </label>
                            {{$user->email}} - {{$user->role}}
                        </p>
                      
                    </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Cấp độ hoa hồng</label>
                      <select class="form-control" name="commision_id" onchange="return changeRole(this);">
                          @foreach ($commissions as $commission)
                              <option value="{{ $commission->commission_id }}"
                                      {{ $user->commission_id == $commission->commission_id ? 'selected' : ''  }}
                              >
                                  {{ $commission->position }}
                              </option>
                          @endforeach
                      </select>
                      <input type="hidden" class="domainUserId" value="{{ $user->domain_user_id }}" name="" />
                  </div>
              </div>
             
            </div>
          </div>
        </div>
    @endforeach

    <script type="text/javascript">
        function changeRole(e){
            var commissionId = $(e).val();
            console.log(commissionId);
            var id = $(e).parent().find('.domainUserId').val();

            $.ajax({
                url: '{{route("edit_user_add")}}',
                type: 'get',
                data: {
                    id: id,
                    commission_id: commissionId,
                },
            })
            .done(function() {
                console.log("success");
               alert ('Thay đổi trạng thái thành công ');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
            
        }
    </script>

    </section>
    @include('admin.partials.popup_delete')
@endsection


