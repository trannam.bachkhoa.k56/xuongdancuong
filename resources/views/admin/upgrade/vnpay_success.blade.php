@extends('admin.layout.admin')

@section('title', 'Thông tin thanh toán ví điện tử VN Pay')

@section('content')
    <section class="content-header">
        <h1>
            THANH TOÁN THÀNH CÔNG
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Mua thêm</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <div class="col-xs-12 col-md-12">
                        <div class="box-header with-border">
                            <h3 class="box-title">Hoàn tất thanh toán mua thêm thời gian sử dụng</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th> Mã đơn hàng </th>
                                        <th> Tổng số tiền </th>
                                        <th> Mã GD Tại VNPAY </th>
                                        <th> Mã Ngân hàng </th>
                                        <th> Kết quả </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> {{ $orderId }} </td>
                                        <td> {{ number_format($money, 0, ',', '.') }} VNĐ </td>
                                        <td> {{ $transactionId }} </td>
                                        <td> {{ $bankCode }} </td>
                                        <td> {{ $status }} </td>
                                    </tr>
                                </tbody>
                            </table>
                            <a href="{{route('admin_home')}}"><button type="button" class="btn btn-info bkg whiteText">Quay lại trang chủ</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection