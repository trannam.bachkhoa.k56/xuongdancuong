@extends('admin.layout.admin')

@section('title', 'Nâng cấp tài khoản')

@section('content')
    <section class="content-header">
        <h1>
            NÂNG CẤP GÓI WEBSITE
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Nâng cấp tài khoản</li>
        </ol>
    </section>
    <section class="content">
            <div class="box box-primary">
                <div class="row">
						<div class="col-xs-12 col-md-3 mgt10">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-blue" >
                                    <div class="widget-user-image absolute">
                                        <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <h4 class="center"><b>MOMA - 3 Tháng</b></h4>
                                    <p class="center"><i >(Tiết kiệm 0 đồng)</i></p>
                                    <h4 class="center"><b>599.000 VND</b></h4>
                                </div>
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
										<li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">3 tháng</span></a></li>
										<li><a href="#">MOMA PRO có trí tuệ nhân tạo mạnh mẽ.</a></li>
										<li><a href="#">Nhận view lên đến 130-150%, chạy không giới hạn cửa số và máy tính, tăng lượt view chất lượng ổn định.</a></li>
                                        <li><a href="#">5.000 khách hàng</a></li>
                                        <li><a href="#">Chứng chỉ bảo mật https miễn phí</a></li>
                                        <li><a href="#">Tích hợp crm</a></li>
										<li><a href="#">Tích hợp mạng xã hội</a></li>
										<li><a href="#">Tích hợp quảng cáo facebook theo đuổi khách hàng mọi nơi</a></li>
										<li><a href="#">50 Email automation Đột phá doanh số</a></li>
										<li><a href="#">Kiếm tiền online thu nhập thụ động với moma afiliate </a></li>
                                        <li>
                                            <a href="{{ route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 599000, 'type' => 1, 'month' => 6]) }}"><button type="button" style="width: 100%; margin-top: 10px;" class="btn btn-success">Chọn</button>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3 mgt10">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-yellow" >
                                    <div class="widget-user-image absolute">
                                        <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <h4 class="center"><b>MOMA - 1 NĂM</b></h4>
                                    <p class="center"><i>(Tiết kiệm 100.000 đồng)</i></p>
                                    <h4 class="center"><b>1.199.000 VND</b></h4>
                                </div>
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
										<li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
										<li><a href="#">MOMA PRO có trí tuệ nhân tạo mạnh mẽ.</a></li>
										<li><a href="#">Nhận view lên đến 130-150%, chạy không giới hạn cửa số và máy tính, tăng lượt view chất lượng ổn định.</a></li>
                                        <li><a href="#">5.000 khách hàng</a></li>
                                        <li><a href="#">Chứng chỉ bảo mật https miễn phí</a></li>
                                        <li><a href="#">Tích hợp crm</a></li>
										<li><a href="#">Tích hợp mạng xã hội</a></li>
										<li><a href="#">Tích hợp quảng cáo facebook theo đuổi khách hàng mọi nơi</a></li>
										<li><a href="#">50 Email automation Đột phá doanh số</a></li>
										<li><a href="#">Kiếm tiền online thu nhập thụ động với moma afiliate </a></li>
                                        <li>
                                            <a href="{{ route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 1199000, 'type' => 1, 'month' => 12]) }}"><button type="button" style="width: 100%; margin-top: 10px;" class="btn btn-success">Chọn</button>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3 mgt10">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-green">
                                    <div class="widget-user-image absolute">
                                        <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <h4 class="center"><b><br>MOMA PRO <br>2 NĂM TẶNG  1 NĂM</b></h4>
                                    <p class="center"><i >(Tiết kiệm 1.200.000 đồng)</i></p>
                                    <h4 class="center"><b>2.399.000 VNĐ</b></h4>
                                </div>
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">3 năm</span></a></li>
										<li><a href="#">MOMA PRO có trí tuệ nhân tạo mạnh mẽ.</a></li>
										<li><a href="#">Nhận view lên đến 130-150%, chạy không giới hạn cửa số và máy tính, tăng lượt view chất lượng ổn định.</a></li>
										<li><a href="#">5.000 khách hàng</a></li>
                                        <li><a href="#">Chứng chỉ bảo mật https miễn phí</a></li>
                                        <li><a href="#">Tích hợp crm</a></li>
										<li><a href="#">Tích hợp mạng xã hội</a></li>
										<li><a href="#">Tích hợp quảng cáo facebook theo đuổi khách hàng mọi nơi</a></li>
										<li><a href="#">50 Email automation Đột phá doanh số</a></li>
										<li><a href="#">Kiếm tiền online thu nhập thụ động với moma afiliate </a></li>
                                        <li>
                                            <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 2399000, 'type' => 1, 'month' => 36])}}"><button type="button" style="width: 100%; margin-top: 10px;" class="btn btn-success">Chọn</button>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3 mgt10">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-green">
                                    <div class="widget-user-image absolute">
                                        <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <h4 class="center"><b><br>MOMA PRO <br>3 NĂM TẶNG 2 NĂM</b></h4>
                                    <p class="center"><i >(Tiết kiệm 2.400.000 đồng)</i></p>
                                    <h4 class="center"><b>3.599.000 VNĐ</b></h4>
                                </div>
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">5 năm</span></a></li>
										<li><a href="#">MOMA PRO có trí tuệ nhân tạo mạnh mẽ.</a></li>
										<li><a href="#">Nhận view lên đến 130-150%, chạy không giới hạn cửa số và máy tính, tăng lượt view chất lượng ổn định.</a></li>
										<li><a href="#">5.000 khách hàng</a></li>
                                        <li><a href="#">Chứng chỉ bảo mật https miễn phí</a></li>
                                        <li><a href="#">Tích hợp crm</a></li>
										<li><a href="#">Tích hợp mạng xã hội</a></li>
										<li><a href="#">Tích hợp quảng cáo facebook theo đuổi khách hàng mọi nơi</a></li>
										<li><a href="#">50 Email automation Đột phá doanh số</a></li>
										<li><a href="#">Kiếm tiền online thu nhập thụ động với moma afiliate </a></li>
                                        <li>
                                            <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 3599000, 'type' => 1, 'month' => 36])}}"><button type="button" style="width: 100%; margin-top: 10px;" class="btn btn-success">Chọn</button>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
</section>
        <style>
            .center{text-align: center;}
            .absolute{position: absolute;}
            .mgt10{margin-top: 10px;}
            .table thead th{
                text-align:center;
            }
            .table thead th,td{
                border-left: 1px solid #fff
            }
            .bg-yellow{
                background: #ffc107;
            }
            .bg-footer-yellow{
                background:#ffc10714;
            }
            .bg-blue{
                background: #03a9f4;
            }
            .bg-footer-blue{
                background:#03a9f417;
            }

            h3.center{
                padding: 10px;
            }

            .bg-vip1{
                background: #fcaf42;
            }
            .vip2{
                background: #429bcf;
                color: #FFF;
            }
            .bg-vip2{
                background:#2189c7;
            }
            .box-footer2{
                background:#deedf7;
            }

            .vip{
                background: #dedddd;
                color: #333 !important;
            }
            .bg-vip{
                background : #a0a0a0 ;
            }
            .box-footer0{
                background:#d1d0d045;
            }

            .color-blue{
                color:#0086ff;
            }
            .color-red{
                color:red;
            }
            .center{
                color:white;
            }
        </style>

    @endsection

