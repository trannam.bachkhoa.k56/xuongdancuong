@extends('admin.layout.admin')

@section('title', 'Thanh toán qua ví điện tử VN Pay ')

@section('content')
    <section class="content-header">
        <h1>
            THANH TOÁN QUA VÍ ĐIỆN TỬ VN PAY
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Mua thêm</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="box box-primary">
                    <form action="{{route('create_payment')}}" method="POST" style="margin: 10px 20px">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="hidden">
                            <input type="text" name="order_type" value="billpayment">
                            <input type="text" name="order_id" value="<?= date("YmdHis") ?>"/>
                            <input type="text" name="amount" value="{{ $money }}">
                            <input type="text" name="order_desc" value="{{ $formality }}">
                            <input type="text" name="bank_code" id="bankCode">
                            <input type="text" name="language" value="vn">
                            <input type="text" name="gift" value="{{$gift}}">
                        </div>
                        <div class="col-md-12  white-box-padding">
                            <div class="center-screen">
                                <div class="device">Dịch vụ: {{ $device }}</div>
                                <div class="heading_row content-main">
                                    <p class="price-text">
                                        <span class="pull-left">Tổng số tiền cần thanh toán:</span>
                                        <span class="pull-right money-text">{{ number_format($money, 0, ',', '.') }} VNĐ</span>
                                    </p>
                                    <div class="type-trans-text">
                                        <strong>Chọn hình thức thanh toán</strong>
                                    </div>
                                    <ul class="select-bank-trans-text">
                                        <li>
											
											  <div class="well">
													<div class="domestic-bank domestic-bank-list">
                                               
                                                <div class="list-bank clearfix">
                                                    <ul id="firstListOfBank" class="bank-list-ul clearfix">
                                                        <li class="bank" data-value="VIETCOMBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/vietcombank_logo.png') }}"></li>
                                                        <li class="bank" data-value="BIDV" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/bidv_logo.png') }}"></li>
                                                        <li class="bank" data-value="AGRIBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/agribank_logo.png') }}"></li>
                                                        <li class="bank" data-value="SACOMBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/sacombank_logo.png') }}"></li>
                                                        <li class="bank" data-value="MBBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/mbb_logo.png') }}" ></li>
                                                        <li class="bank" data-value="TECHCOMBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/techcombank_logo.png') }}"></li>
                                                        <li class="bank" data-value="ACB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/acb_logo.png') }}"></li>
                                                        <li class="bank" data-value="VPBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/vpbank_logo.png') }}"></li>
                                                        <li class="bank" data-value="SHB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/shb_logo.png') }}"></li>
                                                        <li class="bank" data-value="DONGABANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/dongabank_logo.png') }}"></li>
                                                        <li class="bank" data-value="EXIMBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/eximbank_logo.png') }}"></li>
                                                        <li class="bank" data-value="TPBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/tpbank_logo.png') }}"></li>
                                                        <li class="bank" data-value="NCB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/ncb_logo.png') }}" ></li>
                                                        <li class="bank" data-value="OJB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/oceanbank_logo.png') }}"></li>
                                                        <li class="bank" data-value="MSBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/msbank_logo.png') }}"></li>
                                                        <li class="bank" data-value="HDBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/hdbank_logo.png') }}"></li>
                                                        <li class="bank" data-value="NAMABANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/namabank_logo.png') }}"></li>
                                                        <li class="bank" data-value="OCB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/ocb_logo.png') }}" ></li>
                                                        <li class="bank" data-value="SCB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/scb_logo.png') }}"></li>
                                                        <li class="bank" data-value="IVB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/ivb_logo.png') }}"></li>
                                                        <li class="bank" data-value="ABBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/abbank_logo.png') }}"></li>
                                                        <li class="bank" data-value="BACABANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/bacabank_logo.png') }}"></li>
                                                        <li class="bank" data-value="VIB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/vib_logo.png') }}" ></li>
                                                        <li class="bank" data-value="SEABANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/seabank_logo.png') }}"></li>
                                                        <li class="bank" data-value="SAIGONBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/saigonbank_logo.png') }}" ></li>
                                                        <li class="bank" data-value="PVCOMBANK" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/PVComBank_logo.png') }}" ></li>
                                                    </ul>
                                                </div>
                                            </div>
											  </div>
										
                                            
                                        </li>

                                        <!--<li>
											
											<div class="btn btn-default" type="button" data-toggle="collapse" data-target="#nganhangquocte" aria-expanded="true" aria-controls="nganhangquocte">
												<i class="fa fa-square-o" aria-hidden="true"></i> Thẻ ngân hàng quốc tế
											</div>
											<div class="collapse" id="nganhangquocte">
											  <div class="well">
													<div class="domestic-bank domestic-bank-list">
														<div>
															<i id="paymentTypeBank"></i>
															<span class="bank-text">Thẻ ngân hàng quốc tế</span>
														</div>
														<div class="list-bank clearfix">
															<ul id="firstListOfBank" class="bank-list-ul clearfix">
																<li class="bank" data-value="VISA" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/visa_logo.png') }}"></li>
																<li class="bank" data-value="MASTERCARD" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/mastercard_logo.png') }}"></li>
																<li class="bank" data-value="JCB" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/jcb.png') }}"></li>
																<li class="bank" data-value="UPI" onclick="selectBank(this)"><img src="{{ asset('adminstration/img/credit/upi.png') }}"></li>
															</ul>
														</div>
													</div>
											  </div>
											</div>
                                        </li>-->
                                    </ul>
                                    <p class="clearfix"></p>
                                    <div class="button-pay-ment">
                                        <div id="paymentOffset"></div>
                                        <button type="button" id="btnPayment" class="pay-ment">
                                            <strong>THANH TOÁN NGAY!</strong>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script>
        function selectBank(e) {
            $('.bank').removeClass('selectedItem');
            $(e).addClass('selectedItem');
            $('#btnPayment').addClass('success');
            $('#btnPayment').attr('type', 'submit');
            $('#bankCode').val($(e).attr('data-value'));
        }
    </script>
    <style>
        .content-main{
            margin-top:1px
        }

        .select-bank-trans-text{
            list-style:none;
            border:none;
            box-shadow:none;
            padding:0;
            margin:0
        }
        .select-bank-trans-text a{
            color:#013267;
            opacity:.7
        }

        .select-bank-trans-text li{
            box-sizing:border-box;
            min-height:48px;
            max-width:480px;
            margin:0 auto;
            position:relative;
            margin-bottom:10px
        }

        .select-bank-trans-text li .domestic-bank-list{
            border-radius:3px;
            line-height:48px;
            border:1px solid rgba(0,0,0,.1);
            min-height:48px;
            display:block;
            text-align:left;
            padding:0;
            box-sizing:border-box;
            background:none;
            position:relative;
            outline:none;
            vertical-align:middle;
            width:100%;
            overflow:hidden;
            cursor:pointer;
            clear:both
        }

        .select-bank-trans-text li .domestic-bank-list .bank-text{
            margin-left:45px;
            line-height: 48px;
        }

        .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul{
            list-style:none;
            padding:0;
            margin-left:40px;
            margin-right:10px
        }
        .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul li{
            display:block;
            border:1px solid rgba(0,0,0,.1);
            cursor:pointer;
            width:30%;
            float:left;
            position:relative;
            margin-right:5%;
            margin-bottom:10px;
            border-radius:4px;
            text-align:center;
            font-size:12px
        }

        .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul li img{
            height:auto;
            width:90%;
            display:inline-block;
            max-width:100px
        }

        .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul li:nth-child(3n){
            margin-right:0
        }
        .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul li.domestic-bank{
            border:1px solid #5a9e3f
        }

        .select-bank-trans-text li .domestic-bank-list.domestic-bank{
            box-shadow:0 1px 8px 0 rgba(0,0,0,.2)
        }

        .pay-ment{
            border:none;
            display:block;
            margin:auto;
            background:none;
            background-color:#ebebeb;
            text-align:center;
            padding:0;
            box-shadow:0 2px 2px #d1d1d1;
            width:100%;
            color:#666;
            outline:none;
            margin-bottom:10px
        }

        .pay-ment strong{
            display:block;
            padding:10px 0
        }

        .pay-ment.pay-ment-text{
            position:fixed;
            bottom:0;
            z-index:99
        }

        .selectedItem{
            border: 1px solid #5a9e3f !important;
        }

        .success{
            color: #fff;
            background-color: #5a9e3f;
        }

        .pay-ment.domestic-bank{
            color:#fff;
            background-color:#5a9e3f
        }
        .pay-ment.domestic-bank:hover{
            background-color:#75bd4f
        }
        .pay-ment.domestic-bank:focus{
            background-color:#3b8123
        }

        .price-text{
            height:45px;
            line-height:45px;
            clear:both;
            z-index:1;
            position:relative;
            border-top:1px solid rgba(0,0,0,.1);
            border-bottom:1px solid rgba(0,0,0,.1)
        }
        .price-text .money-text{
            color:#d0021b;
            font-weight:700
        }

        .type-trans-text{
            position:relative;
            height:35px;
            margin:5px auto;
            line-height:35px
        }

        .button-pay-ment{
            text-align:center
        }

        .center-screen{
            max-width:480px;
            width:100%;
            margin:auto
        }
        .center-screen .device{
            height:30px;
            line-height:30px;
            margin-top:10px;
            font-weight:700
        }

        @media only screen and (max-width:767px){
            .select-bank-trans-text{
                width:100%;
                margin:auto;
                font-size:12px
            }

            ._2BBqywP5BSPwzHVtOt5TCw li .domestic-bank-list,.select-bank-trans-text li .domestic-bank-list{
                line-height:40px;
                min-height:40px
            }

            .content-main{
                margin-top:0
            }

            .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul li.domestic-bank:after{
                width:15px;
                height:15px;
                top:-7px;
                right:-7px
            }

            .button-pay-ment .pay-ment{
                font-size:12px
            }

            .price-text{
                padding:0 10px;
                margin:10px -10px;
                font-size:12px
            }

            .pay-ment{
                width:100%;
                max-width:480px;
                margin-bottom:0
            }

            .pay-ment.pay-ment-text{
                left:0
            }

            .select-bank-trans-text{
                font-size:12px
            }

            .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul li img{
                width:100%
            }

            .select-bank-trans-text li .domestic-bank-list .list-bank ul.bank-list-ul li{
                width:32%;
                min-height:40px;
                height:40px;
                line-height:40px;
                margin-right:2%
            }
        }

        @media only screen and (min-width:768px){
            .pay-ment{
                width:480px;
                margin-left:calc(50% - 240px)
            }

            .pay-ment.pay-ment-text{
                margin-left:0;
            }
        }

        @media only screen and (min-width:320px) and (max-width:480px){
            .select-bank-trans-text{
                width:100%;
                margin:auto
            }
        }

    </style>
@endsection
