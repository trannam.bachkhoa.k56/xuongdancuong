@extends('admin.layout.admin')

@section('title', 'Nâng cấp tài khoản')

@section('content')
	@if(\Illuminate\Support\Facades\Auth::user()->vip < 1)
	<section class="content dragCustomer">
        <div class="row">
            <div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
						<h1>Nguồn cơ hội kinh doanh liên tục</h1>
					</div>
					<div class="box-body">
						<p>Hầu hết mọi người khi tham gia kinh doanh, thu thập được rất là nhiều thông tin khách hàng nhưng rồi không biết phải làm gì với nó?</p>
						<p>Trên thương mại điện tử, khi bạn nhận được một lần đăng ký tư vấn từ khách hàng, đây có nghĩa là một cơ hội kinh doanh đang chờ để bạn thực hiện. Đây không còn là một cuộc điện thoại lạ vì đang có người chờ điện thoại của bạn.</p>
						<p>Mỗi một lần sử dụng một kênh marketing, sẽ có một vài khách hàng tiềm năng đăng ký tư vấn sản phẩm, dịch vụ của bạn.</p>
						<p>Trong khi rất nhiều doanh nhân không biết khách hàng đến từ kênh marketing nào? Và làm thế nào để có những lời giới thiệu từ những khách hàng mới.</p>
					</div>
					<div class="box-footer">
						<a  href="/admin/nang-cap-tai-khoan" class="btn btn-primary">Nâng cấp quản lý khách hàng</a>
					</div>
				</div>
			</div>
		</div>
	</section>	
	@endif
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            NÂNG CẤP GÓI WEBSITEs
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Nâng cấp tài khoản</li>
        </ol>
    </section>
  
		
        <div class="row">
            <!-- form start -->
                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
							<div class="col-xs-12 col-md-3 mgt10">
                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header center" style="background-color: #7395a9b3 !important; color:#fff">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI WEBSITE 1</b></h3>
                                        <p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
                                        <h4 class="center"><b>300.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 tháng</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Không</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-website1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
							 <div class="col-xs-12 col-md-4 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header" style="background-color: #7bbadeab !important; color:#fff">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI WEBSITE 2</b></h3>
                                        <p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
                                        <h4 class="center"><b>806.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">3 tháng</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Không</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-website2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-header -->
                            <div class="col-xs-12 col-md-4 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-blue" >
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 1</b></h3>
                                        <p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
                                        <h4 class="center"><b>2.400.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Không</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-yellow">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 2</b></h3>
                                        <p class="center"><i>(Mua 2 năm tặng 1 năm)</i></p>
                                        <h4 class="center"><b>4.800.000 VNĐ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">3 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
											<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-green">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 3</b></h3>
                                        <p class="center"><i>(Mua 3 năm tặng 2 năm)</i></p>
                                        <h4 class="center"><b>7.200.000 VNĐ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">5 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
											<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip4" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-4 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <div class="widget-user-header bg-red">
                                        <div class="widget-user-image absolute">
                                            <i style="font-size: 40px" class="fa fa-diamond" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 4</b></h3>
                                        <p class="center"><i>(Dành cho doanh nghiệp cỡ lớn)</i></p>
                                        <h4 class="center"><b>Liên hệ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
											<li><button style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#"><b>Liên hệ: 097 456 1735 </b></button></li>
                                     <!--       <li><button data-toggle="modal" data-target="#modal-vip3" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>   -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-header -->
                    </div>
                    <!-- /.box -->
                    <!-- Nội dung thêm mới -->
        </div>
		
		 <div class="modal fade" id="modal-website1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói website 1</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="1" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="300000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="0" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
		 <div class="modal fade" id="modal-website2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói website 1</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="3" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="806000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="0" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
		
		
        <div class="modal fade" id="modal-vip1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 1</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2400000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-vip2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 2</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="36" value="36" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="4800000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-vip4">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 3</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="60" value="60" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="7200000" name="money" />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-vip3">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Nâng cấp gói VIP 4</h4>
                    </div>
                    <form action="{{ route('submit_upgrade') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã giới thiệu (Nếu có): </label>
                                <input type="text" class="form-control" name="gift_code"  placeholder="Moma-***" value="">
                            </div>
                            <input type="hidden" value="6000000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
	
	<section class="content-header">
        <h1>
            GIA HẠN THỜI GIAN SỬ DỤNG GÓI EMAIL MARKETING
        </h1>
    </section>
	<section class="content">
			 
					<div class="row">
						<!-- form start -->
						<div class="col-xs-12 col-md-12 mgt10">

							<div class="box box-widget widget-user-2">
								<!-- Add the bg color to the header using any of the bg-* classes -->
								<div class="widget-user-header bg-blue">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png">
									</div>
									<!-- /.widget-user-image -->
									<h3 class="center"><b>GÓI DOANH NGHIỆP VỪA VÀ NHỎ </b></h3>
									<h4 class="center"><b>5.000 Email trong 1 tháng</b></h4>
								</div>
								
								<div class="box-footer no-padding bg-footer-blue">
									<table class="table" style="overflow-x: scroll;">
										  <thead class="vip2">
											<tr>
											  <th scope="col">Gói Email 1</th>
											  <th scope="col">Gói Email 2</th>
											  <th scope="col">Gói Email 3</th>
											</tr>
										  </thead>
										  <tbody>
											<tr>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
											 </tr>
											<tr>
											  <td>Giá tiền : 300.000 vnđ</td>
											  <td>Giá tiền : 806.000 vnđ</td>
											  <td>Giá tiền : 2.400.000 vnđ</td>
											</tr>
											<tr>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											</tr>
											<tr>
											  <td><button data-toggle="modal" data-target="#modal-email1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email3" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											</tr>
										  </tbody>
										</table>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-md-12 mgt10">
							<div class="box box-widget widget-user-2">
								<!-- Add the bg color to the header using any of the bg-* classes -->
								<div class="widget-user-header bg-yellow">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png">
									</div>
									<!-- /.widget-user-image -->
									<h3 class="center"><b>GÓI DOANH NGHIỆP LỚN</b></h3>
									<h4 class="center"><b>20.000 Email trong 1 tháng </b></h4>
								</div>
								<div class="box-footer bg-footer-yellow no-padding">
									<table class="table" style="overflow-x: scroll;">
										  <thead class="bg-vip1" style="color:#fff">
											<tr>
											  <th scope="col">Gói Email 1</th>
											  <th scope="col">Gói Email 2</th>
											  <th scope="col">Gói Email 3</th>
											</tr>
										  </thead>
										  <tbody>
											<tr>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
											 </tr>
											<tr>
											  <td>Giá tiền : 800.000 vnđ</td>
											  <td>Giá tiền : 2.000.000 vnđ</td>
											  <td>Giá tiền : 6.000.000 vnđ</td>
											</tr>
											<tr>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											</tr>
											<tr>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											</tr>
											<tr>
											  <td><button data-toggle="modal" data-target="#modal-email4" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email5" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											  <td><button data-toggle="modal" data-target="#modal-email6" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
											</tr>
										  </tbody>
										</table>
								</div>
							</div>
						</div>
					</div>
				</section>
				<style>
				.table thead th{
					text-align:center;
				}
				.table thead th,td{
						border-left: 1px solid #fff
				}
				.bg-yellow{
					background: #ffc107;
				}
				.bg-footer-yellow{
					background:#ffc10714;
				}
				.bg-blue{
					background: #03a9f4;
				}
				.bg-footer-blue{
					background:#03a9f417;
				}
				
				h3.center{
					padding: 10px;	
				}
				
				.bg-vip1{
								background: #fcaf42;
				}
				.vip2{
					background: #429bcf;
					color: #FFF;
				}
				.bg-vip2{
					background:#2189c7;
				}
				.box-footer2{
					background:#deedf7;
				}
				
				.vip{
					background: #dedddd;
					color: #333 !important;
				}
				.bg-vip{
					background : #a0a0a0 ;
				}
				.box-footer0{
					background:#d1d0d045;
				}
			
				.color-blue{
					color:#0086ff;
				}
				.color-red{
					color:red;
				}
				.center{
					color:white;
				}
			
			</style>
			
	   

        <div class="modal fade" id="modal-email1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="1" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="300000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="3" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="806000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email3">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2400000" name="money" />
                            <input type="hidden" value="1" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email4">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="1" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="800000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email5">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="3" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2000000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email6">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thời gian sử dụng gói Email Marketing</h4>
                    </div>
                    <form action="{{ route('submit_upgrade_email') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="month" min="1" placeholder="12" value="12" required>
                            </div>
							<input type="hidden" class="form-control" name="name" min="1" placeholder="12" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}" required>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán: </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="6000000" name="money" />
                            <input type="hidden" value="2" name="vip" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

       
    </section>
	
	
	
<style>
	.center{text-align: center;}
	.absolute{position: absolute;}
	.mgt10{margin-top: 10px;}
</style>

@endsection

