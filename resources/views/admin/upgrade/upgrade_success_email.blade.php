@extends('admin.layout.admin')

@section('title', 'Nâng cấp tài khoản thành công')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Hoàn tất thanh toán mua email marketing của moma
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Hoàn tất thanh toán mua email marketing của moma</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <div class="col-xs-12 col-md-12">
                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="col-xs-12 col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Hoàn tất thanh toán mua thêm thời gian sử dụng</h3>
                            </div>
                            <div class="box-body">
                                <p>Để đơn hàng được khởi tạo, quý khách vui lòng thực hiện việc chuyển khoản tổng tiền là <span style="color: orange; font-weight: bold;">{{ number_format($totalPrice) }}</span> vào một trong những tài khoản ngân hàng hiển thị ngay bên dưới.</p>
                                <p>Trong nội dung chuyển khoản quý khách vui lòng điền mã đơn hàng là <span style="color: orange; font-weight: bold;">VN3C-EMAILNC{{ $vip }}{{ $month }}-{{ $orderId }}</span></p>

                                <p>Nếu quý khách không thể ghi mã đơn hàng vào nội dung chuyển khoản, quy khách vui lòng chụp hình ủy nhiệm chi và gửi vào mail: vn3ctran@gmail.com với tựa đề: Ủy nhiệm đơn hàng <span style="color: orange; font-weight: bold;">VN3C-EMAILNC{{ $vip }}{{ $month }}-{{ $orderId }}</span></p>
                                <div class="col-xs-12 col-md-6">
                                    <table  class="table table-bordered table-striped">
                                        <tr>
                                            <td width="20%">
                                                <img src="https://i.dlpng.com/static/png/517303_thumb.png" title="BIDV" width="150"/>
                                            </td>
                                            <td>
                                                <h4>Ngân hàng BIDV - Chi nhánh Thanh Xuân - Hà Nội</h4>
                                                <p>Chủ tài khoản: Trần Hải Nam</p>
                                                <p>Số tài khoản: 2221.0003.8490.14</p>
                                                <p>Nội dung chuyển khoản: <span style="color: orange; font-weight: bold;">VN3C-EMAILNC{{ $vip }}{{ $month }}-{{ $orderId }}</span></p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>


                    <!-- /.box-header -->
                </div>
                <!-- /.box -->
                <!-- Nội dung thêm mới -->
            </div>
        </div>
    </section>
@endsection

