@extends('admin.layout.admin')

@section('title', 'mua thêm ')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            GIA HẠN THỜI GIAN SỬ DỤNG WEBSITE
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Gia hạn thời gian</li>
        </ol>
    </section>
    
  
    <section class="content">
        <div class="row">
            <!-- form start -->
            <div class="col-xs-12 col-md-12">
                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <!-- /.box-header -->
                      <div class="box box-primary">
							<div class="col-xs-12 col-md-3 mgt10">
                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header" style="background-color: #7395a9b3 !important; color:#fff; text-align: center">
                                         <div class="widget-user-image" style="position: absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI WEBSITE 1</b></h3>
                                        <p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
                                        <h4 class="center"><b>300.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 tháng</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Không</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-website1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
							 <div class="col-xs-12 col-md-3 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header" style="background-color: #7bbadeab !important; color:#fff; text-align: center">
                                         <div class="widget-user-image" style="position: absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI WEBSITE 2</b></h3>
                                        <p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
                                        <h4 class="center"><b>806.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">3 tháng</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Không</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-website2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-header -->
                            <div class="col-xs-12 col-md-3 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-blue" style="text-align: center" >
                                        <div class="widget-user-image" style="position: absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 1</b></h3>
                                        <p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
                                        <h4 class="center"><b>2.400.000 VND</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Không</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-3 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-yellow" style="text-align: center">
                                        <div class="widget-user-image absolute" style="position: absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 2</b></h3>
                                        <p class="center"><i>(Mua 2 năm tặng 1 năm)</i></p>
                                        <h4 class="center"><b>4.800.000 VNĐ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">3 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
											<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-green" style="text-align: center">
                                        <div class="widget-user-image absolute" style="position: absolute">
                                            <i style="font-size: 40px" class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 3</b></h3>
                                        <p class="center"><i>(Mua 3 năm tặng 2 năm)</i></p>
                                        <h4 class="center"><b>7.200.000 VNĐ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">5 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
											<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
                                            <li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
                                            <li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">Không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
                                            <li><button data-toggle="modal" data-target="#modal-vip3" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6 mgt10">

                                <div class="box box-widget widget-user-2">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class="widget-user-header bg-red" style="text-align: center">
                                        <div class="widget-user-image absolute" style="position: absolute">
                                            <i style="font-size: 40px" class="fa fa-diamond" aria-hidden="true"></i>
                                        </div>
                                        <!-- /.widget-user-image -->
                                        <h3 class="center"><b>GÓI VIP 4</b></h3>
                                        <p class="center"><i>(Dành cho doanh nghiệp cỡ lớn)</i></p>
                                        <h4 class="center"><b>Liên hệ</b></h4>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="#">Thời gian sử dụng: <span class="pull-right badge bg-red">1 năm</span></a></li>
                                            <li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
                                            <li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
                                            <li><a href="#">Website 1 ngôn ngữ</a></li>
                                            <li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
                                            <li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
                                            <li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
                                            <li><a href="#">Số lượng sản phẩm đăng tải: <span class="pull-right badge bg-red">không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng tin tức đăng tải: <span class="pull-right badge bg-aqua">Không giới hạn</span></a></li>
                                            <li><a href="#">Số lượng khách hàng: <span class="pull-right badge bg-green">không giới hạn</span></a></li>
                                            <li><a href="#">Thay đổi tên miền: <span class="pull-right badge bg-green">Miễn phí</span></a></li>
											<li><button style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#"><b>Liên hệ: 097 456 1735 </b></button></li>
                                     <!--       <li><button data-toggle="modal" data-target="#modal-vip3" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></li>   -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-header -->
                    </div>
                    <!-- /.box -->
                    <!-- Nội dung thêm mới -->
                </div>
                <!-- /.box -->
                <!-- Nội dung thêm mới -->
            </div>
        </div>

        <div class="modal fade" id="modal-website1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng website 1 tháng</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 300000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="300000" name="money" />
                            <input type="hidden" value="1" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 300000, 'type' => 1, 'month' => 1])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-website2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng website  3 tháng</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 806000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="806000" name="money" />
                            <input type="hidden" value="3" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 806000, 'type' => 1, 'month' => 3])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-vip1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng website 1 năm</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 2400000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2400000" name="money" />
                            <input type="hidden" value="12" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 2400000, 'type' => 1, 'month' => 12])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
		 <div class="modal fade" id="modal-vip2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng website 2 năm</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 4800000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="4800000" name="money" />
                            <input type="hidden" value="24" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 4800000, 'type' => 1, 'month' => 24])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
		 <div class="modal fade" id="modal-vip3">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng website 3 năm</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 7200000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="7200000" name="money" />
                            <input type="hidden" value="60" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 7200000, 'type' => 1, 'month' => 36])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </section>
	
	<section class="content-header">
        <h1>
            GIA HẠN THỜI GIAN SỬ DỤNG GÓI EMAIL MARKETING
        </h1>
    </section>
	<section class="content">
			 
					<div class="row">
						<!-- form start -->
						<div class="col-xs-12 col-md-12 mgt10">

							<div class="box box-widget widget-user-2">
								<!-- Add the bg color to the header using any of the bg-* classes -->
								<div class="widget-user-header bg-blue">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png">
									</div>
									<!-- /.widget-user-image -->
									<h3 class="center" style="text-align: center"><b>GÓI DOANH NGHIỆP VỪA VÀ NHỎ </b></h3>
									<h4 class="center" style="text-align: center"><b>5.000 Email trong 1 tháng</b></h4>
								</div>
								
								<div class="box-footer no-padding bg-footer-blue">
									<table class="table" style="overflow-x: scroll;">
                                      <thead class="vip2">
                                        <tr>
                                          <th scope="col">Gói Email 1</th>
                                          <th scope="col">Gói Email 2</th>
                                          <th scope="col">Gói Email 3</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
                                          <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
                                          <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
                                         </tr>
                                        <tr>
                                          <td>Giá tiền : 300.000 vnđ</td>
                                          <td>Giá tiền : 806.000 vnđ</td>
                                          <td>Giá tiền : 2.400.000 vnđ</td>
                                        </tr>
                                        <tr>
                                          <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
                                          <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
                                          <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
                                        </tr>
                                        <tr>
                                          <td><button data-toggle="modal" data-target="#modal-email1" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
                                          <td><button data-toggle="modal" data-target="#modal-email2" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
                                          <td><button data-toggle="modal" data-target="#modal-email3" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
                                        </tr>
                                      </tbody>
                                    </table>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-md-12 mgt10">
							<div class="box box-widget widget-user-2">
								<!-- Add the bg color to the header using any of the bg-* classes -->
								<div class="widget-user-header bg-yellow">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png">
									</div>
									<!-- /.widget-user-image -->
									<h3 class="center" style="text-align: center"><b>GÓI DOANH NGHIỆP LỚN</b></h3>
									<h4 class="center" style="text-align: center"><b>20.000 Email trong 1 tháng </b></h4>
								</div>
								<div class="box-footer bg-footer-yellow no-padding">
									<table class="table" style="overflow-x: scroll;">
                                      <thead class="bg-vip1" style="color:#fff">
                                        <tr>
                                          <th scope="col">Gói Email 1</th>
                                          <th scope="col">Gói Email 2</th>
                                          <th scope="col">Gói Email 3</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
                                          <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
                                          <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
                                         </tr>
                                        <tr>
                                          <td>Giá tiền : 800.000 vnđ</td>
                                          <td>Giá tiền : 2.000.000 vnđ</td>
                                          <td>Giá tiền : 6.000.000 vnđ</td>
                                        </tr>
                                        <tr>
                                          <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
                                          <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
                                          <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
                                        </tr>
                                        <tr>
                                          <td>Gửi mail tới khách hàng</td>
                                          <td>Gửi mail tới khách hàng</td>
                                          <td>Gửi mail tới khách hàng</td>
                                        </tr>
                                        <tr>
                                          <td><button data-toggle="modal" data-target="#modal-email4" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
                                          <td><button data-toggle="modal" data-target="#modal-email5" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
                                          <td><button data-toggle="modal" data-target="#modal-email6" style="width: 100%; margin-top: 10px;" class="btn btn-success" href="#">Chọn </button></td>
                                        </tr>
                                      </tbody>
                                    </table>
								</div>
							</div>
						</div>
					</div>
				</section>
				<style>
				.table thead th{
					text-align:center;
				}
				.table thead th,td{
						border-left: 1px solid #fff
				}
				.bg-yellow{
					background: #ffc107;
				}
				.bg-footer-yellow{
					background:#ffc10714;
				}
				.bg-blue{
					background: #03a9f4;
				}
				.bg-footer-blue{
					background:#03a9f417;
				}
				
				h3.center{
					padding: 10px;	
				}
				
				.bg-vip1{
								background: #fcaf42;
				}
				.vip2{
					background: #429bcf;
					color: #FFF;
				}
				.bg-vip2{
					background:#2189c7;
				}
				.box-footer2{
					background:#deedf7;
				}
				
				.vip{
					background: #dedddd;
					color: #333 !important;
				}
				.bg-vip{
					background : #a0a0a0 ;
				}
				.box-footer0{
					background:#d1d0d045;
				}
			
				.color-blue{
					color:#0086ff;
				}
				.color-red{
					color:red;
				}
				.center{
					color:white;
				}
			
			</style>

        <div class="modal fade" id="modal-email1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng gói Email Marketing 1 tháng</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 300000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="300000" name="money" />
                            <input type="hidden" value="1" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 300000, 'month' => 1])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
        </div>
		</div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email2">
          <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng gói Email Marketing 3 tháng</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 806000])}}">--}}
{{--                            <button type="button" class="btn btn-success mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="806000" name="money" />
                            <input type="hidden" value="3" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 806000, 'month' => 3])}}">--}}
{{--                        <button type="button" class="btn btn-success mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
            <!-- /.modal-dialog -->
        </div>
		</div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email3">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng gói Email Marketing 1 năm</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 2400000])}}">--}}
{{--                            <button type="button" class="btn btn-success mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2400000" name="money" />
                            <input type="hidden" value="12" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 2400000, 'month' => 12])}}">--}}
{{--                        <button type="button" class="btn btn-success mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
            <!-- /.modal-dialog -->
        </div>
		</div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email4">
             <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng gói Email Marketing 1 tháng</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 800000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="800000" name="money" />
                            <input type="hidden" value="1" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 800000, 'month' => 1])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
            <!-- /.modal-dialog -->
        </div>
		</div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email5">
             <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng gói Email Marketing 3 tháng</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 2000000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="2000000" name="money" />
                            <input type="hidden" value="3" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 2000000, 'month' => 3])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
            <!-- /.modal-dialog -->
        </div>
		</div>
        <!-- /.modal -->
		<div class="modal fade" id="modal-email6">
           <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Gia hạn thêm thời gian sử dụng gói Email Marketing 1 năm</h4>
                    </div>
{{--                    <div class="modal-body">--}}
{{--                        <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 6000000])}}">--}}
{{--                            <button type="button" class="btn btn-info mgTop3Left35">Thanh toán</button>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                    <form action="{{ route('submit_buy_more') }}" method="post">
                        {!! csrf_field() !!}
                        {{ method_field('POST') }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại xác nhận thanh toán </label>
                                <input type="number" class="form-control" name="phone"  placeholder="097xxx" value="" required>
                            </div>
                            <input type="hidden" value="6000000" name="money" />
                            <input type="hidden" value="12" name="month" />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
                            <button type="submit" class="btn btn-primary">Thanh toán</button>
                        </div>
                    </form>
{{--                    <hr class="mgTop0"/>--}}
{{--                    <p class="textFloat">Hoặc</p>--}}
{{--                    <a href="{{route('thong-tin-thanh-toan-vi-dien-tu', ['money' => 6000000, 'month' => 12])}}">--}}
{{--                        <button type="button" class="btn btn-info mgTop3Left35">Thanh toán bằng VNPay</button>--}}
{{--                    </a>--}}
                </div>
            <!-- /.modal-dialog -->
        </div>
		</div>
        <!-- /.modal -->
		
		<style>
			.modal-dialog .textFloat  {
				text-align:center;
			}
			.modal-dialog a  {
				text-align:center;
				display: block;
				
				margin-bottom: 10px;
			}
		</style>
	   
@endsection

