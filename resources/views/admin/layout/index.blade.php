<!DOCTYPE html>
<html>
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>@yield('title')</title>
		<meta name="description" content="Công ty cổ phần công nghệ MOMA">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{asset('MomaManager/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('MomaManager/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="{{asset('MomaManager/img/logoMM2.png')}}" />
		
		<!-- jQuery 3 -->
		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{asset('MomaManager/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('MomaManager/js/scripts.bundle.js')}}" type="text/javascript"></script>

		<!--end::Page Scripts -->
		
		<!-- jQuery UI 1.11.4 -->
		<script src="{{ asset('adminstration/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
			
			<script src="{{ asset('js/jquery-sortable-lists.js') }}"></script>
		<!-- Select2 -->
		<script src="{{ asset('adminstration/select2/dist/js/select2.full.min.js') }}"></script>
		<!-- Morris.js charts -->
		<!-- <script src="{{ asset('adminstration/raphael/raphael.min.js') }}"></script>
		<script src="{{ asset('adminstration/morris.js/morris.min.js') }}"></script>-->

		<!-- Sparkline -->
		<script src="{{ asset('adminstration/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
		<!-- jvectormap -->
		<script src="{{ asset('adminstration/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
		<script src="{{ asset('adminstration/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
		<!-- jQuery Knob Chart -->
		<script src="{{ asset('adminstration/jquery-knob/dist/jquery.knob.min.js') }}"></script>
		<!-- daterangepicker -->
		<script src="{{ asset('adminstration/moment/min/moment.min.js') }}"></script>
		<script src="{{ asset('adminstration/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
		<!-- datepicker -->
		<script src="{{ asset('adminstration/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
		<!-- Bootstrap WYSIHTML5 -->
		<script src="{{ asset('adminstration/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
		<!-- Slimscroll -->
		<script src="{{ asset('adminstration/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
		<!-- FastClick -->
		<script src="{{ asset('adminstration/fastclick/lib/fastclick.js') }}"></script>
		<!-- iCheck 1.0.1 -->
		<!-- <script src="{{ asset('adminstration/plugins/iCheck/icheck.min.js') }}"></script> -->
		<!-- FastClick -->
		<script src="{{ asset('adminstration/fastclick/lib/fastclick.js') }}"></script>
		<script src="{{ asset('adminstration/time/time.min.js') }}"></script>
		<!-- InputMask -->
		<!-- <script src="{{ asset('adminstration/plugins/input-mask/jquery.inputmask.js') }}"></script>
		<script src="{{ asset('adminstration/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
		<script src="{{ asset('adminstration/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script> -->
		<!-- CK Editor -->
		<script src="{{ asset('adminstration/ckeditor/ckeditor.js') }}"></script>
		<!-- AdminLTE App -->
		<script src="{{ asset('adminstration/js/adminlte.min.js') }}"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="{{ asset('adminstration/js/demo.js') }}"></script>
		<script src="{{ asset('adminstration/jquery.priceformat.js') }}"></script>
		<script src="{{ asset('adminstration/emojionearea/emojionearea.js') }}"></script>

		<script src="{{ asset('adminstration/js/intro.js') }}"></script>
		<!-- <script src="{{ asset('adminstration/dashboard/dashboard.js') }}"></script> -->
		<script src="{{ asset('adminstration/js/Highchart/highcharts.js') }}"></script>
		<script src="{{ asset('adminstration/js/Highchart/highcharts-3d.js') }}"></script>
		<script src="{{ asset('adminstration/js/Highchart/modules/cylinder.js') }}"></script>
		<script src="{{ asset('adminstration/js/Highchart/modules/funnel3d.js') }}"></script>
		<script src="{{ asset('adminstration/js/Highchart/modules/exporting.js') }}"></script>
		<script src="{{ asset('adminstration/js/Highchart/modules/export-data.js') }}"></script>
		<script src="{{ asset('adminstration/js/Highchart/modules/accessibility.js') }}"></script>
		<script src="{{ asset('adminstration/js/Chart.js') }}"></script>
		</body>

		<!-- end::Body -->
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="mm-quick-panel--right mm-panel--right mm-offcanvas-panel--right mm-header--fixed mm-header-mobile--fixed mm-subheader--fixed mm-subheader--enabled mm-subheader--solid mm-aside--enabled mm-aside--fixed mm-page--loading">

		<!-- begin:: Page -->

		@include('admin.common.header')
		<!-- end:: Header Mobile -->
		<div class="mm-grid mm-grid--hor mm-grid--root">
			<div class="mm-grid__item mm-grid__item--fluid mm-grid mm-grid--ver mm-page">

				<!-- begin:: Aside -->
				<button class="mm-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
				<div class="mm-aside  mm-aside--fixed  mm-grid__item mm-grid mm-grid--desktop mm-grid--hor-desktop" id="kt_aside">

					<!-- begin:: Aside -->
					<div class="mm-aside__brand mm-grid__item  " id="kt_aside_brand">
						<div class="mm-aside__brand-logo">
							<a href="/">
								<img alt="Logo" width="50" src="{{asset('MomaManager/img/logoMM2.png')}}" />
							</a>
						</div>
					</div>

					<!-- end:: Aside -->

					@include('admin.common.sidebar')
				</div>

				<!-- end:: Aside -->
				<div class="mm-grid__item mm-grid__item--fluid mm-grid mm-grid--hor mm-wrapper" id="kt_wrapper">

					@include('admin.common.nav')
					<!-- <div class="mm-container  mm-container--fluid  mm-grid__item mm-grid__item--fluid">
						<div class="row">
							<div class="col">
								<div class="alert alert-light alert-elevate fade show" role="alert">
									<div class="alert-icon"><i class="flaticon-warning mm-font-brand"></i></div>
									<div class="alert-text">
										Hiển thị thông báo
									</div>
								</div>
							</div>
						</div>
					</div> -->
					@yield('content')
					
					@include('admin.common.nav')
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Quick Panel -->
		<div id="kt_quick_panel" class="mm-quick-panel">
			<a href="#" class="mm-quick-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></a>
			<div class="mm-quick-panel__nav">
				<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  mm-notification-item-padding-x" role="tablist">
					<li class="nav-item active">
						<a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_logs" role="tab">Audit Logs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
					</li>
				</ul>
			</div>
			<div class="mm-quick-panel__content">
				<div class="tab-content">
					<div class="tab-pane fade show mm-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">
						<div class="mm-notification">
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-line-chart mm-font-success"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New order has been received
									</div>
									<div class="mm-notification__item-time">
										2 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-box-1 mm-font-brand"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New customer is registered
									</div>
									<div class="mm-notification__item-time">
										3 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-chart2 mm-font-danger"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										Application has been approved
									</div>
									<div class="mm-notification__item-time">
										3 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-image-file mm-font-warning"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New file has been uploaded
									</div>
									<div class="mm-notification__item-time">
										5 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-drop mm-font-info"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New user feedback received
									</div>
									<div class="mm-notification__item-time">
										8 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-pie-chart-2 mm-font-success"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										System reboot has been successfully completed
									</div>
									<div class="mm-notification__item-time">
										12 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-favourite mm-font-danger"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New order has been placed
									</div>
									<div class="mm-notification__item-time">
										15 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item mm-notification__item--read">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-safe mm-font-primary"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										Company meeting canceled
									</div>
									<div class="mm-notification__item-time">
										19 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-psd mm-font-success"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New report has been received
									</div>
									<div class="mm-notification__item-time">
										23 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon-download-1 mm-font-danger"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										Finance report has been generated
									</div>
									<div class="mm-notification__item-time">
										25 hrs ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon-security mm-font-warning"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New customer comment recieved
									</div>
									<div class="mm-notification__item-time">
										2 days ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-pie-chart mm-font-warning"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										New customer is registered
									</div>
									<div class="mm-notification__item-time">
										3 days ago
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="tab-pane fade mm-scroll" id="kt_quick_panel_tab_logs" role="tabpanel">
						<div class="mm-notification-v2">
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon-bell mm-font-brand"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										5 new user generated report
									</div>
									<div class="mm-notification-v2__item-desc">
										Reports based on sales
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon2-box mm-font-danger"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										2 new items submited
									</div>
									<div class="mm-notification-v2__item-desc">
										by Grog John
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon-psd mm-font-brand"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										79 PSD files generated
									</div>
									<div class="mm-notification-v2__item-desc">
										Reports based on sales
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon2-supermarket mm-font-warning"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										$2900 worth producucts sold
									</div>
									<div class="mm-notification-v2__item-desc">
										Total 234 items
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon-paper-plane-1 mm-font-success"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										4.5h-avarage response time
									</div>
									<div class="mm-notification-v2__item-desc">
										Fostest is Barry
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon2-information mm-font-danger"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										Database server is down
									</div>
									<div class="mm-notification-v2__item-desc">
										10 mins ago
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon2-mail-1 mm-font-brand"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										System report has been generated
									</div>
									<div class="mm-notification-v2__item-desc">
										Fostest is Barry
									</div>
								</div>
							</a>
							<a href="#" class="mm-notification-v2__item">
								<div class="mm-notification-v2__item-icon">
									<i class="flaticon2-hangouts-logo mm-font-warning"></i>
								</div>
								<div class="mm-notification-v2__itek-wrapper">
									<div class="mm-notification-v2__item-title">
										4.5h-avarage response time
									</div>
									<div class="mm-notification-v2__item-desc">
										Fostest is Barry
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="tab-pane mm-quick-panel__content-padding-x fade mm-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">
						<form class="mm-form">
							<div class="mm-heading mm-heading--sm mm-heading--space-sm">Customer Care</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Notifications:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--success mm-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_1">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Case Tracking:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--success mm-switch--sm">
										<label>
											<input type="checkbox" name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Support Portal:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--success mm-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="mm-separator mm-separator--space-md mm-separator--border-dashed"></div>
							<div class="mm-heading mm-heading--sm mm-heading--space-sm">Reports</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Generate Reports:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--sm mm-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Report Export:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--sm mm-switch--danger">
										<label>
											<input type="checkbox" name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Allow Data Collection:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--sm mm-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_4">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="mm-separator mm-separator--space-md mm-separator--border-dashed"></div>
							<div class="mm-heading mm-heading--sm mm-heading--space-sm">Memebers</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable Member singup:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--sm mm-switch--brand">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Allow User Feedbacks:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--sm mm-switch--brand">
										<label>
											<input type="checkbox" name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Enable Customer Portal:</label>
								<div class="col-4 mm-align-right">
									<span class="mm-switch mm-switch--sm mm-switch--brand">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_6">
											<span></span>
										</label>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- end::Quick Panel -->

		<!-- begin::Sticky Toolbar -->
		<!-- <ul class="mm-sticky-toolbar" style="margin-top: 30px;">
			<li class="mm-sticky-toolbar__item mm-sticky-toolbar__item--success" id="kt_demo_panel_toggle" data-toggle="mm-tooltip" title="Check out more demos" data-placement="right">
				<a href="#" class=""><i class="flaticon2-drop"></i></a>
			</li>
			<li class="mm-sticky-toolbar__item mm-sticky-toolbar__item--brand" data-toggle="mm-tooltip" title="Layout Builder" data-placement="left">
				<a href="https://keenthemes.com/metronic/preview/demo3/builder.html" target="_blank"><i class="flaticon2-gear"></i></a>
			</li>
			<li class="mm-sticky-toolbar__item mm-sticky-toolbar__item--warning" data-toggle="mm-tooltip" title="Documentation" data-placement="left">
				<a href="https://keenthemes.com/metronic/?page=docs" target="_blank"><i class="flaticon2-telegram-logo"></i></a>
			</li>
			<li class="mm-sticky-toolbar__item mm-sticky-toolbar__item--danger" id="kt_sticky_toolbar_chat_toggler" data-toggle="mm-tooltip" title="Chat Example" data-placement="left">
				<a href="#" data-toggle="modal" data-target="#kt_chat_modal"><i class="flaticon2-chat-1"></i></a>
			</li>
		</ul>-->

		<!-- end::Sticky Toolbar -->

<script type="text/javascript">
        function changeRole(e){
            
            var role = $(e).val();
            var id = $(e).parent().parents().find('input.id').val();

            $.ajax({
                url: '{{route("edit_user_add")}}',
                type: 'POST',     
                data: {
                    id: id,
                    role: role,
                },
            })
            .done(function() {
                console.log("success");
                alert ('thành công ');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
            
        }
    </script>
	@stack('scripts')

<script>
    $(function () {
        jQuery("#contentFacebook").emojioneArea({
            pickerPosition: "left",
            tonesStyle: "bullet"
        });


        $('.select2').select2({
           width: '100%'
        });



        $('.editor').each(function(e){
            CKEDITOR.replace( this.id, {
                filebrowserImageBrowseUrl : '/kcfinder-master/browse.php?type=images&dir=images/public',
            });
        });

        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });
        
    });

    function uploadImage(e) {
        window.KCFinder = {
           callBack: function(url) {window.KCFinder = null;
                var img = new Image();
                img.src = url;
                $(e).next().attr("src",url);
                $(e).next().next().val(url);
            }
        };
        window.open('/kcfinder-master/browse.php?type=images&dir=images/public',
            'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
            'directories=0, resizable=1, scrollbars=0, width=800, height=600'
        );
    }
    function openKCFinder(e) {
        window.KCFinder = {
            callBackMultiple: function(files) {
                window.KCFinder = null;
                var urlFiles = "";
                $(e).next().empty();
                for (var i = 0; i < files.length; i++){
                    $(e).next().append('<img src="'+ files[i] +'" width="80" height="70" style="margin-left: 5px; margin-bottom: 5px;"/>')
                    urlFiles += files[i] ;
                    if (i < (files.length - 1)) {
                        urlFiles += ',';
                    }
                }

                $(e).next().next().val(urlFiles);
            }
        };
        window.open('/kcfinder-master/browse.php?type=images&dir=images/public',
            'kcfinder_multiple', 'status=0, toolbar=0, location=0, menubar=0, ' +
            'directories=0, resizable=1, scrollbars=0, width=800, height=600'
        );
    }
</script>
@if (!empty($domainUser))
	<?php
	$datetime1 = new DateTime();
	$datetime2 = new DateTime($domainUser->end_at);
	$interval = $datetime1->diff($datetime2);
	?>
	@if ($interval->format('%a') <= 30)
	<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution="setup_tool"
  page_id="1507394329398088">
</div>
	 @endif
@endif
<script>
    jQuery(function($) {
        $(document).ready(function() {
            $('#popupVitural').hide();

            setInterval(function(){
                $('#popupVitural').show().slideDown();
            }, 300000);

            $('#popupVitural .Closed').click(function(){
                $('#popupVitural').slideUp().hide();
                $('#popupVitural').addClass('hide');
            });
        });
    });
</script>


<script>
 $(document).ready(function () {
     setInterval(function() {
         $.ajax({
             url: '{{route("update_time")}}',
             type: 'GET',
             success: function(response)
             {
                 console.log('thanh cong');
             }
         });
     }, 1000 * 10 * 1);
</script>
@include ('general.contact', [
	  'phone' => '0982840758',
	  'zalo' => '<div class="zalo-chat-widget" data-oaid="3198175513277492032" data-welcome-message="Moma giúp anh chị có khách hàng tự động" data-autopopup="2" data-width="350" data-height="420"></div>

<script src="https://sp.zalo.me/plugins/sdk.js"></script>',
	  'fanpage' => 'webmienphimoma'
  ])


		<!--ENd:: Chat-->
</html>
