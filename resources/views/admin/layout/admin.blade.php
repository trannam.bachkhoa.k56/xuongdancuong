<!DOCTYPE html>
<html 	xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('adminstration/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('adminstration/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminstration/font-awesome/css/font-awesome.min.css')}}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('adminstration/plugins/iCheck/all.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminstration/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('adminstration/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminstration/css/AdminLTE.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('adminstration/emojionearea/emojionearea.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('adminstration/css/skins/_all-skins.min.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('adminstration/jvectormap/jquery-jvectormap.css') }}">
    <!-- jquery ui -->
    <link rel="stylesheet" href="{{ asset('adminstration/jquery-ui-1.12.1.custom/jquery-ui.min.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('adminstration/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('adminstration/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('adminstration/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('adminstration/select2/dist/css/select2.min.css') }}">

    <link rel="stylesheet" href="{{asset('adminstration/time/time.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
      folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('adminstration/css/skins/_all-skins.min.css') }}">
	<link rel="stylesheet" href="{{ asset('adminstration/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('adminstration/js/Highchart/css/highcharts.css') }}">
    <link rel="stylesheet" href="{{ asset('adminstration/css/Chart.css') }}">
	<link rel="stylesheet" href="{{ asset('adminstration/css/introjs.css') }}">

    <link rel="stylesheet" href="{{ asset('adminstration/morris.js/morris.css') }}">
	<base href="http://{!! \App\Ultility\Ultility::getCurrentHttpHost() !!}">
	
    <!--[if lt IE 9]>
       <script src="{{ asset('js/html5shiv.js') }}"></script>
       <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->
    <!-- jQuery 3 -->
    <script src="{{ asset('adminstration/jquery/dist/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('adminstration/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="{{ asset('js/jquery-sortable-lists.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('adminstration/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminstration/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('adminstration/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminstration/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('adminstration/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('adminstration/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('adminstration/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('adminstration/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('adminstration/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminstration/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('adminstration/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('adminstration/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('adminstration/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminstration/fastclick/lib/fastclick.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminstration/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('adminstration/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('adminstration/time/time.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('adminstration/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminstration/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminstration/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <!-- CK Editor -->
    <script src="{{ asset('adminstration/ckeditor/ckeditor.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('adminstration/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('adminstration/js/demo.js') }}"></script>
	<script src="{{ asset('adminstration/jquery.priceformat.js') }}"></script>
	<script src="{{ asset('adminstration/emojionearea/emojionearea.js') }}"></script>

    <script src="{{ asset('adminstration/js/intro.js') }}"></script>
    <!-- <script src="{{ asset('adminstration/dashboard/dashboard.js') }}"></script> -->
    <script src="{{ asset('adminstration/js/Highchart/highcharts.js') }}"></script>
    <script src="{{ asset('adminstration/js/Highchart/highcharts-3d.js') }}"></script>
    <script src="{{ asset('adminstration/js/Highchart/modules/cylinder.js') }}"></script>
    <script src="{{ asset('adminstration/js/Highchart/modules/funnel3d.js') }}"></script>
    <script src="{{ asset('adminstration/js/Highchart/modules/exporting.js') }}"></script>
    <script src="{{ asset('adminstration/js/Highchart/modules/export-data.js') }}"></script>
    <script src="{{ asset('adminstration/js/Highchart/modules/accessibility.js') }}"></script>
    <script src="{{ asset('vattucaycanhnew/javascript/numeral.min.js')}} " type="text/javascript"></script>
    <!-- Morris.js charts -->
    <script src="{{ asset('adminstration/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('adminstration/morris.js/morris.js') }}"></script>
    <script src="{{ asset('adminstration/js/Chart.js') }}"></script>
    <script src="{{ asset('adminstration/chart.js/Chart.js') }}"></script>
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="layout-boxed skin-yellow-light sidebar-mini">

<div class="wrapper">
    @if ($menuTop != 'detail_customer')
        @include('admin.partials.nav')
    @endif


    @if ($menuTop == 'products')
        @include('admin.partials.sidebar_product')
    @endif

    @if ($menuTop == 'orders')
        @include('admin.partials.sidebar_order')
    @endif

    @if ($menuTop == 'customers')
        @include('admin.partials.sidebar_customer')
    @endif

    @if ($menuTop == 'websites')
        @include('admin.partials.sidebar_website')
    @endif
	
	@if ($menuTop == 'communicate' || $menuTop == 'show_campains')
        @include('admin.partials.slidebar_communicate')
    @endif
	
	@if ($menuTop == 'utilities')
        @include('admin.partials.sidebar_utilities')
    @endif
	
	@if ($menuTop == 'cskh')
        @include('admin.partials.siderbar_cskh')
    @endif

    @if ($menuTop == 'setting')
        @include('admin.partials.sidebar_setting')
    @endif

    @if ($menuTop == 'statistic')
        @include('admin.partials.sidebar_statistic')
    @endif

    @if ($menuTop == 'facebookcommunicate')
        @include('admin.partials.sidebar_facebook')
    @endif

    <div class="content-wrapper" @if ($menuTop == 'detail_customer')
        style="margin-left: 0px;"
    @endif>
        <div style="color: red; text-align: center"> {!! \App\Ultility\Error::getErrorMessage() !!}</div>
		@yield('content')

        @include ('admin.partials.popup_task')
    </div>
    @if ($menuTop != 'detail_customer')
    <footer class="main-footer hideMB">
		@if (\Illuminate\Support\Facades\Auth::user()->vip < 1)
			<p class="" style="background: #5ca4f1; text-align: center; color: white; padding: 5px px; font-size: 18px;">
				
				<a href="/admin/nang-cap-tai-khoan" class="btn btn-danger">Nâng Cấp Ngay MOMA</a> Tự động lên top 1 google miễn phí + tặng kèm phần mềm bán hàng quản lý tiền, hàng trọn đời.
			</p>
		@else
			<div class="pull-right hidden-xs">
				<b>Version</b> 1.0
			</div>
			<strong>Copyright &copy; <?= date('Y')?> <a href="https://moma.vn">Moma giải pháp công nghệ</a>.</strong>

		@endif
    </footer>
    @endif
<div class="hideMB ">
    <?php $information['zalo_group'] = "https://zalo.me/g/zlatlr584"; ?>
    @include ('general.contact', [
	  'phone' => '0988940068',
	  'zalo' => '',
	  'fanpage' => ''
  ])
</div>

</div>    <!--/.main-->


  
@stack('scripts')

<script>
	function uploadImage(e) {
        window.KCFinder = {
           callBack: function(url) {window.KCFinder = null;
                var img = new Image();
                img.src = url;
                $(e).next().attr("src",url);
                $(e).next().next().val(url);
            }
        };
        window.open('/kcfinder-master/browse.php?type=images&dir=images/public',
            'kcfinder_image', 'status=0, toolbar=0, location=0, menubar=0, ' +
            'directories=0, resizable=1, scrollbars=0, width=800, height=600'
        );
    }
    function openKCFinder(e) {
        window.KCFinder = {
            callBackMultiple: function(files) {
                window.KCFinder = null;
                var urlFiles = "";
                $(e).next().empty();
                for (var i = 0; i < files.length; i++){
                    $(e).next().append('<img src="'+ files[i] +'" width="80" height="70" style="margin-left: 5px; margin-bottom: 5px;"/>');
                    urlFiles += files[i] ;
                    if (i < (files.length - 1)) {
                        urlFiles += ',';
                    }
                }

                $(e).next().next().val(urlFiles);
            }
        };
        window.open('/kcfinder-master/browse.php?type=images&dir=images/public',
            'kcfinder_multiple', 'status=0, toolbar=0, location=0, menubar=0, ' +
            'directories=0, resizable=1, scrollbars=0, width=800, height=600'
        );
    }
	
	function contact(e) {
    var $btn = $(e).find('button');
  	$btn.attr('disabled', 'disabled');
  	var data = $(e).serialize();
    
    $.ajax({
            type: "POST",
            url: '{{route("sub_contact_marketing")}}',
            data: data,
            success: function(result){
                var obj = jQuery.parseJSON( result);
                /* gửi thành công */
                if (obj.status == 200) {
                      alert(obj.message);
                      $btn.removeAttr('disabled');

                    if(obj.redirect != null){
                        location.href = obj.redirect;
                    }

                    return;
                }

                /* gửi thất bại */
                if (obj.status == 500) {
                      alert(obj.message);
                          $btn.removeAttr('disabled');
                            return;
                        }
              },
            error: function(error) {
            }

        });

    return false;
  }
  

    $(function () {
        jQuery("#contentFacebook").emojioneArea({
            pickerPosition: "left",
            tonesStyle: "bullet"
        });
        $('#user').DataTable();

        $('.select2').select2({
           width: '100%'
        });

        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        });

        $('.editor').each(function(e){
            CKEDITOR.replace( this.id, {
                filebrowserImageBrowseUrl : '/kcfinder-master/browse.php?type=images&dir=images/public',
            });
        });

        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'MM/DD/YYYY h:mm A'
            }
        });

    });
	
</script>
@if (!empty($domainUser))
	<?php
	$datetime1 = new DateTime();
	$datetime2 = new DateTime($domainUser->end_at);
	$interval = $datetime1->diff($datetime2);
	?>
	@if ($interval->format('%a') <= 30)
	<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution="setup_tool"
  page_id="1507394329398088">
</div>
	 @endif
@endif

<style>
    .Notification {
        /*background: #3c8dbc !important; */
        color: black;
    }
    .Notification h3 {
        color: black;
        text-align:center;
        text-tranform: uppercase;
    }
    .Notification label {
        color: white;
    }
    .Notification input[type=submit] {
        background: white;
    }
    #popupVitural {
        position: fixed;
        width: 300px;
        bottom: 20px;
        right: 10px;
        display: none;
        box-shadow: 0 2px 3px #d0d0d0;
        border: 1px solid #d0d0d0;
        background: white;
        padding: 10px;
        z-index: 99999;
    }
    #popupVitural .Closed {
        position: absolute;
        right: -10px;
        background: #092b6e;
        color: white;
        border-radius: 100%;
        top: -11px;
        padding: 2px 6px;
    }
    @media (max-width: 769px)
    {
        #popupVitural {
            display: none;
            width: 80%;
        }
    }
</style>
<script>
    jQuery(function($) {
        $(document).ready(function() {
            $('#popupVitural').hide();
        });
    });
	
</script>
<div class="Notification" id="popupVitural">
    <div class="Closed">X</div>
    <div class="Content">
        @if (\Illuminate\Support\Facades\Auth::user()->vip == 0)
            <h3 >Miễn phí</h3>
        @endif
        @if (\Illuminate\Support\Facades\Auth::user()->vip == 1)
            <h3>Doanh nghiệp</h3>
        @endif
        @if (\Illuminate\Support\Facades\Auth::user()->vip == 2)
            <h3>Thương mại điện tử</h3>
        @endif
        {!! $accountMessage !!}
        <p ><button  style="width: 100%;" data-toggle="modal" data-target="#helpPerson" class="btn btn-success">Yêu cầu với với chúng tôi</button><p>
    </div>

</div>

<div class="modal fade" id="helpPerson">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">YÊU CẦU</h4>
            </div>
            <div class="modal-body">
                <div id="getfly-optin-form-iframe-1563540050794"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var regex = /(https?:\/\/.*?)\//g; var furl = regex.exec(r); r = furl ? furl[0] : r; var f = document.createElement("iframe"); const url_string = new URLSearchParams(window.location.search); var utm_source, utm_campaign, utm_medium, utm_content, utm_term; if((!url_string.has('utm_source') || url_string.get('utm_source') == '') && document.cookie.match(new RegExp('utm_source' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_source' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_source') != null ? "&utm_source=" + url_string.get('utm_source') : "";} if((!url_string.has('utm_campaign') || url_string.get('utm_campaign') == '') && document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_campaign' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_campaign') != null ? "&utm_campaign=" + url_string.get('utm_campaign') : "";} if((!url_string.has('utm_medium') || url_string.get('utm_medium') == '') && document.cookie.match(new RegExp('utm_medium' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_medium' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_medium') != null ? "&utm_medium=" + url_string.get('utm_medium') : "";} if((!url_string.has('utm_content') || url_string.get('utm_content') == '') && document.cookie.match(new RegExp('utm_content' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_content' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_content') != null ? "&utm_content=" + url_string.get('utm_content') : "";} if((!url_string.has('utm_term') || url_string.get('utm_term') == '') && document.cookie.match(new RegExp('utm_term' + '=([^;]+)')) != null){ r+= "&" +document.cookie.match(new RegExp('utm_term' + '=([^;]+)'))[0]; } else { r+= url_string.get('utm_term') != null ? "&utm_term=" + url_string.get('utm_term') : "";} f.setAttribute("src", "https://sachvidan.getflycrm.com/api/forms/viewform/?key=ImKntOp5nWQJWtJdlljSoHamAZnyy8nPlKTnimTcYnHKYEu3Pj&referrer="+r); f.style.width = "100%";f.style.height = "500px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1563540050794");s.appendChild(f); })(); </script>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
    <script>
     /*$(document).ready(function () {
         setInterval(function() {
             $.ajax({
                 url: '{{route("update_time")}}',
                 type: 'GET',
                 success: function(response)
                 {
                     console.log('thanh cong');
                 }
             });
         }, 1000 * 10 * 1);
     }); */
    </script>


</body>
</html>
