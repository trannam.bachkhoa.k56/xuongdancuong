@extends('admin.layout.admin')

@section('title', 'Danh sách yêu cầu rút tiền')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Danh sách yêu cầu rút tiền
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách yêu cầu rút tiền</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">STT</th>
                                <th>User_id</th>
                                <th>Tên chuyển khoản</th>
                                <th>Tên ngân hàng </th>
                                <th>Số tài khoản</th>
                                <th>Số tiền</th>
                                <th>Thao tác </th>
                            </tr>
                            </thead>

                            <tbody>
                                @foreach($requestContacts as $id => $request )
                                 <tr>
                                    <td width="5%">{{$id+1}}</td>
                                    <td>{{$request->user_id}}</td>
                                    <td><a href="/admin/show-gift?user_id={{ $request->user_id }}">{{ $request->payment_name }}</a></td>
                                    <td>{{$request->bank_branch}} </td>
                                    <td>{{$request->bank_number}}</td>
                                    <td>{{number_format($request->money_request)}} VNĐ</td>
                                    <td> 
                                        <button href="{{route('confirm_request_money',['id' => $request->request_id, 'money' => $request->money_request]) }}" onclick="return submit(this)" class="btn btn-primary btn-submit">Xác nhận</button> 

                                        <button href="{{route('destroy_request_money',['id' => $request->request_id ])}}" class="btn btn-danger submitDelete" onclick="return remove(this);">
                                            <i class="fa fa-trash"></i>
                                        </button> 
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <th width="5%">STT</th>
                                <th>User_id</th>
                                <th>Tên chuyển khoản</th>
                                <th>Tên ngân hàng </th>
                                <th>Số tài khoản</th>
                                <th>Số tiền</th>
                                <th>Thao tác </th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>

    <script type="text/javascript">
            function submit(e){
                if(confirm('Bạn có chắc chắn muốn xác nhận không') == true){                    
                    var urls = $(e).attr('href');
                    console.log(urls);
                    window.location.href = urls ;
                    return false;
                }
            }
            function remove(e){
                if(confirm('Bạn có chắc chắn muốn Xóa không') == true){                    
                    var url = $(e).attr('href');
                    console.log(url);
                    window.location.href = url ;
                    return false;
                }
            }
    </script> 
    @include('admin.partials.popup_delete')
@endsection

