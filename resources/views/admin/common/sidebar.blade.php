<!--begin:: Aside Menu -->
	<div class="mm-aside-menu-wrapper mm-grid__item mm-grid__item--fluid" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="mm-aside-menu  mm-aside-menu--dropdown " data-ktmenu-vertical="1" data-ktmenu-dropdown="1" data-ktmenu-scroll="0">
			<ul class="mm-menu__nav ">
				<li class="mm-menu__item mm-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('sendEmail') }}" class="mm-menu__link"><i class="mm-menu__link-icon flaticon2-mail-1"></i><span class="mm-menu__link-text">Email</span></a>
					<div class="mm-menu__submenu "><span class="mm-menu__arrow"></span>
						<ul class="mm-menu__subnav">
							<li class="mm-menu__item "><a href="{{ route('sendEmail') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--line"><span></span></i><span class="mm-menu__link-text">Gửi mail</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('settingSendEmail') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--line"><span></span></i><span class="mm-menu__link-text">Đặt lịch gửi mail</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('settingCustomer') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--line"><span></span></i><span class="mm-menu__link-text">Mẫu Email</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('manager-email') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--line"><span></span></i><span class="mm-menu__link-text">Thống kê Email</span></a></li>
						</ul>
					</div>
				</li>
				<li class="mm-menu__item mm-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('SettingCampaign') }}" class="mm-menu__link "><i class="mm-menu__link-icon flaticon2-analytics-2"></i><span class="mm-menu__link-text">Chiến dịch</span></a>
					<div class="mm-menu__submenu "><span class="mm-menu__arrow"></span>
						<ul class="mm-menu__subnav">
							<li class="mm-menu__item "><a href="{{route('campaign-customer.index')}}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--line"><span></span></i><span class="mm-menu__link-text">Chiến dịch khách hàng</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('SettingCampaign') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--line"><span></span></i><span class="mm-menu__link-text">Cấu hình chiến dịch</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('optin-form.index') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--line"><span></span></i><span class="mm-menu__link-text">Quản lý form</span></a></li>
						</ul>
					</div>
				</li>
				<li class="mm-menu__item mm-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('show_getfly') }}" class="mm-menu__link "><i class="mm-menu__link-icon flaticon2-graph"></i><span class="mm-menu__link-text">CRM</span></a></li>
				<li class="mm-menu__item  mm-menu__item--submenu"  aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('information.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon2-drop"></i><span class="mm-menu__link-text">Cấu hình</span></a>
					<div class="mm-menu__submenu "><span class="mm-menu__arrow"></span>
						<ul class="mm-menu__subnav">
							<li class="mm-menu__item "><a href="{{ route('information.index') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Nội dung</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('method_payment') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Thanh toán</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('config_template') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Giao diện</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('config_template') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Form lấy thông tin</span></a></li>
						</ul>
					</div>
				</li>			
				<li class="mm-menu__item  mm-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('users.index') }}" class="mm-menu__link "><i class="mm-menu__link-icon flaticon2-gear"></i><span class="mm-menu__link-text">Quản lý</span></a>
					@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
					<div class="mm-menu__submenu "><span class="mm-menu__arrow"></span>
						<ul class="mm-menu__subnav">
							<li class="mm-menu__item "><a href="{{ route('show_add_user') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Quản lý nhân viên</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('commission.index') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Cơ chế hoa hồng</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('goals.index') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">KPI Cho nhân viên</span></a></li>
						</ul>
					</div>
					@endif
				</li>

				<li class="mm-menu__item  mm-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('advertisement.index') }}" class="mm-menu__link "><i class="mm-menu__link-icon flaticon2-gear"></i><span class="mm-menu__link-text">Quảng cáo</span></a>
					@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
					<div class="mm-menu__submenu "><span class="mm-menu__arrow"></span>
						<ul class="mm-menu__subnav">
							<li class="mm-menu__item "><a href="{{ route('advertisement.index') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Quản lý quảng cáo</span></a>
							</li>
							<!-- <li class="mm-menu__item "><a href="{{ route('commission.index') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">Cơ chế hoa hồng</span></a></li>
							<li class="mm-menu__item "><a href="{{ route('goals.index') }}" class="mm-menu__link "><i class="mm-menu__link-bullet mm-menu__link-bullet--dot"><span></span></i><span class="mm-menu__link-text">KPI Cho nhân viên</span></a></li> -->
						</ul>
					</div>
					@endif
				</li>
			</ul>
		</div>
	</div>
	<!-- end:: Aside Menu -->