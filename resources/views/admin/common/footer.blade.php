<!-- begin:: Footer -->
<div class="mm-footer  mm-grid__item mm-grid mm-grid--desktop mm-grid--ver-desktop" id="kt_footer">
	<div class="mm-container  mm-container--fluid ">
		<div class="mm-footer__copyright">
			2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="mm-link">Keenthemes</a>
		</div>
		<div class="mm-footer__menu">
			<a href="http://keenthemes.com/metronic" target="_blank" class="mm-footer__menu-link mm-link">About</a>
			<a href="http://keenthemes.com/metronic" target="_blank" class="mm-footer__menu-link mm-link">Team</a>
			<a href="http://keenthemes.com/metronic" target="_blank" class="mm-footer__menu-link mm-link">Contact</a>
		</div>
	</div>
</div>

<!-- end:: Footer -->