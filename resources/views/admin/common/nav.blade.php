<!-- begin:: Header -->
<div id="kt_header" class="mm-header mm-grid__item  mm-header--fixed ">

<!-- begin: Header Menu -->
<button class="mm-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div class="mm-header-menu-wrapper" id="kt_header_menu_wrapper">
	<div id="kt_header_menu" class="mm-header-menu mm-header-menu-mobile  mm-header-menu--layout-tab ">
		<ul class="mm-menu__nav ">
			<li class="mm-menu__item  mm-menu__item--submenu mm-menu__item--rel" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('products.index') }}" class="mm-menu__link mm-menu__toggle"><span class="mm-menu__link-text">Sản phẩm</span><i class="mm-menu__ver-arrow la la-angle-right"></i></a>

				@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
				<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--left">
					<ul class="mm-menu__subnav">
						<li class="mm-menu__item  mm-menu__item--submenu"><a href="{{ route('products.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon-computer"></i><span class="mm-menu__link-text">Quản lý sản phẩm</span></a>
						</li>
						<li class="mm-menu__item  mm-menu__item--submenu"><a href="{{ route('category-products.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon2-writing"></i><span class="mm-menu__link-text">Danh mục sản phẩm</span></a>
						</li>
					</ul>
				</div>
				@endif
			</li>
			<li class="mm-menu__item aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{route('order-customer.index')}}" class="mm-menu__link "><span class="mm-menu__link-text">Đơn hàng</span></a>
			</li>
			
			<li class="mm-menu__item aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('contact.index') }}" class="mm-menu__link "><span class="mm-menu__link-text">Khách hàng</span></a>
				@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
				<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--left">
					<ul class="mm-menu__subnav">
						<li class="mm-menu__item  mm-menu__item--submenu"><a href="{{ route('contact.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon-presentation-1"></i><span class="mm-menu__link-text">Khách hàng</span></a>
						</li>
						<li class="mm-menu__item  mm-menu__item--submenu"><a href="{{ route('settingCustomer') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon2-gear"></i><span class="mm-menu__link-text">Cấu hình khách hàng mới</span></a>
						</li>
					</ul>
				</div>
				@endif
			</li>
			<li class="mm-menu__item aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="" class="mm-menu__link "><span class="mm-menu__link-text">Website</span></a>
				@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
				<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--left">
					<ul class="mm-menu__subnav">
						<li class="mm-menu__item">
							<a href="{{ route('information.index') }}" class="mm-menu__link mm-menu__toggle">
								<i class="mm-menu__link-icon flaticon-list" aria-hidden="true"></i><span class="mm-menu__link-text">Nội dung website</span>
							</a>
						</li>
						<li class="mm-menu__item">
							<a href="{{ route('posts.index') }}" class="mm-menu__link mm-menu__toggle">
								<i class="mm-menu__link-icon flaticon-list" aria-hidden="true"></i><span class="mm-menu__link-text">Bài viết</span>
							</a>
						</li>
						<li class="mm-menu__item">
							<a href="{{ route('categories.index') }}" class="mm-menu__link mm-menu__toggle">
								<i class="mm-menu__link-icon flaticon-list" aria-hidden="true"></i><span class="mm-menu__link-text">Danh mục</span>
							</a>
						</li>
						@foreach($typeSubPostsAdmin as $typeSubPost)
                            <li class="mm-menu__item">
                                <a href="{{ route('sub-posts.index', ['typePost' => $typeSubPost->slug]) }}" class="mm-menu__link mm-menu__toggle">
                                    <i class="mm-menu__link-icon flaticon-list" aria-hidden="true"></i><span class="mm-menu__link-text">{{ $typeSubPost->title }}</span>
                                </a>
                            </li>
                        @endforeach
						<li class="mm-menu__item">
							<a href="/admin/hinh-thuc-thanh-toan" class="mm-menu__link mm-menu__toggle">
								<i class="mm-menu__link-icon flaticon-list" aria-hidden="true"></i><span class="mm-menu__link-text">Cấu hình</span>
							</a>
						</li>
					</ul>
				</div>
				@endif
			</li>
			<li class="mm-menu__item aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="#" class="mm-menu__link "><span class="mm-menu__link-text">Marketing</span></a>
				<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--left">
					<ul class="mm-menu__subnav">
					<li class="mm-menu__item  mm-menu__item--submenu aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('communicate.google_ads') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon2-start-up"></i><span class="mm-menu__link-text">Phân tích</span><i class="mm-menu__hor-arrow la la-angle-right"></i></a>
						@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
						<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--right">
							<ul class="mm-menu__subnav">
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.analytics') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Phân tích đối thủ</span>
									</a>
								</li>
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.google_ads') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Google ads</span>
									</a>
								</li>

							</ul>
						</div>
						@endif
					</li>
					<li class="mm-menu__item  mm-menu__item--submenu aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('products.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon2-start-up"></i><span class="mm-menu__link-text">Rao vặt</span><i class="mm-menu__hor-arrow la la-angle-right"></i></a>
						@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
						<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--right">
							<ul class="mm-menu__subnav">
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.shopee') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Shopee</span>
									</a>
								</li>
								
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.lazada') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">lazada</span>
									</a>
								</li>

								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.sendo') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Sendo</span>

									</a>
								</li>
								
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.tiki') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Tiki</span>
									</a>
								</li>

								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.voso') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Vỏ sò</span>
									</a>
								</li>
								
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.muare') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Muare</span>
									</a>
								</li>

								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.muaban') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Muaban</span>
									</a>
								</li>

								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.enbac') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Én bạc</span>

									</a>
								</li>
								
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.raovatvietnamnet') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Rao Vặt Vietnamnet</span>
									</a>
								</li>

								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.chophien') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Chợ phiên</span>
									</a>
								</li>

								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.ndfloodinfo') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">NDfloodinfo</span>
									</a>
								</li>

								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('communicate.muabanraovat') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">muabanraovat</span>
									</a>
								</li>
							</ul>
						</div>
						@endif
					</li>
					<li class="mm-menu__item  mm-menu__item--submenu aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('transport.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon flaticon2-start-up"></i><span class="mm-menu__link-text">Vận chuyển</span><i class="mm-menu__hor-arrow la la-angle-right"></i></a>
						@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
						<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--right">
							<ul class="mm-menu__subnav">
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('transport.giaohangtietkiem') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Giao hàng tiết kiệm</span>
									</a>
								</li>
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('transport.viettel') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Giao hàng viettel</span>
									</a>
								</li>
								<li class="mm-menu__item  mm-menu__item--submenu">
									<a href="{{ route('transport.giaohangnhanh') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-share" aria-hidden="true"></i> <span class="mm-menu__link-text">Giao hàng nhanh</span>
									</a>
								</li>
							</ul>
						</div>
						@endif
					</li>
					</ul>
				</div>
			</li>
			<li class="mm-menu__item"><a href="{{ route('show_zalo') }}" class="mm-menu__link "><span class="mm-menu__link-text">Zalo</span></a></li>
			<li class="mm-menu__item  mm-menu__item--active mm-menu__item--submenu mm-menu__item--rel" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('admin_home')  }}" class="mm-menu__link "><span class="mm-menu__link-text">Báo cáo</span></a>
			</li>
			@if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
			<li class="mm-menu__item mm-menu__item--submenu mm-menu__item--rel" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="{{ route('domains.index') }}" class="mm-menu__link"><span class="mm-menu__link-text">ADMIN</span></a>
				@if(!\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
					<div class="mm-menu__submenu mm-menu__submenu--classic mm-menu__submenu--left">
						<ul class="mm-menu__subnav">

							@if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
								<li class="mm-menu__item">
									<a href="{{ route('type-sub-post.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-clipboard" aria-hidden="true"></i><span class="mm-menu__link-text">Dạng bài viết</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('type-input.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-clipboard" aria-hidden="true"></i><span class="mm-menu__link-text">Trường dữ liệu</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('type-information.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-info-circle" aria-hidden="true"></i><span class="mm-menu__link-text">Trường Thông tin</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('templates.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-desktop" aria-hidden="true"></i><span class="mm-menu__link-text">Template</span></a>
								</li>

								<li class="mm-menu__item">
									<a href="{{ route('domains.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-globe" aria-hidden="true"></i><span class="mm-menu__link-text">Domains</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('group-theme.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-object-group" aria-hidden="true"></i><span class="mm-menu__link-text">Nhóm theme</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('themes.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-object-group" aria-hidden="true"></i><span class="mm-menu__link-text">Theme</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('group-help-video.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-object-group" aria-hidden="true"></i><span class="mm-menu__link-text">Nhóm hướng dẫn</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('help-video.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-object-group" aria-hidden="true"></i><span class="mm-menu__link-text">Hướng dẫn</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('change-domain') }}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-info-circle" aria-hidden="true"></i><span class="mm-menu__link-text">Thay đổi tên miền</span>
									</a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('users.index') }}" class="mm-menu__link mm-menu__toggle"><i class="mm-menu__link-icon fa fa-object-group" aria-hidden="true"></i><span class="mm-menu__link-text">Quản lý users</span></a>
								</li>
								<li class="mm-menu__item">
									<a href="{{ route('orderAdmin') }}" class="mm-menu__link"><i class="mm-menu__link-icon fa fa-object-group" aria-hidden="true"></i><span class="mm-menu__link-text">Đơn hàng</span></a>
								</li>
							   @if(\Illuminate\Support\Facades\Auth::user()->role >= 4)
								<li class="mm-menu__item">
									<a href="{{ route('mail_user')}}" class="mm-menu__link mm-menu__toggle">
										<i class="mm-menu__link-icon fa fa-envelope-open" aria-hidden="true"></i><span class="mm-menu__link-text">Gia hạn Gói Email</span>
									</a>
								</li>
								@endif
							@endif
						</ul>
					</div>
                @endif
		
			</li>
			@endif
		</ul>
	</div>
</div>

<!-- end: Header Menu -->

<!-- begin:: Header Topbar -->
<div class="mm-header__topbar">

	<!--begin: Search -->
	<div class="mm-header__topbar-item mm-header__topbar-item--search">
		<div class="mm-header__topbar-wrapper">
			<a href="/admin/help" class="mm-header__topbar-icon"><i class="flaticon-questions-circular-button"></i></a>
		</div>
 	</div>

	<!--end: Search -->

	<!--begin: Notifications -->
	<div class="mm-header__topbar-item dropdown">
		<div class="mm-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="true">
			<span class="mm-header__topbar-icon"><i class="flaticon2-bell-alarm-symbol"></i></span>
			@if (!empty($countRp) || \App\Entity\RemindContact::remindContact()->count() > 0)
			<span class="mm-badge mm-badge--notify mm-badge--danger mm-badge--md mm-badge--rounded">
				{!! ($countRp + \App\Entity\RemindContact::remindContact()->count()) !!}</span>
			@endif
		</div>
		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
			<form>

				<!--begin: Head -->
				<div class="mm-head mm-head--skin-light mm-head--fit-x mm-head--fit-b">
					<h3 class="mm-head__title">
						Thông Báo
						&nbsp;
						@if (!empty($countRp) || \App\Entity\RemindContact::remindContact()->count() > 0)
						<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">{!! ($countRp + \App\Entity\RemindContact::remindContact()->count()) !!} cái mới</span>
						@endif
					</h3>
				</div>

				<!--end: Head -->
				<div class="tab-content">
					<div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
						
						<div class="mm-notification mm-margin-t-10 mm-margin-b-10 mm-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
							@foreach (\App\Entity\RemindContact::remindContact() as $remindContact)
							<a href="{{ route('contact.show', ['contact_id' => $remindContact->contact_id, 'remind' => 1]) }}" class="mm-notification__item">
								<div class="mm-notification__item-icon">
									<i class="flaticon2-box-1 mm-font-brand"></i>
								</div>
								<div class="mm-notification__item-details">
									<div class="mm-notification__item-title">
										{!! $remindContact->content !!}
									</div>
									<div class="mm-notification__item-time">
										1 phút trước
									</div>
								</div>
							</a>
							@endforeach
						</div>
						@if (!empty($countRp) || \App\Entity\RemindContact::remindContact()->count() == 0)
						<div class="mm-grid mm-grid--ver" style="min-height: 200px;">
							<div class="mm-grid mm-grid--hor mm-grid__item mm-grid__item--fluid mm-grid__item--middle">
								<div class="mm-grid__item mm-grid__item--middle mm-align-center">
									Hiện tại bạn không có thông báo nào
								</div>
							</div>
						</div>
						@endif
					</div>
				</div>
			</form>
		</div>
	</div>

	<!--end: Notifications -->

	@if (!empty($domainUser))
		<?php
		$datetime1 = new DateTime();
		$datetime2 = new DateTime($domainUser->end_at);
		$interval = $datetime1->diff($datetime2);
		?>
	@endif
	<!--begin: Quick panel -->
	<div class="mm-header__topbar-item mm-header__topbar-item--quick-panel" title="Xem Website">
		<a class="mm-header__topbar-icon" id="kt_quick_panel_toggler_btn" target="_blank" href="{{ !empty($domainUser->domain) ? $domainUser->domain : !empty($domainUser->url) ? $domainUser->url : '/' }}">
			<i class="flaticon-globe"></i>
		</a>
	</div>

	<!--end: Quick panel -->

	<!--begin: User Bar -->
	<div class="mm-header__topbar-item mm-header__topbar-item--user">
		<div class="mm-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
			<div class="mm-header__topbar-user">
				<!--use below badge element instead the user avatar to display username's first letter(remove mm-hidden class to display it) -->
				<span class="mm-badge mm-badge--username mm-badge--unified-success mm-badge--lg mm-badge--rounded mm-badge--bolder"><img alt="Pic" src="{{ (isset(Auth::user()->image) && !empty(Auth::user()->image)) ? asset(Auth::user()->image) : asset('image/avatar_admin.png') }}" /></span>
			</div>
		</div>
		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

			<!--begin: Head -->
			<div class="mm-user-card mm-user-card--skin-dark mm-notification-item-padding-x" style="background-image: url({{asset('MomaManager/media/misc/bg-1.jpg')}}">
				<div class="mm-user-card__avatar">
					<span class="mm-badge mm-badge--lg mm-badge--rounded mm-badge--bold mm-font-success"><img alt="Pic" src="{{ (isset(Auth::user()->image) && !empty(Auth::user()->image)) ? asset(Auth::user()->image) : asset('image/avatar_admin.png') }}" /></span>
				</div>
				<div class="mm-user-card__name">
					{{Auth::user()->name}}
				</div>
				@if (!empty($countRp) || \App\Entity\RemindContact::remindContact()->count() > 0)
					<div class="mm-user-card__badge">
						<span class="btn btn-success btn-sm btn-bold btn-font-md">{!! ($countRp + \App\Entity\RemindContact::remindContact()->count()) !!} Thông báo</span>
					</div>
				@endif
			</div>

			<!--end: Head -->

			<!--begin: Navigation -->
			<div class="mm-notification">
				<a href="{{ route('users.edit', ['id' => Auth::user()->id]) }}" class="mm-notification__item">
					<div class="mm-notification__item-icon">
						<i class="flaticon2-calendar-3 mm-font-success"></i>
					</div>
					<div class="mm-notification__item-details">
						<div class="mm-notification__item-title mm-font-bold">
							Trang cá nhân
						</div>
						<div class="mm-notification__item-time">
							Cài đặt thông tin cá nhân
						</div>
					</div>
				</a>
				<div class="mm-notification__custom mm-space-between">
					<a onclick="event.preventDefault();                                                    document.getElementById('logout-form').submit();" class="btn btn-label btn-label-brand btn-sm btn-bold">Thoát</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</div>
			</div>

			<!--end: Navigation -->
		</div>
	</div>

	<!--end: User Bar -->
</div>

<!-- end:: Header Topbar -->
</div>

<!-- end:: Header -->
