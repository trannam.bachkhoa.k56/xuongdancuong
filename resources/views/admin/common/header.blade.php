<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="mm-header-mobile  mm-header-mobile--fixed ">
	<div class="mm-header-mobile__logo">
		<a href="/">
			<img alt="Logo" width="100" src="{{asset('MomaManager/img/logomoma.png')}}" />
		</a>
	</div>
	<div class="mm-header-mobile__toolbar">
		<button class="mm-header-mobile__toolbar-toggler mm-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
		<button class="mm-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
		<button class="mm-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
	</div>
</div>
