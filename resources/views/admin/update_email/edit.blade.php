@extends('admin.layout.admin')

@section('title', 'Chinh sửa '.$user->email )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update email tài khoản {{ $user->email }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Update email tài khoản </a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{route('update_user_mail') }}" method="POST">
                <div class="col-xs-12 col-md-6">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->
        
                            <div class="box-body">                        
                                <table id="domains" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th>Tên</th>
                                        <th>email </th>          
                                        <th>Số lượng mail Còn lại</th>
                                    </tr>
                                    </thead>
                                    <tfoot class="box-inline">
                                      <tr>
                                        <td>{{ $user->id}}</td>
                                        <td>{{ $user->name}}</td>
                                        <td>{{ $user->email}}</td>
                                        <td>
                                            <input type="hidden" name="userId" value="{{$user->id}}" >
                                           <input type="number" name="email_remaining" value="{{$user->email_remaining}}">
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                        </div>
                        <!-- /.box -->
                    <!-- /.box -->
                </div>
            </form>
        </div>
    </section>
    @endsection

