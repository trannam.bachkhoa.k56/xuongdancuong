@extends('admin.layout.admin')

@section('title', 'Gia hạn mail cho khách hàng' )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Gia hạn mail cho khách hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Gia hạn mail cho khách hàng</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                 <!-- Nội dung thêm mới -->
                    <div class="box box-primary">

                        <div class="box-header with-border">
                        	<h3 class="box-title">Tìm kiếm khách hàng</h3>
	                    </div>
	                    <!-- /.box-header -->

	                    <div class="box-body">

	                        <form action="" method="GET">
	                            <div class="form-group col-xs-12 col-md-10">
	                                <input class="form-control" value="" id='search'
	                                       name="name" onkeyup="sendUser(this)" placeholder="Tên khách hàng"/>
	                            </div>
	                          
	                        </form>
	                    </div>
                        <!-- /.box-header -->
	                       		<table id="domains" class="table table-bordered table-striped">
		                            <thead>
		                            <tr>
		                                <th width="5%">ID</th>
		                                <th>Tên</th>
		                                <th>Email </th>		   
		                                <th>Thao tác</th>
		                            </tr>
		                            </thead>
		                            <tfoot class="box-inline">
			                        
		                            </tfoot>
		                        </table>
			                <!-- /.box-body -->
			            </div>

                    </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    <script>
    	function sendUser(e){
    		$.ajax({
				type : 'get',
				url : '{{route("find_user")}}',
				data: {
					value : $(e).val()
				},
				success:function(data){
					$('.box-inline').html(data);
				}
			});
    	}
	</script>
		

@endsection

