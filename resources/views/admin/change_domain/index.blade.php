@extends('admin.layout.admin')

@section('title', 'Thay đổi tên miền')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thay đổi tên miền
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Thay đổi tên miền</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('post_change_domain') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-8">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tự thay tên miền</h3>
                            <p>Bước 1: Trỏ tên miền về ip: 123.30.186.20</p>
                            <p>Bước 2: Cập nhật tên miền của bạn vào ô tên miền dưới dạng http://tenmien.xxx</p>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Cập nhật tên miền: </label>
                                <input type="text" class="form-control" name="domain" placeholder="Cập nhật tên miền" required>
                            </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thay tên miền</button>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Yêu cầu hỗ trợ thay tên miền từ Moma</h3>
                            <p>Tên miền chính là địa chỉ và cũng là thương hiệu trực tuyến của doanh nghiệp trên Internet. Sở hữu một tên miền sẽ giúp khách hàng truy cập trực tiếp vào website của bạn thay vì đối thủ. </p>
                            <p>Nếu bạn cần moma hỗ trợ thay tên miền thì chi phí tiền công là 100.000 đ / 1 lần thay.</p>
                            <p>Đăng ký hỗ trợ thay tên miền <a href="">tại đây</a></p>
                        </div>
                        <!-- /.box-header -->

                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

