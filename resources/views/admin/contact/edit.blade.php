@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa liên hệ')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Chỉnh sửa liên hệ {{ $contact->name }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Liên hệ</a></li>
        <li class="active">Chỉnh sửa</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- form start -->
        <form role="form" action="{{ route('contact.update', ['contact_id' => $contact->contact_id]) }}" method="POST">
            {!! csrf_field() !!}
            {{ method_field('PUT') }}
            <div class="col-xs-12 col-md-8">

                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nội dung</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Họ và tên</label>
                            <input type="text" class="form-control" name="name" placeholder="Họ và tên"
                                   value="{{ $contact->name }}" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Điện thoại</label>
                            <input type="text" class="form-control" name="phone" placeholder="Điện thoại" value="{{ $contact->phone }}" />
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $contact->email }}"  />
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Địa chỉ</label>
                            <input type="text" class="form-control" name="address" placeholder="Địa chỉ" value="{{ $contact->address }}" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Nguồn dữ liệu</label>
                            <input type="text" class="form-control" name="utm_source" placeholder="Nguồn dữ liệu" value="{{ $contact->utm_source }}" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tiền hoa hồng nhận được khi giới thiệu</label>
                            <input type="number" min='0' max="100" class="form-control formatPrice" name="affiliate_money" placeholder="Hoa hồng tính theo %" value="{{ $contact->affiliate_money }}" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tag</label>
                            <input type="text" class="form-control" name="tag" placeholder="Tag"  value="{{$contact->tag}}" />
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Chọn màu sắc</label>
                            <input type="color" class="" name="color" placeholder="Chọn màu sắc" value="{{ $contact->color }}">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Message</label>
                                <textarea rows="4" id="editor" class="form-control editor" name="message"
                                          placeholder="">{{ $contact->message }}</textarea>
                        </div>


                        <div class="form-group" style="color: red;">
                            @if ($errors->has('name'))
                                <label for="exampleInputEmail1">{{ $errors->first('name') }}</label>
                            @endif
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </div>
                <!-- /.box -->

            </div>

        </form>
    </div>
</section>
@endsection

