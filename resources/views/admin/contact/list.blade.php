@extends('admin.layout.admin')

@section('title', 'Danh sách khách hàng')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Danh sách khách hàng
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Danh sách khách hàng</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <form action="{{ route('contact_delete_all') }}" method="post" style="float:left; margin-right: 5px;">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id_delete" id="idDeletes" value=""/>
                                <button type="submit" class="btn btn-danger">Xóa nhiều</button>
                            </form>
                            <a href="{{ route('contact.create') }}" ><button class="btn btn-success" id="" data-step="1" data-intro="Ấn vào đây nếu bạn muốn thêm mới sản phẩm, ...">Thêm mới</button></a>
                            <a href="{{ route('contact.index') }}" ><button class="btn btn-warning" id="" data-step="1" data-intro="Ấn vào đây nếu bạn muốn thêm mới sản phẩm, ...">Danh sách Kanban</button></a>
                        </div>
                    </div>
                    <p></p>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <table id="contact2" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Khách Hàng</th>
                        </tr>
                        </thead>
                    </table>
                    <table id="contact" class="table table-bordered table-striped hideMB" style="width:1800px !important;">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>
                                    <input type="checkbox" name="checkall" id="select_all" onClick="check_uncheck_checkbox(this                                         .checked);" />
                                </th>
                                <th width="10%">Họ và tên</th>
                                <th width="10%">Số điện thoại</th>
                                <th width="10%">Tổng tiền</th>
								<th width="10%">% HH</th>
                                <th width="10%">LH lần cuối</th>
                                <th width="10%">TĐ lần cuối</th>   
                                <th width="10%">Email</th>
                                <th width="15%">Địa chỉ</th>
                                <th width="10%">Giới thiệu</th>
                                <th width="10%">Nguồn</th>
                                <th>Ngày tạo</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@include('admin.partials.popup_delete')


@endsection
@push('scripts')
<script>
    function check_uncheck_checkbox(isChecked) {
        if(isChecked) {
            $('.chkDelete').each(function() {
                this.checked = true;
            });
        } else {
            $('.chkDelete').each(function() {
                this.checked = false;
            });
        }
        getIdDelete();
    }

    function checkElement(e) {
        if($('.chkDelete:checked').length == $('.chkDelete').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
        getIdDelete();
    }

    function getIdDelete() {
        var idDeletes = '';
        $('.chkDelete:checked').each(function() {
            var idDelete = $(this).val();
            idDeletes += ',' + idDelete;
        });

        $('#idDeletes').val(idDeletes);
    }
    
    $(function() {
        $window = $(window);
        var windowsize = $window.width();
        if(windowsize > 765) {
            $('#contact').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                scrollX: true,
                scrollY: 400,
                autoWidth: false,
                pageLength: 100,
                type: "GET",
                ajax: '{!! route('contact_datatable') !!}',
                columns: [
                    { data: 'contact_id', name: 'contact.contact_id' },
                    { data: 'contact_id', name: 'posts.contact_id', orderable: false,
                        render: function ( data, type, row, meta ) {
                            return '<input type="checkbox" name="chkdelete" class="chkDelete" value="'+data+'" onclick="return checkElement(this);"/>';
                        },
                        searchable: false
                    },
                    { data: 'name', name: 'contact.name',
                        render: function (data, type, row) {
                            return '<a href="/admin/contact/' + row.contact_id + '" target="_blank">' + data + '</a>';
                        }
                    },
                    { data: 'phone', name: 'contact.phone', render: function (data) {
                            return `<a href="tel:${data}">${data}</a>`;
                        }
                    },
                    { data: 'totalprice', name: 'order_customer.total_price', render: function (data, type, row) {
                            return numeral(data).format('0,0') + ' đ';
                        }
                    },
					{ data: 'affiliate_money', name: 'order_customer.affiliate_money', render: function (data, type, row) {
							if (data !== null) {
								return data + '%';
							}
                            return 0;
                        }
                    },
                    { data: 'dayConn', orderable: false, searchable: false },
                    { data: 'convention', orderable: false, searchable: false },
                    { data: 'email', name: 'contact.email' },
                    { data: 'address', name: 'contact.address' },
                    { data: 'affiliate_name', name: 'contact_affiliate.name', render: function (data, type, row) {
                            return `<a href="/admin/contact/${row.affiliate_id}" target="_blank">${data}</a>`;
                        } 
                    },
                    { data: 'utm_source', name: 'contact.utm_source' },
                    { data: 'created_at', name: 'contact.created_at' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        }else {
            $('#contact2').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                type: "GET",
                ajax: '{!! route('contact_datatable') !!}',
                columns: [
                    { data: 'contact_id', name: 'contact.contact_id', render: function (data, type, row, meta) {
                            return `<div class="box box-warning box-solid mgBottom0">
                                <div class="box-header with-border">
                                  <h3 class="box-title">${row.name}</h3>
                                  <div class="box-tools pull-right">
                                        <input type="checkbox" name="chkdelete" class="chkDelete" value="'+ row.contact_id +'" onclick="return checkElement(this);"/>
                                        ${row.action}
                                  </div>
                                    </div>
                                    <div class="box-body">
                                         <p>Tổng tiền: ${numeral(row.totalprice).format('0,0')} đ</p>
                                         <p>Email: ${row.email} </p>
                                         <p>SDT: ${row.phone} </p>
                                         <p>Địa chỉ: ${row.address} </p>
                                         <p>Liên hệ lần cuối: ${row.dayConn} </p>
                                         <p>Trao đổi lần cuối: ${row.convention} </p>
                                         <p>Ngày tạo: ${row.created_at} </p>
                                    </div>
                            </div>`;
                        }
                    },
                ]
            });
        }

    });

    function check_uncheck_checkbox(isChecked) {
        if(isChecked) {
            $('.chkDelete').each(function() {
                this.checked = true;
            });
        } else {
            $('.chkDelete').each(function() {
                this.checked = false;
            });
        }
        getIdDelete();
    }

    function checkElement(e) {
        if($('.chkDelete:checked').length == $('.chkDelete').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
        getIdDelete();
    }

    function getIdDelete() {
        var idDeletes = '';
        $('.chkDelete:checked').each(function() {
            var idDelete = $(this).val();
            idDeletes += ',' + idDelete;
        });

        $('#idDeletes').val(idDeletes);
    }

    function getCateProductGetfly(e) {
        var btn = $(e).button('loading');
        $.ajax({
            type: "get",
            url: '{!! route('getCateProductGetfly') !!}',
            success: function(result){
                $('#getCateProductGetFly').show();
                var obj = jQuery.parseJSON( result);
                if (obj.httpCode == 500) {
                    alert('Đã có lỗi xảy ra vui lòng liên hệ với chúng tôi, để chúng tôi hỗ trợ.');
                    btn.button('reset');

                    return true;
                }
                if (obj.httpCode == 200) {
                    $(e).hide();
                    btn.button('reset');
                    var categories = obj.categories;
                    $.each(categories, function(index, element) {
                        var html = '<div class="form-group col-xs-12">';
                        html += '<label style="cursor: pointer;"><input type="checkbox"  name="cate_getfly[]" value="'+element.category_id+', '+element.category_name+', '+ element.parent_id + '" class="flat-red" /> '+ element.cate_name_show +'</label>';
                        html += '</div>';

                        $('#getCateProductGetFly').find('.formCateProduct').append(html);
                    });

                    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                        checkboxClass: "icheckbox_flat-green",
                        radioClass   : "iradio_flat-green"
                    })

                }
            }
        });
    }

</script>


@endpush
