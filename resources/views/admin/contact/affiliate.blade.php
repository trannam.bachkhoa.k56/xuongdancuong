<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="day1.css">
     <!-- Font Awesome -->
     <link rel="stylesheet" href="{{asset('adminstration/font-awesome/css/font-awesome.min.css')}}">
    <title>Mã giảm giá</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
</head>
<body>
    <div class="body" id="html-content-holder">
		<div class="left">
			<p class="txtCenter"><img class="logo" src="{{ $information['logo'] }}" alt="" style="width: 150px; height: auto;"></p>
			<p class="txtCenter uppercase">Thẻ {!! isset($nameCard) ? $nameCard : 'Vip 1' !!}</p>
            <!--<p class="txtCenter uppercase">Giảm Giá: {!! number_format($affiliate, 0,",",".") !!}% / đơn hàng</p> -->
			<p class="txtCenter"><i>Quét mã zalo</i></p>
			<div class="qrCode txtCenter">
                <img src="{!! $imageQr !!}"/>
			</div>
            
		</div>
		<div class="right">
			<p class="txtCenter">{!! empty($domainUser->domain) ? $domainUser->url : $domainUser->domain !!} </p>
            <div class="contact">
                <p><span class="iconCall"><i class="fa fa-phone-square" aria-hidden="true"></i></span> {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai'] : ''  }}</p>
                <p><span class="iconZalo">zalo</span> {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai'] : ''  }}</p>
            </div>
            <p class="txtCenter mgTop">{{ isset($information['dia-chi-dau-trang']) ? $information['dia-chi-dau-trang'] : ''  }} <span class="icon"><i class="fa fa-home" aria-hidden="true"></i></span></p>
		</div>
	</div>

    <style>
        .body {
            width: 1020px;
            height: 560px;
            border: 1px solid #e5e5e5;
        }
        .left {
            width: 50%;
            display: inline-block;
        }
        .right {
            width: 48.2%;
            display: inline-block;
            border-left: 16px solid #464343;
            background: #060606;
            color: white;
            padding-top: 15px;
            padding-bottom: 14px;
            height: 95%;
            vertical-align: top;
            -webkit-print-color-adjust: exact;
        }
        .txtCenter {
            text-align: center;
        }
        .uppercase {
            text-transform: uppercase;
        }
        p {
            font-size:35px;
            margin-top: 2%;
            margin-bottom: 5%;
        }
        .contact {
            padding: 5%;
            background: #f79706;
            margin-top: 24%;
            margin-bottom: 15px;
        }
        .qrCode img {
            border: 1px solid red;
            border-radius: 8px;
        }
        .iconZalo {
            background: white;
            color: #f79706;
            padding: 2px 1px;
            font-size: 18px;
        }
        .iconCall {
            background: white;
            color: #f79706;
            padding: 2px 1px;
        }
        .mgTop {
            margin-top: 20%;
        }
    </style>
</body>
</html>
