@if (!empty($messageErrorVip))
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Lỗi:</span> {!!$messageErrorVip !!}
    </div>
@endif
@if (empty($messageErrorVip) || !isset($messageErrorVip))
    <div id="myKanban"></div>

    <script>
        function callUpdateStatus (contactId, status) {
            $.ajax({
                url: '{!! route('updateContactStatus') !!}',
                data: {
                    contact_id: contactId,
                    status: status
                },
                success: function(result){

                },
                error: function(error) {
                }
            });
        }

        function minusCountContact(status) {
            var countContact = $("#status"+status).html();
            countContact = parseInt(countContact) - 1;
            $("#status"+status).html(countContact);
        }

        function addCountContact(status) {
            var countContact = $("#status"+status).html();
            countContact = parseInt(countContact) + 1;
            $("#status"+status).html(countContact);
        }

        var KanbanTest = new jKanban({
            element: "#myKanban",
            gutter: "10px",
            widthBoard: "300px",
            click: function(el) {
                console.log("Trigger on all items click!");
            },
            buttonClick: function(el, boardId) {

            },
            addItemButton: false,
            boards: [
                {
                    id: "0",
                    title: 'KH Mới (<span id="status0">{{ $countContactNews }}</span>) <div class="number">{{ number_format($sumPriceContactNews) }} đ</div>',
                    class: "info,good",
                    item: [
                            @foreach ($contacts as $contact)
                            @if ($contact->status == 0)
                        {
                            id: "{{ $contact->contact_id }}",
                            title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
                            drag: function(el, source) {
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                minusCountContact(status);
                            },
                            dragend: function(el) {
                                var contactId = el.dataset.eid;
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                addCountContact(status);
                                callUpdateStatus (contactId, status)
                            },
                            click: function (el) {
                                window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
                            }
                        },
                        @endif
                        @endforeach
                    ]
                },
                {
                    id: "1",
                    title: 'KH tiếp cận (<span id="status1">{{ $countContactApproachs }}</span>) <div class="number">{{ number_format($sumPriceContactApproachs) }} đ</div>',
                    class: "warning",
                    item: [
                            @foreach ($contacts as $contact)
                            @if ($contact->status == 1)
                        {
                            id: "{{ $contact->contact_id }}",
                            title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
                            drag: function(el, source) {
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                minusCountContact(status);
                            },
                            dragend: function(el) {
                                var contactId = el.dataset.eid;
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                addCountContact(status);
                                callUpdateStatus (contactId, status)
                            },
                            click: function (el) {
                                window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
                            }
                        },
                        @endif
                        @endforeach
                    ]
                },
                {
                    id: "2",
                    title: 'KH mua hàng (<span id="status2">{{ $countContactOrders }}</span>) <div class="number">{{ number_format($sumPriceContactOrders) }} đ</div>',
                    class: "success",
                    item: [
                            @foreach ($contacts as $contact)
                            @if ($contact->status == 2)
                        {
                            id: "{{ $contact->contact_id }}",
                            title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
                            drag: function(el, source) {
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                minusCountContact(status);
                            },
                            dragend: function(el) {
                                var contactId = el.dataset.eid;
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                addCountContact(status);
                                callUpdateStatus (contactId, status)
                            },
                            click: function (el) {
                                window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
                            }
                        },
                        @endif
                        @endforeach
                    ]
                },
                {
                    id: "3",
                    title: 'KH hoàn đơn(hoàn tiền) (<span id="status3">{{ $countContactReturns }}</span>) <div class="number">{{ number_format($sumPriceContactReturns) }} đ</div>',
                    class: "danger",
                    item: [
                            @foreach ($contacts as $contact)
                            @if ($contact->status == 3)
                        {
                            id: "{{ $contact->contact_id }}",
                            title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
                            drag: function(el, source) {
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                minusCountContact(status);
                            },
                            dragend: function(el) {
                                var contactId = el.dataset.eid;
                                var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                addCountContact(status);
                                callUpdateStatus (contactId, status)
                            },
                            click: function (el) {
                                window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
                            }
                        },
                        @endif
                        @endforeach
                    ]
                }
            ]
        });
    </script>
    {{ $contacts->links() }}
@endif
