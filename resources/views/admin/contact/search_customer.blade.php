@extends('admin.layout.admin')

@section('title', 'Danh sách khách hàng')

@section('content')
	<style type="text/css">
		#frm_others_date{
			z-index: 99999;
			background: #eee;
			border: 1px solid #666;
			padding: 5px
		}
		#frm_others_date input{
			margin-bottom: 10px;
		}
		ul.filte_time li{
			display: inline-block;
			padding: 10px;
			cursor: pointer;
		}
		ul.filte_time {
			float: right;
		}
		.w-100{
			width: 100% !important;
		}
	</style>
	<link rel="stylesheet" href="{{ asset('adminstration/css/jkanban.min.css') }}">
	<script src="{{ asset('adminstration/js/jkanban.min.js') }}"></script>
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Khách hàng
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Danh sách khách hàng</a></li>
		</ol>
	</section>
	<section class="content dragCustomer">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header with-border">
						<div class="col-md-3">
							<h3 class="box-title">Tìm kiếm khách hàng</h3>
						</div>

						<div class="col-md-9">
							<ul class="filte_time">
								<li><a data-time="THIS_DAY" class="btn_choose_time">Hôm nay </a></li>
								<li><a data-time="THIS_WEEK" class="btn_choose_time">Tuần này</a></li>
								<li><a data-time="LAST_WEEK" class="btn_choose_time">Tuần trước</a></li>

								<li><a data-time="THIS_MONTH" class="btn_choose_time">Tháng này</a></li>
								<li><a data-time="LAST_MONTH" class="btn_choose_time">Tháng trước</a></li>

								<li><a data-time="THIS_YEAR" class="btn_choose_time">Năm nay</a></li>
								<li><a data-time="LAST_YEAR" class="btn_choose_time">Năm trước</a></li>

								<li>
									<a data-time="TIME_REQUEST" onclick="return showModalTime();">
										Thời gian khác
									</a>
									<div class="add-drop add-d-right" id="frm_others_date" style="left: auto; right: 0px; display:  none;position: absolute;">
										<s class="gf-icon-neotop"></s>
										<div class="padding">
											<p>Ngày bắt đầu</p>
											<input placeholder="Ngày bắt đầu" id="start_date" type="date" class="datepick form-control hasDatepicker"> </div>
										<div class="padding">
											<p>Ngày kết thúc</p>
											<input placeholder="Ngày kết thúc" id="end_date" type="date" class="datepick form-control hasDatepicker"> </div>
										<div class="padding tl">
											<button class="btn btn-info btn_choose_time" data-time="TIME_REQUEST" id="filterByOthersTime">Áp dụng</button>

											<button class="btn btn-default"  onclick="return showModalTime();" id="closeFilterOthersTime">Đóng</button>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="box-body">
						<form action="{{route('search_customer')}}" method="GET">
							<div class="row">
								<div class="form-group col-xs-12 col-md-2">
									<select class="form-control" id="chooseSearch" name="search" onchange="chooseInputSearch(this)">
										<option value="0" {{$_GET['search'] == 0 ? 'selected' : ''}} >Tìm kiếm theo tên khách hàng</option>
										<option value="1" {{$_GET['search'] == 1 ? 'selected' : ''}}>Tìm kiếm theo tag khách hàng</option>
									</select>
								</div>

								<div class="form-group col-xs-12 col-md-4">
									<input class="form-control"
										   name="name" id="{{isset($_GET['name']) ? 'name' : 'tag'}}"
										   placeholder="{{isset($_GET['name']) ? 'Tên khách hàng' : 'Tag khách hàng'}}"
										   value="{{isset($_GET['name']) ? $_GET['name'] : $_GET['tag']}}"
										   onkeyup="suggestionInput(this)"/>
								</div>
								<div class="form-group col-xs-12 col-md-6">
									<button type="submit" class="btn btn-warning"><i class="fa fa-search-plus" aria-hidden="true"></i> Tìm kiếm</button>
								</div>
							</div>
						</form>
					</div>
					<div class="box-header">
						<div class="row">
							<div class="form-group col-xs-12 col-md-2">
								<a href="{{ route('contact.create') }}">
									<button class="btn btn-primary"> <i class="fa fa-user-plus" aria-hidden="true"></i>Thêm mới khách hàng
									</button>
								</a>
							</div>

							<div class="form-group col-xs-12 col-md-2">
								<button data-toggle="modal" data-target="#upload-excel" class="btn btn-info" href="#">
									<i class="fa fa-upload" aria-hidden="true"></i> Tải lên file excel khách hàng
								</button>

							</div>

							<div class="form-group col-xs-12 col-md-2">
								<button data-toggle="modal" data-target="#download-excel" class="btn btn-success" href="#">
									<i class="fa fa-cloud-download" aria-hidden="true"></i>Tải về file excel khách hàng
								</button>
							</div>
						</div>
					</div>
					@if (!empty($messageErrorVip))
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							<span class="sr-only">Lỗi:</span> {!!$messageErrorVip !!}
						</div>
					@endif
					@if (empty($messageErrorVip) || !isset($messageErrorVip))
						<div id="myKanban">

						</div>
						<script>
							function callUpdateStatus (contactId, status) {
								$.ajax({
									url: '{!! route('updateContactStatus') !!}',
									data: {
										contact_id: contactId,
										status: status
									},
									success: function(result){

									},
									error: function(error) {
									}
								});
							}

							function minusCountContact(status) {
								var countContact = $("#status"+status).html();
								countContact = parseInt(countContact) - 1;
								$("#status"+status).html(countContact);
							}

							function addCountContact(status) {
								var countContact = $("#status"+status).html();
								countContact = parseInt(countContact) + 1;
								$("#status"+status).html(countContact);
							}

							var KanbanTest = new jKanban({
								element: "#myKanban",
								gutter: "10px",
								widthBoard: "300px",
								click: function(el) {
									console.log("Trigger on all items click!");
								},
								buttonClick: function(el, boardId) {

								},
								addItemButton: false,
								boards: [
									{
										id: "0",
										title: 'KH Mới (<span id="status0">{{ $countContactNews }}</span>) <div class="number">{{ number_format($sumPriceContactNews) }} đ</div>',
										class: "info,good",
										item: [
												@foreach ($contacts as $contact)
												@if ($contact->status == 0)
											{
												id: "{{ $contact->contact_id }}",
												title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
												drag: function(el, source) {
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													minusCountContact(status);
												},
												dragend: function(el) {
													var contactId = el.dataset.eid;
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													addCountContact(status);
													callUpdateStatus (contactId, status)
												},
												click: function (el) {
													window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
												}
											},
											@endif
											@endforeach
										]
									},
									{
										id: "1",
										title: 'KH tiếp cận (<span id="status1">{{ $countContactApproachs }}</span>) <div class="number">{{ number_format($sumPriceContactApproachs) }} đ</div>',
										class: "warning",
										item: [
												@foreach ($contacts as $contact)
												@if ($contact->status == 1)
											{
												id: "{{ $contact->contact_id }}",
												title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
												drag: function(el, source) {
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													minusCountContact(status);
												},
												dragend: function(el) {
													var contactId = el.dataset.eid;
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													addCountContact(status);
													callUpdateStatus (contactId, status)
												},
												click: function (el) {
													window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
												}
											},
											@endif
											@endforeach
										]
									},
									{
										id: "2",
										title: 'KH mua hàng (<span id="status2">{{ $countContactOrders }}</span>) <div class="number">{{ number_format($sumPriceContactOrders) }} đ</div>',
										class: "success",
										item: [
												@foreach ($contacts as $contact)
												@if ($contact->status == 2)
											{
												id: "{{ $contact->contact_id }}",
												title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
												drag: function(el, source) {
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													minusCountContact(status);
												},
												dragend: function(el) {
													var contactId = el.dataset.eid;
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													addCountContact(status);
													callUpdateStatus (contactId, status)
												},
												click: function (el) {
													window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
												}
											},
											@endif
											@endforeach
										]
									},
									{
										id: "3",
										title: 'KH hoàn đơn(hoàn tiền) (<span id="status3">{{ $countContactReturns }}</span>) <div class="number">{{ number_format($sumPriceContactReturns) }} đ</div>',
										class: "danger",
										item: [
												@foreach ($contacts as $contact)
												@if ($contact->status == 3)
											{
												id: "{{ $contact->contact_id }}",
												title: '<p style="color: {{ !empty($contact->color) ? $contact->color : "#000" }}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
												drag: function(el, source) {
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													minusCountContact(status);
												},
												dragend: function(el) {
													var contactId = el.dataset.eid;
													var status = KanbanTest.getParentBoardID(el.dataset.eid);
													addCountContact(status);
													callUpdateStatus (contactId, status)
												},
												click: function (el) {
													window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
												}
											},
											@endif
											@endforeach
										]
									}
								]
							});
						</script>
						{{ $contacts->links() }}
					@endif
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="upload-excel">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tải lên file Excel khách hàng</h4>
				</div>
				<form action="{{ route('import-customer-excel') }}" method="post" enctype="multipart/form-data">
					{!! csrf_field() !!}
					{{ method_field('POST') }}
					<div class="modal-body">
						<div class="form-group">
							<label for="type_customer">Loại khách hàng tải lên </label><br>
							<select id="type_customer" name="type_customer" class="select2 form-control">
								<option value="4">Tất cả khách hàng</option>
								<option value="0">Khách hàng mới</option>
								<option value="1">Khách hàng tiếp cận</option>
								<option value="2">Khách hàng mua hàng</option>
								<option value="3">Khách hàng hoàn đơn (hoàn tiền)</option>
							</select>
						</div>
						<div class="form-group">
							<label for="file">File tải lên</label>
							<input type="file" name="file" id="file" class="w-100" onchange="checkExtensionFile(this)" required/>
						</div>
						<div class="form-group">
							<label for="file">Để file Excel tải lên chúng tôi có thể đọc được, mời các bạn tham khảo mẫu của chúng tôi dưới đây</label>
							<a href="{{ asset('adminstration/excel/form/mau-tai-danh-sach-khach-hang.xls') }}" download><i class="fa fa-file-text-o" aria-hidden="true"></i> Tải xuống file mẫu</a>
						</div>
						<span id="error" style="color: #FF0000"></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
						<button type="submit" class="btn btn-primary">Tải lên</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="download-excel">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tải về file Excel khách hàng</h4>
				</div>
				<form action="{{ route('export-customer-excel') }}" method="get" >
					<div class="modal-body">
						<div class="form-group">
							<label for="status">Loại khách hàng tải xuống </label><br>
							<select id="status" name="status" class="select2">
								<option value="4">Tất cả khách hàng</option>
								<option value="0">Khách hàng mới</option>
								<option value="1">Khách hàng tiếp cận</option>
								<option value="2">Khách hàng mua hàng</option>
								<option value="3">Khách hàng hoàn đơn (hoàn tiền)</option>
							</select>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
						<button type="submit" class="btn btn-primary">Tải xuống</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function showModalTime(){
			$('#frm_others_date').toggle(400)
		}

		$('.btn_choose_time').click(function(e) {
			var time = $(this).attr('data-time');

			var start_date = $("#start_date").val();
			var end_date = $("#end_date").val();

			$('#myKanban').remove();
			$.ajax({
						url: '{{route("filterContactForTime")}}',
						type: 'POST',
						data: {
							time: time,
							start_date: start_date,
							end_date: end_date
						},
					})
					.done(function(data) {

						$('.box').append(data);

					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});
		});

		function checkExtensionFile(e) {
			if ($(e).val().search('.xlsx') !== -1 || $(e).val().search('.xls') !== -1 || $(e).val().search('.xlt') !== -1 || $(e).val().search('.xltx') !== -1){
				$(e).parent().parent().find($('#error')).text('');
			}else {
				$(e).parent().parent().find($('#error')).text('File của bạn tải lên không phải là file excel. Mời bạn vui lòng tải lên file excel (.xlsx, .xls, .xlt, .xltx)');
			}
		}

		function chooseInputSearch(e) {
			if($(e).val() == 1){
				$(e).parent().parent().find($('#name')).attr({
					'placeholder': 'Tag khách hàng',
					'name': 'tag'
				});
			}else {
				$(e).parent().parent().find($('#name')).attr({
					'placeholder': 'Tên khách hàng',
					'name': 'name'
				});
			}
		}

		function suggestionInput(e) {
			if($(e).parent().parent().find($('#chooseSearch')).val() == 1){
				$.ajax({
					url: '{{ route('show-tag-customer') }}',
					type: 'GET',
					data:{
						tag: $(e).val()
					},
					success: function (result) {
						$(e).autocomplete({
							source: result,
							appendTo: "#someElem"
						});
					},
					error: function (error) {
						console.log(error);
					}
				});
				$(e).attr({
					'placeholder': 'Tag khách hàng',
					'name': 'tag'
				});
				return false;
			}

			$.ajax({
				url: '{{ route('show-name-customer') }}',
				type: 'GET',
				data:{
					name: $(e).val()
				},
				success: function (result) {
					$(e).autocomplete({
						source: result,
						appendTo: "#someElem"
					});
				},
				error: function (error) {
					console.log(error);
				}
			});
			$(e).attr({
				'placeholder': 'Tên khách hàng',
				'name': 'name'
			});
		}

	</script>
	@if(session('success'))
		<script>
			$(function () {
				alert("{{ session('success') }}");
			});
		</script>
	@endif

	@if(session('failed'))
		<script>
			$(function () {
				alert("{{ session('failed') }}");
			})
		</script>
	@endif
@endsection
