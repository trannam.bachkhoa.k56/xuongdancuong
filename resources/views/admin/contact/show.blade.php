@extends('admin.layout.admin')

@section('title', 'KH '.$contact->name)

@section('content')
	<link rel="stylesheet" href="/adminstration/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- bootstrap datepicker -->
	<script src="/adminstration/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Khách hàng {{ $contact->name }}
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Khách hàng</a></li>
			<li class="active">{{ $contact->name }}</li>
		</ol>
	</section>

	<section class="content">
		<div>
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">Khách hàng</a></li>
				<li role="presentation"><a href="#order_customer" aria-controls="profile" role="tab" data-toggle="tab">Đơn hàng</a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="contact">
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-6">

							<!-- Nội dung thêm mới -->
							<div class="box box-primary">
								<div class="box-header with-border">
									<h3 class="box-title">Nội dung</h3>
								</div>
								<!-- /.box-header -->
								<div class="box-footer">
									<a href="{!! route('contact.index') !!}">
										<button class="btn btn-primary"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Quay lại </button>
									</a> | 
									<a href="{{ route('contact.edit', ['contact_id' => $contact->contact_id]) }}">
										<button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Chỉnh sửa </button>
									</a> |  
									<a  href="{{ route('contact.destroy', ['contact_id' => $contact->contact_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
										<i class="fa fa-trash-o" aria-hidden="true"></i>
									</a> |
									<a  href="#" class="btn btn-success " data-toggle="modal" data-target="#modalMoney" >
										<i class="fa fa-money" aria-hidden="true"></i> Trừ tiền khuyến mãi
									</a>
								</div>

								<div class="box-body">

									<div class="col-md-6">
										<div class="form-group">
											<label for="exampleInputEmail1">Họ và tên: {{ $contact->name }}</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Điện thoại: {{ $contact->phone }}
														<!--<a href="tell:{{ $contact->phone }}" style="color: #fb1100" title="Gọi ngay ">
                                    <img width="40" src="/public/adminstration/img/phone.gif"> </a> -->
											</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Email: {{ $contact->email }}</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Địa chỉ: {{ $contact->address }}</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Hoa hồng/ đơn hàng: {{ number_format($contact->affiliate_money) }} %</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Tiền hiện có: {{ number_format($contact->money) }} vnđ</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Nguồn dữ liệu: {{ $contact->utm_source }}</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Tags: {{ $contact->tag }}</label>
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Message:</label>
											<div >{!! $contact->message !!}</div>
										</div>
										@php $sum = 0; @endphp
										@if (!$orderContacts->isEmpty())
											@foreach ($orderContacts as $orderContact)
												@php $sum += str_replace('.', "", $orderContact->price ) @endphp
											@endforeach
											Tổng tiền <b>{{ number_format($sum) }}đ</b>
										@endif

										<div class="form-group">
											<label for="exampleInputEmail1">Thuộc chiến dịch: </label>
											{{isset($contact->campaign_title) ? $contact->campaign_title : 'Chưa có'  }}
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Trạng thái trong chiến dịch :
											</label>
											{{isset($contact->process_name) ? $contact->process_name : 'Chưa có'  }}
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Người phụ trách: </label>
											{{isset($contact->user_responsibility) ? $contact->user_responsibility : 'Chưa có'  }}
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-md-6 col-sm-6">
							<div class="box box-primary">

							</div>
							<div class="box box-primary">
								<div>
									<!-- Nav tabs -->
									<ul class="nav nav-tabs" role="tablist">
										<li role="presentation" class="{{ !isset($_GET['remind']) && !session()->has('order') ? "active" : "" }}"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Trạng thái</a></li>
										<li role="presentation" class="{{ isset($_GET['remind']) ? "active" : "" }}"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Hẹn gặp</a></li>

									</ul>

									<!-- Tab panes -->
									<div class="tab-content">
										<div role="tabpanel" class="tab-pane {{  !isset($_GET['remind']) && !session()->has('order') ? "active" : "" }}" id="home">
											<div class="box-header"><h3 class="box-title">Lịch sử trao đổi</h3></div>
											<div class="box-body">
												<form action="{{ route('noteContact') }}" >
													<div class="form-group">
                                        <textarea rows="4" class="form-control" name="note"
												  id="note-contact" placeholder="Ghi chú" required></textarea>

													</div>
													<input type="hidden" value="{{ $contact->contact_id }}" name="contact_id">
													<div class="form-group">
														<button type="submit" class="btn btn-success" id="note">Ghi</button>
													</div>
												</form>

												<div class="form-group">
													@foreach ($noteContacts as $noteContact)
														<?php
														$date=date_create( $noteContact->created_at);
														?>
														<p>{{ date_format($date,"d/m/Y H:i") }} - {{ $noteContact->content }}</p>
													@endforeach
												</div>
											</div>

											<div class="box-header"><h3 class="box-title">Lịch sử email hoặc cuộc gọi</h3></div>
											<div class="box-body">
												<div class="form-group">
													@foreach($emailCampaigns as $emailCampaign)
														<p>
															- Đã gửi email từ chiến dịch
															{{
                                                                \App\Entity\Campaign::getCampaignName($emailCampaign->campaign_id)
                                                            }}
														</p>
													@endforeach
													@foreach($emailSeenArray as $emailSeen)
														<p>
															- Đã đọc email của bạn lúc {{ $emailSeen }} từ nhóm khách hàng
															<?php
															switch ($contact->status) {
																case 1:
																	echo 'Khách hàng tiếp cận';
																	break;
																case 2:
																	echo 'Khách hàng mua hàng';
																	break;
																case 3:
																	echo 'Khách hàng hoàn tiền';
																	break;
																default:
																	echo 'Khách hàng mới';
																	break;
															}
															?>
														</p>
													@endforeach
												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane {{ isset($_GET['remind']) ? "active" : "" }}" id="profile">
											<div class="box-header"><h3 class="box-title">Hẹn gặp</h3></div>
											<div class="box-body">
												<form action="{{ route('remindContact') }}" >
													<div class="form-group">
                                            <textarea rows="4" class="form-control" name="note"
													  id="recomenderContact" placeholder="Nội dung hẹn gặp" required></textarea>
													</div>
													<div class="form-group">
														<div class="input-group date">
															<div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															</div>
															<input type="text" name="date" class="form-control pull-right" id="datepicker" placeholder="Chọn thời gian" required>
														</div>
													</div>
													<input type="hidden" value="{{ $contact->contact_id }}" name="contact_id">
													<div class="form-group">
														<button type="submit" class="btn btn-success" id="note">Đặt lịch hẹn gặp</button>
													</div>
												</form>
												<div class="form-group" >
													@foreach ($remindContacts as $remindContact)
														<?php
														$date = date_create($remindContact->date);
														?>
														<label><input type="checkbox" remindContactId="{{ $remindContact->remind_contact_id }}" onChange="return changeRemindContact(this);" {{ $remindContact->status == 1 ? "checked" : '' }}/> {{ date_format($date,"d/m/Y H:i") }} - {{ $remindContact->content }}</label>
														<hr>
													@endforeach

												</div>
												<script>
													function changeRemindContact(e) {
														var remindContactId = $(e).attr('remindContactId');
														if($(e). prop("checked") == true){
															var status = 1;
														} else {
															var status = 0;
														}

														$.ajax({
															url: '{!! route('updateRemindContact') !!}',
															data: {
																remind_contact_id: remindContactId,
																status: status
															},
															success: function(result){

															},
															error: function(error) {
															}
														});
													}
												</script>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="order_customer">
					<div class="row">
						<!-- form start -->

						<div class="col-xs-12 col-md-12">
							<div class="box">
								<div class="box-header">
									<a  href="{{ route('order-customer.create', ['contact_id' => $contact->contact_id]) }}"><button class="btn btn-primary">Thêm mới</button> </a>
								</div>
								<!-- /.box-header -->
								<div class="box-body">
									<table id="example1" class="table table-bordered table-striped">
										<thead>
										<tr>
											<th width="5%">STT</th>
											<th>Ngày đặt hàng</th>
											<th>khách hàng</th>
											<th>Doanh số</th>
											<th>Giá vốn</th>
											<th>phụ phí</th>
											<th>Đã thanh toán</th>
											<th>Công nợ</th>
											<th>Lợi nhuận</th>
											<th>Người thực hiện</th>
											<th>Xóa</th>
										</tr>
										</thead>
										<tbody>
										@foreach ($orderCustomers as $id => $orderCustomer)
											<tr>
												<td width="5%">{{ ($id+1) }}</td>
												<td>{{ date_format(date_create($orderCustomer->created_at), "d/m/Y H:i") }}</td>
												<td>{{ $orderCustomer->contact_name }}</td>
												<td>{{ number_format($orderCustomer->total_price, 0,",",".") }} đ</td>
												<td>{{ number_format($orderCustomer->cost, 0,",",".") }} đ</td>
												<td>{{ number_format($orderCustomer->surcharge, 0,",",".") }} đ</td>
												<td>{{ number_format($orderCustomer->payment, 0,",",".") }} đ</td>
												<td>{{ number_format(($orderCustomer->total_price - $orderCustomer->payment ), 0,",",".") }} đ</td>
												<td>{{ number_format(($orderCustomer->total_price - $orderCustomer->cost - $orderCustomer->surcharge ), 0,",",".") }} đ</td>
												<td>{{ $orderCustomer->user_name }}</td>
												<td>
													<a  href="{{ route('order-customer.show', ['order_customer_id' => $orderCustomer->order_customer_id]) }}" class="btn btn-primary" >
														<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
													<a  href="{{ route('order-customer.destroy', ['order_customer_id' => $orderCustomer->order_customer_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
														<i class="fa fa-trash-o" aria-hidden="true"></i>
													</a>
												</td>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
								<!-- /.box-body -->
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</section>
	@include('admin.partials.popup_delete')
	<!-- Modal -->
	<div class="modal fade" id="modalMoney" tabindex="-1" role="dialog" aria-labelledby="modalMoney">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="modalQrCodeShow">Trừ tiền khuyến mãi cho khách hàng</h4>
				</div>
				<form action="{!! route('minus_money') !!}" method="post">
					<div class="modal-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Số tiền sẽ trừ</label>
							<input type="number" min="0" max="{{ !empty($contact->money) ? $contact->money : 0 }}" class="form-control" name="money" placeholder="Số tiền sẽ trừ" required>
						</div>
					</div>
					<input type="hidden" value="{{ $contact->contact_id }}" name="contact_id"/>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
						<button type="submit" class="btn btn-success">Trừ tiền</button>
					</div>
				</form>

			</div>
		</div>
	</div>

	<script>
		$('#datepicker').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy'
		})
	</script>
@endsection

