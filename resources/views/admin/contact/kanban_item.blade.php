<div class="box box-primary kanbanConnect">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $title }}</h3>

        <div class="box-tools pull-right">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </div>
    </div>
    <div>
        <ul class="products-list product-list-in-box connectedSortable" id="sortable1">
            @foreach ($contacts as $contact)
                <li class="item itemEmployer">
                    <div class="product-img">
                        <img src="{{ !empty($contact->image) ? asset($contact->image) : '/public/library/images/favicon.png' }}" title="{{ !empty($employer->name) ? $employer->name : 'Chưa có người phụ trách' }}">
                    </div>
                    <div class="product-info">
                        <a href="{!! route('contact.edit',['employer_id' => $contact->contact_id]) !!}" class="product-title">{{ $contact->contact_id }}
                            <span class="label label-warning pull-right">
                                                         {{ 'không phụ trách' }}
                                                    </span>
                        </a>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-5">
                            <p style="font-size:12px;">Nguồn: đang cập nhật</p>
                        </div>
                        <div class="col-md-7">
                            <div style="float: right;">
                                <?php
                                $date = date_create($contact->created_at);
                                ?>
                                <p style="font-size:12px; margin-bottom: 0px;">Tạo ngày: {{ date_format($date, "d/m/Y H:i") }}</p>
                                <p style="font-size:12px; margin-bottom: 0px;">Lần cuối: 1</p>
                                <p class="floatRight" style="font-size:12px; margin-bottom: 0px; float: right;">
                                    <i class="fa fa-sticky-note-o" aria-hidden="true" title="trao đổi"></i> 0 |
                                    <i class="fa fa-phone" aria-hidden="true" title="Cuộc gọi"></i> 0
                                </p>
                            </div>
                        </div>
                    </div>
                    <div style="width: 100%">
                        <i class="fa fa-arrows-alt showEmployer" aria-hidden="true" data-toggle="collapse" href="#{!! $type !!}{{ $contact->contact_id }}" aria-expanded="false" aria-controls="collapseExample"></i>
                        <a href="{{ route('contact.create') }}?employer_id={!! $contact->contact_id !!}&user_id={!! \Illuminate\Support\Facades\Auth::user()->id !!}" class="showEmployer" >
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        </a>
                        <input type="checkbox" class="chkDelete" value="{!! $contact->contact_id !!}"onclick="return checkElement(this);">
                    </div>
                    <div class="collapse" id="{!! $type !!}{{ $contact->contact_id }}">
                        <div class="well" style="font-size: 12px;">
                            <!-- About Me Box -->
                            <strong><i class="fa fa-book margin-r-5"></i> Số điện thoại: {{ $contact->phone }}</strong>
                            <strong><i class="fa fa-map-marker margin-r-5"></i> Email: {{ $contact->email }}</strong>
                            <hr>
                            <div class="form-group">
                                <label>Trao đổi</label>
                                <textarea rows="4" class="form-control note-employer" name="note"
                                          placeholder="Ghi chú"></textarea>
                                <input type="hidden" value="{{ $contact->contact_id }}" class="employer_id"/>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-success" onclick="return note(this);">Ghi</button>
                            </div>
                            <div class="form-group noteContent">
                            </div>

                        </div>
                    </div>
                </li>
        @endforeach
            {{--@if ($countEmployer > 5)--}}
            {{--<li class="item itemEmployer">--}}
                {{--<p style="text-align: center; cursor: pointer;"><a onclick="return addItemKanban(this);" offset="5" limit="10" type="{!! $type !!}">Xem thêm</a></p>--}}
            {{--</li>--}}
            {{--@endif--}}
        <!-- /.item -->
        </ul>
    </div>
</div>
<!-- ket thuc nha tuyen dung moi -->

<script>
    {{--function note(e) {--}}
        {{--$.ajax({--}}
            {{--url: '{{ route('note-employer') }}',--}}
            {{--method: 'GET',--}}
            {{--data: {--}}
                {{--user_id: {{Illuminate\Support\Facades\Auth::user()->id}},--}}
                {{--employer_id: $(e).parent().prev().find('.employer_id').val(),--}}
                {{--content: $(e).parent().prev().find('.note-employer').val()--}}
            {{--},--}}
            {{--success: function (data) {--}}
                {{--$(e).parent().next().html(data);--}}
                {{--$(e).parent().prev().find('.note-employer').val('')--}}
            {{--}--}}
        {{--});--}}
    {{--}--}}

    {{--function addItemKanban(e) {--}}
        {{--$.ajax({--}}
            {{--url: '{{route('kanban_employer_load')}}',--}}
            {{--method: 'GET',--}}
            {{--data: {--}}
                {{--offset: $(e).attr('ofsset'),--}}
                {{--limit: $(e).attr('limit'),--}}
                {{--type: $(e).attr('type'),--}}
            {{--},--}}
            {{--success: function (data) {--}}
                {{--$(e).parent().parent().parent().append(data);--}}
                {{--$(e).parent().remove();--}}

            {{--}--}}
        {{--});--}}
    {{--}--}}
</script>
