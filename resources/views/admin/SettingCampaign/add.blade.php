@extends('admin.layout.admin')

@section('title', 'Thêm mới subcribe email')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới Chiến dịch 
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Chiến dịch </a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('addCampaign') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-8">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên chiến dịch </label>
                                <input type="text" class="form-control" name="name" placeholder="Tên chiến dịch" required>
                            </div>

                        <div class="form-group">
                           <label>Chọn group</label>
                            <select class="form-control" name="group">      
                                    <option value="" >Khách hàng mới</option>
                                    <option value="1" >Khách hàng Tiếp cận </option>
                                    <option value="2" >Khách hàng Mua hàng </option>
                                    <option value="3" >Khách hàng Hoàn đơn (Hoàn tiền)</option>       
                            </select>
                         </div>

                       <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả </label>
                            <textarea rows="4" class="form-control" name="description"></textarea>
                        </div>

                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>

            </form>
        </div>
    </section>
@endsection

