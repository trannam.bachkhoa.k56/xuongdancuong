@extends('admin.layout.admin') @section('title', 'Chỉnh sửa chiến dịch') 
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
            Chỉnh sửa Chiến dịch 
        </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Chiến dịch </a></li>
        <li class="active">{{$campaign->campaign_name}}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">

            <!-- form start -->
            <form role="form" class="update_campaign" action="{{ route('update_campaign',['id'=>$campaign->campaign_id]) }}" method="POST">
                {!! csrf_field() !!} {{ method_field('POST') }}

                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nội dung</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên chiến dịch </label>
                            <input type="text" class="form-control" name="name" placeholder="Tên chiến dịch" value="{{$campaign->campaign_name}}" required>
                        </div>
                         <div class="form-group">
                           <label>Chọn group</label>

                            <select id="select_group" class="form-control" name="group" disabled="disabled">      
                                    <option <?php if($campaign->group_id == null) { echo "selected";} ?> value="">Khách hàng mới</option>
                                    <option <?php if($campaign->group_id == 1) { echo "selected";} ?> value="1">Khách hàng Tiếp cận </option>
                                    <option <?php if($campaign->group_id == 2) { echo "selected";} ?> value="2">Khách hàng Mua hàng </option>
                                    <option <?php if($campaign->group_id == 3) { echo "selected";} ?> value="3">Khách hàng Hoàn đơn (Hoàn tiền)</option>       
                            </select>

                         </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả </label>
                            <textarea rows="4" class="form-control" name="description">{{$campaign->description}}</textarea>
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button id="update_campaign_id" type="submit" class="btn btn-primary">Chỉnh sửa</button>
                    </div>
                </div>

                <!-- /.box -->
            </form>
            <section class="content-header">
                <h1>
                            Đặt lịch gửi Email
                        </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active">Cấu hình khách hàng</li>
                </ol>
            </section>
            <br>

           <div class="box box-primary">
                 <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Automation</h3>
                    <div class="box-tools pull-right">
                        {{ $emails->links() }}
                    </div>
                </div>
                <style type="text/css">
                    .pagination {
                        margin: 0;
                    }
            </style>
              <!-- /.box-header -->
            <div class="box-body">
                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                <ul class="todo-list ui-sortable">
                    @foreach(App\Entity\MailAutomation::getAllWithId($campaign->campaign_id) as $id => $email)
                    <li class="" style="">
                        <!-- drag handle -->
                        <span class="handle ui-sortable-handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                                </span>
                        <span class="text">{{$id +1}} : 
                                   @if($email->type_id == 1)
                                    Email 
                                   @else
                                     Nhắc nhở {{$email->subject}}
                                   @endif 
                                   CSKH sau {{$email->time_delay}} Ngày </span>
                        <div class="tools">
                            <a style="cursor: pointer;font-size: 20px" title="Sửa">
                                <i class="fa fa-edit" data-toggle="collapse" data-target="#collapse{{$id}}" aria-expanded="false" aria-controls="collapse1"></i></a>
                            <a href="{{route('delete_mail_customer',['id' => $email->mail_id])}}"  style="cursor: pointer;font-size: 20px" title="Xóa">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>

                <div class="box-footer clearfix no-border">
                    <button type="button" class="btn btn-primary pull-left" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse"><i class="fa fa-plus"></i> Thêm automation</button>
                </div>
                <div class="collapse" id="collapse">

                    <form action="{{route('add_mail_customer')}}" method="POST">
                        {!! csrf_field() !!} {{ method_field('POST') }}
                        <div class="box-body">

                            <div class="form-group">
                                <label>Chọn dạng</label>
                                <select class="form-control" name="type">
                                    <option value="1">Email</option>
                                    <option value="2">Nhắc nhở</option>
                                </select>
                            </div>
                       
                            <input class="group_id" type="hidden" name="group" value="{{$campaign->group_id}}">
                            
                            
                            <input type="hidden" name="campaign" value="{{$campaign->campaign_id}}">

                      

                            <div class="form-group">
                                Chọn thời gian sau:
                                <input type="number" name="time_delay" class="" min="0" value="0">&emsp; ngày &emsp;

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text" name="subject" class="form-control" placeholder="Tiêu đề">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nội dung</label>
                                <textarea class="editor" id="content" name="content" rows="10" cols="80" /></textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary submit_form" id="step7">Thêm mới</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-md-4">
    <div class="box box-primary">
         @foreach(App\Entity\MailAutomation::getAllWithId($campaign->campaign_id) as $id => $email)
        <div class="collapse" id="collapse{{$id}}">
            <div class="box-header">
                <h4 style="font-weight: bold;">Sửa CSKH sau {{$email->time_delay}} Ngày </h4>
            </div>
            <a href="#" data-toggle="collapse" data-target="#collapse{{$id}}" style="font-size: 18px;color: #000;position: absolute; right: 20px;top: 20px" title="đóng">
                <i class="fa fa-times-circle" aria-hidden="true"></i>
            </a>
            <form action="{{route('update_mail_customer',['id' => $email->mail_id])}}" method="POST">
                {!! csrf_field() !!} {{ method_field('POST') }}
                <div class="box-body">
                    <div class="form-group">
                        <div class="form-group">
                            <label>Chọn dạng</label>
                            <select class="form-control" name="type" disabled="disabled">
                                <option value="1" <?php if($email->type_id == 1) { echo "selected";} ?>>Email</option>
                                <option value="2" <?php if($email->type_id == 2) { echo "selected";} ?>>Nhắc nhở</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Sau bao nhiêu Ngày</label>
                        <input type="number" name="time_delay" value="{{$email->time_delay}}" class="form-control" min="0">
                    </div>

                     <input class="group_id" type="hidden" name="group" value="{{$campaign->group_id}}">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Tiêu đề</label>
                        <input type="text" name="subject" class="form-control" value="{{$email->subject}}" placeholder="Tiêu đề">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nội dung</label>
                        <textarea class="editor" id="content{{$id}}" name="content" rows="10" cols="40" />{{$email->content}}</textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary submit_form" id="stepProductCreate7">Sửa</button>
                </div>
            </form>
        </div>
        @endforeach
    </div>
</div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#select_group').change(function() {
            /* Act on the event */
            var val = $(this).val();
            $('.group_id').val(val); 
            console.log($('.group_id').val());

        });
        
    });
</script>


@if (session('add'))
   <script type="text/javascript">
      alert ('{{ session('add') }}')
   </script>

@endif

@if (session('update'))
<script type="text/javascript">
      alert ('{{ session('update') }}')
   </script>
@endif

@if (session('remove'))
<script type="text/javascript">
      alert ('{{ session('remove') }}')
   </script>
@endif


<script type="text/javascript">
    
        $(".submit_form").click(function() {
            /* Act on the event */
            $(".update_campaign").submit();
        });
</script>

@endsection