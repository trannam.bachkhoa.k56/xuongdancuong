@extends('admin.layout.admin')

@section('title', 'Danh sách đăng ký nhận email' )

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
         Tạo mới chiến dịch chăm sóc khách hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Chiến dịch</a></li>
        </ol>
    </section>
    <section class="content">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" s>
            <li class="active"><a href="#email" aria-controls="email">Chiến dịch</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="email">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <a  href="{{ route('showAddCampaign') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="posts" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th>Tên chiến dịch</th>
                                        <th>Mô tả</th>
                                        <th>Thao tác</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="setting">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Cài đặt</h3>
                            </div>
                            <!-- /.box-header -->

                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th width="5%">STT</th>
                                        <th>Tên group</th>
                                        <th>Mô tả</th>
                                        <th>Thao tác</th>
                                    </tr>
                                    </thead>
                                   </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')

        @if (session('add'))
           <script type="text/javascript">
              alert ('{{ session('add') }}')
           </script>

        @endif

        @if (session('update'))
        <script type="text/javascript">
              alert ('{{ session('update') }}')
           </script>
        @endif

        @if (session('remove'))
        <script type="text/javascript">
              alert ('{{ session('remove') }}')
           </script>
        @endif

@endsection

@push('scripts')
<script>
    $(function() {
        $('#posts').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('datatable_campaign') !!}',
            columns: [
                { data: 'campaign_id', name: 'campaign_id' },
                { data: 'campaign_name', name: 'campaign_name' },
                { data: 'description', name: 'description' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    });
</script>
@endpush

