@extends('admin.layout.admin')

@section('title', 'Danh sách Quảng cáo ')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        	Quảng cáo 
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách Quảng cáo </a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                   
                  <!--<div class="box-header">
                        <a  href="{{ route('domains.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                    </div> -->
                    <!-- /.box-header -->
                    <div class="box-body" id="box">
                        <table id="advs" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tên bài viết</th>
                                <th>Tên khách hàng</th>
                                <th>Số điện thoại</th>
                                <th>Website</th>
                                <th>Message</th>
                                <th>Hình thức quảng cáo</th>
                                <th>View</th>
                                <th>Submit Form</th>
                                <th>Tổng tiền (VNĐ)</th>                                

                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th width="5%">ID</th>
                                <th>Tên bài viết</th>
                                <th>Tên khách hàng</th>
                                <th>Số điện thoại</th>
                                <th>Website</th>
                                <th>Message</th>
                                <th>Hình thức quảng cáo</th>
                                <th>View</th>
                                <th>Submit Form</th>
                                <th>Tổng tiền (VNĐ)</th>

                                <th>Thao tác</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('admin.partials.popup_delete')
@endsection

@push('scripts')
    <script>
        $(function() {
            var table = $('#advs').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('datatable_advertisement') !!}',
                columns: [
                    { data: 'adv_id', name: 'adv_id' },
                    { data: 'title', name: 'title' },
                    { data: 'name', name: 'name' },
                    { data: 'phone', name: 'phone' },
                    { data: 'website', name: 'website' },
                    { data: 'message', name: 'message' },
                    { data: 'method', name: 'method' },
                    { data: 'views', name: 'views' },                                     
                    { data: 'submit_form', name: 'submit_form' },
                    { data: 'total_money', render: $.fn.dataTable.render.number( '.')  },   
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                ]
            });
        });
    </script>
@endpush

