@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa ')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa quảng cáo bài viết  :  {{ $adv->title }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quảng cáo </a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('advertisement.update', ['id' => $adv->adv_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-6">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->


                        <div class="box-body">    

	                        <div class="form-group">
			                    <label>Tên bài viết </label>
			                    <input  class="form-control" value="{{$adv->title}}"  type="text"  disabled="disabled">
			                </div>  

			                <div class="form-group">
			                    <label>Họ Tên </label>
			                    <input  class="form-control" value="{{$adv->name}}"   type="text" name="name">
			                </div>
			                <div class="form-group">
			                    <label>Số điện thoại</label>
			                    <input class="form-control"  value="{{$adv->phone}}"  type="number" name="phone">
			                </div> 
			                 <div class="form-group">
			                    <label>Website</label>
			                    <input class="form-control" value="{{$adv->website}}"   type="text" name="website">
			                </div> 
			                 <div class="form-group">
			                    <label>Nhu cầu</label>
			                    <textarea class="form-control"  name="message" rows="5">{{$adv->message}}</textarea>  
			                </div> 
			                 <div class="form-group">
			                    <label>Hình thức quảng cáo </label>
			                    <input class="form-control" value="{{$adv->method}}"  type="text" name="method">
			                </div> 
                            <div class="form-group">
                                <label>Số tiền (VNĐ)</label>
                                <input class="form-control" value="{{($adv->total_money)}}"  type="text" name="total_money">
                            </div> 
			                         
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                
            </form>
        </div>
    </section>
@endsection

