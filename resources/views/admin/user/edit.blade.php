@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa '.$user->email )

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa Thông tin thành viên {{ $user->email }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cài đặt thông tin</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('users.update', ['id' => $user->id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-6">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phân quyền</label>
                                <select class="form-control" name="role">
                                    <option value="1" @if($user->role == 1) selected @endif>Thành viên</option>
                                    <option value="2" @if($user->role == 2) selected @endif>Biên tập viên</option>
                                    <option value="3" @if($user->role == 3) selected @endif>Quản trị</option>
                                </select>
                            </div>
							@endif	@if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                            <div class="form-group">
                                <label for="exampleInputEmail1">Quyền Active theme</label>
                                <select class="form-control" name="vip">
                                    <option value="0" @if($user->vip == 0) selected @endif>Active 1 theme</option>
                                    <option value="1" @if($user->vip == 1) selected @endif>Active 3 theme</option>
                                    <option value="2" @if($user->vip == 2) selected @endif>Active thoải mái</option>
                                </select>
                            </div>
                            @endif
                            @if(\App\Entity\User::isCreater(\Illuminate\Support\Facades\Auth::user()->role))
                            <div class="form-group">
                                <label for="exampleInputEmail1">Gói email</label>
                                <select class="form-control" name="package_mail">
                                    <option value="0"> ------ Chọn gói email ------ </option>
                                    <option value="1" {{ (int)$user->package_mail === 1 ? 'selected' : '' }}>Gói 1 ( 200k / 5000 mail )</option>
                                    <option value="2" {{ (int)$user->package_mail === 2 ? 'selected' : '' }}>Gói 2 ( 500k / 20000 mail )</option>
                                    
                                </select>
                            </div>  
                            <div class="form-group">
                                <label for="exampleInputEmail1">Thời gian </label>
                                 <select class="form-control" name="time_email">
                                    <option value="0"> ----- Chọn Thời gian ------- </option>                                  
                                    <option value="1_month">1 Tháng </option>
                                    <option value="6_month">6 Tháng </option>  
                                    <option value="1_year">1 Năm </option>                                    
                                    <option value="2_year">2 Năm  </option>                                    
                                    <option value="3_year">5 Năm </option>                                    
                                </select>
                            </div>                           
                            @endif


                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Email" required value="{{ $user->email }}" disabled />
                            </div>
							<input type="hidden" class="form-control" name="email" placeholder="Email" required value="{{ $user->email }}"  />
                            <div class="form-group">
                                <label for="exampleInputEmail1">Họ và tên</label>
                                <input type="text" class="form-control" name="name" placeholder="Họ và tên" value="{{ $user->name }}" />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Số điện thoại</label>
                                <input type="text" class="form-control" name="phone" placeholder="Số điện thoại" value="{{ $user->phone }}"/>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Lĩnh vực kinh doanh<span style="color:red;">(*)</span></label>

                                <select name="bussiness" class="form-control select2">
                                    <option value="quan-ao-thoi-trang" {!! $user->bussiness == "quan-ao-thoi-trang" ? 'selected' : '' !!}>Quần áo - thời trang</option>
                                    <option value="may-tinh-cong-nghe" {!! $user->bussiness == "may-tinh-cong-nghe" ? 'selected' : '' !!}>Máy tính - công nghệ</option>
                                    <option value="my-pham-lam-dep" {!! $user->bussiness == "my-pham-lam-dep" ? 'selected' : '' !!}>Mỹ phẩm - làm đẹp</option>
                                    <option value="san-pham-giai-tri" {!! $user->bussiness == "san-pham-giai-tri" ? 'selected' : '' !!}>Sản phẩm giải trí</option>
                                    <option value="cay-canh" {!! $user->bussiness == "cay-canh" ? 'selected' : '' !!}>Cây cảnh</option>
                                    <option value="trang-tri-nha-cua" {!! $user->bussiness == "trang-tri-nha-cua" ? 'selected' : '' !!}>Trang trí nhà cửa</option>
                                    <option value="dien-thoai" {!! $user->bussiness == "dien-thoai" ? 'selected' : '' !!} >Điện thoại</option>
                                    <option value="dien-tu-gia-dung" {!! $user->bussiness == "dien-tu-gia-dung" ? 'selected' : '' !!}>Điện tử - gia dụng</option>
                                    <option value="me-be" {!! $user->bussiness == "me-be" ? 'selected' : '' !!}>Mẹ - bé</option>
                                    <option value="noi-that" {!! $user->bussiness == "noi-that" ? 'selected' : '' !!}>Nội thất</option>
                                    <option value="am-thuc-nha-hang" {!! $user->bussiness == "am-thuc-nha-hang" ? 'selected' : '' !!}>Ẩm thực - nhà hàng</option>
                                    <option value="do-the-thao" {!! $user->bussiness == "do-the-thao" ? 'selected' : '' !!}>Đồ thể thao</option>
                                    <option value="thiet-bi-van-phong" {!! $user->bussiness == "thiet-bi-van-phong" ? 'selected' : '' !!}>Thiết bị văn phòng</option>
                                    <option value="thiet-bi-dien" {!! $user->bussiness == "thiet-bi-dien" ? 'selected' : '' !!}>Thiết bị điện</option>
                                    <option value="hoa-qua-tang" {!! $user->bussiness == "hoa-qua-tang" ? 'selected' : '' !!}>Hoa - Quà tặng</option>
                                    <option value="giao-duc" {!! $user->bussiness == "giao-duc" ? 'selected' : '' !!}>Giáo dục</option>
                                    <option value="khach-san-du-lich" {!! $user->bussiness == "khach-san-du-lich" ? 'selected' : '' !!}>Khách sạn và du lịch</option>
                                    <option value="nong-nghiep-thuc-pham" {!! $user->bussiness == "nong-nghiep-thuc-pham" ? 'selected' : '' !!}>Nông nghiệp - thực phẩm</option>
                                    <option value="may-cong-nghiep" {!! $user->bussiness == "may-cong-nghiep" ? 'selected' : '' !!}>Máy công nghiệp</option>
                                    <option value="bat-dong-san" {!! $user->bussiness == "bat-dong-san" ? 'selected' : '' !!}>Bất động sản</option>
                                    <option value="vat-nuoi-thu-cung" {!! $user->bussiness == "vat-nuoi-thu-cung" ? 'selected' : '' !!}>Vật nuôi - thú cưng</option>
                                    <option value="co-quan-to-chuc" {!! $user->bussiness == "co-quan-to-chuc" ? 'selected' : '' !!}>Cơ quan - tổ chức</option>
                                    <option value="y-te-cham-soc-suc-khoe" {!! $user->bussiness == "y-te-cham-soc-suc-khoe" ? 'selected' : '' !!}>Y tế - chăm sóc sức khỏe</option>
                                    <option value="oto-xe-may" {!! $user->bussiness == "oto-xe-may" ? 'selected' : '' !!}>Oto - xe máy</option>
                                    <option value="hoi-thao-su-kien" {!! $user->bussiness == "hoi-thao-su-kien" ? 'selected' : '' !!}>Hội thảo và sự kiện</option>
                                    <option value="tiec-cuoi-hoi-nghi" {!! $user->bussiness == "tiec-cuoi-hoi-nghi" ? 'selected' : '' !!} >Tiệc cưới - hội nghị</option>
                                    <option value="tu-van-dich-vu" {!! $user->bussiness == "tu-van-dich-vu" ? 'selected' : '' !!}>Tư vấn dịch vụ</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="checkbox" name="is_change_password" value="1" class="flat-red"> Chọn nếu muốn thay đổi mật khẩu
                                <label for="exampleInputEmail1">Mật khẩu</label>
                                <input type="password" class="form-control" name="password" placeholder="Mật khẩu" value="{{ $user->password }}" />
                            </div>

                            <div class="form-group">
                                <input type="button" onclick="return uploadImage(this);" value="Chọn ảnh"
                                       size="20"/>
                                <img src="{{ $user->image }}" width="80" height="70"/>
                                <input name="image" type="hidden" value="{{ $user->image }}"/>
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('email'))
                                    <label for="exampleInputEmail1">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                
            </form>
        </div>
    </section>
@endsection

