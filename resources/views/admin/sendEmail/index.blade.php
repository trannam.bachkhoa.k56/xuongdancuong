@extends('admin.layout.admin')

@section('title', 'Gửi email')

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Gửi email
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Gửi email</li>
	</ol>

</section>
<section class="content">
	<div class="row">
		<div class="col-md-8 col-xs-12">
			<ul  class="nav nav-pills">
				<li class="active">
					<a  href="#forGroup" data-toggle="tab" style="text-transform: uppercase;">Gửi Mail theo nhóm khách hàng</a>
				</li>
				<li>
					<a href="#forTag" data-toggle="tab" style="text-transform: uppercase;">Gửi mail theo Tag</a>
				</li>
				<li>
					<a href="#forCampaign" data-toggle="tab" style="text-transform: uppercase;">Gửi mail theo Chiến dịch </a>
				</li>

			</ul>
			<div class="tab-content">
				<div class="box box-info tab-pane fade in active" id="forGroup">
					<div class="box-header">
						<i class="fa fa-envelope"></i>
						<h3 class="box-title">Gửi mail theo nhóm khách hàng</h3>
					</div>
					<form action="{{ route('subcribe-email_send') }}" method="post">
						{!! csrf_field() !!}
						<div class="box-body">
							<div class="form-group">
								<label>Chọn group</label>
								<select class="form-control" name="group">
									<option value="">Khách hàng mới</option>
									<option value="1">Khách hàng Tiếp cận </option>
									<option value="2">Khách hàng Mua hàng </option>
									<option value="3">Khách hàng Hoàn đơn (Hoàn tiền)</option>
								</select>
							</div>
							<div class="form-group">
								<label>Chủ đề</label>
								<input type="text" class="form-control" name="subject" placeholder="Chủ đề">
							</div>
							<div>
								<label>Nội dung</label>
								<textarea class="editor" id="content" name="content" rows="10" cols="80" placeholder="Nội dung"/></textarea>
							</div>

						</div>
						<div class="box-footer clearfix">
							<button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
								<i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</form>
				</div>
				<div class="box box-info tab-pane fade" id="forTag">
					<div class="box-header">
						<i class="fa fa-envelope"></i>
						<h3 class="box-title">Gửi mail theo Tag khách hàng</h3>
					</div>
					<form action="{{ route('send_for_tag') }}" method="post">
						{!! csrf_field() !!}
						<div class="box-body">

							<div class="form-group">
								<label>Chọn Tag</label>
								<input type="text" class="form-control" name="tag" onkeyup="suggestionInput(this);" placeholder="Tìm kiếm theo tag">
							</div>
							<div class="form-group">
								<label>Chủ đề</label>
								<input type="text" class="form-control" name="subject" placeholder="Chủ đề">
							</div>
							<div>
								<label>Nội dung</label>
								<textarea class="editor" id="content1" name="content" rows="10" cols="80" placeholder="Nội dung"/></textarea>
							</div>

						</div>
						<div class="box-footer clearfix">
							<button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
								<i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</form>
				</div>

				<div class="box box-info tab-pane fade" id="forCampaign">
					<div class="box-header">
						<i class="fa fa-envelope"></i>
						<h3 class="box-title">Gửi mail theo Chiến dịch </h3>
					</div>

					<form action="{{ route('send_for_campaign') }}" method="post">
						{!! csrf_field() !!}
						<div class="box-body">
							<div class="form-group">
								<label>Chọn Chiến dịch </label>
								<select onchange="return getCampaignId(this)" class="form-control" name="campaign" id="campaign">
									<option value="">--- Chọn chiến dịch ---</option>
									@foreach($campaigns as $campaign)
										<option  value="{{$campaign->campaign_customer_id}}">{{$campaign->campaign_title}}</option>
									@endforeach
								</select>
							</div>

							<div class="form-group" id="showGroup" >
								<label>Chọn group Trong chiến dịch </label>
								<select class="form-control" name="process" id="group">
									<option value="">-- Nhóm khách hàng --</option>
								</select>
							</div>

							<div class="form-group">
								<label>Chủ đề</label>
								<input type="text" class="form-control" name="subject" placeholder="Chủ đề">
							</div>
							<div>
								<label>Nội dung</label>
								<textarea class="editor" id="content2" name="content" rows="10" cols="80" placeholder="Nội dung"/></textarea>
							</div>

						</div>
						<div class="box-footer clearfix">
							<button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
								<i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</form>
				</div>
			</div>

			<script>
				function suggestionInput(e) {
					$.ajax({
						url: '{{ route('show-tag-customer') }}',
						type: 'GET',
						data:{
							tag: $(e).val()
						},
						success: function (result) {
							$(e).autocomplete({
								source: result,
								appendTo: "#someElem"
							});
						},
						error: function (error) {
							console.log(error);
						}
					});
				}

				function getCampaignId(d){
					var campaign_id = $(d).children('option:selected').val();
					$.ajax({
								url: '{{route("get_opportunity")}}',
								type: 'POST',
								data: {
									campaign_id: campaign_id ,
								},
							})
							.done(function(data) {
								console.log("success");
								$('#group').remove();
								$('#showGroup').append(data);

							})
							.fail(function() {
								console.log("error");
							})
							.always(function() {
								console.log("complete");
							});

				}
			</script>

		</div>
		<div class="col-md-4">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title" style="padding-bottom: 20px"><i class="fa fa-address-card-o" aria-hidden="true"></i>  Chọn mẫu email có sẵn</h3>
					<ul  class="nav nav-pills">
						<li class="active">
							<a  href="#getNewCustomer" data-toggle="tab" style="text-transform: uppercase;">Khách hàng mới</a>
						</li>

						<li class="">
							<a  href="#getApproachCustomer" data-toggle="tab" style="text-transform: uppercase;">Kh Tiếp Cận </a>
						</li>

						<li class="">
							<a  href="#getBuyCustomer" data-toggle="tab" style="text-transform: uppercase;">Kh Mua Hàng</a>
						</li>


						<li class="">
							<a  href="#getReturnCustomer" data-toggle="tab" style="text-transform: uppercase;">Kh Hoàn đơn </a>
						</li>
					</ul>
				</div>
				<div class="tab-content">
					<div class="box-body tab-pane fade in active" id="getNewCustomer">
						@foreach( App\Entity\EmailTemplate::getNewCustomer() as $email )
							<div class="radioholder" onclick="showlist(this);" >
								<input type="radio"name="select_template" value="{{$email->template_id}}">
								<label for="pt_illustration">{{$email->template_title}}</label>
							</div>
						@endforeach
					</div>

					<div class="box-body tab-pane fade" id="getApproachCustomer">
						@foreach( App\Entity\EmailTemplate::getApproachCustomer() as $email1 )

							<div class="radioholder" onclick="showlist(this);" >
								<input type="radio"name="select_template" value="{{$email1->template_id}}">
								<label for="pt_illustration">{{$email1->template_title}}</label>
							</div>
						@endforeach
					</div>

					<div class="box-body tab-pane fade" id="getBuyCustomer">
						@foreach( App\Entity\EmailTemplate::getBuyCustomer() as $email2 )
							<div class="radioholder" onclick="showlist(this);" >
								<input type="radio"name="select_template" value="{{$email2->template_id}}">
								<label for="pt_illustration">{{$email2->template_title}}</label>
							</div>
						@endforeach
					</div>

					<div class="box-body tab-pane fade" id="getReturnCustomer">
						@foreach( App\Entity\EmailTemplate::getReturnCustomer() as $email3 )
							<div class="radioholder" onclick="showlist(this);" >
								<input type="radio"name="select_template" value="{{$email3->template_id}}">
								<label for="pt_illustration">{{$email3->template_title}}</label>
							</div>
						@endforeach
					</div>

				</div>

			</div>
			<script type="text/javascript">

				function showTemplate(){
					$('#template_list').toggle();
				}

				function showlist(e){
					var id = $(e).find('input').val();
					var content = $('textarea#content');
					var content1 = $('textarea#content1');

					$.ajax({
								url: '{{route("get_template")}}',
								type: 'GET',
								data: {id : id},
							})
							.done(function(data) {
								console.log("success");
								CKEDITOR.instances.content.setData(data);
								CKEDITOR.instances.content1.setData(data);

							})
							.fail(function() {
								console.log("error");
							})
							.always(function() {
								console.log("complete");
							});

				}
			</script>

			<style type="text/css">

				.radioholder {
					width: 100%;
					background: #fff;
					margin-bottom: 1em;
					font-size: 1.3em;
					height: 2.4em;
					color: #666;
					-o-transition: .1s ease-out;
					-ms-transition: .1s ease-out;
					-moz-transition: .1s ease-out;
					-webkit-transition: .1s ease-out;
					transition: .1s ease-out;
					cursor: pointer;
				}

				.activeradioholder {
					background: #39A9A4;
					color: #fff;
				}

				.radioholder .desc {
					display: inline-block;
					vertical-align: middle;
					padding-left: .6em;
					line-height: 2.4em;
				}

				.radioholder .tick {
					display: inline-block;
					vertical-align: middle;
					width: 2.4em;
					height: 100%;
					background-color: #eee;
					background-image: none;
					-o-transition: .1s ease-out;
					-ms-transition: .1s ease-out;
					-moz-transition: .1s ease-out;
					-webkit-transition: .1s ease-out;
					transition: .1s ease-out;
				}

				.activeradioholder .tick {
					background-color: #2A817C;
					background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAJFBMVEX////////////////////////////////////////////////Vd7HLAAAAC3RSTlMAESIzRGaZu8zd7m0smEwAAAGRSURBVHja7dtLioQwAADRsW0dP/e/7zCrgl4IgtCWVJ0gLxBCSPJTVVVVVVVVVVVVVVVVVVVVVVVVVVWfzcvwDMe+I3E7kLgdSNwOJG4HErsDidvhlwy/GMySYUGAxO5AYncgsTuQ2B1I5A4kL7eDttHuQGJ3ILE7kNgdSOwOJHYHErkDidtBU44cOXLkyPHf6yGOcburY3iKY5nlDlYuErcDiduBxO1Acq1je3/BgeQ6x/gFBxKzA4ndgUTuoGWwO5DYHUjsDiR2BxKDg5PRkUTgYGSHErsDid2BxO5AYncgsTuQ2B1Ipv1mDjo1w+vtHDTt14fDLcHhluCwS3D4JSsOqYQdxy3B4ZbgcEtwuCU4BBKBA4nWgcTuQGJ3IJE76L3ZHZwY7Q4kdgcSuwOJ3YHE7kBidyCxO5DYHUjsDiR2BxK74/z9j18yCxRIzA4kdgcSuwOJ3YHE70DidyA57xBIcFglOKySVes4ftfsluCQS3DYJTjsEr+DH0tVVVVVVVVVVVVVVVVVVVVVVVXX9Aekh8txA/HicgAAAABJRU5ErkJggg==);
					background-size: cover;
				}

				.radioholder:nth-child(odd) {
					float: right;
				}

				.radioholder:nth-child(even) {
					float: left;
				}

				.radioholder:hover {
					background-color: #eee;
				}

				.radioholder:hover .tick {
					background-color: #ddd;
				}

				.activeradioholder:hover {
					background-color: #39A9A4;
				}

				.activeradioholder:hover .tick {
					background-color: #2A817C;
				}

				.selectholder {
					clear: both;
					width: 100%;
					background: #eee;
					margin-bottom: 1em;
					font-size: 1.3em;
					height: 2.4em;
					color: #666;
					-o-transition: .1s ease-out;
					-ms-transition: .1s ease-out;
					-moz-transition: .1s ease-out;
					-webkit-transition: .1s ease-out;
					transition: .1s ease-out;
					cursor: pointer;
				}

				.selectholder .desc {
					display: inline-block;
					vertical-align: middle;
					padding-left: .8em;
					line-height: 2.4em;
				}
			</style>

			<script>
				$(document).ready(function(){
					$('.radioholder').each(function(){
						$(this).children().hide();
						var description = $(this).children('label').html();
						$(this).append('<span class="desc">'+description+'</span>');
						$(this).prepend('<span class="tick"></span>');
						$(this).click(function(){
							$(this).children('input').prop('checked', true);
							$(this).children('input').trigger('change');
						});
					});
					$('input[type=radio]').change(function(){
						$('input[type=radio]').each(function(){
							if($(this).prop('checked') == true) {
								$(this).parent().addClass('activeradioholder');
							}
							else $(this).parent().removeClass('activeradioholder');
						});
					});
					$('input[type=radio]').change();

					$('.selectholder').each(function(){
						$(this).children().hide();
						var description = $(this).children('label').text();
						$(this).append('<span class="desc">'+description+'</span>');
						$(this).append('<span class="pulldown"></span>');
						$(this).append('<div class="selectdropdown"></div>');
						$(this).children('select').children('option').each(function(){
							if($(this).attr('value') != '0') {
								$drop = $(this).parent().siblings('.selectdropdown');
								var name = $(this).attr('value');
								$drop.append('<span>'+name+'</span>');
							}
						});
						$(this).click(function(){
							if($(this).hasClass('activeselectholder')) {
								$(this).children('.selectdropdown').slideUp(200);
								$(this).removeClass('activeselectholder');
								if($(this).children('select').val() != '0') {
									$(this).children('.desc').fadeOut(100, function(){
										$(this).text($(this).siblings("select").val());
										$(this).fadeIn(100);
									});
								}
							}
							else {
								$('.activeselectholder').each(function(){
									$(this).children('.selectdropdown').slideUp(200);
									if($(this).children('select').val() != '0') {
										$(this).children('.desc').fadeOut(100, function(){
											$(this).text($(this).siblings("select").val());
											$(this).fadeIn(100);
										});
									}
									$(this).removeClass('activeselectholder');
								});
								$(this).children('.selectdropdown').slideDown(200);
								$(this).addClass('activeselectholder');
								if($(this).children('select').val() != '0') {
									$(this).children('.desc').fadeOut(100, function(){
										$(this).text($(this).siblings("select").children("option[value=0]").text());
										$(this).fadeIn(100);
									});
								}
							}
						});
					});
					$('.selectholder .selectdropdown span').click(function(){
						$(this).siblings().removeClass('active');
						$(this).addClass('active');
						var value = $(this).text();
						$(this).parent().siblings('select').val(value);
						$(this).parent().siblings('.desc').fadeOut(100, function(){
							$(this).text(value);
							$(this).fadeIn(100);
						});
					});

				});
			</script>
		</div>
	</div>
</section>
@endsection

