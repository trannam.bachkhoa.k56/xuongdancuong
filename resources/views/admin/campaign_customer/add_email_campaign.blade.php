@extends('admin.layout.admin')
@section('title', 'Thêm chiến dịch') 
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
            Thêm mới Email CSKH cho {{$campaign->campaign_title}}
        </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Chiến dịch </a></li>
        <li class="active">{{$campaign->campaign_title}}</li>
    </ol>
</section>



<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-8">

           <div class="box box-primary">
                 <div class="box-header ui-sortable-handle" style="cursor: move;">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Automation</h3>
                    <div class="box-tools pull-right">
                        {{ $emails->links() }}
                    </div>
                </div>
                <style type="text/css">
                    .pagination {
                        margin: 0;
                    }
            </style>
              <!-- /.box-header -->
            <div class="box-body">
                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                <ul class="todo-list ui-sortable">
                    @foreach(App\Entity\MailAutomation::getAllWithCampaignId($campaign->campaign_customer_id) as $id => $email)
                    <li class="" style="">
                        <!-- drag handle -->
                        <span class="handle ui-sortable-handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                                </span>
                        <span class="text">{{$id +1}} : 
                                   @if($email->type_id == 1)
                                    Email 
                                   @else
                                     Nhắc nhở {{$email->subject}}
                                   @endif 
                                   CSKH sau {{$email->time_delay}} Ngày
                        </span>
                        <span>
                           Trạng thái: {{$email->process_name}}
                        </span>
                        <div class="tools">
                            <a style="cursor: pointer;font-size: 20px" title="Sửa">
                                <i class="fa fa-edit" data-toggle="collapse" data-target="#collapse{{$id}}" aria-expanded="false" aria-controls="collapse1"></i></a>
                            <a href="{{route('delete_mail_customer',['id' => $email->mail_id])}}"  style="cursor: pointer;font-size: 20px" title="Xóa">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>

                <div class="box-footer clearfix no-border">
                    <button type="button" class="btn btn-primary pull-left" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse"><i class="fa fa-plus"></i> Thêm automation</button>
                </div>
                <div class="collapse" id="collapse">
                    <form action="{{route('add_mail_customer')}}" method="POST">
                        {!! csrf_field() !!} {{ method_field('POST') }}
                        <div class="box-body">

                            <div class="form-group">
                                <label>Chọn dạng</label>
                                <select class="form-control" name="type">
                                    <option value="1">Email</option>
                                    <option value="2">Nhắc nhở</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Tiến trình khách hàng</label>
                                <select class="form-control" name="process">
                                    <option value="">--Chọn tiến trình--</option>

                                    @foreach( $proceses as $process )
                                    <option value="{{$process->process_id}}">{{$process->process_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <input class="campaign" type="hidden" name="campaign_customer_id" value="{{$campaign->campaign_customer_id}}">
                        

                            <div class="form-group">
                                Chọn thời gian sau:
                                <input type="number" name="time_delay" class="" min="0" value="0">&emsp; ngày &emsp;

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text" name="subject" class="form-control" placeholder="Tiêu đề">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nội dung</label>
                                <textarea class="editor" id="content" name="content" rows="10" cols="80" /></textarea>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary submit_form" id="step7">Thêm mới</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-md-4">

    <div class="box box-primary">
         @foreach(App\Entity\MailAutomation::getAllWithCampaignId($campaign->campaign_customer_id) as $id => $email)
        <div class="collapse" id="collapse{{$id}}">
            <div class="box-header">
                <h4 style="font-weight: bold;">Sửa CSKH sau {{$email->time_delay}} Ngày </h4>
            </div>
            <a href="#" data-toggle="collapse" data-target="#collapse{{$id}}" style="font-size: 18px;color: #000;position: absolute; right: 20px;top: 20px" title="đóng">
                <i class="fa fa-times-circle" aria-hidden="true"></i>
            </a>
            <form action="{{route('update_mail_customer',['id' => $email->mail_id])}}" method="POST">
                {!! csrf_field() !!} {{ method_field('POST') }}
                 <div class="box-body">
                     <div class="form-group">
                        <div class="form-group">
                            <label>Chọn dạng</label>
                            <select class="form-control" name="type" disabled="disabled">
                                <option value="1" <?php if($email->type_id == 1) { echo "selected";} ?>>Email</option>
                                <option value="2" <?php if($email->type_id == 2) { echo "selected";} ?>>Nhắc nhở</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Tiến trình khách hàng</label>
                        <select class="form-control" name="process" disabled="disabled">
                            <option value="">--Chọn tiến trình--</option>

                            @foreach( $proceses as $process )
                            <option value="{{$process->process_id}}" <?php if($email->process_id == $process->process_id){ echo 'selected'; } ?> >{{$process->process_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <input class="campaign" type="hidden" name="campaign_customer_id" value="{{$campaign->campaign_customer_id}}">
                

                    <div class="form-group">
                        Chọn thời gian sau:
                        <input type="number" name="time_delay" value="{{$email->time_delay}}" class="form-control" min="0">

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tiêu đề</label>
                       <input type="text" name="subject" class="form-control" value="{{$email->subject}}" placeholder="Tiêu đề">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nội dung</label>
                      <textarea class="editor" id="content{{$id}}" name="content" rows="10" cols="40" />{{$email->content}}</textarea>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary submit_form" id="step7">Thêm mới</button>
                </div>
            </form>
        </div>
        @endforeach
    </div>
</div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#select_group').change(function() {
            /* Act on the event */
            var val = $(this).val();
            $('.group_id').val(val); 
            console.log($('.group_id').val());

        });
        
    });
</script>


@if (session('add'))
   <script type="text/javascript">
      alert ('{{ session('add') }}')
   </script>

@endif

@if (session('update'))
<script type="text/javascript">
      alert ('{{ session('update') }}')
   </script>
@endif

@if (session('remove'))
<script type="text/javascript">
      alert ('{{ session('remove') }}')
   </script>
@endif


<script type="text/javascript">
    
        $(".submit_form").click(function() {
            /* Act on the event */
            $(".update_campaign").submit();
        });
</script>

@endsection