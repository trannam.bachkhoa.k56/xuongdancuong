@extends('admin.layout.admin')

@section('title', 'Danh sách chiến dịch')

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Cơ hội
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Danh sách chiến dịch</a></li>
	</ol>
</section>
<section class="content">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#home">Chiến dịch của tôi </a></li>
		<li><a data-toggle="tab" href="#menu1"> Chiến dịch đã tham gia </a></li>
	</ul>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<a href="{{ route('campaign-customer.create') }}" >
								<button class="btn btn-primary" id="stepProductGeneral3" data-step="1" data-intro="Ấn vào đây nếu bạn muốn thêm mới , ...">Thêm mới chiến dịch</button></a>
						</div>
					</div>
					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<div class="box-body">
								<table id="camnpaign_customers" class="table table-bordered table-striped">
									<thead>
									<tr>
										<th>ID</th>
										<th>Tên chiến dịch</th>
										<th width="20%">Người khởi tạo</th>
										<th>Thời gian khởi tạo</th>
										<th>Thao tác</th>
									</tr>
									</thead>

								</table>

							</div>

						</div>

						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<script type="text/javascript">
					document.jQuery(document).ready(function($) {

					});
				</script>
			</div>
		</div>
</section>
@include('admin.partials.popup_delete')
<section class="content">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			@include('admin.partials.form_support_marketing')
		</div>
	</div>
</section>

@endsection
@push('scripts')
<script>
	$(function() {
		$('#camnpaign_customers').DataTable({
			processing: true,
			serverSide: true,
			responsive: true,
			type: "POST",
			ajax: '{!! route('databtable_campaign_customer') !!}',
			columns: [
				{ data: 'campaign_customer_id', name: 'campaign_customer.campaign_customer_id' },
				{ data: 'campaign_title', name: 'campaign_customer.campaign_title' },
				{ data: 'name', name: 'campaign_customer.name' },
				{ data: 'created_at', name: 'campaign_customer.created_at' },
				{ data: 'action', name: 'action', orderable: false, searchable: false },
			]
		});
	});


</script>


@endpush
