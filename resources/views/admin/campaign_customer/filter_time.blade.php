  <div class="kanbans">
    <div id="myKanban">
        
    </div>
    <script type="text/javascript">                    
        function minusCountContact(status) {
            var countContact = $("#status"+status).html();
            /* trừ đi 1 */
            countContact = parseInt(countContact) - 1;
            $("#status"+status).html(countContact);
        }

        function addCountContact(status) {
            var countContact = $("#status"+status).html();
            /* trừ đi 1*/
            countContact = parseInt(countContact) + 1;
            $("#status"+status).html(countContact);
        }

        var KanbanTest = new jKanban({
            element: "#myKanban",
            gutter: "10px",
            widthBoard: "200px",
            click: function(el) {
                console.log("Trigger on all items click!");
            },
            buttonClick: function(el, boardId) {

            },
            addItemButton: false,
            boards: [
             @foreach($proceses as $process)
                {
                    id: "{{$process->process_id}}",
                    title: '<div style="color: #fff;">{{$process->process_name}}</div> ',
                    class: "info,good",
                    item: [
                        @foreach ($contacts as $contact)
                            @if ($contact->campaign_id >= 0)
                                @if($contact->campaign_status == $process->process_id)
                                {
                                    id: "{{ $contact->contact_id }}",
                                    title: '<p style="color:{{isset($contact->color) ? $contact->color : "#000"}}">{{ $contact->name }}-{{ $contact->phone }}- {{ $contact->email }}</p><div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
                                    drag: function(el, source) {
                                        var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                        minusCountContact(status);
                                    },
                                    dragend: function(el) {
                                        var contactId = el.dataset.eid;
                                        var status = KanbanTest.getParentBoardID(el.dataset.eid);
                                        addCountContact(status);
                                        callUpdateStatus (contactId, status)
                                    },

                                    click: function (el) {
                                        window.location.replace("{!! route("contact.show", ["contact_id" => $contact->contact_id]) !!}");
                                    }
                                },
                                @endif

                            @endif
                        @endforeach
                    ]
                },
                @endforeach
            ]
        });

    </script>
</div>
