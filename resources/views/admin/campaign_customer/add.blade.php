@extends('admin.layout.admin')

@section('title', 'Thêm mới Khách hàng')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới Chiến dịch 
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Sản phẩm</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-primary boxCateScoll">
            <form role="form" action="{{ route('campaign-customer.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
            <div class="box-body">      
                <div class="row">
                    <div class="col-xs-8 col-md-8">
                       
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên Chiến dịch</label>
                                    <input type="text" class="form-control" name="campaign_title" placeholder="Tên Chiến dịch" required id="stepProductCreate1">
                                </div>   

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Người phụ trách</label>
                                    <select class="form-control" name="manager_id">
                                        @foreach($managers as $manager)
                                             <option value="{{$manager->id}}">
                                               {{$manager->name}} -
                                               @if($manager->role == 1)
                                                   <b>Thành viên</b>
                                               @elseif($manager->role == 2)
                                                    <b>Biên tập viên</b>
                                               @elseif($manager->role == 3)
                                                   <b> Quản trị</b>
                                               @endif
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Thành viên Tham gia</label>
                                    <select class="form-control select2" name="employees[]" multiple="multiple" >
                                        @foreach($employees as $employee)
                                            <option value="{{$employee->id}}">
                                               {{$employee->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" id="stepProductCreate7">Thêm mới</button>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-4">                    
                        <label for="exampleInputEmail1">Các Hành trình khách hàng</label> 

                        <ul class="list-group" id="process">  
                            <li class="list-group-item list-group-item-action" style="position: relative;">

                                <input type="text" class="form-control hideInput"  disabled="true" value="Khách hàng Mới">
                                <input type="text" class="form-control showInput" onkeyup="return changeValue(this)" name="process[]" value="Khách hàng Mới"> 

                                <span>
                                <a class="shows" onclick="return showInput(this);">              
                                    <i class="fa fa-pencil-square-o " aria-hidden="true" ></i>
                                </a>
                                <a class="hides" style="display: none;" onclick="return hideInput(this);"> 
                                    <i class="fa fa-check " aria-hidden="true" ></i>
                                </a>
                                <a onclick="return removeInput(this);">
                                    <i class="fa fa-trash remove" aria-hidden="true"></i>
                                </a>
                                </span>
                            </li>

                            <li class="list-group-item list-group-item-action">
                                <input type="text" class="form-control hideInput" disabled="true" value="Khách hàng Tiếp cận">
                                <input type="text" class="form-control showInput" onkeyup="return changeValue(this)" name="process[]" value="Khách hàng Tiếp cận">

                                <span >
                                <a class="shows" onclick="return showInput(this);">              
                                    <i class="fa fa-pencil-square-o " aria-hidden="true" ></i>
                                </a>
                                <a class="hides" style="display: none;" onclick="return hideInput(this);"> 
                                    <i class="fa fa-check " aria-hidden="true" ></i>
                                </a>
                                <a onclick="return removeInput(this);">
                                    <i class="fa fa-trash remove" aria-hidden="true"></i>
                                </a>
                                </span>
                            </li>

                            <li class="list-group-item list-group-item-action">
                                <input type="text" class="form-control hideInput" disabled="true" value="Khách hàng Mua hàng"> 
                                <input type="text" class="form-control showInput" onkeyup="return changeValue(this)" name="process[]" value="Khách hàng Mua hàng">

                                <span >
                                <a class="shows" onclick="return showInput(this);">              
                                    <i class="fa fa-pencil-square-o " aria-hidden="true" ></i>
                                </a>
                                <a class="hides" style="display: none;" onclick="return hideInput(this);"> 
                                    <i class="fa fa-check " aria-hidden="true" ></i>
                                </a>
                                <a onclick="return removeInput(this);">
                                    <i class="fa fa-trash remove" aria-hidden="true"></i>
                                </a>
                                </span>
                            </li>

                            <li class="list-group-item list-group-item-action">
                                <input type="text" class="form-control hideInput" disabled="true" value="Khách hàng Hoàn tiền"> 
                                <input type="text" class="form-control showInput" onkeyup="return changeValue(this)" name="process[]" value="Khách hàng Hoàn tiền"> 

                                <span>
                                <a class="shows" onclick="return showInput(this);">              
                                    <i class="fa fa-pencil-square-o " aria-hidden="true" ></i>
                                </a>
                                <a class="hides" style="display: none;" onclick="return hideInput(this);"> 
                                    <i class="fa fa-check " aria-hidden="true" ></i>
                                </a>
                                <a onclick="return removeInput(this);">
                                    <i class="fa fa-trash remove" aria-hidden="true"></i>
                                </a>
                                </span>
                            </li>                                                  
                        </ul>                    
                        
                        <div class="form-group">
                            <input type="text" id="valProcess" class="form-control" placeholder="Thêm mới hành trình khách hàng!"> 
                            <a style="margin-top: 10px" onclick ="addProcess()" class="btn btn-success">Thêm </a> 
                        </div>

                    </div> 
                </div>
        </form>
    </section>
    <style type="text/css">
        .list-group .list-group-item .showInput{
            display: none;
        }
        .list-group .list-group-item{
            margin: 5px;
            border-radius: unset;
            position: relative;
        }
        .list-group .list-group-item span{
            position: absolute; right: 30px;top:30%;font-size: 16px ;
        }
        .list-group .list-group-item input{
            padding-right:50px; 
        }
    </style>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });

        function changeValue(e) {
            var valChange = $(e).val();
            $(e).parent().find('.hideInput').val(valChange);

        }

        function showInput(e){
            $(e).hide();

            $(e).parent().parent().find('.hideInput').hide();
            $(e).parent().parent().find('.showInput').show();

            $(e).parent().parent().find('.hides').show();
        }

        function hideInput(e){
            $(e).hide();
            $(e).parent().parent().find('.hideInput').show();
            $(e).parent().parent().find('.showInput').hide();

            $(e).parent().parent().find('.shows').show();
        }

        function removeInput(e){
            $(e).parent().parent().remove();
        }

        function addProcess(){
            value = $('#valProcess').val();

            $('#process').append('<li class="list-group-item list-group-item-action"><input type="text" class="form-control hideInput" disabled="true" value="'+ value +'"><input type="text" class="form-control showInput" onkeyup="return changeValue(this)" name="process[]" value="'+ value +'"><span ><a class="shows" onclick="return showInput(this);"><i class="fa fa-pencil-square-o " aria-hidden="true" ></i></a><a class="hides" style="display: none;" onclick="return hideInput(this);"><i class="fa fa-check " aria-hidden="true" ></i></a><a onclick="return removeInput(this);"><i class="fa fa-trash remove" aria-hidden="true"></i></a></span></li> ')
        }
    </script>
@endsection

