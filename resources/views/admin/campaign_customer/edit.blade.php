@extends('admin.layout.admin')

@section('title', 'Quản lý chiến dịch '.$campaign_customers->campaign_title)

@section('content')

<!-- kiểm tra xem có phải là người tạo chiến dịch k  -->

   <style type="text/css">
        #frm_others_date{
            z-index: 99999;
            background: #eee;
            border: 1px solid #666;
            padding: 5px
        }
        #frm_others_date input{
            margin-bottom: 10px;
        }
        ul.filte_time li{
            display: inline-block;
            padding: 10px;
            cursor: pointer;
        }
        ul.filte_time {
            float: right;
        }
        .w-100{
            width: 100% !important;
        }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
			{{ $campaign_customers->campaign_title }}
        </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Chiến dịch </a></li>
            <li class="active">Thêm mới</li>
        </ol>

    </section>
    <link rel="stylesheet" href="{{ asset('adminstration/css/jkanban.min.css') }}">
    <script src="{{ asset('adminstration/js/jkanban.min.js') }}"></script>
    <section class="content">
        <div class="box box-primary boxCateScoll">
           <div class="box-body">      
         
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                 Thêm mới cơ hội
                </button>
				<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
					Thông tin chiến dịch
				</button>
			   	<button data-toggle="modal" data-target="#upload-excel" class="btn btn-info" href="#">
				   <i class="fa fa-upload" aria-hidden="true"></i> Tải lên file excel khách hàng
			   	</button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">

							<div class="modal-header">
								<h4 style="display: inline;" class="modal-title" id="exampleModalLabel">Thêm mới cơ hội</h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							</div>

							<div class="modal-body">
								<form role="form" action="{{ route('create_contact_opportunity') }}" method="POST">
									{!! csrf_field() !!}
									{{ method_field('POST') }}
									<!-- mã chiến dịch -->

									<input type="hidden" name="campaign_id" value="{{$campaign_customers->campaign_customer_id}}">


									<div class="form-group">
										<label for="exampleInputEmail1">Khách hàng </label>  
										<select class="form-control" name="contact"> 
											<option value="{{$campaign_customers->manager_id}}">--- Chọn khách hàng ---</option>     @foreach($contactsNullCampaign as $contact)
												<option value="{{$contact->contact_id}}">{{$contact->name}}</option>
											@endforeach
										</select>
									</div>

									<div class="form-group">
										<label for="exampleInputEmail1">Người phụ trách</label>  
										<select class="form-control" name="user_responsibility"> 
											<option value="{{$campaign_customers->manager_id}}">--- Mặc định ---</option>                         
											@foreach(App\Entity\CampaignCustomerUser::getUserWithCampaignCusId($campaign_customers->campaign_customer_id) as $customer )
												<option value="{{$customer->id}}">{{$customer->name}}</option>
											@endforeach
										</select>
									</div>

									<!-- Trạng thái trong chiến dịch -->                          
									<div class="form-group">
										<label for="exampleInputEmail1">Trạng thái đơn hàng trong chiến dịch</label>  
										<select class="form-control" name="campaign_status">          
											@foreach($proceses as $process) as $customer )
												<option value="{{$process->process_id}}">{{$process->process_name}}</option>
											@endforeach
										</select>
									</div>                 
									<!-- /.box-body -->
									<div class="form-group">
										<button type="submit" class="btn btn-primary">Thêm mới</button>
									</div>  
								</form>
							</div>
						</div>
					</div>         
				</div>

				<div class="collapse" id="collapseExample">
					<div class="well">
						@if($campaign_customers->manager_id == $thisUser)
							<form role="form" action="{{route('campaign-customer.update', ['id' => $campaign_customers->campaign_customer_id])}}" method="POST">
								{!! csrf_field() !!}
								{{ method_field('PUT') }}
							<div class="box-body">      
								<div class="row">
									<div class="col-xs-12 col-md-8">
											<div class="box-body">                              
												<div class="form-group">
													<label for="exampleInputEmail1">Tên Chiến dịch</label>
													<input value="{{$campaign_customers->campaign_title}}" type="text" class="form-control" name="campaign_title" placeholder="Tên Chiến dịch" required id="stepProductCreate1">
												</div>   

												<div class="form-group">
													<label for="exampleInputEmail1">Người phụ trách</label>
													<select class="form-control" name="manager_id">
														@foreach($managers as $manager)
															 <option value="{{$manager->id}}" <?php if($campaign_customers->manager_id == $manager->id ){ echo "selected"; } ?> >
															   {{$manager->name}}
															</option>
														@endforeach
													</select>
												</div>

												<div class="form-group">
													<label for="exampleInputEmail1">Thành viên Tham gia</label>
													<select class="form-control select2" name="employees[]" multiple="multiple" >
														@foreach($employees as $employee)
															<option value="{{$employee->user_id}}"
																@if(in_array($employee->user_id, $employeesCampains) > 0 ) selected @endif >
															   {{$employee->name}}
															</option>
														@endforeach                                         
													</select>
												</div>
												<div class="form-group">
													<button type="submit" class="btn btn-primary" id="stepProductCreate7">Chỉnh sửa</button>
												</div>
											</div>
									</div>
									<div class="col-md-4 col-xs-12">                    
										<label for="exampleInputEmail1">Các Hành trình khách hàng</label> 

										<ul class="list-group" id="process">  
											@foreach($proceses as $process)
												<li class="list-group-item list-group-item-action" style="position: relative;">
													<input type="text" class="form-control hideInput"  disabled="true" value="{{$process->process_name}}">
													<input type="text" process_id="{{$process->process_id}}" class="form-control showInput" onchange="return changeValue(this)" value="{{$process->process_name}}"> 
													<span>
													<a class="shows" onclick="return showInput(this);">              
														<i class="fa fa-pencil-square-o " aria-hidden="true" ></i>
													</a>
													<a class="hides" style="display: none;" onclick="return hideInput(this);"> 
														<i class="fa fa-check " aria-hidden="true" ></i>
													</a>
												   <!--  <a onclick="return removeInput(this);">
														<i class="fa fa-trash remove" aria-hidden="true"></i>
													</a> -->
													</span>
												</li>
											@endforeach

										</ul>                    
										<!--    
										<div class="form-group">
											<input type="text" id="valProcess" class="form-control" placeholder="Thêm mới hành trình khách hàng!"> 
											<a style="margin-top: 10px" onclick ="addProcess()" class="btn btn-success">Thêm </a> 
										</div>
										-->
									</div> 
								</div>
							</div>
						</form>
						@else
						<div class="box-body">      
							<div class="row">
								<div class="col-xs-12 col-md-8">
										<div class="box-body">                              
											<div class="form-group">
												<label for="exampleInputEmail1">Tên Chiến dịch</label>
												<input value="{{$campaign_customers->campaign_title}}" type="text" class="form-control" disabled="disabled" placeholder="Tên Chiến dịch" required id="stepProductCreate1">
											</div>   

											<div class="form-group">
												<label for="exampleInputEmail1">Người phụ trách</label>
												<select disabled="disabled" class="form-control">
													@foreach($managers as $manager)
														 <option value="{{$manager->id}}" <?php if($campaign_customers->manager_id == $manager->id ){ echo "selected"; } ?> >
														   {{$manager->name}} -
														   @if($manager->role == 1)
															   <b>Thành viên</b>
														   @elseif($manager->role == 2)
																<b>Biên tập viên</b>
														   @elseif($manager->role == 3)
															   <b> Quản trị</b>
														   @endif
														</option>
													@endforeach
												</select>
											</div>

											<div class="form-group">
												<label for="exampleInputEmail1">Thành viên Tham gia</label>
												<select class="form-control select2" disabled="disabled" multiple="multiple" >
													@foreach($employeesCampains as $user )
														@foreach($managers as $employee)
															<option value="{{$manager->id}}" <?php if($user->user_id == $employee->id ){ echo "selected"; } ?> >
															   {{$employee->name}}
															</option>
														@endforeach                                          
													@endforeach
												</select>
											</div>
										   
										</div>
								</div>
								<div class="col-md-4 col-xs-12">                    
									<label for="exampleInputEmail1">Các Hành trình khách hàng</label> 

									<ul class="list-group" id="process">  
										@foreach($proceses as $process)
											<li class="list-group-item list-group-item-action" style="position: relative;">
												<input type="text" class="form-control hideInput"  disabled="true" value="{{$process->process_name}}">
												<input type="text" process_id="{{$process->process_id}}" class="form-control showInput" onchange="return changeValue(this)" value="{{$process->process_name}}"> 
												<span>
												<a class="shows" onclick="return showInput(this);">              
													<i class="fa fa-pencil-square-o " aria-hidden="true" ></i>
												</a>
												<a class="hides" style="display: none;" onclick="return hideInput(this);"> 
													<i class="fa fa-check " aria-hidden="true" ></i>
												</a>
											   <!--  <a onclick="return removeInput(this);">
													<i class="fa fa-trash remove" aria-hidden="true"></i>
												</a> -->
												</span>
											</li>
										@endforeach

									</ul>                    
									<!--    
									<div class="form-group">
										<input type="text" id="valProcess" class="form-control" placeholder="Thêm mới hành trình khách hàng!"> 
										<a style="margin-top: 10px" onclick ="addProcess()" class="btn btn-success">Thêm </a> 
									</div>
									-->
								</div> 
							</div>
						</div>
						@endif
					</div>
				</div>

            <div class="box-header with-border">
                <div class="col-md-12">
                    <ul class="filte_time">
                        <li><a data-time="THIS_DAY" class="btn_choose_time">Hôm nay </a></li>
                        <li><a data-time="THIS_WEEK" class="btn_choose_time">Tuần này</a></li>
                        <li><a data-time="LAST_WEEK" class="btn_choose_time">Tuần trước</a></li>
                      
                        <li><a data-time="THIS_MONTH" class="btn_choose_time">Tháng này</a></li>
                        <li><a data-time="LAST_MONTH" class="btn_choose_time">Tháng trước</a></li>
            
                        <li><a data-time="THIS_YEAR" class="btn_choose_time">Năm nay</a></li>
                        <li><a data-time="LAST_YEAR" class="btn_choose_time">Năm trước</a></li>

                        <li>
                        <a data-time="TIME_REQUEST" onclick="return showModalTime();">
                        Thời gian khác
                        </a>
                            <div class="add-drop add-d-right" id="frm_others_date" style="left: auto; right: 0px; display:  none;position: absolute;">
                                <s class="gf-icon-neotop"></s>
                                <div class="padding">
                                    <p>Ngày bắt đầu</p>
                                    <input placeholder="Ngày bắt đầu" id="start_date" type="date" class="datepick form-control hasDatepicker"> </div>
                                <div class="padding">
                                    <p>Ngày kết thúc</p>
                                    <input placeholder="Ngày kết thúc" id="end_date" type="date" class="datepick form-control hasDatepicker"> </div>
                                <div class="padding tl">
                                    <button class="btn btn-info btn_choose_time" data-time="TIME_REQUEST" id="filterByOthersTime">Áp dụng</button>

                                    <button class="btn btn-default"  onclick="return showModalTime();" id="closeFilterOthersTime">Đóng</button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
            	<div class="kanbans col-xs-12">
	                <div id="myKanban">
	                    
	                </div>
	                <script type="text/javascript">                    
	                    function minusCountContact(status) {
	                        var countContact = $("#status"+status).html();
	                        /* trừ đi 1 */
	                        countContact = parseInt(countContact) - 1;
	                        $("#status"+status).html(countContact);
	                    }

	                    function addCountContact(status) {
	                        var countContact = $("#status"+status).html();
							/* trừ đi 1 */
	                        countContact = parseInt(countContact) + 1;
	                        $("#status"+status).html(countContact);
	                    }

	                    var KanbanTest = new jKanban({
	                        element: "#myKanban",
	                        gutter: "10px",
	                        widthBoard: "200px",
	                        click: function(el) {
	                            console.log("Trigger on all items click!");
	                        },
	                        buttonClick: function(el, boardId) {

	                        },
	                        addItemButton: false,
	                        boards: [
	                         @foreach($proceses as $process)
	                            {
	                                id: "{{$process->process_id}}",
	                                title: '<div style="color: #fff;">{{$process->process_name}}</div> ',
	                                class: "info,good",
	                                item: [
	                                    @foreach ($contacts as $contact)
	                                            @if($contact->status == $process->process_id)
	                                            {
	                                                id: "{{ $contact->contact_id }}",
	                                                title: '<p style="color:{{isset($contact->color) ? $contact->color : "#000"}}">Phụ trách: {{ $contact->user_name_responsibility }} </p> <p>KH: {{ $contact->name }}</p> <div class="number">{{ !empty($contact->sum_order) ? number_format($contact->sum_order)."đ" : "" }}</div>',
	                                                drag: function(el, source) {
	                                                    var status = KanbanTest.getParentBoardID(el.dataset.eid);
	                                                    minusCountContact(status);
	                                                },
	                                                dragend: function(el) {
	                                                    var contactId = el.dataset.eid;
	                                                    var status = KanbanTest.getParentBoardID(el.dataset.eid);
	                                                    addCountContact(status);
	                                                    callUpdateStatus (contactId, status)
	                                                },

	                                                click: function (el) {
	                                                	var contactId = el.dataset.eid;
														document.getElementById('show_customer').src = '';
														document.getElementById('show_customer').src = '/admin/contact/'+contactId;
													}
	                                            },
	                                            @endif
	                                    @endforeach
	                                ]
	                            },
	                            @endforeach
	                            
	                        ]
	                    });
						var content_start_loading = function() {
							$('#loadCustomer').modal('show');
						};
						var content_finished_loading = function(iframe) {
							var srcIframe = document.getElementById("show_customer").src;

							if ($('#show_customer').attr('src') != '') {
								$('#loadCustomer').modal('hide');
								$('#showCustomer').modal('show');
							}
							iframe.contentWindow.onunload = content_start_loading;
						};
	                </script>
	            </div>
				<div class="modal" id="showCustomer" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document" style="width: 1100px;">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Chi tiết khách hàng</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<iframe src="" onload="content_finished_loading(this)" id="show_customer" style="border:none; height: 600px; width: 100%" ></iframe>

							</div>
						</div>
					</div>
				</div>

				<div class="modal" id="loadCustomer" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document" style="width: 1100px;">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Chi tiết khách hàng</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<iframe src="/image/tenor.gif" style="border:none; height: 600px; width: 100%" ></iframe>
							</div>
						</div>
					</div>
				</div>
            </div>

        </div>
        <script>

            function showModalTime(){
                $('#frm_others_date').toggle(400)
            }

            $('.btn_choose_time').click(function(e) {
            var time = $(this).attr('data-time');

            var start_date = $("#start_date").val();
            var end_date = $("#end_date").val();
            var campaign_id = {{$campaign_customers->campaign_customer_id}};

            $('#myKanban').remove();
            $.ajax({
                url: '{{route("filter_time_campaign")}}',
                type: 'POST',
                data: {
                    campaign_id: campaign_id,
                    time: time,
                    start_date: start_date,
                    end_date: end_date
                },
            })
                .done(function(data) {

                    $('.box').append(data);

                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });
            });


            function callUpdateStatus (contactId, status) {
                $.ajax({
                    url: '{!! route('update_campaign_status') !!}',
                    type: 'POST',
                    data: {
                        contact_id: contactId,
                        campaign_status: status,
                        campaign_customer_id : {!! $campaign_customers->campaign_customer_id !!},
                        _token: $('input[name=_token]').val()

                    },
                    success: function(result){
                        alert('Chuyển trạng thái thành công')
                    },
                    error: function(error) {
                    }
                });
            }

        </script>
        {{ $contacts->links() }}
                 
    </section>
	<div class="modal fade" id="upload-excel">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tải lên file Excel khách hàng</h4>
				</div>
				<form action="{{ route('import-customer-excel-campaign') }}" method="post" enctype="multipart/form-data">
					{!! csrf_field() !!}
					{{ method_field('POST') }}
					<input type="hidden" value="{!! $campaign_customers->campaign_customer_id !!}" name="campaign_customer_id" />
					<div class="modal-body">
						<div class="form-group">
							<label for="file">File tải lên</label>
							<input type="file" name="file" id="file" class="w-100" onchange="checkExtensionFile(this)" required/>
						</div>
						<div class="form-group">
							<label for="file">Để file Excel tải lên chúng tôi có thể đọc được, mời các bạn tham khảo mẫu của chúng tôi dưới đây</label>
							<a href="{{ asset('adminstration/excel/form/mau-tai-danh-sach-khach-hang.xls') }}" download><i class="fa fa-file-text-o" aria-hidden="true"></i> Tải xuống file mẫu</a>
						</div>
						<span id="error" style="color: #FF0000"></span>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Đóng</button>
						<button type="submit" class="btn btn-primary">Tải lên</button>
					</div>
				</form>
			</div>
		</div>
	</div>
    <style type="text/css">
        .kanban-board-header{
            background:#0089f2;;
        }
        .list-group .list-group-item .showInput{
            display: none;
        }
        .list-group .list-group-item{
            margin: 5px;
            border-radius: unset;
            position: relative;
        }
        .list-group .list-group-item span{
            position: absolute; right: 30px;top:30%;font-size: 16px ;
        }
        .list-group .list-group-item input{
            padding-right:50px; 
        }
    </style>

    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });

        function changeValue(e) {
            var valChange = $(e).val();
            var process_id = $(e).attr('process_id')
            $(e).parent().find('.hideInput').val(valChange);

            $.ajax({
                url: '{{route("change_process")}}',
                type: 'POST',
                data: { 
                    process_id: process_id ,
                    process_title: valChange
                    },
            })
            .done(function() {
                console.log("success");
                alert('Chỉnh sửa thành công');
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        }

        function showInput(e){
            $(e).hide();

            $(e).parent().parent().find('.hideInput').hide();
            $(e).parent().parent().find('.showInput').show();

            $(e).parent().parent().find('.hides').show();
        }

        function hideInput(e){
            $(e).hide();
            $(e).parent().parent().find('.hideInput').show();
            $(e).parent().parent().find('.showInput').hide();

            $(e).parent().parent().find('.shows').show();
        }

        function removeInput(e){
            $(e).parent().parent().remove();
        }


    </script>
@endsection

