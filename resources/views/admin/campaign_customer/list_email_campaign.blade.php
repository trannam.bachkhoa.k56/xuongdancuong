@extends('admin.layout.admin')

@section('title', 'Danh sách chiến dịch')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Email Chiến dịch
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách chiến dịch</a></li>
        </ol>
    </section>
    <section class="content">
    	
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <a href="{{ route('add_mail_automation') }}" >
                                    <button class="btn btn-primary" id="stepProductGeneral3" data-step="1" data-intro="Ấn vào đây nếu bạn muốn thêm mới , ...">Thêm mới </button>
                                </a>
                            </div>                          
                        </div>
                        <div class="tab-content">
                            <div  class="">
                                <div class="box-body">
                                    <table id="camnpaign_customers_email" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Tên chiến dịch</th>
                                            <th width="20%">Mô tả</th>               
                                            <th>Thao tác</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                <!-- /.box -->
                    </div>                    
                </div>
            </div>
    </section>
    @include('admin.partials.popup_delete')
    
@endsection
@push('scripts')
<script>
    $(function() {
        $('#camnpaign_customers_email').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
			type: "POST",
            ajax: '{!! route('databtable_email_campaign_customer') !!}',
            columns: [
                { data: 'mail_id', name: 'campaign_customer.mail_id' },
                { data: 'campaign_title', name: 'campaign_customer.campaign_title' },
                { data: 'process', name: 'process.process_name' },      
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    });


</script>

@endpush
