@extends('admin.layout.admin')

@section('title', 'Thêm mới đơn hàng' )

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Thêm mới đơn hàng
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">đơn hàng</a></li>
        <li class="active">Thêm mới</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- form start -->
        <form role="form" action="{{ route('order-customer.store') }}" method="POST">
            {!! csrf_field() !!}
            {{ method_field('POST') }}
                    <!-- Nội dung thêm mới -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Nội dung</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <div class="col-xs-12 col-md-6">
						@if (isset($contact))
                        <div class="form-group">
                            <label for="exampleInputEmail1">Khách hàng: {{ $contact->name }}</label>
                            <input type="hidden" class="form-control" name="contact_id" value="{{ $contact->contact_id }}" />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Số điện thoại: {{ $contact->phone }}</label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email: {{ $contact->email }}</label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Địa chỉ: {{ $contact->address }}</label>
                        </div>
						@else
							<div class="form-group">
								<label for="exampleInputEmail1">Lựa chọn khách hàng: </label>
								<select name="contact_id" class="form-control">
									@foreach ( \App\Entity\Contact::getAllContact() as $contact)
									<option value="{{ $contact->contact_id }}">{{$contact->name}}</option>
									@endforeach
								</select>
							</div>
						@endif
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nhập số điện thoại giảm giá: </label>
                            <input type="text" class="form-control" name="contact_affiliate" value="" />
                        </div>

                        <!--<div class="form-group">
                            @if (empty($userIdPartner))
                                <label for="exampleInputEmail1">Cộng sự tạo đơn hàng</label>
                                <select name="user_id" class="form-control">
                                    @foreach ($employees as $employee)
                                        <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <input type="hidden" value="{{ $userIdPartner }}" name="user_id"/>
                            @endif
                        </div> -->
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ghi chú văn bản</label>
                            <textarea  class="form-control" name="notes" placeholder="Ghi chú đơn hàng" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="col-xs-12 col-md-6">
                        <div class="input-group">
                            <select name="product" class="form-control" aria-describedby="basic-addon1">
                                <option value="0">Thêm mới sản phẩm vào đơn hàng</option>
                                @foreach($products as $product)
                                    <option value="{{ $product->product_id }}">{{ $product->title }}</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon btn btn-success" id="basic-addon1" onClick="return addProduct(this);"><i class="fa fa-plus" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-body">
                    <div class="col-xs-12 col-md-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="20%">Sản phẩm</th>
                                <th width="10%">Qty (giờ)</th>
                                <th width="30%">Đơn giá</th>
                                <th width="15%">Triết khấu (%)</th>
                                <th width="15%">Vat(%)</th>
                                <th width="15%">Tổng tiền</th>
                                <th>Xóa</th>
                            </tr>
                            </thead>
                            <tbody id="productAdd">
                            </tbody>
                            <tbody >
                            <td colspan="4">Tổng tiền<td>
                            <td colspan="2">
                                <span class="totalPrice"></span>
                                <input type="hidden" value="" name="total_price" class="totalHidden" />
                                <input type="hidden" value="" name="cost_total" class="costsHidden" />
                            </td>
                            </tbody>
                            <tbody >
                            <td colspan="4">Thanh toán<td>
                            <td colspan="2">
                                <input type="text" class="form-control formatPrice payment" name="payment" value="" />
                            </td>
                            </tbody>
                            <tbody >
                            <td colspan="4">Phụ Phí<td>
                            <td colspan="2">
                                <input type="text" class="form-control formatPrice payment" name="surcharge" value="" />
                            </td>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Lưu đơn hàng</button>
                </div>
            </div>
            <!-- /.box -->
        </form>
    </div>
</section>
<script >
    $('.formatPrice').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: '.'
    });

        function addProduct(e) {
            var productId = $(e).prev().val();

            if (productId == 0) {
                return;
            }

            $.ajax({
                type: "GET",
                url: '{!! route('add_product_order') !!}',
                data: {
                    product_id: productId
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    if (obj.status == 500) {
                        return;
                    }

                    if (obj.status == 200) {
                        var product = obj.product;
                        var html =
                                `<tr>
    <td>
        ${product.title}
        <input type="hidden" value="${product.product_id}" name="product_id[]" />
    </td>
    <td>
        <input type="number" onChange="return changeElement(this);" class="qty" min=1 max=100 value="1" name="qty[]" />
    </td>
    <td>
        <select name="price[]" class="form-control price" onchange="return changeElement(this);">
            <option value="${product.price}">Giá bán lẻ: ${numeral(product.price).format('0,0')} đ</option>
            <option value="${product.discount}">Giá khuyến mãi: ${numeral(product.discount).format('0,0')} đ</option>
            <option value="${product.wholesale}">Giá bán buôn: ${numeral(product.wholesale).format('0,0')} đ</option>
        </select>
    </td>
    <td>
        <input type="hidden" class="cost_product" value="${product.cost}" name=""/>
        <input type="number" onChange="return changeElement(this);" value="" class="tax" placeholder="% triết khấu" name="tax[]"/>
    </td>
    <td>
        <input type="number" onChange="return changeElement(this);" value="" class="vat" placeholder="Thuế VAT" name="vat[]"/>
    </td>
    <td>
       <span class="sumPrice">${numeral(product.price).format('0,0')} đ</span>
    </td>
    <td>
        <a  class="btn btn-danger btnDelete" onclick="return deleteProduct(this);">
            <i class="fa fa-trash-o" aria-hidden="true"></i>
        </a>
    </td>
</tr>`;
                        $('#productAdd').append(html);
                        totalPrice();
                    }
                }
            });
        }

        function deleteProduct(e) {
            $(e).parent().parent().remove();
            totalPrice();
        }
        function changeElement(e) {
            var qty = $(e).parent().parent().find('.qty').val();
            console.log(qty);
            var price = $(e).parent().parent().find('.price').val();
            var tax = $(e).parent().parent().find('.tax').val();
            var vat = $(e).parent().parent().find('.vat').val();
            var sum = parseFloat(qty)*parseFloat(price);
            /* triết khấu */
            if (tax > 0) {
                sum -= parseFloat(sum)*parseFloat(tax)/100;
            }
            /* thuế */
            if (vat > 0) {
                sum += parseFloat(sum)*parseFloat(vat)/100;
            }
            $(e).parent().parent().find('.sumPrice').html(numeral(sum).format('0,0'));

            totalPrice();
        }

        function totalPrice () {
            var total = 0;
            /* tính tổng tiền */
            $('.sumPrice').each(function () {
                var totalPrice = $(this).html();
                total += parseInt(numeral(totalPrice).value());
            });

            /* tính giá vốn */
            var costs = 0;
            $('.cost_product').each(function () {
                var cost = $(this).val();
                if (cost !== "null") {
                    costs += parseInt(cost);
                }

            });

            $('.totalPrice').html(numeral(total).format('0,0') + 'đ');
            $('.totalHidden').val(total);
            $('.costsHidden').val(costs);
        }
</script>
@endsection

