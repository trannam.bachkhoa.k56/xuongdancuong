@extends('admin.layout.admin')

@section('title', 'Chỉnh sửa cơ chế hoa hồng'.$commission->position )

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Chỉnh sửa cơ chế hoa hồng {{ $commission->position }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Cơ chế hoa hồng</a></li>
        <li class="active">Chỉnh sửa</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- form start -->
        <form role="form" action="{{ route('commission.update', ['commission_id' => $commission->commission_id]) }}" method="POST">
            {!! csrf_field() !!}
            {{ method_field('PUT') }}
            <div class="col-xs-12 col-md-6">

                <!-- Nội dung thêm mới -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nội dung</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mức độ nhân viên</label>
                            <input type="text" class="form-control" name="position"  value="{{ $commission->position }}"
                                   placeholder="Thực tập sinh, nhân viên, chuyên viên, leader" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Lương nhân viên bán hàng</label>
                            <input type="text" class="form-control formatPrice" name="salary"
                                   value="{{ $commission->salary }}"
                                   placeholder="đường dẫn tĩnh" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Phụ cấp</label>
                            <input type="text" class="form-control formatPrice" name="sub_salary"
                                   value="{{ $commission->sub_salary }}"
                                   placeholder="phụ cấp" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Bảo hiểm</label>
                            <input type="text" class="form-control formatPrice" name="security"
                                   value="{{ $commission->security }}"
                                   placeholder="bảo hiểm" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Doanh thu cần đạt được</label>
                            <input type="text" class="form-control formatPrice" name="revenue"
                                   value="{{ $commission->revenue }}"
                                   placeholder="Mức doanh số cần đạt được" >
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Cơ chế hoa hồng (%)</label>
                            <input type="number" class="form-control" name="commission"
                                   value="{{ $commission->commission }}"
                                   placeholder="ví dụ: 25%" >
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </div>
                <!-- /.box -->

            </div>
        </form>
    </div>
</section>
<script>
    $('.formatPrice').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: '.'
    });
</script>
@endsection

