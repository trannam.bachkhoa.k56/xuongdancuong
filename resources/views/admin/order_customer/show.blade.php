@extends('admin.layout.admin')

@section('title', 'Chi tiết đơn hàng' )

@section('content')
    <section class="content-header">
        <h1>
            Hóa đơn
            <small>#{{ $orderCustomer->order_customer_id }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Hóa đơn</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> Hóa đơn
                    <small class="pull-right">Thời gian: {{ date_format(date_create($orderCustomer->created_at), "d/m/Y H:i") }}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                Khách hàng
                <address>
                    <strong></strong><br>
                    Họ tên: {{ !empty($contact->name) ? $contact->name : '' }}<br>
                    Địa chỉ: {{ !empty($contact->address) ? $contact->address : '' }}<br>
                    Phone: {{ !empty($contact->phone) ? $contact->phone : '' }}<br>
                    Email: {{  !empty($contact->email) ? $contact->email : '' }}
                </address>
            </div>
            <!-- /.col -->
            <!-- /.col -->
            <div class="col-sm-6 invoice-col">
                <b>Hóa đơn</b><br>
                <br>
                <b>Order ID:</b> {{ $orderCustomer->order_customer_id }}<br>
                <b>Payment Due:</b> {{ date_format(date_create($orderCustomer->created_at), "d/m/Y H:i") }}<br>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th width="20%">Sản phẩm</th>
                        <th width="10%">Qty (giờ)</th>
                        <th width="30%">Đơn giá</th>
                        <th width="15%">Triết khấu (%)</th>
                        <th width="15%">Vat(%)</th>
                        <th width="15%">Tổng tiền</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $total =0; @endphp
                    @foreach ($orderCustomerItems as $orderCustomerItem)
                        <tr>
                            <td>{{  $orderCustomerItem->title }}</td>
                            <td>{{  $orderCustomerItem->qty }}</td>
                            <td>{{ number_format($orderCustomerItem->price, 0,",",".") }} đ</td>
                            <td>{{  $orderCustomerItem->tax }}</td>
                            <td>{{  $orderCustomerItem->vat }}</td>
                            <td>
                                @php
                                $sum = $orderCustomerItem->price*$orderCustomerItem->qty;
                                if ($orderCustomerItem->tax > 0) {
                                $sum -= $sum*$orderCustomerItem->tax/100;
                                }
                                if ($orderCustomerItem->vat > 0) {
                                $sum += $sum*$orderCustomerItem->vat/100;
                                }

                                $total += $sum;
                                @endphp
                                {{ number_format($sum, 0,",",".") }} đ
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <!-- /.col -->
            <div class="col-xs-6">
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <td>{{ $orderCustomer->notes }}</td>
                </p>
            </div>
            <div class="col-xs-6">
                <p class="lead">Hóa đơn lập ngày {{ date_format(date_create($orderCustomer->created_at), "d/m/Y H:i") }}</p>

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Tổng tiền:</th>
                            <td>{{ number_format($total, 0,",",".") }} đ</td>
                        </tr>
                        <tr>
                            <th>Phụ phí:</th>
                            <td>{{ number_format($orderCustomer->surcharge, 0,",",".") }} đ</td>
                        </tr>
                        <tr>
                            <th>Đã thanh toán:</th>
                            <td>{{ number_format($orderCustomer->payment, 0,",",".") }} đ </td>
                        </tr>
                        <tr>
                            <th>Công nợ:</th>
                            <td>{{ number_format(($total-$orderCustomer->surcharge-$orderCustomer->payment), 0,",",".") }} đ</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        @if (($total-$orderCustomer->surcharge-$orderCustomer->payment) > 0)
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                    <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-usd" aria-hidden="true"></i> Thanh toán đơn hàng
                    </button>
                </div>
            </div>
        @endif
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Thanh toán đơn hàng</h4>
                </div>
                <form action="{{ route('order_customer_payment') }}" method="get">
                    <div class="modal-body">
                        <p class="lead">Hóa đơn lập ngày {{ date_format(date_create($orderCustomer->created_at), "d/m/Y H:i") }}</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="width:50%">Tổng tiền:</th>
                                    <td>{{ number_format($total, 0,",",".") }} đ</td>
                                </tr>
                                <tr>
                                    <th>Phụ phí:</th>
                                    <td>{{ number_format($orderCustomer->surcharge, 0,",",".") }} đ</td>
                                </tr>
                                <tr>
                                    <th>Đã thanh toán:</th>
                                    <td>{{ number_format($orderCustomer->payment, 0,",",".") }} đ </td>
                                </tr>
                                <tr>
                                    <th>Công nợ:</th>
                                    <td>{{ number_format(($total-$orderCustomer->surcharge-$orderCustomer->payment), 0,",",".") }} đ</td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Thanh toán</label>
                            <input type="text" class="form-control formatPrice" name="payment" placeholder="Số tiền thanh toán"  >
                        </div>
                        <input type="hidden" name="order_customer_id" value="{{ $orderCustomer->order_customer_id }}" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary">Thanh toán</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $('.formatPrice').priceFormat({
            prefix: '',
            centsLimit: 0,
            thousandsSeparator: '.'
        });
    </script>
@endsection

