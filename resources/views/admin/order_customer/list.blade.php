@extends('admin.layout.admin')

@section('title', 'Quản lý đơn hàng' )

@section('content')
		<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Quản lý đơn hàng
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Danh sách quản lý đơn hàng</a></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<a  href="{{ route('order-customer.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th width="5%">STT</th>
							<th>Ngày đặt hàng</th>
							<th>khách hàng</th>
							<th>Doanh số</th>
							<th>Hoa hồng</th>
							<th>Giá vốn</th>
							<th>phụ phí</th>
							<th>Đã thanh toán</th>
							<th>Công nợ</th>
							<th>Lợi nhuận</th>
							<th>Người thực hiện</th>
							<th>Xóa</th>
						</tr>
						</thead>
						<tbody>
						@foreach ($orderCustomers as $id => $orderCustomer)
							<tr>
								<td width="5%">{{ ($id+1) }}</td>
								<td>{{ date_format(date_create($orderCustomer->created_at), "d/m/Y H:i") }}</td>
								<td>{{ $orderCustomer->contact_name }}</td>
								<td>{{ number_format($orderCustomer->total_price, 0,",",".") }} đ</td>
								<td>{{ $orderCustomer->affiliate_money }} %</td>
								<td>{{ !empty($orderCustomer->cost) ? number_format($orderCustomer->cost, 0,",",".") : '0' }} đ</td>
								<td>{{ !empty($orderCustomer->surcharge) ? number_format($orderCustomer->surcharge, 0,",",".") : 0 }} đ</td>
								<td>{{ !empty($orderCustomer->payment) ? number_format($orderCustomer->payment, 0,",",".") : 0 }} đ</td>
								<?php 
									$realTotal = $orderCustomer->total_price - $orderCustomer->payment - $orderCustomer->surcharge;
									$realTotal = $orderCustomer->total_price - ($orderCustomer->total_price*$orderCustomer->affiliate_money/100);
								 ?>
								<td>{{ number_format(($realTotal ), 0,",",".") }} đ</td>
								<td>{{ !empty($orderCustomer->payment) && !empty($orderCustomer->surcharge) ? number_format(($orderCustomer->total_price - $orderCustomer->cost - $orderCustomer->surcharge ), 0,",",".") : 0 }} đ</td>
								<td>{{ $orderCustomer->user_name }}</td>
								<td>
									<a  href="{{ route('order-customer.show', ['order_customer_id' => $orderCustomer->order_customer_id]) }}" class="btn btn-primary" >
										<i class="fa fa-eye" aria-hidden="true"></i>
									</a>
									<a  href="{{ route('order-customer.destroy', ['order_customer_id' => $orderCustomer->order_customer_id]) }}" class="btn btn-danger btnDelete" data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
										<i class="fa fa-trash-o" aria-hidden="true"></i>
									</a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
@include('admin.partials.popup_delete')
@endsection

