<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title" style="padding-bottom: 20px"> <i class="fa fa-list-ul"></i>  Danh sách đơn hàng </h3>
    </div>

    <div class="box-body">
        <table id="user" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th width="5%">STT</th>
                <th>Tên đơn hàng</th>
                <th>Email</th>
                <th>Gói mua </th>
                <th>Giá tiền </th>
                <!--  <th>Triết khấu (30%)</th>     -->
                <th>Mã giới thiệu</th>
				<th>Trạng thái</th>  
                <th>Ngày tạo</th>
                <!-- <th>Trạng thái</th>                                      -->
            </tr>
            </thead>
            <tbody>

            @foreach(App\Entity\CustomerIntroduct::getIntroduceWithUserID(isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id ) as $id => $order  )
                <tr>
                    <td>{{ ($id+1) }}</td>
                    <td>{{ $order->user_buy_name }}</td>
                    <td>{{ $order->shipping_email }}</td>
                    <td>{{ $order->shipping_address }}</td>
                    <td>{{ number_format($order->total_price) }} <span style="display: none"  class="price">{{ $order->total_price }}</span>
                    </td>
                    <td>{{ $order->gift_code}}</td>
					<td> @php
							switch ($order->status) {
								case 0:
									echo "Hủy đơn hàng";
									break;
								case 1:
									echo "Đã đặt đơn hàng <br> (chưa thanh toán)";
									break;
								case 2:
									echo "Đã nhận đơn hàng";
									break;
								case 3:
									echo "Đang vận chuyển";
									break;
								case 4:
									echo "Đã Thanh Toán";
									break;
							}
						@endphp
					</td>
                    <td>{{ $order->created_at}}</td> 
					<td style="text-align: center;">
						  
                     </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th width="5%">STT</th>
                <th>Tên đơn hàng</th>
                <th>Email</th>
                <th>Gói mua </th>
                <th>Giá tiền </th>
                <!-- <th>Triết khấu</th> -->
                <th>Mã giới thiệu</th>
				<th>Trạng thái</th>
                <th>Ngày tạo</th>
                <!-- <th>Trạng thái</th> -->
            </tr>
            </tfoot>
        </table>
    </div>

</div>
