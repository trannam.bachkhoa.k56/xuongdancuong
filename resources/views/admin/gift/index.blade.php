@extends('admin.layout.admin')

@section('title', 'Quyền lợi hợp tác với MOMA')

@section('content')
<?php 
     $payment =  App\Entity\PaymentInfomation::getWithUser(isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id );                                 
 ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Danh sách đơn hàng giới thiệu với Moma.vn
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"> Quyền lợi hợp tác với MOMA </a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p >
                            <img src="/adminstration/img/banner_affiliate.jpg" style="width: 100% !important;">
                        </p>
                        <div class=row>
                            <div class="col-xs-offset-0 col-md-offset-1 col-xs-12 col-md-5" >
                                <div class="form-group">
                                    <h4>Mời bạn bè tham gia Moma.vn với facebook</h4>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://moma.vn/gift/{{Auth::id()}}">
                                        <button type="button" class="btn btn-primary"><i class="fa fa-facebook" aria-hidden="true"></i> Chia sẻ liên kết lên Facebook</button>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-5" >
                                <div class="form-group">
                                    <h4>Mời bạn bè bằng liên kết</h4>
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="http://moma.vn/gift/{{Auth::id()}}" id="myCoppy" placeholder="Username" aria-describedby="basic-addon1" disabled>
                                        <span class="input-group-addon btn btn-primary" id="basic-addon1"  onclick="copyCode('#myCoppy')">@ Coppy</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <section class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#customer" aria-controls="customer" role="tab" data-toggle="tab">Khách hàng tạo website</a></li>
                                        <li role="presentation"><a href="#order" aria-controls="order" role="tab" data-toggle="tab">Dơn hàng</a></li>
                                        <li role="presentation"><a href="#top_affiliate" aria-controls="order" role="tab" data-toggle="tab">Top giới thiệu</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="customer">
                                            @include('admin.gift.domains')
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="order">
                                            @include('admin.gift.order')
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="top_affiliate">
                                            @include('admin.gift.top_affiliate')
                                        </div>
                                    </div>
                                </div>

                                <div>
                                    <p style="font-size: 20px;color: red">Tổng tiền đơn hàng đã giới thiệu :
                                        <?php
                                        $sumprice = App\Entity\CustomerIntroduct::getSumPriceWithUserID(isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id );
                                        echo number_format($sumprice). " VNĐ </br>";
                                        if ($sumprice < 10000000) {
                                            # code...
                                            echo 'Mức Hoa hồng 23%';
                                        }
                                        else if ($sumprice >= 10000000 || $sumprice < 20000000 ) {
                                            # code...
                                            echo 'Mức Hoa hồng 25%';
                                        }
                                        else if ($sumprice >= 20000000 || $sumprice < 50000000 ) {
                                            # code...
                                            echo 'Mức Hoa hồng 27%';
                                        }
                                        else if ($sumprice >= 50000000 || $sumprice < 70000000 ) {
                                            # code...
                                            echo 'Mức Hoa hồng 29%';
                                        }
                                        else if ($sumprice >= 70000000 || $sumprice < 100000000 ) {
                                            # code...
                                            echo 'Mức Hoa hồng 30%';
                                        }
                                        else if ($sumprice >= 100000000 || $sumprice < 200000000 ) {
                                            # code...
                                            echo 'Mức Hoa hồng 25%';
                                        }
                                        else if ($sumprice >= 20000000 ) {
                                            # code...
                                            echo 'Mức Hoa hồng 35%';
                                        }
                                        ?>
                                    </p>

                                    <p>
                                        Số tiền đang có: {{number_format(isset($user->coin) ? $user->coin : 0 )}} VNĐ
                                    </p>
                                    @if($user->coin != 0 )
                                        @if(!empty($payment))
                                            <div>
                                                <a onclick="pheDuyet();" class="btn btn-success">Chuyển tiền vào ví moma</a>
                                            </div>
                                        @else
                                            <p style="color: red; font-weight: bold;font-size: 20px ">
                                                Bạn cần Phải Khởi Tạo ví moma trước khi chuyển tiền vào ví
                                            </p>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">

                                <div class="tab-content">
                                    @include('admin.gift.payment')
                                </div>
                            </div>
                        </div>
                    </section>

                    <script type="text/javascript">

                        function rutTien(){
                            $('#money').toggle(400);
                        }

                        function submit_money(){
							$('#btn-rutTien').attr('disabled',true);

							/*số tiền rút ra */
							var money_number = $('#money_number').val();
							/* số tiền trong ví */
							var coin_total = $('#coin_total').val();
							var sub_coin = coin_total - money_number;

							console.log(sub_coin);
							if( sub_coin  < 0){
								alert('Bạn cần giữ cho tài khoản không dưới 0 đồng');
								$('#btn-rutTien').attr('disabled',false);
								return; 
							}
							else{
								$.ajax({
									url: '{{route("submit_money")}}',
									type: "GET",
									data: {
										user_id : '{{$user->id }}',
										user_name : '{{$user->name}}',
										user_email : '{{$user->email}}',
										money_number: money_number,
									},
								})
								.done(function() {
									console.log("success");
									alert('Yêu cầu của bạn đã được Gửi cho quản trị viên');
								})
								.fail(function() {
									console.log("error");
								})
								.always(function() {
									console.log("complete");
								});

							}  
                        }


                        function pheDuyet(){
                            let coin = {{ $user->coin ? $user->coin : 0}};
                            var user_id = {{isset($user->id) ? $user->id : ''}};

                            $.ajax({
                                        url: '{{route("change_introduce")}}',
                                        type: 'GET' ,
                                        data: {
                                            coin: coin,
                                            user_id: user_id
                                        },
                                    })

                                    .done(function() {
                                        console.log("success");
                                        alert('Đã chuyển tiền vào ví thành công ');
                                        location.reload();
                                    })
                                    .fail(function() {
                                        console.log("error");
                                    })
                                    .always(function() {
                                        console.log("complete");
                                    });
                        }

                    </script>
                </div>
            </div>
        </div>
    </div>
</section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">

                        <div class="note" style="">
                         <h2 style="text-transform: uppercase;text-align: center;">Bảng cơ chế hoa hồng moma và cộng sự</h2>
                          <p style="text-transform: uppercase;">Thay mặt giám đốc trần hải nam công bố:</p>            
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Doanh số (Triệu vnđ) </th>                                
                                <th>Hoa hồng mua lần 1 / Giá trị đơn hàng</th>
                                <th>Hoa hồng mua lần 2 / Giá trị đơn hàng</th>
                                <th>Chênh lệch mức hoa hồng</th>
                                <th>Hoa hồng nhận được 1 năm</th>
                                <th>Hoa hồng nhận được 2 năm</th>
                                <th>Tổng thu nhập 1 năm </th>
                                <th>Tổng thu nhập 2 năm</th>                            
                              </tr>
                            </thead>
                            <tbody>
                         
                              <tr>
                                 <td>0-10 (Tr/VNĐ)</td>                                                           
                                <td>23%</td>
                                <td>35%</td>
                                <td>10.000.000 VNĐ</td>
                                <td>2.300.000 VNĐ</td>
                                <td>3.500.000 VNĐ</td>
                                <td>7.300.000 VNĐ</td>
                                <td>10.800.000 VNĐ</td>
                              </tr> 
                              <tr>
                                <td>10-20 (Tr/VNĐ)</td>                                                                                  
                                <td>25%</td>
                                <td>35%</td>
                                <td>10.000.000 VNĐ</td>
                                <td>2.500.000 VNĐ</td>
                                <td>3.500.000 VNĐ</td>
                                <td>9.800.000 VNĐ</td>
                                <td>16.800.000 VNĐ</td>
                              </tr>
                               <tr>
                                <td>20-50 (Tr/VNĐ)</td>            
                                                                                               
                                <td>27%</td>
                                <td>35%</td>
                                <td>30.000.000 VNĐ</td>
                                <td>8.100.000 VNĐ</td>
                                <td>10.500.000 VNĐ</td>
                                <td>17.900.000 VNĐ</td>
                                <td>35.400.000 VNĐ</td>
                              </tr>
                               <tr>
                                <td>50-70 (Tr/VNĐ)</td> 
                                                                                    
                                <td>29%</td>
                                <td>35%</td>
                                <td>20.000.000 VNĐ</td>
                                <td>5.800.000 VNĐ</td>
                                <td>7.000.000 VNĐ</td>
                                <td>23.700.000 VNĐ</td>
                                <td>48.200.000 VNĐ</td>
                              </tr>
                              <tr>
                               
                                <td>70-100 (Tr/VNĐ)</td>                                                                           
                                <td>30%</td>
                                <td>35%</td>
                                <td>30.000.000 VNĐ</td>
                                <td>9.000.000 VNĐ</td>
                                <td>10.500.000 VNĐ</td>
                                <td>32.700.000 VNĐ</td>
                                <td>67.700.000 VNĐ</td>
                              </tr>
                              <tr>
                                <td>200 (Tr/VNĐ)</td>
                                <td>35%</td>
                                <td>35%</td>
                                <td>100.000.000 VNĐ</td>
                                <td>35.800.000 VNĐ</td>
                                <td>35.000.000 VNĐ</td>
                                <td>62.700.000 VNĐ</td>
                                <td>132.700.000 VNĐ</td>
                              </tr>
                            </tbody>
                          </table>                                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <style>
        #myCode{
            border: none;
            width: 50%;
            background: honeydew;
        }
        table td,th {
            text-align: center;
            border: 1px solid #333;
        }
        table td {
          /*color: red;*/
        }
        table {
            border-top:1px solid #333; 
        }
        table > thead > tr > th{
            border-bottom:1px solid #333 !important;
        }

    </style>
    <script>
        function copyCode(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).val()).select();
            document.execCommand("copy");
            $temp.remove();

            alert("Sao chép mã giới thiệu thành công: " + $(element).val());
        }
    </script>
@endsection
