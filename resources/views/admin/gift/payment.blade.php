
<div class="box box-info" id="forGroup">
    <div class="box-header">
        <i class="fa fa-address-book"></i>
        <h3 class="box-title" >Thông tin Khách hàng </h3>
    </div>
    <div class="box-body">
        <div class="card" style="width: 30rem;position: relative;">
            <div class="card-body" >
                <h3 class="card-title" style="margin-top: 10px">  {{ isset($customerIntroducts->user_name) ? $customerIntroducts->user_name : $user->name }}
                </h3>

                <p class="card-text">
                    <i class="fa fa-phone"></i>
                    {{ isset($customerIntroducts->user_phone) ? $customerIntroducts->user_phone : $user->phone }}
                </p>
                <p class="card-text">
                    <i class="fa fa-envelope"></i>
                    {{ isset($customerIntroducts->user_email) ? $customerIntroducts->user_email : $user->email }}
                </p>

                <p class="card-text" >
                    @if(!empty($payment))
                        <label>Số dư trong ví:</label>
                        <span style="color: #ff9800;font-size: 16px;font-weight: bold;">{{ $payment->coin_total }} <i class="fa fa-usd" aria-hidden="true"></i></span>
                        <input type="hidden" id="coin_total" value="{{ $payment->coin_total }}">

						<a style="margin-left: 10px;" onclick="rutTien()"> Rút tiền </a>
						<div id="money" style="display: none;">
							<div class="form-group">
								<input class="form-control" id="money_number" type="number" name="" placeholder="Số tiền ...." min="1">
							</div>

							<div class="form-group">
								<button class="btn btn-rutTien" id="btn-rutTien" onclick="submit_money()">Rút tiền</button>
							</div>
						</div>
					@endif
                </p>
                <!-- nếu đăng nhập bằng user thành viên thì có nút đổi thông tin -->
                @if(\Illuminate\Support\Facades\Auth::user()->role != 4 )
                    <a style="position: absolute;top: 0;right: 0" href="{{ route('users.edit', ['id' => Auth::user()->id]) }}" class="btn btn-primary"> Đổi thông tin </a>
                @endif
            </div>
        </div>
    </div>

    <!-- nếu không có ví thì hiện form tạo mới ,Nếu có rồi thì ra edit -->
    @if(empty($payment))
        <div id="formPayment">
            <h3 class="box-title"> Thêm mới thông tin tài khoản </h3>
            <form method="POST" action="{{route('add_payment_infomation')}}">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <input class="form-control" type="hidden" name="user_id" value="{{isset($customerIntroducts->id) ? $customerIntroducts->id : $user->id }}">

                <div class="form-group">
                    <label>Tên Chủ tài khoản</label>
                    <input class="form-control" type="text" name="payment_name" required="">
                </div>
                <div class="form-group">
                    <label>Số tài khoản</label>
                    <input class="form-control" type="number" name="bank_number" required="">
                </div>
                <div class="form-group">
                    <label>Tên Ngân hàng</label>
                    <input class="form-control" type="text" name="payment_bank" required="">
                </div>
                <div class="form-group">
                    <label>Tên Chi nhánh</label>
                    <input class="form-control" type="text" name="bank_branch" required="">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Kích hoạt ví MOMA</button>
                </div>

            </form>
        </div>
    @else
        <div id="formPayment">
            <h3 class="box-title"> Thông tin ví Moma </h3>
            <form method="POST" action="{{route('edit_payment_infomation')}}">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="form-group">
                    <label>Tên Chủ tài khoản</label>
                    <input class="form-control" type="text" name="payment_name" value="{{ $payment->payment_name }}">
                </div>
                <input type="hidden" name="id" value="{{$payment->payment_id}}">
                <div class="form-group">
                    <label>Số tài khoản</label>
                    <input class="form-control" type="text" name="bank_number" value="{{ $payment->bank_number }}">
                </div>
                <div class="form-group">
                    <label>Tên Ngân hàng</label>
                    <input class="form-control" type="text" name="payment_bank" value="{{ $payment->payment_bank }}">
                </div>
                <div class="form-group">
                    <label>Tên Chi nhánh</label>
                    <input class="form-control" type="text" name="bank_branch" value="{{ $payment->bank_branch }}">
                </div>
                <div class="form-group">
                    <button  type="submit" class="btn btn-primary">Lưu thông tin </button>
                </div>

            </form>
        </div>
    @endif
</div>
