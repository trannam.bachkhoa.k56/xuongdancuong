<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title" style="padding-bottom: 20px"> <i class="fa fa-list-ul"></i>  Danh sách khách hàng </h3>
    </div>

    <div class="box-body">
        <table id="domains" class="table table-bordered table-striped" style="width:1800px !important;">
            <thead>
                <tr>
                    <th width="5%">ID</th>
                    <th>Tên domain</th>
                    <th>Tên kh</th>
                    <th>SDT</th>
                    <th>Đường dẫn</th>
                    <th>email</th>
                    <th>Vip</th>
                    <th>Ngày tạo</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th width="5%">ID</th>
                    <th>Tên domain</th>
                    <th>Tên kh</th>
                    <th>SDT</th>
                    <th>Đường dẫn</th>
                    <th>email</th>
                    <th>Vip</th>
                    <th>Ngày tạo</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

@push('scripts')
<script>
    $(function() {
        var table = $('#domains').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollY: 200,
            autoWidth: false,
            pageLength: 10,
            ajax: '{!! route('domain_invite_customer') !!}?user_id={{ $user->id }}',
            columns: [
                { data: 'domain_id', name: 'domain_id' },
                { data: 'name', name: 'name'},
                { data: 'user_name', name: 'users.name' },
                { data: 'phone', name: 'users.phone' },
                { data: 'url', name: 'url' },
                { data: 'email', name: 'users.email' },
                { data: 'vip', name: 'users.vip' , render (data) {
                    if (data > 0) {
                        return 'đã nâng cấp';
                    }

                    return 'đang dùng miễn phí';
                }},
                { data: 'created_at', name: 'domains.created_at' },
            ]
        });
    });
</script>
@endpush
