<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title" style="padding-bottom: 20px"> <i class="fa fa-list-ul"></i>Top 10 người giới thiệu nhiều nhất moma </h3>
    </div>

    <div class="box-body">
        <table id="user" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th width="5%">STT</th>
                    <th>Họ và tên</th>
                    <th>Website</th>
                    <th>Giá trị</th>
					<th>Tien trong vi</th>
                </tr>
            </thead>
            @foreach ($topAffiliates as $id => $topAffiliate)
            <tbody>
                <tr>
                    <th width="5%">{!! ($id+1) !!}</th>
                    <th>{!! $topAffiliate->name !!}</th>
                    <th>{!! !empty($topAffiliate->url) ? $topAffiliate->url : $topAffiliate->domain !!}</th>
                    <th>{!! number_format($topAffiliate->total_affiliate) !!}</th>
					<th>{!! number_format($topAffiliate->coin_total) !!}</th>
                </tr>
            </tbody>
            @endforeach
            <tfoot>
                <tr>
                    <th width="5%">STT</th>
                    <th>Họ và tên</th>
                    <th>Website</th>
                    <th>Giá trị</th>
					<th>Tien trong vi</th>
                </tr>
            </tfoot>
        </table>
    </div>

</div>
