<!DOCTYPE html>
<html class="avada-html-layout-wide" lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- This site is optimized with the Yoast SEO plugin v14.4.1 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>Mở bán liền kề, biệt thự Tnr Grand Palace Thái Bình - TNR Holdings Việt Nam</title>
    <meta name="description"
          content="Mở bán liền kề, shophouse, biệt thự Tnr Grand Palace Đông Mỹ Thái Bình; DT: 80m2 - 225m2; Giá bán từ 20tr/m2; Xây 5 tầng; Website Chính thức chủ đầu tư"/>
    <meta name="robots" content="index, follow"/>
    <meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1"/>
    <link rel="canonical" href="index.html"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Mở bán liền kề, biệt thự Tnr Grand Palace Thái Bình - TNR Holdings Việt Nam"/>
    <meta property="og:description"
          content="Mở bán liền kề, shophouse, biệt thự Tnr Grand Palace Đông Mỹ Thái Bình; DT: 80m2 - 225m2; Giá bán từ 20tr/m2; Xây 5 tầng; Website Chính thức chủ đầu tư"/>
    <meta property="og:url" content="https://tnrgrandpalacethaibinh.com/"/>
    <meta property="og:site_name" content="TNR Holdings Việt Nam"/>
    <meta property="article:modified_time" content="2020-07-02T03:04:47+00:00"/>
    <meta property="og:image"
          content="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph
        ":[{"@type":"WebSite","@id
        ":"https://tnrgrandpalacethaibinh.com/#website","url":"https://tnrgrandpalacethaibinh.com/","name":"TNR Holdings Vi\u1ec7t Nam","description":"TNR Holdings","potentialAction":[{"@type":"SearchAction","target":"https://tnrgrandpalacethaibinh.com/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"en-US"},{"@type":"ImageObject","@id
        ":"https://tnrgrandpalacethaibinh.com/#primaryimage","inLanguage":"en-US","url":"https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg","width":162,"height":3},{"@type":"WebPage","@id
        ":"https://tnrgrandpalacethaibinh.com/#webpage","url":"https://tnrgrandpalacethaibinh.com/","name":"M\u1edf b\u00e1n li\u1ec1n k\u1ec1, bi\u1ec7t th\u1ef1 Tnr Grand Palace Th\u00e1i B\u00ecnh - TNR Holdings Vi\u1ec7t Nam","isPartOf":{"@id
        ":"https://tnrgrandpalacethaibinh.com/#website"},"primaryImageOfPage":{"@id
        ":"https://tnrgrandpalacethaibinh.com/#primaryimage"},"datePublished":"2020-06-29T06:58:53+00:00","dateModified":"2020-07-02T03:04:47+00:00","description":"M\u1edf b\u00e1n li\u1ec1n k\u1ec1, shophouse, bi\u1ec7t th\u1ef1 Tnr Grand Palace \u0110\u00f4ng M\u1ef9 Th\u00e1i B\u00ecnh; DT: 80m2 - 225m2; Gi\u00e1 b\u00e1n t\u1eeb 20tr/m2; X\u00e2y 5 t\u1ea7ng; Website Ch\u00ednh th\u1ee9c ch\u1ee7 \u0111\u1ea7u t\u01b0","inLanguage":"en-US","potentialAction":[{"@type":"ReadAction","target":["https://tnrgrandpalacethaibinh.com/"]}]}]}
    </script>
    <!-- / Yoast SEO plugin. -->


    <link rel='dns-prefetch' href='https://s.w.org/'/>
    <link rel="alternate" type="application/rss+xml" title="TNR Holdings Việt Nam &raquo; Feed" href="feed/index.html"/>
    <link rel="alternate" type="application/rss+xml" title="TNR Holdings Việt Nam &raquo; Comments Feed"
          href="comments/feed/index.html"/>
    <link rel="shortcut icon" href="wp-content/uploads/2020/06/logo-favicon-1.png" type="image/x-icon"/>

    <!-- For iPhone -->
    <link rel="apple-touch-icon" href="wp-content/uploads/2020/06/logo-favicon-1.png">


    <!-- For iPad -->
    <link rel="apple-touch-icon" sizes="72x72" href="wp-content/uploads/2020/06/logo-favicon-1.png">


    <meta property="og:title" content="Mở bán liền kề, biệt thự Tnr Grand Palace Thái Bình"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://tnrgrandpalacethaibinh.com/"/>
    <meta property="og:site_name" content="TNR Holdings Việt Nam"/>
    <meta property="og:description" content="TNR Grand Palace
Thông tin tổng quan dự án"/>

    <meta property="og:image"
          content="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/3.-logo-menu-1.png"/>
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/tnrgrandpalacethaibinh.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.2"}
        };
        /*! This file is auto-generated */
        !function (e, a, t) {
            var r, n, o, i, p = a.createElement("canvas"), s = p.getContext && p.getContext("2d");

            function c(e, t) {
                var a = String.fromCharCode;
                s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0);
                var r = p.toDataURL();
                return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()
            }

            function l(e) {
                if (!s || !s.fillText)return !1;
                switch (s.textBaseline = "top", s.font = "600 32px Arial", e) {
                    case"flag":
                        return !c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]));
                    case"emoji":
                        return !c([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340])
                }
                return !1
            }

            function d(e) {
                var t = a.createElement("script");
                t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
            }

            for (i = Array("flag", "emoji"), t.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, o = 0; o < i.length; o++)t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports.everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[i[o]]);
            t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function () {
                t.DOMReady = !0
            }, t.supports.everything || (n = function () {
                t.readyCallback()
            }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function () {
                "complete" === a.readyState && t.readyCallback()
            })), (r = t.source || {}).concatemoji ? d(r.concatemoji) : r.wpemoji && r.twemoji && (d(r.twemoji), d(r.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css'
          href='wp-includes/css/dist/block-library/style.min.css@ver=5.4.2.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='contact-form-7-css'
          href='wp-content/plugins/contact-form-7/includes/css/styles.css@ver=5.1.9.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='avada-stylesheet-css' href='wp-content/themes/Avada/assets/css/style.min.css@ver=5.6.css'
          type='text/css' media='all'/>
    <!--[if lte IE 9]>
    <link rel='stylesheet' id='avada-IE-fontawesome-css'
          href='https://tnrgrandpalacethaibinh.com/wp-content/themes/Avada/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6'
          type='text/css' media='all'/>
    <![endif]-->
    <!--[if IE]>
    <link rel='stylesheet' id='avada-IE-css'
          href='https://tnrgrandpalacethaibinh.com/wp-content/themes/Avada/assets/css/ie.min.css?ver=5.6'
          type='text/css' media='all'/>
    <![endif]-->
    <link rel='stylesheet' id='recent-posts-widget-with-thumbnails-public-style-css'
          href='wp-content/plugins/recent-posts-widget-with-thumbnails/public.css@ver=6.7.0.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='popup-maker-site-css'
          href='wp-content/uploads/pum/pum-site-styles.css@generated=1593678971&amp;ver=1.11.0.css' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='fusion-dynamic-css-css'
          href='wp-content/uploads/fusion-styles/4872fd85ce7c5d6e05c1a82d8e22f395.min.css@timestamp=1593659398&amp;ver=5.4.2.css'
          type='text/css' media='all'/>
    <link rel="stylesheet" type="text/css"
          href="wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/smartslider.min.css@ver=70fceec4.css"
          media="all"/>
    <style type="text/css">.n2-ss-spinner-simple-white-container {
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -20px;
            background: #fff;
            width: 20px;
            height: 20px;
            padding: 10px;
            border-radius: 50%;
            z-index: 1000;
        }

        .n2-ss-spinner-simple-white {
            outline: 1px solid RGBA(0, 0, 0, 0);
            width: 100%;
            height: 100%;
        }

        .n2-ss-spinner-simple-white:before {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 20px;
            height: 20px;
            margin-top: -11px;
            margin-left: -11px;
        }

        .n2-ss-spinner-simple-white:not(:required):before {
            content: '';
            border-radius: 50%;
            border-top: 2px solid #333;
            border-right: 2px solid transparent;
            animation: n2SimpleWhite .6s linear infinite;
        }

        @keyframes n2SimpleWhite {
            to {
                transform: rotate(360deg);
            }
        }</style>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery.js@ver=1.12.4-wp'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min.js@ver=1.4.1'></script>
    <link rel='https://api.w.org/' href='wp-json/index.html'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://tnrgrandpalacethaibinh.com/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml"/>
    <link rel='shortlink' href='index.html'/>
    <link rel="alternate" type="application/json+oembed"
          href="wp-json/oembed/1.0/embed@url=https%253A%252F%252Ftnrgrandpalacethaibinh.com%252F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="wp-json/oembed/1.0/embed@url=https%253A%252F%252Ftnrgrandpalacethaibinh.com%252F&amp;format=xml"/>


    <script type="text/javascript">
        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    </script>


    <script type="text/javascript">(function () {
            var N = this;
            N.N2_ = N.N2_ || {r: [], d: []}, N.N2R = N.N2R || function () {
                        N.N2_.r.push(arguments)
                    }, N.N2D = N.N2D || function () {
                        N.N2_.d.push(arguments)
                    }
        }).call(window);
        if (!window.n2jQuery) {
            window.n2jQuery = {
                ready: function (cb) {
                    console.error('n2jQuery will be deprecated!');
                    N2R(['$'], cb)
                }
            }
        }
        window.nextend = {
            localization: {}, ready: function (cb) {
                console.error('nextend.ready will be deprecated!');
                N2R('documentReady', function ($) {
                    cb.call(window, $)
                })
            }
        };</script>
    <script type="text/javascript"
            src="wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/n2.min.js@ver=70fceec4"></script>
    <script type="text/javascript"
            src="wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/smartslider-frontend.min.js@ver=70fceec4"></script>
    <script type="text/javascript"
            src="wp-content/plugins/smart-slider-3/Public/SmartSlider3/Slider/SliderType/Simple/Assets/dist/smartslider-simple-type-frontend.min.js@ver=70fceec4"></script>
    <script type="text/javascript">N2R('documentReady', function ($) {
            N2R(["documentReady", "smartslider-frontend", "smartslider-simple-type-frontend"], function () {
                new N2Classes.SmartSliderSimple('#n2-ss-7', {
                    "admin": false,
                    "callbacks": "",
                    "background.video.mobile": 1,
                    "alias": {"id": 0, "smoothScroll": 0, "slideSwitch": 0, "scrollSpeed": 400},
                    "align": "normal",
                    "isDelayed": 0,
                    "load": {"fade": 1, "scroll": 0},
                    "playWhenVisible": 1,
                    "playWhenVisibleAt": 0.5,
                    "responsive": {
                        "hideOn": {
                            "desktopLandscape": false,
                            "desktopPortrait": false,
                            "tabletLandscape": false,
                            "tabletPortrait": false,
                            "mobileLandscape": false,
                            "mobilePortrait": false
                        },
                        "onResizeEnabled": true,
                        "type": "auto",
                        "downscale": 1,
                        "upscale": 1,
                        "minimumHeight": 0,
                        "maximumSlideWidth": {
                            "desktopLandscape": 10000,
                            "desktopPortrait": 10000,
                            "tabletLandscape": 10000,
                            "tabletPortrait": 10000,
                            "mobileLandscape": 10000,
                            "mobilePortrait": 10000
                        },
                        "forceFull": 0,
                        "forceFullOverflowX": "body",
                        "forceFullHorizontalSelector": "",
                        "constrainRatio": 1,
                        "sliderHeightBasedOn": "real",
                        "decreaseSliderHeight": 0,
                        "focusUser": 1,
                        "focusEdge": "auto",
                        "breakpoints": [{
                            "device": "tabletPortrait",
                            "type": "max-screen-width",
                            "portraitWidth": 1199,
                            "landscapeWidth": 1199
                        }, {
                            "device": "mobilePortrait",
                            "type": "max-screen-width",
                            "portraitWidth": 700,
                            "landscapeWidth": 900
                        }],
                        "enabledDevices": {
                            "desktopLandscape": 0,
                            "desktopPortrait": 1,
                            "tabletLandscape": 0,
                            "tabletPortrait": 1,
                            "mobileLandscape": 0,
                            "mobilePortrait": 1
                        },
                        "sizes": {
                            "desktopPortrait": {"width": 1000, "height": 900, "max": 3000, "min": 1000},
                            "tabletPortrait": {"width": 701, "height": 630, "max": 1199, "min": 701},
                            "mobilePortrait": {"width": 320, "height": 288, "max": 900, "min": 320}
                        },
                        "normalizedDeviceModes": {
                            "unknown": "desktopPortrait",
                            "desktopPortrait": "desktopPortrait",
                            "desktopLandscape": "desktopPortrait",
                            "tabletLandscape": "desktopPortrait",
                            "tabletPortrait": "tabletPortrait",
                            "mobileLandscape": "tabletPortrait",
                            "mobilePortrait": "mobilePortrait"
                        },
                        "overflowHiddenPage": 0,
                        "focus": {"offsetTop": "#wpadminbar", "offsetBottom": ""}
                    },
                    "controls": {"mousewheel": 0, "touch": "horizontal", "keyboard": 1, "blockCarouselInteraction": 1},
                    "lazyLoad": 0,
                    "lazyLoadNeighbor": 0,
                    "blockrightclick": 0,
                    "maintainSession": 0,
                    "autoplay": {
                        "enabled": 1,
                        "start": 1,
                        "duration": 8000,
                        "autoplayLoop": 1,
                        "allowReStart": 0,
                        "pause": {"click": 1, "mouse": "0", "mediaStarted": 1},
                        "resume": {"click": 0, "mouse": "0", "mediaEnded": 1, "slidechanged": 0},
                        "interval": 1,
                        "intervalModifier": "loop",
                        "intervalSlide": "current"
                    },
                    "perspective": 1500,
                    "layerMode": {"playOnce": 0, "playFirstLayer": 1, "mode": "skippable", "inAnimation": "mainInEnd"},
                    "bgAnimationsColor": "RGBA(51,51,51,1)",
                    "bgAnimations": 0,
                    "mainanimation": {
                        "type": "horizontal",
                        "duration": 400,
                        "delay": 0,
                        "ease": "easeOutQuad",
                        "parallax": 0,
                        "shiftedBackgroundAnimation": 0
                    },
                    "carousel": 1,
                    "dynamicHeight": 0,
                    "initCallbacks": function ($) {
                        N2D("SmartSliderWidgetArrowImage", "SmartSliderWidget", function (e, i) {
                            function r(e, i, t, s, r, o) {
                                this.key = e, this.action = t, this.desktopRatio = s, this.tabletRatio = r, this.mobileRatio = o, N2Classes.SmartSliderWidget.prototype.constructor.call(this, i)
                            }

                            return ((r.prototype = Object.create(N2Classes.SmartSliderWidget.prototype)).constructor = r).prototype.onStart = function () {
                                this.deferred = e.Deferred(), this.slider.sliderElement.on("SliderDevice", this.onDevice.bind(this)).trigger("addWidget", this.deferred), this.$widget = e("#" + this.slider.elementID + "-arrow-" + this.key).on("click", function (e) {
                                    e.stopPropagation(), this.slider[this.action]()
                                }.bind(this)), this.$resize = this.$widget.find(".n2-resize"), 0 === this.$resize.length && (this.$resize = this.$widget), e.when(this.$widget.n2imagesLoaded(), this.slider.stages.get("ResizeFirst").getDeferred()).always(this.onLoad.bind(this))
                            }, r.prototype.onLoad = function () {
                                this.$widget.addClass("n2-ss-widget--calc"), this.previousWidth = this.$resize.width(), this.previousHeight = this.$resize.height(), this.$widget.removeClass("n2-ss-widget--calc"), this.$resize.find("img").css("width", "100%"), this.onDevice(null, {device: this.slider.responsive.getDeviceMode()}), this.deferred.resolve()
                            }, r.prototype.onDevice = function (e, i) {
                                var t = 1;
                                switch (i.device) {
                                    case"tabletPortrait":
                                    case"tabletLandscape":
                                        t = this.tabletRatio;
                                        break;
                                    case"mobilePortrait":
                                    case"mobileLandscape":
                                        t = this.mobileRatio;
                                        break;
                                    default:
                                        t = this.desktopRatio
                                }
                                this.$resize.width(this.previousWidth * t), this.$resize.height(this.previousHeight * t)
                            }, function (e, i, t, s) {
                                this.key = "arrow", this.previous = new r("previous", e, "previousWithDirection", i, t, s), this.next = new r("next", e, "nextWithDirection", i, t, s)
                            }
                        });
                        new N2Classes.SmartSliderWidgetArrowImage(this, 1, 1, 0.5);
                        N2D("SmartSliderWidgetThumbnailDefaultHorizontal", "SmartSliderWidget", function (l, d) {
                            "use strict";
                            var i = !1, c = {videoDark: '<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><circle cx="24" cy="24" r="24" fill="#000" opacity=".6"/><path fill="#FFF" d="M19.8 32c-.124 0-.247-.028-.36-.08-.264-.116-.436-.375-.44-.664V16.744c.005-.29.176-.55.44-.666.273-.126.592-.1.84.07l10.4 7.257c.2.132.32.355.32.595s-.12.463-.32.595l-10.4 7.256c-.14.1-.31.15-.48.15z"/></svg>'};

                            function t(t, i) {
                                this.key = "thumbnail", this.parameters = l.extend({
                                    captionSize: 0,
                                    minimumThumbnailCount: 1.5,
                                    invertGroupDirection: 0
                                }, i), N2Classes.SmartSliderWidget.prototype.constructor.call(this, t)
                            }

                            ((t.prototype = Object.create(N2Classes.SmartSliderWidget.prototype)).constructor = t).prototype.onStart = function () {
                                this.ratio = 1, this.itemsPerPane = 1, this.currentI = 0, this.offset = 0, this.group = parseInt(this.parameters.group), this.$widget = this.slider.sliderElement.find(".nextend-thumbnail-default"), this.bar = this.$widget.find(".nextend-thumbnail-inner"), this.scroller = this.bar.find(".nextend-thumbnail-scroller"), this.$groups = l();
                                for (var t = 0; t < this.group; t++)this.$groups = this.$groups.add(l('<div class="nextend-thumbnail-scroller-group"></div>').appendTo(this.scroller));
                                n2const.rtl.isRtl ? (this.previous = this.$widget.find(".nextend-thumbnail-next").on("click", this.previousPane.bind(this)), this.next = this.$widget.find(".nextend-thumbnail-previous").on("click", this.nextPane.bind(this))) : (this.previous = this.$widget.find(".nextend-thumbnail-previous").on("click", this.previousPane.bind(this)), this.next = this.$widget.find(".nextend-thumbnail-next").on("click", this.nextPane.bind(this))), this.slider.stages.done("BeforeShow", this.onBeforeShow.bind(this)), this.slider.stages.done("WidgetsReady", this.onWidgetsReady.bind(this))
                            }, t.prototype.renderThumbnails = function () {
                                var t;
                                this.parameters.invertGroupDirection && (t = Math.ceil(this.slider.visibleRealSlides.length / this.group));
                                for (var i = 0; i < this.slider.visibleRealSlides.length; i++) {
                                    var e, s, h = this.slider.visibleRealSlides[i], n = l('<div class="' + this.parameters.slideStyle + ' n2-ow" style="' + this.parameters.containerStyle + '"></div>');
                                    if (this.parameters.invertGroupDirection ? n.appendTo(this.$groups.eq(Math.floor(i / t))) : n.appendTo(this.$groups.eq(i % this.group)), n.data("slide", h), h.$thumbnail = n, this.parameters.thumbnail !== d && (e = h.getThumbnailType(), s = c[e] !== d ? c[e] : "", l('<div class="n2-ss-thumb-image n2-ow" style="width:' + this.parameters.thumbnail.width + "px; height:" + this.parameters.thumbnail.height + "px;" + this.parameters.thumbnail.code + '">' + s + "</div>").css("background-image", "url('" + h.getThumbnail() + "')").appendTo(n)), this.parameters.caption !== d) {
                                        var r, o = l('<div class="' + this.parameters.caption.styleClass + "n2-ss-caption n2-ow n2-caption-" + this.parameters.caption.placement + '" style="' + this.parameters.caption.style + '"></div>');
                                        switch (this.parameters.caption.placement) {
                                            case"before":
                                                o.prependTo(n);
                                                break;
                                            default:
                                                o.appendTo(n)
                                        }
                                        this.parameters.title !== d && o.append('<div class="n2-ow ' + this.parameters.title.font + '">' + h.getTitle() + "</div>"), this.parameters.description === d || (r = h.getDescription()) && o.append('<div class="n2-ow ' + this.parameters.description.font + '">' + r + "</div>")
                                    }
                                }
                                var a = "universalclick";
                                "mouseenter" === this.parameters.action ? a = "universalenter" : this.slider.hasTouch() && (a = "n2click"), this.dots = this.scroller.find(".nextend-thumbnail-scroller-group > div").on(a, this.onDotClick.bind(this)), this.images = this.dots.find(".n2-ss-thumb-image")
                            }, t.prototype.onTap = function (t) {
                                i || (l(t.target).trigger("n2click"), i = !0, setTimeout(function () {
                                    i = !1
                                }, 500))
                            }, t.prototype.onBeforeShow = function () {
                                var t = !1;
                                switch (this.parameters.area) {
                                    case 5:
                                        t = "left";
                                        break;
                                    case 8:
                                        t = "right"
                                }
                                t && (this.offset = parseFloat(this.$widget.data("offset")), this.slider.responsive.addHorizontalSpacingControl(t, this)), this.renderThumbnails(), this.slider.hasTouch() && (N2Classes.EventBurrito(this.$widget.get(0), {
                                    mouse: !0,
                                    axis: "x",
                                    start: function () {
                                        this.bar.width();
                                        this._touch = {
                                            start: parseInt(this.scroller.css(n2const.rtl.left)),
                                            max: 0
                                        }, this.getScrollerWidth() < this.thumbnailDimension.width * Math.ceil(this.dots.length / this.group) && (this._touch.max = -Math.round(this.thumbnailDimension.width * this.ratio * Math.ceil(this.dots.length / this.group - 1))), this._touch.current = this._touch.start
                                    }.bind(this),
                                    move: function (t, i, e, s, h) {
                                        return this._touch.current = Math.max(this._touch.max, Math.min(0, this._touch.start + e.x)), this.scroller.css(n2const.rtl.left, this._touch.current), 5 < Math.abs(e.x)
                                    }.bind(this),
                                    end: function (t, i, e, s, h) {
                                        Math.abs(this._touch.start - this._touch.current) < 40 ? this.resetPane() : this._touch.current > this._touch.start ? this.previousPane() : this.nextPane(), Math.abs(e.x) < 10 && Math.abs(e.y) < 10 ? this.onTap(t) : nextend.preventClick(), delete this._touch
                                    }.bind(this)
                                }), this.slider.parameters.controls.drag || this.$widget.on("click", this.onTap.bind(this))), this.widthPercent = this.$widget.data("width-percent"), this.thumbnailDimension = {
                                    widthLocal: this.dots.width(),
                                    width: this.dots.outerWidth(!0),
                                    height: this.dots.outerHeight(!0),
                                    widthBorder: parseInt(this.dots.css("borderLeftWidth")) + parseInt(this.dots.css("borderRightWidth")) + parseInt(this.dots.css("paddingLeft")) + parseInt(this.dots.css("paddingRight")),
                                    heightBorder: parseInt(this.dots.css("borderTopWidth")) + parseInt(this.dots.css("borderBottomWidth")) + parseInt(this.dots.css("paddingTop")) + parseInt(this.dots.css("paddingBottom"))
                                }, this.thumbnailDimension.widthMargin = this.thumbnailDimension.width - this.dots.outerWidth(), this.thumbnailDimension.heightMargin = this.thumbnailDimension.height - this.dots.outerHeight(), this.imageDimension = {
                                    width: this.images.outerWidth(!0),
                                    height: this.images.outerHeight(!0)
                                }, this.sideDimension = .25 * this.thumbnailDimension.width, this.scroller.height(this.thumbnailDimension.height * this.ratio * this.group), this.bar.height(this.scroller.outerHeight(!0)), this.horizontalSpacing = this.bar.outerWidth() - this.bar.width(), this.slider.sliderElement.on({
                                    SlideWillChange: this.onSlideSwitch.bind(this),
                                    visibleRealSlidesChanged: this.onVisibleRealSlidesChanged.bind(this)
                                })
                            }, t.prototype.onWidgetsReady = function () {
                                this.activateDots(this.slider.currentSlide.index), this.slider.sliderElement.on("SliderResize", this.onSliderResize.bind(this)), this.onSliderResize()
                            }, t.prototype.filterSliderVerticalCSS = function (t) {
                            };
                            var e = !(t.prototype.onSliderResize = function () {
                                var t, i, e, s, h, n, r;
                                this.slider.visibleRealSlides.length && (this.lastScrollerWidth !== this.getScrollerWidth() && (t = 1, e = (i = this.getScrollerWidth()) - 2 * this.sideDimension, (n = i / this.thumbnailDimension.width) < this.dots.length / this.group && (n = e / this.thumbnailDimension.width), this.localSideDimension = this.sideDimension, this.parameters.minimumThumbnailCount >= n && (this.localSideDimension = .1 * i, t = (e = i - 2 * this.localSideDimension) / (this.parameters.minimumThumbnailCount * this.thumbnailDimension.width), n = e / (this.thumbnailDimension.width * t), (n = i / (this.thumbnailDimension.width * t)) < this.dots.length / this.group && (n = e / (this.thumbnailDimension.width * t))), this.ratio !== t && ((h = {}).width = Math.floor(this.thumbnailDimension.width * t - this.thumbnailDimension.widthMargin - this.thumbnailDimension.widthBorder), h.height = Math.floor((this.thumbnailDimension.height - this.parameters.captionSize) * t - this.thumbnailDimension.heightMargin + this.parameters.captionSize - this.thumbnailDimension.heightBorder), this.dots.css(h), s = this.dots.width() / this.thumbnailDimension.widthLocal, (h = {}).width = Math.ceil(this.imageDimension.width * s), h.height = Math.ceil(this.imageDimension.height * s), this.images.css(h), this.bar.css("height", "auto"), this.ratio = t), this.itemsPerPane = Math.floor(n), r = this.slider.responsive.dynamicHeightSlide || this.slider.currentSlide, this.currentI = r.index, this.scroller.css(n2const.rtl.left, this.getScrollerTargetLeft(this.getPaneByIndex(this.currentI))), this.scroller.css("width", this.thumbnailDimension.width * this.ratio * Math.ceil(this.dots.length / this.group))), this.scroller.height(this.thumbnailDimension.height * this.ratio * this.group), this.bar.height(this.scroller.outerHeight(!0)))
                            });
                            return t.prototype.onDotClick = function (t) {
                                nextend.shouldPreventClick || (e || (this.slider.directionalChangeToReal(l(t.currentTarget).data("slide").index), e = !0), setTimeout(function () {
                                    e = !1
                                }.bind(this), 400))
                            }, t.prototype.onSlideSwitch = function (t, i) {
                                this.activateDots(i.index), this.goToDot(this.slider.getRealIndex(i.index))
                            }, t.prototype.activateDots = function (t) {
                                this.dots.removeClass("n2-active");
                                for (var i = this.slider.slides[t].slides, e = 0; i.length > e; e++)i[e].$thumbnail.addClass("n2-active")
                            }, t.prototype.resetPane = function () {
                                this.goToDot(this.currentI)
                            }, t.prototype.previousPane = function () {
                                this.goToDot(this.currentI - this.itemsPerPane * this.group)
                            }, t.prototype.nextPane = function () {
                                this.goToDot(this.currentI + this.itemsPerPane * this.group)
                            }, t.prototype.getPaneByIndex = function (t) {
                                return t = Math.max(0, Math.min(this.dots.length - 1, t)), this.parameters.invertGroupDirection ? Math.floor(t % Math.ceil(this.dots.length / this.group) / this.itemsPerPane) : Math.floor(t / this.group / this.itemsPerPane)
                            }, t.prototype.getScrollerTargetLeft = function (t) {
                                this.lastScrollerWidth = this.getScrollerWidth();
                                var i = 0;
                                t === Math.floor((this.dots.length - 1) / this.group / this.itemsPerPane) ? (i = -(t * this.itemsPerPane * this.thumbnailDimension.width * this.ratio), 0 === t ? this.previous.removeClass("n2-active") : this.previous.addClass("n2-active"), this.next.removeClass("n2-active")) : (0 < t ? (i = -(t * this.itemsPerPane * this.thumbnailDimension.width * this.ratio - this.localSideDimension), this.previous.addClass("n2-active")) : (i = 0, this.previous.removeClass("n2-active")), this.next.addClass("n2-active"));
                                var e = this.getScrollerWidth(), s = this.thumbnailDimension.width * this.ratio * Math.ceil(this.dots.length / this.group);
                                return e < s && (i = Math.max(i, -(s - e))), i
                            }, t.prototype.goToDot = function (t) {
                                this.tween && this.tween.progress() < 1 && this.tween.pause();
                                var i = {};
                                i[n2const.rtl.left] = this.getScrollerTargetLeft(this.getPaneByIndex(t)), this.tween = NextendTween.to(this.scroller, .5, i), this.currentI = t
                            }, t.prototype.onVisibleRealSlidesChanged = function () {
                                this.dots.remove(), this.renderThumbnails(), this.lastScrollerWidth = 0
                            }, t.prototype.getScrollerWidth = function () {
                                return this.widthPercent ? Math.ceil(this.slider.responsive.resizeContext.sliderWidth * this.widthPercent / 100) - this.horizontalSpacing : this.bar.width()
                            }, t.prototype.getSize = function () {
                                return 0
                            }, t.prototype.refreshSliderSize = function (t) {
                            }, t
                        });
                        new N2Classes.SmartSliderWidgetThumbnailDefaultHorizontal(this, {
                            "area": 12,
                            "action": "click",
                            "minimumThumbnailCount": 1.5,
                            "group": 1,
                            "invertGroupDirection": 0,
                            "captionSize": 0,
                            "orientation": "horizontal",
                            "slideStyle": "n2-style-462cb0983aca6cda3fc34a0feea6024b-dot ",
                            "containerStyle": "width: 120px; height: 81px;",
                            "thumbnail": {"width": 120, "height": 81, "code": "background-size: cover;"}
                        })
                    }
                })
            })
        });</script>
</head>

<body data-rsssl=1
      class="home page-template page-template-100-width page-template-100-width-php page page-id-1679 fusion-image-hovers fusion-body ltr fusion-sticky-header no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-flyout fusion-show-pagination-text fusion-header-layout-v2 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">

<div id="wrapper" class="">
    <div id="home" style="position:relative;top:-1px;"></div>

    <header class="fusion-header-wrapper">
        <div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-  fusion-mobile-menu-design-flyout fusion-header-has-flyout-menu">

            <div class="fusion-secondary-header">
                <div class="fusion-row">
                    <div class="fusion-alignleft">
                        <div class="fusion-contact-info">Website chính thức dự án | Hotline1: 0918 960 888 | Hotline2:
                            0936 291 567<span class="fusion-header-separator">|</span><a
                                    href="mailto:in&#102;o&#64;tnr&#103;ra&#110;&#100;p&#97;&#108;&#97;&#99;e&#116;ha&#105;&#98;in&#104;.&#99;&#111;m">in&#102;o&#64;tnr&#103;ra&#110;&#100;p&#97;&#108;&#97;&#99;e&#116;ha&#105;&#98;in&#104;.&#99;&#111;m</a>
                        </div>
                    </div>
                    <div class="fusion-alignright">
                        <div class="fusion-social-links-header">
                            <div class="fusion-social-networks">
                                <div class="fusion-social-networks-wrapper"><a
                                            class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook"
                                            style="color:#e2b95d;"
                                            href="https://www.facebook.com/chungcumipecrubik360xuanthuy.com.vn"
                                            target="_blank" data-placement="bottom" data-title="Facebook"
                                            data-toggle="tooltip" title="Facebook"><span class="screen-reader-text">Facebook</span></a><a
                                            class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube"
                                            style="color:#e2b95d;" href="https://youtu.be/K6LTLgnkkxQ" target="_blank"
                                            rel="noopener noreferrer" data-placement="bottom" data-title="YouTube"
                                            data-toggle="tooltip" title="YouTube"><span class="screen-reader-text">YouTube</span></a><a
                                            class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail"
                                            style="color:#e2b95d;"
                                            href="mailto:&#110;guy&#101;&#110;quan&#103;hoa.&#098;d&#115;&#064;&#103;ma&#105;l&#046;c&#111;m"
                                            target="_self" rel="noopener noreferrer" data-placement="bottom"
                                            data-title="Email" data-toggle="tooltip" title="Email"><span
                                                class="screen-reader-text">Email</span></a><a
                                            class="custom fusion-social-network-icon fusion-tooltip fusion-custom fusion-icon-custom"
                                            style="color:#e2b95d;position:relative;" href="http://zalo.me/0936 291 567"
                                            target="_blank" rel="noopener noreferrer" data-placement="bottom" data-title
                                            data-toggle="tooltip" title><span class="screen-reader-text"></span><img
                                                src="wp-content/uploads/2019/11/logo-zalo-75x75.jpg"
                                                style="width:auto;max-height:16px;" alt=""/></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fusion-header-sticky-height"></div>
            <div class="fusion-header">
                <div class="fusion-row">
                    <div class="fusion-header-has-flyout-menu-content">
                        <div class="fusion-logo" data-margin-top="1px" data-margin-bottom="1px" data-margin-left="0px"
                             data-margin-right="0px">
                            <a class="fusion-logo-link" href="index.html">

                                <!-- standard logo -->
                                <img src="wp-content/uploads/2020/06/3.-logo-menu-1.png"
                                     srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/3.-logo-menu-1.png 1x"
                                     width="99" height="50" alt="TNR Holdings Việt Nam Logo" retina_logo_url=""
                                     class="fusion-standard-logo"/>


                            </a>
                        </div>
                        <nav class="fusion-main-menu" aria-label="Main Menu">
                            <ul role="menubar" id="menu-menu-trang-chu" class="fusion-menu">
                                <li role="menuitem" id="menu-item-1784"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1679 current_page_item menu-item-1784">
                                    <a href="index.html" class="fusion-flex-link fusion-bar-highlight"><span
                                                class="fusion-megamenu-icon"><i
                                                    class="glyphicon fa-accusoft fab"></i></span><span
                                                class="menu-text">TRANG CHỦ</span></a></li>
                                <li role="menuitem" id="menu-item-1023"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1023"><a
                                            href="index.html#tienich"
                                            class="fusion-flex-link fusion-bar-highlight"><span
                                                class="fusion-megamenu-icon"><i
                                                    class="glyphicon fa-heart fas"></i></span><span class="menu-text">TIỆN ÍCH</span></a>
                                </li>
                                <li role="menuitem" id="menu-item-395"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-395"><a
                                            href="index.html#matbang"
                                            class="fusion-flex-link fusion-bar-highlight"><span
                                                class="fusion-megamenu-icon"><i
                                                    class="glyphicon fa-cube fas"></i></span><span class="menu-text">MẶT BẰNG</span></a>
                                </li>
                                <li role="menuitem" id="menu-item-400"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-400"><a
                                            href="index.html#sanpham"
                                            class="fusion-flex-link fusion-bar-highlight"><span
                                                class="fusion-megamenu-icon"><i class="glyphicon fa-chart-line fas"></i></span><span
                                                class="menu-text">GIÁ BÁN</span></a></li>
                                <li role="menuitem" id="menu-item-397"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-397"><a
                                            href="index.html#chinhsach"
                                            class="fusion-flex-link fusion-bar-highlight"><span
                                                class="fusion-megamenu-icon"><i
                                                    class="glyphicon fa-donate fas"></i></span><span class="menu-text">ƯU ĐÃI, CHIẾT KHẤU</span></a>
                                </li>
                                <li role="menuitem" id="menu-item-1025"
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1025"><a
                                            href="index.html#lienhe" class="fusion-flex-link fusion-bar-highlight"><span
                                                class="fusion-megamenu-icon"><i
                                                    class="glyphicon fa-user-tie fas"></i></span><span
                                                class="menu-text">LIÊN HỆ</span></a></li>
                                <li role="menuitem" id="menu-item-1791"
                                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1791">
                                    <a href="index.html#chudautu" class="fusion-bar-highlight"><span class="menu-text">CHỦ ĐẦU TƯ</span></a>
                                </li>
                            </ul>
                        </nav>
                        <div class="fusion-flyout-menu-icons fusion-flyout-mobile-menu-icons">


                            <a class="fusion-flyout-menu-toggle" aria-hidden="true" aria-label="Toggle Menu"
                               href="index.html#">
                                <div class="fusion-toggle-icon-line"></div>
                                <div class="fusion-toggle-icon-line"></div>
                                <div class="fusion-toggle-icon-line"></div>
                            </a>
                        </div>


                        <div class="fusion-flyout-menu-bg"></div>

                        <nav class="fusion-mobile-nav-holder fusion-flyout-menu fusion-flyout-mobile-menu"></nav>

                    </div>
                </div>
            </div>
        </div>
        <div class="fusion-clearfix"></div>
    </header>


    <div id="sliders-container">
    </div>


    <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
        <div class="fusion-row" style="max-width:100%;">
            <section id="content" class="full-width">
                <div id="post-1679" class="post-1679 page type-page status-publish hentry">
                    <span class="entry-title rich-snippet-hidden">Mở bán liền kề, biệt thự Tnr Grand Palace Thái Bình</span><span
                            class="vcard rich-snippet-hidden"><span class="fn"><a
                                    href="https://tnrgrandpalacethaibinh.com/author/admintnrgrandpalacethaibinh.com/"
                                    title="Posts by admintnrgrandpalacethaibinh.com" rel="author">admintnrgrandpalacethaibinh.com</a></span></span><span
                            class="updated rich-snippet-hidden">2020-07-02T03:04:47+00:00</span>
                    <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"
                             style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;'>
                            <div class="fusion-builder-row fusion-row ">
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                     style='margin-top:0px;margin-bottom:0px;'>
                                    <div class="fusion-column-wrapper"
                                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                         data-bg-url="">
                                        <div class="fusion-text">
                                            <p>
                                                <video id="video-player" preload="metadata" autoplay="autoplay"
                                                       loop="loop" muted="" width="100%" height="auto">
                                                    <source src="wp-content/uploads/2020/07/video-oke.mp4"
                                                            type="video/mp4"/>
                                                </video>
                                            </p>
                                        </div>
                                        <div class="fusion-clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tongquan">
                            <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
                                 style='background-color: rgba(255,255,255,0);background-image: url("https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nen-tong-quan-2.png");background-position: right top;background-repeat: no-repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 22px;background-image: url('wp-content/uploads/2020/06/Viụ2581n-tong-quan-1-1.png');background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/Viền-tong-quan-1-1.png">
                                            <div class="fusion-text"><p><a href="index.html"><span
                                                                style="color: #000000; font-family: times new roman, times, serif; font-size: x-large;"><span
                                                                    style="caret-color: #000000;"><b>TNR Grand
                                                                    Palace</b></span></span></a><br/>
                                                    <span style="color: #009485;"><strong><em><span
                                                                        style="font-size: 24px;">Thông tin tổng quan dự án</span></em></strong></span><br/>
                                                    <img class="alignnone size-full wp-image-860"
                                                         src="wp-content/uploads/2020/01/gach-chann-duoi-1.jpg" alt=""
                                                         width="162" height="3"
                                                         srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1-150x3.jpg 150w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg 162w"
                                                         sizes="(max-width: 162px) 100vw, 162px"/></p>
                                            </div>
                                            <div class="fusion-text"><p><span
                                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Tên thương mại: <a
                                                                href="index.html">Tnr Grand Palace Thái
                                                            Bình</a></span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Chủ đầu tư: Công ty CP Bất động sản Mỹ</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Đơn vị phát triển dự án : Công ty Cổ phần Đầu tư &amp; Phát triển Bất động sản TNR Holdings Việt Nam</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Tổng diện tích dự án: 7,2 ha</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Diện tích khu đất 7,1285 ha</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Đất ở: 29,878m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Đất nhà văn hoá: 632m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Đất chung cư nhà ở xã hội: 9.240m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Đất xây trường mầm non: 2.264m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Đất cây xanh: 5.417m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Đất đường giao thông: 23.152m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"> + Đất khe kỹ thuật: 702m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Phát triển với các loại hình sản phẩm:</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Liền kề: 65 lô diện tích 80m2; 87m2; 104m2; Xây 3 tầng 1 tum</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Shophouse: 67 lô diện tích: 80m2; 98m2; 101m2; 139m2; 210m2; Xây 4 tầng 1 tum</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Biệt thự song lập: Phong cách Hy Lạp diện tích: 180m2; 208m2; 210m2; 220m2</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Bàn giao: Dự kiến Tháng 04 năm 2022</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><span
                                                                style="color: #440202;">♦</span> Sở hữu: Sổ đỏ lâu dài</span>
                                                </p>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 15px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text">
                                                <div class="n2-section-smartslider fitvidsignore " role="region"
                                                     aria-label="Slider">
                                                    <style>div#n2-ss-7 {
                                                            width: 1000px;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slider-1 {
                                                            position: relative;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slider-background-video-container {
                                                            position: absolute;
                                                            left: 0;
                                                            top: 0;
                                                            width: 100%;
                                                            height: 100%;
                                                            overflow: hidden;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slider-2 {
                                                            position: relative;
                                                            overflow: hidden;
                                                            padding: 0px 0px 0px 0px;
                                                            height: 900px;
                                                            border: 0px solid RGBA(62, 62, 62, 1);
                                                            border-radius: 0px;
                                                            background-clip: padding-box;
                                                            background-repeat: repeat;
                                                            background-position: 50% 50%;
                                                            background-size: cover;
                                                            background-attachment: scroll;
                                                        }

                                                        div#n2-ss-7.n2-ss-mobileLandscape .n2-ss-slider-2, div#n2-ss-7.n2-ss-mobilePortrait .n2-ss-slider-2 {
                                                            background-attachment: scroll;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slider-3 {
                                                            position: relative;
                                                            width: 100%;
                                                            height: 100%;
                                                            overflow: hidden;
                                                            outline: 1px solid rgba(0, 0, 0, 0);
                                                            z-index: 10;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slide-backgrounds, div#n2-ss-7 .n2-ss-slider-3 > .n-particles-js-canvas-el, div#n2-ss-7 .n2-ss-slider-3 > .n2-ss-divider {
                                                            position: absolute;
                                                            left: 0;
                                                            top: 0;
                                                            width: 100%;
                                                            height: 100%;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slide-backgrounds {
                                                            z-index: 10;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slider-3 > .n-particles-js-canvas-el {
                                                            z-index: 12;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slide-backgrounds > * {
                                                            overflow: hidden;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slide {
                                                            position: absolute;
                                                            top: 0;
                                                            left: 0;
                                                            width: 100%;
                                                            height: 100%;
                                                            z-index: 20;
                                                            display: block;
                                                            -webkit-backface-visibility: hidden;
                                                        }

                                                        div#n2-ss-7 .n2-ss-layers-container {
                                                            position: relative;
                                                            width: 1000px;
                                                            height: 900px;
                                                        }

                                                        div#n2-ss-7 .n2-ss-parallax-clip > .n2-ss-layers-container {
                                                            position: absolute;
                                                            right: 0;
                                                        }

                                                        div#n2-ss-7 .n2-ss-slide {
                                                            perspective: 1500px;
                                                        }

                                                        div#n2-ss-7[data-ie] .n2-ss-slide {
                                                            perspective: none;
                                                            transform: perspective(1500px);
                                                        }

                                                        div#n2-ss-7 .n2-ss-slide-active {
                                                            z-index: 21;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow {
                                                            cursor: pointer;
                                                            overflow: hidden;
                                                            line-height: 0 !important;
                                                            z-index: 20;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow img {
                                                            position: relative;
                                                            min-height: 0;
                                                            min-width: 0;
                                                            vertical-align: top;
                                                            width: auto;
                                                            height: auto;
                                                            max-width: 100%;
                                                            max-height: 100%;
                                                            display: inline;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow img.n2-arrow-hover-img {
                                                            display: none;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow:HOVER img.n2-arrow-hover-img {
                                                            display: inline;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow:HOVER img.n2-arrow-normal-img {
                                                            display: none;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated {
                                                            overflow: hidden;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated > div {
                                                            position: relative;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated .n2-active {
                                                            position: absolute;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-fade {
                                                            transition: background 0.3s, opacity 0.4s;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-horizontal > div {
                                                            transition: all 0.4s;
                                                            left: 0;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-horizontal .n2-active {
                                                            top: 0;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-previous.nextend-arrow-animated-horizontal:HOVER > div, div#n2-ss-7 .nextend-arrow-previous.nextend-arrow-animated-horizontal:FOCUS > div, div#n2-ss-7 .nextend-arrow-next.nextend-arrow-animated-horizontal .n2-active {
                                                            left: -100%;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-previous.nextend-arrow-animated-horizontal .n2-active, div#n2-ss-7 .nextend-arrow-next.nextend-arrow-animated-horizontal:HOVER > div, div#n2-ss-7 .nextend-arrow-next.nextend-arrow-animated-horizontal:FOCUS > div {
                                                            left: 100%;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow.nextend-arrow-animated-horizontal:HOVER .n2-active, div#n2-ss-7 .nextend-arrow.nextend-arrow-animated-horizontal:FOCUS .n2-active {
                                                            left: 0;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-vertical > div {
                                                            transition: all 0.4s;
                                                            top: 0;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-vertical .n2-active {
                                                            left: 0;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-vertical .n2-active {
                                                            top: -100%;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-vertical:HOVER > div, div#n2-ss-7 .nextend-arrow-animated-vertical:FOCUS > div {
                                                            top: 100%;
                                                        }

                                                        div#n2-ss-7 .nextend-arrow-animated-vertical:HOVER .n2-active, div#n2-ss-7 .nextend-arrow-animated-vertical:FOCUS .n2-active {
                                                            top: 0;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default {
                                                            display: flex;
                                                            user-select: none;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default, div#n2-ss-7 .nextend-thumbnail-inner {
                                                            overflow: hidden;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-inner {
                                                            flex: 1 1 auto;
                                                            display: flex;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-inner {
                                                            flex-flow: column;
                                                        }

                                                        div#n2-ss-7 .n2-ss-thumb-image {
                                                            display: flex;
                                                            flex: 0 0 auto;
                                                            align-items: center;
                                                            justify-content: center;
                                                            background-position: center center;
                                                            background-size: cover;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-button {
                                                            position: absolute;
                                                            z-index: 2;
                                                            transition: all 0.4s;
                                                            opacity: 0;
                                                            cursor: pointer;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-button {
                                                            left: 50%;
                                                            margin-left: -13px !important;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-previous {
                                                            top: -26px;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-previous.n2-active {
                                                            top: 10px;
                                                            opacity: 1;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-next {
                                                            bottom: -26px;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-next.n2-active {
                                                            bottom: 10px;
                                                            opacity: 1;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-button {
                                                            top: 50%;
                                                            margin-top: -13px !important;
                                                            transform: rotateZ(-90deg);
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-previous {
                                                            left: -26px;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-previous.n2-active {
                                                            left: 10px;
                                                            opacity: 1;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-next {
                                                            right: -26px;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-next.n2-active {
                                                            right: 10px;
                                                            opacity: 1;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller {
                                                            flex: 1 0 auto;
                                                            position: relative;
                                                            box-sizing: border-box !important;
                                                            white-space: nowrap;
                                                            display: flex;
                                                            flex-direction: column;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-scroller {
                                                            flex-direction: column;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-scroller {
                                                            flex-direction: row;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-start .nextend-thumbnail-scroller-group {
                                                            justify-content: flex-start;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-center .nextend-thumbnail-scroller-group {
                                                            justify-content: center;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-end .nextend-thumbnail-scroller-group {
                                                            justify-content: flex-end;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-space-around .nextend-thumbnail-scroller-group {
                                                            justify-content: space-around;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-space-between .nextend-thumbnail-scroller-group {
                                                            justify-content: space-between;
                                                        }

                                                        html[dir="rtl"] div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller {
                                                            position: relative;
                                                            float: right;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-scroller-group {
                                                            display: flex;
                                                            flex-flow: column;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-scroller-group {
                                                            display: flex;
                                                            flex-flow: row;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller .nextend-thumbnail-scroller-group > div {
                                                            display: flex;
                                                            position: relative;
                                                            flex: 0 0 auto;
                                                            box-sizing: content-box !important;
                                                            cursor: pointer;
                                                            overflow: hidden;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-scroller .nextend-thumbnail-scroller-group > div {
                                                            flex-flow: column;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller .nextend-thumbnail-scroller-group > div.n2-active {
                                                            cursor: default;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default .n2-ss-caption {
                                                            display: inline-block;
                                                            white-space: normal;
                                                            box-sizing: border-box !important;
                                                            overflow: hidden;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default .n2-caption-overlay {
                                                            position: absolute;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-default .n2-caption-overlay div {
                                                            float: left;
                                                            clear: left;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-horizontal .n2-ss-caption {
                                                            display: block;
                                                        }

                                                        div#n2-ss-7 .nextend-thumbnail-vertical .n2-caption-after, div#n2-ss-7 .nextend-thumbnail-vertical .n2-caption-before {
                                                            height: 100%;
                                                        }

                                                        div#n2-ss-7 .n2-style-df32cb3742fdac43eb0251df7d67c185-simple {
                                                            background: #242424;
                                                            opacity: 1;
                                                            padding: 3px 3px 3px 3px;
                                                            box-shadow: none;
                                                            border-width: 0px;
                                                            border-style: solid;
                                                            border-color: #000000;
                                                            border-color: RGBA(0, 0, 0, 1);
                                                            border-radius: 0px;
                                                        }

                                                        div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot {
                                                            background: RGBA(0, 0, 0, 0);
                                                            opacity: 1;
                                                            padding: 0px 0px 0px 0px;
                                                            box-shadow: none;
                                                            border-width: 0px;
                                                            border-style: solid;
                                                            border-color: #ffffff;
                                                            border-color: RGBA(255, 255, 255, 0);
                                                            border-radius: 0px;
                                                            opacity: 0.4;
                                                            margin: 3px;
                                                            transition: all 0.4s;
                                                            background-size: cover;
                                                        }

                                                        div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot.n2-active, div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot:HOVER, div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot:FOCUS {
                                                            border-width: 0px;
                                                            border-style: solid;
                                                            border-color: #ffffff;
                                                            border-color: RGBA(255, 255, 255, 0.8);
                                                            opacity: 1;
                                                        }</style>
                                                    <div id="n2-ss-7-align" class="n2-ss-align">
                                                        <div class="n2-padding">
                                                            <div id="n2-ss-7" data-creator="Smart Slider 3"
                                                                 class="n2-ss-slider n2-ow n2-has-hover n2notransition  n2-ss-load-fade "
                                                                 style="font-size: 1rem;" data-fontsize="16">
                                                                <div class="n2-ss-slider-1 n2_ss__touch_element n2-ow"
                                                                     style="">
                                                                    <div class="n2-ss-slider-2 n2-ow" style="">
                                                                        <div class="n2-ss-slider-3 n2-ow" style="">

                                                                            <div class="n2-ss-slide-backgrounds"></div>
                                                                            <div data-first="1" data-slide-duration="0"
                                                                                 data-id="149"
                                                                                 data-thumbnail="https://tnrgrandpalacethaibinh.com/wp-content/uploads/resized/39cad28195b1de412f4adcb378dff8a4/phoi-canh-2-tnr-grand-palace-dong-my-thai-binh.jpg"
                                                                                 style=""
                                                                                 class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-149">
                                                                                <div class="n2-ss-slide-background n2-ow"
                                                                                     data-mode="fill">
                                                                                    <div data-hash="3909c1fb277238a38599bb75f3869ce1"
                                                                                         data-desktop="//tnrgrandpalacethaibinh.com/wp-content/uploads/resized/a2886d2ecc85f44f0becd925f8197f9e/phoi-canh-2-tnr-grand-palace-dong-my-thai-binh.jpg"
                                                                                         class="n2-ss-slide-background-image"
                                                                                         data-blur="0" data-alt=""
                                                                                         data-title=""></div>
                                                                                </div>
                                                                                <div class="n2-ss-layers-container n2-ow">
                                                                                    <div class="n2-ss-layer n2-ow"
                                                                                         style="padding:10px 10px 10px 10px;"
                                                                                         data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                                         data-sstype="slide"
                                                                                         data-csstextalign="center"
                                                                                         data-pm="default"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-slide-duration="0" data-id="150"
                                                                                 data-thumbnail="https://tnrgrandpalacethaibinh.com/wp-content/uploads/resized/3a7c127fc70c1aa5b3b92d51ac25cd04/phoi-canh-tnr-grand-palace-dong-my-thai-binh.png"
                                                                                 style=""
                                                                                 class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-150">
                                                                                <div class="n2-ss-slide-background n2-ow"
                                                                                     data-mode="fill">
                                                                                    <div data-hash="378cab0e6f57a8ab62bd9aa92f2365e7"
                                                                                         data-desktop="//tnrgrandpalacethaibinh.com/wp-content/uploads/resized/b60c5a068381d2d5544068c91376fdd6/phoi-canh-tnr-grand-palace-dong-my-thai-binh.png"
                                                                                         class="n2-ss-slide-background-image"
                                                                                         data-blur="0" data-alt=""
                                                                                         data-title=""></div>
                                                                                </div>
                                                                                <div class="n2-ss-layers-container n2-ow">
                                                                                    <div class="n2-ss-layer n2-ow"
                                                                                         style="padding:10px 10px 10px 10px;"
                                                                                         data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                                         data-sstype="slide"
                                                                                         data-csstextalign="center"
                                                                                         data-pm="default"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-slide-duration="0" data-id="152"
                                                                                 data-thumbnail="https://tnrgrandpalacethaibinh.com/wp-content/uploads/resized/47a955f7b67206731cee15e7bfcf06b8/BT.jpg"
                                                                                 style=""
                                                                                 class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-152">
                                                                                <div class="n2-ss-slide-background n2-ow"
                                                                                     data-mode="fill">
                                                                                    <div data-hash="2dbe7b927b631effde40786773a95abf"
                                                                                         data-desktop="//tnrgrandpalacethaibinh.com/wp-content/uploads/resized/9b88a34450b00e60fd93e0c2c2847082/BT.jpg"
                                                                                         class="n2-ss-slide-background-image"
                                                                                         data-blur="0" data-alt=""
                                                                                         data-title=""></div>
                                                                                </div>
                                                                                <div class="n2-ss-layers-container n2-ow">
                                                                                    <div class="n2-ss-layer n2-ow"
                                                                                         style="padding:10px 10px 10px 10px;"
                                                                                         data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                                         data-sstype="slide"
                                                                                         data-csstextalign="center"
                                                                                         data-pm="default"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-slide-duration="0" data-id="153"
                                                                                 data-thumbnail="https://tnrgrandpalacethaibinh.com/wp-content/uploads/resized/75431541f233d69b920847515319fe3d/Nhà-VH.jpg"
                                                                                 style=""
                                                                                 class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-153">
                                                                                <div class="n2-ss-slide-background n2-ow"
                                                                                     data-mode="fill">
                                                                                    <div data-hash="2459b4d09a1b8c36537674f68c9738a7"
                                                                                         data-desktop="//tnrgrandpalacethaibinh.com/wp-content/uploads/resized/95d2ca3ed6c7bc8f4a7e662b015e353a/Nhà-VH.jpg"
                                                                                         class="n2-ss-slide-background-image"
                                                                                         data-blur="0" data-alt=""
                                                                                         data-title=""></div>
                                                                                </div>
                                                                                <div class="n2-ss-layers-container n2-ow">
                                                                                    <div class="n2-ss-layer n2-ow"
                                                                                         style="padding:10px 10px 10px 10px;"
                                                                                         data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                                         data-sstype="slide"
                                                                                         data-csstextalign="center"
                                                                                         data-pm="default"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-slide-duration="0" data-id="154"
                                                                                 data-thumbnail="https://tnrgrandpalacethaibinh.com/wp-content/uploads/resized/5e0390675064d129d3b093b725539fb1/SH.jpg"
                                                                                 style=""
                                                                                 class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-154">
                                                                                <div class="n2-ss-slide-background n2-ow"
                                                                                     data-mode="fill">
                                                                                    <div data-hash="9304819a241b0b82a99b1859eca32998"
                                                                                         data-desktop="//tnrgrandpalacethaibinh.com/wp-content/uploads/resized/7e26baea57c50136590373aab4dc501d/SH.jpg"
                                                                                         class="n2-ss-slide-background-image"
                                                                                         data-blur="0" data-alt=""
                                                                                         data-title=""></div>
                                                                                </div>
                                                                                <div class="n2-ss-layers-container n2-ow">
                                                                                    <div class="n2-ss-layer n2-ow"
                                                                                         style="padding:10px 10px 10px 10px;"
                                                                                         data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                                         data-sstype="slide"
                                                                                         data-csstextalign="center"
                                                                                         data-pm="default"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-slide-duration="0" data-id="155"
                                                                                 data-thumbnail="https://tnrgrandpalacethaibinh.com/wp-content/uploads/resized/71991fe50ba0a388c5eaae7e99c1cea8/Trường-MN.jpg"
                                                                                 style=""
                                                                                 class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-155">
                                                                                <div class="n2-ss-slide-background n2-ow"
                                                                                     data-mode="fill">
                                                                                    <div data-hash="9eec2b8226608eb91e93cdd6805656bd"
                                                                                         data-desktop="//tnrgrandpalacethaibinh.com/wp-content/uploads/resized/66360e5faffd8a5e8f36c78a0b914a7b/Trường-MN.jpg"
                                                                                         class="n2-ss-slide-background-image"
                                                                                         data-blur="0" data-alt=""
                                                                                         data-title=""></div>
                                                                                </div>
                                                                                <div class="n2-ss-layers-container n2-ow">
                                                                                    <div class="n2-ss-layer n2-ow"
                                                                                         style="padding:10px 10px 10px 10px;"
                                                                                         data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                                         data-sstype="slide"
                                                                                         data-csstextalign="center"
                                                                                         data-pm="default"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div data-slide-duration="0" data-id="156"
                                                                                 data-thumbnail="https://tnrgrandpalacethaibinh.com/wp-content/uploads/resized/56f5141676c169850a933dabe36b03b3/Vườn-Nhật.jpg"
                                                                                 style=""
                                                                                 class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-156">
                                                                                <div class="n2-ss-slide-background n2-ow"
                                                                                     data-mode="fill">
                                                                                    <div data-hash="6aecbd3574028416dd257b78d0f5e186"
                                                                                         data-desktop="//tnrgrandpalacethaibinh.com/wp-content/uploads/resized/1305eaf944ca295cdf71af80fed7cc40/Vườn-Nhật.jpg"
                                                                                         class="n2-ss-slide-background-image"
                                                                                         data-blur="0" data-alt=""
                                                                                         data-title=""></div>
                                                                                </div>
                                                                                <div class="n2-ss-layers-container n2-ow">
                                                                                    <div class="n2-ss-layer n2-ow"
                                                                                         style="padding:10px 10px 10px 10px;"
                                                                                         data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                                         data-sstype="slide"
                                                                                         data-csstextalign="center"
                                                                                         data-pm="default"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div data-ssleft="0+15"
                                                                         data-sstop="sliderHeight/2-previousheight/2"
                                                                         id="n2-ss-7-arrow-previous"
                                                                         class="n2-ss-widget nextend-arrow n2-ow nextend-arrow-previous  nextend-arrow-animated-fade n2-ib"
                                                                         style="position: absolute;" role="button"
                                                                         aria-label="previous arrow" tabindex="0"><img
                                                                                class="n2-ow" data-no-lazy="1"
                                                                                data-hack="data-lazy-src"
                                                                                src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxwYXRoIGQ9Ik0xMS40MzMgMTUuOTkyTDIyLjY5IDUuNzEyYy4zOTMtLjM5LjM5My0xLjAzIDAtMS40Mi0uMzkzLS4zOS0xLjAzLS4zOS0xLjQyMyAwbC0xMS45OCAxMC45NGMtLjIxLjIxLS4zLjQ5LS4yODUuNzYtLjAxNS4yOC4wNzUuNTYuMjg0Ljc3bDExLjk4IDEwLjk0Yy4zOTMuMzkgMS4wMy4zOSAxLjQyNCAwIC4zOTMtLjQuMzkzLTEuMDMgMC0xLjQybC0xMS4yNTctMTAuMjkiCiAgICAgICAgICBmaWxsPSIjZmZmZmZmIiBvcGFjaXR5PSIwLjgiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPgo8L3N2Zz4="
                                                                                alt="previous arrow"/></div>
                                                                    <div data-ssright="0+15"
                                                                         data-sstop="sliderHeight/2-nextheight/2"
                                                                         id="n2-ss-7-arrow-next"
                                                                         class="n2-ss-widget nextend-arrow n2-ow nextend-arrow-next  nextend-arrow-animated-fade n2-ib"
                                                                         style="position: absolute;" role="button"
                                                                         aria-label="next arrow" tabindex="0"><img
                                                                                class="n2-ow" data-no-lazy="1"
                                                                                data-hack="data-lazy-src"
                                                                                src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxwYXRoIGQ9Ik0xMC43MjIgNC4yOTNjLS4zOTQtLjM5LTEuMDMyLS4zOS0xLjQyNyAwLS4zOTMuMzktLjM5MyAxLjAzIDAgMS40MmwxMS4yODMgMTAuMjgtMTEuMjgzIDEwLjI5Yy0uMzkzLjM5LS4zOTMgMS4wMiAwIDEuNDIuMzk1LjM5IDEuMDMzLjM5IDEuNDI3IDBsMTIuMDA3LTEwLjk0Yy4yMS0uMjEuMy0uNDkuMjg0LS43Ny4wMTQtLjI3LS4wNzYtLjU1LS4yODYtLjc2TDEwLjcyIDQuMjkzeiIKICAgICAgICAgIGZpbGw9IiNmZmZmZmYiIG9wYWNpdHk9IjAuOCIgZmlsbC1ydWxlPSJldmVub2RkIi8+Cjwvc3ZnPg=="
                                                                                alt="next arrow"/></div>
                                                                </div>
                                                                <div data-position="below" data-offset="0"
                                                                     data-width-percent="100"
                                                                     class="n2-ss-widget nextend-thumbnail nextend-thumbnail-default n2-ow nextend-thumbnail-horizontal"
                                                                     style="margin-top:0px;width:100%;"><img
                                                                            class="nextend-thumbnail-button nextend-thumbnail-previous n2-ow"
                                                                            style="width:26px;margin-top:-13px!important;"
                                                                            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMjYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgICA8ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxjaXJjbGUgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIG9wYWNpdHk9Ii41IiBmaWxsPSIjMDAwIiBjeD0iMTMiIGN5PSIxMyIgcj0iMTIiLz4KICAgICAgICA8cGF0aCBkPSJNMTMuNDM1IDkuMTc4Yy0uMTI2LS4xMjEtLjI3LS4xODItLjQzNi0uMTgyLS4xNjQgMC0uMzA2LjA2MS0uNDI4LjE4MmwtNC4zOCA0LjE3NWMtLjEyNi4xMjEtLjE4OC4yNjItLjE4OC40MjQgMCAuMTYxLjA2Mi4zMDIuMTg4LjQyM2wuNjUuNjIyYy4xMjYuMTIxLjI3My4xODIuNDQxLjE4Mi4xNyAwIC4zMTQtLjA2MS40MzYtLjE4MmwzLjMxNC0zLjE2MSAzLjI0OSAzLjE2MWMuMTI2LjEyMS4yNjkuMTgyLjQzMi4xODIuMTY0IDAgLjMwNy0uMDYxLjQzMy0uMTgybC42NjItLjYyMmMuMTI2LS4xMjEuMTg5LS4yNjIuMTg5LS40MjMgMC0uMTYyLS4wNjMtLjMwMy0uMTg5LS40MjRsLTQuMzczLTQuMTc1eiIKICAgICAgICAgICAgICBmaWxsPSIjZmZmIi8+CiAgICA8L2c+Cjwvc3ZnPg=="
                                                                            alt="previous arrow"/><img
                                                                            class="nextend-thumbnail-button nextend-thumbnail-next n2-ow n2-active"
                                                                            style="width:26px;margin-top:-13px!important;"
                                                                            src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNiIgaGVpZ2h0PSIyNiI+CiAgICA8ZyBmaWxsPSJub25lIj4KICAgICAgICA8Y2lyY2xlIGN4PSIxMyIgY3k9IjEzIiByPSIxMiIgZmlsbD0iIzAwMCIgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIG9wYWNpdHk9Ii41Ii8+CiAgICAgICAgPHBhdGggZmlsbD0iI2ZmZiIKICAgICAgICAgICAgICBkPSJNMTIuNTY1IDE2LjgyMmMuMTI2LjEyLjI3LjE4Mi40MzYuMTgyLjE2OCAwIC4zMS0uMDYuNDMtLjE4Mmw0LjM4LTQuMTc1Yy4xMjgtLjEyLjE5LS4yNjIuMTktLjQyNCAwLS4xNi0uMDYyLS4zMDItLjE5LS40MjNsLS42NS0uNjIyYy0uMTI1LS4xMi0uMjcyLS4xODItLjQ0LS4xODItLjE3IDAtLjMxNC4wNi0uNDM2LjE4MmwtMy4zMTQgMy4xNi0zLjI1LTMuMTZjLS4xMjYtLjEyLS4yNy0uMTgyLS40My0uMTgyLS4xNjYgMC0uMzEuMDYtLjQzNS4xODJsLS42NjIuNjIyYy0uMTI2LjEyLS4xOS4yNjItLjE5LjQyMyAwIC4xNjIuMDY0LjMwMy4xOS40MjRsNC4zNzMgNC4xNzV6Ii8+CiAgICA8L2c+Cjwvc3ZnPg=="
                                                                            alt="next arrow"/>
                                                                    <div class="nextend-thumbnail-inner n2-ow n2-style-df32cb3742fdac43eb0251df7d67c185-simple ">
                                                                        <div class="nextend-thumbnail-scroller n2-ow n2-align-content-start"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="n2-ss-7-spinner" style="display: none;">
                                                                <div>
                                                                    <div class="n2-ss-spinner-simple-white-container">
                                                                        <div class="n2-ss-spinner-simple-white"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="n2_clear"></div>
                                                    <div id="n2-ss-7-placeholder"
                                                         style="position: relative;z-index:2;background-color:RGBA(0,0,0,0); background-color:RGBA(255,255,255,0);">
                                                        <img style="width: 100%; max-width:10000px; display: block;opacity:0;margin:0px;"
                                                             class="n2-ow"
                                                             src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAwIiBoZWlnaHQ9IjkwMCIgPjwvc3ZnPg=="
                                                             alt="Slider"/></div>
                                                </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="vitri">
                            <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"
                                 style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;border-top-width:1px;border-bottom-width:1px;border-color:#eae9e9;border-top-style:solid;border-bottom-style:solid;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                                         style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                                     style='margin-top: 0px;margin-bottom: 10px;'>
                                                    <div class="fusion-column-wrapper"
                                                         style="background-color:#009485;padding: 15px 0px 5px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                                         data-bg-url="">
                                                        <div class="fusion-text"><p><span
                                                                        style="font-family: 'times new roman', times, serif; font-size: 14pt; color: #ffffff;"><strong>VỊ
                                                                        TRÍ TRUNG TÂM HÀNH CHÍNH &#8211; VĂN
                                                                        HOÁ</strong></span><br/>
                                                                <span style="font-size: 14pt; color: #ffffff;"><strong><span
                                                                                style="font-family: helvetica, arial, sans-serif;">Thành phố Thái Bình</span></strong></span>
                                                            </p>
                                                            <p>
                                                                <span style="font-family: arial, helvetica, sans-serif; color: #ffffff; font-size: 12pt;"><a
                                                                            style="color: #ffffff;"
                                                                            href="index.html"><strong>TNR Grand
                                                                            Palace</strong></a> là dự án được mong chờ nhất Thái Bình, với vị trí kim cương trung tâm khu hành chính – văn hóa mới của thành phố. Dự án toạ lạc tại mặt đường Võ Nguyên Giáp và Hưng Long được đầu tư bài bản, kĩ lưỡng về hạ tầng, cảnh quan, mang những tư tưởng mới mẻ thổi hồn vào mảnh đất vàng giữa trung tâm TP Thái Bình thịnh vượng</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><a
                                                        href="wp-content/uploads/2020/06/vi-tri-gif-1.gif"
                                                        class="fusion-lightbox"
                                                        data-rel="iLightbox[ba07846865775267496]"
                                                        data-title="vi-tri-gif-1" title="vi-tri-gif-1"><img
                                                            src="wp-content/uploads/2020/06/vi-tri-gif-1.gif"
                                                            width="1200" height="697" alt=""
                                                            class="img-responsive wp-image-1701"/></a></span>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                                         style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img
                                                        src="wp-content/uploads/2020/06/vi-tri-thuc-te.gif" width="1000"
                                                        height="484" alt="" title="vi-tri-thuc-te"
                                                        class="img-responsive wp-image-1704"/></span>
                                            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                                     style='margin-top: 0px;margin-bottom: 10px;'>
                                                    <div class="fusion-column-wrapper"
                                                         style="background-color:#fefaef;padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                                         data-bg-url="">
                                                        <div class="fusion-text"><p><span
                                                                        style="color: #008c7d;"><strong><span
                                                                                style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">Cửa ngõ giao thương trục phát triển kinh tế</span></strong></span>
                                                            </p>
                                                            <p><span style="color: #008c7d;"><strong><span
                                                                                style="font-family: arial, helvetica, sans-serif; font-size: 14pt;"> “Hà Nam – Nam Định – Thái Bình – Hải Phòng – Quảng Ninh” </span></strong></span>
                                                            </p>
                                                            <ul>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">2km là đến trung tâm Hành chính tỉnh</span>
                                                                </li>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">500m là đến quảng trường Hồ Chí Minh</span>
                                                                </li>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">800m là đến chợ đầu mối</span>
                                                                </li>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">3km là đến trường Đại Học Y Thái Bình</span>
                                                                </li>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">4km là đến Big C</span>
                                                                </li>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">3km là đến Vincom</span>
                                                                </li>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">4km là đến Bệnh Viện Đa Khoa Tỉnh</span>
                                                                </li>
                                                                <li>
                                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">6km là đến bến xe Thái Bình</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tienich">
                            <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
                                 style='background-color: #f5f5f5;background-position: left top;background-repeat: repeat;padding-top:40px;padding-right:2px;padding-bottom:0px;padding-left:2px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                         style='margin-top:0px;margin-bottom:10px;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text"><p style="text-align: center;"><span
                                                            style="color: #000000;"><span
                                                                style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">ĐƯỢC PHÁT TRIỂN THEO MÔ HÌNH THÀNH PHỐ THÔNG MINH</span></span><br/>
                                                    <strong><span style="color: #008c7d;"><span
                                                                    style="font-family: 'times new roman', times, serif; font-size: x-large;"><span
                                                                        style="caret-color: #000000;">VỚI HỆ THỐNG TIỆN ÍCH ĐỒNG BỘ</span></span></span></strong>
                                                </p>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 10% ) * 0.5 ) );margin-right: 10%;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-title title fusion-title-size-one"
                                                 style="margin-top:0px;margin-bottom:0px;"><h1
                                                        class="title-heading-left"><span
                                                            style="color: #008c7d;"><strong><span
                                                                    style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">DANH SÁCH TIỆN ÍCH</span></strong></span>
                                                </h1>
                                                <div class="title-sep-container">
                                                    <div class="title-sep sep-single sep-dashed"
                                                         style="border-color:#e2b95d;"></div>
                                                </div>
                                            </div>
                                            <div class="fusion-text"><p><span
                                                            style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;"><strong><span
                                                                    style="color: #800000;">(1)</span></strong> Khu công viên ánh sáng với hệ thống cây ánh sáng trung tâm</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;"><strong><span
                                                                    style="color: #800000;">(2)</span></strong> Sử dụng công nghệ kết nối thông minh (smart connect) kết nối toàn bộ cư dân</span>
                                                </p>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 10% ) * 0.5 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 20px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text"><p><span
                                                            style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;"><strong><span
                                                                    style="color: #800000;">(3)</span></strong> Phố đi bộ 3D, khu vui chơi trẻ em Kid Worlds</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;"><strong><span
                                                                    style="color: #800000;">(4)</span></strong> Tháp cây ánh sáng năng lượng mặt trời</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;"><span
                                                                style="color: #800000;"><strong>(5)</strong></span> Khu tiện ích vườn Nhật …</span>
                                                </p>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="imageframe-liftup"><span
                                                        class="fusion-imageframe imageframe-none imageframe-3"><img
                                                            src="wp-content/uploads/2020/06/Vưụ259Dn-Nhật-1.jpg"
                                                            width="1000" height="551" alt="" title="Vườn-Nhật"
                                                            class="img-responsive wp-image-1715"
                                                            srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/Vườn-Nhật-1-200x110.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/Vườn-Nhật-1-400x220.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/Vườn-Nhật-1-600x331.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/Vườn-Nhật-1-800x441.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/Vườn-Nhật-1.jpg 1000w"
                                                            sizes="(max-width: 800px) 100vw, 1000px"/></span></div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-column-inner-bg-wrapper 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                                        <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;"
                                             data-bg-url="">
                                            <div class="imageframe-liftup"><span
                                                        class="fusion-imageframe imageframe-none imageframe-4"><img
                                                            src="wp-content/uploads/2020/06/nha-van-hoa.jpg"
                                                            width="1000" height="551" alt="" title="nha-van-hoa"
                                                            class="img-responsive wp-image-1713"
                                                            srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-van-hoa-200x110.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-van-hoa-400x220.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-van-hoa-600x331.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-van-hoa-800x441.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-van-hoa.jpg 1000w"
                                                            sizes="(max-width: 800px) 100vw, 1000px"/></span></div>
                                            <div class="fusion-clearfix"></div>
                                        </div>
					<span class="fusion-column-inner-bg hover-type-liftup">
						<a aria-label="">
                            <span class="fusion-column-inner-bg-image"
                                  style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"></span></a>
					</span>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-column-inner-bg-wrapper 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                                        <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;"
                                             data-bg-url="">
                                            <span class="fusion-imageframe imageframe-none imageframe-5 hover-type-none"><img
                                                        src="wp-content/uploads/2020/06/tien-ich-quang-truong-anh-sang.jpg"
                                                        width="1000" height="551" alt=""
                                                        title="tien-ich-quang-truong-anh-sang"
                                                        class="img-responsive wp-image-1714"
                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/tien-ich-quang-truong-anh-sang-200x110.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/tien-ich-quang-truong-anh-sang-400x220.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/tien-ich-quang-truong-anh-sang-600x331.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/tien-ich-quang-truong-anh-sang-800x441.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/tien-ich-quang-truong-anh-sang.jpg 1000w"
                                                        sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                            <div class="fusion-clearfix"></div>
                                        </div>
					<span class="fusion-column-inner-bg hover-type-liftup">
						<a aria-label="">
                            <span class="fusion-column-inner-bg-image"
                                  style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"></span></a>
					</span>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-column-inner-bg-wrapper 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                                        <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;"
                                             data-bg-url="">
                                            <span class="fusion-imageframe imageframe-none imageframe-6 hover-type-none"><img
                                                        src="wp-content/uploads/2020/06/nha-tre.jpg" width="1000"
                                                        height="551" alt="" title="nha-tre"
                                                        class="img-responsive wp-image-1712"
                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-tre-200x110.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-tre-400x220.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-tre-600x331.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-tre-800x441.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/nha-tre.jpg 1000w"
                                                        sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                            <div class="fusion-clearfix"></div>
                                        </div>
					<span class="fusion-column-inner-bg hover-type-liftup">
						<a aria-label="">
                            <span class="fusion-column-inner-bg-image"
                                  style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"></span></a>
					</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="matbang">
                            <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"
                                 style='background-color: rgba(255,255,255,0);background-position: left top;background-repeat: repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-blend-mode fusion-spacing-no 1_4"
                                         style='margin-top:0px;margin-bottom:0px;width:25%;width:calc(25% - ( ( 0% ) * 0.25 ) );margin-right: 0%;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-color:#009485;padding: 10px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text"><p><a href="index.html"><span
                                                                style="font-family: 'times new roman', times, serif; font-size: 18pt; color: #ffffff;">TNR GRAND PALACE</span></a><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 24pt; color: #ffffff;"><strong>MẶT
                                                            BẰNG TỔNG THỂ</strong></span>
                                                <div class="fusion-sep-clear"></div>
                                                <div class="fusion-separator sep-single sep-solid"
                                                     style="border-color:#ffffff;border-top-width:3px;margin-top:-10px;margin-bottom:15px;width:100%;max-width:20%;"></div>
                                                </p>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_4  fusion-three-fourth fusion-column-last fusion-spacing-no 3_4"
                                         style='margin-top:0px;margin-bottom:0px;width:75%;width:calc(75% - ( ( 0% ) * 0.75 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 10px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-title title fusion-title-size-three"
                                                 style="margin-top:0px;margin-bottom:0px;">
                                                <div class="title-sep-container">
                                                    <div class="title-sep sep-single sep-solid"
                                                         style="border-color:#009485;"></div>
                                                </div>
                                                <h3 class="title-heading-right"><span
                                                            style="font-family: arial, helvetica, sans-serif; font-size: 14pt; color: #440202;">WELCOME TO TNR GRAND PALACE</span>
                                                </h3></div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                         style='margin-top:0px;margin-bottom:0px;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <span class="fusion-imageframe imageframe-none imageframe-7 hover-type-none"><img
                                                        src="wp-content/uploads/2020/06/mat-bang-tong-file-net-scaled.jpg"
                                                        width="2560" height="1767" alt="" title="mat-bang-tong-file-net"
                                                        class="img-responsive wp-image-1725"
                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/mat-bang-tong-file-net-200x138.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/mat-bang-tong-file-net-400x276.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/mat-bang-tong-file-net-600x414.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/mat-bang-tong-file-net-800x552.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/mat-bang-tong-file-net-1200x828.jpg 1200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/mat-bang-tong-file-net-scaled.jpg 2560w"
                                                        sizes="(max-width: 800px) 100vw, 2560px"/></span>
                                            <div class="fusion-sep-clear"></div>
                                            <div class="fusion-separator fusion-full-width-sep sep-none"
                                                 style="margin-left: auto;margin-right: auto;margin-top:10px;margin-bottom:10px;"></div>
                                            <div class="fusion-button-wrapper fusion-aligncenter">
                                                <style type="text/css"
                                                       scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {
                                                        color: #ffffff;
                                                    }

                                                    .fusion-button.button-1 {
                                                        border-width: 0px;
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-1 .fusion-button-icon-divider {
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-1.button-3d {
                                                        -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                                        -moz-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                                        box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                                    }

                                                    .button-1.button-3d:active {
                                                        -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                                        -moz-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                                        box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                                    }

                                                    .fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i, .fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i, .fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active {
                                                        color: #ffffff;
                                                    }

                                                    .fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active {
                                                        border-width: 0px;
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider {
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-1 {
                                                        background: #ff1f1c;
                                                    }

                                                    .fusion-button.button-1:hover, .button-1:focus, .fusion-button.button-1:active {
                                                        background: #d176b0;
                                                        background-image: -webkit-gradient(linear, left bottom, left top, from(#16a6c1), to(#d176b0));
                                                        background-image: -webkit-linear-gradient(bottom, #16a6c1, #d176b0);
                                                        background-image: -moz-linear-gradient(bottom, #16a6c1, #d176b0);
                                                        background-image: -o-linear-gradient(bottom, #16a6c1, #d176b0);
                                                        background-image: linear-gradient(to top, #16a6c1, #d176b0);
                                                    }

                                                    .fusion-button.button-1 {
                                                        width: auto;
                                                    }</style>
                                                <a class="fusion-button button-3d fusion-button-square button-large button-custom button-1 popmake-318"
                                                   target="_self"><span
                                                            class="fusion-button-icon-divider button-icon-divider-left"><i
                                                                class="fa-download fas"></i></span><span
                                                            class="fusion-button-text fusion-button-text-left">Tải xuống mặt bằng tổng thể (bản gốc)</span></a>
                                            </div>
                                            <div class="fusion-sep-clear"></div>
                                            <div class="fusion-separator fusion-full-width-sep sep-none"
                                                 style="margin-left: auto;margin-right: auto;margin-top:10px;margin-bottom:10px;"></div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="sanpham">
                            <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
                                 style='background-color: rgba(255,255,255,0);background-image: url("wp-content/uploads/2020/06/patten-san-pham.png");background-position: center top;background-repeat: no-repeat;padding-top:0px;padding-bottom:0px;border-top-width:2px;border-bottom-width:2px;border-color:#008c7d;border-top-style:solid;border-bottom-style:solid;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"
                                         style='margin-top:0px;margin-bottom:10px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right: 4%;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 20px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text"><p><span
                                                            style="font-size: 24pt; color: #008c7d;"><strong><span
                                                                    style="font-family: arial, helvetica, sans-serif;">CÁC LOẠI SẢN PHẨM DỰ ÁN</span></strong></span><br/>
                                                    <a href="index.html"><span style="font-size: 18pt; color: #008c7d;"><strong><span
                                                                        style="font-family: 'times new roman', times, serif;">TNR GRAND PALACE</span></strong></span></a>
                                                </p>
                                                <div class="fusion-sep-clear"></div>
                                                <div class="fusion-separator sep-single sep-solid"
                                                     style="border-color:#440202;border-top-width:4px;margin-top:;width:100%;max-width:20%;"></div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"
                                         style='margin-top:0px;margin-bottom:10px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 20px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text"><p><span style="color: #440202;"><em><span
                                                                    style="font-size: 14pt;">Để lại thông tin chúng tôi sẽ gọi lại ngay!</span></em></span>
                                                </p>
                                            </div>
                                            <div class="fusion-text">
                                                <div role="form" class="wpcf7" id="wpcf7-f930-p1679-o13" lang="en-US"
                                                     dir="ltr">
                                                    <div class="screen-reader-response" aria-live="polite"></div>
                                                    <form action="index.html#wpcf7-f930-p1679-o13" method="post"
                                                          class="wpcf7-form" novalidate="novalidate">
                                                        <div style="display: none;">
                                                            <input type="hidden" name="_wpcf7" value="930"/>
                                                            <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                                            <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                                            <input type="hidden" name="_wpcf7_unit_tag"
                                                                   value="wpcf7-f930-p1679-o13"/>
                                                            <input type="hidden" name="_wpcf7_container_post"
                                                                   value="1679"/>
                                                        </div>
                                                        <p><label> Số điện thoại (*)<br/>
                                                                <span class="wpcf7-form-control-wrap your-subject"><input
                                                                            type="text" name="your-subject" value=""
                                                                            size="40"
                                                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                            aria-required="true" aria-invalid="false"/></span>
                                                            </label></p>
                                                        <div class="one_fourth">
                                                            <input type="submit" value="GỬI ĐI"
                                                                   class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                                                        </div>
                                                        <div class="clear_column"></div>
                                                        <input type='hidden' class='wpcf7-pum'
                                                               value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                                        <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                                             style="background-color:;color:;border-color:;border-width:1px;">
                                                            <button style="color:;border-color:;" type="button"
                                                                    class="close toggle-alert" data-dismiss="alert"
                                                                    aria-hidden="true">&times;</button>
                                                            <div class="fusion-alert-content-wrapper"><span
                                                                        class="fusion-alert-content"></span></div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling fusion-equal-height-columns"
                             style='background-color: rgba(255,255,255,0);background-position: left top;background-repeat: repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;margin-top: 20px;'>
                            <div class="fusion-builder-row fusion-row ">
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"
                                     style='margin-top:0px;margin-bottom:10px;width:33.33%;width:calc(33.33% - ( ( 0.5% + 0.5% ) * 0.3333 ) );margin-right: 0.5%;'>
                                    <div class="fusion-column-wrapper"
                                         style="border:1px solid #440202;padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                         data-bg-url="">
                                        <span class="fusion-imageframe imageframe-none imageframe-8 hover-type-zoomin"><img
                                                    src="wp-content/uploads/2020/06/1.-lien-ke.jpg" width="1000"
                                                    height="557" alt="" title="1.-lien-ke"
                                                    class="img-responsive wp-image-1727"
                                                    srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-lien-ke-200x111.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-lien-ke-400x223.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-lien-ke-600x334.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-lien-ke-800x446.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-lien-ke.jpg 1000w"
                                                    sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                        <div class="fusion-text">
                                            <hr/>
                                            <p style="text-align: center;"><span style="color: #800000;"><strong><span
                                                                style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">NHÀ LIỀN KỀ</span></strong></span>
                                            </p>
                                            <p style="text-align: center;"><strong><span style="color: #800000;">KIẾN TRÚC PHÁP HOA LỆ</span></strong>
                                            </p>
                                            <hr/>
                                        </div>
                                        <div class="fusion-text">
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                                                        gốc:</strong> <del>liên hệ vnđ</del></span></p>
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                                                        bán từ: <span
                                                                style="font-size: 14pt; color: #ff0000;">Liên hệ </span></strong>vnđ</span>
                                            </p>
                                            <hr/>
                                        </div>
                                        <div class="fusion-text"><p><span
                                                        style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">Giá trên đã bao gồm Vat + Kpbt</span>
                                            </p>
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Diện tích: 80m2; 87,56m2; 104,99M2</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Hướng cửa vào: Dãy LK1, LK2 hướng đông</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Hướng cửa vào: Dãy LK3, Lk4 hướng Tây</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Mặt tiền: 5m</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Mặt đường 13m</span>
                                            </p>
                                        </div>
                                        <div class="fusion-text">
                                            <hr/>
                                            <p style="text-align: center;"><span
                                                        style="font-size: 12pt; font-family: arial, helvetica, sans-serif; color: #ff0000;">Để lại thông tin, chúng tôi sẽ gọi lại ngay!</span>
                                            </p>
                                        </div>
                                        <div class="fusion-text">
                                            <div role="form" class="wpcf7" id="wpcf7-f930-p1679-o14" lang="en-US"
                                                 dir="ltr">
                                                <div class="screen-reader-response" aria-live="polite"></div>
                                                <form action="index.html#wpcf7-f930-p1679-o14" method="post"
                                                      class="wpcf7-form" novalidate="novalidate">
                                                    <div style="display: none;">
                                                        <input type="hidden" name="_wpcf7" value="930"/>
                                                        <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                                        <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                                        <input type="hidden" name="_wpcf7_unit_tag"
                                                               value="wpcf7-f930-p1679-o14"/>
                                                        <input type="hidden" name="_wpcf7_container_post" value="1679"/>
                                                    </div>
                                                    <p><label> Số điện thoại (*)<br/>
                                                            <span class="wpcf7-form-control-wrap your-subject"><input
                                                                        type="text" name="your-subject" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                        aria-required="true"
                                                                        aria-invalid="false"/></span> </label></p>
                                                    <div class="one_fourth">
                                                        <input type="submit" value="GỬI ĐI"
                                                               class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                                                    </div>
                                                    <div class="clear_column"></div>
                                                    <input type='hidden' class='wpcf7-pum'
                                                           value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                                    <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                                         style="background-color:;color:;border-color:;border-width:1px;">
                                                        <button style="color:;border-color:;" type="button"
                                                                class="close toggle-alert" data-dismiss="alert"
                                                                aria-hidden="true">&times;</button>
                                                        <div class="fusion-alert-content-wrapper"><span
                                                                    class="fusion-alert-content"></span></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="fusion-clearfix"></div>

                                    </div>
                                </div>
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"
                                     style='margin-top:0px;margin-bottom:10px;width:33.33%;width:calc(33.33% - ( ( 0.5% + 0.5% ) * 0.3333 ) );margin-right: 0.5%;'>
                                    <div class="fusion-column-wrapper"
                                         style="border:1px solid #440202;padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                         data-bg-url="">
                                        <span class="fusion-imageframe imageframe-none imageframe-9 hover-type-zoomin"><img
                                                    src="wp-content/uploads/2020/06/shophouse.jpg" width="1000"
                                                    height="557" alt="" title="shophouse"
                                                    class="img-responsive wp-image-1729"
                                                    srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/shophouse-200x111.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/shophouse-400x223.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/shophouse-600x334.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/shophouse-800x446.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/shophouse.jpg 1000w"
                                                    sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                        <div class="fusion-text">
                                            <hr/>
                                            <p style="text-align: center;"><span style="color: #800000;"><strong><span
                                                                style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">NHÀ PHỐ SHOPHOUSE</span></strong></span>
                                            </p>
                                            <p style="text-align: center;"><span style="color: #800000;"><strong>KIẾN
                                                        TRÚC PHÁP HOA LỆ</strong></span></p>
                                            <hr/>
                                        </div>
                                        <div class="fusion-text">
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                                                        gốc từ:</strong> <del>Liên hệ vnđ</del></span></p>
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                                                        bán từ: <span
                                                                style="font-size: 14pt; color: #ff0000;">Liên hệ </span></strong>vnđ</span>
                                            </p>
                                            <hr/>
                                        </div>
                                        <div class="fusion-text"><p><span
                                                        style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">Giá trên đã bao gồm Vat, kpbt</span>
                                            </p>
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Diện tích: 80m2; 101m2; 110m2; 139m2</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Hướng cửa vào: LK1,2 hướng tây</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Hướng ban công: LK3,4 hướng Đông</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Mặt tiền: 5m</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Mặt đường: 64m</span>
                                            </p>
                                        </div>
                                        <div class="fusion-text">
                                            <hr/>
                                            <p style="text-align: center;"><span
                                                        style="font-size: 12pt; font-family: arial, helvetica, sans-serif; color: #ff0000;">Để lại thông tin, chúng tôi sẽ gọi lại ngay!</span>
                                            </p>
                                        </div>
                                        <div class="fusion-text">
                                            <div role="form" class="wpcf7" id="wpcf7-f930-p1679-o15" lang="en-US"
                                                 dir="ltr">
                                                <div class="screen-reader-response" aria-live="polite"></div>
                                                <form action="index.html#wpcf7-f930-p1679-o15" method="post"
                                                      class="wpcf7-form" novalidate="novalidate">
                                                    <div style="display: none;">
                                                        <input type="hidden" name="_wpcf7" value="930"/>
                                                        <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                                        <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                                        <input type="hidden" name="_wpcf7_unit_tag"
                                                               value="wpcf7-f930-p1679-o15"/>
                                                        <input type="hidden" name="_wpcf7_container_post" value="1679"/>
                                                    </div>
                                                    <p><label> Số điện thoại (*)<br/>
                                                            <span class="wpcf7-form-control-wrap your-subject"><input
                                                                        type="text" name="your-subject" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                        aria-required="true"
                                                                        aria-invalid="false"/></span> </label></p>
                                                    <div class="one_fourth">
                                                        <input type="submit" value="GỬI ĐI"
                                                               class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                                                    </div>
                                                    <div class="clear_column"></div>
                                                    <input type='hidden' class='wpcf7-pum'
                                                           value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                                    <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                                         style="background-color:;color:;border-color:;border-width:1px;">
                                                        <button style="color:;border-color:;" type="button"
                                                                class="close toggle-alert" data-dismiss="alert"
                                                                aria-hidden="true">&times;</button>
                                                        <div class="fusion-alert-content-wrapper"><span
                                                                    class="fusion-alert-content"></span></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="fusion-clearfix"></div>

                                    </div>
                                </div>
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"
                                     style='margin-top:0px;margin-bottom:10px;width:33.33%;width:calc(33.33% - ( ( 0.5% + 0.5% ) * 0.3333 ) );'>
                                    <div class="fusion-column-wrapper"
                                         style="border:1px solid #440202;padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                         data-bg-url="">
                                        <span class="fusion-imageframe imageframe-none imageframe-10 hover-type-zoomin"><img
                                                    src="wp-content/uploads/2020/06/biet-thu-song-lap.jpg" width="1000"
                                                    height="557" alt="" title="biet-thu-song-lap"
                                                    class="img-responsive wp-image-1728"
                                                    srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/biet-thu-song-lap-200x111.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/biet-thu-song-lap-400x223.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/biet-thu-song-lap-600x334.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/biet-thu-song-lap-800x446.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/biet-thu-song-lap.jpg 1000w"
                                                    sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                        <div class="fusion-text">
                                            <hr/>
                                            <p style="text-align: center;"><span
                                                        style="color: #800000; font-size: 13pt;"><strong><span
                                                                style="font-family: arial, helvetica, sans-serif;">BIỆT THỰ SONG LẬP</span></strong></span>
                                            </p>
                                            <p style="text-align: center;"><span
                                                        style="color: #800000; font-size: 13pt;"><strong>KIẾN TRÚC PHÁP
                                                        HOA LỆ</strong></span></p>
                                            <hr/>
                                        </div>
                                        <div class="fusion-text">
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                                                        gốc:</strong> <del>Liên hệ vnđ</del></span></p>
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                                                        bán từ: <span
                                                                style="font-size: 14pt; color: #ff0000;">Liên hệ </span></strong>vnđ</span>
                                            </p>
                                            <hr/>
                                        </div>
                                        <div class="fusion-text"><p><span
                                                        style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">Giá trên đã bao gồm Vat, Kpbt</span>
                                            </p>
                                            <p>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Diện tích: 180m2; 210m2; 225m2</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Hướng cửa vào: BT1,3 bắc hoặc Nam</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Hướng ban công: BT2,5 Bắc hoặc Nam</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Mặt tiền: 10m; 12m</span><br/>
                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">♦ Mặt đường: 13m; 22m</span>
                                            </p>
                                        </div>
                                        <div class="fusion-text">
                                            <hr/>
                                            <p style="text-align: center;"><span
                                                        style="font-size: 12pt; font-family: arial, helvetica, sans-serif; color: #ff0000;">Để lại thông tin, chúng tôi sẽ gọi lại ngay!</span>
                                            </p>
                                        </div>
                                        <div class="fusion-text">
                                            <div role="form" class="wpcf7" id="wpcf7-f930-p1679-o16" lang="en-US"
                                                 dir="ltr">
                                                <div class="screen-reader-response" aria-live="polite"></div>
                                                <form action="index.html#wpcf7-f930-p1679-o16" method="post"
                                                      class="wpcf7-form" novalidate="novalidate">
                                                    <div style="display: none;">
                                                        <input type="hidden" name="_wpcf7" value="930"/>
                                                        <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                                        <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                                        <input type="hidden" name="_wpcf7_unit_tag"
                                                               value="wpcf7-f930-p1679-o16"/>
                                                        <input type="hidden" name="_wpcf7_container_post" value="1679"/>
                                                    </div>
                                                    <p><label> Số điện thoại (*)<br/>
                                                            <span class="wpcf7-form-control-wrap your-subject"><input
                                                                        type="text" name="your-subject" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                        aria-required="true"
                                                                        aria-invalid="false"/></span> </label></p>
                                                    <div class="one_fourth">
                                                        <input type="submit" value="GỬI ĐI"
                                                               class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                                                    </div>
                                                    <div class="clear_column"></div>
                                                    <input type='hidden' class='wpcf7-pum'
                                                           value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                                    <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                                         style="background-color:;color:;border-color:;border-width:1px;">
                                                        <button style="color:;border-color:;" type="button"
                                                                class="close toggle-alert" data-dismiss="alert"
                                                                aria-hidden="true">&times;</button>
                                                        <div class="fusion-alert-content-wrapper"><span
                                                                    class="fusion-alert-content"></span></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="fusion-clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="chinhsach">
                            <div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling fusion-equal-height-columns"
                                 style='background-color: #e3f1f3;background-image: url("wp-content/uploads/2020/05/patten-200x200-1.png");background-position: center center;background-repeat: repeat;padding-top:30px;padding-right:5px;padding-left:5px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 50px 0px 0px 20px;background-position:center bottom;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text"><p><a href="index.html"><span
                                                                style="font-family: 'times new roman', times, serif; font-size: x-large; color: #008c7d;"><b>TNR
                                                                GRAND PALACE</b></span></a><br/>
                                                    <strong><em><span style="font-size: 24px;">Ưu đãi, chiết khấu mua nhà tháng 07/2020</span></em></strong><br/>
                                                    <img class="alignnone size-full wp-image-860"
                                                         src="wp-content/uploads/2020/01/gach-chann-duoi-1.jpg" alt=""
                                                         width="162" height="3"
                                                         srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1-150x3.jpg 150w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg 162w"
                                                         sizes="(max-width: 162px) 100vw, 162px"/></p>
                                            </div>
                                            <div class="fusion-text">
                                                <div class="page" title="Page 4">
                                                    <div class="layoutArea">
                                                        <div class="column">
                                                            <p>
                                                                <span style="font-size: 12pt; color: #000000;"><strong><span
                                                                                style="font-family: arial, helvetica, sans-serif;">(1) TIẾN ĐỘ THANH TOÁN THÔNG THƯỜNG (dự kiến)</span></strong></span><br/>
                                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đặt cọc: 100 triệu đồng</span><br/>
                                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 1: 30% Giá trị đất đã gồm tiền đặt cọc (ký Hợp đồng đặt chỗ thuận sau 7 ngày đặt cọc)</span><br/>
                                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 2: 20% giá trị đất (12/08/2021)</span><br/>
                                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 3: 20% giá trị đất (11/11/2021)</span><br/>
                                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 4: 20% giá trị đất (03/04/2022)</span><br/>
                                                                <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 5: 5% Giá trị đất (Khi nhận bàn giao đất)</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p><span style="font-size: 12pt; color: #000000;"><strong><span
                                                                    style="font-family: arial, helvetica, sans-serif;">(2) TIẾN ĐỘ THANH TOÁN TIỀN XÂY DỰNG NHÀ Ở (dự kiến)</span></strong></span>
                                                </p>
                                                <p>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đặt cọc: 100 triệu đồng</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 1: 30% Giá trị Xây dựng nhà ở (Ký hợp đồng mua bán dự kiến 18/07/2021)</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 2: 20% giá trị nhà ( Thi công xong tầng 2 12/08/2021)</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 3: 20% giá trị nhà (Thi công đổ mái xong tầng 5 11/11/2021)</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 4: 20% giá trị nhà (Bàn giao nhà ở 03/04/2022)</span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt; color: #000000;">  ♦ Đợt 5: 5% Giá trị đất (Khi nhận bàn giao đất)</span>
                                                </p>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-title title fusion-title-size-one"
                                                 style="margin-top:0px;margin-bottom:0px;">
                                                <div class="title-sep-container">
                                                    <div class="title-sep sep-single sep-solid"
                                                         style="border-color:#353535;"></div>
                                                </div>
                                                <h1 class="title-heading-right"><span
                                                            style="color: #008c7d;"><strong><span
                                                                    style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">Ưu đãi chiết khấu mua nhà</span></strong></span>
                                                </h1></div>
                                            <span class="fusion-imageframe imageframe-none imageframe-11 hover-type-none"><img
                                                        src="wp-content/uploads/2020/06/1.-Khung-1.jpg" width="1349"
                                                        height="1005" alt="" title="1.-Khung-1"
                                                        class="img-responsive wp-image-1736"
                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-Khung-1-200x149.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-Khung-1-400x298.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-Khung-1-600x447.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-Khung-1-800x596.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-Khung-1-1200x894.jpg 1200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/1.-Khung-1.jpg 1349w"
                                                        sizes="(max-width: 800px) 100vw, 1349px"/></span>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="chinhsach">
                            <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
                                 style='background-color: #e3f1f3;background-image: url("wp-content/uploads/2020/05/patten-200x200-1.png");background-position: center center;background-repeat: repeat;padding-right:5px;padding-bottom:40px;padding-left:5px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                         style='margin-top:0px;margin-bottom:10px;'>
                                        <div class="fusion-column-wrapper"
                                             style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"
                                                     style='margin-top: 0px;margin-bottom: 10px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                    <div class="fusion-column-wrapper"
                                                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                                         data-bg-url="">
                                                        <div class="imageframe-liftup"><span
                                                                    class="fusion-imageframe imageframe-none imageframe-12"><img
                                                                        src="wp-content/uploads/2020/07/trang-11.jpg"
                                                                        width="1000" height="1377" alt=""
                                                                        title="trang-11"
                                                                        class="img-responsive wp-image-1796"
                                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-11-200x275.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-11-400x551.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-11-600x826.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-11-800x1102.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-11.jpg 1000w"
                                                                        sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3"
                                                     style='margin-top: 0px;margin-bottom: 10px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right:4%;'>
                                                    <div class="fusion-column-wrapper"
                                                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                                         data-bg-url="">
                                                        <div class="imageframe-liftup"><span
                                                                    class="fusion-imageframe imageframe-none imageframe-13"><img
                                                                        src="wp-content/uploads/2020/07/trang-22.jpg"
                                                                        width="1000" height="1377" alt=""
                                                                        title="trang-22"
                                                                        class="img-responsive wp-image-1797"
                                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-22-200x275.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-22-400x551.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-22-600x826.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-22-800x1102.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-22.jpg 1000w"
                                                                        sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"
                                                     style='margin-top: 0px;margin-bottom: 10px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                                    <div class="fusion-column-wrapper"
                                                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                                         data-bg-url="">
                                                        <div class="imageframe-liftup"><span
                                                                    class="fusion-imageframe imageframe-none imageframe-14"><img
                                                                        src="wp-content/uploads/2020/07/trang-33.jpg"
                                                                        width="1000" height="1377" alt=""
                                                                        title="trang-33"
                                                                        class="img-responsive wp-image-1798"
                                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-33-200x275.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-33-400x551.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-33-600x826.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-33-800x1102.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/trang-33.jpg 1000w"
                                                                        sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fusion-button-wrapper fusion-aligncenter">
                                                <style type="text/css"
                                                       scoped="scoped">.fusion-button.button-2 .fusion-button-text, .fusion-button.button-2 i {
                                                        color: #ffffff;
                                                    }

                                                    .fusion-button.button-2 {
                                                        border-width: 0px;
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-2 .fusion-button-icon-divider {
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-2.button-3d {
                                                        -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                                        -moz-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                                        box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                                    }

                                                    .button-2.button-3d:active {
                                                        -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                                        -moz-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                                        box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                                    }

                                                    .fusion-button.button-2:hover .fusion-button-text, .fusion-button.button-2:hover i, .fusion-button.button-2:focus .fusion-button-text, .fusion-button.button-2:focus i, .fusion-button.button-2:active .fusion-button-text, .fusion-button.button-2:active {
                                                        color: #ffffff;
                                                    }

                                                    .fusion-button.button-2:hover, .fusion-button.button-2:focus, .fusion-button.button-2:active {
                                                        border-width: 0px;
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:active .fusion-button-icon-divider {
                                                        border-color: #ffffff;
                                                    }

                                                    .fusion-button.button-2 {
                                                        background: #ff1f1c;
                                                    }

                                                    .fusion-button.button-2:hover, .button-2:focus, .fusion-button.button-2:active {
                                                        background: #d176b0;
                                                        background-image: -webkit-gradient(linear, left bottom, left top, from(#16a6c1), to(#d176b0));
                                                        background-image: -webkit-linear-gradient(bottom, #16a6c1, #d176b0);
                                                        background-image: -moz-linear-gradient(bottom, #16a6c1, #d176b0);
                                                        background-image: -o-linear-gradient(bottom, #16a6c1, #d176b0);
                                                        background-image: linear-gradient(to top, #16a6c1, #d176b0);
                                                    }

                                                    .fusion-button.button-2 {
                                                        width: auto;
                                                    }</style>
                                                <a class="fusion-button button-3d fusion-button-square button-medium button-custom button-2 popmake-352"
                                                   target="_self"><span
                                                            class="fusion-button-icon-divider button-icon-divider-left"><i
                                                                class="fa-download fas"></i></span><span
                                                            class="fusion-button-text fusion-button-text-left">Tải xuống chính sách ưu đãi, chiết khấu từ chủ đầu tư</span></a>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="chudautu">
                            <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling fusion-equal-height-columns"
                                 style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:30px;padding-right:10px;padding-bottom:30px;padding-left:10px;'>
                                <div class="fusion-builder-row fusion-row ">
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-blend-mode 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                                        <div class="fusion-column-wrapper"
                                             style="background-color:#ffffff;padding: 8px 8px 8px 8px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-text"><p><span
                                                            style="font-family: arial, helvetica, sans-serif; font-size: 24pt; color: #008c7d;"><strong>CHỦ
                                                            ĐẦU TƯ UY TÍN</strong></span><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 14pt; color: #440202;">&#8220;Tập đoàn kinh tế tư nhân đa ngành hàng đầu Việt Nam &#8220;</span>
                                                </p>
                                                <p>
                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000;">Công ty Cổ phần Đầu tư &amp; Phát triển Bất động sản TNR Holdings Việt Nam (gọi tắt là &#8220;TNR Holdings&#8221;), là một thành viên của Tập đoàn Đầu tư TNG vốn được biết đến là một trong số những tập đoàn kinh tế tư nhân đa ngành hàng đầu của Việt Nam với hơn 20 năm hoạt động trên các lĩnh vực đầu tư phát triển khu công nghiệp.</span>
                                                </p>
                                                <p>
                                                    <span style="font-family: arial, helvetica, sans-serif; color: #000000;">TNR Holdings quy tụ những nhân sự lãnh đạo và quản lý có tầm nhìn, cùng đội ngũ nhân viên trẻ, chuyên nghiệp, có nhiều kỹ năng và kinh nghiệm trong lĩnh vực bất động sản. Với danh mục hoạt động đa dạng bao gồm: dự án khu dân cư, dự án thương mại và khu nghỉ dưỡng, TNR Holdings hướng tới phát triển chuyên nghiệp và bền vững trong lĩnh vực Đầu tư &#8211; Quản lý &#8211; Phát triển các dự án bất động sản trải rộng khắp mọi miền Việt Nam.</span>
                                                </p>
                                            </div>
                                            <span class="fusion-imageframe imageframe-none imageframe-15 hover-type-none"><img
                                                        src="wp-content/uploads/2020/06/giai-thuong-2.jpg" width="1000"
                                                        height="883" alt="" title="giai-thuong-2"
                                                        class="img-responsive wp-image-1747"
                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/giai-thuong-2-200x177.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/giai-thuong-2-400x353.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/giai-thuong-2-600x530.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/giai-thuong-2-800x706.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/giai-thuong-2.jpg 1000w"
                                                        sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-blend-mode 1_2"
                                         style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                                        <div class="fusion-column-wrapper"
                                             style="background-color:#ffffff;padding: 8px 8px 8px 8px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                             data-bg-url="">
                                            <div class="fusion-title title fusion-title-size-one"
                                                 style="margin-top:0px;margin-bottom:0px;"><h1
                                                        class="title-heading-left"><span
                                                            style="color: #800000;"><strong><span
                                                                    style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">Các dự án đã thực hiện trên toàn Quốc</span></strong></span>
                                                </h1>
                                                <div class="title-sep-container">
                                                    <div class="title-sep sep-single sep-solid"
                                                         style="border-color:#000000;"></div>
                                                </div>
                                            </div>
                                            <div class="fusion-text">
                                                <div class="page" title="Page 5">
                                                    <div class="section">
                                                        <div class="layoutArea">
                                                            <div class="column">
                                                                <p><img class="alignnone size-full wp-image-1745"
                                                                        src="wp-content/uploads/2020/06/cac-du-an-da-thuc-hien.jpg"
                                                                        alt="" width="1000" height="1263"
                                                                        srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien-200x253.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien-238x300.jpg 238w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien-400x505.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien-600x758.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien-768x970.jpg 768w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien-800x1010.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien-811x1024.jpg 811w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/cac-du-an-da-thuc-hien.jpg 1000w"
                                                                        sizes="(max-width: 1000px) 100vw, 1000px"/></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="fusion-clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling"
                             style='background-color: rgba(255,255,255,0);background-image: url("wp-content/uploads/2020/06/anh-nen-thuc-te-1.jpg");background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:40px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                            <div class="fusion-builder-row fusion-row ">
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                     style='margin-top:0px;margin-bottom:10px;'>
                                    <div class="fusion-column-wrapper"
                                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                         data-bg-url="">
                                        <div class="fusion-text"><p style="text-align: center;"><span
                                                        style="font-size: 18pt; color: #ffffff;"><strong><span
                                                                style="font-family: helvetica, arial, sans-serif;">THƯ VIỆN</span></strong></span><br/>
                                                <span style="text-align: center;"><span
                                                            style="font-size: 18pt; color: #ffffff;"><strong><span
                                                                    style="font-family: helvetica, arial, sans-serif;">HÌNH ẢNH, VIDEO REVIEW THỰC TẾ DỰ ÁN</span></strong></span></span>
                                            </p>
                                            <p style="text-align: center;"><img class="alignnone size-full wp-image-860"
                                                                                src="wp-content/uploads/2020/01/gach-chann-duoi-1.jpg"
                                                                                alt="" width="162" height="3"
                                                                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1-150x3.jpg 150w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg 162w"
                                                                                sizes="(max-width: 162px) 100vw, 162px"/>
                                            </p>
                                        </div>
                                        <style type="text/css" scoped="scoped">.fusion-gallery-1 .fusion-gallery-image {
                                                border: 2px solid #440202;
                                            }</style>
                                        <div class="fusion-gallery fusion-gallery-container fusion-grid-3 fusion-columns-total-6 fusion-gallery-layout-grid fusion-gallery-1"
                                             style="margin:-5px;">
                                            <div style="padding:5px;"
                                                 class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3">
                                                <div class="fusion-gallery-image fusion-gallery-image-liftup"><a
                                                            href="wp-content/uploads/2020/06/tnr-grand-palace-thai-binh-6.jpg"
                                                            class="fusion-lightbox"
                                                            data-rel="iLightbox[195786979bfa05db790]"><img
                                                                src="wp-content/uploads/2020/06/tnr-grand-palace-thai-binh-6-460x295.jpg"
                                                                alt="" title="tnr-grand-palace-thai-binh-6"
                                                                aria-label="tnr-grand-palace-thai-binh-6"
                                                                class="img-responsive wp-image-1759"/></a></div>
                                            </div>
                                            <div style="padding:5px;"
                                                 class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3">
                                                <div class="fusion-gallery-image fusion-gallery-image-liftup"><a
                                                            href="wp-content/uploads/2020/06/b93e444375a388fdd1b2.jpg"
                                                            class="fusion-lightbox"
                                                            data-rel="iLightbox[195786979bfa05db790]"><img
                                                                src="wp-content/uploads/2020/06/b93e444375a388fdd1b2-460x295.jpg"
                                                                alt="" title="b93e444375a388fdd1b2"
                                                                aria-label="b93e444375a388fdd1b2"
                                                                class="img-responsive wp-image-1757"/></a></div>
                                            </div>
                                            <div style="padding:5px;"
                                                 class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3">
                                                <div class="fusion-gallery-image fusion-gallery-image-liftup"><a
                                                            href="wp-content/uploads/2020/06/10b463d45234af6af625.jpg"
                                                            class="fusion-lightbox"
                                                            data-rel="iLightbox[195786979bfa05db790]"><img
                                                                src="wp-content/uploads/2020/06/10b463d45234af6af625-460x295.jpg"
                                                                alt="" title="10b463d45234af6af625"
                                                                aria-label="10b463d45234af6af625"
                                                                class="img-responsive wp-image-1754"/></a></div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div style="padding:5px;"
                                                 class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3">
                                                <div class="fusion-gallery-image fusion-gallery-image-liftup"><a
                                                            href="wp-content/uploads/2020/06/383fce40ffa002fe5bb1.jpg"
                                                            class="fusion-lightbox"
                                                            data-rel="iLightbox[195786979bfa05db790]"><img
                                                                src="wp-content/uploads/2020/06/383fce40ffa002fe5bb1-460x295.jpg"
                                                                alt="" title="383fce40ffa002fe5bb1"
                                                                aria-label="383fce40ffa002fe5bb1"
                                                                class="img-responsive wp-image-1755"/></a></div>
                                            </div>
                                            <div style="padding:5px;"
                                                 class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3">
                                                <div class="fusion-gallery-image fusion-gallery-image-liftup"><a
                                                            href="wp-content/uploads/2020/06/14525c326dd2908cc9c3.jpg"
                                                            class="fusion-lightbox"
                                                            data-rel="iLightbox[195786979bfa05db790]"><img
                                                                src="wp-content/uploads/2020/06/14525c326dd2908cc9c3-460x295.jpg"
                                                                alt="" title="14525c326dd2908cc9c3"
                                                                aria-label="14525c326dd2908cc9c3"
                                                                class="img-responsive wp-image-1756"/></a></div>
                                            </div>
                                            <div style="padding:5px;"
                                                 class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3">
                                                <div class="fusion-gallery-image fusion-gallery-image-liftup"><a
                                                            href="wp-content/uploads/2020/06/dbf0ef90de70232e7a61.jpg"
                                                            class="fusion-lightbox"
                                                            data-rel="iLightbox[195786979bfa05db790]"><img
                                                                src="wp-content/uploads/2020/06/dbf0ef90de70232e7a61-460x295.jpg"
                                                                alt="" title="dbf0ef90de70232e7a61"
                                                                aria-label="dbf0ef90de70232e7a61"
                                                                class="img-responsive wp-image-1758"/></a></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="fusion-clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
                             style='background-color: #fefaef;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:30px;padding-left:30px;'>
                            <div class="fusion-builder-row fusion-row ">
                                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                                     style='margin-top:0px;margin-bottom:10px;'>
                                    <div class="fusion-column-wrapper"
                                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                         data-bg-url="">
                                        <div class="fusion-text"><p style="text-align: center;"><span
                                                        style="font-family: helvetica, arial, sans-serif; font-size: 14pt;">LIÊN HỆ ĐẶT MUA TRỰC TIẾP CHỦ ĐẦU TƯ TNR HOLDINGS</span>
                                            </p>
                                            <p style="text-align: center;"><strong><span
                                                            style="font-size: 16pt; color: #008c7d;">&#8220;LIỀN KỀ, SHOPHOUSE, BIỆT THỰ <a
                                                                href="index.html">TNR GRAND
                                                            PALACE</a> ĐÔNG MỸ&#8221;</span></strong></p>
                                            <p style="text-align: center;"><span
                                                        style="color: #000000; font-size: 12pt;">Để lại thông tin chúng tôi sẽ gọi lại ngay!</span>
                                            </p>
                                        </div>
                                        <div class="fusion-text">
                                            <div role="form" class="wpcf7" id="wpcf7-f294-p1679-o17" lang="en-US"
                                                 dir="ltr">
                                                <div class="screen-reader-response" aria-live="polite"></div>
                                                <form action="index.html#wpcf7-f294-p1679-o17" method="post"
                                                      class="wpcf7-form" novalidate="novalidate">
                                                    <div style="display: none;">
                                                        <input type="hidden" name="_wpcf7" value="294"/>
                                                        <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                                        <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                                        <input type="hidden" name="_wpcf7_unit_tag"
                                                               value="wpcf7-f294-p1679-o17"/>
                                                        <input type="hidden" name="_wpcf7_container_post" value="1679"/>
                                                    </div>
                                                    <p><label> Họ và tên (*)<br/>
                                                            <span class="wpcf7-form-control-wrap your-name"><input
                                                                        type="text" name="your-name" value="" size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                        aria-required="true"
                                                                        aria-invalid="false"/></span> </label></p>
                                                    <p><label> Địa chỉ EMail (*)<br/>
                                                            <span class="wpcf7-form-control-wrap your-email"><input
                                                                        type="email" name="your-email" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                        aria-required="true"
                                                                        aria-invalid="false"/></span> </label></p>
                                                    <p><label> Số điện thoại (*)<br/>
                                                            <span class="wpcf7-form-control-wrap your-subject"><input
                                                                        type="text" name="your-subject" value=""
                                                                        size="40"
                                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                        aria-required="true"
                                                                        aria-invalid="false"/></span> </label></p>
                                                    <p><label> Loại hình bất động sản bạn quan tâm<br/>
                                                            <span class="wpcf7-form-control-wrap checkbox-312"><span
                                                                        class="wpcf7-form-control wpcf7-checkbox"><span
                                                                            class="wpcf7-list-item first"><input
                                                                                type="checkbox" name="checkbox-312[]"
                                                                                value="Nhà Liền Kề"/><span
                                                                                class="wpcf7-list-item-label">Nhà Liền Kề</span></span><span
                                                                            class="wpcf7-list-item"><input
                                                                                type="checkbox" name="checkbox-312[]"
                                                                                value="Nhà phố Shophouse"/><span
                                                                                class="wpcf7-list-item-label">Nhà phố Shophouse</span></span><span
                                                                            class="wpcf7-list-item"><input
                                                                                type="checkbox" name="checkbox-312[]"
                                                                                value="Biệt thự song lập"/><span
                                                                                class="wpcf7-list-item-label">Biệt thự song lập</span></span><span
                                                                            class="wpcf7-list-item last"><input
                                                                                type="checkbox" name="checkbox-312[]"
                                                                                value="Biệt thự song lập kinh doanh"/><span
                                                                                class="wpcf7-list-item-label">Biệt thự song lập kinh doanh</span></span></span></span>
                                                    </p>
                                                    <div class="one_fourth">
                                                        <input type="submit" value="GỌI LẠI TƯ VẤN CHO TÔI NGAY"
                                                               class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                                                    </div>
                                                    <div class="clear_column"></div>
                                                    <input type='hidden' class='wpcf7-pum'
                                                           value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                                    <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                                         style="background-color:;color:;border-color:;border-width:1px;">
                                                        <button style="color:;border-color:;" type="button"
                                                                class="close toggle-alert" data-dismiss="alert"
                                                                aria-hidden="true">&times;</button>
                                                        <div class="fusion-alert-content-wrapper"><span
                                                                    class="fusion-alert-content"></span></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="fusion-clearfix"></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>  <!-- fusion-row -->
    </main>  <!-- #main -->


    <div class="fusion-footer">

        <footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
            <div class="fusion-row">
                <div class="fusion-columns fusion-columns-4 fusion-widget-area">

                    <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-2" class="fusion-footer-widget-column widget widget_text"
                                 style="background-color: #a97829;">
                            <div class="textwidget"><img class="size-full wp-image-1751 aligncenter"
                                                         src="wp-content/uploads/2020/06/logo-footer.png" alt=""
                                                         width="300" height="152"/>

                                <ul class="fusion-checklist fusion-checklist-1"
                                    style="font-size:14px;line-height:23.8px;">
                                    <li class="fusion-li-item"><span
                                                style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                                class="icon-wrapper circle-no"><i
                                                    class="fusion-li-icon fa-map-marker fas" style="color:#ffffff;"></i></span>
                                        <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                            Khu đô thị Đông Mỹ, X. Đông Mỹ, TP. Thái Bình

                                        </div>
                                    </li>
                                    <li class="fusion-li-item"><span
                                                style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                                class="icon-wrapper circle-no"><i
                                                    class="fusion-li-icon fa-phone-volume fas"
                                                    style="color:#ffffff;"></i></span>
                                        <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                            Hotline 1: 0918 960 888

                                        </div>
                                    </li>
                                    <li class="fusion-li-item"><span
                                                style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                                class="icon-wrapper circle-no"><i
                                                    class="fusion-li-icon fa-phone-volume fas"
                                                    style="color:#ffffff;"></i></span>
                                        <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                            Hotline 2: 0983 985 556

                                        </div>
                                    </li>
                                    <li class="fusion-li-item"><span
                                                style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                                class="icon-wrapper circle-no"><i class="fusion-li-icon fa-envelope far"
                                                                                  style="color:#ffffff;"></i></span>
                                        <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                            info@tnrgrandpalacethaibinh.com

                                        </div>
                                    </li>
                                </ul>

                                <img class="size-full wp-image-1752 aligncenter"
                                     src="wp-content/uploads/2020/06/hotline-footer-1.png" alt="" width="313"
                                     height="100"/></div>
                            <div style="clear:both;"></div>
                        </section>
                    </div>
                    <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-5" class="fusion-footer-widget-column widget widget_text"><h4
                                    class="widget-title">/ TẢI XUỐNG TÀI LIỆU DỰ ÁN</h4>
                            <div class="textwidget">
                                <hr/>

                                <div class="fusion-button-wrapper">
                                    <style type="text/css"
                                           scoped="scoped">.fusion-button.button-3 .fusion-button-text, .fusion-button.button-3 i {
                                            color: #ffffff;
                                        }

                                        .fusion-button.button-3 {
                                            border-width: 0px;
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-3 .fusion-button-icon-divider {
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-3:hover .fusion-button-text, .fusion-button.button-3:hover i, .fusion-button.button-3:focus .fusion-button-text, .fusion-button.button-3:focus i, .fusion-button.button-3:active .fusion-button-text, .fusion-button.button-3:active {
                                            color: #d176b0;
                                        }

                                        .fusion-button.button-3:hover, .fusion-button.button-3:focus, .fusion-button.button-3:active {
                                            border-width: 0px;
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:active .fusion-button-icon-divider {
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-3 {
                                            background: rgba(160, 206, 78, 0);
                                            background-image: -webkit-gradient(linear, left bottom, left top, from(rgba(160, 206, 78, 0.01)), to(rgba(160, 206, 78, 0)));
                                            background-image: -webkit-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -moz-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -o-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: linear-gradient(to top, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                        }

                                        .fusion-button.button-3:hover, .button-3:focus, .fusion-button.button-3:active {
                                            background: rgba(150, 195, 70, 0);
                                        }

                                        .fusion-button.button-3 {
                                            width: auto;
                                        }</style>
                                    <a class="fusion-button button-flat fusion-button-round button-large button-custom button-3 popmake-370"
                                       target="_self" rel="noopener noreferrer"><i
                                                class="fa-download fas button-icon-left"></i><span
                                                class="fusion-button-text">BROCHURE TNR GRand Palace.PDF</span></a>
                                </div>

                                <hr/>

                                <div class="fusion-button-wrapper">
                                    <style type="text/css"
                                           scoped="scoped">.fusion-button.button-4 .fusion-button-text, .fusion-button.button-4 i {
                                            color: #ffffff;
                                        }

                                        .fusion-button.button-4 {
                                            border-width: 0px;
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-4 .fusion-button-icon-divider {
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-4:hover .fusion-button-text, .fusion-button.button-4:hover i, .fusion-button.button-4:focus .fusion-button-text, .fusion-button.button-4:focus i, .fusion-button.button-4:active .fusion-button-text, .fusion-button.button-4:active {
                                            color: #d176b0;
                                        }

                                        .fusion-button.button-4:hover, .fusion-button.button-4:focus, .fusion-button.button-4:active {
                                            border-width: 0px;
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-4:hover .fusion-button-icon-divider, .fusion-button.button-4:hover .fusion-button-icon-divider, .fusion-button.button-4:active .fusion-button-icon-divider {
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-4 {
                                            background: rgba(160, 206, 78, 0);
                                            background-image: -webkit-gradient(linear, left bottom, left top, from(rgba(160, 206, 78, 0.01)), to(rgba(160, 206, 78, 0)));
                                            background-image: -webkit-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -moz-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -o-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: linear-gradient(to top, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                        }

                                        .fusion-button.button-4:hover, .button-4:focus, .fusion-button.button-4:active {
                                            background: rgba(150, 195, 70, 0);
                                        }

                                        .fusion-button.button-4 {
                                            width: auto;
                                        }</style>
                                    <a class="fusion-button button-flat fusion-button-round button-large button-custom button-4 popmake-1779"
                                       target="_self" rel="noopener noreferrer"><i
                                                class="fa-download fas button-icon-left"></i><span
                                                class="fusion-button-text">Bảng giá gốc 222 lô liền kề, shophouse, biệt thự.PDF</span></a>
                                </div>

                                <hr/>

                                <div class="fusion-button-wrapper">
                                    <style type="text/css"
                                           scoped="scoped">.fusion-button.button-5 .fusion-button-text, .fusion-button.button-5 i {
                                            color: #ffffff;
                                        }

                                        .fusion-button.button-5 {
                                            border-width: 0px;
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-5 .fusion-button-icon-divider {
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-5:hover .fusion-button-text, .fusion-button.button-5:hover i, .fusion-button.button-5:focus .fusion-button-text, .fusion-button.button-5:focus i, .fusion-button.button-5:active .fusion-button-text, .fusion-button.button-5:active {
                                            color: #d176b0;
                                        }

                                        .fusion-button.button-5:hover, .fusion-button.button-5:focus, .fusion-button.button-5:active {
                                            border-width: 0px;
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-5:hover .fusion-button-icon-divider, .fusion-button.button-5:hover .fusion-button-icon-divider, .fusion-button.button-5:active .fusion-button-icon-divider {
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-5 {
                                            background: rgba(160, 206, 78, 0);
                                            background-image: -webkit-gradient(linear, left bottom, left top, from(rgba(160, 206, 78, 0.01)), to(rgba(160, 206, 78, 0)));
                                            background-image: -webkit-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -moz-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -o-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: linear-gradient(to top, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                        }

                                        .fusion-button.button-5:hover, .button-5:focus, .fusion-button.button-5:active {
                                            background: rgba(150, 195, 70, 0);
                                        }

                                        .fusion-button.button-5 {
                                            width: auto;
                                        }</style>
                                    <a class="fusion-button button-flat fusion-button-round button-large button-custom button-5 popmake-381"
                                       target="_self" rel="noopener noreferrer"><i
                                                class="fa-download fas button-icon-left"></i><span
                                                class="fusion-button-text">Toàn bộ văn bản pháp lý dự án.PDF</span></a>
                                </div>

                                <hr/>

                                <div class="fusion-button-wrapper">
                                    <style type="text/css"
                                           scoped="scoped">.fusion-button.button-6 .fusion-button-text, .fusion-button.button-6 i {
                                            color: #ffffff;
                                        }

                                        .fusion-button.button-6 {
                                            border-width: 0px;
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-6 .fusion-button-icon-divider {
                                            border-color: #ffffff;
                                        }

                                        .fusion-button.button-6:hover .fusion-button-text, .fusion-button.button-6:hover i, .fusion-button.button-6:focus .fusion-button-text, .fusion-button.button-6:focus i, .fusion-button.button-6:active .fusion-button-text, .fusion-button.button-6:active {
                                            color: #d176b0;
                                        }

                                        .fusion-button.button-6:hover, .fusion-button.button-6:focus, .fusion-button.button-6:active {
                                            border-width: 0px;
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-6:hover .fusion-button-icon-divider, .fusion-button.button-6:hover .fusion-button-icon-divider, .fusion-button.button-6:active .fusion-button-icon-divider {
                                            border-color: #d176b0;
                                        }

                                        .fusion-button.button-6 {
                                            background: rgba(160, 206, 78, 0);
                                            background-image: -webkit-gradient(linear, left bottom, left top, from(rgba(160, 206, 78, 0.01)), to(rgba(160, 206, 78, 0)));
                                            background-image: -webkit-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -moz-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: -o-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                            background-image: linear-gradient(to top, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                        }

                                        .fusion-button.button-6:hover, .button-6:focus, .fusion-button.button-6:active {
                                            background: rgba(150, 195, 70, 0);
                                        }

                                        .fusion-button.button-6 {
                                            width: auto;
                                        }</style>
                                    <a class="fusion-button button-flat fusion-button-round button-large button-custom button-6 popmake-376"
                                       target="_self" rel="noopener noreferrer"><i
                                                class="fa-download fas button-icon-left"></i><span
                                                class="fusion-button-text">Mẫu hợp đồng mua bán dự án.PDF</span></a>
                                </div>

                                <hr/>

                                &nbsp;

                                &nbsp;</div>
                            <div style="clear:both;"></div>
                        </section>
                    </div>
                    <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-4" class="fusion-footer-widget-column widget widget_text"><h4
                                    class="widget-title">/ ĐĂNG KÝ ĐẶT MUA CĂN HỘ DỰ ÁN</h4>
                            <div class="textwidget">
                                <div role="form" class="wpcf7" id="wpcf7-f301-o18" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response" aria-live="polite"></div>
                                    <form action="index.html#wpcf7-f301-o18" method="post" class="wpcf7-form"
                                          novalidate="novalidate">
                                        <div style="display: none;">
                                            <input type="hidden" name="_wpcf7" value="301"/>
                                            <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                            <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f301-o18"/>
                                            <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                        </div>
                                        <p><label> Họ và tên (*)<br/>
                                                <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                                       name="your-name"
                                                                                                       value=""
                                                                                                       size="40"
                                                                                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                       aria-required="true"
                                                                                                       aria-invalid="false"/></span>
                                            </label></p>
                                        <p><label> Địa chỉ EMail (*)<br/>
                                                <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                        name="your-email"
                                                                                                        value=""
                                                                                                        size="40"
                                                                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                        aria-required="true"
                                                                                                        aria-invalid="false"/></span>
                                            </label></p>
                                        <p><label> Số điện thoại (*)<br/>
                                                <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                          name="your-subject"
                                                                                                          value=""
                                                                                                          size="40"
                                                                                                          class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                          aria-required="true"
                                                                                                          aria-invalid="false"/></span>
                                            </label></p>
                                        <p><label> Loại hình bất động sản bạn quan tâm<br/>
                                                <span class="wpcf7-form-control-wrap checkbox-313"><span
                                                            class="wpcf7-form-control wpcf7-checkbox"><span
                                                                class="wpcf7-list-item first"><input type="checkbox"
                                                                                                     name="checkbox-313[]"
                                                                                                     value="Nhà Liền kề"/><span
                                                                    class="wpcf7-list-item-label">Nhà Liền kề</span></span><span
                                                                class="wpcf7-list-item"><input type="checkbox"
                                                                                               name="checkbox-313[]"
                                                                                               value="Nhà phố Shophouse"/><span
                                                                    class="wpcf7-list-item-label">Nhà phố Shophouse</span></span><span
                                                                class="wpcf7-list-item"><input type="checkbox"
                                                                                               name="checkbox-313[]"
                                                                                               value="Biệt thự Song Lập"/><span
                                                                    class="wpcf7-list-item-label">Biệt thự Song Lập</span></span><span
                                                                class="wpcf7-list-item last"><input type="checkbox"
                                                                                                    name="checkbox-313[]"
                                                                                                    value="Biệt thự Song lập kinh doanh"/><span
                                                                    class="wpcf7-list-item-label">Biệt thự Song lập kinh doanh</span></span></span></span>
                                        </p>
                                        <div class="one_fourth">
                                            <input type="submit" value="ĐĂNG KÝ"
                                                   class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                        <div class="clear_column"></div>
                                        <input type='hidden' class='wpcf7-pum'
                                               value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                        <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                             style="background-color:;color:;border-color:;border-width:1px;">
                                            <button style="color:;border-color:;" type="button"
                                                    class="close toggle-alert" data-dismiss="alert"
                                                    aria-hidden="true">&times;</button>
                                            <div class="fusion-alert-content-wrapper"><span
                                                        class="fusion-alert-content"></span></div>
                                        </div>
                                    </form>
                                </div>

                                &nbsp;

                                &nbsp;</div>
                            <div style="clear:both;"></div>
                        </section>
                    </div>
                    <div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
                        <section id="text-3" class="fusion-footer-widget-column widget widget_text">
                            <div class="textwidget">
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTnr-Grand-Palace-Th%C3%A1i-B%C3%ACnh-103301778115232%2F%3Fmodal%3Dadmin_todo_tour&#038;tabs=timeline&#038;width=340&#038;height=500&#038;small_header=false&#038;adapt_container_width=true&#038;hide_cover=false&#038;show_facepile=true&#038;appId"
                                        width="400px" height="380px" style="border:none;overflow:hidden" scrolling="no"
                                        frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                            </div>
                            <div style="clear:both;"></div>
                        </section>
                    </div>

                    <div class="fusion-clearfix"></div>
                </div> <!-- fusion-columns -->
            </div> <!-- fusion-row -->
        </footer> <!-- fusion-footer-widget-area -->


        <footer id="footer" class="fusion-footer-copyright-area">
            <div class="fusion-row">
                <div class="fusion-copyright-content">

                    <div class="fusion-copyright-notice">
                        <div>
                            Copyright 2012 - 2020 | Chủ đầu tư: TNR Holdings Việt Nam | TNR Tower, 54A Nguyễn Chí Thanh,
                            quận Đống Đa, Hà Nội | Tổng đài: 0918 960 888
                        </div>
                    </div>
                    <div class="fusion-social-links-footer">
                        <div class="fusion-social-networks">
                            <div class="fusion-social-networks-wrapper"><a
                                        class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook"
                                        style="color:#e2b95d;"
                                        href="https://www.facebook.com/chungcumipecrubik360xuanthuy.com.vn"
                                        target="_blank" data-placement="top" data-title="Facebook" data-toggle="tooltip"
                                        title="Facebook"><span class="screen-reader-text">Facebook</span></a><a
                                        class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube"
                                        style="color:#e2b95d;" href="https://youtu.be/K6LTLgnkkxQ" target="_blank"
                                        rel="noopener noreferrer" data-placement="top" data-title="YouTube"
                                        data-toggle="tooltip" title="YouTube"><span
                                            class="screen-reader-text">YouTube</span></a><a
                                        class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail"
                                        style="color:#e2b95d;"
                                        href="mailto:ng&#117;y&#101;nq&#117;a&#110;&#103;&#104;o&#097;&#046;&#098;&#100;&#115;&#064;&#103;&#109;&#097;i&#108;.&#099;o&#109;"
                                        target="_self" rel="noopener noreferrer" data-placement="top" data-title="Email"
                                        data-toggle="tooltip" title="Email"><span
                                            class="screen-reader-text">Email</span></a><a
                                        class="custom fusion-social-network-icon fusion-tooltip fusion-custom fusion-icon-custom"
                                        style="color:#e2b95d;position:relative;" href="http://zalo.me/0936 291 567"
                                        target="_blank" rel="noopener noreferrer" data-placement="top" data-title
                                        data-toggle="tooltip" title><span class="screen-reader-text"></span><img
                                            src="wp-content/uploads/2019/11/logo-zalo-75x75.jpg"
                                            style="width:auto;max-height:16px;" alt=""/></a></div>
                        </div>
                    </div>

                </div> <!-- fusion-fusion-copyright-content -->
            </div> <!-- fusion-row -->
        </footer> <!-- #footer -->
    </div> <!-- fusion-footer -->

    <div id="slidingbar-area"
         class="slidingbar-area fusion-sliding-bar-area fusion-widget-area fusion-sliding-bar-position-top fusion-sliding-bar-text-align-left fusion-sliding-bar-toggle-triangle fusion-sliding-bar-sticky"
         data-breakpoint="800" data-toggle="triangle">
        <div class="fusion-sb-toggle-wrapper">
            <a class="fusion-sb-toggle" href="index.html#"><span
                        class="screen-reader-text">Toggle Sliding Bar Area</span></a>
        </div>

        <div id="slidingbar" class="fusion-sliding-bar">
            <div class="fusion-row">
                <div class="fusion-columns row fusion-columns-2columns columns-2">

                    <div class="fusion-column col-lg-6 col-md-6 col-sm-6">
                    </div>
                    <div class="fusion-column col-lg-6 col-md-6 col-sm-6">
                    </div>
                    <div class="fusion-clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- wrapper -->

<a class="fusion-one-page-text-link fusion-page-load-link"></a>

<div id="pum-1810"
     class="pum pum-overlay pum-theme-430 pum-theme-2-theme-pupup-xuat-hien-giua-trang popmake-overlay pum-click-to-close auto_open click_open"
     data-popmake="{&quot;id&quot;:1810,&quot;slug&quot;:&quot;0-1-popup-giua-trang-ra-hang-bt1-bt2&quot;,&quot;theme_id&quot;:430,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;auto_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;delay&quot;:&quot;10000&quot;}},{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true">

    <div id="popmake-1810"
         class="pum-container popmake theme-430 pum-responsive pum-responsive-medium responsive size-medium">


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><span
                        style="font-family: arial, helvetica, sans-serif; font-size: large;"><b>CHÍNH THỨC RA HÀNG BIỆT
                        THỰ SONG LẬP; SHOPHOUSEVILLAS</b></span></p>
            <p style="text-align: center;"><strong><span
                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">DÃY BT01 VÀ BT 02</span></strong>
            </p>
            <p style="text-align: center;"><span style="font-size: 12pt;"><em><span
                                style="font-family: arial, helvetica, sans-serif;">(Tải xuống bảng giá 37 lô biệt thự BT1 và BT2 ngay)</span></em></span>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f1808-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f1808-o1" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="1808"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1808-o1"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-777"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-777[]"
                                                                                             value="Gửi mã xác nhận qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-777[]"
                                                                                            value="Gửi mã xác nhận qua nhắn tin"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua nhắn tin</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="alignnone size-full wp-image-1811"
                                src="wp-content/uploads/2020/07/biet-thu-song-lap.jpg" alt="" width="1433" height="960"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-200x134.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-300x201.jpg 300w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-400x268.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-600x402.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-768x515.jpg 768w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-800x536.jpg 800w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-1024x686.jpg 1024w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap-1200x804.jpg 1200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/07/biet-thu-song-lap.jpg 1433w"
                                sizes="(max-width: 1433px) 100vw, 1433px"/></p>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1048"
     class="pum pum-overlay pum-theme-430 pum-theme-2-theme-pupup-xuat-hien-giua-trang popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1048,&quot;slug&quot;:&quot;0-0-popup-giua-trang&quot;,&quot;theme_id&quot;:430,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true">

    <div id="popmake-1048"
         class="pum-container popmake theme-430 pum-responsive pum-responsive-medium responsive size-medium">


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><span
                        style="font-size: 14pt; font-family: arial, helvetica, sans-serif;"><strong>TẢI NGAY BẢNG GIÁ
                        222 LÔ</strong></span><br/>
                <span style="text-align: center;"><span
                            style="font-size: 14pt; font-family: arial, helvetica, sans-serif;"><strong>LIỀN KỀ &#8211;
                            SHOPHOUSE &#8211; BIỆT THỰ SONG LẬP </strong></span></span></p>
            <p style="text-align: center;"><span style="font-size: 12pt;"><em><span
                                style="font-family: arial, helvetica, sans-serif;">(Hệ thống tự động gửi bảng giá về Email và Số điện thoại của khách hàng)</span></em></span>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f1768-o2" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f1768-o2" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="1768"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1768-o2"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-777"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-777[]"
                                                                                             value="Gửi mã xác nhận qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-777[]"
                                                                                            value="Gửi mã xác nhận qua nhắn tin"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua nhắn tin</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="alignnone size-full wp-image-1770"
                                src="wp-content/uploads/2020/01/tai-lieu-du-an.jpg" alt="" width="800" height="482"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/tai-lieu-du-an-200x121.jpg 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/tai-lieu-du-an-300x181.jpg 300w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/tai-lieu-du-an-400x241.jpg 400w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/tai-lieu-du-an-600x362.jpg 600w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/tai-lieu-du-an-768x463.jpg 768w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/01/tai-lieu-du-an.jpg 800w"
                                sizes="(max-width: 800px) 100vw, 800px"/></p>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1787"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1787,&quot;slug&quot;:&quot;0-3-tai-xuong-bang-gia-lk1234-bt12345&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;a[href=\&quot;gialk1234bt12345_url\&quot;]&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_1787">

    <div id="popmake-1787"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_1787" class="pum-title popmake-title">
            Tải xuống bảng giá LK1,LK2,LK3,LK4; BT1,BT2,BT3,BT4,BT5
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f1788-o3" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f1788-o3" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="1788"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1788-o3"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-777"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-777[]"
                                                                                             value="Gửi mã xác nhận qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-777[]"
                                                                                            value="Gửi mã xác nhận qua nhắn tin"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua nhắn tin</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1785"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1785,&quot;slug&quot;:&quot;0-2-tai-xuong-bang-check-lo-con-cac-lo-lien-ke-shophouse-biet-thu&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;a[href=\&quot;checkerlocon_url\&quot;]&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_1785">

    <div id="popmake-1785"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_1785" class="pum-title popmake-title">
            Tải xuống bảng check lô chưa bán, đã bán shophouse, liền kề, biệt thự dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f470-o4" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f470-o4" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="470"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f470-o4"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-777"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-777[]"
                                                                                             value="Gửi mã xác nhận qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-777[]"
                                                                                            value="Gửi mã xác nhận qua nhắn tin"/><span
                                                            class="wpcf7-list-item-label">Gửi mã xác nhận qua nhắn tin</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-833"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:833,&quot;slug&quot;:&quot;0-1-tai-xuong-bang-gia-goc-cuoi-chan-trang-mobile&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;a[href=\&quot;taibanggiamobile_url\&quot;]&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_833">

    <div id="popmake-833"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_833" class="pum-title popmake-title">
            Tải xuống bảng giá gốc 222 lô shophouse, liền kề, biệt thự
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f403-o5" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f403-o5" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="403"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f403-o5"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-821"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-821[]"
                                                                                             value="Nhận mã qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-821[]"
                                                                                            value="Nhận mã qua nhắn tin"/><span
                                                            class="wpcf7-list-item-label">Nhận mã qua nhắn tin</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1779"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1779,&quot;slug&quot;:&quot;1-tai-xuong-bang-gia-goc-222-lo&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_1779">

    <div id="popmake-1779"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_1779" class="pum-title popmake-title">
            TẢI XUỐNG BẢNG GIÁ GỐC 222 LÔ LIỀN KỀ, BIỆT THỰ, SHOPHOUSE
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f1780-o6" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f1780-o6" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="1780"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1780-o6"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-858"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-858[]"
                                                                                             value="Nhận mã xác minh qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-858[]"
                                                                                            value="Nhận mã xác minh qua tin nhắn"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua tin nhắn</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-381"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:381,&quot;slug&quot;:&quot;18-tai-xuong-toan-bo-van-ban-phap-ly-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_381">

    <div id="popmake-381"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_381" class="pum-title popmake-title">
            Tải xuống toàn bộ văn bản pháp lý dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f383-o7" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f383-o7" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="383"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f383-o7"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-854"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-854[]"
                                                                                             value="Nhận mã xác minh qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-854[]"
                                                                                            value="Nhận mã xác minh qua tin nhắn"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua tin nhắn</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-359"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:359,&quot;slug&quot;:&quot;13-tai-xuong-bang-vat-lieu-ban-giao-kem-theo-hop-dong-mua-ban&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_359">

    <div id="popmake-359"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_359" class="pum-title popmake-title">
            Tải xuống bảng vật liệu bàn giao kèm theo hợp đồng mua bán
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        [contact-form-7 404 "Not Found"]
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <p style="text-align: center;">[fusion_serator style_type=&#8221;single solid&#8221;
                            hide_on_mobile=&#8221;small-visibility,medium-visibility,large-visibility&#8221; class=&#8221;&#8221;
                            id=&#8221;&#8221; sep_color=&#8221;#ffffff&#8221; top_margin=&#8221;&#8221; bottom_margin=&#8221;&#8221;
                            border_size=&#8221;3&#8243; icon=&#8221;&#8221; icon_circle=&#8221;&#8221;
                            icon_circle_color=&#8221;&#8221; width=&#8221;30%&#8221; alignment=&#8221;center&#8221;][/fusion_separator]
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-373"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:373,&quot;slug&quot;:&quot;15-tai-mau-hop-dong-dat-coc-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_373">

    <div id="popmake-373"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_373" class="pum-title popmake-title">
            Tải xuống mẫu hợp đồng đặt cọc dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f372-o8" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f372-o8" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="372"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f372-o8"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-836"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-836[]"
                                                                                             value="Nhận mã xác minh qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-836[]"
                                                                                            value="Nhận mã xác minh qua tin nhắn"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua tin nhắn</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-376"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:376,&quot;slug&quot;:&quot;16-tai-mau-hop-dong-mua-ban-nha-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_376">

    <div id="popmake-376"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_376" class="pum-title popmake-title">
            Tải xuống mẫu hợp đồng mua bán nhà dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f375-o9" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f375-o9" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="375"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f375-o9"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-850"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-850[]"
                                                                                             value="Nhận mã xác minh qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-850[]"
                                                                                            value="Nhận mã xác minh qua tin nhắn"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua tin nhắn</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-370"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:370,&quot;slug&quot;:&quot;14-tai-xuong-brochure-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_370">

    <div id="popmake-370"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_370" class="pum-title popmake-title">
            Tải xuống Brochure Dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f369-o10" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f369-o10" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="369"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f369-o10"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-833"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-833[]"
                                                                                             value="Nhận mã xác minh qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-833[]"
                                                                                            value="Nhận mã xác minh qua tin nhắn"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua tin nhắn</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-352"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:352,&quot;slug&quot;:&quot;10-tai-xuong-bang-chinh-sach-ban-hang-dang-ban-hanh&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_352">

    <div id="popmake-352"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_352" class="pum-title popmake-title">
            Tải xuống chính sách bán hàng (dấu đỏ đang ban hành)
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f351-o11" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f351-o11" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="351"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f351-o11"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-829"><span
                                                    class="wpcf7-form-control wpcf7-checkbox"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-829[]"
                                                                                             value="Nhận mã xác minh qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-829[]"
                                                                                            value="Nhận mã xác minh qua tin nhắn"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua tin nhắn</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-318"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:318,&quot;slug&quot;:&quot;3-tai-xuong-mat-bang-tong-the-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_318">

    <div id="popmake-318"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_318" class="pum-title popmake-title">
            MẶT BẰNG TỔNG THỂ DỰ ÁN
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f317-o12" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f317-o12" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="317"/>
                                    <input type="hidden" name="_wpcf7_version" value="5.1.9"/>
                                    <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f317-o12"/>
                                    <input type="hidden" name="_wpcf7_container_post" value="0"/>
                                </div>
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                               name="your-name" value=""
                                                                                               size="40"
                                                                                               class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                               aria-required="true"
                                                                                               aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                                name="your-email"
                                                                                                value="" size="40"
                                                                                                class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                                  name="your-subject"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Xác minh danh tính (*)<br/>
                                        <span class="wpcf7-form-control-wrap checkbox-858"><span
                                                    class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required"><span
                                                        class="wpcf7-list-item first"><input type="checkbox"
                                                                                             name="checkbox-858[]"
                                                                                             value="Nhận mã xác minh qua gọi điện"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua gọi điện</span></span><span
                                                        class="wpcf7-list-item last"><input type="checkbox"
                                                                                            name="checkbox-858[]"
                                                                                            value="Nhận mã xác minh qua tin nhắn"/><span
                                                            class="wpcf7-list-item-label">Nhận mã xác minh qua tin nhắn</span></span></span></span>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                srcset="https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer-200x101.png 200w, https://tnrgrandpalacethaibinh.com/wp-content/uploads/2020/06/logo-footer.png 300w"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var ajaxurl = 'https://tnrgrandpalacethaibinh.com/wp-admin/admin-ajax.php';
        if (0 < jQuery('.fusion-login-nonce').length) {
            jQuery.get(ajaxurl, {'action': 'fusion_login_nonce'}, function (response) {
                jQuery('.fusion-login-nonce').html(response);
            });
        }
    });
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js@ver=5.1.9'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var zerospam = {"key": "EISQG4kn4ficu^BuUj8PQrEpD^mFH9oa%7WsM@H3kdui5qJ#Zc9W^!5iZNUtq&kx"};
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/zero-spam/js/zerospam.js@ver=3.1.1'></script>
<!--[if IE 9]>
<script type='text/javascript'
        src='https://tnrgrandpalacethaibinh.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
<![endif]-->
<script type='text/javascript'
        src='wp-content/uploads/fusion-scripts/bb59f1f1f0555af0a437a8d887b035d4.min.js@timestamp=1593497390'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/core.min.js@ver=1.11.4'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/position.min.js@ver=1.11.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var pum_vars = {
        "version": "1.11.0",
        "pm_dir_url": "https:\/\/tnrgrandpalacethaibinh.com\/wp-content\/plugins\/popup-maker\/",
        "ajaxurl": "https:\/\/tnrgrandpalacethaibinh.com\/wp-admin\/admin-ajax.php",
        "restapi": "https:\/\/tnrgrandpalacethaibinh.com\/wp-json\/pum\/v1",
        "rest_nonce": null,
        "default_theme": "6",
        "debug_mode": "",
        "disable_tracking": "",
        "home_url": "\/",
        "message_position": "top",
        "core_sub_forms_enabled": "1",
        "popups": []
    };
    var ajaxurl = "https:\/\/tnrgrandpalacethaibinh.com\/wp-admin\/admin-ajax.php";
    var pum_sub_vars = {
        "ajaxurl": "https:\/\/tnrgrandpalacethaibinh.com\/wp-admin\/admin-ajax.php",
        "message_position": "top"
    };
    var pum_popups = {
        "pum-1810": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "triggers": [{"type": "auto_open", "settings": {"cookie_name": "", "delay": "10000"}}],
            "theme_id": "430",
            "size": "medium",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "cookies": [],
            "theme_slug": "2-theme-pupup-xuat-hien-giua-trang",
            "id": 1810,
            "slug": "0-1-popup-giua-trang-ra-hang-bt1-bt2"
        },
        "pum-1048": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "430",
            "size": "medium",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "2-theme-pupup-xuat-hien-giua-trang",
            "id": 1048,
            "slug": "0-0-popup-giua-trang"
        },
        "pum-1787": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "triggers": [{
                "type": "click_open",
                "settings": {"cookie_name": "", "extra_selectors": "a[href=\"gialk1234bt12345_url\"]"}
            }],
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 1787,
            "slug": "0-3-tai-xuong-bang-gia-lk1234-bt12345"
        },
        "pum-1785": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "triggers": [{
                "type": "click_open",
                "settings": {"cookie_name": "", "extra_selectors": "a[href=\"checkerlocon_url\"]"}
            }],
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 1785,
            "slug": "0-2-tai-xuong-bang-check-lo-con-cac-lo-lien-ke-shophouse-biet-thu"
        },
        "pum-833": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "triggers": [{
                "type": "click_open",
                "settings": {"cookie_name": "", "extra_selectors": "a[href=\"taibanggiamobile_url\"]"}
            }],
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 833,
            "slug": "0-1-tai-xuong-bang-gia-goc-cuoi-chan-trang-mobile"
        },
        "pum-1779": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 1779,
            "slug": "1-tai-xuong-bang-gia-goc-222-lo"
        },
        "pum-381": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 381,
            "slug": "18-tai-xuong-toan-bo-van-ban-phap-ly-du-an"
        },
        "pum-359": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 359,
            "slug": "13-tai-xuong-bang-vat-lieu-ban-giao-kem-theo-hop-dong-mua-ban"
        },
        "pum-373": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 373,
            "slug": "15-tai-mau-hop-dong-dat-coc-du-an"
        },
        "pum-376": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 376,
            "slug": "16-tai-mau-hop-dong-mua-ban-nha-du-an"
        },
        "pum-370": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 370,
            "slug": "14-tai-xuong-brochure-du-an"
        },
        "pum-352": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 352,
            "slug": "10-tai-xuong-bang-chinh-sach-ban-hang-dang-ban-hanh"
        },
        "pum-318": {
            "disable_on_mobile": false,
            "disable_on_tablet": false,
            "custom_height_auto": false,
            "scrollable_content": false,
            "position_from_trigger": false,
            "position_fixed": false,
            "overlay_disabled": false,
            "stackable": false,
            "disable_reposition": false,
            "close_on_form_submission": false,
            "close_on_overlay_click": true,
            "close_on_esc_press": false,
            "close_on_f4_press": false,
            "disable_form_reopen": false,
            "disable_accessibility": false,
            "theme_id": "315",
            "size": "small",
            "responsive_min_width": "0%",
            "responsive_max_width": "100%",
            "custom_width": "640px",
            "custom_height": "380px",
            "animation_type": "fade",
            "animation_speed": "350",
            "animation_origin": "center top",
            "open_sound": "none",
            "custom_sound": "",
            "location": "center top",
            "position_top": "100",
            "position_bottom": "0",
            "position_left": "0",
            "position_right": "0",
            "zindex": "1999999999",
            "close_button_delay": "0",
            "close_on_form_submission_delay": "0",
            "triggers": [],
            "cookies": [],
            "theme_slug": "1-theme-contact",
            "id": 318,
            "slug": "3-tai-xuong-mat-bang-tong-the-du-an"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='wp-content/uploads/pum/pum-site-scripts.js@defer&amp;generated=1593678971&amp;ver=1.11.0'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min.js@ver=5.4.2'></script>
<!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync = _Hasync || [];
    _Hasync.push(['Histats.start', '1,4422297,4,0,0,0,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function () {
        var hs = document.createElement('script');
        hs.type = 'text/javascript';
        hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
<noscript><a href="index.html" target="_blank"><img src="https://sstatic1.histats.com/0.gif?4422297&amp;101"
                                                    alt="web hit counter" border="0"></a></noscript>
<!-- Histats.com  END  -->
</body>
<div class="hotline-footer">
    <div class="left">
        <a href="https://tnrgrandpalacethaibinh.com/taibanggiamobile_url"><img
                    src="https://tnrgrandpalacethaibinh.com/wp-content//uploads/2019/11/icon-tai-xuong-2.png"/>Bảng giá
            gốc 222 lô liền kề, shophouse</a>
    </div>
    <div class="right">
        <a href="tel:0918960888"><img
                    src="https://tnrgrandpalacethaibinh.com/wp-content//uploads/2019/11/icon-call.png"/>Gọi ngay</a>
    </div>
    <div class="clearboth"></div>
</div>
<div class="bottom-contact">
    <div class="container">
        <div class="left">
            <a href="https://tnrgrandpalacethaibinh.com/checkerlocon_url" target="blank"><img
                        src="wp-content/uploads/2019/11/icon-tai-xuong-2.png"/>Bảng check lô đã bán, chưa bán</a>
        </div>
        <div class="center">
            <a href="https://tnrgrandpalacethaibinh.com/gialk1234bt12345_url" target="blank"><img
                        src="wp-content/uploads/2019/11/icon-tai-xuong-2.png"/>Bảng giá
                Lk1,LK2,LK3,LK4,BT1,BT2,BT3,BT4,BT5</a>
        </div>
        <div class="right">
            <a href="tel:0918960888" target="blank"><img src="wp-content/uploads/2019/11/icon-call.png"/>Hotline:0918
                960 888</a>
        </div>
        <div class="clearboth"></div>
    </div>
</div>
</html>
