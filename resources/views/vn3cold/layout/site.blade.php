<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta name="ROBOTS" content="index, follow"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta itemprop="image" content="@yield('meta_image')" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:locale" content="vi_VN"/>
	<meta property="og:type" content="@yield('type_meta')"/>

    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('meta_description')" />
    <meta property="og:url" content="@yield('meta_url')" />
    <meta property="og:image" content="@yield('meta_image')" />
    <meta property="og:image:secure_url" content="@yield('meta_image')" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="@yield('meta_description')" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:image" content="@yield('meta_image')" />
	
	<link rel="shortcut icon" href="/favicon.ico">
    <!-- css he thong -->
    <link rel="stylesheet" href="{{ asset('vn3c/css/bootstrap.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('vn3c/css/extract.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('vn3c/css/fontawesome-all.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('vn3c/stylesheet/styles.css') }}" media="all" type="text/css"/>
    <!-- tab -->
    <link rel="stylesheet" href="{{ asset('vn3c/stylesheet/styles-tab.css') }}" media="all" type="text/css"/>
    <!-- mobile -->
    <link rel="stylesheet" href="{{ asset('vn3c/stylesheet/styles-mobile.css') }}" media="all" type="text/css"/>

    <!-- script he thong -->
    <script src="{{ asset('vn3c/js/jquery.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/popper.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('vn3c/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vn3c/js/smooth-scroll.js') }}"></script>
	<script src="{{ asset('vn3c/js/jquery.matchHeight-min.js') }}"></script>
    <script src="{{ asset('vn3c/js/modernizr.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/wow.min.js') }}"></script>
	
	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '331589171004896');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=331589171004896&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

    <script>
        $(document).ready(function () {
            //cố định menu khi lăn chuột
            var s = $(".navbar");
            var pos = s.position();
            $(window).scroll(function () {
                var windowpos = $(window).scrollTop();

                if (windowpos >= pos.top) {
                    s.addClass("sticker");
                } else {
                    s.removeClass("sticker");
                }
            });

            //click vào nút lên top
            // Hide the toTop button when the page loads.
            $("#toTop").css("display", "none");

            // This function runs every time the user scrolls the page.
            $(window).scroll(function () {

                // Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
                if ($(window).scrollTop() > 0) {

                    // If it's more than or equal to 0, show the toTop button.
                    console.log("is more");
                    $("#toTop").fadeIn("slow");

                }
                else {
                    // If it's less than 0 (at the top), hide the toTop button.
                    console.log("is less");
                    $("#toTop").fadeOut("slow");
                }
            });


            // When the user clicks the toTop button, we want the page to scroll to the top.
            $("#toTop").click(function () {

                // Disable the default behaviour when a user clicks an empty anchor link.
                // (The page jumps to the top instead of // animating)
                event.preventDefault();

                // Animate the scrolling motion.
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
            });

            //thu viện wow
            wow = new WOW(
                {
                    animateClass: 'animated',
                    offset: 100,
                    callback: function (box) {
                        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
                    }
                }
            );
            wow.init();
        });

        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: '{!! route('addToCart') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    window.location.replace("/gio-hang");
                },
                error: function(error) {
                    console.log(error);
                }

            });

            return false;
        }
        function contact(e) {
            var $btn = $(e).find('button').button('loading');
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: '{!! route('sub_contact') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    // gửi thành công
                    if (obj.status == 200) {
                        alert(obj.message);
                        $btn.button('reset');

                        return;
                    }

                    // gửi thất bại
                    if (obj.status == 500) {
                        alert(obj.message);
                        $btn.button('reset');

                        return;
                    }
                },
                error: function(error) {
                    //alert('Lỗi gì đó đã xảy ra!')
                }

            });


            return false;
        }
    </script>
	




    <head>
<body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120136381-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-120136381-1');
	</script>

<style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff; width: 60px; height: 60px; text-align: center; bottom: 30px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{width: 120px; left: -125px; top: -8px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style><div class="fb-livechat"> <div class="ctrlq fb-overlay"></div><div class="fb-widget"> <div class="ctrlq fb-close"></div><div class="fb-page" data-href="https://www.facebook.com/LinkMediaVietnam" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div><div class="fb-credit"> <a href="https://vn3c.com" target="_blank">Powered by VN3C</a> </div><div id="fb-root"></div></div><a href="https://m.me/LinkMediaVietnam" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button"> <div class="bubble">1</div><div class="bubble-msg">Xin chào, Anh (chị) cần tư vấn gì thêm không ạ!</div></a></div><script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><script>$(document).ready(function(){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){return true;}else{return false;}}var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")}; setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>
@include('vn3c.common.header')

<!-- Phần nội dung -->
@yield('content')

@include('vn3c.common.footer')
@include('vn3c.popup.login')
@include('vn3c.popup.forget_password')
@include('vn3c.popup.register')
</body>
</html>