<div id="portfolio-section-22" class="portfolio-section white-section">
    <div class="section-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 tab-block">
                    <div class="tab-block-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#property-for-sale"
                                   aria-controls="property-for-sale" role="tab" data-toggle="tab">Hình ảnh hoạt động của công ty</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="property-for-sale" class="tab-pane fade in active" role="tabpanel">

                                <div class="col-md-4 col-sm-6 portfolio-block">
                                    <div class="portfolio-block-container" style="height: 326px;">
                                        <div class="image-block">
                                            <div class="image-block-container">
                                                <img src="http://vn3c.net/labor_export/xuat_khau_lao_dong.jpg" alt="Image Block">
                                            </div>
                                        </div>
                                        <h4>Thi tuyển tháng 3 năm 2017</h4>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 portfolio-block">
                                    <div class="portfolio-block-container" style="height: 326px;">
                                        <div class="image-block">
                                            <div class="image-block-container">
                                                <img src="{{ asset('laborExport/img/product/2.jpg') }}" alt="Image Block">
                                            </div>
                                        </div>
                                        <h4>Thi tuyển 25/03/2017</h4>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 portfolio-block">
                                    <div class="portfolio-block-container" style="height: 326px;">
                                        <div class="image-block">
                                            <div class="image-block-container">
                                                <img src="{{ asset('laborExport/img/product/3.jpg') }}" alt="Image Block">
                                            </div>
                                        </div>
                                        <h4>Lễ chia tay</h4>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 portfolio-block">
                                    <div class="portfolio-block-container" style="height: 326px;">
                                        <div class="image-block">
                                            <div class="image-block-container">
                                                <img src="{{ asset('laborExport/img/product/4.jpg') }}" alt="Image Block">
                                            </div>
                                        </div>
                                        <h4>Tiễn thực tập sinh lên máy bay</h4>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 portfolio-block">
                                    <div class="portfolio-block-container" style="height: 326px;">
                                        <div class="image-block">
                                            <div class="image-block-container">
                                                <img src="{{ asset('laborExport/img/product/5.jpg') }}" alt="Image Block">

                                            </div>
                                        </div>
                                        <h4>Thực tập sinh tại sân bay</h4>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 portfolio-block">
                                    <div class="portfolio-block-container" style="height: 326px;">
                                        <div class="image-block">
                                            <div class="image-block-container">
                                                <img src="{{ asset('laborExport/img/product/6.jpg') }}" alt="Image Block">
                                            </div>
                                        </div>
                                        <h4>Thực tập sinh chụp ảnh lưu niệm</h4>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>