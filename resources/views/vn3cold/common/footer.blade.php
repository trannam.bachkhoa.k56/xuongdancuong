<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h3>Fanpage</h3>
				<div class="mb20">
                <?= $information['fanpage-facebook']?>
				</div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 right">
                <h3>Thông tin website</h3>
				<p>Cơ sở 1 : Tầng 3 - Số nhà 9 - Ngõ 1 - Lê Văn Thiêm - Thành Phố Hà Nội</p>
				<p>Cơ sở 2 : Cổng trường sĩ quan lục quân 1 - Cổ đông - Sơn Tây - Hà Nội</p>
				<p>Hotline : 093 455 3435 - 097 456 1735</p>
				<p>Email : vn3ctran@gmail.com</p>
				<p>Website : http://vn3c.net or http://vn3c.com</p>
                <!--@foreach (\App\Entity\Menu::showWithLocation('footer-second') as $menu)
                    {!! \App\Entity\MenuElement::showMenuElementPage($menu->slug, 'nav flex-column') !!}
                @endforeach-->
            </div>
            <!--<div class="col-lg-2 col-md-3 col-sm-12">
                <h3>Hướng dẫn</h3>
				@foreach (\App\Entity\Menu::showWithLocation('footer-third') as $menu)
                    {!! \App\Entity\MenuElement::showMenuElementPage($menu->slug, 'nav flex-column') !!}
                @endforeach
                <!-- <ul class="nav flex-column">
                    @foreach (\App\Entity\Post::newPost('tin-tuc', 5) as $post)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">
                            {{ $post->title }}
                        </a>
                    </li>
                    @endforeach
                </ul> -->
            <!--</div>-->
            <div class="col-lg-4 col-md-12 col-sm-12 right">
                <h3>Đăng ký nhận bản tin mới</h3>
                <p>Bạn hãy để lại cho chúng tôi Email, chúng tôi sẽ cung cấp cho bạn những tin tức bổ ích về bán hàng qua website</p>
                <form class="form-inline" onsubmit="return subcribeEmailSubmit(this)">
                    {{ csrf_field() }}
                    <div class="form-group mb-2">
                        <input type="text" class="emailSubmit form-control-plaintext" id="staticEmail2"
                               placeholder="email@example.com" required/>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Đăng ký</button>
                </form>
                <div class="socialFooter">
                    <a href="#"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>
                    <a href="#"><i class="fab fa-google-plus-square"></i></a>
					<a href="#"><i class="fab fa-twitter-square"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
{!! isset($information['ma-nhung-chat-facebook']) ? $information['ma-nhung-chat-facebook'] : '' !!}
<section class="CoppyRight">
    <div class="container tc">
        © 2018 Vn3c, Bản quyền thuộc về Công ty cổ phần công nghệ VN3C Việt Nam
    </div>
</section>