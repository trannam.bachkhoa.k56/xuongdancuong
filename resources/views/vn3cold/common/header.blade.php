<header>
    <div class="container">
        <div class="row">
            <div class="col-md-1 col-sm-3 col-12">
                <a href="/" class="logo logopc"><img src="{{ $information['logo'] }}"/></a>
            </div>
            <div class="col-md-8 col-sm-6 col-8 menumoble">

                <div class="menu">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a href="/" class="logo logomoble"><img src="{{ $information['logo'] }}"/></a>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $menu)
                               {!! \App\Entity\MenuElement::showMenuElementPage($menu->slug, 'navbar-nav mr-auto') !!}
                            @endforeach

                            {{--<ul class="navbar-nav mr-auto">--}}
                                {{--<li class="nav-item">--}}
                                    {{--<a href="#" class="nav-link hvr-overline-from-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chúng tôi </font></font></a>--}}
                                {{--</li>--}}

                                {{--<li class="nav-item">--}}
                                    {{--<a href="#" class="nav-link hvr-overline-from-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thiết kế </font></font></a>--}}
                                {{--</li>--}}
                                {{--<li class="nav-item">--}}
                                    {{--<a href="#" class="nav-link hvr-overline-from-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">website</font></font></a>--}}
                                {{--</li>--}}
                                {{--<li class="nav-item">--}}
                                    {{--<a href="#" class="nav-link hvr-overline-from-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">vui lòng</font></font></a>--}}
                                {{--</li>--}}
                                {{--<li class="nav-item">--}}
                                    {{--<a href="#" class="nav-link hvr-overline-from-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Liên hệ</font></font></a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}

                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-4 right-mb">
                <div class="right pull-right">
                    <ul>
                        @if (\Illuminate\Support\Facades\Auth::check())
                            <li>
                                <a href="/trang/quan-ly-theme" class="hd-br">Quản lý theme</a>
                            </li>
                            <li class="hidden-xs">
                                <a href="{{ route('logoutHome') }}" class="hd-br" >Thoát</a>
                            </li>
                        @else
                            <li class="hidden-xs">
                                <a href="/trang/tao-moi-website" class="hd-br" >Đăng kí</a>
                            </li>
                            <li><a href="" class="hd-br" data-toggle="modal" data-target="#modal-login">Đăng nhập</a>
                            </li>
                        @endif

                        <!--<li class="hidden-xs"><a href="" class="hvr-buzz-out"><img src="upload-img/home_03.jpg"></a>
                        </li>
                        <li class="hidden-xs"><a href="" class="hvr-buzz-out"><img src="upload-img/home_05.jpg"></a>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>