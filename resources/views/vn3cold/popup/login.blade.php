<!-- login -->
<div class="modal fade modal-lg" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content login">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Đăng nhập hệ thống</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="button-lg">
                    <a href="{{ route('google_login') }}" title="" >
                        <img src="{{ asset('vn3c/upload-img/btn-lg-google.jpg') }}" alt="">
                    </a>
                    <a href="{{ $urlLoginFace }}" title="">
                        <img src="{{ asset('vn3c/upload-img/btn-lg-fb.jpg') }}" alt="">
                    </a>

                </div>
                <form action="/dang-nhap-vn3c" method="post" >
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Tài khoản:</label>
                        <input type="text" name="email" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Mật khẩu:</label>
                        <input type="password" name="password" class="form-control" id="recipient-name">
                    </div>
                    <div class="checkbox">
                        <input type="checkbox" class="">
                        <span>
                            Ghi nhớ mật khẩu
                         </span>
                    </div>
                    <div class="form-group">
                        <button type="" class="btn-submit" >Đăng Nhập</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer hidden-xs">
                <span class="left">
                     <a class="btn btn-link" href="#" onclick="return forgetPassword(this);">
                        Quên mật khẩu?
                    </a>
                </span>
                <span class="right"><a href="" title="">Bạn chưa có tài khoản ? </a><a href="/dang-ky" title="">Hãy đăng kí</a></span>
            </div>

            <div class="modal-footer hidden-md">
                <span class="left"><a href="" title="">Quên mật khẩu ? </a></span>
                <span class="right"><a href="/dang-ky" title="">Bạn chưa có tài khoản ? </a><a href="/dang-ky" title="">Hãy đăng kí</a></span>
            </div>
        </div>
    </div>
</div>