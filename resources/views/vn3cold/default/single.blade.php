@extends('vn3c.layout.site')

@section('type_meta', 'website')
@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
    <section class="banner-detail-new" style="background-image: url({{ asset($category->image) }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="title-ct-new">
                        {!! $post->title !!}
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="content-detail-new">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="link-url">
                        <?php \App\Entity\Post::getBreadcrumb($post->post_id, $category->slug)?>
                    </div>
                    <div class="content-it-ct">
						<div class="mb20">
							<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-541a6755479ce315"></script>
							<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
						</div>
                        <div class="mb20 bold description">{{ $post->description }}</div>
                        <div class="mb20 tc">
                            @if (isset($post->image) && !empty($post->image))
								<img src="{{ asset($post->image) }}" />
							@endif
                        </div>
						<div class="contentView">
							{!! $post->content !!}
						</div>
						<div class="optin">
							{!! $post['optinform-dang-ky'] !!}
						</div>
                    </div>

                    <!-- <div class="tag">
                        <span><i class="fa fa-tags" aria-hidden="true"></i></span> Tag : </span>
                        <ul>
                            <li><a href="" title="">thiet ke website</a></li>
                            <li><a href="" title="">thiet ke website</a></li>
                        </ul>
                    </div> -->

                    <div class="involve">
                        <h3 class="title-ct-new">bài viết liên quan</h3>
                        <ul>
                            @foreach (\App\Entity\Post::relativeProduct($post->slug, 5) as $post)
                            <li>
                                <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" title="">{{ $post->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection


