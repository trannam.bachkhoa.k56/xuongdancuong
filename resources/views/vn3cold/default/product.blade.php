@extends('vn3c.layout.site')

@section('type_meta', 'article')
@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', asset($product->image) )
@section('meta_url', route('product', [ 'post_slug' => $product->slug]) )

@section('content')
    <section class="img-product">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="link-url">
                        <?php \App\Entity\Post::getBreadcrumb($product->post_id, 'san-pham')?>
                    </div>
                    <div class="img-pr">
                        <img src="{{ asset($product->image) }}" alt="">
                    </div>
                </div>
                <div class="col-md-4 sidebar-th">
                    <h1>{{ $product->title }}</h1>
					{{--<div class="mb20">--}}
						{{--<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-541a6755479ce315"></script>--}}
						{{--<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>--}}
					{{--</div>--}}
                    <div class="Gift">
                        <p> <i class="fas fa-gift"></i> Miễn phí <span class="red">3 tháng</span> sử dụng hosting.</p>
                        <p> <i class="fas fa-gift"></i> Tặng tên miền quốc tế <span class="red">(280.000 đ)</span> khi đăng ký theme trên 3.000.000 đ. </p>
                        <p> <i class="fas fa-gift"></i> Miễn phí hệ thống <span class="red">đông bộ facebook</span> .</p>
                        <p> <i class="fas fa-gift"></i> Miễn phí Tự động <span class="red">comment (bình luận)</span> bài viết, sản phẩm.</p>
                        <img src="{{ asset('vn3c/img/qua_tang.png') }}" class="giftBox"/>
                        <!-- <img src="{{ asset('vn3c/img/cay_tien.png') }}" class="tree"/> -->
                    </div>
                    <div class="cost">
                        @if (!empty($product->discount))
                            <span class="priceOld">{{ number_format($product->price, 0, ',', '.') }}</span>
                            <span class="priceDiscount">{{ number_format($product->discount, 0, ',', '.') }} VNĐ</span>
                        @elseif (empty($product->price))
                            <span class="priceDiscount">Miễn phí</span>
                        @else
                            <span class="priceDiscount">{{ number_format($product->price, 0, ',', '.') }} VNĐ</span>
                        @endif
                    </div>
                    <!-- <div class="info">
                        {!! $product->content !!}
                    </div> -->
                    <div class="button">
                        <button type="button" class="btn btn-warning">
                            <a href="/view/{{ $product->slug }}">Xem demo</a>
                        </button>
                        {{--<form method="post" onsubmit="return addToOrder(this);">--}}
                            {{--{{ csrf_field() }}--}}
                            {{--<input type="hidden" name="product_id[]" value="{{ $product->product_id }}">--}}
                            {{--<input  type="hidden" name="quantity[]" value="1" min="0">--}}
                            {{--<button class="btn btn-primary" type="submit">Đăng ký sử dụng</button>--}}
                        {{--</form>--}}
                        <button type="button" class="btn btn-primary">
                            <a  data-toggle="modal" data-target="#registerTheme">Đăng ký sử dụng</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="Character">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="function Highlights">
                        <h3>Đặc điểm nổi bật của website</h3>
                        <div class="info">
                            <p><i class="fas fa-check-circle"></i> Thiết kế giao diện mobile responsive hoàn toàn - Hệ thống quản trị nội dung (CMS) - Tối ưu máy tìm kiếm (SEO)</p>
                            <p><i class="fas fa-check-circle"></i> Tích hợp tính năng chia sẻ qua mạng xã hội, hotline, chát skype trực tuyến,...</p>
                            <p><i class="fas fa-check-circle"></i> Giao diện quản trị chuyên nghiệp, bạn không cần phải am hiểu về lĩnh vực CNTT cũng có thể quản trị dễ dàng</p>
                            <p><i class="fas fa-check-circle"></i> Quản trị tự động chăm sóc khách hàng bằng email không cần sử dụng bất kỳ bên thứ 3.</p>
                            <p><i class="fas fa-check-circle"></i> Có hệ thống quản trị trên mobile, dễ dàng phát triển thương hiệu,...</p>
                            <p><i class="fas fa-check-circle"></i> ... Và hơn thế nữa</p>
                        </div>
                    </div>

                    <div class="function mb30">
                        <h3>Chúng tôi cam kết</h3>
                        <div class="info">
                            <p><i class="fas fa-check-circle"></i> Thiết kế website theo đúng thiết kế.</p>
                            <p><i class="fas fa-check-circle"></i> Cài đặt website cho khách hàng.</p>
                            <p><i class="fas fa-check-circle"></i> Đảm bảo tiến độ, sửa lỗi nếu có.</p>
                            <p><i class="fas fa-check-circle"></i> Đảm bảo bảo mật ở mức cơ bản.</p>
                        </div>
                    </div>

                    <div class="function mb30">
                        <h3>Bảo hành</h3>
                        <div class="info">
                            <p><i class="fas fa-check-circle"></i> Bảo hành, bảo trì website trọn đời.</p>
                            <p><i class="fas fa-check-circle"></i> Bảo mật tuyệt đối thông tin, dữ liệu lưu trữ trên hosting bên chúng tôi.</p>
                            <p><i class="fas fa-check-circle"></i> Hỗ trợ khách hàng miễn phí trong quá trình sử dụng website.</p>
                        </div>
                    </div>

                    <div class="function mb30">
                        <div class="">
                            @include('general.sub_comments', ['post_id' => $product->post_id] )
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="service">
                        <h3 class="title-bg">
                            Các dịch vụ di kèm nếu có
                        </h3>
                        @foreach (\App\Entity\Menu::showWithLocation('side-right-menu') as $menu)
                            {!! \App\Entity\MenuElement::showMenuElementPage($menu->slug, '') !!}
                        @endforeach
                    </div>

                </div>
            </div>
        </div>

    </section>


    <!-- login -->
    <div class="modal fade modal-lg" id="registerTheme" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content login">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Đăng ký nhanh tay, sở hữu ngay</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2>Đăng ký nhanh tay, sở hữu ngay</h2>
                    <form action="{{ route('sub_contact') }}" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Họ và tên: </label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="name" aria-describedby="emailHelp" placeholder="Nhập họ và tên của bạn">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email: </label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Nhập email của bạn">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại: </label>
                                    <input type="number" class="form-control" id="exampleInputEmail1" name="phone" aria-describedby="emailHelp" placeholder="Số điện thoại">
                                </div>
                            </div>
                        </div>
                        <div class="ip-textarea">
                            <input type="hidden" class="form-control" id="exampleInputEmail1" name="message" value="{!! $product->code !!}">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Gửi thông tin</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
