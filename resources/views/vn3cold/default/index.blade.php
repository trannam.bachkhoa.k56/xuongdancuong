@extends('vn3c.layout.site')

@section('title', $information['meta_title'])
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])
@section('type_meta', 'website')

@section('content')
    <!-- @include('vn3c.partials.slide')-->
    <section class="BoxThree bgGray mb30">

      
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 item-icon">
                    <h1 class="hello_title">Nền tảng website bán hàng google và facebook tốt nhất</h1>
                    <div class="underline" style="height: 2px; background: #2888da; width: 30%;"></div>
                    <div>
                        <h4 class="expectHello"> Sự kết hợp tinh tế giữa bán hàng google và facebook </h4>
                    </div>
                    <p>
                        <a class="btnTry" href="/trang/tao-moi-website">Dùng thử miễn phí</a>
                    </p>
                </div>
                <div class="col-md-6 col-sm-12 item-icon">
                    <img src="{{ asset('/vn3c/img/girl_chao.png') }}" style="max-width: 100%; margin-top: 60px;"/>
                </div>
            </div>
        </div>
    </section>

	
	 <section class="getflyCrm">
        <div class="container">
            <div class="title">
                <p>Làm website với vn3c giúp ích gì cho bạn</p>
                <h2>Chúng tôi làm website cung cấp các tính năng hoàn hảo giúp bạn chốt đơn hàng chỉ sau 3s</h2>
            </div>
            <div class="row">
                <div class="col-md-6 item">
                    <div class="row">
                        <div class="col-md-3 tc">
							<i class="iconfa far fa-thumbs-up"></i>
                        </div>
                        <div class="col-md-9">
                            <h3>WEBSITE CHUẨN SEO</h3>
                            <p>Giao diện đẹp mắt, tối ưu website (Mobile - PC - Table), tối ưu tốc độ load trang nhanh theo yêu cầu đưa ra của google, tối ưu seo, xác nhận địa chỉ google map</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 item">
                    <div class="row">
                        <div class="col-md-3 tc">
                            <i class="iconfa fas fa-exchange-alt"></i>
                        </div>
                        <div class="col-md-9">
                            <h3>ĐỒNG BỘ WEBSITE VỚI FACEBOOK</h3>
                            <p>Tìm kiếm bài viết nhiều like và comment nhất trên facebook từ website, tự động gửi request kết bạn, đồng bộ bài viết, sản phẩm với cả fanpage facebook và trang cá nhân.</p>
                        </div>
                    </div>
                </div>
				<div class="col-md-6 item">
                    <div class="row">
                        <div class="col-md-3 tc">
                            <i class="iconfa fas fa-user-circle"></i>
                        </div>
                        <div class="col-md-9">
                            <h3>WEBSITE GIÁ CẠNH TRANH</h3>
                            <p>Chúng tôi có những gói website từ cơ bản đến chuyên nghiệp với giá thành chi phí phải chăng với túi tiền của bạn, giúp bạn bán hàng online tốt nhất.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 item">
                    <div class="row">
                        <div class="col-md-3 tc">
                            <i class="iconfa fas fa-comments"></i>
                        </div>
                        <div class="col-md-9">
                            <h3>TẠO WEBSITE LẤY LÒNG TIN VỚI KHÁCH HÀNG</h3>
                            <p>Tự động tạo bình luận trên website hoàn toàn tự động, sinh đánh giá đối với sản phẩm. hỗ trợ chat trực tuyến của facebook vào website, .... </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 item">
                    <div class="row">
						<div class="col-md-3 tc">
                            <i class="iconfa fab fa-rocketchat"></i>
                        </div>
                        <div class="col-md-9">
                            <h3>CHĂM SÓC KHÁCH HÀNG TỐT NHẤT TRÊN WEBSITE</h3>
                            <p>Chăm sóc khách hàng không chỉ là một yếu tố giúp doanh nghiệp của bạn sở hữu được những khách hàng thân thiết mà còn khẳng định được thương hiệu trên thị trường. Chatbox hiện tại đang là công cụ hàng đầu hỗ trợ việc bán hàng cho các khách hàng thân quen. Với tính năng thông minh này, bạn sẽ không phải lo lắng khách hàng của mình sẽ phàn nàn về chất lượng dịch vụ, từ đó tạo nên uy tín tốt cho doanh nghiệp.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 item">
                    <div class="row">
                        <div class="col-md-3 tc">
                            <i class="iconfa fas fa-users"></i>
                        </div>
                        <div class="col-md-9">
                            <h3>LẤY THÔNG TIN NGƯỜI DÙNG TỪ GROUP FACEBOOK</h3>
                            <p>Khi bán hàng, bạn phải tham gia vào nhiều group khác nhau để tìm kiếm khách hàng. Tuy nhiên, không phải group nào cũng phê duyệt bài quảng cáo của bạn. Website của chúng tôi sẽ hỗ trợ bạn lấy thông tin người dùng từ những group này thuận lợi cho việc bán hàng của bạn hơn. </p>
                        </div>
                    </div>
                </div>
            </div>
          
            <!-- Button trigger modal -->


            <!-- Modal -->
          
        </div>
        <script>
        //Đồng bộ chiều cao các div
            $(function() {
                $('.getflyCrm .item').matchHeight();
            });
        </script>
    </section>  
	
	
	
	
	 <section class="marketingFacebook">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 fbNopadding colleftFB">
                    <div class="leftFB">
                        <div class="content">
                            <h2>Tất cả những gì bạn cần <br>để bán hàng khắp mọi nơi
                            </h2>
                            <div class="form">
                                <div id="getfly-optin-form-iframe-1527559390562"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=a1LbhUyCxpXIdzSPmGhkiWc7t0qpIZQ9d7zDGyLA736pwWdvnC&referrer="+r); f.style.width = "100%";f.style.height = "350px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1527559390562");s.appendChild(f); })(); </script>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 fbNopadding colrightFB">
                    <div class="rightFB">
                        <div class="wapper">
                            <div class="cricle">
                                <div class="contentcricle">
                                    <a href="" title=""> 
                                        <img src="{{asset('vn3c/upload-img/vn3comichanel.png')}}" alt="">
                                    </a> 
                                </div>
                            </div>
                        </div>
                        <div class="textsologan">
                           <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fvn3ctran%2F&width=450&layout=standard&action=like&size=small&show_faces=false&share=true&height=35&appId" width="100%" height="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
	
	<div class="clearfix"></div>
    <section class="boxWeb tc">
        <div class="container">
			<div class="col-12">
				<div class="title">
					<p>{{ $information['lua-chon-website-phu-hop-voi-ban'] }}</p>
					<h2>{{ $information['cac-giao-dien-moi-duoc-cap-nhat'] }}</h2>
				</div>
			</div>
        </div>
    </section>
    <section class="Product bgGray">
        <div class="container">
            <div class="listProduct">
                <div class="tab-center">
                    <ul class="nav nav-tabs tab-ul justify-content-center" id="myTab" role="tablist">
                        @foreach (\App\Entity\Menu::showWithLocation('menu-tab-index') as $id => $menu)
							<li class="nav-item">
								<a class="nav-link {{ ($id == 0) ? 'active' : '' }}" id="home-tab" data-toggle="tab"
								   href="#tab{{ $id }}" role="tab" aria-controls="tab{{ $id }}" aria-selected="true">
									{{ $menu->title }}</a>
							</li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content" id="ListProduct">
                    @foreach (\App\Entity\Menu::showWithLocation('menu-tab-index') as $id => $menu)
                            <div class="tab-pane fade show {{ ($id == 0) ? 'active' : '' }}" id="tab{{ $id }}" role="tabpane{{ $id }}" aria-labelledby="home-tab">
                                <div class="row product-row">
									@foreach (\App\Entity\MenuElement::showMenuPageArray($menu->slug) as $id => $menuElement)
                                    <div class="col-lg-4 col-md-6 col-sm-12 item">
                                        <figure class="pro-item">
                                            <img src="{{ asset($menuElement['image']) }}" />
                                            <figcaption>
                                                <div class="button">
                                                    <button type="button" class="btn btn-primary">
                                                        <a href="{{ $menuElement['url'] }}">Xem chi tiết danh mục</a>
                                                    </button>
                                                </div>
                                            </figcaption>
                                            <h3>
                                                <a href="{{ $menuElement['url'] }}">
                                                    {{ $menuElement['title_show'] }}
                                                </a>
                                            </h3>
                                        </figure>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                    @endforeach
                </div>
            </div><!-- end listproduct -->
			<div class="ReadMore tc">
                <div class="button">
                    <button type="button" class="btn btn-warning"><a href="/cua-hang/website">Xem kho giao diện</a></button>
                    <button type="button" class="btn btn-primary"><a href="/trang/tao-moi-website">Đăng ký lic miễn phí</a></button>
                </div>
            </div>
        </div>
    </section>
	<section class="getflyCrm">
		<div class="container">
			<div class="clearfix textBox borBlue mt20">
				Bạn đã có công cụ tuyệt vời nào để bán hàng online chưa. VN3C web là giải pháp không thể tuyệt vời hơn dành cho cửa hàng của bạn. Nó sẽ giúp bạn tìm kiếm khách hàng mới trên facebook, chăm sóc khách hàng đã mua hàng trên facebook. Với tiêu chí chỉ <b>3s khách hàng không thể bỏ bạn mà đi được</b>. Đăng ký dùng thử ngay nhé
			</div>
		</div>
	</section>	
    @include('vn3c.partials.customer_say')
@endsection

