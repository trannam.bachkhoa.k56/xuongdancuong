@extends('news-03.layout.site')
@section('title','Gửi đơn hàng thành công')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] :'')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    {{--@include('site.partials.menu_main', ['classHome' => ''])--}}
   <p>Bạn đã mua hàng thành công</p>
    <a href="/">Quay lại trang chủ</a>
@endsection
