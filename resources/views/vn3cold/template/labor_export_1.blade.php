<!DOCTYPE html>
<!-- saved from url=(0059)http://slimweb.vn/vinno-slimweb2/get/field_lp_sitepage/9832 -->
<html lang="en" class="js no-touch backgroundsize csstransforms3d csstransitions" data-sbro-deals-lock="true" style="">
<!--<![endif]-->
<head>
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:type" content="article"/>

    <meta property="og:title" content="NHẬT BẢN KHÔNG PHẢI LÀ THIÊN ĐƯỜNG!" />
    <meta property="og:description" content="Nhật Bản không cho bạn những ngày tháng vô tư vô lo, ung dung tự tại…" />
    <meta property="og:url" content="http://vn3c.net/xuat-khau-lao-dong-nhat-ban/dang-ky-xuat-khau-lao-dong-nhat-ban" />
    <meta property="og:image" content="{{ asset('labor_export/xuat_khau_lao_dong_2.jpg') }}" />
    <meta property="og:image:secure_url" content="{{ asset('labor_export/xuat_khau_lao_dong_2.jpg') }}" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }
    </style>

    <style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }
    </style>
    <style type="text/css">@media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }
    </style>
    <style type="text/css">.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }</style>

    <style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }</style>
    <style type="text/css">@media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }</style>
    <style type="text/css">.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }</style>
    <style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }</style>
    <style type="text/css">@media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }</style>
    <script async="" src="{{ asset('laborExport/ex_1/js/analytics.js') }}"></script>
    <title>OLECO xuất khẩu lao động nhật bản</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="OLECO chúng tôi cung cấp các giải pháp hàng đầu cho thực tập sinh về xuất khẩu lao động tại Nhật Bản.">
    <meta name="author" content="oleco">

    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminstration/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/social-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/mediaelementplayer.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/jquery.fancybox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/owl.theme.default.css') }}">

    <style>
    </style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style id="fit-vids-style">.fluid-width-video-wrapper {
            width: 100%;
            position: relative;
            padding: 0;
        }

        .fluid-width-video-wrapper iframe, .fluid-width-video-wrapper object, .fluid-width-video-wrapper embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }

        .gm-style img {
            max-width: none;
        }</style>
</head>
<body data-spy="scroll" data-target=".header-menu-container" data-offset="61">
<div id="page">
    <header id="header-section-2" class="header-section header-style-2">
        <div class="header-section-container">
            <div class="header-menu">
                <div class="header-menu-container">
                    <nav class="navbar">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-2">
                                    <p style="text-align: center;">
                                        <img src="{{ asset('laborExport/img/oleco_logo_03.png') }}" alt="logo" width="200"/>
                                    </p>
                                </div>
                                <div class="col-xs-12 col-md-10">
                                    <div class="titleHeader">
                                        <h2 class="title" style="text-align: center;">NHẬT BẢN KHÔNG PHẢI LÀ THIÊN ĐƯỜNG!</h2>
                                        <h1 style="color: #365899; text-align: center;">
                                            Nếu bạn đã sẵn sàng thay đổi bản thân và mong muốn có cơ hội đặt chân đến đất nước Nhật Bản để sinh sống và làm việc, chúng tôi sẽ đồng hành cùng bạn
                                            </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    @include('vn3c.labor_export.slide')
    @include('vn3c.labor_export.introduction')
    @include('vn3c.labor_export.library')
    @include('vn3c.labor_export.footer')

</div>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/mobile.min.js') }}"></script>
<script src="{{ asset('laborExport/ex_1/js/owl.carousel.js') }}"></script>
<script>
    $('.introductionSlide').owlCarousel({
        margin:10,
        nav:true,
        loop: true,
        navText: false,
        dotData: true,
        items: 1,
        autoplayTimeout: 3000,
        autoplay: true
    });
</script>

<style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff; width: 60px; height: 60px; text-align: center; bottom: 30px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{width: 120px; left: -125px; top: -8px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style><div class="fb-livechat"> <div class="ctrlq fb-overlay"></div><div class="fb-widget"> <div class="ctrlq fb-close"></div><div class="fb-page" data-href="https://www.facebook.com/olecoxkld/" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div><div class="fb-credit"> <a href="https://vn3c.com" target="_blank">Powered by VN3C</a> </div><div id="fb-root"></div></div><a href="https://m.me/olecoxkld" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button"> <div class="bubble">1</div><div class="bubble-msg">Nhắn tin với chúng tôi!</div></a></div><script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><script>$(document).ready(function(){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){return true;}else{return false;}}var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")}; setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>

<div class="showPhone clearfix">
   <a href="tel:0338.157.256" class="fancybox">
	<div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show" id="coccoc-alo-phoneIcon" style=""> 
			<div class="coccoc-alo-ph-circle"></div> 
			<div class="coccoc-alo-ph-circle-fill"></div> <div class="coccoc-alo-ph-img-circle"></div> 
		</div>
	</a>
</div>
</body>
</html>