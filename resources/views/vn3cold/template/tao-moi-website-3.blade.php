	@extends('vn3c.layout.create-theme')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('page', ['post_slug' => $post->slug]) )

@section('content')
    <section class="create-theme">

        <img src="{{ $information['anh-nen-tao-theme'] }}" alt="" class="bg-img">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div>
                        @foreach (\App\Entity\Product::getAllProduct() as $id => $theme)
                            <img class="imagePage {{ $theme->code }}" src="{{ asset($theme->image) }}" title="image page" @if ($id != 0) style="display: none;" @endif/>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-7 right-ft">
                    <img src="{{ asset('vn3c/upload-img/bg-form.png') }}" alt="">
                    <div class="content">
                        <h1>tạo website 30s</h1>
                        <div class="form-bg">
                            <form method="POST" action="" id="createWebsite" onSubmit="return contact(this);">
                                {{ csrf_field() }}
								<input type="hidden" name="is_json" class="form-control captcha" value="1" placeholder="">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Tên cửa hàng</label>
                                    <div class="col-sm-9">
                                        http://<input type="text" name="name" value="{{ old('domain') }}" placeholder="ten-cua-hang" required/>.vn3c.net
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Lựa chọn Theme</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" name="message[]" onchange="return changeTheme(this);">
                                            @foreach (\App\Entity\Product::getAllProduct() as $theme)
												@if ($theme['co-hien-thi-chon-theme'] == 1)
												<option value="{{ $theme->code }}" {{ (old('theme_code') == $theme->code) ? 'selected' : '' }}
														{{ (empty(old('theme_code')) && isset($_GET['theme']) && $_GET['theme'] == $theme->code) ? 'selected' : '' }}
												>
                                                {{ $theme->title }}</option>
												@endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <script>
                                    function changeTheme(e) {
                                        var theme = $(e).val();
                                        $('.imagePage').hide();
                                        $('.' + theme).show();
                                    }
                                </script>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Số điện thoại</label>
                                    <div class="col-sm-9">
                                        <input type="number" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Số điện thoại" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Mật khẩu</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="message[]" class="form-control" id="inputPassword" placeholder="Mật khẩu" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Nhập lại MK</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="message[]" class="form-control" id="inputPassword" placeholder="Nhập lại mật khẩu" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                        @if ($errors->has('errorDomain'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('errorDomain') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-submit" onclick="return clickRegister(this);" data-loading-text="Đang trong quá trình tạo website">Đăng kí</button>
                                    </div>
                                </div>
								<script>
									function clickRegister(e) {
										var btn = $(e).html("Đang trong quá trình tạo website");
										$(e).attr('style', 'background: #5a6268;');
										$(e).prop('disabled', true);
										
										$('#createWebsite').submit();
									}
									
									function contact(e) {
										var $btn = $(e).find('button').button('loading');
										var data = $(e).serialize();

										$.ajax({
											type: "POST",
											url: '{!! route('sub_contact') !!}',
											data: data,
											success: function(result){
												var obj = jQuery.parseJSON( result);
												// gửi thành công
												if (obj.status == 200) {
													alert('Cảm ơn bạn đã đăng ký dùng thử website bên tôi, chúng tôi sẽ sớm liên lạc và tư vấn cho bạn.');
													$btn.button('reset');

													return;
												}

												// gửi thất bại
												if (obj.status == 500) {
													alert(obj.message);
													$btn.button('reset');

													return;
												}
											},
											error: function(error) {
												//alert('Lỗi gì đó đã xảy ra!')
											}

										});


										return false;
									}
								</script>

                            </form>
                            <span class="left"><a href="" title="">Quên mật khẩu ? </a></span>
                            <span class="right"><a href="" title="">Bạn có tài khoản ? </a> <a href="" title="">Đăng nhập</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


