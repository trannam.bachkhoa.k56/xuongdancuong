@extends('vn3c.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('page', ['post_slug' => $post->slug]) )

@section('content')
   
  <!-- Main Content Pages -->
            <div class="content-pages pd20">
                <!-- Subpages -->
                <div class="sub-home-pages">
          <!-- Blog Subpage -->
                    <section id="blog" class="sub-page">
                        <div class="sub-page-inner">
                            <div class="section-title">
                                <div class="main-title">
                                    <div class="title-main-page">
                                        <h4>Khách hàng của Moma</h4>
                                        <p>Khách hàng đã tin tưởng sử dụng dịch vụ website của MOMA</p>
                                    </div>
                                </div>
                            </div>

                            <div class="section-content">
                                <div class="row blog-grid-flex">
                                    @foreach (App\Entity\Domain::showDomainCustomer() as $id => $domain)
									@if($domain->logo != '/libraries/libraryxhome-1/images/logo_vuong.png')
                    
                                      <div class="col-md-3 col-sm-4 blog-item">
                                          <div class="blog-article">
                                              <div class="CropImg">
                                                  <a class="thumbs" href="{{$domain->url }}" class="img-responsive" alt="{{ $domain->name }}">
                                                    <img src="{{isset($domain->logo) ? $domain->logo : '/libraries/libraryvn3c-1/images/LogoMoMa%20(1).jpg' }}">
                                                  </a>
                                              </div>
                                              <div class="article-link"> <a href="{{$domain->url }}"> 
                                                <img src="{{isset($domain->logo) ? $domain->logo : '' }}"><i class="lnr lnr-arrow-right"></i></a> </div>
                                              <div class="article-content">
                                                  <h4><a href="{{$domain->url }}">{{$domain->name}}</a></h4>
                                                  <?php $date = date_create($domain->created_at); ?>
                                                  <div class="meta"> <span>{{ $domain->url }}  |  {{ date_format($date,"d/m/Y H:i") }}</span></div>
                                                 
                                              </div>
                                          </div>
                                      </div>
									@endif
                                    @endforeach
                                </div>
                                <div class="pagination-nav nav-center">
            
                                    <!--<a href="#" class="btn btn-prev"><i class="lnr lnr-arrow-left"></i> prev</a> 
                                    <a href="#" class="btn btn-next">next <i class="lnr lnr-arrow-right"></i></a>-->
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /Blog Subpage -->
                </div>
                <!-- /Page changer wrapper -->
            </div>
            <style type="text/css">
              .CropImg:before {
                padding-bottom: 50%;
            }
            </style>
            <!-- /Main Content -->
@endsection

