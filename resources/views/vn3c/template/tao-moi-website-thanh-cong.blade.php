@extends('vn3c.layout.create-theme')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('page', ['post_slug' => $post->slug]) )

@section('content')
    <div class="content-pages pdTop60 pdb60">
        <!-- Subpages -->
        <div class="sub-home-pages pdTop60 pd20">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 right-ft">
                        <div class="content">
                            <h1 class="f24 mgBottom20">ĐĂNG KÝ WEBSITE THÀNH CÔNG</h1>
                            <div class="form-bg">
                                <div class="content">
                                    <div class="form-bg">
                                        @if (isset($_GET['website']))
                                            <h3>Cảm ơn bạn đã đăng ký website miễn phí MOMA!</h3>
                                            <p class="pSuccess">
                                                Để quản trị website bạn vui lòng truy cập:
                                            </p>
                                            <p class="pSuccess">
                                                <a href="{{ $_GET['website'] }}/admin" target="_blank">
                                                    {{ $_GET['website'] }}/admin
                                                </a>
                                            </p>
                                            <p class="pSuccess">
                                                Tài khoản truy cập: email của bạn và password bạn vừa tạo.
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .pSuccess {
            font-size: 18px;
        }
    </style>
@endsection


