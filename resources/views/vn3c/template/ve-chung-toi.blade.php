@extends('vn3c.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )
@section('type_meta', 'website')

@section('content')
    <!-- Main Content Pages -->
    <div class="content-pages">
        <!-- Subpages -->
        <div class="sub-home-pages">
           <!-- About Me Subpage -->
			<section id="about-me" class="sub-page">
				<div class="container">
					<div class="section-title">
						<div class="main-title">
							<div class="title-main-page">
								<h4>MOMA</h4>
								<p>Giới thiệu về công ty cổ phần tri thức vì dân</p>
							</div>
						</div>
					</div>
					<div class="section-content">
						<!-- about me -->
						<div class="row pb-30">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h3>CÔNG TY CỔ PHẦN TRI THỨC VÌ DÂN</h3>
								<span class="about-location"><i class="lnr lnr-map-marker"></i> Tầng 3, số nhà 9B, Lê Văn Thiêm, Hà Nội</span>
								<div class="about-content justify">
									{!! isset($information['gioi-thieu-ve-cong-ty']) ? $information['gioi-thieu-ve-cong-ty'] : '' !!}
								</div>
								<ul class="bout-list-summry row">
									<li class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="icon-info">
											<i class="lnr lnr-briefcase"></i> 
										</div>
										<div class="details-info">
											<h6>9 Năm</h6>
											<p>Kinh nghiệm</p>
										</div>
									</li>
									<li class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="icon-info">
											<i class="lnr lnr-layers"></i> 
										</div>
										<div class="details-info">
											<h6>300+</h6>
											<p>Dự án</p>
										</div>
									</li>
									<li class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="icon-info">
											<i class="lnr lnr-coffee-cup"></i> 
										</div>
										<div class="details-info">
											<h6>1500+</h6>
											<p>Khách hàng</p>
										</div>
									</li>
								</ul>
							</div>

							<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
								<div class="box-img">
									<img src="{{ asset('assets/vn3ctheme/img/about.png') }}" class="img-fluid" alt="image">
								</div>
							</div>
						</div>
						<!-- /about me -->

						@include('vn3c.partials.services')
						@include('vn3c.partials.customer_say')
						@include('vn3c.partials.testemorial')
					</div>
				</div>
			</section>
			<!-- About Me Subpage -->
        </div>
        <!-- /Page changer wrapper -->
    </div>
    <!-- /Main Content -->
@endsection

