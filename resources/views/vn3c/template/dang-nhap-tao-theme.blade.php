@extends('vn3c.layout.create-theme')

@section('title', 'Đăng nhập tạo theme')
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('page', ['post_slug' => $post->slug]) )

@section('content')
    <section class="create-theme">
        <img src="{{ $information['anh-nen-tao-theme'] }}" alt="" class="bg-img">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                </div>
                <div class="col-md-7 right-ft">
                    <img src="{{ asset('vn3c/upload-img/bg-form.png') }}" alt="">
                    <div class="content">
                        <h1>tạo website 30s</h1>
                        <div class="form-bg">
                            <form method="POST" action="/dang-nhap-vn3c">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Mật khẩu</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Mật khẩu" required/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn-submit">Đăng nhập</button>
                                    </div>
                                </div>

                            </form>
                            <span class="left"><a href="" title="">Quên mật khẩu ? </a></span>
                            <span class="right">
                                <a href="/trang/tao-moi-website" title="">Đăng ký</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


