@extends('vn3c.layout.site')
@section('title',' Làm website tại Moma chỉ với giá')
@section('meta_description', 'Làm website tại moma, xem chi tiết bảng giá báo giá website tại moma chuyên nghiệp nhất')
@section('keywords', '')

@section('content')
	
	<section class="content-contact content-pages">
	
		<div class="container">
			<div class="section-title">
				<div class="main-title">
					<div class="title-main-page">
						<!--h1>{{$post->title}}</h1>
						<p>Thông tin các dịch vụ công ty cổ phần tri thức vì dân</p-->
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	<section class="content-price">
        <div class="container">
		{!! isset($information['bang-gia-dich-vu-moma']) ? $information['bang-gia-dich-vu-moma'] : '' !!}
			<!--section class="content">
			
				 <div class="row">
							
				<div class="col-xs-12 col-md-3 mgt10 no-padding">

					<div class="box box-widget widget-user-2" style="overflow-x: scroll;">
					
						<div class="widget-user-header bg-vip">
							<h3 class="center vip" >
							<b>GÓI WEBSITE MIỄN PHÍ</b></h3>
							<div class="gw-go-price-wrap">
								<span>
									<i style="color:#fff; font-size:76px;" class="fa fa-cloud"></i>
								</span>
								
								</small>
							</div>
							
							<p class="center"><i>(Tiết kiệm 0 đồng )</i></p>
							<h4 class="center"><b>Miễn phí</b></h4>
						</div>
						
						<div class="box-footer box-footer0 no-padding">
							<ul class="nav nav-stacked">
								<li><a href="#">Thời gian sử dụng: <span class="color-red textUpper">15 Ngày</span></a></li>
								<li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
								<li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
								<li><a href="#">Website 1 ngôn ngữ</a></li>
								<li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
								<li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
								<li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
								<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
								<li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
								<li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
								<li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
								<li><a href="#">Số lượng sản phẩm đăng tải: <span class="color-red textUpper">Tối đa 30 sản phẩm</span></a></li>
								<li><a href="#">Số lượng tin tức đăng tải: <span class="color-red textUpper">Tối đa 30 Tin Tức</span></a></li>
								<li><a href="#">Số lượng khách hàng: <span class="color-red textUpper">30 khách hàng </span></a></li>
								<li><a href="#">Thay đổi tên miền: <span class="color-red textUpper">Không</span></a></li>
								<li>
								<a href="https://moma.vn/trang/tao-moi-website" style="width: 100%; margin-top: 10px; color:#fff" class="btn btn-success">Chọn </a></li>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-3 mgt10 no-padding">
					<div class="box box-widget widget-user-2">
						
						<div class="widget-user-header bg-vip1">
							<h3 class="center vip1" >
							<b>GÓI WEBSITE 1</b></h3>
							<div class="gw-go-price-wrap">
								<span>
									<i style="color:#fff; font-size:76px;" class="fa fa-cloud"></i>
								</span>
								
								</small>
							</div>
							
							<p class="center"><i>(Tiết kiệm 0 đồng)</i></p>
							<h4 class="center"><b>300.000 VND</b></h4>
						</div>
					
						<div class="box-footer1 box-footer no-padding">
							<ul class="nav nav-stacked">
								<li><a href="#">Thời gian sử dụng: <span class="textUpper color-blue">1 Tháng</span></a></li>
								<li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
								<li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
								<li><a href="#">Website 1 ngôn ngữ</a></li>
								<li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
								<li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
								<li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
								<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
								<li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
								<li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
								<li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
								<li><a href="#">Số lượng sản phẩm đăng tải: <span class="textUpper color-blue">Không giới hạn</span></a></li>
								<li><a href="#">Số lượng tin tức đăng tải: <span class="textUpper color-blue">Không giới hạn</span></a></li>
								<li><a href="#">Số lượng khách hàng: <span class="textUpper color-blue">Không giới hạn</span></a></li>
								<li><a href="#">Thay đổi tên miền: <span class="textUpper color-red">Không</span></a></li>
								<li>
								<a href="https://moma.vn/trang/tao-moi-website" style="width: 100%; margin-top: 10px; color:#fff" class="btn btn-success">Chọn </a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-md-3 mgt10 no-padding">

					<div class="box box-widget widget-user-2">
						
						<div class="widget-user-header bg-vip2">
							<h3 class="center vip2" >
							<b>GÓI WEBSITE 2</b></h3>
							<div class="gw-go-price-wrap">
								<span>
									<i style="color:#fff; font-size:76px;" class="fa fa-cloud"></i>
								</span>
								
								</small>
							</div>
						
							<p class="center"><i>(Tiết kiệm 94.000 VNĐ)</i></p>
							<h4 class="center"><b>806.000 VNĐ</b></h4>
						</div>
						
						<div class="box-footer box-footer2 no-padding">
							<ul class="nav nav-stacked">
								<li><a href="#">Thời gian sử dụng: <span class="color-blue textUpper">3 Tháng</span></a></li>
								<li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
								<li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
								<li><a href="#">Website 1 ngôn ngữ</a></li>
								<li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
								<li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
								<li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
								<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
								<li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
								<li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
								<li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
								<li><a href="#">Số lượng sản phẩm đăng tải: <span class="color-blue textUpper">Không giới hạn</span></a></li>
								<li><a href="#">Số lượng tin tức đăng tải: <span class="color-blue textUpper">Không giới hạn</span></a></li>
								<li><a href="#">Số lượng khách hàng: <span class="color-blue textUpper">Không giới hạn</span></a></li>
								<li><a href="#">Thay đổi tên miền: <span class="color-red textUpper">Không</span></a></li>
								<li>
								<a href="https://moma.vn/trang/tao-moi-website" style="width: 100%; margin-top: 10px; color:#fff" class="btn btn-success">Chọn </a></li>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-xs-12 col-md-3 mgt10 no-padding">

					<div class="box box-widget widget-user-2">
						
						<div class="widget-user-header bg-vip3">
							<h3 class="center vip3" >
							<b>GÓI VIP</b></h3>
							<div class="gw-go-price-wrap">
								<span>
									<i style="color:#fff; font-size:76px;" class="fa fa-cloud"></i>
								</span>
								
								</small>
							</div>
						
							<p class="center"><i>(Tiết kiệm 1.200.000 VNĐ)</i></p>
							<h4 class="center"><b>2.400.000 VNĐ</b></h4>
						</div>
						<div class="box-footer box-footer3 no-padding">
							<ul class="nav nav-stacked">
								<li><a href="#">Thời gian sử dụng: <span class="textUpper color-blue">1 năm</span></a></li>
								<li><a href="#">Đăng ký Website lên các công cụ tìm kiếm</a></li>
								<li><a href="#">Hỗ trợ mọi phiên bản màn hình <b>SMARTPHONE</b> và <b>MÁY TÍNH BẢNG</b></a></li>
								<li><a href="#">Website 1 ngôn ngữ</a></li>
								<li><a href="#">Khởi tạo, tích hợp Google Analytics</a></li>
								<li><a href="#">Chuẩn SEO, đầy đủ các công cụ hỗ trợ SEO</a></li>
								<li><a href="#">Tích hợp Fanpage, Google map trên website</a></li>
								<li><a href="#">Tích hợp công cụ bán hàng đa kênh</a></li>
								<li><a href="#">Tích hợp công cụ phân tích đối thủ</a></li>
								<li><a href="#">Tích hợp công cụ quản lý vận chuyển</a></li>
								<li><a href="#">Tích hợp công cụ quản lý mạng xã hội</a></li>
								<li><a href="#">Số lượng sản phẩm đăng tải: <span class="textUpper color-blue">Không giới hạn</span></a></li>
								<li><a href="#">Số lượng tin tức đăng tải: <span class="textUpper color-blue">Không giới hạn</span></a></li>
								<li><a href="#">Số lượng khách hàng: <span class="textUpper color-blue">Không giới hạn</span></a></li>
								<li><a href="#">Thay đổi tên miền: <span class="textUpper color-blue">Miễn phí</span></a></li>
								<li>
								<a href="https://moma.vn/trang/tao-moi-website" style="width: 100%; margin-top: 10px; color:#fff" class="btn btn-success">Chọn </a>
								</li></li>
							</ul>
						</div>
					</div>
				</div>

			</section-->
				
				 <!--
				<section class="content-contact content-pages">
					<div class="container">
						<div class="section-title">
							<div class="main-title">
								<div class="title-main-page">
									
									<p style="font-size:24px">GIA HẠN THỜI GIAN SỬ DỤNG GÓI EMAIL MARKETING</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="content">
			 
					<div class="row">
				
						<div class="col-xs-12 col-md-12 mgt10">

							<div class="box box-widget widget-user-2">

								<div class="widget-user-header bg-blue">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png" />
									</div>
			
									<h3 class="center"><b>GÓI DOANH NGHIỆP VỪA VÀ NHỎ </b></h3>
									<h4 class="center"><b>5.000 Email trong 1 tháng</b></h4>
								</div>
								
								<div class="box-footer no-padding bg-footer-blue">
									<table class="table" style="overflow-x: scroll;">
										  <thead class="vip2">
											<tr>
											  <th scope="col">Gói Email 1</th>
											  <th scope="col">Gói Email 2</th>
											  <th scope="col">Gói Email 3</th>
											</tr>
										  </thead>
										  <tbody>
											<tr>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
											 </tr>
											<tr>
											  <td>Giá tiền : 300.000 vnđ</td>
											  <td>Giá tiền : 806.000 vnđ</td>
											  <td>Giá tiền : 2.400.000 vnđ</td>
											</tr>
											<tr>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											</tr>
											<tr>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											</tr>
										  </tbody>
										</table>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-md-12 mgt10">
							<div class="box box-widget widget-user-2">
			
								<div class="widget-user-header bg-yellow">
									<div class="widget-user-image">
										<img width="50" src="/vn3c/img/EmailTesting.png" />
									</div>
				
									<h3 class="center"><b>GÓI DOANH NGHIỆP LỚN</b></h3>
									<h4 class="center"><b>20.000 Email trong 1 tháng </b></h4>
								</div>
								<div class="box-footer bg-footer-yellow no-padding">
									<table class="table" style="overflow-x: scroll;">
										  <thead class="bg-vip1" style="color:#fff">
											<tr>
											  <th scope="col">Gói Email 1</th>
											  <th scope="col">Gói Email 2</th>
											  <th scope="col">Gói Email 3</th>
											</tr>
										  </thead>
										  <tbody>
											<tr>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">3 tháng</span></td>
											  <td>Thời gian sử dụng : <span class="color-red textUpper">1 năm</span></td>
											 </tr>
											<tr>
											  <td>Giá tiền : 800.000 vnđ</td>
											  <td>Giá tiền : 2.000.000 vnđ</td>
											  <td>Giá tiền : 6.000.000 vnđ</td>
											</tr>
											<tr>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											  <td>Đặt lịch gửi mail chăm sóc khách hàng</td>
											</tr>
											<tr>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											  <td>Gửi mail tới khách hàng</td>
											</tr>
										  </tbody>
										</table>
								</div>
							</div>
						</div>
					</div>
				</section>
					<style>
							.table thead th{
								text-align:center;
							}
							.table thead th,td{
								    border-left: 1px solid #fff
							}
							.bg-yellow{
								background: #ffc107;
							}
							.bg-footer-yellow{
								background:#ffc10714;
							}
							.bg-blue{
								background: #03a9f4;
							}
							.bg-footer-blue{
								background:#03a9f417;
							}
							.bg-vip1{
								background: #fcaf42;
							}
							.widget-user-header{
								text-align:center;
								padding-bottom: 10px;
							}
							h3.center{
								padding: 10px;	
							}
							.vip1{
								background: #f5cb4dc2;
								color: #FFF;
							}
							.box-footer1{
								background:#f2eddfc2;
							}
							.vip2{
								background: #429bcf;
								color: #FFF;
							}
							.bg-vip2{
								background:#2189c7;
							}
							.box-footer2{
								background:#deedf7;
							}
							
							.vip3{
								background: #70cd73;
								color: #FFF;
							}
							.bg-vip3{
								background:#4caf50;
							}
							.box-footer3{
								background:#cfdecb7a;
							}
							
							.vip{
								background: #dedddd;
								color: #333 !important;
							}
							.bg-vip{
								background : #a0a0a0 ;
							}
							.box-footer0{
								background:#d1d0d045;
							}
							
							.box-footer ul li {
								padding: 5px;
								width: 100%;
								border-bottom: 1px solid #3333331a;
							}
							.box-footer ul li a {
									color:#000;
							}
							.color-blue{
								color:#0086ff;
							}
							.color-red{
								color:red;
							}
							.center{
								color:white;
							}
							.no-padding{
								padding: 0;
							}
							input:hover{
								text-indent: 15px;
								border :1px solid #11d6f0;
								border-bottom: 1px solid #11d6f0;
							}
						</style> -->
                <!--div class="col-md-9 col-sm-12 price-tb ">
                    <h6>Đăng ký ngay hôm nay: </h6>
                    <div id="getfly-optin-form-iframe-1527739324245"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=tWOH8rvOsLTLFmL7CYBFsg7kGefFXyLHuLXSKy9aJM1Y9mMdha&referrer="+r); f.style.width = "100%";f.style.height = "400px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1527739324245");s.appendChild(f); })(); </script>
                </div-->
				<section class="content-contact content-pages">
					<div class="container">
						<div class="section-title">
							<div class="main-title">
								<div class="title-main-page">
									
									<p style="font-size:24px">Để lại thông tin để nhận tư vấn  </p>
								</div>
							</div>
						</div>
					</div>
				</section>
				
				<div class="col-md-8 col-sm-8 col-xs-8" style="margin:auto">
							<form onSubmit="return contact(this);" class="wpcf7-form" method="post" action="{{route('sub_contact')}} ">
								{!! csrf_field() !!}
								<input type="hidden" name="is_json"
									class="form-control captcha" value="1" placeholder="">
								<div class="form-group">
								  <div>
									<input type="text" class="form-control"  placeholder="Tên của bạn"name="name" required>
								  </div>
								</div>
								<div class="form-group">
								  <div>
									<input type="email" class="form-control" placeholder="Email của bạn" name="email" required>
								  </div>
								</div>
								<div class="form-group">
								  <div > 
									<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required>
								  </div>
								</div>
								<div class="form-group">
								  <div > 
									<textarea class="form-control" rows="2" name="message"placeholder="Nội dung ghi chú"></textarea>
								  </div>
								</div>
									 <input type="hidden" value="" name="" id="utm_source"/>
									 <input type="hidden" value="" name="" id="utm_medium"/>
									 <input type="hidden" value="" name="" id="utm_campaign"/>
								<div class="form-group">
								  <div>
									<button type="submit" class="btn  btn-warning">ĐĂNG KÝ TƯ VẤN</button>
								  </div>
								</div>
							 </form>	
						</div>
					</div>				
				</div>
            </div>
    </section>

@endsection

