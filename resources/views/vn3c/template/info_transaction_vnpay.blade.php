@extends('vn3c.layout.site')

@section('title',' Làm website tại Moma chỉ với giá')
@section('meta_description', 'Làm website tại Moma, xem chi tiết bảng giá báo giá website tại vn3c chuyên nghiệp nhất')
@section('keywords', '')

@section('content')
    <section class="content-contact content-pages">
        <div class="container">
            <div class="section-title">
                <div class="main-title">
                    <div class="title-main-page">
                        <h1>{{ $status }}</h1>
                        @if ((string)$status === 'THANH TOÁN THÀNH CÔNG')
                            <p>Hoàn tất thanh toán {{ $type }} thành công gói {{ $package }}</p>
                        @else
                            <p>Thanh toán giao dịch thất bại. Mời bạn vui lòng giao dịch lại. Xin cảm ơn !</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-price">
        <div class="container">
            <section class="content">
                <div class="row">
                    <div class="col-xs-12 col-md-12 mgt10 no-padding">
                        <div class="box box-widget widget-user-2" style="overflow-x: scroll;">
                            <div class="box-body">
							<a href="{{$domain}}/admin/home"><button type="button" class="btn btn-info bkg whiteText">Quay lại trang quản trị của bạn</button></a>
                            @if(isset($message))
                                    <p style="text-align: center">{{ $message }}</p>
                            @else
                                <table class="table table-striped text-center">
                                    <thead>
                                    <tr>
                                        <th> Tên Website </th>
                                        <th> Dịch vụ </th>
                                        <th> Thời gian sử dụng </th>
                                        <th> Tổng số tiền </th>
                                        <th> Mã giao dịch của VNPay </th>
                                        <th> Kết quả </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td> {{ $domain }} </td>
                                        @if ((int)$month === 100)
                                            <td> Đấu giá mã giảm giá moma </td>
                                        @elseif ((string)$status === 'THANH TOÁN THÀNH CÔNG')
                                            <td> {{ $type }} thành công gói {{ $package }} {{ $month }} tháng </td>
                                        @else
                                            <td> {{ $type }} thất bại </td>
                                        @endif
                                        <td>
                                            @if((int)$month === 100)
                                                không thời hạn
                                            @elseif((int)$month === 24)
                                                36 tháng
                                            @elseif((int)$month === 36)
                                                60 tháng
                                            @else
                                                {{ $month }} tháng
                                            @endif
                                        </td>
                                        <td> {{ number_format($money, 0, ',', '.') }} VNĐ </td>
                                        <td> {{ $transactionId }} </td>
                                        <td> {{ $status }} </td>
                                    </tr>
                                    </tbody>
                                </table>
                           @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <style>
        .table thead th{
            text-align:center;
        }
        .table thead th,td{
            border-left: 1px solid #fff
        }

        .box-footer ul li {
            padding: 5px;
            width: 100%;
            border-bottom: 1px solid #3333331a;
        }
        .box-footer ul li a {
            color:#000;
        }
        .no-padding{
            padding: 0;
        }
        .text-center {
            margin: 1% 0;
            text-align: center;
        }
    </style>

@endsection

