@extends('vn3c.layout.create-theme')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('page', ['post_slug' => $post->slug]) )

@section('content')
	<!--div class="content pdTop60 pdb60">
		
		<div class="sub-home-pages pdTop60 pd20">
			<div class="container">
			<div class="row">
				<div class="col-lg-6 theme-image pdt70">
					<div class="titlePriceList textLeft pdb30 sm-pdb10">
						<h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
							Cho mọi người biết bạn đang mở kinh doanh</h2>
					</div>
					<div class="textLeft">
						<p class="">Hiển thị facebook, zalo, số điện thoại và chỉ đường đến doanh nghiệp của bạn trên Google Tìm kiếm và Maps — với tạo website miễn phí chúng tôi</p>
					</div>
								
				</div>
				<div class="col-md-6 right-ft">
					<div class="content">
						<h1 class="f24 mgBottom20">ĐĂNG KÝ WEBSITE MIỄN PHÍ</h1>
						<div class="form-bg">
							<form method="POST" action="{{ route('check-website') }}" id="createWebsite">
								{{ csrf_field() }}
								<div class="row">
									<div class="col-md-2">
										<input disabled placeholder="http://"/>
									</div>
									<div class="col-md-6">
										<input type="text" name="domain" value="{{ old('domain') }}" placeholder="Tên thương hiệu, dịch vụ" required/>
									</div>
								</div>
								  
								<select class="form-control mgBottom20" name="theme_code" onchange="return changeTheme(this);">
									
									@foreach (\App\Entity\Product::getAllProduct() as $theme)
										@if ($theme['co-hien-thi-chon-theme'] == 1)
										<option value="{{ $theme->code }}" {{ (old('theme_code') == $theme->code) ? 'selected' : '' }}
												{{ (empty(old('theme_code')) && isset($_GET['theme']) && $_GET['theme'] == $theme->code) ? 'selected' : '' }}
										>
										{{ $theme->title }}</option>
										@endif
									@endforeach
								</select>
								<script>
									function changeTheme(e) {
										var theme = $(e).val();
										$('.imagePage').hide();
										$('.' + theme).show();
									}
								</script>
								<input type="text" name="name_home" value="{{ old('name_home') }}" placeholder="Họ và tên" required/>
								<input type="email" name="email" value="{{ old('email') }}" placeholder="Email" required/>
								<input type="number" name="phone" value="{{ old('phone') }}" placeholder="Số điện thoại" required />
								<input type="password" name="password" id="inputPassword" placeholder="Mật khẩu" required/>
								<input type="password" name="password_confirmation" id="inputPassword" placeholder="Nhập lại mật khẩu" required/>
								<div class="form-group row">
									<label for="inputPassword" class="col-sm-3 col-form-label"></label>
									<div class="col-sm-9">
										@if ($errors->has('email'))
											<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
										@endif
										@if ($errors->has('name'))
											<span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
										@endif
										@if ($errors->has('phone'))
											<span class="help-block">
										<strong>{{ $errors->first('phone') }}</strong>
									</span>
										@endif
										@if ($errors->has('password'))
											<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
										@endif
										@if ($errors->has('errorDomain'))
											<span class="help-block">
										<strong>{{ $errors->first('errorDomain') }}</strong>
									</span>
										@endif
									</div>
									<input type="hidden" value="{{ isset($_GET['utm_source']) ? $_GET['utm_source'] : '' }}" name="utm_source">
								</div>
								<div class="form-group row">
									<label for="inputPassword" class="col-sm-3 col-form-label"></label>
									<div class="col-sm-12">
										<button type="submit" class="btn btn-submit bgrOrange" onclick="return clickRegister(this);" data-loading-text="Đang trong quá trình tạo website">ĐĂNG KÝ SỬ DỤNG</button>
									</div>
								</div>
								<script>
									function clickRegister(e) {
										var btn = $(e).html("Đang trong quá trình tạo website");
										$(e).attr('style', 'background: #5a6268;');
										$(e).prop('disabled', true);
										
										$('#createWebsite').submit();
									}
								</script>

							</form>
						</div>
					</div>
				</div>
				
			</div>
			</div>
		</div>
    </div-->
<div class="content pdTop60 " id="registerWebsite">
		<!-- Subpages -->
		<div class="sub-home-pages pdTop60 pd20">
			<div class="container">
			<div class="row">
				<div class="col-lg-6 theme-image">
					<div class="titlePriceList textLeft pdb30 sm-pdb10">
						<h2 class="fw7 blueMoma f28 md-f23 sm-f18 col-f15 mgb0">
							VÌ SAO BẠN NÊN CHỌN WEBSITE MOMA?</h2>
					</div>
					<div class="textLeft">
					<!--	<p>1. Website bán hàng tự động hóa mang đến giải pháp quản lý và chăm sóc khách hàng toàn diện.</p>
						<p>2. Hỗ trợ quản lý quy trình sale - chăm sóc khách hàng trên một nền tảng.</p>
						<p>3. Ứng dụng thống nhất quy trình, tiết kiệm thời gian, tối ưu nguồn lực, đột phá doanh thu. </p>
						<p>4. Bảo mật thông tin khách hàng tuyệt đối.</p>
						<p>5. Liên tục cập nhật tính năng và công nghệ mới nhất.</p>
						<p>6. Đội ngũ tư vấn giải pháp năng lực, chuyên nghiệp.</p>
						<p>7. Kinh nghiệm triển khai 2000+ doanh nghiệp với 150+ ngành nghề.</p>
						<p>8. Tiết kiệm chi phí, thời gian, đột phá doanh thu.</p>
						<p>9. Hệ thống quản trị website dễ dàng sử dụng.</p>
						<p>10. Hiển thị tốt trên màn hình điện thoại và máy tính bảng.</p>
						<p>11. Tương thích tốt trên tất cả các ngành nghề hiện có.</p>    -->
						<p class=""><img src="../assets/vn3ctheme/img/check.png" alt="check" width= "20" height="20" /> Bạn có thể kinh doanh tại nhà, không cần kiến thức phức tạp, chỉ cần 5 phút là biết làm ngay. Được làm thương hiệu riêng để phát triển lâu dài. Thu nhập ổn định hàng tháng, hàng năm, mọi thứ đều chạy online không cần cài đặt. Và sẽ được chúng tôi hướng dẫn tìm kiếm khách hàng.</p>
						<p class=""><img src="../assets/vn3ctheme/img/check.png" alt="check" width= "20" height="20" /> Bạn sẽ sở hữu ngay một website để bán tất cả các ngành nghề như sau: Tuyển dụng , nhân sự, Đào tạo ,Thiết kế, Thi công văn phòng, Kế toán, Pháp lý, Pháp chế doanh nghiệp, Dịch vụ IT, Dịch vụ vệ sinh, Các dịch vụ tư vấn, Marketing, Logitics, Điện hoa, In ấn, Tổ chức sự kiện, Dịch thuật, Dịch vụ cho thuê PG, Team Building, Dịch vụ khám sức khoẻ, Dịch vụ bảo hiểm, Vé máy bay, Dịch vụ phòng khách sạn, Cho thuê văn phòng, Tổ chức tour, Dịch vụ cấp giấy phép - visa hộ chiếu, Cước viễn thông - internet, Thiết bị văn phòng, Thiết bị công nghệ, Dịch vụ ăn uống - Giải trí, Các loại phần mềm, Đồ gia dụng, Dịch chăm sóc sắc đẹp...</P>   
						<p class=""><img src="../assets/vn3ctheme/img/check.png" alt="check" width= "20" height="20" /> Ưu điểm của tạo website miễn phí chỉ mất 5 phút là bạn có một website để có thể đăng lên sản phẩm, dịch vụ hoặc tin tức </p>
					</div>
			<!--	 	<div class="textCenter white mgb30 home-buttons mgt30">
						<a href="#" class="bt-submit md-f18 btGreen sm-f14Im col-f9 uppercase">Đăng ký ngay</a>
					</div>     -->
				</div>
				<div class="col-md-6 right-ft">
					<div class="content">
						<h1 class="f24 mgBottom20">ĐĂNG KÝ WEBSITE MIỄN PHÍ</h1>
						<div class="form-bg">
							<script src="https://www.google.com/recaptcha/api.js" async defer></script>
							<div class="thrv_wrapper thrv_lead_generation" data-connection="api" data-css="tve-u-1616256124e">
								<div class="thrv_lead_generation_container tve_clearfix ">
									<form method="POST" action="{{ route('check-website') }}" id="createWebsite">
										{{ csrf_field() }}
										<div class="row">
											<div class="col-md-2">
												<input disabled placeholder="http://"/>
											</div>
											<div class="col-md-10">
												<input type="text" name="domain" value="{{ old('domain') }}" placeholder="Tên thương hiệu, dịch vụ" required/>
											</div>
										</div>
										<input type="hidden" name="theme_code" value="xhome" />
									
										<script>
											function changeTheme(e) {
												var theme = $(e).val();
												$('.imagePage').hide();
												$('.' + theme).show();
											}
										</script>
										<input type="text" name="name_home" value="{{ old('name_home') }}" placeholder="Họ và tên" required/>
										<input type="email" name="email" value="{{ old('email') }}" placeholder="Email" required/>
										<input type="number" name="phone" value="{{ old('phone') }}" placeholder="Số điện thoại" required />
										<input type="password" name="password" id="inputPassword" placeholder="Mật khẩu" required/>
										<input type="password" name="password_confirmation" id="inputPassword" placeholder="Nhập lại mật khẩu" required/>
										<div class="g-recaptcha" data-sitekey="6Let294UAAAAAGrUDEUsEUn-tw24S7E50Dqe4jcj"></div>
										<br/>
										<div class="form-group row">
											<label for="inputPassword" class="col-sm-3 col-form-label"></label>
											<div class="col-sm-9">
												@if ($errors->has('email'))
													<span class="help-block">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
												@endif
												@if ($errors->has('name'))
													<span class="help-block">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
												@endif
												@if ($errors->has('phone'))
													<span class="help-block">
												<strong>{{ $errors->first('phone') }}</strong>
											</span>
												@endif
												@if ($errors->has('password'))
													<span class="help-block">
												<strong>{{ $errors->first('password') }}</strong>
											</span>
												@endif
												@if ($errors->has('errorDomain'))
													<span class="help-block">
												<strong>{{ $errors->first('errorDomain') }}</strong>
											</span>
												@endif
											</div>
											<input type="hidden" value="{{ isset($_GET['utm_source']) ? $_GET['utm_source'] : '' }}" name="utm_source">
										</div>
										<div class="form-group row">
											<label for="inputPassword" class="col-sm-3 col-form-label"></label>
											<div class="col-sm-12">
												<button type="submit" class="btn btn-submit bgrOrange" onclick="return clickRegister(this);" data-loading-text="Đang trong quá trình tạo website">ĐĂNG KÝ SỬ DỤNG</button>
											</div>
										</div>
										<script>
											function clickRegister(e) {
												var btn = $(e).html("Đang trong quá trình tạo website");
												$(e).attr('style', 'background: #5a6268;');
												$(e).prop('disabled', true);
												
												$('#createWebsite').submit();
											}
										</script>
									</form>                                                     
								</div>
								
							</div>
						</div>
					</div>
				
				</div>
				
			</div>
			</div>
		</div>
    </div>
<section class="productPriceList pdt3p container">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
			Tạo website bán hàng với Moma</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10">
                <i class="fa fa-handshake-o orange f20" aria-hidden="true"></i>
            </div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
    <div class="contentProductPriceList">
        <div class="row">
            <div class="col-md-12 col-12 sm-pdb20">
                <iframe width="100%" height="500" src="https://www.youtube.com/embed/gT6Fz7qf9Jk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@endsection
