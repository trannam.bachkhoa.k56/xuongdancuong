@extends('vn3c.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )
@section('type_meta', 'website')

@section('content')
 <!-- Main Content Pages -->
	<div class="content-pages pdTop60">
		<!-- Subpages -->
		<div class="sub-home-pages">
			<!-- Contact Subpage -->
			<section id="contact" class="sub-page">
				<div class="container">
					<div class="section-title">
						<div class="main-title">
							<div class="title-main-page">
								<h4>Liên hệ</h4>
								<p>Công ty Cổ phần Tri thức Vì Dân</p>
							</div>
						</div>
					</div>
					<!-- Contact Form -->
					<div class="row contact-form pb-30">
						<div class="col-sm-12 col-md-5 col-lg-5 left-background justify">
							{!! $post->content !!}
						</div>
						<div class="col-sm-12 col-md-7 col-lg-7">
							<div id="getfly-optin-form-iframe-1520616863207"></div> 
							<script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=peDZ9p2OrU9fucv7HrqZaCVlaqnZLEPquXnETngxh5xJsWJM8b&referrer="+r); f.style.width = "100%";f.style.height = "450px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1520616863207");s.appendChild(f); })(); </script>
						</div>
					</div>
					<!-- /Contact Form -->

					<!-- Contact Details -->
					<div class="pb-30">
						<div class="section-head">
							<h4>
								<span>Thông tin</span>
								Liên hệ
							</h4>
						</div>

						<!-- Contact Info -->
						<div class="sidebar-textbox row pb-50">
							<div class="contact-info d-flex col-md-4">
								<div class="w-25">
									<div class="contact-icon">
										<i class="fas fa-phone"></i>
									</div>
								</div>
								<div class="contact-text w-75">
									<h2>Điện thoại</h2>
									<p>093 455 3435</p>
									<p>097 456 1735</p>
								</div>
							</div>
							<div class="contact-info d-flex col-md-4">
								<div class="w-25">
									<div class="contact-icon">
										<i class="far fa-envelope-open"></i>
									</div>
								</div>
								<div class="contact-text w-75">
									<h2>Email</h2>
									<p>info@vn3c.com</p>
									<p>miaki0512@gmail.com</p>
								</div>
							</div>
							<div class="contact-info d-flex col-md-4">
								<div class="w-25">
									<div class="contact-icon">
										<i class="fas fa-map-marker-alt"></i>
									</div>
								</div>
								<div class="contact-text w-75">
									<h2>Địa chỉ</h2>
									<p>Tầng 3, Số nhà 9, Ngõ 1, Lê Văn Thiêm, Hà Nội</p>
								</div>
							</div>
						</div>
						<!-- /Contact info -->
					</div>
					<!-- /Contact Details -->  
				</div>
			</section>
			<!-- End Contact Subpage -->

		</div>
		<!-- /Page changer wrapper -->
	</div>
	<!-- /Main Content -->
@endsection