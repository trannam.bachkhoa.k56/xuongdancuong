@extends('vn3c.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )
@section('type_meta', 'website')

@section('content')
<section class="bannerLarge 70vh mgt80">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-12 mg" >
                <div class="inBanner pd9p">
                    <div class="titleLarge textLeft textUpper">
                        <h1 class="fw7 f22 md-f28 sm-f20 col-f16 top25 mgb20">
                            Cho mọi người biết bạn mở cửa kinh doanh.
                        </h1>
                    </div>
                    <div class="titleSmall textLeft">
                        <h5 class=" md-f14 sm-f12 col-f11 fw4 f18">Tạo cho mình 1 website bán hàng hiển thị giờ làm việc, số điện thoại và chỉ đường đến doanh nghiệp của bạn - với website của chúng tôi.</h5>
                    </div>
                    <div class="home-buttons textLeft">
                        <a href="/trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="bt-submit md-f18 sm-f14Im col-f9"><i class="lnr lnr-briefcase"></i>
                            Tạo website bán hàng miễn phí
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-12">
                <img src="{{ asset('business/image/benefits-control_1x.png') }}" style="width:100%" />
            </div>
        </div>
    </div>



</section>

<section class="productPriceList pd3p container">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
			VÌ SAO PHẢI CHỌN WEBSITE BÁN HÀNG MOMA</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10">
                <i class="fa fa-handshake-o orange f20" aria-hidden="true"></i>
            </div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
    <div class="contentProductPriceList">
        <div class="row">
			<div class="col-md-6 col-12 sm-pdb20">
				<p>1. Website bán hàng tự động hóa mang đến giải pháp quản lý và chăm sóc khách hàng toàn diện.</p>
				<p>2. Hỗ trợ quản lý quy trình sale - chăm sóc khách hàng trên một nền tảng.</p>
				<p>3. Ứng dụng thống nhất quy trình, tiết kiệm thời gian, tối ưu nguồn lực, đột phá doanh thu. </p>
				<p>4. Bảo mật thông tin khách hàng tuyệt đối.</p>
				<p>5. Liên tục cập nhật tính năng và công nghệ mới nhất.</p>
				<p>6. Đội ngũ tư vấn giải pháp năng lực, chuyên nghiệp.</p>
				<p>7. Kinh nghiệm triển khai 2000+ doanh nghiệp với 150+ ngành nghề.</p>
				<p>8. Tiết kiệm chi phí, thời gian, đột phá doanh thu.</p>
				
			</div>
            <div class="col-md-6 col-12 sm-pdb20">
                <iframe width="100%" height="300" width="300" src="https://www.youtube.com/embed/9VrCOzjWyMA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="productPriceList pd3p container">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
			HƯỚNG DẪN CÁCH TẠO WEBSITE BÁN HÀNG
		</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10"><i class="fa fa-handshake-o orange f20" aria-hidden="true"></i></div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
    <div class="contentProductPriceList">
        <div class="row">
            <div class="col-md-6 col-12 sm-pdb20">
                <iframe width="100%" height="300" src="https://www.youtube.com/embed/wFioYfJeNm8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
			<div class="col-md-6 col-12 sm-pdb20">
				<p>1. Ấn vào button tạo website bán hàng miễn phí.</p>
				<p>2. Điền đầy đủ thông tin. ấn tạo mới website.</p>
				<p>3. Vào email kích hoạt website bán hàng.</p>
				<p>4. Vào email lấy thông tin quản trị website và tiến hành truy cập vào quản lý website và xem website của mình.</p>
				<div class="home-buttons textLeft">
					<a href="/trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="bt-submit md-f18 sm-f14Im col-f9"><i class="lnr lnr-briefcase"></i>
						Tạo website bán hàng miễn phí
					</a>
				</div>
			</div>
        </div>
    </div>
</section>
@endsection
