@extends('vn3c.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('keywords', '')

@section('content')
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<div class="content-pages" id="login" >
	<!-- Subpages -->
<div class="sub-home-pages">
	<!-- Titlebar -->
<div id="titlebar">
<div class="container">
<div class="row">
<aside class="ask_left col-md-3 col-xs-12 ">
    <!-- === col_topic -->
    <div class="col_topic">
        <span class="title textUpper" style="font-size:24px;color:#000">Các chủ đề khác</span>
        <ul class="highlight_topic  list-group-flush">
		@foreach(App\Entity\Post::getPostWithTemplate('hoi-dap') as $postNew)
            <li class="list-group-item">
             <div class="subtopic hide">
				<a href="{{route('post',['cate_slug'=>'tin-tuc-moi','post_slug' => $postNew->slug ])}}" style="text-transform: capitalize;">
				{{$postNew->title}}
				</a>
            </div>

            </li>
		@endforeach
        </ul>
       

	</div>
    <!-- col_content-->
</aside>

<aside class="col_content  col-md-7  col-xs-12 ">
	<!--form class="search_ask" action="" onsubmit="return submitSearchFormFAQ()">
		<input type="text" id="txtKeysearch" name="word"" autocomplete="off" placeholder="Tìm hướng dẫn, mẹo hay, câu hỏi, sản phẩm">
		<button type="submit" class="btn btn-primary">Tìm kiếm</button>
	</form-->

	<article data-type="3">
		<h1 class="title" style="font-size:22px;margin-top:20px;text-transform: uppercase;">
		{{$post->title}}
		</h1>
		<div class="content">
		{!! $post->content !!}
		</div>
		<hr>
		
		@if(empty($socialAccount))
		<div class="question ">
			<p>Bạn cần <a style="color:blue" href="{{ route('facebook_redirect', ['social' => 'facebook']) }}" >Đăng nhập</a> để xem được các câu hỏi khác:</p>
			<a href="#login" class="plIq " onclick="showInputQuestion();">
				Nhập nội dung câu hỏi của bạn			
				<div id="txtContentQS " class="hide">
				<textarea rows="4" disabled  cols="50" style="border: 1px solid #eeee" ></textarea>
				</div>
			</a>
			<div id="lberror " class="hide "></div>
			<div class="barsend ">
			<a id="spanImg " href="javascript:; "><i class="iconask-pic "></i>Gửi ảnh</a>
			<button type="submit " onclick="sendQuestionCustomer() ">Gửi</button>
			<input id="upNotTitle " type="file " multiple="multiple " hidden="hidden " accept="image/x-png, image/gif, image/jpeg " />
			</div>
		</div>
		@else
		<div class="comment">
			<div class="fb-comments" data-href="{{$actual_link}}" data-width="" data-numposts="5"></div>
		</div>
		
		@endif
		
		
	</article>
</aside>

<!-- right-->
<aside class="ask_right col-md-2  col-xs-12" >
	<div class="area_input ">
	
		<div class="loginFB">
			<p>Đăng nhập</p>
			
			<div style="position:relative">
				<a class="facebook block" style="color:#fff;padding: 5px 10px;text-align: center;
				background: #006eaf;" href="{{ route('facebook_redirect', ['social' => 'facebook']) }}" ><i class="fab fa-facebook"></i> Facebook </a>
			   <p>Bạn cần đăng nhập để bình luận</p>
			</div>
		</div>
	
		@if(empty($socialAccount))
		<div class="question1 ">
			<a class="plIq " onclick="showInputQuestion();">
			Nhập nội dung câu hỏi của bạn
			</a>
			<div id="txtContentQS " class="hide">
			<textarea rows="4" onclick="showInputQuestion();" cols="50" style="border: 1px solid #eeee" ></textarea>
			</div>
			<div id="lberror " class="hide "></div>
			<div class="barsend ">
			<a id="spanImg " href="javascript:; "><i class="iconask-pic "></i>Gửi ảnh</a>
			<button type="submit " onclick="sendQuestionCustomer() ">Gửi</button>
			<input id="upNotTitle " type="file " multiple="multiple " hidden="hidden " accept="image/x-png, image/gif, image/jpeg " />
			</div>
		</div>
		@else
			<div class="question ">
				<div class="row">
					<div class="col-md-4" style="padding:0 10px">
					 <img class="card-img-top" style="border-radius:50%" src="{{ $socialAccount->avatar }}" alt="Card image cap">
					</div>
				 
					  <div class="col-md-8" style="padding:10px 10px">
						<a style="color:blue" class="card-title">{{ $socialAccount->name }}</a>
					  </div>
				</div>
			</div>
		@endif
		
		<ul class="question">
		    @foreach(\App\Entity\SubPost::showSubPost('ads', 5) as $id => $ads)
			<li style="margin-top:5px;">
				<a href="{{$ads->description}}" title="{{$ads->title}}">
					<img src="{{$ads->image}}" style="with:100%;" />
				</a>			
			</li>
			@endforeach
		</ul>
		
		
		
	</div>

</aside>
 
</div>
</div>
</div>
<style>
a {
	color:#000;
}
li.list-group-item{
	padding:10px 0;
}
.loginFB{
	display:block;
	position:absolute;
	top:-500px;
	height:0;
}
</style>
<script>
	function showInputQuestion(){
		$('.loginFB').css('top',0);
		$('.loginFB').css('position','relative');
		$('.loginFB').css('height',100);
		
		$('.question1').hide();
	}
</script>
@endsection