@extends('vn3c.layout.create-theme')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('page', ['post_slug' => $post->slug]) )

@section('content')
    <div class="content-pages pdt60 pdTop60">
        <!-- Subpages -->
        <div class="sub-home-pages pdt60 pd20">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 right-ft">
                        <div class="content">
                            <h1 class="f24 mgBottom20">Kích hoạt tài khoản</h1>
                            <div class="form-bg">
                                <div class="content">
                                    <div class="form-bg">
                                        <h3>Cảm ơn bạn đã đăng ký website miễn phí của chúng tôi.</h3>
                                        <p class="pSuccess">
                                            Để có thể sử dụng được website, bạn vui lòng kích hoạt tài khoản bằng <b>Email</b> bạn vừa đăng ký.
                                        </p>
                                        <p class="pSuccess">
                                            Chúng tôi chân thành cảm ơn!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .pSuccess {
            font-size: 18px;
        }
    </style>
@endsection


