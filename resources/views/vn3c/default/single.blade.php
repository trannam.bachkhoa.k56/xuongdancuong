@extends('vn3c.layout.layout_xhome')

@section('title', isset($post->title) ? $post->title : '')
@section('meta_description', !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', !empty($post->meta_keyword) ? $post->meta_keyword : '') 
@section('meta_url', $domainUrl.'/tin-tuc/'.$post->slug) 
@section('meta_image', !empty($post->image) ? asset($post->image) : '')

@push('scriptHeader')
	<script async defer type="application/ld+json">
		{
		  "@context": "https://schema.org",
		  "@type": "NewsArticle",
		  "mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "{!! $domainUrl !!}/danh-muc/tin-tuc/{!! $post->slug !!}"
		  },
		  "headline": "{!! isset($post->title) ? $post->title : $domainUrl !!}",
		  "image": [
			"{!! !empty($post->image) ? asset($post->image) : '' !!}"
		   ],
		  "datePublished": "{!! $post->created_at !!}",
		  "dateModified": "{!! $post->updated_at !!}",
		  "author": {
			"@type": "Person",
			"name": "{!! $domainUrl !!}"
		  },
		   "publisher": {
			"@type": "Organization",
			"name": "{!! $domainUrl !!}",
			"logo": {
			  "@type": "ImageObject",
			  "url": "{!! $domainUrl !!}/{{ isset($information['logo']) ?  $information['logo'] : '' }}"
			}
		  },
		  "description": "{!! !empty($post->meta_description) ? $post->meta_description : $post->description !!}"
		}
    </script>
@endpush
@section('content')
<section class="moreProduct bgrGray pdb20">
        <div class="container">
            <div class="link">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="/danh-muc/{{ isset($category->slug) ? $category->slug : 'Tin tức' }}">{{  isset($category->title) ? $category->title : 'Tin tức' }}</a></li>
                <li class="breadcrumb-item"><a href="#">{{$post->title}}</a></li>
              </ol>
            </div>
			
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<div class="News bgrWhite pd15">
						<div class="title">
							<h1>{{$post->title}}</h1>
							<div id="loadShare">
							</div>
							<script>
								$(window).load(function() { 
									$("#loadShare").html('<iframe src="https://www.facebook.com/plugins/share_button.php?href={!! $domainUrl.'/tin-tuc/'.$post->slug !!}&layout=button_count&size=large&appId=1875296572729968&width=138&height=28" width="138" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>');
								});
							 </script>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-12">
								
								<div class="contentNews" id="parentScroller">
									
									{!!$post->content!!}
									
									@if(empty($post['optinform-dang-ky']))
									<div class="form_moma col-md-6 col-xs-12">

										<?php $form =  App\Entity\OptinForm::showWithId($post->form_id);
										?>

										@if (!empty($post->form_id))
											<form class="wpcf7-form" onsubmit="return contact(this);" method="post" action="/submit/contact" >
												{!! csrf_field() !!}
												<input type="hidden" name="is_json"
													   class="form-control captcha" value="1" placeholder="">
											
												@if($form->showName == 1)
													<div class="form-group">
														<div>
															<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
														</div>
													</div>
												@endif

												@if($form->showEmail == 1)
													<div class="form-group">
														<div>
															<input type="email" class="form-control" placeholder="Email của bạn" name="email">
														</div>
													</div>
												@endif

												@if($form->showPhone == 1)
													<div class="form-group">
														<div>
															<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
														</div>
													</div>
												@endif

												@if($form->showAddress == 1)
													<div class="form-group">
														<div>
															<input type="text" class="form-control" placeholder="Địa chỉ" name="address" required="">
														</div>
													</div>
												@endif

												@if($form->showDescription == 1)
													<div class="form-group">
														<div>
											<textarea class="form-control" rows="5" name="message"
													  placeholder="Nội dung ghi chú"></textarea>
														</div>
													</div>
												@endif
												<input type="hidden" value="{{$form->campaign_id}}" name="campaign_id">

												<input type="hidden" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}/tin-tuc/{{ $post->slug }}" name="utm_source" />

												<input type="hidden" value="{{isset($form->formRedirect) ? $form->formRedirect : ''}}" name="formRedirect" />

												@if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
													<div class="form-group">
														<div class="">
															<button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
														</div>
													</div>
												@endif
											</form>
										@else
											<form class="wpcf7-form" onsubmit="return contact(this);" method="post" action="/submit/contact" >
												{!! csrf_field() !!}
												<input type="hidden" name="is_json"
													   class="form-control captcha" value="1" placeholder="">
												<input type="hidden" name="post_id" value="{{$post->post_id}}">
												<div class="form-group">
													<div>
														<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
													</div>
												</div>
												<div class="form-group">
													<div>
														<input type="email" class="form-control" placeholder="Email của bạn" name="email">
													</div>
												</div>
												<div class="form-group">
													<div>
														<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
													</div>
												</div>
												<div class="form-group">
													<div>
														<input type="text" class="form-control" placeholder="Địa chỉ" name="address" required="">
													</div>
												</div>
												<div class="form-group">
													<div>
											<textarea class="form-control" rows="5" name="message"
													  placeholder="Nội dung ghi chú"></textarea>
													</div>
												</div>

												<input type="hidden" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}/tin-tuc/{{ $post->slug }}" name="utm_source" />

												<input type="hidden" value="{{isset($form->formRedirect) ? $form->formRedirect : ''}}" name="formRedirect" />
												<div class="form-group">
													<div class="">
														<button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
													</div>
												</div>
											</form>

										@endif

									</div>

									<div>
									@else
										{!! isset($post['optinform-dang-ky']) ? $post['optinform-dang-ky'] : '' !!}
									@endif
									
									</div>
								</div>
							</div>
						</div>
						
						<p class="mgb0 f24 fw6">Bình luận</p>
						<div id="fb-root"></div>
						<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=1875296572729968&autoLogAppEvents=1"></script>
						<div class="fb-comments" data-href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" data-width="100%" data-numposts="5"></div>

					</div>
				</div>

				<div class="col-xs-12 col-md-4">
					<div class="News bgrWhite pd15 " id="scroller" >
						<div class="title">
							<h2 style="font-size: 22px;">CÓ THỂ BẠN QUAN TÂM</h2>
						</div>

						<div  class="contentNews" id="groupFacebookEmbbed">
							
						</div>
						<script>
							$(window).load(function() { 
								$("#groupFacebookEmbbed").html('<table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;"><tr style=""><td height="28" style="line-height:28px;">&nbsp;</td></tr><tr><td style=""><table border="0" width="280" cellspacing="0" cellpadding="0" style="border-collapse:separate;background-color:#ffffff;border:1px solid #dddfe2;border-radius:3px;font-family:Helvetica, Arial, sans-serif;margin:0px auto;"><tr style="padding-bottom: 8px;"><td style=""><img class="img" src="https://scontent.fhan5-3.fna.fbcdn.net/v/t1.0-9/c0.0.306.315a/101172825_2787438318155338_4643496586331029504_n.jpg?_nc_cat=106&amp;_nc_sid=ca434c&amp;_nc_ohc=0K2qOB0TwikAX9En6r1&amp;_nc_ht=scontent.fhan5-3.fna&amp;oh=83fa6d43b476661b075f59c59e5a2408&amp;oe=5EFF3AFC" width="280" height="146" alt="" /></td></tr><tr><td style="font-size:14px;font-weight:bold;padding:8px 8px 0px 8px;text-align:center;">Nhóm Kết Công Nghệ</td></tr><tr><td style="color:#90949c;font-size:12px;font-weight:normal;text-align:center;">Thành viên · 168,868 thành viên</td></tr><tr><td style="padding:8px 12px 12px 12px;"><table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;width:100%;"><tr><td style="background-color:#4267b2;border-radius:3px;text-align:center;"><a style="color:#3b5998;text-decoration:none;cursor:pointer;width:100%;" href="https://www.facebook.com/plugins/group/join/popup/?group_id=513213432665245&amp;source=email_campaign_plugin" target="_blank" rel="noopener"><table border="0" cellspacing="0" cellpadding="3" align="center" style="border-collapse:collapse;"><tr><td style="border-bottom:3px solid #4267b2;border-top:3px solid #4267b2;color:#FFF;font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;">Tham gia nhóm</td></tr></table></a></td></tr></table></td></tr><tr><td style="border-top:1px solid #dddfe2;font-size:12px;padding:8px 12px;">Hỏi tất cả những gì về kinh doanh và marketing, Đầu tư</td></tr></table></td></tr><tr style=""><td height="28" style="line-height:28px;">&nbsp;</td></tr></table>
				
				<table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;"><tr style=""><td height="28" style="line-height:28px;">&nbsp;</td></tr><tr><td style=""><table border="0" width="280" cellspacing="0" cellpadding="0" style="border-collapse:separate;background-color:#ffffff;border:1px solid #dddfe2;border-radius:3px;font-family:Helvetica, Arial, sans-serif;margin:0px auto;"><tr style="padding-bottom: 8px;"><td style=""><img class="img" src="https://scontent.fhan3-3.fna.fbcdn.net/v/t1.0-0/c0.0.957.987a/s526x296/78593932_137989977621508_7436280469051670528_o.jpg?_nc_cat=106&amp;_nc_sid=ca434c&amp;_nc_ohc=DuGFT7U3Cx0AX_T1qm3&amp;_nc_ht=scontent.fhan3-3.fna&amp;oh=043aa4c3a4767679aad834cc652926a1&amp;oe=5EB20119" width="280" height="146" alt="" /></td></tr><tr><td style="font-size:14px;font-weight:bold;padding:8px 8px 0px 8px;text-align:center;">Việc Làm Tiva.vn</td></tr><tr><td style="color:#90949c;font-size:12px;font-weight:normal;text-align:center;">Thành viên · 168,123 thành viên</td></tr><tr><td style="padding:8px 12px 12px 12px;"><table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;width:100%;"><tr><td style="background-color:#4267b2;border-radius:3px;text-align:center;"><a style="color:#3b5998;text-decoration:none;cursor:pointer;width:100%;" href="https://www.facebook.com/plugins/group/join/popup/?group_id=2383165595234367&amp;source=email_campaign_plugin" target="_blank" rel="noopener"><table border="0" cellspacing="0" cellpadding="3" align="center" style="border-collapse:collapse;"><tr><td style="border-bottom:3px solid #4267b2;border-top:3px solid #4267b2;color:#FFF;font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;">Tham gia nhóm</td></tr></table></a></td></tr></table></td></tr><tr><td style="border-top:1px solid #dddfe2;font-size:12px;padding:8px 12px;">&quot;Cộng đồng thành viên Tiva.vn &quot; là diễn đàn kết nối các thành viên sử dụng Tiva.vn  để tìm và làm việc trên ứng dụng.

“Cộng đồng thành viên Tiva.vn ”...</td></tr></table></td></tr><tr style=""><td height="28" style="line-height:28px;">&nbsp;</td></tr></table>');
							});
							</script>
							<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwebmienphimoma%2Fvideos%2F192803138826949%2F&show_text=0&width=560" width="400" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-12">
					<div class="News bgrWhite pd15 mgt30">
						<div class="title">
							<h2 style="font-size: 22px; text-align: left;">BÀI VIẾT LIÊN QUAN <i>"{!! $post->title !!}"</i></h2>
							<hr>
						</div>
						<div  class="contentNews">
							<ul class="list-group list-group-flush">
								@if (count(\App\Entity\Post::relativeProduct($post->slug, 5)->toArray()) > 0 ) 
									@foreach (\App\Entity\Post::relativeProduct($post->slug, 5) as $postRelative)
										<li class="list-group-item"><a href="/tin-tuc/{!!  $postRelative->slug !!}" style="color: black;"><i class="far fa-hand-point-right" style="color: red;"></i> {!!  $postRelative->title !!}</a></li>
									@endforeach
								@else
									@foreach (\App\Entity\Post::newPostIndex(5) as $postRelative)
										<li class="list-group-item"><a href="/tin-tuc/{!!  $postRelative->slug !!}" style="color: black;"><i class="far fa-hand-point-right" style="color: red;"></i> {!!  $postRelative->title !!}</a></li>
									@endforeach
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
        </div>
    </section>
	
@include('xhome.partials.postVip')
	<script async defer>
        $(document).ready(function(){
            $(window).scroll(function () {
                var w = window.innerWidth;
                if (w > 767 && $("#parentScroller").height() > 400) {
                    $("#scroller").stick_in_parent({offset_top: 75});
                }
            });

        });
    </script>
    @include('xhome.partials.slideCustomer')
@endsection
