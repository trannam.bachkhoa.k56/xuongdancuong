@extends('vn3c.layout.site')

@section('type_meta', 'Login facebook')
@section('content')

@include('xhome.partials.slider')
<script>
    $(document).ready(function () {
        $('#loginFaceBook').modal('show'); 
    });        
</script>

<!-- Modal -->
<div class="modal fade" id="loginFaceBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width:500px">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Đăng nhập bằng facebook</h5>
            <a type="button" class="" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </a>
        </div>
        <div class="modal-body" style="text-align: center; height:50px ">
        <a style="padding: 20px 20px; background: #045fea; color:#fff;display:block;font-size:18px;text-decoration: none;" href="{{route('facebook_redirect',['social' => 'facebook'])}}">
            Kết nối với Facebook <i class="fab fa-facebook-square"></i>
        </a>
        </div>
        <div class="modal-footer">

        </div>
        </div>
    </div>
</div>


 
@endsection


