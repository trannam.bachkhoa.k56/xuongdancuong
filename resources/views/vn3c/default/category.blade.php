@extends('vn3c.layout.site')

@section('type_meta', 'website')
@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description', !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
    <!-- Main Content Pages -->
            <div class="content-pages pd20">
                <!-- Subpages -->
                <div class="sub-home-pages">
					<!-- Blog Subpage -->
                    <section id="blog" class="sub-page">
                        <div class="sub-page-inner">
                            <div class="section-title">
                                <div class="main-title">
                                    <div class="title-main-page">
                                        <h4>{{$category->title}}</h4>
                                        <p>MOMA Cung cấp các dịch vụ thiết kế website như sau</p>
                                    </div>
                                </div>
                            </div>

                            <div class="section-content">
                                <div class="row blog-grid-flex">
									@foreach ($posts as $id => $post)
									
                                    <div class="col-md-3 col-sm-4 blog-item">
                                        <div class="blog-article">
                                            <div class="CropImg">
                                                <a class="thumbs" href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}"> <img src="{{ asset($post->image) }}" class="img-responsive" alt="{{ $post->title }}"></a>
                                            </div>
                                            <div class="article-link"> <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}"> <img src="{{ asset($post->image) }}"><i class="lnr lnr-arrow-right"></i></a> </div>
                                            <div class="article-content">
                                                <h4><a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}">{{ $post->title }}</a></h4>
												<?php $date = date_create($post->created_at); ?>
                                                <div class="meta"> <span>{{ $post->user_email }}  |  {{ date_format($date,"d/m/Y H:i") }}</span> <span><i>Trong</i> <a href="/danh-muc/{{ $category->slug }}">{{$category->title}}</a></span></div>
                                                <!-- <p>{{ $post->description }}</p> -->
                                            </div>
                                        </div>
                                    </div>
									@endforeach
                                </div>
                                <div class="pagination-nav nav-center">
									{{ $posts->links() }}
                                    <!--<a href="#" class="btn btn-prev"><i class="lnr lnr-arrow-left"></i> prev</a> 
                                    <a href="#" class="btn btn-next">next <i class="lnr lnr-arrow-right"></i></a>-->
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /Blog Subpage -->
                </div>
                <!-- /Page changer wrapper -->
            </div>
            <!-- /Main Content -->
@endsection

