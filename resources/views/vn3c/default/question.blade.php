@extends('vn3c.layout.site')

@section('type_meta', 'website')
@section('title', 'Hỏi đáp' )
@section('meta_description', 'Trang hỏi đáp' )
@section('keywords', '')

@section('content')

<!--section class="content-contact content-pages">
	<div class="container">
		<div class="section-title">
			<div class="main-title">
				<div class="title-main-page">
					<h1>Hỏi đáp</h1>
					<p></p>
				</div>
			</div>
		</div>
	</div>
</section-->

<div class="content-pages" >
	<!-- Subpages -->
<div class="sub-home-pages">
	<!-- Titlebar -->
<div id="titlebar">
<div class="container">
<div class="row">
<aside class="ask_left col-md-3 col-xs-12 ">
    <!-- === col_topic -->
    <div class="col_topic">
        <h2 class="title"> Tin nổi bật</h2>
        <ul class="highlight_topic  list-group-flush">
		@foreach(App\Entity\Post::newPostIndex() as $postNew)
            <li class="list-group-item item">
             <div class="subtopic hide">
				<a href="{{route('post',['cate_slug'=>'tin-tuc-moi','post_slug' => $postNew->slug ])}}" style="text-transform: capitalize;">
				{{$postNew->title}}
				</a>
            </div>

            </li>
		@endforeach
        </ul>
       

	</div>
    <!-- col_content-->
</aside>
<aside class="col_content  col-md-9  col-xs-12 ">
	<div style="position:relative">
		<input type="text" id="txtKeysearch" name="word" onkeyup="submitSearch()" autocomplete="off" placeholder="Tìm hướng dẫn, mẹo hay, câu hỏi, sản phẩm">
		
		<i class="fas fa-search" style="position:absolute; right:10px; top:40%;"></i>		
		<!--button type="submit"  class="btn btn-primary">Tìm kiếm</button-->
	</div>

	<article id="content-page">
		<h1 class="title" style="font-size:22px;margin-top:20px">HƯỚNG DẪN, THỦ THUẬT MỚI NHẤT</h1>
		<div id="list-question">
			<ul class="list-group">
				@foreach($posts as $post)
					  <li class="list-group-item">
						<div class="row">
							<div class="col-md-4">
								<img src="{{$post->image}}" />
							</div>
							<div class="col-md-8">
								<a href="hoi-dap/{{$post->slug}}" style="18px;text-transform: uppercase">
								<strong>{{$post->title}}</strong>								
								</a>
								<p>
								{{ App\Ultility\Ultility::textLimit($post->description ,60)}}
								</p>
							</div>
						</div>			 		  
					  </li>
				@endforeach
			</ul>
			{{$posts->links()}}
		</div>
	</article>
</aside>

</div>
</div>
</div>
<style>
a {
	color:#000;
}

li.item{
	padding:10px 0;
}
.loginFB{
	display:block;
	position:absolute;
	top:-500px;
	height:0;
}
</style>
<script type="text/javascript">
	function showInputQuestion(){
		$('.loginFB').css('top',0);
		$('.loginFB').css('position','relative');
		$('.loginFB').css('height',100);
		
		$('.question').hide();
	}
	
	function submitSearch(){
		var word = $('#txtKeysearch').val();
		$.ajax({
			url: '{{route("filter_question")}}',
			type: 'POST',     
			data: {
				_token: "{{ csrf_token() }}",
				word: word
			},
		})
		.done(function(data) {
			console.log("success");
			$('#list-question').remove();
			$('#content-page').append(data);
			
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}
</script>


@endsection