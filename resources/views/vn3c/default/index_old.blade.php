<!DOCTYPE html>
<html class="google mmfb" dir="ltr" lang="vi">
<head>

    <meta charset="utf-8">
    <meta content="initial-scale=1, minimum-scale=1, width=device-width" name="viewport">
    <title>MOMA Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng</title>

    <meta name="ROBOTS" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="title" content="MOMA Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng">
    <meta name="description"
          content="Website miễn phí, phần mềm quản lý, phần mềm bán hàng, Bộ nhận dạng thương hiệu, kết nối website với mạng xã hội."/>
    <meta name="keywords" content="Website miễn phí, thiết kế website, thiết kế website miễn phí"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta itemprop="image" content=""/>
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->
    <meta property="og:image:type" content="image/jpeg"/>
    <meta property="og:locale" content="vi_VN">
    <meta property="og:type" content="website">

    <meta property="og:title"
          content="VN3C Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng"/>
    <meta property="og:description"
          content="Website miễn phí, phần mềm quản lý, phần mềm bán hàng, Bộ nhận dạng thương hiệu, kết nối website với mạng xã hội."/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:image:secure_url" content=""/>

    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description"
          content="Website miễn phí, phần mềm quản lý, phần mềm bán hàng, Bộ nhận dạng thương hiệu, kết nối website với mạng xã hội."/>
    <meta name="twitter:title"
          content="VN3C Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng"/>
    <meta name="twitter:image" content=""/>

    <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;lang=vi" rel=
    "stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto+Slab:300,400,500&amp;lang=vi" rel=
    "stylesheet">
    <link href="//fonts.googleapis.com/css?family=Product+Sans:400&amp;lang=vi" rel="stylesheet">
    <link href="{{ asset('business/css/mybusiness.min.css') }}" rel="stylesheet">
    <script src="{{ asset('business/js/basic.js') }}"></script>

    <link href="" rel="canonical">

</head>
<body class="page-home">
<header class="header" role="banner">
    <h1 class="product-logo">
        <a class="pass-params" data-g-action="My Business" data-g-event="" data-g-label=
        "Global Nav Logo" href="/">
            <img alt="Moma" src=
            "{{ isset($information['logo']) ? $information['logo'] : '' }}"
                 srcset="{{ isset($information['logo']) ? $information['logo'] : '' }} 2x"
                 style="width: 70px; display: inline-block">
            <span class="site-name">Doanh nghiệp của tôi</span>
        </a>
    </h1>
    <nav class="main-navigation" data-ng-controller="rs.navigation.NavCtrl as navCtrl">
        <h1>
            Doanh nghiệp của tôi
        </h1>
        <ul>
            @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
                @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
                    <li class="{{ $id == 0 ? 'active' : 'pass-params' }}">
                        <a href="{{ $menuElement['url'] }}">{{ $menuElement['title_show'] }}</a>
                    </li>
                @endforeach
            @endforeach
        </ul>
        <div class="cta cta-wrapper">
            <a class="cta-signup pass-params" href="/trang/tao-moi-website">
                Đăng ký website Miễn Phí</a>
        </div>
    </nav>
    <button class="nav-toggle-button"><span class="icon-menu-els"><span class=
                                                                        "menu-bar"></span> <span
                    class="menu-bar"></span> <span class=
                                                   "menu-bar"></span></span></button>
</header>
<div class="main" role="main">
    <div ng-controller="ParallaxCtrl"></div>
    <section class="section section-hero section-hero-v3" >
        <div class="site-width">
            <div class="cols-row">
                <div class="section-hero-copy col-5">
                    <h1>
                        Cho mọi người biết bạn mở cửa kinh doanh.
                    </h1>
                    <p class="font-large">
                        Tạo cho mình 1 website hiển thị giờ làm việc, số điện thoại và chỉ đường đến doanh nghiệp của bạn - với website của chúng tôi.
                    </p>
                    <p>
                        <a class="button pass-params" href="/trang/tao-moi-website">
                            Bắt đầu tạo website</a>
                    </p>
                    <div data-eto="" data-eto-locale="ALL_vn">
                        <p class="eto-hero font-medium margin-bottom-0"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-hero-media section-hero-media-nrs3 deep-teal" >
            <img alt="website cho doanh nghiệp"
                 class="" src=
                 "{{ asset('/business/image/benefits-control_1x.png') }}" style="width: 100%;">
        </div>

    </section>
    <div id="section-benefits">
        <section class="section light-gray monitor ">
            <div class="site-width">
                <div class="cols-row">
                    <div class="col-4 business-info">
                        <h4 class="section-sub-heading border-red benefits-title">
                            Khách hàng
                        </h4>
                        <h2>
                            Quản lý quan hệ khách hàng!
                        </h2>
                        <p class="font-large">
                            là một phương pháp giúp các doanh nghiệp tiếp cận và giao tiếp với khách hàng một cách có hệ thống và hiệu quả, quản lý các thông tin của khách hàng như thông tin về tài khoản, nhu cầu, liên lạc và các vấn đề khác nhằm phục vụ khách hàng tốt hơn.
                        </p>
                        <p class="font-large">
                            <a class="link-more pass-params" href="/trang/tao-moi-website">
                                Trải nghiệm website
                            </a>
                        </p>
                    </div>
                    <div class="col-7 col-offset-1  ">
                        <img alt="" src="{{ asset('business/image/quan_ly_kh.png') }}"
                             srcset=
                             "{{ asset('business/image/quan_ly_kh.png') }} 2x" style="width: 100%">

                    </div>
                </div>
            </div>
        </section>
        <section class="section monitor section-business-benefits section-benefits-2">
            <div class="site-width">
                <div class="cols-row">
                    <div class="col-4 business-info">
                        <h4 class="section-sub-heading border-red benefits-title">
                            Bán hàng
                        </h4>
                        <h2>
                            Bán hàng đa kênh
                        </h2>
                        <p class="font-large">
                            Nền tảng quản lý bán hàng đa kênh, giúp bạn hợp nhất kinh doanh Online và chuỗi cửa hàng hoàn hảo.
                        </p>
                        <p class="font-large">
                            <a class="link-more pass-params" href="/trang/tao-moi-website">Trải nghiệm website
                            </a>
                        </p>
                    </div>
                    <div class="col-7 col-offset-1">
                        <img alt="" src="{{ asset('business/image/dakenh.png') }}"
                             srcset=
                             "{{ asset('business/image/dakenh.png') }} 2x" style="width: 100%">

                    </div>
                </div>
            </div>
        </section>
        <section class="section light-gray monitor section-benefits-3">
            <div class="site-width">
                <div class="cols-row">
                    <div class="col-4">
                        <h4 class="section-sub-heading border-red benefits-title">
                            Zalo
                        </h4>
                        <h2>
                            Tích hợp chat zalo trên một nền tảng
                        </h2>
                        <p class="font-large">
                            Zalo là 1 phần mềm cho cho phép chát, nhắn tin, gọi điện miễn phí. Nó còn là 1 mạng xã hội thân thiện với người dùng Việt Nam, đặc biệt là giới trẻ.
                        </p>
                        <p class="font-large">
                            <a class="link-more pass-params" href="/trang/tao-moi-website">
                                Trải nghiệm website
                            </a>
                        </p>
                    </div>
                    <div class="col-7 col-offset-1">
                        <img alt="" src="{{ asset('business/image/zalo.png') }}"
                             srcset=
                             "{{ asset('business/image/zalo.png') }} 2x" style="width: 100%">
                    </div>
                </div>
            </div>
        </section>
    </div>

</div>
<footer class="footer" id="footer" role="contentinfo">

</footer>
<script src="{{ asset('business/js/picturefill.min.js') }}"></script>
</body>
</html>