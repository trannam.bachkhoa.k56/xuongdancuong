@extends('vn3c.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )
@section('type_meta', 'website')
@section('content')

<section  class="bannerLarge 70vh mgt80">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-12 mgt50" >
				@if (empty($userLogins))
				<div class="titleLarge textLeft textUpper">
						<h1 class="fw7 f24 md-f28 sm-f20 col-f16 top25 mgb20 white">
							<font color="orange">MOMA</font> WEBSITE TỰ ĐỘNG HÓA CHĂM SÓC KHÁCH HÀNG
						</h1>
					</div>
					<div class="titleSmall textLeft mgb30">
						<h5 class=" md-f14 sm-f12 col-f11 fw4 f16 white">MOMA <font color="orange">là website  </font> kết nối <font color="orange">thương mại toàn cầu</font> giúp đỡ công việc kinh doanh của bạn!</h5>
					</div>
				@endif
				<h3 class="uppercase white">Tài khoản đăng nhập gần đây</h3>
				@if (!empty($userLogins))
                <ul class="listLogin">
					@foreach ($userLogins as $userLogin)
					<li onclick="return getLoginedMoma(this);"><a href="#" class="row">
						<div class="col-md-2"><img src="{{ !empty($userLogin['image']) ? asset($userLogin['image']) : asset('image/avatar_admin.png') }}"/></div>
						<div class="col-md-10">
							<h4 class="url_logined">{{ $userLogin['url'] }}</h4>
							<p class="email_logined">{{ $userLogin['email'] }}</p>
						</div>
					</a></li>
					@endforeach
				</ul>
				@endif
				
            </div>
			<div class="col-md-7 col-12 mg">
				<div class="inBanner pd9p">
					
					<div class="home-buttons textCenter">
					<form action="" method="post" id="formLoginMoma">
						
						<div class="form-group">
							<div class="input-group">
								<input type="email" class="form-control" value="{{ old('email') }}" id="momaEmail1" aria-describedby="emailHelp" name="email" value="{{ old('email') }}" placeholder="Thông tin tài khoản">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="input-group">
								<input type="password" class="form-control" id="momaPassword" name="password" placeholder="Mật khẩu">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
							</div>
							@if ($errors->has('password'))
								<span class="help-block white">
									<strong >{{ $errors->first('password') }}</strong>
								</span>
							@endif
							@if ($errors->has('email'))
								<span class="help-block white">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
					  <!-- <div class="form-check">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label" for="exampleCheck1">Duy trì đăng nhập</label>
					  </div> -->
					  <button  type="submit" onclick="return loginMoma(this);" class="bt-submit btBlue md-f18 sm-f14Im col-f9">ĐĂNG NHẬP QUẢN LÝ WEBSITE</button>
					</form>
					
				
					<!--<input type="text" placeholder="Thông tin tài khoản"/>
					<input type="password" placeholder="Mật khẩu"/>
					<input type="submit" class="bt-submit md-f18 sm-f14Im col-f9" value="Tiếp tục truy cập Website"/> -->
					<!--<a href="trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="bt-submit md-f18 sm-f14Im col-f9"><i class="lnr lnr-briefcase"></i>
						Tạo website bán hàng miễn phí
					</a> -->
				</div>
				<div class="textCenter mgt30 white"><a href="http://moma.vn/admin/password/reset">Quên mật khẩu?</a></div>
				<div class="textCenter white mgb30 home-buttons mgt30 bortwhite">
					<a href="trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="bt-submit md-f18 btGreen sm-f14Im col-f9 uppercase">Tạo website miễn phí</a>
				</div>
				
                </div>
			</div>
        </div>
    </div>
</section>


<!-- <section class="slideMoma  70vh mgt80 mgt80-sm">
	  <div class="momaSlideForm">
		<div class="bannerEmail">
			<div class="item"  style="background-image: url('/assets/vn3ctheme/img/quang-cao-email.jpg');" >
				<div class="container">
					<div class="row">
						<div class="col-6 img bounceInLeft">
							<img src="/assets/vn3ctheme/img/thietkebanner.png" alt="Thiết kế wesbite"/>
						</div>
						<div class="col-6 img bounceInRight">
							<a href="https://moma.vn/trang/tao-moi-website">
								<img src="/assets/vn3ctheme/img/textthietke.png" alt="Thiết kế wesbite"/>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bannerEmail">
			<div  class="item"  style="background-image: url('/assets/vn3ctheme/img/quang-cao-mail.jpg');" >
				<div class="container">
					<div class="row">
						<div class="col-6 img bounceInLeft">
							<img src="/assets/vn3ctheme/img/emailbanner.png" alt="Online marketing A-Z"/>
						</div>
						<div class="col-6 img bounceInRight">
							<a href="https://moma.vn/trang/tao-moi-website">
								<img src="/assets/vn3ctheme/img/textemail.png" alt="Online marketing A-Z"/>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bannerEmail" >
			<div class="item"  style="background-image: url('/assets/vn3ctheme/img/quang-cao-google.jpg');" >
				<div class="container">
					<div class="row">
						<div class="col-6 img bounceInLeft">
							<img src="/assets/vn3ctheme/img/googlebanner.png" alt="Google Ads"/>
						</div>
						<div class="col-6 img bounceInRight">
						<a href="https://moma.vn/trang/tao-moi-website">
							<img src="/assets/vn3ctheme/img/textgoogle.png" alt="Google Ads"/>
						</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bannerEmail">
			<div class="item"  style="background-image: url('/assets/vn3ctheme/img/quang-cao-email.jpg');">
				<div class="container">
					<div class="row">
						<div class="col-6 img bounceInLeft">
							<img src="/assets/vn3ctheme/img/mailbanner.png" alt="Email marketing"/>
						</div>
						<div class="col-6 img bounceInRight">
							<a href="https://moma.vn/trang/tao-moi-website">
								<img src="/assets/vn3ctheme/img/textmail.png" alt="Email marketing"/>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bannerEmail" >
			<div class="item" style="background: url('/assets/vn3ctheme/img/quang-cao-facebook.jpg');">
				<div class="container">
					<div class="row">
						<div class="col-6 img bounceInLeft">
							<img src="/assets/vn3ctheme/img/fbbanner.png" alt="Quảng cáo Facebook"/>
						</div>
						<div class="col-6 img bounceInRight">
							<a href="https://moma.vn/trang/tao-moi-website">
								<img src="/assets/vn3ctheme/img/textfb.png" alt="Quảng cáo Facebook"/>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	  </div>
</section> -->

<div class="section-content relative" style="background : url('/assets/vn3ctheme/img/bgrFooterSlide.jpg')  ; background-size:cover;padding: 10px;" ">
	<div class="container">
		<div class="row row-small" id="row-833374531">
		<div class="col-sm-12 col-lg-7 col-xs-12"><div class="col-inner dark">
		<p class="f18 md-f16 sm-f14 col-f12"><span style="font-size: 95%; color: #fff;"> Với hơn 5 năm kinh nghiệm trong lĩnh vực Website Tự động hóa, Moma tự hào triển khai
		<strong style=" color: #FFEB3B;"> 900+ Website</strong> cho<strong style=" color: #FFEB3B;"> 800+ khách hàng</strong> và<strong> hàng trăm thương hiệu</strong>
		trong nước cũng như quốc tế.</span></p>
		</div></div>
		<div class="col-sm-12 col-lg-3 col-xs-12">
		<div class="col-inner text-center">
		<div class="gap-element" style="display:block; height:auto; padding-top:12px"></div>
		<a data-animate="bounceIn" href="https://moma.vn/trang/tao-moi-website" target="_self" class="btn btn-success f18 md-f16 sm-f14 col-f12" style="border-radius:5px; color:#fff" data-animated="true">
		  <span>Tạo website ngay</span>
		</a>
		</div>
		</div>
		<div class="none-tb col col-md-2 col-sm-12 col-lg-2 col-xs-12"><div class="col-inner text-center">
		<div class="gap-element" style="display:block; height:auto; padding-top:12px"></div>
			<a data-animate="bounceIn" class="btn btn-danger" style="border-radius:5px; color:#fff" data-animated="true">
			<i class="fa fa-star"></i> Portfolio <span></span>
		  </a>

		</div></div>

		<style scope="scope">

		</style>
		</div>
		</div>
		</div>


<script>
	$(document).ready(function(){
	  $('.momaSlideForm').slick();
	});
</script>
<section class="goodProduct pdt30 pdb30">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-12 mg">
				<h2 class="textUpper f32 fw7 md-f23 sm-f18 col-f15 ">BẠN CÓ SẢN PHẨM TỐT</h2>
				<p class="f18 md-f16 sm-f14 col-f12">Bạn là một nhà kinh doanh truyền thống, công việc của bạn cũng đang tốt nhưng với tham vọng của mình bạn muốn sản phẩm của mình hiện diện trên internet và quan trọng hơn hết bạn muốn Internet là một kênh bán hàng chủ đạo của mình.</p>
				<div class="row">
					<div class="col-md-3 col-6">
						<img src="./assets/vn3ctheme/img/diennuocicon.jpg" alt="https://moma.vn"/>
					</div>
					<div class="col-md-3 col-6">
						<img src="./assets/vn3ctheme/img/dieuhoaicon.jpg" alt="https://moma.vn"/>
					</div>
					<div class="col-md-3 col-6">
						<img src="./assets/vn3ctheme/img/goicon.jpg" alt="https://moma.vn" />
					</div>
					<div class="col-md-3 col-6">
						<img src="./assets/vn3ctheme/img/tranthachicon.jpg" alt="https://moma.vn" />
					</div>
					<div class="col-md-12 col-12 mgt20 hideOnMobile">
						<a data-animate="bounceIn" href="https://moma.vn/trang/tao-moi-website" 
						target="_self" class="btn btn-warning f18 md-f16 sm-f14 col-f12" style="border-radius:5px; color:#fff;" data-animated="true">
						  <span>Tạo website ngay </span>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-12">
				<img src="./assets/vn3ctheme/img/inter.png" alt="https://moma.vn" width="100%"/>
			</div>
			<div class="col-md-12 col-12 mgt20 showOnMobile">
				<a data-animate="bounceIn" href="https://moma.vn/trang/tao-moi-website" 
				target="_self" class="btn btn-warning f18 md-f16 sm-f14 col-f12" style="border-radius:5px; color:#fff;" data-animated="true">
				  <span>Tạo website ngay </span>
				</a>
			</div>
		</div>
	</div>
</section>
<section class="askWhy pdt30 pdb30" style="background-color: rgb(36, 36, 38);">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-12">
				<img src="./assets/vn3ctheme/img/seox.png" alt="https://moma.vn" width="100%" />
			</div>
			<div class="col-md-6 col-12 mg">
				<h2 class="white textUpper f32 fw7 md-f23 sm-f18 col-f15 ">Bạn không biết bắt đầu từ đâu?</h2>
				<p class="white f18 md-f16 sm-f14 col-f12">Với sự phát triển chóng mặt của thương mại điện tử cộng với nguồn lợi khổng lồ từ nó ai cũng mong muốn mình có được một kênh bán hàng trên đó. Nhưng không phải ai cũng có thể biết cách để có thể bán hàng tốt nhất trên Internet.</p>
				<p class="white f18 md-f16 sm-f14 col-f12">Đến với chúng tôi bạn sẽ được thiết lập một hệ thống kinh doanh từ A-Z để có thể mang lại lợi nhuận tối đa từ Internet.</p>
				
				<div class="mgt30">
					<a data-animate="bounceIn" href="https://moma.vn/trang/tao-moi-website" 
					target="_self" class="btn btn-warning f18 md-f16 sm-f14 col-f12" style="border-radius:5px; color:#fff;" data-animated="true">
					  <span>Bắt đầu ngay </span>
					</a>
				</div>
			</div>
			
		</div>
	</div>
</section>
<section class="YouWorry pdt30 pdb30">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-12 mg">
				<h2 class="textUpper f32 fw7 md-f23 sm-f18 col-f15 ">Bạn lo lắng về chi phí?</h2>
				<p class="f18 md-f16 sm-f14 col-f12">Chi phí thiết lập một hệ thống kinh doanh online luôn luôn thấp hơn rất nhiều so với một hệ thống kinh doanh truyền thống.</p>
				<p class="f18 md-f16 sm-f14 col-f12">Với chúng tôi điều đó lại càng thể hiện rõ hơn nữa, Chúng tôi cam kết sẽ giúp các bạn có được lợi nhuận trong thời gian cộng tác cùng chúng tôi.</p>
				<div class="row">
					<div class="col-md-6 col-12">
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/connect.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18"> Mạng kết nối rộng khắp </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/fb.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18"> Quảng cáo di động </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/vantay.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18"> Widget Ads </span>
						</div>
					</div>
					<div class="col-md-6 col-12">
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/refetch.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18">Responsive Ads </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/do.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18"> InterGrate Google Ads </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/set.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18">  Chi phí cực rẻ </span>
						</div>
					</div>
					
					<div class="mgt20 col-md-12 col-12 hideOnMobile">
						<a data-animate="bounceIn" href="https://moma.vn/trang/tao-moi-website" 
						target="_self" class="btn btn-warning f18 md-f16 sm-f14 col-f12" style="border-radius:5px; color:#fff;" data-animated="true">
						  <span>Bắt đầu ngay </span>
						</a>
					</div>

				</div>
			</div>
			<div class="col-md-6 col-12">
				<img src="./assets/vn3ctheme/img/seoy.png" alt="https://moma.vn" width="100%" />
			</div>
				<div class="mgt20 col-md-12 col-12 showOnMobile">
						<a data-animate="bounceIn" href="https://moma.vn/trang/tao-moi-website" 
						target="_self" class="btn btn-warning f18 md-f16 sm-f14 col-f12" style="border-radius:5px; color:#fff;" data-animated="true">
						  <span>Bắt đầu ngay</span>
						</a>
					</div>
		</div>
	</div>
</section>
<section class="supportYou pdt30 pdb30" style="background-color: rgb(97, 102, 239)">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-12">
				<img src="./assets/vn3ctheme/img/social.png" alt="https://moma.vn" width="100%" />
			</div>
			<div class="col-md-6 col-12 mg">
				<h2 class="textUpper white f32 fw7 md-f23 sm-f18 col-f15  centert">CHÚNG TÔI LÀM GÌ ĐỂ GIÚP BẠN?</h2>
				<p class="white f18 md-f16 sm-f14 col-f12">Chúng tôi giúp bạn thiết lập hệ thống Online Marketing chuyên nghiệp để tiếp cận khách hàng tiềm năng ở mọi kênh truyền thông.</p>
				<div class="row">
					<div class="col-md-6 col-12">
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/refetch1.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18 md-f16 sm-f14 col-f12 white"> Nghiên cứu marketing </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/vantay1.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18 md-f16 sm-f14 col-f12 white"> Thiết kế website </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/fb1.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18 md-f16 sm-f14 col-f12 white"> Thiết lập kênh bán hàng </span>
						</div>
					</div>
					<div class="col-md-6 col-12">
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/huychuong.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18 md-f16 sm-f14 col-f12 white">Quảng cáo sản phẩm </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/set1.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18 md-f16 sm-f14 col-f12 white"> SEO từ khóa </span>
						</div>
						<div class="iconText mgb5">
							<img src="./assets/vn3ctheme/img/connet1.jpg" alt="https://moma.vn" width="25" height="25"/> <span class="f18 md-f16 sm-f14 col-f12 white">  Chăm sóc hệ thống </span>
						</div>
					</div>
				</div>
				<p class="white f18 md-f16 sm-f14 col-f12">Chi phí linh hoạt dựa vào sản phẩm và độ khó của sản phẩm với thị trường nhưng chúng tôi luôn cam kết chi phí là thấp nhất so với các nhà cung cấp khác. Chi phí theo giai đoạn: bạn chỉ sẽ trả tiền theo từng giai đoạn mà chúng tôi cam kết làm được cho bạn.</p>
				<div class="mgt20">
					<a data-animate="bounceIn" href="https://moma.vn/trang/tao-moi-website" 
					target="_self" class="btn btn-warning f18 md-f16 sm-f14 col-f12" style="border-radius:5px; color:#fff;float-right" data-animated="true">
					  <span>Dùng thử ngay</span>
					  
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="productPriceList pdt30 pdb30 container">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 f32 md-f23 sm-f18 col-f15 mgb0">
			CHÚNG TÔI CAM KẾT VỚI BẠN</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10">
                <i class="fa fa-handshake-o orange f20" aria-hidden="true"></i>
            </div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
		<p class="f18 md-f16 sm-f14 col-f12">Một khi cộng tác với nhau trong công việc, chúng tôi luôn mong mong muốn cam kết với khách hàng những lợi ích và ưu đãi tốt nhất.</p>
    </div>
    <div class="contentProductPriceList">
        <div class="row">
			<div class="col-md-4 col-12">
				<img src="./assets/vn3ctheme/img/x1.jpg" alt="https://moma.vn" width="100%" />
				<h3 class="textCenter f18 textUpper">Tạo ra lợi nhuận</h3>
				<p class="textJustify f18 md-f16 sm-f14 col-f12">Với tôn chỉ hoạt động của chúng tôi “  Tạo ra lợi nhuận cho khách hàng ” Cho nên lợi nhuận của khách hàng chính là sự sống còn của chúng tôi. Chúng tôi nghiên cứu, vận hành làm sao cho các bạn mang lại càng nhiều lợi nhuận càng tốt.</p>
			</div>
			<div class="col-md-4 col-12">
				<img src="./assets/vn3ctheme/img/x2.jpg" alt="https://moma.vn" width="100%" />
				<h3 class="textCenter f18 textUpper">Mối quan hệ và hợp tác</h3>
				<p class="textJustify f18 md-f16 sm-f14 col-f12">Với 5 năm hoạt động trong lĩnh vực digital Marketing, chúng tôi đã thiết lập hệ thống kinh doanh cho rất nhiều khách hàng với rất nhiều sản phẩm. Bạn có cơ hội được giới thiệu sản phẩm đến những đối tác phù hợp với bạn.</p>
			</div>
			<div class="col-md-4 col-12">
				<img src="./assets/vn3ctheme/img/x3.jpg" alt="https://moma.vn" width="100%" />
				<h3 class="textCenter f18 textUpper">Sự hỗ trợ trọn đời</h3>
				<p class="textJustify f18 md-f16 sm-f14 col-f12">Chúng tôi sẽ luôn cùng đồng hành với sự phát triển của bạn cho đến khi bạn không còn cần chúng tôi nữa. Sự cam kết này thể hiện sự tận tâm của chúng tôi đối với khách hàng đúng theo tiêu chí “một ngày cộng tác, trọn đời làm bạn”!</p>
			</div>
        </div>
    </div>
</section>



<section class="productPriceList pdt30 pdb30" style="background-color: #0a2953">
<div class="container">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 white f32 md-f23 sm-f18 col-f15 mgb0 textUpper">
			Đánh giá của khách hàng về MOMA
			</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10">
                <i class="fa fa-handshake-o orange f20" aria-hidden="true"></i>
            </div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
    <div class="contentProductPriceList">
			<div class="slideCustomer">
		<div class="row">
		 <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs " title="Đánh giá của khách hàng">
					   <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/1.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">MOMA tham gia "Đánh thức sự giàu có" của thầy Phạm Thành Long</h4>
			  </div-->
		   </div>
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs" title="Lễ ra mắt website tự động hóa chăm sóc khách hàng">
					    <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/moma1.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">Lễ ra mắt website tự động hóa chăm sóc khách hàng</h4>
			  </div-->
		   </div>
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/ngoc-tpcn.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">Đánh giá của khách hàng</h4>
			  </div-->
		   </div>
		   
		    <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/diep%20oto.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">Đánh giá của khách hàng</h4>
			  </div-->
		   </div>
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg ">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="/assets/vn3ctheme/img/f1.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!-- div class="infoCustomer">
				 <h4 class="white">MOMA  tại chương trình "Đánh thức sự giàu có"</h4>
			  </div-->
		   </div>
		    
			
			<div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="/assets/vn3ctheme/img/f2.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">MOMA tham gia "Đánh thức sự giàu có" của thầy Phạm Thành Long</h4>
			  </div-->
		   </div>
		   
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs" title="Đánh giá của khách hàng">
					   <img src="/assets/vn3ctheme/img/f3.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">Lễ ra mắt website tự động hóa chăm sóc khách hàng</h4>
			  </div-->
		   </div>
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="/assets/vn3ctheme/img/f4.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">Đánh giá của khách hàng</h4>
			  </div-->
		   </div>
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg ">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="/assets/vn3ctheme/img/f5.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!-- div class="infoCustomer">
				 <h4 class="white">MOMA  tại chương trình "Đánh thức sự giàu có"</h4>
			  </div-->
		   </div>
		    <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="/assets/vn3ctheme/img/f6.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">MOMA tham gia "Đánh thức sự giàu có" của thầy Phạm Thành Long</h4>
			  </div-->
		   </div>
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs" title="Lễ ra mắt website tự động hóa chăm sóc khách hàng">
					    <img src="/assets/vn3ctheme/img/f7.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">Lễ ra mắt website tự động hóa chăm sóc khách hàng</h4>
			  </div-->
		   </div>
		   
		   <div class="item pd10 col-lg-3 col-md-3 col-sm-4 col-xs-12">
			  <div class="imgItem CropImg">
					<a class="thumbs " title="Đánh giá của khách hàng">
					    <img src="/assets/vn3ctheme/img/f8.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <!--div class="infoCustomer">
				 <h4 class="white">Đánh giá của khách hàng</h4>
			  </div-->
		   </div>
		   
		  
    </div>
</div>
</div>
</div>
</section>

<style>
.CropImg:before {
	
    padding-bottom: 130%;
}
.slick-next:before{
	content: "\f054" !important;
	font-family: "Font Awesome 5 Free"; font-weight: 900 !important;
}
.slick-prev:before{
	content: "\f053" !important;
	font-family: "Font Awesome 5 Free"; font-weight: 900 !important;
}
</style>


<!--

<section class="productPriceList pdt3p container">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
			VÌ SAO CHỌN TẢI WEBSITE BÁN HÀNG MIỄN PHÍ MOMA</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10">
                <i class="fa fa-handshake-o orange f20" aria-hidden="true"></i>
            </div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
    <div class="contentProductPriceList">
        <div class="row">
			<div class="col-md-6 col-12 sm-pdb20">
				<p>1. Website bán hàng tự động hóa mang đến giải pháp quản lý và chăm sóc khách hàng toàn diện.</p>
				<p>2. Hỗ trợ quản lý quy trình sale - chăm sóc khách hàng trên một nền tảng.</p>
				<p>3. Ứng dụng thống nhất quy trình, tiết kiệm thời gian, tối ưu nguồn lực, đột phá doanh thu. </p>
				<p>4. Bảo mật thông tin khách hàng tuyệt đối.</p>
				<p>5. Liên tục cập nhật tính năng và công nghệ mới nhất.</p>
				<p>6. Đội ngũ tư vấn giải pháp năng lực, chuyên nghiệp.</p>
				<p>7. Kinh nghiệm triển khai 2000+ doanh nghiệp với 150+ ngành nghề.</p>
				<p>8. Tiết kiệm chi phí, thời gian, đột phá doanh thu.</p>
				
			</div>
            <div class="col-md-6 col-12 sm-pdb20">
                <iframe width="100%" height="300" width="300" src="https://www.youtube.com/embed/9VrCOzjWyMA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="productPriceList pdt3p container">
    <div class="titlePriceList textCenter pdb30 sm-pdb10">
        <h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
			HƯỚNG DẪN CÁCH TẠO WEBSITE BÁN HÀNG MIỄN PHÍ
		</h2>
        <div class="Line textCenter">
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
            <div class="underline inBlock mgl10 mgr10"><i class="fa fa-handshake-o orange f20" aria-hidden="true"></i></div>
            <div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
        </div>
    </div>
    <div class="contentProductPriceList">
        <div class="row">
            <div class="col-md-6 col-12 sm-pdb20">
                <iframe width="100%" height="300" src="https://www.youtube.com/embed/QGPIhCJE-9I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
			<div class="col-md-6 col-12 sm-pdb20">
				<p>1. Ấn vào button tạo website bán hàng miễn phí.</p>
				<p>2. Điền đầy đủ thông tin. ấn tạo mới website.</p>
				<p>3. Vào email kích hoạt website bán hàng.</p>
				<p>4. Vào email lấy thông tin quản trị website và tiến hành truy cập vào quản lý website và xem website của mình.</p>
				<div class="home-buttons textLeft">
					<a href="trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="bt-submit md-f18 sm-f14Im col-f9"><i class="lnr lnr-briefcase"></i>
						Tạo website bán hàng miễn phí
					</a>
				</div>
			</div>
        </div>
    </div>
</section>
<section class="formAndCompanyDescription pdt3p">
	<div class="container">
		 <div class="titlePriceList textCenter pdb30 sm-pdb10">
			<h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
				MOMA NỀN TẢNG TỰ ĐỘNG HOÁ CHĂM SÓC VÀ TÌM KIẾM KHÁCH HÀNG
			</h2>
			<div class="Line textCenter">
				<div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
				<div class="underline inBlock mgl10 mgr10"><i class="fa fa-handshake-o orange f20" aria-hidden="true"></i></div>
				<div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-12 sm-pdb20" >
				<div id="parentScroller">
			
				<h3>Nền tảng công nghệ Moma</h3>
				<ul>
					<li>Giải pháp giúp các đơn vị kinh doanh trên nhiều kênh như facebook, zalo, sàn thương mại điện tử, diễn đàn rao vặt.</li>
					<li>Giúp các chủ doanh nghiệp quản lý toàn bộ khách hàng và đơn hàng về một chỗ.</li>
					<li>Chăm sóc khách hàng hoàn toàn tự động.</li>
					<li>Với giải pháp của moma giúp các chủ doanh nghiệp không cần phòng marketing mà có thể tiết kiệm được 40 - 60 triệu/tháng.</li>
					<li>Trong tháng 10 chúng tôi đang có chính sách khuyến mãi gói cài đặt google ads khi mua website. (gói cài đặt google ads ngoài thị trường đang phải trả 30tr).</li>
				</ul>
				<h3>Moma ra mắt nền tảng website tự động hóa chăm sóc khách hàng</h3>
				<img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/71312836_2560380020861170_7044360580123066368_o.jpg" alt="Hoạt động của MOMA" />
				<h3>Moma tài trợ chương trình "đánh thức sự giàu có" của thầy Phạm Thành Long</h3>
				<img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/72470369_137134171003700_4997387791143796736_n%20(1).jpg" alt="MOMA tham dự chương trình Đánh thức sự giàu có của thầy Phạm Thành Long" />
				<h3>Đội ngũ chăm sóc khách hàng chuyên nghiệp của moma</h3>
				<p>Moma luôn muốn mang đến dịch vụ tốt nhất cho khách hàng: chăm sóc khách hàng tốt nhất, phục vụ khách hàng nhanh nhất, khách hàng hài lòng nhất.</p>
				<img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/74235605_137595494290901_1053836033562509312_o.jpg" alt="Đội ngũ nhân viên giàu kinh nghiệm của MOMA" />
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12" >
						<div id="scroller">
							<form onSubmit="return contact(this);" class="wpcf7-form" method="post" action="{{route('sub_contact')}} ">
              								{!! csrf_field() !!}
              								<input type="hidden" name="is_json"
              									class="form-control captcha" value="1" placeholder="">
              								<div class="form-group">
              								  <div>
              									<input type="text" class="form-control"  placeholder="Tên của bạn"name="name" required>
              								  </div>
              								</div>
              								<div class="form-group">
              								  <div>
              									<input type="email" class="form-control" placeholder="Email của bạn" name="email" required>
              								  </div>
              								</div>
              								<div class="form-group">
              								  <div > 
              									<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required>
              								  </div>
              								</div>
              								<div class="form-group">
              								  <div > 
              									<textarea class="form-control" rows="2" name="message"placeholder="Nội dung ghi chú"></textarea>
              								  </div>
              								</div>
              									 <input type="hidden" value="" name="" id="utm_source"/>
              									 <input type="hidden" value="" name="" id="utm_medium"/>
              									 <input type="hidden" value="" name="" id="utm_campaign"/>
              								<div class="form-group">
              								  <div>
              									<button type="submit" class="btn  btn-warning">ĐĂNG KÝ TƯ VẤN</button>
              								  </div>
              								</div>
              							 </form>	
						</div>
					</div>
				
		</div>
	</div>
	<script>
        $(document).ready(function(){
            $(window).scroll(function () {
                var w = window.innerWidth;
                if (w > 767 && $("#parentScroller").height() > 400) {
                    $("#scroller").stick_in_parent({offset_top: 120});
                }
            });

        });
    </script>
</section>
<section class="memorableMoment pdt3p">
	<div class="container">
		<div class="titlePriceList textCenter pdb30 sm-pdb10">
			<h2 class="fw7 blueMoma f32 md-f23 sm-f18 col-f15 mgb0">
				KHOẢNG KHẮC ĐẸP CỦA CÔNG TY MOMA
			</h2>
			<div class="Line textCenter">
				<div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
				<div class="underline inBlock mgl10 mgr10"><i class="fa fa-handshake-o orange f20" aria-hidden="true"></i></div>
				<div class="underline h1x w100x inBlock bgrOrange mgb7"></div>
			</div>
		</div>
		<div class="slideCustomer">
		   <div class="item pd10">
			  <div class="imgItem">
					<a title="Đánh giá của khách hàng">
					   <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/72470369_137134171003700_4997387791143796736_n.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <div class="infoCustomer">
				 <h4>MOMA tham gia "Đánh thức sự giàu có" của thầy Phạm Thành Long</h4>
			  </div>
		   </div>
		   
		   <div class="item pd10">
			  <div class="imgItem">
					<a title="Lễ ra mắt website tự động hóa chăm sóc khách hàng">
					   <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/71312836_2560380020861170_7044360580123066368_o.jpg" alt="Lễ ra mắt website tự động hóa chăm sóc khách hàng" class="mg">
					</a>
			  </div>
			  <div class="infoCustomer">
				 <h4>Lễ ra mắt website tự động hóa chăm sóc khách hàng</h4>
			  </div>
		   </div>
		   
		   <div class="item pd10">
			  <div class="imgItem">
					<a title="Đánh giá của khách hàng">
					   <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/74235605_137595494290901_1053836033562509312_o.jpg" alt="Đánh giá của khách hàng" class="mg">
					</a>
			  </div>
			  <div class="infoCustomer">
				 <h4>Đánh giá của khách hàng</h4>
			  </div>
		   </div>
		   
		   <div class="item pd10">
			  <div class="imgItem">
					<a title="Đánh giá của khách hàng">
					   <img src="https://taowebsitemienphi.com/libraries/libraryxhome-692/images/khachhang/72470369_137134171003700_4997387791143796736_n.jpg" alt="MOMA tại chương trình Đánh thức sự giàu có" class="mg">
					</a>
			  </div>
			  <div class="infoCustomer">
				 <h4>MOMA tại chương trình "Đánh thức sự giàu có"</h4>
			  </div>
		   </div>
		</div>
	</div>
	
</section>  -->


@endsection
