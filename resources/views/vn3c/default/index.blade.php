@extends('vn3c.layout.layout_xhome')
@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')
@section('meta_image', '/vn3c/img/share_fb.jpg')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'home')
@section('dynx_totalvalue', '0')
@section('content')

    @include('xhome.partials.slider')
    <section class="content pdt20">
        <div class="container">
            <div class="title">
                <h2 class="colortl titl">Danh sách khách hàng đã tạo website</h2>
            </div>
            <div class="row">

                @foreach(\App\Entity\Domain::getAffiliateMoma() as $id => $domain)
                    <div class="col-6 col-lg-3 pd10 itemProduct">
                        <div class="imgItem relative">
                            <div class="boxDetails"> 
                                <div class="borderBoxDetails hvr-shutter-in-vertical">
                                </div>
                            </div>
                            <div class="CropImg">
                                <a href="{{ $domain->url }}" class="noDecoration thumbs" target="_blank">
                                    <img src="{{ !empty($domain->image) ? $domain->image : '/libraries/libraryvn3c-1/images/LogoMoMa%20(1).jpg' }}"
                                         alt="{{ $domain->name }}">
                                </a>
                            </div>
                        </div>
                        <div class="infoItem boxP">
                            <a href="{{ $domain->url }}" target="_blank" class="noDecoration"><h3>{!! $domain->name !!}</h3></a>
                            <a href="{{ $domain->url }}" target="_blank" class="noDecoration">
                                <i>{!! $domain->url !!}</i>
                            </a>
                            <p class="star">
                                <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star"
                                                                                aria-hidden="true"></i><i
                                        class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star"
                                                                                     aria-hidden="true"></i><i
                                        class="fa fa-star" aria-hidden="true"></i>
                            </p>
                        </div>
                    </div>
                @endforeach
                <script>
                    $('.boxP').matchHeight();
                </script>
            </div>
        </div>

    </section>

    <div class="v2_bnc_intro_company col-xs-12 no-padding mgt30 pdt20 pdb30">
        <div class="container">
            <div class="title">
                <h1 class="colortl titl">{{ isset($information['tieu-de-trang']) ? $information['tieu-de-trang']  : '' }}</h1>
            </div>
            <div class="row" >
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div id="parentScroller">
                        <?= isset($information['sologan']) ? $information['sologan'] : '' ?>

                    </div>
                </div>
				  <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                <div class="col-md-6 col-sm-12 col-xs-12" >
                    <div id="scroller" class="">
                        <form method="POST" action="{{ route('check-website') }}" id="createWebsite">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="domain" class="form-control" value="{{ old('domain') }}" placeholder="Đặt tên website, thương hiệu hoặc dịch vụ" required/>
                            </div>
                            <input type="hidden" name="theme_code" value="xhome" />

                            <script>
                                function changeTheme(e) {
                                    var theme = $(e).val();
                                    $('.imagePage').hide();
                                    $('.' + theme).show();
                                }

                            </script>
                            <div class="form-group">
                                <input type="text" name="name_home" class="form-control" value="{{ old('name_home') }}" placeholder="Họ và tên Khách hàng bắt buộc" required/>
                            </div>

                            <div class="form-group">
                                <input type="number" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Số điện thoại bắt buộc" required />
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email bắt buộc" required/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Mật khẩu bắt buộc" required/>
                            </div>

                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="form-control" id="inputPassword" placeholder="Nhập lại mật khẩu bắt buộc" required/>
                            </div>

							<div class="g-recaptcha" data-sitekey="6Let294UAAAAAGrUDEUsEUn-tw24S7E50Dqe4jcj"></div>
							<br/>
							
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-3 col-form-label"></label>
                                <div class="col-sm-9">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
                                    @endif
                                    @if ($errors->has('name'))
                                        <span class="help-block">
												<strong>{{ $errors->first('name') }}</strong>
											</span>
                                    @endif
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
												<strong>{{ $errors->first('phone') }}</strong>
											</span>
                                    @endif
                                    @if ($errors->has('password'))
                                        <span class="help-block">
												<strong>{{ $errors->first('password') }}</strong>
											</span>
                                    @endif
                                    @if ($errors->has('errorDomain'))
                                        <span class="help-block">
												<strong>{{ $errors->first('errorDomain') }}</strong>
											</span>
                                    @endif
                                </div>
                                <input type="hidden" value="{{ isset($_GET['utm_source']) ? $_GET['utm_source'] : '' }}" name="utm_source">
                            </div>
                            <div class="form-group ">
                                <button type="submit" class="btn  btn-warning" onclick="return clickRegister(this);" data-loading-text="Đang trong quá trình tạo website">ĐĂNG KÝ NGAY</button>
                            </div>
                            <script>
                                function clickRegister(e) {
                                    var btn = $(e).html("Đang trong quá trình tạo website");
                                    $(e).attr('style', 'background: #5a6268;');
                                    $(e).prop('disabled', true);

                                    $('#createWebsite').submit();
                                }
                            </script>

                        </form>
                    </div>
                   
                </div>


            </div>
        </div>
    </div>


    <script>
        $(document).ready(function(){
            $(window).scroll(function () {
                var w = window.innerWidth;
                if (w > 767 && $("#parentScroller").height() > 400) {
                    $("#scroller").stick_in_parent({offset_top: 175});
                }
            });

        });
    </script>
    @include('xhome.partials.slideCustomer')
    @include('xhome.partials.popup')

@endsection
