@extends('vn3c.layout.site')

@section('type_meta', 'website')
@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description', !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
    <!-- Main Content Pages -->
	<div class="content-pages pdTop60 bgfd">
		<!-- Subpages -->
		<div class="sub-home-pages">
			<div class="row">
				<div class="container">
					<!-- Portfolio Subpage -->
					<section id="portfolio" class="sub-page">
						<div class="sub-page-inner">
							<div class="section-title">
								<div class="main-title">
									<div class="title-main-page">
										<h4>{{ $category->title }}</h4>
										<p>Lựa chọn giao diện phù hợp từ kho giao diện</p>
									</div>
								</div>
							</div>

							<div class="section-content">
								<!-- <div class="filter-tabs">
									<button class="fil-cat" data-rel="all"><span>0</span> All</button>
									<button class="fil-cat" data-rel="photography"><span>05</span> Websites</button>
									<button class="fil-cat" data-rel="web-design"><span>07</span> Decorations</button>
									<button class="fil-cat" data-rel="branding"><span>12</span> Business Logo</button>
								</div> -->
								
								<div class="portfolio-grid portfolio-trigger" id="portfolio-page">
									<div class="row">
										@foreach ($products as $id => $product)
										<div class="col-lg-4 col-md-6 col-sm-6 col-12 portfolio-item">
											<div class="column">
												<a href="{{ route('product', ['post_slug' => $product->slug]) }}" class="portfolio-img">
													<img src="{{ asset($product->image) }}" class="img-responsive" alt="{{ $product->title }}">
												</a>
												<div class="portfolio-data">
													<h4><a href="{{ route('product', ['post_slug' => $product->slug]) }}">{{ $product->title }}</a></h4>
													<p class="price">
														@if (!empty($product->discount))
															<span class="priceOld"><del>{{ number_format($product->price, 0, ',', '.') }}</del></span>
															<b class="priceDiscount">{{ number_format($product->discount, 0, ',', '.') }} VNĐ</b>
														@elseif (empty($product->price))
															<span class="priceDiscount">Miễn phí</span>
														@else
															<b class="priceDiscount">{{ number_format($product->price, 0, ',', '.') }} VNĐ</b>
														@endif
													</p>
													<div class="portfolio-attr"> 
														<a class="btn" href="{{ route('product', ['post_slug' => $product->slug]) }}">Chi tiết</a>
														<a class="btn btnOrange" href="{{ route('productDemo', ['post_slug' => $product->slug]) }}">Xem Demo</a>
													</div>
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div>
								<div class="show-pr">
								@if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
									{{ $products->links() }}
								@endif
								</div>
							</div>
						</div>
					</section>
					<!-- /Portfolio Subpage -->
				</div>
			</div>
			
		</div>
		<!-- /Page changer wrapper -->
	</div>
	<!-- /Main Content -->
@endsection
