@extends('vn3c.layout.site')

@section('type_meta', 'article')
@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', asset($product->image) )
@section('meta_url', route('product', [ 'post_slug' => $product->slug]) )

@section('content')
    <!-- Main Content Pages -->
	<div class="content-pages pdTop30">
		<!-- Subpages -->
		<div class="sub-home-pages pd20">
			<!-- Titlebar -->
			<div id="titlebar">
				<div class="container">
					<div class="special-block-bg">
						<h2>{{ $product->title }}</h2>
						<!-- Breadcrumbs -->
						<nav class="clearfix" id="breadcrumbs">
							<?php \App\Entity\Post::getBreadcrumb($product->post_id, 'san-pham')?>
						</nav>
					</div>
				</div>
			</div>
			<!-- /Titlebar -->

			<!-- Content -->
			<div class="container detail-container">
				<div class="section">
					<div class="row theme-intro">
						<div class="col-lg-5">
							<h1>Template, Giao diện mẫu <span>{{ $product->title }}</span></h1>
							<div class="desc">
								{{ $product->description }}
							</div>
							<div class="other-info clearfix">
								<span class="price float-left ">
									@if (!empty($product->discount))
										<span class="priceOld"><del>{{ number_format($product->price, 0, ',', '.') }}</del></span>
										<b class="priceDiscount">{{ number_format($product->discount, 0, ',', '.') }} VNĐ</b>
									@elseif (empty($product->price))
										<span class="priceDiscount">Miễn phí</span>
									@else
										<b class="priceDiscount">{{ number_format($product->price, 0, ',', '.') }} VNĐ</b>
									@endif
								</span>
							</div>
							<div class="Gift">
								<p> <i class="fas fa-gift"></i> Miễn phí <span class="red">3 tháng</span> sử dụng hosting.</p>
								<p> <i class="fas fa-gift"></i> Tặng tên miền quốc tế <span class="red">(280.000 đ)</span> khi đăng ký theme trên 3.000.000 đ. </p>
								<p> <i class="fas fa-gift"></i> Miễn phí hệ thống <span class="red">quản trị website</span> .</p>
								<p> <i class="fas fa-gift"></i> Miễn phí Tự động <span class="red">comment (bình luận)</span> bài viết, sản phẩm.</p>
								<img src="{{ asset('assets/vn3ctheme/img/qua_tang.png') }}" class="giftBox">
							</div>
							<div class="theme-action">
								<a href="dang-ky.php" data-toggle="modal" data-target="#registerTheme" class="install-theme">
									ĐẶT HÀNG WEBSITE
								</a>
								<a href="/view/{{ $product->slug }}" class="view-demo action-preview-theme" data-url="/view/{{ $product->slug }}">XEM TRƯỚC GIAO DIỆN</a>
							</div>
						</div>
						<div class="col-lg-7 theme-image">
							<div class="image-desktop d-none d-md-block">
								<a class="action-preview-theme">
									<img src="{{ asset($product->image) }}">
								</a>
							</div>
							<div class="image-mobile">
								<a class="action-preview-theme" >
									<img src="{{ asset($product->image) }}">
								</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="text-block pt-50">
								{!! $product->content !!}
							</div>
							<div class="function Highlights">
								<h3>Đặc điểm nổi bật của website</h3>
								<div class="info">
									<p><i class="fas fa-check-circle"></i> Thiết kế giao diện mobile responsive hoàn toàn - Hệ thống quản trị nội dung (CMS) - Tối ưu máy tìm kiếm (SEO)</p>
									<p><i class="fas fa-check-circle"></i> Tích hợp tính năng chia sẻ qua mạng xã hội, hotline, chát skype trực tuyến,...</p>
									<p><i class="fas fa-check-circle"></i> Giao diện quản trị chuyên nghiệp, bạn không cần phải am hiểu về lĩnh vực CNTT cũng có thể quản trị dễ dàng</p>
									<p><i class="fas fa-check-circle"></i> Quản trị tự động chăm sóc khách hàng bằng email không cần sử dụng bất kỳ bên thứ 3.</p>
									<p><i class="fas fa-check-circle"></i> Có hệ thống quản trị trên mobile, dễ dàng phát triển thương hiệu,...</p>
									<p><i class="fas fa-check-circle"></i> ... Và hơn thế nữa</p>
								</div>
							</div>

							<div class="function mb30">
								<h3>Chúng tôi cam kết</h3>
								<div class="info">
									<p><i class="fas fa-check-circle"></i> Thiết kế website theo đúng thiết kế.</p>
									<p><i class="fas fa-check-circle"></i> Cài đặt website cho khách hàng.</p>
									<p><i class="fas fa-check-circle"></i> Đảm bảo tiến độ, sửa lỗi nếu có.</p>
									<p><i class="fas fa-check-circle"></i> Đảm bảo bảo mật ở mức cơ bản.</p>
								</div>
							</div>

							<div class="function mb30">
								<h3>Bảo hành</h3>
								<div class="info">
									<p><i class="fas fa-check-circle"></i> Bảo hành, bảo trì website trọn đời.</p>
									<p><i class="fas fa-check-circle"></i> Bảo mật tuyệt đối thông tin, dữ liệu lưu trữ trên hosting bên chúng tôi.</p>
									<p><i class="fas fa-check-circle"></i> Hỗ trợ khách hàng miễn phí trong quá trình sử dụng website.</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="block-centered pt-50"> 
								<img class="img-responsive" src="img/portfolio-single-img-2.png" alt="">
							</div>
						</div>
					</div>
					<div class="row create-website">
						<div class="col-12 col-lg-8 intro">
							<h4>Bạn chưa chọn được giao diện? Hãy đăng ký tạo website cho thương hiệu riêng của mình</h4>
						</div>
						<div class="col-12 col-lg-4">
							<a href="" data-toggle="modal" data-target="#registerTheme" class="btn-registration">TẠO GIAO DIỆN RIÊNG</a>
						</div>
					</div>
				</div>
				<div class="related-projects pb-50 pt-50">
					<h4>XEM THÊM CÁC MẪU GIAO DIỆN CÙNG NGÀNH NGHỀ</h4>
					<div class="row portfolio-grid pt-30">
						@foreach(\App\Entity\Product::relativeProduct($product->slug, $product->product_id, 9) as $id => $productRelative)
						<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 portfolio-item">
							<div class="column">
								<a href="{{ route('product', ['post_slug' => $productRelative->slug]) }}" class="portfolio-img">
									<img src="{{ asset($productRelative->image) }}" class="img-responsive" alt="{{ $productRelative->title }}">
								</a>
								<div class="portfolio-data">
									<h4><a href="{{ route('product', ['post_slug' => $productRelative->slug]) }}">{{ $productRelative->title }}</a></h4>
									<p class="price">
										@if (!empty($productRelative->discount))
											<span class="priceOld"><del>{{ number_format($productRelative->price, 0, ',', '.') }}</del></span>
											<b class="priceDiscount">{{ number_format($productRelative->discount, 0, ',', '.') }} VNĐ</b>
										@elseif (empty($productRelative->price))
											<span class="priceDiscount">Miễn phí</span>
										@else
											<b class="priceDiscount">{{ number_format($productRelative->price, 0, ',', '.') }} VNĐ</b>
										@endif
									</p>
									<div class="portfolio-attr"> 
										<a class="btn" href="{{ route('product', ['post_slug' => $productRelative->slug]) }}">Chi tiết</a>
										<a class="btn btnOrange" href="{{ route('productDemo', ['post_slug' => $productRelative->slug]) }}">Xem Demo</a>
									</div>
								</div>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			<!-- #content -->
		</div>
		<!-- /Page changer wrapper -->
	</div>
	<!-- /Main Content -->
	
	<!-- login -->
    <div class="modal fade modal-lg" id="registerTheme" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content login">
                <div class="modal-header">
                    <h3 class="modal-title uppercase" id="exampleModalLabel">Đặt hàng WEBISTE <br/> Giao diện mẫu <span>{{ $product->title }}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body form-bg">
                    <form action="{{ route('sub_contact') }}" method="post" onSubmit="return contact(this);" id="createWebsite">
                        {!! csrf_field() !!}
						<input type="hidden" name="is_json" class="form-control captcha" value="1" placeholder="">
						<div class="row">
							<div class="col-3">
								<input disabled placeholder="http://"/>
							</div>
							
							<div class="col-6">
								<input type="text" name="address" value="{{ old('domain') }}" placeholder="Tên cửa hàng" required/>
							</div>
							<div class="col-3">
								<input disabled placeholder=".vn3c.net"/>
							</div>
						</div>
						<input type="text" id="exampleInputEmail1" name="name" aria-describedby="emailHelp" placeholder="Nhập họ và tên của bạn">
						<input type="email" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Nhập email của bạn">
						<input type="number" id="exampleInputEmail1" name="phone" aria-describedby="emailHelp" placeholder="Số điện thoại">
						<input type="hidden" id="exampleInputEmail1" name="message" value="{!! $product->title !!} {!! $product->code !!} ">

                        <div class="form-group mgTop20">
                            <button type="submit" class="btn btn-submit" onclick="return clickRegister(this);">
							Đăng Ký Sử Dụng
							</button>
                        </div>
                    </form>
					<script>
						function clickRegister(e) {
							var btn = $(e).html("Đang trong quá trình tạo website...");
							$(e).attr('style', 'background: #5a6268;');
							$(e).prop('disabled', true);
							
							$('#createWebsite').submit();
						}
					</script>
                </div>
            </div>
        </div>
    </div>
@endsection
