@extends('vn3c.layout.site')

@section('title','Đăng nhập')
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])

@section('content')

   <section id ="showLogin" class="bannerLarge 70vh mgt80">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-12 mgt50" >
				@if (empty($userLogins))
				<div class="titleLarge textLeft textUpper">
						<h1 class="fw7 f24 md-f28 sm-f20 col-f16 top25 mgb20 white">
							<font color="orange">MOMA</font> WEBSITE TỰ ĐỘNG HÓA CHĂM SÓC KHÁCH HÀNG
						</h1>
					</div>
					<div class="titleSmall textLeft mgb30">
						<h5 class=" md-f14 sm-f12 col-f11 fw4 f16 white">MOMA <font color="orange">Giúp bạn kết nối </font> và chia sẻ <font color="orange">với khách hàng</font> trong công việc kinh doanh của bạn!</h5>
					</div>
				@endif
				<h3 class="uppercase white">Tài khoản đăng nhập gần đây</h3>
				@if (!empty($userLogins))
                <ul class="listLogin">
					@foreach ($userLogins as $userLogin)
					<li onclick="return getLoginedMoma(this);"><a href="#" class="row">
						<div class="col-md-2"><img src="{{ !empty($userLogin['image']) ? asset($userLogin['image']) : asset('image/avatar_admin.png') }}"/></div>
						<div class="col-md-10">
							<h4 class="url_logined">{{ $userLogin['url'] }}</h4>
							<p class="email_logined">{{ $userLogin['email'] }}</p>
						</div>
					</a></li>
					@endforeach
				</ul>
				@endif
				<!-- <img class="mgt30 mgb30" src="{{ asset('business/image/benefits-control_1x.png') }}" style="width:100%" /> -->
            </div>
			<div class="col-md-7 col-12 mg">
				<div class="inBanner pd9p">
					
					<div class="home-buttons textCenter">
					<form action="" method="post" id="formLoginMoma">
						
						<div class="form-group">
							<div class="input-group">
								<input type="email" class="form-control" value="{{ old('email') }}" id="momaEmail1" aria-describedby="emailHelp" name="email" value="{{ old('email') }}" placeholder="Thông tin tài khoản">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
							</div>
							
						</div>
						<div class="form-group">
							<div class="input-group">
								<input type="password" class="form-control" id="momaPassword" name="password" placeholder="Mật khẩu">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
							</div>
							@if ($errors->has('password'))
								<span class="help-block white">
									<strong >{{ $errors->first('password') }}</strong>
								</span>
							@endif
							@if ($errors->has('email'))
								<span class="help-block white">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
					  <!-- <div class="form-check">
						<input type="checkbox" class="form-check-input" id="exampleCheck1">
						<label class="form-check-label" for="exampleCheck1">Duy trì đăng nhập</label>
					  </div> -->
					  <button  type="submit" onclick="return loginMoma(this);" class="bt-submit btBlue md-f18 sm-f14Im col-f9">ĐĂNG NHẬP QUẢN LÝ WEBSITE</button>
					</form>
					
				
					<!--<input type="text" placeholder="Thông tin tài khoản"/>
					<input type="password" placeholder="Mật khẩu"/>
					<input type="submit" class="bt-submit md-f18 sm-f14Im col-f9" value="Tiếp tục truy cập Website"/>
					<a href="trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="bt-submit md-f18 sm-f14Im col-f9"><i class="lnr lnr-briefcase"></i>
						Tạo website bán hàng miễn phí
					</a> -->
				</div>
				<div class="textCenter mgt30 white"><a href="http://moma.vn/admin/password/reset">Quên mật khẩu?</a></div>
				<div class="textCenter white mgb30 home-buttons mgt30 bortwhite">
					<a href="trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="bt-submit md-f18 btGreen sm-f14Im col-f9 uppercase">Tạo website miễn phí</a>
				</div>
				
                </div>
			</div>
        </div>
    </div>
</section>
@endsection
