<!DOCTYPE html>
<html lang="vi">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" 					content="width=device-width, initial-scale=1.0" />   
    <meta http-equiv="Content-Type" 		content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
	<meta name="title" 						content="@yield('title')" />
    <meta name="description" 				content="@yield('meta_description')" />
    <meta name="keywords" 					content="@yield('keywords')" />
	<meta name="theme-color"				content="#07a0d2" />
	<meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
	
	<link rel="canonical" href="@yield('meta_url')"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <meta name="google-site-verification" content="RYNcHP0p0DC3jEmH4EuJTdIEBN9VnG-gLGtFMrWPoRs" />-->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/bootstrap.min.css') }}" type="text/css">
    <!-- Font icons -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/font-awesome.min.css') }}" type="text/css">
 
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/owl.carousel.css') }}" type="text/css">
    <!-- Light Case -->
	   <!-- Animation -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/lightcase.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/extra.css') }}" type="text/css">
    <!-- Template style -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/main.css') }}" type="text/css">
    <link rel="stylesheet" 	type="text/css" media="screen" 	href="/public/xhome1/css/style.css" />
	<link rel="stylesheet" 	type="text/css" media="screen" 	href="/public/xhome1/css/slick.css" />
	<link rel="stylesheet" 	type="text/css" media="screen" 	href="/public/xhome1/css/slick-theme.css"/ >
    <!-- End Facebook Pixel Code -->
	<!--JS Files-->
		<script src="{{ asset('assets/vn3ctheme/js/jquery.min.js') }}"></script>	
		
		<script src="{{ asset('assets/vn3ctheme/js/bootstrap.min.js') }}"></script>
		<!--Owl Coursel-->
		<script src="{{ asset('assets/vn3ctheme/js/owl.carousel.min.js') }}"></script>
		<!-- Typing Text -->
		<!-- <script src="assets/vn3ctheme/js/typed.js"></script> -->
		<!--Images LightCase-->
		<script src="{{ asset('assets/vn3ctheme/js/lightcase.js') }}"></script>
		<!-- Portfolio filter -->
		<script src="{{ asset('assets/vn3ctheme/js/jquery.isotope.js') }}"></script>
		<!-- Wow Animation -->
		<script src="{{ asset('assets/vn3ctheme/js/wow.js') }}"></script>
		<!-- Map -->
		<script src="https://maps.google.com/maps/api/js?key=AIzaSyBkdsK7PWcojsO-o_q2tmFOLBfPGL8k8Vg&amp;language=en"></script>
		<!-- Main Script -->
		<script src="{{ asset('assets/vn3ctheme/js/main.js') }}"></script>
			<script src="/public/xhome1/js/jquery.sticky-kit.js"></script>
		<script src="/public/xhome1/js/iscroll.min.js"></script>
		<script src="/public/xhome1/js/slick.min.js"></script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W5NSMND');</script>
	<!-- End Google Tag Manager -->
    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token = $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: 'subcribe-email',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: 'dat-hang',
                data: data,
                success: function(result) {
                    var obj = jQuery.parseJSON(result);
                    window.location.replace("gio-hang");
                },
                error: function(error) {
                    console.log(error);
                }

            });

            return false;
        }

        function contact(e) {
            var $btn = $(e).find('button').button('loading');
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: 'submit/contact',
                data: data,
                success: function(result) {
                    var obj = jQuery.parseJSON(result);
                    /* gửi thành công */
                    if (obj.status == 200) {
                        alert(obj.message);
                        $btn.button('reset');

                        location.reload();
                        return;
                    }

                    /* gửi thất bại */
                    if (obj.status == 500) {
                        alert(obj.message);
                        $btn.button('reset');

                        location.reload();
                        return;
                    }
                },
                error: function(error) {
                    /* alert('Lỗi gì đó đã xảy ra!') */
                }

            });

            return false;
        }
		
		function loginMoma(e) {
            var email = $(e).parent().find('#momaEmail1').val();
				  
			$.ajax({
				type: "get",
				url: '/infor-website',
				data: {
					email: email,
				},
				success: function(data) {
					var obj = jQuery.parseJSON(data);

					console.log(obj);
					$('#formLoginMoma').removeAttr("action");
					
					$('#formLoginMoma').attr('action', obj.url);
					$('#formLoginMoma').submit();
					
					return false;
				}
			});
				return false;
        }
		
		function getLoginedMoma(e) {
            var email = $(e).find('.email_logined').text();
            var url = $(e).find('.url_logined').text();
			
			$('#formLoginMoma').removeAttr("action");
			$('#formLoginMoma').attr('action', url + '/admin/login');	  
			$('#momaEmail1').val(email);
			
			return false;
        }
    </script>

	@stack('scriptHeader')
	
	{!! isset($information['google-anylic']) ? $information['google-anylic'] : '' !!}
	
	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}
	
</head>
<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5NSMND"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
        @include('vn3c.common.header')
        <!-- Phần nội dung -->
        @yield('content')
        @include('xhome.common.footer')
        <style>
            .bgmt {
                background: black;
            }
        </style>
		<!-- Your customer chat code -->
		
		<!--Start of Tawk.to Script-->
		<!--<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5e0dc5fd27773e0d832b80dc/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>-->
		<!--End of Tawk.to Script-->
		
</body>
</html>
