<header>
    <div class="headerTop bgmt" id="hideHeader">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 site-branding col-xs-12">
                    <div class="logoHeader pdt10 pdb10">
                        <a href="/" title="logo">
                            <img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 white">
                    <div class="row">
                        <div class="contactHeaders inBlock col-md-3">
                            <div class="contactHeader">
                                <i class="fas fa-phone-alt"></i>
                            </div>
                            <div class="header-info">
                                <span class="phone">{{ isset($information['so-dien-thoai']) && !($information['so-dien-thoai'] == '097.4xxx.xxx') ? $information['so-dien-thoai']  : $phoneUser }}</span>
                            </div>
                        </div>
                        <div class="contactHeaders inBlock col-md-9">
                            <div class="contactHeader">
                                <i class="fas fa-home"></i>
                            </div>
                            <div class="header-info">
                                <span class="phone">{{ isset($information['dia-chi-dau-trang']) ?  $information['dia-chi-dau-trang'] : '' }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="navigation gray-bg" id="myHeader">
        <div class="container">
            <div class="row">
                <ul class="nav col-md-10">
                    @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
                        @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
                            <li>
                                <a class="nav-link textUpper black fw7 colorhv" href="{{ $menuElement['url'] }}" class="black fw7">{{ $menuElement['title_show'] }}</a>
                            </li>
                        @endforeach
                    @endforeach
                </ul>
                <div class="col-md-2 nav-item " id="search-form">
                    <a style="cursor: pointer;" >
                        <form method="get" action="/tim-kiem">
                            <input id="search_input" value="{{ isset($word) ? $word : '' }}" type="text" name="word" placeholder="Tìm kiếm...">
                        </form>
                    </a>
                </div>
            </div>


            <script type="text/javascript">

                $('#search_input').mouseout(function() {
                    if($(this).val() != "" ){
                        $(this).parent().submit();
                        console.log($(this).val())
                    }
                });

            </script>
        </div>
    </nav>
</header>
<button class="openbtn" onclick="openNav()">☰</button>
<div id="mySidebar" class="sidebar" style="width:0px">
    <a href="javascript:void(0)" class="closebtn noBorder" onclick="closeNav()">×</a>
    <ul>
        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
            @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
                <li>
                    <a class="nav-link textUpper black fw7 colorhv" href="{{ $menuElement['url'] }}" class="black fw7">{{ $menuElement['title_show'] }}</a>
                </li>
            @endforeach
        @endforeach
    </ul>
    <script>
        function changeToggle(e) {
            if($(e).hasClass('fa-plus-square')) {
                $(e).removeClass('fa-plus-square');
                $(e).addClass('fa-minus-square');
            } else {
                $(e).addClass('fa-plus-square');
                $(e).removeClass('fa-minus-square');
            }

        }
        $(document).ready(function(){
            $(".plusToggle").click(function(){
                $(this).parent().parent().find('.subMenu').toggle();

                return false;
            });
            $(".plusToggle2").click(function(){
                $(this).parent().parent().find('.subMenu2').toggle();

                return false;
            });
        });
    </script>


</div>
<style>


    .sticky {
        position: fixed;
        top: 0;
        width: 100%;
        padding-top: 0;
        padding-bottom: 0;
        background-color: #2125298a;
        color: white;
    }
    .sticky a{
        color: white
    }
    .sticky + .content {
        padding-top: 102px;
    }
</style>
<script>
    window.onscroll = function() {myFunction()};
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }
</script>
