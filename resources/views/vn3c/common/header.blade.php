<header class="hideOnTable bgrF1 menu fixed z999 shadow">
    <div class="row mgr0">
        <div class="col-lg-2 textCenter bgrWhite mg xl-mg xl-pdt13 xl-pdb13 xl-pdr0 lg-pdt19 lg-pdb19">
            <a href="">
                <img src="{{ isset($information['logo']) ? $information['logo'] : '' }}" class="img-fluid" alt="Kho giao diện website của chúng tôi" width="50%">
            </a>
        </div>
        <div class="col-lg-7 mg">
            <ul class="nav justify-content-center">
                @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
                    @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
                        <li class="nav-item">
                            <a class="nav-link active black fw7 hvBlueD lg-f11 textUpper" href="{{ $menuElement['url'] }}">{{ $menuElement['title_show'] }}</a>
                        </li>
                    @endforeach
                @endforeach
            </ul>
        </div>
        <div class="col-lg-3 textCenter mg pdb20 pdt20 xl-pdb23 xl-pdt23 xl-pdl0">
			<a href="{{route('login_website')}}" style="cursor: pointer;color:#fff"  class="hd-br white fw7 hvBlueD lg-f11 bgrOrange pd10 radius7"><i class="lnr lnr-user mgRight7"></i>Đăng nhập</a>
            <a href="/trang/tao-moi-website{{ !empty($_SERVER['HTTP_REFERER']) ? '?utm_source='.$_SERVER['HTTP_REFERER'] : '' }}" class="hd-br white fw7 hvBlueD lg-f11 bgrOrange pd10 radius7"><i class="lnr lnr-briefcase mgRight7"></i>Đăng ký</a>
        </div>
    </div>
</header>
<header class="showOnTable">
    <div class="row">
        <div class="col-6 mg">
            <a href="">
                <img src="{{ isset($information['logo']) ? $information['logo'] : '' }}" class="img-fluid w50 sm-w75 col-w100 " alt="Kho giao diện website của chúng tôi">
            </a>
        </div>
        <div class="col-6 textRight">
            <p class="mgb0">
                <button class="btn mgt18 mgr20 sm-mgt10 sm-mgb10" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-bars f28" aria-hidden="true"></i>
                </button>
            </p>
        </div>
        <div class="col-12">
            <div class="collapse" id="collapseExample">
                <div class="pdl35 sm-pdl20">
                    <ul>
                        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
                            @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
                                <li>
                                    <a href="{{ $menuElement['url'] }}" class="black fw7">{{ $menuElement['title_show'] }}</a>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class=""></div>