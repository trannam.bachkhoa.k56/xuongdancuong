<section class="slider">
    <div id="slideHome" class="carousel slide mainSlide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach (\App\Entity\SubPost::showSubPost('slide', 8) as $id => $slide)
            <div class="carousel-item {{ ($id == 0) ? 'active' : '' }}" style="background-image: url({{ asset($slide->image) }})">
                <div class="carousel-caption  d-md-block text-slide">
                    <p data-animate-in="fadeInDown" data-animate-out="fadeOutUp" class="titleCicle">{{ $slide['tieu-de-slide'] }}</p>
                    <h4 data-animate-in="fadeInLeft" data-animate-out="fadeOutRight">{{ $slide['noi-dung-load-ben-phai-slide'] }}</h4>
                    <p data-animate-in="fadeInRight" data-animate-out="fadeOutLeft" class="except">{{ $slide['noi-dung-load-ben-trai-slide'] }}</p>
                    <button data-animate-in="fadeInUp" data-animate-out="fadeOutDown">
                        <a href="{{ $slide['duong-dan-slide'] }}">{{ $slide['noi-dung-button-slide'] }}</a>
                    </button>
                </div>
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#slideHome" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#slideHome" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery(".mainSlide").removeClass('invisible').addClass('animated fadeIn');

            var slider43 = jQuery('#slideHome'),
                animateClass;

            slider43.carousel({
                interval: 20000,
                pause: 'none'
            });
            slider43.find('[data-animate-in]').addClass('animated');

            function animateSlide(slider43) {
                slider43.find('.active').find('[data-animate-in]').each(function () {
                    var $this = jQuery(this);
                    animateClass = $this.data('animate-in');
                    $this.addClass(animateClass)
                });

                slider43.find('.active').find('[data-animate-out]').each(function () {
                    var $this = jQuery(this);
                    animateClass = $this.data('animate-out');
                    $this.removeClass(animateClass)
                });
            }

            function animateSlideEnd(slider43) {
                slider43.find('.active').find('[data-animate-in]').each(function () {
                    var $this = jQuery(this);
                    animateClass = $this.data('animate-in');
                    $this.removeClass(animateClass)
                });
                slider43.find('.active').find('[data-animate-out]').each(function () {
                    var $this = jQuery(this);
                    animateClass = $this.data('animate-out');
                    $this.addClass(animateClass)
                });
            }

            animateSlide(slider43);

            slider43.on('slid.bs.carousel', function () {
                animateSlide(slider43);
            });
            slider43.on('slide.bs.carousel', function () {
                animateSlideEnd(slider43);
            });

            jQuery(document).ready(function () {
                if (Modernizr.touch) {
                    slider43.find('.carousel-inner').swipe({
                        swipeLeft: function () {
                            jQuery(this).parent().carousel('next');
                        },
                        swipeRight: function () {
                            jQuery(this).parent().carousel('prev');
                        },
                        threshold: 30
                    })
                }
            });
        });
    </script>
</section>