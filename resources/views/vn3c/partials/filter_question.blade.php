<div id="list-question">
<ul class="list-group">
	@foreach($posts as $post)
		  <li class="list-group-item">
			<div class="row">
				<div class="col-md-4">
					<img src="{{$post->image}}" />
				</div>
				<div class="col-md-8">
					<a href="hoi-dap/{{$post->slug}}">
					{{$post->title}}
					</a>
					<p>
					{{ App\Ultility\Ultility::textLimit($post->description ,60)}}
					</p>
				</div>
			</div>			 		  
		  </li>
	@endforeach
</ul>
{{$posts->links()}}
</div>