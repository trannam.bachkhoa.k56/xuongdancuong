<footer id="footer-section-6" class="footer-section grey-section">
    <div class="section-container">
        <div class="container">
            <div class="row">
                <div class="col-md-6 widget-block text-widget-block">
                    <div class="widget-block-container">
                        <div class="widget-block-title">
                            <h4>Thông tin của chúng tôi</h4>
                        </div>
                        <div class="widget-block-content">
                            <strong>EMAIL</strong>
                            <p>Support: <a href="mailto:namcuong.oleco@gmail.com" title="Support">namcuong.oleco@gmail.com</a>
                            </p>
                            <strong>PHONE</strong>
                            <p>0338.157.256</p>
                            <strong>Địa điểm</strong>
                            <p>146 Tây Sơn thi tran Phùng Đan Phượng Hà Nội.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 widget-block gmap-widget-block" id="registerGetfly">
                    <div class="widget-block-container">
                        <div class="widget-block-title">
							<h3 style="text-align: center;"><i class="fa fa-download" aria-hidden="true"></i></h3>
                            <h4 style="text-align: center;">ĐIỀN ĐẦY ĐỦ THÔNG TIN ĐỂ TẢI ĐƠN HÀNG MỚI NHẤT </h4>
                        </div>
                        <div id="getfly-optin-form-iframe-1545563217529"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=OXpOUS5Rj1nMhqyK2I8bV1Z0P8uLcb03GMpUw8Nch1Ejj4aRQA&referrer="+r); f.style.width = "100%";f.style.height = "230px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1545563217529");s.appendChild(f); })(); </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
