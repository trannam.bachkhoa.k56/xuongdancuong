<div id="SECTION496" class="widget-section ladi-drop lazy-hidden ui-droppable" lp-type="widget_section"
     lp-widget="widget" lp-lang="SECTION" lp-display="block">
    <div class="container">
        <div id="PARAGRAPH497" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><h6 class="widget-content" lp-node="h6">Máy rửa xe áp lực cao -
                Chính hãng Lutian LT303B được sản xuất theo tiêu công nghệ hàng đầu thế giới.</h6></div>
        <div id="HEADLINE498" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h2 class="widget-content" lp-node="h2">CHÍNH SÁCH SẢN PHẨM VÀ
                BẢO HÀNH</h2></div>
        <div id="HEADLINE499" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h6 class="widget-content" lp-node="h6">KHUYẾN MÃI 35% Ngày Hôm
                Nay</h6></div>
        <div id="SHAPE500" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE" lp-display="block">
            <div class="widget-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24"
                     fill="rgba(255,87,34,1)">
                    <path d="M20.39,19.37L16.38,18L15,22L11.92,16L9,22L7.62,18L3.61,19.37L6.53,13.37C5.57,12.17 5,10.65 5,9A7,7 0 0,1 12,2A7,7 0 0,1 19,9C19,10.65 18.43,12.17 17.47,13.37L20.39,19.37M7,9L9.69,10.34L9.5,13.34L12,11.68L14.5,13.33L14.33,10.34L17,9L14.32,7.65L14.5,4.67L12,6.31L9.5,4.65L9.67,7.66L7,9Z"></path>
                </svg>
            </div>
        </div>
        <div id="HEADLINE501" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h1 class="widget-content" lp-node="h1"></h1></div>
        <div id="PARAGRAPH502" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><p class="widget-content" lp-node="p">Nhanh tay nhận khuyến mãi
                ngay hôm nay</p></div>
        <div id="PARAGRAPH503" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><p class="widget-content" lp-node="p">Sản phẩm được bảo hành 12
                tháng theo quy định của nhà sản xuất.</p></div>
        <div id="SHAPE504" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE" lp-display="block">
            <div class="widget-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24"
                     fill="rgba(255,87,34,1)">
                    <path d="M10,17L6,13L7.41,11.59L10,14.17L16.59,7.58L18,9M12,1L3,5V11C3,16.55 6.84,21.74 12,23C17.16,21.74 21,16.55 21,11V5L12,1Z"></path>
                </svg>
            </div>
        </div>
        <div id="HEADLINE505" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h6 class="widget-content" lp-node="h6">BẢO HÀNH 12 THÁNG</h6>
        </div>
        <div id="HEADLINE506" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h6 class="widget-content" lp-node="h6">1 ĐỔI 1 TRONG 10
                NGÀY</h6></div>
        <div id="SHAPE507" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE" lp-display="block">
            <div class="widget-content">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100%"
                     height="100%" viewBox="0 0 24 24" fill="rgba(255,87,34,1)">
                    <path d="M20,11H4V8H20M20,13H13V12H20M20,15H13V14H20M20,17H13V16H20M20,19H13V18H20M12,19H4V12H12M20.33,4.67L18.67,3L17,4.67L15.33,3L13.67,4.67L12,3L10.33,4.67L8.67,3L7,4.67L5.33,3L3.67,4.67L2,3V19A2,2 0 0,0 4,21H20A2,2 0 0,0 22,19V3L20.33,4.67Z"></path>
                </svg>
            </div>
        </div>
        <div id="PARAGRAPH508" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><p class="widget-content" lp-node="p">Nếu sản phẩm lỗi chúng tôi
                cho đổi 1-1 trong 10 ngày.</p></div>
        <div id="IMAGE674" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="image" lp-lang="IMAGE"
             lp-display="block">
            <div class="widget-content">
                <div class="lp-show-image" alt="59b6e70c9dc0c6364b8a43ac/ava-1538930699.png"></div>
            </div>
            <div class="ladi-widget-overlay"></div>
        </div>
        <div id="HEADLINE675" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h6 class="widget-content" lp-node="h6">TẶNG 1 nước đầu ra
                10m</h6></div>
        <div id="SHAPE676" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE" lp-display="block">
            <div class="widget-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24"
                     fill="rgba(255,87,34,1)">
                    <path d="M5,9V21H1V9H5M9,21A2,2 0 0,1 7,19V9C7,8.45 7.22,7.95 7.59,7.59L14.17,1L15.23,2.06C15.5,2.33 15.67,2.7 15.67,3.11L15.64,3.43L14.69,8H21C22.11,8 23,8.9 23,10V10.09L23,12C23,12.26 22.95,12.5 22.86,12.73L19.84,19.78C19.54,20.5 18.83,21 18,21H9M9,19H18.03L21,12V10H12.21L13.34,4.68L9,9.03V19Z"></path>
                </svg>
            </div>
        </div>
        <div id="PARAGRAPH677" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><p class="widget-content" lp-node="p">Dây 12m đủ dùng cho mọi
                trường hợp.</p></div>
        <div id="HEADLINE682" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h6 class="widget-content" lp-node="h6">MIỄN PHÍ GIAO HÀNG TOÀN
                QUỐC</h6></div>
        <div id="SHAPE683" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE" lp-display="block">
            <div class="widget-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24"
                     fill="rgba(255,87,34,1)">
                    <path d="M3,4A2,2 0 0,0 1,6V17H3A3,3 0 0,0 6,20A3,3 0 0,0 9,17H15A3,3 0 0,0 18,20A3,3 0 0,0 21,17H23V12L20,8H17V4M10,6L14,10L10,14V11H4V9H10M17,9.5H19.5L21.47,12H17M6,15.5A1.5,1.5 0 0,1 7.5,17A1.5,1.5 0 0,1 6,18.5A1.5,1.5 0 0,1 4.5,17A1.5,1.5 0 0,1 6,15.5M18,15.5A1.5,1.5 0 0,1 19.5,17A1.5,1.5 0 0,1 18,18.5A1.5,1.5 0 0,1 16.5,17A1.5,1.5 0 0,1 18,15.5Z"></path>
                </svg>
            </div>
        </div>
        <div id="PARAGRAPH684" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><p class="widget-content" lp-node="p">Chúng tôi hợp tác chiến
                lược cùng Viettel post mang sản phẩm đến tay người dùng nhanh nhất, an toàn nhất.</p></div>
        <div id="HEADLINE685" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h6 class="widget-content" lp-node="h6">NHẬN HÀNG, DÙNG THỬ,
                THANH TOÁN TẠI NHÀ</h6></div>
        <div id="SHAPE686" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE" lp-display="block">
            <div class="widget-content">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24"
                     fill="rgba(255,87,34,1)">
                    <path d="M11.8,10.9C9.53,10.31 8.8,9.7 8.8,8.75C8.8,7.66 9.81,6.9 11.5,6.9C13.28,6.9 13.94,7.75 14,9H16.21C16.14,7.28 15.09,5.7 13,5.19V3H10V5.16C8.06,5.58 6.5,6.84 6.5,8.77C6.5,11.08 8.41,12.23 11.2,12.9C13.7,13.5 14.2,14.38 14.2,15.31C14.2,16 13.71,17.1 11.5,17.1C9.44,17.1 8.63,16.18 8.5,15H6.32C6.44,17.19 8.08,18.42 10,18.83V21H13V18.85C14.95,18.5 16.5,17.35 16.5,15.3C16.5,12.46 14.07,11.5 11.8,10.9Z"></path>
                </svg>
            </div>
        </div>
        <div id="PARAGRAPH687" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><p class="widget-content" lp-node="p">Là sản phẩm siêu bền và
                chính hãng lutian chúng tôi cho phép người mua được dùng thử và thanh toán tại nhà.</p></div>
    </div>
    <div class="ladi-widget-overlay"></div>
</div>
<style>
    #IMAGE674 .widget-content:first-child .lp-show-image:first-child {
        background-image: url("assets/mayruaxe/images/s700x700/59b6e70c9dc0c6364b8a43ac/ava-1538930699.png");
    }

</style>