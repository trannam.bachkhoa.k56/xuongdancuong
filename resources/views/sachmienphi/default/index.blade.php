@extends('sachmienphi.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')

  <div class="wrp cnt bSe" style="display: none">
        <div class="awr"></div>
    </div>
		
    <div class="tve_wrap_all" id="tcb_landing_page">
        <div class="tve_post_lp tve_lp_tcb2-corp-webinar-registration-page tve_lp_template_wrapper" style="">
            <div id="tve_flt" class="tve_flt">
                <div id="tve_editor" class="tve_shortcode_editor">
                    <div class="thrv_wrapper thrv-page-section tcb-window-width" style="width: 1365px; left: -112.508px;" data-css="tve-u-15dea135089">
                        <div class="tve-page-section-out" data-css="tve-u-15dea12db0a"></div>
                        <div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-15dea12c634">
                            <div class="thrv_wrapper tve_image_caption" data-css="tve-u-15dea13298b" style=""><span class="tve_image_frame" style="width: 100%;">
                                <a href="#">
                                    <img class="tve_image wp-image-74" alt="" width="256" height="88" 
                                    title="sach-mien-phi-sa" data-id="74" 
                                    src="{{ isset($information['logo']) ? $information['logo'] : ''}}" style="width: 100%;"><span class="tve-image-overlay" data-css="tve-u-161617df075"></span></a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="thrv_wrapper thrv-page-section tcb-window-width" style="width: 1365px; left: -112.508px;" data-css="tve-u-15dea13df9b">
                        <div class="tve-page-section-out" style="background-image: linear-gradient(rgba(91, 58, 27, 0.66), rgba(91, 58, 27, 0.66)),
        url('{{ isset($information['background-1']) ? $information['background-1'] : ''}}') !important;" >
                            <svg width="0" height="0" class="tve-decoration-svg">
                                <defs></defs>
                            </svg>
                        </div>
                        <div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-15dea13cb43">
                            <div class="thrv_wrapper thrv-columns" style="" data-css="tve-u-15dea16e2c2">
                                <div class="tcb-flex-row tcb-medium-wrap tcb--cols--1" data-css="tve-u-15dea15e30f">
                                    <div class="tcb-flex-col">
                                        <div class="tcb-col tve_empty_dropzone" style="">
                                            <div class="thrv_wrapper thrv_heading" style="" data-css="tve-u-15dea17b951" data-tag="h1">
                                                <h1 data-css="tve-u-15dea153329" style="text-align: center;"><span data-css="tve-u-161614a934d" style="font-family: &quot;Roboto Condensed&quot;; font-weight: 300;">
													{{ isset($information['headding-1']) ? $information['headding-1'] : ''}}
                                                </span></h1></div>
                                            <div class="thrv_wrapper thrv_text_element tve_empty_dropzone" style="" data-css="tve-u-15dea1ed2a7">
                                                <p data-css="tve-u-15dea178cf7" style="text-align: center;">
                                                	{{ isset($information['title-1']) ? $information['title-1'] : ''}}
                                                </p>
                                            </div>
                                            <div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" style="" data-css="tve-u-15dec28737c">
                                                <div class="tve-content-box-background" data-css="tve-u-15dec276b6d" data-clip-id="f438b71825662">
                                                    <svg width="0" height="0" class="tve-decoration-svg">
                                                        <defs></defs>
                                                    </svg>
                                                </div>
                                                <div class="tve-cb tve_empty_dropzone">
                                                    <div class="thrv_wrapper thrv_text_element tve_empty_dropzone" style="">
                                                        {!! isset($information['content-1']) ? $information['content-1'] : ''!!}
                                                    </div>
                                                    <div class="thrv_wrapper thrv-button" data-css="tve-u-1616150ae2c" style="" data-tcb_hover_state_parent="">
                                                        <a href="#download" class="tcb-button-link" rel="">
                                                            <span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-15dea1b0112"><span data-css="tve-u-16161754e3a">{!! isset($information['title-button-1']) ? $information['title-button-1'] : 'DOWNLOAD SÁCH'!!}</span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="thrv_wrapper thrv-page-section tcb-window-width" style="width: 1365px; left: -112.5px;" data-css="tve-u-15dec2e40c0" id="download">
                        <div class="tve-page-section-out" style=" background-image: linear-gradient(rgba(255, 255, 255, 0.88), rgba(255, 255, 255, 0.88)),
        			url('{{ isset($information['background-2']) ? $information['background-2'] : ''}}') !important;">
        	
        			</div>
                        <div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-16162329f35">
                            <div class="thrv_wrapper thrv-columns" style="" data-css="tve-u-15dec3dddcd">
                                <div class="tcb-flex-row tcb-resized tcb--cols--2" data-css="tve-u-161623a49b2">
                                    <div class="tcb-flex-col" data-css="tve-u-15dec2f80dc" style="">
                                        <div class="tcb-col tve_empty_dropzone" style="">
                                            <div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-161623662f8">
                                                <div class="tve-content-box-background" data-css="tve-u-161624b3ce5"></div>
                                                <div class="tve-cb tve_empty_dropzone" data-css="tve-u-161624ca116">
                                                    <div class="thrv_wrapper thrv_text_element tve_empty_dropzone" style="" data-css="tve-u-15dec3c4d03">
                                                       {!! isset($information['content-2']) ? $information['content-2'] : ''!!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
								
                                    <div class="tcb-flex-col" data-css="tve-u-15dec2f80f0" style="">
                                        <div class="tcb-col tve_empty_dropzone" style="" data-css="tve-u-16162357267">
                                            <div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-css="tve-u-1616283e138">
                                                <div class="tve-content-box-background" data-css="tve-u-1616237248e"></div>
                                                <div class="tve-cb tve_empty_dropzone" data-css="tve-u-16162361f27">
                                                    <div class="thrv_wrapper thrv_text_element tve_empty_dropzone">
                                                        <p data-css="tve-u-161623b05ec" style="text-align: center;">
                                                         {!! isset($information['title-step-1']) ? $information['title-step-1'] : ''!!}
                                                    </p>
                                                        <p data-css="tve-u-161623cba77" style="text-align: center;">  {!! isset($information['description-step-1']) ? $information['description-step-1'] : ''!!}</p>
                                                    </div>                                                
													<form method="POST" action="{{ route('check-website') }}" id="createWebsite">
														{{ csrf_field() }}
														<div class="form-group row">														
															<div class="col-md-3">
																<input class="http" disabled placeholder="http://"/>
															</div>
															<div class="col-md-9">
																<input type="text" name="domain" value="{{ old('domain') }}" 
																placeholder="Tên thương hiệu, dịch vụ" required/>
															</div>
														</div>
														<input type="hidden" name="theme_code" value="xhome" />
													
														<script>
															function changeTheme(e) {
																var theme = $(e).val();
																$('.imagePage').hide();
																$('.' + theme).show();
															}
														</script>
														<div class="form-group">
														<input  type="text" name="name_home" value="{{ old('name_home') }}" placeholder="Họ và tên" required/>
														</div>
														<div class="form-group">
														<input type="email" name="email" value="{{ old('email') }}" placeholder="Email" required/>
														</div>
														<div class="form-group">
														<input type="number" name="phone" value="{{ old('phone') }}" placeholder="Số điện thoại" required />
														</div>
														<div class="form-group">
														<input  type="password" name="password" id="inputPassword" placeholder="Mật khẩu" required/>
														</div>
														<div class="form-group">
														<input type="password" name="password_confirmation" id="inputPassword" placeholder="Nhập lại mật khẩu" required/>
														</div>														
														<div class="form-group row">
															<label for="inputPassword" class="col-sm-3 col-form-label"></label>
															<div class="col-sm-9">
																@if ($errors->has('email'))
																	<span class="help-block">
																<strong>{{ $errors->first('email') }}</strong>
															</span>
																@endif
																@if ($errors->has('name'))
																	<span class="help-block">
																<strong>{{ $errors->first('name') }}</strong>
															</span>
																@endif
																@if ($errors->has('phone'))
																	<span class="help-block">
																<strong>{{ $errors->first('phone') }}</strong>
															</span>
																@endif
																@if ($errors->has('password'))
																	<span class="help-block">
																<strong>{{ $errors->first('password') }}</strong>
															</span>
																@endif
																@if ($errors->has('errorDomain'))
																	<span class="help-block">
																<strong>{{ $errors->first('errorDomain') }}</strong>
															</span>
																@endif
															</div>
															<input type="hidden" value="{{ isset($_GET['utm_source']) ? $_GET['utm_source'] : '' }}" name="utm_source">
														</div>
														<div class="form-group row">
															<label for="inputPassword" class="col-sm-3 col-form-label"></label>														
															<div class="center">
																<button type="submit" class="btn btn-warning" onclick="return clickRegister(this);" data-loading-text="Đang trong quá trình tạo website" >{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "ĐĂNG KÝ SỬ DỤNG" }}</button>
															</div>
														</div>
														
														@if (session('submitSuccess'))
															<div class="alert alert-primary" role="alert">
															{{session('submitSuccess')}}
															</div>														
														@endif
														<script>
															function clickRegister(e) {
																var btn = $(e).html("Đang trong quá trình tạo website");
																$(e).attr('style', 'background: #5a6268;');
																$(e).prop('disabled', true);
																
																$('#createWebsite').submit();
															}
														</script>

													</form>                                                     
                                                      
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				<style>
				.http{
				    outline: none !important;
					width: 100%;
					border: 0;
					border-bottom: 1px solid #ddd;
					margin-bottom: 10px;
					transition: all .5s ease;
					font-size: 1rem;
					padding: 0.4rem;
					line-height: 1.5;
				}
				input {
					-webkit-writing-mode: horizontal-tb !important;
					text-rendering: auto;
					color: -internal-light-dark-color(black, white);
					letter-spacing: normal;
					word-spacing: normal;
					text-transform: none;
					text-indent: 0px;
					text-shadow: none;
					display: inline-block;
					text-align: start;
					-webkit-appearance: textfield;
					background-color: -internal-light-dark-color(white, black);
					-webkit-rtl-ordering: logical;
					cursor: text;
					margin: 0em;
					font: 400 13.3333px Arial;
					padding: 1px 0px;
					border-width: 2px;
					border-style: inset;
					border-color: initial;
					border-image: initial;
				}
				</style>
                    <div class="thrv_wrapper thrv-page-section tcb-window-width" style="width: 1365px; left: -112.508px;" data-css="tve-u-15dec310ce6">
			        <div class="tve-page-section-out" style=" background-image: radial-gradient(rgba(91, 58, 27, 0.9) 0%, rgb(0, 0, 0) 100%),url('{{ isset($information['background-1']) ? $information['background-1'] : ''}}') !important;">
			        	
			        </div>
                        <div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-15dec311c67">
		                        <div class="thrv_wrapper thrv-columns ">
		                        	<div class="row">
		                        		 @foreach (\App\Entity\SubPost::showSubPost('4-cau-hoi') as $question)
				                            <div class="col-md-6 item" data-css="tve-u-1616266cdfe">
				                                <div class="tcb-flex-col">
				                                    <div class="tcb-col tve_empty_dropzone">
				                                        <div class="thrv_wrapper thrv_text_element tve_empty_dropzone" data-css="tve-u-16162913c78">
				                                            <p data-css="tve-u-16162cd2164" style="text-align: center;padding: 10px 0"><span data-css="tve-u-1616260e522"><span data-css="tve-u-16162cac4f1">
				                                            	{{$question->title}}		                                   
				                                            </p>
				                                            <p data-css="tve-u-161625f14a2" style="text-align: justify;line-height: 1.5">
				                                            	{!! isset($question->content) ? $question->content : "" !!}
				                                            </p>
				                                        </div>
				                                        <div class="thrv_wrapper thrv-divider tcb-mobile-hidden" data-style="tve_sep-1" data-thickness="5" data-color="#a37c57" data-css="tve-u-1616266a89c">
				                                            <hr class="tve_sep tve_sep-1">
				                                        </div>
				                                    </div>
				                                </div>
				                                
				                            </div>
			                            @endforeach
			                       
		                            </div>
		                        
		                        </div>  

		                      	

		                         <div class="thrv_wrapper thrv-button" data-css="tve-u-16162ad1f1b" style="" data-tcb_hover_state_parent="">
	                                <a href="#download" class="tcb-button-link" rel="">
	                                    <span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-15dea1b0112">
	                                    	<span data-css="tve-u-16161754e3a">
	                                    		{!! isset($information['title-button-3']) ? $information['title-button-3'] : 'DownLoad sách' !!}
	                                    	</span></span>
	                                    </span>
	                                </a>
                            	</div>                     
                            </div>

                        </div>
                    </div>

                    <div class="thrv_wrapper thrv-page-section tcb-window-width" style="width: 1365px; left: -112.5px;" data-css="tve-u-1616269ae33" id="huongdan">
                        <div class="tve-page-section-out" data-css="tve-u-161626f5a94" style="background-image: linear-gradient(rgba(255, 244, 212, 0.86), rgba(255, 244, 212, 0.86)),
        				url({!! isset($information['background-3']) ? $information['background-3'] : '/sachmienphi/wp-content/uploads/2018/02/books-wall-sach-mien-phi-e1517774940967.jpg' !!}) !important; ">
        					
        				</div>
                        <div class="tve-page-section-in tve_empty_dropzone" data-css="tve-u-1616269030f">
                            <div class="thrv_wrapper thrv_heading" data-tag="h2">
                                <h2 class="" data-css="tve-u-161626a9983" style="text-align: center;">
								{!! isset($information['headding-2']) ? $information['headding-2'] : '' !!}
                                </h2></div>
                            <div class="thrv_wrapper thrv_text_element tve_empty_dropzone">
                               {!! isset($information['content-3']) ? $information['content-3'] : '' !!}
                            </div>
                            <div class="thrv_wrapper thrv-columns">
                                <div class="tcb-flex-row tcb--cols--2" data-css="tve-u-16162b09d8f">
                                	@foreach (\App\Entity\SubPost::showSubPost('video') as $video)
                                    <div class="tcb-flex-col">
                                        <div class="tcb-col tve_empty_dropzone">
                                            <div class="thrv_wrapper thrv_heading" data-tag="h2">
                                                <h2 data-css="tve-u-161627d4e1c" style="text-align: center; ">
                                                	<strong>
                                                	{!! isset($video['title']) ? $video['title'] : '' !!}
                                                	</strong>
                                                </h2></div>
                                            <div class="thrv_responsive_video thrv_wrapper rv_style_white_frame" data-type="youtube" data-showinfo="0" data-modestbranding="0" data-rel="0" data-url="https://youtu.be/23uZgH3kXEY">
                                                <div class="tve_responsive_video_container">
                                                    <div class="video_overlay"></div>
                                                  	{!! isset($video['description']) ? $video['description'] : '' !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                
                                </div>
                            </div>
                            <div class="thrv_wrapper thrv-button" data-css="tve-u-16162acfe19" style="" data-tcb_hover_state_parent="">
                                <a href="#download" class="tcb-button-link" rel="">
                                    <span class="tcb-button-texts"><span class="tcb-button-text thrv-inline-text" data-css="tve-u-15dea1b0112"><span data-css="tve-u-16161754e3a">
                                       {!! isset($information['title-button-4']) ? $information['title-button-4'] : 'DownLoad sách' !!}
                                	</span></span>
                                    </span>
                                </a>
                            </div>
                            <div class="thrv_wrapper thrv_contentbox_shortcode thrv-content-box" data-value-type="percent" data-css="tve-u-1616279553b">
                                <div class="tve-content-box-background" data-css="tve-u-161627826a1" data-clip-id="39801d8f6bd37">
                                    <svg width="0" height="0" class="tve-decoration-svg">
                                        <defs></defs>
                                    </svg>
                                </div>
                                <div class="tve-cb tve_empty_dropzone" data-css="tve-u-16162788a99">
                                    <div class="thrv_wrapper thrv_text_element tve_empty_dropzone">
                                        <p style="text-align: center;" data-css="tve-u-1616279ba5b">
                                        {!! isset($information['copy-right']) ? $information['copy-right'] : '' !!}
                                    </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<style>
	.tve-page-section-out{
		    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
	}
	</style>
@endsection

