<!DOCTYPE html >
<html>
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />

	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />

    <!-- Favicon
		============================================ -->

    <link rel="shortcut icon" type="image/x-icon" href="{{ isset($information['fa-vicon']) ? $information['fa-vicon'] : ''}}" />
      <!-- Bootstrap -->
    <link rel="stylesheet" href="https://moma.vn/assets/vn3ctheme/css/bootstrap.min.css" type="text/css">

    <link rel='dns-prefetch' href='http://fonts.googleapis.com' />

    <link rel='stylesheet' id='tve_style_family_tve_flt-css' href='/sachmienphi/wp-content/plugins/thrive-visual-editor/editor/css/thrive_flat.css' type='text/css' media='all' />
    
    <link rel='stylesheet' id='twentyseventeen-style-css' href='/sachmienphi/wp-content/themes/twentyseventeen/style.css' type='text/css' media='all' />

    <link rel='stylesheet' href="/sachmienphi/css.css">
    <!--[if lt IE 9]>
    <link rel='stylesheet' id='twentyseventeen-ie8-css'  href='/sachmienphi/wp-content/themes/twentyseventeen/assets/css/ie8.css?ver=1.0' type='text/css' media='all' />
    <![endif]-->

    <script type='text/javascript' src='/sachmienphi/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript' src='/sachmienphi/wp-includes/js/jquery/jquery-migrate.min.js'></script>

    <script src="https://moma.vn/assets/vn3ctheme/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
    <script type='text/javascript' src='/sachmienphi/wp-content/themes/twentyseventeen/assets/js/html5.js?ver=3.7.3'></script>
    <![endif]-->
    <style type="text/css">
        html {
            height: auto;
        }
        
        body:before,
        body:after {
            height: 0 !important;
        }
        
        .thrv_page_section .out {
            max-width: none
        }
    </style>

</head>

<script type='text/javascript'>
        /* <![CDATA[ */
        var tve_frontend_options = {
            "is_editor_page": "",
            "page_events": [],
            "is_single": "1",
            "social_fb_app_id": "",
            "translations": {
                "Copy": "Copy"
            },
            "post_id": "2"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='/sachmienphi/wp-content/plugins/thrive-visual-editor/editor/js/dist/frontend.min.js'></script>
    <script type='text/javascript' src='/sachmienphi/wp-content/themes/twentyseventeen/assets/js/jquery.scrollTo.js'></script>

<body>
    <div class="ladi-wraper-page" >
        @include('sachmienphi.common.header')

        @yield('content')

        @include('sachmienphi.common.footer')
    </div>

    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function contact(e) {
            var data = jQuery(e).serialize();
            jQuery(e).find('button[type=submit]').attr('disabled', 'disabled')
            jQuery(e).find('button[type=submit]').attr('style', 'background: gray;')
            jQuery.ajax({
                type: "POST",
                url: '{!! route('sub_contact') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    // gửi thành công
                    if (obj.status == 200) {
                        alert(obj.message);
                        jQuery(e).find('button[type=submit]').removeAttr('disabled', 'disabled')
                        jQuery(e).find('button[type=submit]').removeAttr('style')
                        return false;
                    }

                    // gửi thất bại
                    if (obj.status == 500) {
                        alert(obj.message);
                        jQuery(e).find('button[type=submit]').removeAttr('disabled', 'disabled')
                        jQuery(e).find('button[type=submit]').removeAttr('style')

                        return false;
                    }
                },
                error: function(error) {
                    //alert('Lỗi gì đó đã xảy ra!')
                    jQuery(e).find('button[type=submit]').removeAttr('disabled', 'disabled')
                    jQuery(e).find('button[type=submit]').removeAttr('style')

                    return false;
                }

            });
            return false;
        }

    </script>

</body>
</html>
