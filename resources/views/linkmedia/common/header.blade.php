<!-- Preloader -->
 <div class="preloader">
  <div class="preloader-inner">
	<div class="single-loader one"></div>
	<div class="single-loader two"></div>
	<div class="single-loader three"></div>
	<div class="single-loader four"></div>
	<div class="single-loader five"></div>
	<div class="single-loader six"></div>
	<div class="single-loader seven"></div>
	<div class="single-loader eight"></div>
	<div class="single-loader nine"></div>
  </div>
</div>
<!-- End Preloader -->

<!-- Start Header -->
<header id="header" class="header">
	<!-- Topbar -->
	<div class="topbar">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12">
					<!-- Contact -->
					<ul class="contact">
						<li><i class="fa fa-headphones"></i> 093 455 3435 - 097 456 1735</li>
						<li><i class="fa fa-envelope"></i> <a href="mailto:info@yourmail.com">info@vn3c.net</a></li>
					</ul>
					<!--/ End Contact -->
				</div>
				<div class="col-lg-6 col-12">
					<div class="topbar-right">
						<!-- Search Form -->
						<div class="search-form active">
							<a class="icon" href="#"><i class="fa fa-search"></i></a>
							<form class="form" action="#">
								<input placeholder="Search & Enter" type="search">
							</form>
						</div>
						<!--/ End Search Form -->
						<!-- Social -->
						<ul class="social">
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-behance"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube"></i></a></li>
						</ul>
						<!--/ End Social -->
					</div>
				</div>
			</div>
		</div>	
	</div>
	<!--/ End Topbar -->
	<!-- Middle Bar -->
	<div class="middle-bar">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-12">
					<!-- Logo -->
					<div class="logo">
						<a href="/"><img src="{!! isset($information['logo']) ?$information['logo']  : '' !!}" alt="logo"></a>
					</div>
					<div class="link"><a href="/"><span>V</span>n3c</a></div>
					<!--/ End Logo -->
					<button class="mobile-arrow"><i class="fa fa-bars"></i></button>
					<div class="mobile-menu"></div>
				</div>
				<div class="col-lg-10 col-12">
					<!-- Main Menu -->
					<div class="mainmenu">
						<nav class="navigation">
							<ul class="nav menu">
								<li class="active"><a href="index.html">Home<i class="fa fa-caret-down"></i></a>
									<ul class="dropdown">
										<li><a href="index.html">Homepage Default</a></li>
										<li><a href="index2.html">Homepage Onepage</a></li>
									</ul>
								</li>
								<li><a href="#">Pages<i class="fa fa-caret-down"></i></a>
									<ul class="dropdown">
										<li><a href="about-us.html">About Us</a></li>
										<li><a href="#">Our Team<i class="fa fa-caret-right"></i></a>
											<ul class="dropdown submenu">
												<li><a href="team.html">Team</a></li>
												<li><a href="team-single.html">Team Single</a></li>
											</ul>
										</li>
										<li><a href="pricing.html">Pricing</a></li>
										<li><a href="faqs.html">FAQs</a></li>
										<li><a href="404.html">404 Error</a></li>
									</ul>
								</li>
								<li><a href="#">Services<i class="fa fa-caret-down"></i></a>
									<ul class="dropdown">
										<li><a href="services.html">Services</a></li>
										<li><a href="service-single.html">Service Single</a></li>
									</ul>
								</li>	
								<li><a href="#">Portfolio<i class="fa fa-caret-down"></i></a>
									<ul class="dropdown">
										<li><a href="portfolio-masonry.html">Portfolio Masonry</a></li>
										<li><a href="portfolio-4-column.html">Portfolio 4 Column</a></li>
										<li><a href="portfolio-3-column.html">Portfolio 3 Column</a></li>
										<li><a href="portfolio-2-column.html">Portfolio 2 Column</a></li>
										<li><a href="portfolio-single.html">Portfolio Single</a></li>
									</ul>
								</li>	
								<li><a href="#">Blogs<i class="fa fa-caret-down"></i></a>
									<ul class="dropdown">
										<li><a href="#">Blogs<i class="fa fa-caret-right"></i></a>
											<ul class="dropdown submenu">
												<li><a href="blogs.html">Grid Layout</a></li>
												<li><a href="blogs-grid-left.html">Grid Left Sidebar</a></li>
												<li><a href="blogs-grid-right.html">Grid Right Sidebar</a></li>
												<li><a href="blogs-list.html">List Layout</a></li>
												<li><a href="blogs-list-left.html">List Left Sidebar</a></li>
												<li><a href="blogs-list-right.html">List Right Sidebar</a></li>
											</ul>
										</li>
										<li><a href="#">Blog Single<i class="fa fa-caret-right"></i></a>
											<ul class="dropdown submenu">
												<li><a href="blog-single-full.html">Single Full Width</a></li>
												<li><a href="blog-single-left.html">Single Left Sidebar</a></li>
												<li><a href="blog-single.html">Single Right Sidebar</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li><a href="contact.html">Contact</a></li>
							</ul>
						</nav>
						<!-- Button -->
						<div class="button">
							<a href="contact.html" class="btn">Đăng ký ngay</a>
						</div>
						<!--/ End Button -->
					</div>
					<!--/ End Main Menu -->
				</div>
			</div>
		</div>
	</div>
	<!--/ End Middle Bar -->
</header>
<!--/ End Header -->