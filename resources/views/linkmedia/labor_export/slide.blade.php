<div class="camKet">
    <div class="container h-100">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <h3 style="color: red; padding-top: 20px;"><b>OLECO</b></h3>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Cam kết 100% bay.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Công việc và thu nhập đúng với cam kết.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Thời gian học ngắn, chi phí xuất cảnh siêu rẻ.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Có nhiều đơn hàng về thực phẩm, nông nghiệp, cơ khí.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Có bất kể vấn đề nào không đúng với hợp đồng, công ty cam kết HOÀN TIỀN 100% chi phí.</p>
                <p><a class="btn btn-danger" href="#registerGetfly" style="padding: 10px 10px; font-weight: 600; font-size: 22px;">ĐĂNG KÝ NGAY TẠI ĐÂY</a></p>
            </div>
            <div class="col-xs-12 col-md-6">
                <h3 style="color: red; padding-top: 20px; text-align: center"><b>Hãy để chúng tôi tư vấn bạn</b></h3>
                <div id="getfly-optin-form-iframe-1541727665533"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=EW0FNsa9FBOCtcooexONuhuKtSczJHH3zZlsGlof4BK4Fp65W8&referrer="+r); f.style.width = "100%";f.style.height = "350px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1541727665533");s.appendChild(f); })(); </script>
            </div>
        </div>
    </div>
</div>


{{--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">--}}
    {{--<!-- Indicators -->--}}
    {{--<ol class="carousel-indicators">--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="2"></li>--}}
    {{--</ol>--}}

    {{--<!-- Wrapper for slides -->--}}
    {{--<div class="carousel-inner" role="listbox">--}}
        {{--<div class="item active">--}}
            {{--<div class="CropImg">--}}
                {{--<a href="#" class="thumbs">--}}
                    {{--<img src="{{ asset('laborExport/ex_1/img/slide/anh-doan-ket.jpg') }}" alt="halasuco ảnh đoàn kết">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="carousel-caption">--}}
                {{--...--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="item">--}}
            {{--<div class="CropImg">--}}
                {{--<a href="#" class="thumbs">--}}
                    {{--<img src="{{ asset('laborExport/ex_1/img/slide/anh-hop-cong-ty.jpg') }}" alt="halasuco ảnh họp công ty">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="carousel-caption">--}}
                {{--...--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="item">--}}
            {{--<div class="CropImg">--}}
                {{--<a href="#" class="thumbs">--}}
                    {{--<img src="{{ asset('laborExport/ex_1/img/slide/slider-2.jpg') }}" alt="halasuco slider 2">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="carousel-caption">--}}
                {{--...--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="item">--}}
            {{--<div class="CropImg">--}}
                {{--<a href="#" class="thumbs">--}}
                    {{--<img src="{{ asset('laborExport/ex_1/img/slide/slider-2.jpg') }}" alt="halasuco ảnh slider2">--}}
                {{--</a>--}}
            {{--</div>--}}
            {{--<div class="carousel-caption">--}}
                {{--...--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<!-- Controls -->--}}
    {{--<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">--}}

    {{--</a>--}}
    {{--<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">--}}

    {{--</a>--}}
{{--</div>--}}