@extends('vn3c.layout.create-theme')

@section('title','Quay thưởng')
@section('meta_description', 'Quay thưởng để trúng thưởng')
@section('keywords', '')

@section('content')
    <link rel="stylesheet" href="{{ asset('vn3c/rotate/demo.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('vn3c/css/bootstrap.css') }}" media="all" type="text/css"/>

    <script src="{{ asset('vn3c/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vn3c/rotate/awardRotate.js') }}"></script>
    <script src="{{ asset('vn3c/rotate/jquerysession.js') }}"></script>
    <script src="{{ asset('vn3c/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(function (){

            var rotateTimeOut = function (){
                $('#rotate').rotate({
                    angle:0,
                    animateTo:2160,
                    duration:8000,
                    callback:function (){
                        alert('Cảm ơn bạn đã tham gia quay thưởng！');
                    }
                });
            };
            var bRotate = false;
            var rotateFn = function (awards, angles, txt){
                bRotate = !bRotate;
                $('#rotate').stopRotate();
                $('#rotate').rotate({
                    angle:0,
                    animateTo:angles+1800,
                    duration:8000,
                    callback:function (){
                        var rotated = $.session.get('success');
                        console.log(rotated);
                        if (rotated == 1) {
                            $('#rotatePopUp').find('.modal-body').empty();
                            $('#rotatePopUp').find('.modal-body').html("Chúng tôi xin lỗi! Bạn đã hết lượt quay :(");
                            $('#rotatePopUp').modal('show');

                            return true;
                        }

                        $('#rotatePopUp').find('.modal-body').empty();
                        $('#rotatePopUp').find('.modal-body').html(txt);
                        $('#rotatePopUp').modal('show');
                        var key = 'success';
                        var value = 1;
                        $.session.set(key,value);
                        bRotate = !bRotate;
                    }
                })
            };
            $('.pointer').click(function (){

                if(bRotate)return;
                var item = rnd(0,7);
                $("#sss").html(item);
                switch (item) {
                    case 0:
                        //var angle = [26, 88, 137, 185, 235, 287, 337];
                        rotateFn(0, 337, 'Tạch rồi =))');
                        break;
                    case 1:
                        //var angle = [88, 137, 185, 235, 287];
                        rotateFn(1, 26, 'Xin chúc mừng! chúc mừng bạn đã được tặng thẻ quà tặng trị giá 1.000.000 VNĐ khi làm dịch vụ bên chúng tôi!');
                        break;
                    case 2:
                        //var angle = [137, 185, 235, 287];
                        rotateFn(2, 88, 'Xin chúc mừng! bạn được tặng một lần thiết kế BANNER miễn phí :D');
                        break;
                    case 3:
                        //var angle = [137, 185, 235, 287];
                        rotateFn(3, 137, 'Xin chúc mừng! chúc mừng bạn đã được tặng thẻ quà tặng trị giá 500.000 VNĐ khi làm dịch vụ bên chúng tôi!');
                        break;
                    case 4:
                        //var angle = [185, 235, 287];
                        rotateFn(4, 185, 'Haiz! Chúc bạn may mắn trong cuộc sống :(');
                        break;
                    case 5:
                        //var angle = [185, 235, 287];
                        rotateFn(5, 185, 'Haiz! Chúc bạn may mắn trong cuộc sống :(');
                        break;
                    case 6:
                        //var angle = [235, 287];
                        rotateFn(6, 235, 'Xin chúc mừng! chúc mừng bạn đã được tặng thẻ quà tặng trị giá 200.000 VNĐ khi làm dịch vụ bên chúng tôi!');
                        break;
                    case 7:
                        //var angle = [287];
                        rotateFn(7, 287, 'Chúc mừng bạn được tặng một thiết kế LOGO khi tham gia dịch vụ bên chúng tôi!');
                        break;
                }

                console.log(item);
            });
        });
        function rnd(n, m){
            return Math.floor(Math.random()*(m-n+1)+n)
        }
    </script>
    <section class="create-theme">

        <img src="{{ $information['anh-nen-tao-theme'] }}" alt="" class="bg-img">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12  col-12">
					<div class="right-ft leftGit">
						<div>
							<h1>Vòng quay may mắn,
								<br>chào mừng bạn đến với VN3C</h1>
						</div>
					</div>
					
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12  col-12">
					<div class="right-ft">
						<div class="turntable-bg">
                        <!--<div class="mask"><img src="images/award_01.png"/></div>-->
                        <div class="pointer"><img src="{{ asset('vn3c/rotate/pointer.png') }}" alt="pointer"/></div>
                        <div class="rotate" ><img id="rotate" src="{{ asset('vn3c/rotate/turntable.png') }}" alt="turntable"/></div>
						</div>
						<div style="text-align:center;">
							<a id="aurl" ></a>
						</div>
					</div>
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal" tabindex="-1" role="dialog" id="rotatePopUp">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chương trình quay thưởng VN3C</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
	
	<section class="giftOften">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12  border"></div>
				<div class="col-lg-12 col-md-12 col-sm-12  contentGift">
					<p>NHÂN DỊP CHÀO ĐÓN GIÁNG SINH VÀ TẾT DƯƠNG LỊCH!
					ĐỂ TRI ÂN QUÝ KHÁCH ĐÃ ỦNG HỘ GIẢI TRÍ K8， K8 RA MẮT CHƯƠNG TRÌNH KHUYẾN MÃI
					VÒNG QUAY MAY MẮN. CHÚC QUÝ KHÁCH LUÔN MAY MẮN VÀ CHÚC MỪNG NĂM MỚI!
					</p>
					<p>
						THỜI GIAN KHUYẾN MÃI : <span> 23/12/2016 ĐẾN 05/01/2017 </span>
					</p>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12  border"></div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-12 col-sm-12  ">
					<div class="box">
					   <div class="table">
						  <h3>DANH SÁCH TRÚNG THƯỞNG</h3>
						  <table width="" cellspacing="0" cellpadding="0">
							 <tbody>
								<tr>
								   <th width="90">Tên</th>
								   <th width="122">Giải Thưởng</th>
								   <th>Thời Gian</th>
								</tr>
							 </tbody>
						  </table>
						  <marquee direction="up" height="164" scrollamount="3">
							 <table width="" cellspacing="0" cellpadding="0" id="Winlist">
								<tbody>
								   <tr>
									  <td width="90">n***st</td>
									  <td width="122">8888</td>
									  <td>2017-01-26 15:07:03</td>
								   </tr>
								   <tr>
									  <td width="90">n***27</td>
									  <td width="122">38888</td>
									  <td>2017-01-26 15:04:35</td>
								   </tr>
								   <tr>
									  <td width="90">n***28</td>
									  <td width="122">18888</td>
									  <td>2017-01-25 16:29:46</td>
								   </tr>
								   <tr>
									  <td width="90">n***bc</td>
									  <td width="122">38000</td>
									  <td>2017-01-05 19:52:25</td>
								   </tr>
								   <tr>
									  <td width="90">n***85</td>
									  <td width="122">20000</td>
									  <td>2017-01-05 18:45:15</td>
								   </tr>
								   <tr>
									  <td width="90">n***30</td>
									  <td width="122">188000</td>
									  <td>2016-12-23 10:08:11</td>
								   </tr>
								</tbody>
							 </table>
						  </marquee>
					   </div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 ">
					<div class="box">
					   <div class="table">
						  <h3>LỊCH SỬ CỦA TÔI</h3>
						  <table width="" cellspacing="0" cellpadding="0">
							 <tbody>
								<tr>
								   <th width="90">Tên</th>
								   <th width="122">Giải Thưởng</th>
								   <th>Thời Gian</th>
								</tr>
							 </tbody>
						  </table>
						  <marquee direction="up" height="164" scrollamount="3">
							 <table width="" cellspacing="0" cellpadding="0" id="Winlist">
								<tbody>
								   <tr>
									  <td width="90">n***st</td>
									  <td width="122">8888</td>
									  <td>2017-01-26 15:07:03</td>
								   </tr>
								   <tr>
									  <td width="90">n***27</td>
									  <td width="122">38888</td>
									  <td>2017-01-26 15:04:35</td>
								   </tr>
								   <tr>
									  <td width="90">n***28</td>
									  <td width="122">18888</td>
									  <td>2017-01-25 16:29:46</td>
								   </tr>
								   <tr>
									  <td width="90">n***bc</td>
									  <td width="122">38000</td>
									  <td>2017-01-05 19:52:25</td>
								   </tr>
								   <tr>
									  <td width="90">n***85</td>
									  <td width="122">20000</td>
									  <td>2017-01-05 18:45:15</td>
								   </tr>
								   <tr>
									  <td width="90">n***30</td>
									  <td width="122">188000</td>
									  <td>2016-12-23 10:08:11</td>
								   </tr>
								</tbody>
							 </table>
						  </marquee>
					   </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12  border"></div>
				<div class="col-lg-12 col-md-12 col-sm-12  contentGift contentDesc">
					<p>
						NỘI DUNG:
						TÍCH LŨY TIỀN GỬI TRONG THỜI GIAN KHUYẾN MÃI, MỖI 3,000,000 VND SẼ ĐƯỢC 
						1 LẦN QUAY THƯỞNG, SỐ LẦN QUAY KHÔNG GIỚI HẠN (VÍ DỤ: QUÝ KHÁCH TÍCH 
						LŨY TIỀN GỬI LÀ 9,000,000VND SẼ CÓ 3 LẦN QUAY THƯỞNG.)
						SỐ LẦN QUAY THƯỞNG BUỘC PHẢI SỬ DỤNG HẾT TRONG NGÀY (00:00 ĐẾN 23:59),
						NẾU QUA NGÀY MỚI SẼ ĐƯỢC XEM NHƯ VÔ HIỆU LỰC. ĐỂ THỂ HIỆN SỰ CÔNG 
						BẰNG MINH BẠCH K8 SẼ CÔNG BỐ DANH SÁCH TRÚNG THƯỞNG MỖI NGÀY.
						THỜI GIAN QUAY THƯỞNG KẾT THÚC VÀO LÚC 23:59 PHÚT HÀNG NGÀY.
						GIẢI THƯỞNG MỖI NGÀY NHƯ SAU:(GIẢI THƯỞNG TIỀN MẶT YÊU CẦU 1 VÒNG CƯỢC.)
					</p>
				</div>
			</div>
		</div>
	
	</section>


@endsection

