@extends('vn3c.layout.create-theme')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('page', ['post_slug' => $post->slug]) )

@section('content')
    <section class="create-theme">

        <img src="{{ $information['anh-nen-tao-theme'] }}" alt="" class="bg-img">
        <div class="container">
            <div class="row">
                <div class="col-md-5">

                </div>
                <div class="col-md-7 right-ft">
                    <img src="{{ asset('vn3c/upload-img/bg-form.png') }}" alt="">
                    <div class="content">
                        <h1>tạo website 30s</h1>
                        <div class="form-bg">
                            <p>{{ $information['cam-on-khi-tao-website'] }}</p>
                            @if (isset($_GET['website']))
                            <p>
                                Website của bạn là:
                                <a href="{{ $_GET['website'] }}" target="_blank">
                                    {{ $_GET['website'] }}
                                </a>
                            </p>
                            <p>
                                Thay đổi nội dung của website cho đẹp bằng cách <br>
                                Truy cập quản lý nội dung của bạn bằng cách vào
                                <a href="{{ $_GET['website'] }}/admin" target="_blank">
                                    {{ $_GET['website'] }}/admin
                                </a>
                                Tài khoản đăng nhập của bạn là : email của bạn và password bạn vừa tạo.
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


