<!DOCTYPE html>
<!-- saved from url=(0059)http://slimweb.vn/vinno-slimweb2/get/field_lp_sitepage/9832 -->
<html lang="en" class="js no-touch backgroundsize csstransforms3d csstransitions" data-sbro-deals-lock="true" style="">
<!--<![endif]-->
<head>
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:type" content="article"/>

    <meta property="og:title" content="NHẬT BẢN KHÔNG PHẢI LÀ THIÊN ĐƯỜNG!" />
    <meta property="og:description" content="Nhật Bản không cho bạn những ngày tháng vô tư vô lo, ung dung tự tại…" />
    <meta property="og:url" content="http://vn3c.net/xuat-khau-lao-dong-nhat-ban/dang-ky-xuat-khau-lao-dong-nhat-ban" />
    <meta property="og:image" content="{{ asset('labor_export/xuat_khau_lao_dong_2.jpg') }}" />
    <meta property="og:image:secure_url" content="{{ asset('labor_export/xuat_khau_lao_dong_2.jpg') }}" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }
    </style>

    <style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }
    </style>
    <style type="text/css">@media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }
    </style>
    <style type="text/css">.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }</style>

    <style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }</style>
    <style type="text/css">@media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }</style>
    <style type="text/css">.gm-style .gm-style-mtc label, .gm-style .gm-style-mtc div {
            font-weight: 400
        }</style>
    <style type="text/css">.gm-style .gm-style-cc span, .gm-style .gm-style-cc a, .gm-style .gm-style-mtc div {
            font-size: 10px
        }</style>
    <style type="text/css">@media print {
            .gm-style .gmnoprint, .gmnoprint {
                display: none
            }
        }

        @media screen {
            .gm-style .gmnoscreen, .gmnoscreen {
                display: none
            }
        }</style>
    <style type="text/css">.gm-style {
            font-family: Roboto, Arial, sans-serif;
            font-size: 11px;
            font-weight: 400;
            text-decoration: none
        }

        .gm-style img {
            max-width: none
        }</style>
    <script async="" src="{{ asset('laborExport/ex_1/js/analytics.js') }}"></script>
    <title>OLECO xuất khẩu lao động nhật bản</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="OLECO chúng tôi cung cấp các giải pháp hàng đầu cho thực tập sinh về xuất khẩu lao động tại Nhật Bản.">
    <meta name="author" content="oleco">

    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminstration/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/social-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/mediaelementplayer.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/jquery.fancybox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('laborExport/ex_1/css/owl.theme.default.css') }}">

    <style>
    </style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style id="fit-vids-style">.fluid-width-video-wrapper {
            width: 100%;
            position: relative;
            padding: 0;
        }

        .fluid-width-video-wrapper iframe, .fluid-width-video-wrapper object, .fluid-width-video-wrapper embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.fancybox-margin {
            margin-right: 0px;
        }</style>
    <style type="text/css">.gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }

        .gm-style img {
            max-width: none;
        }</style>
</head>
<body data-spy="scroll" data-target=".header-menu-container" data-offset="61">
<div id="page">
    <header id="header-section-2" class="header-section header-style-2">
        <div class="header-section-container">
            <div class="header-menu">
                <div class="header-menu-container">
                    <nav class="navbar">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-md-2">
                                    <p style="text-align: center;">
                                        <img src="{{ asset('laborExport/img/oleco_logo_03.png') }}" alt="logo" width="200"/>
                                    </p>
                                </div>
                                <div class="col-xs-12 col-md-10">
                                    <div class="titleHeader">
                                        <h2 class="title" style="text-align: center;">NHẬT BẢN KHÔNG PHẢI LÀ THIÊN ĐƯỜNG!</h2>
                                        <h1 style="color: #365899; text-align: center;">
                                            Nếu bạn đã sẵn sàng thay đổi bản thân và mong muốn có cơ hội đặt chân đến đất nước Nhật Bản để sinh sống và làm việc, chúng tôi sẽ đồng hành cùng bạn
                                            </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    @include('vn3c.labor_export.slide')
    @include('vn3c.labor_export.introduction')
    @include('vn3c.labor_export.library')
    @include('vn3c.labor_export.footer')

</div>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('laborExport/ex_1/js/mobile.min.js') }}"></script>
<script src="{{ asset('laborExport/ex_1/js/owl.carousel.js') }}"></script>
<script>
    $('.introductionSlide').owlCarousel({
        margin:10,
        nav:true,
        loop: true,
        navText: false,
        dotData: true,
        items: 1,
        autoplayTimeout: 3000,
        autoplay: true
    });
</script>

</body>
</html>