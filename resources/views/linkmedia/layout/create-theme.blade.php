<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta name="ROBOTS" content="index, follow"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
    <!-- css he thong -->
    <link rel="stylesheet" href="{{ asset('vn3c/css/bootstrap.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('vn3c/css/extract.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('vn3c/stylesheet/styles.css') }}" media="all" type="text/css"/>
    <!-- tab -->
    <link rel="stylesheet" href="{{ asset('vn3c/stylesheet/styles-tab.css') }}" media="all" type="text/css"/>
    <!-- mobile -->
    <link rel="stylesheet" href="{{ asset('vn3c/stylesheet/styles-mobile.css') }}" media="all" type="text/css"/>

    <!-- script he thong -->
    <script src="{{ asset('vn3c/js/jquery.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/popper.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('vn3c/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vn3c/js/smooth-scroll.js') }}"></script>
    <script src="{{ asset('vn3c/js/modernizr.min.js') }}"></script>
    <script src="{{ asset('vn3c/js/wow.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            //cố định menu khi lăn chuột
            var s = $(".navbar");
            var pos = s.position();
            $(window).scroll(function () {
                var windowpos = $(window).scrollTop();

                if (windowpos >= pos.top) {
                    s.addClass("sticker");
                } else {
                    s.removeClass("sticker");
                }
            });

            //click vào nút lên top
            // Hide the toTop button when the page loads.
            $("#toTop").css("display", "none");

            // This function runs every time the user scrolls the page.
            $(window).scroll(function () {

                // Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
                if ($(window).scrollTop() > 0) {

                    // If it's more than or equal to 0, show the toTop button.
                    console.log("is more");
                    $("#toTop").fadeIn("slow");

                }
                else {
                    // If it's less than 0 (at the top), hide the toTop button.
                    console.log("is less");
                    $("#toTop").fadeOut("slow");
                }
            });


            // When the user clicks the toTop button, we want the page to scroll to the top.
            $("#toTop").click(function () {

                // Disable the default behaviour when a user clicks an empty anchor link.
                // (The page jumps to the top instead of // animating)
                event.preventDefault();

                // Animate the scrolling motion.
                $("html, body").animate({
                    scrollTop: 0
                }, "slow");
            });

            //thu viện wow
            wow = new WOW(
                {
                    animateClass: 'animated',
                    offset: 100,
                    callback: function (box) {
                        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
                    }
                }
            );
            wow.init();
        });

        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }
    </script>
    <head>
<body>
@include('vn3c.common.header')

<!-- Phần nội dung -->
@yield('content')

@include('vn3c.common.footer-theme')
@include('vn3c.popup.login')
@include('vn3c.popup.forget_password')
@include('vn3c.popup.register')
</body>
</html>