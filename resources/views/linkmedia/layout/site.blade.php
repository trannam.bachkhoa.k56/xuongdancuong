<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta name="ROBOTS" content="index, follow"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta itemprop="image" content="@yield('meta_image')" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:locale" content="vi_VN"/>
	<meta property="og:type" content="@yield('type_meta')"/>

    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('meta_description')" />
    <meta property="og:url" content="@yield('meta_url')" />
    <meta property="og:image" content="@yield('meta_image')" />
    <meta property="og:image:secure_url" content="@yield('meta_image')" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="@yield('meta_description')" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:image" content="@yield('meta_image')" />
	
	<link rel="shortcut icon" href="/favicon.ico">
    <!-- css he thong -->
    <link rel="stylesheet" href="{{ asset('linkmedia/css/bootstrap.min.css') }}" media="all" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('linkmedia/css/font-awesome.min.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/slicknav.min.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/cubeportfolio.min.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/magnific-popup.min.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/jquery.fancybox.min.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/niceselect.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/owl.theme.default.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/owl.carousel.min.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/slickslider.min.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/reset.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/style.css') }}" media="all" type="text/css"/>
	<link rel="stylesheet" href="{{ asset('linkmedia/css/color.css') }}" media="all" type="text/css"/>
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,800" rel="stylesheet">
	
</head>


@include('linkmedia.common.header')

<!-- Phần nội dung -->
@yield('content')

<!-- @include('linkmedia.common.footer')
@include('linkmedia.popup.login')
@include('linkmedia.popup.forget_password')
@include('linkmedia.popup.register') -->
<!-- Jquery -->
		<script src="{{ asset('linkmedia/js/jquery.min.js') }}"></script>
		<script src="{{ asset('linkmedia/js/jquery-migrate.min.js') }}"></script>
		<!-- Popper JS -->
		<script src="{{ asset('linkmedia/js/popper.min.js') }}"></script>
		<!-- Bootstrap JS -->
		<script src="{{ asset('linkmedia/js/bootstrap.min.js') }}"></script>	
		<!-- Colors JS -->
		<script src="{{ asset('linkmedia/js/colors.js') }}"></script>
		<!-- Modernizer JS -->
		<script src="{{ asset('linkmedia/js/modernizr.min.js') }}"></script>
		<!-- Nice select JS -->
		<script src="{{ asset('linkmedia/js/niceselect.js') }}"></script>
		<!-- Tilt Jquery JS -->
		<script src="{{ asset('linkmedia/js/tilt.jquery.min.js') }}"></script>
		<!-- Fancybox  -->
		<script src="{{ asset('linkmedia/js/jquery.fancybox.min.js') }}"></script>
		<!-- Jquery Nav -->
		<script src="{{ asset('linkmedia/js/jquery.nav.js') }}"></script>
		<!-- Owl Carousel JS -->
		<script src="{{ asset('linkmedia/js/owl.carousel.min.js') }}"></script>
		<!-- Slick Slider JS -->
    	<script src="{{ asset('linkmedia/js/slickslider.min.js') }}"></script>
		<!-- Cube Portfolio JS -->
		<script src="{{ asset('linkmedia/js/cubeportfolio.min.js') }}"></script>
		<!-- Slicknav JS -->
    	<script src="{{ asset('linkmedia/js/jquery.slicknav.min.js') }}"></script>
		<!-- Jquery Steller JS -->
    	<script src="{{ asset('linkmedia/js/jquery.stellar.min.js') }}"></script>
		<!-- Magnific Popup JS -->
    	<script src="{{ asset('linkmedia/js/magnific-popup.min.js') }}"></script>
		<!-- Wow JS -->
		<script src="{{ asset('linkmedia/js/wow.min.js') }}"></script>
		<!-- CounterUp JS -->
		<script src="{{ asset('linkmedia/js/jquery.counterup.min.js') }}"></script>
		<!-- Waypoint JS -->
		<script src="{{ asset('linkmedia/js/waypoints.min.js') }}"></script>
		<!-- Jquery Easing JS -->
    	<script src="{{ asset('linkmedia/js/easing.min.js') }}"></script>
		<!-- Google Map JS -->
    	<script src="{{ asset('linkmedia/js/gmap.min.js') }}"></script>
		<!-- Main JS -->
		<script src="{{ asset('linkmedia/js/main.js') }}"></script>
</body>
</html>