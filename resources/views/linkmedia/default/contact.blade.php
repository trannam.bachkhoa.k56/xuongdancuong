@extends('vn3c.layout.site')

@section('title','Tạo website tại vn3c liên hệ')
@section('meta_description', 'Tạo website tại vn3c vui lòng liên hệ với chúng tôi thông qua form sau...')
@section('keywords', $information['meta_keyword'])

@section('content')
	<!-- banner -->
	@include('vn3c.partials.slide')
	<!-- end banner -->

	<section class="content-contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 link-url">
					<ul>
						<li><a href="/" title="">Trang chủ |</a></li>
						<li><a href="/lien-he" title=""> Liên hệ</a></li>

					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5 col-sm-12">
					<div class="information">
						<h3 class="title">
							Thông tin công ty
						</h3>
						{!! $information['thong-tin-website'] !!}
						<p style="color: red; font-size: 18px; padding: 25px 0 40px 0;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
					</div>
				</div>
				<div class="col-md-7 col-sm-12">

					<div class="mailbox">
						<div class="content-mail">
							<h2>Để lại lời nhắn cho chúng tôi</h2>
							<h3 class="title mb20">Bạn cần hỗ trợ chúng tôi luôn sẵn sàng</h3>
							<div id="getfly-optin-form-iframe-1520616863207"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=peDZ9p2OrU9fucv7HrqZaCVlaqnZLEPquXnETngxh5xJsWJM8b&referrer="+r); f.style.width = "100%";f.style.height = "450px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1520616863207");s.appendChild(f); })(); </script>
							{{-- <form action="{{ route('sub_contact') }}" method="post" onSubmit="return contact(this);">
								{!! csrf_field() !!}
								<div class="ip-text form-group">
									<input type="text" name="name" class="form-control email" placeholder="Họ và tên">
									<input type="text" name="phone" class="form-control captcha" placeholder="Số điện thoại">
								</div>
								<div class="ip-text form-group">
									<input type="email" name="email" class="form-control email" placeholder="Email">
									<input type="text" name="address" class="form-control captcha" placeholder="Địa chỉ">
								</div>

								<div class="ip-textarea">
									<textarea name="message" rows="5" placeholder="Lời nhắn"></textarea>
								</div>
								<input type="hidden" name="is_json" class="form-control captcha" value="1" placeholder="">
								<div class="tr">
									<button type="submit">Gửi phản hồi</button>
								</div>

							</form> --}}
						</div>
					</div>
				</div>
				<p style="color: red; font-size: 18px; padding: 25px 0 40px 0;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
			</div>
		</div>
	</section>
	<section class="map mb30">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<h3 class="title">Bản đồ</h3>
					<div class="content-map">
						{!! $information['map-ban-do'] !!}
						<!-- map -->
					</div>
				</div>

			</div>
		</div>
	</section>
	@include('vn3c.partials.customer_say')
@endsection
