@extends('vn3c.layout.site')

@section('type_meta', 'website')
@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description', !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
    <section class="banner-category-news">
        <div class="container">

            <div class="row">
                <div class="col-md-12 link-url">
                    <ul>
                        <li><a href="/" title="">Trang chủ |</a></li>
                        <li><a href="/danh-muc/{{ $category->slug }}" title=""> {{$category->title}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ">
                    <div class="category-title-new">
                        <h1 class="title-ct-new">{{$category->title}}</h1>
                    </div>
                </div>
            </div>

            @foreach ($posts as $id => $post)
                @if ($id == 0)
                    <?php $date=date_create($post->created_at); ?>
                    <div class="row">
                        <div class="col-md-7 left-ct">
                            <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}">
                                <img src="{{ asset($post->image) }}" alt="">
                            </a>
                        </div>
                        <div class="col-md-5 right-ct">
                            <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}">
                                <h2 class="title-ct-new">{{ $post->title }}</h2>
                            </a>
                            <p><i>{{ $post->user_email }}  |  {{ date_format($date,"d/m/Y H:i") }}</i></p>
                            <p class="content">
                                {{ $post->description }}
                            </p>
                            <button type="">
                                <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}" class="title-ct-new">
                                    đọc thêm <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                </a>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </section>

    <section class="content-ct">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    @foreach ($posts as $id => $post)
                        @if ($id > 0)
                            <div class="content-it-ct">
                                <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}">
                                    <h3 class="title-ct-new">{{ $post->title }}</h3>
                                </a>
                                <p><i>{{ $post->user_email }}  |  {{ date_format($date,"d/m/Y H:i") }}</i></p>
                                <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}">
                                    <img src="{{ asset($post->image) }}" alt="">
                                </a>
                                <p class="content-it">
                                    {{ $post->description }}
                                </p>
                                <button type="">
                                    <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}" class="title-ct-new">
                                        đọc thêm <i></i>
                                    </a>
                                </button>
                            </div>
                        @endif
                    @endforeach
                </div>


            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="pagination-ct">
                        {{ $posts->links() }}
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

