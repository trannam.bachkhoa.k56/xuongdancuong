@extends('linkmedia.layout.site')

@section('title', $information['meta-title'])
@section('meta_description', $information['meta-description'])
@section('keywords', $information['meta-keyword'])
@section('type_meta', 'website')

@section('content')
<!-- Hero Area -->
	<section id="hero-area" class="hero-area">
		<!-- Slider -->
		<div class="slider-area">
			<!-- Single Slider -->
			<div class="single-slider" style="background-image:url('http://themelamp.com/templates/radix/images/slider/slider-image1.jpg')">
				<div class="container">
					<div class="row">
						<div class="col-lg-7 col-md-6 col-12">
							<!-- Slider Text -->
							<div class="slider-text">
								<h1>VN3C <span>Thiết kế</span> Website chuẩn seo<span>!</span></h1>
								<p>Nếu bạn cần một website tốt cho doanh nghiệp của mình, Hãy đặt hàng ngay với chúng tôi</p>
								<div class="button">
									<a href="portfolio-3-column.html" class="btn">Our Portfolio</a>
									<a href="https://www.youtube.com/watch?v=FZQPhrdKjow" class="btn video video-popup mfp-fade"><i class="fa fa-play"></i>Play Now</a>
								</div>
							</div>
							<!--/ End Slider Text -->
						</div>
						<div class="col-lg-5 col-md-6 col-12">
							<!-- Image Gallery -->
							<div class="image-gallery">
								<div class="single-image">
									<img src="http://themelamp.com/templates/radix/images/slider/gallery-image1.jpg" alt="#">
								</div>
								<div class="single-image two">
									<img src="http://themelamp.com/templates/radix/images/slider/gallery-image2.jpg" alt="#">
								</div>
							</div>
							<!--/ End Image Gallery -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Single Slider -->
			<!-- Single Slider -->
			<div class="single-slider slider-right" style="background-image:url('http://themelamp.com/templates/radix/images/slider/slider-image2.jpg')">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-6 col-12">
							<!-- Image Gallery -->
							<div class="image-gallery">
								<div class="single-image">
									<img src="http://themelamp.com/templates/radix/images/slider/gallery-image1.jpg" alt="#">
								</div>
								<div class="single-image two">
									<img src="http://themelamp.com/templates/radix/images/slider/gallery-image2.jpg" alt="#">
								</div>
							</div>
							<!--/ End Image Gallery -->
						</div>
						<div class="col-lg-7 col-md-6 col-12">
							<!-- Slider Text -->
							<div class="slider-text text-right">
								<h1>Radix <span>Business</span> World That Possible anything<span>!</span></h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi laoreet urna ante, quis luctus nisi sodales sit amet. Aliquam a enim in massa molestie mollis Proin quis velit at nisl vulputate egestas non in arcu Proin a magna hendrerit, tincidunt neque sed. </p>
								<div class="button">
									<a href="services.html" class="btn">Our Services</a>
									<a href="https://www.youtube.com/watch?v=FZQPhrdKjow" class="btn video video-popup mfp-fade"><i class="fa fa-play"></i>Play Now</a>
								</div>
							</div>
							<!--/ End Slider Text -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Single Slider -->
			<!-- Single Slider -->
			<div class="single-slider slider-center" style="background-image:url('http://themelamp.com/templates/radix/images/slider/slider-image1.jpg')">
				<div class="container">
					<div class="row">
						<div class="col-lg-10 offset-lg-1 col-12">
							<!-- Slider Text -->
							<div class="slider-text text-center">
								<h1>Build your website with Radix Multipurpose <span>Business</span> Template.</h1>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi laoreet urna ante, quis luctus nisi sodales sit amet. Aliquam a enim in massa molestie mollis Proin quis velit at nisl vulputate egestas non in arcu Proin a magna hendrerit, tincidunt neque sed. </p>
								<div class="button">
									<a href="about-us.html" class="btn">About Company</a>
									<a href="https://www.youtube.com/watch?v=FZQPhrdKjow" class="btn video video-popup mfp-fade"><i class="fa fa-play"></i>Play Now</a>
								</div>
							</div>
							<!--/ End Slider Text -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Single Slider -->
		</div>
		<!--/ End Slider -->
	</section>
	<!--/ End Hero Area -->
	
	<!-- About Us -->
	<section class="about-us section">
		<div class="container">
			<!-- <div class="row">
				<div class="col-12">
					<div class="section-title wow fadeInUp">
						<span class="title-bg">VN3C</span>
						<h2>Website chính thức công ty cổ phần công nghệ VN3C Việt Nam</h2>
						<p>Chúng tôi cung cấp dịch vụ thiết kế website, bộ nhận dạng thương hiệu, giải pháp truyền thông, quảng cáo google, facebook. <p>
					</div>
				</div>
			</div>-->
			<div class="row">
				<div class="col-lg-6 col-12 wow fadeInLeft" data-wow-delay="0.6s">
					<!-- Video -->
					<div class="about-video">
						<div class="single-video overlay">
							<img src="{{ asset('linkmedia/images/design-website.png') }}" alt="Thiết kế website">
						</div>
					</div>
					<!--/ End Video -->
				</div>
				<div class="col-lg-6 col-12 wow fadeInRight" data-wow-delay="0.8s">
					<!-- About Content -->
					<div class="about-content">
						<h2>Thiết Kế Website Chuẩn SEO</h2>
						<p>VN3C Cung cấp dịch vụ thiết kế website theo tiêu chuẩn, Với hệ thống ưu việt. Đảm bảo tính thẩm mỹ, trình độ kỹ thuật cao. Chúng tôi sẽ phân tích để đưa ra những tính năng phù hợp nhất với website của bạn, phương hướng phát triển và kinh phí dự kiến phù hợp với từng khách hàng.</p>
						<ul class="list">
							<li>Tư vấn và triển khai theo nhu cầu và yêu cầu</li>
							<li>Giao diện thiết kế độc quyền.</li>
							<li>Cấu trúc web chuẩn SEO</li>
							<li>Hiển thị tốt trên các thiết bị di động</li>
							<li>Tối ưu trải nghiệm người dùng, tốc độ website.</li>
							<li>Dễ dàng quản lý website</li>
						</ul>
					</div>
					<!--/ End About Content -->
				</div>
			</div>
		</div>
	</section>
	<!--/ End About Us -->
	
	<!-- Services -->
	<section id="services" class="services section">
		<div class="container">
			<div class="row">
				<div class="col-12 wow fadeInUp">
					<div class="section-title">
						<span class="title-bg">Dịch vụ</span>
						<h2>Các dịch vụ do VN3C cung cấp</h2>
						<p>Chúng tôi cung cấp các giải pháp truyền thông, marketing, thiết kế website, logo, nhận dạng thương hiệu<p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="service-slider">
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-magic"></i>
							<h3><a href="service-single.html">Thiết kế website</a></h3>
							<p>welcome to our consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-lightbulb-o"></i>
							<h3><a href="service-single.html">Nhận dạng thương hiệu</a></h3>
							<p>Creative and erat, porta non porttitor non, dignissim et enim Aenean ac enim feugiat classical Latin</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-wordpress"></i>
							<h3><a href="service-single.html">Lập trình ứng dụng App</a></h3>
							<p>just fine erat, porta non porttitor non, dignissim et enim Aenean ac enim feugiat classical Latin</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-bullhorn "></i>
							<h3><a href="service-single.html">Tư vấn Marketing</a></h3>
							<p>Possible of erat, porta non porttitor non, dignissim et enim Aenean ac enim feugiat classical Latin</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-magic"></i>
							<h3><a href="service-single.html">Thiết kế logo</a></h3>
							<p>welcome to our consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-lightbulb-o"></i>
							<h3><a href="service-single.html">Quảng cáo google</a></h3>
							<p>Creative and erat, porta non porttitor non, dignissim et enim Aenean ac enim feugiat classical Latin</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-wordpress"></i>
							<h3><a href="service-single.html">Quảng cáo facebook</a></h3>
							<p>just fine erat, porta non porttitor non, dignissim et enim Aenean ac enim feugiat classical Latin</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-bullhorn "></i>
							<h3><a href="service-single.html">Chuyển psd thành website</a></h3>
							<p>Possible of erat, porta non porttitor non, dignissim et enim Aenean ac enim feugiat classical Latin</p>
						</div>
						<!-- End Single Service -->
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-bullseye "></i>
							<h3><a href="service-single.html">Wordpress Design</a></h3>
							<p>Everything ien erat, porta non porttitor non, dignissim et enim Aenean ac enim feugiat Latin</p>
						</div>
						<!-- End Single Service -->	
						<!-- Single Service -->
						<div class="single-service">
							<i class="fa fa-cube"></i>
							<h3><a href="service-single.html">Viết nội dung website</a></h3>
							<p>Information sapien erat,  non porttitor non, dignissim et enim Aenean ac enim feugiat classical Latin</p>
						</div>
						<!-- End Single Service -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Services -->
	
	<!-- Fun Facts -->
	<section id="fun-facts" class="fun-facts section">
		<div class="container">	
			<div class="row">
				<div class="col-lg-5 col-12 wow fadeInLeft" data-wow-delay="0.5s">
					<div class="text-content">
						<div class="section-title">
							<h1><span>Our achievements</span>With smooth animation numbering </h1>
							<p>Pellentesque vitae gravida nulla. Maecenas molestie ligula quis urna viverra venenatis. Donec at ex metus. Suspendisse ac est et magna viverra eleifend. Etiam varius auctor est eu eleifend.</p>
							<a href="contact.html" class="btn primary">Contact Us</a>
						</div>
					</div>
				</div>
				<div class="col-lg-7 col-12">
					<div class="row">	
						<div class="col-lg-6 col-md-6 col-12 wow fadeIn" data-wow-delay="0.6s">
							<!-- Single Fact -->
							<div class="single-fact">
								<div class="icon"><i class="fa fa-clock-o"></i></div>
								<div class="counter">
									<p><span class="count">35</span></p>
									<h4>years of success</h4>
								</div>
							</div>
							<!--/ End Single Fact -->
						</div>
						<div class="col-lg-6 col-md-6 col-12 wow fadeIn" data-wow-delay="0.8s">
							<!-- Single Fact -->
							<div class="single-fact">
								<div class="icon"><i class="fa fa-bullseye"></i></div>
								<div class="counter">
									<p><span class="count">88</span>K</p>
									<h4>Project Complete</h4>
								</div>
							</div>
							<!--/ End Single Fact -->
						</div>
						<div class="col-lg-6 col-md-6 col-12 wow fadeIn" data-wow-delay="1s">
							<!-- Single Fact -->
							<div class="single-fact">
								<div class="icon"><i class="fa fa-dollar"></i></div>
								<div class="counter">
									<p><span class="count">10</span>M</p>
									<h4>Total Earnings</h4>
								</div>
							</div>
							<!--/ End Single Fact -->
						</div>
						<div class="col-lg-6 col-md-6 col-12 wow fadeIn" data-wow-delay="1.2s">
							<!-- Single Fact -->
							<div class="single-fact">
								<div class="icon"><i class="fa fa-trophy"></i></div>
								<div class="counter">
									<p><span class="count">32</span></p>
									<h4>Winning Awards</h4>
								</div>
							</div>
							<!--/ End Single Fact -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Fun Facts -->
	
	<!-- Why Choose Us -->
	<section id="why-choose" class="why-choose section">
		<div class="container-fluid fix">
			<div class="row fix">
				<div class="col-lg-6 col-md-6 col-12 fix">
					<div class="why-video">
						<!-- Video -->
						<div class="video wow zoomIn">
							<a href="https://www.youtube.com/watch?v=FZQPhrdKjow" class="btn video-popup mfp-fade"><i class="fa fa-play"></i></a>
							<p>Watch this awesome video!</p>
						</div>
						<!--/ End Video -->
					</div>
				</div>		
				<div class="col-lg-6 col-md-6 col-12 fix">
					<!-- Choose Main -->
					<div class="choose-main">
						<div class="working-process">
							<h2>Why Choose Us?</h2>
							<p>We are Professional long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
						</div>
						<div class="single-choose wow fadeInRight" data-wow-delay="0.2s">
							<span class="number">01</span>
							<h4><span>Planing</span>for every new project.</h4>
						</div>
						<div class="single-choose wow fadeInRight" data-wow-delay="0.4s">
							<span class="number">03</span>
							<h4><span>We Have</span>strong & creative team members.</h4>
						</div>
						<div class="single-choose wow fadeInRight" data-wow-delay="0.6s">
							<span class="number">04</span>
							<h4><span>24/7 Dedicated</span>Support ticket system.</h4>
						</div>
						<div class="single-choose wow fadeInRight" data-wow-delay="0.8s">
							<span class="number">02</span>
							<h4><span>We Build</span>Pixel perfect website theme.</h4>
						</div>
					</div>
					<!--/ End Choose Main -->
				</div>			
			</div>
		</div>
	</section>	
	<!--/ End Why Choose Us -->

	<!-- Portfolio -->
	<section id="portfolio" class="portfolio section">
		<div class="container">
			<div class="row">
				<div class="col-12 wow fadeInUp">
					<div class="section-title">
						<span class="title-bg">Projects</span>
						<h1>Our Portfolio</h1>
						<p>Sed lorem enim, faucibus at erat eget, laoreet tincidunt tortor. Ut sed mi nec ligula bibendum aliquam. Sed scelerisque maximus magna, a vehicula turpis Proin<p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<!-- portfolio Nav -->
					<div class="portfolio-nav">
						<ul class="tr-list list-inline" id="portfolio-menu">
							<li data-filter="*" class="cbp-filter-item active">All Works<div class="cbp-filter-counter"></div></li>  
							<li data-filter=".animation" class="cbp-filter-item">Animation<div class="cbp-filter-counter"></div></li>
							<li data-filter=".website" class="cbp-filter-item">Website<div class="cbp-filter-counter"></div></li>
							<li data-filter=".package" class="cbp-filter-item">Package<div class="cbp-filter-counter"></div></li>
							<li data-filter=".development" class="cbp-filter-item">Development<div class="cbp-filter-counter"></div></li>
							<li data-filter=".printing" class="cbp-filter-item">Printing<div class="cbp-filter-counter"></div></li>
						</ul>  		
					</div>
					<!--/ End portfolio Nav -->
				</div>
			</div>
			<div class="portfolio-inner">
				<div class="row">	
					<div class="col-12">	
						<div id="portfolio-item">
							<!-- Single portfolio -->
							<div class="cbp-item website animation printing">
								<div class="portfolio-single">
									<div class="portfolio-head">
										<img src="http://themelamp.com/templates/radix/images/portfolio/p1.jpg" alt="#"/>
									</div>
									<div class="portfolio-hover">
										<h4><a href="portfolio-single.html">Creative Work</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim</p>
										<div class="button">
											<a class="primary" data-fancybox="gallery" href="http://themelamp.com/templates/radix/images/portfolio/p1.jpg"><i class="fa fa-search"></i></a>
											<a href="portfolio-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							<!--/ End portfolio -->	
							<!-- Single portfolio -->
							<div class="cbp-item website package development">
								<div class="portfolio-single">
									<div class="portfolio-head">
										<img src="http://themelamp.com/templates/radix/images/portfolio/p2.jpg" alt="#"/>
									</div>
									<div class="portfolio-hover">
										<h4><a href="portfolio-single.html">Responsive Design</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim</p>
										<div class="button">
											<a href="https://www.youtube.com/watch?v=E-2ocmhF6TA" class="primary cbp-lightbox"><i class="fa fa-play"></i></a>
											<a href="portfolio-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							<!--/ End portfolio -->	
							<!-- Single portfolio -->
							<div class="cbp-item website animation">
								<div class="portfolio-single">
									<div class="portfolio-head">
										<img src="http://themelamp.com/templates/radix/images/portfolio/p3.jpg" alt="#"/>
									</div>
									<div class="portfolio-hover">
										<h4><a href="portfolio-single.html">Bootstrap Based</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim</p>
										<div class="button">
											<a class="primary" data-fancybox="gallery" href="http://themelamp.com/templates/radix/images/portfolio/p3.jpg"><i class="fa fa-search"></i></a>
											<a href="portfolio-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							<!--/ End portfolio -->	
							<!-- Single portfolio -->
							<div class="cbp-item development printing">
								<div class="portfolio-single">
									<div class="portfolio-head">
										<img src="http://themelamp.com/templates/radix/images/portfolio/p4.jpg" alt="#"/>
									</div>
									<div class="portfolio-hover">
										<h4><a href="portfolio-single.html">Clean Design</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim</p>
										<div class="button">
											<a href="https://www.youtube.com/watch?v=E-2ocmhF6TA" class="primary cbp-lightbox"><i class="fa fa-play"></i></a>
											<a href="portfolio-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							<!--/ End portfolio -->	
							<!-- Single portfolio -->
							<div class="cbp-item development package">
								<div class="portfolio-single">
									<div class="portfolio-head">
										<img src="http://themelamp.com/templates/radix/images/portfolio/p5.jpg" alt="#"/>
									</div>
									<div class="portfolio-hover">
										<h4><a href="portfolio-single.html">Animation</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim</p>
										<div class="button">
											<a class="primary" data-fancybox="gallery" href="http://themelamp.com/templates/radix/images/portfolio/p5.jpg"><i class="fa fa-search"></i></a>
											<a href="portfolio-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							<!--/ End portfolio -->	
							<!-- Single portfolio -->
							<div class="cbp-item website animation printing">
								<div class="portfolio-single">
									<div class="portfolio-head">
										<img src="http://themelamp.com/templates/radix/images/portfolio/p6.jpg" alt="#"/>
									</div>
									<div class="portfolio-hover">
										<h4><a href="portfolio-single.html">Parallax</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim</p>
										<div class="button">
											<a href="https://www.youtube.com/watch?v=E-2ocmhF6TA" class="primary cbp-lightbox"><i class="fa fa-play"></i></a>
											<a href="portfolio-single.html"><i class="fa fa-link"></i></a>
										</div>
									</div>
								</div>
							</div>
							<!--/ End portfolio -->	
						</div>
					</div>
					<div class="col-12">
						<div class="button">
							<a class="btn primary" href="portfolio-3-column.html">More Portfolio</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End portfolio -->
	
	<!-- Consulting -->
	<section id="consulting" class="consulting section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12 wow fadeInLeft" data-wow-delay="0.4s">
					<div class="form-area">
						<h2><i class="fa fa-phone"></i>Request A Call Back</h2>
						<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim feugiat, facilisis arcu vehicula</p>
						<!-- Consult Form -->
						<form class="form" action="#">
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<div class="nice-select form-control wide" tabindex="0"><span class="current">I would like to discuss about</span>
											<ul class="list">
												<li data-value="Starting a startup" class="option selected focus">Starting a new business</li>
												<li data-value="1" class="option">Startup Consultation</li>
												<li data-value="2" class="option">Financial Consultation</li>
												<li data-value="3" class="option">Business Consultation</li>
											</ul>
										</div>
									</div>
									<div class="form-group">
										<input name="name" placeholder="Your Name" required="required" type="text">
									</div>
									<div class="form-group">
										<input name="email" placeholder="Your Email" required="required" type="email">
									</div>
									<div class="form-group">
										<input name="phone" placeholder="Your Mobile" required="required" type="text">
									</div>
									<div class="form-group button">	
										<button type="submit" class="btn primary">Submit Now</button>
									</div>
								</div>
							</div>
						</form>
						<!--/ End Consult Form -->
					</div>
				</div>
			</div>
		</div>
		<!-- Consult Right -->
		<div class="consult-right wow fadeInRight" data-wow-delay="0.6s">
			<div class="text-content">
				<h2>We are Professional Business & Consulting Agency</h2>
				<p>We are Professional long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
				<ul class="list">
					<li><i class="fa fa-star"></i>Perfect guidance to build your start-up Business. </li>
					<li><i class="fa fa-clock-o"></i>Save time, resource and money!</li>
					<li><i class="fa fa-money"></i>Helping to increase the world economic growth!</li>
					<li><i class="fa fa-clock-o"></i>We will help to improve your business!</li>
					<li><i class="fa fa-star"></i>We provide startup, business & Financial consulting</li>
				</ul>
			</div>	
		</div>	
		<!--/ End Consult Right -->
	</section>
	<!--/ End Consulting -->
	
	<!-- Pricing -->
	<section class="pricing-plan section">
		<div class="container">
			<div class="row">
				<div class="col-12 wow fadeInUp">
					<div class="section-title">
						<span class="title-bg">Pricing</span>
						<h1>Our Plan</h1>
						<p>Sed lorem enim, faucibus at erat eget, laoreet tincidunt tortor. Ut sed mi nec ligula bibendum aliquam. Sed scelerisque maximus magna, a vehicula turpis Proin<p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-12 wow fadeInUp" data-wow-delay="0.4s">
					<!-- Single Table -->
					<div class="single-table">
						<!-- Table Head -->
						<div class="table-head">
							<h2 class="title">Basic</h2>
							<div class="price">
								<p class="amount"><span class="currency">$</span>20<span>/month</span></p>
							</div>	
						</div>
						<!--/ End Table Head -->
						<!-- Table List -->
						<ul class="table-list">	
							<li>Limited Times Marketing</li>
							<li>5 Analytics Campaigns</li>
							<li>300 Keywords</li>
							<li>250,000 crawled Page</li>
							<li>Unlimited Updates</li>
							<li>+ Free Website Design</li>
						</ul>
						<!--/ End Table List -->
						<!-- Table Bottom -->
						<div class="table-bottom">
							<a class="btn" href="#">Start Now</a>
						</div>
						<!-- Table Bottom -->
					</div>
					<!-- End Single Table-->
				</div>
				<div class="col-lg-4 col-12 wow fadeInUp" data-wow-delay="0.6s">
					<!-- Single Table -->
					<div class="single-table active">
						<!-- Table Head -->
						<div class="table-head">
							<h2 class="title">Premium</h2>
							<div class="price">
								<p class="amount"><span class="currency">$</span>50<span>/month</span></p>
							</div>	
						</div>
						<!--/ End Table Head -->
						<!-- Table List -->
						<ul class="table-list">	
							<li>Limited Times Marketing</li>
							<li>20 Analytics Campaigns</li>
							<li>500 Keywords</li>
							<li>800,000 crawled Page</li>
							<li>Unlimited Updates</li>
							<li>+ Free Website Design</li>
						</ul>
						<!--/ End Table List -->
						<!-- Table Bottom -->
						<div class="table-bottom">
							<a class="btn" href="#">Start Now</a>
						</div>
						<!-- Table Bottom -->
					</div>
					<!-- End Single Table-->
				</div>
				<div class="col-lg-4 col-12 wow fadeInUp" data-wow-delay="0.8s">
					<!-- Single Table -->
					<div class="single-table">
						<!-- Table Head -->
						<div class="table-head">
							<h2 class="title">Advanced</h2>
							<div class="price">
								<p class="amount"><span class="currency">$</span>100<span>/month</span></p>
							</div>	
						</div>
						<!--/ End Table Head -->
						<!-- Table List -->
						<ul class="table-list">	
							<li>Unlimited Times Marketing</li>
							<li>5 Analytics Campaigns</li>
							<li>300 Keywords</li>
							<li>250,000 crawled Page</li>
							<li>Unlimited Updates</li>
							<li>+ Free Website Design</li>
						</ul>
						<!--/ End Table List -->
						<!-- Table Bottom -->
						<div class="table-bottom">
							<a class="btn" href="#">Start Now</a>
						</div>
						<!-- Table Bottom -->
					</div>
					<!-- End Single Table-->
				</div>
			</div>	
		</div>	
	</section>	
	<!--/ End Pricing -->
	
	<!-- Testimonials -->
	<section id="testimonials" class="testimonials section">
		<div class="container">
			<div class="col-12 wow fadeInUp">
				<div class="section-title">
					<span class="title-bg">Review</span>
					<h1>What Clients Says</h1>
					<p>Sed lorem enim, faucibus at erat eget, laoreet tincidunt tortor. Ut sed mi nec ligula bibendum aliquam. Sed scelerisque maximus magna, a vehicula turpis Proin<p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 offset-lg-2 col-12">
					<div class="testimonial-nav">
						<div class="single-nav">
							<img src="http://themelamp.com/templates/radix/images/client1.jpg" alt="#">
						</div>
						<div class="single-nav">
							<img src="http://themelamp.com/templates/radix/images/client2.jpg" alt="#">
						</div>
						<div class="single-nav">
							<img src="http://themelamp.com/templates/radix/images/client3.jpg" alt="#">
						</div>
						<div class="single-nav">
							<img src="http://themelamp.com/templates/radix/images/client4.jpg" alt="#">
						</div>
						<div class="single-nav">
							<img src="http://themelamp.com/templates/radix/images/client1.jpg" alt="#">
						</div>
						<div class="single-nav">
							<img src="http://themelamp.com/templates/radix/images/client2.jpg" alt="#">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 offset-lg-1 col-12">
					<div class="testimonial-content">
						<div class="single-content">
							<p>condimentum augue volutpat, luctus tellus sit amet, feugiat ipsum. Proin sem augue, posuere id turpis eget, semper feugiat mi. Cras semper risus at leo venenatis sagittis. In iaculis eros ac eros maximus, ut bibendum tellus condimentum. Mauris massa nisl, accumsan sit amet feugiat gravida, rhoncus at diam. Vestibulum ultricies eu nulla et maximus. Nullam eu nibh turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							<div class="testimonial-info">							
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
								<h4>Lufia Rosan<span>Manager, Creative Ltd</span></h4>
							</div>
						</div>
						<div class="single-content">
							<p>condimentum augue volutpat, luctus tellus sit amet, feugiat ipsum. Proin sem augue, posuere id turpis eget, semper feugiat mi. Cras semper risus at leo venenatis sagittis. In iaculis eros ac eros maximus, ut bibendum tellus condimentum. Mauris massa nisl, accumsan sit amet feugiat gravida, rhoncus at diam. Vestibulum ultricies eu nulla et maximus. Nullam eu nibh turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							<div class="testimonial-info">							
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
								<h4>Josep Bamb<span>Founder, Graysoft LLC</span></h4>
							</div>
						</div>
						<div class="single-content">
							<p>condimentum augue volutpat, luctus tellus sit amet, feugiat ipsum. Proin sem augue, posuere id turpis eget, semper feugiat mi. Cras semper risus at leo venenatis sagittis. In iaculis eros ac eros maximus, ut bibendum tellus condimentum. Mauris massa nisl, accumsan sit amet feugiat gravida, rhoncus at diam. Vestibulum ultricies eu nulla et maximus. Nullam eu nibh turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							<div class="testimonial-info">							
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
								<h4>Trolia Ula<span>Ceo, Micromedia Ltd</span></h4>
							</div>
						</div>
						<div class="single-content">
							<p>condimentum augue volutpat, luctus tellus sit amet, feugiat ipsum. Proin sem augue, posuere id turpis eget, semper feugiat mi. Cras semper risus at leo venenatis sagittis. In iaculis eros ac eros maximus, ut bibendum tellus condimentum. Mauris massa nisl, accumsan sit amet feugiat gravida, rhoncus at diam. Vestibulum ultricies eu nulla et maximus. Nullam eu nibh turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							<div class="testimonial-info">							
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
								<h4>Siman Prodap<span>Developer, GoogleInc</span></h4>
							</div>
						</div>
						<div class="single-content">
							<p>condimentum augue volutpat, luctus tellus sit amet, feugiat ipsum. Proin sem augue, posuere id turpis eget, semper feugiat mi. Cras semper risus at leo venenatis sagittis. In iaculis eros ac eros maximus, ut bibendum tellus condimentum. Mauris massa nisl, accumsan sit amet feugiat gravida, rhoncus at diam. Vestibulum ultricies eu nulla et maximus. Nullam eu nibh turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							<div class="testimonial-info">							
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
								<h4>Esika Prua<span>Co-founder, Fashioncrown</span></h4>
							</div>
						</div>
						<div class="single-content">
							<p>condimentum augue volutpat, luctus tellus sit amet, feugiat ipsum. Proin sem augue, posuere id turpis eget, semper feugiat mi. Cras semper risus at leo venenatis sagittis. In iaculis eros ac eros maximus, ut bibendum tellus condimentum. Mauris massa nisl, accumsan sit amet feugiat gravida, rhoncus at diam. Vestibulum ultricies eu nulla et maximus. Nullam eu nibh turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							<div class="testimonial-info">							
								<ul class="rating">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
								<h4>Lamana Drema<span>Freelancer</span></h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Testimonials -->
	
	<!-- Start Team -->
	<section id="team" class="team section">
		<div class="container">
			<div class="row">
				<div class="col-12 wow fadeInUp" data-wow-delay="0.2s">
					<div class="section-title">
						<span class="title-bg">Team</span>
						<h1>Our Leaders</h1>
						<p>Sed lorem enim, faucibus at erat eget, laoreet tincidunt tortor. Ut sed mi nec ligula bibendum aliquam. Sed scelerisque maximus magna, a vehicula turpis Proin<p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-6 col-12 wow fadeInUp" data-wow-delay="0.4s" data-tilt>
					<!-- Single Team -->
					<div class="single-team">
						<div class="t-head">
							<img src="http://themelamp.com/templates/radix/images/t2.jpg" alt="#">
							<div class="t-icon">
								<a href="team-single.html"><i class="fa fa-plus"></i></a>
							</div>
						</div>
						<div class="t-bottom">
							<p>Founder</p>
							<h2>Collis Molate</h2>
							<ul class="t-social">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="active"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-behance"></i></a></li>				
							</ul>
						</div>
					</div>
					<!-- End Single Team -->
				</div>	
				<div class="col-lg-3 col-md-6 col-12 wow fadeInUp" data-wow-delay="0.6s" data-tilt>
					<!-- Single Team -->
					<div class="single-team">
						<!-- Team Head -->
						<div class="t-head">
							<img src="http://themelamp.com/templates/radix/images/t1.jpg" alt="#">
							<div class="t-icon">
								<a href="team-single.html"><i class="fa fa-plus"></i></a>
							</div>
						</div>
						<!-- Team Bottom -->
						<div class="t-bottom">
							<p>Co-Founder</p>
							<h2>Domani Plavon</h2>
							<ul class="t-social">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="active"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-behance"></i></a></li>				
							</ul>
						</div>
						<!--/ End Team Bottom -->
					</div>
					<!-- End Single Team -->
				</div>	
				<div class="col-lg-3 col-md-6 col-12 wow fadeInUp" data-wow-delay="0.8s" data-tilt>
					<!-- Single Team -->
					<div class="single-team">
						<!-- Team Head -->
						<div class="t-head">
							<img src="http://themelamp.com/templates/radix/images/t3.jpg" alt="#">
							<div class="t-icon">
								<a href="team-single.html"><i class="fa fa-plus"></i></a>
							</div>
						</div>
						<!-- Team Bottom -->
						<div class="t-bottom">
							<p>Developer</p>
							<h2>John Mard</h2>
							<ul class="t-social">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="active"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-behance"></i></a></li>				
							</ul>
						</div>
						<!--/ End Team Bottom -->
					</div>
					<!-- End Single Team -->
				</div>		
				<div class="col-lg-3 col-md-6 col-12 wow fadeInUp" data-wow-delay="1s" data-tilt>
					<!-- Single Team -->
					<div class="single-team">
						<!-- Team Head -->
						<div class="t-head">
							<img src="http://themelamp.com/templates/radix/images/t4.jpg" alt="#">
							<div class="t-icon">
								<a href="team-single.html"><i class="fa fa-plus"></i></a>
							</div>
						</div>
						<!-- Team Bottom -->
						<div class="t-bottom">
							<p>Marketer</p>
							<h2>Amanal Frond</h2>
							<ul class="t-social">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="active"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-behance"></i></a></li>				
							</ul>
						</div>
						<!--/ End Team Bottom -->
					</div>
					<!-- End Single Team -->
				</div>		
			</div>
		</div>
	</section>
	<!--/ End Team -->
	
	<!-- Call To Action -->
	<section class="call-to-action section" data-stellar-background-ratio="0.5">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12 wow fadeInUp">
					<div class="call-to-main">
						<h2>We have 35+ Years of experiences for creating creative website project.</h2>
						<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac enim feugiat, facilisis arcu vehicula, consequat sem. Cras et vulputate nisi, ac dignissim mi. Etiam laoreet</p>
						<a href="contact.html" class="btn">Buy This Theme</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Call To Action -->
	
	<!-- Blogs Area -->
	<section class="blogs-main section">
		<div class="container">
			<div class="row">
				<div class="col-12 wow fadeInUp">
					<div class="section-title">
						<span class="title-bg">News</span>
						<h1>Latest Blogs</h1>
						<p>Sed lorem enim, faucibus at erat eget, laoreet tincidunt tortor. Ut sed mi nec ligula bibendum aliquam. Sed scelerisque maximus magna, a vehicula turpis Proin<p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="row blog-slider">
						<div class="col-lg-4 col-12">
							<!-- Single Blog -->
							<div class="single-blog">
								<div class="blog-head">
									<img src="http://themelamp.com/templates/radix/images/blogs/blog1.jpg" alt="#">
								</div>
								<div class="blog-bottom">
									<div class="blog-inner">
										<h4><a href="blog-single.html">Recognizing the need is the primary</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac tincidunt tortor sedelon bond</p>
										<div class="meta">
											<span><i class="fa fa-bolt"></i><a href="#">Marketing</a></span>
											<span><i class="fa fa-calendar"></i>03 May, 2018</span>
											<span><i class="fa fa-eye"></i><a href="#">333k</a></span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Single Blog -->
						</div>
						<div class="col-lg-4 col-12">
							<!-- Single Blog -->
							<div class="single-blog">
								<div class="blog-head">
									<img src="http://themelamp.com/templates/radix/images/blogs/blog2.jpg" alt="#">
								</div>
								<div class="blog-bottom">
									<div class="blog-inner">
										<h4><a href="blog-single.html">How to grow your business with blank table!</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac tincidunt tortor sedelon bond</p>
										<div class="meta">
											<span><i class="fa fa-bolt"></i><a href="#">Business</a></span>
											<span><i class="fa fa-calendar"></i>28 April, 2018</span>
											<span><i class="fa fa-eye"></i><a href="#">5m</a></span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Single Blog -->
						</div>
						<div class="col-lg-4 col-12">
							<!-- Single Blog -->
							<div class="single-blog">
								<div class="blog-head">
									<img src="http://themelamp.com/templates/radix/images/blogs/blog3.jpg" alt="#">
								</div>
								<div class="blog-bottom">
									<div class="blog-inner">
										<h4><a href="blog-single.html">10 ways to improve your startup Business</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac tincidunt tortor sedelon bond</p>
										<div class="meta">
											<span><i class="fa fa-bolt"></i><a href="#">Brand</a></span>
											<span><i class="fa fa-calendar"></i>15 April, 2018</span>
											<span><i class="fa fa-eye"></i><a href="#">10m</a></span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Single Blog -->
						</div>
						<div class="col-lg-4 col-12">
							<!-- Single Blog -->
							<div class="single-blog">
								<div class="blog-head">
									<img src="http://themelamp.com/templates/radix/images/blogs/blog4.jpg" alt="#">
								</div>
								<div class="blog-bottom">
									<div class="blog-inner">
										<h4><a href="blog-single.html">Recognizing the need is the primary</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac tincidunt tortor sedelon bond</p>
										<div class="meta">
											<span><i class="fa fa-bolt"></i><a href="#">Online</a></span>
											<span><i class="fa fa-calendar"></i>25 March, 2018</span>
											<span><i class="fa fa-eye"></i><a href="#">38k</a></span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Single Blog -->
						</div>
						<div class="col-lg-4 col-12">
							<!-- Single Blog -->
							<div class="single-blog">
								<div class="blog-head">
									<img src="http://themelamp.com/templates/radix/images/blogs/blog5.jpg" alt="#">
								</div>
								<div class="blog-bottom">
									<div class="blog-inner">
										<h4><a href="blog-single.html">How to grow your business with blank table!</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac tincidunt tortor sedelon bond</p>
										<div class="meta">
											<span><i class="fa fa-bolt"></i><a href="#">Marketing</a></span>
											<span><i class="fa fa-calendar"></i>10 March, 2018</span>
											<span><i class="fa fa-eye"></i><a href="#">100k</a></span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Single Blog -->
						</div>
						<div class="col-lg-4 col-12">
							<!-- Single Blog -->
							<div class="single-blog">
								<div class="blog-head">
									<img src="http://themelamp.com/templates/radix/images/blogs/blog6.jpg" alt="#">
								</div>
								<div class="blog-bottom">
									<div class="blog-inner">
										<h4><a href="blog-single.html">10 ways to improve your startup Business</a></h4>
										<p>Maecenas sapien erat, porta non porttitor non, dignissim et enim. Aenean ac tincidunt tortor sedelon bond</p>
										<div class="meta">
											<span><i class="fa fa-bolt"></i><a href="#">Website</a></span>
											<span><i class="fa fa-calendar"></i>21 February, 2018</span>
											<span><i class="fa fa-eye"></i><a href="#">320k</a></span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Single Blog -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Blogs Area -->
	
	<!-- Partners -->
	<section id="partners" class="partners section">
		<div class="container">
			<div class="row">
				<div class="col-12 wow fadeInUp">
					<div class="section-title">
						<span class="title-bg">Clients</span>
						<h1>Our Partners</h1>
						<p>Sed lorem enim, faucibus at erat eget, laoreet tincidunt tortor. Ut sed mi nec ligula bibendum aliquam. Sed scelerisque maximus magna, a vehicula turpis Proin<p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="partners-inner">
						<div class="row no-gutters">
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-1.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-2.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-3.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-4.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-5.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-6.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-7.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-8.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-5.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-6.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-7.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
							<!-- Single Partner -->
							<div class="col-lg-2 col-md-3 col-12">
								<div class="single-partner">
									<a href="#" target="_blank"><img src="http://themelamp.com/templates/radix/images/partner-3.png" alt="#"></a>
								</div>
							</div>
							<!--/ End Single Partner -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/ End Partners -->
@endsection

