@extends('xhome.layout.site')
@section('title', isset($category->title) ?$category->title : 'Tin tức' )
@section('meta_description',  isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($category->title) ?$category->title : 'Tin tức')
@section('dynx_itemid', isset($category->category_id) ?$category->category_id : '88')
@section('dynx_pagetype', 'searchresults')
@section('dynx_totalvalue', '0')
@section('content')


<section class="moreProduct bgrGray listNews">
   <div class="container">
      <div class="link">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="#">{{ isset($category->title) ?$category->title : 'Tin tức' }}</a></li>
         </ol>
      </div>
      <div class="title">
         <h2 class="gray titl">{{ isset($category->title) ?$category->title : 'Tin tức' }}</h2>
      </div>
   </div>
   <div class="container">
      <div class="row">
        @foreach($posts as $post) 
         <div class="col-md-3 col-sm-6 col-xs-12 mgb20">
            <div class="listcate item">

               <div class="img">
                  <div class="CropImg CropImgFull">
                     <a href="/{{ isset($category->slug) ?$category->slug : 'tin-tuc' }}/{{ $post->slug }}" class="noDecoration thumbs">
                        <img src="{{ isset($post['image']) ?  $post['image'] : '' }}"
                             alt="{{ isset($post['title']) ?  $post['title'] : '' }}">
                     </a>
                  </div>
               </div>
               <div class="newContent">
               <a class="black" href="/{{ isset($category->slug) ?$category->slug : 'tin-tuc' }}/{{ $post->slug }}" >
			   <p class="CutText2 mg0">{{ $post['title'] }}</p>
			   </a>
               </div>
            </div>
         </div>
         @endforeach
      </div>
	  {{ $posts->links() }}
   </div>
</section>
@endsection
