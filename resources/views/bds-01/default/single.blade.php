@extends('xhome.layout.site')

@section('title', isset($post->title) ? $post->title : '')
@section('meta_description', !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', !empty($post->meta_keyword) ? $post->meta_keyword : '') 
@section('meta_url', $domainUrl.'/tin-tuc/'.$post->slug) 
@section('meta_image', !empty($post->image) ? asset($post->image) : '')

@push('scriptHeader')
	<script async defer type="application/ld+json">
		{
		  "@context": "https://schema.org",
		  "@type": "NewsArticle",
		  "mainEntityOfPage": {
			"@type": "WebPage",
			"@id": "{!! $domainUrl !!}/danh-muc/tin-tuc/{!! $post->slug !!}"
		  },
		  "headline": "{!! isset($post->title) ? $post->title : $domainUrl !!}",
		  "image": [
			"{!! !empty($post->image) ? asset($post->image) : '' !!}"
		   ],
		  "datePublished": "{!! $post->created_at !!}",
		  "dateModified": "{!! $post->updated_at !!}",
		  "author": {
			"@type": "Person",
			"name": "{!! $domainUrl !!}"
		  },
		   "publisher": {
			"@type": "Organization",
			"name": "{!! $domainUrl !!}",
			"logo": {
			  "@type": "ImageObject",
			  "url": "{!! $domainUrl !!}/{{ isset($information['logo']) ?  $information['logo'] : '' }}"
			}
		  },
		  "description": "{!! !empty($post->meta_description) ? $post->meta_description : $post->description !!}"
		}
    </script>
@endpush
@section('content')
<section class="moreProduct bgrGray pdb20">
        <div class="container">
            <div class="link">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="/danh-muc/{{ isset($category->slug) ? $category->slug : 'Tin tức' }}">{{  isset($category->title) ? $category->title : 'Tin tức' }}</a></li>
                <li class="breadcrumb-item"><a href="#">{{$post->title}}</a></li>
              </ol>
            </div>
			
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<div class="News bgrWhite pd15">
						<div class="title">
							<h1>{{$post->title}}</h1>
							<div id="loadShare">
							</div>
							<script>
								$(window).load(function() { 
									$("#loadShare").html('<iframe src="https://www.facebook.com/plugins/share_button.php?href={!! $domainUrl.'/tin-tuc/'.$post->slug !!}&layout=button_count&size=large&appId=1875296572729968&width=138&height=28" width="138" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>');
								});
							 </script>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-12">
								
								<div class="contentNews" id="parentScroller">
									
									{!!$post->content!!}
									
									@if(empty($post['ofin-form']))
									<div class="form_moma col-md-6 col-xs-12">

										<?php $form =  App\Entity\OptinForm::showWithId($post->form_id);
										?>

										@if (!empty($post->form_id))
											<form class="wpcf7-form" onsubmit="return contact(this);" method="post" action="/submit/contact" >
												{!! csrf_field() !!}
												<input type="hidden" name="is_json"
													   class="form-control captcha" value="1" placeholder="">
											
												@if($form->showName == 1)
													<div class="form-group">
														<div>
															<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
														</div>
													</div>
												@endif

												@if($form->showEmail == 1)
													<div class="form-group">
														<div>
															<input type="email" class="form-control" placeholder="Email của bạn" name="email">
														</div>
													</div>
												@endif

												@if($form->showPhone == 1)
													<div class="form-group">
														<div>
															<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
														</div>
													</div>
												@endif

												@if($form->showAddress == 1)
													<div class="form-group">
														<div>
															<input type="text" class="form-control" placeholder="Địa chỉ" name="address" required="">
														</div>
													</div>
												@endif

												@if($form->showDescription == 1)
													<div class="form-group">
														<div>
											<textarea class="form-control" rows="5" name="message"
													  placeholder="Nội dung ghi chú"></textarea>
														</div>
													</div>
												@endif
												<input type="hidden" value="{{$form->campaign_id}}" name="campaign_id">

												<input type="hidden" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}/tin-tuc/{{ $post->slug }}" name="utm_source" />

												<input type="hidden" value="{{isset($form->formRedirect) ? $form->formRedirect : ''}}" name="formRedirect" />

												@if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
													<div class="form-group">
														<div class="">
															<button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
														</div>
													</div>
												@endif
											</form>
										@else
											<form class="wpcf7-form" onsubmit="return contact(this);" method="post" action="/submit/contact" >
												{!! csrf_field() !!}
												<input type="hidden" name="is_json"
													   class="form-control captcha" value="1" placeholder="">
												<input type="hidden" name="post_id" value="{{$post->post_id}}">
												<div class="form-group">
													<div>
														<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
													</div>
												</div>
												<div class="form-group">
													<div>
														<input type="email" class="form-control" placeholder="Email của bạn" name="email">
													</div>
												</div>
												<div class="form-group">
													<div>
														<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
													</div>
												</div>
												<div class="form-group">
													<div>
														<input type="text" class="form-control" placeholder="Địa chỉ" name="address" required="">
													</div>
												</div>
												<div class="form-group">
													<div>
											<textarea class="form-control" rows="5" name="message"
													  placeholder="Nội dung ghi chú"></textarea>
													</div>
												</div>

												<input type="hidden" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}/tin-tuc/{{ $post->slug }}" name="utm_source" />

												<input type="hidden" value="{{isset($form->formRedirect) ? $form->formRedirect : ''}}" name="formRedirect" />
												<div class="form-group">
													<div class="">
														<button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
													</div>
												</div>
											</form>

										@endif

									</div>

									<div>
									@endif
										{!! isset($post['ofin-form']) ? $post['ofin-form'] : '' !!}
									</div>
								</div>
							</div>
						</div>
						
						<p class="mgb0 f24 fw6">Bình luận</p>
						<div id="fb-root"></div>
						<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=1875296572729968&autoLogAppEvents=1"></script>
						<div class="fb-comments" data-href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" data-width="100%" data-numposts="5"></div>

					</div>
				</div>

				<div class="col-xs-12 col-md-4">
					<?php
						$domain = App\Ultility\Ultility::getCurrentHttpHost();
						$userId = App\Entity\Domain::getUserIdWithUrlLike($domain);
						$user = App\Entity\User::getUserByUserId($userId['user_id']);
					?>
						@if (isset($information['nhung-moi-group-facebook']) )
						<div class="News bgrWhite pd15 " id="scroller" >
							<div class="title">
								<h2 style="font-size: 22px;">CÓ THỂ BẠN QUAN TÂM</h2>
							</div>

							<div  class="contentNews" id="groupFacebookEmbbed">
								
							</div>
							<script>
								$(window).load(function() { 
									$("#groupFacebookEmbbed").html('{!! $information['nhung-moi-group-facebook'] !!}');
								});
							 </script>
						</div>
						@elseif(!empty($user) && $user->vip < 1)
						<div class="News bgrWhite pd15" id="scroller">
							<div class="title">
								<h2 style="font-size: 22px;">CÓ THỂ BẠN QUAN TÂM</h2>
							</div>
							<div  class="contentNews">
								<table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;"><tr style=""><td height="28" style="line-height:28px;">&nbsp;</td></tr><tr><td style=""><table border="0" width="280" cellspacing="0" cellpadding="0" style="border-collapse:separate;background-color:#ffffff;border:1px solid #dddfe2;border-radius:3px;font-family:Helvetica, Arial, sans-serif;margin:0px auto;"><tr style="padding-bottom: 8px;"><td style=""><img class="img" src="https://scontent-hkt1-1.xx.fbcdn.net/v/t1.0-0/c0.0.1146.302a/p600x600/90057370_2716318728600631_8501594635209015296_o.jpg?_nc_cat=108&amp;_nc_sid=ca434c&amp;_nc_ohc=MwwVyBZLM6MAX8VPPB0&amp;_nc_ht=scontent-hkt1-1.xx&amp;oh=4ea48267401cd38bf8abfe34c7ba804c&amp;oe=5EB20380" width="280" height="146" alt="" /></td></tr><tr><td style="font-size:14px;font-weight:bold;padding:8px 8px 0px 8px;text-align:center;">Website Miễn Phí - Moma.vn</td></tr><tr><td style="color:#90949c;font-size:12px;font-weight:normal;text-align:center;">Public group · 3,674 members</td></tr><tr><td style="padding:8px 12px 12px 12px;"><table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;width:100%;"><tr><td style="background-color:#4267b2;border-radius:3px;text-align:center;"><a style="color:#3b5998;text-decoration:none;cursor:pointer;width:100%;" href="https://www.facebook.com/plugins/group/join/popup/?group_id=663394590433585&amp;source=email_campaign_plugin" target="_blank" rel="noopener"><table border="0" cellspacing="0" cellpadding="3" align="center" style="border-collapse:collapse;"><tr><td style="border-bottom:3px solid #4267b2;border-top:3px solid #4267b2;color:#FFF;font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;">Tham gia nhóm</td></tr></table></a></td></tr></table></td></tr><tr><td style="border-top:1px solid #dddfe2;font-size:12px;padding:8px 12px;">DẬY THIẾT KẾ SITE GOOGLE, BLOGSPOT, WORDPRESS. , SEO  .CHIA SẺ SÁCH CÁC NGÀNH HỌC MIỄN PHÍ CHO CÁC BẠN YÊU THÍCH ĐỌC SÁCH.CHIA SẺ KIẾN THỨC VỀ CNTT</td></tr></table></td></tr><tr style=""><td height="28" style="line-height:28px;">&nbsp;</td></tr></table>
							</div>
						</div>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-12">
					<div class="News bgrWhite pd15 mgt30">
						<div class="title">
							<h2 style="font-size: 22px; text-align: left;">BÀI VIẾT LIÊN QUAN <i>"{!! $post->title !!}"</i></h2>
							<hr>
						</div>
						<div  class="contentNews">
							<ul class="list-group list-group-flush">
								@if (count(\App\Entity\Post::relativeProduct($post->slug, 5)->toArray()) > 0 ) 
									@foreach (\App\Entity\Post::relativeProduct($post->slug, 5) as $postRelative)
										<li class="list-group-item"><a href="/tin-tuc/{!!  $postRelative->slug !!}" style="color: black;"><i class="far fa-hand-point-right" style="color: red;"></i> {!!  $postRelative->title !!}</a></li>
									@endforeach
								@else
									@foreach (\App\Entity\Post::newPostIndex(5) as $postRelative)
										<li class="list-group-item"><a href="/tin-tuc/{!!  $postRelative->slug !!}" style="color: black;"><i class="far fa-hand-point-right" style="color: red;"></i> {!!  $postRelative->title !!}</a></li>
									@endforeach
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
        </div>
    </section>
	
@include('xhome.partials.postVip')
	<script async defer>
        $(document).ready(function(){
            $(window).scroll(function () {
                var w = window.innerWidth;
                if (w > 767 && $("#parentScroller").height() > 400) {
                    $("#scroller").stick_in_parent({offset_top: 75});
                }
            });

        });
    </script>
    @include('xhome.partials.slideCustomer')
@endsection
