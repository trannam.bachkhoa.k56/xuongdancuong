@extends('xhome.layout.site')

@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('content')
<section class="contact bgrGray pdb20">
        <div class="container">
            <div class="link">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="#">Liên hệ</a></li>
              </ol>
            </div>
            <div class="title">
                <h2 class="gray titl">Thông Tin Liên Hệ</h2>
              </div>
            <div class="contactContent bgrWhite pd15 mgb20">
              <div class="map">
                {!! isset($information['map']) ?  $information['map'] : '' !!}
              </div>
            </div>
            <div class="contactCompany bgrWhite pdl15 pdr15 pdb20">
              <div class="row">
                <div class="col-md-6">
                  <div class="title">
                    <h2 class="titl">THÔNG TIN DOANH NGHIỆP</h2>
                  </div>
                  {!! isset($information['thong-tin-lien-he']) ?  $information['thong-tin-lien-he'] : '' !!}
                </div>

                <div class="col-md-6">
                  <div class="title">
                    <h2 class="titl">Gửi liên hệ cho chúng tôi</h2>
                  </div>
				  <div id="scroller">
					<form onSubmit="return contact(this);" class="wpcf7-form" method="post"
						  action="/submit/contact" >
						{!! csrf_field() !!}
						<input type="hidden" name="is_json"
							   class="form-control captcha" value="1" placeholder="">

						@if(!isset($customerDisplay->statusName) || $customerDisplay->statusName != 0)
						<div class="form-group">
							<div>
								<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
							</div>
						</div>
						@endif
						
						@if(!isset($customerDisplay->statusEmail) || $customerDisplay->statusEmail != 0)
						<div class="form-group">
							<div>
								<input type="email" class="form-control" placeholder="Email của bạn" name="email">
							</div>
						</div>
						@endif

						@if(!isset($customerDisplay->statusPhone) || $customerDisplay->statusPhone != 0)
						<div class="form-group">
							<div>
								<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
							</div>
						</div>
						@endif

						@if(!isset($customerDisplay->statusMessage) ||  $customerDisplay->statusMessage != 0)
						<div class="form-group">
							<div>
							<textarea class="form-control" rows="2" name="message"
									  placeholder="Nội dung ghi chú"></textarea>
							</div>
						</div>
						@endif
						@if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
							<input type="hidden" value="{{ $_SERVER['HTTP_REFERER'] }}" name="utm_source" />
						@endif

						@if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
						<div class="form-group">
							<div class="center">
								<button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
							</div>
						</div>
						@endif
					</form>
				  </div>
                
                </div>

              </div>
            </div>
        </div>      
    </section>

    @include('xhome.partials.slideCustomer')
@endsection