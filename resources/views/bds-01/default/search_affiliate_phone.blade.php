@extends('xhome.layout.site')
@section('title', 'Tra cứu tiền hoa hồng của bạn')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('content')
    <section class="content pdt20">
        <div class="container">
            <div class="title">
                <h2 class="colortl titl">Tra cứu số điện thoại của bạn</h2>
            </div>
            <div class="row">
                <div class="col-12 col-lg-12 pd10">
                    <form action="{!! route('search_affiliate_form_phone') !!}" method="get">
                    <div class=row>
                        <div class="col-xs-offset-0 offset-md-2 col-xs-12 col-md-10" >
                            <div class="form-group">
                                <input type="text" value="" class="form-control" name="phone" placeholder="Nhập số điện thoại để tra cứu số tiền kiếm được"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success">Tra cứu</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
