@extends('bds-01.layout.site')
@section('title', isset($information['tieu-de-tren-google']) ? $information['tieu-de-tren-google'] : '')
@section('meta_description', isset($information['mo-ta-tren-google']) ? $information['mo-ta-tren-google'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'home')
@section('dynx_totalvalue', '0')

@section('content')
        <!-- slide đầu trang -->
        @include ('bds-01.block.slide')

        <!-- thông tin dự án -->
        @include ('bds-01.block.information')

        <!-- tiện ích dự án -->
        @include ('bds-01.block.ultility')

        <!-- Mặt bằng tổng thể dự án -->
        @include ('bds-01.block.area')

        <!-- giá bán tổng thể dự án -->
        @include ('bds-01.block.cost')

        <!-- Ưu đãi và triết khấu mua nhà -->
        @include ('bds-01.block.discount')

        <!-- chủ đầu tư-->
        @include ('bds-01.block.boss')

        <!-- liên hệ -->
        @include ('bds-01.block.content')

        <!-- cuối trang -->
        @include ('bds-01.block.footer')

@endsection
