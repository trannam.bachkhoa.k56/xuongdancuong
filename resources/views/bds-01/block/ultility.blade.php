<div id="tienich">
    <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
         style='background-color: #f5f5f5;background-position: left top;background-repeat: repeat;padding-top:40px;padding-right:2px;padding-bottom:0px;padding-left:2px;'>
        <div class="fusion-builder-row fusion-row ">
            
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                 style='margin-top:0px;margin-bottom:10px;'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text"><p style="text-align: center;"><span
                                    style="color: #000000;"><span
                                        style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">ĐƯỢC PHÁT TRIỂN THEO MÔ HÌNH THÀNH PHỐ THÔNG MINH</span></span><br/>
                            <strong><span style="color: #008c7d;"><span
                                            style="font-family: 'times new roman', times, serif; font-size: x-large;"><span
                                                style="caret-color: #000000;">VỚI HỆ THỐNG TIỆN ÍCH ĐỒNG BỘ</span></span></span></strong>
                        </p>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>

            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 10% ) * 0.5 ) );margin-right: 10%;'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-title title fusion-title-size-one"
                         style="margin-top:0px;margin-bottom:0px;"><h1
                                class="title-heading-left"><span
                                    style="color: #008c7d;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">DANH SÁCH TIỆN ÍCH</span></strong></span>
                        </h1>
                        <div class="title-sep-container">
                            <div class="title-sep sep-single sep-dashed"
                                 style="border-color:#e2b95d;"></div>
                        </div>
                    </div>

                    <div class="fusion-text">
                        <p>
                            @foreach(\App\Entity\SubPost::showSubPost('tien-ich-du-an', 2) as $id => $ultility)
                            <span style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;"><strong><span
                                            style="color: #800000;">({!! ($id+1) !!})</span>
                            </strong> {!! $ultility->title !!}</span>
                            <br/>
                            @endforeach
                        </p>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>

            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 10% ) * 0.5 ) );'>
                <div class="fusion-column-wrapper"
                     style="padding: 20px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text">
                        <p>
                        @foreach(\App\Entity\SubPost::showSubPost('tien-ich-du-an', 5, 'asc') as $id => $ultility)
                            @if ($id > 1)
                                <span  style="font-family: arial, helvetica, sans-serif; color: #000000; font-size: 12pt;">
                                <strong><span style="color: #800000;">({!! ($id+1) !!})</span></strong> 
                                {{ $ultility->title }}</span>
                                <br/>
                            @endif
                        @endforeach
                        </p>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
			@php $idx = 0; @endphp
            @foreach(\App\Entity\SubPost::showSubPost('tien-ich-du-an', 5) as $id => $ultility)
                @if (!empty($ultility->image))
                    <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 10% ) * 0.5 ) );'>
                <div class="fusion-column-wrapper"
                     style="padding: 20px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                            <div class="imageframe-liftup">
                                <span class="fusion-imageframe imageframe-none imageframe-3"><img
                                    src="{{ $ultility->image }}"
                                    width="1000" height="551" alt="" title="Vườn-Nhật"
                                    class="img-responsive wp-image-1715"
                                    sizes="(max-width: 800px) 100vw, 1000px"/></span>
                            </div>
                            <div class="fusion-clearfix"></div>

                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
