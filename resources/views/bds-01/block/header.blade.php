<header class="fusion-header-wrapper">
<div class="fusion-header-v2 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-  fusion-mobile-menu-design-flyout fusion-header-has-flyout-menu">

    <div class="fusion-secondary-header">
        <div class="fusion-row">
            <div class="fusion-alignleft">
                <div class="fusion-contact-info">Website chính thức dự án | Hotline1: {!! isset($information['hotline1']) ? $information['hotline1'] : '' !!} | Hotline2: {!! isset($information['hotline2']) ? $information['hotline2'] : '' !!}
                    <span class="fusion-header-separator">|</span><a
                            href="mailto:in&#102;o&#64;tnr&#103;ra&#110;&#100;p&#97;&#108;&#97;&#99;e&#116;ha&#105;&#98;in&#104;.&#99;&#111;m">in&#102;o&#64;tnr&#103;ra&#110;&#100;p&#97;&#108;&#97;&#99;e&#116;ha&#105;&#98;in&#104;.&#99;&#111;m</a>
                </div>
            </div>
            <div class="fusion-alignright">
                <div class="fusion-social-links-header">
                    <div class="fusion-social-networks">
                        <div class="fusion-social-networks-wrapper"><a
                                    class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook"
                                    style="color:#e2b95d;"
                                    href="{!! isset($information['facebook']) ? $information['facebook'] : '' !!}" target="_blank"
                                    data-placement="bottom" data-title="Facebook" data-toggle="tooltip"
                                    title="Facebook"><span class="screen-reader-text">Facebook</span></a>
                                    <a
                                    class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube"
                                    style="color:#e2b95d;" href="{!! isset($information['youtube']) ? $information['youtube'] : '' !!}" target="_blank"
                                    rel="noopener noreferrer" data-placement="bottom" data-title="YouTube"
                                    data-toggle="tooltip" title="YouTube"><span
                                        class="screen-reader-text">YouTube</span></a>
                                    <a
                                    class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail"
                                    style="color:#e2b95d;"
                                    href="mailto:{!! isset($information['email']) ? $information['email'] : '' !!}"
                                    target="_self" rel="noopener noreferrer" data-placement="bottom" data-title="Email"
                                    data-toggle="tooltip" title="Email"><span
                                        class="screen-reader-text">Email</span></a><a
                                    class="custom fusion-social-network-icon fusion-tooltip fusion-custom fusion-icon-custom"
                                    style="color:#e2b95d;position:relative;" href="{!! isset($information['zalo']) ? $information['zalo'] : '' !!}"
                                    target="_blank" rel="noopener noreferrer" data-placement="bottom" data-title
                                    data-toggle="tooltip" title><span class="screen-reader-text"></span><img
                                        src="/bds-01/wp-content/uploads/2019/11/logo-zalo-75x75.jpg"
                                        style="width:auto;max-height:16px;" alt=""/></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fusion-header-sticky-height"></div>
    <div class="fusion-header">
        <div class="fusion-row">
            <div class="fusion-header-has-flyout-menu-content">
                <div class="fusion-logo" data-margin-top="1px" data-margin-bottom="1px" data-margin-left="0px"
                     data-margin-right="0px">
                    <a class="fusion-logo-link" href="/">

                        <!-- standard logo -->
                        <img src="{!! isset($information['logo']) ? asset($information['logo']) : '' !!}"

                             width="99" height="50" alt="TNR Holdings Việt Nam Logo" retina_logo_url=""
                             class="fusion-standard-logo"/>


                    </a>
                </div>
                <nav class="fusion-main-menu" aria-label="Main Menu">
                    <ul role="menubar" id="menu-menu-trang-chu" class="fusion-menu">
                        <li role="menuitem" id="menu-item-1784"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1679 current_page_item menu-item-1784">
                            <a href="/" class="fusion-flex-link fusion-bar-highlight"><span
                                        class="fusion-megamenu-icon"><i
                                            class="glyphicon fa-accusoft fab"></i></span><span class="menu-text">TRANG CHỦ</span></a>
                        </li>
                        <li role="menuitem" id="menu-item-1023"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1023"><a
                                    href="/#tienich" class="fusion-flex-link fusion-bar-highlight"><span
                                        class="fusion-megamenu-icon"><i class="glyphicon fa-heart fas"></i></span><span
                                        class="menu-text">TIỆN ÍCH</span></a></li>
                        <li role="menuitem" id="menu-item-395"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-395"><a
                                    href="/#matbang" class="fusion-flex-link fusion-bar-highlight"><span
                                        class="fusion-megamenu-icon"><i class="glyphicon fa-cube fas"></i></span><span
                                        class="menu-text">MẶT BẰNG</span></a></li>
                        <li role="menuitem" id="menu-item-400"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-400"><a
                                    href="/#sanpham" class="fusion-flex-link fusion-bar-highlight"><span
                                        class="fusion-megamenu-icon"><i
                                            class="glyphicon fa-chart-line fas"></i></span><span class="menu-text">GIÁ BÁN</span></a>
                        </li>
                        <li role="menuitem" id="menu-item-397"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-397"><a
                                    href="/#chinhsach" class="fusion-flex-link fusion-bar-highlight"><span
                                        class="fusion-megamenu-icon"><i class="glyphicon fa-donate fas"></i></span><span
                                        class="menu-text">ƯU ĐÃI, CHIẾT KHẤU</span></a></li>
                        <li role="menuitem" id="menu-item-1025"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1025"><a
                                    href="/#lienhe" class="fusion-flex-link fusion-bar-highlight"><span
                                        class="fusion-megamenu-icon"><i
                                            class="glyphicon fa-user-tie fas"></i></span><span
                                        class="menu-text">LIÊN HỆ</span></a></li>
                        <li role="menuitem" id="menu-item-1791"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1791">
                            <a href="index.html#chudautu" class="fusion-bar-highlight"><span class="menu-text">CHỦ ĐẦU TƯ</span></a>
                        </li>
                    </ul>
                </nav>
                <div class="fusion-flyout-menu-icons fusion-flyout-mobile-menu-icons">
                    <a class="fusion-flyout-menu-toggle" aria-hidden="true" aria-label="Toggle Menu" href="index.html#">
                        <div class="fusion-toggle-icon-line"></div>
                        <div class="fusion-toggle-icon-line"></div>
                        <div class="fusion-toggle-icon-line"></div>
                    </a>
                </div>


                <div class="fusion-flyout-menu-bg"></div>

                <nav class="fusion-mobile-nav-holder fusion-flyout-menu fusion-flyout-mobile-menu"></nav>

            </div>
        </div>
    </div>
</div>
<div class="fusion-clearfix"></div>
</header>
