<div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"
     style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;'>
    <div class="fusion-builder-row fusion-row ">
        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
             style='margin-top:0px;margin-bottom:0px;'>
            <div class="fusion-column-wrapper"
                 style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                 data-bg-url="">
                <div class="fusion-text">
                    <p>
                        <img src="{!! isset($information['anh-bia-trang-chu']) ? $information['anh-bia-trang-chu'] : '' !!}" alt="ảnh bìa mô tả dự án"/>
                    </p>
                </div>
                <div class="fusion-clearfix"></div>
            </div>
        </div>
    </div>
</div>
