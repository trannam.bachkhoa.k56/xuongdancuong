<div id="tongquan">
    <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
         style='background-color: rgba(255,255,255,0);background-image: url("/bds-01/wp-content/uploads/2020/06/nen-tong-quan-2.png");background-position: right top;background-repeat: no-repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
        <div class="fusion-builder-row fusion-row ">
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 22px;background-image: url('/bds-01/wp-content/uploads/2020/06/Viụ2581n-tong-quan-1-1.png');background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="/bds-01/wp-content/uploads/2020/06/Viền-tong-quan-1-1.png">
                    <div class="fusion-text"><p>
                        <a href="index.html">
                            <span
                                        style="color: #000000; font-family: times new roman, times, serif; font-size: x-large;"><span
                                            style="caret-color: #000000;">
                                            <b>{!! isset($information['ten-du-an']) ? $information['ten-du-an'] : '' !!}</b>
                                            </span></span></a><br/>
                                                    <span style="color: #009485;"><strong><em><span
                                                                        style="font-size: 24px;">Thông tin tổng quan dự án</span></em></strong></span><br/>
                            <img class="alignnone size-full wp-image-860"
                                 src="/bds-01/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg" alt=""
                                 width="162" height="3"
                                 srcset="/bds-01//wp-content/uploads/2020/01/gach-chann-duoi-1-150x3.jpg 150w, /bds-01/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg 162w"
                                 sizes="(max-width: 162px) 100vw, 162px"/></p>
                    </div>
                    <div class="fusion-text">
                        @foreach(\App\Entity\SubPost::showSubPost('thong-tin-du-an', 1,  $sort = 'asc') as $id => $inforProject)
                            {!! isset($inforProject->content) ? $inforProject->content : '' !!}
                        @endforeach
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                <div class="fusion-column-wrapper"
                     style="padding: 15px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text">
                        <div class="n2-section-smartslider fitvidsignore " role="region"
                             aria-label="Slider">
                            <style>div#n2-ss-7 {
                                    width: 1000px;
                                }

                                div#n2-ss-7 .n2-ss-slider-1 {
                                    position: relative;
                                }

                                div#n2-ss-7 .n2-ss-slider-background-video-container {
                                    position: absolute;
                                    left: 0;
                                    top: 0;
                                    width: 100%;
                                    height: 100%;
                                    overflow: hidden;
                                }

                                div#n2-ss-7 .n2-ss-slider-2 {
                                    position: relative;
                                    overflow: hidden;
                                    padding: 0px 0px 0px 0px;
                                    height: 900px;
                                    border: 0px solid RGBA(62, 62, 62, 1);
                                    border-radius: 0px;
                                    background-clip: padding-box;
                                    background-repeat: repeat;
                                    background-position: 50% 50%;
                                    background-size: cover;
                                    background-attachment: scroll;
                                }

                                div#n2-ss-7.n2-ss-mobileLandscape .n2-ss-slider-2, div#n2-ss-7.n2-ss-mobilePortrait .n2-ss-slider-2 {
                                    background-attachment: scroll;
                                }

                                div#n2-ss-7 .n2-ss-slider-3 {
                                    position: relative;
                                    width: 100%;
                                    height: 100%;
                                    overflow: hidden;
                                    outline: 1px solid rgba(0, 0, 0, 0);
                                    z-index: 10;
                                }

                                div#n2-ss-7 .n2-ss-slide-backgrounds, div#n2-ss-7 .n2-ss-slider-3 > .n-particles-js-canvas-el, div#n2-ss-7 .n2-ss-slider-3 > .n2-ss-divider {
                                    position: absolute;
                                    left: 0;
                                    top: 0;
                                    width: 100%;
                                    height: 100%;
                                }

                                div#n2-ss-7 .n2-ss-slide-backgrounds {
                                    z-index: 10;
                                }

                                div#n2-ss-7 .n2-ss-slider-3 > .n-particles-js-canvas-el {
                                    z-index: 12;
                                }

                                div#n2-ss-7 .n2-ss-slide-backgrounds > * {
                                    overflow: hidden;
                                }

                                div#n2-ss-7 .n2-ss-slide {
                                    position: absolute;
                                    top: 0;
                                    left: 0;
                                    width: 100%;
                                    height: 100%;
                                    z-index: 20;
                                    display: block;
                                    -webkit-backface-visibility: hidden;
                                }

                                div#n2-ss-7 .n2-ss-layers-container {
                                    position: relative;
                                    width: 1000px;
                                    height: 900px;
                                }

                                div#n2-ss-7 .n2-ss-parallax-clip > .n2-ss-layers-container {
                                    position: absolute;
                                    right: 0;
                                }

                                div#n2-ss-7 .n2-ss-slide {
                                    perspective: 1500px;
                                }

                                div#n2-ss-7[data-ie] .n2-ss-slide {
                                    perspective: none;
                                    transform: perspective(1500px);
                                }

                                div#n2-ss-7 .n2-ss-slide-active {
                                    z-index: 21;
                                }

                                div#n2-ss-7 .nextend-arrow {
                                    cursor: pointer;
                                    overflow: hidden;
                                    line-height: 0 !important;
                                    z-index: 20;
                                }

                                div#n2-ss-7 .nextend-arrow img {
                                    position: relative;
                                    min-height: 0;
                                    min-width: 0;
                                    vertical-align: top;
                                    width: auto;
                                    height: auto;
                                    max-width: 100%;
                                    max-height: 100%;
                                    display: inline;
                                }

                                div#n2-ss-7 .nextend-arrow img.n2-arrow-hover-img {
                                    display: none;
                                }

                                div#n2-ss-7 .nextend-arrow:HOVER img.n2-arrow-hover-img {
                                    display: inline;
                                }

                                div#n2-ss-7 .nextend-arrow:HOVER img.n2-arrow-normal-img {
                                    display: none;
                                }

                                div#n2-ss-7 .nextend-arrow-animated {
                                    overflow: hidden;
                                }

                                div#n2-ss-7 .nextend-arrow-animated > div {
                                    position: relative;
                                }

                                div#n2-ss-7 .nextend-arrow-animated .n2-active {
                                    position: absolute;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-fade {
                                    transition: background 0.3s, opacity 0.4s;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-horizontal > div {
                                    transition: all 0.4s;
                                    left: 0;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-horizontal .n2-active {
                                    top: 0;
                                }

                                div#n2-ss-7 .nextend-arrow-previous.nextend-arrow-animated-horizontal:HOVER > div, div#n2-ss-7 .nextend-arrow-previous.nextend-arrow-animated-horizontal:FOCUS > div, div#n2-ss-7 .nextend-arrow-next.nextend-arrow-animated-horizontal .n2-active {
                                    left: -100%;
                                }

                                div#n2-ss-7 .nextend-arrow-previous.nextend-arrow-animated-horizontal .n2-active, div#n2-ss-7 .nextend-arrow-next.nextend-arrow-animated-horizontal:HOVER > div, div#n2-ss-7 .nextend-arrow-next.nextend-arrow-animated-horizontal:FOCUS > div {
                                    left: 100%;
                                }

                                div#n2-ss-7 .nextend-arrow.nextend-arrow-animated-horizontal:HOVER .n2-active, div#n2-ss-7 .nextend-arrow.nextend-arrow-animated-horizontal:FOCUS .n2-active {
                                    left: 0;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-vertical > div {
                                    transition: all 0.4s;
                                    top: 0;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-vertical .n2-active {
                                    left: 0;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-vertical .n2-active {
                                    top: -100%;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-vertical:HOVER > div, div#n2-ss-7 .nextend-arrow-animated-vertical:FOCUS > div {
                                    top: 100%;
                                }

                                div#n2-ss-7 .nextend-arrow-animated-vertical:HOVER .n2-active, div#n2-ss-7 .nextend-arrow-animated-vertical:FOCUS .n2-active {
                                    top: 0;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default {
                                    display: flex;
                                    user-select: none;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default, div#n2-ss-7 .nextend-thumbnail-inner {
                                    overflow: hidden;
                                }

                                div#n2-ss-7 .nextend-thumbnail-inner {
                                    flex: 1 1 auto;
                                    display: flex;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-inner {
                                    flex-flow: column;
                                }

                                div#n2-ss-7 .n2-ss-thumb-image {
                                    display: flex;
                                    flex: 0 0 auto;
                                    align-items: center;
                                    justify-content: center;
                                    background-position: center center;
                                    background-size: cover;
                                }

                                div#n2-ss-7 .nextend-thumbnail-button {
                                    position: absolute;
                                    z-index: 2;
                                    transition: all 0.4s;
                                    opacity: 0;
                                    cursor: pointer;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-button {
                                    left: 50%;
                                    margin-left: -13px !important;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-previous {
                                    top: -26px;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-previous.n2-active {
                                    top: 10px;
                                    opacity: 1;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-next {
                                    bottom: -26px;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-next.n2-active {
                                    bottom: 10px;
                                    opacity: 1;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-button {
                                    top: 50%;
                                    margin-top: -13px !important;
                                    transform: rotateZ(-90deg);
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-previous {
                                    left: -26px;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-previous.n2-active {
                                    left: 10px;
                                    opacity: 1;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-next {
                                    right: -26px;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-next.n2-active {
                                    right: 10px;
                                    opacity: 1;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller {
                                    flex: 1 0 auto;
                                    position: relative;
                                    box-sizing: border-box !important;
                                    white-space: nowrap;
                                    display: flex;
                                    flex-direction: column;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-scroller {
                                    flex-direction: column;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .nextend-thumbnail-scroller {
                                    flex-direction: row;
                                }

                                div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-start .nextend-thumbnail-scroller-group {
                                    justify-content: flex-start;
                                }

                                div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-center .nextend-thumbnail-scroller-group {
                                    justify-content: center;
                                }

                                div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-end .nextend-thumbnail-scroller-group {
                                    justify-content: flex-end;
                                }

                                div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-space-around .nextend-thumbnail-scroller-group {
                                    justify-content: space-around;
                                }

                                div#n2-ss-7 .nextend-thumbnail-scroller.n2-align-content-space-between .nextend-thumbnail-scroller-group {
                                    justify-content: space-between;
                                }

                                html[dir="rtl"] div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller {
                                    position: relative;
                                    float: right;
                                }

                                div#n2-ss-7 .nextend-thumbnail-scroller-group {
                                    display: flex;
                                    flex-flow: column;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-scroller-group {
                                    display: flex;
                                    flex-flow: row;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller .nextend-thumbnail-scroller-group > div {
                                    display: flex;
                                    position: relative;
                                    flex: 0 0 auto;
                                    box-sizing: content-box !important;
                                    cursor: pointer;
                                    overflow: hidden;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .nextend-thumbnail-scroller .nextend-thumbnail-scroller-group > div {
                                    flex-flow: column;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default .nextend-thumbnail-scroller .nextend-thumbnail-scroller-group > div.n2-active {
                                    cursor: default;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default .n2-ss-caption {
                                    display: inline-block;
                                    white-space: normal;
                                    box-sizing: border-box !important;
                                    overflow: hidden;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default .n2-caption-overlay {
                                    position: absolute;
                                }

                                div#n2-ss-7 .nextend-thumbnail-default .n2-caption-overlay div {
                                    float: left;
                                    clear: left;
                                }

                                div#n2-ss-7 .nextend-thumbnail-horizontal .n2-ss-caption {
                                    display: block;
                                }

                                div#n2-ss-7 .nextend-thumbnail-vertical .n2-caption-after, div#n2-ss-7 .nextend-thumbnail-vertical .n2-caption-before {
                                    height: 100%;
                                }

                                div#n2-ss-7 .n2-style-df32cb3742fdac43eb0251df7d67c185-simple {
                                    background: #242424;
                                    opacity: 1;
                                    padding: 3px 3px 3px 3px;
                                    box-shadow: none;
                                    border-width: 0px;
                                    border-style: solid;
                                    border-color: #000000;
                                    border-color: RGBA(0, 0, 0, 1);
                                    border-radius: 0px;
                                }

                                div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot {
                                    background: RGBA(0, 0, 0, 0);
                                    opacity: 1;
                                    padding: 0px 0px 0px 0px;
                                    box-shadow: none;
                                    border-width: 0px;
                                    border-style: solid;
                                    border-color: #ffffff;
                                    border-color: RGBA(255, 255, 255, 0);
                                    border-radius: 0px;
                                    opacity: 0.4;
                                    margin: 3px;
                                    transition: all 0.4s;
                                    background-size: cover;
                                }

                                div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot.n2-active, div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot:HOVER, div#n2-ss-7 .n2-style-462cb0983aca6cda3fc34a0feea6024b-dot:FOCUS {
                                    border-width: 0px;
                                    border-style: solid;
                                    border-color: #ffffff;
                                    border-color: RGBA(255, 255, 255, 0.8);
                                    opacity: 1;
                                }</style>
                            <div id="n2-ss-7-align" class="n2-ss-align">
                                <div class="n2-padding">
                                    <div id="n2-ss-7" data-creator="Smart Slider 3"
                                         class="n2-ss-slider n2-ow n2-has-hover n2notransition  n2-ss-load-fade "
                                         style="font-size: 1rem;" data-fontsize="16">
                                        <div class="n2-ss-slider-1 n2_ss__touch_element n2-ow"
                                             style="">
                                            <div class="n2-ss-slider-2 n2-ow" style="">
                                                <div class="n2-ss-slider-3 n2-ow" style="">

                                                    <div class="n2-ss-slide-backgrounds"></div>
                                                    @foreach(\App\Entity\SubPost::showSubPost('thong-tin-du-an', 1, $sort = 'asc') as $customer)
                                                        @foreach (explode(',', $customer['slide-anh-thong-tin-du-an']) as $id => $imgInforProject)
                                                            <div @if ($id == 0) data-first="1" @endif data-slide-duration="0"
                                                                data-id="{{ ($id+1) }}"
                                                                data-thumbnail="{!! $imgInforProject !!}"
                                                                style=""
                                                                class=" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-149">
                                                                <div class="n2-ss-slide-background n2-ow"
                                                                    data-mode="fill">
                                                                    <div data-hash="3909c1fb277238a38599bb75f3869ce1"
                                                                        data-desktop="{!! $imgInforProject !!}"
                                                                        class="n2-ss-slide-background-image"
                                                                        data-blur="0" data-alt=""
                                                                        data-title=""></div>
                                                                </div>
                                                                <div class="n2-ss-layers-container n2-ow">
                                                                    <div class="n2-ss-layer n2-ow"
                                                                        style="padding:10px 10px 10px 10px;"
                                                                        data-desktopportraitpadding="10|*|10|*|10|*|10"
                                                                        data-sstype="slide"
                                                                        data-csstextalign="center"
                                                                        data-pm="default"></div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div data-ssleft="0+15"
                                                 data-sstop="sliderHeight/2-previousheight/2"
                                                 id="n2-ss-7-arrow-previous"
                                                 class="n2-ss-widget nextend-arrow n2-ow nextend-arrow-previous  nextend-arrow-animated-fade n2-ib"
                                                 style="position: absolute;" role="button"
                                                 aria-label="previous arrow" tabindex="0"><img
                                                        class="n2-ow" data-no-lazy="1"
                                                        data-hack="data-lazy-src"
                                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxwYXRoIGQ9Ik0xMS40MzMgMTUuOTkyTDIyLjY5IDUuNzEyYy4zOTMtLjM5LjM5My0xLjAzIDAtMS40Mi0uMzkzLS4zOS0xLjAzLS4zOS0xLjQyMyAwbC0xMS45OCAxMC45NGMtLjIxLjIxLS4zLjQ5LS4yODUuNzYtLjAxNS4yOC4wNzUuNTYuMjg0Ljc3bDExLjk4IDEwLjk0Yy4zOTMuMzkgMS4wMy4zOSAxLjQyNCAwIC4zOTMtLjQuMzkzLTEuMDMgMC0xLjQybC0xMS4yNTctMTAuMjkiCiAgICAgICAgICBmaWxsPSIjZmZmZmZmIiBvcGFjaXR5PSIwLjgiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPgo8L3N2Zz4="
                                                        alt="previous arrow"/></div>
                                            <div data-ssright="0+15"
                                                 data-sstop="sliderHeight/2-nextheight/2"
                                                 id="n2-ss-7-arrow-next"
                                                 class="n2-ss-widget nextend-arrow n2-ow nextend-arrow-next  nextend-arrow-animated-fade n2-ib"
                                                 style="position: absolute;" role="button"
                                                 aria-label="next arrow" tabindex="0"><img
                                                        class="n2-ow" data-no-lazy="1"
                                                        data-hack="data-lazy-src"
                                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxwYXRoIGQ9Ik0xMC43MjIgNC4yOTNjLS4zOTQtLjM5LTEuMDMyLS4zOS0xLjQyNyAwLS4zOTMuMzktLjM5MyAxLjAzIDAgMS40MmwxMS4yODMgMTAuMjgtMTEuMjgzIDEwLjI5Yy0uMzkzLjM5LS4zOTMgMS4wMiAwIDEuNDIuMzk1LjM5IDEuMDMzLjM5IDEuNDI3IDBsMTIuMDA3LTEwLjk0Yy4yMS0uMjEuMy0uNDkuMjg0LS43Ny4wMTQtLjI3LS4wNzYtLjU1LS4yODYtLjc2TDEwLjcyIDQuMjkzeiIKICAgICAgICAgIGZpbGw9IiNmZmZmZmYiIG9wYWNpdHk9IjAuOCIgZmlsbC1ydWxlPSJldmVub2RkIi8+Cjwvc3ZnPg=="
                                                        alt="next arrow"/></div>
                                        </div>
                                        <div data-position="below" data-offset="0"
                                             data-width-percent="100"
                                             class="n2-ss-widget nextend-thumbnail nextend-thumbnail-default n2-ow nextend-thumbnail-horizontal"
                                             style="margin-top:0px;width:100%;"><img
                                                    class="nextend-thumbnail-button nextend-thumbnail-previous n2-ow"
                                                    style="width:26px;margin-top:-13px!important;"
                                                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMjYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgICA8ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPgogICAgICAgIDxjaXJjbGUgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIG9wYWNpdHk9Ii41IiBmaWxsPSIjMDAwIiBjeD0iMTMiIGN5PSIxMyIgcj0iMTIiLz4KICAgICAgICA8cGF0aCBkPSJNMTMuNDM1IDkuMTc4Yy0uMTI2LS4xMjEtLjI3LS4xODItLjQzNi0uMTgyLS4xNjQgMC0uMzA2LjA2MS0uNDI4LjE4MmwtNC4zOCA0LjE3NWMtLjEyNi4xMjEtLjE4OC4yNjItLjE4OC40MjQgMCAuMTYxLjA2Mi4zMDIuMTg4LjQyM2wuNjUuNjIyYy4xMjYuMTIxLjI3My4xODIuNDQxLjE4Mi4xNyAwIC4zMTQtLjA2MS40MzYtLjE4MmwzLjMxNC0zLjE2MSAzLjI0OSAzLjE2MWMuMTI2LjEyMS4yNjkuMTgyLjQzMi4xODIuMTY0IDAgLjMwNy0uMDYxLjQzMy0uMTgybC42NjItLjYyMmMuMTI2LS4xMjEuMTg5LS4yNjIuMTg5LS40MjMgMC0uMTYyLS4wNjMtLjMwMy0uMTg5LS40MjRsLTQuMzczLTQuMTc1eiIKICAgICAgICAgICAgICBmaWxsPSIjZmZmIi8+CiAgICA8L2c+Cjwvc3ZnPg=="
                                                    alt="previous arrow"/><img
                                                    class="nextend-thumbnail-button nextend-thumbnail-next n2-ow n2-active"
                                                    style="width:26px;margin-top:-13px!important;"
                                                    src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyNiIgaGVpZ2h0PSIyNiI+CiAgICA8ZyBmaWxsPSJub25lIj4KICAgICAgICA8Y2lyY2xlIGN4PSIxMyIgY3k9IjEzIiByPSIxMiIgZmlsbD0iIzAwMCIgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIG9wYWNpdHk9Ii41Ii8+CiAgICAgICAgPHBhdGggZmlsbD0iI2ZmZiIKICAgICAgICAgICAgICBkPSJNMTIuNTY1IDE2LjgyMmMuMTI2LjEyLjI3LjE4Mi40MzYuMTgyLjE2OCAwIC4zMS0uMDYuNDMtLjE4Mmw0LjM4LTQuMTc1Yy4xMjgtLjEyLjE5LS4yNjIuMTktLjQyNCAwLS4xNi0uMDYyLS4zMDItLjE5LS40MjNsLS42NS0uNjIyYy0uMTI1LS4xMi0uMjcyLS4xODItLjQ0LS4xODItLjE3IDAtLjMxNC4wNi0uNDM2LjE4MmwtMy4zMTQgMy4xNi0zLjI1LTMuMTZjLS4xMjYtLjEyLS4yNy0uMTgyLS40My0uMTgyLS4xNjYgMC0uMzEuMDYtLjQzNS4xODJsLS42NjIuNjIyYy0uMTI2LjEyLS4xOS4yNjItLjE5LjQyMyAwIC4xNjIuMDY0LjMwMy4xOS40MjRsNC4zNzMgNC4xNzV6Ii8+CiAgICA8L2c+Cjwvc3ZnPg=="
                                                    alt="next arrow"/>
                                            <div class="nextend-thumbnail-inner n2-ow n2-style-df32cb3742fdac43eb0251df7d67c185-simple ">
                                                <div class="nextend-thumbnail-scroller n2-ow n2-align-content-start"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="n2-ss-7-spinner" style="display: none;">
                                        <div>
                                            <div class="n2-ss-spinner-simple-white-container">
                                                <div class="n2-ss-spinner-simple-white"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="n2_clear"></div>
                            <div id="n2-ss-7-placeholder"
                                 style="position: relative;z-index:2;background-color:RGBA(0,0,0,0); background-color:RGBA(255,255,255,0);">
                                <img style="width: 100%; max-width:10000px; display: block;opacity:0;margin:0px;"
                                     class="n2-ow"
                                     src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAwIiBoZWlnaHQ9IjkwMCIgPjwvc3ZnPg=="
                                     alt="Slider"/></div>
                        </div>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="vitri">
    <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"
         style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;border-top-width:1px;border-bottom-width:1px;border-color:#eae9e9;border-top-style:solid;border-bottom-style:solid;'>
        <div class="fusion-builder-row fusion-row ">
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                 style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
				  @foreach(\App\Entity\SubPost::showSubPost('thong-tin-du-an', 2,'asc') as $id => $imgInforProject)
						@if ($id ==1 )
                    <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                             style='margin-top: 0px;margin-bottom: 10px;'>
                            <div class="fusion-column-wrapper"
                                 style="background-color:#009485;padding: 15px 0px 5px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                 data-bg-url="">

                                <div class="fusion-text">
                                   
                                            <p>
                                                <span
                                                    style="font-family: 'times new roman', times, serif; font-size: 14pt; color: #ffffff;">
                                                    <strong>{!! $imgInforProject->title !!}</strong>
                                                </span>
                                                <br/>
                                                <span style="font-size: 14pt; color: #ffffff;">
                                                    <strong>
                                                        <span style="font-family: helvetica, arial, sans-serif;">
                                                        {!! $imgInforProject->description !!}
                                                        </span>
                                                    </strong>
                                                </span>
                                            </p>
                                            <p>
                                                {!! $imgInforProject->content !!}
                                            </p>
                                        
                                    
                                </div>

                            </div>
                        </div>
                    </div>
						<span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><a
									 href="{!! $imgInforProject['slide-anh-thong-tin-du-an'] !!}"
									class="fusion-lightbox"
									data-rel="iLightbox[ba07846865775267496]"
									data-title="vi-tri-gif-1" title="vi-tri-gif-1"><img
										 src="{!! $imgInforProject['slide-anh-thong-tin-du-an'] !!}"
										width="1200" height="697" alt=""
										class="img-responsive wp-image-1701"/></a></span>
                    <div class="fusion-clearfix"></div>
						@endif
					@endforeach
                </div>
            </div>
			 @foreach(\App\Entity\SubPost::showSubPost('thong-tin-du-an', 3, 'asc') as $id => $inforProject)
			  @if ($id == 2)
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                 style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
						<span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none">
						<img src="{!! $inforProject['slide-anh-thong-tin-du-an'] !!}" width="1000"
									height="484" alt="" title="vi-tri-thuc-te"
									class="img-responsive wp-image-1704"/></span>
				 
					<div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                             style='margin-top: 0px;margin-bottom: 10px;'>
                            <div class="fusion-column-wrapper"
                                 style="background-color:#fefaef;padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                 data-bg-url="">
                                <div class="fusion-text">
                                  
                                            <p>
                                                <span style="color: #008c7d;">
                                                    <strong>
                                                        <span style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">
                                                        {!! $inforProject->title !!}
                                                        </span>
                                                    </strong>
                                                </span>
                                            </p>
                                            <p><span style="color: #008c7d;"><strong><span
                                                                style="font-family: arial, helvetica, sans-serif; font-size: 14pt;"> {!! $inforProject->title !!} </span></strong></span>
                                            </p>
                                            {!! $inforProject->content !!}
                                      
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="fusion-clearfix"></div>

                </div>
            </div>
        </div>
		  @endif
		@endforeach
    </div>
</div>

<script type="text/javascript">N2R('documentReady', function ($) {
        N2R(["documentReady", "smartslider-frontend", "smartslider-simple-type-frontend"], function () {
            new N2Classes.SmartSliderSimple('#n2-ss-7', {
                "admin": false,
                "callbacks": "",
                "background.video.mobile": 1,
                "alias": {"id": 0, "smoothScroll": 0, "slideSwitch": 0, "scrollSpeed": 400},
                "align": "normal",
                "isDelayed": 0,
                "load": {"fade": 1, "scroll": 0},
                "playWhenVisible": 1,
                "playWhenVisibleAt": 0.5,
                "responsive": {
                    "hideOn": {
                        "desktopLandscape": false,
                        "desktopPortrait": false,
                        "tabletLandscape": false,
                        "tabletPortrait": false,
                        "mobileLandscape": false,
                        "mobilePortrait": false
                    },
                    "onResizeEnabled": true,
                    "type": "auto",
                    "downscale": 1,
                    "upscale": 1,
                    "minimumHeight": 0,
                    "maximumSlideWidth": {
                        "desktopLandscape": 10000,
                        "desktopPortrait": 10000,
                        "tabletLandscape": 10000,
                        "tabletPortrait": 10000,
                        "mobileLandscape": 10000,
                        "mobilePortrait": 10000
                    },
                    "forceFull": 0,
                    "forceFullOverflowX": "body",
                    "forceFullHorizontalSelector": "",
                    "constrainRatio": 1,
                    "sliderHeightBasedOn": "real",
                    "decreaseSliderHeight": 0,
                    "focusUser": 1,
                    "focusEdge": "auto",
                    "breakpoints": [{
                        "device": "tabletPortrait",
                        "type": "max-screen-width",
                        "portraitWidth": 1199,
                        "landscapeWidth": 1199
                    }, {
                        "device": "mobilePortrait",
                        "type": "max-screen-width",
                        "portraitWidth": 700,
                        "landscapeWidth": 900
                    }],
                    "enabledDevices": {
                        "desktopLandscape": 0,
                        "desktopPortrait": 1,
                        "tabletLandscape": 0,
                        "tabletPortrait": 1,
                        "mobileLandscape": 0,
                        "mobilePortrait": 1
                    },
                    "sizes": {
                        "desktopPortrait": {"width": 1000, "height": 900, "max": 3000, "min": 1000},
                        "tabletPortrait": {"width": 701, "height": 630, "max": 1199, "min": 701},
                        "mobilePortrait": {"width": 320, "height": 288, "max": 900, "min": 320}
                    },
                    "normalizedDeviceModes": {
                        "unknown": "desktopPortrait",
                        "desktopPortrait": "desktopPortrait",
                        "desktopLandscape": "desktopPortrait",
                        "tabletLandscape": "desktopPortrait",
                        "tabletPortrait": "tabletPortrait",
                        "mobileLandscape": "tabletPortrait",
                        "mobilePortrait": "mobilePortrait"
                    },
                    "overflowHiddenPage": 0,
                    "focus": {"offsetTop": "#wpadminbar", "offsetBottom": ""}
                },
                "controls": {"mousewheel": 0, "touch": "horizontal", "keyboard": 1, "blockCarouselInteraction": 1},
                "lazyLoad": 0,
                "lazyLoadNeighbor": 0,
                "blockrightclick": 0,
                "maintainSession": 0,
                "autoplay": {
                    "enabled": 1,
                    "start": 1,
                    "duration": 8000,
                    "autoplayLoop": 1,
                    "allowReStart": 0,
                    "pause": {"click": 1, "mouse": "0", "mediaStarted": 1},
                    "resume": {"click": 0, "mouse": "0", "mediaEnded": 1, "slidechanged": 0},
                    "interval": 1,
                    "intervalModifier": "loop",
                    "intervalSlide": "current"
                },
                "perspective": 1500,
                "layerMode": {"playOnce": 0, "playFirstLayer": 1, "mode": "skippable", "inAnimation": "mainInEnd"},
                "bgAnimationsColor": "RGBA(51,51,51,1)",
                "bgAnimations": 0,
                "mainanimation": {
                    "type": "horizontal",
                    "duration": 400,
                    "delay": 0,
                    "ease": "easeOutQuad",
                    "parallax": 0,
                    "shiftedBackgroundAnimation": 0
                },
                "carousel": 1,
                "dynamicHeight": 0,
                "initCallbacks": function ($) {
                    N2D("SmartSliderWidgetArrowImage", "SmartSliderWidget", function (e, i) {
                        function r(e, i, t, s, r, o) {
                            this.key = e, this.action = t, this.desktopRatio = s, this.tabletRatio = r, this.mobileRatio = o, N2Classes.SmartSliderWidget.prototype.constructor.call(this, i)
                        }

                        return ((r.prototype = Object.create(N2Classes.SmartSliderWidget.prototype)).constructor = r).prototype.onStart = function () {
                            this.deferred = e.Deferred(), this.slider.sliderElement.on("SliderDevice", this.onDevice.bind(this)).trigger("addWidget", this.deferred), this.$widget = e("#" + this.slider.elementID + "-arrow-" + this.key).on("click", function (e) {
                                e.stopPropagation(), this.slider[this.action]()
                            }.bind(this)), this.$resize = this.$widget.find(".n2-resize"), 0 === this.$resize.length && (this.$resize = this.$widget), e.when(this.$widget.n2imagesLoaded(), this.slider.stages.get("ResizeFirst").getDeferred()).always(this.onLoad.bind(this))
                        }, r.prototype.onLoad = function () {
                            this.$widget.addClass("n2-ss-widget--calc"), this.previousWidth = this.$resize.width(), this.previousHeight = this.$resize.height(), this.$widget.removeClass("n2-ss-widget--calc"), this.$resize.find("img").css("width", "100%"), this.onDevice(null, {device: this.slider.responsive.getDeviceMode()}), this.deferred.resolve()
                        }, r.prototype.onDevice = function (e, i) {
                            var t = 1;
                            switch (i.device) {
                                case"tabletPortrait":
                                case"tabletLandscape":
                                    t = this.tabletRatio;
                                    break;
                                case"mobilePortrait":
                                case"mobileLandscape":
                                    t = this.mobileRatio;
                                    break;
                                default:
                                    t = this.desktopRatio
                            }
                            this.$resize.width(this.previousWidth * t), this.$resize.height(this.previousHeight * t)
                        }, function (e, i, t, s) {
                            this.key = "arrow", this.previous = new r("previous", e, "previousWithDirection", i, t, s), this.next = new r("next", e, "nextWithDirection", i, t, s)
                        }
                    });
                    new N2Classes.SmartSliderWidgetArrowImage(this, 1, 1, 0.5);
                    N2D("SmartSliderWidgetThumbnailDefaultHorizontal", "SmartSliderWidget", function (l, d) {
                        "use strict";
                        var i = !1, c = {videoDark: '<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"><circle cx="24" cy="24" r="24" fill="#000" opacity=".6"/><path fill="#FFF" d="M19.8 32c-.124 0-.247-.028-.36-.08-.264-.116-.436-.375-.44-.664V16.744c.005-.29.176-.55.44-.666.273-.126.592-.1.84.07l10.4 7.257c.2.132.32.355.32.595s-.12.463-.32.595l-10.4 7.256c-.14.1-.31.15-.48.15z"/></svg>'};

                        function t(t, i) {
                            this.key = "thumbnail", this.parameters = l.extend({
                                captionSize: 0,
                                minimumThumbnailCount: 1.5,
                                invertGroupDirection: 0
                            }, i), N2Classes.SmartSliderWidget.prototype.constructor.call(this, t)
                        }

                        ((t.prototype = Object.create(N2Classes.SmartSliderWidget.prototype)).constructor = t).prototype.onStart = function () {
                            this.ratio = 1, this.itemsPerPane = 1, this.currentI = 0, this.offset = 0, this.group = parseInt(this.parameters.group), this.$widget = this.slider.sliderElement.find(".nextend-thumbnail-default"), this.bar = this.$widget.find(".nextend-thumbnail-inner"), this.scroller = this.bar.find(".nextend-thumbnail-scroller"), this.$groups = l();
                            for (var t = 0; t < this.group; t++)this.$groups = this.$groups.add(l('<div class="nextend-thumbnail-scroller-group"></div>').appendTo(this.scroller));
                            n2const.rtl.isRtl ? (this.previous = this.$widget.find(".nextend-thumbnail-next").on("click", this.previousPane.bind(this)), this.next = this.$widget.find(".nextend-thumbnail-previous").on("click", this.nextPane.bind(this))) : (this.previous = this.$widget.find(".nextend-thumbnail-previous").on("click", this.previousPane.bind(this)), this.next = this.$widget.find(".nextend-thumbnail-next").on("click", this.nextPane.bind(this))), this.slider.stages.done("BeforeShow", this.onBeforeShow.bind(this)), this.slider.stages.done("WidgetsReady", this.onWidgetsReady.bind(this))
                        }, t.prototype.renderThumbnails = function () {
                            var t;
                            this.parameters.invertGroupDirection && (t = Math.ceil(this.slider.visibleRealSlides.length / this.group));
                            for (var i = 0; i < this.slider.visibleRealSlides.length; i++) {
                                var e, s, h = this.slider.visibleRealSlides[i], n = l('<div class="' + this.parameters.slideStyle + ' n2-ow" style="' + this.parameters.containerStyle + '"></div>');
                                if (this.parameters.invertGroupDirection ? n.appendTo(this.$groups.eq(Math.floor(i / t))) : n.appendTo(this.$groups.eq(i % this.group)), n.data("slide", h), h.$thumbnail = n, this.parameters.thumbnail !== d && (e = h.getThumbnailType(), s = c[e] !== d ? c[e] : "", l('<div class="n2-ss-thumb-image n2-ow" style="width:' + this.parameters.thumbnail.width + "px; height:" + this.parameters.thumbnail.height + "px;" + this.parameters.thumbnail.code + '">' + s + "</div>").css("background-image", "url('" + h.getThumbnail() + "')").appendTo(n)), this.parameters.caption !== d) {
                                    var r, o = l('<div class="' + this.parameters.caption.styleClass + "n2-ss-caption n2-ow n2-caption-" + this.parameters.caption.placement + '" style="' + this.parameters.caption.style + '"></div>');
                                    switch (this.parameters.caption.placement) {
                                        case"before":
                                            o.prependTo(n);
                                            break;
                                        default:
                                            o.appendTo(n)
                                    }
                                    this.parameters.title !== d && o.append('<div class="n2-ow ' + this.parameters.title.font + '">' + h.getTitle() + "</div>"), this.parameters.description === d || (r = h.getDescription()) && o.append('<div class="n2-ow ' + this.parameters.description.font + '">' + r + "</div>")
                                }
                            }
                            var a = "universalclick";
                            "mouseenter" === this.parameters.action ? a = "universalenter" : this.slider.hasTouch() && (a = "n2click"), this.dots = this.scroller.find(".nextend-thumbnail-scroller-group > div").on(a, this.onDotClick.bind(this)), this.images = this.dots.find(".n2-ss-thumb-image")
                        }, t.prototype.onTap = function (t) {
                            i || (l(t.target).trigger("n2click"), i = !0, setTimeout(function () {
                                i = !1
                            }, 500))
                        }, t.prototype.onBeforeShow = function () {
                            var t = !1;
                            switch (this.parameters.area) {
                                case 5:
                                    t = "left";
                                    break;
                                case 8:
                                    t = "right"
                            }
                            t && (this.offset = parseFloat(this.$widget.data("offset")), this.slider.responsive.addHorizontalSpacingControl(t, this)), this.renderThumbnails(), this.slider.hasTouch() && (N2Classes.EventBurrito(this.$widget.get(0), {
                                mouse: !0,
                                axis: "x",
                                start: function () {
                                    this.bar.width();
                                    this._touch = {
                                        start: parseInt(this.scroller.css(n2const.rtl.left)),
                                        max: 0
                                    }, this.getScrollerWidth() < this.thumbnailDimension.width * Math.ceil(this.dots.length / this.group) && (this._touch.max = -Math.round(this.thumbnailDimension.width * this.ratio * Math.ceil(this.dots.length / this.group - 1))), this._touch.current = this._touch.start
                                }.bind(this),
                                move: function (t, i, e, s, h) {
                                    return this._touch.current = Math.max(this._touch.max, Math.min(0, this._touch.start + e.x)), this.scroller.css(n2const.rtl.left, this._touch.current), 5 < Math.abs(e.x)
                                }.bind(this),
                                end: function (t, i, e, s, h) {
                                    Math.abs(this._touch.start - this._touch.current) < 40 ? this.resetPane() : this._touch.current > this._touch.start ? this.previousPane() : this.nextPane(), Math.abs(e.x) < 10 && Math.abs(e.y) < 10 ? this.onTap(t) : nextend.preventClick(), delete this._touch
                                }.bind(this)
                            }), this.slider.parameters.controls.drag || this.$widget.on("click", this.onTap.bind(this))), this.widthPercent = this.$widget.data("width-percent"), this.thumbnailDimension = {
                                widthLocal: this.dots.width(),
                                width: this.dots.outerWidth(!0),
                                height: this.dots.outerHeight(!0),
                                widthBorder: parseInt(this.dots.css("borderLeftWidth")) + parseInt(this.dots.css("borderRightWidth")) + parseInt(this.dots.css("paddingLeft")) + parseInt(this.dots.css("paddingRight")),
                                heightBorder: parseInt(this.dots.css("borderTopWidth")) + parseInt(this.dots.css("borderBottomWidth")) + parseInt(this.dots.css("paddingTop")) + parseInt(this.dots.css("paddingBottom"))
                            }, this.thumbnailDimension.widthMargin = this.thumbnailDimension.width - this.dots.outerWidth(), this.thumbnailDimension.heightMargin = this.thumbnailDimension.height - this.dots.outerHeight(), this.imageDimension = {
                                width: this.images.outerWidth(!0),
                                height: this.images.outerHeight(!0)
                            }, this.sideDimension = .25 * this.thumbnailDimension.width, this.scroller.height(this.thumbnailDimension.height * this.ratio * this.group), this.bar.height(this.scroller.outerHeight(!0)), this.horizontalSpacing = this.bar.outerWidth() - this.bar.width(), this.slider.sliderElement.on({
                                SlideWillChange: this.onSlideSwitch.bind(this),
                                visibleRealSlidesChanged: this.onVisibleRealSlidesChanged.bind(this)
                            })
                        }, t.prototype.onWidgetsReady = function () {
                            this.activateDots(this.slider.currentSlide.index), this.slider.sliderElement.on("SliderResize", this.onSliderResize.bind(this)), this.onSliderResize()
                        }, t.prototype.filterSliderVerticalCSS = function (t) {
                        };
                        var e = !(t.prototype.onSliderResize = function () {
                            var t, i, e, s, h, n, r;
                            this.slider.visibleRealSlides.length && (this.lastScrollerWidth !== this.getScrollerWidth() && (t = 1, e = (i = this.getScrollerWidth()) - 2 * this.sideDimension, (n = i / this.thumbnailDimension.width) < this.dots.length / this.group && (n = e / this.thumbnailDimension.width), this.localSideDimension = this.sideDimension, this.parameters.minimumThumbnailCount >= n && (this.localSideDimension = .1 * i, t = (e = i - 2 * this.localSideDimension) / (this.parameters.minimumThumbnailCount * this.thumbnailDimension.width), n = e / (this.thumbnailDimension.width * t), (n = i / (this.thumbnailDimension.width * t)) < this.dots.length / this.group && (n = e / (this.thumbnailDimension.width * t))), this.ratio !== t && ((h = {}).width = Math.floor(this.thumbnailDimension.width * t - this.thumbnailDimension.widthMargin - this.thumbnailDimension.widthBorder), h.height = Math.floor((this.thumbnailDimension.height - this.parameters.captionSize) * t - this.thumbnailDimension.heightMargin + this.parameters.captionSize - this.thumbnailDimension.heightBorder), this.dots.css(h), s = this.dots.width() / this.thumbnailDimension.widthLocal, (h = {}).width = Math.ceil(this.imageDimension.width * s), h.height = Math.ceil(this.imageDimension.height * s), this.images.css(h), this.bar.css("height", "auto"), this.ratio = t), this.itemsPerPane = Math.floor(n), r = this.slider.responsive.dynamicHeightSlide || this.slider.currentSlide, this.currentI = r.index, this.scroller.css(n2const.rtl.left, this.getScrollerTargetLeft(this.getPaneByIndex(this.currentI))), this.scroller.css("width", this.thumbnailDimension.width * this.ratio * Math.ceil(this.dots.length / this.group))), this.scroller.height(this.thumbnailDimension.height * this.ratio * this.group), this.bar.height(this.scroller.outerHeight(!0)))
                        });
                        return t.prototype.onDotClick = function (t) {
                            nextend.shouldPreventClick || (e || (this.slider.directionalChangeToReal(l(t.currentTarget).data("slide").index), e = !0), setTimeout(function () {
                                e = !1
                            }.bind(this), 400))
                        }, t.prototype.onSlideSwitch = function (t, i) {
                            this.activateDots(i.index), this.goToDot(this.slider.getRealIndex(i.index))
                        }, t.prototype.activateDots = function (t) {
                            this.dots.removeClass("n2-active");
                            for (var i = this.slider.slides[t].slides, e = 0; i.length > e; e++)i[e].$thumbnail.addClass("n2-active")
                        }, t.prototype.resetPane = function () {
                            this.goToDot(this.currentI)
                        }, t.prototype.previousPane = function () {
                            this.goToDot(this.currentI - this.itemsPerPane * this.group)
                        }, t.prototype.nextPane = function () {
                            this.goToDot(this.currentI + this.itemsPerPane * this.group)
                        }, t.prototype.getPaneByIndex = function (t) {
                            return t = Math.max(0, Math.min(this.dots.length - 1, t)), this.parameters.invertGroupDirection ? Math.floor(t % Math.ceil(this.dots.length / this.group) / this.itemsPerPane) : Math.floor(t / this.group / this.itemsPerPane)
                        }, t.prototype.getScrollerTargetLeft = function (t) {
                            this.lastScrollerWidth = this.getScrollerWidth();
                            var i = 0;
                            t === Math.floor((this.dots.length - 1) / this.group / this.itemsPerPane) ? (i = -(t * this.itemsPerPane * this.thumbnailDimension.width * this.ratio), 0 === t ? this.previous.removeClass("n2-active") : this.previous.addClass("n2-active"), this.next.removeClass("n2-active")) : (0 < t ? (i = -(t * this.itemsPerPane * this.thumbnailDimension.width * this.ratio - this.localSideDimension), this.previous.addClass("n2-active")) : (i = 0, this.previous.removeClass("n2-active")), this.next.addClass("n2-active"));
                            var e = this.getScrollerWidth(), s = this.thumbnailDimension.width * this.ratio * Math.ceil(this.dots.length / this.group);
                            return e < s && (i = Math.max(i, -(s - e))), i
                        }, t.prototype.goToDot = function (t) {
                            this.tween && this.tween.progress() < 1 && this.tween.pause();
                            var i = {};
                            i[n2const.rtl.left] = this.getScrollerTargetLeft(this.getPaneByIndex(t)), this.tween = NextendTween.to(this.scroller, .5, i), this.currentI = t
                        }, t.prototype.onVisibleRealSlidesChanged = function () {
                            this.dots.remove(), this.renderThumbnails(), this.lastScrollerWidth = 0
                        }, t.prototype.getScrollerWidth = function () {
                            return this.widthPercent ? Math.ceil(this.slider.responsive.resizeContext.sliderWidth * this.widthPercent / 100) - this.horizontalSpacing : this.bar.width()
                        }, t.prototype.getSize = function () {
                            return 0
                        }, t.prototype.refreshSliderSize = function (t) {
                        }, t
                    });
                    new N2Classes.SmartSliderWidgetThumbnailDefaultHorizontal(this, {
                        "area": 12,
                        "action": "click",
                        "minimumThumbnailCount": 1.5,
                        "group": 1,
                        "invertGroupDirection": 0,
                        "captionSize": 0,
                        "orientation": "horizontal",
                        "slideStyle": "n2-style-462cb0983aca6cda3fc34a0feea6024b-dot ",
                        "containerStyle": "width: 120px; height: 81px;",
                        "thumbnail": {"width": 120, "height": 81, "code": "background-size: cover;"}
                    })
                }
            })
        })
    });</script>
