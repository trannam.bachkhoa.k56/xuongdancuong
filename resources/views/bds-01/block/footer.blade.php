<div class="fusion-footer">

    <footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
        <div class="fusion-row">
            <div class="fusion-columns fusion-columns-4 fusion-widget-area">

                <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                    <section id="text-2" class="fusion-footer-widget-column widget widget_text"
                             style="background-color: #a97829;">
                        <div class="textwidget"><img class="size-full wp-image-1751 aligncenter"
                                                     src="{!! isset($information['logo']) ? $information['logo'] : '' !!}" alt=""
                                                     width="300" height="152"/>

                            <ul class="fusion-checklist fusion-checklist-1"
                                style="font-size:14px;line-height:23.8px;">
                                <li class="fusion-li-item"><span
                                            style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                            class="icon-wrapper circle-no"><i
                                                class="fusion-li-icon fa-map-marker fas" style="color:#ffffff;"></i></span>
                                    <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                        {!! isset($information['dia-chi']) ? $information['dia-chi'] : '' !!}

                                    </div>
                                </li>
                                <li class="fusion-li-item"><span
                                            style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                            class="icon-wrapper circle-no"><i
                                                class="fusion-li-icon fa-phone-volume fas"
                                                style="color:#ffffff;"></i></span>
                                    <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                        Hotline 1: {!! isset($information['hotline1']) ? $information['hotline1'] : '' !!}

                                    </div>
                                </li>
                                <li class="fusion-li-item"><span
                                            style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                            class="icon-wrapper circle-no"><i
                                                class="fusion-li-icon fa-phone-volume fas"
                                                style="color:#ffffff;"></i></span>
                                    <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                        Hotline 2: {!! isset($information['hotline2']) ? $information['hotline2'] : '' !!}

                                    </div>
                                </li>
                                <li class="fusion-li-item"><span
                                            style="height:23.8px;width:23.8px;margin-right:9.8px;"
                                            class="icon-wrapper circle-no"><i class="fusion-li-icon fa-envelope far"
                                                                              style="color:#ffffff;"></i></span>
                                    <div class="fusion-li-item-content" style="margin-left:33.6px;">

                                        {!! isset($information['email']) ? $information['email'] : '' !!}

                                    </div>
                                </li>
                            </ul>

                            <img class="size-full wp-image-1752 aligncenter"
                                 src="wp-content/uploads/2020/06/hotline-footer-1.png" alt="" width="313"
                                 height="100"/></div>
                        <div style="clear:both;"></div>
                    </section>
                </div>
                <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                    <section id="text-5" class="fusion-footer-widget-column widget widget_text"><h4
                                class="widget-title">/ TẢI XUỐNG TÀI LIỆU DỰ ÁN</h4>
                        <div class="textwidget">
                            <hr/>

                            @foreach(\App\Entity\SubPost::showSubPost('tai-lieu-du-an', 4) as $id => $document) 
                            <div class="fusion-button-wrapper">
                                <style type="text/css"
                                       scoped="scoped">.fusion-button.button-3 .fusion-button-text, .fusion-button.button-3 i {
                                        color: #ffffff;
                                    }

                                    .fusion-button.button-3 {
                                        border-width: 0px;
                                        border-color: #ffffff;
                                    }

                                    .fusion-button.button-3 .fusion-button-icon-divider {
                                        border-color: #ffffff;
                                    }

                                    .fusion-button.button-3:hover .fusion-button-text, .fusion-button.button-3:hover i, .fusion-button.button-3:focus .fusion-button-text, .fusion-button.button-3:focus i, .fusion-button.button-3:active .fusion-button-text, .fusion-button.button-3:active {
                                        color: #d176b0;
                                    }

                                    .fusion-button.button-3:hover, .fusion-button.button-3:focus, .fusion-button.button-3:active {
                                        border-width: 0px;
                                        border-color: #d176b0;
                                    }

                                    .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:hover .fusion-button-icon-divider, .fusion-button.button-3:active .fusion-button-icon-divider {
                                        border-color: #d176b0;
                                    }

                                    .fusion-button.button-3 {
                                        background: rgba(160, 206, 78, 0);
                                        background-image: -webkit-gradient(linear, left bottom, left top, from(rgba(160, 206, 78, 0.01)), to(rgba(160, 206, 78, 0)));
                                        background-image: -webkit-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                        background-image: -moz-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                        background-image: -o-linear-gradient(bottom, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                        background-image: linear-gradient(to top, rgba(160, 206, 78, 0.01), rgba(160, 206, 78, 0));
                                    }

                                    .fusion-button.button-3:hover, .button-3:focus, .fusion-button.button-3:active {
                                        background: rgba(150, 195, 70, 0);
                                    }

                                    .fusion-button.button-3 {
                                        width: auto;
                                    }</style>
                                <a class="fusion-button button-flat fusion-button-round button-large button-custom button-3 popmake-370"
                                   target="_self" rel="noopener noreferrer"><i
                                            class="fa-download fas button-icon-left"></i><span
                                            class="fusion-button-text">{{ $document->title}}</span></a>
                            </div>

                            <hr/>
                            @endforeach
                            &nbsp;</div>
                        <div style="clear:both;"></div>
                    </section>
                </div>
                <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                    <section id="text-4" class="fusion-footer-widget-column widget widget_text"><h4
                                class="widget-title">/ ĐĂNG KÝ ĐẶT MUA CĂN HỘ DỰ ÁN</h4>
                        <div class="textwidget">
                            <div role="form" class="wpcf7"  lang="en-US" dir="ltr">
                                <div class="screen-reader-response" aria-live="polite"></div>
                                <form action="" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                      novalidate="novalidate">
                                    
									{!! csrf_field() !!}
									<input type="hidden" name="is_json"
										   class="form-control captcha" value="1" placeholder="">
									   
                                    <p><label> Họ và tên (*)<br/>
                                                <span class="wpcf7-form-control-wrap your-name">
												<input type="text"
												   name="name"
												   value=""
												   size="40"
												   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
												   aria-required="true"
												   aria-invalid="false"/>
											   </span>
                                        </label></p>
                                    <p><label> Địa chỉ EMail (*)<br/>
                                                <span class="wpcf7-form-control-wrap your-email">
												<input type="email"
													name="email"
													value=""
													size="40"
													class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
													aria-required="true"
													aria-invalid="false"/></span>
                                        </label></p>
                                    <p><label> Số điện thoại (*)<br/>
                                                <span class="wpcf7-form-control-wrap your-subject">
												<input type="text"
												  name="phone"
												  value=""
												  size="40"
												  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
												  aria-required="true"
												  aria-invalid="false"/></span>
                                        </label></p>
                                   
                                    <div class="one_fourth">
                                        <input type="submit" value="ĐĂNG KÝ"
                                               class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                    <div class="clear_column"></div>
                                    <input type='hidden' class='wpcf7-pum'
                                           value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                    <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                         style="background-color:;color:;border-color:;border-width:1px;">
                                        <button style="color:;border-color:;" type="button"
                                                class="close toggle-alert" data-dismiss="alert"
                                                aria-hidden="true">&times;</button>
                                        <div class="fusion-alert-content-wrapper"><span
                                                    class="fusion-alert-content"></span></div>
                                    </div>
                                </form>
                            </div>

                            &nbsp;

                            &nbsp;</div>
                        <div style="clear:both;"></div>
                    </section>
                </div>
                <div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
                    <section id="text-3" class="fusion-footer-widget-column widget widget_text">
                        <div class="textwidget">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTnr-Grand-Palace-Th%C3%A1i-B%C3%ACnh-103301778115232%2F%3Fmodal%3Dadmin_todo_tour&#038;tabs=timeline&#038;width=340&#038;height=500&#038;small_header=false&#038;adapt_container_width=true&#038;hide_cover=false&#038;show_facepile=true&#038;appId"
                                    width="400px" height="380px" style="border:none;overflow:hidden" scrolling="no"
                                    frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        </div>
                        <div style="clear:both;"></div>
                    </section>
                </div>

                <div class="fusion-clearfix"></div>
            </div> <!-- fusion-columns -->
        </div> <!-- fusion-row -->
    </footer> <!-- fusion-footer-widget-area -->


    <footer id="footer" class="fusion-footer-copyright-area">
        <div class="fusion-row">
            <div class="fusion-copyright-content">

                <div class="fusion-copyright-notice">
                    <div>
                        Copyright 2012 - 2020 | Chủ đầu tư: {!! isset($information['ten-du-an']) ? $information['ten-du-an'] : '' !!} | {!! isset($information['dia-chi']) ? $information['dia-chi'] : '' !!} | Tổng đài: {!! isset($information['hotline1']) ? $information['hotline1'] : '' !!}
                    </div>
                </div>
                <div class="fusion-social-links-footer">
                    <div class="fusion-social-networks">
                        <div class="fusion-social-networks-wrapper">
                            <a
                                class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook"
                                style="color:#e2b95d;"
                                href="{!! isset($information['facebook']) ? $information['facebook'] : '' !!}"
                                target="_blank" data-placement="top" data-title="Facebook" data-toggle="tooltip"
                                title="Facebook"><span class="screen-reader-text">Facebook</span></a>
                            <a
                                    class="fusion-social-network-icon fusion-tooltip fusion-youtube fusion-icon-youtube"
                                    style="color:#e2b95d;" href="{!! isset($information['youtube']) ? $information['youtube'] : '' !!}" target="_blank"
                                    rel="noopener noreferrer" data-placement="top" data-title="YouTube"
                                    data-toggle="tooltip" title="YouTube"><span
                                        class="screen-reader-text">YouTube</span></a>     
                            <a
                                    class="fusion-social-network-icon fusion-tooltip fusion-mail fusion-icon-mail"
                                    style="color:#e2b95d;"
                                    href="mailto:{!! isset($information['email']) ? $information['email'] : '' !!}"
                                    target="_self" rel="noopener noreferrer" data-placement="top" data-title="Email"
                                    data-toggle="tooltip" title="Email"><span
                                        class="screen-reader-text">Email</span></a>
                            <a
                                    class="custom fusion-social-network-icon fusion-tooltip fusion-custom fusion-icon-custom"
                                    style="color:#e2b95d;position:relative;" href="http://zalo.me/{!! isset($information['zalo']) ? $information['zalo'] : '' !!}"
                                    target="_blank" rel="noopener noreferrer" data-placement="top" data-title
                                    data-toggle="tooltip" title><span class="screen-reader-text"></span><img
                                        src="/bds-01/wp-content/uploads/2019/11/logo-zalo-75x75.jpg"
                                        style="width:auto;max-height:16px;" alt=""/></a>
                        </div>
                    </div>
                </div>

            </div> <!-- fusion-fusion-copyright-content -->
        </div> <!-- fusion-row -->
    </footer> <!-- #footer -->
</div> <!-- fusion-footer -->

<div id="slidingbar-area"
     class="slidingbar-area fusion-sliding-bar-area fusion-widget-area fusion-sliding-bar-position-top fusion-sliding-bar-text-align-left fusion-sliding-bar-toggle-triangle fusion-sliding-bar-sticky"
     data-breakpoint="800" data-toggle="triangle">
    <div class="fusion-sb-toggle-wrapper">
        <a class="fusion-sb-toggle" href="index.html#"><span
                    class="screen-reader-text">Toggle Sliding Bar Area</span></a>
    </div>

    <div id="slidingbar" class="fusion-sliding-bar">
        <div class="fusion-row">
            <div class="fusion-columns row fusion-columns-2columns columns-2">

                <div class="fusion-column col-lg-6 col-md-6 col-sm-6">
                </div>
                <div class="fusion-column col-lg-6 col-md-6 col-sm-6">
                </div>
                <div class="fusion-clearfix"></div>
            </div>
        </div>
    </div>
</div>
</div> <!-- wrapper -->

<a class="fusion-one-page-text-link fusion-page-load-link"></a>

<div id="pum-1810"
     class="pum pum-overlay pum-theme-430 pum-theme-2-theme-pupup-xuat-hien-giua-trang popmake-overlay pum-click-to-close auto_open click_open"
     data-popmake="{&quot;id&quot;:1810,&quot;slug&quot;:&quot;0-1-popup-giua-trang-ra-hang-bt1-bt2&quot;,&quot;theme_id&quot;:430,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;auto_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;delay&quot;:&quot;10000&quot;}},{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true">

    <div id="popmake-1810"
         class="pum-container popmake theme-430 pum-responsive pum-responsive-medium responsive size-medium">


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><span
                        style="font-family: arial, helvetica, sans-serif; font-size: large;"><b>CHÍNH THỨC RA HÀNG BIỆT
                        THỰ SONG LẬP; SHOPHOUSEVILLAS</b></span></p>
            <p style="text-align: center;"><strong><span
                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">DÃY BT01 VÀ BT 02</span></strong>
            </p>
            <p style="text-align: center;"><span style="font-size: 12pt;"><em><span
                                style="font-family: arial, helvetica, sans-serif;">(Tải xuống bảng giá 37 lô biệt thự BT1 và BT2 ngay)</span></em></span>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" onSubmit="return contact(this);" id="wpcf7-f1808-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f1808-o1" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="text"
                                        name="name"
                                        value="" size="40"
                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                        aria-required="true"
                                        aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                        name="email"
                                        value="" size="40"
                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                        aria-required="true"
                                        aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
     
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="alignnone size-full wp-image-1811"
                                src="/bds-01/wp-content/uploads/2020/07/biet-thu-song-lap.jpg" alt="" width="1433" height="960"
                                sizes="(max-width: 1433px) 100vw, 1433px"/></p>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1048"
     class="pum pum-overlay pum-theme-430 pum-theme-2-theme-pupup-xuat-hien-giua-trang popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1048,&quot;slug&quot;:&quot;0-0-popup-giua-trang&quot;,&quot;theme_id&quot;:430,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true">

    <div id="popmake-1048"
         class="pum-container popmake theme-430 pum-responsive pum-responsive-medium responsive size-medium">


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><span
                        style="font-size: 14pt; font-family: arial, helvetica, sans-serif;"><strong>TẢI NGAY BẢNG GIÁ
                        222 LÔ</strong></span><br/>
                <span style="text-align: center;"><span
                            style="font-size: 14pt; font-family: arial, helvetica, sans-serif;"><strong>LIỀN KỀ &#8211;
                            SHOPHOUSE &#8211; BIỆT THỰ SONG LẬP </strong></span></span></p>
            <p style="text-align: center;"><span style="font-size: 12pt;"><em><span
                                style="font-family: arial, helvetica, sans-serif;">(Hệ thống tự động gửi bảng giá về Email và Số điện thoại của khách hàng)</span></em></span>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f1768-o2" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f1768-o2" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
								<p><label> Họ và tên (*)<br/>
									<span class="wpcf7-form-control-wrap your-email">
									<input type="text"
										name="name"
										value="" size="40"
										class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
										aria-required="true"
										aria-invalid="false"/></span>
								</label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                        name="phone"
                                        value="" size="40"
                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                        aria-required="true"
                                        aria-invalid="false"/></span>
                                    </label></p>
                               
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="alignnone size-full wp-image-1770"
                                src="/bds-01/wp-content/uploads/2020/01/tai-lieu-du-an.jpg" alt="" width="800" height="482"
                                sizes="(max-width: 800px) 100vw, 800px"/></p>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1787"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1787,&quot;slug&quot;:&quot;0-3-tai-xuong-bang-gia-lk1234-bt12345&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;a[href=\&quot;gialk1234bt12345_url\&quot;]&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_1787">

    <div id="popmake-1787"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_1787" class="pum-title popmake-title">
            Tải xuống bảng giá LK1,LK2,LK3,LK4; BT1,BT2,BT3,BT4,BT5
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f1788-o3" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f1788-o3" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
								<p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="text"
                                            name="name"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                               
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
   
                               
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="{!! isset($information['logo']) ? $information['logo'] : '' !!}" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">{!! isset($information['hotline1']) ? $information['hotline1'] : '' !!}</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1785"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1785,&quot;slug&quot;:&quot;0-2-tai-xuong-bang-check-lo-con-cac-lo-lien-ke-shophouse-biet-thu&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;a[href=\&quot;checkerlocon_url\&quot;]&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_1785">

    <div id="popmake-1785"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_1785" class="pum-title popmake-title">
            Tải xuống bảng check lô chưa bán, đã bán shophouse, liền kề, biệt thự dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f470-o4" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f470-o4" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
								<p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="text"
                                            name="name"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                               
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="{!! isset($information['logo']) ? $information['logo'] : '' !!}" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">{!! isset($information['hotline1']) ? $information['hotline1'] : '' !!}</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-833"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:833,&quot;slug&quot;:&quot;0-1-tai-xuong-bang-gia-goc-cuoi-chan-trang-mobile&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:&quot;&quot;,&quot;extra_selectors&quot;:&quot;a[href=\&quot;taibanggiamobile_url\&quot;]&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_833">

    <div id="popmake-833"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_833" class="pum-title popmake-title">
            Tải xuống bảng giá gốc 222 lô shophouse, liền kề, biệt thự
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f403-o5" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="index.html#wpcf7-f403-o5" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">

                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">{!! isset($information['hotline1']) ? $information['hotline1'] : '' !!}</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-1779"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:1779,&quot;slug&quot;:&quot;1-tai-xuong-bang-gia-goc-222-lo&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_1779">

    <div id="popmake-1779"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_1779" class="pum-title popmake-title">
            TẢI XUỐNG BẢNG GIÁ GỐC 222 LÔ LIỀN KỀ, BIỆT THỰ, SHOPHOUSE
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f1780-o6" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="" method="post" onSubmit="return contact(this);" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                            <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/>
                                            </span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-381"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:381,&quot;slug&quot;:&quot;18-tai-xuong-toan-bo-van-ban-phap-ly-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_381">

    <div id="popmake-381"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_381" class="pum-title popmake-title">
            Tải xuống toàn bộ văn bản pháp lý dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f383-o7" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                 
                                  {!! csrf_field() !!}

								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                            
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-359"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:359,&quot;slug&quot;:&quot;13-tai-xuong-bang-vat-lieu-ban-giao-kem-theo-hop-dong-mua-ban&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_359">

    <div id="popmake-359"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_359" class="pum-title popmake-title">
            Tải xuống bảng vật liệu bàn giao kèm theo hợp đồng mua bán
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                                
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <p style="text-align: center;">[fusion_serator style_type=&#8221;single solid&#8221;
                            hide_on_mobile=&#8221;small-visibility,medium-visibility,large-visibility&#8221; class=&#8221;&#8221;
                            id=&#8221;&#8221; sep_color=&#8221;#ffffff&#8221; top_margin=&#8221;&#8221; bottom_margin=&#8221;&#8221;
                            border_size=&#8221;3&#8243; icon=&#8221;&#8221; icon_circle=&#8221;&#8221;
                            icon_circle_color=&#8221;&#8221; width=&#8221;30%&#8221; alignment=&#8221;center&#8221;][/fusion_separator]
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-373"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:373,&quot;slug&quot;:&quot;15-tai-mau-hop-dong-dat-coc-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_373">

    <div id="popmake-373"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_373" class="pum-title popmake-title">
            Tải xuống mẫu hợp đồng đặt cọc dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f372-o8" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-376"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:376,&quot;slug&quot;:&quot;16-tai-mau-hop-dong-mua-ban-nha-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_376">

    <div id="popmake-376"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_376" class="pum-title popmake-title">
            Tải xuống mẫu hợp đồng mua bán nhà dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f375-o9" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="" onSubmit="return contact(this);"  method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                            
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-370"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:370,&quot;slug&quot;:&quot;14-tai-xuong-brochure-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_370">

    <div id="popmake-370"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_370" class="pum-title popmake-title">
            Tải xuống Brochure Dự án
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f369-o10" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action=""  onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                            
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI NGAY"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-352"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:352,&quot;slug&quot;:&quot;10-tai-xuong-bang-chinh-sach-ban-hang-dang-ban-hanh&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_352">

    <div id="popmake-352"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_352" class="pum-title popmake-title">
            Tải xuống chính sách bán hàng (dấu đỏ đang ban hành)
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f351-o11" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">

                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                               
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<div id="pum-318"
     class="pum pum-overlay pum-theme-315 pum-theme-1-theme-contact popmake-overlay pum-click-to-close click_open"
     data-popmake="{&quot;id&quot;:318,&quot;slug&quot;:&quot;3-tai-xuong-mat-bang-tong-the-du-an&quot;,&quot;theme_id&quot;:315,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;extra_selectors&quot;:&quot;&quot;,&quot;cookie_name&quot;:null}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;size&quot;:&quot;small&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width&quot;:&quot;100%&quot;,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_width_unit&quot;:false,&quot;custom_height&quot;:&quot;380px&quot;,&quot;custom_height_unit&quot;:false,&quot;custom_height_auto&quot;:false,&quot;location&quot;:&quot;center top&quot;,&quot;position_from_trigger&quot;:false,&quot;position_top&quot;:&quot;100&quot;,&quot;position_left&quot;:&quot;0&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_fixed&quot;:false,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;350&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;0&quot;,&quot;overlay_click&quot;:&quot;1&quot;,&quot;esc_press&quot;:false,&quot;f4_press&quot;:false},&quot;click_open&quot;:[]}}"
     role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_318">

    <div id="popmake-318"
         class="pum-container popmake theme-315 pum-responsive pum-responsive-small responsive size-small">


        <div id="pum_popup_title_318" class="pum-title popmake-title">
            MẶT BẰNG TỔNG THỂ DỰ ÁN
        </div>


        <div class="pum-content popmake-content">
            <p style="text-align: center;"><strong><span style="font-size: 12pt;"><em><span
                                    style="font-family: arial, helvetica, sans-serif; color: #000000;">(Hệ thống tự động của website sẽ tự động gửi tài liệu về Email và Số điện thoại của quý vị)</span></em></span></strong>
            </p>
            <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%; margin-right:4%;'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <div role="form" class="wpcf7" id="wpcf7-f317-o12" lang="en-US" dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="" onSubmit="return contact(this);" method="post" class="wpcf7-form"
                                  novalidate="novalidate">
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">
                                <p><label> Họ và tên (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-name">
                                        <input type="text"
                                            name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Địa chỉ EMail (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-email">
                                        <input type="email"
                                            name="email"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                <p><label> Số điện thoại (*)<br/>
                                        <span class="wpcf7-form-control-wrap your-subject">
                                        <input type="text"
                                            name="phone"
                                            value="" size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span>
                                    </label></p>
                                
                                <div class="one_fourth">
                                    <input type="submit" value="TẢI XUỐNG"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/></div>
                                <div class="clear_column"></div>
                                <input type='hidden' class='wpcf7-pum'
                                       value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button" class="close toggle-alert"
                                            data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                     style='margin-top: 0px;margin-bottom: 10px;width:48%'>
                    <div class="fusion-column-wrapper"
                         style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                         data-bg-url="">
                        <p><img class="size-full wp-image-1751 aligncenter"
                                src="/bds-01/wp-content/uploads/2020/06/logo-footer.png" alt="" width="300" height="152"
                                sizes="(max-width: 300px) 100vw, 300px"/></p>
                        <p style="text-align: center;">
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 12pt;">Tổng đài tư vấn bán hàng</span></strong></span>
                        </p>
                        <p style="text-align: center;"><span style="color: #ffffff;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 18pt;">0918 960 888</span></strong></span>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:30%;"></div>
                    </div>
                </div>
            </div>
        </div>


        <button type="button" class="pum-close popmake-close" aria-label="Close">
            X
        </button>

    </div>

</div>
<!--[if IE 9]>
<script type='text/javascript'
        src='https://tnrgrandpalacethaibinh.com/wp-content/themes/Avada/includes/lib/assets/min/js/general/fusion-ie9.js?ver=1'></script>
<![endif]-->
<script type='text/javascript'
        src='/bds-01/wp-content/uploads/fusion-scripts/bb59f1f1f0555af0a437a8d887b035d4.min.js@timestamp=1593497390'></script>
<script type='text/javascript' src='/bds-01/wp-includes/js/jquery/ui/core.min.js@ver=1.11.4'></script>
<script type='text/javascript' src='/bds-01/wp-includes/js/jquery/ui/position.min.js@ver=1.11.4'></script>

<script type='text/javascript'
        src='/bds-01/wp-content/uploads/pum/pum-site-scripts.js@defer&amp;generated=1593678971&amp;ver=1.11.0'></script>
<script type='text/javascript' src='/bds-01/wp-includes/js/wp-embed.min.js@ver=5.4.2'></script>

<noscript><a href="" target="_blank"><img src=""
                                                    alt="web hit counter" border="0"></a></noscript>
<!-- Histats.com  END  -->
</body>
<div class="hotline-footer">
    <div class="left">
        <a href="checkerlocon_url"><img
                    src="/bds-01/wp-content//uploads/2019/11/icon-tai-xuong-2.png"/>Bảng giá
            </a>
    </div>
    <div class="right">
        <a href="tel:{!! isset($information['hotline1']) ? $information['hotline1'] : '' !!}"><img
                    src="/bds-01/wp-content//uploads/2019/11/icon-call.png"/>Gọi ngay</a>
    </div>
    <div class="clearboth"></div>
</div>
<div class="bottom-contact">
    <div class="container">
        <div class="left">
            <a href="checkerlocon_url" target="blank"><img
                        src="/bds-01/wp-content/uploads/2019/11/icon-tai-xuong-2.png"/>Bảng check lô đã bán, chưa bán</a>
        </div>
        <div class="center">
            <a href="gialk1234bt12345_url" target="blank"><img
                        src="/bds-01/wp-content/uploads/2019/11/icon-tai-xuong-2.png"/>{!! isset($information['bang-gia-lo']) ? $information['bang-gia-lo'] : '' !!}</a>
        </div>
        <div class="right">
            <a href="tel:0918960888" target="blank"><img src="/bds-01/wp-content/uploads/2019/11/icon-call.png"/>Hotline:
            {!! isset($information['hotline1']) ? $information['hotline1'] : '' !!}</a>
        </div>
        <div class="clearboth"></div>
    </div>
</div>
