
@foreach(\App\Entity\SubPost::showSubPost('uu-dai-va-triet-khau-du-an', 1) as $id => $discount)
<div id="chinhsach">
    <div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling fusion-equal-height-columns"
         style='background-color: #e3f1f3;background-image: url("wp-content/uploads/2020/05/patten-200x200-1.png");background-position: center center;background-repeat: repeat;padding-top:30px;padding-right:5px;padding-left:5px;'>
        <div class="fusion-builder-row fusion-row ">
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                <div class="fusion-column-wrapper"
                     style="padding: 50px 0px 0px 20px;background-position:center bottom;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text">
                        <p>
                        <a href="#">
                            <span
                            style="font-family: 'times new roman', times, serif; font-size: x-large; color: #008c7d;">
                            <b>{!! isset($information['ten-du-an']) ? $information['ten-du-an'] : '' !!}</b></span></a><br/>
                            <strong><em><span style="font-size: 24px;">
                                {!! $discount->title !!}
                            </span>
                            </em>
                            </strong>
                            <br/>
                            <img class="alignnone size-full wp-image-860"
                                 src="/bds-01/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg" alt=""
                                 width="162" height="3"
                                 sizes="(max-width: 162px) 100vw, 162px"/></p>
                    </div>
                    <div class="fusion-text">
                        {!! $discount->content !!}
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-title title fusion-title-size-one"
                         style="margin-top:0px;margin-bottom:0px;">
                        <div class="title-sep-container">
                            <div class="title-sep sep-single sep-solid"
                                 style="border-color:#353535;"></div>
                        </div>
                        <h1 class="title-heading-right"><span
                                    style="color: #008c7d;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">Ưu đãi chiết khấu mua nhà</span></strong></span>
                        </h1></div>
                                            <span class="fusion-imageframe imageframe-none imageframe-11 hover-type-none"><img
                                                        src="{!! $discount->image !!}" width="1349"
                                                        height="1005" alt="" title="1.-Khung-1"
                                                        class="img-responsive wp-image-1736"
                                                        sizes="(max-width: 800px) 100vw, 1349px"/></span>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<div id="chinhsach">
    <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
         style='background-color: #e3f1f3;background-image: url("wp-content/uploads/2020/05/patten-200x200-1.png");background-position: center center;background-repeat: repeat;padding-right:5px;padding-bottom:40px;padding-left:5px;'>
        <div class="fusion-builder-row fusion-row ">
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                 style='margin-top:0px;margin-bottom:10px;'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                        @foreach (explode(',', $discount['chon-nhieu-anh']) as $id => $imgDiscount)
                       <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3" style="margin-top: 0px;margin-bottom: 10px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) ); @if ($id != 2) margin-right: 4%; @endif">
                            <div class="fusion-column-wrapper"
                                 style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                                 data-bg-url="">
                                <div class="imageframe-liftup"><span
                                            class="fusion-imageframe imageframe-none imageframe-12">
											<img src="{!! $imgDiscount !!}"
                                                width="1000" height="1377" alt=""
                                                title="trang-11"
                                                class="img-responsive wp-image-1796"
                                                sizes="(max-width: 800px) 100vw, 1000px"/></span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="fusion-button-wrapper fusion-aligncenter">
                        <style type="text/css"
                               scoped="scoped">.fusion-button.button-2 .fusion-button-text, .fusion-button.button-2 i {
                                color: #ffffff;
                            }

                            .fusion-button.button-2 {
                                border-width: 0px;
                                border-color: #ffffff;
                            }

                            .fusion-button.button-2 .fusion-button-icon-divider {
                                border-color: #ffffff;
                            }

                            .fusion-button.button-2.button-3d {
                                -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                -moz-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                            }

                            .button-2.button-3d:active {
                                -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                -moz-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                                box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px rgba(124, 204, 191, 0.69), 1px 5px 5px 3px rgba(0, 0, 0, 0.3);
                            }

                            .fusion-button.button-2:hover .fusion-button-text, .fusion-button.button-2:hover i, .fusion-button.button-2:focus .fusion-button-text, .fusion-button.button-2:focus i, .fusion-button.button-2:active .fusion-button-text, .fusion-button.button-2:active {
                                color: #ffffff;
                            }

                            .fusion-button.button-2:hover, .fusion-button.button-2:focus, .fusion-button.button-2:active {
                                border-width: 0px;
                                border-color: #ffffff;
                            }

                            .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:hover .fusion-button-icon-divider, .fusion-button.button-2:active .fusion-button-icon-divider {
                                border-color: #ffffff;
                            }

                            .fusion-button.button-2 {
                                background: #ff1f1c;
                            }

                            .fusion-button.button-2:hover, .button-2:focus, .fusion-button.button-2:active {
                                background: #d176b0;
                                background-image: -webkit-gradient(linear, left bottom, left top, from(#16a6c1), to(#d176b0));
                                background-image: -webkit-linear-gradient(bottom, #16a6c1, #d176b0);
                                background-image: -moz-linear-gradient(bottom, #16a6c1, #d176b0);
                                background-image: -o-linear-gradient(bottom, #16a6c1, #d176b0);
                                background-image: linear-gradient(to top, #16a6c1, #d176b0);
                            }

                            .fusion-button.button-2 {
                                width: auto;
                            }</style>
                        <a class="fusion-button button-3d fusion-button-square button-medium button-custom button-2 popmake-352"
                           target="_self"><span
                                    class="fusion-button-icon-divider button-icon-divider-left"><i
                                        class="fa-download fas"></i></span><span
                                    class="fusion-button-text fusion-button-text-left">Tải xuống chính sách ưu đãi, chiết khấu từ chủ đầu tư</span></a>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
@endforeach