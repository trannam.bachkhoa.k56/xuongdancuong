<div id="matbang">
    <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"
         style='background-color: rgba(255,255,255,0);background-position: left top;background-repeat: repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;'>
        <div class="fusion-builder-row fusion-row ">
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_4  fusion-one-fourth fusion-column-first fusion-blend-mode fusion-spacing-no 1_4"
                 style='margin-top:0px;margin-bottom:0px;width:25%;width:calc(25% - ( ( 0% ) * 0.25 ) );margin-right: 0%;'>
                <div class="fusion-column-wrapper"
                     style="background-color:#009485;padding: 10px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text"><p><a href="index.html"><span
                                        style="font-family: 'times new roman', times, serif; font-size: 18pt; color: #ffffff;">TNR GRAND PALACE</span></a><br/>
                                                    <span style="font-family: arial, helvetica, sans-serif; font-size: 24pt; color: #ffffff;"><strong>MẶT
                                                            BẰNG TỔNG THỂ</strong></span>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#ffffff;border-top-width:3px;margin-top:-10px;margin-bottom:15px;width:100%;max-width:20%;"></div>
                        </p>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_3_4  fusion-three-fourth fusion-column-last fusion-spacing-no 3_4"
                 style='margin-top:0px;margin-bottom:0px;width:75%;width:calc(75% - ( ( 0% ) * 0.75 ) );'>
                <div class="fusion-column-wrapper"
                     style="padding: 10px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-title title fusion-title-size-three"
                         style="margin-top:0px;margin-bottom:0px;">
                        <div class="title-sep-container">
                            <div class="title-sep sep-single sep-solid"
                                 style="border-color:#009485;"></div>
                        </div>
                        <h3 class="title-heading-right"><span
                                    style="font-family: arial, helvetica, sans-serif; font-size: 14pt; color: #440202;">WELCOME TO TNR GRAND PALACE</span>
                        </h3></div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
                 style='margin-top:0px;margin-bottom:0px;'>
                <div class="fusion-column-wrapper"
                     style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                                            <span class="fusion-imageframe imageframe-none imageframe-7 hover-type-none"><img
                                                        src="{!! isset($information['mat-bang-du-an']) ? $information['mat-bang-du-an'] : ''   !!}"
                                                        width="2560" height="1767" alt="" title="mat-bang-tong-file-net"
                                                        class="img-responsive wp-image-1725"
                                                        sizes="(max-width: 800px) 100vw, 2560px"/></span>
                    <div class="fusion-sep-clear"></div>
                    <div class="fusion-separator fusion-full-width-sep sep-none"
                         style="margin-left: auto;margin-right: auto;margin-top:10px;margin-bottom:10px;"></div>
                    <div class="fusion-button-wrapper fusion-aligncenter">
                        <style type="text/css"
                               scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {
                                color: #ffffff;
                            }

                            .fusion-button.button-1 {
                                border-width: 0px;
                                border-color: #ffffff;
                            }

                            .fusion-button.button-1 .fusion-button-icon-divider {
                                border-color: #ffffff;
                            }

                            .fusion-button.button-1.button-3d {
                                -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                -moz-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                            }

                            .button-1.button-3d:active {
                                -webkit-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                -moz-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                                box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px rgba(124, 204, 191, 0.69), 1px 6px 6px 3px rgba(0, 0, 0, 0.3);
                            }

                            .fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i, .fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i, .fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active {
                                color: #ffffff;
                            }

                            .fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active {
                                border-width: 0px;
                                border-color: #ffffff;
                            }

                            .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider {
                                border-color: #ffffff;
                            }

                            .fusion-button.button-1 {
                                background: #ff1f1c;
                            }

                            .fusion-button.button-1:hover, .button-1:focus, .fusion-button.button-1:active {
                                background: #d176b0;
                                background-image: -webkit-gradient(linear, left bottom, left top, from(#16a6c1), to(#d176b0));
                                background-image: -webkit-linear-gradient(bottom, #16a6c1, #d176b0);
                                background-image: -moz-linear-gradient(bottom, #16a6c1, #d176b0);
                                background-image: -o-linear-gradient(bottom, #16a6c1, #d176b0);
                                background-image: linear-gradient(to top, #16a6c1, #d176b0);
                            }

                            .fusion-button.button-1 {
                                width: auto;
                            }</style>
                        <a class="fusion-button button-3d fusion-button-square button-large button-custom button-1 popmake-318"
                           target="_self"><span
                                    class="fusion-button-icon-divider button-icon-divider-left"><i
                                        class="fa-download fas"></i></span><span
                                    class="fusion-button-text fusion-button-text-left">Tải xuống mặt bằng tổng thể (bản gốc)</span></a>
                    </div>
                    <div class="fusion-sep-clear"></div>
                    <div class="fusion-separator fusion-full-width-sep sep-none"
                         style="margin-left: auto;margin-right: auto;margin-top:10px;margin-bottom:10px;"></div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
