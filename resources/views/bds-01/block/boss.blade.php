@foreach(\App\Entity\SubPost::showSubPost('chu-dau-tu', 1) as $id => $boss)
<div id="chudautu">
    <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling fusion-equal-height-columns"
         style='background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;padding-top:30px;padding-right:10px;padding-bottom:30px;padding-left:10px;'>
        <div class="fusion-builder-row fusion-row ">
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-blend-mode 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );margin-right: 0.5%;'>
                <div class="fusion-column-wrapper"
                     style="background-color:#ffffff;padding: 8px 8px 8px 8px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text">
                        {!! $boss->content !!}
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-blend-mode 1_2"
                 style='margin-top:0px;margin-bottom:10px;width:50%;width:calc(50% - ( ( 0.5% ) * 0.5 ) );'>
                <div class="fusion-column-wrapper"
                     style="background-color:#ffffff;padding: 8px 8px 8px 8px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-title title fusion-title-size-one"
                         style="margin-top:0px;margin-bottom:0px;"><h1
                                class="title-heading-left"><span
                                    style="color: #800000;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">Các dự án đã thực hiện trên toàn Quốc</span></strong></span>
                        </h1>
                        <div class="title-sep-container">
                            <div class="title-sep sep-single sep-solid"
                                 style="border-color:#000000;"></div>
                        </div>
                    </div>
                    <div class="fusion-text">
                        <div class="page" title="Page 5">
                            <div class="section">
                                <div class="layoutArea">
                                    <div class="column">
                                        <p><img class="alignnone size-full wp-image-1745"
                                                src="{!! $boss->image !!}"
                                                alt="" width="1000" height="1263"
                                                sizes="(max-width: 1000px) 100vw, 1000px"/></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling"
     style='background-color: rgba(255,255,255,0);background-image: url("/bds-01/wp-content/uploads/2020/06/anh-nen-thuc-te-1.jpg");background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:40px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
    <div class="fusion-builder-row fusion-row ">
        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
             style='margin-top:0px;margin-bottom:10px;'>
            <div class="fusion-column-wrapper"
                 style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                 data-bg-url="">
                <div class="fusion-text"><p style="text-align: center;"><span
                                style="font-size: 18pt; color: #ffffff;"><strong><span
                                        style="font-family: helvetica, arial, sans-serif;">THƯ VIỆN</span></strong></span><br/>
                                                <span style="text-align: center;"><span
                                                            style="font-size: 18pt; color: #ffffff;"><strong><span
                                                                    style="font-family: helvetica, arial, sans-serif;">HÌNH ẢNH, VIDEO REVIEW THỰC TẾ DỰ ÁN</span></strong></span></span>
                    </p>
                    <p style="text-align: center;"><img class="alignnone size-full wp-image-860"
                                                        src="/bds-01/wp-content/uploads/2020/01/gach-chann-duoi-1.jpg"
                                                        alt="" width="162" height="3"
                                                        sizes="(max-width: 162px) 100vw, 162px"/>
                    </p>
                </div>
                <style type="text/css" scoped="scoped">.fusion-gallery-1 .fusion-gallery-image {
                        border: 2px solid #440202;
                    }</style>
                <div class="fusion-gallery fusion-gallery-container fusion-grid-3 fusion-columns-total-6 fusion-gallery-layout-grid fusion-gallery-1"
                     style="margin:-5px;">
                     @foreach (explode(',', $boss['anh-thuc-te-du-an']) as $id => $imgBoss)
                    <div style="padding:5px;"
                         class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3">
                        <div class="fusion-gallery-image fusion-gallery-image-liftup"><a
                                    href="{!! $imgBoss !!}"
                                    class="fusion-lightbox"
                                    data-rel="iLightbox[195786979bfa05db790]"><img
                                        src="{!! $imgBoss !!}"
                                        alt="" title="tnr-grand-palace-thai-binh-6"
                                        aria-label="tnr-grand-palace-thai-binh-6"
                                        class="img-responsive wp-image-1759"/></a></div>
                    </div>
                    <div class="clearfix"></div>
                    @endforeach
                    
                </div>
                <div class="fusion-clearfix"></div>

            </div>
        </div>
    </div>
</div>
<div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
     style='background-color: #fefaef;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:30px;padding-left:30px;'>
    <div class="fusion-builder-row fusion-row ">
        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"
             style='margin-top:0px;margin-bottom:10px;'>
            <div class="fusion-column-wrapper"
                 style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                 data-bg-url="">
                <div class="fusion-text"><p style="text-align: center;"><span
                                style="font-family: helvetica, arial, sans-serif; font-size: 14pt;">LIÊN HỆ ĐẶT MUA TRỰC TIẾP CHỦ ĐẦU TƯ</span>
                    </p>
                    <p style="text-align: center;"><strong><span
                                    style="font-size: 16pt; color: #008c7d;">&#8220;LIỀN KỀ, SHOPHOUSE, BIỆT THỰ <a
                                        href="index.html">TNR GRAND
                                    PALACE</a> ĐÔNG MỸ&#8221;</span></strong></p>
                    <p style="text-align: center;"><span
                                style="color: #000000; font-size: 12pt;">Để lại thông tin chúng tôi sẽ gọi lại ngay!</span>
                    </p>
                </div>
                <div class="fusion-text">
                    <div role="form" class="wpcf7" id="wpcf7-f294-p1679-o17" lang="en-US"
                         dir="ltr">
                        <div class="screen-reader-response" aria-live="polite"></div>
                        <form action="" onSubmit="return contact(this);" method="post"
                              class="wpcf7-form" novalidate="novalidate">
                              
                              {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">

                            <p><label> Họ và tên (*)<br/>
                                <span class="wpcf7-form-control-wrap your-name"><input
                                        type="text" name="name" value="" size="40"
                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                        aria-required="true"
                                        aria-invalid="false"/></span> 
                                </label>
                            </p>
                            <p><label> Địa chỉ EMail (*)<br/>
                                <span class="wpcf7-form-control-wrap your-email"><input
                                            type="email" name="email" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                            aria-required="true"
                                            aria-invalid="false"/></span> 
                                </label>
                            </p>
                            <p><label> Số điện thoại (*)<br/>
                                <span class="wpcf7-form-control-wrap your-subject"><input
                                type="text" name="phone" value=""
                                size="40"
                                class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                aria-required="true"
                                aria-invalid="false"/></span> 
                                </label>
                            </p>
                        
                            <div class="one_fourth">
                                <input type="submit" value="GỌI LẠI TƯ VẤN CHO TÔI NGAY"
                                       class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                            </div>
                            <div class="clear_column"></div>
                            <input type='hidden' class='wpcf7-pum'
                                   value='{"closepopup":false,"closedelay":0,"openpopup":false,"openpopup_id":0}'/>
                            <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                 style="background-color:;color:;border-color:;border-width:1px;">
                                <button style="color:;border-color:;" type="button"
                                        class="close toggle-alert" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <div class="fusion-alert-content-wrapper"><span
                                            class="fusion-alert-content"></span></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="fusion-clearfix"></div>

            </div>
        </div>
    </div>
</div>
@endforeach