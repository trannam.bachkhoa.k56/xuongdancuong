<div id="sanpham">
    <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"
         style='background-color: rgba(255,255,255,0);background-image: url("wp-content/uploads/2020/06/patten-san-pham.png");background-position: center top;background-repeat: no-repeat;padding-top:0px;padding-bottom:0px;border-top-width:2px;border-bottom-width:2px;border-color:#008c7d;border-top-style:solid;border-bottom-style:solid;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
        <div class="fusion-builder-row fusion-row ">
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"
                 style='margin-top:0px;margin-bottom:10px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right: 4%;'>
                <div class="fusion-column-wrapper"
                     style="padding: 20px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text"><p><span
                                    style="font-size: 24pt; color: #008c7d;"><strong><span
                                            style="font-family: arial, helvetica, sans-serif;">CÁC LOẠI SẢN PHẨM DỰ ÁN</span></strong></span><br/>
                            <a href="index.html"><span style="font-size: 18pt; color: #008c7d;"><strong><span
                                                style="font-family: 'times new roman', times, serif;">{!! isset($information['ten-du-an']) ? $information['ten-du-an'] : ''   !!}</span></strong></span></a>
                        </p>
                        <div class="fusion-sep-clear"></div>
                        <div class="fusion-separator sep-single sep-solid"
                             style="border-color:#440202;border-top-width:4px;margin-top:;width:100%;max-width:20%;"></div>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"
                 style='margin-top:0px;margin-bottom:10px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                <div class="fusion-column-wrapper"
                     style="padding: 20px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"
                     data-bg-url="">
                    <div class="fusion-text"><p><span style="color: #440202;"><em><span
                                            style="font-size: 14pt;">Để lại thông tin chúng tôi sẽ gọi lại ngay!</span></em></span>
                        </p>
                    </div>
                    <div class="fusion-text">
                        <div role="form" class="wpcf7" id="wpcf7-f930-p1679-o13" lang="en-US"
                             dir="ltr">
                            <div class="screen-reader-response" aria-live="polite"></div>
                            <form action="" onSubmit="return contact(this);" method="post"
                                  class="wpcf7-form" novalidate="novalidate">
                                  
                                  {!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">

                                <p><label> Họ và tên (*)<br/>
                                    <span class="wpcf7-form-control-wrap your-subject"><input
                                        type="text" name="name" value=""
                                        size="40"
                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                        aria-required="true" aria-invalid="false"/>
                                    </span>
                                    </label>
                                </p>
                                <p><label> Số điện thoại (*)<br/>
                                    <span class="wpcf7-form-control-wrap your-subject"><input
                                        type="text" name="phone" value=""
                                        size="40"
                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                        aria-required="true" aria-invalid="false"/>
                                    </span>
                                    </label>
                                </p>
                                <div class="one_fourth">
                                    <input type="submit" value="GỬI ĐI"
                                           class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                                </div>
                                <div class="clear_column"></div>
                                <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                     style="background-color:;color:;border-color:;border-width:1px;">
                                    <button style="color:;border-color:;" type="button"
                                            class="close toggle-alert" data-dismiss="alert"
                                            aria-hidden="true">&times;</button>
                                    <div class="fusion-alert-content-wrapper"><span
                                                class="fusion-alert-content"></span></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="fusion-clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling fusion-equal-height-columns"
     style='background-color: rgba(255,255,255,0);background-position: left top;background-repeat: repeat;padding-top:0px;padding-right:5px;padding-bottom:0px;padding-left:5px;margin-top: 20px;'>
    <div class="fusion-builder-row fusion-row ">
        @foreach (\App\Entity\Product::newProduct(3) as $id => $product)
        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third 1_3 fusion-column-last"
             style='margin-top:0px;margin-bottom:10px;width:33.33%;width:calc(33.33% - ( ( 0.5% + 0.5% ) * 0.3333 ) ); @if($id != 2) margin-right: 0.5%; @endif'>
            <div class="fusion-column-wrapper"
                 style="border:1px solid #440202;padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
						<span class="fusion-imageframe imageframe-none imageframe-9 hover-type-zoomin">
						<img src="{!! $product->image !!}" width="1000"
									height="557" alt="" title="shophouse"
									class="img-responsive wp-image-1729"
									sizes="(max-width: 800px) 100vw, 1000px"/></span>
                <div class="fusion-text">
                    <hr/>
                    <p style="text-align: center;">
                        <span style="color: #800000;">
                        <strong>
                        <span
                            style="font-family: arial, helvetica, sans-serif; font-size: 14pt;">
                            {!! $product->title !!}
                            </span>
                            </strong>
                        </span>
                    </p>
                    <p style="text-align: center;">
                        <span style="color: #800000;">
                            <strong>
                            {!! $product->description !!}
                            </strong>
                        </span>
                    </p>
                    <hr/>
                </div>
                <div class="fusion-text">
                    <p>
                        <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                            gốc từ:</strong> <del>Liên hệ vnđ</del></span></p>
                    <p>
                        <span style="font-family: arial, helvetica, sans-serif; font-size: 12pt;"><strong>Giá
                                bán từ: <span
                                        style="font-size: 14pt; color: #ff0000;">Liên hệ </span></strong>vnđ</span>
                    </p>
                    <hr/>
                </div>
                <div class="fusion-text">
                    <p>
                        {!! $product->content !!}
                    </p>
                </div>
                <div class="fusion-text">
                    <hr/>
                    <p style="text-align: center;"><span
                                style="font-size: 12pt; font-family: arial, helvetica, sans-serif; color: #ff0000;">Để lại thông tin, chúng tôi sẽ gọi lại ngay!</span>
                    </p>
                </div>
                <div class="fusion-text">
                    <div role="form" class="wpcf7" id="wpcf7-f930-p1679-o15" lang="en-US"
                         dir="ltr">
                        <div class="screen-reader-response" aria-live="polite"></div>
                        <form action="" onSubmit="return contact(this);" method="post"
                              class="wpcf7-form" novalidate="novalidate">
                            
                            {!! csrf_field() !!}
                            <input type="hidden" name="is_json"
                                    class="form-control captcha" value="1" placeholder="">

                            <p><label> Họ và tên (*)<br/>
                                <span class="wpcf7-form-control-wrap your-subject"><input
                                            type="text" name="name" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span> 
                                </label>
                            </p>

                            <p><label> Số điện thoại (*)<br/>
                                <span class="wpcf7-form-control-wrap your-subject"><input
                                            type="text" name="phone" value=""
                                            size="40"
                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                            aria-required="true"
                                            aria-invalid="false"/></span> 
                                </label>
                            </p>
                            <div class="one_fourth">
                                <input type="submit" value="GỬI ĐI"
                                       class="wpcf7-form-control wpcf7-submit cf7-btn-horizontal"/>
                            </div>
                            <div class="clear_column"></div>
                            <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none"
                                 style="background-color:;color:;border-color:;border-width:1px;">
                                <button style="color:;border-color:;" type="button"
                                        class="close toggle-alert" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                <div class="fusion-alert-content-wrapper"><span
                                            class="fusion-alert-content"></span></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="fusion-clearfix"></div>

            </div>
        </div>
        @endforeach
    </div>
</div>
