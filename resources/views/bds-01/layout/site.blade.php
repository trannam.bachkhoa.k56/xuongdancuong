<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" 					content="width=device-width, initial-scale=1.0" />   
    <meta http-equiv="Content-Type" 		content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
	<meta name="title" 						content="@yield('title')" />
    <meta name="description" 				content="@yield('meta_description')" />
    <meta name="keywords" 					content="@yield('keywords')" />
	<meta name="theme-color"				content="#07a0d2" />
	<meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="canonical" href="@yield('meta_url')"/>
	<meta http-eqiv="Content-Language" content="vi">
	
    <link rel="icon" type="image/x-icon" href="{{ !empty($information['icon']) ?  asset($information['icon']) : '' }}"  />

    <link rel='stylesheet' id='wp-block-library-css'  href='/bds-01/wp-includes/css/dist/block-library/style.min.css@ver=5.4.2.css' type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href='/bds-01/wp-content/plugins/contact-form-7/includes/css/styles.css@ver=5.1.9.css' type='text/css' media='all' />
    <link rel='stylesheet' id='avada-stylesheet-css'  href='/bds-01/wp-content/themes/Avada/assets/css/style.min.css@ver=5.6.css' type='text/css' media='all' />
    <!--[if lte IE 9]>
    <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='/bds-01/wp-content/themes/Avada/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6' type='text/css' media='all' />
    <![endif]-->
    <!--[if IE]>
    <link rel='stylesheet' id='avada-IE-css'  href='/bds-01//wp-content/themes/Avada/assets/css/ie.min.css?ver=5.6' type='text/css' media='all' />
    <![endif]-->
    <link rel='stylesheet' id='recent-posts-widget-with-thumbnails-public-style-css'  href='/bds-01/wp-content/plugins/recent-posts-widget-with-thumbnails/public.css@ver=6.7.0.css' type='text/css' media='all' />

    <link rel='stylesheet' id='popup-maker-site-css'  href='/bds-01/wp-content/uploads/pum/pum-site-styles.css@generated=1593678971&amp;ver=1.11.0.css' type='text/css' media='all' />

    <link rel='stylesheet' id='fusion-dynamic-css-css'  href='/bds-01/wp-content/uploads/fusion-styles/4872fd85ce7c5d6e05c1a82d8e22f395.min.css@timestamp=1593659398&amp;ver=5.4.2.css' type='text/css' media='all' />

    <link rel="stylesheet" type="text/css" href="/bds-01/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/smartslider.min.css@ver=70fceec4.css" media="all" />

    <style type="text/css">.n2-ss-spinner-simple-white-container {
            position: absolute;
            top: 50%;
            left: 50%;
            margin: -20px;
            background: #fff;
            width: 20px;
            height: 20px;
            padding: 10px;
            border-radius: 50%;
            z-index: 1000;
        }

        .n2-ss-spinner-simple-white {
            outline: 1px solid RGBA(0,0,0,0);
            width:100%;
            height: 100%;
        }

        .n2-ss-spinner-simple-white:before {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 20px;
            height: 20px;
            margin-top: -11px;
            margin-left: -11px;
        }

        .n2-ss-spinner-simple-white:not(:required):before {
            content: '';
            border-radius: 50%;
            border-top: 2px solid #333;
            border-right: 2px solid transparent;
            animation: n2SimpleWhite .6s linear infinite;
        }
        @keyframes n2SimpleWhite {
            to {transform: rotate(360deg);}
        }</style>
    <script type='text/javascript' src='/bds-01/wp-includes/js/jquery/jquery.js@ver=1.12.4-wp'></script>
    <script type='text/javascript' src='/bds-01/wp-includes/js/jquery/jquery-migrate.min.js@ver=1.4.1'></script>


    <script type="text/javascript">
        var doc = document.documentElement;
        doc.setAttribute('data-useragent', navigator.userAgent);
    </script>


    <script type="text/javascript">(function(){var N=this;N.N2_=N.N2_||{r:[],d:[]},N.N2R=N.N2R||function(){N.N2_.r.push(arguments)},N.N2D=N.N2D||function(){N.N2_.d.push(arguments)}}).call(window);if(!window.n2jQuery){window.n2jQuery={ready:function(cb){console.error('n2jQuery will be deprecated!');N2R(['$'],cb)}}}window.nextend={localization:{},ready:function(cb){console.error('nextend.ready will be deprecated!');N2R('documentReady',function($){cb.call(window,$)})}};</script>

    <script type="text/javascript" src="/bds-01/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/n2.min.js@ver=70fceec4"></script>

    <script type="text/javascript" src="/bds-01/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Application/Frontend/Assets/dist/smartslider-frontend.min.js@ver=70fceec4"></script>

    <script type="text/javascript" src="/bds-01/wp-content/plugins/smart-slider-3/Public/SmartSlider3/Slider/SliderType/Simple/Assets/dist/smartslider-simple-type-frontend.min.js@ver=70fceec4"></script>

  

    @stack('scriptHeader')

    {!! isset($information['google-anylic']) ? $information['google-anylic'] : '' !!}
	
    {!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}

</head>
<body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-1679 fusion-image-hovers fusion-body ltr fusion-sticky-header no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-flyout fusion-show-pagination-text fusion-header-layout-v2 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
    <div id="wrapper" class="">
        <div id="home" style="position:relative;top:-1px;"></div>

                <!-- slide đầu trang -->
        @include ('bds-01.block.header')
        <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
                <section id="content" class="full-width">
                    <div id="post-1679" class="post-1679 page type-page status-publish hentry">
                       
                        <div class="post-content">

                        </div>
                    </div>
                </section>
            </div>
        </main>
        @yield('content')
    </div>

</body>

	@if(isset($_COOKIE['form_value']))

  <?php 
      $cookie = unserialize($_COOKIE['form_value']);
  ?>

  <script async defer type="text/javascript">
    $(document).ready(function(){
      $("input[name='name']").val('{{ $cookie['name'] }}');
      $("input[name='phone']").val('{{ $cookie['phone'] }}');
      $("input[name='email']").val('{{ $cookie['email'] }}');
      $("input[name='address']").val('{{ $cookie['address'] }}');
    });
  </script>
  @endif

<script async defer>

  function contact(e) {
    var btn = jQuery(e).find('button');
  	btn.attr('disabled', 'disabled');
  	var data = jQuery(e).serialize();
    
    jQuery.ajax({
            type: "POST",
            url: '/submit/contact',
            data: data,
            success: function(result){
                var obj = jQuery.parseJSON( result);
				/* gửi thành công */
				console.log(obj);
				if (obj.status == 200) {
				  alert(obj.message);
				  btn.removeAttr('disabled');

				if(obj.redirect != null){
					location.href = obj.redirect;
				}

				  return;
				}
        
				/* gửi thất bại */
				if (obj.status == 500) {
					alert(obj.message);
					btn.removeAttr('disabled');          
					return;
				}
			},
			error: function(error) {
				/* alert('Lỗi gì đó đã xảy ra!') */
			}

        });

		return false;
  }
 
    

</script>
  
</html>
