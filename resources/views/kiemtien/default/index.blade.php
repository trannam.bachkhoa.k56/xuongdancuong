
@extends('kiemtien.layout.site')

@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : "")
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : "")
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : "")
    
@section('content')
   <section class="content pdt20">
        <div class="container">
            <div class="title">
                <h2 class="colortl titl">{!! isset($information['tieu-de-noi-dung']) ? $information['tieu-de-noi-dung'] : 'Danh sách mã giảm giá với moma.vn' !!}</h2>
				<p>{!! isset($information['noi-dung-tren-website']) ? $information['noi-dung-tren-website'] : '' !!}</p>
                  <p>
                     <button class="btn btn-success form-control" data-toggle="modal" data-target="#modalAffiliateMoma"><i class="fa fa-money" aria-hidden="true"></i> Tham gia đấu giá lên vị trí top 1</button>
                  </p>
            </div>
            <div class="row">
                  <div class="col-xs-12 col-md-12">
                     <table id="posts" class="table table-bordered table-striped">
                           <thead>
                              <tr>
                                 <th width="10%">Thứ tự</th>
                                 <th width="10%">Website</th>
                                 <th width="30%">Nội dung</th>
                                
                                 <th width="10%">Khuyến mãi</th>
                                 <th width="30%">QR giảm giá</th>
								 <th width="10%">Đường dẫn</th>
                                 <th width="15%">Tiền đấu thầu</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($affiliateMomas as $id => $affiliateMomaShow)
                                 <tr>
                                       <td width="10%">{!! ($id+1) !!}</td>
                                       <td>{!! !empty($affiliateMomaShow->domain) ? $affiliateMomaShow->domain : $affiliateMomaShow->url !!}</td>
                                       <td width="30%">{!! $affiliateMomaShow->title !!}</td>
                                       
                                       <td width="10%">{!! $affiliateMomaShow->affiliate !!}%</td>
                                       <td width="30%"><img src="https://chart.googleapis.com/chart?chs=100x100&cht=qr&chl={!! $affiliateMomaShow->link !!}&choe=UTF-8" /></td>
									   <td width="30%"><a href="{!! $affiliateMomaShow->link !!}" target="_blank">truy cập</a></td>
                                       <td width="15%">{!! !empty($affiliateMomaShow->money) ? number_format($affiliateMomaShow->money) : '0' !!} đ</td>
                                 </tr>
                              @endforeach
                           </tbody>
                     </table>
                      {!! $affiliateMomas->links() !!}
                     <p>
                        <button class="btn btn-success form-control" data-toggle="modal" data-target="#modalAffiliateMoma"><i class="fa fa-money" aria-hidden="true"></i> Tham gia đấu giá lên vị trí top 1</button>
                     </p>
                     <div id="fb-root"></div>
                     <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=653346515227406&autoLogAppEvents=1" nonce="ycjS3BHJ"></script>
                     <div class="fb-comments" data-href="https://kiemtien.moma.vn" data-numposts="5" data-width="100%"></div>
                  </div>
            </div>
         </div>
   </section> 

<!-- Modal -->
<div class="modal fade" id="modalAffiliateMoma" tabindex="-1" role="dialog" aria-labelledby="modalAffiliateMoma" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalAffiliateMoma">Tham gia đấu giá mã giảm giá</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <h4>Tôi chưa có website moma.vn!</h4>
            <div class="form-group">
               <a href="https://moma.vn/gift/840" class="btn btn-danger">Tạo website miễn phí với moma.vn</a>
            </div>
            <hr>
            <h4>Tôi đã có website của moma.vn!</h4>
            <div class="form-group">
               <p>Bước 1: truy cập vào quản trị website của bạn!</p>   
               <p>Bước 2: truy cập vào marekting!</p>
               <p>Bước 3: Vào mục affiliate!</p>
               <p>Bước 4: Điền đầy đủ thông tin và tham gia hệ thống!</p>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
      </div>
    </div>
  </div>
</div>

<script>
   function acceptAffiliate (e) {
      var domain = $('#domains').val();

      window.location = `${domain}/admin/affiliate`;
   }
</script>
@endsection


