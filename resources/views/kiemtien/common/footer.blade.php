<footer class="footerTop bgmt pdt20 pdb20">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <div class="titleFooter">
                     <h4 class="colorft">Tìm kiếm chúng tôi trên Facebook</h4>
                  </div>
                  <div class="contentFooter"> 
                     <iframe src="https://www.facebook.com/plugins/page.php?href=<?= isset($information['nhung-facebook']) ? $information['nhung-facebook']  : '' ?>&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media">
                     </iframe>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="titleFooter">
                     <h4 class="colorft">Liên hệ chúng tôi</h4>
                  </div>
                  <div class="contentFooter colorft">
                     {!! isset($information['thong-tin-lien-he']) ?  $information['thong-tin-lien-he'] : '' !!}
					 
					<ul class="thongke" style="list-style-type:none;padding-inline-start: 00px;">
						<li class="pd5-0"><i class="fas fa-user-check"></i> Đang Online: <span>145</span></li>
						<li class="pd5-0"><i class="fas fa-users"></i> Hôm nay: <span>2.056</span></li>
						<li class="pd5-0"><i class="fas fa-user-clock"></i> Hôm qua: <span>49.105</span></li>
						<li class="pd5-0"><i class="fas fa-signal"></i> Tuần này: <span>3.452.665</span></li>
						<li class="pd5-0"><i class="fas fa-temperature-high"></i> Tổng Truy Cập: <span>151.536.351</span></li>
					</ul>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="titleFooter">
                     <h4 class="colorft">Tin tức nổi bật</h4>
                  </div>
                  <div class="contentFooter colorft">
                     <nav class="nav flex-column">
						@if (!\App\Entity\Post::categoryShow('tin-tuc',5)->isEmpty()) 		
							@foreach(\App\Entity\Post::categoryShow('tin-tuc',5) as $post)
							<a class="nav-link item pdl0 pdr0" href="/tin-tuc/{{ $post->slug }}">
							   <div class="image">
								  <img src="{!! isset($post['image']) ?  $post['image'] : '' !!}" alt="{{ isset($post['title']) ?  $post['title'] : '' }}">
							   </div>
							   <div class="titleImage">
								  <span class="cutText2 colorft">{{ isset($post['title']) ?  $post['title'] : '' }}</span>
							   </div>
							</a>
							@endforeach
						@else
							@foreach(\App\Entity\Post::newPostMoma('huong-dan',5) as $post)
							<a class="nav-link item pdl0 pdr0" href="https://moma.vn/tin-tuc/{{ $post->slug }}" target="_blank">
							   <div class="image">
								  <img src="{{ isset($post->image) ?  $post->image :  (isset($information['logo']) ?  $information['logo'] : '') }}" alt="{{ isset($post['title']) ?  $post['title'] : '' }} ">
							   </div>
							   <div class="titleImage">
								  <span class="cutText2 colorft">{{ isset($post['title']) ?  $post['title'] : '' }}</span>
							   </div>
							</a>
							@endforeach
						@endif
                     </nav>
                  </div>
               </div>
            </div>
         </div>
      </footer>
		<?php
			$domain = App\Ultility\Ultility::getCurrentHttpHost();
			$userId = App\Entity\Domain::getUserIdWithUrlLike($domain);
			$user = App\Entity\User::getUserByUserId($userId['user_id']);
		?>
		@if(!empty($user) && $user->vip < 1)
			<div class="copyright">
				 <span><a href="https://moma.vn">Tạo website marketing miễn phí trọn đời tại moma.vn</a></span>
			</div>
		
		@else
			<div class="copyright">
				 <span>{{ isset($information['copyright']) ?  $information['copyright'] : '' }}</span>
			</div>

		@endif

      @include ('general.contact', [
		  'phone' => isset($information['so-dien-thoai']) ? $information['so-dien-thoai']  : '',
		  'zalo' => isset($information['chat-zalo']) ? $information['chat-zalo'] : '',
		  'fanpage' => isset($information['ten-tom-tat-fanpage-facebook']) ? $information['ten-tom-tat-fanpage-facebook'] : ''
      ])