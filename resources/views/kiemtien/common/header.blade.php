<header>
	<div class="headerTop bgmt" id="hideHeader">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-3 site-branding col-xs-12">
					<div class="logoHeader pdt10">
						<a href="/" title="logo">
							<img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" alt="">
						</a>
					</div>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12 white">
					<div class="row">
						<div class="contactHeaders inBlock col-md-3">
							<div class="contactHeader">
								<i class="fas fa-phone-alt"></i>
							</div>
							<div class="header-info">
								<span class="phone">{{ isset($information['so-dien-thoai']) && !($information['so-dien-thoai'] == '097.4xxx.xxx') ? $information['so-dien-thoai']  : $phoneUser }}</span>
							</div>
						</div>
						<div class="contactHeaders inBlock col-md-9">
							<div class="contactHeader">
								<i class="fas fa-home"></i>
							</div>
							<div class="header-info">
								<span class="phone">{{ isset($information['dia-chi-dau-trang']) ?  $information['dia-chi-dau-trang'] : '' }}</span>
								@if (\Illuminate\Support\Facades\Auth::check())
									<a href="/admin" class="admin" target="_blank">Quản trị website</a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<button class="openbtn" onclick="openNav()">☰</button>
<div id="mySidebar" class="sidebar" style="width:0px">
	<a href="javascript:void(0)" class="closebtn noBorder" onclick="closeNav()">×</a>
	<ul>
		<li>
			<a href="/">TRANG CHỦ</a>
		</li>
		@foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
		@if ($cateProduct->slug != 'san-pham-trang-chu')
		<li>
			<a class="nav-link textUpper black fw7 colorhv" href="/cua-hang/{{ $cateProduct->slug }}">{{ $cateProduct->title  }} 
			@if (!empty(\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product')))
			<span class="plusToggle"><i class="fas fa-plus-square" onclick="changeToggle(this)"></i> </span>
			@endif
			</a>
			@if (!empty(\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product')))
				<ul class="subMenu">
					@foreach (\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product') as $childProduct)
					<li class="nav-item">
						<a class="nav-link textUpper black fw7 colorhv menuPosition" href="/cua-hang/{{ $childProduct->slug }}">
						{{ $childProduct->title  }}
							@if (!empty(\App\Entity\Category::getChildrenCategory($childProduct->category_id, 'product')))
							<span class="plusToggle2"><i class="fas fa-plus-square" onclick="changeToggle(this)"></i> </span>
							@endif
						</a>
						@if (!empty(\App\Entity\Category::getChildrenCategory($childProduct->category_id, 'product')))
							<ul class="subMenu2">
								@foreach (\App\Entity\Category::getChildrenCategory($childProduct->category_id, 'product') as $childProduct2)
								<li class="nav-item">
									<a class="nav-link textUpper black fw7 colorhv menuPosition" href="/cua-hang/{{ $childProduct2->slug }}">{{ $childProduct2->title  }}</a>
								</li>
								@endforeach
							</ul>
						@endif
						
					</li>
					@endforeach
				</ul>
			@endif
		</li>
		@endif
		@endforeach
		<li>
			<a href="/danh-muc/tin-tuc">TIN TỨC
			@if (!empty(\App\Entity\Category::getChildrenCategory(0, 'post')))
			<span class="plusToggle"><i class="fas fa-minus-square" onclick="changeToggle(this)"></i> </span>
			@endif
			</a>
			@if (!empty(\App\Entity\Category::getChildrenCategory(0, 'post')))
			<ul class="subMenu" style="display: block;">
				@foreach (\App\Entity\Category::getChildrenCategory(0, 'post') as $childProduct)
				<li class="nav-item">
					<a class="nav-link textUpper black fw7 colorhv menuPosition" href="/danh-muc/{{ $childProduct->slug }}">{{ $childProduct->title  }}</a>
				</li>
				@endforeach
			</ul>
			@endif
		</li>
		<li>
			<a href="/lien-he">LIÊN HỆ</a>
		</li>
	</ul>
	<script async defer>
	function changeToggle(e) {
		if($(e).hasClass('fa-plus-square')) {
			$(e).removeClass('fa-plus-square');
			$(e).addClass('fa-minus-square');
		} else {
			$(e).addClass('fa-plus-square');
			$(e).removeClass('fa-minus-square');
		}
			
	}
		$(document).ready(function(){
		  $(".plusToggle").click(function(){
			  $(this).parent().parent().find('.subMenu').toggle();
				
			return false;
		  });
		  $(".plusToggle2").click(function(){
			  $(this).parent().parent().find('.subMenu2').toggle();
			
			return false;
		  });
		});
	</script>
	
	
</div>
<style>


	.sticky {
	  position: fixed;
	  top: 0;
	  width: 100%;
	  padding-top: 0;
		padding-bottom: 0;
		background-color: #2125298a;
		color: white;
	}
	.sticky a{
		color: white
	}
	.sticky + .content {
	  padding-top: 102px;
	}
</style>
<script async defer>
window.onscroll = function() {myFunction()};
var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>
