<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" 					content="width=device-width, initial-scale=1.0" />   
    <meta http-equiv="Content-Type" 		content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
	<meta name="title" 						content="@yield('title')" />
    <meta name="description" 				content="@yield('meta_description')" />
    <meta name="keywords" 					content="@yield('keywords')" />
	<meta name="theme-color"				content="#07a0d2" />
	<meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="canonical" href="@yield('meta_url')"/>
	<meta http-eqiv="Content-Language" content="vi">
	
  <link rel="icon" 		type="image/x-icon"				href="{{ !empty($information['icon']) ?  asset($information['icon']) : '' }}"  />
  
	<!--<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/bootstrap.min.css" media="screen" />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/extra.css" media="screen"  />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/style.css"  media="screen" />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/animate.css"  media="screen" />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/hover.css" media="screen" />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/all.css" media="screen" />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/slick.css" media="screen" />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/slick-theme.css" media="screen" />
	<link rel="stylesheet"	type="text/css" href="/public/xhome1/css/fontawesome.min.css" media="screen" />
	<link rel="stylesheet"	type="text/css" href="/public/xhome1/css/drawer.min.css" media="screen" />
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/hover-min.css" media="screen" /> -->
	
	<link rel="stylesheet" 	type="text/css" href="/public/xhome1/css/general.css" media="screen" />

	<script  src="/public/xhome1/js/jquery.min.js" ></script>
	<script async defer src="/public/xhome1/js/bootstrap.min.js" ></script>
	<script async defer  src="/public/xhome1/js/slick.min.js" ></script>
	<!--script src="/public/xhome1/js/wow.js"></script-->
	<script async defer src="/public/xhome1/js/iscroll.min.js" ></script>
	<script async defer  src="/public/xhome1/js/drawer.min.js" ></script>
	<script   src="/public/xhome1/js/jquery.sticky-kit.js" ></script>
	<script  src="/public/xhome1/js/jquery.matchHeight-min.js"></script>

    @stack('scriptHeader')

    {!! isset($information['google-anylic']) ? $information['google-anylic'] : '' !!}
	
	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}
	
	<style>
		/* Màu chữ toàn trang */
		.colormt {
			color: {{ isset($colorWebsite->text_full) ? $colorWebsite->text_full : 'black' }};
			}
		/* Màu background */
		.bgmt {
			background: {{ isset($colorWebsite->background_full) ? $colorWebsite->background_full : 'black' }};
			}
		/* Màu link chữ khi hover */
		.colorhv:hover {
			color: {{ isset($colorWebsite->color_hover) ? $colorWebsite->color_hover : 'red' }};
			}
		/* Màu các thẻ tiêu đề */
		.colortl {
			color: {{ isset($colorWebsite->color_title) ? $colorWebsite->color_title : 'black' }};
			}
		/* Màu chữ footer */
		.colorft {
			color: {{ isset($colorWebsite->color_footer) ? $colorWebsite->color_footer : 'white' }};
			}
	</style>

  

</head>
<body class="f3 f1">
	@include('kiemtien.common.header')
	@yield('content')
	@include('kiemtien.common.footer')
</body>

	@if(isset($_COOKIE['form_value']))

  <?php 
      $cookie = unserialize($_COOKIE['form_value']);

  ?>
  <script async defer type="text/javascript">
    $(document).ready(function(){
      $("input[name='name']").val('{{ $cookie['name'] }}');
      $("input[name='phone']").val('{{ $cookie['phone'] }}');
      $("input[name='email']").val('{{ $cookie['email'] }}');
      $("input[name='address']").val('{{ $cookie['address'] }}');
    });
  </script>
  @endif
      
  

<script type="text/javascript">
	/* Đồng bộ chiều cao các div */
	$(function() {
		$('.boxP').matchHeight();
	});
</script>
<script type="text/javascript">
        /* SLIDE */
      
      /* SLIDE PRODUCT */
     /* $('.slideProduct').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 3000,
        responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      ]
      }); */
        /* SLIDE Customer */
      
   </script>
<script async defer>


    function openNav() {
      $('#mySidebar').css('width','300px');
       $('#main').css('marginLeft','300px');
    }

    function closeNav() {
      document.getElementById("mySidebar").style.width = "0";
      document.getElementById("main").style.marginLeft= "0";
    }
</script>

<script async defer>
    $('.itemProduct').matchHeight();

    function subcribeEmailSubmit(e) {
        var email = $(e).find('.emailSubmit').val();
        var token =  $(e).find('input[name=_token]').val();

        $.ajax({
            type: "POST",
            url: '{!! route('subcribe_email') !!}',
            data: {
                email: email,
                _token: token
            },
            success: function(data) {
                var obj = jQuery.parseJSON(data);

                alert(obj.message);
            }
        });
        return false;
    }

    function addToOrder(e) {
        var data = $(e).serialize();
       
        $.ajax({
            type: "POST",
            url: '{!! route('addToCart') !!}',
            data: data,
            success: function(result){
                var obj = jQuery.parseJSON( result);

                window.location.replace("/gio-hang");
            },
            error: function(error) {
            }

        });

        return false;
    }
  
  function contact(e) {
    var $btn = $(e).find('button');
  	$btn.attr('disabled', 'disabled');
  	var data = $(e).serialize();
    
    $.ajax({
            type: "POST",
            url: '/submit/contact',
            data: data,
            success: function(result){
                var obj = jQuery.parseJSON( result);
				/* gửi thành công */
				console.log(obj);
				if (obj.status == 200) {
				  alert(obj.message);
				  $btn.removeAttr('disabled');

				if(obj.redirect != null){
					location.href = obj.redirect;
				}

				  return;
				}
        
				/* gửi thất bại */
				if (obj.status == 500) {
					alert(obj.message);
					$btn.removeAttr('disabled');          
					return;
				}
			},
			error: function(error) {
				/* alert('Lỗi gì đó đã xảy ra!') */
			}

        });

    return false;
  }
  
	/* $( document ).ready(function () {
		$.ajax({
            type: "get",
            url: '{!! route('views_website') !!}',
            data: {
                source: '{!! isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $domainUrl !!}',
            },
            success: function(result){
				
			},
			error: function(error) {
			}

        });
	}); */
    

</script>
  
</html>
