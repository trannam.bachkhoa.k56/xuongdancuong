
<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
    <title>@yield('title')</title>
    <!-- meta -->
    <meta name="ROBOTS" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->

    <link rel="icon" href="{{ !empty($information['icon-logo']) ?  asset($information['icon-logo']) : '' }}" type="image/x-icon" />

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />

 
	   
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i&amp;subset=vietnamese" rel="stylesheet">

    {{--CSS--}}
    {{--JS--}}
    
	

	<link href="{{ asset('vattucaycanhnew/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('vattucaycanhnew/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" media="screen" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
	
   
    <link href="{{ asset('vattucaycanhnew/css/stylesheet.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('vattucaycanhnew/css/responsive.css') }}" rel="stylesheet" media="screen" />
	
	<link href="{{ asset('vattucaycanhnew/owl-carousel/owl.carousel.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('vattucaycanhnew/owl-carousel/owl.transitions.css') }}" rel="stylesheet" media="screen" />
   
     <link href="{{ asset('vattucaycanhnew/css/customCss.css') }}" rel="stylesheet" media="screen" />
	 
	 <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
	 



    <!-- <link href="vattucaycanhnew/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="vattucaycanhnew/css/stylesheet.css" rel="stylesheet">
    <link href="vattucaycanhnew/css/responsive.css" rel="stylesheet">
    <link href="vattucaycanhnew/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="vattucaycanhnew/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet" media="screen" /> -->


     <script src="{{ asset('vattucaycanhnew/javascript/jquery-2.1.1.min.js')}} " type="text/javascript"></script>
     <script src="{{ asset('vattucaycanhnew/bootstrap/js/bootstrap.min.js')}} " type="text/javascript"></script>
     <script src="{{ asset('vattucaycanhnew/javascript/jstree.min.js')}} " type="text/javascript"></script>
     <script src="{{ asset('vattucaycanhnew/javascript/template.js')}} " type="text/javascript"></script>
     <script src="{{ asset('vattucaycanhnew/javascript/common.js')}} " type="text/javascript"></script>
     <script src="{{ asset('vattucaycanhnew/javascript/global.js')}} " type="text/javascript"></script>
     <script src="{{ asset('vattucaycanhnew/owl-carousel/owl.carousel.min.js')}} " type="text/javascript"></script>
     <script src="{{ asset('vattucaycanhnew/javascript/DioProgress.js')}} " type="text/javascript"></script>
	<script src="{{ asset('vattucaycanhnew/javascript/numeral.min.js')}} " type="text/javascript"></script>
	<script src="{{ asset('vattucaycanhnew/javascript/jquery.matchHeight-min.js')}} " type="text/javascript"></script>
   


    <!-- <script src="vattucaycanhnew/javascript/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="vattucaycanhnew/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="vattucaycanhnew/javascript/jstree.min.js" type="text/javascript"></script>
    <script src="vattucaycanhnew/javascript/template.js" type="text/javascript" ></script>
    <script src="vattucaycanhnew/javascript/common.js" type="text/javascript"></script>
    <script src="vattucaycanhnew/javascript/global.js" type="text/javascript"></script>
    <script src="vattucaycanhnew/owl-carousel/owl.carousel.min.js" type="text/javascript"></script> -->
<!-- Google Tag Manager -->

</head>

<body>
	<?php echo 1; exit;?>
    @include('vattucaycanhnew.common.header')

    @yield('content')

    @include('vattucaycanhnew.common.footer')
 
<script>
    function subcribeEmailSubmit(e) {
        var email = $(e).find('.emailSubmit').val();
        var token =  $(e).find('input[name=_token]').val();

        $.ajax({
            type: "POST",
            url: '{!! route('subcribe_email') !!}',
            data: {
                email: email,
                _token: token
            },
            success: function(data) {
                var obj = jQuery.parseJSON(data);

                alert(obj.message);
            }
        });
        return false;
    }

    function addToOrder(e) {
        var gold = $(e).find('.goldVal').val();
        var size = $(e).find('.sizeVal').val();
        var properties = 'vàng: ' + gold + ' - cỡ: '+ size;
        $(e).find('.properties').val(properties);
        var data = $(e).serialize();

        $.ajax({
            type: "POST",
            url: '{!! route('addToCart') !!}',
            data: data,
            success: function(result){
                var obj = jQuery.parseJSON( result);

                window.location.replace("/gio-hang");
            },
            error: function(error) {
                alert('Lỗi gì đó đã xảy ra!')
            }

        });

        return false;
    }
	
	//Đồng bộ chiều cao các div
	$(function() {
	  $('.product-detail .product-name').matchHeight();
	});
	$(function() {
	  $('#content .blog-wrapper .blog-name h2').matchHeight();
	});

</script>


</body>
</html>