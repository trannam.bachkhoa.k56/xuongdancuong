@extends('vattucaycanhnew.layout.site')

@section('title', isset($product->title) ? $product->title : '' )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)


@section('content')
  <div class="container">
    <div class="breadcrumb" style="padding: 8px 10px">
       <?php \App\Entity\Post::getBreadcrumb($product->post_id, 'san-pham')?>
    </div>
 
  <div class="row">
   
    @include('vattucaycanhnew.partials.sidebar')
    <div id="content" class="col-sm-9">
      <div class="row">
        <div class="col-sm-6">

          <div class="thumbnails">

            <div>
              @if(!empty($product->image_list))
                @foreach(explode(',', $product->image_list) as $idImage => $imageProduct)
                      @if($idImage == 0)
              <a class="thumbnail" href="{{ isset($imageProduct) ? $imageProduct : ''}}" title="{{ isset($product['title']) ?$product['title']  : ''}}"><img src="{{ isset($imageProduct) ? $imageProduct : ''}}" title="{{ isset($product['title']) ?$product['title']  : ''}}" alt="{{ isset($product['title']) ?$product['title']  : ''}}" /></a>
                @endif
              @endforeach
              @endif
            </div>
            <div id="product-thumbnail" class="owl-carousel">
            @if(!empty($product->image_list))
                @foreach(explode(',', $product->image_list) as $idImage => $imageProduct)
              <div class="item">
                <div class="image-additional"><a class="thumbnail  " href="{{ isset($imageProduct) ? $imageProduct : ''}}" title="{{ isset($product['title']) ?$product['title']  : ''}}"> <img src="{{ isset($imageProduct) ? $imageProduct : ''}}" title="{{ isset($product['title']) ?$product['title']  : ''}}" alt="{{ isset($product['title']) ?$product['title']  : ''}}" /></a></div>
              </div>
             @endforeach
              @endif  
              
            </div>
          </div>


        </div>
        <div class="col-sm-6">
          <h1 class="productpage-title">{{ isset($product['title']) ?$product['title']  : ''}}</h1>

          



    

          <ul class="list-unstyled productinfo-details-top pdleft">
             @if($product['discount'] > 0)
             <li>
              <h2 class="productpage-price">{{ number_format( $product['discount'] , 0) }} vnđ</h2>
            </li>
            <li><span class="productinfo-tax"><del>{{ number_format( $product['price'] , 0) }} </del> vnđ</span></li>
             @else
             <li>
              <h2 class="productpage-price">{{ number_format( $product['price'] , 0) }} vnđ</h2>
            </li>
              @endif
            
            
          </ul>
		  <div class="rating pdleft">
			@if(isset($product['danh-gia-sao']))
				<?php 
					$starBlack = 5 - $product['danh-gia-sao'];
				?>
				@for($i = 0 ;$i < $product['danh-gia-sao'];$i++)
					<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> 
				@endfor
				
				@for($i = 0 ;$i < $starBlack ;$i++)
					<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
				@endfor
			@endif
			</div>
          <hr>
          <ul class="list-unstyled product_info pdleft">
            <li>
              <label>Mã sản phẩm:</label>
              <span> {{ isset($product['code']) ? $product['code'] : ''}}</span></li>
            <li>
              <label>Thương hiệu:</label>
              <span> {{ isset($product['thuong-hieu']) ? $product['thuong-hieu'] : ''}}</span></li>
            <li>
              <label>Tình trạng:</label>
              <span> {{ isset($product['tinh-trang']) ? $product['tinh-trang'] : ''}}</span></li>
          </ul>
          <hr>
          <p class="product-desc"> {{ isset($product['description']) ?$product['description']  : ''}}</p>
          <div id="product">

              <div class="button-group pdleft">

                  <form onsubmit="return addToOrder(this);" enctype="multipart/form-data"
                                  id="add-to-cart-form" method="post" accept-charset="utf-8">
                                {{ csrf_field() }}
                                <input type="number" class="quantity" name="quantity[]" value="1"/>
                                <input type="hidden" class="product_id" name="product_id[]"
                                       value="{{ $product->product_id }}"/>
                                <div class="clearfix"></div>
                                <p></p>
                                <button class="dat_hang add-cart-0" style="display: block;  border: none;"
                                        type="submit">Thêm vào giỏ hàng
                                </button>
                            </form>

<!-- 
                 <form onsubmit="return addToOrder(this);" method="post" accept-charset="utf-8" id="" enctype="multipart/form-data">
                   {{ csrf_field() }}
                  <input type="number" class="input_quantity quantity" id="input_quantity" name="quantity[]" value="1" style="padding-left: 20px; " />
                  <input type="hidden" class="input_quantity product_id" id="input_quantity" name="product_id[]" value="{{ $product->product_id }}"> 
                  <br>
                  <p></p>
                <button type="submit" class="addtocart-btn" >Thêm vào giỏ hàng</button>
                  </form> -->
              </div>
          </div>
        </div>
      </div>
      <div class="productinfo-tab">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-description" data-toggle="tab">Thông tin</a></li>
         
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab-description">
            <div class="cpt_product_description ">
              <div>
                {!! isset($product['content']) ? $product['content'] : '' !!}
              </div>
            </div>
            <!-- cpt_container_end --></div>
        
        </div>
      </div>
      <h3 class="productblock-title">Sản phẩm liên quan</h3>
      <div class="box">
        <div id="related-slidertab" class="row owl-carousel product-slider">
         @foreach(\App\Entity\Product::relativeProduct($product->slug, $product->product_id, 20) as $id => $product)
          <div class="item">
            
            @include('vattucaycanhnew.partials.item-product')
           
          </div>
           @endforeach

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
