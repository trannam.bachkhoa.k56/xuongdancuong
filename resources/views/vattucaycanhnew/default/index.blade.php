@extends('vattucaycanhnew.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')

@include('vattucaycanhnew.partials.slider')
<style>
@media only screen and (min-width: 1000px)
{
	#content
	{
		width:100%;
	}
}
</style>
<div class="container">
  <div class="row">
        @php 
          $bannerTops = array();
          foreach (\App\Entity\SubPost::showSubPost('danh-sach-banner-1', 4) as $id => $bannertop) {
            $bannerTops[$bannertop['vi-tri-banner']] = $bannertop;
          }
        @endphp
        @if(isset($bannerTops[1]))
          <div class="col-sm-4 cms-banner-left  ads-banner"> 
            <a href="{{ isset($bannerTops[1]['link-banner']) ? $bannerTops[1]['link-banner'] : '#' }}"><img alt="{{ isset($bannerTops[1]['title']) ? $bannerTops[1]['title'] : '' }}" src="{{ isset($bannerTops[1]['image']) ? $bannerTops[1]['image'] : '/vattucaycanhnew/image/banners/subbannertop.jpg' }}"></a> 
          </div>
        @endif
        <div class="col-sm-4 cms-banner-middle-top  ads-banner">
            @if(isset($bannerTops[2]))
            <div class="md1"><a href="{{ isset($bannerTops[2]['link-banner']) ? $bannerTops[2]['link-banne'] : '#' }}"> <img alt="{{ isset($bannerTops[2]['title']) ? $bannerTops[2]['title'] : '' }}" src="{{ isset($bannerTops[2]['image']) ? $bannerTops[2]['image'] : '/vattucaycanhnew/image/banners/subbanner2.jpg' }}"></a> 
            </div>
            @endif
            @if(isset($bannerTops[3]))
             <div class="md2"><a href="{{ isset($bannerTops[3]['link-banner']) ? $bannerTops[3]['link-banne'] : '#' }}"> <img alt="{{ isset($bannerTops[3]['title']) ? $bannerTops[3]['title'] : '' }}" src="{{ isset($bannerTops[3]['image']) ? $bannerTops[3]['image'] : '/vattucaycanhnew/image/banners/subbanner2-1.jpg' }}"></a> 
            </div>
             @endif
        </div>
        
        @if(isset($bannerTops[4]))
        <div class="col-sm-4 cms-banner-right  ads-banner"> 
          <a href="{{ isset($bannerTops[4]['link-banner']) ? $bannerTops[4]['link-banne'] : '#' }}"><img alt="{{ isset($bannerTops[4]['title']) ? $bannerTops[4]['title'] : '' }}" src="{{ isset($bannerTops[4]['image']) ? $bannerTops[4]['image'] : '/vattucaycanhnew/image/banners/subbanner3.jpg' }}"></a> 
        </div>
        @endif
  </div>

  <div class="row">
    <div id="content" class="col-sm-12">
      <div class="customtab">
        <div id="tabs" class="customtab-wrapper">
          <ul class='customtab-inner'>
             @foreach (\App\Entity\Menu::showWithLocation('menu-category_product') as $Mainmenucate)
                @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenucate->slug) as $id=>$menutab)
                  <li class='tab'><a href="#tab-{{ $id }}">{{ $menutab['title_show'] }}</a></li>
               @endforeach 
             @endforeach   
          </ul>
        </div>
        
        @foreach (\App\Entity\Menu::showWithLocation('menu-category_product') as $Mainmenucate)
          @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenucate->slug) as $id=>$menutab)  
          <div id="tab-{{ $id }}" class="tab-content">
            
            <div class="box">
              @if($id == 0)
              <div id="latest-slidertab" class="row owl-carousel product-slider">
              @elseif($id == 1)  
              <div id="special-slidertab" class="row owl-carousel product-slider">
              @elseif($id == 2)
              <div id="bestseller-slidertab" class="row owl-carousel product-slider">
              @endif
                <?php $urls = explode('/', $menutab['url']) ?>
                @foreach(\App\Entity\Product::showProduct($urls[2],8) as $id => $product)
                <div class="item">
                  <!-- item-product -->
                  @include('vattucaycanhnew.partials.item-product')
                </div>
                @endforeach  
              </div>
            </div>
          </div>
          @endforeach 
        @endforeach   

         @foreach (\App\Entity\Menu::showWithLocation('show-category-index') as  $Mainmenu)
          @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id => $menuCateIndex)
          <h3 class="productblock-title">
        
          {{ $menuCateIndex['title_show'] }}
        </h3>
        <div class="box">
          <div id="Weekly-slider{{ $menuCateIndex['menu_id'] }}" class="row owl-carousel product-slider">
            <?php $urls = explode('/', $menuCateIndex['url']) ?>
             @foreach(\App\Entity\Product::showProduct($urls[2],8) as $id => $product)
            <div class="item product-slider-item">
              @include('vattucaycanhnew.partials.item-product')
            </div>
            @endforeach
          </div>
        </div>
        <script type="text/javascript">
          $('#Weekly-slider' + {{ $menuCateIndex['menu_id'] }}).owlCarousel({
            items: 4,
            navigation: true,
            pagination: false,
            itemsDesktop : [1199, 3],
            itemsDesktopSmall : [979, 2],
            itemsTablet : [768, 2],
            itemsTabletSmall : false,
            itemsMobile : [479, 1]
          });
        </script>
         @endforeach
        @endforeach

      </div>

     @include('vattucaycanhnew.partials.khach-hang')

      <div class="row mgbottom20 text-ct">
          @foreach(\App\Entity\SubPost::showSubPost('danh-sach-banner-2', 2) as $id => $bannerBottom)
          @if($bannerBottom['vi-tri-banner'] == 1)
            <div class="col-md-6 cms-banner-left ads-banner"> <a href="{{ isset($bannerBottom['link-banner']) ? $bannerBottom['link-banne'] : '#' }}"><img alt="{{ isset($bannerBottom['title']) ? $bannerBottom['title'] : '' }}" src="{{ isset($bannerBottom['image']) ? $bannerBottom['image'] : 'vattucaycanhnew/image/banners/subbanner5.jpg'}}"></a> </div>
          @endif
          @if($bannerBottom['vi-tri-banner'] == 2)
            <div class="col-md-6 cms-banner-middle  ads-banner"> <a href="{{ isset($bannerBottom['link-banner']) ? $bannerBottom['link-banne'] : '#' }}"><img alt="{{ isset($bannerBottom['title']) ? $bannerBottom['title'] : '' }}" src="{{ isset($bannerBottom['image']) ? $bannerBottom['image'] : 'vattucaycanhnew/image/banners/subbanner6.jpg'}}"></a> </div>
          @endif
          @endforeach
      </div>

      <div id="subbanner4" class="text-ct">
        <div class="item text-ct  ads-banner">
          @foreach(\App\Entity\SubPost::showSubPost('danh-sach-banner-2', 4) as $id => $bannerBottom)
            @if($bannerBottom['vi-tri-banner'] == 4)
              <a href="{{ isset($bannerBottom['link-banner']) ? $bannerBottom['link-banne'] : '#' }}"><img class="img-responsive"  alt="{{ isset($bannerBottom['title']) ? $bannerBottom['title'] : '' }}" src="{{ isset($bannerBottom['image']) ? $bannerBottom['image'] : 'vattucaycanhnew/image/banners/subbanner4.jpg'}}"></a>
            @endif
          @endforeach 
        </div>
      </div>
      @include('vattucaycanhnew.partials.blog')
     <!-- KHACH HANG -->
      @include('vattucaycanhnew.partials.doi-tac')
    </div>
  </div>
</div>
@endsection