@extends('vattucaycanhnew.layout.site')

@section('title', isset($category->title) ? $category->title : '')
@section('meta_description',  isset($category->description) ? $category->description : '' )
@section('keywords', '')

@section('content')
  <div class="container">
  <ul class="breadcrumb" style="padding:8px 10px">
    <li><a href="/"><i class="fa fa-home"></i></a></li>
    <li><a>{{ isset($category['title']) ? $category['title'] : ''}}</a></li>
  </ul>
  <div class="row">
  <style>
  #content .product-list .product-thumb .rating {
    float: right;
    display: inline-block;
    margin: 0 0 10px;
}
.FitterHidden
{
	display:block;
}
  </style>
   <!-- SIDEBAR -->
   	@include('vattucaycanhnew.partials.sidebar')

    <div id="content" class="col-sm-9">
      <h2 class="category-title">{{ isset($category['title']) ? $category['title'] : ''}}</h2>
      <div class="row category-banner">
        <div class="col-sm-12 category-image"><img src="{{ isset($category['image']) ? $category['image'] : 'vattucaycanhnew/image/banners/category-banner.jpg' }}" alt="Desktops" title="Desktops" class="img-thumbnail" /></div>
        <div class="col-sm-12 category-desc">{{ isset($category['description']) ? $category['description'] : ''}}</div>
      </div>
      <div class="category-page-wrapper">
        <div class="col-md-12 text-right page-wrapper" style="margin-right: 15px;">

        	<form id="frm_filter_header" method="get" action="/cua-hang/{{ $category->slug }}" class="sortProduct">
               
                    <label style="padding: 10px ">Sắp xếp:</label>
                    <select name="sort" id="frm_filter_header_sort_by" onchange="return sortProduct(this);" style="margin: -10px;" >
                        <option value="default" style="margin: -10px;">Mặc định</option>
                        <option value="priceIncrease"
                                {{ (isset($_GET['sort']) && ($_GET['sort'] == 'priceIncrease') ) ? 'selected' : ''}}>Giá tăng dần
                        </option>
                        <option value="priceReduction"
                        {{ (isset($_GET['sort']) && ($_GET['sort'] == 'priceReduction') ) ? 'selected' : ''}}>
                        Giá giảm dần
                        </option>
                        <option value="sortName"
                                {{ (isset($_GET['sort']) && ($_GET['sort'] == 'sortName') ) ? 'selected' : ''}}>
                            Xếp theo tên
                        </option>
                    </select>
               
            </form>
            <script>
            function sortProduct(e) {
                $('.sortProduct').submit();
            }
        </script>

        </div>
        
    
      <br />
      <div class="grid-list-wrapper">
        <div class="product-layout product-list col-xs-12">
			@if($products->isEmpty())
                 <p style="color:red;padding-left: 10px">Không tìm thấy sản phẩm phù hợp !</p>       
            @else	
           @foreach ($products as $id => $product)
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
			   
				@include('vattucaycanhnew.partials.item-product')
			</div>
                  
            @endforeach
			@endif
        </div>
		
		
		
		
		
        
		
		
		
		
      </div>
      <div class="category-page-wrapper">
        
        <div class="pagination-inner">

          	@if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                {{ $products->links() }}
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
</div>

@endsection


