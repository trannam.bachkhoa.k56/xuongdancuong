@extends('diamond.layout.site')

@section('title','Đăng ký')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    @include('diamond.partials.slider')
    <section class="filtercategory">
      <div class="container">
        <div class="row">

          <div class="col-12 breackrum">
            <div class="">
              <ul>
                <li><a href="/" title="">Trang chủ</a></li>
                <a href="/dang-ky" class="active">Đăng ký</a>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
	<section class="detailnew contact">
        <div class="container">
            <div class="row bgdetail">
                <div class="col-12 colcontact">
                    <div class="register hotDeal clearfix">
                        <div class="mainTitle lineblue">
                            <h1 class="titleV bgblue"><i class="fa fa-key" aria-hidden="true"></i> Đăng ký</h1>
                        </div>
                        <div class="underline"></div>
                        <div class="col-md-8 col-xs-12">
                            <p>Vui lòng điền đầy đủ thông tin bên dưới, các mục có dấu <span class="red">(*)</span> là bắt buộc</p>
                            <form action="/dang-ky" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Họ và tên <span class="red">(*)</span></label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Họ và tên" />
                                </div>
                                <div class="form-group">
                                    <label>Email <span class="red">(*)</span></label>
                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required/>
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="control-label">Mật khẩu <span class="red">(*)</span></label>

                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password-confirm" class="control-label">Nhập lại mật khẩu <span class="red">(*)</span></label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                                <div class="form-group">
                                    <label> <input type="checkbox" name="is_register" value="1" required/> Tôi xác nhận đồng ý với các
                                        <a href="">điều khoản</a> của website</label>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger">Đăng ký</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
