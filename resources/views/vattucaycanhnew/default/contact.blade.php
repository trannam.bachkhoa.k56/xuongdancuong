@extends('vattucaycanhnew.layout.site')

@section('title', "Liên hệ" )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
 <div class="container">
  <ul class="breadcrumb" style="padding: 8px 10px">
    <li><a href="/"><i class="fa fa-home"></i></a></li>
    <li><a>Liên hệ</a></li>
  </ul>
  <div class="row">
     @include('vattucaycanhnew.partials.sidebar')

    <div class="col-sm-9" id="content">
      <h1>Liên hệ</h1>
      
    
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-4 left">
              <address>
              <strong>Địa chỉ:</strong>
              <div class="address"> {{ isset($information['dia-chi']) ?  $information['dia-chi'] : '' }}</div>
              <br>
              <strong>Email:</strong>{{ isset($information['email']) ?  $information['email'] : '' }}<br>
              <br>
              <strong>Số điện thoại: </strong>{{ isset($information['so-dien-thoai']) ?  $information['so-dien-thoai'] : '' }}
              </address>
            </div>
            <div class="col-sm-8 rigt">
              <div class="map">
                {!! isset($information['google-map']) ?  $information['google-map'] : '' !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        @if(isset($success))
          <div class="col-md-12" style="margin:20px 0">
              <p style="color: red; font-size: 18px;text-align: center;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
          </div>
       @endif
      </div>
      <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('sub_contact') }}" onSubmit="return contact(this);">
        {!! csrf_field() !!}
        <fieldset>
          <h3> Form Liên hệ</h3>

          <div class="form-group required">
            <label for="input-name" class="col-sm-2 control-label">Họ và tên</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input-name" value="" name="name">
            </div>
          </div>
          <div class="form-group required">
            <label for="input-name" class="col-sm-2 control-label">Số điện thoại</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="input-name" value="" name="phone">
            </div>
          </div>
          <div class="form-group required">
            <label for="input-email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="input-email" value="" name="email">
            </div>
          </div>
          <div class="form-group required">
            <label for="input-enquiry" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
              <textarea class="form-control" id="input-enquiry" rows="10" name="message" placeholder="Nội dung"></textarea>
            </div>
          </div>
        </fieldset>
        <div class="buttons">
          <div class="pull-right">
            <input type="submit" value="Gửi liên hệ" class="btn btn-primary">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
