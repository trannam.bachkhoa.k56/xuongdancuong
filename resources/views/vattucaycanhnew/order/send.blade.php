@extends('vattucaycanh.layout.site')

@section('title','Gửi đơn hàng thành công')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )

@section('content')
   
<div class="container">
  <ul class="breadcrumb" style="padding: 8px 10px;">
    <li><a href="/"><i class="fa fa-home"></i></a></li>
    <li><a>Thanh toán </a></li>
  </ul>
  <div class="row">
    @include('vattucaycanh.partials.sidebar')
     <div class="col-sm-9" id="content">
       <div class="row">
            <div class="col-12">
                <h1>Giỏ hàng</h1>
            </div>
            <form action="{{ route('send') }}" class="formCheckOut validate" method="post">
                {{ csrf_field() }}
                <div class="col-12 order">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">ảnh</th>
                            <th scope="col">Sản phẩm</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Tổng số tiền</th>
                        </tr>
                        </thead>
                        <?php $sumPrice = 0;?>
                        <tbody>
                        @if (!empty($orderItems))
                            @foreach($orderItems as $id => $orderItem)
                                <tr>
                                    <td >
                                        <a href="{{ route('post', ['cate_slug' => 'san-pham', 'post_slug' => $orderItem->slug]) }}">
                                            <img src="{{ !empty($orderItem->image) ?  asset($orderItem->image) : asset('/site/img/no-image.png') }}" alt="{{ $orderItem->title }}" width="50"> </a>
                                    </td>
                                    <td>
                                        <div class="content">
                                            <h3><a href="{{ route('post', ['cate_slug' => 'san-pham', 'post_slug' => $orderItem->slug]) }}">{{ $orderItem->title }}-{{$orderItem->color}}-{{ $orderItem->size }}</a></h3>
                                            <p>Thông số: {{ $orderItem->properties }}</p>
                                            <p class="price">
                                                @if (!empty($orderItem->discount))
                                                    <span class="discont">Giá : <del>{{ number_format($orderItem->price , 0) }} VND</del>{{ number_format($orderItem->discount , 0) }} VND</span>
                                                @else
                                                    <span class="discont">Giá : {{ number_format($orderItem->price , 0) }} VND</span>
                                                @endif
                                            </p>
                                        </div>
                                    </td>
                                    <td>{{ $orderItem->quantity }}</td>
                                    <td>
                                        <span class="total totalPrice"><?php $sumPrice += !empty($orderItem->discount) ? ($orderItem->discount*$orderItem->quantity) : ($orderItem->price*$orderItem->quantity) ?>
                                            {{ !empty($orderItem->discount) ? number_format(($orderItem->discount*$orderItem->quantity) , 0) : number_format(($orderItem->price*$orderItem->quantity) , 0, ',', '.') }}</span>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            
                            <td colspan="4" rowspan="" headers="" style="text-align: right;">
                                <a href="" title="" class="total">Thành tiền : <span class="sumPrice">{{ number_format($sumPrice , 0) }}</span> VND </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pay">
                    <h3 class="titleV bgorange">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>Thông tin nhận hàng
                    </h3>
                    <div class="col-12 col-md-12">
                        <p>Tên người đặt hàng : {{ $customer['ship_name'] }}</p>
                        <p>Điện thoại: {{ $customer['ship_phone'] }}</p>
                        <p>Email: {{ $customer['ship_email'] }}</p>
                        <p>Địa chỉ: {{ $customer['ship_address'] }}</p>
                    </div>
                    <div class="col-12 col-md-12">
                        <p class="titlePayment clearfix">Cảm ơn bạn đã mua hàng của chúng tôi, Mã đơn hàng của bạn là #{{ $orderId }}</p>
                        <p class="">chúng tôi sẽ xác nhận và gửi đơn hàng trong thời gian ngắn nhất.</p>
                    </div>
                </div>
            </form>
        </div>
   
     </div>    
    
    
  </div>

</div>
<script>
    function changeQuantity(e) {
        var unitPrice = $(e).parent().parent().find('.unitPrice').val();
        var quantity = $(e).val();
        var totalPrice = unitPrice*quantity;
        var sum = 0;
        $(e).parent().parent().find('.totalPrice').empty();
        $(e).parent().parent().find('.totalPrice').html(numeral(totalPrice).format('0,0'));

        $('.totalPrice').each(function () {
            var totalPrice = $(this).html();
            console.log(totalPrice);
            sum += parseInt(numeral(totalPrice).value());
        });

        $('.sumPrice').empty();
        $('.sumPrice').html(numeral(sum).format('0,0'));
    }
</script>
@endsection
