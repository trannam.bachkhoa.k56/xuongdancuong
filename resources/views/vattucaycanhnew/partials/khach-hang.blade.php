<div class="parallax">
        <ul id="testimonial" class="row owl-carousel product-slider">
          @foreach(\App\Entity\SubPost::showSubPost('khach-hang-noi-ve-chung-toi', 8) as $id => $customer)
          <li class="item">
            <div class="panel-default">
              <div class="testimonial-desc"> {{ isset($customer['description']) ? $customer['description'] : '' }}</div>
              <div class="testimonial-image">
			  <div class="CropImgParent">
				<div class="CropImg CropImg60">
					<div class="thumbs">
						  <img src="{{ isset($customer['image']) ? $customer['image'] : '' }}" alt="#">
					</div>
				</div>
			  </div>
			  
			  
			
			  </div>
              <div class="testimonial-name"><h2> {{ isset($customer['title']) ? $customer['title'] : '' }}</h2></div>
              <div class="testimonial-designation"><p> {{ isset($customer['cong-viec']) ? $customer['cong-viec'] : '' }}</p></div>

            </div>
          </li>
          @endforeach

      
        </ul>
      </div>