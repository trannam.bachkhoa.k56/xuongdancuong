<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div id="facebook-inbox">
    <button class="facebook-inbox-tab" style="display: block; ">
<span class="facebook-inbox-tab-icon">
<img src="https://facebookinbox.bizwebapps.vn/Content/Images/fb-icon-1.png" alt="Facebook Chat" />
</span>
        <span class="facebook-inbox-tab-title">Chat với chúng tôi</span>
    </button>

    <div id="facebook-inbox-frame">
        <div id="fb-root">&nbsp;</div>
        <div class="fb-page" data-adapt-container-width="true" data-hide-cover="false" data-href="" data-show-facepile="true" data-small-header="true" data-width="250" data-height="350" data-tabs="messages">
            <div class="fb-xfbml-parse-ignore">
                <blockquote cite=""><a href="">Chat với chúng tôi</a></blockquote>
            </div>
        </div>
    </div>
</div>
<style>
#facebook-inbox {
position: fixed;
bottom: 0px;
z-index: 110000;
text-align: center;
}

.facebook-inbox-tab-icon {
float: left;
}

.facebook-inbox-tab-title {
float: left;
margin-left: 10px;
line-height: 25px;
}

#facebook-inbox-frame {
display: none;
width: 100%;
min-height: 200px;
overflow: hidden;
position: relative;
background-color: #f5f5f5;
}

#fb-root {
height: 0px;
}

.facebook-inbox-tab {
top: 0px;
bottom: 0px;
margin: -40px 0px 0px 0px;
position: relative;
height: 40px;
width: 250px;
border: 1px solid;
border-radius: 0px 0px 0px 0px;
text-align: center;
background-color: #19a3dd;
color: #ffffff;
padding: 8px 23px;
}
</style>

<script>
(function( $ ) {
if (true)
{
    $("#facebook-inbox").show();
fbInboxFillPage(
"{{ $information['link-facebook'] }}",
"/Content/Images/fb-icon-1.png",
"#3366CC",
"#797979",
"#FFFFFF",
"0",
"Chat với chúng tôi"
);
facebookShowPanelButton();
}

function facebookShowPanelButton() {
$(".facebook-inbox-tab").click(function () {
    var isHiden = $("#facebook-inbox").attr("hiden");
fbInboxHideBottom(isHiden);

});
}

function fbInboxHideBottom(isHiden){

if (isHiden !== undefined && isHiden !== "false") {
    $("#facebook-inbox").attr("hiden", false);
    $("#facebook-inbox-frame").hide();
} else {
      $("#facebook-inbox-frame").show();
      $("#facebook-inbox").attr("hiden", true);
  }
}

function fbInboxFillPage(facebookPage, imageSrc, bgColor, bdColor, textColor, location, title){
if (facebookPage.indexOf("facebook.com/") <= -1) {
    facebookPage =  "https://facebook.com/" + facebookPage;
}
$("#facebook-inbox-frame .fb-page").attr("data-href", facebookPage);
$("#facebook-inbox-frame .fb-page blockquote").attr("cite", facebookPage);
$("#facebook-inbox-frame .fb-page blockquote a").attr("href", facebookPage);
if(imageSrc != ""){
    $(".facebook-inbox-tab-icon").html("<img src='https://facebookinbox.bizwebapps.vn/" + imageSrc + "' />");
}
$(".facebook-inbox-tab").css({"background-color": bgColor, "border-color": bdColor, "color" : textColor});
$(".facebook-inbox-tab .facebook-inbox-tab-title").html(title);
$(".facebook-inbox-tab .facebook-inbox-tab-title").css('font-weight','bold');
if(location == "0"){
    $("#facebook-inbox").css("right","35px");
}
else{
    $("#facebook-inbox").css("left","35px");
}
}
})( jQuery );
</script>
