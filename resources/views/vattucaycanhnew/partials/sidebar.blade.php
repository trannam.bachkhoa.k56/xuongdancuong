<div id="column-left" class="col-sm-3 hidden-xs column-left">
      <div class="column-block">
        <div class="columnblock-title">Danh mục sản phẩm </div>
        <div class="category_block">
          <ul class="box-category treeview-list treeview">
             @foreach (\App\Entity\Menu::showWithLocation('side-left-menu') as $Mainmenu)
                  @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
            <li><a href="{{ $menuelement['url'] }}">{{ $menuelement['title_show']}}</a></li>
                @endforeach
            @endforeach 
          </ul>
        </div>
      </div>
	  
	  <!-- BO LOC -->
	  <div class="panel panel-default filter FitterHidden">
        <div class="panel-heading columnblock-title">TÌM KIẾM</div>
        <div class="filter-block">
          <div class="list-group"> 
		 
			@foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
            <a class="list-group-item">{{ $filterGroup->group_name }}</a>
            <div class="list-group-item">
              <div id="filter-group2">
				@foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                <label class="checkbox">
					<a  data-value="{{ $filter->name_filter }}" onClick="return checkFilter(this);"><i class="fa fa-angle-double-right" aria-hidden="true"></i> {{ $filter->name_filter }}</a>
                  <!--<input name="filter[]" type="checkbox" value="{{ $filter->name_filter }}"  />-->
                  
				</label>
				@endforeach  
              </div>
            </div>
			@endforeach
			<form action="" method="get" id="filterProduct">
			</form>
			<script>
				function checkFilter(e) {
					var valFilter = $(e).attr('data-value');
					$('#filterProduct').append('<input type="hidden" value="' + valFilter + '" name="filter[]">')
					$('#filterProduct').submit();

					return true;
				}
			</script>
			
          
          </div>
          <!--
		  <div class="panel-footer text-right">
            <button type="button" id="button-filter" class="btn btn-primary">Refine Search</button>
          </div>
		  -->
        </div>
      </div>
      <div class="banner" >
        <div class="item"> <a href="#"><img src="{{ isset($information['banner-trai']) ?  $information['banner-trai'] : asset('image/banners/LeftBanner.jpg') }}" alt="Left Banner" class="img-responsive" /></a> </div>
      </div>
	  
	  
	  	
	  
	  <!--
        <?php $cateTour = \App\Entity\Category::getDetailCategory('san-pham-ban-chay'); ?>
      <h3 class="productblock-title">{{ $cateTour['title'] }}</h3>

      <div class="row special-grid product-grid">

        @foreach (\App\Entity\Product::showProduct('san-pham-ban-chay', 8) as $product)
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
          <div class="product-thumb transition">
            <div class="image product-imageblock"> <a href="{{ route('product',['cate_slug' => $product->slug]) }}"><img src="{{ isset($product['image']) ? $product['image'] : '' }}" alt="{{ isset($product['title']) ? $product['title'] : ''}}" title="{{ isset($product['title']) ? $product['title'] : ''}}" class="img-responsive" /></a>
              <div class="button-group">
                
               <form onsubmit="return addToOrder(this);" method="post" accept-charset="utf-8" id="" enctype="multipart/form-data">
               {{ csrf_field() }}
              <input type="hidden" class="input_quantity" id="input_quantity" name="quantity[]" value="1"/>
              <input type="hidden" class="input_quantity" id="input_quantity" name="product_id[]" value="{{ $product->product_id }}">
              <button type="submit" class="add tocart-btn" >Thêm vào giỏ hàng</button>
               </form>
               
              </div>
            </div>

            <div class="caption product-detail" style="width: 100%">
              <h4 class="product-name"> <a href="{{ route('product',['cate_slug' => $product->slug]) }}" title="{{ isset($product['title']) ? $product['title'] : ''}}">{{ isset($product['title']) ? $product['title'] : ''}}</a> </h4>
              @if($product['discount'] > 0)
                  <p class="price product-price" style="font-size: 14px">{{ number_format( $product['discount'] , 0) }} vnđ<span class="" style="text-align: right;
                display: inline-block;
                float: right;
                padding-left: 10px;" ><del style="font-size: 13px">{{ number_format( $product['price'] , 0) }}</del> vnđ</span></p>
                @else
                 <p class="price product-price" style="font-size: 14px"> <span class="price-new">{{ number_format( $product['price'] , 0) }} vnđ</span></p>
                @endif
            </div>
            <div class="button-group">
              <form onsubmit="return addToOrder(this);" method="post" accept-charset="utf-8" id="" enctype="multipart/form-data">
               {{ csrf_field() }}
              <input type="hidden" class="input_quantity" id="input_quantity" name="quantity[]" value="1"/>
              <input type="hidden" class="input_quantity" id="input_quantity" name="product_id[]" value="{{ $product->product_id }}">
              <button type="submit" class="add tocart-btn" >Thêm vào giỏ hàng</button>
               </form>
            </div>
          </div>
        </div>
        @endforeach
      </div>-->
	  
	  
	  
	    <?php $cateTour = \App\Entity\Category::getDetailCategory('san-pham-ban-chay'); ?>
      <h3 class="productblock-title">{{ $cateTour['title'] }}</h3>
	  <div class="row special-grid product-grid">
		
		<div class="row bestseller-grid product-grid"  style="
    padding: 0;
    border: none;
">
			@foreach (\App\Entity\Product::showProduct('san-pham-ban-chay', 8) as $product)
			  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
				<div class="product-thumb transition">
				  <div class="image product-imageblock"> <a href="{{ route('product',['cate_slug' => $product->slug]) }}"> <img src="{{ isset($product['image']) ? $product['image'] : '' }}" alt="{{ isset($product['title']) ? $product['title'] : ''}}" alt="{{ isset($product['title']) ? $product['title'] : ''}}" title="{{ isset($product['title']) ? $product['title'] : ''}}" class="img-responsive" width="50" height="59"> </a>
					<div class="button-group">
					  
					   <form onsubmit="return addToOrder(this);" method="post" accept-charset="utf-8" id="" enctype="multipart/form-data">
               {{ csrf_field() }}
              <input type="hidden" class="input_quantity" id="input_quantity" name="quantity[]" value="1"/>
              <input type="hidden" class="input_quantity" id="input_quantity" name="product_id[]" value="{{ $product->product_id }}">
              <button type="submit" class="add tocart-btn" >Thêm vào giỏ hàng</button>
               </form>
					
					</div>
				  </div>
				  <div class="caption product-detail">
					<h4 class="product-name"> <a href="{{ route('product',['cate_slug' => $product->slug]) }}" title="{{ isset($product['title']) ? $product['title'] : ''}}">{{ isset($product['title']) ? $product['title'] : ''}}</a> </h4>
					 @if($product['discount'] > 0)
                  <p class="price product-price" style="font-size: 14px">{{ number_format( $product['discount'] , 0) }}đ<span class="" style="text-align: right;
                display: inline-block;
                float: right;
                padding-left: 10px;" ><del style="font-size: 13px">{{ number_format( $product['price'] , 0) }}</del>đ</span></p>
                @else
                 <p class="price product-price" style="font-size: 14px"> <span class="price-new">{{ number_format( $product['price'] , 0) }}đ</span></p>
                @endif
				
				  </div>
				  <div class="button-group">
					
					 <form onsubmit="return addToOrder(this);" method="post" accept-charset="utf-8" id="" enctype="multipart/form-data">
               {{ csrf_field() }}
              <input type="hidden" class="input_quantity" id="input_quantity" name="quantity[]" value="1"/>
              <input type="hidden" class="input_quantity" id="input_quantity" name="product_id[]" value="{{ $product->product_id }}">
              <button type="submit" class="add tocart-btn" >Thêm vào giỏ hàng</button>
               </form>
				
				  </div>
				</div>
			  </div>
		  @endforeach
        </div>
		
		
       
      </div>
	  
	  
	  

</div>