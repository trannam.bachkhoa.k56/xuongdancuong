<section class="slide mgtop20">
	<div class="container">
		<div class="row">
			<div class="col-md-3 NavHome">
				<div class="column-block">
				<div class="columnblock-title">Danh mục sản phẩm </div>
				<div class="category_block">
				  <ul class="box-category treeview-list treeview">
					 @foreach (\App\Entity\Menu::showWithLocation('side-left-menu') as $Mainmenu)
						  @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
					<li><a href="{{ $menuelement['url'] }}">{{ $menuelement['title_show']}}</a></li>
						@endforeach
					@endforeach 
				  </ul>
				</div>
			  </div>
			</div>
			<div class="col-md-9">
				<div class="mainbanner">
				  <div id="main-banner" class="owl-carousel home-slider">
				   
					@foreach( \App\Entity\SubPost::showSubPost('slider',8) as $id => $slide )
					<div class="item"> <a href="#"><img src="{{ isset($slide['image']) ? $slide['image'] : '' }}" alt="{{ isset($slide['title']) ? $slide['title'] : '' }}" class="img-responsive" /></a> </div>
					@endforeach
				   
				  </div>
				</div>
			</div>
		</div>
	</div>
</section>