<!-- <div class="preloader loader" style="display: block; background:#f2f2f2;"> <img src="vattucaycanhnew/image/loader.gif"  alt="#"/></div> -->
<header>
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            
         <div class="top-left pull-left pd-10">
           <ul class="list-inline mgbottom0">
                  <li><span style="    display: inline-block;
    padding-top: 4px;"><i class="fa fa-mobile" aria-hidden="true"></i> Hotline:{{ isset($information['dia-chi']) ?  $information['so-dien-thoai'] : '' }}</span></li>
                <li class="pdleft15"><span style="    display: inline-block;
    padding-top: 4px;"><i class="fa fa-map-marker"></i> Địa chỉ : {{ isset($information['dia-chi']) ?  $information['dia-chi'] : '' }}</span></li>
              </ul>
            
          </div>

          <div class="top-right  mdds-none mbds-block">
              <div class="formMenu">
                 <form action="{{ route('search_product') }}" method="get" id="headearch" class="input-group search-bar search_form">
                <input class="input-text" placeholder="Tìm kiếm .." type="search"  name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}">
                <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
              </div>
              
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="header-inner">
      <div class="col-md-3 col-sm-12 col-xs-6 header-left">
        <div class="shipping text-ct">
          <!-- <div class="shipping-img"></div>
          <div class="shipping-text">{{ isset($information['so-dien-thoai']) ?  $information['so-dien-thoai'] : '' }}
          <br>
            <span class="shipping-detail">{{ isset($information['thoi-gian-mo-cua']) ?  $information['thoi-gian-mo-cua'] : '' }}</span></div> -->
            <div id="logo"> <a href="/">
              <img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" title="{{ isset($information['ten-website']) ?  $information['ten-website'] : '' }}" alt="{{ isset($information['ten-website']) ?  $information['ten-website'] : '' }}" class="img-responsive" />
             <!--  <img src="vattucaycanhnew/image/logo.png"> -->
            </a> </div>
            

        </div>
      </div>
      <div class="col-md-7 col-sm-12 col-xs-12 header-middle">
        <div class="header-middle-top">
          <div class="row">
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
                <img src="{{ asset('vattucaycanhnew/image/1.png')}}">
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                  GIAO HÀNG NHANH CHÓNG
                </h3>
                <p class="mgbottom0">
                 {{ isset($information['giao-hang-nhanh-chong']) ?  $information['giao-hang-nhanh-chong'] : '' }}
                </p>
              </div>
            </div>
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
			    <img src="{{ asset('vattucaycanhnew/image/2.png')}}">
              
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                 HỖ TRỢ 24/7
                </h3>
                <p class="mgbottom0">
                {{ isset($information['ho-tro-24-7']) ?  $information['ho-tro-24-7'] : '' }}
                </p>
              </div>
            </div>
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
			    <img src="{{ asset('vattucaycanhnew/image/3.png')}}">
             
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                 GIỜ LÀM VIỆC
                </h3>
                <p class="mgbottom0">
                {{ isset($information['gio-lam-viec']) ?  $information['gio-lam-viec'] : '' }}
                </p>
              </div>
            </div>
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
			    <img src="{{ asset('vattucaycanhnew/image/4.jpg')}}">
               
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                  UY TÍN, CHẤT LƯỢNG
                </h3>
                <p class="mgbottom0">
                  {{ isset($information['uy-tin-chat-luong']) ?  $information['uy-tin-chat-luong'] : '' }}
                </p>
              </div>
            </div>

          </div>
          
        </div>
      </div>
      <div class="col-md-2 col-sm-12 col-xs-12 header-right">
        <div class="formMenu mdds-none msds-block mbds-none">
                 <form action="{{ route('search_product') }}" method="get" id="headearch" class="input-group search-bar search_form">
                <input class="input-text" placeholder="Tìm kiếm .." type="search"  name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}">
                <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
        </div>
         <?php $countOrder = \App\Entity\Order::countOrder();?>
        <div id="cart" class="btn-group btn-block ">
          <button type="button" class="btn btn-inverse btn-block btn-lg dropdown-toggle cart-dropdown-button cartRight">
            <p class="mgbottom0 msds-none"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> 
              <span>Số sản phẩm : {{ $countOrder }}</span>
            </p>
            <p class="mgbottom0 text-ct"><span class="bold">Xem giỏ hàng</span></p>
         
           <!-- <span id="cart-total"><span class="cart-title">Giỏ hàng</span><br>
          {{ $countOrder }} (sản phẩm)</span>  -->
        </button>

         <ul class="dropdown-menu pull-right cart-dropdown-menu">
            <li>
              <table class="table table-striped">
                <tbody>
               
                  @if (!empty(\App\Entity\Order::getOrderItems()))
                        @foreach(\App\Entity\Order::getOrderItems() as $orderItem)
                  <tr>
                    <td class="text-center"><a href="{{ route('post', ['cate_slug' => 'san-pham', 'post_slug' => $orderItem->slug]) }}"><img class="img-thumbnail" title="{{ $orderItem->title }}" alt="{{ $orderItem->title }}" src="{{ !empty($orderItem->image) ?  asset($orderItem->image) : asset('/site/img/no-image.png') }}" style="width:60px" ></a></td>
                    <td class="text-left"><a href="{{ route('post', ['cate_slug' => 'san-pham', 'post_slug' => $orderItem->slug]) }}">{{ $orderItem->title }}</a></td>
                    <td class="text-right">  {{ $orderItem->quantity }} x </td>
                    <td class="text-right">
                      @if(($orderItem->discount) > 0)
                        <del>{{ number_format($orderItem->price) }}</del> 
                        <span>{{ number_format($orderItem->discount) }}</span> 
                      @else
                      <span>{{ number_format($orderItem->price) }}</span> 
                      @endif
                    </td>
                   
                  </tr>
                  <tr>
                    <td colspan="4"  class="text-right">  
                        <?php  $sumPrice = 0 ?>
                      @if(($orderItem->discount) > 0)
                        <?php  $sumPrice = $orderItem->discount *  $orderItem->quantity   ?>
                      @else
                        <?php  $sumPrice = $orderItem->price *  $orderItem->quantity   ?>
                      @endif
                    Tổng tiền {{ number_format($sumPrice)}} vnđ
                  </td>
                  </tr>
                    @endforeach
                  @endif
                </tbody>
              </table>
            </li>
            <li>
              <div>
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td class="text-right"><strong>Tổng số sản phẩm</strong></td>
                      <td class="text-right">{{ $countOrder }} (sản phẩm)</td>
                    </tr>
                    <tr>
                        <div style="display: none;">
                       @if (!empty(\App\Entity\Order::getOrderItems()))
                       <?php $sumPrices = 0;?>
                        @foreach(\App\Entity\Order::getOrderItems() as $orderItem)
                           
                         <?php 

                         $sumPrices += !empty($orderItem->discount) ? ($orderItem->discount*$orderItem->quantity) : ($orderItem->price*$orderItem->quantity) ?>
                                                {{ !empty($orderItem->discount) ? number_format(($orderItem->discount*$orderItem->quantity) , 0) : number_format(($orderItem->price*$orderItem->quantity) , 0, ',', '.') }}
                         @endforeach
                       @endif
                       </div>
                      <td class="text-right"><strong>Tổng tiền thanh toán</strong></td>
                      <td class="text-right">{{ isset($sumPrices) ? number_format($sumPrices) : '' }} vnđ</td>
                    </tr>
                    

                   
                   
                  </tbody>
                </table>
                <p class="text-right"> <span class="btn-viewcart"><a href="/gio-hang"><strong><i class="fa fa-shopping-cart"></i> Xem giỏ hàng</strong></a></span>  </p>
              </div>
            </li>
          </ul>
         
        </div>
      </div>
    </div>
  </div>
</header>
<nav id="menu" class="navbar">
  <div class="nav-inner container">
    <div class="navbar-header"><span id="category" class="visible-xs">Danh mục</span>
      <button type="button" class="btn btn-navbar navbar-toggle" ><i class="fa fa-bars"></i></button>
    </div>
    <div class="navbar-collapse">
      <!--<ul class="main-navigation menuHeader">
        @foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
          <li><a href="{{ route('site_category_product', ['cate_slug' => $cateProduct->slug]) }}" class="
            @if (!empty(\App\Entity\Category::getChildrenCategory($cateProduct->product_id, 'product'))
                active
            @endif
            parent">{{ $cateProduct->title  }}</a>
            @if (!empty(\App\Entity\Category::getChildrenCategory($cateProduct->product_id, 'product'))
            <ul>
              @foreach (\App\Entity\Category::getChildrenCategory($cateProduct->product_id, 'product') as $cateProductChild)
              <li><a href="{{ route('site_category_product', ['cate_slug' => $cateProductChild->slug]) }}">{{ $cateProductChild->title  }}</a></li>
              @endforeach
            </ul>
            @endif
          </li>
        @endforeach -->

        <div class="formMenu msds-none">
                 <form action="{{ route('search_product') }}" method="get" id="headearch" class="input-group search-bar search_form">
                <input class="input-text" placeholder="Tìm kiếm .." type="search"  name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}">
                <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
        </div>
      </ul>
    </div>
  </div>
</nav>