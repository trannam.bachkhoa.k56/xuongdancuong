
@extends('diamond.layout.site')

@section('title', "liên hệ")
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
@include('diamond.partials.slider')
	<section class="detailnew contact">
      <div class="container">
        <div class="row bgdetail">
          <div class="col-12 colcontact">
            <h1>
              Liên hệ
            </h1>	
			<div class="mapsGoogle">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.3676601307925!2d105.85444901535898!3d21.01797009352152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abf2736bf33d%3A0x5d492690c0dacd66!2zMSBOZ8O1IEjDoG5nIENodeG7kWkgMSwgUGjhuqFtIMSQw6xuaCBI4buTLCBIYWkgQsOgIFRyxrBuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1526875468540" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
              
            <div class="content row">
				<div class="col-md-6">
					<h2 class="titl">THÔNG TIN CỬA HÀNG</h2>
					<div class="descript"><p>Cửa Hàng trang sức đá quý Quỳnh Mai</p>
					<p>Địa chỉ: Số 1 Hàng Chuối, Bờ Hồ, Hà Nội</p>
					<p>Điện thoại: 093 455 3435 - 097 456 1735</p>
					<p>Email: info@quynhmaikimcuong.com</p>
					<p>Website: http://quynhmaikimcuong.com</p></div>
				</div>
				<div class="col-md-6">
					<h2 class="titl">GỬI LIÊN HỆ CHO CHÚNG TÔI</h2>
					<p>Chúng tôi sẽ phản hồi ngay khi nhận được liên hệ của bạn</p>
					<form action="{{ route('sub_contact') }}" method="post">
					  {!! csrf_field() !!}
					<div class="form-group">
					  <label for="exampleInputEmail1">Tên khách hàng <span>*</span></label>
					  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tên khách hàng" name="name" >
					 
					</div>
					<div class="form-group">
					  <label for="exampleInputPassword1">Điện thoại <span>*</span></label>
					  <input type="number" class="form-control" id="exampleInputPassword1" placeholder="VD : 016399685887" name="phone">
					</div>
					 <div class="form-group">
					  <label for="exampleInputPassword1">Email <span>*</span></label>
					  <input type="email" class="form-control" id="exampleInputPassword1" placeholder="VD : diamond@gmail.com" name="email">
					</div>
					 <div class="form-group">
					  <label for="exampleInputPassword1">Địa chỉ <span>*</span></label>
					  <input type="email" class="form-control" id="exampleInputPassword1" placeholder="" name="address">
					</div>

					 <div class="form-group">
					  <label for="exampleInputPassword1">Mô tả <span>*</span></label>
					   <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" name="message" ></textarea>
					</div>
					
					<button type="submit" class="btn btn-primary">Gửi liên hệ</button>
				  </form>
				  <p style="color: red; font-size: 18px; padding: 25px 0 40px 0;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
				</div>
              
            </div>

          </div>
        </div>
      </div>
    </section>
@endsection
