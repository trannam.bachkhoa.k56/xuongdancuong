

@extends('diamond.layout.site')

@section('title',isset($post->title) ? $post->title : '')
@section('meta_description', isset($information['meta_description']) ?$information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
@include('diamond.partials.slider')
    </div>
    <section class="detailnew contact">
      <div class="container">
        <div class="row bgdetail">
          <div class="col-12 colcontact">
            <h1>
              Đăng kí
            </h1>
            <div class="content">
              <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">Họ và tên <span>*</span></label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Họ và tên">
                 
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Email <span>*</span></label>
                  <input type="email" class="form-control" id="exampleInputPassword1" placeholder="VD : diamond@gmail.com">
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Mật khẩu <span>*</span></label>
                    <input type="password" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nhập lại mật khẩu <span>*</span></label>
                    <input type="password" class="form-control" id="exampleInputPassword1">
                </div>
                
                <button type="submit" class="btn btn-primary">Đăng kí</button>
              </form>
            </div>

          </div>
        </div>
      </div>
    </section>

@endsection
