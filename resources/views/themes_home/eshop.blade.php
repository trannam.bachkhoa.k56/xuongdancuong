<!DOCTYPE html>
<html lang="zxx">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
    <title>{!! isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' !!}</title>
    <meta name="title" 						content="{!! isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' !!}" />
    <meta name="description" 				content="{!! isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : ''  !!}" />
    <meta name="keywords" 					content="{!! isset($information['tu-khoa']) ? $information['tu-khoa'] : '' !!}" />

    <meta name="theme-color"				content="#07a0d2" />
    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="{!! isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' !!}" />
    <meta property="og:description"        content="{!! isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : ''  !!}" />
    <meta property="og:image"              content="@yield('meta_image')" />

	<!-- Favicon -->
	<link rel="icon" type="image/png" href="{{ !empty($information['icon']) ?  asset($information['icon']) : '' }}">
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	
	<!-- StyleSheet -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="/themes/eshop/css/bootstrap.css">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="/themes/eshop/css/magnific-popup.min.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="/themes/eshop/css/font-awesome.css">
	<!-- Fancybox -->
	<link rel="stylesheet" href="/themes/eshop/css/jquery.fancybox.min.css">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="/themes/eshop/css/themify-icons.css">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="/themes/eshop/css/niceselect.css">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="/themes/eshop/css/animate.css">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="/themes/eshop/css/flex-slider.min.css">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="/themes/eshop/css/owl-carousel.css">
	<!-- Slicknav -->
    <link rel="stylesheet" href="/themes/eshop/css/slicknav.min.css">
	
	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="/themes/eshop/css/reset.css">
	<link rel="stylesheet" href="/themes/eshop/css/style.css">
    <link rel="stylesheet" href="/themes/eshop/css/responsive.css">

    <!-- Jquery -->
    <script src="/themes/eshop/js/jquery.min.js"></script>
    <script src="/themes/eshop/js/jquery-migrate-3.0.0.js"></script>
	<script src="/themes/eshop/js/jquery-ui.min.js"></script>
	
    {!! isset($information['google-anylic']) ? $information['google-anylic'] : '' !!}
	
	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}

</head>
<body class="js">
	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
	
	
	<!-- Header -->
	<header class="header shop">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-12 col-12">
						<!-- Top Left -->
						<div class="top-left">
							<ul class="list-main">
								<li><i class="ti-headphone-alt"></i> {{ isset($information['so-dien-thoai']) && !($information['so-dien-thoai'] == '097.4xxx.xxx') ? $information['so-dien-thoai']  : $phoneUser }}</li>
								<li><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ isset($information['dia-chi-dau-trang']) ?  $information['dia-chi-dau-trang'] : '' }}</li>
							</ul>
						</div>
						<!--/ End Top Left -->
					</div>
                    <div class="col-lg-3 col-md-12 col-12">
                        <div class="top-right">
                            @if (\Illuminate\Support\Facades\Auth::check())
								<a href="/admin" class="admin" target="_blank">Quản trị website</a>
							@endif
                        </div>
                    </div>
				</div>
			</div>
		</div>
		<!-- End Topbar -->
		<div class="middle-inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-12">
						<!-- Logo -->
						<div class="logo">
							<a href="/"><img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" style="width: 50px;" alt="logo"></a>
						</div>
						<!--/ End Logo -->
						<!-- Search Form -->
						<div class="search-top">
							<div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
							<!-- Search Form -->
							<div class="search-top">
								<form class="search-form" method="get" action="/tim-kiem"> 
									<input type="text" value="{{ isset($word) ? $word : '' }}" placeholder="Tìm kiếm sản phẩm..." name="word">
									<button value="search" type="submit"><i class="ti-search"></i></button>
								</form>
							</div>
							<!--/ End Search Form -->
						</div>
						<!--/ End Search Form -->
						<div class="mobile-nav"></div>
					</div>
					<div class="col-lg-9 col-md-7 col-12">
						<div class="search-bar-top">
							<div class="search-bar">
								<form method="get" action="/tim-kiem">
                                <input type="text" value="{{ isset($word) ? $word : '' }}" placeholder="Tìm kiếm sản phẩm..." name="word">
									<button class="btnn"><i class="ti-search"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Header Inner -->
		<div class="header-inner">
			<div class="container">
				<div class="cat-nav-head">
					<div class="row">
						<div class="col-lg-3">
							<div class="all-category">
								<h3 class="cat-heading"><i class="fa fa-bars" aria-hidden="true"></i>SẢN PHẨM</h3>

                                <ul class="main-category">
                                @foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
		                            @if ($cateProduct->slug != 'san-pham-trang-chu')
                                    <li><a href="/cua-hang/{{ $cateProduct->slug }}">{{ $cateProduct->title  }}  
                                        @if (!empty(\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product')))
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                        @endif
                                    </a>
									@if (!empty(\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product')))
										<ul class="sub-category">
										
                                            @foreach (\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product') as $childProduct)
											<li><a href="/cua-hang/{{ $childProduct->slug }}">{{ $childProduct->title  }}</a></li>
											@endforeach
										
										</ul>
									@endif
									</li>
									@endif
                                @endforeach
								</ul>
							</div>
						</div>
						<div class="col-lg-9 col-12">
							<div class="menu-area">
								<!-- Main Menu -->
								<nav class="navbar navbar-expand-lg">
									<div class="navbar-collapse">	
										<div class="nav-inner">	
											<ul class="nav main-menu menu navbar-nav">
                                                <li class="active"><a href="/">Trang chủ</a></li>
                                                @if (!empty(\App\Entity\Category::getChildrenCategory(0, 'post')))
                                                    @foreach (\App\Entity\Category::getChildrenCategory(0, 'post') as $childProduct)
                                                    <li><a href="/danh-muc/{{ $childProduct->slug }}">{{ $childProduct->title  }}</a></li>		
                                                    @endforeach			
                                                @endif
                                                <li><a href="/danh-muc/tin-tuc">Tin tức</a></li>				
                                                <li><a href="/lien-he">Liên hệ</a></li>
                                            </ul>
										</div>
									</div>
								</nav>
								<!--/ End Main Menu -->	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Header Inner -->
	</header>
	<!--/ End Header -->
	
	<!-- Slider Area -->
	<section class="hero-slider">
        @foreach(\App\Entity\SubPost::showSubPost('slider', 1) as $id => $slide)
		<!-- Single Slider -->
		<div class="single-slider">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-lg-9 offset-lg-3 col-12">
						<div class="text-inner">
							<div class="row">
								<div class="col-lg-7 col-12">
									<div class="hero-text">
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        <style>
            .hero-slider .single-slider {
                background-image: url('{{ isset($slide['image']) ?  $slide['image'] : '' }}');
            }
        </style>
        @endforeach
		<!--/ End Single Slider -->
	</section>
	<!--/ End Slider Area -->
	
	<!-- Start Product Area -->
    <div class="product-area section">
            <div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section-title">
							<h2><?= isset($information['tieu-de-san-pham-hoac-dich-vu']) ? $information['tieu-de-san-pham-hoac-dich-vu'] : 'Top sản phẩm hoặc dịch vụ' ?></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="product-info">
                            @php 
                                if (!empty(\App\Entity\Category::getDetailCategory('san-pham-trang-chu'))) {
                                    $productIndexs = \App\Entity\Product::showProduct('san-pham-trang-chu', 20);
                                } else {
                                    $productIndexs = \App\Entity\Product::newProduct(20);
                                }
                            
                            @endphp
							<div class="tab-content" id="myTabContent">
								<!-- Start Single Tab -->
								<div class="tab-pane fade show active" id="man" role="tabpanel">
									<div class="tab-single">
										<div class="row">
                                            @foreach($productIndexs as $hotProduct)
											<div class="col-xl-3 col-lg-4 col-md-4 col-12">
												<div class="single-product">
													<div class="product-img">
														<a href="/{{ $hotProduct->slug }}">
															<img class="default-img" src="{{ isset($hotProduct['image']) ?  $hotProduct['image'] : '' }}" alt="#">
															<img class="hover-img" src="{{ isset($hotProduct['image']) ?  $hotProduct['image'] : '' }}" alt="#">
														</a>
														<div class="button-head">
															<div class="product-action">
																<a title="Quick View" href="/{{ $hotProduct->slug }}"><i class=" ti-eye"></i><span>Xem nhanh</span></a>
																<a title="Wishlist" href="#"><i class=" ti-heart "></i><span>097.456.1735</span></a>
																
															</div>
															<div class="product-action-2">
																<a title="Add to cart" href="/{{ $hotProduct->slug }}">Xem chi tiết</a>
															</div>
														</div>
													</div>
													<div class="product-content">
														<h3><a href="product-details.html">{{ isset($hotProduct['title']) ?  $hotProduct['title'] : '' }}</a></h3>
														<div class="product-price">
                                                        @if(empty($hotProduct->price))
                                                            <span>liên hệ</span>
                                                        @elseif ($hotProduct->price == 1)
                                                            <span>Miễn phí</span>
                                                        @elseif($hotProduct->discount > 0)
                                                            <span><del>{{ number_format($hotProduct->price) }} đ</del></span> - <span>{{ number_format($hotProduct->discount) }} đ</span>
                                                        @else
                                                            <span>{{ number_format($hotProduct->price) }} đ</span>
                                                        @endif
														</div>
													</div>
												</div>
											</div>
                                            @endforeach
										</div>
									</div>
								</div>
								<!--/ End Single Tab -->
							</div>
						</div>
					</div>
				</div>
            </div>
    </div>
	<!-- End Product Area -->
	
	<!-- Start Cowndown Area -->
	<section class="">
		<div class="section-inner ">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section-title">
							<h2>{{ isset($information['tieu-de-trang']) ? $information['tieu-de-trang']  : '' }}</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-12 padding-right">
						<div class="content">
							<div class="heading-block">
                                <p><?= isset($information['sologan']) ? $information['sologan'] : '' ?></p>
							</div>
						</div>
					</div>	
					<div class="col-lg-6 col-12 padding-left">
						<div class="content">
							<div class="form-main">
                                <form onSubmit="return contact(this);" class="wpcf7-form" method="post"
                                    action="/submit/contact" style="padding: 20px">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="is_json"
                                        class="form-control captcha" value="1" placeholder="">

                                    <div class="form-group">
                                        <div>
                                            <input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div>
                                            <input type="email" class="form-control" placeholder="Email của bạn" name="email">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div>
                                            <input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div>
                                        <textarea class="form-control" rows="2" name="message"
                                                placeholder="Nội dung ghi chú"></textarea>
                                        </div>
                                    </div>
                                    @if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
                                        <input type="hidden" value="{{ $_SERVER['HTTP_REFERER'] }}" name="utm_source" />
                                    @endif

                                    @if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
                                    <div class="form-group">
                                        <div class="center">
                                            <button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
                                        </div>
                                    </div>
                                    @endif
                                </form>
							</div>
						</div>
							
					</div>	
				</div>
			</div>
		</div>
	</section>
	<!-- /End Cowndown Area -->
	
	<!-- Start Shop Blog  -->
	<section class="shop-blog section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2><?= isset($information['tieu-de-cho-phan-hoi-khach-hang']) ? $information['tieu-de-cho-phan-hoi-khach-hang'] : 'Phản hồi khách hàng' ?></h2>
					</div>
				</div>
			</div>
			<div class="row">
                @foreach(\App\Entity\SubPost::showSubPost('khach-hang', 8) as $id => $customer)
				<div class="col-lg-3 col-md-6 col-12">
					<!-- Start Single Blog  -->
					<div class="shop-single-blog">
                        @if (isset($customer['ma-chia-se-youtube']) && !empty($customer['ma-chia-se-youtube']))
							<div id="embbedYoutube{{$id}}">
						
							</div>
							<script>
								$(window).load(function() { 
									$("#embbedYoutube{{$id}}").html('{!! $customer['ma-chia-se-youtube'] !!}');
								});
							 </script>
						 @elseif (isset($customer['image']) && !empty($customer['image']))
							<a title="{{ isset($customer['title']) ?  $customer['title'] : '' }}">
							   <img src="{{ isset($customer['image']) ? $customer['image'] : '' }}" alt="" class="mg">
							</a>
						 @endif
						<div class="content">
							<p class="date">{{ isset($customer['title']) ?  $customer['title'] : '' }}</p>
						</div>
					</div>
					<!-- End Single Blog  -->
				</div>
                @endforeach
			</div>
		</div>
	</section>
	<!-- End Shop Blog  -->
	
	<!-- Start Footer Area -->
	<footer class="footer">
		<!-- Footer Top -->
		<div class="footer-top section">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Fanpage</h4>
							<div class="contentFooter" id="loadFanpageFacebook"> 
                            </div>
                            <script>
                                $(window).load(function() { 
                                        $("#loadFanpageFacebook").html('<iframe src="https://www.facebook.com/plugins/page.php?href=<?= isset($information['nhung-fb']) ? $information['nhung-fb']  : '' ?>&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media">
                                </iframe>');
                                    });
                            </script>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
                            {!! isset($information['thong-tin-lien-he']) ?  $information['thong-tin-lien-he'] : '' !!}
					 
                            <ul class="thongke" style="list-style-type:none;padding-inline-start: 00px;">
                                <li class="pd5-0"><i class="fas fa-user-check"></i> Đang Online: <span>145</span></li>
                                <li class="pd5-0"><i class="fas fa-users"></i> Hôm nay: <span>2.056</span></li>
                                <li class="pd5-0"><i class="fas fa-user-clock"></i> Hôm qua: <span>49.105</span></li>
                                <li class="pd5-0"><i class="fas fa-signal"></i> Tuần này: <span>3.452.665</span></li>
                                <li class="pd5-0"><i class="fas fa-temperature-high"></i> Tổng Truy Cập: <span>151.536.351</span></li>
                            </ul>
						</div>
						<!-- End Single Widget -->
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer links">
							<h4>Bài viết mới</h4>
							<ul>
                                @foreach(\App\Entity\Post::categoryShow('tin-tuc',5) as $post)
								<li><a href="/tin-tuc/{{ $post->slug }}">{{ isset($post['title']) ?  $post['title'] : '' }}</a></li>
                                @endforeach
							</ul>
						</div>
						<!-- End Single Widget -->
					</div>
					
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row">
						<div class="col-lg-12 col-12">
							<div class="left">
                            <?php
                                $domain = App\Ultility\Ultility::getCurrentHttpHost();
                                $userId = App\Entity\Domain::getUserIdWithUrlLike($domain);
                                $user = App\Entity\User::getUserByUserId($userId['user_id']);
                            ?>
                            @if(!empty($user) && $user->vip < 1)
                                <div class="copyright">
                                    <span><a href="https://moma.vn">Tạo website marketing miễn phí trọn đời tại moma.vn</a></span>
                                </div>
                            @else
                                <div class="copyright">
                                    <span>{{ isset($information['copyright']) ?  $information['copyright'] : '' }}</span>
                                </div>
                            @endif
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /End Footer Area -->
    @include ('general.contact', [
		  'phone' => isset($information['so-dien-thoai']) ? $information['so-dien-thoai']  : '',
		  'zalo' => isset($information['chat-zalo']) ? $information['chat-zalo'] : '',
		  'fanpage' => isset($information['ten-tom-tat-fanpage-facebook']) ? $information['ten-tom-tat-fanpage-facebook'] : ''
      ])

	<!-- Popper JS -->
	<script src="/themes/eshop/js/popper.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="/themes/eshop/js/bootstrap.min.js"></script>
	<!-- Slicknav JS -->
	<script src="/themes/eshop/js/slicknav.min.js"></script>
	<!-- Owl Carousel JS -->
	<script src="/themes/eshop/js/owl-carousel.js"></script>
	<!-- Magnific Popup JS -->
	<script src="/themes/eshop/js/magnific-popup.js"></script>
	<!-- Waypoints JS -->
	<script src="/themes/eshop/js/waypoints.min.js"></script>
	<!-- Countdown JS -->
	<script src="/themes/eshop/js/finalcountdown.min.js"></script>
	<!-- Nice Select JS -->
	<script src="/themes/eshop/js/nicesellect.js"></script>
	<!-- Flex Slider JS -->
	<script src="/themes/eshop/js/flex-slider.js"></script>
	<!-- ScrollUp JS -->
	<script src="/themes/eshop/js/scrollup.js"></script>
	<!-- Onepage Nav JS -->
	<script src="/themes/eshop/js/onepage-nav.min.js"></script>
	<!-- Easing JS -->
	<script src="/themes/eshop/js/easing.js"></script>
	<!-- Active JS -->
	<script src="/themes/eshop/js/active.js"></script>
</body>
</html>