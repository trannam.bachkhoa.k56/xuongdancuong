<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='copyright' content=''>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Title Tag  -->
    <title>{!! isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' !!}</title>
    <meta name="title" content="{!! isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' !!}"/>
    <meta name="description"
          content="{!! isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : ''  !!}"/>
    <meta name="keywords" content="{!! isset($information['tu-khoa']) ? $information['tu-khoa'] : '' !!}"/>

    <meta name="theme-color" content="#07a0d2"/>
    <meta property="og:url" content="@yield('meta_url')"/>
    <meta property="og:type" content="Website"/>
    <meta property="og:title" content="{!! isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' !!}"/>
    <meta property="og:description"
          content="{!! isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : ''  !!}"/>
    <meta property="og:image" content="@yield('meta_image')"/>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ !empty($information['icon']) ?  asset($information['icon']) : '' }}">

    <!-- Bootstrap -->
    <link href="/themes/quick/css/bootstrap.css" rel="stylesheet">
    <link href="/themes/quick/css/custom.css" rel="stylesheet">
    <link href="/themes/quick/css/carousel.css" rel="stylesheet">
    <link href="/themes/quick/css/carousel-recommendation.css" rel="stylesheet">
    <link href="/themes/quick/ionicons-2.0.1/css/ionicons.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Catamaran:400,100,300' rel='stylesheet' type='text/css'>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/themes/quick/js/jquery-latest.min.js"></script>
    <script  src="/public/xhome1/js/jquery.matchHeight-min.js"></script>

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Danh mục</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"> <img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" style="width: 50px;" alt="logo"></a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Trang chủ</a></li>
                @foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
                    @if ($cateProduct->slug != 'san-pham-trang-chu')
                <li
                        @php $checkChildren = !empty(\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product')) @endphp
                        @if ($checkChildren)
                        class="dropdown"
                        @endif
                >

                    <a href="/cua-hang/{{ $cateProduct->slug }}"
                       @if ($checkChildren)
                       class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
                        @endif
                    >
                        {{ $cateProduct->title  }}
                        @if ($checkChildren)
                        <span class="caret"></span>
                        @endif

                    </a>
                    @if ($checkChildren)
                        <ul class="dropdown-menu">
                            @foreach (\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product') as $childProduct)
                                <li><a href="/cua-hang/{{ $childProduct->slug }}">{{ $childProduct->title  }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                        @php $checkChildren = false; @endphp
                    @endif
                @endforeach

                <li
                    @php $checkChildren = !empty(\App\Entity\Category::getChildrenCategory(0, 'post')); @endphp
                    @if ($checkChildren)
                        class="dropdown"
                    @endif
                    >

                    <a href="/danh-muc/tin-tuc"
                       @if ($checkChildren)
                       class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"
                            @endif
                    >
                        Tin tức
                        @if ($checkChildren)
                            <span class="caret"></span>
                        @endif

                    </a>
                    @if ($checkChildren)
                        <ul class="dropdown-menu">
                            @foreach (\App\Entity\Category::getChildrenCategory(0, 'post') as $childProduct)
                                <li><a href="/danh-muc/{{ $childProduct->slug }}">{{ $childProduct->title  }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                <li><a href="/lien-he">Liên hệ</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>


<header>
    <div class="carousel" data-count="3" data-current="2">
        <!-- <button class="btn btn-control"></button> -->

        <div class="items">
            @foreach(\App\Entity\SubPost::showSubPost('slider', 3) as $id => $slide)
            <div class="item {{ $id == 0 ? 'active' : '' }}" data-marker="{{ ($id+1) }}">
                <img src="{{ isset($slide['image']) ?  $slide['image'] : '' }}" alt="Background" class="background"/>

                <div class="content">
                    <div class="outside-content">
                        <div class="inside-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 align-center">


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <ul class="markers">
            @foreach(\App\Entity\SubPost::showSubPost('slider', 3) as $id => $slide)
            <li data-marker="{{ ($id+1) }}" class="{{ $id == 0 ? 'active' : '' }}"><img src="{{ isset($slide['image']) ?  $slide['image'] : '' }}" alt="{!! $slide->title !!}"/></li>
            </li>
            @endforeach
        </ul>
    </div>
</header>
<br><br>

@php
if (!empty(\App\Entity\Category::getDetailCategory('san-pham-trang-chu'))) {
$productIndexs = \App\Entity\Product::showProduct('san-pham-trang-chu', 20);
} else {
$productIndexs = \App\Entity\Product::newProduct(20);
}

@endphp

<div class="container">
    <div class="row">
        <div class="col-sm-12 align-center">
            <hr class="offset-md">
            <h2 class="h3 upp"><?= isset($information['tieu-de-san-pham-hoac-dich-vu']) ? $information['tieu-de-san-pham-hoac-dich-vu'] : 'Top sản phẩm hoặc dịch vụ' ?></h2>
            <hr class="offset-sm">
            <hr class="offset-md">
            <hr class="offset-lg">
        </div>

        @foreach($productIndexs as $hotProduct)
        <div class="col-sm-3 align-center">
            <div class="boxP">
                <a href="/{{ $hotProduct->slug }}" >
                    <img src="{{ isset($hotProduct['image']) ?  $hotProduct['image'] : '' }}" alt="{{ isset($hotProduct['title']) ?  $hotProduct['title'] : '' }}" class="image"/>
                </a>
                <br><br>

                <a href="/{{ $hotProduct->slug }}">
                    <h3>{{ isset($hotProduct['title']) ?  $hotProduct['title'] : '' }}</h3>
                </a>
                <p class="price red">
                    @if(empty($hotProduct->price))
                        <span>liên hệ</span>
                    @elseif ($hotProduct->price == 1)
                        <span>Miễn phí</span>
                    @elseif($hotProduct->discount > 0)
                        <span><del>{{ number_format($hotProduct->price) }} đ</del></span> - <span>{{ number_format($hotProduct->discount) }} đ</span>
                    @else
                        <span>{{ number_format($hotProduct->price) }} đ</span>
                    @endif
                </p>
            </div>
        </div>
        @endforeach
        <script async defer>
            $('.boxP').matchHeight();
        </script>
    </div>
</div>
<br><br>

<div class="subscribe">
    <div class="container align-left">
        <hr class="offset-md">
        <h1 class="h3 upp">{{ isset($information['tieu-de-trang']) ? $information['tieu-de-trang']  : '' }}</h1>

        <hr class="offset-sm">
        <div class="row">
            <div class="col-md-6">
                <p> <?= isset($information['sologan']) ? $information['sologan'] : '' ?></p>
            </div>
            <div class="col-md-6">
                <form onSubmit="return contact(this);" class="wpcf7-form" method="post"
                      action="/submit/contact" >
                    {!! csrf_field() !!}
                    <input type="hidden" name="is_json"
                           class="form-control captcha" value="1" placeholder="">

                    <div class="form-group">
                        <div>
                            <input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <input type="email" class="form-control" placeholder="Email của bạn" name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
									<textarea class="form-control" rows="2" name="message"
                                              placeholder="Nội dung ghi chú"></textarea>
                        </div>
                    </div>
                    @if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
                        <input type="hidden" value="{{ $_SERVER['HTTP_REFERER'] }}" name="utm_source" />
                    @endif

                    @if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
                        <div class="form-group">
                            <div class="center">
                                <button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <hr class="offset-lg">
        <hr class="offset-md">

    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-sm-12 align-center">
            <hr class="offset-md">
            <h2 class="h3 upp"><?= isset($information['tieu-de-cho-phan-hoi-khach-hang']) ? $information['tieu-de-cho-phan-hoi-khach-hang'] : 'Phản hồi khách hàng' ?></h2>
        </div>
        @foreach(\App\Entity\SubPost::showSubPost('khach-hang', 8) as $id => $customer)
        <div class="col-sm-3 align-center">
            @if (isset($customer['ma-chia-se-youtube']) && !empty($customer['ma-chia-se-youtube']))
                <div id="embbedYoutube{{$id}}">

                </div>
                <script>
                    $(window).load(function() {
                        $("#embbedYoutube{{$id}}").html('{!! $customer['ma-chia-se-youtube'] !!}');
                    });
                </script>
            @elseif (isset($customer['image']) && !empty($customer['image']))
                <a title="{{ isset($customer['title']) ?  $customer['title'] : '' }}">
                    <img src="{{ isset($customer['image']) ? $customer['image'] : '' }}" alt="" class="mg" style="max-width: 100%;">
                </a>
            @endif
            <br><br>

                <h4>{{ isset($customer['title']) ?  $customer['title'] : '' }}</h4>
        </div>
        @endforeach

    </div>
</div>
<hr class="offset-md">
<hr class="offset-md">
<hr>

<footer>
    <div class="container">
        <hr class="offset-md">

        <div class="row menu">
            <div class="col-sm-4 col-md-4">
                <h1 class="h4">Fanpage <i class="ion-plus-round hidden-sm hidden-md hidden-lg"></i></h1>

                <div class="contentFooter" id="loadFanpageFacebook">
                </div>
                <script>
                    $(window).load(function() {
                        $("#loadFanpageFacebook").html('<iframe src="https://www.facebook.com/plugins/page.php?href=<?= isset($information['nhung-fb']) ? $information['nhung-fb']  : '' ?>&tabs=timeline&width=340&height=200&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media">
                                </iframe>');
                    });
                </script>

            </div>
            <div class="col-sm-4 col-md-4">
                <h1 class="h4">Bài viết mới <i class="ion-plus-round hidden-sm hidden-md hidden-lg"></i></h1>

                <div class="list-group">
                    @foreach(\App\Entity\Post::categoryShow('tin-tuc',5) as $post)
                    <a href="/tin-tuc/{{ $post->slug }}" class="list-group-item">{{ isset($post['title']) ?  $post['title'] : '' }}</a>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-4 col-md-4 align-right hidden-sm hidden-xs">
                <h2 class="h4">Liên hệ chúng tôi</h2>

                {!! isset($information['thong-tin-lien-he']) ?  $information['thong-tin-lien-he'] : '' !!}
            </div>
        </div>
    </div>

    <hr>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 payments">
                @if(!empty($user) && $user->vip < 1)
                    <div class="copyright">
                        <span><a href="https://moma.vn">Tạo website marketing miễn phí trọn đời tại moma.vn</a></span>
                    </div>

                @else
                    <div class="copyright">
                        <span>{{ isset($information['copyright']) ?  $information['copyright'] : '' }}</span>
                    </div>

                @endif
            </div>
        </div>
    </div>
</footer>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/themes/quick/js/bootstrap.min.js"></script>
<script src="/themes/quick/js/core.js"></script>
<script src="/themes/quick/js/carousel.js"></script>
<script src="/themes/quick/js/carousel-recommendation.js"></script>

</body>
</html>
