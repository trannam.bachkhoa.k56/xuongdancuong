<div class="headerTop">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 headermenuTop">
					<div class="contenthd">

						<div id="logotop" class="owl-carousel">
                      @foreach(\App\Entity\SubPost::showSubPost('logo-doi-tac', 6) as $id => $logo)
	                        <div class="item">
	                            <div class="block">
	                               <img src="{{ $logo->image }}" alt="{{ $logo->title }}" width="100px">
	                            </div>
	                        </div>
                        
                        @endforeach
                    	</div>

					</div>
					<style type="text/css">
						
					</style>
					<script>
                        $("#logotop").owlCarousel({
                            autoPlay: 5000, //Set AutoPlay to 3 seconds
                            items : 6,
                            itemsDesktop : [1199,6],
                            itemsDesktopSmall : [979,4],
                            itemsTablet: [768,3],
                            itemsMobile : [481,2],
                            navigation:true,
                            paginationNumbers:true,
                            dots: true
                        });
                        
                        
                    </script>
				</div>
			</div>
		</div>
	</div>
<!-- <header> -->
	<!-- <div class="topnav">
		<div class="container position">
			<div class="pull-right tr col-md-8">
				<span><i class="fa fa-phone" aria-hidden="true"></i>+{{ isset($information['hotline']) ? $information['hotline'] : '' }}</span>
				<ul class="social">
					<li><a href="{{ isset($information['link-facebook']) ? $information['link-facebook'] : '' }}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="{{ isset($information['link-twitter']) ? $information['link-twitter'] : '' }}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="{{ isset($information['link-instagram']) ? $information['link-instagram'] : '' }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="{{ isset($information['link-google-']) ? $information['link-google-'] : '' }}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>

					<li><a href="{{ isset($information['hotline']) ? $information['hotline'] : '' }}"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
				</ul>
				<div class="search">
					<form action="{{ route('search_product') }}" method="get" id="headearch">
					<i class="fa fa-search" aria-hidden="true"></i>
					<div class="searchContent">
						<input type="text" placeholder="Tìm kiếm..." name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" onkeypress="return searchAjax(this);" onchange="return searchAjax(this);"/>
						<input type="hidden" name="category" value="all">
					</div>
					</form>
				</div>
				<script>
                    $('.search i').click(function(){
                        if($(this).parent().find('.searchContent').is(":hidden")){
                            $(this).parent().find('.searchContent').show();
                        } else {
                            $(this).parent().find('.searchContent').hide();
                        }
                    });
				</script>
			</div>
		</div>
	</div> -->
<!-- 	<div class="container">
			<div class="row">
				<div class="col-xs-12">
	<nav class="navbar navbar-default">
		
					<div class="logo">
						<a href="/"><img src="{{ $information['logo'] }}"/></a>
					</div>
					
					<div class="navbar-header">
						<button type="button" class="Navbutton navbar-toggle collapsed" data-toggle="collapse" data-target="#navMobile" aria-expanded="false">
							<span class="sr-only">Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="logoMobile hiddenPC">	
							<a href="/"><img src="{{ $information['logo'] }}"/></a>
						</div>
					</div>

					
					<div class="collapse navbar-collapse" id="navMobile">
						<div class="hiddenPC navMenu" id="navClose">
							<span id="hiddenNav" class="pull-left">x</span>

							<a href="/"><img src="{{ $information['logo'] }}"/></a>
							<a class="pull-right searchMain">
								<i class="fa fa-search" aria-hidden="true"></i>
							</a>
						</div>
						<div class="hiddenPC">
							<div class="searchMenu">
								<form>
									<input type="text" placeholder="Tìm kiếm" id="search_query_top" />
								</form>
							</div>
						</div>
		                <?php echo \App\Entity\MenuElement::showMenuElementPage('menu-chinh', 'nav navbar-nav', true) ?>
						<div class="hiddenPC socialNav">
							<ul class="social">
								<li><a href="{{ isset($information['link-facebook']) ? $information['link-facebook'] : '' }}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="{{ isset($information['link-twitter']) ? $information['link-twitter'] : '' }}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="{{ isset($information['link-instagram']) ? $information['link-instagram'] : '' }}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="{{ isset($information['link-google-']) ? $information['link-google-'] : '' }}"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>

							</ul>
						</div>
					</div>
				
	</nav>
	</div>
			</div>
		</div>
</header> -->

<header>
			
			<nav class="navbar navbar-default">
				<div class="container">
					<div class="logo">
						<a href="{{ isset($domainUrl) ? $domainUrl : ''}}"><img src="{{ $information['logo'] }}"/></a>
					</div>
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="Navbutton navbar-toggle collapsed" data-toggle="collapse" data-target="#navMobile" aria-expanded="false">
							<span class="sr-only">Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="logoMobile hiddenPC">	
							<a href="/"><img src="{{ $information['logo'] }}"/></a>
						</div>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navMobile">
						<div class="hiddenPC navMenu" id="navClose">
							<span id="hiddenNav" class="pull-left">x</span>
							<a href="{{ isset($domainUrl) ? $domainUrl : ''}}"><a href="{{ isset($domainUrl) ? $domainUrl : ''}}"><img src="{{ $information['logo'] }}"/></a></a>
							<a class="pull-right searchMain">
								<i class="fa fa-search" aria-hidden="true"></i>
							</a>
						</div>
						<div class="hiddenPC">
							<div class="searchMenu">

								<form action="{{ route('search_product') }}" method="get" id="headearch">
									
								
										<input type="text" placeholder="Tìm kiếm..." name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" onkeypress="return searchAjax(this);" onchange="return searchAjax(this);"  id="search_query_top"/>
										<input type="hidden" name="category" value="all">
								
								</form>

								<!-- <form>
									<input type="text" placeholder="Tìm kiếm" id="search_query_top" />
								</form> -->
							</div>
						</div>
						<?php echo \App\Entity\MenuElement::showMenuElementPage('menu-chinh', 'nav navbar-nav', true) ?>

						<!-- <div class="hiddenPC socialNav">
							<ul class="social">
								<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
								<li class="pull-right"><a rel="nofflow" title="english" href=""><img width="15" alt="en" src="img/icon-english.png"></a></li>
								<li class="pull-right"><a rel="nofflow" title="english" href=""><img width="15" alt="en" src="img/icon-vietnam.png"></a></li>
							</ul>
						</div> -->
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</header>