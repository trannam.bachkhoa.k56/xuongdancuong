@extends('news-01.layout.site')


@section('title', 'tim-kiem')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', '')

@section('content')

    <section class="viewBg viewTextbox wow fadeInUp" data-wow-offset="300">
         <div class="mask"></div>
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-xs-12">
                    <div class="categoryProduct">
                        <div class="hotDeal clearfix">
                            <div class="mainTitle lineblue">
                                <h1 class="titleV bgblue">
                                    <i class="fa fa-cubes" aria-hidden="true"></i>Từ khóa tìm kiếm : ' {{ (!empty($_GET['word'])) ? $_GET['word'] : '' }} '
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <ul class="listProduct">
                        @if(empty($products))
                            <p>Không tồn tại danh mục này</p>
                        @else
                        @foreach($products as $product)
                            <li class="row left">
                            <div class="col-md-6 col-xs-12">
                                <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="thumbs"><img src="{{ isset($product->image) ? $product->image : "" }}"/></a>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h3><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{{ isset($product->title) ? $product->title : "" }}</a></h3>
                                <div class="except">
                                    {{ isset($product->description) ? $product->description : "" }}
                                </div>
                                <div class="tr readMore"><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="BtnBlack">Đọc thêm</a></div>
                            </div>
                        </li>
                        @endforeach
                            <div class="paginationProduct col-xs-12 col-md-12">
                                @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                                    {{ $products->links() }}
                                @endif
                            </div>
                            @endif
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
