@extends('news-01.layout.site')


@section('title', !empty($category->title) ? $category->title : $category->title)
@section('meta_description',  !empty($category->meta_description) ? $category->meta_description : $category->description)
@section('keywords', $category->meta_keyword )
@section('content')

    <section class="viewBg viewTextbox wow fadeInUp mgtop" data-wow-offset="300" style="background: url('{!! isset($information['background-trang-chu']) ? $information['background-trang-chu'] : "" !!}') repeat">
        <div class="mask"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="infoProduct row" style="background: url('{{ isset($information['backgruod-san-pham']) ? 
                        asset($information['backgruod-san-pham']) : "" }}');background-attachment: fixed;
    background-size: cover;">
                        <div class="cont">
                            <h2><span>{{ isset($category->title) ? $category->title : ''}}</span></h2>
                           
                            <div class="Views" style="color: #000">
                                {!! isset($category->description) ? $category->description : "" !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <ul class="listProduct">
                        @foreach($products as $product)
                        <li class="row left">
                            <div class="col-md-6 col-xs-12">
                                <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="thumbs"><img src="{{ isset($product->image) ? $product->image : "" }}"/></a>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h3><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{{ isset($product->title) ? $product->title : "" }}</a></h3>
                                <div class="except">
                                    {{ isset($product->description) ? $product->description : "" }}
                                </div>
                                <div class="tr readMore"><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="BtnBlack">Đọc thêm</a></div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <style type="text/css">
       
    </style>

    <!-- <section class="tabProduct">
        <div class="container">
            <div class="row list">
                <div class="col-md-12 ultab">
                    <ul class="nav nav-tabs" role="tablist">
                       
                        @foreach (\App\Entity\Product::showProduct('danh-sach-du-an',6) as $id => $product_project)
                        <li role="presentation" class="{{ $id == 0 ? 'active' : '' }}"><a href="#{{$id}}" aria-controls="home" role="tab" data-toggle="tab">{{ isset($product_project['title']) ? $product_project['title'] : ''}}</a></li>
                        @endforeach
                    </ul>
                </div>
               

                <div class="col-md-12 ultabMobile">
                    <div class="dropdown">
                      <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Danh sách dự án 
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu nav nav-tabs" aria-labelledby="dLabel" role="tablist">
                         @foreach (\App\Entity\Product::showProduct('danh-sach-du-an',6) as $id => $product_project)
                        <li role="presentation" class="{{ $id == 0 ? 'active' : '' }}"><a href="#{{$id}}" aria-controls="home" role="tab" data-toggle="tab">{{ isset($product_project['title']) ? $product_project['title'] : ''}}</a></li>
                        @endforeach
                      </ul>
                    </div>
                </div>
              
            </div>
            
        </div>
    </section> -->
   <!--  <section class="contentTab">
        <div class="container">
            <div class="row contentlist">
                <div class="col-md-12">
                    <div class="tab-content">
                        @foreach (\App\Entity\Product::showProduct('danh-sach-du-an',6) as $id => $product_project)
                            <div role="tabpanel" class="tab-pane {{ $id == 0 ? 'active' : '' }}" id="{{ $id}}">
                                <div class="content">
                                  {!! isset($product_project['content']) ? $product_project['content'] : 'Đang cập nhật' !!}
                                </div>  

                            </div>
                         @endforeach

                       
                    </div>
                </div>
            </div>
        </div>
    </section> -->

@endsection
