@extends('news-01.layout.site')


@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)

@section('content')
	<section class="viewNews wow fadeInUp" data-wow-offset="300" style="background: url('{!! isset($information['background-trang-chu']) ? $information['background-trang-chu'] : "" !!}') repeat">
		<div class="mask"></div>
		<div class="container">
			<div class="infoNews">
				<div class="cont">
					<h1 class="titleNews">{{ isset($product->title) ? $product->title : "" }}</h1>
					
					<div class="exceptNews mb20">
						{{ isset($product->description) ? $product->description : "" }}
					</div>
					<div class="contentNews">
						<!-- <div class="tc mb20"><img src="{{ isset($product->image) ? $product->image : "" }}"/></div> -->
						<p>
							{!! isset($product->content) ? $product->content : "" !!}
						</p>
					</div>

					<div class="Listreact">
                        <h3>Sản phẩm liên quan</h3>
                        <ul class="">
                            @foreach(\App\Entity\Product::relativeProduct($product->slug,$product->product_id, 6) as $id => $productRelative)
                             <li><a href="{{ route('product',['cate_slug' => $productRelative->slug]) }}"> <img src="{{ asset('news-01/img/iconlist.png')}}"> {{ $productRelative->title }}</a></li>
                            @endforeach
                            <!-- <li><a href=""><img src="{{ asset('news-01/img/iconlist.png')}}">Hải Phát land tiến bước vững chắc</a></li>
                            <li><a href=""><img src="{{ asset('news-01/img/iconlist.png')}}">Lời khuyên cho nhà đầu tư</a></li>
                            <li><a href=""><img src="{{ asset('news-01/img/iconlist.png')}}">Cận cảnh 4 cây cầu xây tại hà nội</a></li> -->
                        </ul>
                    </div>
					
				</div>
			</div>
		</div>
	</section>
@endsection
