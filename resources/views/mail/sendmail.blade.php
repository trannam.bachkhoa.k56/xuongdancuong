<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<div class="sendmail" style="width: 500px;text-align: center;">
		<div><img src="{{ $link_web.$link_logo}}" alt="" width="200px"></div>
		<p>Xin chào : {{ $name }}</p>
		<p>Mật khẩu mới của bạn là : {{ $newPassword }} </p>
		<p style="font-size: 30px">Thông tin liên hệ website </p>
		<div style="text-align: justify;margin-left: 30%;">
			<p>Email : {{ $email }}</p>
			<p>Địa chỉ : {{ $address }}</p>
			<p>Số điện thoại : {{ $phone }}</p>
			<p>Facebook : {{ $link_fb }}</p>
		</div>
	</div>
</body>
</html>