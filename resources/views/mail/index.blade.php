<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
    <p><?= $content ?></p>
    <p >{!! $sign !!}</p>
    @if (!empty($contact_id))
        <img src='https://moma.vn/mailcheck?contact_id={{ $contact_id }}&subject={{ $subjectContact }}&email_manager={{ $emailManager }}' />
    @endif
</body>
</html>
