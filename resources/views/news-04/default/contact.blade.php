@extends('news-01.layout.site')

@section('title','Liên hệ')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : "")

@section('content')
	<section class="contacForm">
		<div class="container contactContent">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<form action="{{ route('sub_contact') }}" method="post">
						{!! csrf_field() !!}
						<div class="row">
							<div class="col-md-6">
								<div class="infoContact">
									<img src="{{ $information['logo-contact'] }}"/>
									<h2>TẬP ĐOÀN FLC</h2>
									<p>Địa chỉ: Tầng 1, tòa nhà FLC Landmark Tower, đường Lê Đức Thọ, phường Mỹ Đình 2, quận Nam Từ Liêm, thành phố Hà Nội, Việt Nam
										<br>Điện thoại: <a href="#">(84-4) 3556 2666</a> / Fax: (84-4) 3724 2999
										<br>Email: <a href="#">kinhdoanhbds@flc.vn</a></p>
								</div>
							</div>
							<div class="col-md-6 boxForm">
								<div class="form">
									<div class="form-group">
										<input type="text" class="form-control" id="name" name="name" value="" placeholder="Full name *">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Phone *">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="email" name="email" value="" placeholder="Email *">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="address" name="address" value="" placeholder="Address">
									</div>
									<div class="form-group">
										<textarea style="height: 134px;" class="form-control" id="Message" name="Message" value="" placeholder="Message"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="tr col-md-12">
								<button type="submit" class="btnContact">Gửi</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
@endsection
