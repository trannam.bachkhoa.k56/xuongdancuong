@extends('news-01.layout.site')


@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('content')
	<section class="viewNews wow fadeInUp" data-wow-offset="300">
		<div class="mask"></div>
		<div class="container">
			<div class="infoNews">
				<div class="cont">
					<h1 class="titleNews">{{ isset($product->title) ? $product->title : "" }}</h1>
					<div class="DateTime">
                        <?php $date=date_create($product->created_at); ?>
						<i class="fa fa-calendar"></i> <?= date_format($date,"d-m-Y H:i") ?>
					</div>
					<div class="exceptNews mb20">
						{{ isset($product->description) ? $product->description : "" }}
					</div>
					<div class="contentNews">
						<div class="tc mb20"><img src="{{ isset($product->image) ? $product->image : "" }}"/></div>
						<p>
							{!! isset($product->content) ? $product->content : "" !!}
						</p>
					</div>
					<div class="Tags">
						<i class="fa fa-tag" aria-hidden="true"></i>Tags
                        <?php $tags = explode(',', $product->tags)?>
						@foreach($tags as $tag)
							<a href="">{!! $tag !!}</a>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
