@extends('news-04.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : "")
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : "")
    
@section('content')

    <section class="textBox">
        <div class="mask"></div>
        <div class="container">
            <div class="infoText">
                <div class="cont">
                    <h3 class="title"><strong>GIỚI THIỆU</strong><br>{{ isset($information['tieu-de-gioi-thieu-trang-chu']) ? $information['tieu-de-gioi-thieu-trang-chu'] : "" }}<span></span></h3>
                    <p class="except">{{ isset($information['mo-ta-gioi-thieu-trang-chu']) ? $information['mo-ta-gioi-thieu-trang-chu'] : "" }}</p>
                </div>
            </div>
            <div class="row infoComp">
                @foreach(\App\Entity\Post::categoryShow('gioi-thieu', 3) as $introduction)
                <div class="boxLeft col-md-4">
                    <img src="{{ $introduction->image }}"/>
                    <div class="content">
                        <h3>{{ $introduction->title }}</h3>
                        <p>{{ $introduction->description }}</p>
                        <a class="btnY" href="{{ route('post', ['cate_slug' => 'gioi-thieu', 'post_slug' => $introduction->slug]) }}">Đọc thêm</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="Product wow fadeInUp" data-wow-offset="300">
        <div class="container">
            <div class="box">
                <h2>SẢN PHẨM<span></span></h2>
                <p>{{ isset($information['mo-ta-san-pham-trang-chu']) ? $information['mo-ta-san-pham-trang-chu'] : "" }}</p>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div id="product" class="owl-carousel">
                        @foreach (\App\Entity\Product::showProduct('nha-dat', 5) as $id => $product)
                        <div class="item">
                            <div class="block">
                                <a class="thumbs" href="{{ route('product', [ 'post_slug' => $product->slug]) }}"><img src="{{ $product->image }}"/>
                                </a>
                                <h3>{{ $product->title }}</h3>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <script>
                        $("#product").owlCarousel({

                            autoPlay: 5000, //Set AutoPlay to 3 seconds

                            items : 3,
                            itemsDesktop : [1199,3],
                            itemsDesktopSmall : [979,3],
                            itemsTablet: [768,2],
                            itemsMobile : [481,1]

                        });
                    </script>
                </div>
            </div>
        </div>
    </section>
    <section class="infomation container wow fadeInUp" data-wow-offset="300">
        <div class="infoText row">
            <div class="col-md-8 col-md-offset-2 cont">
                <h3  class="title"><strong>LỢI ÍCH</strong><br>ĐẦU TƯ<span></span></h3>
                <p class="except">{{ isset($information['mo-ta-loi-ich-dau-tu-trang-chu']) ? $information['mo-ta-loi-ich-dau-tu-trang-chu'] : "" }} </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="infomation" class="owl-carousel">
                    @foreach(\App\Entity\Post::categoryShow('loi-ich-dau-tu',10 ) as $id => $useful)
                    <div class="item">
                        <div class="block">
                            <a class="thumbs" href="#"><img src="{{ $useful->image }}"/>
                                <div class="mask"></div>
                                <div class="detail"></div>
                            </a>
                            <h3>{{ $useful->title }}</h3>
                            <p>{{ $useful->description }}</p>
                            <div class="more"><a href="/loi-ich-dau-tu/{{ $useful->slug }}">View Hotel Now</a></div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="readMore">
                    <a href="#">
                        Đọc thêm
                    </a>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#infomation").owlCarousel({

            autoPlay: 5000, //Set AutoPlay to 3 seconds

            items : 3,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,1],
            itemsTablet: [768,1],
            itemsMobile : [481,1]

        });
    </script>
    <!-- Chi danh cho danh sach anh -->
    <script type="text/javascript" src="http://cdn.jsdelivr.net/hammerjs/2.0.3/hammer.min.js"></script>
    <script type="text/javascript" language="javascript" src="js/FlameViewportScale.js"></script>
    <script src="js/masonry.pkgd.js"></script>
    <script src="js/jquery.tosrus.min.all.js"></script>
    <section class="Extra">
        <div class="container">
            <div class="infoText infoTextWhite row">
                <div class="col-md-6 col-md-offset-3 cont">
                    <h3 class="title"><strong>DỊCH VỤ</strong><br>& TIỆN ÍCH<span></span></h3>
                    <p class="except">{{ isset($information['mo-ta-dich-vu-tien-ich']) ? $information['mo-ta-dich-vu-tien-ich'] : "" }} </p>
                </div>
            </div>
            <div class="grid" id="listImage">
                <div class="grid-sizerl"></div>
                @foreach (\App\Entity\Product::showProduct('dich-vu-tien-ich', 5) as $id => $product)
                    @if ($id == 0)
                        <div class="grid-iteml gridH1 grid-box">
                            <a href="{{ $product->slug }}"><img src="{{ $product->image }}"/></a>
                            <h3 class="PoTop"><b>{{ $product->title }}</b></h3>
                        </div>
                    @else
                        <div class="grid-iteml gridH2 grid-box">
                            <a href="{{ $product->slug }}"><img src="{{ $product->image }}"/></a>
                            <h3 class="PoTop"><b>{{ $product->title }}</b></h3>
                        </div>
                    @endif
                @endforeach

            </div>
            <script>
                $('#listImage a').tosrus({
                    buttons: 'inline',
                    pagination	: {
                        add			: true,
                        type		: 'thumbnails'
                    }
                });
                $('.grid').masonry({
                    // options
                    columnWidth: '.grid-sizerl',
                    itemSelector: '.grid-iteml',
                    percentPosition: true
                });
            </script>
        </div>
    </section>

@endsection

