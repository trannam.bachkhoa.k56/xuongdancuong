@extends('news-01.layout.site')


@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title)
@section('meta_description',  !empty($category->meta_description) ? $category->meta_description : $category->description)
@section('keywords', $category->meta_keyword )
@section('content')

    <section class="viewBg viewTextbox wow fadeInUp" data-wow-offset="300">
        <div class="mask"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="infoProduct row">
                        <div class="cont">
                            <h2><span>Sản phẩm</span></h2>
                            <div class="Views">
                                {{ isset($information['mo-ta-san-pham-trang-chu']) ? $information['mo-ta-san-pham-trang-chu'] : "" }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <ul class="listProduct">
                        @foreach($products as $product)
                        <li class="row left">
                            <div class="col-md-6 col-xs-6">
                                <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="thumbs"><img src="{{ isset($product->image) ? $product->image : "" }}"/></a>
                            </div>
                            <div class="col-md-6 col-xs-6">
                                <h3><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{{ isset($product->title) ? $product->title : "" }}</a></h3>
                                <div class="except">
                                    {{ isset($product->description) ? $product->description : "" }}
                                </div>
                                <div class="tr readMore"><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="BtnBlack">Đọc thêm</a></div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
