@extends('news-01.layout.site')


@section('title', 'tim-kiem')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', '')

@section('content')

    <section class="contentCategoryProduct">
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-xs-12">
                    <div class="categoryProduct">
                        <div class="hotDeal clearfix">
                            <div class="mainTitle lineblue">
                                <h1 class="titleV bgblue">
                                    <i class="fa fa-cubes" aria-hidden="true"></i>Tìm kiếm sản phẩm
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <ul class="listProduct">
                        @foreach($products as $product)
                            <li class="row left">
                                <div class="col-md-6 col-xs-6">
                                    <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="thumbs"><img src="{{ isset($product->image) ? $product->image : "" }}"/></a>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <h3><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{{ isset($product->title) ? $product->title : "" }}</a></h3>
                                    <div class="except">
                                        {{ isset($product->description) ? $product->description : "" }}
                                    </div>
                                    <div class="tr readMore"><a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="BtnBlack">Đọc thêm</a></div>
                                </div>
                            </li>
                        @endforeach
                            <div class="paginationProduct col-xs-12 col-md-12">
                                @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                                    {{ $products->links() }}
                                @endif
                            </div>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
