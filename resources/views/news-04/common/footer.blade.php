<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 hiddenMB">
                <h3>FLC HA LONG BAY GOLF CLUB
                    <br>& LUXURY RESORT</h3>
                <div class="infoCompany">
                    <p>Address: {{ isset($information['address']) ? $information['address'] : "" }}<br>
                        Resort Map<br>
                        Phone: <a href="#">{{ isset($information['phone']) ? $information['phone'] : "" }}</a><br>
                        Email: <a href="#">{{ isset($information['email']) ? $information['email'] : "" }}</a></p>
                </div>
            </div>
            <div class="col-md-3 hiddenMB">
                <h3>view all awards</h3>
                <p>As a subscriber to our newsletter
                    you are automatically entered in </p>
            </div>
            <div class="col-md-3 tcMb">
                <h3>MẠNG XÃ HỘI LIÊN KẾT</h3>
                <ul class="list-link">
                    <li class="list-link-item"><a href="{{ isset($information['link-facebook']) ? $information['link-facebook'] : "" }}"><i class="fa fa-facebook" aria-hidden="true"></i><span class="hiddenMB">Kết nối Facebook</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-youtube']) ? $information['link-youtube'] : "" }}"><i class="fa fa-youtube" aria-hidden="true"></i><span class="hiddenMB">Xem trên Youtube</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-twitter']) ? $information['link-twitter'] : "" }}"><i class="fa fa-twitter" aria-hidden="true"></i><span class="hiddenMB">Theo dõi trên Twitter</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-instagram']) ? $information['link-instagram'] : "" }}"><i class="fa fa-instagram" aria-hidden="true"></i><span class="hiddenMB">Theo dõi trên Instagram</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-google+']) ? $information['link-google+'] : "" }}"><i class="fa fa-google" aria-hidden="true"></i><span class="hiddenMB">Tham gia Google+</span></a></li>
                </ul>
            </div>
            <div class="col-md-3 hiddenMB">
                <h3>ĐĂNG KÝ NHẬN BẢN TIN</h3>
                <p class="regMail"><i class="fa fa-send" aria-hidden="true"></i>Vui lòng nhập địa chỉ Email để nhận được
                    bản tin khuyến mại của chúng tôi</p>
                <div class="search-footer">
                    <form onsubmit="return subcribeEmailSubmit(this)">
                        {{ csrf_field() }}
                    <input type="text" class="emailSubmit form-control" name="" placeholder="Email" required/><button type="submit" >Đăng ký</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</footer>
<section class="linkSite">
    <nav class="linkFooter">
        <div class="container">
            {{--<ul class="hiddenMB">--}}
                {{--<li><a href="#">GIỚI THIỆU&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">Dự án&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">Sản phẩm&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">Lợi ích đầu tư&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">TIN TỨC&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">LIÊN HỆ&nbsp;&nbsp;/</a></li>--}}
            {{--</ul>--}}
        <?php echo \App\Entity\MenuElement::showMenuElementPage('menu-phu', 'nav navbar-nav', true) ?>
        </div>
    </nav>
    <div class="logoFooter">
        <div class="container">
            <div class="throught"></div>
            <a href="#"><img src="img/logoFooter.png"/></a>
        </div>
    </div>
    <div class="container coppyRight">
        <a href="#">ABOUT FLC GROUP / CAREERS / DEVELOPMENT / CONTACT US  / FAQ /  TERMS & CONDITIONS /  PHISHING  / WEB ACCESSIBILITY / SITE MAP </a>
        <div>© 2016 FLC Group - FLC Quynhon Beach and Golf Resort</div>
    </div>
</section>
<a id="toTop" href="#"></a>