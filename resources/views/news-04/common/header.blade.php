<header>
	<div class="topnav">
		<div class="container position">
			<div class="pull-right tr col-md-8">
				<span><i class="fa fa-phone" aria-hidden="true"></i>+8493 455 3435</span>
				<ul class="social">
					<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
				</ul>
				<div class="search">
					<form action="{{ route('search_product') }}" method="get" id="headearch">
					<i class="fa fa-search" aria-hidden="true"></i>
					<div class="searchContent">
						<input type="text" placeholder="Tìm kiếm..." name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" onkeypress="return searchAjax(this);" onchange="return searchAjax(this);"/>
						<input type="hidden" name="category" value="all">
					</div>
					</form>
				</div>
				<script>
                    $('.search i').click(function(){
                        if($(this).parent().find('.searchContent').is(":hidden")){
                            $(this).parent().find('.searchContent').show();
                        } else {
                            $(this).parent().find('.searchContent').hide();
                        }
                    });
				</script>
				<select>
					<option>EN</option>
				</select>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="logo">
				<a href="/"><img src="{{ $information['logo'] }}"/></a>
			</div>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="Navbutton navbar-toggle collapsed" data-toggle="collapse" data-target="#navMobile" aria-expanded="false">
					<span class="sr-only">Menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="logoMobile hiddenPC">
					<a href="index.php"><img src="img/logo.png"/></a>
				</div>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navMobile">
				<div class="hiddenPC navMenu" id="navClose">
					<span id="hiddenNav" class="pull-left">x</span>
					<a href="index.php"><img src="img/logo.png"/></a>
					<a class="pull-right searchMain">
						<i class="fa fa-search" aria-hidden="true"></i>
					</a>
				</div>
				<div class="hiddenPC">
					<div class="searchMenu">
						<form>
							<input type="text" placeholder="Tìm kiếm" id="search_query_top" />
						</form>
					</div>
				</div>
                <?php echo \App\Entity\MenuElement::showMenuElementPage('menu-chinh', 'nav navbar-nav', true) ?>
				<div class="hiddenPC socialNav">
					<ul class="social">
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
						<li class="pull-right"><a rel="nofflow" title="english" href=""><img width="15" alt="en" src="img/icon-english.png"></a></li>
						<li class="pull-right"><a rel="nofflow" title="english" href=""><img width="15" alt="en" src="img/icon-vietnam.png"></a></li>
					</ul>
				</div>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
</header>
<section class="slider">

	<!-- Wrapper for slides -->
	<script type="text/javascript" src="{{ asset('news-01/js/modernizr.custom.js') }}"></script>
	<script type="text/javascript" src="{{ asset('news-01/js/jquery.cslider.js') }}"></script>
	<div id="da-slider" class="da-slider">
		{{--<div class="da-slide w-video">--}}
		{{--<video width="1920" height="1080" id="video1" autoplay loop>--}}
		{{--<source src="http://flcquynhon.com.vn/wp-content/themes/blankslate/videos/6.mp4" type="video/mp4">--}}
		{{--</video>--}}
		{{--</div>--}}
		@foreach(\App\Entity\SubPost::showSubPost('slide', 8) as $id => $slide)
			<div class="da-slide">
				<div class="da-img"><img src="{{ $slide->image }}" alt="{{ $slide->title }}"></div>
				<div class="daExcept">
					<h2>{{ $slide->title }}<span></span></h2>
					<p>{{ $slide->description }}</p>
				</div>
			</div>
		@endforeach
		<nav class="da-arrows">
			<span class="da-arrows-prev"></span>
			<span class="da-arrows-next"></span>
		</nav>
	</div>
	<script type="text/javascript">
        $('#da-slider').cslider();

        var vid = document.getElementById("video1");
        // vid.play();
        $('.w-video').click(function(){
            $(this).toggleClass('pause');
            if(vid.paused == false){
                vid.pause();
            }else{
                vid.play();
            }

        })
	</script>
</section>
