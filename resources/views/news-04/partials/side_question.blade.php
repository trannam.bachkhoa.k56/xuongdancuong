@if (isset($cateSlug) && $cateSlug != 'hot-deal')
<div class="sideBarContent hotDealProduct">
    <h2 class="titleV bgred"><i class="fa fa-flag-o" aria-hidden="true"></i>Hotdeal</h2>
    <div id="slideHotdeal" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach(\App\Entity\Product::showProduct('hot-deal', 4) as $id => $product)
                <div class="item @if($id ==0) active @endif productList" style="text-align: center;">
                    <div class="item">
                        <div class="CropImg CropImgP">
                            <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="image thumbs">
                                <img src="{{ $product->image }}" alt="{{ $product->title }}" title="{{ $product->title }}"/>
                                @if (!empty($product->discount))
                                    <span class="discountPersent">-{{ round(($product->price - $product->discount) / $product->price * 100) }}%</span>
                                @endif
                            </a>
                        </div>
                        <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                            <h3>{{ \App\Ultility\Ultility::textLimit($product->title, 10) }}</h3>
                        </a>
                       
						
						<p class="labelProduct {{ App\Ultility\Ultility::createSlug($product['nhan-cho-san-pham']) }}">
                                         {{ $product['nhan-cho-san-pham'] }}
                                     </p>
						
                        <div class="price">
                            @if (!empty($product->price_deal))
                                <span class="priceOld">{{ number_format($product->price, 0, ',', '.') }}</span>
                                <span class="priceDiscount">{{ number_format($product->price_deal, 0, ',', '.') }}</span> VNĐ
                            @elseif (!empty($product->discount))
                                <span class="priceOld">{{ number_format($product->price, 0, ',', '.') }}</span>
                                <span class="priceDiscount">{{ number_format($product->discount, 0, ',', '.') }}</span> VNĐ
                            @else
                                <span class="priceDiscount">{{ number_format($product->price, 0, ',', '.') }}</span> VNĐ
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#slideHotdeal" role="button" data-slide="prev">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
        </a>
        <a class="right carousel-control" href="#slideHotdeal" role="button" data-slide="next">
            <i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
</div>
@endif
<div class="sideBarContent comment clearfix mb20">
    <h3 class="titleV bgorange"><i class="fa fa-comments" aria-hidden="true"></i> Bình luận mới</h3>
    @foreach(\App\Entity\Comment::getCommentHome() as $id => $comment)
    <div class="item">
        <p class="name">{{ $comment->user_full_name }}</p>
        <p class="link"><a href="{{ ($comment->post_type == 'post') ?  route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $comment->slug]) : route('product', [ 'post_slug' => $comment->slug]) }}">
                {{ $comment->title }}</a></p>
        <p class="content"><i>"{{ $comment->content }}"</i></p>
    </div>
    @endforeach
</div>

<div class="sideBarContent newProduct mb20">
    <h3 class="titleV bggreen"><i class="fa fa-cubes" aria-hidden="true"></i>Sản phẩm bán chạy</h3>
    <div id="carousel-new-product" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner productList productNew" role="listbox">
            @foreach(\App\Entity\Product::newProduct(5) as $id => $product)
                <div class="item @if($id ==0) active @endif" style="text-align: center;">
                    <div class="newPro">
						<div class="CropImg CropImgP mb10">
							<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="image thumbs">
								<img src="{{ $product->image }}" title="{{ $product->title }}" alt="{{ $product->title }}"/>
                                @if (!empty($product->price_deal)
                                    && !empty($product->discount_end) && (time() < strtotime($product->discount_end))
                                    && !empty($product->discount_start) && (time() < strtotime($product->discount_start))
                                )
                                    <span class="discountPersent">-{{ round(($product->price - $product->price_deal) / $product->price * 100) }}%</span>
                                @elseif (!empty($product->discount))
									<span class="discountPersent">-{{ round(($product->price - $product->discount) / $product->price * 100) }}%</span>
								@endif
							</a>
						</div>
                        <a alt="{{ \App\Ultility\Ultility::textLimit($product->title, 15) }}" href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                            <h3>{{ \App\Ultility\Ultility::textLimit($product->title, 8) }}</h3>
                        </a>
                        <p class="labelProduct {{ App\Ultility\Ultility::createSlug($product['nhan-cho-san-pham']) }}">
                            {{ $product['nhan-cho-san-pham'] }}
                        </p>
						
						
                        <div class="price">
                            @if (!empty($product->price_deal)
                            && !empty($product->discount_end) && (time() < strtotime($product->discount_end))
                            && !empty($product->discount_start) && (time() > strtotime($product->discount_start))
                            )
                                <span class="priceOld">{{ number_format($product->price, 0, ',', '.') }}</span>
                                <span class="priceDiscount">{{ number_format($product->price_deal, 0, ',', '.') }}</span> VNĐ
                            @elseif (!empty($product->discount))
                                <span class="priceOld">{{ number_format($product->price, 0, ',', '.') }}</span>
                                <span class="priceDiscount">{{ number_format($product->discount, 0, ',', '.') }}</span> VNĐ
                            @else
                                <span class="priceDiscount">{{ number_format($product->price, 0, ',', '.') }}</span> VNĐ
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-new-product" role="button" data-slide="prev">
			<i class="fa fa-angle-left" aria-hidden="true"></i>
        </a>
        <a class="right carousel-control" href="#carousel-new-product" role="button" data-slide="next">
			<i class="fa fa-angle-right" aria-hidden="true"></i>
        </a>
    </div>
</div>

<div class="sideBarContent question mb20">
    <h3 class="titleV bgorange"><i class="fa fa-cubes" aria-hidden="true"></i>Các câu hỏi thường gặp</h3>
    @foreach (\App\Entity\SubPost::showSubPost('cau-hoi-thuong-gap', 8) as $id => $question)
        <p>
            <a href="/bo-sung/cau-hoi-thuong-gap">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i> {{ $question->title }}
            </a>
        </p>
    @endforeach
</div>
<div class="sideBarContent fanpageFacebook">
    <h3 class="titleV bggreen"><i class="fa fa-facebook-official" aria-hidden="true"></i>FANPAGE FACEBOOK</h3>
    <?= $information['fanpage-facebook'] ?>
</div>
