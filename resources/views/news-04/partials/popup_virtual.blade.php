<!-- Modal -->
<div class="Notification" id="popupVitural">
	<div class="Closed"><i class="fa fa-close" aria-hidden="true"></i></div>
	<div class="Content">
		<div class="row">
			<div class="col-md-3 left">
			
			</div>
			<div class="col-md-9 right">
			
			</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function() {
		$('#popupVitural').hide();
        setInterval(function(){
            $.ajax({
                type: "get",
                url: '{!! route('virtural_comment') !!}',
                success: function(data){
                    var obj = jQuery.parseJSON( data);
                    $('#popupVitural').find('.Content .left').empty();
                    $('#popupVitural').find('.Content .left').html(obj.image);

                    $('#popupVitural').find('.Content .right').empty();
                    $('#popupVitural').find('.Content .right').html(obj.content);

                }
            });
            $('#popupVitural').show().slideDown();
        }, 15000);
		
		$('#popupVitural .Closed').click(function(){
			$('#popupVitural').slideUp();
			
		});
    });
</script>
