﻿<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta name="ROBOTS" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="title" content="Artisan wines of the world" />

    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="geo.position" content="10.763945;106.656201" />

    <link rel="stylesheet" href="{{ asset('news-04/css/bootstrap.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('news-04/css/style.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('news-01/css/font-awesome.css') }}" media="all" type="text/css" />

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,800&subset=latin,vietnamese' rel='stylesheet' type='text/css'>

    <script src="{{ asset('news-01/js/jquery.min.js') }}"></script>
    <script src="{{ asset('news-01/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('news-01/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('news-01/js/bootstrap-transition.js') }}"></script>
    <script src="{{ asset('news-01/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('news-01/js/wow.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            //stick menu
            var s = $(".navbar");
            var pos = s.position();
            $(window).scroll(function() {
                var windowpos = $(window).scrollTop();

                if (windowpos >= pos.top) {
                    s.addClass("sticker");
                } else {
                    s.removeClass("sticker");
                }
            });
            //top
            // Hide the toTop button when the page loads.
            $("#toTop").css("display", "none");

            // This function runs every time the user scrolls the page.
            $(window).scroll(function(){

                // Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
                if($(window).scrollTop() > 0){

                    // If it's more than or equal to 0, show the toTop button.
                    console.log("is more");
                    $("#toTop").fadeIn("slow");

                }
                else {
                    // If it's less than 0 (at the top), hide the toTop button.
                    console.log("is less");
                    $("#toTop").fadeOut("slow");
                }
            });


            // When the user clicks the toTop button, we want the page to scroll to the top.
            $("#toTop").click(function(){

                // Disable the default behaviour when a user clicks an empty anchor link.
                // (The page jumps to the top instead of // animating)
                event.preventDefault();

                // Animate the scrolling motion.
                $("html, body").animate({
                    scrollTop:0
                },"slow");
            });



            $('.Navbutton').click(function(){
                $('body').addClass('Nobody');
            });
            $('#hiddenNav').click(function(){
                $('#navMobile').removeClass('in');
                $('body').removeClass('Nobody');
            });
            $('.navMenu .searchMain').click(function(){
                $('.searchMenu').slideDown();
            });
            wow = new WOW(
                {
                    animateClass: 'animated',
                    offset:       100,
                    callback:     function(box) {
                        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
                    }
                }
            );
            wow.init();
        });
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }
        function searchAjax(e) {
            var word = $(e).val();
            $('.search .bodySearch ').empty();

            $.ajax({
                type: "get",
                url: '{!! route('search_product_ajax') !!}',
                data: {
                    word: word,
                },
                success: function (result) {
                    var obj = jQuery.parseJSON(result);
                    $('.search .bodySearch ').empty();

                    $.each(obj.products, function (index, element) {

                        var html = '<div class="item">';
                        html += '<p><a href="/' + element.slug + '"><img src="' + element.image + '" alt="' + element.title + '"/>' + element.title + '' +
                            '<span class="price "> ';
                        console.log();
                        if (element.price_deal != null
                            && (element.discount_end != null) && (new Date().getTime() < Date.parse(element.discount_end))
                            && (element.discount_start != null) && (new Date().getTime() > Date.parse(element.discount_start))
                        ) {
                            html += '<span class="priceOld">' + numeral(element.price).format('0,0') + '</span>';
                            html += '<span class="priceDiscount">' + numeral(element.price_deal).format('0,0') + ' VNĐ</span>';
                        }
                        else if (element.discount != null) {
                            html += '<span class="priceOld">' + numeral(element.price).format('0,0') + '</span>';
                            html += '<span class="priceDiscount">' + numeral(element.discount).format('0,0') + ' VNĐ</span>';
                        } else {
                            html += '<span class="priceDiscount">' + numeral(element.price).format('0,0') + ' VNĐ</span>';
                        }
                        html += '</span></a></p>';

                        html += '</div>';

                        $('.search .bodySearch ').append(html);

                    });

                    $('.search .bodySearch ').append('<button class="btn btn-danger" onclick="return submitSearch(this);">Xem tất cả</button>')
                }

            });

            return true;
        }

        function submitSearch(e) {
            $('#headearch').submit();
        }
    </script>
</head>
<body class="">
    @include('news-01.common.header')

    <!-- Phần nội dung -->
    @yield('content')

    @include('news-01.common.footer')

    @include('news-01.common.login')

</body>
</html>
