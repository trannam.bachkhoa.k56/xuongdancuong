<!-- <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 hiddenMB">
                <h3>FLC HA LONG BAY GOLF CLUB
                    <br>& LUXURY RESORT</h3>
                <div class="infoCompany">
                    <p>Address: {{ isset($information['address']) ? $information['address'] : "" }}<br>
                        Resort Map<br>
                        Phone: <a href="#">{{ isset($information['phone']) ? $information['phone'] : "" }}</a><br>
                        Email: <a href="#">{{ isset($information['email']) ? $information['email'] : "" }}</a></p>
                </div>
            </div>
            <div class="col-md-3 hiddenMB">
                <h3>view all awards</h3>
                <p>As a subscriber to our newsletter
                    you are automatically entered in </p>
            </div>
            <div class="col-md-3 tcMb">
                <h3>MẠNG XÃ HỘI LIÊN KẾT</h3>
                <ul class="list-link">
                    <li class="list-link-item"><a href="{{ isset($information['link-facebook']) ? $information['link-facebook'] : "" }}"><i class="fa fa-facebook" aria-hidden="true"></i><span class="hiddenMB">Kết nối Facebook</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-youtube']) ? $information['link-youtube'] : "" }}"><i class="fa fa-youtube" aria-hidden="true"></i><span class="hiddenMB">Xem trên Youtube</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-twitter']) ? $information['link-twitter'] : "" }}"><i class="fa fa-twitter" aria-hidden="true"></i><span class="hiddenMB">Theo dõi trên Twitter</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-instagram']) ? $information['link-instagram'] : "" }}"><i class="fa fa-instagram" aria-hidden="true"></i><span class="hiddenMB">Theo dõi trên Instagram</span></a></li>
                    <li class="list-link-item"><a href="{{ isset($information['link-google+']) ? $information['link-google+'] : "" }}"><i class="fa fa-google" aria-hidden="true"></i><span class="hiddenMB">Tham gia Google+</span></a></li>
                </ul>
            </div>
            <div class="col-md-3 hiddenMB">
                <h3>ĐĂNG KÝ NHẬN BẢN TIN</h3>
                <p class="regMail"><i class="fa fa-send" aria-hidden="true"></i>Vui lòng nhập địa chỉ Email để nhận được
                    bản tin khuyến mại của chúng tôi</p>
                <div class="search-footer">
                    <form onsubmit="return subcribeEmailSubmit(this)">
                        {{ csrf_field() }}
                    <input type="text" class="emailSubmit form-control" name="" placeholder="Email" required/><button type="submit" >Đăng ký</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</footer> -->
<style type="text/css">
    
</style>
<footer class="footer">
    <div class="container">
        <div class="row footerTop">
            <div class="col-lg-4 col-md-6 col-xs-12 itemfooter hiddenMobile mdss-none">
                <div class="titlelast">
                   <h3>Phòng kinh doanh chính thức dự án</h3>
                </div>
                <div class="contentlast">
                    {!! isset($information['mo-ta-footer']) ? $information['mo-ta-footer'] : '' !!}
                </div>        
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 itemfooter itemfooterFirt">
                <div class="title">
                    <h3>{{ isset($information['tieu-de-form-footer']) ? $information['tieu-de-form-footer'] : '' }}</h3>
                    <p class="hotline">Hotline dự án : {{ isset($information['hotline']) ? $information['hotline'] : '' }}</p>
                </div>
                <div class="contentForm">
				
				
				
		
                    <form action="{{ route('sub_contact') }}" method="post">
                        {!! csrf_field() !!}
                      
                        
                        <div class="form-inner">
                                <input type="text" name="name" placeholder="Họ tên" required>
                        </div>

                        <div class="form-inner">
                              <input type="text" name="phone" placeholder="số điện thoại" required>
                        </div>
                        <div class="form-inner">
                                <input type="text" name="email" placeholder="Email" required>
                        </div>
						<input type="hidden"  class="form-control" id="address" name="address" value="" placeholder="Address">
                        <input type="hidden" class="form-control" id="address" name="Message" value="" placeholder="Address">
                        <div class="button">
                             <button>Đăng ký</button>
                        </div> 
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12 itemfooter itemfooterLast hiddenMobile mdss-none">
                <div class="title">
                     <h3>{{ isset($information['tieu-de-footer']) ? $information['tieu-de-footer'] : '' }}</h3>
                    
                </div>
                <div class="contentlast">
                    {!! isset($information['noi-dung-tieu-de-footer']) ? $information['noi-dung-tieu-de-footer'] : '' !!}
                </div>
            </div>
        </div>
        
    </div>
   <!--  <section class="footerBottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left copyright">
                        <p>{{ isset($information['coppyright']) ? $information['coppyright'] : '' }} </p>
                    </div>
                </div>
            </div>   
        </div>
    </section> -->
         
</footer>

<section class="linkSite">
    <nav class="linkFooter">
        <div class="container">
            {{--<ul class="hiddenMB">--}}
                {{--<li><a href="#">GIỚI THIỆU&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">Dự án&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">Sản phẩm&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">Lợi ích đầu tư&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">TIN TỨC&nbsp;&nbsp;/</a></li>--}}
                {{--<li><a href="#">LIÊN HỆ&nbsp;&nbsp;/</a></li>--}}
            {{--</ul>--}}
            <div class="row">
                <div class="col-md-12">
                    <!-- <ul class="mainMenu nav navbar-nav"  id="sortableListsBase">
                        <li class="itemMenu menu-li hasChild nav-item">
                            <a class="nav-link hvr-overline-from-center" href="">
                                
                            </a>
                        </li>
                    </ul> -->
                   <?php echo \App\Entity\MenuElement::showMenuElementPage('menu-phu', 'nav navbar-nav', true) ?> 
                </div>
            </div>
        </div>
    </nav>
    <div class="logoFooter">
        <div class="container">
            <div class="throught"></div>
            <a href="./"><img src="{{ $information['logo'] }}"/></a>
        </div>
    </div>
    <div class="container coppyRight">
        <!-- <a href="#">ABOUT FLC GROUP / CAREERS / DEVELOPMENT / CONTACT US  / FAQ /  TERMS & CONDITIONS /  PHISHING  / WEB ACCESSIBILITY / SITE MAP </a> -->
        <div style="font-size: 16px">{!! isset($information['copy-right']) ?$information['copy-right'] : '© 2018 vn3c.net'  !!} <!-- © 2018 <a href="http://vn3c.net">vn3c.net</a>--></div>
    </div>
</section>

<div class="messengerHidden" id="hiddenShow">
    <div class="content">
        <div class=""> <i class="fa fa-times delete" aria-hidden="true"></i></div>
        <h5>Nhận thông tin về dự án</h5>
        <p>Tải đầy đủ mặt bằng căn hộ, bảng giá, hợp đồng mẫu, chính sách bán hàng mới nhất từ CĐT.</p>
        <form action="{{ route('sub_contact') }}" method="post">
             {!! csrf_field() !!}
            <div class="inner">
                <input type="text" name="name" placeholder="Họ tên" required>
            </div>
            <div class="inner">
                <input type="text" name="phone" placeholder="Số điện thoại" required>
            </div>
            <div class="inner">
                <input type="text" name="email" placeholder="Email" required>
            </div>

            <input type="hidden"  class="form-control" id="address" name="address" value="" placeholder="Address">
            <input type="hidden" class="form-control" id="address" name="Message" value="" placeholder="Address">

            <div class="inner">
               <button>Gửi</button>
            </div>
        </form>
    </div>
</div>


<div class="showEmail">
   <i class="fa fa-envelope-o" aria-hidden="true"></i>
</div>
<div class="showPhone">
   <a href="tel:{{ isset($information['hotline']) ? $information['hotline'] : '' }}" class="fancybox">
    <div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show" id="coccoc-alo-phoneIcon" style=""> 
            <div class="coccoc-alo-ph-circle"></div> 
            <div class="coccoc-alo-ph-circle-fill"></div> <div class="coccoc-alo-ph-img-circle"></div> 
        </div> {{ isset($information['hotline']) ? $information['hotline'] : '' }} </a>
</div>
<script type="text/javascript">
$( document ).ready(function() {
    var widthside = $( window ).width();
    if ( widthside <= 500) {
        $('.delete').click(function(){
           
            $('.messengerHidden').hide(500);
            $('.showEmail').show(500);
        });
        $('.showEmail').click(function(){
            $('.messengerHidden').show(500);
            $(this).hide(500);
        });
         setTimeout(function(){
          $('#hiddenShow').show();
          $('.showEmail').hide(500);
        }, 30000); 
    }
    else
    {
        setTimeout(function(){
          $('#hiddenShow').show();
        }, 30000); 
        $('.delete').click(function(){
           
            $('.messengerHidden').hide(500);
        });
        $('.showEmail').click(function(){
            $('.messengerHidden').toggle(500);
        });
    } 
});
</script>
<a id="toTop" href="#"></a>
<style type="text/css">
    .coccoc-alo-phone.coccoc-alo-show {
    visibility: visible;
}
.coccoc-alo-phone {
    position: fixed;
    visibility: hidden;
    background-color: transparent;
    width: 200px;
    height: 200px;
    cursor: pointer;
    z-index: 200000!important;
    -webkit-backface-visibility: hidden;
    -webkit-transform: translateZ(0);
    -webkit-transition: visibility .5s;
    -moz-transition: visibility .5s;
    -o-transition: visibility .5s;
    transition: visibility .5s;
}
.coccoc-alo-phone.coccoc-alo-green .coccoc-alo-ph-circle {
    border-color: #00aff2;
    border-color: #bfebfc 9;
    opacity: .5;
}
.coccoc-alo-ph-circle {
    width: 160px;
    height: 160px;
    top: 20px;
    left: 20px;
    position: absolute;
    background-color: transparent;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    border: 2px solid rgba(30,30,30,0.4);
    border: 2px solid #bfebfc 9;
    opacity: .1;
    -webkit-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -moz-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -ms-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -o-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
    -webkit-transform-origin: 50% 50%;
    -moz-transform-origin: 50% 50%;
    -ms-transform-origin: 50% 50%;
    -o-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
.coccoc-alo-phone.coccoc-alo-green .coccoc-alo-ph-circle-fill {
    background-color: rgba(0,165,233,0.9);
    background-color: #a6e3fa 9;
    opacity: .75!important;
}
.coccoc-alo-ph-circle-fill {
    width: 100px;
    height: 100px;
    top: 50px;
    left: 50px;
    position: absolute;
    background-color: #000;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    border: 2px solid transparent;
    opacity: .1;
    -webkit-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -moz-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -ms-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -o-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
    -webkit-transform-origin: 50% 50%;
    -moz-transform-origin: 50% 50%;
    -ms-transform-origin: 50% 50%;
    -o-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
.coccoc-alo-phone.coccoc-alo-green .coccoc-alo-ph-img-circle {
    background-color: #20ad5d;
}
.coccoc-alo-ph-img-circle {
    width: 60px;
    height: 60px;
    top: 70px;
    left: 70px;
    position: absolute;
    background:rgba(30,30,30,0.1) url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==") no-repeat center center;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    border: 2px solid transparent;
    opacity: .7;
    -webkit-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -moz-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -ms-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -o-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -webkit-transform-origin: 50% 50%;
    -moz-transform-origin: 50% 50%;
    -ms-transform-origin: 50% 50%;
    -o-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
.coccoc-alo-phone.coccoc-alo-green.coccoc-alo-hover .coccoc-alo-ph-img-circle, .coccoc-alo-phone.coccoc-alo-green:hover .coccoc-alo-ph-img-circle {
    background-color: #75eb50;
}
@-moz-keyframes coccoc-alo-circle-anim {
  0% {
    -moz-transform:rotate(0) scale(.5) skew(1deg);
    opacity:.1;
    -moz-opacity:.1;
    -webkit-opacity:.1;
    -o-opacity:.1;
  }
  30% {
    -moz-transform:rotate(0) scale(.7) skew(1deg);
    opacity:.5;
    -moz-opacity:.5;
    -webkit-opacity:.5;
    -o-opacity:.5;
  }
  100% {
    -moz-transform:rotate(0) scale(1) skew(1deg);
    opacity:.6;
    -moz-opacity:.6;
    -webkit-opacity:.6;
    -o-opacity:.1;
  }
}

@-webkit-keyframes coccoc-alo-circle-anim {
  0% {
    -webkit-transform:rotate(0) scale(.5) skew(1deg);
    -webkit-opacity:.1;
  }
  30% {
    -webkit-transform:rotate(0) scale(.7) skew(1deg);
    -webkit-opacity:.5;
  }
  100% {
    -webkit-transform:rotate(0) scale(1) skew(1deg);
    -webkit-opacity:.1;
  }
}

@-o-keyframes coccoc-alo-circle-anim {
  0% {
    -o-transform:rotate(0) kscale(.5) skew(1deg);
    -o-opacity:.1;
  }
  30% {
    -o-transform:rotate(0) scale(.7) skew(1deg);
    -o-opacity:.5;
  }
  100% {
    -o-transform:rotate(0) scale(1) skew(1deg);
    -o-opacity:.1;
  }
}

@-moz-keyframes coccoc-alo-circle-fill-anim {
  0% {
    -moz-transform:rotate(0) scale(.7) skew(1deg);
    opacity:.2;
  }
  50% {
    -moz-transform:rotate(0) -moz-scale(1) skew(1deg);
    opacity:.2;
  }
  100% {
    -moz-transform:rotate(0) scale(.7) skew(1deg);
    opacity:.2;
  }
}

@-webkit-keyframes coccoc-alo-circle-fill-anim {
  0% {
    -webkit-transform:rotate(0) scale(.7) skew(1deg);
    opacity:.2;
  }
  50% {
    -webkit-transform:rotate(0) scale(1) skew(1deg);
    opacity:.2;
  }
  100% {
    -webkit-transform:rotate(0) scale(.7) skew(1deg);
    opacity:.2;
  }
}

@-o-keyframes coccoc-alo-circle-fill-anim {
  0% {
    -o-transform:rotate(0) scale(.7) skew(1deg);
    opacity:.2;
  }
  50% {
    -o-transform:rotate(0) scale(1) skew(1deg);
    opacity:.2;
  }
  100% {
    -o-transform:rotate(0) scale(.7) skew(1deg);
    opacity:.2;
  }
}

@-moz-keyframes coccoc-alo-circle-img-anim {
  0% {
    transform:rotate(0) scale(1) skew(1deg);
  }
  10% {
    -moz-transform:rotate(-25deg) scale(1) skew(1deg);
  }
  20% {
    -moz-transform:rotate(25deg) scale(1) skew(1deg);
  }
  30% {
    -moz-transform:rotate(-25deg) scale(1) skew(1deg);
  }
  40% {
    -moz-transform:rotate(25deg) scale(1) skew(1deg);
  }
  50% {
    -moz-transform:rotate(0) scale(1) skew(1deg);
  }
  100% {
    -moz-transform:rotate(0) scale(1) skew(1deg);
  }
}

@-webkit-keyframes coccoc-alo-circle-img-anim {
  0% {
    -webkit-transform:rotate(0) scale(1) skew(1deg);
  }
  10% {
    -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
  }
  20% {
    -webkit-transform:rotate(25deg) scale(1) skew(1deg);
  }
  30% {
    -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
  }
  40% {
    -webkit-transform:rotate(25deg) scale(1) skew(1deg);
  }
  50% {
    -webkit-transform:rotate(0) scale(1) skew(1deg);
  }
  100% {
    -webkit-transform:rotate(0) scale(1) skew(1deg);
  }
}

@-o-keyframes coccoc-alo-circle-img-anim {
  0% {
    -o-transform:rotate(0) scale(1) skew(1deg);
  }
  10% {
    -o-transform:rotate(-25deg) scale(1) skew(1deg);
  }
  20% {
    -o-transform:rotate(25deg) scale(1) skew(1deg);
  }
  30% {
    -o-transform:rotate(-25deg) scale(1) skew(1deg);
  }
  40% {
    -o-transform:rotate(25deg) scale(1) skew(1deg);
  }
  50% {
    -o-transform:rotate(0) scale(1) skew(1deg);
  }
  100% {
    -o-transform:rotate(0) scale(1) skew(1deg);
  }
}
</style>