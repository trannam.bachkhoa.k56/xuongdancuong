@extends('news-01-1.layout.site')

@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title)
@section('meta_description',  !empty($category->meta_description) ? $category->meta_description : $category->description)
@section('keywords', $category->meta_keyword )
@section('content')

    <section class="viewNews wow fadeInUp" data-wow-offset="300" style="background: url('{!! isset($information['background-trang-chu']) ? $information['background-trang-chu'] : "" !!}') repeat">
        <div class="mask"></div>
       <!--  <div class="RelatedNews">
            <div class="container">
                <ul class="nav nav-tabs navTab" id="tabBar" role="tablist">
                    <li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">Tin mới nhất</a></li>
                    <li role="presentation"><a href="#tab2" aria-controls="home" role="tab" data-toggle="tab">Tin hot</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <div class="row">
                            <div class="listRelate">
                                @foreach(\App\Entity\Post::newPost($category->slug,5) as $newPost)
                                    <div class="col-md-3 col-sm-6 itemm">
                                        <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $newPost->slug]) }}">
                                            <img src="{{ isset($newPost->image) ? asset($newPost->image) : "" }}"/>
                                            <h3>{{ isset($newPost->title) ? $newPost->title : "" }}</h3>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab2">
                        <div class="row">
                            <div class="listRelate">
                                @foreach(\App\Entity\Post::categoryShow('tin-hot',4) as $hotPost)
                                    <div class="col-md-3 col-sm-6 itemm">
                                        <a href="{{ route('post', ['cate_slug' => 'tin-hot', 'post_slug' => $hotPost->slug]) }}">
                                            <img src="{{ isset($hotPost->image) ? asset($hotPost->image) : "" }}"/>
                                            <h3>{{ isset($hotPost->title) ? $hotPost->title : "" }}</h3>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="container">
            <div class="listNews">
                @foreach($posts as $post)
                <div class="itemList row wow fadeInUp" data-wow-delay="0.2s">
                    <div class="col-md-4 col-sm-4">
                        <a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" class="thumbs"><img src="{{ $post->image }}"/></a>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3 class="tl"><a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}">{{ $post->title }}</a></h3>
                        <div class="DateTime">
                            <?php $date=date_create($post->updated_at);?>
							{{ date_format($date,"d/m/Y H:i") }}
                        </div>
                        <div class="except tl">
                            {{ $post->description }}
                        </div>
                        <div class="more tl"><a class="BtnBlack" href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}">Đọc thêm</a></div>
                    </div>
                </div>
                @endforeach
                <div class="pagging">
                        {{ $posts->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection

