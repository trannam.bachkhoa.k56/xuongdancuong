@extends('news-01-1.layout.site')

@section('title','Liên hệ')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : "")

@section('content')
	<div class="container">
		<div class="row">
			@if(isset($success))
				<div class="col-md-12" style="margin:20px 0">
					 <p style="color: red; font-size: 18px; padding: 25px 0 40px 0;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
				</div>
			@endif
		</div>
	</div>
	<section class="contacForm" style="background: url('{!! isset($information['background-lien-he']) ? $information['background-lien-he'] : "" !!}') no-repeat">
		<div class="container contactContent">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<form action="{{ route('sub_contact') }}" method="post">
						{!! csrf_field() !!}
						<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="infoContact">
									<img src="{{ isset($information['logo-contact']) ? $information['logo-contact'] : '' }}"/>
									{!! isset($information['dia-chi-lien-he']) ? $information['dia-chi-lien-he'] : '' !!}	
								</div>
							</div>
							<div class="col-md-6 col-xs-12 boxForm">
								<div class="form">
									<div class="form-group">
										<input type="text" class="form-control" id="name" name="name" value="" placeholder="Họ và tên *" required>
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Số điện thoại *" required>
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="email" name="email" value="" placeholder="Email *" required>
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="address" name="address" value="" placeholder="Địa chỉ" required>
									</div>
									<div class="form-group">
										<textarea style="height: 134px;" class="form-control" id="Message" name="Message" value="" placeholder="Thông tin"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="tr col-md-12">
								<button type="submit" class="btnContact">Gửi</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	<!-- <section class="contacForm"> 
		<div class="container contactContent">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<form action="{{ route('sub_contact') }}" method="post">
						{!! csrf_field() !!}
						<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="infoContact">
									<img src="{{ isset($information['logo-contact']) ? $information['logo-contact'] : '' }}"/>
									{!! isset($information['dia-chi-lien-he']) ? $information['dia-chi-lien-he'] : '' !!}	
								</div>
							</div>
							<div class="col-md-6 col-xs-12 boxForm">
								<div class="form">
									<div class="form-group">
										<input type="text" class="form-control" id="name" name="name" value="" placeholder="Họ và tên *">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Số điện thoại *">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="email" name="email" value="" placeholder="Email *">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="address" name="address" value="" placeholder="Địa chỉ">
									</div>
									<div class="form-group">
										<textarea style="height: 134px;" class="form-control" id="Message" name="Message" value="" placeholder="Thông tin"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="tr col-md-12">
								<button type="submit" class="btnContact">Gửi</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section> -->
@endsection
