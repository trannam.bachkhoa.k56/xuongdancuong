<section class="slider">
	<div id="SlideHome" class="carousel slide carousel-fade" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			@foreach(\App\Entity\SubPost::showSubPost('slide', 8) as $id => $slide)
			<div class="item {{ ($id ==0 ) ? 'active' : '' }}">
				<a href="{{ $slide['duong-dan-slide'] }}">
					<img src="{{ asset($slide->image) }}" alt="{{ $slide->title }}" title="{{ $slide->title }}">
				</a>
			</div>
			@endforeach
			<!-- <div class="item">
                <img src="img/slide1.jpg" alt="...">
            </div> -->
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#SlideHome" role="button" data-slide="prev">
			<i class="fa fa-angle-left" aria-hidden="true"></i>
		</a>
		<a class="right carousel-control" href="#SlideHome" role="button" data-slide="next">
			<i class="fa fa-angle-right" aria-hidden="true"></i>
		</a>
	</div>

</section>
<script>
	/* Demo Scripts for Bootstrap Carousel and Animate.css article
	* on SitePoint by Maria Antonietta Perna
	*/
	(function( $ ) {

		//Function to animate slider captions 
		function doAnimations( elems ) {
			//Cache the animationend event in a variable
			var animEndEv = 'webkitAnimationEnd animationend';
			
			elems.each(function () {
				var $this = $(this),
					$animationType = $this.data('animation');
				$this.addClass($animationType).one(animEndEv, function () {
					$this.removeClass($animationType);
				});
			});
		}
		
		//Variables on page load 
		var $myCarousel = $('#SlideHome'),
			$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
			
		//Initialize carousel 
		$myCarousel.carousel();
		
		//Animate captions in first slide on page load 
		doAnimations($firstAnimatingElems);
		
		//Pause carousel  
		$myCarousel.carousel('pause');
		
		
		//Other slides to be animated on carousel slide event 
		$myCarousel.on('slide.bs.carousel', function (e) {
			var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
			doAnimations($animatingElems);
		});  
		
	})(jQuery);
</script>