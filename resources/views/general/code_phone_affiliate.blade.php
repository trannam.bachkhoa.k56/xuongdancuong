@extends('admin.layout.login')

@section('content')
    <div class="login-box">
    
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p style="text-align: center;" ><img src="{!! isset($information['logo']) ? $information['logo'] : '' !!}" style="width: 20px;"/></p>
            <h4 class="login-box-msg">Xác nhận số điện thoại của bạn để nhận mã giảm giá!</h4>
            <form class="form-horizontal" method="get" action="{!! $link !!}">
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="affiliate_contact" placeholder="Xác nhân số điện thoại!" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            Xác nhận
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    
@endsection
