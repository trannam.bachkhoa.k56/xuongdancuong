<div class="clockCountdown">
    <div class="contentDiscount">
        Khuyến Mãi
    </div>
    <div class="timeDiscount">
        <i class="far fa-clock"></i> Kết thúc trong <span id="demo"></span>
    </div>
</div>

<style>
  .contentDiscount {
    width: 35%;
    display: inline-block;
    font-size: 18px;
    font-weight: bold;
  }
  .timeDiscount {
    width: 64%;
    display: inline-block;
    text-align: right;
    font-size: 15px;
  }
  .clockCountdown {
    padding: 10px;
    stop(96%,#f32424));
    background-image: url('/site/image/background_discount.jpg'),linear-gradient(-90deg,#f0451e 9%,#f32424 96%);
    color: white;
    text-transform: uppercase;
    
  }
</style>
<script>

var countDownDate = new Date();
countDownDate.setHours(countDownDate.getHours() + 1);

var x = setInterval(function() {

  var now = new Date().getTime();
    
  var distance = countDownDate - now;
    
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  document.getElementById("demo").innerHTML = hours + ": "
  + minutes + ": " + seconds;
    
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>