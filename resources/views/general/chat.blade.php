<div class="Notification" id="popupVitural">
    <div class="Closed">X</div>
    <div class="Content">
        <h3>Phòng kinh doanh: {{ isset($information['hotline']) ? $information['hotline'] : '' }} </h3>
        <form action="{{ route('sub_contact') }}" method="post">
            {!! csrf_field() !!}
            <div class="contentChat">
            </div>
            
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Nhập tin nhắn ..." aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">Gửi</span>
                </div>
            </div>
        </form>
    </div>

</div>
<style>
    .Notification {
    }
    .Notification h3 {
        box-shadow: 1px 2px #888888;
        color: black;
        padding: 10px 5px;
    }
    #popupVitural {
        position: fixed;
        width: 400px;
        bottom: 20px;
        right: 10px;
        display: none;
        box-shadow: 0 2px 3px #d0d0d0;
        border: 1px solid #d0d0d0;
        background: white;
        padding: 10px;
        z-index: 9999999999999;
    }
    #popupVitural .Closed {
        position: absolute;
        right: -10px;
        background: #092b6e;
        border-radius: 100%;
        top: -11px;
        padding: 2px 6px;
    }
    @media (max-width: 769px)
    {
        #popupVitural {
            display: none;
        }
    }
    .contentChat {
        height: 300px;
        border: 1px solid #e5e5e5;
    }
</style>
<script>
    $(document).ready(function() {
        $('#popupVitural').hide();

        setInterval(function(){
            $('#popupVitural').show().slideDown();
        }, 1000);


        $('#popupVitural .Closed').click(function(){
            $('#popupVitural').slideUp().hide();
            $('#popupVitural').addClass('hide');
        });
    });
</script>