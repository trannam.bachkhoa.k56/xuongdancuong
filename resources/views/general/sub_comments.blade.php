<!-- Styles -->
<link rel="stylesheet" type="text/css" href="{{ asset('comments/css/jquery-comments.css') }}">
<!-- Libraries -->
<script type="text/javascript" src="{{ asset('comments/js/jquery.textcomplete.js') }}"></script>
<script type="text/javascript" src="/comments/js/jquery-comments.js"></script>

<?php $commentDb = new \App\Entity\Comment(); ?>
<?php
    $commentParents = $commentDb->getAllComment($post_id);
?>
{!! csrf_field() !!}
<!-- Dữ liệu comment truyền vào -->
<script type="text/javascript">
    var commentsArray = [
        @foreach ($commentParents as $commentParent)
        {
            "id": {!! $commentParent['comment_id'] !!},
            "parent": null,
            "created": "{{ $commentParent['created_at'] }}",
            "content": '{!! $commentParent['content'] !!}',
            "creator": {{ !empty($commentParent['user_id']) ? $commentParent['user_id'] : 0 }},
            "fullname": "{{ $commentParent['user_full_name'] }}",
            "profile_picture_url": "{{ empty($commentParent['user_image']) ? asset('comments/images/user-icon.png') : asset($commentParent['user_image']) }}"
        },
        @if(!empty($commentParent['childrenComments']))
            @foreach($commentParent['childrenComments'] as $child)
                {
                    "id": {!! $child->comment_id !!},
                    "parent": {!! $child->parent !!},
                    "created": "{{ $child->created_at }}",
                    "content": "{{ $child->content }}",
                    "creator": {{ !empty($child->user_id) ? $child->user_id : 0   }},
                    "fullname": "{{ $child->user_full_name }}",
                    "profile_picture_url": "{{ empty($child->user_image) ? asset('comments/images/user-icon.png') : asset($child->user_image) }}"
                },
            @endforeach
        @endif
        @endforeach
    ];

    var usersArray = [
        @foreach (\App\Entity\User::getUserComment() as $user)
        {
            id: {!! $user->id !!},
            fullname: "{{ $user->name }}",
            email: "{{ $user->email }}",
            profile_picture_url: "{{ empty($user->image) ? asset('comments/images/user-icon.png') : asset($user->image) }}"
        },
        @endforeach
    ];
</script>





<?php $userCheck = \Illuminate\Support\Facades\Auth::check() ?>

<!-- Init jquery-comments -->
<script type="text/javascript">
    $(function() {
        function isEmpty( el ){
            return !$.trim(el);
        }

        var saveComment = function(data) {
			console.log(data);
           
            $(data.pings).each(function(index, id) {
                var user = usersArray.filter(function(user){return user.id == id})[0];
                data.content = data.content.replace('@' + id, '@' + user.fullname);
            });

            $.ajax({
                type: "POST",
                url: '{!! route('comment') !!}',
                data: {
                    post_id: '{{ $post_id }}',
                    parent: isEmpty(data.parent) ? 0 : data.parent,
                    message: data.content,
                    comment_id: data.id,
					name: data.name,
					email: data.email,
					phone: data.phone,
                    _token:  $('input[name=_token]').val(),
                    user_id: '{{ $userCheck ? \Illuminate\Support\Facades\Auth::user()->id : 0 }}'
                },
                async: false,
                success: function(success) {
                    var obj = jQuery.parseJSON( success);
                    data.id = obj.comment_id;

                   return data;
                }
            });

            return data;
        };
		
	

        var deleteComment = function(data) {
            $.ajax({
                type: "GET",
                url: '{!! route('delete_comment') !!}',
                data: {
                    comment_id: data.id,
                },
                success: function(data){

                },

                error: function () {
                }
            });

            return data;
        };

        $('#commentsContainer').comments({
            profilePictureURL: '{{ $userCheck ? asset(\Illuminate\Support\Facades\Auth::user()->image) : asset('comments/images/user-icon.png') }}',
            currentUserId: '{{ $userCheck ? \Illuminate\Support\Facades\Auth::user()->id : 0 }}',
            roundProfilePictures: true,
            textareaRows: 1,
            enableAttachments: true,
            enableHashtags: true,
            enablePinging: true,
            user_has_upvoted: false,
            enableUpvoting: false,
            textareaPlaceholderText: 'Đánh giá sản phẩm của chúng tôi',
            newestText: 'Mới nhất',
            oldestText: 'Cũ hơn',
            popularText: 'Phổ biến',
            sendText: 'Bình luận',
            replyText: 'Trả lời',
            editText: 'Chỉnh sửa',
            saveText: 'Cập nhật',
            deleteText: 'Xóa',
            editedText: 'Đã chỉnh sửa',
            youText: "{!! $userCheck ? \Illuminate\Support\Facades\Auth::user()->name : 'Ẩn Danh' !!}",
            getUsers: function(success, error) {
                setTimeout(function() {
                    success(usersArray);
                }, 500);
            },
            getComments: function(success, error) {
                setTimeout(function() {
                    success(commentsArray);
                }, 500);
            },
            postComment: function(data, success, error) {
                setTimeout(function() {
                    success(saveComment(data));
                }, 500);
            },
            putComment: function(data, success, error) {
                setTimeout(function() {
                    success(saveComment(data));
                }, 500);
            },
            deleteComment: function(data, success, error) {
                setTimeout(function() {
                    success(deleteComment(data));
                }, 500);
            },
            upvoteComment: function(data, success, error) {
                setTimeout(function() {
                    success(data);
                }, 500);
            },
            uploadAttachments: function(dataArray, success, error) {
                 setTimeout(function() {
                    console.log(1);
                }, 500);
            },
        });
    });
</script>

<div id="commentsContainer"></div>