<section class="heading-news3">
	<div class="heading-news-box">
		<div class="owl-wrapper">
			<div class="owl-carousel" data-num="4">
<!--=== VÒNG LẶP SLIDER ===-->	
				@foreach(\App\Entity\Post::newPost('tin-tuc', 5) as $id => $new)
				<div class="item">
					<div class="news-post image-post2">
						<div class="post-gallery">
							<img src="{{ !empty($new->image) ?  asset($new->image) : asset('/site/img/no-image.png') }}"  alt="{{ isset($new['title']) ?  $new['title'] : '' }}">
							<div class="hover-box">
								<div class="inner-hover">
									<a class="category-post world" href="world.html">business</a>
									<h2><a href="single-post.html">{{ isset($new['title']) ?  $new['title'] : '' }}</a></h2>
									<ul class="post-tags">
										<li><i class="fa fa-clock-o"></i><?php $date=date_create($new->created_at); ?> {{ date_format($date,"d/m/Y H:i") }}</li>
										<li><i class="fa fa-user"></i>by <a href="#">{{ isset($new['user_email']) ?  $new['user_email'] : '' }}</a></li>
										<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
										<li><i class="fa fa-eye"></i>{{ $new->views }}</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
<!--=== KẾT THÚC VÒNG LẶP SLIDER ===-->
			</div>
		</div>
	</div>
</section>