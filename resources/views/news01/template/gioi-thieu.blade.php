@extends('news-01.layout.site')
@section('content')

    <section class="textBox About mgtop">
        <div class="mask"></div>
        <div class="container">
            <div class="infoText">
                <div class="cont">
                    <h2 class="title"><strong>{{ isset($post->title) ? $post->title : "" }}</strong><br>{{ isset($post->description) ? $post->description : "" }}<span></span></h2>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="ContentNews">
                        <div class="content">
                              {!! isset($information['content-gioi-thieu']) ? $information['content-gioi-thieu'] : "" !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="ContentNews">
                        {!! isset($information['video-gioi-thieu']) ? $information['video-gioi-thieu'] : "" !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection