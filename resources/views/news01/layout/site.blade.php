<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta name="ROBOTS" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="title" content="@yield('title')" />

    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />

    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="geo.position" content="10.763945;106.656201" />
	
	<!--=== NHÚNG CSS STYLE ==-->
    <link rel="icon" href="{{ isset($information['icon']) ? $information['icon'] : '' }} " type="image/x-icon" />
	<link rel="stylesheet" href="{{ asset('news01/css/bootstrap.min.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('news01/css/jquery.bxslider.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('news01/css/font-awesome.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('news01/css/magnific-popup.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('news01/css/owl.carousel.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('news01/css/owl.theme.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('news01/css/ticker-style.css') }}" media="all" type="text/css" />
	<link rel="stylesheet" href="{{ asset('news01/css/style.css') }}" media="all" type="text/css" />
	
	<!--=== NHÚNG FONT GOOGLE ==-->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic' rel='stylesheet' type='text/css'>

	<!--=== NHÚNG SCRIPT  ==-->
    <script src="{{ asset('news01/js/jquery.min.js') }}"></script>
    <script src="{{ asset('news01/js/jquery.migrate.js') }}"></script>
    <script src="{{ asset('news01/js/jquery.bxslider.min.js') }}"></script>
    <script src="{{ asset('news01/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('news01/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('news01/js/jquery.imagesloaded.min.js') }}"></script>
    <script src="{{ asset('news01/js/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('news01/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('news01/js/retina-1.1.0.min.js') }}"></script>
	<script src="{{ asset('news01/js/plugins-scroll.js') }}"></script>
	<script src="{{ asset('news01/js/script.js') }}"></script>
</head>
<body class="">
	<!--=== HEADER WEBSITE  ==-->
    @include('news01.common.header')

    <!--=== CONTENT WEBSITE  ==-->
    @yield('content')
	
	<!--=== FOOOTER WEBSITE  ==-->
    @include('news01.common.footer')
</body>
</html>
