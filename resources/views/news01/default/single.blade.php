@extends('news01.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )

@section('content')
<section class="block-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<!-- block content -->
				<div class="block-content">
					<!-- single-post box -->
					<div class="single-post-box">
						<div class="title-post">
							<h1>{{ $post->title }}</h1>
							<ul class="post-tags">
								<li><i class="fa fa-clock-o"></i><?php $date=date_create($post->created_at); ?> {{ date_format($date,"d/m/Y H:i") }}</li>
								<li><i class="fa fa-user"></i>bởi <a href="#">{{ isset($post['user_email']) ?  $post['user_email'] : '' }}</a></li>
								<li><i class="fa fa-eye"></i>{{ $post->views }}</li>
							</ul>
						</div>

	<!--				<div class="share-post-box">
							<ul class="share-box">
								<li><i class="fa fa-share-alt"></i><span>Share Post</span></li>
								<li><a class="facebook" href="#"><i class="fa fa-facebook"></i><span>Share on Facebook</span></a></li>
								<li><a class="twitter" href="#"><i class="fa fa-twitter"></i><span>Share on Twitter</span></a></li>
								<li><a class="google" href="#"><i class="fa fa-google-plus"></i><span></span></a></li>
								<li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i><span></span></a></li>
							</ul>
						</div>       -->
						<div class="post-content">
							<?=  isset($post->content ) ?$post->content  : 'đang cập nhật tin tức'?>
						</div>
						<div class="article-inpost">
							<div class="row">
								<div class="col-md-6">
									<div class="image-content">
										<div class="image-place">
											<img src="upload/news-posts/single-art.jpg" alt="">
											<div class="hover-image">
												<a class="zoom" href="upload/news-posts/single-art.jpg">
													<i class="fa fa-arrows-alt"></i>
												</a>
											</div>
										</div>
										<span class="image-caption">Cras eget sem nec dui volutpat ultrices.</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="text-content">
										<h2>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. </h2>
										<p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscing risus a sem. Nullam quis massa sit amet nibh viverra malesuada. </p>
										<p>Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu. Ut scelerisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. </p>
										<p>Nunc iaculis mi in ante. Vivamus nibh feugiat est.</p>
									</div>
								</div>
							</div>
						</div>

						<div class="post-tags-box">
							<ul class="tags-box">
								<li><i class="fa fa-tags"></i><span>Tags:</span></li>
								<li><a href="#">News</a></li>
								<li><a href="#">Fashion</a></li>
								<li><a href="#">Politics</a></li>
								<li><a href="#">Sport</a></li>
							</ul>
						</div>
					</div>
					<!-- End single-post box -->
				</div>
				<!-- End block content -->
			</div>
			<div class="col-sm-4">

				<!-- sidebar -->
				<div class="sidebar">

					<div class="widget social-widget">
						<div class="title-section">
							<h1><span>Stay Connected</span></h1>
						</div>
						<ul class="social-share">
							<li>
								<a href="#" class="rss"><i class="fa fa-rss"></i></a>
								<span class="number">9,455</span>
								<span>Subscribers</span>
							</li>
							<li>
								<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
								<span class="number">56,743</span>
								<span>Fans</span>
							</li>
							<li>
								<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
								<span class="number">43,501</span>
								<span>Followers</span>
							</li>
							<li>
								<a href="#" class="google"><i class="fa fa-google-plus"></i></a>
								<span class="number">35,003</span>
								<span>Followers</span>
							</li>
						</ul>
					</div>

					<div class="widget features-slide-widget">
						<div class="title-section">
							<h1><span>Featured Posts</span></h1>
						</div>
						<div class="image-post-slider">
							<ul class="bxslider">
								<li>
									<div class="news-post image-post2">
										<div class="post-gallery">
											<img src="upload/news-posts/im3.jpg" alt="">
											<div class="hover-box">
												<div class="inner-hover">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
														<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
														<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
														<li><i class="fa fa-eye"></i>872</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="news-post image-post2">
										<div class="post-gallery">
											<img src="upload/news-posts/im1.jpg" alt="">
											<div class="hover-box">
												<div class="inner-hover">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
														<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
														<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
														<li><i class="fa fa-eye"></i>872</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="news-post image-post2">
										<div class="post-gallery">
											<img src="upload/news-posts/im2.jpg" alt="">
											<div class="hover-box">
												<div class="inner-hover">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
														<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
														<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
														<li><i class="fa fa-eye"></i>872</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="widget tab-posts-widget">

						<ul class="nav nav-tabs" id="myTab">
							<li class="active">
								<a href="#option1" data-toggle="tab">Popular</a>
							</li>
							<li>
								<a href="#option2" data-toggle="tab">Recent</a>
							</li>
							<li>
								<a href="#option3" data-toggle="tab">Top Reviews</a>
							</li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active" id="option1">
								<ul class="list-posts">
									<li>
										<img src="upload/news-posts/listw1.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw2.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Sed arcu. Cras consequat. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw3.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus.  </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw4.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw5.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>
								</ul>
							</div>
							<div class="tab-pane" id="option2">
								<ul class="list-posts">

									<li>
										<img src="upload/news-posts/listw3.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw4.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw5.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>
									<li>
										<img src="upload/news-posts/listw1.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw2.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Sed arcu. Cras consequat.</a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>
								</ul>										
							</div>
							<div class="tab-pane" id="option3">
								<ul class="list-posts">

									<li>
										<img src="upload/news-posts/listw4.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw1.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw3.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus.  </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw2.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Sed arcu. Cras consequat.</a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw5.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>
								</ul>										
							</div>
						</div>
					</div>

					<div class="widget post-widget">
						<div class="title-section">
							<h1><span>Featured Video</span></h1>
						</div>
						<div class="news-post video-post">
							<img alt="" src="upload/news-posts/video-sidebar.jpg">
							<a href="https://www.youtube.com/watch?v=LL59es7iy8Q" class="video-link"><i class="fa fa-play-circle-o"></i></a>
							<div class="hover-box">
								<h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. </a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
								</ul>
							</div>
						</div>
						<p>Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede. Donec nec justo eget felis facilisis. </p>
					</div>

					<div class="widget subscribe-widget">
						<form class="subscribe-form">
							<h1>Subscribe to RSS Feeds</h1>
							<input type="text" name="sumbscribe" id="subscribe" placeholder="Email"/>
							<button id="submit-subscribe">
								<i class="fa fa-arrow-circle-right"></i>
							</button>
							<p>Get all latest content delivered to your email a few times a month.</p>
						</form>
					</div>

					<div class="widget tags-widget">

						<div class="title-section">
							<h1><span>Popular Tags</span></h1>
						</div>

						<ul class="tag-list">
							<li><a href="#">News</a></li>
							<li><a href="#">Fashion</a></li>
							<li><a href="#">Politics</a></li>
							<li><a href="#">Sport</a></li>
							<li><a href="#">Food</a></li>
							<li><a href="#">Videos</a></li>
							<li><a href="#">Business</a></li>
							<li><a href="#">Travel</a></li>
							<li><a href="#">World</a></li>
							<li><a href="#">Music</a></li>
						</ul>

					</div>

					<div class="advertisement">
						<div class="desktop-advert">
							<span>Advertisement</span>
							<img src="upload/addsense/300x250.jpg" alt="">
						</div>
						<div class="tablet-advert">
							<span>Advertisement</span>
							<img src="upload/addsense/200x200.jpg" alt="">
						</div>
						<div class="mobile-advert">
							<span>Advertisement</span>
							<img src="upload/addsense/300x250.jpg" alt="">
						</div>
					</div>

				</div>
				<!-- End sidebar -->

			</div>

		</div>

	</div>
</section>
    
@endsection

