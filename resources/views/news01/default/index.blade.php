@extends('news01.layout.site')
@section('title', isset($information['tieu-de-web']) ? $information['tieu-de-web'] : "")
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : "")
@section('content')
<!--=== Slide Top Header ===-->
<div class="list-line-posts">
	<div class="container">
		<div class="owl-wrapper">
			<div class="owl-carousel" data-num="4">
			
<!--=== VÒNG LẶP Slide Top Header - TIN TỨC MỚI===-->
				@foreach(\App\Entity\Post::newPost('tin-tuc', 5) as $id => $new)
				<div class="item list-post">
					<img src="{{ !empty($new->image) ?  asset($new->image) : asset('/site/img/no-image.png') }}" alt="{{ isset($new['title']) ?  $new['title'] : '' }}">
					<div class="post-content">
						<a href="{{ route('post', ['cate_slug' => 'tin-tuc-noi-bat', 'post_slug' => $new->slug]) }}">{{ isset($new['title']) ?  $new['title'] : '' }}</a>
						<h2><a href="single-post.html">{{ isset($new['description']) ?  $new['description'] : '' }}</a></h2>
						<ul class="post-tags">
							<li>
							<i class="fa fa-clock-o"></i><?php $date=date_create($new->created_at); ?> {{ date_format($date,"d/m/Y H:i") }}</li>
						</ul>
					</div>
				</div>
				@endforeach
 <!--=== KẾT THÚC VÒNG LẶP Slide Top Header - TIN TỨC MỚI===-->	
			</div>
		</div>

	</div>
</div>
		<!-- End list line posts section -->

@include('news01.partials.slider')
		<!-- heading-news-section
			================================================== -->
		<!-- End heading-news-section -->

		<!-- features-today-section
			================================================== -->
		<section class="features-today second-style">
			<div class="container">

				<div class="title-section">
					<h1><span>Today's Featured</span></h1>
				</div>

				<div class="features-today-box owl-wrapper">
					<div class="owl-carousel" data-num="4">
					
						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st1.jpg" alt="">
								<a class="category-post world" href="world.html">Music</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>

						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st2.jpg" alt="">
								<a class="category-post sport" href="sport.html">Sport</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>

						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st3.jpg" alt="">
								<a class="category-post food" href="food.html">Food &amp; Health</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>

						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st4.jpg" alt="">
								<a class="category-post sport" href="sport.html">Sport</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. </a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>

						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st1.jpg" alt="">
								<a class="category-post travel" href="travel.html">Travel</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>
					
						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st1.jpg" alt="">
								<a class="category-post world" href="world.html">Music</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>

						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st2.jpg" alt="">
								<a class="category-post sport" href="sport.html">Sport</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>

						<div class="item news-post standard-post">
							<div class="post-gallery">
								<img src="upload/news-posts/st3.jpg" alt="">
								<a class="category-post food" href="food.html">Food &amp; Health</a>
							</div>
							<div class="post-content">
								<h2><a href="single-post.html">Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</a></h2>
								<ul class="post-tags">
									<li><i class="fa fa-clock-o"></i>27 may 2013</li>
									<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
									<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- End features-today-section -->

		<!-- block-wrapper-section
			================================================== -->
		<section class="block-wrapper">
			<div class="container">
				<div class="row">







				

					<div class="col-md-9 col-sm-8">

						<!-- block content -->
						<div class="block-content">

							<!-- grid box -->
							<div class="grid-box owl-wrapper">

								<div class="title-section">
									<h1>
										<span>
										{{ isset($catePost['title']) ?  $catePost['title'] : '' }}
										</span>
									</h1>
								</div>

								<div class="row">
								@foreach(\App\Entity\Post::newPost('tin-tuc', 5) as $id => $new)
									<div class="col-md-6">
										<div class="news-post image-post2">
											<div class="post-gallery">
												<img src="{{ !empty($new->image) ?  asset($new->image) : asset('/site/img/no-image.png') }}" alt="{{ isset($new['title']) ?  $new['title'] : '' }}">
												<div class="hover-box">
													<div class="inner-hover">
														<a class="category-post travel" href="travel.html">travel</a>
														<h2><a href="single-post.html">{{ isset($new['title']) ?  $new['title'] : '' }}</a></h2>
														<ul class="post-tags">
															<li><i class="fa fa-clock-o"></i><?php $date=date_create($new->created_at); ?> {{ date_format($date,"d/m/Y H:i") }}</li>
															<li><a href="#"><i class="fa fa-comments-o"></i><span>{{ $new->views }}</span></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>

										<ul class="list-posts">
											<li>
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>							
									</div>
								@endforeach
								</div>

								<div class="center-button">
									<a href="#"><i class="fa fa-refresh"></i> More from featured</a>
								</div>

							</div>
							<!-- End grid box -->

							<!-- slider-caption-box -->
							<div class="slider-caption-box">
								<div class="slider-holder">
									<ul class="slider-call">
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/art7.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<a class="category-post fashion" href="fashion.html">fashion</a>
															<h2><a href="single-post.html">Donec nec justo eget felis facilisis fermentum. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/art5.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<a class="category-post world" href="world.html">world</a>
															<h2><a href="single-post.html">Suspendisse urna nibh, viverra non, semper </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/art6.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<a class="category-post world" href="world.html">Design</a>
															<h2><a href="single-post.html">Aliquam porttitor mauris sit amet orci. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/art8.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<a class="category-post travel" href="travel.html">travel</a>
															<h2><a href="single-post.html">Aenean dignissim pellentesque felis.</a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/art9.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<a class="category-post world" href="world.html">world</a>
															<h2><a href="single-post.html">Morbi in sem quis dui placerat ornare.</a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div id="bx-pager">
									<a data-slide-index="0" href="">
										Donec nec justo eget felis facilisis fermentum. 
									</a>
									<a data-slide-index="1" href="">
										Suspendisse urna nibh, viverra non, semper 
									</a>
									<a data-slide-index="2" href="">
										Aliquam porttitor mauris sit amet orci. 
									</a>
									<a data-slide-index="3" href="">
										Aenean dignissim pellentesque felis.
									</a>
									<a data-slide-index="4" href="">
										Morbi in sem quis dui placerat ornare. 
									</a>
								</div>
							</div>
							<!-- End slider-caption-box -->

							<!-- grid box -->
							<div class="grid-box">

								<div class="row">
									<div class="col-md-6">

										<div class="title-section">
											<h1><span>Technology</span></h1>
										</div>

										<ul class="list-posts">
											<li>
												<img src="upload/news-posts/list1.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/list2.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/list3.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
											<li>
												<img src="upload/news-posts/list1.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>

									<div class="col-md-6">

										<div class="title-section">
											<h1><span>Business</span></h1>
										</div>

										<ul class="list-posts">
											<li>
												<img src="upload/news-posts/list4.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/list5.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/list6.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
											<li>
												<img src="upload/news-posts/list4.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>

								</div>

							</div>
							<!-- End grid box -->

							<!-- google addsense -->
							<div class="advertisement">
								<div class="desktop-advert">
									<span>Advertisement</span>
									<img src="upload/addsense/600x80.jpg" alt="">
								</div>
								<div class="tablet-advert">
									<span>Advertisement</span>
									<img src="upload/addsense/468x60-white.jpg" alt="">
								</div>
								<div class="mobile-advert">
									<span>Advertisement</span>
									<img src="upload/addsense/300x250.jpg" alt="">
								</div>
							</div>
							<!-- End google addsense -->

							<!-- galery box -->
							<div class="galery-box">

								<div class="title-section">
									<h1><span>Gallery</span></h1>
								</div>

								<ul class="slider-call2">
									<li><img src="upload/news-posts/im-large9.jpg" alt=""/></li>
									<li><img src="upload/news-posts/im-large8.jpg" alt=""/></li>
									<li><img src="upload/news-posts/im-large4.jpg" alt=""/></li>
									<li><img src="upload/news-posts/im-large1.jpg" alt=""/></li>
									<li><img src="upload/news-posts/im-large6.jpg" alt=""/></li>
									<li><img src="upload/news-posts/im-large5.jpg" alt=""/></li>
									<li><img src="upload/news-posts/im-large3.jpg" alt=""/></li>
								</ul>
								<div id="bx-pager2">
									<a data-slide-index="0" href=""><img src="upload/news-posts/im-thumb9.jpg" alt=""/></a>
									<a data-slide-index="1" href=""><img src="upload/news-posts/im-thumb8.jpg" alt=""/></a>
									<a data-slide-index="2" href=""><img src="upload/news-posts/im-thumb4.jpg" alt=""/></a>
									<a data-slide-index="3" href=""><img src="upload/news-posts/im-thumb1.jpg" alt=""/></a>
									<a data-slide-index="4" href=""><img src="upload/news-posts/im-thumb6.jpg" alt=""/></a>
									<a data-slide-index="5" href=""><img src="upload/news-posts/im-thumb5.jpg" alt=""/></a>
									<a data-slide-index="6" href=""><img src="upload/news-posts/im-thumb3.jpg" alt=""/></a>
								</div>
							</div>
							<!-- End galery box -->

							<!-- article box -->
							<div class="article-box">
								<div class="title-section">
									<h1><span>Latest Articles</span></h1>
								</div>

								<div class="news-post article-post">
									<div class="row">
										<div class="col-sm-4">
											<div class="post-gallery">
												<img alt="" src="upload/news-posts/art1.jpg">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="post-content">
												<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
													<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
													<li><i class="fa fa-eye"></i>872</li>
												</ul>
												<span class="post-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</span>
												<p>Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
											</div>
										</div>
									</div>
								</div>

								<div class="news-post article-post">
									<div class="row">
										<div class="col-sm-4">
											<div class="post-gallery">
												<img alt="" src="upload/news-posts/art2.jpg">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="post-content">
												<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
													<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
													<li><i class="fa fa-eye"></i>872</li>
												</ul>
												<span class="post-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</span>
												<p>Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
											</div>
										</div>
									</div>
								</div>

								<div class="news-post article-post">
									<div class="row">
										<div class="col-sm-4">
											<div class="post-gallery">
												<img alt="" src="upload/news-posts/art3.jpg">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="post-content">
												<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
													<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
													<li><i class="fa fa-eye"></i>872</li>
												</ul>
												<span class="post-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</span>
												<p>Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
											</div>
										</div>
									</div>
								</div>

								<div class="news-post article-post">
									<div class="row">
										<div class="col-sm-4">
											<div class="post-gallery">
												<img alt="" src="upload/news-posts/art4.jpg">
											</div>
										</div>
										<div class="col-sm-8">
											<div class="post-content">
												<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</a></h2>
												<ul class="post-tags">
													<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													<li><i class="fa fa-user"></i>by <a href="#">John Doe</a></li>
													<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
													<li><i class="fa fa-eye"></i>872</li>
												</ul>
												<span class="post-rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
												</span>
												<p>Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
											</div>
										</div>
									</div>
								</div>

							</div>
							<!-- End article box -->

							<!-- Pagination box -->
							<div class="pagination-box">
								<ul class="pagination-list">
									<li><a class="active" href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><span>...</span></li>
									<li><a href="#">9</a></li>
									<li><a href="#">Next</a></li>
								</ul>
								<p>Page 1 of 9</p>
							</div>
							<!-- End Pagination box -->

						</div>
						<!-- End block content -->

					</div>














					<div class="col-md-3 col-sm-4">

						<!-- sidebar -->
						<div class="sidebar large-sidebar">

							<div class="widget search-widget">
								<form role="search" class="search-form">
									<input type="text" id="search" name="search" placeholder="Search here">
									<button type="submit" id="search-submit"><i class="fa fa-search"></i></button>
								</form>
							</div>

							<div class="widget social-widget">
								<div class="title-section">
									<h1><span>Stay Connected</span></h1>
								</div>
								<ul class="social-share">
									<li>
										<a href="#" class="rss"><i class="fa fa-rss"></i></a>
										<span class="number">9,455</span>
										<span>Subscribers</span>
									</li>
									<li>
										<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
										<span class="number">56,743</span>
										<span>Fans</span>
									</li>
									<li>
										<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
										<span class="number">43,501</span>
										<span>Followers</span>
									</li>
									<li>
										<a href="#" class="google"><i class="fa fa-google-plus"></i></a>
										<span class="number">35,003</span>
										<span>Followers</span>
									</li>
								</ul>
							</div>

							<div class="widget features-slide-widget">
								<div class="title-section">
									<h1><span>Featured Posts</span></h1>
								</div>
								<div class="image-post-slider">
									<ul class="bxslider">
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/im3.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/im1.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="news-post image-post2">
												<div class="post-gallery">
													<img src="upload/news-posts/im2.jpg" alt="">
													<div class="hover-box">
														<div class="inner-hover">
															<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
															<ul class="post-tags">
																<li><i class="fa fa-clock-o"></i>27 may 2013</li>
																<li><a href="#"><i class="fa fa-comments-o"></i><span>23</span></a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
								<ul class="list-posts">

									<li>
										<img src="upload/news-posts/listw5.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>
									<li>
										<img src="upload/news-posts/listw1.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>

									<li>
										<img src="upload/news-posts/listw2.jpg" alt="">
										<div class="post-content">
											<h2><a href="single-post.html">Sed arcu. Cras consequat.</a></h2>
											<ul class="post-tags">
												<li><i class="fa fa-clock-o"></i>27 may 2013</li>
											</ul>
										</div>
									</li>
								</ul>
							</div>

							<div class="advertisement">
								<div class="desktop-advert">
									<span>Advertisement</span>
									<img src="upload/addsense/250x250.jpg" alt="">
								</div>
								<div class="tablet-advert">
									<span>Advertisement</span>
									<img src="upload/addsense/200x200.jpg" alt="">
								</div>
								<div class="mobile-advert">
									<span>Advertisement</span>
									<img src="upload/addsense/300x250.jpg" alt="">
								</div>
							</div>

							<div class="widget tab-posts-widget">

								<ul class="nav nav-tabs" id="myTab">
									<li class="active">
										<a href="#option1" data-toggle="tab">Popular</a>
									</li>
									<li>
										<a href="#option2" data-toggle="tab">Recent</a>
									</li>
								</ul>

								<div class="tab-content">
									<div class="tab-pane active" id="option1">
										<ul class="list-posts">
											<li>
												<img src="upload/news-posts/listw1.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/listw2.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Sed arcu. Cras consequat. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/listw3.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus.  </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/listw4.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/listw5.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
									<div class="tab-pane" id="option2">
										<ul class="list-posts">

											<li>
												<img src="upload/news-posts/listw3.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Phasellus ultrices nulla quis nibh. Quisque a lectus. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/listw4.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Donec consectetuer ligula vulputate sem tristique cursus. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/listw5.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
											<li>
												<img src="upload/news-posts/listw1.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. </a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>

											<li>
												<img src="upload/news-posts/listw2.jpg" alt="">
												<div class="post-content">
													<h2><a href="single-post.html">Sed arcu. Cras consequat.</a></h2>
													<ul class="post-tags">
														<li><i class="fa fa-clock-o"></i>27 may 2013</li>
													</ul>
												</div>
											</li>
										</ul>										
									</div>
								</div>
							</div>

							<div class="widget post-widget">
								<div class="title-section">
									<h1><span>Featured Video</span></h1>
								</div>
								<div class="news-post video-post">
									<img alt="" src="upload/news-posts/video-sidebar.jpg">
									<a href="https://www.youtube.com/watch?v=LL59es7iy8Q" class="video-link"><i class="fa fa-play-circle-o"></i></a>
									<div class="hover-box">
										<h2><a href="single-post.html">Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. </a></h2>
										<ul class="post-tags">
											<li><i class="fa fa-clock-o"></i>27 may 2013</li>
										</ul>
									</div>
									<p></p>
								</div>
								<p>Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede. Donec nec justo eget felis facilisis. </p>
							</div>

						</div>
						<!-- End sidebar -->

					</div>

				</div>

			</div>
		</section>
@endsection