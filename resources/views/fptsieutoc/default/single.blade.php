@extends('fptsieutoc.layout.site')

@section('type_meta', 'website')
@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')

  <div id="main" class="clearfix">
	<div class="inner-wrap clearfix">

		<div id="primary">
			<div id="content" class="clearfix">

				<article id="post-45" class="post-45 post type-post status-publish format-standard has-post-thumbnail hentry category-khong-phan-loai category-lap-mang-fpt-ha-noi">

					<div class="featured-image">
						<img width="800" height="445" src="{{$post->image}}" class="attachment-colormag-featured-image size-colormag-featured-image wp-post-image" alt="" srcset="{{$post->image}} 800w, {{$post->image}} 300w, {{$post->image}} 768w" sizes="(max-width: 800px) 100vw, 800px" /> </div>

					<div class="article-content clearfix">
						
						<header class="entry-header">
							<h1 class="entry-title">
						{{$post->title}} 
						</h1>
						</header>

						<div class="below-entry-meta">
							<span class="posted-on"><a href="#" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{date_format($post->updated_at, 'd/m/Y')}}">{{date_format($post->updated_at, 'd/m/Y')}}</time><time class="updated" datetime="{{date_format($post->updated_at, 'd/m/Y')}}">{{date_format($post->updated_at, 'd/m/Y')}}</time></a></span>
							<span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="#"title="admin">admin</a></span></span>

							<!-- <span class="comments"><a href="lap-truyen-hinh-fpt/#comments"><i class="fa fa-comments"></i> 4 Comments</a></span> -->
						</div>
						<div class="entry-content clearfix">
							<div class="kk-star-ratings top-left lft" data-id="546">
								<div class="kksr-legend">
									<h1>{{$post->title}}</h1>
								</div>
								{!! $post->content !!}
							</div>
							<div class="optin" style="padding: -36px -43px">
								{!! $post['optinform-dang-ky'] !!}
							</div>
						</div>    
					</div>

				</article>

			</div>
		

			<div id="comments" class="comments-area">
				<?php 
					$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";                        
				 ?>
				<h3 class="comments-title">
				 <span>{{$post->title}}</span>&rdquo;		</h3>

				<div id="respond" class="comment-respond">
					<div class="comment">
						<div class="fb-comments" data-href="{{$actual_link}}" data-width="" data-numposts="5" width="100%"></div>
					</div>
				</div>
			</div>
			<!-- #comments -->
		</div>
		<!-- #primary -->

	   @include('fptsieutoc.common.sider_bar')
	</div>
	<!-- .inner-wrap -->
</div>

@endsection


