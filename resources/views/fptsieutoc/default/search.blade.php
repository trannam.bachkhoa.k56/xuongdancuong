@extends('fptsieutoc.layout.site')


@section('title', 'tim-kiem')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', '')

@section('content')
	
 <div id="main" class="clearfix">
            <div class="inner-wrap clearfix">

                <div id="primary">
                    <div id="content" class="clearfix">

                        <header class="page-header">
                            <h1 class="page-title"><span>Tìm kiếm từ khóa : {{$word}}</span></h1> </header>
                        <!-- .page-header -->

                        <div class="article-container">
                        	@foreach ($products as $id => $product)
                            <article id="post-428" class="post-428 post type-post status-publish format-standard has-post-thumbnail hentry category-lap-mang-fpt-ha-noi">

                                <div class="featured-image">
                                    <a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}" title="{{$product->title}}">
                                    	<img width="600" height="346" src="{{$product->image}}" class="attachment-colormag-featured-image size-colormag-featured-image wp-post-image" alt="" srcset="{{$product->image}} 600w, {{$product->image}} 300w" sizes="(max-width: 600px) 100vw, 600px" /></a>
                                </div>

                                <div class="article-content clearfix">

                                    <header class="entry-header">
                                        <h2 class="entry-title">
							            <a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}">{{$product->title}}</a>
							         </h2>
                                    </header>

                                    <div class="below-entry-meta">
                                        <span class="posted-on"><a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}" title="4:28 sáng" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{date_format($product->updated_at, "d/m/Y")}}">{{date_format($product->updated_at, "d/m/Y")}}</time><time class="updated" datetime="{{date_format($product->updated_at, "d/m/Y")}}">{{date_format($product->updated_at, "d/m/Y")}}</time></a></span>
                                        <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n"
                                             href="#"
                                             title="admin">admin</a></span></span>

                                        <span class="comments"><a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}"><i class="fa fa-comment"></i> 0 Comments</a></span>
                                    </div>
                                    <div class="entry-content clearfix">
                                        <p>"{{$product->description}}</p>
                                        <a class="more-link" title="{{$product->title}}" href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}"><span>Read more</span></a>
                                    </div>

                                </div>

                            </article>
                            @endforeach
                          
                        </div>

                      <!--   <ul class="default-wp-page clearfix">
                            <li class="previous"><a href="category/lap-mang-fpt-ha-noi/page/2/">&larr; Previous</a></li>
                            <li class="next"></li>
                        </ul> -->

                    </div>
                    <!-- #content -->
                </div>
                <!-- #primary -->

               @include('fptsieutoc.common.sider_bar')

            </div>
            <!-- .inner-wrap -->
        </div>
        <!-- #main -->

    
@endsection
