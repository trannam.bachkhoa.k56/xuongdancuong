@extends('fptsieutoc.layout.site')

@section('type_meta', 'article')
@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', asset($product->image) )
@section('meta_url', route('product', [ 'post_slug' => $product->slug]) )

@section('content')
     <div id="main" class="clearfix">
            <div class="inner-wrap clearfix">

                <div id="primary">
                    <div id="content" class="clearfix">

                        <article id="post-45" class="post-45 post type-post status-publish format-standard has-post-thumbnail hentry category-khong-phan-loai category-lap-mang-fpt-ha-noi">

                            <div class="featured-image">
                                <img width="800" height="445" src="{{$product->image}}" class="attachment-colormag-featured-image size-colormag-featured-image wp-post-image" alt="" srcset="{{$product->image}} 800w, {{$product->image}} 300w, {{$product->image}} 768w" sizes="(max-width: 800px) 100vw, 800px" /> </div>

                            <div class="article-content clearfix">

                                <div class="above-entry-meta"><span class="cat-links"><a href="#"  rel="category tag">Chưa được phân loại</a>&nbsp;<a href="#"  rel="category tag">Lắp Mạng FPT Hà Nội</a>&nbsp;</span></div>
								
                                <header class="entry-header">
                                    <h1 class="entry-title">
   								{{$product->title}} 
                                </h1>
                                </header>

                                <div class="below-entry-meta">
                                    <span class="posted-on"><a href="#" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{date_format($product->updated_at, 'd/m/Y')}}">{{date_format($product->updated_at, 'd/m/Y')}}</time><time class="updated" datetime="{{date_format($product->updated_at, 'd/m/Y')}}">{{date_format($product->updated_at, 'd/m/Y')}}</time></a></span>
                                    <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="#"title="admin">admin</a></span></span>

                                    <!-- <span class="comments"><a href="lap-truyen-hinh-fpt/#comments"><i class="fa fa-comments"></i> 4 Comments</a></span> -->
                                </div>
                                <div class="entry-content clearfix">
                                    <div class="kk-star-ratings top-left lft" data-id="546">
                                        <div class="kksr-legend">
                                            <h1>{{$product->title}}</h1>
                                        </div>
                                        {!! $product->content !!}
                                    </div>
                                </div>    
                            </div>

                        </article>

                    </div>
                    <!-- #content -->

                   <!--  <ul class="default-wp-page clearfix">
                        <li class="previous"><a href="fpt-play-box-2018/" rel="prev"><span class="meta-nav">&larr;</span> FPT Play Box 2018</a></li>
                        <li class="next"><a href="lap-mang-cap-quang-fpt/" rel="next">Lắp Mạng Cáp Quang FPT <span class="meta-nav">&rarr;</span></a></li>
                    </ul> -->

                    <h4 class="related-posts-main-title">
                        <i class="fa fa-thumbs-up"></i><span>Có thể bạn quan tâm</span>
                    </h4>

                    <div class="related-posts clearfix">
                        @foreach(App\Entity\Product::relativeProduct($product->slug, $product->product_id, 3) as $productRelative)
                        <div class="single-related-posts">

                            <div class="related-posts-thumbnail">
                                <a href="{{$productRelative->slug}}" title="{{$productRelative->title}}">
                                    <img width="390" height="205" src="{{$productRelative->image}}" class="attachment-colormag-featured-post-medium size-colormag-featured-post-medium wp-post-image" alt="" /> </a>
                            </div>

                            <div class="article-content">

                                <h3 class="entry-title">
                                    <a href="{{$productRelative->slug}}" rel="bookmark" title="{{$productRelative->title}}">{{$productRelative->title}}</a>
                                 </h3>
                                <!--/.post-title-->

                                <div class="below-entry-meta">
                                    <span class="posted-on"><a href="{{$productRelative->slug}}" title="{{date_format($productRelative->updated_at, 'd/m/Y')}}" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{date_format($productRelative->updated_at, 'd/m/Y')}}">{{date_format($productRelative->updated_at, 'd/m/Y')}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$productRelative->slug}}" title="admin">admin</a></span></span>
                                    <span class="comments"><i class="fa fa-comment"></i><a href="{{$productRelative->slug}}">0</a></span>
                                </div>

                            </div>

                        </div>
                        @endforeach
                        <!--/.related-->
                      
                       
                    </div>
                    <!--/.post-related-->

                    <div id="comments" class="comments-area">
                        <?php 
                            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";                        
                         ?>
                        <h3 class="comments-title">
			             <span>{{$product->title}}</span>&rdquo;		</h3>

                        <div id="respond" class="comment-respond">
                            <div class="comment">
                                <div class="fb-comments" data-href="{{$actual_link}}" data-width="" data-numposts="5" width="100%"></div>
                            </div>
                        </div>
                    </div>
                    <!-- #comments -->
                </div>
                <!-- #primary -->

               @include('fptsieutoc.common.sider_bar')
            </div>
            <!-- .inner-wrap -->
        </div>
        <!-- #main -->
@endsection
