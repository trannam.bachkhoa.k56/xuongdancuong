@extends('fptsieutoc.layout.site')

@section('title','Liên Hệ ')
@section('meta_description', 'Liên hệ với chúng tôi ')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )

@section('content')
	   <div id="main" class="clearfix">
            <div class="inner-wrap clearfix">

                <div id="primary">
                    <div id="content" class="clearfix">

                        <article id="post-12" class="post-12 page type-page status-publish hentry">

                            <header class="entry-header">
                                <h1 class="entry-title">
          						  Liên hệ        
          						</h1>
                            </header>

                            <div class="">
                                <div >
                                    <form onSubmit="return contact(this);" method="post"
										  action="/submit/contact" >
										{!! csrf_field() !!}
										<input type="hidden" name="is_json"
											   class="form-control captcha" value="1" placeholder="">

										@if(!isset($customerDisplay->statusName) || $customerDisplay->statusName != 0)

										<div class="form-group">
											<div>
												<label> Tên của bạn (bắt buộc)</label>
												<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
											</div>
										</div>
										@endif
										
										@if(!isset($customerDisplay->statusEmail) || $customerDisplay->statusEmail != 0)
										<div class="form-group">
											<div>
												<label> Địa chỉ Email (bắt buộc) </label>
												
												<input type="email" class="form-control" placeholder="Email của bạn" name="email">
											</div>
										</div>
										@endif

										@if(!isset($customerDisplay->statusPhone) || $customerDisplay->statusPhone != 0)
										<div class="form-group">
											<div>
												<label> Số điện thoại </label>
												<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
											</div>
										</div>
										@endif

										@if(!isset($customerDisplay->statusMessage) ||  $customerDisplay->statusMessage != 0)
										<div class="form-group">
											<div>
												<label>Thông điệp</label>
											<textarea class="form-control" name="message" rows="6" 
													  placeholder="Nội dung ghi chú"></textarea>
											</div>
										</div>
										@endif
										@if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
											<input type="hidden" value="{{ $_SERVER['HTTP_REFERER'] }}" name="utm_source" />
										@endif

										@if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
										<div class="form-group">
											<div>
												<button class="btn btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
											</div>
										</div>
										@endif
									</form>

                                </div>
                            </div>

                        </article>

                    </div>
                    <!-- #content -->
                </div>
                <!-- #primary -->

               @include('fptsieutoc.common.sider_bar')

            </div>
            <!-- .inner-wrap -->
        </div>

      
@endsection
