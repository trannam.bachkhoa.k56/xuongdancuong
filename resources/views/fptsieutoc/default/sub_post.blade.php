@extends('vn3c.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
    <section class="banner-detail-new" style="background-image: url(img/slide.jpg)">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="title-ct-new">BOOST YOUR EMAIL SUBSCRIPTION RATE <br>
                        WITH THESE 5 QUICK CONTENT FIXES</p>
                </div>
            </div>
        </div>
    </section>

    <section class="content-detail-new">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="link-url">
                        <ul>
                            <li><a href="" title="">Trang chủ |</a></li>
                            <li><a href="" title=""> Liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="content-it-ct">
                        <p class="content-it">
                            Bootstrap Based Responsive Multi-Purpose HTML5 Template built using HTML5/CSS3 features and suitable for creative companies, agencies, and freelancers which need a professional way to showcase their projects, services, and sell their products.
                        </p>
                        <img src="upload-img/ct-item1.jpg" alt="">
                        <p class="content-it">
                            Bootstrap Based Responsive Multi-Purpose HTML5 Template built using HTML5/CSS3 features and suitable for creative companies, agencies, and freelancers which need a professional way to showcase their projects, services, and sell their products.
                        </p>

                    </div>

                    <div class="tag">
                        <span><i class="fa fa-tags" aria-hidden="true"></i></span> Tag : </span>
                        <ul>
                            <li><a href="" title="">thiet ke website</a></li>
                            <li><a href="" title="">thiet ke website</a></li>
                        </ul>
                    </div>

                    <div class="involve">
                        <h3 class="title-ct-new">bài viết liên quan</h3>
                        <ul>
                            <li><a href="" title="">How to make your online presence profitable. Digital nomads</a>
                            </li>
                            <li><a href="" title="">7 Essential KPIs That Tell Everything About Your Blog’s Performance</a>
                            </li>
                            <li><a href="" title="">7 Best WordPress Plugins to Increase Your Email Conversions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection


