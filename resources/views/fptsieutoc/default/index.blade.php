@extends('fptsieutoc.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )
@section('type_meta', 'website')

@section('content')
<div id="main" class="clearfix">
    <div class="inner-wrap clearfix">
        <div class="front-page-top-section clearfix">
            <div class="widget_slider_area">
                <section id="colormag_featured_posts_slider_widget-2" class="widget widget_featured_slider widget_featured_meta clearfix">
                    <div class="widget_slider_area_rotate">
                        @foreach(App\Entity\Product::getAllProduct() as $id => $product )
							@if($id == 0)
							<div class="single-slide displayblock">
								<figure class="slider-featured-image" >
									<a href="{{$product->slug}}" title="{{$product->title}}">
										<img width="800"  style="height:445px" src="{{$product->image}}" class="attachment-colormag-featured-image size-colormag-featured-image wp-post-image" alt="{{$product->title}}" title="{{$product->tite}}" /></a>
								</figure>
								<div class="slide-content">
									<div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-1/"  rel="category tag">{{isset($information['danh-muc-1']) ? $information['danh-muc-1'] : ''}}</a>&nbsp;</span></div>
									<h3 class="entry-title">
										<a href="{{$product->slug}}" title="{{$product->title}}">
										{{$product->title}}
										</a>
									</h3>
									<div class="below-entry-meta">
										<span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark">
											<i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{date_format($product->updated_at, "d/m/Y")}}</time></a>
										</span>
										<span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->slug}}" title="admin">admin</a></span></span>
									   
										 <span class="comments"><i class="fa fa-comment"></i><a href="khuyen-mai-chao-xuan-2020/#respond">0</a></span>
									</div>
								</div>
							</div>
							@endif
                        @endforeach

                        @foreach(App\Entity\Product::getAllProduct() as $id => $product )
                        <div class="single-slide displaynone">
                            <figure class="slider-featured-image">
                                <a href="{{$product->slug}}" title="{{$product->title}}">
								<img width="800"  style="height:445px" src="{{$product->image}}" class="attachment-colormag-featured-image size-colormag-featured-image wp-post-image" alt="{{$product->title}}" title="{{$product->tite}}" /></a>
                            </figure>
                            <div class="slide-content">
                                <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-1/"  rel="category tag">{{isset($information['danh-muc-1']) ? $information['danh-muc-1'] : ''}}</a>&nbsp;</span></div>
                                <h3 class="entry-title">
                                    <a href="{{$product->slug}}" title="{{$product->title}}">
                                    {{$product->title}}
                                    </a>
                                </h3>
                                <div class="below-entry-meta">
                                    <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark">
                                        <i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{date_format($product->updated_at, "d/m/Y")}}</time></a>
                                    </span>
                                    <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->slug}}" title="admin">admin</a></span></span>
                                   
                                     <span class="comments"><i class="fa fa-comment"></i><a href="khuyen-mai-chao-xuan-2020/#respond">0</a></span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </section>
            </div>

            <div class="widget_beside_slider">
                <section id="colormag_highlighted_posts_widget-2" class="widget widget_highlighted_posts widget_featured_meta clearfix">
                    <div class="widget_highlighted_post_area">
                        @foreach(App\Entity\Product::showProduct('san-pham-noi-bat', 4) as $product )
                        <div class="single-article">
                            <figure class="highlights-featured-image">
                                <a href="{{$product->slug}}" title="{{$product->title}}">
                                    <img width="392"  style="height:217px" src="{{$product->image}}" class="attachment-colormag-highlighted-post size-colormag-highlighted-post wp-post-image" alt="Khuyến Mại Chào Xuân 2020" title="{{$product->title}}" srcset="{{$product->image}} 392w, {{$product->image}} 130w" sizes="(max-width: 392px) 100vw, 392px" /></a>
                            </figure>
                            <div class="article-content">
                                <h3 class="entry-title">
            						<a href="{{$product->slug}}" title="{{$product->title}}">{{$product->title}}</a>
            					</h3>
                                <div class="below-entry-meta">
                                    <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark">
                                        <i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{date_format($product->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->slug}}" title="admin">admin</a></span></span>
                                    <span class="comments"><i class="fa fa-comment"></i><a href="{{$product->slug}}">0</a></span>
                                </div>
                            </div>

                        </div>
                        @endforeach
                     
                    </div>
                </section>
            </div>
        </div>

        <div class="main-content-section clearfix">
            <div id="primary">
                <div id="content" class="clearfix">

                    <section id="colormag_featured_posts_widget-2" class="widget widget_featured_posts widget_featured_meta clearfix">
						<?php $categoryDetail = App\Entity\Category::getDetailCategory( isset($information['danh-muc-1']) ? $information['danh-muc-1'] : '' ); ?>
                        <h3 class="widget-title"><span >{{ $categoryDetail->title }}</span></h3>
                        <div class="first-post">
                            @foreach(App\Entity\Product::showProduct(isset($information['danh-muc-1']) ? $information['danh-muc-1'] : '') as $id => $product )
                            @if($id == 0)
                            <div class="single-article clearfix">
                                <figure>
                                    <a href="{{$product->slug}}" title="{{$product->title}}"><img width="390" height="205" src="{{$product->image}}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{$product->title}}" title="{{$product->title}}" srcset="{{$product->image}} 130w, {{$product->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                </figure>
                                <div class="article-content">
                                    <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-1"  rel="category tag">{{ $categoryDetail->title }}</a>&nbsp;</span></div>
                                    <h3 class="entry-title">
                                        <a href="{{$product->slug}}" title="{{$product->title}}">{{$product->title}}</a>
                                    </h3>
                                    <div class="below-entry-meta">
                                        <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{date_format($product->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->updated_at}}" title="admin">admin</a></span></span>
                                        <span class="comments"><i class="fa fa-comment"></i><a href="{{$product->slug}}">0</a></span>
                                    </div>
                                </div>
                                </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="following-post">
                            @foreach(App\Entity\Product::showProduct(isset($information['danh-muc-1']) ? $information['danh-muc-1'] : '') as $id => $product )
                            
                            <div class="single-article clearfix">
                                <figure>
                                    <a href="{{$product->slug}}" title="{{$product->title}}"><img width="130" height="90" src="{{$product->image}}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{$product->title}}" title="{{$product->title}}" srcset="{{$product->image}} 130w, {{$product->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                </figure>
                                <div class="article-content">
                                    <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-1"  rel="category tag">{{ $categoryDetail->title }}</a>&nbsp;</span></div>
                                    <h3 class="entry-title">
                    					<a href="{{$product->slug}}" title="{{$product->title}}">{{$product->title}}</a>
                    				</h3>
                                    <div class="below-entry-meta">
                                        <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{date_format($product->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->updated_at}}" title="admin">admin</a></span></span>
                                        <span class="comments"><i class="fa fa-comment"></i><a href="{{$product->slug}}">0</a></span>
                                    </div>
                                </div>
                                </div>
             
                            @endforeach
                        </div>
                        <!-- </div> -->
                    </section>
                    <div class="tg-one-half">
                        <section id="colormag_featured_posts_vertical_widget-3" class="widget widget_featured_posts widget_featured_posts_vertical widget_featured_meta clearfix">
							<?php $categoryDetail = App\Entity\Category::getDetailCategory( isset($information['danh-muc-2']) ? $information['danh-muc-2'] : '' ); ?>
                            <h3 class="widget-title"><span >{{ $categoryDetail->title }}</span></h3>
                            <div class="first-post">
                                @foreach(App\Entity\Product::showProduct( isset($information['danh-muc-2']) ? $information['danh-muc-2'] : '' ) as $id => $product )
                                @if($id == 0)
                                <div class="single-article clearfix">
                                    <figure>
                                        <a href="{{$product->slug}}" title="{{$product->title}}"><img width="390" height="205" src="{{$product->image}}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{$product->title}}" title="{{$product->title}}" srcset="{{$product->image}} 130w, {{$product->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                    </figure>
                                    <div class="article-content">
                                        <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-2"  rel="category tag">{{ $categoryDetail->title }}</a>&nbsp;</span></div>
                                        <h3 class="entry-title">
                                            <a href="{{$product->slug}}" title="{{$product->title}}">{{$product->title}}</a>
                                        </h3>
                                        <div class="below-entry-meta">
                                            <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{$product->updated_at}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->updated_at}}" title="admin">admin</a></span></span>
                                            <span class="comments"><i class="fa fa-comment"></i><a href="danh-muc-1-12-2019/#respond">0</a></span>
                                        </div>
                                    </div>
                                    </div>
                                @endif
                                @endforeach
                            </div>
                            <div class="following-post">
                                @foreach(App\Entity\Product::showProduct(isset($information['danh-muc-2']) ? $information['danh-muc-2'] : '') as $id => $product )
                                <div class="single-article clearfix">
                                    <figure>
                                        <a href="{{ $product->slug }}" title="{{ $product->title }}"><img width="130" height="90" src="{{ $product->image }}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{ $product->title }}" title="{{$product->title}}" srcset="{{$product->image}} 130w, {{$product->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                    </figure>
                                    <div class="article-content">
                                        <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-2" rel="category tag">{{ $categoryDetail->title }}</a>&nbsp;</span></div>
                                        <h3 class="entry-title">
                        					<a href="{{$product->slug}}" title="{{$product->title}}">{{$product->title}}</a>
                        				</h3>
                                        <div class="below-entry-meta">
                                            <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="">{{date_format($product->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->slug}}" title="admin">admin</a></span></span>
                                            <span class="comments"><i class="fa fa-comment"></i><a href="{{$product->slug}}">0</a></span>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                            </div>
                        </section>
                    </div>

                    <div class="tg-one-half tg-one-half-last">
                        <section id="colormag_featured_posts_vertical_widget-4" class="widget widget_featured_posts widget_featured_posts_vertical widget_featured_meta clearfix">
							<?php $categoryDetail = App\Entity\Category::getDetailCategory( isset($information['danh-muc-3']) ? $information['danh-muc-3'] : '' ); ?>
                            <h3 class="widget-title"><span >{{ $categoryDetail->title }}</span></h3>
                            <div class="first-post">
                                    @foreach(App\Entity\Product::showProduct(isset($information['danh-muc-3']) ? $information['danh-muc-3'] : '') as $id => $product )
                                    @if($id == 0)
                                    <div class="single-article clearfix">
                                        <figure>
                                            <a href="{{$product->slug}}" title="{{$product->title}}"><img width="390" height="205" src="{{$product->image}}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{$product->title}}" title="{{$product->title}}" srcset="{{$product->image}} 130w, {{$product->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                        </figure>
                                        <div class="article-content">
                                            <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-3"  rel="category tag">{{ $categoryDetail->title }}</a>&nbsp;</span></div>
                                            <h3 class="entry-title">
                                                <a href="{{$product->slug}}" title="{{$product->title}}">{{$product->title}}</a>
                                            </h3>
                                            <div class="below-entry-meta">
                                                <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{$product->updated_at}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->updated_at}}" title="admin">admin</a></span></span>
                                                <span class="comments"><i class="fa fa-comment"></i><a href="{{$product->slug}}">0</a></span>
                                            </div>
                                        </div>
                                        </div>
                                    @endif
                                    @endforeach
                            </div>
                            <div class="following-post">
                                 @foreach(App\Entity\Product::showProduct(isset($information['danh-muc-3']) ? $information['danh-muc-3'] : '') as $id => $product )
                                <div class="single-article clearfix">
                                    <figure>
                                        <a href="{{ $product->slug }}" title="{{ $product->title }}"><img width="130" height="90" src="{{ $product->image }}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{ $product->title }}" title="{{$product->title}}" srcset="{{$product->image}} 130w, {{$product->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                    </figure>
                                    <div class="article-content">
                                        <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-3" rel="category tag">{{ $categoryDetail->title }}</a>&nbsp;</span></div>
                                        <h3 class="entry-title">
                                            <a href="{{$product->slug}}" title="{{$product->title}}">{{$product->title}}</a>
                                        </h3>
                                        <div class="below-entry-meta">
                                            <span class="posted-on"><a href="{{$product->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="">{{date_format($product->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->slug}}" title="admin">admin</a></span></span>
                                            <span class="comments"><i class="fa fa-comment"></i><a href="{{$product->slug}}">0</a></span>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                               
                            </div>
                        </section>
                    </div>

                    <div class="clearfix"></div>
                    <section id="colormag_featured_posts_widget-3" class="widget widget_featured_posts widget_featured_meta clearfix">
                        <h3 class="widget-title"><span >Tin tức công nghệ</span></h3>
                       <div class="first-post">
                            @foreach(App\Entity\Post::newPostIndex(5) as $id => $postIndex )
                            @if($id == 0)
                            <div class="single-article clearfix">
                                <figure>
                                    <a href="/tin-tuc/{{$postIndex->slug}}" title="{{$postIndex->title}}"><img width="390" height="205" src="{{$postIndex->image}}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{$postIndex->title}}" title="{{$postIndex->title}}" srcset="{{$postIndex->image}} 130w, {{$postIndex->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                </figure>
                                <div class="article-content">
                                    <div class="above-entry-meta"><span class="cat-links"><a href="/danh-muc/tin-tuc"  rel="category tag">Mới</a>&nbsp;</span></div>
                                    <h3 class="entry-title">
                                        <a href="/tin-tuc/{{$postIndex->slug}}" title="{{$postIndex->title}}">{{$postIndex->title}}</a>
                                    </h3>
                                    <div class="below-entry-meta">
                                        <span class="posted-on"><a href="{{$postIndex->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$postIndex->updated_at}}">{{date_format($postIndex->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="/tin-tuc/{{$postIndex->updated_at}}" title="admin">admin</a></span></span>
                                        <span class="comments"><i class="fa fa-comment"></i><a href="{{$postIndex->slug}}">0</a></span>
                                    </div>
                                </div>
                                </div>
                            @endif
                            @endforeach
                        </div>
                        <div class="following-post">
                            @foreach(App\Entity\Post::newPostIndex(5) as $id => $postIndex )
                            
                            <div class="single-article clearfix">
                                <figure>
                                    <a href="/tin-tuc/{{$postIndex->slug}}" title="{{$postIndex->title}}"><img width="130" height="90" src="{{$postIndex->image}}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{$postIndex->title}}" title="{{$postIndex->title}}" srcset="{{$postIndex->image}} 130w, {{$postIndex->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                </figure>
                                <div class="article-content">
                                    <div class="above-entry-meta"><span class="cat-links"><a href="/danh-muc/tin-tuc"  rel="category tag">mới</a>&nbsp;</span></div>
                                    <h3 class="entry-title">
                                        <a href="/tin-tuc/{{$postIndex->slug}}" title="{{$postIndex->title}}">{{$postIndex->title}}</a>
                                    </h3>
                                    <div class="below-entry-meta">
                                        <span class="posted-on"><a href="{{$postIndex->slug}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$postIndex->updated_at}}">{{date_format($postIndex->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="/tin-tuc/{{$postIndex->slug}}" title="admin">admin</a></span></span>
                                        <span class="comments"><i class="fa fa-comment"></i><a href="{{$postIndex->slug}}">0</a></span>
                                    </div>
                                </div>
                                </div>
             
                            @endforeach
                        </div>
                        <!-- </div> -->
                    </section>
                </div>
            </div>
            @include('fptsieutoc.common.sider_bar')
        </div>

    </div>
    <!-- .inner-wrap -->
</div>
<!-- #main -->


@endsection
