<!DOCTYPE html>
<html>

<head>
    <title>MOMA Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng</title>
    <meta name="ROBOTS" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="title" content="MOMA Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng">
    <meta name="description" content="Website miễn phí, phần mềm quản lý, phần mềm bán hàng, Bộ nhận dạng thương hiệu, kết nối website với mạng xã hội." />
    <meta name="keywords" content="Website miễn phí, thiết kế website, thiết kế website miễn phí" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta itemprop="image" content="" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:locale" content="vi_VN">
    <meta property="og:type" content="website">

    <meta property="og:title" content="Moma Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng" />
    <meta property="og:description" content="Website miễn phí, phần mềm quản lý, phần mềm bán hàng, Bộ nhận dạng thương hiệu, kết nối website với mạng xã hội." />
    <meta property="og:url" content="" />
    <meta property="og:image" content="" />
    <meta property="og:image:secure_url" content="" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Website miễn phí, phần mềm quản lý, phần mềm bán hàng, Bộ nhận dạng thương hiệu, kết nối website với mạng xã hội." />
    <meta name="twitter:title" content="VN3C Thiết kế website chuẩn seo miễn phí cho các chủ doanh nghiệp, chủ cửa hàng" />
    <meta name="twitter:image" content="" />

    <!-- <meta name="google-site-verification" content="RYNcHP0p0DC3jEmH4EuJTdIEBN9VnG-gLGtFMrWPoRs" />-->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/bootstrap.min.css') }}" type="text/css">
    <!-- Font icons -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/font-awesome.min.css') }}" type="text/css">
    <!-- Animation -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/animate.css') }}" type="text/css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/owl.carousel.css') }}" type="text/css">
    <!-- Light Case -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/lightcase.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/extra.css') }}" type="text/css">
    <!-- Template style -->
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/main.css') }}" type="text/css">

    <!-- End Facebook Pixel Code -->
    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token = $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: 'subcribe-email',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: 'dat-hang',
                data: data,
                success: function(result) {
                    var obj = jQuery.parseJSON(result);
                    window.location.replace("gio-hang");
                },
                error: function(error) {
                    console.log(error);
                }

            });

            return false;
        }

        function contact(e) {
            var $btn = $(e).find('button').button('loading');
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: 'submit/contact',
                data: data,
                success: function(result) {
                    var obj = jQuery.parseJSON(result);
                    // gửi thành công
                    if (obj.status == 200) {
                        alert(obj.message);
                        $btn.button('reset');

                        location.reload();
                        return;
                    }

                    // gửi thất bại
                    if (obj.status == 500) {
                        alert(obj.message);
                        $btn.button('reset');

                        location.reload();
                        return;
                    }
                },
                error: function(error) {
                    //alert('Lỗi gì đó đã xảy ra!')
                }

            });

            return false;
        }
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '366243167327085');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=366243167327085&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140228270-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-140228270-1');
    </script>

	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}

    {!! isset($information['google-analynic']) ? $information['google-analynic'] : '' !!}
</head>
<body>
	@include('vn3c.common.header')
        <!-- Phần nội dung -->
        @yield('content')
		
		<!-- Your customer chat code -->
	{!! isset($information['ma-nhung-chat-facebook']) ? $information['ma-nhung-chat-facebook'] : '' !!}
	<!--JS Files-->
	<script src="{{ asset('assets/vn3ctheme/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/vn3ctheme/js/bootstrap.min.js') }}"></script>
	<!--Owl Coursel-->
	<script src="{{ asset('assets/vn3ctheme/js/owl.carousel.min.js') }}"></script>
	<!-- Typing Text -->
	<!-- <script src="assets/vn3ctheme/js/typed.js"></script> -->
	<!--Images LightCase-->
	<script src="{{ asset('assets/vn3ctheme/js/lightcase.js') }}"></script>
	<!-- Portfolio filter -->
	<script src="{{ asset('assets/vn3ctheme/js/jquery.isotope.js') }}"></script>
	<!-- Wow Animation -->
	<script src="{{ asset('assets/vn3ctheme/js/wow.js') }}"></script>
	<!-- Map -->
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyBkdsK7PWcojsO-o_q2tmFOLBfPGL8k8Vg&amp;language=en"></script>
	<!-- Main Script -->
	<script src="{{ asset('assets/vn3ctheme/js/main.js') }}"></script>
</body>
</html>