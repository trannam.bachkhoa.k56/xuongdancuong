<!DOCTYPE html>
<html lang="vi">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
    <title>@yield('title')</title>
    <meta name="viewport" 					content="width=device-width, initial-scale=1.0" />   
    <meta http-equiv="Content-Type" 		content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" 		content="IE=edge">
	<meta name="title" 						content="@yield('title')" />
    <meta name="description" 				content="@yield('meta_description')" />
    <meta name="keywords" 					content="@yield('keywords')" />
	<meta name="theme-color"				content="#07a0d2" />
	<meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
	
	<link rel="canonical" href="@yield('meta_url')"/>
    <link rel="icon" href="{{isset($information['meta_image']) ? $information['meta_image'] : '' }} " type="image/x-icon" />
    <!-- <meta name="google-site-verification" content="RYNcHP0p0DC3jEmH4EuJTdIEBN9VnG-gLGtFMrWPoRs" />-->

    <!-- Bootstrap -->
    <!-- <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/bootstrap.min.css') }}" type="text/css"> -->
    <!-- Font icons -->
    <!-- <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/font-awesome.min.css') }}" type="text/css"> -->
 
    <!-- Owl Carousel -->
    <!-- <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/owl.carousel.css') }}" type="text/css"> -->
    <!-- Light Case -->
	   <!-- Animation -->
  <!--   <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/lightcase.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/extra.css') }}" type="text/css"> -->
    <!-- Template style -->
    <!-- <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/main.css') }}" type="text/css">
	<link rel="stylesheet" 	type="text/css" media="screen" 	href="/public/xhome1/css/slick.css" />
	<link rel="stylesheet" 	type="text/css" media="screen" 	href="/public/xhome1/css/slick-theme.css"/ > -->
    <!-- End Facebook Pixel Code -->

     <!-- css page -->
    <link rel="stylesheet"  type="text/css" media="screen"  href="/public/xhome1/css/extra.css" />
    <link rel='stylesheet' id='contact-form-7-css'  href='/fptsieutoc/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.2' type='text/css' media='all' />
    <link rel='stylesheet' id='easy-callnow-css'  href='/fptsieutoc/wp-content/plugins/easy-call-now/public/css/easy-callnow-public.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='colormag_google_fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A400%2C600&#038;ver=4.9.13' type='text/css' media='all' />
    <link rel='stylesheet' id='colormag_style-css'  href='/fptsieutoc/wp-content/themes/colormag/style.css?ver=4.9.13' type='text/css' media='all' />
    <link rel='stylesheet' id='colormag-fontawesome-css'  href='/fptsieutoc/wp-content/themes/colormag/fontawesome/css/font-awesome.css?ver=4.2.1' type='text/css' media='all' />
    
    <script src="{{ asset('assets/vn3ctheme/js/jquery.min.js') }}"></script>    
    <script src="/public/xhome1/js/jquery-3.3.1.min.js"></script> 
    
    <script type='text/javascript' src='/fptsieutoc/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/plugins/easy-call-now/public/js/easy-callnow-public.js?ver=1.0.0'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/colormag-custom.js?ver=4.9.13'></script>
    <!--[if lte IE 8]>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/html5shiv.min.js?ver=4.9.13'></script>
    <![endif]-->
    <link rel='https://api.w.org/' href='wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" /> 
    <script src="code.php.js?f=3c1b7b"></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.2'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/jquery.bxslider.min.js?ver=4.2.10'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/colormag-slider-setting.js?ver=4.9.13'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/navigation.js?ver=4.9.13'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/news-ticker/jquery.newsTicker.min.js?ver=1.0.0'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/news-ticker/ticker-setting.js?ver=20150304'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/sticky/jquery.sticky.js?ver=20150309'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/sticky/sticky-setting.js?ver=20150309'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/fitvids/jquery.fitvids.js?ver=20150311'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-content/themes/colormag/js/fitvids/fitvids-setting.js?ver=20150311'></script>
    <script type='text/javascript' src='/fptsieutoc/wp-includes/js/wp-embed.min.js?ver=4.9.13'></script>

    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token = $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: 'subcribe-email',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: 'dat-hang',
                data: data,
                success: function(result) {
                    var obj = jQuery.parseJSON(result);
                    window.location.replace("gio-hang");
                },
                error: function(error) {
                    console.log(error);
                }

            });

            return false;
        }

        function contact(e) {
            // var $btn = $(e).find('button').button('loading');
            var data = $(e).serialize();
            alert('1');
            $.ajax({
                type: "POST",
                url: 'submit/contact',
                data: data,
                success: function(result) {
                    var obj = jQuery.parseJSON(result);
                    alert('thành công ');
                    // gửi thành công
                    if (obj.status == 200) {
                        alert(obj.message);
                        $btn.button('reset');

                        location.reload();
                        return;
                    }

                    // gửi thất bại
                    if (obj.status == 500) {
                        alert(obj.message);
                        $btn.button('reset');

                       location.reload();
                        return;
                    }
                },
                error: function(error) {
                    //alert('Lỗi gì đó đã xảy ra!')
                }

            });

            return false;
        }
		
		  
    </script>
        
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v5.0&appId=475816949623557&autoLogAppEvents=1"></script>

	{!! isset($information['google-anylic']) ? $information['google-anylic'] : '' !!}
	
	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}
	
</head>
<body>
    	
    <div id="page" class="hfeed site">
            @include('fptsieutoc.common.header')
            <!-- Phần nội dung -->
            @yield('content')

    		@include('fptsieutoc.common.footer')
    </div>
 <style>
	 .tel{
		 display:none ;
	 }
 </style>
</body>
</html>