<footer id="colophon" class="clearfix ">

    <div class="footer-widgets-wrapper">
        <div class="inner-wrap">
            <div class="footer-widgets-area clearfix">
                <div class="tg-footer-main-widget">
                    <div class="tg-first-footer-widget">
                        <aside id="text-3" class="widget widget_text clearfix">
                            <h3 class="widget-title"><span> {{isset( $information['ten-cong-ty']) ?  $information['ten-cong-ty'] : ''}}</span></h3>
                            <div class="textwidget">
                               {!! isset($information['content-footer']) ? $information['content-footer'] : '' !!}
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="tg-footer-other-widgets">
                    <div class="tg-second-footer-widget">
                        <aside id="text-4" class="widget widget_text clearfix">
                            <h3 class="widget-title"><span>Tin tức mới nhất</span></h3>
                            <div class="textwidget">
                                <ul>
                                    @foreach(App\Entity\Post::newPostIndex(5) as $id => $postNew )
                                    <li><a href="{{$postNew->slug}}">{{$postNew->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="tg-third-footer-widget">
                        <aside id="text-5" class="widget widget_text clearfix">
                            <h3 class="widget-title"><span>sản phẩm mới</span></h3>
                            <div class="textwidget">
                                <ul>
                                     @foreach(App\Entity\Product::newProduct(5) as $id => $productNew )
                                    <li><a href="{{$productNew->slug}}">{{$productNew->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class="tg-fourth-footer-widget">
                        <aside id="colormag_300x250_advertisement_widget-2" class="widget widget_300x250_advertisement clearfix">
                            <div class="advertisement_300x250">
                                <div class="advertisement-title">
                                    <h3 class="widget-title"><span>Địa Chỉ</span></h3> </div>
                                <div class="advertisement-content"><img src="http://localhost/fpt/wp-content/uploads/2018/05/xphomesa.jpg" width="300" height="250" alt=""></div>
                            </div>
                        </aside>
                        <aside id="text-6" class="widget widget_text clearfix">
                            <div class="textwidget">
                                <ul>
                                    <li>{{isset($information['dia-chi-1']) ? $information['dia-chi-1'] : ''}}</li>
                                    <li>{{isset($information['dia-chi-2']) ? $information['dia-chi-2'] : ''}}</li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-socket-wrapper clearfix">
        <div class="inner-wrap">
            <div class="footer-socket-area">
                <div class="footer-socket-right-section">
                    <div class="social-links clearfix">
                        <ul>
                            @foreach(\App\Entity\SubPost::showSubPost('lien-ket', 8) as $id => $link)
                            <li><a href="{{isset($link->link) ? $link->link : '#' }}"><i class="{{isset($link->description) ? $link->description : '#' }}"></i></a></li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- .social-links -->
                </div>

                <div class="footer-socket-left-section">
                    <p style="color:#fff;">{{isset($information['copyright']) ? $information['copyright'] : ''}}</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#masthead" id="scroll-up"><i class="fa fa-chevron-up"></i></a>

@include ('general.contact', ['phone' => isset($information['hotline1']) ? $information['hotline1']  : '',
'zalo' => isset($information['chat-zalo']) ? $information['chat-zalo'] : '',
'fanpage' => isset($information['ten-tom-tat-fanpage-facebook']) ? $information['ten-tom-tat-fanpage-facebook'] : ''
])


