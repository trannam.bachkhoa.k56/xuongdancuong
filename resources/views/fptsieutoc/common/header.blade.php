<header id="masthead" class="site-header clearfix ">
    <div id="header-text-nav-container" class="clearfix">

        <div class="news-bar">
            <div class="inner-wrap clearfix">

                <div class="date-in-header" id="date" >
                    
                </div>

                <script type="text/javascript">
                    $(document).ready(function() {
                        var d = new Date();
                        var month = d.getMonth()+1;
                        var day = d.getDate();
                        var year = d.getFullYear();
                       
                        var date = 'Ngày '+ day + ' Tháng '+ month + ' Năm ' + year ;
                        $('#date').append(date)
              
                    });
                   
                </script>

                <div class="breaking-news">
                    <strong class="breaking-news-latest">Bài Viết Mới: </strong>
                    <ul class="newsticker">
                        @foreach(App\Entity\Product::getAllProduct() as $post)
                        <li>
                            <a href="{{route('product',['post_slug' => $post->slug ])}}" title="{{$post->title}}">{{$post->title}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>

                <div class="social-links clearfix">
                    <ul>
                        @foreach(\App\Entity\SubPost::showSubPost('lien-ket', 8) as $id => $link)
                        <li><a href="{{isset($link->link) ? $link->link : '#' }}"><i class="{{isset($link->description) ? $link->description : '#' }}"></i></a></li>
                        @endforeach
                    </ul>
                </div>
                <!-- .social-links -->
            </div>
        </div>

        <div class="inner-wrap">

            <div id="header-text-nav-wrap" class="clearfix">
                <div id="header-left-section">
                    <div id="header-logo-image">

                        <a href="#" class="custom-logo-link" rel="home" itemprop="url">
                            <img width="227" height="66" src=" {{isset($information['logo']) ? $information['logo'] : ''}} " class="custom-logo" alt="logo" itemprop="logo" />
                        </a>
                    </div>
                    <!-- #header-logo-image -->
                    <div id="header-text" class="screen-reader-text">
                        <h1 id="site-title">
                            <a href="" title="{{isset($information['ten-website']) ? $information['ten-website'] : ''}}" rel="home">{{isset($information['ten-website']) ? $information['ten-website'] : ''}}</a>
                        </h1>
                        <p id="site-description">{{isset($information['ten-website']) ? $information['ten-website'] : ''}}</p>
                        <!-- #site-description -->
                    </div>
                    <!-- #header-text -->
                </div>
                <!-- #header-left-section -->
                <div id="header-right-section">
                    <div id="header-right-sidebar" class="clearfix">
                        <aside id="colormag_728x90_advertisement_widget-2" class="widget widget_728x90_advertisement clearfix">
                            <div class="advertisement_728x90">
                                <div class="advertisement-content">
									<p>Hotline 1: {{isset($information['hotline1']) ? $information['hotline1'] : ''}} </p>
									<p>Hotline 2: {{isset($information['hotline2']) ? $information['hotline2'] : ''}} </p>
                                    <a href="#" class="single_ad_728x90" target="_blank" rel="nofollow">
                                        <!--<img src="{{isset($information['image-header']) ? $information['image-header'] : ''}}" width="728" alt=""> -->
                                    </a>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
                <!-- #header-right-section -->

            </div>
            <!-- #header-text-nav-wrap -->

        </div>
        <!-- .inner-wrap -->

        <nav id="site-navigation" class="main-navigation clearfix" role="navigation">
            <div class="inner-wrap clearfix">

                <div class="home-icon ">
                    <a href="/" title="{{isset($information['ten-website']) ? $information['ten-website'] : ''}}">
                        <i class="fa fa-home"></i>
                    </a>
                </div>
                <h4 class="menu-toggle" onclick="toggleMenu();"></h4>
                <div class="menu-mobile" style="display: none;">
                    <ul style="display: block;position: absolute;width: 100%;bottom: -11em;background: #333;">
                        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                           @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                              <li style="display: block;width: 100%;text-align: left;border: 1px solid #eee;" id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27
                                "><a href="{{ $menuelement['url'] }}">{{ $menuelement['title_show'] }}</a>
                              </li>
                            @endforeach
                        @endforeach                       
                    </ul>
                </div>
                <script type="text/javascript">
                    function toggleMenu(){
                        $('.menu-mobile').toggle(500);
                    }
                </script>

                <div class="menu-primary-container">
                    <ul id="menu-primary" class="menu">
                        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                           @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                              <li id="menu-item-27" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-27
                                "><a href="{{ $menuelement['url'] }}">{{ $menuelement['title_show'] }}</a>
                              </li>
                            @endforeach
                        @endforeach                       
                    </ul>
                </div>
                <div class="random-post">
                    <a href="#" title="View a random post"><i
                        class="fa fa-random"></i>
                    </a>
                </div>

                <i class="fa fa-search search-top"></i>
                <div class="search-form-top">
                    <form method="get" action="/tim-kiem"  class="search-form searchform clearfix">
                        <div class="search-wrap">
                        <input id="search_input" value="{{ isset($word) ? $word : '' }}" type="text" name="word" placeholder="Search" class="s field">
                        <button class="search-icon" type="submit"></button>
                        </div>
                    </form>
                    <!-- .searchform -->
                </div>
            </div>
        </nav>

    </div>
    <!-- #header-text-nav-container -->

</header>