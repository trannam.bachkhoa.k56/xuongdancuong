
            <div id="secondary">

                <aside id="custom_html-3" class="widget_text widget widget_custom_html clearfix">
                    <h3 class="widget-title"><span>{{isset($information['title-sider-bar-1']) ? $information['title-sider-bar-1'] : ''}}</span></h3>
                    <div class="textwidget custom-html-widget">
                        <p>
                            <center><span style="font-size:120%;">HOTLINE 1: <span style="background-color: #8ff9eb;">{{isset($information['hotline1']) ? $information['hotline1'] : ''}}</span></span>
                            </center>
                        </p>
                        <hr />
                        <p>
                            <center><span style="font-size:120%;">HOTLINE 2: <span style="background-color: #8ff9eb;">{{isset($information['hotline2']) ? $information['hotline2'] : ''}}</span></span>
                            </center>
                        </p>

                    </div>
                </aside>
				<aside id="custom_html-3" class="widget_text widget widget_custom_html clearfix">
					 <h3 class="widget-title"><span>Đăng ký nhận tư vấn</span></h3>
					<form onSubmit="return contact(this);" method="post"
						  action="/submit/contact" >
						{!! csrf_field() !!}
						<input type="hidden" name="is_json"
							   class="form-control captcha" value="1" placeholder="">

						@if(!isset($customerDisplay->statusName) || $customerDisplay->statusName != 0)

						<div class="form-group">
							<div>
								<label> Tên của bạn (bắt buộc)</label>
								<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
							</div>
						</div>
						@endif
						
						@if(!isset($customerDisplay->statusEmail) || $customerDisplay->statusEmail != 0)
						<div class="form-group">
							<div>
								<label> Địa chỉ Email (bắt buộc) </label>
								
								<input type="email" class="form-control" placeholder="Email của bạn" name="email">
							</div>
						</div>
						@endif

						@if(!isset($customerDisplay->statusPhone) || $customerDisplay->statusPhone != 0)
						<div class="form-group">
							<div>
								<label> Số điện thoại </label>
								<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
							</div>
						</div>
						@endif

						@if(!isset($customerDisplay->statusMessage) ||  $customerDisplay->statusMessage != 0)
						<div class="form-group">
							<div>
								<label>Thông điệp</label>
							<textarea class="form-control" name="message" rows="6" 
									  placeholder="Nội dung ghi chú"></textarea>
							</div>
						</div>
						@endif
						@if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
							<input type="hidden" value="{{ $_SERVER['HTTP_REFERER'] }}" name="utm_source" />
						@endif

						@if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
						<div class="form-group">
							<div>
								<button class="btn btn-warning" style="padding:5px 10px">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký ngay" }}</button>
							</div>
						</div>
						@endif
					</form>
				</aside>
				
                <aside id="colormag_featured_posts_vertical_widget-2" class="widget widget_featured_posts widget_featured_posts_vertical widget_featured_meta clearfix">
                    <h3 class="widget-title"><span >Sản phẩm mua nhiều</span></h3>
                        <div class="first-post">
                        @foreach(App\Entity\Product::showProduct('san-pham-noi-bat') as $id => $product )
                        @if($id == 0)
                        <div class="single-article clearfix">
                            <figure>
                                <a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}" title="{{$product->title}}"><img width="390" height="205" src="{{$product->image}}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{$product->title}}" title="{{$product->title}}" srcset="{{$product->image}} 130w, {{$product->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                            </figure>
                            <div class="article-content">
                                <div class="above-entry-meta"><span class="cat-links"><a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}"  rel="category tag">mới</a>&nbsp;</span></div>
                                <h3 class="entry-title">
                                    <a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}" title="{{$product->title}}">{{$product->title}}</a>
                                </h3>
                                <div class="below-entry-meta">
                                    <span class="posted-on"><a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="{{$product->updated_at}}">{{$product->updated_at}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{$product->updated_at}}" title="admin">admin</a></span></span>
                                    <span class="comments"><i class="fa fa-comment"></i><a href="{{route('product',['post_slug' => $product->slug ])}}" title="{{$product->title}}">0</a></span>
                                </div>
                            </div>
                            </div>
                        @endif
                        @endforeach
                        </div>
						
                        <div class="following-post">
                             @foreach(App\Entity\Product::showProduct('san-pham-noi-bat') as $id => $productHot )
							 @if($id != 0)
                            <div class="single-article clearfix">
                                <figure>
                                    <a href="{{route('product',['post_slug' => $productHot->slug ])}}" title="{{$productHot->title}}" title="{{ $productHot->title }}"><img width="130" height="90" src="{{ $productHot->image }}" class="attachment-colormag-featured-post-small size-colormag-featured-post-small wp-post-image" alt="{{ $productHot->title }}" title="{{$productHot->title}}" srcset="{{$productHot->image}} 130w, {{$productHot->image}} 392w" sizes="(max-width: 130px) 100vw, 130px" /></a>
                                </figure>
                                <div class="article-content">
                                    <div class="above-entry-meta"><span class="cat-links"><a href="cua-hang/danh-muc-5" rel="category tag">nổi bật</a>&nbsp;</span></div>
                                    <h3 class="entry-title">
                                        <a href="{{route('product',['post_slug' => $productHot->slug ])}}" title="{{$productHot->title}}" title="{{$productHot->title}}">{{$productHot->title}}</a>
                                    </h3>
                                    <div class="below-entry-meta">
                                        <span class="posted-on"><a href="{{route('product',['post_slug' => $productHot->slug ])}}" title="{{$productHot->title}}" title="" rel="bookmark"><i class="fa fa-calendar-o"></i> <time class="entry-date published" datetime="">{{date_format($productHot->updated_at, "d/m/Y")}}</time></a></span> <span class="byline"><span class="author vcard"><i class="fa fa-user"></i><a class="url fn n" href="{{route('product',['post_slug' => $productHot->slug ])}}" title="{{$productHot->title}}" title="admin">admin</a></span></span>
                                        <span class="comments"><i class="fa fa-comment"></i><a href="{{route('product',['post_slug' => $productHot->slug ])}}" title="{{$productHot->title}}">0</a></span>
                                    </div>
                                </div>

                            </div>
							@endif
                            @endforeach
                           
                        </div>
                </aside>
                <aside id="text-2" class="widget widget_text clearfix">
                    <h3 class="widget-title"><span>{{isset($information['title-sider-bar-2']) ? $information['title-sider-bar-2'] : ''}} &#038; Zalo -IMessage</span></h3>
                    <div class="textwidget">
                        <div class="content wk-content clearfix">
                            <h1><span style="font-size:18px"><strong>Call hoặc SMS: {{isset($information['zalo']) ? $information['zalo'] : ''}}</strong></span></h1>
                            <p>
                                <a title="fpttelecom.com.vn" href="../"></a>
                            </p>
                        </div>
                    </div>
                </aside>
                <aside id="colormag_125x125_advertisement_widget-2" class="widget widget_125x125_advertisement clearfix">
                    <div class="advertisement_125x125">
                        <div class="advertisement-title">
                            <h3 class="widget-title"><span>Thành Viên</span></h3> </div>
                        <div class="advertisement-content">
                            <a href="#" class="single_ad_125x125" target="_blank" rel="nofollow">
                                <img src="http://localhost/fpt/wp-content/uploads/2018/05/01.jpg" width="125" height="125" alt="">
                            </a>
                            <a href="#" class="single_ad_125x125" target="_blank" rel="nofollow">
                                <img src="http://localhost/fpt/wp-content/uploads/2018/05/02.jpg" width="125" height="125" alt="">
                            </a>
                            <a href="https://themegrill.com/" class="single_ad_125x125" target="_blank" rel="nofollow">
                                <img src="http://localhost/fpt/wp-content/uploads/2018/05/04.jpg" width="125" height="125" alt="">
                            </a>
                            <a href="https://themegrill.com/" class="single_ad_125x125" target="_blank" rel="nofollow">
                                <img src="http://localhost/fpt/wp-content/uploads/2018/05/125x125_logo_fpt.jpg" width="125" height="125" alt="">
                            </a>
                        </div>
                    </div>
                </aside>
            </div>