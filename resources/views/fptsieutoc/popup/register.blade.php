<div class="modal fade modal-regist" id="modal-registration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content regist">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Đăng kí</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="button-lg">
                    <a href="" title=""><img src="{{ asset('vn3c/upload-img/btn-lg-google.jpg') }}" alt=""></a>
                    <a href="" title=""> <img src="{{ asset('vn3c/upload-img/btn-lg-fb.jpg') }}" alt=""></a>

                </div>
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Họ và tên:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Email:</label>
                        <input type="email" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Mật khẩu:</label>
                        <input type="password" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nhập lại MK</label>
                        <input type="password" class="form-control" id="recipient-name">
                    </div>



                    <div class="form-group">
                        <button type="" class="btn-submit">Đăng kí</button>
                    </div>

                </form>
            </div>
            <div class="modal-footer hidden-xs">
                <span class="left "><a href="" title="">Quên mật khẩu ? </a></span>
                <span class="right "><a href="" title="">Bạn có tài khoản ? </a><a href="" title="">Đăng nhập </a></span>
            </div>

            <div class="modal-footer hidden-md">
                <span class="left "><a href="" title="">Quên mật khẩu ? </a></span>
                <span class="right "><a href="" title="">Bạn có tài khoản ? </a><a href="" title="">Đăng nhập </a></span>
            </div>

        </div>
    </div>
</div>