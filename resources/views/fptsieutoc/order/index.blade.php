@extends('vn3c.layout.site')

@section('title','Đặt hàng')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '' )

@section('content')

    <link rel="stylesheet" href="{{ asset('vn3c/stylesheet/faq.css') }}" media="all" type="text/css"/>
    <section class="vn3cFaq">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="step">
                        <div class="cricle one">
                            <span>1</span>
                            Thông tin thanh toán
                        </div>
                        <div class="borderStep"></div>
                        <div class="cricle two">
                            <span>2</span>
                            Hoàn tất
                        </div>
                    </div>
                    <div class="contentTheme">
                        <h3 class="title">Thông tin theme</h3>
                        <table class="table">
                            <tbody>
                            <?php $sumPrice = 0;?>
                            @foreach($orderItems as $id => $orderItem)
                            <tr>
                                <td><a href="#" title="" class="delete"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                <td class="tdimg"><a href="#" title=""><img src="img/pr.png" alt=""></a></td>
                                <td><p class="title">theme {{ $orderItem->title }}</p>
                                    <span>Mã SP: {{ $orderItem->code }}</span>

                                </td>
                                <td class="qty">{{ $orderItem->quantity }}</td>
                                <td class="tdprice">Giá:
                                    @if (!empty($orderItem->discount))
                                        <p class="pricedisCont"><del>{{ number_format($orderItem->price , 0)}}</del></p><br>
                                        <span class="price">{{ number_format($orderItem->discount , 0) }} VNĐ</span>
                                       <?php  $sumPrice += $orderItem->discount; ?>
                                    @else
                                        <span class="price">{{ number_format($orderItem->price , 0) }} VNĐ</span>
                                        <?php  $sumPrice += $orderItem->price; ?>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            <tr class="totalsale">
                                <td colspan="3">
                                    Mã giảm giá <input type="text" name="" value="" placeholder="">
                                </td>
                                <td colspan="2">
                                    <p>Tổng tiền <span class="price">{{ number_format($sumPrice , 0) }} VNĐ</span></p>
                                    <span>Miễn phí hosting 3 tháng</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="contentPay">
                        <h3 class="title">Thông tin thanh toán</h3>
                        <div class="contentdes">
                            <p>Chúc mừng bạn đã mua hàng thành công</p>
                            <p>Để đơn hàng được khởi tạo , quý khách vui lòng thực hiện chuyển khoản tổng tiền <span class="price">{{ number_format($sumPrice , 0) }} đ</span> vào một trong những ngân hàng bên dưới.</p>
                            <p>Trong nội dung chuyển khoản  quý khách vui lòng điền duy nhất mã đơn hàng là : <span>Vn3c-{{ $orderItem->code }}</span></p>
                            <p>Nếu quý khách không thể ghi mã đơn hàng vào nội dung chuyển khoản ,quý khách vui lòng chụp hình ủy nhiệm chi và gửi vào email : vn3ctran@gmail.com với tựa để : Ủy nhiệm chi đơn hàng <span>Vn3c-{{ $orderItem->code }}</span></p>
                        </div>

                        @foreach (\App\Entity\SubPost::showSubPost('faq',20) as $id => $faq)
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="logobank">
                                        <a href="#" title="">
                                            <img src="{{ asset($faq->image) }}" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                    <div class="infobank">
                                        <h4 class="title">{{ $faq->title }}</h4>
                                        <p>
                                            <?= $faq['thong-tin-ngan-hang'] ?>
                                        </p>
                                        <p>Nội dung chuyển khoản :<span style="background-color:#ffffff">&nbsp;<strong><span style="color:#ff8d00">Vn3c-{{ $orderItem->code }}</span></strong></span></p>
                                    </div>
                                </div>

                            </div>
                        @endforeach

                        <div class="row">
                            <div class="gmai">
                                Chúng tôi đã gửi thông tin đơn hàng vào mail  của quý khách. Quý khách vui lòng kiểm tra cả inbox và spam trong email <br>
                                để đảm bảo có thể thấy thông tin. <br>
                                VN3C xin cám ơn!
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>

    </section>

@endsection
