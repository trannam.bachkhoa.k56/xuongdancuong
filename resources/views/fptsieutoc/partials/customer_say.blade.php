<!-- Testimonials -->
<div class="special-block-bg mgTop20">
	<div class="section-head">
		<h4>
			<span>1500+ Khách hàng tin dùng</span>
			Khách hàng nói về chúng tôi
		</h4>
	</div>
	<div class="testimonials owl-carousel">
		<!-- Testimonial 1 -->
		@foreach (\App\Entity\SubPost::showSubPost('khach-hang-noi-ve-chung-toi', 30) as $id => $customerSay)
		<div class="testimonial-item">
			<div class="testimonial-content">
				<div class="testimonial-text">
					<p>{!! $customerSay->description !!}</p>
				</div>
			</div>            
			<div class="testimonial-credits">
				<div class="testimonial-picture">
					<img src="{{ asset($customerSay->image) }}" alt="{{ $customerSay['ten-khach-hang'] }}"/>
				</div>              
				<div class="testimonial-author-info">
					<p class="testimonial-author">{{ $customerSay['ten-khach-hang'] }}</p>
					<p class="testimonial-firm">{{ $customerSay['chuc-vu'] }} | {{ $customerSay['website-khach-hang'] }}</p>
					<ul class="testimonial-stars">
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
						<li><i class="fa fa-star"></i></li>
					</ul>
				</div>
			</div>
		</div>
		@endforeach
		<!-- /Testimonial 1 -->
	</div>

</div>
<!-- /Testimonials -->