<!-- services -->
<div class="special-block-bg">
	<div class="section-head">
		<h4>
			<span>MOMA Giúp gì cho doanh nghiệp của bạn?</span>
			Dịch vụ của chúng tôi
		</h4>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<div class="services-list row">
				<!-- Service Item 1 -->
				@foreach (\App\Entity\Post::newPost('thiet-ke-website', 5) as $post)	
				<?php $date=date_create($post->created_at); ?>
				<div class="service-block col-md-6">
					<div class="row">
						<div class="service-icon col-md-4">
							@if (isset($post->image) && !empty($post->image))
								<img alt="{{ $post->title }}" src="{{ asset($post->image) }}" />
							@endif
						</div>
						<div class="service-text col-md-8">
							<h4>{{ $post->title }}</h4>
							<p class="justify">{{ $post->description }}</p>
						</div>
					</div>
					
				</div>
				@endforeach
				<!-- /Service Item 1 -->
			</div>
		</div>
	</div>
</div>
<!-- /services -->