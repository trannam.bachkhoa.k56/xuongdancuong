<!-- Clients Block -->
<div class="row pt-50">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="section-head">
			<h4>
				<span>Đối tác</span>
				Khách hàng đối tác của chúng tôi
			</h4>
		</div>
		<!-- Clients Carousel Block -->
		<div class="clients owl-carousel">
			@foreach (\App\Entity\SubPost::showSubPost('doi-tac-cua-chung-toi', 30) as $id => $testemorial)
				<div class="client-block">
					<a title="Logo">
						<img src="{{ asset($testemorial->image) }}" alt="Logo">
					</a>
				</div>
			@endforeach
		</div>
		<!-- /Clients Carousel Block -->
	</div>
</div>
<!-- /Clients Block -->