<!-- Widgets -->
<div class="col-lg-4 col-md-4">
	<div class="sidebar right">
		<!-- Widget -->
		<div class="widget">
			<h4 class="widget-title">Tin tức liên quan</h4>
			<ul class="widget-tabs">
				@foreach (\App\Entity\Post::newPost('thiet-ke-website', 15) as $post)	
				<?php $date=date_create($post->created_at); ?>
				<li>
					<div class="widget-content">
						<div class="widget-thumb">
							<a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}">
							@if (isset($post->image) && !empty($post->image))
								<img alt="{{ $post->title }}" src="{{ asset($post->image) }}" />
							@endif
						</div>

						<div class="widget-text">
							<h5><a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug ]) }}">{{ $post->title }} </a></h5>
							<span>{{ date_format($date,"d/m/Y H:i") }}</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>
		<!-- Widget / End-->
	</div>
</div>