@extends('vattucaycanh.layout.site')

@section('title', isset($category->title) ?$category->title : '')
@section('meta_description',  isset($category->description) ?$category->description : '')
@section('keywords', '')

@section('content')
  <div class="container">
  <ul class="breadcrumb" style="padding: 8px 10px;margin-bottom: 10px">
      <li><a href="/"><i class="fa fa-home"></i></a></li>
      <li><a>{{ isset($category['title']) ? $category['title'] : ''}}</a></li>
    </ul>
  <div class="row">
    @include('vattucaycanh.partials.sidebar')

    <div id="content" class="col-sm-9">
      @foreach($posts as $post)
      <div class="blog1 blog">
        <div class="blog_img"><img src="{{ isset($post['image']) ? $post['image'] : '' }}" alt="{{ isset($post['title']) ? $post['title'] : '' }}" /></div>
        <h4 class="p-name"><a href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}">{{ isset($post['title']) ? $post['title'] : '' }} </a></h4>
        <ul class="blog-meta">
          <li><i class="fa fa-clock-o"></i><span class="dt-published"><?php
$date=date_create("2013-03-15");
echo date_format($date,"Y/m/d");
?></span></li>      
        </ul>
        <p class="p-summary"></p>
        <p> {{ isset($post['description']) ? \App\Ultility\Ultility::textLimit($post['description'], 20) : '' }}  </p>
        <a class="u-url" href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}">Xem thêm</a> 
      </div>
      @endforeach
    </div>
    <!-- end blog-home -->

  
  </div>
</div>  
@endsection

