@extends('vattucaycanh.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
  <div class="container">
  <ul class="breadcrumb" style="padding: 8px 10px">
     <?php \App\Entity\Post::getBreadcrumb($post->post_id, $category->slug)?>
    </ul>
  <div class="row">
    @include('vattucaycanh.partials.sidebar')
    <div id="content" class="col-sm-9">
      <div class="blog1 blog">
        <div class="blog_img" style="height:auto"><img src="{{ isset($post['image']) ? $post['image'] : '' }}" alt="{{ isset($post['title']) ? $post['title'] : '' }}" title="{{ isset($post['title']) ? $post['title'] : '' }}" /></div>
        <h4 class="p-name"><a>{{ isset($post['title']) ? $post['title'] : '' }}</a></h4>
         <ul class="blog-meta">
          <li><i class="fa fa-clock-o"></i><span class="dt-published"><?php
$date=date_create("2013-03-15");
echo date_format($date,"Y/m/d");
?></span></li>      
        </ul>
        <p class="p-summary"></p>
        <p>{!! isset($post['content']) ? $post['content'] : '' !!}</p>

      

    </div>
    </div>
   
    <!-- end blog-sidebar -->

  </div>
</div>
@endsection


