@extends('vattucaycanh.layout.site')


@section('title', 'tim-kiem')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', '')

@section('content')
  <style>
.FitterHidden
{
	display:block;
}
  </style>
   <div class="container">
  <ul class="breadcrumb">
    <li><a href="/"><i class="fa fa-home"></i></a></li>
    <li><a>Tìm kiếm : {{ isset($_GET["word"]) ? $_GET["word"] : ''}}</a></li>
  </ul>
  <div class="row">
   <!-- SIDEBAR -->
    @include('vattucaycanh.partials.sidebar')

    <div id="content" class="col-sm-9">
      <h2 class="category-title">{{ isset($category['title']) ? $category['title'] : ''}}</h2>
      <div class="row category-banner">
        <div class="col-sm-12 category-image"><img src="{{ isset($information['banner-tim-kiem']) ? $information['banner-tim-kiem'] : 'vattucaycanh/image/banners/category-banner.jpg' }}" alt="Desktops" title="Desktops" class="img-thumbnail" /></div>
        <div class="col-sm-12 category-desc">{{ isset($category['description']) ? $category['description'] : ''}}</div>
      </div>
      <div class="category-page-wrapper">
      <div class="grid-list-wrapper">
        <div class="product-layout product-list col-xs-12">
            
           
			
			@foreach ($products as $id => $product)
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-grid-item">
			   
				@include('vattucaycanh.partials.item-product')
			</div>
                  
            @endforeach

        </div>
      </div>
      <div class="category-page-wrapper">
        
        <div class="pagination-inner">
            @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                {{ $products->links() }}
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
