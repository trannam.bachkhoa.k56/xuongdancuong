




<div class="product-thumb transition">
  <div class="image product-imageblock"> 
	<div class="CropImg CropImg100">
        <div class="thumbs">
			<a href="{{ route('product',['cate_slug' => $product->slug]) }}">
				<img src="{{ isset($product['image']) ? $product['image'] : 'vattucaycanh/image/product/product4.jpg' }}" alt="{{ isset($product['title']) ? $product['title'] : ''}}" title="{{ isset($product['title']) ? $product['title'] : ''}}"  class="img-responsive" />
			</a>
		</div>
    </div>
	
    <!-- <div class="button-group">
		<button type="button" class="wishlist" data-toggle="tooltip" title="Xem chi tiết" ><a href="{{ route('product',['cate_slug' => $product->slug]) }}"><i class="fa fa-search"></i></a></button>
      <form onsubmit="return addToOrder(this);" method="post" accept-charset="utf-8" id="" enctype="multipart/form-data" style="display:inline-block">
       {{ csrf_field() }}
      <input type="hidden" class="input_quantity" id="input_quantity" name="quantity[]" value="1"/>
      <input type="hidden" class="input_quantity" id="input_quantity" name="product_id[]" value="{{ $product->product_id }}"> 
   
		<button type="submit" class="addtocart-btn" >Giỏ hàng</button>
		<button type="submit" class="compare" data-toggle="tooltip" title="Thêm vào giỏ hàng" ><i class="fa fa-shopping-cart"></i></button>
      </form>
	  
    </div> -->

  </div>
  <div class="caption product-detail">
    <h4 class="product-name"><a href="{{ route('product',['cate_slug' => $product->slug]) }}" title="{{ isset($product['title']) ? $product['title'] : ''}}">{{ isset($product['title']) ? $product['title'] : ''}}</a></h4>
  
  <p class="price product-price">Giá : liên hệ</p>

	<br>
	<div class="rating">
	@if(isset($product['danh-gia-sao']))
		<?php 
			$starBlack = 5 - $product['danh-gia-sao'];
		?>
		@for($i = 0 ;$i < $product['danh-gia-sao'];$i++)
			<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> 
		@endfor
		
		@for($i = 0 ;$i < $starBlack ;$i++)
			<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
		@endfor
	@else
		@for($i = 0 ;$i < 5 ;$i++)
			<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
		@endfor
	@endif
	</div>


  
    
  </div>

    <!-- <div class="button-group">
		<button type="button" class="wishlist" data-toggle="tooltip" title="Xem chi tiết" ><a href="{{ route('product',['cate_slug' => $product->slug]) }}"><i class="fa fa-search"></i></a></button>
      <form onsubmit="return addToOrder(this);" method="post" accept-charset="utf-8" id="" enctype="multipart/form-data" style="display:inline-block">
       {{ csrf_field() }}
      <input type="hidden" class="input_quantity" id="input_quantity" name="quantity[]" value="1"/>
      <input type="hidden" class="input_quantity" id="input_quantity" name="product_id[]" value="{{ $product->product_id }}"> 
   
		<button type="submit" class="addtocart-btn" >Giỏ hàng</button>
		<button type="submit" class="compare" data-toggle="tooltip" title="Thêm vào giỏ hàng" ><i class="fa fa-shopping-cart"></i></button>
      </form>
	  
    </div> -->
</div>



 <!-- <div class="item">
                <div class="product-thumb transition">
                  <div class="image product-imageblock"> <a href="product.html"><img src="image/product/product1.jpg" alt="lorem ippsum dolor dummy" title="lorem ippsum dolor dummy" class="img-responsive" /> </a>
                    <div class="button-group">
                      <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                      <button type="button" class="addtocart-btn" >Add To Cart</button>
                      <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                    </div>
                  </div>
                  <div class="caption product-detail">
                    <h4 class="product-name"><a href="#" title="lorem ippsum dolor dummy">lorem ippsum dolor dummy</a></h4>
                    <p class="price product-price">$122.00<span class="price-tax">Ex Tax: $100.00</span></p>
					
                    <div class="rating"> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                  </div>
                  <div class="button-group">
                    <button type="button" class="wishlist" data-toggle="tooltip" title="Add to Wish List" ><i class="fa fa-heart-o"></i></button>
                    <button type="button" class="addtocart-btn" >Add To Cart</button>
                    <button type="button" class="compare" data-toggle="tooltip" title="Compare this Product" ><i class="fa fa-exchange"></i></button>
                  </div>
                </div>
              </div>-->


               