<div id="brand_carouse" class="owl-carousel brand-logo">
    @foreach(\App\Entity\SubPost::showSubPost('doi-tac', 20) as $id => $partner)
        <div class="item text-center"> <a href="#"><img src="{{ isset($partner['image']) ? $partner['image'] : '' }}" alt="{{ isset($partner['title']) ? $partner['title'] : '' }}" class="img-responsive" /></a> </div>
    @endforeach   
</div>