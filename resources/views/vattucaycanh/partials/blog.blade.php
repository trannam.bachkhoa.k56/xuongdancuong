<div class="blog">
    <div class="blog-heading">
        <h3><?php $categoryNew = \App\Entity\Category::getDetailCategory('tin-tuc'); ?>
          {{ $categoryNew['title'] }}
        </h3>
    </div>

    <div class="blog-inner box">
        <ul class="list-unstyled blog-wrapper" id="latest-blog">
            @foreach(\App\Entity\Post::categoryShow('tin-tuc',8) as $post)
            <?php $date=date_create($post['updated_at']); ?>
                <li class="item blog-slider-item">
                    <div class="panel-default">
                        <div class="blog-image">
                            <div class="">
                                <div class="CropImg CropImg60">
                                    <div class="thumbs">
                                        <a href="{{ route('post', ['cate_slug' => 'tin tức', 'post_slug' => $post->slug]) }}" class="blog-imagelink"><img src="{{ isset($post['image']) ? $post['image'] : ''}}" alt="{{ isset($post['title']) ? $post['title'] : ''}}"></a>
                                    </div>
                                </div>
                            </div> <span class="blog-hover"></span> <span class="blog-date"><?php  echo date_format($date,"Y/m/d"); ?></span> <span class="blog-readmore-outer"><a href="{{ route('post', ['cate_slug' => 'tin tức', 'post_slug' => $post->slug]) }}" class="blog-readmore">Xem chi tiết</a></span> </div>
                        <div class="blog-content">
                            <a href="{{ route('post', ['cate_slug' => 'tin tức', 'post_slug' => $post->slug]) }}" class="blog-name">
                                <h2>{{ isset($post['title']) ? $post['title'] : ''}}</h2>
                            </a>
                            <div class="blog-desc">{{ isset($post['description']) ? \App\Ultility\Ultility::textLimit($post['description'], 30) : '' }}   </div>
                            <a href="{{ route('post', ['cate_slug' => 'tin tức', 'post_slug' => $post->slug]) }}" class="blog-readmore">Xem thêm</a>
                            <span class="blog-date"><?php  echo date_format($date,"Y/m/d"); ?></span>
                        </div>
                    </div>
                </li>
                @endforeach

        </ul>
    </div>
</div>
