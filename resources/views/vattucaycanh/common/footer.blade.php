<footer>
  <div class="container">
    <div class="row">
      <div class="footer-top-cms">
        <div class="col-sm-7">
          <div class="newslatter">
           

             <form class="" onsubmit="return subcribeEmailSubmit(this)">
                {{ csrf_field() }}
              <h5>Đăng kí nhận email</h5>
              <div class="input-group">
                <input type="text" class=" form-control emailSubmit" placeholder="vui lòng nhập email ...">
                <button type="submit" value="Sign up" class="btn btn-large btn-primary">Đăng kí</button>
              </div>
            </form>
          </div>
        </div>
        <div class="col-sm-5">
          <div class="footer-social">
            <h5>Xã hội</h5>
            <ul>
              <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
              <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li class="gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4 footer-block">
        <h5 class="footer-title">Thông tin</h5>
        <ul class="list-unstyled ul-wrapper">
           @foreach (\App\Entity\Menu::showWithLocation('footer-first') as $MainmenuFooter)
            @foreach (\App\Entity\MenuElement::showMenuPageArray($MainmenuFooter->slug) as $id=>$menufooter)
               <li><a href="{{ $menufooter['url'] }}">{{ $menufooter['title_show'] }}</a></li>
            @endforeach
           @endforeach
          
        </ul>
      </div>

      <div class="col-sm-4 footer-block">
        <div class="content_footercms_right">
          <div class="footer-contact">
            <h5 class="contact-title footer-title">Liên hệ</h5>
            <ul class="ul-wrapper">
              <li><i class="fa fa-map-marker"></i><span class="location2">{{ isset($information['dia-chi']) ?  $information['dia-chi'] : '' }}</span></li>
              <li><i class="fa fa-envelope"></i><span class="mail2"><a href="#">{{ isset($information['email']) ?  $information['email'] : '' }}</a></span></li>
              <li><i class="fa fa-mobile"></i><span class="phone2">{{ isset($information['so-dien-thoai']) ?  $information['so-dien-thoai'] : '' }}</span></li>
            </ul>
          </div>
        </div>
      </div>

       <div class="col-sm-4 footer-block">
        <h5 class="footer-title">Fage facebook</h5>
        <div style="width: 100%">
          {!! isset($information['fage-facebook']) ?  $information['fage-facebook'] : '' !!}
          
        </div>
      </div>
    </div>
  </div>
  <a id="scrollup">Scroll</a> </footer>
<div class="footer-bottom">
  <div class="container">
    <div class="copyright">{{ isset($information['copy-right']) ?  $information['copy-right'] : '' }}</div>
    
  </div>
</div>
<!-- <script src="{{ asset('vattucaycanh/javascript/parally.js')}} " type="text/javascript"></script>

<script>
$('.parallax').parally({offset: -40});
</script> -->