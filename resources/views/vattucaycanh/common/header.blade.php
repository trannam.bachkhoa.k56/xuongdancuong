<!-- <div class="preloader loader" style="display: block; background:#f2f2f2;"> <img src="vattucaycanh/image/loader.gif"  alt="#"/></div> -->
<header>
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
            
         <div class="top-left pull-left pd-10">
           <ul class="list-inline mgbottom0">
                  <li><span style="    display: inline-block;
    padding-top: 4px;"><i class="fa fa-mobile" aria-hidden="true"></i> Hotline:{{ isset($information['dia-chi']) ?  $information['so-dien-thoai'] : '' }}</span></li>
                <li class="pdleft15"><span style="    display: inline-block;
    padding-top: 4px;"><i class="fa fa-map-marker"></i> Địa chỉ : {{ isset($information['dia-chi']) ?  $information['dia-chi'] : '' }}</span></li>
              </ul>
            
          </div>

          <div class="top-right  mdds-none mbds-block">
              <div class="formMenu">
                 <form action="{{ route('search_product') }}" method="get" id="headearch" class="input-group search-bar search_form">
                <input class="input-text" placeholder="Tìm kiếm .." type="search"  name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}">
                <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
              </div>
              
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="header-inner">
      <div class="col-md-3 col-sm-12 col-xs-6 header-left">
        <div class="shipping text-ct">
          <!-- <div class="shipping-img"></div>
          <div class="shipping-text">{{ isset($information['so-dien-thoai']) ?  $information['so-dien-thoai'] : '' }}
          <br>
            <span class="shipping-detail">{{ isset($information['thoi-gian-mo-cua']) ?  $information['thoi-gian-mo-cua'] : '' }}</span></div> -->
            <div id="logo"> <a href="/">
              <img src="{{ isset($information['logo']) ?  $information['logo'] : '' }}" title="{{ isset($information['ten-website']) ?  $information['ten-website'] : '' }}" alt="{{ isset($information['ten-website']) ?  $information['ten-website'] : '' }}" class="img-responsive" />
             <!--  <img src="vattucaycanh/image/logo.png"> -->
            </a> </div>
            

        </div>
      </div>
      <div class="col-md-9 col-sm-12 col-xs-12 header-middle">
        <div class="header-middle-top">
          <div class="row">
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
                <img src="{{ asset('vattucaycanh/image/1.png')}}">
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                  GIAO HÀNG NHANH CHÓNG
                </h3>
                <p class="mgbottom0">
                 {{ isset($information['giao-hang-nhanh-chong']) ?  $information['giao-hang-nhanh-chong'] : '' }}
                </p>
              </div>
            </div>
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
			    <img src="{{ asset('vattucaycanh/image/2.png')}}">
              
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                 HỖ TRỢ 24/7
                </h3>
                <p class="mgbottom0">
                {{ isset($information['ho-tro-24-7']) ?  $information['ho-tro-24-7'] : '' }}
                </p>
              </div>
            </div>
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
			    <img src="{{ asset('vattucaycanh/image/3.png')}}">
             
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                 GIỜ LÀM VIỆC
                </h3>
                <p class="mgbottom0">
                {{ isset($information['gio-lam-viec']) ?  $information['gio-lam-viec'] : '' }}
                </p>
              </div>
            </div>
            <div class="col-sm-6 iconHerderCenter">
              <div class="img">
			    <img src="{{ asset('vattucaycanh/image/4.jpg')}}">
               
              </div>
              <div class="text">
                <h3 class="mgbottom0">
                  UY TÍN, CHẤT LƯỢNG
                </h3>
                <p class="mgbottom0">
                  {{ isset($information['uy-tin-chat-luong']) ?  $information['uy-tin-chat-luong'] : '' }}
                </p>
              </div>
            </div>

          </div>
          
        </div>
      </div>
      <div class="col-md-2 col-sm-12 col-xs-12 header-right">
        <div class="formMenu mdds-none msds-block mbds-none">
                 <form action="{{ route('search_product') }}" method="get" id="headearch" class="input-group search-bar search_form">
                <input class="input-text" placeholder="Tìm kiếm .." type="search"  name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}">
                <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
        </div>

         
      </div>
    </div>
  </div>
</header>
<nav id="menu" class="navbar">
  <div class="nav-inner container">
    <div class="navbar-header"><span id="category" class="visible-xs">Danh mục</span>
      <button type="button" class="btn btn-navbar navbar-toggle" ><i class="fa fa-bars"></i></button>
    </div>
    <div class="navbar-collapse">
      <ul class="main-navigation menuHeader">
        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
          @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
          <li><a href="{{ $menuelement['url'] }}" class="
            @if (!empty($menuelement['children']))
                active
            @endif
            parent">{{ $menuelement['title_show'] }}</a>
            @if (!empty($menuelement['children']))
            <ul>
              @foreach ($menuelement['children'] as $elementparent)
              <li><a href="{{ $elementparent['url'] }}">{{ $elementparent['title_show'] }}</a></li>
              @endforeach
            </ul>
            @endif
          </li>
          @endforeach
        @endforeach

        <div class="formMenu msds-none">
                 <form action="{{ route('search_product') }}" method="get" id="headearch" class="input-group search-bar search_form">
                <input class="input-text" placeholder="Tìm kiếm .." type="search"  name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}">
                <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
        </div>
      </ul>
    </div>
  </div>
</nav>