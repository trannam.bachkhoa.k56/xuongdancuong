

@extends('vattucaycanh.layout.site')

@section('title',isset($post->title) ? $post->title : '')
@section('meta_description', isset($information['meta_description']) ?$information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')

<div class="container">
  <ul class="breadcrumb" style="padding: 8px 10px">
      <li><a href="/"><i class="fa fa-home"></i></a></li>
      <li><a>{{ isset($post['title']) ? $post['title'] : ''}}</a></li>
    </ul>

  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="services">
          <div class="col-sm-12">
            <h4 class="tf">{{ isset($post['title']) ? $post['title'] : ''}}</h4>
            <p>{!! isset($post['content']) ? $post['content'] : ''  !!}</p>
           
          </div>

        </div>
      </div>
    </div>
  </div>
 


</div>
@endsection
