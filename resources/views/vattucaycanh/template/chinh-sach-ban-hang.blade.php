@extends('diamond.layout.site')

@section('title',isset($post->title) ? $post->title : '')
@section('meta_description', isset($information['meta_description']) ?$information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
@include('diamond.partials.slider')
    </div>
   <section class="detailnew">
      <div class="container">
        <div class="row bgdetail">
          <div class="col-12">
            <h1>
              {{$post->title}}
            </h1>
            <div class="content">
              <p><?= isset($post->content) ?$post->content : '' ?></p>
            </div>

          </div>
        </div>
      </div>
    </section>
@endsection