@extends('vn3c.layout.site')

@section('title',' Làm website tại vn3c chỉ với giá')
@section('meta_description', 'Làm website tại vn3c, xem chi tiết bảng giá báo giá website tại vn3c chuyên nghiệp nhất')
@section('keywords', '')

@section('content')
    <!-- banner -->
    <section class="banner" style="background-image: url({{ asset('vn3c/img/slide.jpg') }})">
        <p class="title-bn">Nếu bạn muốn website của bạn khác biệt</p>
        <p class="option-bn">lựa chọn nâng cấp dành cho website</p>
    </section>

    <section class="content-price">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 price-tb">
                    <h1>
                        Làm website tại vn3c chỉ với giá
                    </h1>
                    <p>Hãy lựa chọn gói giải pháp phù hợp với nhu cầu của bạn và dùng thử miễn phí 30 ngày</p>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 ">
                            <div class="item itemprice">
                                <div class="title2">
                                    <h4>Cơ bản</h4>
                                    <p>1.548.000 đ</p>
                                </div>
                                <div class="content-table">
                                    <ul>
                                        <li><a href="">Không hỗ trợ tên miền</a></li>
                                        <li><a href="">Sử dụng 1 năm</a></li>
                                        <li><a href="">Sử dụng giao diện miễn phí của vn3c</a></li>
                                        <li><a href="">Quản lý cơ bản</a></li>
                                        <li><a href="">Tự sinh bình luận website</a></li>
                                        <li><a href="">Tìm kiếm bài viết nhiều like và comment facebook</a></li>
                                        <li><a href="">Không nhập sản phẩm</a></li>
                                        <li><a href="">Không cài đặt chatbox</a></li>
                                        <li><a href="">không Chat online facebook</a></li>
                                        <li><a href="">Gửi email đồng loạt</a></li>
                                        <li><a href="">Tự động gửi kết bạn facebook</a></li>
                                    </ul>
                                </div>
                                <div class="button">
                                    <a href="/trang/tao-moi-website" title="">Dùng thử</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="item itemAdvanced">
                                <div class="title2">
                                    <h4>Nâng cao</h4>
                                    <p>5.000.000 đ</p>
                                </div>
                                <div class="content-table">
                                    <ul>
                                        <li><a href="">Tên miền <= 300.000đ</a></li>
                                        <li><a href="">Sử dụng 1 năm</a></li>
                                        <li><a href="">Thiết kế giao diện giới thiệu theo yêu cầu</a></li>
                                        <li><a href="">Quản lý cơ bản</a></li>
                                        <li><a href="">Tự sinh bình luận website</a></li>
                                        <li><a href="">Tìm kiếm bài viết nhiều like và comment facebook</a></li>
                                        <li><a href="">Hỗ trợ nhập dưới 50 sản phẩm</a></li>
                                        <li><a href="">Hỗ trợ cài đặt chatbox <br></a></li>
                                        <li><a href="">Hỗ trợ Chat online facebook <br></a></li>
                                        <li><a href="">Gửi email đồng loạt</a></li>
                                        <li><a href="">Tự động gửi kết bạn facebook</a></li>
                                    </ul>
                                </div>
                                <div class="button">
                                    <a href="/trang/tao-moi-website" title="">Dùng thử</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="item itemProfessional">
                                <div class="title2">
                                    <h4>Bán hàng chuyên nghiệp</h4>
                                    <p>7.000.000 đ</p>
                                </div>
                                <div class="content-table">
                                    <ul>
                                        <li><a href="">Tên miền <= 300.000đ</a></li>
                                        <li><a href="">Sử dụng 1 năm</a></li>
                                        <li><a href="">Thiết kế giao diện bán hàng theo yêu cầu</a></li>
                                        <li><a href="">Quản lý cơ bản</a></li>
                                        <li><a href="">Tự sinh bình luận website</a></li>
                                        <li><a href="">Tìm kiếm bài viết nhiều like và comment facebook</a></li>
                                        <li><a href="">Hỗ trợ nhập dưới 50 sản phẩm</a></li>
                                        <li><a href="">Hỗ trợ cài đặt chatbox <br></a></li>
                                        <li><a href="">Hỗ trợ Chat online facebook <br></a></li>
                                        <li><a href="">Gửi email đồng loạt</a></li>
                                        <li><a href="">Tự động gửi kết bạn facebook</a></li>
                                    </ul>
                                </div>
                                <div class="button">
                                    <a href="/trang/tao-moi-website" title="">Dùng thử</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <div class="item itemcustom">
                                <div class="title2">
                                    <h4>Đặc biệt</h4>
                                    <p>10.000.000 đ</p>
                                </div>
                                <div class="content-table">
                                    <ul>
                                        <li><a href="">Tên miền <= 300.000đ</a></li>
                                        <li><a href="">Sử dụng 1 năm</a></li>
                                        <li><a href="">Thiết kế giao diện bán hàng theo yêu cầu</a></li>
                                        <li><a href="">Quản lý cơ bản</a></li>
                                        <li><a href="">Tự sinh bình luận website</a></li>
                                        <li><a href="">Tìm kiếm bài viết nhiều like và comment facebook</a></li>
                                        <li><a href="">Hỗ trợ nhập dưới 1000 sản phẩm</a></li>
                                        <li><a href="">Hỗ trợ cài đặt chatbox <br></a></li>
                                        <li><a href="">Hỗ trợ Chat online facebook <br></a></li>
                                        <li><a href="">Gửi email đồng loạt</a></li>
                                        <li><a href="">Tự động gửi kết bạn facebook</a></li>
                                    </ul>
                                </div>
                                <div class="button">
                                    <a href="/trang/tao-moi-website" title="">Dùng thử</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 price-tb ">
                    <h5>Chú ý với gói cơ bản: <i>Phí cài đặt 200.000đ hỗ trợ cài đặt chatbox. 100.000đ hỗ trợ cài đặt chat facebook. Nếu yêu cầu nhập liệu thì sẽ có báo giá chi tiết hơn.</i></h5>
                </div>
                <div class="col-md-9 col-sm-12 price-tb ">
                    {!! $post->content !!}
                    <h6>Đăng ký ngay hôm nay: </h6>
                    <div id="getfly-optin-form-iframe-1527739324245"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=tWOH8rvOsLTLFmL7CYBFsg7kGefFXyLHuLXSKy9aJM1Y9mMdha&referrer="+r); f.style.width = "100%";f.style.height = "400px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1527739324245");s.appendChild(f); })(); </script>
                </div>
            </div>
        </div>
    </section>

@endsection

