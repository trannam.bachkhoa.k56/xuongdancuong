<div id="content-section-50" class="content-section white-section">
    <div class="section-container">
        <div class="container">
            <div class="">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 title-block">
                            <div class="title-block-container">
                                <h3>Giới thiệu</h3>
                                <div class="line-separator"></div>
                            </div>
                        </div>
                        <div class="col-md-12 content-block">
                            <div class="content-block-container">
                                <div class="image-block">
                                    <div class="image-block-container">
                                        <img src="{{ asset('laborExport/img/cong_ty.jpg') }}" alt="Image Block">
                                    </div>
                                </div>
                                <p><strong>Thông tin khái quát về công ty của chúng tôi: </strong></p>
                                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> 20 công ty tiếp nhận đang hợp tác.</p>
                                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> 102 thực tập sinh xuất cảnh năm ngoái</p>
                                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Dự kiến: 120 thực tập sinh (năm 2018)</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 title-block">
                            <div class="title-block-container">
                                <h3>Lịch sử phát triển</h3>
                                <div class="line-separator"></div>
                            </div>
                        </div>
                        <div class="col-md-12 timeline-block">
                            <div class="timeline-block-container">
                                <div class="timeline-block-item">
                                    <div class="timeline-block-item-container">
                                        <div class="timeline-footer">
                                            <div class="date-block">
                                                <div class="date-block-container">
                                                    <span><i class="fa fa-clock-o"></i>2004</span>
                                                </div>
                                            </div>
                                            <h4>Thành lập công ty</h4>
                                            <p>Quyết định số QDUB/849 được thành lập doanh nghiệp. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-block-item">
                                    <div class="timeline-block-item-container">
                                        <div class="timeline-footer">
                                            <div class="date-block">
                                                <div class="date-block-container">
                                                    <span><i class="fa fa-clock-o"></i>2008</span>
                                                </div>
                                            </div>
                                            <h4>Giấy phép xuất khẩu</h4>
                                            <p>Từ bộ lao động thương binh và xã hội đã quyết định cấp giấy phép xuất nhập khẩu.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="timeline-block-item">
                                    <div class="timeline-block-item-container">
                                        <div class="timeline-footer">
                                            <div class="date-block">
                                                <div class="date-block-container">
                                                    <span><i class="fa fa-clock-o"></i>2017</span>
                                                </div>
                                            </div>
                                            <h4>Xếp hạng Doanh nghiệp 5 Sao từ 5/2014 - 6/2017</h4>
                                            <p>Phái cử 1759 người trong đó có 274 người đến thị trường Nhật Bản.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 title-block">
                            <div class="title-block-container">
                                <h3>Khách hàng nói về chúng tôi</h3>
                                <div class="line-separator"></div>
                            </div>
                        </div>
                        <div class="owl-carousel owl-theme introductionSlide">
                            <div class="item">
                                <div class="swiper-slide swiper-slide-duplicate" 
                                     data-swiper-slide-index="2">
                                    <div class="swiper-slide-container">
										<div class="col-md-12 testimonials-block testimonials-block-style-2">
											<div class="testimonials-block-container">
												<div class="image-block">
													<div class="image-block-container">
														<img src="{{ asset('laborExport/ex_1/img/tran-thi-hoa.jpg') }}"
															 alt="Image Block">
													</div>
												</div>
												<div class="testimonials-block-title">
													<h4>Tran Thi Hoa</h4>
												</div>
												<div class="testimonials-block-desc">
													<p>Hiện mình đã làm tại Nhật Bản được 2 năm theo đơn hàng chế biến thuỷ sản của công ty OLECO. Lúc đầu sang đây do không quen khí hậu và thức ăn nên mình ốm suốt, nhưng may mà có các anh chị quản lý của công ty bên này giúp đỡ, chăm sóc tận tình.</p>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="swiper-slide swiper-slide-duplicate" 
                                     data-swiper-slide-index="2">
                                    <div class="swiper-slide-container">
                                        <div class="col-md-12 testimonials-block testimonials-block-style-2">
                                            <div class="testimonials-block-container">
                                                <div class="image-block">
                                                    <div class="image-block-container">
                                                        <img src="{{ asset('laborExport/ex_1/img/do-minh-anh.jpg') }}"
                                                             alt="Image Block">
                                                    </div>
                                                </div>
                                                <div class="testimonials-block-title">
                                                    <h4>Do Minh Anh</h4>
                                                </div>
                                                <div class="testimonials-block-desc">
                                                    <p>Mình mới về nước sau 3 năm làm việc tại Nhật. Mình từng làm chế biến thuỷ sản tại Yamaguchi, theo đơn hàng của OLECO. Vào những thời gian cao điểm thì công việc cũng khá vất vả, nhưng được cái thu nhập cao hơn nhiều so với ở nhà mà lại ổn định.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>