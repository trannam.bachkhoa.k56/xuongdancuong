<section class="author bgGray">
	<div class="container mb30">
		<h2>ĐƯỢC SỰ TÍN NHIỆM <br>
		CỦA CÁC CHUYÊN GIA MARKETING HÀNG ĐẦU THẾ GIỚI VÀ VIỆT NAM</h2>
	</div>
	<div class="container">
		<div class="owl-carousel owl-theme" id="customerSay">
			@foreach (\App\Entity\SubPost::showSubPost('khach-hang-noi-ve-chung-toi', 30) as $id => $customerSay)
				<div class="item">
					<div class="avatar">
						<img src="{{ asset($customerSay->image) }}"/>
					</div>
					<div class="CustomSay">
						{!! $customerSay->description !!}
					</div>
					<h4>{{ $customerSay['ten-khach-hang'] }}</h4>
					<p>{{ $customerSay['chuc-vu'] }} | <span class="orange">{{ $customerSay['website-khach-hang'] }}</span></p>
				</div>
			@endforeach
		</div>
	</div>
</section>
<script>
    $(document).ready(function () {
        $('#customerSay').owlCarousel({
            items: 1,
            nav:true,
            dotData: true,
            autoplay: true
        });
    });
</script>