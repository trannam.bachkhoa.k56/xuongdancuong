<!-- Modal -->
<div class="modal fade" id="myModalForget">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user" aria-hidden="true"></i> Quên mật khẩu</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('forget_password') }}" class="submitDelete" method="post" >
                    {!! csrf_field() !!}
                    <div class="form-group col-xs-12">
                        <input id="email" type="email" class="form-control" name="email"  placeholder="Nhập email đăng ký" required autofocus>
                    </div>
                    <div class="form-group col-xs-12">
                        <button type="submit" class="btn btn-primary">Gửi</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<style>
    .modal-content_1 {
        background: #fff;
    }
    .modal-body {
        background: #fff;
    }
</style>
<script>
    function forgetPassword() {
        $('#myModalForget').modal('show');
        $('#modal-login').modal('hide');
    }
</script>