<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta name="ROBOTS" content="index, follow"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta itemprop="image" content="@yield('meta_image')" />
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:locale" content="vi_VN"/>
	<meta property="og:type" content="@yield('type_meta')"/>
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('meta_description')" />
    <meta property="og:url" content="@yield('meta_url')" />
    <meta property="og:image" content="@yield('meta_image')" />
    <meta property="og:image:secure_url" content="@yield('meta_image')" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="@yield('meta_description')" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:image" content="@yield('meta_image')" />
	<link rel="shortcut icon" href="/favicon.ico">
    {{--<link rel="stylesheet" href="{{ asset('linkmedia/css/bootstrap.min.css') }}" media="all" type="text/css"/>--}}
    {{--<link rel="stylesheet" href="{{ asset('linkmedia/css/font-awesome.min.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/slicknav.min.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/cubeportfolio.min.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/magnific-popup.min.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/jquery.fancybox.min.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/niceselect.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/owl.theme.default.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/owl.carousel.min.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/slickslider.min.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/reset.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/style.css') }}" media="all" type="text/css"/>--}}
	{{--<link rel="stylesheet" href="{{ asset('linkmedia/css/color.css') }}" media="all" type="text/css"/>--}}
	{{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,800" rel="stylesheet">--}}
    <link href="/public/site/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="/public/site/jquery-3.3.1.min.js"></script>
    <script src="/public/site/js/bootstrap.min.js"></script>
    <link href="/public/site/style.css" rel="stylesheet" type="text/css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU'
          crossorigin='anonymous'>
</head>
<body>
@include('linkmedia-01.common.header')
@yield('content')
@include('linkmedia-01.common.footer')
</body>
</html>