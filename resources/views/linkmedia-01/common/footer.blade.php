<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-left" style="padding-top:30px;padding-bottom: 20px;">
                    <p>CÔNG TY CỔ PHẨN LINKMEDIA VIỆT NAM</p>
                    <p>Địa chỉ : Số 9B, Ngõ 1, Lê Văn Thiêm,Hà Nội</p>
                    <p>Điện thoại : 093 455 3435</p>
                    <p>Email : Info@linkmedia.com</p>
                    <p>Website : WWW.linkmedia.com.vn</p>
                </div>
                <div class="col-md-6 text-left" style="padding-top:30px">
                    <p style="padding-top:10px"><a href="#" style="color: white">- Thông tin thanh toán</a></p>
                    <p style="padding-top:10px"><a href="#" style="color: white">- Bảng giá thiết kế website</a></p>
                    <p style="padding-top:10px"><a href="#" style="color: white">- Bảng giá hosting</a></p>
                    <p style="padding-top:10px"><a href="#" style="color: white">- Bảng giá tên miền</a></p>
                </div>
            </div>
        </div>
    </footer>
    <div class="container text-center">
        <h5 style="color:rgb(63, 63, 63)">CopyRight 2018 Bản quyền thuộc về Linkmedia Việt Nam</h5>
    </div>