<header>
        <div class="menu1">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link" href="#">Giới thiệu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Dịch vụ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Giải pháp</a>
                </li>
                <li class="nav-item" style="padding-left: 30px;">
                    <img id="logo" src="/public/site/image/logo.png" alt="logo">
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Sản phẩm</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Tin tức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Liên hệ</a>
                </li>
            </ul>
        </div>
        <div class="menu2">
            <div class="menu">
                <nav class="navbar navbar-light navbar-expand-md">
                    <a class="navbar-brand" href="#" style="color:#34cded">Linkmedia</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar1">
                        <hr>
                        <a class="nav-link" href="#">Giới thiệu</a>
                        <hr>
                        <a class="nav-link" href="#">Dịch vụ</a>
                        <hr>
                        <a class="nav-link" href="#">Giải pháp</a>
                        <hr>
                        <a class="nav-link" href="#">Sản phẩm</a>
                        <hr>
                        <a class="nav-link" href="#">Tin tức</a>
                        <hr>
                        <a class="nav-link" href="#">Liên hệ</a>
                    </div>
                </nav>
            </div>
        </div>
    </header>