@extends('linkmedia-01.layout.site')

@section('type_meta', 'website')
@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description', !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
    <!-- banner -->
    <!--<section class="banner" style="background-image: url({{ asset($category->image) }})">
       {!! $category->description !!}
    </section>-->
	 @include('vn3c.partials.slide')

    <section class="list-category"	>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu">
                        @foreach (\App\Entity\Menu::showWithLocation('menu-product') as $menu)
                            {!! \App\Entity\MenuElement::showMenuElementPage($menu->slug, '') !!}
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="Product bgGray pr-category">
        <div class="container">

            <div class="ft-search">
                <form class="form-inline center" method="get">
                    <input class="form-control input-search" type="search" placeholder="Nhập từ khóa tìm kiếm ..." aria-label="Search" name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" />
                    <button class="btn" type="submit">Tìm kiếm</button>

                </form>
            </div>
			<div class="row">
				<div class="col-12">
					<h1>{{ $category->title }}</h1>
					<p>{!! $category->description !!}</p>
				</div>
			</div>
            <div class="listProduct">
				

                <div class="tab-content" id="ListProduct">

                    <div class="row product-row">
                        @foreach ($products as $id => $product)
                        <div class="col-lg-4 col-md-6 col-sm-12 item">
                            <figure class="pro-item">
                                <a href="{{ route('product', ['post_slug' => $product->slug]) }}">
                                    <img src="{{ asset($product->image) }}"/>
                                </a>
                                <figcaption>
                                    <div class="button">
                                        <button type="button" class="btn btn-warning">
											<a href="{{ route('productDemo', ['post_slug' => $product->slug]) }}">Xem demo</a>
										</button>
										<button type="button" class="btn btn-primary">
											<a href="{{ route('product', ['post_slug' => $product->slug]) }}">Xem thêm</a>
										</button>
                                    </div>
                                </figcaption>
                                <h3>
                                    <a href="{{ route('product', ['post_slug' => $product->slug]) }}">{{ $product->title }}</a>
                                </h3>
                            </figure>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div><!-- end listproduct -->

            <div class="show-pr">
                @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{ $products->links() }}
                @endif
                {{--<a href="" title="" class="hvr-radial-in">Xem thêm</a>--}}
            </div>

        </div>
    </section>


@endsection
