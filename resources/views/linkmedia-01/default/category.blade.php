@extends('vn3c.layout.site')

@section('type_meta', 'website')
@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description', !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
    <div class="landing text-center">
        <div style="padding-top:190px">
            <b style="font-size: 70px">Tin tức</b>
            <p style="font-size : 23px"><a href="#" style="color:white">Trang chủ </a><i class="fas fa-chevron-right"></i>
                Tin tức</p>
        </div>
    </div>
    <div class="container" style="padding-top:130px">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <div class="sidebar">
                    <div class="search-bar" style="display: inline;">
                        <input type="text" placeholder="Search something..." size="28">
                        <a href="#"><i class="fas fa-search"></i></a>
                    </div>
                    <hr>
                    <div class="categori" style="padding-bottom:70px;padding-top: 30px;">
                        <div class="header">
                            <h3 class="title">Categories</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li><a href="#">Business<span>(15)</span></a></li>
                                <li><a href="#">Corporate<span>(12)</span></a>
                                <li><a href="#">Creative<span>(15)</span></a></li>
                                <li><a href="#">Technology<span>(18)</span></a></li>
                                <li><a href="#">Development<span>(20)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="recentPost" style="padding-bottom: 70px">
                        <div class="header">
                            <h3 class="title">Recent Post</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li class="post">
                                    <div class="post-cate">
                                        <img src="/public/site/image/image7.jpg" width="100%" height="100%">
                                    </div>
                                    <div class="post-content">
                                        <div class="post-title"><a href="#"">Lorem ipsum dolor</a></div>
                                        <div class="
                                                time"><a href="#">Ronchi / 06 Jun, 2016</a></div>
                                        <p>Lorem must explain to ten how mistakenea</p>
                                    </div>
                                </li>
                                <li class="post">
                                    <div class="post-cate">
                                        <img src="/public/site/image/image6.jpg" width="100%" height="100%">
                                    </div>
                                    <div class="post-content">
                                        <div class="post-title"><a href="#;">Lorem ipsum dolor</a></div>
                                        <div class="time"><a href="#">Ronchi / 06 Jun, 2016</a></div>
                                        <p>Lorem must explain to ten how mistakenea</p>
                                    </div>
                                </li>
                                <li class="post">
                                    <div class="post-cate">
                                        <img src="/public/site/image/image5.jpg" width="100%" height="100%">
                                    </div>
                                    <div class="post-content">
                                        <div class="post-title"><a href="#">Lorem ipsum dolor</a>
                                            <div class="time"><a href="#">Ronchi / 06 Jun, 2016</a></div>
                                            <p>Lorem must explain to ten how mistakenea</p>
                                        </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="categori" style="padding-bottom:70px;">
                        <div class="header">
                            <h3 class="title">Archive</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li><a href="#">Cooperation<span>(1)</span></a></li>
                                <li><a href="#">Design<span>(3)</span></a>
                                <li><a href="#">Event & Festivals<span>(3)</span></a></li>
                                <li><a href="#">Links<span>(1)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="categori" style="padding-bottom:70px;">
                        <div class="header">
                            <h3 class="title">Last Tweets</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li>
                                    <div class="tweet">
                                        <p><a href="#">@Lorem Ipsum</a>
                                            dolor sit amet, costetur adipiscing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tweet">
                                        <p><a href="#">@Lorem Ipsum</a>
                                            dolor sit amet, costetur adipiscing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tweet">
                                        <p><a href="#">@Lorem Ipsum</a>
                                            dolor sit amet, costetur adipiscing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tag">
                        <div class="header">
                            <h3 class="title">Tag</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li><a href="#">Forum sedding</a></li>
                                <li><a href="#">Web Design</a></li>
                                <li><a href="#">Google ads</a></li>
                                <li><a href="#">Seo onpage</a></li>
                                <li><a href="#">Email Marketing</a></li>
                                <li><a href="#">SMS marketing</a></li>
                                <li><a href="#">Marketing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-9">
                <div class="row">
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image5.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image6.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image7.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image5.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image6.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image7.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image5.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <div class="col-md-6 col-sm-12" style="padding-bottom:40px">
                        <div class="post-image">
                            <img src="/public/site/image/image6.jpg" alt="post" width="100%">
                        </div>
                        <div class="post-caption">
                            <h4 style="margin-top: -8px;"><a href="#">Lorem ipsum dolor sit amet conse</a></h4>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p>Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="post-text">
                            <p>Lorem must explain to you how all this mistakenea of denouncing pleasure and praising
                                pain
                                was rnad I will give you a complete pain was praising</p>
                        </div>
                        <a href="#"><b>READ MORE</b> <i class="fas fa-chevron-right"></i><i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="pagination pl15">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

