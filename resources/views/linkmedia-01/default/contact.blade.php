@extends('linkmedia-01.layout.site')

@section('title','Tạo website tại vn3c liên hệ')
@section('meta_description', 'Tạo website tại vn3c vui lòng liên hệ với chúng tôi thông qua form sau...')
@section('keywords', $information['meta_keyword'])

@section('content')
	<div class="landing-contact text-center">
		<div style="padding-top:180px">
			<b style="font-size: 60px">LIÊN HỆ</b>
			<p style="font-size : 23px"><a href="#" style="color:white">Trang chủ </a><i class="fas fa-chevron-right"></i>
				Liên hệ</p>
		</div>
	</div>
	<div class="contact-form" style="padding-top:120px;padding-bottom: 70px;">
		<div class="container">
			<div class="address-box">
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="form-contact">
							<b style="font-size:25px;">GET IN TOUCH</b>
							<form style="padding-top:20px;">
								<input type="text" placeholder="    Name*" size="40">
								<input type="email" placeholder="   E-mail*" size="40">
								<input type="text" placeholder="   Subject" size="40">
								<textarea placeholder="   Message" cols="42" rows="5"></textarea>
								<div class="button-submit">
									<button class="btn btn-default" type="submit">Submit Message</button>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="company-info">
							<b style="font-size:25px;">CONTACT FORM</b>
							<div class="company-address">
								<ul class="info">
									<li>
										<p><i class="fas fa-map-marker-alt"></i> House No 08, Road No 08, <br>
											Din Bari, Dhaka, Bangladesh</p>
									</li>
									<li>
										<p><i class="far fa-envelope"></i> Username@gmail.com <br>
											Damo@gmail.com</p>
									</li>
									<li>
										<p><i class="fas fa-mobile-alt"></i> +660 256 24857<br>
											+660 256 24857</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
