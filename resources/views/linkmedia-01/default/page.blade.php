@extends('vn3c.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )

@section('content')
    <section class="banner-detail-new" style="background-image: url({{ asset($post->image) }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="title-ct-new">
                        {!! $post->title !!}
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="content-detail-new mb30">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="content-it-ct">
						<div class="mb20 mt20">
							<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-541a6755479ce315"></script>
							<!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
						</div>
                        <div class="mb20 bold description">{{ $post->description }}</div>
                        <div class="contentView">
							{!! $post->content !!}
						</div>
						<div class="optin">
							{!! $post['optinform-dang-ky'] !!}
						</div>
                    </div>

                </div>
            </div>

        </div>
    </section>
@endsection

