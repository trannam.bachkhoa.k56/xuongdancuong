@extends('linkmedia-01.layout.site')

@section('type_meta', 'website')
@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
    <div class="landing text-center">
        <div style="padding-top:190px">
            <b style="font-size: 55px;">Chi tiết tin tức</b>
            <p style="font-size : 23px"><a href="#" style="color:white">Trang chủ </a><i class="fas fa-chevron-right"></i>
                Chi tiết tin tức</p>
        </div>
    </div>
    <div class="container" style="padding-top:130px;">

        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div class="single">
                    <div class="post-header">
                        <img src="/public/site/image/banner.jpg" alt="" width=100%>
                    </div>
                    <div class="post-body">
                        <div class="post-caption">
                            <p style="margin:-5px 5px 1px 5px;font-size: 20px">Lorem ipsum dolor sit amet conse</p>
                            <div class="post-time">
                                <small>05</small>
                                <p class="month">JUN</p>
                            </div>
                            <p style="margin-left:5px">Ricardo / Business, at 3 pm</p>
                        </div>
                        <div class="single-post-content" style="padding-top:10px;color:#6d6d6d">
                            <p>New trend in UI and UX ipsum dolor sit amet,
                                consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                labore et dolore magna iqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                commodo consequat. Duis aute irure dolor in reprehenderit
                                in voluptate velit esse cillum dolore eu fugiat nulla pariatur
                                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.</p>
                            <br>
                            <p>UI and UX should be the consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna iqua. Ut enim ad minim
                                veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <br>
                            <blockquote><i>UI and UX should be the consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna iqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                    labris nisi ut aliquip ex ea commodo consequat quis nostrud exercitation
                                    ullamco.</i>
                                <br>
                            </blockquote>
                            <br>
                            <p>There are some design tends ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna iqua. Ut enim ad minim
                                veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>
                        </div>
                        <br>
                    </div>
                    <div class="post-footer" style="padding-bottom : 100px">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="post-tag">
                                    <ul>
                                        <li>
                                            <h4>Tags :</h4>
                                        </li>
                                        <li><a href="#">Design,</a></li>
                                        <li><a href="#"> Corporate,</a></li>
                                        <li><a href="#"> Business,</a></li>
                                        <li><a href="#">Ideas</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="post-share">
                                    <ul>
                                        <li>
                                            <h4>Share:</h4>
                                        </li>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <h4>Comment</h4>
                        <br>
                        <div class="post-comment">
                            <ul class="comment-list">
                                <li class="comment-contant">
                                    <div class="user-img">
                                        <img src="/public/site/image/doanh nhan.jpg" alt="user" width=100% height="100%">
                                    </div>
                                    <div class="user-comment">
                                        <h4 class="user-name"><a href="#">Lionel Messi</a></h4>
                                        <p>10 hour ago</p>
                                        <p style="font-size: 16px;">There are some business lorem ipsum dolor sit
                                            amet,
                                            consectetur
                                            adipiscing
                                            elit, sed do eiusmod tempor inc ididunt ut labore et dolore magna
                                            aliqua.
                                            Ut enim ad minim veniam, quis nostrud xeret </p>
                                        <a href="#">Reply</a>
                                    </div>
                                </li>
                                <li class="comment-contant">
                                    <div class="user-img">
                                        <img src="/public/site/image/doanh nhan.jpg" alt="user" width=100% height="100%">
                                    </div>
                                    <div class="user-comment">
                                        <h4 class="user-name"><a href="#">Lionel Messi</a></h4>
                                        <p>10 hour ago</p>
                                        <p style="font-size: 16px;">There are some business lorem ipsum dolor sit
                                            amet,
                                            consectetur
                                            adipiscing
                                            elit, sed do eiusmod tempor inc ididunt ut labore et dolore magna
                                            aliqua.
                                            Ut enim ad minim veniam, quis nostrud xeret </p>
                                        <a href="#">Reply</a>
                                    </div>
                                </li>
                                <li class="comment-contant">
                                    <div class="user-img">
                                        <img src="/public/site/image/doanh nhan.jpg" alt="user" width=100% height="100%">
                                    </div>
                                    <div class="user-comment">
                                        <h4 class="user-name"><a href="#">Lionel Messi</a></h4>
                                        <p>10 hour ago</p>
                                        <p style="font-size: 16px;">There are some business lorem ipsum dolor sit
                                            amet,
                                            consectetur
                                            adipiscing
                                            elit, sed do eiusmod tempor inc ididunt ut labore et dolore magna
                                            aliqua.
                                            Ut enim ad minim veniam, quis nostrud xeret </p>
                                        <a href="#">Reply</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="leave-area" style="padding-top:60px">
                        <h4>Leave a Comment</h4>
                        <div class="write-area" style="padding-top:40px;padding-bottom:70px;">
                            <form class="form-inline">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-lg-6" style="padding-bottom:20px;">
                                        <input type="text" placeholder="Your name">
                                    </div>
                                    <div class="col-xs-12 col-md-12 col-lg-6" style="padding-bottom:20px">
                                        <input type="text" id="email"placeholder="Email here">
                                    </div>
                                    <div class="col-xs-12 col-md-12" style="padding-bottom:20px">
                                        <textarea placeholder="Write here" rows="5"></textarea>
                                    </div>
                                    <br>
                                    <div class="col-md-12">
                                        <div class="submit">
                                            <button type="submit" class="btn btn-default">SUBMIT COMMENT</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3">
                <div class="sidebar">
                    <div class="search-bar" style="display: inline;">
                        <input type="text" placeholder="Search something..." size="27">
                        <a href="#"><i class="fas fa-search"></i></a>
                    </div>
                    <hr>
                    <div class="categori" style="padding-bottom:70px;padding-top:30px;">
                        <div class="header">
                            <h3 class="title">Categories</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li><a href="#">Business<span>(15)</span></a></li>
                                <li><a href="#">Corporate<span>(12)</span></a>
                                <li><a href="#">Creative<span>(15)</span></a></li>
                                <li><a href="#">Technology<span>(18)</span></a></li>
                                <li><a href="#">Development<span>(20)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="recentPost" style="padding-bottom: 70px">
                        <div class="header">
                            <h3 class="title">Recent Post</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li class="post">
                                    <div class="post-cate">
                                        <img src="/public/site/image/image7.jpg" width="100%" height="100%">
                                    </div>
                                    <div class="post-content">
                                        <div class="post-title"><a href="#">Lorem ipsum dolor</a></div>
                                        <div class="
                                                    time"><a href="#">Ronchi / 06 Jun, 2016</a></div>
                                        <p>Lorem must explain to ten how mistakenea</p>
                                    </div>
                                </li>
                                <li class="post">
                                    <div class="post-cate">
                                        <img src="/public/site/image/image6.jpg" width="100%" height="100%">
                                    </div>
                                    <div class="post-content">
                                        <div class="post-title"><a href="#;">Lorem ipsum dolor</a></div>
                                        <div class="time"><a href="#">Ronchi / 06 Jun, 2016</a></div>
                                        <p>Lorem must explain to ten how mistakenea</p>
                                    </div>
                                </li>
                                <li class="post">
                                    <div class="post-cate">
                                        <img src="/public/site/image/image5.jpg" width="100%" height="100%">
                                    </div>
                                    <div class="post-content">
                                        <div class="post-title"><a href="#">Lorem ipsum dolor</a>
                                            <div class="time"><a href="#">Ronchi / 06 Jun, 2016</a></div>
                                            <p>Lorem must explain to ten how mistakenea</p>
                                        </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="categori" style="padding-bottom:70px;">
                        <div class="header">
                            <h3 class="title">Archive</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li><a href="#">Cooperation<span>(1)</span></a></li>
                                <li><a href="#">Design<span>(3)</span></a>
                                <li><a href="#">Event & Festivals<span>(3)</span></a></li>
                                <li><a href="#">Links<span>(1)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="categori" style="padding-bottom:70px;">
                        <div class="header">
                            <h3 class="title">Last Tweets</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li>
                                    <div class="tweet">
                                        <p><a href="#">@Lorem Ipsum</a>
                                            dolor sit amet, costetur adipiscing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tweet">
                                        <p><a href="#">@Lorem Ipsum</a>
                                            dolor sit amet, costetur adipiscing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="tweet">
                                        <p><a href="#">@Lorem Ipsum</a>
                                            dolor sit amet, costetur adipiscing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tag">
                        <div class="header">
                            <h3 class="title">Tag</h3>
                        </div>
                        <div class="list">
                            <ul>
                                <li><a href="#">Forum sedding</a></li>
                                <li><a href="#">Web Design</a></li>
                                <li><a href="#">Google ads</a></li>
                                <li><a href="#">Seo onpage</a></li>
                                <li><a href="#">Email Marketing</a></li>
                                <li><a href="#">SMS marketing</a></li>
                                <li><a href="#">Marketing</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


