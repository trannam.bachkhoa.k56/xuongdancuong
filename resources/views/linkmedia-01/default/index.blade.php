@extends('linkmedia-01.layout.site')

{{--@section('title', $information['meta-title'])--}}
{{--@section('meta_description', $information['meta-description'])--}}
{{--@section('keywords', $information['meta-keyword'])--}}
@section('type_meta', 'website')

@section('content')
<body>
    <section class="banner">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6  text-left" style="padding-top:80px;">
                    <h3>GIẢI PHÁP MARKETING TOÀN THIỆN CHO DOANH NGHIỆP</h3><br>
                    <div class="service">
                        <div class="col-12" style="padding-top:10px;font-size:18px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Thiết kế website
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:18px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Fanpage website<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:18px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Google ads<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:18px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Forum sedding<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:18px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Đăng giao vặt<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:18px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Google map<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:18px"><i class='fas fa-location-arrow'
                                style="color:white"></i>
                            Content chuẩn seo<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:20px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Seo onpage<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:20px">
                            <i class='fas fa-location-arrow' style="color:white"></i> Email Marketing<br>
                        </div>
                        <div class="col-12" style="padding-top:5px;font-size:20px">
                            <i class='fas fa-location-arrow' style="color:white"></i> SMS marketing<br>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div class="container" style="padding-bottom:20px;padding-top:10px">
                    <div class="form" style="padding-left:20px;padding-right:20px">
                        <br>
                        <h2>ĐĂNG KÝ NGAY</h2>
                        <p>Đăng ký ngay để nhận bộ giải pháp marketing toàn diện cho doanh nghiệp</p>
                        <div class="formInfo">
                            <input class="form-control" type="text" placeholder="Họ và tên(*)"><br>
                            <input class="form-control" type="text" placeholder="Điện thoại(*)"><br>
                            <input class="form-control" type="text" placeholder="Email(*)"><br>
                            <textarea class="form-control" aria-label="With textarea" placeholder="Yêu cầu"></textarea><br>
                            <button class="btn btn-warning btn-block" style="color:white;font-size:25px">Đăng ký</button><br>
                            <p>100% miễn phí tư vấn</p><br>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="padding-top:30px;padding-bottom: 30px;">
        <div class="row">
            <div class="col-12 text-center">
                <h1 style="color:#183a5c">LINK MEDIA - GIẢI PHÁP MARKETING THUÊ NGOÀI</h1>
                <p style="font-size : 22px;">để dễ dàng đưa doanh nghiệp của
                    mình
                    tiếp cận trên internet,
                    Chúng tôi chuyên giúp đỡ các giải pháp marketing thuê ngoài cho các doanh nghiệp</p>
            </div>
        </div>
        <br>
        <div class="row text-center">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <img src=" /public/site/image/image1.jpg" alt="" style="width: 70%;">
                <p id="con"><a href="#">THIẾT KẾ WEBSITE</a></p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <img src="/public/site/image/image2.jpg" alt="" style="width: 70%;">
                <p id="con"><a href="#">QUẢNG CÁO FACEBOOK GOOGLE</a></p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <img src="/public/site/image/image3.jpg" alt="" style="width: 70%;">
                <p id="con"><a href="#">EMAIL, SMS MARKETING</a></p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                <img src="/public/site/image/image4.jpg" alt="" style="width: 70%;">
                <p id="con"><a href="#">SEO ONPAGE</a></p>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" style="padding-top:20px;color:white">
                    <h1>TẠI SAO LINK MEDIA LÀ DỊCH VỤ TỐT NHẤT</h1>
                    <p style="font-size:18px;">Linkmedia là công ty truyền
                        thông
                        hàng đầu về các giải pháp marketingonline.
                        Khách hàng có thể lựa chọn tin tưởng đến với Linkmedia</p>
                </div>
            </div>
            <div class="container" style="padding-left:5%;padding-right:5%">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2" style="background-color:white;">
                        <img src="/public/site/image/logo2.png" alt="">
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10" style="background-color: rgba(209, 209, 209, 0.9);">
                        <p style="font-size : 20px;padding: 3% ;color:rgb(87, 86, 86);">Cảm ơn quý khách đã tin tưởng
                            dịch vụ của
                            Linkmedia.
                            Chúng tôi luôn nỗ lực để đưa sản phẩm tốt nhất đến từng quý vị khách hàng</p>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2" style="background-color:white;">
                        <img src="/public/site/image/logo3.png" alt="">
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10" style="background-color: rgba(209, 209, 209, 0.9);">
                        <p style="font-size : 20px;padding: 3% ;color:rgb(87, 86, 86);">Cảm ơn quý khách đã tin tưởng
                            dịch vụ của
                            Linkmedia.
                            Chúng tôi luôn nỗ lực để đưa sản phẩm tốt nhất đến từng quý vị khách hàng</p>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2" style="background-color:white;">
                        <img src="/public/site/image/logo4.png" alt="">
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10" style="background-color: rgba(209, 209, 209, 0.9);">
                        <p style="font-size : 20px;padding: 3% ;color:rgb(87, 86, 86);">Cảm ơn quý khách đã tin tưởng
                            dịch vụ của
                            Linkmedia.
                            Chúng tôi luôn nỗ lực để đưa sản phẩm tốt nhất đến từng quý vị khách hàng</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="aboutUs">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center" style="padding-top:50px;color:#183a5c">
                    <h1>KHÁCH HÀNG NÓI GÌ VỀ CHÚNG TÔI</h1>
                    <p style="font-size:22px; padding-left:100px;padding-right:100px;color:black">
                        Cảm ơn quý khách đã tin tưởng dịch vụ của Linkmedia.Chúng tôi luôn nỗ lực để đưa sản phẩm
                        tốt
                        nhất đến từng quý vị khách hàng
                    </p>
                </div>
            </div>
        </div>
        <div class="container-fluid">
                <div class="row">
                    <div class="col-6" style="position: relative;">
                        <div class="text" style="padding:20px;background-color:white;margin-left: 150px;margin-top: 30px;">
                            <i class="fas fa-quote-right"></i>
                            <p>Tôi đang rất hài lòng về khả năng truyền thông của linkmedia với doanh nghiệp của tôi.
                                Sức
                                bánh hàng của công ty tôi đã tăng vọt sau khi sử dụng giải pháp marketing pro của
                                Linkmedia</p>
                        </div>
                        <div class="image" style="padding-left:40px;position: absolute;top: 85%;">
                            <img src="/public/site/image/doanh nhan.jpg" alt="" class="rounded-circle" width="160" height="160">
                        </div>
                        <div style="position:absolute;padding-left:200px;padding-top:60px">
                            <b style="color:#183a5c">Anh Tuấn - TH Home</b>
                            <p>Giám Đốc</p>
                        </div>
                    </div>
                    <div class="col-6" style="position: relative;">
                        <div class="" style="padding:20px;background-color:white;margin-left:150px;margin-top: 30px;">
                            <i class="fas fa-quote-right"></i>
                            <p>Tôi đang rất hài lòng về khả năng truyền thông của linkmedia với doanh nghiệp của tôi.
                                Sức
                                bánh hàng của công ty tôi đã tăng vọt sau khi sử dụng giải pháp marketing pro của
                                Linkmedia</p>
                        </div>
                        <div class="image" style="padding-left:40px;position: absolute;top: 85%;">
                            <img src="/public/site/image/doanh nhan.jpg" alt="" class="rounded-circle" width="160" height="160">
                        </div>
                        <div style="position:absolute;padding-left:200px;padding-top:60px">
                            <b style="color:#183a5c">Anh Tuấn - TH Home</b>
                            <p>Giám Đốc</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
        <div class="search">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center" style="padding-top:30px;color:white;">
                        <h2>ĐĂNG KÝ NHẬN NGAY BÁO GIÁ GIẢI PHÁP TRUYỀN THÔNG MARKETING ONLINE TỐT NHẤT</h2>
                    </div>
                    <form class="form-inline" style="padding:30px 90px 0px 80px">
                        <div class="input-group">
                            <input type="email" class="form-control" size="150" placeholder="Thông tin Email" required
                                alt="Thông tin Email">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-warning" style="color:white;">NHẬN BÁO GIÁ</button>
                            </div>
                        </div>
                    </form>
                    <br>
                </div>
            </div>
        </div>
    </section>
    <div class="contact" style="padding-bottom:30px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center" style="padding-top:30px;color:#183a5c">
                    <h2>NHẬN HỖ TRỢ TRỰC TIẾP TỪ CÁC CHUYÊN GIA TƯ VẤN</h2>
                    <p style="font-size:22px; color:black">
                        Các chuyên gia tư vấn của chúng tôi sẽ giúp các bạn bán hàng online tốt hơn
                    </p>
                </div>
            </div>
            <div class="container">
                <div class="row" style="padding-top: 30px;">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <img src="/public/site/image/doanh nhan.jpg" class="rounded-circle" width="150px" height="150px">
                        <p style="color:#183a5c;padding-left:18px"><a href="#">Trần Quang Hải</a></p>
                        <p style="padding-left:5px">Chuyên viên tư vấn</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <img src="/public/site/image/doanh nhan.jpg" alt="" class="rounded-circle" width="150px" height="150px">
                        <p style="color:#183a5c;padding-left:18px"><a href="#">Trần Quang Hải</a></p>
                        <p style="padding-left:5px">Chuyên viên tư vấn</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <img src="/public/site/image/doanh nhan.jpg" alt="" class="rounded-circle" width="160px" height="160px">
                        <p style="color:#183a5c;padding-left:20px"><a href="#">Trần Quang Hải</a></p>
                        <p style="padding-left:7px">Chuyên viên tư vấn</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <img src="/public/site/image/doanh nhan.jpg" alt="" class="rounded-circle" width="160px" height="160px">
                        <p style="color:#183a5c;padding-left:20px"><a href="#">Trần Quang Hải</a></p>
                        <p style="padding-left:7px">Chuyên viên tư vấn</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="post" style="padding-bottom:30px;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center" style="padding-top:30px;color:#183a5c;padding-bottom: 30px;">
                    <h2>HƯỚNG DẪN MARKETING ONLINE</h2>
                    <p style="font-size:22px; color:black">
                        Các bài viết hay sẽ giúp bạn hiểu rõ hơn về marketing và thực hành nó thật tốt
                    </p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="/public/site/image/image5.jpg" alt="Lights" style="width:80%">
                        <div class="caption">
                            <a href="#">
                                <h4>Lorem ipsum dolor sit amet conse </h4>
                            </a>
                            <p style="padding-right:30px;">Lorem ipsum dolor sit amet consectetur, adipisicing
                                elit.
                                Laboriosam,
                                voluptatum! Dolor quo, perspiciatis
                                voluptas totam
                            </p>
                            <a href="#">Read</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="/public/site/image/image6.jpg" alt="Lights" style="width:80%">
                        <div class="caption">
                            <a href="#">
                                <h4>Lorem ipsum dolor sit amet conse</h4>
                            </a>
                            <p style="padding-right:30px;">Lorem ipsum dolor sit amet consectetur, adipisicing
                                elit.
                                Laboriosam,
                                voluptatum! Dolor quo, perspiciatis
                                voluptas totam
                            </p>
                            <a href="#">Read</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="/public/site/image/image7.jpg" alt="Lights" style="width:80%">
                        <div class="caption">
                            <a href="#">
                                <h4>Lorem ipsum dolor sit amet conse</h4>
                            </a>
                            <p style="padding-right:30px;">Lorem ipsum dolor sit amet consectetur, adipisicing
                                elit.
                                Laboriosam,
                                voluptatum! Dolor quo, perspiciatis
                                voluptas totam
                            </p>
                            <a href="#">Read</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</body>
@endsection

