@extends('zito.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', 'trang/'.$post->slug )

@section('content')
<div class="container">
	<div class="row">
		<main id="main" class="">
        <div id="content" class="blog-wrapper blog-archive page-wrapper">
            <!-- .page-header -->
            <div class="row row-large ">
                <div class="large-9 col col-lg-9 col-md-9 col-sm-12 col-12">
                    <article id="post-4793" class="post-4793 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-tuc">
                        <div class="article-inner has-shadow box-shadow-1 box-shadow-3-hover">
                            <header class="entry-header">
                                <div class="entry-header-text entry-header-text-top  text-left">
                                    <h1 class="entry-title">{{ $post->title }}</h1>
                                    <div class="entry-divider is-divider small"></div>
                                    <div class="entry-meta uppercase is-xsmall">
                                        <span class="posted-on">Đăng vào
                                            <?php $date=date_create($post->created_at); ?>
                                            <?= date_format($date,"d-m-Y H:i") ?>
                                        </span>
                                    </div>
                                    <!-- .entry-meta -->
                                </div>
                            </header>
                            <!-- post-header -->
                            <div class="entry-content single-page">
                                {!! $post->content !!}
                            </div>
                        </div>
                        <!-- .article-inner -->
                    </article>
                    <!-- #-4793 -->
                    <div id="comments" class="comments-area">
                        <div id="respond" class="comment-respond">
                            <h3 id="reply-title" class="comment-reply-title">Trả lời</h3>
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=887742841384108';
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-comments" data-href="{{ route('product', ['post_slug' => $post->slug]) }}"
                                 data-numposts="5" data-width="100%"></div>
                        </div>
                        <!-- #respond -->
                    </div>
                    <!-- #comments -->
                </div>
                <!-- .large-9 -->
            @include('zito.partials.sidebar-product')
            <!-- .post-sidebar -->
            </div>
            <!-- .row -->
        </div>
    </main>
	</div>
</div>
    
@endsection


