@extends('zito.layout.site')

@section('title', $information['meta_title'])
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])

@section('content')
    <main id="main" class="">
        <div id="content" role="main" class="content-area">
        @include('zito.partials.slider')
        @include('zito.partials.sologan')

            <section class="productHighlights">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" align="center">
                            <h2 class="title">SẢN PHẨM <strong>BÁN CHẠY</strong></h2>
                            <div class="bordertitle">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            @if(empty(\App\Entity\Product::showProduct('san-pham-dang-ban-chay-nhat',1)))
                                <p>Sản phẩm không tồn tại</p>
                            @else
                                @foreach (\App\Entity\Product::showProduct('san-pham-dang-ban-chay-nhat',1) as $id => $product_discount)
                                    @if($id == 0)
                                        <div class="left">
											<div class="CropImg CropImgP">
                                            <a class="thumbs" href="{{ route('product', [ 'post_slug' => $product_discount->slug]) }}" title="{{ $product_discount->title }}">
                                                <img src="{{ asset($product_discount->image) ? asset($product_discount->image) :'' }}" alt="{{ $product_discount->title }}"></a>
											</div>
                                            <div class="content">
                                                <a href="{{ route('product', [ 'post_slug' => $product_discount->slug]) }}" title="{{ $product_discount->title }}" class="color">{{ $product_discount->title }}</a>
                                                <p class="price">
                                                    Giá :
                                                    <span style="color: #444;margin-right: 5px">
                                                    <del>
                                                        <?php if($product_discount->discount > 0)
                                                        {
                                                            echo number_format($product_discount->price).' đ';
                                                        }
                                                        else
                                                        {
                                                        }
                                                        ?>
                                                    </del>
                                                </span>
                                                <span style="color: red;font-size: 17px; font-weight: 700;;">
                                                    <?php if($product_discount->discount > 0)
                                                                    {
                                                                        echo number_format($product_discount->discount).' đ';
                                                                    }
                                                                    else
                                                                    {
                                                                        echo number_format($product_discount->price).' đ';
                                                                    }
                                                                    ?>
                                                </span> 
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="row right">
                                @if(empty(\App\Entity\Product::showProduct('san-pham-dang-ban-chay-nhat',5)))
                                    <p>Sản phẩm không tồn tại</p>
                                @else
                                    @foreach (\App\Entity\Product::showProduct('san-pham-dang-ban-chay-nhat',5) as $id => $product_discount)
                                        @if($id > 0)
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
												<div class="CropImg">
                                                <a class="thumbs" href="{{ route('product', [ 'post_slug' => $product_discount->slug]) }}" title="{{ $product_discount->title }}" class="">
                                                    <img src="{{ asset($product_discount->image) ? asset($product_discount->image) :'' }}" alt="{{ $product_discount->title }}"></a>
												</div>
                                                <div class="content">
                                                    <a href="{{ route('product', [ 'post_slug' => $product_discount->slug]) }}" title="{{ $product_discount->title }}" class="color">{{ $product_discount->title }}</a>
                                                    <p class="price">
                                                        Giá :
                                                        <span style="color: #444;margin-right: 5px">
                                                        <del>
                                                            <?php if($product_discount->discount > 0)
                                                            {
                                                                echo number_format($product_discount->price).' đ';
                                                            }
                                                            else
                                                            {

                                                            }
                                                            ?>
                                                        </del>
                                                        </span>
                                                        <span style="color: red;font-size: 17px;
    font-weight: 700;">
                                                            <?php if($product_discount->discount > 0)
                                                                    {
                                                                        echo number_format($product_discount->discount).' đ';
                                                                    }
                                                                    else
                                                                    {
                                                                        echo number_format($product_discount->price).' đ';
                                                                    }
                                                                    ?>
                                                        </span> 
                                                    </p>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			@foreach (\App\Entity\Menu::showWithLocation('show-category-index') as $menu)
				@foreach (\App\Entity\MenuElement::showMenuElement($menu->slug) as $element)
					<?php $slugCategoryTokens = explode('/', $element->url);?>
					@if (isset($slugCategoryTokens[2]))
            <section class="section sphome hiddenPc showMb" id="section_1700251332">
                <div class="bg section-bg fill bg-fill  bg-loaded" >
                </div>
				
                <!-- .section-bg -->
                <div class="section-content relative container">
                    <div class="row align-center"  id="row-775226297">
                        <div class="col small-12 large-12"  >
                            <div class="col-inner text-center"  >
                                <div class="container section-title-container" >
                                    <h3 class="section-title section-title-normal"><b></b><span class="section-title-main">{!! $element->title_show !!}</span><b></b></h3>
                                </div>
                                <!-- .section-title -->
                                <div class="row large-columns-4 medium-columns- small-columns-2 row-small has-shadow row-box-shadow-1 row-box-shadow-3-hover">
                                    @foreach (\App\Entity\Product::showProduct($slugCategoryTokens[2],8) as $id => $product)
                                        @include('zito.partials.product-item', ['product'=> $product])
                                    @endforeach
                                </div>
                               
                            </div>
                        </div>
                        <style scope="scope">
                            #row-775226297 > .col > .col-inner {
                                padding: 0px 0px 0px 0px;
                            }
                        </style>
                    </div>
                </div>
                <!-- .section-content -->
						
                <style scope="scope">
                    #section_1700251332 {
                        padding-top: 0px;
                        padding-bottom: 0px;
                        background-color: rgb(237, 239, 237);
                    }
                </style>
            </section>
					@endif
				@endforeach	
			@endforeach
			
			
			@foreach (\App\Entity\Menu::showWithLocation('menu-tab-index') as $menu)
				@foreach (\App\Entity\MenuElement::showMenuElement($menu->slug) as $element)
					<?php $slugCategoryTokens = explode('/', $element->url);?>
					@if (isset($slugCategoryTokens[2]))
            <section class="section sphome hiddenMb" id="section_1700251332">
                <div class="bg section-bg fill bg-fill  bg-loaded" >
                </div>
				
                <!-- .section-bg -->
                <div class="section-content relative container">
                    <div class="row align-center"  id="row-775226297">
                        <div class="col small-12 large-12"  >
                            <div class="col-inner text-center"  >
                                <div class="container section-title-container" >
                                    <h3 class="section-title section-title-normal"><b></b><span class="section-title-main">{!! $element->title_show !!}</span><b></b></h3>
                                </div>
                                <!-- .section-title -->
                                <div class="row large-columns-4 medium-columns- small-columns-2 row-small has-shadow row-box-shadow-1 row-box-shadow-3-hover">
                                    @foreach (\App\Entity\Product::showProduct($slugCategoryTokens[2],8) as $id => $product)
                                        @include('zito.partials.product-item', ['product'=> $product])
                                    @endforeach
                                </div>
                               
                            </div>
                        </div>
                        <style scope="scope">
                            #row-775226297 > .col > .col-inner {
                                padding: 0px 0px 0px 0px;
                            }
                        </style>
                    </div>
                </div>
                <!-- .section-content -->
						
                <style scope="scope">
                    #section_1700251332 {
                        padding-top: 0px;
                        padding-bottom: 0px;
                        background-color: rgb(237, 239, 237);
                    }
                </style>
            </section>
					@endif
				@endforeach	
				@if (!empty($menu->image) )
					<div style="background-color: rgb(237, 239, 237);">
						<div class="container" >
							<div class="row">
								<div class="col-md-12 col-sm-12 col-12"  style="padding-left: 20px;padding-right: 20px;">
								 <a href="{{ isset($information['link-banner-ngoai-trang-chu']) ? $information['link-banner-ngoai-trang-chu'] : '#' }}"><img src="{{ asset($menu->image) }}" style="max-width: 100%"/></a>
							</div>
						</div>
					</div>
				@endif
			@endforeach
        </div>
    </main>
@endsection

