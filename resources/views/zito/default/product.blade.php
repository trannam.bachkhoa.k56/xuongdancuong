@extends('zito.layout.site')

@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', asset($product->image) )
@section('meta_url', route('product', [ 'post_slug' => $product->slug]) )

@section('content')
<!--link  owl -->
	<link rel="stylesheet" href="{{ asset('zito/css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('zito/css/owl.theme.default.min.css') }}">
	<script type='text/javascript' src="{{ asset('zito/js/jquery.min.js') }}"></script>
	<script type='text/javascript' src="{{ asset('zito/js/owl.carousel.min.js') }}"></script>
<!--end  owl -->
   
	<section class="beachrum">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<ul>
						<li>
							<a class="color" href="/"><i class="fa fa-home" aria-hidden="true"></i></a>
						</li>
						<?php $showCategory = false; ?>  
						@foreach ($categories as $id => $category) 
							@if ($category->title != 'Sản phẩm khuyến mãi' && !$showCategory)
								<style>
									.cua-hang{{ $category->slug }} {
										background: red;
									}
								</style>	
								<?php $showCategory = true; ?>  
								<li class="bgbrum">
									<a class="color" href="{{ route('site_category_product', [ 'cate_slug' => $category->slug]) }}">{{ $category->title }}</a>
								</li>
							@endif
						@endforeach
						
						<li class="bgbrum">
							<a  class="color" href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{{ $product->title }}</a>
							
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="detailproduct">
     <div class="container">
       <div class="row">
         <div class="col-lg-6 col-md-6 col-sm-12 col-12 left">
			<div class="leftImg">
				<script type='text/javascript' src='{{ asset('zito/js/jquery.elevatezoom.js') }}'></script>
				<div class="imgLage pc">
				    <img id="zoom_01" src="{{ asset($product->image)}}" data-zoom-image="{{ asset($product->image)}}" />
			    </div>
			    <div class="mobile imageProductMobile" style="margin-top: 15px;">
                    <img  src="{{ asset($product->image)}}"  />
                </div>
										
				<div id="gal1" class="imgmedum">
				@if(!empty($product->image_list))
					@foreach(explode(',', $product->image_list) as $imageProduct)   
					   <a  data-image="{{$imageProduct}}" data-zoom-image="{{$imageProduct}}" onclick="return changeImage(this);">
						<img id="zoom_01" src="{{$imageProduct}}" />
						</a>
					   
					 @endforeach
				@endif
				</div>
			
			
			   <script>
				//initiate the plugin and pass the id of the div containing gallery images 
				$("#zoom_01").elevateZoom({ gallery: 'gal1', cursor: 'pointer' });
				//pass the images to Fancybox 
				$("#zoom_01").bind("click", function (e) {
				var ez = $('#zoom_01').data('elevateZoom');
				$.fancybox(ez.getGalleryList()); return false; 
				$('#zoom_01').css('width','420px');
				$('#zoom_01').css('height','280px');
				});
				
				function changeImage(e) {
					var image = $(e).attr('data-image');
					$('.imageProductMobile').find('img').attr('src', image);
					
					return false;
				}
			   </script>
			 </div>
         </div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-12 rightitem">
           <div class="right">
             <h1>{{ $product->title }}</h1>
            
             <div class="des">
			 @if($product['properties'] != '')
			<?= $product['properties'] ?>
			  @else
					Đang cập nhập
				@endif
	
			
              <!--<p><i class="fa fa-check-square-o" aria-hidden="true"></i><strong>Kích thước: </strong>1350*750*780</p>-->
             
             </div>
              <p class="price">Giá : <span>
									<?php if($product->discount > 0)
                                    {
                                        echo number_format($product->discount);
                                    }
                                    else
                                    {
                                        echo number_format($product->price);
                                    }
                                    ?> đ
				</span> 
				<?php if($product->discount > 0)
                                        {
                                            echo 'Giá cũ : <del>'. number_format($product->price).'đ </del> ';
											$price_sale = $product->price - $product->discount;
											echo '<br><span class="sale">Tiết kiệm:'.number_format($price_sale).'đ</span>';
                                        }
                                        else
                                        {

                                        }
                                        ?>
					</p>					
										
				
				<div class="salePro">
                <h3 class="title"><i class="fa fa-free-code-camp" aria-hidden="true"></i> KHUYẾN MẠI</h3>
				 @if($product['thong-tin-khuyen-mai'] != '')
				<?= $product['thong-tin-khuyen-mai'] ?>
				  @else
					Đang cập nhập
				@endif
				
                <!--<p>Giảm trực tiếp vào hoá đơn mua bàn ăn: 2.000.000đ </p>-->
               
              </div>
              <div class="cartship">
              <form onsubmit="return addToOrder(this);" method="post" method="post" id="add-to-cart-form" class="form-inline margin-bottom-10dqdt-formformCart" enctype="multipart/form-data">
			  {{ csrf_field() }}
                <input type="hidden" class="quantity" name="quantity[]" value="1" />
                <input type="hidden" class="product_id" name="product_id[]" value="{{ $product->product_id }}">
                <button type="submit"><b>MUA NGAY</b> <br>
               Giao hàng  toàn quốc</button>
              </form>
	
              <button  class="center" type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">
               <b>MUA ONLINE</b> <br>
               Thêm nhiều ưu đãi
              </button>
              
              <button class="last" type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">
                 <b> NGHE TƯ VẤN</b> <br>
                Nghe tư vấn miễn phí
               
              </button>
              <p class="time"><i class="fa fa-calendar-times-o" aria-hidden="true"></i>Giờ làm việc: 8h00-20h00 (Tất cả các ngày)</p>
              </div>

              <div class="address">
                <h4><strong>ĐỊA CHỈ MUA HÀNG</strong></h4>
				  @if(isset($information['dia-chi-showroom']) && !empty($information['dia-chi-showroom']))
				<?= $information['dia-chi-showroom'] ?>
				  @else
					Đang cập nhập
				@endif
				
                <!--<p><i class="fa fa-map-marker" aria-hidden="true"></i> Showroom 01 - Số 356 Bạch Mai, Hai Bà Trưng, Hà Nội  </p>-->
                
              </div>
           </div>
         </div>

          <!-- MODAL -->
          <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Thông tin sản phẩm</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 bgModalLeft">
                                  <div class="modalLeft">
                                      <img src="{{ asset($product->image) ?asset($product->image) : '' }}" alt="{{ $product->title}}">
                                      <h2>{{ $product->title }}</h2>
                                      <div class="des">
                                         @if($product['properties'] != '')
										<?= $product['properties'] ?>
										  @else
												Đang cập nhập
											@endif
                                      </div>
										<p class="price"><span>
															<?php if($product->discount > 0)
															{
																echo 'Giá : '.number_format($product->discount);
															}
															else
															{
																echo 'Giá : '.number_format($product->price);
															}
															?> đ
										</span> 
										<?php if($product->discount > 0)
																{
																	echo 'Giá cũ : <del>'. number_format($product->price).'đ </del> ';
																	$price_sale = $product->price - $product->discount;
																	echo '<span class="sale">Tiết kiệm:'.number_format($price_sale).'đ</span>';
																}
																else
																{

																}
																?>
										</p>	
                                      <!--<p class="price"> <span>Giá : 25,000,000 đ</span> Giá cũ : <del>23,000,000 đ </del> <span class="sale">Tiết kiệm: 4,000,000 đ</span></p>-->
                                  </div>
                                </div>
                                <div class="col-lg-7 col-md-12 bgModal">
                                  <div class="modalRight">
                                    <p><strong>ĐẶT ONLINE NGHE TƯ VẤN MIỄN PHÍ</strong></p>
                                    <form action="{{ route('sub_contact') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="nz-cf7">
                                            <div class="nz-cf7-f1 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-name">
                                                                <input type="text" name="name" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name"
                                                                       aria-required="true" aria-invalid="false" placeholder="Tên quý khách" required/>
                                                            </span>
                                            </div>
                                            <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-phone">
                                                                <input type="tel" name="phone" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                                                       id="your-phone" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" required/>
                                                            </span>
                                            </div>
                                            <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-email">
                                                                <input type="email" name="email" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                       id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" required/></span>
                                            </div>
                                            <div class="nz-cf7-f1">
                                                            <span class="wpcf7-form-control-wrap your-message">
                                                                <textarea name="message" cols="40" rows="10"
                                                                          class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                                                          aria-required="true" aria-invalid="false" placeholder="Lời nhắn thêm (có thể để trống)"></textarea></span>
                                            </div>
                                            <div class="nz-cf7-f1">
                                                <input type="submit" value="Gửi tin yêu cầu tư vấn" class="wpcf7-form-control wpcf7-submit c-button button" />
                                            </div>
                                        </div>
                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                    </form>
                                  </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
          </div>
      </div>
     </div>
	</section>
	
	<section class="producTomorrow productSelling">
		<div class="container">
			<div class="row">
				 <div class="col-12">
                    <h3 class="title"><strong>SẢN PHẨM ĐANG BÁN CHẠY NHẤT</strong></h3>
                </div>
				<div class="col-12">
					<div class="owl-carousel">
						@foreach (\App\Entity\Product::showProduct('san-pham-dang-ban-chay-nhat',100) as $id => $productRelative)
						<div class="item">
							<div class="itempro">
								<a class="title color" href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}" >
									{{$productRelative->title}}
								</a>
								<div class="CropImg">
									<a class="thumbs" href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}" title=""> <img src="{{$productRelative->image}}" alt=""></a>
								</div>
								@if (!empty($productRelative->discount) )
								<div class="sale">
								{{ round( ($productRelative->price - $productRelative->discount) / $productRelative->price * 100) }}%
								</div>
								@endif
								
								<p><span style="color: #444;margin-right: 5px"><del>
											<?php if($productRelative->discount > 0)
											{
												echo number_format($productRelative->price).'đ';
											}
											else
											{

											}
											?>
										</del>
									</span>
									<span class="priceRight">
										<?php if($productRelative->discount > 0)
										{
											echo number_format($productRelative->discount).'đ';
										}
										else
										{
											echo number_format($productRelative->price).'đ';
										}
										?>
									</span>
								</p>
								<form onsubmit="return addToOrder(this);" method="post" method="post" id="add-to-cart-form" class="form-inline margin-bottom-10dqdt-formformCart" enctype="multipart/form-data">
								  {{ csrf_field() }}
									<input type="hidden" class="quantity" name="quantity[]" value="1" />
									<input type="hidden" class="product_id" name="product_id[]" value="{{ $productRelative->product_id }}">
									<button type="submit"><b>MUA NGAY</b></button>
								  </form>

							</div>
						</div>
						@endforeach
						
					</div>
				</div>
			</div>
		<div/>
	</section>
	<script>
	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		dots: false,
		responsiveClass:true,
		autoplay:true,
		nav:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:4,
				nav:true,
				loop:false
			}
		}
		});
		$('.owl-prev').html('');
		$('.owl-next').html('');
	</script>
	
	<section class="categoryDetail">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-12 col-12">
					  <nav>
					  <div class="nav nav-tabs" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Thông tin</a>
					
						<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact2" role="tab" aria-controls="nav-contact" aria-selected="false"><i class="fa fa-truck" aria-hidden="true"></i> Miễn phí vận chuyển</a>
					  </div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
						 @if($product['content'] != '')
							<?= $product['content'] ?>
							  @else
								Đang cập nhập
							@endif
						
						</div>
					
						<div class="tab-pane fade" id="nav-contact2" role="tabpanel" aria-labelledby="nav-contact-tab">
						 @if(isset($information['mien-phi-van-chuyen']) && $information['mien-phi-van-chuyen'] != '')
							<?= $information['mien-phi-van-chuyen'] ?>
						  @else
								Đang cập nhập
							@endif
					
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-12 rightPro">
					<div class="product">
						<div class="titlePro">
							<h3>SẢN PHẨM MỚI RA MẮT</h3>
						</div>
						
						
						@foreach (\App\Entity\Product::showProduct('san-pham-moi-ra-mat',4) as $id => $productRelative)
						<div class="productNew">
							<div class="itempronew">
								<div class="CropImg"><a class="thumbs" href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}" title=""> <img src="{{$productRelative->image}}" alt=""></a></div>
								<h4 class="color"><a class="title color" href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}" >
									{{$productRelative->title}}
								</a>
								</h4>
								<p><?php
									echo number_format($productRelative->price).'đ';	
								?>
								</p>
										
								<p>
								<?php if($productRelative->discount > 0)
										{
											echo 'Giá KM: <span>'.number_format($productRelative->discount).'đ</span>';
										}
										else
										{
											
										}
										?>
								
								</p>
									<form onsubmit="return addToOrder(this);" method="post" method="post" id="add-to-cart-form" class="form-inline margin-bottom-10dqdt-formformCart" enctype="multipart/form-data">
								  {{ csrf_field() }}
									<input type="hidden" class="quantity" name="quantity[]" value="1" />
									<input type="hidden" class="product_id" name="product_id[]" value="{{ $productRelative->product_id }}">
									<button type="submit"><b>MUA NGAY</b></button>
								  </form>
							</div>
						</div>
						@endforeach
						<div class="banner" style="background-color: none !important;">
						@foreach (\App\Entity\SubPost::showSubPost('banner-quang-cao', 10) as $id => $banner)
							<a href="{{ $banner['duong-dan-banner'] }}"><img src="{{ asset($banner->image) }}" width="100%" title="{{ $banner->title}}"></a>
						@endforeach
							
						</div>
					</div>
				</div>
			</div>
		   
		</div>
   </section>

	
	
	
	
	
	
	
	
	<!--<section class="productInvolve">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <h3 class="title">CHI TIẾT SẢN PHẨM</h3>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-12 ContentProduct">
                    <style>
                        .ContentProduct
                        {
                            padding-left: 30px;
                        }
                    </style>
                    <?php  echo $product['content'] ?>
                </div>
            </div>
        </div>
    </section>-->
    <section class="productInvolve">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h3 class="title">SẢN PHẨM CÙNG LOẠI</h3>
                </div>
            </div>
            <div class="row">
                @foreach (\App\Entity\Product::relativeProduct($product->slug, $product->product_id) as $id => $productRelative)
                    <div class="col-lg-3 col-md-6 col-sm-12 col-12">
                        <div class="itempro">
                            <div class="CropImg">
								<a class="thumbs" href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}" title=""> <img src="{{$productRelative->image}}" alt=""></a>
							</div>
                            <a class="title color" href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}">
                                {{$productRelative->title}}
                            </a>
                            <p><span style="color: #444;margin-right: 5px"><del>
                                        <?php if($productRelative->discount > 0)
                                        {
                                            echo number_format($productRelative->price).'';
                                        }
                                        else
                                        {

                                        }
                                        ?>
                                    </del>
                                </span>
                                <span>
                                    <?php if($productRelative->discount > 0)
                                    {
                                        echo number_format($productRelative->discount);
                                    }
                                    else
                                    {
                                        echo number_format($productRelative->price);
                                    }
                                    ?>
                                        </span> vnđ</p>

                        </div>
                    </div>
                @endforeach

            </div>

        </div>
    </section>
    <section class="producTomorrow">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h3 class="title">SẢN PHẨM <strong>KHUYẾN MÃI</strong></h3>
                </div>
            </div>
            <div class="row">
                @if(empty(\App\Entity\Product::showProduct('san-pham-khuyen-ma',9)))
                    <p>Sản phẩm không tồn tại</p>
                @else
                @foreach (\App\Entity\Product::showProduct('san-pham-khuyen-mai',9) as $id => $product_discount)
                    <div class="col-lg-4 col-md-6 col-sm-12 itemTomo">
                        <a href="{{ route('product', [ 'post_slug' => $product_discount->slug]) }}" title="{{ $product_discount->title }}" class="proTitle color">
                            {{ $product_discount->title }}</a>
                        <div class="CropImg">
							<a class="thumbs" href="{{ route('product', [ 'post_slug' => $product_discount->slug]) }}" title="{{ $product_discount->title }}">
								<img src="{{ asset($product_discount->image) }}" alt="">
							</a>
						</div>
						@if (!empty($product_discount->discount))
                        <div class="sale">
                            {{ round(($product_discount->price - $product_discount->discount) / $product_discount->price * 100) }}%
                        </div>
						@endif
                        <div class="price" style="float:left;">
                            <h3>Giá : <span style="color:red">
                                        <?php if($product_discount->discount > 0)
                                            {
                                                echo number_format($product_discount->discount);
                                            }
                                             else
                                            {
                                                echo number_format($product_discount->price);
                                            }
                                        ?>
                                        </span> vnđ</h3>

                            <p><del>
                                        <?php if($product_discount->discount > 0)
                                            {
                                                echo number_format($product_discount->price).' vnđ';
                                            }
                                            else
                                            {

                                            }
                                            ?>

                                    </del></p>
                        </div>
                    </div>
                @endforeach
                @endif

            </div>
        </div>
    </section>
    @include('zito.partials.sologan')
@endsection