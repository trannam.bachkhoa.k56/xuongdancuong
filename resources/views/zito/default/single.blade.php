@extends('zito.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
    <main id="main" class="">
        <div id="content" class="blog-wrapper blog-archive page-wrapper container">

            <!-- .page-header -->
            <div class="row row-large ">
                <div class="large-9 col col-lg-9 col-md-12 col-sm-12 col-12">
                    <article id="post-4793" class="post-4793 post type-post status-publish format-standard has-post-thumbnail hentry category-tin-tuc">
                        <div class="article-inner has-shadow box-shadow-1 box-shadow-3-hover">
                            <header class="entry-header">
                                <div class="entry-header-text entry-header-text-top  text-left">
                                    <h6 class="entry-category is-xsmall">
                                        <a href="{{ route('site_category_post', ['cate_slug' => $category->slug]) }}" rel="category tag">{{ $category->title }}</a>
                                    </h6>
                                    <h1 class="entry-title">{{ $post->title }}</h1>
                                    <div class="entry-divider is-divider small"></div>
                                    <div class="entry-meta uppercase is-xsmall">
                                        <span class="posted-on">Đăng vào
                                        <?php $date=date_create($post->created_at); ?>
                                            <?= date_format($date,"d-m-Y H:i") ?>
                                        </span>
                                    </div>
                                    <!-- .entry-meta -->
                                </div>
                            </header>
                            <!-- post-header -->
                            <div class="entry-content single-page">
                                {!! $post->content !!}
                            </div>
                            <!-- .entry-content2 -->
                            <footer class="entry-meta text-left">
                                Bài viết thuộc chuyên mục <a href="{{ route('site_category_post', ['cate_slug' => $category->slug]) }}" rel="category tag">{{ $category->title }}</a>.
                            </footer>
                            <!-- .entry-meta -->
                        </div>
                        <!-- .article-inner -->
                    </article>
                    <!-- #-4793 -->
                    <div id="comments" class="comments-area">
                        <div id="respond" class="comment-respond">
                            <h3 id="reply-title" class="comment-reply-title">Trả lời</h3>
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=887742841384108';
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-comments" data-href="{{ route('product', ['post_slug' => $post->slug]) }}"
                                 data-numposts="5" data-width="100%"></div>
                        </div>
                        <!-- #respond -->
                    </div>
                    <!-- #comments -->
                </div>
                <!-- .large-9 -->
            @include('zito.partials.sidebar-product')
            <!-- .post-sidebar -->
            </div>
            <!-- .row -->
            <div class="nz-bvlq-main">
                <div class="nz-bvlq">
                    <div class="row large-columns-4 medium-columns-2 small-columns-1 row-small">
                        <h3>Bài viết liên quan</h3>
                        @foreach (\App\Entity\Post::relativeProduct($post->slug, 4) as $id => $post)
                            <div class="nz-bvlq-content col post-item">
                                <div class="col-inner">
                                    <a href="{{ route('post',['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}"
                                       title="{{ $post->title }}">
                                        <div class="nz-bvlq-img">
                                            <img width="800" height="480"
                                                 src="{{ asset($post->image) }}" />
                                        </div>
                                        <div class="nz-bvlq-td">{{ $post->title }}</div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- .page-wrapper .blog-wrapper -->
        </div>
    </main>
@endsection


