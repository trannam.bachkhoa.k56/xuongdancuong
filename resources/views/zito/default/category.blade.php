@extends('zito.layout.site')

@section('title', $category->title)
@section('meta_description',  $category->description )
@section('keywords', '')

@section('content')

    <main id="main" class="">
        <div id="content" class="blog-wrapper blog-archive page-wrapper container">
            <header class="archive-page-header">
                <div class="row">
                    <div class="large-12 text-center col col-12">
                        <h1 class="page-title is-large uppercase">
                            Chuyên mục: <span>{{ $category->title }}</span>
                        </h1>
                        <div class="taxonomy-description">
                            <p style="text-align: justify;">
                                {!! $category->description !!}
                            </p>
                        </div>
                    </div>
                </div>
            </header>
            <!-- .page-header -->
            <div class="row row-large categoryNews">
                <div class="large-9 col col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="row">
						@foreach ($posts as $id => $post)
						<div class="col-lg-4 col-md-6 col-sm-6 col-12">
							<div class="itemcatenew">
								<a href="{{ route('post',['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" title="{{ $post->title }}"><img src="{{ asset($post->image) }}" class="attachment-medium size-medium wp-post-image lazy-load-active" alt="{{ $post->title }}" title="{{ $post->title }}"></a>
								<h3 class="titlenew"><a href="{{ route('post',['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" class="color">{{ $post->title }} </a></h3>
								<b></b>
								<p class="des">
									 {{ $post->description }}
								</p>
								<a href="{{ route('post',['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" class="link color">Xem chi tiết <i class="fa fa-caret-right" aria-hidden="true"></i></a>
							</div>
						</div>
						@endforeach
                    </div>
                    @if($posts instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        {{ $posts->links() }}
                    @endif
                </div>
                <!-- .large-9 -->
            @include('zito.partials.sidebar-product')
            <!-- .post-sidebar -->
            </div>
            <!-- .row -->
            <!-- .page-wrapper .blog-wrapper -->
        </div>
    </main>
@endsection

