@extends('zito.layout.site')


@section('title', 'tim-kiem')
@section('meta_description', $information['meta_description'] )
@section('keywords', '')

@section('content')
    <div class="shop-page-title category-page-title page-title featured-title dark ">
        <div class="page-title-bg fill">
            <div class="title-bg fill bg-fill" data-parallax-fade="true" data-parallax="-2" data-parallax-background data-parallax-container=".page-title"></div>
            <div class="title-overlay fill"></div>
        </div>
        <div class="page-title-inner flex-row  medium-flex-wrap container">
            <div class="flex-col flex-grow medium-text-center">
                <div class="is-large">
                    <nav class="woocommerce-breadcrumb breadcrumbs"><a href="/">Trang chủ</a>
                        <span class="divider">&#47;</span>
                        <a href=""> Tìm kiếm sản phẩm</a></nav>
                </div>
            </div>
        </div>
        <!-- flex-row -->
    </div>

    <main id="main" class="container">
        <div class="row category-page-row">
        @include('zito.partials.sidebar-product')
        <!-- #shop-sidebar -->
            <div class="col large-9 col-lg-9 col-md-9 col-sm12 col-12">
                <div class="shop-container">
                    <div class="term-description">
                        <p>
                            @include('zito.partials.slider')
                        </p>
                    </div>
                    <div class="products row row-small large-columns-3 medium-columns-3 small-columns-2 has-shadow row-box-shadow-1 row-box-shadow-3-hover">
                        @foreach($products as $id => $product )
                            <div class="product-small col has-hover post-4201 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l first instock shipping-taxable purchasable product-type-simple">
                                <div class="col-inner">
                                    <div class="badge-container absolute left top z-1"></div>
                                    <div class="product-small box ">
                                        <div class="box-image">
                                            <div class="image-none">
                                                <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                                                    <img width="260" height="160" src="{!! asset($product->image) !!}"
                                                         alt="{!! $product->title !!}" title="{!! $product->title !!}" />
                                                </a>
                                            </div>
                                            <div class="image-tools is-small top right show-on-hover">
                                            </div>
                                            <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                            </div>
                                            <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                            </div>
                                        </div>
                                        <!-- box-image -->
                                        <div class="box-text box-text-products text-center grid-style-2">
                                            <div class="title-wrapper">
                                                <p class="name product-title">
                                                    <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{!! $product->title !!}</a>
                                                </p>
                                            </div>
                                            <div class="price-wrapper">
                                            <span class="price"><span class="woocommerce-Price-amount amount">
                                                    @if (!empty($product->price_deal)
                                                    && !empty($product->discount_end) && ( time() < strtotime($product->discount_end))
                                                    && !empty($product->discount_start) && (time() > strtotime($product->discount_start))
                                                    )
                                                        {{ number_format($product->price_deal  , 0, ',', '.') }} VND
                                                    @elseif (!empty($product->discount))
                                                        {{ number_format($product->discount  , 0, ',', '.') }} VND
                                                    @else
                                                        {{ number_format($product->price  , 0, ',', '.') }} VND
                                                    @endif
                                                    <span class="woocommerce-Price-currencySymbol">&#8363;</span></span>
                                            </span>
                                            </div>
                                        </div>
                                        <!-- box-text -->
                                    </div>
                                    <!-- box -->
                                </div>
                                <!-- .col-inner -->
                            </div>
                            <!-- col -->
                        @endforeach
                    </div>
                    <!-- row -->

                    <div class="container">
                        @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                            {{ $products->links() }}
                        @endif
                    </div>
                </div>
                <!-- shop container -->
            </div>
        </div>
    </main>
@endsection
