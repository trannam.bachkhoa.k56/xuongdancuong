@extends('store-02.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
    <!-- MAIN-CONTENT-SECTION START -->
    <!-- MAIN-CONTENT-SECTION START -->
    <section class="main-content-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- BSTORE-BREADCRUMB START -->
                    <div style="padding: 15px 0;" >
                        <?php \App\Entity\Post::getBreadcrumb($post->post_id, 'san-pham')?>
                    </div>
                    <!-- BSTORE-BREADCRUMB END -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2 class="page-title about-page-title">{{ $post->title }}</h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="single-about-info">
                        <!-- OUR-TESTIMONIALS START -->
                        <div class="our-testimonials">
                            <!-- SINGLE-TESTIMONIALS START -->
                            <div class="single-testimonials">
                                <div class="testimonials-text">
                                    <span class="before"></span>
                                    {!! $post->description !!}
                                    <span class="after"></span>
                                </div>
                            </div>
                            <p>{!! $post->content !!}</p>
                        </div>
                        <!-- OUR-TESTIMONIALS END -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- MAIN-CONTENT-SECTION END -->

    <!-- LATEST-NEWS-AREA END -->
@endsection


