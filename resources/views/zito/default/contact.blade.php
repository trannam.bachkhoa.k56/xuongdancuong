@extends('zito.layout.site')

@section('title','Hệ thống nhà hàng')
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])

@section('content')
    <main id="main" class="container">


        <div class="row page-wrapper">
            <div id="content" class="large-12 col-12" role="main">

                <header class="entry-header text-center">
                    <h1 class="entry-title">Liên hệ</h1>
                    <div class="is-divider medium"></div>
                </header>

                <div class="entry-content">
                    <div class="row hotro">
                        <div class="col medium-6  small-12 large-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="col-inner">
                                <div class="nz-tensp">Giới thiệu về chúng tôi</div>
                                <div class="row row-collapse align-middle infohotro2" id="row-534330868">
                                    <div class="col small-12 large-12">
                                        <div class="col-inner text-left">
                                            <strong>{{ $information['ten-cong-ty'] }}</strong></br>
                                            <strong>Địa chỉ:</strong> {{ $information['dia-chi'] }}</br>
                                            <strong>Tel:</strong> {{ $information['tel'] }}</br>
                                            <strong>Email:</strong> {{ $information['email'] }}</br>
                                            <strong>Số ĐKKD:</strong> {{ $information['so-dang-ky-kinh-doanh'] }}
                                            <div class="nz-tensp">Chúng tôi sẽ gọi ngay cho bạn</div>
                                        </div>
                                    </div>

                                    <style scope="scope">

                                    </style>
                                </div>
                                <div role="form" class="wpcf7" id="wpcf7-f3055-p3124-o1" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <form action="{{ route('sub_contact') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="nz-cf7">
                                            <div class="nz-cf7-f1 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-name">
                                                                <input type="text" name="name" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name"
                                                                       aria-required="true" aria-invalid="false" placeholder="Tên quý khách" required/>
                                                            </span>
                                            </div>
                                            <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-phone">
                                                                <input type="tel" name="phone" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                                                       id="your-phone" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" required/>
                                                            </span>
                                            </div>
                                            <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-email">
                                                                <input type="email" name="email" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                       id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" required/></span>
                                            </div>
                                            <div class="nz-cf7-f1">
                                                            <span class="wpcf7-form-control-wrap your-message">
                                                                <textarea name="message" cols="40" rows="10"
                                                                          class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                                                          aria-required="true" aria-invalid="false" placeholder="Lời nhắn thêm (có thể để trống)"></textarea></span>
                                            </div>
                                            <div class="nz-cf7-f1">
                                                <input type="submit" value="Gửi tin yêu cầu tư vấn" class="wpcf7-form-control wpcf7-submit c-button" />
                                            </div>
                                        </div>
                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col medium-6 small-12 large-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="col-inner">
                                <div class="nz-tensp">Bản đồ dẫn đường</div>
                                <p>
                                    {!! $information['nhung-ban-do'] !!}
                                </p>
                            </div>
                        </div>

                        <style scope="scope">

                        </style>
                    </div>

                </div>


            </div><!-- #content -->
        </div><!-- .row -->


    </main>
@endsection
