@extends('zito.layout.site')


@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description',  !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
  
	<section class="beachrum" style="border-bottom: 1px solid #ececec;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<ul>
						<li>
							<a class="color" href="/"><i class="fa fa-home" aria-hidden="true"></i></a>
							
						</li>
						<li class="bgbrum">
							<a class="color" href="{{ route('site_category_product', [ 'cate_slug' => $category->slug]) }}">Cửa hàng</a>
						</li>
						<li class="bgbrum">
							<a class="color" href="{{ route('site_category_product', [ 'cate_slug' => $category->slug]) }}">{{ $category->title}}</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="orderBy">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="pull-right rightOrder">
						<form action="/cua-hang/{{ $category->slug }}" method="get" class="sortProduct woocommerce-ordering">
							<select name="sort" class="orderby">
								<option value="">Sắp xếp</option>
								<option value="priceIncrease"
										{{ (isset($_GET['sort']) && ($_GET['sort'] == 'priceIncrease') ) ? 'selected' : ''}}>
									Giá: từ thấp đến cao</option>
								<option value="priceReduction"
										{{ (isset($_GET['sort']) && ($_GET['sort'] == 'priceReduction') ) ? 'selected' : ''}}>Giá: từ cao đến thấp</option>
								<option value="sortName"
										{{ (isset($_GET['sort']) && ($_GET['sort'] == 'sortName') ) ? 'selected' : ''}}>Sắp xếp theo tên</option>
							</select>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

    <main id="main" class="">
        <div class="container">
        <div class="row category-page-row categoryPadding">
        @include('zito.partials.sidebar-product')
        <!-- #shop-sidebar -->
            <div class="col large-9">
                <div class="shop-container">
                    <div class="term-description">
                        <p>

                        @include('zito.partials.slider')
                        {!! $category->description !!}
                        </p>
                    </div>
                    <div class="products row row-small large-columns-3 medium-columns-3 small-columns-2 has-shadow row-box-shadow-1 row-box-shadow-3-hover">
                        @foreach($products as $id => $product )
                        <div class="product-small col has-hover post-4201 product type-product status-publish has-post-thumbnail product_cat-san-pham-noi-bat product_cat-sofa-go product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l first instock shipping-taxable purchasable product-type-simple">
                            <div class="col-inner">
                                <div class="badge-container absolute left top z-1"></div>
                                <div class="product-small box ">
                                    <div class="box-image">
                                        <div class="image-none CropImg">
                                            <a class="thumbs" href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                                                <img src="{!! asset($product->image) !!}"
                                                    
                                                     alt="{!! $product->title !!}" title="{!! $product->title !!}" />
                                            </a>
                                        </div>
                                        <div class="image-tools is-small top right show-on-hover">
                                        </div>
                                        <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                                        </div>
                                        <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        </div>
                                    </div>
                                    <!-- box-image -->
                                    <div class="box-text box-text-products text-center grid-style-2">
                                        <div class="title-wrapper">
                                            <p class="name product-title">
                                                <a class="color" href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{!! $product->title !!}</a>
                                            </p>
                                        </div>
                                        <div class="price-wrapper">
                                            <span class="price">
											 <p style="color:red"><span style="color: #444;margin-right: 5px"><del style="    font-size: 16px;">
											<?php if($product->discount > 0)
											{
												echo number_format($product->price).'';
											}
											else
											{

											}
											?>
										</del>
									</span>
									<span style="    font-size: 17px;
    font-weight: 500;">
										<?php if($product->discount > 0)
										{
											echo number_format($product->discount).'đ';
										}
										else
										{
											echo number_format($product->price).'đ';
										}
										?>
											</span></p>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- box-text -->
                                </div>
                                <!-- box -->
                            </div>
                            <!-- .col-inner -->
                        </div>
                        <!-- col -->
                        @endforeach
                    </div>
                    <!-- row -->

                    <div class="container">
                        @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                            {{ $products->links() }}
                        @endif
                    </div>
                </div>
                <!-- shop container -->
            </div>
        </div>
        </div>
    </main>
@endsection
