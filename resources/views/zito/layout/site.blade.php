<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />   
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />

    <!-- Favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ isset($information['fa-vicon']) ? $information['fa-vicon'] : ''}}">
    <!-- FONTS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('zito/css/font-awesome.min.css') }}">
    <link rel='stylesheet' id='contact-form-7-css'  href='{{ asset('zito/css/styles.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='yith_wcas_frontend-css'  href='{{ asset('zito/css/yith_wcas_ajax_search.css') }}' type='text/css' media='all' />

    <style id='yith_wcas_frontend-inline-css' type='text/css'>
        .autocomplete-suggestion{
            padding-right: 20px;
        }
        .woocommerce .autocomplete-suggestion  span.yith_wcas_result_on_sale,
        .autocomplete-suggestion  span.yith_wcas_result_on_sale{
            background: #7eb742;
            color: #ffffff
        }
        .woocommerce .autocomplete-suggestion  span.yith_wcas_result_outofstock,
        .autocomplete-suggestion  span.yith_wcas_result_outofstock{
            background: #7a7a7a;
            color: #ffffff
        }
        .woocommerce .autocomplete-suggestion  span.yith_wcas_result_featured,
        .autocomplete-suggestion  span.yith_wcas_result_featured{
            background: #c0392b;
            color: #ffffff
        }
        .autocomplete-suggestion img{
            width: 50px;
        }
        .autocomplete-suggestion .yith_wcas_result_content .title{
            color: #004b91;
        }
        .autocomplete-suggestion{
            min-height: 60px;
        }
    </style>

    <link rel='stylesheet' id='flatsome-main-css'  href='{{ asset('zito/css/flatsome.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-shop-css'  href='{{ asset('zito/css/flatsome-shop.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-style-css'  href='{{ asset('zito/css/style1.css') }}' type='text/css' media='all' />
    <!-- BOOTSTRAP CSS
   ============================================ -->
    {{--<link rel="stylesheet" href="{{ asset('store-02/css/bootstrap.min.css') }}">--}}
    <script type='text/javascript' src='{{ asset('vn3c/js/jquery.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/jquery-migrate.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/advanced-cf7-db-public.js') }}'></script>

    {{--<!-- bootstrap js -->--}}
    {{--<script src="{{ asset('store-02/js/bootstrap.min.js') }}"></script>--}}

    <style>.bg{opacity: 0; transition: opacity 1s; -webkit-transition: opacity 1s;} .bg-loaded{opacity: 1;}</style>

    <style>.product-gallery img.lazy-load, .product-small img.lazy-load, .product-small img[data-lazy-srcset]:not(.lazyloaded){ padding-top: 61.538461538462%;}</style>
    <noscript>
        <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
    </noscript>
    <style id="custom-css" type="text/css">:root {--primary-color: #000000;}/* Site Width */.full-width .ubermenu-nav, .container, .row{max-width: 1200px}.row.row-collapse{max-width: 1170px}.row.row-small{max-width: 1192.5px}.row.row-large{max-width: 1230px}#main,#main.dark{background-color: #FFFFFF}.header-main{height: 116px}#logo img{max-height: 116px}#logo{width:241px;}#logo a{max-width:200px;}.header-top{min-height: 40px}.transparent .header-main{height: 265px}.transparent #logo img{max-height: 265px}.has-transparent + .page-title:first-of-type,.has-transparent + #main > .page-title,.has-transparent + #main > div > .page-title,.has-transparent + #main .page-header-wrapper:first-of-type .page-title{padding-top: 295px;}.header.show-on-scroll,.stuck .header-main{height:70px!important}.stuck #logo img{max-height: 70px!important}.search-form{ width: 100%;}.header-bottom {background-color: #f1f1f1}.header-main .nav > li > a{line-height: 13px }.header-wrapper:not(.stuck) .header-main .header-nav{margin-top: -1px }.stuck .header-main .nav > li > a{line-height: 50px }@media (max-width: 549px) {.header-main{height: 70px}#logo img{max-height: 70px}}.header-top{background-color:#F1F1F1!important;}/* Color */.accordion-title.active, .has-icon-bg .icon .icon-inner,.logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active> a,.nav-outline >li.active > a, .cart-icon strong,[data-color='primary'], .is-outline.primary{color: #000000;}/* Color !important */[data-text-color="primary"]{color: #000000!important;}/* Background */.scroll-to-bullets a,.featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline),.featured-table .title,.is-outline:hover, .has-icon:hover .icon-label,.nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover,.grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt,.nav-box > li > a:hover,.nav-box > li.active > a,.nav-pills > li.active > a ,.current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before,.banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner{background-color: none;}/* Border */.nav-vertical.nav-tabs > li.active > a,.scroll-to-bullets a.active,.nav-pagination > li > .current,.nav-pagination > li > span:hover,.nav-pagination > li > a:hover,.has-hover:hover .badge-outline .badge-inner,.accordion-title.active,.featured-table,.is-outline:hover, .tagcloud a:hover,blockquote, .has-border, .cart-icon strong:after,.cart-icon strong,.blockUI:before, .processing:before,.loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover{border-color: #000000}.nav-tabs > li.active > a{border-top-color: #000000}/* Fill */.slider .flickity-prev-next-button:hover svg,.slider .flickity-prev-next-button:hover .arrow{fill: #000000;}/* Background Color */[data-icon-label]:after, .secondary.is-underline:hover,.secondary.is-outline:hover,.icon-label,.button.secondary:not(.is-outline),.button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button{ background-color:#FDCC30; }/* Color */.secondary.is-underline,.secondary.is-link, .secondary.is-outline,.stars a.active, .star-rating:before, .woocommerce-page .star-rating:before,.star-rating span:before, .color-secondary{color: #FDCC30}/* Color !important */[data-text-color="secondary"]{color: #FDCC30!important;}/* Border */.secondary.is-outline:hover{border-color:#FDCC30}.success.is-underline:hover,.success.is-outline:hover,.success{background-color: #1BA79D}.success-color, .success.is-link, .success.is-outline{color: #1BA79D;}.success-border{border-color: #1BA79D!important;}.alert.is-underline:hover,.alert.is-outline:hover,.alert{background-color: #C69A66}.alert.is-link, .alert.is-outline, .color-alert{color: #C69A66;}body{font-size: 105%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family:"Roboto Condensed", sans-serif}body{font-weight: 300}body{color: #000000}.nav > li > a {font-family:"Roboto Condensed", sans-serif;}.nav > li > a {font-weight: 0;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: "Open Sans Condensed", sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2{font-weight: 700;}h1,h2,h3,h4,h5,h6,.heading-font{color: #1BA79D;}.section-title span{text-transform: none;}h3.widget-title{text-transform: none;}button,.button{text-transform: none;}.alt-font{font-family: "Roboto", sans-serif;}.header:not(.transparent) .header-nav.nav > li > a {color: #000000;}.header:not(.transparent) .header-nav.nav > li > a:hover,.header:not(.transparent) .header-nav.nav > li.active > a,.header:not(.transparent) .header-nav.nav > li.current > a,.header:not(.transparent) .header-nav.nav > li > a.active,.header:not(.transparent) .header-nav.nav > li > a.current{color: #FDCC30;}.header-nav.nav-line-bottom > li > a:before,.header-nav.nav-line-grow > li > a:before,.header-nav.nav-line > li > a:before,.header-nav.nav-box > li > a:hover,.header-nav.nav-box > li.active > a,.header-nav.nav-pills > li > a:hover,.header-nav.nav-pills > li.active > a{color:#FFF!important;background-color: #FDCC30;}a{color: #000000;}a:hover{color: #1BA79D;}.tagcloud a:hover{border-color: #1BA79D;background-color: #1BA79D;}.shop-page-title.featured-title .title-bg{background-image: url(https://dv3.zweb.design/wp-content/uploads/2017/10/nz-footer.jpg);}@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 260px!important;width: 260px!important;}}.absolute-footer, html{background-color: #1E1E1E}.label-new.menu-item > a:after{content:"New";}.label-hot.menu-item > a:after{content:"Hot";}.label-sale.menu-item > a:after{content:"Sale";}.label-popular.menu-item > a:after{content:"Popular";}</style>

    <link rel='stylesheet' id='flatsome-effects-css'  href='{{ asset('zito/css/effects.css') }}' type='text/css' media='all' />

    <script type='text/javascript' src='{{ asset('zito/js/scripts.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/add-to-cart.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/jquery.blockUI.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/js.cookie.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/woocommerce.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/cart-fragments.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/yith-autocomplete.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/flatsome-live-search.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/hoverIntent.min.js') }}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var flatsomeVars = {"ajaxurl":"0","rtl":"","sticky_height":"70"};
        /* ]]> */
    </script>
    <!-- <script type='text/javascript' src='{{ asset('zito/js/flatsome.js') }}'></script>-->
     <!-- <script type='text/javascript' src='{{ asset('zito/js/flatsome-lazy-load.js') }}'></script>-->
    <!--<script type='text/javascript' src='{{ asset('zito/js/woocommerce.js') }}'></script> -->
    <script type='text/javascript' src='{{ asset('zito/js/wp-embed.min.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/packery.pkgd.min.js') }}'></script>
    {{--<script type='text/javascript' src='{{ asset('zito/js/zxcvbn-async.min.js') }}'></script>--}}

    <script type='text/javascript' src='{{ asset('zito/js/password-strength-meter.min.js') }}'></script>

    <script type='text/javascript' src='{{ asset('zito/js/password-strength-meter.min.js') }}'></script>
    <script src="{{ asset('js/numeral.min.js') }}"></script>
    {{--http://erado.vn/--}}


    <link rel='stylesheet' href='{{ asset('zito/css/bootstrap.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{ asset('zito/css/font-awesome.css') }}' type='text/css' media='all' />
    <link rel='stylesheet'  href='{{ asset('zito/css/eroda.css') }}' type='text/css' media='all' />


    <script type='text/javascript' src='{{ asset('zito/js/jquery3-3.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/bootstrap.bundle.js') }}'></script>
    <script type='text/javascript' src='{{ asset('zito/js/bootstrap.js') }}'></script>
</head>

<body data-rsssl=1 class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3056 ywcas-flatsome-child yith-wcan-pro full-width lightbox lazy-icons nav-dropdown-has-arrow">
	{!! isset($information['nhung-google-analytics']) ? $information['nhung-google-analytics'] : '' !!}
	{!! isset($information['ma-nhung-facebook']) ? $information['ma-nhung-facebook'] : '' !!}
		 <a class="skip-link screen-reader-text" href="#main">Skip to content</a>
        <div id="wrapper">
            @include('zito.common.header')
            @yield('content')

            @include('zito.common.footer')

            @include('zito.common.header-mobile')
        </div>
    <!-- Phần nội dung -->


    <!-------FOTTER----->


    {{--@include('zito.common.login')--}}
    <script>

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: '{!! route('addToCart') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    // alert('thêm mới giỏ hàng thành công');
                    window.location.replace("/gio-hang");
                },
                error: function(error) {
                    alert('Lỗi gì đó đã xảy ra!')
                }

            });

            return false;
        }

        jQuery(function($) {
		// $(window).load(function() {
		// 	// Animate loader off screen
		// 	$(".loadding").fadeOut("slow");;
		// });
        // $( document ).ready(function() {
         //    var x = setInterval(function() {
         //        $('.timeDiscount').each(function(index) {
        //
         //            var countDownDate = new Date($(this).val()).getTime();
        //
         //            // Get todays date and time
         //            var now = new Date().getTime();
        //
         //            // Find the distance between now an the count down date
         //            var distance = countDownDate - now;
        //
         //            // Time calculations for days, hours, minutes and seconds
         //            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
         //            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
         //            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
         //            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        //
         //            $(this).parent().find('.day').html(days + ' Ngày');
         //            $(this).parent().find('.hour').html(hours + ' Giờ');
         //            $(this).parent().find('.minute').html(minutes + ' Phút');
         //            $(this).parent().find('.second').html(seconds + ' Giây');
        //
         //            // If the count down is over, write some text
         //            if (distance < 0) {
         //                clearInterval(x);
         //                $(this).parent().find('.day').html('');
         //                $(this).parent().find('.hour').html('');
         //                $(this).parent().find('.minute').html('');
         //                $(this).parent().find('.second').html('');
         //            }
        //
         //        });
         //    }, 1000);
        //
        //
        //
        // });
        
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }



        function showLogin(e) {
            $('#myModalLogin .notify ').empty();
            $('#myModalLogin').modal('show');
        }
        
        function showMenu(e) {
            if ($(e).hasClass('active')) {
                $(e).addClass('active');
            } else {
                $(e).removeClass('active')
            }
        }
		
		//top
		// Hide the toTop button when the page loads.
		$(".back-to-top").css("display", "none");

		// This function runs every time the user scrolls the page.
		$(window).scroll(function(){

			// Check weather the user has scrolled down (if "scrollTop()"" is more than 0)
			if($(window).scrollTop() > 0){

				// If it's more than or equal to 0, show the toTop button.
				console.log("is more");
				$(".back-to-top").fadeIn("slow");

			}
			else {
				// If it's less than 0 (at the top), hide the toTop button.
				console.log("is less");
				$(".back-to-top").fadeOut("slow");
			}
		});


		// When the user clicks the toTop button, we want the page to scroll to the top.
		$(".back-to-top").click(function(){

			// Disable the default behaviour when a user clicks an empty anchor link.
			// (The page jumps to the top instead of // animating)
			event.preventDefault();

			// Animate the scrolling motion.
			$("html, body").animate({
				scrollTop:0
			},"slow");
		});

            $(document).ready(function() {
                $('#popupVitural').hide();
                setInterval(function(){
                    $('#popupVitural').show().slideDown();
                }, 5000);

                $('#popupVitural .Closed').click(function(){
                    $('#popupVitural').slideUp().hide();
                    $('#popupVitural').addClass('hide');
                });
            });
        });
		
    </script>

    @include('zito.order.popup_add_cart')
    {{--@include('zito.partials.forget_password')--}}

</body>
</html>
