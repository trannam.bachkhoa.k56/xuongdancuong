<div class="product offStore">
    <div class="deliver-top info ">
        <div class="tit-color">Sẽ có tại nhà bạn</div>
        <div class="descrip">Từ 1-5 ngày làm việc</div>
    </div>
    <div class="deliver-top-no deliver-dt">
        <div class="deliver-icon "><img src="{{ asset('site/img/icon-deliver.png') }}">
        </div>
        <a href="javascript:void(0)">
            <div class="tit">Giao hàng miễn phí</div>
            <div class="descrip">sản phẩm trên 300,000đ</div>
        </a>
    </div>
    <div class="deliver-top-no deliver-ch">
        <div class="deliver-icon"><img src="{{ asset('site/img/icon-day90.png') }}">
        </div>
        <a href="javascript:void(0)">
            <div class="tit">Đổi trả miễn phí</div>
            <div class="descrip">Đổi trả miễn phí 90 ngày</div>
        </a>
    </div>
    <div class="deliver-top-no deliver-pay">
        <div class="deliver-icon "><img src="{{ asset('site/img/icon-pay.png') }}">
        </div>
        <a href="javascript:void(0)">
            <div class="tit">thanh toán</div>
            <div class="descrip">Thanh toán khi nhận hàng</div>
        </a>
    </div>
    <div class="deliver-top-no deliver-phone">
        <div class="deliver-icon "><img src="{{ asset('site/img/icon-phonehot.png') }}">
        </div>
        <div class="tit">Hỗ trợ mua nhanh</div>
        <div class="tit-color">01232779888</div>
        <div class="descrip">từ 8:30 - 21:30 mỗi ngày</div>
    </div>
</div>
