<section class="slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
			@foreach (\App\Entity\SubPost::showSubPost('slide', 10) as $id => $slide)
				<li data-target="#carouselExampleIndicators" data-slide-to="{{ $id }}" class="<?php if($id == 0 )
                    {
                        echo 'active';
                    }?>"></li>
			@endforeach
        </ol>
        <div class="carousel-inner">
            @foreach (\App\Entity\SubPost::showSubPost('slide', 10) as $id => $slide)
                <div class="carousel-item <?php if($id == 0 )
                    {
                        echo 'active';
                    }?> ">
					<a href="{{ $slide['duong-dan-slide'] }}">
                    <img class="d-block w-100" src="{{ asset($slide->image) }}" alt="First slide">
					</a>
                </div>
            @endforeach

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

{{--@foreach (\App\Entity\SubPost::showSubPost('slide', 10) as $id => $slide)--}}
    {{--<div class="carousel-item active">--}}
        {{--<img class="d-block w-100" src="{{ asset($slide->image) }}" alt="First slide">--}}
    {{--</div>--}}
{{--@endforeach--}}