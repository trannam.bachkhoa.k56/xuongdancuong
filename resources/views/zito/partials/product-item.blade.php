
<div class="col-lg-3 col-md-6 col-sm-12 col-12">
    <div class="product-small mgbottom20 has-hover post-166 product type-product status-publish has-post-thumbnail product_cat-phong-khach
    product_cat-san-pham-ban-chay product_cat-san-pham-noi-bat product_cat-sofa-go product_cat-sofa-goc
    product_tag-sofa-go-dem-ni product_tag-sofa-go-goc-chu-l last instock shipping-taxable purchasable product-type-simple

    ">
        <div class="col-inner">
            <div class="badge-container absolute left top z-1"></div>
            <div class="product-small box ">
                <div class="box-image">
                    <div class="image-none CropImg">
                        <a class="thumbs" href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                            <img width="260" height="160" src="{{ asset($product->image) }}"
                                title="{{ $product->title }}" srcset="" />
                        </a>
                    </div>
                    <div class="image-tools is-small top right show-on-hover">
                    </div>
                    <div class="image-tools is-small hide-for-small bottom left show-on-hover">
                    </div>
                    <div class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                    </div>
                </div>
                <!-- box-image -->
                <div class="box-text box-text-products text-center grid-style-2">
                    <div class="title-wrapper">
                        <p class="name product-title"><a class="color" href="{{ route('product', [ 'post_slug' => $product->slug]) }}">{{ $product->title }}</a></p>
                    </div>
                    <div class="price-wrapper">
                        <p class="price">Giá :
                            <span style="color: #444;margin-right: 5px">
                                        <del>
                                            <?php if($product->discount > 0)
                                            {
                                                echo number_format($product->price).'';
                                            }
                                            else
                                            {

                                            }
                                            ?>
                                        </del>
                                    </span>
                            <span style="color: red;
    font-weight: 700;
    font-size: 15px;">
                                        <?php if($product->discount > 0)
                                {
                                    echo number_format($product->discount);
                                }
                                else
                                {
                                    echo number_format($product->price);
                                }
                                ?>
                                    </span> đ
                        </p>
                    </div>
                </div>
                <!-- box-text -->
            </div>
            <!-- box -->
        </div>
        <!-- .col-inner -->
    </div>
</div>