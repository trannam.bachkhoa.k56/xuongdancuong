
<section class="artist hiddenMb">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 sologen" align="center">
                <h3 style="text-transform: uppercase">"{{ $information['sologan'] }}"</h3>

                <div class="bordertitle">
                </div>
            </div>
        </div>
        <div class="row">

            @foreach (\App\Entity\SubPost::showSubPost('slogan', 4) as $id => $slogan)
                <div class="col-lg-3 col-md-6 itemSologen" align="center">
                    <div title="" class="bgsologan" id="img{{ $id }}" style="height: 105px; background: url('{{ asset($slogan->image) ? asset($slogan->image) : '' }}')no-repeat center top">
                    </div>
                    <p class="title"><a href="" title="">{{ $slogan->title }}</a></p>
                    <p class="content">{{ $slogan->description }}
                    </p>

                </div>
                <style>
                    #img{!! $id !!}:hover
                    {
                        background: url('{{ asset($slogan['anh-hover']) ? asset($slogan['anh-hover']) : '' }}') no-repeat center top !important;
                    }
                </style>
            @endforeach

        </div>
    </div>
</section>