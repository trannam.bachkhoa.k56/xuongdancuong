

<div class="col large-3 hide-for-medium col-lg-3 col-md-3 col-sm-12 col-12">
    <div id="shop-sidebar" class="sidebar-inner col-inner">
        <!--<aside id="search-6" class="widget widget_search">
            <span class="widget-title shop-sidebar">Tìm kiếm</span>
            <div class="is-divider small"></div>
            <form method="get" action="{{ route('search_product') }}" >
                <div class="flex-row relative">
                    <div class="flex-col flex-grow">
                        <input type="search" class="search-field mb-0" name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" placeholder="Tìm kiếm..." />
                    </div>
                   
                    <div class="flex-col">
                        <button type="submit" class="ux-search-submit submit-button secondary button icon mb-0">
                            <i class="icon-search" ></i>				</button>
                    </div>
                    
                </div>
                
                <div class="live-search-results text-left z-top"></div>
            </form>
        </aside>-->
        <aside id="woocommerce_product_categories-2" class="widget woocommerce widget_product_categories">
            <span class="widget-title shop-sidebar">Danh mục sản phẩm</span>
            <div class="is-divider small"></div>
            <ul class="product-categories">
                @foreach (\App\Entity\Menu::showWithLocation('menu-product') as $menu)
                    @foreach (\App\Entity\MenuElement::showMenuElement($menu->slug) as $element)
                        <li class="cat-item cat-item-33">
                            <a href="{!! $element->url !!}">{!! $element->title_show !!}</a>
                        </li>
                    @endforeach
                @endforeach
            </ul>
        </aside>
        <aside id="text-2" class="widget widget_text">
            <div class="textwidget">
                <p><img class="aligncenter size-full wp-image-4246" src="{!! $information['anh-quang-cao-trang-san-pham'] !!}" alt="" width="300" height="783" /></p>
            </div>
        </aside>
        <aside id="woocommerce_products-2" class="widget woocommerce widget_products sideBarPro">
            <span class="widget-title shop-sidebar">Sản phẩm mới</span>
            <div class="is-divider small"></div>
            <ul class="product_list_widget">
                @foreach (\App\Entity\Product::newProduct(5) as $product)
                <li>
                    <a class="color titlesi" href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                        <img width="130" height="80"
                             src="{{ asset($product->image) }}"
                             alt="{{ $product->title }}" />
                        <span class="product-title">{{ $product->title }}</span>
                    </a>
                    <span class="woocommerce-Price-amount amount">
                        <span style="color: #444;margin-right: 5px">
                                    <del style="font-size: 13px;">
                                        <?php if($product->discount > 0)
                                        {
                                            echo number_format($product->price).'</br>';
                                        }
                                        else
                                        {

                                        }
                                        ?>
                                    </del>
                                </span>
                                <span style="    font-size: 16px;
    font-weight: 500;">
                                    <?php if($product->discount > 0)
                                    {
                                        echo number_format($product->discount).'đ';
                                    }
                                    else
                                    {
                                        echo number_format($product->price).'đ';
                                    }
                                    ?>
                                </span>
                    </span>
                </li>
                @endforeach
            </ul>
        </aside>
        <style>.rpwe-block ul{
                background: white;
                list-style: none !important;
                margin-left: 0 !important;
                padding-left: 0 !important;
            }
            .rpwe-block li{    margin-left: 0 !important;
                text-align: left;
                border: 1px solid #eee;
                margin-bottom: 0px;
                padding: 5px 10px;
                list-style-type: none;
            }
            .rpwe-block a{
                text-decoration: none;
                font-size: 14px;
                font-weight: normal;
                font-family: "Roboto Condensed", sans-serif;
            }
            .rpwe-block h3{
                background: none !important;
                clear: none;
                margin-bottom: 0 !important;
                margin-top: 0 !important;
                font-weight: 400;
                font-size: 12px !important;
                line-height: 1.5em;
            }
            .rpwe-thumb{
                border-radius:3px;
                box-shadow: none !important;
                margin: 2px 10px 2px 0;
            }
            .rpwe-summary {
                font-size: 20px;
                color: red;
            }
            .rpwe-summary:before {
                font-family: FontAwesome;
                content: "\f095 ";
                padding-right: 5px;
            }
            .rpwe-time{
                color: #909090;
                font-size: 11px;
            }
            .rpwe-comment{
                color: #bbb;
                font-size: 11px;
                padding-left: 5px;
            }
            .rpwe-alignleft{
                display: inline;
                float: left;
            }
            .rpwe-alignright{
                display: inline;
                float: right;
            }
            .rpwe-aligncenter{
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
            .rpwe-clearfix:before,
            .rpwe-clearfix:after{
                content: "";
                display: table !important;
            }
            .rpwe-clearfix:after{
                clear: both;
            }
            .rpwe-clearfix{
                zoom: 1;
            }
        </style>
        <aside id="rpwe_widget-2" class="widget rpwe_widget recent-posts-extended">
            <span class="widget-title shop-sidebar">Tin tức mới</span>
            <div class="is-divider small"></div>
            <div  class="rpwe-block ">
                <ul class="rpwe-ul">
                    @foreach (\App\Entity\Post::newPost('tin-tuc', 5) as $post)
                    <li class="rpwe-li rpwe-clearfix">
                        <a class="rpwe-img" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}"  rel="bookmark">
                            <img width="130" height="80" class="rpwe-alignleft rpwe-thumb"
                                 src="{!! asset($post->image) !!}" alt="{!! $post->title !!}">
                        </a>
                        <h3 class="rpwe-title">
                            <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="{{ $post->title }}" rel="bookmark">
                                {{ $post->title }}
                            </a>
                        </h3>
                        <?php $date=date_create($post->created_at); ?>
                        <time class="rpwe-time published" datetime="<?= date_format($date,"d-m-Y") ?>"> <?= date_format($date,"d-m-Y") ?></time>
                    </li>
                    @endforeach
                </ul>
            </div>
            <!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ -->
        </aside>
    </div>
    <!-- .sidebar-inner -->
</div>
                 