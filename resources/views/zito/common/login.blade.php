<!-- Modal -->
<div class="modal fade" id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content_1">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user" aria-hidden="true"></i> Đăng nhập</h4>
            </div>
            <form action="/dang-nhap" class="submitDelete" method="post" >
                {!! csrf_field() !!}

                <div class="modal-body clearfix">
                    <p class="notify red"></p>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <input id="email" type="email" class="form-control" name="email"  placeholder="Nhập email" required autofocus>
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" required>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Ghi nhớ mật khẩu
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger">Đăng nhập</button>
                        </div>
                        <div class="form-group">
                            <a href="{{ $urlLoginFace }}" class="btn btn-primary"><i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</a>
                            <a href="{{ route('google_login') }}" class="btn btn-danger"><i class="fa fa-google-plus-square" aria-hidden="true"></i> google</a>
                        </div>

                        <div class="form-group">
                            <a class="btn btn-link" href="#" onclick="return forgetPassword(this);">
                                Quên mật khẩu?
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <p>Nếu bạn chưa có tài khoản</p>
                        <div class="form-group mb20">
                            <a href="/dang-ky" class="col-xs-12 btn btn-primary">Đăng ký tài khoản</a> <br>
                        </div>
                        <div class="form-group">
                            <a href="{{ $urlLoginFace }}" class="btn btn-primary"><i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook</a>
                            <a href="{{ route('google_login') }}" class="btn btn-danger"><i class="fa fa-google-plus-square" aria-hidden="true"></i> google</a>
                        </div>
                    </div>


                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .modal-content_1 {
        background: #fff;
    }
    .modal-body {
        background: #fff;
    }
</style>
<script>
    function forgetPassword() {
        $('#myModalForget').modal('show');
        $('#myModalLogin').modal('hide');
    }
</script>


<div id="login-form-popup" class="lightbox-content mfp-hide">
    <div class="account-container lightbox-inner">
        <div class="account-login-inner">
            <h3 class="uppercase">Đăng nhập</h3>
            <form method="post" class="login">
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <label for="username">Tên tài khoản hoặc địa chỉ email <span class="required">*</span></label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="" />
                </p>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <label for="password">Mật khẩu <span class="required">*</span></label>
                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
                </p>
                <p class="form-row">
                    <input type="hidden" id="_wpnonce" name="_wpnonce" value="55fde2adf9" /><input type="hidden" name="_wp_http_referer" value="/" />				<input type="submit" class="woocommerce-Button button" name="login" value="Đăng nhập" />
                    <label for="rememberme" class="inline">
                        <input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> Ghi nhớ mật khẩu				</label>
                </p>
                <p class="woocommerce-LostPassword lost_password">
                    <a href="https://zito.vn/lost-password/">Quên mật khẩu?</a>
                </p>
            </form>
        </div>
        <!-- .login-inner -->
    </div>
    <!-- .account-login-container -->
</div>
