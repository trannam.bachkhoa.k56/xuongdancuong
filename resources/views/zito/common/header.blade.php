<header id="header" class="vn3c">
    <div class="headertop">
        <div class="container">
            <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-12 hiddenTab">
					<div class="pull-left" style="line-height: 33px;">
						<div class="fb-like" data-href="{{ isset($information['link-fb']) ?$information['link-fb']  :'https://www.facebook.com/' }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
						
					</div>
					
					
				</div>
				
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
					<div class="pull-right">
						<ul>
							@foreach (\App\Entity\Menu::showWithLocation('menu-phu-tren-cung') as $menu)
								@foreach (\App\Entity\MenuElement::showMenuElement($menu->slug) as $element)
								 <li><a href="{!! $element->url !!}" title="" class="color">{!! $element->title_show !!}</a></li>
								@endforeach
							@endforeach
								<li> 
									<?php $countOrder = \App\Entity\Order::countOrder();?>
									@if($countOrder > 0)	
									<a href="/gio-hang/"
										class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup"
										data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">
										<span class="header-cart-title">Giỏ hàng</span>
										
										<span class="cart-icon image-icon"><strong>{{ $countOrder }}</strong></span>
									</a>
									@else
										<a href="#"
										class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup"
										data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">
										<span class="header-cart-title">Giỏ hàng</span>
										
										<span class="cart-icon image-icon"><strong>{{ $countOrder }}</strong></span>
									</a>
									@endif	
								</li>
						</ul>
					</div>
                </div>
            </div>
        </div>

    </div>
    <div class="headercontent">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="logo">
                        <a href="/"><img src="{{ $information['logo'] }}" alt="" class=""></a>


                        <div class="hotline hiddenPc showMb">
                            <?php $countOrder = \App\Entity\Order::countOrder();?>
									@if($countOrder > 0)	
									<a href="/gio-hang/"
										class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup"
										data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">
										<span class="header-cart-title">Giỏ hàng</span>
										
										<span class="cart-icon image-icon"><strong>{{ $countOrder }}</strong></span>
									</a>
									@else
										<a href="#"
										class="header-cart-link off-canvas-toggle nav-top-link is-small" data-open="#cart-popup"
										data-class="off-canvas-cart" title="Giỏ hàng" data-pos="right">
										<span class="header-cart-title">Giỏ hàng</span>
										
										<span class="cart-icon image-icon"><strong>{{ $countOrder }}</strong></span>
									</a>
									@endif	

                             <span style="font-size: 30px;
    margin-right: 10px;color: red;"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                            <span><span class="hiddenTab">Hotline : </span><strong>{{ $information['hotline'] }}</strong></span>
                        </div>

                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="search">
                        <form method="get" action="{{ route('search_product') }}" accept-charset="utf-8">
                            <input type="search" name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" placeholder="Nhập tìm kiếm sản phẩm">
                            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="hotline hiddenMb">
                         <span style="font-size: 30px;
    margin-right: 10px;color: red;"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                        <span><span class="hiddenTab">Hotline : </span><strong>{{ $information['hotline'] }}</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>	
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                    @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $menu)
                                        @foreach (\App\Entity\MenuElement::showMenuElement($menu->slug) as $id=>$element)
                                        <li class="nav-item {{ str_replace("/","",$element->url) }}
                                        <?php if( 
										(empty($_SERVER['REQUEST_URI']) && $id == 0) 
										|| (!empty($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $element->url)
										)
                                            {
                                                echo 'active';
                                            }
                                            else
                                                {
                                                    echo '';
                                                }?>
                                        ">
                                            <a class="nav-link color" href="{!! $element->url !!}">{!! $element->title_show !!}<span class="sr-only">(current)</span></a>
                                        </li>
                                        @endforeach
                                    @endforeach
                                   {{--<li><a href="{!! $element->url !!}" title="" class="">{!! $element->title_show !!}</a></li>--}}
                                {{--<li class="nav-item dropdown">--}}
                                    {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                        {{--ghế sofa--}}
                                    {{--</a>--}}
                                    {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                                        {{--<a class="dropdown-item" href="#">Action</a>--}}
                                        {{--<a class="dropdown-item" href="#">Another action</a>--}}
                                {{--</li>--}}

                            </ul>

                        </div>
                    </nav>

                </div>
            </div>
        </div>

    </div>
</header>








