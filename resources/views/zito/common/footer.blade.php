<footer id="footer" class="footer-wrapper">
    <section class="section vd" id="section_55129917">
        <div class="bg section-bg fill bg-fill  bg-loaded" >
        </div>
        <!-- .section-bg -->
        <div class="section-content relative">
            <div class="container">
                <div class="row row-small align-center"  id="row-1597056707">
                    <div class="col medium-6 small-12 large-6 col-lg-6 col-md-6 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <h3>KHUYẾN MẠI</h3>
                            @foreach (\App\Entity\Post::newPost('khuyen-mai', 1) as $id => $post)
                                <div class="banner has-hover" id="banner-422701026">
                                    <div class="banner-inner fill">
                                        <div class="banner-bg fill" >
                                            <div class="bg fill bg-fill "></div>
                                            <div class="overlay"></div>
                                            <div class="effect-snow bg-effect fill no-click"></div>
                                        </div>
                                        <!-- bg-layers -->
                                        <div class="banner-layers container">
                                            <a href="{{ route('post', ['cate_slug' => 'khuyen-mai', 'post_slug' => $post->slug]) }}"  class="fill">
                                                <div class="fill banner-link CropImg CropImgP">
													<span class="thumbs">
														<img src="{{ asset($post->image) }}" class="attachment-orginal size-orginal" alt="{{ $post->title }}" />
													</span>
												</div>
                                            </a>
                                            <div id="text-box-248751777" class="text-box banner-layer x100 md-x100 lg-x100 y90 md-y90 lg-y90 res-text">
                                                <div class="text ">
                                                    <div class="text-inner text-left">
                                                        <a href="{{ route('post', ['cate_slug' => 'khuyen-mai', 'post_slug' => $post->slug]) }}" target="_self" class="button secondary is-larger" >
                                                            <span>Xem Chi Tiết</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- text-box-inner -->
                                                <style scope="scope">
                                                    #text-box-248751777 {
                                                        width: 60%;
                                                    }
                                                    #text-box-248751777 .text {
                                                        font-size: 100%;
                                                    }
                                                    @media (min-width:550px) {
                                                        #text-box-248751777 {
                                                            width: 40%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <!-- text-box -->
                                        </div>
                                        <!-- .banner-layers -->
                                    </div>
                                    <!-- .banner-inner -->
                                    <div class="height-fix is-invisible">
                                        <img width="960" height="540" src="{{ asset($post->image) }}" class="attachment-orginal size-orginal" alt="{{ $post->title }}" /></div>
                                    <style scope="scope">
                                        #banner-422701026 .bg.bg-loaded {
                                            background-image: url({{ asset($post->image) }});
                                        }
                                        #banner-422701026 .overlay {
                                            background-color: rgba(190, 190, 190, 0.2);
                                        }
                                    </style>
                                </div>

                                <!-- .banner -->
                                <p>{{ $post->description }}</p>
                            @endforeach
                        </div>
                    </div>
                    <div class="col medium-6 small-12 large-6 col-lg-6 col-md-6 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <h3>DÀNH CHO KHÁCH HÀNG</h3>
                            <div class="row large-columns-1 medium-columns-1 small-columns-1 row-small has-shadow row-box-shadow-2 row-box-shadow-3-hover">
                                @foreach (\App\Entity\Post::newPost('tin-tuc', 3) as $id => $post)
                                    <div class="col post-item" >
                                        <div class="col-inner">
                                            <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="plain">
                                                <div class="box box-vertical box-text-bottom box-blog-post has-hover">
                                                    <div class="box-image CropImg" style="width:30%;">
                                                        <span class="image-cover thumbs" style="">
                                                            <img src="{{ asset($post->image) }}"
                                                                 data-src="{{ asset($post->image) }}"
                                                                 class="lazy-load attachment-medium size-medium wp-post-image" alt="{{ $post->title }}"  style="    opacity: 1;
																padding-top: 20px;
																margin-top: 0;" />
                                                        </span>
                                                    </div>
                                                    <!-- .box-image -->
                                                    <div class="box-text text-left" >
                                                        <div class="box-text-inner blog-post-inner">
                                                            <h5 class="post-title is-large color">{{ $post->title }}</h5>
                                                            <div class="is-divider"></div>
                                                            <p class="from_the_blog_excerpt ">{{ $post->description }}</p>
                                                        </div>
                                                        <!-- .box-text-inner -->
                                                    </div>
                                                    <!-- .box-text -->
                                                </div>
                                                <!-- .box -->
                                            </a>
                                            <!-- .link -->
                                        </div>
                                        <!-- .col-inner -->
                                    </div>
                                    <!-- .col -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <style scope="scope">
                    </style>
                </div>
            </div>
        </div>
        <!-- .section-content -->
        <style scope="scope">
            #section_55129917 {
                padding-top: 20px;
                padding-bottom: 20px;
                background-color: rgb(241, 241, 242);
            }
        </style>
    </section>
  <!--  <section class="section dark has-mask mask-arrow" id="section_1147578108">
        <div class="bg section-bg fill bg-fill  " >
            <div class="section-bg-overlay absolute fill"></div>
        </div>
        <div class="section-content relative container">
                <div class="row row-collapse align-middle"  id="row-1104168848">
                   <div class="col medium-4 small-12 large-4 col-lg-4 col-md-4 col-sm-12 col-12" data-animate="bounceInDown" data-animated="true">
                        <div class="col-inner text-center" style="text-decoration: none;"  >
                            <h3>ĐỂ ĐƯỢC TƯ VẤN VÀ HỖ TRỢ TỐT NHẤT</h3>
                            <p><strong>Hãy liên hệ với chúng tôi qua số điện thoại hoặc để lại lời nhắn</strong></p>
                            <a href="tel:{{ $information['hotline'] }}" target="_self" class="button secondary"  >
                                <i class="icon-phone" ></i>  <span>Tel: {{ $information['hotline'] }}</span>
                            </a>
                            <a href="#yeucau" target="_self" class="button secondary is-outline"  >
                                <i class="icon-checkmark" ></i>
                                <span>Gửi yêu cầu</span>
                            </a>
                        </div>
                    </div>
					
                    <div class="col medium-4 small-12 large-4 col-lg-4 col-md-4 col-sm-12 col-12"  >
                        <div class="col-inner" style="padding:10px" >
                            <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_2138128550">
                                <div class="img-inner image-zoom image-cover dark" style="padding-top:250px;">
                                    <img width="1020" height="726"
                                         src="{{ $information['hinh-anh-de-duoc-tu-van'] }}"
                                         class="attachment-large size-large" alt=""  />
                                </div>
                                <style scope="scope">
                                </style>
                            </div>
                        </div>
                    </div>
                    <div class="col medium-4 small-12 large-4 col-lg-4 col-md-4 col-sm-12 col-12"  >
                        <div class="col-inner text-center"  >
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14897.812168406506!2d105.8198263!3d21.0145512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8f6e8770cd87b825!2zU2hvd3Jvb20gTuG7mWkgVGjhuqV0IFRIIEhvbWUgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1524215911561" width="350" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <style scope="scope">
                    </style>
                </div>
           
        </div>
  
        <style scope="scope">
            #section_1147578108 {
                padding-top: 30px;
                padding-bottom: 30px;
                min-height: 350px;
            }
            #section_1147578108 .section-bg-overlay {
                background-color: rgba(0, 0, 0, 0.59);
            }
            #section_1147578108 .section-bg.bg-loaded {
                background-image: url({{ $information['banner-tu-van-ho-tro-tot-nhat'] }});
            }
        </style>
    </section> -->
	
	
	
	
	
    <section class="section kh" id="section_841504452">
        <div class="bg section-bg fill bg-fill  bg-loaded" >
        </div>
        <!-- .section-bg -->
        <div class="section-content relative">
            <div class="container">
                <div class="row align-middle align-center"  id="row-1221361185">
                    <div class="col small-12 large-12 col-lg-12 col-md-12 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <h3 style="text-align: center;">KHÁCH HÀNG CỦA CHÚNG TÔI ĐANG SỐNG TẠI</h3>
                            <div class="slider-wrapper relative " id="slider-811337358" >
                                <div class="slider slider-nav-simple slider-nav-large slider-nav-dark slider-nav-outside slider-style-normal"
                                     data-flickity-options='{
                                    "cellAlign": "center",
                                    "imagesLoaded": true,
                                    "lazyLoad": 1,
                                    "freeScroll": true,
                                    "wrapAround": true,
                                    "autoPlay": 1500,
                                    "pauseAutoPlayOnHover" : false,
                                    "prevNextButtons": true,
                                    "contain" : true,
                                    "adaptiveHeight" : true,
                                    "dragThreshold" : 5,
                                    "percentPosition": true,
                                    "pageDots": false,
                                    "rightToLeft": false,
                                    "draggable": true,
                                    "selectedAttraction": 0.1,
                                    "parallax" : 0,
                                    "friction": 0.6        }'
                                >
                                    @foreach (\App\Entity\SubPost::showSubPost('kh-dang-song', 30) as $id => $post)
                                        <div class="ux-logo has-hover align-middle ux_logo inline-block" style="max-width: 100%!important; width: 228.89502762431px!important">
                                            <div class="ux-logo-link block image-zoom" title="{{ $post->title }}" target="_self" href="" style="padding: 15px;">
                                                <img src="{{ $post->image }}" title="{{ $post->title }}" alt="{{ $post->title }}" class="ux-logo-image block" style="height:120px;" />
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="loading-spin dark large centered"></div>
                                <style scope="scope">
                                </style>
                            </div>
                            <!-- .ux-slider-wrapper -->
                        </div>
                    </div>
                    <style scope="scope">
                        #row-1221361185 > .col > .col-inner {
                            padding: 30px 0px 0px 0px;
                        }
                    </style>
                </div>
            </div>
        </div>
        <!-- .section-content -->
        <style scope="scope">
            #section_841504452 {
                padding-top: 0px;
                padding-bottom: 0px;
                background-color: rgb(255, 255, 255);
            }
        </style>
    </section>
    <section class="section sec0f dark" id="section_216691949">
        <div class="bg section-bg fill bg-fill  bg-loaded" >
        </div>
        <!-- .section-bg -->
        <div class="section-content relative">
            <div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>
            <div class="container">
                <div class="row ic"  id="row-1992458976">
                    <style type="text/css" media="screen">
                        .last-reset h3
                        {
                            font-size: 17px;
                        }
                        .icon-box-img .icon i{
                            font-size: 60px;
                            color: #ffcb44;
                        }
                        i.fa-truck {
                            -webkit-transform: rotateY(180deg);
                            -moz-transform: rotateY(180deg);
                            -o-transform: rotateY(180deg);
                            -ms-transform: rotateY(180deg);
                            transform: rotateY(180deg);
                        }
                    </style>
                    <div class="col medium-4 small-12 large-4 col-lg-4 col-md-4 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <div class="icon-box featured-box icon-box-left text-left"  >
                                <div class="icon-box-img" style="width: 60px">
                                    <div class="icon">
                                        <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="icon-box-text last-reset">
                                    <h3><span style="color: #ffcb44;">Đảm bảo chất lượng</span></h3>
                                    <p>{{ $information['bao-dam-chat-luong'] }}</p>
                                </div>
                            </div>
                            <!-- .icon-box -->
                        </div>
                    </div>
                    <div class="col medium-4 small-12 large-4 col-lg-4 col-md-4 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <div class="icon-box featured-box icon-box-left text-left"  >
                                <div class="icon-box-img" style="width: 60px">
                                    <div class="icon">

                                        <i class="fa fa-truck" aria-hidden="true"></i>

                                    </div>
                                </div>
                                <div class="icon-box-text last-reset">
                                    <h3><span style="color: #ffcb44;">GIAO HÀNG NHANH CHÓNG</span></h3>
                                    <p>{{ $information['giao-hang-nhanh-chong'] }}</p>
                                </div>
                            </div>
                            <!-- .icon-box -->
                        </div>
                    </div>
                    <div class="col medium-4 small-12 large-4 col-lg-4 col-md-4 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <div class="icon-box featured-box icon-box-left text-left"  >
                                <div class="icon-box-img" style="width: 60px">
                                    <div class="icon">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="icon-box-text last-reset">
                                    <h3><span style="color: #ffcb44;">Tư vấn - Hỗ trợ 24/7</span></h3>
                                    <p>{{ $information['tu-van-ho-tro-24-7'] }}</p>
                                </div>
                            </div>
                            <!-- .icon-box -->
                        </div>
                    </div>
                    <style scope="scope">
                    </style>
                </div>
                <div class="row row-small"  id="row-1161105673">
                    <div class="col col4 col41 medium-3 small-12 large-3 col-lg-3 col-md-6 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <h5 class="uppercase">THÔNG TIN</h5>
                            <p style="text-align: justify;">
                                {!! $information['gioi-thieu-ve-cong-ty'] !!}
                            </p>
                            <strong>{{ $information['ten-cong-ty'] }}</strong></br>
                            <strong>Địa chỉ:</strong> {{ $information['dia-chi'] }}</br>
                            <strong>Tel:</strong> {{ $information['tel'] }}</br>
                            <strong>Email:</strong> {{ $information['email'] }}</br>
                            <strong>Số ĐKKD:</strong> {{ $information['so-dang-ky-kinh-doanh'] }}
                        </div>
                    </div>
                    <div class="col col4 col41 medium-2 small-12 large-2 col-lg-3 col-md-6 col-sm-12 col-12"  >
                        <div class="col-inner"  >
                            <h5 class="uppercase">Liên kết nhanh</h5>
                            <div class="menu-main-menu-nz-container">
                                <ul id="menu-main-menu-nz-1" class="menu">
								
								 @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $menu)
                                        @foreach (\App\Entity\MenuElement::showMenuElement($menu->slug) as $id=>$element)
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-3056 current_page_item menu-item-3944">
                                                <a href="{{ $element->url }}">{{ $element->title_show }}</a>
                                            </li>
                                        @endforeach
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col col4 col41 medium-3 small-12 large-3 col-lg-3 col-md-6 col-sm-12 col-12 topTab"  >
                        <div class="col-inner"  >
                            <!-- <h5 class="uppercase" style="text-align: center;">Đăng ký nhận khuyến mãi</h5>
                            <div role="form" class="wpcf7" id="wpcf7-f4110-o1" lang="vi" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <form onsubmit="return subcribeEmailSubmit(this)" class="wpcf7-form demo">
                                    <div class="nz-cf7 dkemail">
                                        <div class="nz-cf7-f1">
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <input type="email" name="your-email" value="" size="40"
                                                       class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email emailSubmit"
                                                       aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" />
                                            </span>
                                        </div>
                                        <div class="nz-cf7-f1">
                                            <input type="submit" value="Đăng ký" class="wpcf7-form-control wpcf7-submit c-button" />
                                        </div>
                                    </div>
                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                </form>
                            </div>
                            <div class="gap-element" style="display:block; height:auto; padding-top:30px" class="clearfix"></div>

                            <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1693504053">
                                <a href="#" target="_self" class="">
                                    <div class="img-inner dark" style="margin:0px 15px 10px 15px;">
                                        <img width="395" height="149"
                                             src="{{ $information['dang-ky-bo-cong-thuong'] }}" class="attachment-large size-large" alt=""
                                        />
                                    </div>
                                </a>
                                <style scope="scope">
                                </style>
                            </div> -->
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14897.812168406506!2d105.8198263!3d21.0145512!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8f6e8770cd87b825!2zU2hvd3Jvb20gTuG7mWkgVGjhuqV0IFRIIEhvbWUgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1524215911561" width="350" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col col4 col41 medium-4 small-12 large-4 col-lg-3 col-md-6 col-sm-12 col-12 topTab"  >
                        <div class="col-inner"  >
                            {!! $information['fanpage-facebook'] !!}
                        </div>
                    </div>
                    <style scope="scope">
                    </style>
                </div>
            </div>
            <div class="type nz-button-2">
                <div><a href="tel:{!! $information['hotline'] !!}" class="nz-bt nz-bt-2"><span class="txt">{!! $information['hotline'] !!}</span><span class="round"><i class="fa fa-phone faa-ring faa-slow animated"></i></span></a></div>
                <style>
                    .type.nz-button-2 {display: block;position: fixed;left: 20px;bottom: 5%;text-align: center;z-index: 69696969;}
                    .type.nz-button-2 > div {margin: 5px auto;}
                    .nz-button-2 .nz-bt-1 {background-color: #C69a66;}
                    .nz-button-2 .nz-bt-1 .round {background-color: #fdcc30;}
                    .nz-button-2 .nz-bt-2 {background-color: #C69a66;}
                    .nz-button-2 .nz-bt-2 .round {background-color: #fdcc30;}
                    .nz-button-2 a { line-height: 16px; text-decoration: none;-moz-border-radius: 40px;-webkit-border-radius: 30px;border-radius: 40px;padding: 12px 53px 12px 23px;color: #fff;text-transform: uppercase;font-family: sans-serif;font-weight: bold;position: relative;-moz-transition: all 0.3s;-o-transition: all 0.3s;-webkit-transition: all 0.3s;transition: all 0.3s;display: inline-block;}
                    .nz-button-2 a span {position: relative;z-index: 3;}
                    .nz-button-2 a .round {-moz-border-radius: 50%;-webkit-border-radius: 50%;border-radius: 50%;width: 38px;height: 38px;position: absolute;right: 3px;top: 3px;-moz-transition: all 0.3s ease-out;-o-transition: all 0.3s ease-out;-webkit-transition: all 0.3s ease-out;transition: all 0.3s ease-out;z-index: 2;}
                    .nz-button-2 a .round i {position: absolute;top: 50%;margin-top: -6px;left: 50%;margin-left: -4px;-moz-transition: all 0.3s;-o-transition: all 0.3s;-webkit-transition: all 0.3s;transition: all 0.3s;}
                    .nz-button-2 .txt {font-size: 14px;line-height: 1.45;}
                    .nz-button-2.type a:hover {padding-left: 48px;padding-right: 28px;}
                    .nz-button-2.type a:hover .round {width: calc(100% - 6px);-moz-border-radius: 30px;-webkit-border-radius: 30px;border-radius: 30px;}
                    .nz-button-2.type a:hover .round i {left: 12%;}
                    #dktv form.wpcf7-form {max-width: 70%;margin: 0 auto;}
                    div#dktv .col.small-12.large-12, div#dktv .col.medium-6.small-12.large-6 {padding: 0;}
                    #dktv .nz-tensp {color: #fff;border: 0;}
                    div#dktv > div {margin: 0;}
                    div#dktv > div {background: url(/wp-content/uploads/2017/08/bg-popup.png);}
                    @media only screen and (max-width: 48em) {
                        .nz-button-2 a {padding: 12px 53px 12px 10px;}
                        .type.nz-button-2 > div {margin: 0px auto;display: table-cell;}
                        .type.nz-button-2 {left: 0px;bottom: 0;text-align: left;margin: 0;}
                    }
                </style>
            </div>
            <!-- .section-content -->
            <style scope="scope">
                #section_216691949 {
                    padding-top: 0px;
                    padding-bottom: 0px;
                    background-color: rgb(35, 35, 35);
                }
            </style>
    </section>
    <div class="absolute-footer dark medium-text-center text-center">
        <div class="container clearfix">
            <div class="footer-primary pull-left">
                <div class="copyright-footer">
                    <span style="color: #ddd;">Copyright 2018 Sofa Theme thhome <a href="http://vn3c.com"><strong>VN3C.JSC</strong></a></span>
                    <div id="yeucau"
                         class="lightbox-by-id lightbox-content mfp-hide lightbox-white "
                         style="">
                        <section class="section dark" id="section_1217710058">
                            <div class="bg section-bg fill bg-fill  " >
                                <div class="section-bg-overlay absolute fill"></div>
                            </div><!-- .section-bg -->
                            <div class="section-content relative">
                                <div class="row align-center sectuvan" style="max-width:870px" id="row-1423119082">
                                    <div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >
                                            <p class="lead" style="text-align: center;"><span style="font-size: 95%;">Nếu quý khách có bất kỳ thắc mắc nào có thể liên hệ với chúng tôi qua thông tin bên dưới hoặc để lại lời nhắn vào hộp thoại bên canh</span></p>
                                            <strong>{{ $information['ten-cong-ty'] }}</strong></br>
                                            <strong>Địa chỉ:</strong> {{ $information['dia-chi'] }}</br>
                                            <strong>Tel:</strong> {{ $information['tel'] }}</br>
                                            <strong>Email:</strong> {{ $information['email'] }}</br>
                                            <strong>Số ĐKKD:</strong> {{ $information['so-dang-ky-kinh-doanh'] }}
                                        </div></div>
                                    <div class="col medium-6 small-12 large-6"  ><div class="col-inner"  >
                                            <h3>Hãy để lại lời nhắn<br />Để chúng tôi có thể tư vấn cho bạn</h3>
                                            <div role="form" class="wpcf7" id="wpcf7-f3055-o2" lang="en-US" dir="ltr">
                                                <div class="screen-reader-response"></div>
                                                <form action="{{ route('sub_contact') }}" method="post">
                                                    {{ csrf_field() }}
                                                    <div class="nz-cf7">
                                                        <div class="nz-cf7-f1 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-name">
                                                                <input type="text" name="name" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="your-name"
                                                                       aria-required="true" aria-invalid="false" placeholder="Tên quý khách" required/>
                                                            </span>
                                                        </div>
                                                        <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-phone">
                                                                <input type="tel" name="phone" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                                                       id="your-phone" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" required/>
                                                            </span>
                                                        </div>
                                                        <div class="nz-cf7-f1 nz-cf7-f2 nz-1-3">
                                                            <span class="wpcf7-form-control-wrap your-email">
                                                                <input type="email" name="email" value="" size="40"
                                                                       class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                       id="your-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" required/></span>
                                                        </div>
                                                        <div class="nz-cf7-f1">
                                                            <span class="wpcf7-form-control-wrap your-message">
                                                                <textarea name="message" cols="40" rows="10"
                                                                          class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"
                                                                          aria-required="true" aria-invalid="false" placeholder="Lời nhắn thêm (có thể để trống)"></textarea></span>
                                                        </div>
                                                        <div class="nz-cf7-f1">
                                                            <input type="submit" value="Gửi tin yêu cầu tư vấn" class="wpcf7-form-control wpcf7-submit c-button" />
                                                        </div>
                                                    </div>
                                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                </form>
                                            </div>
                                        </div></div>
                                    <style scope="scope">
                                    </style>
                                </div>
                            </div><!-- .section-content -->
                            <style scope="scope">
                                #section_1217710058 {
                                    padding-top: 0px;
                                    padding-bottom: 0px;
                                }
                                #section_1217710058 .section-bg-overlay {
                                    background-color: rgba(0, 0, 0, 0.7);
                            </style>
                        </section>
						
                    </div>
                </div>
            </div>
        </div>
    </div>
	{!! isset($information['ma-chat-facebook']) ? $information['ma-chat-facebook'] : '' !!}
	<div id="to_top" style="display: block;">
		<a href="#" class="btn btn-primary"><i class="fa fa-chevron-up"></i></a>
	</div>
	<style>
		#to_top {
			position: fixed;
			top: 70%;
			right: 3px;
			transform: translateY(-50%);
			-moz-transform: translateY(-50%);
			-webkit-transform: translateY(-50%);
			-o-transform: translateY(-50%);
			z-index: 2000;
		}
	</style>