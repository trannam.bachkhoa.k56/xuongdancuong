@extends('zito.layout.site')

@section('title','Gửi đơn hàng thành công')
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])

@section('content')
    <main id="main" class="">
        <div id="content" class="content-area page-wrapper container" role="main" >
            <div class="row row-main">
                <div class="large-12 col">
                    <div class="col-inner">
                        <div class="woocommerce"><div class="row">
                                <div class="large-7 col">
                                    <section class="woocommerce-order-details">
                                        <h2 class="woocommerce-order-details__title">Chi tiết đơn hàng</h2>
                                        <table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
                                            <thead>
                                                <tr>
                                                    <th class="woocommerce-table__product-name product-name">Sản phẩm</th>
                                                    <th class="woocommerce-table__product-table product-total">Tổng cộng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $sumPrice = 0;?>
                                                @foreach($orderItems as $id => $orderItem)
                                                <tr class="woocommerce-table__line-item order_item">
                                                    <td class="woocommerce-table__product-name product-name">
                                                        <a href="{{ route('product', [ 'post_slug' => $orderItem->slug]) }}">
                                                            {{ $orderItem->title }}</a> <strong class="product-quantity">× {{ $orderItem->quantity }}
                                                        </strong>
                                                    </td>
                                                    <td class="woocommerce-table__product-total product-total">
                                                        <span class="woocommerce-Price-amount amount">
                                                           <?php
                                                            if (!empty($orderItem->price_deal)
                                                                && !empty($orderItem->discount_end) && (time() < strtotime($orderItem->discount_end))
                                                                && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                                            ) {
                                                                $sumPrice += ($orderItem->price_deal * $orderItem->quantity);
                                                                echo number_format(($orderItem->price_deal * $orderItem->quantity), 0);
                                                            } elseif (!empty($orderItem->discount)) {
                                                                $sumPrice += ($orderItem->discount * $orderItem->quantity);
                                                                echo number_format(($orderItem->discount * $orderItem->quantity), 0);
                                                            } else {
                                                                $sumPrice += ($orderItem->price * $orderItem->quantity);
                                                                echo number_format(($orderItem->price * $orderItem->quantity), 0);
                                                            } ?>
                                                            <span class="woocommerce-Price-currencySymbol">₫</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th scope="row">Tổng số phụ:</th>
                                                    <td><span class="woocommerce-Price-amount amount">{{ $sumPrice }}<span class="woocommerce-Price-currencySymbol">₫</span></span></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Phí vận chuyển:</th>
                                                    <td>+{{ number_format($costShip, 0, ',', '.') }} VNĐ</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Tổng cộng:</th>
                                                    <td><span class="woocommerce-Price-amount amount">{{ number_format(($totalPrice + $costShip - $codeSalePrice - $pointGive), 0, ',', '.') }} <span class="woocommerce-Price-currencySymbol">₫</span></span></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <section class="woocommerce-customer-details">
                                            <h2>Chi tiết khách hàng</h2>
                                            <table class="woocommerce-table woocommerce-table--customer-details shop_table customer_details">
                                                <tbody>
                                                    <tr>
                                                        <th>Họ và tên:</th>
                                                        <td>{{ $customer['ship_name'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email:</th>
                                                        <td>{{ $customer['ship_email'] }}</td>
                                                    </tr>

                                                    <tr>
                                                        <th>Phone:</th>
                                                        <td>{{ $customer['ship_phone'] }}</td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                            <h3 class="woocommerce-column__title">Địa chỉ thanh toán</h3>
                                            <address>
                                                <p>Địa chỉ: {{ $customer['ship_address'] }}</p>
                                            </address>
                                        </section>

                                    </section>

                                </div>

                                <div class="large-5 col">
                                    <div class="is-well col-inner entry-content">
                                        <p class="success-color woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><strong>Cảm ơn bạn. Đơn hàng của bạn đã được nhận.</strong></p>

                                        <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

                                            <li class="woocommerce-order-overview__order order">
                                                Order number: <strong>{{ $orderId }}</strong>
                                            </li>

                                            <li class="woocommerce-order-overview__date date">
                                                Ngày: <strong>{{ date("Y-m-d H:i:s") }}</strong>
                                            </li>

                                            <li class="woocommerce-order-overview__total total">
                                                Tổng cộng: <strong><span class="woocommerce-Price-amount amount">{{ $sumPrice }} <span class="woocommerce-Price-currencySymbol">₫</span></span></strong>
                                            </li>



                                        </ul>

                                        <div class="clear"></div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div><!-- .col-inner -->
                </div><!-- .large-12 -->
            </div><!-- .row -->
        </div>


    </main>
@endsection
