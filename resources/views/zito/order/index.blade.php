@extends('zito.layout.site')

@section('title','Đặt hàng')
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])

@section('content')
    <main id="main" class="">
        <div id="content" class="content-area page-wrapper container" role="main">
            <form action="{{ route('send') }}" method="post">
                {{ csrf_field() }}
            <div class="row row-main">
                <div class="large-12 col col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="col-inner">
                        <div class="woocommerce">
                            <div class="row pt-0 ">
                                <div class="large-7 col col-lg-7 col-md-6 col-sm-12 col-12 ">
                                    <div id="customer_details">
                                        <div class="clear">
                                            <div class="woocommerce-billing-fields">
                                                <h3>Thông tin khách hàng</h3>
                                                <div class="woocommerce-billing-fields__field-wrapper">
                                                    <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field" data-priority="10">
                                                        <label for="billing_first_name" class="">Họ và Tên <abbr class="required" title="bắt buộc">*</abbr></label>
                                                        <input type="text" class="input-text " name="ship_name" value="{{ !empty(old('ship_name')) ? old('ship_name') : '' }}" autocomplete="given-name" autofocus="autofocus" required/>
                                                        @if ($errors->has('ship_name'))
                                                            <span class="red">
                                                                <strong>{{ $errors->first('ship_name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </p>
                                                    <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field" data-priority="10">
                                                        <label for="billing_first_name" class="">Địa chỉ <abbr class="required" title="bắt buộc">*</abbr></label>
                                                        <input type="text" class="input-text " name="ship_address" value="{{ !empty(old('ship_address')) ? old('ship_address') : '' }}" autocomplete="given-name" autofocus="autofocus" required/>
                                                        @if ($errors->has('ship_address'))
                                                            <span class="red">
                                                                <strong>{{ $errors->first('ship_address') }}</strong>
                                                            </span>
                                                        @endif
                                                    </p>


                                                    <p class="form-row form-row-first validate-required validate-phone" id="billing_phone_field" data-priority="100">
                                                        <label for="billing_phone" class="">Số điện thoại <abbr class="required" title="bắt buộc">*</abbr></label>
                                                        <input type="tel" class="input-text " name="ship_phone"
                                                               placeholder="" value="{{ !empty(old('ship_phone')) ? old('ship_phone') : '' }}" autocomplete="tel" required/>
                                                        @if ($errors->has('ship_phone'))
                                                            <span class="red">
                                                                <strong>{{ $errors->first('ship_phone') }}</strong>
                                                            </span>
                                                        @endif
                                                    </p>
                                                    <p class="form-row form-row-last validate-required validate-email" id="billing_email_field" data-priority="110">
                                                        <label for="billing_email" class="">Địa chỉ email <abbr class="required" title="bắt buộc">*</abbr></label>
                                                        <input type="email" class="input-text " name="ship_email"
                                                               placeholder="" value="{{ !empty(old('ship_email')) ? old('ship_email') : '' }}" autocomplete="email username" required/>

                                                        @if ($errors->has('ship_email'))
                                                            <span class="red">
                                                                <strong>{{ $errors->first('ship_email') }}</strong>
                                                            </span>
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="clear">--}}
                                            {{--<div class="woocommerce-shipping-fields">--}}
                                            {{--</div>--}}
                                            {{--<div class="woocommerce-additional-fields">--}}
                                                {{--<h3>Thông tin thêm</h3>--}}
                                                {{--<div class="woocommerce-additional-fields__field-wrapper">--}}
                                                    {{--<p class="form-row notes" id="order_comments_field" data-priority=""><label for="order_comments" class="">Ghi chú thêm về đơn hàng</label>--}}
                                                        {{--<textarea name="order_comments" class="input-text " placeholder="Ví dụ thời gian giao hàng, địa chỉ giao hàng..." rows="2" cols="5"></textarea>--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}

                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>


                                </div><!-- large-7 -->

                                <div class="large-5 col col-lg-5 col-md-6 sol-sm-12 col-12">
                                    <div class="col-inner has-border">
                                        <div class="checkout-sidebar sm-touch-scroll">
                                            <h3 id="order_review_heading">Hình thức thanh toán</h3>

                                            <div id="order_review" class="woocommerce-checkout-review-order">
                                                @foreach($orderBanks as $id => $orderBank)
                                                    <p style="border-bottom: 1px solid #e5e5e5">{{ $orderBank->name_bank }} - {{ $orderBank->manager_account }} - {{ $orderBank->number_bank }} - {{ $orderBank->branch }}</p>
                                                @endforeach
                                                <div id="payment" class="woocommerce-checkout-payment">
                                                    <ul class="wc_payment_methods payment_methods methods">
                                                        <li class="wc_payment_method payment_method_bacs">
                                                            <input id="payment_method_bacs" type="radio" class="input-radio" name="method_payment" value="Thanh toán qua tài khoản ngân hàng" checked="checked" data-order_button_text="">

                                                            <label for="payment_method_bacs">
                                                                Chuyển khoản ngân hàng  </label>
                                                            <div class="payment_box payment_method_bacs">
                                                                <p>Thực hiện thanh toán vào ngay tài khoản ngân hàng của chúng tôi. Vui lòng sử dụng ID Đơn hàng của bạn như một phần để tham khảo khi thanh toán. Đơn hàng của bạn sẽ không được vận chuyển cho tới khi tiền được gửi vào tài khoản của chúng tôi.</p>
                                                            </div>
                                                        </li>
                                                        <li class="wc_payment_method payment_method_cod">
                                                            <input id="payment_method_cod" type="radio" class="input-radio" name="method_payment" value="Thanh toán khi nhận hàng" data-order_button_text="">

                                                            <label for="payment_method_cod">
                                                                Thanh toán khi nhận hàng COD  </label>
                                                            <div class="payment_box payment_method_cod" style="display:none;">
                                                                <p>Quý khách sẽ phải trả $ sau khi nhận hàng</p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                            <div class="html-checkout-sidebar pt-half"></div>       </div>
                                    </div>
                                </div><!-- large-5 -->

                            </div><!-- row -->

                        </div>


                    </div><!-- .col-inner -->
                </div><!-- .large-12 -->
            </div><!-- .row -->

            <div class="row row-main">
                <div class="large-12 col">
                    <div class="col-inner">
                        <div class="woocommerce">
                            <div class="woocommerce row row-large row-divided">
                                <div class="col large-7 pb-0 ">
                                    <div class="cart-wrapper sm-touch-scroll">
                                        <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents"
                                               cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th class="product-name" colspan="3">Sản phẩm</th>
                                                <th class="product-price">Giá</th>
                                                <th class="product-quantity">Số lượng</th>
                                                <th class="product-subtotal">Tổng cộng</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $sumPrice = 0;?>
                                            @foreach($orderItems as $id => $orderItem)
                                            <tr class="woocommerce-cart-form__cart-item cart_item">
												
                                                <td class="product-remove">
                                                    <!--<a href=""
                                                       class="remove" aria-label="Xóa sản phẩm này"
                                                       data-product_id="4742" data-product_sku="">×</a>--></td>
													   
                                                <td class="product-thumbnail">
                                                    <a href="{{ route('product', [ 'post_slug' => $orderItem->slug]) }}"><img
                                                                width="130" height="80"
                                                                src="{{ asset($orderItem->image) }}"
                                                                class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image lazy-load-active"
                                                                alt="{{ $orderItem->title }}"
                                                                title="{{ $orderItem->title }}"
                                                                sizes="(max-width: 130px) 100vw, 130px">
                                                    </a>
                                                </td>
                                                <td class="product-name" data-title="Sản phẩm">
                                                    <a href="{{ route('product', [ 'post_slug' => $orderItem->slug]) }}">
                                                        {{ $orderItem->title }}
                                                    </a>
                                                </td>

                                                <td class="product-price" data-title="Giá">
                                                    @if (!empty($orderItem->price_deal)
                                                     && !empty($orderItem->discount_end) && ( time() < strtotime($orderItem->discount_end))
                                                     && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                                    )
                                                        <span class="woocommerce-Price-amount amount">{{ number_format($orderItem->price_deal , 0) }}<span
                                                                    class="woocommerce-Price-currencySymbol">₫</span></span>
                                                        <input type="hidden" class="unitPrice"
                                                               value="{{ $orderItem->price_deal }}">
                                                    @elseif (!empty($orderItem->discount))
                                                        <span class="woocommerce-Price-amount amount">{{ number_format($orderItem->discount , 0) }}<span
                                                                    class="woocommerce-Price-currencySymbol">₫</span></span>
                                                        <input type="hidden" class="unitPrice"
                                                               value="{{ $orderItem->discount }}">
                                                    @else
                                                        <input type="hidden" class="unitPrice"
                                                               value="{{ $orderItem->price }}">
                                                        <span class="woocommerce-Price-amount amount">{{ number_format($orderItem->price , 0) }}<span
                                                                    class="woocommerce-Price-currencySymbol">₫</span></span>
                                                    @endif


                                                </td>

                                                <td class="product-quantity" data-title="Số lượng">
                                                    <div class="quantity buttons_added">
                                                        <input type="hidden" name="product_id[]"
                                                               value="{{ $orderItem->product_id }}"/>
                                                      
                                                        <input type="number" class="input-text qty text" step="1"
                                                               min="0" max="9999"
                                                               name="quantity[]"
                                                               value="{{ $orderItem->quantity }}" title="SL" size="4" pattern="[0-9]*"
                                                               inputmode="numeric"  onchange="return changeQuantity(this);" />
                                                        
                                                    </div>
                                                </td>

                                                <td class="product-subtotal" data-title="Tổng cộng">
                                                    <span class="woocommerce-Price-amount amount totalPrice">
                                                        <?php
                                                        if (!empty($orderItem->price_deal)
                                                            && !empty($orderItem->discount_end) && (time() < strtotime($orderItem->discount_end))
                                                            && !empty($orderItem->discount_start) && (time() > strtotime($orderItem->discount_start))
                                                        ) {
                                                            $sumPrice += ($orderItem->price_deal * $orderItem->quantity);
                                                            echo number_format(($orderItem->price_deal * $orderItem->quantity), 0);
                                                        } elseif (!empty($orderItem->discount)) {
                                                            $sumPrice += ($orderItem->discount * $orderItem->quantity);
                                                            echo number_format(($orderItem->discount * $orderItem->quantity), 0);
                                                        } else {
                                                            $sumPrice += ($orderItem->price * $orderItem->quantity);
                                                            echo number_format(($orderItem->price * $orderItem->quantity), 0);
                                                        } ?>
                                                    </span>
                                                    <span class="woocommerce-Price-currencySymbol">₫</span>
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="6" class="actions clear">

                                                    <div class="continue-shopping pull-left text-left">
                                                        <a class="button-continue-shopping button primary is-outline"
                                                           href="/">
                                                            ← Tiếp tục mua hàng </a>
                                                    </div>

                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="cart-collaterals large-5 col pb-0">
                                    <div class="cart-sidebar col-inner ">
                                        <div class="cart_totals ">

                                            <table cellspacing="0">
                                                <thead>
                                                <tr>
                                                    <th class="product-name" colspan="2" style="border-width:3px;">Thông
                                                        tin thanh toán
                                                    </th>
                                                </tr>
                                                </thead>
                                            </table>

                                            <h2>Thông tin thanh toán</h2>

                                            <table cellspacing="0" class="shop_table shop_table_responsive">

                                                <tbody>


                                                <tr class="order-total">
                                                    <th>Tổng cộng</th>
                                                    <td data-title="Tổng cộng"><strong><span
                                                        class="woocommerce-Price-amount amount sumPrice">5.220.000&nbsp;<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></span></strong>
                                                    </td>
                                                </tr>


                                                </tbody>
                                            </table>

                                            <div class="wc-proceed-to-checkout">
                                                <button type="submit" class="checkout-button button alt wc-forward">
                                                    Thanh toán ngay
                                                </button>
                                            </div>


                                        </div>
                                        <div class="cart-sidebar-content relative"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="cart-footer-content after-cart-content relative"></div>
                        </div>


                    </div><!-- .col-inner -->
                </div><!-- .large-12 -->
            </div><!-- .row -->
            </form>

        </div>


    </main>

    <script>
        function changeQuantity(e) {
            var unitPrice = $(e).parent().parent().parent().find('.unitPrice').val();
            console.log(unitPrice);
            var quantity = $(e).val();
            var totalPrice = unitPrice * quantity;
            var sum = 0;
            $(e).parent().parent().parent().find('.totalPrice').empty();
            $(e).parent().parent().parent().find('.totalPrice').html(numeral(totalPrice).format('0,0'));

            $('.totalPrice').each(function () {
                var totalPrice = $(this).html();
                console.log(totalPrice);
                sum += parseInt(numeral(totalPrice).value());
            });

            $('.sumPrice').empty();
            $('.sumPrice').html(numeral(sum).format('0,0'));
        }
    </script>
@endsection
