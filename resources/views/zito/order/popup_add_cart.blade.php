<div id="modelAddToCart"
     class="lightbox-by-id lightbox-content mfp-hide lightbox-white "
     style="max-width:80% ;padding:20px">
    <section class="section dark" id="section_1217710058">
        <div class="bg section-bg fill bg-fill  " >
            <div class="section-bg-overlay absolute fill"></div>
        </div><!-- .section-bg -->
        <div class="section-content relative">
            <div class="row align-center sectuvan" style="max-width:870px" id="row-1423119082">
                <div class="col medium-12 small-12 large-12"  >
                    <p class="lead" style="text-align: center;"><span style="font-size: 95%;">Đặt hàng thành công</span></p>
                    <div class="modal-body">
                    </div>
                    <a href="<?= (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>"
                       class="button-continue-shopping button primary" data-dismiss="modal">Tiếp tục mua hàng</a>
                    <a href="{{ route('order') }}" class="checkout-button button alt wc-forward" style="float:right">Giỏ hàng</a>
                </div>
                <style scope="scope">
                </style>
            </div>
        </div><!-- .section-content -->
        <style scope="scope">
            #section_1217710058 {
                padding-top: 0px;
                padding-bottom: 0px;
            }
            #section_1217710058 .section-bg-overlay {
                background-color: rgba(0, 0, 0, 0.7);
            }
            #section_1217710058 .section-bg.bg-loaded {
                background-image: 3272;
            }
        </style>
    </section>
</div>

