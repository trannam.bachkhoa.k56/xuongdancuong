 <div class="owl-carousel slideshow">

    @foreach(\App\Entity\SubPost::showSubPost('slider',10) as $id => $img)
	@if (!empty($img))
   <figure class="slideshow_block_top_img">
      <img src="{{ isset($img['image']) ? asset($img['image']) : '' }}">
      <figcaption class="slideshow_block_top_content">
         <h2></h2>
         <p></p>
      </figcaption>
   </figure>
   @endif
    @endforeach
 
</div>

<script>
   $('.slideshow').owlCarousel({
	  autoplaySpeed:true,
	  autoplay:true,
	  responsive:{
		  0:{
			  items:1,
			  nav:false
		  },
		  600:{
			  items:1,
			  nav:false
		  },
		  1000:{
			  items:1,
			  nav:false,
			  loop:false
		  }
	  }
   });
   $('.list .owl-prev').html('<i class="fa fa-angle-left"></i>');
   $('.list .owl-next').html('<i class="fa fa-angle-right"></i>');
                                                      
</script>