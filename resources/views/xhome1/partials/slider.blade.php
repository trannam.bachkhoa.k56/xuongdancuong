
<div style="margin-bottom: -175px;" class="home-slider owl-carousel" data-lg-items='1' data-md-items='1' data-sm-items='1' data-xs-items="1" data-margin='0'  data-nav="true">

    @foreach(\App\Entity\SubPost::showSubPost('slider',3) as $id => $img)
    <div class="item">
        <a href="#" class="clearfix">
            <img src="{{ isset($img['slider-img']) ? asset($img['slider-img']) : '' }}" alt="slider">
        </a>
    </div>
    @endforeach
</div>