

@extends('travels-01.layout.site')

@section('title',isset($category->title) ? $category->title : '')
@section('meta_description', isset($information['meta_description']) ?$information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <link rel="stylesheet" href="{{ asset('travels-01/css/herderNoslider.css') }}" media="all" type="text/css" />
    <section class="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                        <li class="home">
                            <a itemprop="url" href="/"><span itemprop="title">Trang chủ</span></a>
                            <span><i class="fa fa-angle-right"></i></span>
                        </li>

                        <li><strong itemprop="title">{{$post->title}}</strong></li>

                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="page-title category-title">

                        <h1 class="title-head"><a href="#">{{$post->title}}</a></h1>
                    </div>
                    <div class="content-page rte">

                        <p>{{$post->description}}</p>

                        {!! isset($post->content) ? $post->content : "" !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .google-map {width:100%;}
        .google-map .map {width:100%; height:650px; background:#dedede}
    </style>
@endsection
