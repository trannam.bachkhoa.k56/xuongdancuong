@extends('xhome.layout.site')

@section('title', isset($post->title) ? $post->title : '')
@section('meta_description', !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', '') @section('dynx_itemid', '')
@section('dynx_pagetype', 'other')
@section('dynx_totalvalue', '0')

@section('content')

<section class="v2_bnc_inside_page">
    <div class="v2_bnc_content_top"></div>
    <script type="text/javascript">
        var urlNow = document.URL;
    </script>
    <section id="products-main" class="v2_bnc_products_page">
        <!-- Breadcrumbs -->
        <!-- Breadcrumbs -->
        <div class="clearfix"></div>
        <div class="v2_breadcrumb_main padding-top-10">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="/">Trang chủ</a></li>
                    <li><a href="{{ route('site_category_post', ['cate_slug' => isset($category->slug) ? $category->slug : 'tin-tuc' ]) }}" rel="category tag">{{ isset($category->title) ? $category->title : 'Tin tức' }}</a></li>
                    <li> <a href="#" title="">{{ $post->title }}</a></li>
                </ul>
            </div>
        </div>
        <!-- End Breadcrumbs -->
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-12 bgrWhite mgb30 pdb20">
                    <div class="v2_bnc_title_main">
                        <h1 class="md-f16">{{ $post->title }}</h1>
                    </div>
                    <div class="des">{{ $post->description }}</div>
                    <div>
                        <style>
                            .content h2 {
                                font-size: 20px;
                            }
                            
                            .content {
                                padding-bottom: 30px;
                                background: #fff;
                                padding: 10px;
                                margin: 20px 0;
                            }
                            
                            .des {
                                font-weight: bold;
                                padding-bottom: 10px;
                            }
                        </style>
                        <?=  isset($post->content ) ?$post->content  : 'đang cập nhật tin tức'?>

                            <div>
                                {!! isset($post['ofin-form']) ? $post['ofin-form'] : '' !!}
                            </div>
                            <p class="mgb0 f24 fw6">Bình luận</p>
                            <div id="fb-root"></div>
                            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3&appId=1875296572729968&autoLogAppEvents=1"></script>

                            <div class="fb-comments" data-href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" data-width="100%" data-numposts="5"></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                  <div class="titleRight bgrGrayD white">
                    <p class="fw7 textCenter mgb0 f18 pdt5 pdb5 md-f14">
                      SẢN PHẨM NỔI BẬT
                    </p>
                  </div>

                  <div class="productHot pd10 bgrWhite">
						@foreach (\App\Entity\Product::newProduct(4) as $id => $product)
							<div class="v2_bnc_pr_item_box" style="margin-bottom: 30px;">
								@include('xhome.partials.iterm_product')
							</div>
						@endforeach
                  </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection