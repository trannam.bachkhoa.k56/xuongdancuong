@extends('xhome.layout.site')
@section('title', isset($category->title) ? $category->title : '')
@section('meta_description',  isset($category->description) ? $category->description : '' )
@section('keywords', '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'searchresults')
@section('dynx_totalvalue', '0')

@section('content')
    <style>
         header{
         position: static !important;
         box-shadow: 0 0 6px rgba(0,0,0,0.3);
         }
    </style>
   <section class="v2_bnc_inside_page">
         <div class="v2_bnc_content_top"></div>
         <section id="products-main" class="v2_bnc_products_page">
            <!-- Breadcrumbs -->
            <!-- Breadcrumbs -->
            <div class="clearfix"></div>
            <div class="v2_breadcrumb_main padding-top-10">
               <div class="container">
                  <ul class="breadcrumb">
                     <li ><a href="/">Trang chủ</a></li>
                     <li ><a href="#">{{ isset($category->title) ?$category->title : '' }}</a></li>
                  </ul>
               </div>
            </div>
            <!-- End Breadcrumbs --> 
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 bgrWhite mgb30">
                     <div class="v2_bnc_title_main">
                        <h1>{{ isset($category->title) ? $category->title : '' }}</h1>
						<p>{!!  isset($category->description) ? $category->description : '' !!} </p>
                     </div>
                     <div class="v2_bnc_products_page_body col-xs-12 no-padding">
                        @if(empty($products))
                                <p>Không tồn tại danh mục này</p>
                        @else
                            @foreach ($products as $id => $product)
							<div class="v2_bnc_pr_item_box col-ld-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 30px;">
								@include('xhome.partials.iterm_product')
							</div>
                            @endforeach
                        @endif

                       
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </section>
	<script>
		//Đồng bộ chiều cao các div
		$(function() {
			$('.v2_bnc_pr_item_box').matchHeight();
		});
	</script>
@endsection
