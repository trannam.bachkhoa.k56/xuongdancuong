@extends('xhome.layout.site')

@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')



@section('content')
<style>
   header{
   position: static !important;
   box-shadow: 0 0 6px rgba(0,0,0,0.3);
   }
  .v2_bnc_title_main h2, .v2_bnc_title_main h1 {
       margin: 0 0 20px;
   }
   .v2_bnc_title_main h2.title{
		margin: 0 0 20px;
		font-size: 18px;
		border-bottom: 1px solid;
		text-align: left;
	}
	v2_bnc_title_main h2.title:after, v2_bnc_title_main h2.title:before
	{
		display:none;
	}
   .contact .content
   {
      padding: 0;
   }
   .contact .content form .des
   {
      border-top: 1px solid #ddd;
      border-left: 1px solid #ddd;
      border-right: 1px solid #ddd;
   }
   form .form-group input
   {
	       border-radius: 6px !important;
   }
   form .form-group textarea
   {
	       border-radius: 6px !important;
   }
   form .form-group span
   {
	   color:red;
   }
   @media all and (max-width: 769px)
   {
      .contact .content
      {
         padding: 0 15px;
      }
   }
</style>
<section class="detailnew contact">
   <div class="container">
      <div class="row bgdetail">
		<div class="col-12">
			<div class="v2_breadcrumb_main padding-top-10">
               <div class="container">
                  <ul class="breadcrumb">
                     <li ><a href="/">Trang chủ</a></li>
                     <li ><a href="#">Liên Hệ</a></li>
                  </ul>
               </div>
            </div>
		</div>
		<div class="col-12 colcontact">
            <div class="v2_bnc_title_main">
               <h1 class="color">  Thông tin liên hệ</h1>
            </div>
			<div class="googlemap">
				<?= isset($information['so-dien-thoai']) ? $information['map']  : 'đang cập nhật' ?>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 col-12 colcontact">
			<div class="v2_bnc_title_main">
               <h2 class="title">Thông tin cửa hàng</h2>
            </div>
			<div class="shopinfo">
				<?= isset($information['so-dien-thoai']) ? $information['thong-tin-cua-hang']  : 'đang cập nhật' ?>
			</div>
        </div>
		 
        <div class="col-md-6 col-sm-12 col-12 colcontact">
			<div class="v2_bnc_title_main">
               <h2 class="title">Gửi liên hệ cho chúng tôi</h2>
            </div>
            <div class="content">
				<p>Chú ý: các thông tin đánh dấu (*) là bắt buộc</p>
               <form action="{{ route('sub_contact') }}" method="post">
                  {!! csrf_field() !!}
                  <div class="form-group">
                     <input type="text" class="form-control" placeholder="Tên khách hàng(*)" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Tên khách hàng" name="name" >
                  </div>
                  <div class="form-group">
                     <input type="number" class="form-control" id="exampleInputPassword1" placeholder="Điện thoại(*)" name="phone">
                  </div>
                  <div class="form-group">
                     <input type="email" class="form-control" placeholder="Email(*)" id="exampleInputPassword1" placeholder="" name="email">
                  </div>
                  <div class="form-group">
                     <input type="text" class="form-control" placeholder="Địa chỉ(*)" id="exampleInputPassword1" placeholder="" name="address">
                  </div>
                  <div class="form-group">
                     <textarea class="form-control des" placeholder="Mô tả(*)" id="exampleFormControlTextarea1" rows="3" name="message" ></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Gửi liên hệ</button>
               </form>
            </div>
			<p style="color: red; font-size: 18px; padding:10px">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
         </div>
      </div>
   </div>
</section>
@endsection