@extends('xhome.layout.site')

@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'home')
@section('dynx_totalvalue', '0')

@section('content')
<script src="/xhome/js/product-scroll.js"></script>
      <section class="v2_bnc_content_main">
         <div class="v2_bnc_body_main">
            <div class="v2_bnc_home">
               <div id="fast-container"></div>
               <div id="fast-dialog" class="zoom-anim-dialog mfp-hide">
                  <div id="fast-product-content">
                  </div>
               </div>
               <!-- Slideshow Main -->
               <div class="slideshow_block_top col-xs-12 no-padding margin-bottom-30">
                    @include('xhome.partials.sliderhome')
               </div>
			   <section class="v2_bnc_pr_tab_main  col-xs-12 no-padding">
                      <div class="container">
                         <div class="v2_bnc_title_tab_main">
                            <hgroup class="v2_bnc_title_main">
                               <h2 class="gc">TOP NHỮNG SẢN PHẨM MỚI NHẤT</h2>
                            </hgroup>
                            <ul id="v2_bnc_pr_tab_home" class="v2_bnc_title_tab_home hidden">
                               <li class="active"><a href="#f-pr-tab00" data-toggle="tab">TOP NHỮNG SẢN PHẨM MỚI NHẤT</a></li>
                            </ul>
                         </div>
                         <div class="v2_bnc_body_main">
                            <div class="tab-content row">
                               <div id="f-pr-tab00" class="tab-pane active v2_bnc_tabhome_product">
                                  <div class="row">
                                        <div class="owl-carousel categoryIndex1 list">
                                                @foreach (\App\Entity\Product::newProduct(20) as $id => $product)
                                                    @if (!empty($product))
                                                    <div class="col-xs-12 col-md-3 mgb20">
                                                        <div class="item">
                                                            @include('xhome.partials.itemprhome')
                                                        </div>
                                                    </div>

                                                    @endif
                                                 @endforeach
                                        </div>
                                  </div>
                               </div>
                            </div>
                         </div>

                      </div>

                   </section>
				   
               <div class="v2_bnc_intro_company col-xs-12 no-padding margin-top-20 margin-bottom-30">
                  <div class="container">
                     <div class="row">
						<div class="col-xs-12">
							<div class="v2_bnc_title_tab_main">
								<hgroup class="v2_bnc_title_main">
								   <h1 class="textUpper mgt0 color">{{ isset($information['tieu-de-trang']) ? $information['tieu-de-trang']  : '' }}</h1>	
								</hgroup>
								
							</div>
							
						</div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                           <div>
								<div>
								  <p style="font-size:14px" ><?= isset($information['sologan']) ? $information['sologan']  : '' ?></p>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12" id="scroller">
              						<form onSubmit="return contact(this);" class="wpcf7-form" method="post" action="{{route('sub_contact')}} ">
              								{!! csrf_field() !!}
              								<input type="hidden" name="is_json"
              									class="form-control captcha" value="1" placeholder="">
              								<div class="form-group">
              								  <div>
              									<input type="text" class="form-control"  placeholder="Tên của bạn"name="name" required>
              								  </div>
              								</div>
              								<div class="form-group">
              								  <div>
              									<input type="email" class="form-control" placeholder="Email của bạn" name="email" required>
              								  </div>
              								</div>
              								<div class="form-group">
              								  <div > 
              									<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required>
              								  </div>
              								</div>
              								<div class="form-group">
              								  <div > 
              									<textarea class="form-control" rows="2" name="message"placeholder="Nội dung ghi chú"></textarea>
              								  </div>
              								</div>
              									 <input type="hidden" value="" name="" id="utm_source"/>
              									 <input type="hidden" value="" name="" id="utm_medium"/>
              									 <input type="hidden" value="" name="" id="utm_campaign"/>
              								<div class="form-group">
              								  <div>
              									<button type="submit" class="btn  btn-warning">ĐĂNG KÝ TƯ VẤN</button>
              								  </div>
              								</div>
              							 </form>
                        </div>
						<script>
							 $(document).ready(function(){
								 $(window).scroll(function () {
									var w = window.innerWidth;
									if (w > 767) {
										$("#scroller").stick_in_parent({offset_top: 75});
									}
								});
							 
							 });
						  </script>
                     </div>
                  </div>
               </div>
               <!-- End Intro Company --><!-- Products Tab -->
                <div class="clearfix"></div>
                  

               <!-- End Products Tab -->      
            </div>
         </div>
      
         <!-- Popup Adv Home Loadpage-->
         <!-- End Popup Adv Home Loadpage -->
      </section>
      <!-- End Content -->
      
      <script type="text/javascript">
         $(document).ready(function(){
           $('.close').click(function(){
               $('.modal').css('display','none');
           });
         });
      </script>
@endsection