@extends('xhome.layout.site')
@section('title', isset($product->title) ? $product->title : '' )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('dynx_itemid',  $product->product_id )
@section('dynx_pagetype', 'offerdetail')
@php
$totalValue = 0;
if ( !empty($product->price_deal)
&& !empty($product->discount_end)
&& ( time() < strtotime($product->discount_end))
&& !empty($product->discount_start) && (time() > strtotime($product ->discount_start))) {
$totalValue = $product->price_deal;
}
elseif (!empty($product->discount)) {
$totalValue =  $product->discount; }
else
$totalValue = $product->price;
@endphp
@section('dynx_totalvalue', $totalValue)
@section('content')
<style>
   header{
   position: static !important;
   box-shadow: 0 0 6px rgba(0,0,0,0.3);
   }
</style>
<div></div>
<script src="/xhome/js/product-scroll.js"></script>
<!-- End Header -->
<section class="v2_bnc_inside_page mgb30">
   <div class="v2_bnc_products_view_details">
      <div class="v2_bnc_products_body">
         <!-- Products details -->
         <!-- Breadcrumbs -->
         <div class="v2_breadcrumb_main">
            <div class="container">
               <ul class="breadcrumb padding-top-30 padding-bottom-30" style="display:block;">
                  <li ><a href="/">Trang chủ</a></li>
                  <?php $showCategory = false; ?>  
                  @foreach ($categories as $id => $category) 
                  @if ($category->title != 'Sản phẩm khuyến mãi' && !$showCategory)
                  <?php $showCategory = true; ?>  
                  <li class="">
                     <a class="" href="{{ route('site_category_product', [ 'cate_slug' => $category->slug]) }}">{{ $category->title }}</a>
                  </li>
                  @endif    
                  @endforeach
                  <li ><a href="#">{{ $product->title }}</a></li>
               </ul>
            </div>
         </div>
         <!-- End Breadcrumbs -->
         <div class="v2_bnc_product_details_page">
            <div class="container">
               <div class="row">
                  <!-- Zoom image -->
                  <div class="v2_bnc_products_details_zoom_img col-md-7 col-sm-12 col-xs-12 hideMB">
                     @if(!empty($product->image_list))
                     <?php $i = 1;?>
                     @foreach(explode(',', $product->image_list) as $imageProduct)
                     @if($i == 1)
                     <div class="f-pr-image-zoom">
                        <div class="zoomWrapper">
                           <img id="img_01" src="{{ $imageProduct }}"data-zoom-href='{{ $imageProduct }}'class="img-responsive BNC-image-add-cart-786802" alt="{{ $product->title }}" class="img-responsive" />
                        </div>
                     </div>
                     @else
                     @endif
                     <?php $i++; ?>
                     @endforeach
                     @else
                     <div class="f-pr-image-zoom">
                        <div class="zoomWrapper">
                           <img id="img_01" src="{{ $product->image }}"data-zoom-href='{{ $product->image }}'class="img-responsive BNC-image-add-cart-786802" alt="{{ $product->title }}" class="img-responsive" />
                        </div>
                     </div>
                     @endif
                     <div class="f-pr-image-zoom-gallery">
                        <div id="slidezoompage" class="row">
                           <div class="owl-carousel owl_img_product_details">
                              @if(!empty($product->image_list))
                              @foreach(explode(',', $product->image_list) as $imageProduct)
                              <div class="col-xs-12">
                                 <a href="#" data-href='{{$imageProduct}}'data-zoom-image="{{$imageProduct}}" title="{{$product->title}}">
                                 <img src="{{$imageProduct}}" alt="{{$product->title}}" class="v2_bnc_product_details_img_small img-responsive"/>
                                 </a>
                              </div>
                              @endforeach
                              @else    
                              <p>Đang cập nhật</p>
                              @endif                         
                           </div>
                           <script>
                              $('.owl-carousel').owlCarousel({
                                 loop:true,
                                 autoplaySpeed:true,
                                 autoplay:true,
                                 navigation:false,
                                 responsive:{
                                     0:{
                                         items:1,
                                         nav:false
                                     },
                                     600:{
                                         items:2,
                                         nav:false
                                     },
                                     1000:{
                                         items:4,
                                         nav:false,
                                         loop:false
                                     }
                                 }
                              });
                              $('.list .owl-prev').html('<i class="fa fa-angle-left"></i>');
                              $('.list .owl-next').html('<i class="fa fa-angle-right"></i>');
                              
                              
                           </script>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="col-md-7 col-sm-12 col-xs-12 hidePC">
                     <img src="{{ $product->image }}"data-zoom-href='{{ $product->image }}' class="img-responsive BNC-image-add-cart-786802" alt="{{ $product->image }}" class="img-responsive" />
                  </div>
                  <!-- End Zoom image --> 
                  <!-- Details -->
                  <div class="v2_bnc_products_details_box col-md-5 col-sm-12 col-xs-12">
                     <div class="v2_bnc_products_details_box_name">
                        <h1>{{$product->title}}</h1>
                     </div>
                     <p>Mã sản phẩm : {{$product->code}} </p>
                     <p>
                        <a href="#rateProduct" class="rateAndSaled"> 
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        {{ \App\Entity\Comment::getCountComment($product->post_id) }} <span>|</span> 307 đã bán
                        </a>
                     </p>
                     <div class="v2_bnc_products_details_box_info">
                        <div class="v2_bnc_products_details_box_price">
                           <span class="key">Giá sản phẩm: </span><span class="price" style="font-size: 17px;">
                           @if(empty($product->price))
                           <span>liên hệ</span>
                           @elseif($product->discount > 0)
                           <span><del>{{ number_format($product->price) }} VNĐ</del> </span> -
                           <span>{{ number_format($product->discount) }} VNĐ</span>
                           @else
                           <span>{{ number_format($product->price) }} VNĐ</span>
                           @endif
                           </span>
                        </div>
                        <div class="v2_bnc_products_details_box_description">
                           <?= isset($product->properties) ? $product->properties : '' ?>
                        </div>
                     </div>
                     <form onsubmit="return addToOrder(this);" method="post" id="add-to-cart-form" class="formOrder"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                           <input type="text" class="form-control" name="ship_name" placeholder="Họ và tên (*)" autocomplete="cc-name" required>
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="ship_email" placeholder="Email (*)" autocomplete="email" required>
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="ship_phone" autocomplete="tel" placeholder="Số điện thoại (*)" required>
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="ship_address" placeholder="Địa chỉ (*)" required>
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" name="message" placeholder="Ghi chú"></textarea>
                        </div>
                        <div>
                           <input type="hidden" class="quantity" name="quantity[]" value="1" />
                           <input type="hidden" class="product_id" name="product_id[]" value="{{ $product->product_id }}">
                           <button class="btn btn-danger" style="width: 100%;"style="width: 100%;" title="Chọn sản phẩm"  type="submit">
                           ĐẶT HÀNG NGAY
                           </button>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                  </div>
                  <!-- End Details -->
               </div>
            </div>
         </div>
         <script type="text/javascript">
            $(document).ready(function() {
            $("#img_01").elevateZoom(
            {
            gallery:'slidezoompage',
            cursor: 'pointer',
            galleryActiveClass: 'active',
            imageCrossfade: true,
            scrollZoom : true,
            easing : true
            });
            //
            $("#img_01").bind("click", function(e) {
            var ez = $('#img_01').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            var src=$(this).find('img').attr('src');
            $('#img_01').attr('src',src);
            return false;
            });
            $("#slidezoompage a").bind("click", function(e) {
            var src=$(this).find('img').attr('src');
            $('#img_01').attr('src',src);
            return false;
            });
            
            
            
            });
         </script>
         <!-- End Products details -->
         <!-- Form Register Design -->
         <!-- Modal -->
         <!-- End Form Register Design --><!-- Products Details Tab -->
         <div class="container">
            <div class="row">
               <div class="col-xs-12 col-md-8">
                  <div class="f-product-view-tab-body tab-content">
                     <div id="f-pr-view-01" class="tab-content tab-pane active">
                        <style>
                           h2{
                           font-size: 22px;
                           }
                        </style>
                        <?= isset($product->content) ? $product->content :'đang cập nhật' ?>
                        <div class="">
                           @include('general.sub_comments', ['post_id' => $product->post_id])
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xs-12 col-md-4" id="scroller" style="border: 1px solid #e1e1e1; padding-bottom: 10px;">
                  <div class="v2_bnc_products_details_box_name" style="margin-top: 15px;">
                     <h1>{{$product->title}}</h1>
                  </div>
                  Mã sản phẩm : {{$product->code}}
                  <div class="v2_bnc_products_details_box_info">
                     <div class="v2_bnc_products_details_box_price">
                        <span class="key"></span><span class="price" style="font-size: 17px;">
                        @if($product->discount > 0)
                        <span><del>{{ number_format($product->price) }}  VNĐ</del></span> -
                        <span>{{ number_format($product->discount) }} VNĐ</span>
                        @else
                        <span>{{ number_format($product->price) }} VNĐ</span>
                        @endif
                        </span>
                     </div>
                  </div>
                  <form onsubmit="return addToOrder(this);" method="post" id="add-to-cart-form" class="" enctype="multipart/form-data">
                     {{ csrf_field() }}
                     <div class="form-group">
                        <input type="text" class="form-control" name="ship_name" placeholder="Họ và tên (*)"  required>
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" name="ship_email" placeholder="Email (*)" autocomplete="email" required>
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" name="ship_phone" placeholder="Số điện thoại (*)" autocomplete="tel" required>
                     </div>
                     <div class="form-group">
                        <input type="text" class="form-control" name="ship_address" placeholder="Địa chỉ (*)" required>
                     </div>
                     <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Ghi chú"></textarea>
                     </div>
                     <div>
                        <input type="hidden" class="quantity" name="quantity[]" value="1" />
                        <input type="hidden" class="product_id" name="product_id[]" value="{{ $product->product_id }}">
                        <button class="btn btn-danger" style="width: 100%;" title="Chọn sản phẩm"  type="submit">
                        ĐẶT HÀNG NGAY
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
   $(document).ready(function(){
   $(window).scroll(function () {
   var w = window.innerWidth;
   if (w > 767) {
   $("#scroller").stick_in_parent({offset_top: 75});
   }
   });
   
   });
   
   
</script>
<!-- End Inside Page -->
<!--Footer-->
<!-- END PRODUCTS -->
@endsection