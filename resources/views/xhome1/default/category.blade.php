@extends('xhome.layout.site')
@section('title', 'Tin tức')
@section('meta_description',  isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'searchresults')
@section('dynx_totalvalue', '0')

@section('content')
    <style>
         header{
         position: static !important;
         box-shadow: 0 0 6px rgba(0,0,0,0.3);
         }
    </style>
   <section class="v2_bnc_inside_page">
         <div class="v2_bnc_content_top"></div>
         <script type="text/javascript">
            var urlNow=document.URL;
         </script>
         <section id="products-main" class="v2_bnc_products_page">
            <!-- Breadcrumbs -->
            <!-- Breadcrumbs -->
            <div class="clearfix"></div>
            <div class="v2_breadcrumb_main padding-top-10">
               <div class="container">
                  <ul class="breadcrumb">
                     <li ><a href="/">Trang chủ</a></li>
                     <li ><a href="#">{{ isset($category->title) ?$category->title : 'Tin tức' }}</a></li>
                  </ul>
               </div>
            </div>
            <!-- End Breadcrumbs --> 
            <div class="container">
               <div class="row">
                  <div class="col-xs-12 mgb30 pdb10 bgrWhite">
                     <div class="v2_bnc_title_main">
                        <h1 class="color">{{ isset($category->title) ? $category->title : 'Tin tức' }}</h1>
                     </div>
                     <div class="">
                        @if(empty($posts))
                                <p>Không tồn tại danh mục này</p>
                        @else
                            @foreach ($posts as $id => $post)
                               <div class="col-md-3 col-sm-6 col-xs-12 mgb20">
                                <div class="listcate">
                                   <div class="img">
                                     <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="{{ $post['title'] }}"><img src="{{ !empty($post['image']) ? asset($post['image']) : '' }}" alt="{{ $post['title'] }}"></a>
                                   </div>
                                   <a class="black" href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" title="{{ $post['title'] }}" class="title">{{ $post['title'] }}</a>
                                </div>

                               <!--   <div class="cateitemnuew">
                                     <div class="img">
                                     <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="{{ $post['title'] }}"><img src="{{ !empty($post['image']) ? asset($post['image']) : '' }}" alt=""></a>
                                     </div>
                                 </div>
                                 <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="{{ $post['title'] }}" class="title">{{ $post['title'] }}</a> -->

                              </div>
                            @endforeach
                        @endif
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </section>
    <script>
        //Đồng bộ chiều cao các div
        $(function() {
            $('.listcate').matchHeight();
        });
    </script>
@endsection
