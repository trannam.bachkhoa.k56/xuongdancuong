<div id="fb-root"></div>
<header class="Desktop">
   <div class="headerTop bgr08 pdt10 pdb10">
      <div class="container">
         <a class="white fw6 bdRightWhite pdr15"><i class="fa fa-phone fa-lg"></i> {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai']  : '' }}</a>
         <a class="maps white fw6 pdl15" href="#"><i class="fa fa-map-marker fa-lg"></i> {{ isset($information['dia-chi-dau-trang']) ? $information['dia-chi-dau-trang']  : '' }}</a>
      </div>
   </div>
   <div class="headerBottom bgrWhite">
      <div class="container">
         <div class="row">
            <div class="col-md-1 col-sm-3 col-xs-4 pdr0">
               <div id="logo" class="mgt10 mgb10 sm-mg10-0">
                  <a href="/" rel="nofollow" class="v2_bnc_logo" title="">
                  <img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}


"  alt="" style="max-width: 100%; width: auto; height: 50px;">
                  </a>
               </div>
            </div>
            <div class="col-md-11 col-sm-9 col-xs-8 pdl0">
               <div class="menuDesktop hideOnTable mgt12">
                  <ul class="nav nav-pills">
                    <li role="presentation"><a href="/" class="black fw7 textUpper hvDM hvNoBgr">Trang chủ</a></li>
					@foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
						<li role="presentation"><a href="{{ route('site_category_product', ['cate_slug' => $cateProduct->slug]) }}" class="black fw7 textUpper hvDM hvNoBgr">{{ $cateProduct->title  }}</a></li>
					@endforeach
                    <li role="presentation"><a href="/danh-muc/tin-tuc" class="black fw7 textUpper hvDM hvNoBgr">tin tức</a></li>
                    <li role="presentation"><a href="/lien-he" class="black fw7 textUpper hvDM hvNoBgr">liên hệ</a></li>
                  </ul>
               </div>
               <div class="menuMobile">
                  <!-- <section class="button_menu_mobile">
                     <div id="nav_list">
                        <span class="menu-line menu-line-1"></span>
                        <span class="menu-line menu-line-2"></span>
                        <span class="menu-line menu-line-3"></span>
                     </div>
                  </section> -->
				<a class="bgrDM f20 pd10 flRight f20 pd5-15 mgt20 sm-mgt10" id="clickMenu">
				   <i class="fa fa-bars white" aria-hidden="true"></i>
				</a>



               </div>


            </div>
         </div>
         <div class="row">
            <div class="menuMobiles none pdl20">
               <ul class="">
                 <li><a href="#" class="black fw7 textUpper hvDM">Trang chủ</a></li>
				 @foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
                 <li><a href="{{ route('site_category_product', ['cate_slug' => $cateProduct->slug]) }}" class="black fw7 textUpper hvDM">{{ $cateProduct->title  }}</a></li>
				 @endforeach
                 <li role="presentation"><a href="/danh-muc/tin-tuc" class="black fw7 textUpper hvDM">tin tức</a></li>
                 <li role="presentation"><a href="/lien-he" class="black fw7 textUpper hvDM">liên hệ</a></li>
               </ul>
            </div>
            <script type="text/javascript">
   $(document).ready(function(){
      $('#clickMenu').click(function(){
         $('.menuMobiles').toggle()
      })
   })
</script>
         </div>
         

      </div>
   </div> 
</header>





	  
	  
	
























