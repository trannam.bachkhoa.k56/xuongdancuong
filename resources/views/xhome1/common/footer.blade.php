<section class="customer v2_bnc_pr_tab_main clearfix">
   <div class="container">
     <div class="row bg">
       <div class="col-12 bgCustomer">

        <div class="v2_bnc_title_tab_main">
            <hgroup class="v2_bnc_title_main">
				<h2>PHẢN HỒI TỪ KHÁCH HÀNG</h2>
			</hgroup>
        </div>

         <div class="owl-carousel customerSaid list">
              @foreach(\App\Entity\SubPost::showSubPost('khach-hang',10, 'asc') as $id => $customer)
                <div class="item">
					@if (isset($customer['ma-chia-se-youtube']) && !empty($customer['ma-chia-se-youtube']))
						{!! $customer['ma-chia-se-youtube'] !!}
					@elseif (isset($customer['image']) && !empty($customer['image']))
						<a href="#" title="">
							<img src="{{ isset($customer['image']) ? asset($customer['image']) : '' }}" alt="">
						</a>
					@endif
                 <p>{{ isset($customer['title']) ? $customer['title'] : '' }}</p>
                </div>
              @endforeach
         </div>

<!-- <i class="fa fa-angle-left"></i>
<i class="fa fa-angle-right"></i> -->

          <script>
         
            $('.customerSaid').owlCarousel({
               loop:true,
               margin:30,
               responsiveClass:true,
               autoplaySpeed:false,
               autoplay:false,
               navigation:true,
               responsive:{
                   0:{
                       items:1,
                       nav:true
                   },
                   600:{
                       items:3,
                       nav:false
                   },
                   1000:{
                       items:4,
                       nav:true,
                       loop:false
                   }
               }
            });
            $('.list .owl-prev').html('<i class="fa fa-angle-left"></i>');
            $('.list .owl-next').html('<i class="fa fa-angle-right"></i>');
         </script>

       </div>

     </div>
   </div>
</section>

<footer class="v2_bnc_footer footerxhome">
   <div class="container">
      <div class="row">
         <!-- Info Company -->
         <div class="v2_bnc_footer_info_company">
            <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom-20">
               <div class="v2_bnc_footer_title">
                  <h4>Tìm kiếm chúng tôi trên Facebook</h4>
               </div>
               <div class="v2_bnc_footer_content">
                  <div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0';  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-page" data-href="<?= isset($information['nhung-fb']) ? $information['nhung-fb']  : '' ?>" data-tabs="timeline" data-height="320" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?= isset($information['nhung-fb']) ? $information['nhung-fb']  : '' ?>" class="fb-xfbml-parse-ignore"><a href="<?= isset($information['nhung-fb']) ? $information['nhung-fb']  : '' ?>"></a></blockquote></div>

               </div>
            </div>


            <div class="col-md-4 col-sm-12 col-xs-12 margin-bottom-20">
               <div class="v2_bnc_footer_title">
                  <h4>Liên hệ</h4>
               </div>
               <div class="v2_bnc_footer_content">
                  <p> <?= isset($information['thong-tin-lien-he']) ? $information['thong-tin-lien-he']  : '' ?></p>
               </div>
            </div>


            <div class="col-md-4 col-sm-12 col-xs-12">
               <div class="v2_bnc_footer_title">
                  <h4>Tin tức nổi bật</h4>
               </div>
               <div class="v2_bnc_footer_content">
                  @foreach(\App\Entity\Post::categoryShow('tin-tuc',4) as $new)
                    <div class="itemnew">
                      <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" title="{{ $new['title'] }}"><img src="{{ !empty($new['image']) ? asset($new['image']) : '' }}" alt=""></a>
                      <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" title="{{ $new['title'] }}" class="titlenew" >{{ $new['title'] }} </a>
                    </div>
                  @endforeach
               </div>
            </div>
         </div>
         <!-- End Info Company <-->
      </div>
   </div>
</footer>
<div class="copyright text-center padding-10">
   <span>{{ isset($information['copyright']) ? $information['copyright']  : '' }}</span>
</div>
<div class="hidden-xs">
      </div>
      <script> 
         jQuery(function ($) { 
             $('#show').hide();
             $('#close').click(function (e) {
               $('#rich-media').hide('slow');
               return false;
             });
             $('#hide').click(function (e) {
               $('#show').show('slow');
               $('#hide').hide('slow');
               $('#rich-media-item').height(0);
               return false;
             });
             $('#show').click(function (e) {
               $('#hide').show('slow'); 
               $('#show').hide('slow');
               $('#rich-media-item').height(205);
               return false;
            });
         });
      </script>
      <!-- End Adv Rich Meida -->
      <!-- Scroll To Top -->
      <div class="v2_bnc_scrolltop">
         <a class="v2_bnc_icon_scrolltop" title="Lên đầu trang !">
         <i class="fa fa-caret-up fa-4x"></i>
         </a>
		 
			
      </div>
		<div class="cricleMB">
			<div class="cricle">
				<a href="tel:{{ isset($information['so-dien-thoai-lien-he']) ? $information['so-dien-thoai-lien-he']  : '' }}">
			 </a>
			</div>
		</div>
	
<style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff; width: 60px; height: 60px; text-align: center; bottom: 30px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{width: 120px; left: -140px; top: 5px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style>
<div class="fb-livechat"><div class="ctrlq fb-overlay"></div><div class="fb-widget"> <div class="ctrlq fb-close"></div><div class="fb-page" data-href="https://www.facebook.com/{{ isset($information['ten-tom-tat-fanpage-facebook']) ? $information['ten-tom-tat-fanpage-facebook'] : '' }}" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div><div class="fb-credit"> <a href="https://moma.vn" target="_blank">Powered by MOMA</a> </div><div id="fb-root"></div></div><a href="https://m.me/{{ isset($information['ten-tom-tat-fanpage-facebook']) ? $information['ten-tom-tat-fanpage-facebook'] : '' }}" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button"> <div class="bubble">1</div><div class="bubble-msg">Bạn cần hỗ trợ?</div></a></div><script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script><script>jQuery(document).ready(function($){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){return true;}else{return false;}}var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")}; setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>
	
