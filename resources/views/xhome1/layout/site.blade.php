<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />   
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <link rel="icon" href="{{ !empty($information['icon']) ?  asset($information['icon']) : '' }}" type="image/x-icon" />

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />

    <link rel="stylesheet" href="{{ asset('xhome/css/reset.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/bootstrap.min.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/owl.carousel.css') }}" media="all" type="text/css" /> 
    <link rel="stylesheet" href="{{ asset('xhome/css/owl.theme.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/owl.transitions.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/animate.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/jquery.nouislider.min.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/owl-slideshow-main.css') }}" media="all" type="text/css" />
     <link rel="stylesheet" href="{{ asset('xhome/css/style.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/mobile.css') }}" media="all" type="text/css" />

    <link rel="stylesheet" href="{{ asset('xhome/css/owl.carousel.min.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/owl.theme.default.min.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/coverStyle.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/extra.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/font-awesome.min.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('xhome/css/simple-line-icons.css') }}" media="all" type="text/css" />
	
    <script type="text/javascript" src="{{ asset('xhome/js/jquery.min.js') }}"></script>
    
   <!-- <script type="text/javascript" src="{{ asset('xhome/js/js/search.js') }}"></script> -->
    <!-- <script type="text/javascript" src="{{ asset('xhome/js/jwplayer.js') }}"></script> -->
    <!--<script type="text/javascript" src="{{ asset('xhome/js/jquery.nouislider.all.min.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('diamond/js/jquery.matchHeight-min.js') }}"></script>
   
	<script type="text/javascript" src="{{ asset('xhome/js/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('clothes-02/js/numeral.min.js') }}"></script>
	
	<!--<script src="{{ asset('xhome/js/js_customs.js') }}"></script> -->
    {{--<link rel="shortcut icon" href="{{ asset($information['logo']) }}">--}}

	<!-- Google Tag Manager -->

	{!! isset($information['google-anylic']) ? $information['google-anylic'] : '' !!}
	
	{!! isset($information['facebook-pixel']) ? $information['facebook-pixel'] : '' !!}

    <script>
        var dataLayer = [];
        dataLayer.push({
            'dynx_itemid': '@yield('dynx_itemid')',
            'dynx_pagetype' : '@yield('dynx_pagetype')',
            'dynx_totalvalue' : '@yield('dynx_totalvalue')'
        });
    </script>
</head>

<body>
	
    @include('xhome.common.header')
    <!-- Phần nội dung -->
    @yield('content')
	
    @include('xhome.common.footer')

    <script>
        

        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();
			$(e).find('button[type=submit]').text('Đang gửi đơn hàng ...');

            $.ajax({
                type: "POST",
                url: '{!! route('addToCart') !!}',
                data: data,
                success: function(result){
         

                    alert('Cảm ơn bạn đã đặt hàng bên website chúng tôi, chúng tôi sẽ sớm liên hệ với bạn trong thời gian sớm nhất có thể.');
					$(e).find('button[type=submit]').text('Đặt hàng ngay');
                },
                error: function(error) {
                    alert('Lỗi gì đó đã xảy ra!');
					$(e).find('button[type=submit]').text('Đặt hàng ngay');
                }

            });

            return false;
        }
        
        function contact(e) {
			var $btn = $(e).find('button').button('loading');
			var data = $(e).serialize();
		
			 $.ajax({
				type: "POST",
				url: '{!! route('sub_contact') !!}',
				data: data,
				success: function(result){
					var obj = jQuery.parseJSON( result);
			// gửi thành công
				if (obj.status == 200) {
					alert(obj.message);
					$btn.button('reset');
			  
				return;
			}
			
			// gửi thất bại
			if (obj.status == 500) {
			  alert(obj.message);
			  $btn.button('reset');
			  
			  return;
			}
				},
				error: function(error) {
					//alert('Lỗi gì đó đã xảy ra!')
				}

			});

			return false;
		}
    </script>

	{!! isset($information['chat-zalo']) ? $information['chat-zalo'] : '' !!}

	<style>
	.zalo-chat-widget{
		bottom: 20% !important;
		right: .5em !important;
		z-index:999;
		width: 50px !important;
		height: 50px !important;
		
	}
	.ctrlq.fb-button{
			bottom:10%!important;
			right:1em !important;
			width: 45px;
		height: 45px;
		}

	</style>

    <!--<script type="text/javascript" src="{{ asset('xhome/js/product.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('xhome/js/bootstrap.js') }}"></script>
   
    <!--<script type="text/javascript" src="{{ asset('xhome/js/webcommon.js') }}"></script>
    <script type="text/javascript" src="{{ asset('xhome/js/jquery.validationEngine.js') }}"></script>
    <script type="text/javascript" src="{{ asset('xhome/js/jquery.validationEngine-vi.js') }}"></script>

    <script type="text/javascript" src="{{ asset('xhome/js/loading-overlay.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('xhome/js/load.js') }}"></script>
    <script type="text/javascript" src="{{ asset('xhome/js/fastCart.js') }}"></script>-->
    <!-- CSS -->
      <link rel="stylesheet" href="{{ asset('xhome/css/jquery.fancybox.css') }}" media="all" type="text/css" />
    <!-- END CSS -->
    <!--<script type="text/javascript" src="{{ asset('xhome/js/jquery.fancybox.pack.js') }}"></script>-->
    <script type="text/javascript" src="{{ asset('xhome/js/jquery.elevatezoom.js') }}"></script> 
	
   <!--  <script type="text/javascript" src="{{ asset('xhome/js/cookie.js') }}"></script> -->

</body>
</html>
