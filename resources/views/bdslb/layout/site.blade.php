<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />   
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <link rel="icon" href="{{ !empty($information['icon-logo']) ?  asset($information['icon-logo']) : '' }}" type="image/x-icon" />

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />


    <script type="text/javascript">
        ! function(a, b, c) {
            function d(a) {
                var c, d, e, f, g, h = b.createElement("canvas"),
                    i = h.getContext && h.getContext("2d"),
                    j = String.fromCharCode;
                if (!i || !i.fillText) return !1;
                switch (i.textBaseline = "top", i.font = "600 32px Arial", a) {
                    case "flag":
                        return i.fillText(j(55356, 56806, 55356, 56826), 0, 0), !(h.toDataURL().length < 3e3) && (i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 65039, 8205, 55356, 57096), 0, 0), c = h.toDataURL(), i.clearRect(0, 0, h.width, h.height), i.fillText(j(55356, 57331, 55356, 57096), 0, 0), d = h.toDataURL(), c !== d);
                    case "diversity":
                        return i.fillText(j(55356, 57221), 0, 0), e = i.getImageData(16, 16, 1, 1).data, f = e[0] + "," + e[1] + "," + e[2] + "," + e[3], i.fillText(j(55356, 57221, 55356, 57343), 0, 0), e = i.getImageData(16, 16, 1, 1).data, g = e[0] + "," + e[1] + "," + e[2] + "," + e[3], f !== g;
                    case "simple":
                        return i.fillText(j(55357, 56835), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                    case "unicode8":
                        return i.fillText(j(55356, 57135), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0];
                    case "unicode9":
                        return i.fillText(j(55358, 56631), 0, 0), 0 !== i.getImageData(16, 16, 1, 1).data[0]
                }
                return !1
            }

            function e(a) {
                var c = b.createElement("script");
                c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }
            var f, g, h, i;
            for (i = Array("simple", "flag", "unicode8", "diversity", "unicode9"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, h = 0; h < i.length; h++) c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function() {
                c.DOMReady = !0
            }, c.supports.everything || (g = function() {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function() {
                "complete" === b.readyState && c.readyCallback()
            })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>

    <link rel='stylesheet' id='bootstrap_tab-css' href='{{ asset('bdslb/css/bootstrap_tab.min.css?ver=4.6.12') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap_tab-css' href='{{ asset('bdslb/css/bootstrap.min.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap_dropdown-css' href='{{ asset('bdslb/css/bootstrap_dropdown.min.css?ver=4.6.12') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='ert_tab_icon_css-css' href='{{ asset('bdslb/css/res_tab_icon.css?ver=4.6.12') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='bhittani_plugin_kksr-css' href='{{ asset('bdslb/css/css.css?ver=2.6.1') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='responsive-lightbox-swipebox-css' href='{{ asset('bdslb/css/swipebox.min.css?ver=1.7.2') }}css/' type='text/css' media='all' />
    <link rel='stylesheet' id='siteorigin-panels-front-css' href='{{ asset('bdslb/css/front-flex.css?ver=2.5.16') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='hamburger.css-css' href='{{ asset('bdslb/css/wpr-hamburger.css?ver=1.0') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='wprmenu.css-css' href='{{ asset('bdslb/css/wprmenu.css?ver=1.0') }}' type='text/css' media='all' />

    <style id='wprmenu.css-inline-css' type='text/css'>

    </style>
    <link rel='stylesheet' id='wpr_icons-css' href='{{ asset('bdslb/css/style1.css?ver=1.0') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='addthis_all_pages-css' href='{{ asset('bdslb/css/addthis_wordpress_public.min.css?ver=4.6.12') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='flexslider-css' href='{{ asset('bdslb/css/flexslider.css?ver=4.6.12') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='fontawesome-css' href='{{ asset('bdslb/css/font-awesome.min.css?ver=4.6.12') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='stylesheet-css' href='{{ asset('bdslb/css/style2.css?ver=4.6.12') }}' type='text/css' media='all' />
    <style id='stylesheet-inline-css' type='text/css'>

    </style>
    <link rel='stylesheet' id='responsive-css' href='{{ asset('bdslb/css/responsive.css?ver=4.6.12') }}' type='text/css' media='all' />
    <script type='text/javascript' src='{{ asset('bdslb/js/jquery.js?ver=1.12.4') }}'></script>
    <script type='text/javascript' src='{{ asset('bdslb/js/jquery-migrate.min.js?ver=1.4.1') }}'></script>
    {{--<script type='text/javascript' src='{{ asset('bdslb/js/js.min.js?ver=2.6.1') }}'></script>--}}
    <script type='text/javascript' src='{{ asset('bdslb/js/jquery.swipebox.min.js?ver=1.7.2') }}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var rlArgs = {
            "script": "swipebox",
            "selector": "lightbox",
            "customEvents": "",
            "activeGalleries": "1",
            "animation": "1",
            "hideCloseButtonOnMobile": "0",
            "removeBarsOnMobile": "0",
            "hideBars": "1",
            "hideBarsDelay": "5000",
            "videoMaxWidth": "1080",
            "useSVG": "1",
            "loopAtEnd": "0",
            "woocommerce_gallery": "0"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{ asset('bdslb/js/front.js?ver=1.7.2') }}'></script>
    <script type='text/javascript' src='{{ asset('bdslb/js/modernizr.custom.js?ver=1.0') }}'></script>
    <script type='text/javascript' src='{{ asset('bdslb/js/jquery.touchSwipe.min.js?ver=1.0') }}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wprmenu = {
            "zooming": "yes",
            "from_width": "768",
            "push_width": "400",
            "menu_width": "80",
            "parent_click": "yes",
            "swipe": "no"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{ asset('bdslb/js/wprmenu.js?ver=1.0') }}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var mts_customscript = {
            "responsive": "1",
            "nav_menu": "secondary"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{ asset('bdslb/js/customscript.js?ver=4.6.12') }}'></script>
    <script type='text/javascript' src='{{ asset('bdslb/js/jquery.flexslider-min.js?ver=4.6.12') }}'></script>
    <script type='text/javascript' src='{{ asset('bdslb/js/parallax.js?ver=4.6.12') }}'></script>

    <link href="//fonts.googleapis.com/css?family=Roboto:700|Roboto:normal&amp;subset=latin" rel="stylesheet" type="text/css">

    <link rel='stylesheet' id='stylesheet-css' href='{{ asset('bdslb/css/style_header.css') }}' type='text/css' media='all' />
    <link rel='stylesheet' id='stylesheet-css' href='{{ asset('bdslb/css/ert_css.css') }}' type='text/css' media='all' />
    <script type="text/javascript">
        document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/, 'js');
    </script>
    <script data-cfasync="false" type="text/javascript">
        if (window.addthis_product === undefined) {
            window.addthis_product = "wpp";
        }
        if (window.wp_product_version === undefined) {
            window.wp_product_version = "wpp-6.1.2";
        }
        if (window.wp_blog_version === undefined) {
            window.wp_blog_version = "4.6.12";
        }
        if (window.addthis_share === undefined) {
            window.addthis_share = {};
        }
        if (window.addthis_config === undefined) {
            window.addthis_config = {
                "data_track_clickback": true,
                "ignore_server_config": true,
                "ui_atversion": 300
            };
        }
        if (window.addthis_layers === undefined) {
            window.addthis_layers = {};
        }
        if (window.addthis_layers_tools === undefined) {
            window.addthis_layers_tools = [{
                "sharetoolbox": {
                    "numPreferredServices": 5,
                    "counts": "one",
                    "size": "32px",
                    "style": "fixed",
                    "shareCountThreshold": 0,
                    "elements": ".addthis_inline_share_toolbox_below,.at-below-post-homepage,.at-below-post-arch-page,.at-below-post-cat-page,.at-below-post,.at-below-post-page"
                }
            }];
        } else {
            window.addthis_layers_tools.push({
                "sharetoolbox": {
                    "numPreferredServices": 5,
                    "counts": "one",
                    "size": "32px",
                    "style": "fixed",
                    "shareCountThreshold": 0,
                    "elements": ".addthis_inline_share_toolbox_below,.at-below-post-homepage,.at-below-post-arch-page,.at-below-post-cat-page,.at-below-post,.at-below-post-page"
                }
            });
        }
        if (window.addthis_plugin_info === undefined) {
            window.addthis_plugin_info = {
                "info_status": "enabled",
                "cms_name": "WordPress",
                "plugin_name": "Share Buttons by AddThis",
                "plugin_version": "6.1.2",
                "plugin_mode": "WordPress",
                "anonymous_profile_id": "wp-9bfbdfbe3eff59da53af0058f8ac0a20",
                "page_info": {
                    "template": "home",
                    "post_type": ""
                },
                "sharing_enabled_on_post_via_metabox": false
            };
        }
        (function() {
            var first_load_interval_id = setInterval(function() {
                if (typeof window.addthis !== 'undefined') {
                    window.clearInterval(first_load_interval_id);
                    if (typeof window.addthis_layers !== 'undefined' && Object.getOwnPropertyNames(window.addthis_layers).length > 0) {
                        window.addthis.layers(window.addthis_layers);
                    }
                    if (Array.isArray(window.addthis_layers_tools)) {
                        for (i = 0; i < window.addthis_layers_tools.length; i++) {
                            window.addthis.layers(window.addthis_layers_tools[i]);
                        }
                    }
                }
            }, 1000)
        }());
    </script>
	{!! isset($information['lay-so-dien-thoai']) ? $information['lay-so-dien-thoai'] : '' !!}

</head>

<body data-rsssl=1 id="blog" class="home page page-id-2926 page-template page-template-page-parallax page-template-page-parallax-php main siteorigin-panels siteorigin-panels-before-js siteorigin-panels-home" >
	
	{!! isset($information['google-analytics']) ? $information['google-analytics'] : '' !!}
	
    <h1 id='h1_top'>@yield('title')</h1>
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', '<ua-717453></ua-717453>79-2', 'auto');
        ga('send', 'pageview');
    </script>
    <div class="main-container-wrap">
        <!-- START HEADER  -->
    @include('bdslb.common.header')
    <!-- END HEADER-->
    @yield('content')
        
    @include ('bdslb.common.footer')

    @include ('bdslb.common.footer_phone')

    </div>
    
    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: '{!! route('addToCart') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);

                    window.location.replace("/gio-hang");
                },
                error: function(error) {
                    alert('Lỗi gì đó đã xảy ra!')
                }

            });

            return false;
        }
    </script>

</body>
</html>
