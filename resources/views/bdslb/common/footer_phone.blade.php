<link rel="stylesheet" href="{{ asset('bdslb/css/style.css?v=1526908955') }}" type="text/css" />
<style type="text/css">
    .phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle {
        border-color: #0084ff
    }

    .phonering-alo-phone.phonering-alo-green .phonering-alo-ph-circle-fill,
    .phonering-alo-phone.phonering-alo-green .phonering-alo-ph-img-circle {
        background-color: #0084ff
    }
    /*.phonering-alo-phone.phonering-alo-green .phonering-alo-ph-img-circle a{color: #0084ff}*/

    .phonering-alo-ph-img-circle {
        width: 45px;
        height: 45px;
        top: 45px;
        left: 45px
    }

    .phonering-alo-ph-img-circle a {
        width: 45px;
        line-height: 45px
    }

    .phonering-alo-ph-circle-fill {
        width: 90px;
        height: 90px;
        top: 22.5px;
        left: 22.5px
    }

    .echbay-alo-phone,
    .phonering-alo-ph-circle {
        width: 135px;
        height: 135px
    }

    .style-for-position-cr,
    .style-for-position-cl {
        margin-top: -67.5px;
    }
    /* for mobile */

    @media screen and (max-width:0px) {
        .style-for-position-bl {
            left: -45px;
            bottom: -45px
        }
        .style-for-position-br {
            right: -45px;
            bottom: -45px
        }
        .style-for-position-cl {
            left: -45px;
        }
        .style-for-position-cr {
            right: -45px;
        }
        .style-for-position-tl {
            top: -45px;
            left: -45px
        }
        .style-for-position-tr {
            top: -45px;
            right: -45px
        }
    }

    .echbay-alo-phone {
        display: block
    }

    .phonering-alo-ph-img-circle {
        background-image: none !important
    }

    .phonering-alo-ph-img-circle a {
        text-indent: 0;
        text-align: center;
        font-size: 1px
    }
    /* Custom CSS */
</style>
<div class="echbay-alo-phone phonering-alo-phone phonering-alo-green style-for-position-bl">
    <div class="phonering-alo-ph-circle"></div>
    <div class="phonering-alo-ph-circle-fill"></div>
    <div class="phonering-alo-ph-img-circle"><a href="tel:{{ isset($information['hotline']) ? $information['hotline'] : ''}}" class="{{ isset($information['hotline']) ? $information['hotline'] : ''}}">.</a></div>
</div>
<script type="text/javascript" src="{{ asset('bdslb/js/js.js?v=1526908955') }}" async></script>
<!-- End EchBay Phonering Alo -->
<div id="wprmenu_bar" class="wprmenu_bar bodyslide left">
    <div class="hamburger hamburger--slider">
            <span class="hamburger-box">
            <span class="hamburger-inner"></span>
            </span>
    </div>
    <div class="menu_title">
        MENU
    </div>
</div>
<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left default " id="mg-wprm-wrap">
    <ul id="wprmenu_menu_ul">
        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
            @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id=>$menuElement)
                <li id="menu-item-2940" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2926 current_page_item menu-item-2940">
                    <a href="{{ $menuElement['url'] }}">{{ $menuElement['title_show'] }}</a>
                    @if (!empty($menuElement['children']))
                    <ul class="sub-menu">
                        @foreach ($menuElement['children'] as $elementChild)
                        <li id="menu-item-3612" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-3612">
                            <a href="{{ $elementChild['url'] }}">{{ $elementChild['title_show'] }}</a></li>
                        @endforeach
                    </ul>
                    @endif
                </li>
            @endforeach
        @endforeach
    </ul>
</div>
<link rel='stylesheet' id='ert_tab_css-css' href='{{ asset('bdslb/') }}css/tabdrop.css?ver=4.6.12' type='text/css' media='all' />
<link rel='stylesheet' id='ert_css-css' href='{{ asset('bdslb/') }}css/ert_css.php.css?ver=4.6.12' type='text/css' media='all' />
<script type='text/javascript'>
    window._gscq = window._gscq || []
</script>
{{--<script type='text/javascript' data-cfasync="false" async src='138318/script.js?ver=2.1.1'></script>--}}
<script type='text/javascript' src='{{ asset('bdslb/js/bootstrap-dropdown.js?ver=3.1') }}'></script>
<script type='text/javascript' src='{{ asset('bdslb/js/bootstrap-tab.js?ver=3.1') }}'></script>
<script type='text/javascript' src='{{ asset('bdslb/js/bootstrap-tabdrop.js?ver=3.1') }}'></script>
<script type='text/javascript' src='{{ asset('bdslb/js/ert_js.php.js?ver=3.1') }}'></script>
<script type='text/javascript' src='{{ asset('bdslb/js/wp-embed.min.js?ver=4.6.12') }}'></script>
<script type='text/javascript' src='{{ asset('bdslb/js/sticky.js?ver=4.6.12') }}'></script>
<script type="text/javascript">
    document.body.className = document.body.className.replace("siteorigin-panels-before-js", "");
</script>


</body>
