
<header class="main-header">
	<div id="header">
		<div class="container">
			<div class="header-inner">
				<div class="logo-wrap">
					<h1 id="logo" class="image-logo">
						<a href="">
							<img src="{{ isset($information['logo']) ? $information['logo'] : '' }}" alt="logo">
						</a>
					</h1>
					<!-- END #logo -->
				</div>
				<div id="mts_ad_728_widget-2" class="widget-header">
					<div class="ad-728">
						<a href="">
							<img src="{{ isset($information['banner-dau-trang']) ? $information['banner-dau-trang'] : '' }}" width="728" height="90" alt="" />
						</a>
					</div>
				</div>
			</div>
		</div>
		<!--.container-->
		<div class="clear" id="catcher"></div>
		<div id="sticky" class="secondary-navigation">
			<div class="container clearfix">
				<nav id="navigation" class="clearfix">
					<a href="#" class="toggle-mobile-menu">Menu
					</a>
					<ul id="menu-menu-chinh" class="menu clearfix">
						@foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
							@foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id=>$menuElement)
								<li id="menu-item-2940" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2926 current_page_item">
									<a href="{{ $menuElement['url'] }}">{{ $menuElement['title_show'] }}</a>
									@if (!empty($menuElement['children']))
									<ul class="sub-menu">
										@foreach ($menuElement['children'] as $elementChild)
										<li id="menu-item-3612" class="menu-item menu-item-type-post_type menu-item-object-post">
											<a href="{{ $elementChild['url'] }}">{{ $elementChild['title_show'] }}</a>
										</li>
										@endforeach
									</ul>
									@endif
								</li>
							@endforeach
						@endforeach
					</ul>
				</nav>
			</div>
			<!--.container-->
		</div>
	</div>
	<!--#header-->
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	{{--<script type="text/javascript" src="/s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56ea4191ed5b05d1"></script>--}}
	<style>
		.main-header {
			background-color: #ffffff;
			background-image: url({{ isset($information['banner-to-website']) ? $information['banner-to-website'] : '' }});
		}
	</style>
</header>
