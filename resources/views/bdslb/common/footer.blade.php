
<footer>
    <div class="container">
    <div class="footer-widgets top-footer-widgets widgets-num-3">
        <div class="f-widget f-widget-1">
        </div>
        <div class="f-widget f-widget-2">
            <div id="text-3" class="widget widget_text">
                <h3 class="widget-title">Cam kết từ đơn vị bán hàng</h3>
                <div class="textwidget">
                    <p style="text-align: justify; color: white;">
                         {!! isset($information['cam-ket-tu-don-vi-ban-hang']) ? $information['cam-ket-tu-don-vi-ban-hang'] : '' !!}
                    </p>
                </div>
            </div>
        </div>
        <div class="f-widget f-widget-3 last">
            <div id="text-4" class="widget widget_text">
                <h3 class="widget-title">Liên hệ phòng bán hàng</h3>
                <div class="textwidget">
                    <p style="text-align: justify; color: white;">{!! isset($information['lien-he-ban-hang']) ?
                        $information['lien-he-ban-hang'] : '' !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!--.top-footer-widgets-->
</div>
<div class="copyrights">
    <div class="container">
        <!--start copyrights-->
        <div id="copyright-note">
            <span><a href="" title="" rel="nofollow">{!! isset($information['coppyright']) ?
                        $information['coppyright'] : '' !!}</a> Copyright &copy; 2018.</span>
            <div class="right"></div>
        </div>
        <!--end copyrights-->
    </div>
    <!--.container-->
</div>
</div>
</footer>

<div class="Notification" id="popupVitural">
    <div class="Closed">X</div>
    <div class="Content">
        <h3>Phòng kinh doanh: {{ isset($information['hotline']) ? $information['hotline'] : '' }} </h3>
        <form action="{{ route('sub_contact') }}" method="post">
            {!! csrf_field() !!}
            <div class="form-group">
                <label>Họ và tên (*)</label>
                <input type="text" class="form-control" name="name" required placeholder="Họ và tên"/>
            </div>

            <div class="form-group">
                <label>Email (*)</label>
                <input type="email" class="form-control" name="email" required placeholder="Email"/>
            </div>

            <div class="form-group">
                <label>Số điện thoại (*)</label>
                <input type="number" class="form-control"  name="phone" required placeholder="Số điện thoại"/>
            </div>

            <div class="form-group">
                <label>Nội dung quan tâm (*)</label>
                <textarea name="message" class="form-control" required></textarea>
            </div>

            <div class="form-group">
                <Button type="submit" class="btn btn-warning form-control">GỬI</Button>
            </div>
        </form>
    </div>

</div>
<style>
    .Notification {
        background: rgb(163, 48, 18) !important;
        color: white;
    }
    .Notification h3 {
        color: white;
    }
    #popupVitural {
        position: fixed;
        width: 400px;
        bottom: 20px;
        left: 10px;
        display: none;
        box-shadow: 0 2px 3px #d0d0d0;
        border: 1px solid #d0d0d0;
        background: white;
        padding: 10px;
        z-index: 99999;
    }
    #popupVitural .Closed {
        position: absolute;
        right: -10px;
        background: #092b6e;
        color: white;
        border-radius: 100%;
        top: -11px;
        padding: 2px 6px;
    }
    @media (max-width: 769px)
    {
        #popupVitural {
            display: none;
        }
    }
</style>
<script>
    $(document).ready(function() {
        $('#popupVitural').hide();

        setInterval(function(){
            $('#popupVitural').show().slideDown();
        }, 5000);


        $('#popupVitural .Closed').click(function(){
            $('#popupVitural').slideUp().hide();
            $('#popupVitural').addClass('hide');
        });
    });
</script>
