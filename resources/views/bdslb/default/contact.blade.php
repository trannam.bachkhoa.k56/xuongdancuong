@extends('bdslb.layout.site')

@section('title',isset($category->title) ? $category->title : '')
@section('meta_description','')
@section('keywords', '')

@section('content')
	<p style="color: red; font-size: 18px; padding: 25px 0 40px 0;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
@endsection
