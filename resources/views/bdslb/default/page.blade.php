@extends('clothes-01.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
    <section class="clearfix">
        <div class="container mt30">
            <div class="row">
                {{--@include('site.partials.sidebar_cate_product', ['cateSlug' => $category->slug])--}}
                <div class="col-md-12 col-sm-12 col-xs-12 ViewContent">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/">Trang chủ</a>
                        </li>
                        <li>
                            <a href="/danh-muc/{{ $category->slug }}">{{ $category->title }}</a> <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="active"> {{$post->title}}</a>
                        </li>
                    </ul>

                    <div class="listPost sideBarContent">
                        <div class="PostView">
                            <h1 class="titleHead">{{ $post->title }}</h1>

                            <div class="likeAndShare" style="width:100% !important">
                                @include('clothes-01.common.like_and_share', ['link' => route('post', ['cate_slug' => $category->slug , 'post_slug' => $post->slug])])
                            </div>



                            <p><b>{{ $post->description }}</b></p>
                            <p><?= $post->content ?></p>
                            @if(!empty($post->tags))
                                <p>Tag:
                                    @foreach(explode(',', $post->tags) as $tag)
                                        <a href="/tim-kiem?word={{ $tag }}&category=all"><span class="tag">{{ $tag }},</span></a>
                                    @endforeach
                                </p>
                            @endif
                            <div class="script" style="padding: 10px 0px;">
                                <?= $post['script-bai-viet'] ?>
                            </div>
                            <div class="likeAndShare">
                                @include('clothes-01.common.like_and_share', ['link' => route('post', ['cate_slug' => $category->slug , 'post_slug' => $post->slug])])
                            </div>

                        </div>
                    </div>

                    <div class="postRelative clearfix">
                        <h3 class="titlel"><span class="line"></span>Ý kiến khách hàng</h3>
                        {{--@include('site.partials.comment', ['post_id' => $post->post_id] )--}}
                        <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=887742841384108';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments" data-href="{{ route('post', ['cate_slug' => $category->slug, 'post_slug' => $post->slug]) }}" data-numposts="5" data-width="100%"></div>
                    </div>

                    <div class="postRelative clearfix">
                        <h3 class="titlel"><span class="line"></span>Bài viết liên quan</h3>
                        @foreach (\App\Entity\Post::relativeProduct($post->slug) as $id => $postRelative)
                            <div class="item">
                                <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}">
                                    <h5><i class="fa fa-stop" aria-hidden="true"></i>  {{ $postRelative->title }}</h5>
                                </a>
                            </div>
                        @endforeach
                    </div>

                    @if (!empty($post->product_list))
                        <div class="ProductReaded">
                            <div class="hotDeal clearfix">
                                <div class="mainTitle linegreen">
                                    <h2 class="titleV bggreen"><i class="fa fa-cubes" aria-hidden="true"></i>Có thể bạn quan tâm</h2>
                                </div>
                                <!-- list item hot deal-->
                                <?php $productRelatives = explode(',', $post->product_list);  ?>
                                @foreach($productRelatives as $id => $productSlug)
                                    <?php $productReaded = \App\Entity\Product::detailProduct(trim($productSlug));?>
                                    <div class="col-md-2 col-xs-6">
                                        <div class="item">
                                            <div class="CropImg">
                                                <a href="{{ route('product', [ 'post_slug' => $productReaded->slug]) }}" class="image thumbs">
                                                    <img src="{{ !empty($productSlug->image) ?  asset($productSlug->image) : asset('/site/img/no-image.png') }}" />
                                                    @if (!empty($productReaded->discount))
                                                        <span class="discountPersent">-{{ round(($productReaded->price - $productReaded->discount) / $productReaded->price * 100) }}%</span>
                                                    @endif
                                                </a>
                                            </div>
                                            <a href="{{ route('product', [ 'post_slug' => $productReaded->slug]) }}">
                                                <h3>{{ $productReaded->title }}</h3>
                                            </a>
                                            <div class="price">
                                                @if (!empty($productReaded->discount))
                                                    <span class="priceOld">{{ number_format($productReaded->price  , 0, ',', '.') }}</span>
                                                    <span class="priceDiscount">{{ number_format($productReaded->discount  , 0, ',', '.') }}</span> VNĐ
                                                @else
                                                    <span class="priceDiscount">{{ number_format($productReaded->price  , 0, ',', '.') }}</span> VNĐ
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection


