@extends('bdslb.layout.site')

@section('title', isset($information['meta-title']) ? $information['meta-title'] : '')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '')

@section('content')
    @php $posts = $products; @endphp
    <div class="main-container">
        <div id="page">
            <div class="article">
                <div id="content_box">
                    <h1 class="postsby">
                        <span>Tin tức</span>
                    </h1>
                    <section id="latest-posts" class="clearfix">
                        @foreach ($posts as $post)
                            <article class="latestPost post-box vertical  ">
                                <div class="post-img">
                                    <a href="#" title="Mặt bằng Nhà ở thu nhập thấp No7 Sài Đồng &#8211; Diện tích từ 45-70m2" rel="nofollow">
                                        <img width="390" height="250" src="{{ asset($post->image) }}"
                                             class="attachment-featured size-featured wp-post-image" alt="{{ $post->title }}" title="{{ $post->title }}" />
                                    </a>
                                </div>
                                <div class="post-data">
                                    <div class="post-data-container">
                                        <header>
                                            <h2 class="title post-title">
                                                <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}"
                                                   title="{{ $post->title }}">
                                                    {{ $post->title }}
                                                </a>
                                            </h2>
                                            <div class="post-info">
                                                @php
                                                    $date=date_create($post->created_at);
                                                @endphp
                                                <span class="thetime updated">{{ date_format($date,"d-m-Y") }}</span>
                                            </div>
                                        </header>
                                        <div class="post-excerpt">
                                            <div class="at-above-post-cat-page addthis_tool" >

                                            </div>
                                            {{ $post->description }}
                                        </div>
                                        <div class="readMore">
                                            <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}"
                                               title="{{ $post->title }}" rel="nofollow">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                    @endforeach


                    <!--.post-box-->
                        <!--Start Pagination-->
                        <div class='pagination'>
                            {{ $posts->links() }}
                        </div>
                        <!--End Pagination-->
                    </section>
                    <!--#latest-posts-->
                </div>
            </div>
            <aside class="sidebar c-4-12">
                <div id="sidebars" class="g">
                    <div class="sidebar">
                        <ul class="sidebar_list">
                            <div id="mts_ad_300_widget-2" class="widget mts_ad_300_widget">
                                <div class="ad-300">
                                    <a href="">
                                        <img src="{{ asset('bdslb/images/du-an-berriver-long-bien.jpg') }}" width="300" height="250" alt="" />
                                    </a>
                                </div>
                            </div>
                            @if (!\App\Entity\Post::newPost('tin-tuc',6)->isEmpty())
                                <div id="single_category_posts_widget-4" class="widget widget_single_category_posts_widget">
                                    <h3 class="widget-title">Thông tin mới nhất</h3>
                                    <ul class="category-posts">
                                        @foreach (\App\Entity\Post::newPost('tin-tuc',6) as $id => $postRelative)
                                            <li class="post-box horizontal-small horizontal-container">
                                                <div class="horizontal-container-inner">
                                                    <div class="post-img">
                                                        <a rel="nofollow" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}" title="{{ $postRelative->title }}">
                                                            <img width="115" height="115" src="{{ asset($postRelative->image) }}" class="attachment-smallthumb size-smallthumb wp-post-image" alt="{{ $postRelative->title }}" title="{{ $postRelative->title }}" />
                                                        </a>
                                                    </div>
                                                    <div class="post-data">
                                                        <div class="post-data-container">
                                                            <div class="post-title">
                                                                <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}" title="{{ $postRelative->title }}">
                                                                    {{ $postRelative->title }}
                                                                </a>
                                                            </div>
                                                            <div class="post-info">
                                                                @php
                                                                    $date=date_create($postRelative->created_at);
                                                                @endphp
                                                                <span class="thetime updated">{{ date_format($date,"d-m-Y") }}</span>
                                                            </div>
                                                            <!--.post-info-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div id="siteorigin-panels-builder-2" class="widget widget_siteorigin-panels-builder">
                                    <div id="pl-w5a7423f91a152" class="panel-layout">
                                        <div id="pg-w5a7423f91a152-0" class="panel-grid panel-has-style">
                                            <div class="panel-row-style panel-row-style-for-w5a7423f91a152-0">
                                                <div id="pgc-w5a7423f91a152-0-0" class="panel-grid-cell">
                                                    <div id="panel-w5a7423f91a152-0-0-0" class="so-panel widget panel-first-child panel-last-child" data-index="0">
                                                        <div class="panel-widget-style panel-widget-style-for-w5a7423f91a152-0-0-0"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </ul>
                    </div>
                </div>
                <!--sidebars-->
            </aside>
        </div>
        <!--#page-->
    </div>
@endsection
