@extends('bdslb.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
    <div class="main-container">
        <div id="page" class="single">
            <article class="article" itemscope itemtype="">
                <div id="content_box">
                    <div id="post-3050" class="g post post-3050 type-post status-publish format-standard has-post-thumbnail hentry category-tin-tuc tag-berriver-390-nguyen-van-cu tag-berriver-nguyen-van-cu tag-chung-cu-390-nguyen-van-cu tag-chung-cu-berriver-nguyen-van-cu tag-du-an-berriver-long-bien tag-du-an-berriver-nguyen-van-cu has_thumb">
                        <div class="single_post">
                            <div class="breadcrumb">
                                <div>
                                    <i class="fa fa-home"></i>
                                </div>
                                <div typeof="v:Breadcrumb" class="root">
                                    <a  href="/" rel="nofollow">Home</a>
                                </div>
                                <div><i class="fa fa-caret-right"></i></div>
                                <div typeof='v:Breadcrumb'>
                                    <span property='v:title'>{{ $post->title }}</span>
                                </div>
                            </div>
                            <header>
                                <h1 class="title single-title entry-title" itemprop="headline">{{ $post->title }}</h1>
                                @if(!empty($post->tags))
                                    <div class="post-info">
                                    <span class="thetags"><i class="fa fa-tags"></i>
                                        @foreach(explode(',', $post->tags) as $tag)
                                            <a href="/tim-kiem?word={{ $tag }}&category=all"><span class="tag">{{ $tag }},</span></a>
                                            <a href="/tim-kiem?word={{ $tag }}&category=all" rel="tag" itemprop="keywords">{ $tag }}</a>,
                                        @endforeach
                                    </span>
                                    </div>

                                @endif

                            </header>
                            <!--.headline_area-->
                            <div class="post-single-content box mark-links entry-content">
                                <div class="at-above-post addthis_tool" ></div>
                                {!! $post->content !!}
                                <!-- kk-star-ratings -->
                                <!-- AddThis Advanced Settings above via filter on the_content -->
                                <!-- AddThis Advanced Settings below via filter on the_content -->
                                <!-- AddThis Advanced Settings generic via filter on the_content -->
                                <!-- AddThis Share Buttons above via filter on the_content -->
                                <!-- AddThis Share Buttons below via filter on the_content -->
                                <div class="at-below-post addthis_tool" data-url="tien-ich-du-an-berriver-long-bien.html"></div>
                                <!-- AddThis Share Buttons generic via filter on the_content -->
                            </div>
                            <!--.post-content box mark-links-->
                        </div>
                        <!--.single_post-->
                        <!-- Start Related Posts -->
                        @if (!\App\Entity\Post::relativeProduct($post->slug,6)->isEmpty())
                        <div class="related-posts">
                            <div class="postauthor-top">
                                <h3>Thông tin liên quan</h3></div>
                            <ul>
                                @foreach (\App\Entity\Post::relativeProduct($post->slug,6) as $id => $postRelative)
                                <li class="post-box horizontal-small">
                                    <div class="horizontal-container">
                                        <div class="horizontal-container-inner">
                                            <div class="post-img">
                                                <a rel="nofollow" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}" title="{{ $post->title }}">
                                                    <img src="{{ asset($postRelative->image) }}" class="attachment-smallthumb wp-post-image" alt="{{ $post->title }}">
                                                </a>
                                            </div>
                                            <div class="post-data">
                                                <div class="post-data-container">
                                                    <div class="post-title">
                                                        <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}" title="{{ $post->title }}">
                                                            {{ $post->title }}
                                                        </a>
                                                    </div>
                                                    <div class="post-info">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <!-- .related-posts -->
                    </div>
                    <!--.g post-->
                </div>
            </article>
            <aside class="sidebar c-4-12">
                <div id="sidebars" class="g">
                    <div class="sidebar">
                        <ul class="sidebar_list">
                            <div id="mts_ad_300_widget-2" class="widget mts_ad_300_widget">
                                <div class="ad-300">
                                    <a href="">
                                        <img src="{{ isset($information['anh-lien-he-trang-chi-tiet']) ? asset($information['anh-lien-he-trang-chi-tiet']) : '' }}" width="300" height="250" alt="" />
                                    </a>
                                </div>
                            </div>
                            @if (!\App\Entity\Post::newPost('tin-tuc',6)->isEmpty())
                            <div id="single_category_posts_widget-4" class="widget widget_single_category_posts_widget">
                                <h3 class="widget-title">Thông tin mới nhất</h3>
                                <ul class="category-posts">
                                    @foreach (\App\Entity\Post::newPost('tin-tuc',6) as $id => $postRelative)
                                    <li class="post-box horizontal-small horizontal-container">
                                        <div class="horizontal-container-inner">
                                            <div class="post-img">
                                                <a rel="nofollow" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}" title="{{ $postRelative->title }}">
                                                    <img width="115" height="115" src="{{ asset($postRelative->image) }}" class="attachment-smallthumb size-smallthumb wp-post-image" alt="{{ $postRelative->title }}" title="{{ $postRelative->title }}" />
                                                </a>
                                            </div>
                                            <div class="post-data">
                                                <div class="post-data-container">
                                                    <div class="post-title">
                                                        <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}" title="{{ $postRelative->title }}">
                                                            {{ $postRelative->title }}
                                                        </a>
                                                    </div>
                                                    <div class="post-info">
                                                        @php
                                                            $date=date_create($postRelative->created_at);
                                                        @endphp
                                                        <span class="thetime updated">{{ date_format($date,"d-m-Y") }}</span>
                                                    </div>
                                                    <!--.post-info-->
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div id="siteorigin-panels-builder-2" class="widget widget_siteorigin-panels-builder">
                                <div id="pl-w5a7423f91a152" class="panel-layout">
                                    <div id="pg-w5a7423f91a152-0" class="panel-grid panel-has-style">
                                        <div class="panel-row-style panel-row-style-for-w5a7423f91a152-0">
                                            <div id="pgc-w5a7423f91a152-0-0" class="panel-grid-cell">
                                                <div id="panel-w5a7423f91a152-0-0-0" class="so-panel widget panel-first-child panel-last-child" data-index="0">
                                                    <div class="panel-widget-style panel-widget-style-for-w5a7423f91a152-0-0-0"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </ul>
                    </div>
                </div>
                <!--sidebars-->
            </aside>

        </div>
        <!--#page-->
    </div>
@endsection


