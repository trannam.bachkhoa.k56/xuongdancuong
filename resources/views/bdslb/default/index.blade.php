@extends('bdslb.layout.site')

@section('title', isset($information['meta-title']) ? $information['meta-title'] : '')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '')

@section('content')
    <div class="main-container">
        <div id="page" class="single">
            <article class="ss-full-width">
                <div id="content_box">
                    <div id="post-2926"
                         class="g post post-2926 page type-page status-publish has-post-thumbnail hentry has_thumb">
                        <header>
                            <h1 class="title entry-title"></h1>
                        </header>

                        <div class="post-content box mark-links entry-content">
                            <div class="at-above-post-homepage addthis_tool" data-url=""></div>
                            <div id="pl-2926" class="panel-layout">
                                <div id="pg-2926-0" class="panel-grid panel-no-style">
                                    @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                        @if ($home['vi-tri'] == 1)
                                    <div id="pgc-2926-0-0" class="panel-grid-cell">
                                        <div id="panel-2926-0-0-0"
                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                             data-index="0">
                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                <div class="siteorigin-widget-tinymce ">
                                                    <h1 style="text-align: center;">
															<span style="color: #993300; vertical-align: inherit;">
																<strong>
																	{{ $home['title'] }}
																</strong>
															</span>
                                                    </h1>
                                                    <p style="text-align: center;">
                                                            <span style="color: #ff0000;vertical-align: inherit;">
                                                        		<strong>
                                									{{ $home['description'] }}
                                                           		</strong>
                                                            </span>
                                                    </p>
                                                    <h1 style="text-align: center;">
                                                        <img class="aligncenter" src="{{ asset('bdslb/images/bg_content(13).png') }}"
                                                             alt="{{ $home['title'] }}"/>
                                                    </h1>
                                                    {!! $home['content'] !!}
                                                    <p style="text-align: justify;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        @endif
                                    @endforeach
                                </div>
                                @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                    @if ($home['vi-tri'] == 2)
                                <div id="pg-2926-1" class="panel-grid panel-no-style">
                                    <div id="pgc-2926-1-0" class="panel-grid-cell">
                                        <div id="panel-2926-1-0-0"
                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                             data-index="1">
                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                <div class="siteorigin-widget-tinymce textwidget">
                                                    <p>
                                                        <a href="{{ $home['link'] }}"
                                                           rel="attachment wp-att-3802">
                                                            <img class="aligncenter size-full wp-image-3802"
                                                                 src="{{ $home['image'] }}"
                                                                 alt="{{ $home['title'] }}" width="610"
                                                                 height="522"/>
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="pgc-2926-1-1" class="panel-grid-cell">
                                        <div id="panel-2926-1-1-0"
                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                             data-index="2">
                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                <div class="siteorigin-widget-tinymce textwidget">
                                                    <p>
                                                        <a href="{{ $home['link'] }}" rel="attachment wp-att-3915">
                                                            <img class="aligncenter size-full wp-image-3915"
                                                                 src="{{ asset('bdslb/./images/anh-hoa-van.png')}}" alt="anh-hoa-van"
                                                                 width="389" height="25"/>
                                                        </a>
                                                    </p>
                                                    {!! $home['content'] !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endif
                                @endforeach
                                @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                    @if ($home['vi-tri'] == 3)
                                <div id="pg-2926-2" class="panel-grid panel-no-style">
                                    <div id="pgc-2926-2-0" class="panel-grid-cell">
                                        <div id="panel-2926-2-0-0"
                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                             data-index="3">
                                            <div class="panel-widget-style panel-widget-style-for-2926-2-0-0">
                                                <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                    <div class="siteorigin-widget-tinymce ">
                                                        <h2 style="text-align:center; color: #003300;">
                                                            <strong>{{ $home['title'] }}</strong>
                                                        </h2>
                                                        <h2 style="text-align: center;">
                                                            <img class="size-full wp-image-2984"
                                                                 src="{{ asset('bdslb//images/line-1-300x25.png') }}" alt="{ $home['title'] }}"
                                                                 width="300" height="25"/>
                                                        </h2>
                                                        {!! $home['content'] !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="pgc-2926-2-1" class="panel-grid-cell">

                                        <div id="panel-2926-2-1-0"
                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                             data-index="4">
                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                <div class="siteorigin-widget-tinymce textwidget">
                                                    <p>
                                                        <img class="aligncenter size-full wp-image-3117"
                                                             src="{{ $home['image'] }}"
                                                             alt="{{ $home['title'] }}" width="650"
                                                             height="520"/>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endif
                                @endforeach
                                @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                    @if ($home['vi-tri'] == 4)
                                <div id="pg-2926-3" class="panel-grid panel-no-style">
                                    <div id="pgc-2926-3-0" class="panel-grid-cell">
                                        <div id="panel-2926-3-0-0"
                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                             data-index="5">
                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                <div class="siteorigin-widget-tinymce textwidget">
                                                    <h3 style="text-align: center;">
                                                        	<span style="color: #33cccc;">
                                                        		<strong>
                                                    				<span style="vertical-align: inherit;">{{ $home['title'] }}</span>
                                                        		</strong>
                                                        	</span>
                                                    </h3>
                                                    <h3 style="text-align: center;">
                                                        	<span style="color: #ff0000; vertical-align: inherit;">
                                                				{{ $home['description'] }} 
                                                        	</span>
                                                    </h3>
                                                    <h3 style="text-align: center;">
                                                        <img class="img_lightbox" src="{{ asset('bdslb/images/bg_content(17).png') }}"
                                                             alt=""/>
                                                    </h3>
                                                    {!! $home['description'] !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endif
                                @endforeach
                                <div id="pg-2926-4" class="panel-grid panel-no-style">
                                    @foreach (\App\Entity\SubPost::showSubPost('gioi-thieu-du-an') as $id => $informationProject)
                                        @if ($id < 2)
                                    <div id="pgc-2926-4-0" class="panel-grid-cell">
                                        <div id="panel-2926-4-0-0"
                                             class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                             data-index="6">
                                            <div class="panel-widget-style panel-widget-style-for-2926-4-0-0">
                                                <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                    <div class="siteorigin-widget-tinymce ">
                                                        <p style="text-align: justify;">
                                                            {!! $informationProject['content'] !!}
                                                        </p>
                                                        <p>
                                                            <img class="aligncenter  wp-image-3019"
                                                                 src="{!! $informationProject['image'] !!}"
                                                                 alt="{!! $informationProject['title'] !!}"
                                                                 width="806" height="605"
                                                                 sizes="(max-width: 806px) 100vw, 806px"/>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        @endif
                                    @endforeach
                                </div>
                                    <div id="pg-2926-4" class="panel-grid panel-no-style">
                                        @foreach (\App\Entity\SubPost::showSubPost('gioi-thieu-du-an') as $id => $informationProject)
                                            @if ($id > 1 && $id < 4)
                                        <div id="pgc-2926-4-0" class="panel-grid-cell">
                                            <div id="panel-2926-4-0-0"
                                                 class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                                 data-index="6">
                                                <div class="panel-widget-style panel-widget-style-for-2926-4-0-0">
                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                        <div class="siteorigin-widget-tinymce ">
                                                            <p style="text-align: justify;">
                                                                {!! $informationProject['content'] !!}
                                                            </p>
                                                            <p>
                                                                <img class="aligncenter  wp-image-3019"
                                                                     src="{!! $informationProject['image'] !!}"
                                                                     alt="{!! $informationProject['title'] !!}"
                                                                     width="806" height="605"
                                                                     sizes="(max-width: 806px) 100vw, 806px"/>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endif
                                        @endforeach
                                    </div>
                                <div id="pg-2926-6" class="panel-grid panel-no-style">
                                    <div id="pgc-2926-6-0" class="panel-grid-cell">
                                        @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                            @if ($home['vi-tri'] == 5)
                                        <div id="panel-2926-6-0-0"
                                             class="so-panel widget widget_sow-editor panel-first-child"
                                             data-index="10">
                                            <div class="panel-widget-style panel-widget-style-for-2926-6-0-0">
                                                <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                    <div class="siteorigin-widget-tinymce textwidget">
                                                        <p style="text-align: center;">
                                                            {!! $home['content'] !!}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endif
                                        @endforeach
                                        @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                            @if ($home['vi-tri'] == 6)
                                        <div id="panel-2926-6-0-1" class="so-panel widget widget_sow-editor"
                                             data-index="11">
                                            <div class="panel-widget-style panel-widget-style-for-2926-6-0-1">
                                                <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                    <div class="siteorigin-widget-tinymce ">
                                                        <h2 style="text-align: center;vertical-align: inherit;">
                                                            	<span style="color: #000080;">
                                                            		<strong>
                                                        				{{ $home['title'] }}
                                                            		</strong>
                                                           		</span>
                                                        </h2>
                                                        <h2 style="text-align: center;">
                                                            <img class="img_lightbox aligncenter"
                                                                 src="{{ asset('bdslb/images/bg_content(13).png') }}" alt=""/>
                                                        </h2>
                                                        {!! $home['content'] !!}
                                                        <p style="text-align: left;">
                                                            <a href="2926-2/mat-bang-chung-cu-premier-berriver"
                                                               rel="attachment wp-att-3675">
                                                                <img class="aligncenter size-full wp-image-3675"
                                                                     src="{!! $home['image'] !!}"
                                                                     alt="{{ $home['title'] }}"
                                                                     width="1200" height="700"
                                                                     sizes="(max-width: 1200px) 100vw, 1200px"/>
                                                            </a>
                                                        </p>
                                                        <h2 style="text-align: left;"></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endif
                                        @endforeach
                                        <div id="panel-2926-6-0-2"
                                             class="so-panel widget widget_sow-editor panel-last-child" data-index="12">
                                            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                <div class="siteorigin-widget-tinymce textwidget">
                                                    <p style="text-align: center;">
                                                        <div class="osc-res-tab tabbable   osc-tabs-left">
                                                            <div style="clear:both;width: 100%;">
                                                                <ul class="nav osc-res-nav nav-tabs osc-tabs-left-ul"
                                                                    id="oscitas-restabs-1-2926-2-38685">
                                                                    @foreach (\App\Entity\SubPost::showSubPost('can-ho', 30) as $id => $department)
                                                                    <li class="{{ ($id == 0) ? 'active' : ''}}">
                                                                        <a href="#ert_pane1-{{ $department['post_id'] }}" data-toggle="tab">
                                                                            {{ $department['title'] }}
                                                                        </a>
                                                                    </li>
                                                                     @endforeach
                                                                </ul>
                                                            </div>
                                                            <div style="clear:both;width: 100%;">
                                                                <ul class="tab-content"
                                                                    id="oscitas-restabcontent-1-2926-2-38685">
                                                                    @foreach (\App\Entity\SubPost::showSubPost('can-ho', 30) as $id => $department)
                                                                    <li class="tab-pane {{ ($id == 0) ? 'active' : ''}}" id="ert_pane1-{{ $department['post_id'] }}">
                                                                        <p style="text-align: center;">
                                                                            <a href="{{ $department['link'] }}"
                                                                               rel="attachment wp-att-3683">
                                                                                <img class="aligncenter size-full wp-image-3683"
                                                                                     src="{{ $department['image'] }}"
                                                                                     alt="{{ $department['title'] }}" width="900"
                                                                                     height="700"
                                                                                     sizes="(max-width: 900px) 100vw, 900px"/>
                                                                            </a>
                                                                        </p>
                                                                    </li>
                                                                 @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <p style="text-align: center;"></p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="pg-2926-7" class="panel-grid panel-no-style">
                            <div id="pgc-2926-7-0" class="panel-grid-cell">
                                <div id="panel-2926-7-0-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="13">
                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                        <div class="siteorigin-widget-tinymce textwidget">
                                            <p style="text-align: center;">
                                                <span style="color: #800000;"><strong>HÌNH ẢNH THIẾT KẾ NHÀ MẪU</strong></span>
                                            </p>
                                            <div class="osc-res-tab tabbable   osc-tabs-left">
                                                <div style="clear:both;width: 100%;">
                                                    <ul class="nav osc-res-nav nav-tabs osc-tabs-left-ul"
                                                        id="oscitas-restabs-2-2926-2-89464">
                                                        @foreach (\App\Entity\SubPost::showSubPost('hinh-anh-thiet-ke-nha-mau', 30) as $id => $imageDesign)
                                                        <li class="{{ $id == 0 ? 'active' : '' }}">
                                                            <a href="#ert_pane2-{{ $imageDesign['post_id'] }}" data-toggle="tab">{{ $imageDesign['title'] }}</a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div style="clear:both;width: 100%;">
                                                    <ul class="tab-content" id="oscitas-restabcontent-2-2926-2-89464">
                                                        @foreach (\App\Entity\SubPost::showSubPost('hinh-anh-thiet-ke-nha-mau', 30) as $id => $imageDesign)
                                                        <li class="tab-pane {{ $id == 0 ? 'active' : '' }}" id="ert_pane2-{{ $imageDesign['post_id'] }}">
                                                            <a href="{{ $imageDesign['link'] }}"
                                                               rel="attachment wp-att-3756">
                                                                <img class="aligncenter size-full wp-image-3756"
                                                                     src="{{ $imageDesign['image'] }}"
                                                                     alt="mat-bang-premier-berriver-long-bien-1366-11"
                                                                     width="800" height="500"
                                                                     sizes="(max-width: 800px) 100vw, 800px"/>
                                                            </a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pgc-2926-7-1" class="panel-grid-cell">
                                <div id="panel-2926-7-1-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="14">
                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                        <div class="siteorigin-widget-tinymce textwidget">
                                            <p style="text-align: center;">
                                                <span style="color: #800000;"><strong>TIẾN ĐỘ THI CÔNG DỰ ÁN</strong></span>
                                            </p>
                                            <div class="osc-res-tab tabbable   osc-tabs-left">
                                                <div style="clear:both;width: 100%;">
                                                    <ul class="nav osc-res-nav nav-tabs osc-tabs-left-ul"
                                                        id="oscitas-restabs-3-2926-2-98261">
                                                        @foreach (\App\Entity\SubPost::showSubPost('tien-do-thi-cong-du-an', 30) as $id => $excuteProject)
                                                        <li class="{{ $id == 0 ? 'active' : ''  }}">
                                                            <a href="#ert_pane3-{{ $excuteProject['post_id'] }}" data-toggle="tab">{{ $excuteProject['title'] }}</a>
                                                        </li>
                                                        @endforeach
                                                        
                                                    </ul>
                                                </div>
                                                <div style="clear:both;width: 100%;">
                                                    <ul class="tab-content" id="oscitas-restabcontent-3-2926-2-98261">
                                                        @foreach (\App\Entity\SubPost::showSubPost('tien-do-thi-cong-du-an', 30) as $id => $excuteProject)
                                                        <li class="tab-pane {{ $id == 0 ? 'active' : ''  }}" id="ert_pane3-{{ $excuteProject['post_id'] }}">
                                                            <a href="{{ $excuteProject['link'] }}"
                                                               rel="attachment wp-att-3767">
                                                                <img class="aligncenter size-full wp-image-3767"
                                                                     src="{{ $excuteProject['image'] }}"
                                                                     alt="{{ $excuteProject['title'] }}" width="800" height="500"
                                                                     sizes="(max-width: 800px) 100vw, 800px"/>
                                                            </a>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                            @if ($home['vi-tri'] == 7)
                        <div id="pg-2926-8" class="panel-grid panel-no-style">

                            <div id="pgc-2926-8-0" class="panel-grid-cell">
                                <div id="panel-2926-8-0-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="15">
                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                        <div class="siteorigin-widget-tinymce ">
                                            <h3 style="text-align: center;">
                                                <span style="color: #993300;">
                                                    {!! $home['title'] !!}
                                                </span>
                                            </h3>
                                            <h3 style="text-align: center;">
                                                <img class="size-full wp-image-3040 aligncenter"
                                                     src="{{ asset('bdslb/images/line-2.png') }}" alt="line-2" width="276" height="34"
                                                     sizes="(max-width: 276px) 100vw, 276px"/>
                                            </h3>
                                            {!! $home['content'] !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pgc-2926-8-1" class="panel-grid-cell">
                                <div id="panel-2926-8-1-0"
                                     class="so-panel widget widget_sow-editor panel-first-child panel-last-child"
                                     data-index="16">
                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                        <div class="siteorigin-widget-tinymce textwidget">
                                            <p>
                                                <img class="aligncenter size-full wp-image-3037"
                                                     src="{!! $home['image'] !!}"
                                                     alt="{!! $home['title'] !!}" width="500"
                                                     height="700"/>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                            @endif
                        @endforeach
                        <div id="pg-2926-9" class="panel-grid panel-no-style">
                            <div id="pgc-2926-9-0" class="panel-grid-cell">
                                @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                    @if ($home['vi-tri'] == 8)
                                <div id="panel-2926-9-0-0" class="so-panel widget widget_sow-editor panel-first-child"
                                     data-index="17">
                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                        <div class="siteorigin-widget-tinymce textwidget">
                                            <p style="text-align: center;">
                                                <span style="color: #3366ff;">
                                                    <strong>{{ $home['title'] }}</strong>
                                                </span>
                                                <a href="{{ $home['link'] }}" rel="attachment wp-att-3913">
                                                    <br/>
                                                </a>
                                            </p>
                                            <p style="text-align: center;">
                                                <a href="{{ $home['link'] }}" rel="attachment wp-att-3915"><img
                                                            class="aligncenter size-full wp-image-3915"
                                                            src="{{ asset('bdslb/images/anh-hoa-van.png') }}" alt="{{ $home['title'] }}" width="389"
                                                            height="25"/></a>
                                            </p>
                                            {!! $home['content'] !!}
                                        </div>
                                    </div>
                                </div>
                                    @endif
                                @endforeach
                                    @foreach (\App\Entity\SubPost::showSubPost('trang-chu') as $home)
                                        @if ($home['vi-tri'] == 9)
                                <div id="panel-2926-9-0-1" class="so-panel widget widget_sow-editor panel-last-child"
                                     data-index="18">

                                    <div class="panel-widget-style panel-widget-style-for-2926-9-0-1">
                                        <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                            <div class="siteorigin-widget-tinymce textwidget">
                                                <h4 style="text-align: center;">
                                                    <span style="vertical-align: inherit;">
                                                        {{ $home['title'] }}
                                                    </span>
                                                </h4>
                                                <h4 style="text-align: center;">
                                                    <span style="color: #ff0000; vertical-align: inherit;">
                                                        <strong>
                                                            <span style="vertical-align: inherit;">
                                                                {{ $home['description'] }}
                                                            </span>
                                                        </strong>
                                                    </span>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>

                                    {!! $home['content'] !!}
                                </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- AddThis Advanced Settings above via filter on the_content -->
    <!-- AddThis Advanced Settings below via filter on the_content -->
    <!-- AddThis Advanced Settings generic via filter on the_content -->
    <!-- AddThis Share Buttons above via filter on the_content -->
    <!-- AddThis Share Buttons below via filter on the_content -->
    <div class="at-below-post-homepage addthis_tool" data-url=""></div>
    <!-- AddThis Share Buttons generic via filter on the_content -->
    </div>
    <!--.post-content box mark-links -->
    </div>
    <!--.g post-->
    </div>
    </article>
    </div>
    <!--#page-->
@endsection