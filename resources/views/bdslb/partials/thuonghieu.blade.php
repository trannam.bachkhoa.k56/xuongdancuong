<div class="homepage-partners">
    @foreach(\App\Entity\SubPost::showSubPost('thuong-hieu',6) as $id => $img_thuonghieu)
    <div class="partner-item">
        <a href="#">
            <img src="{{ isset($img_thuonghieu['image']) ? asset($img_thuonghieu['image']) : '' }}" alt="{{ $img_thuonghieu['title'] }}">
        </a>
    </div>
    @endforeach
</div>