<div id="header-wrapper">
    <div id='header-top'>
        <div class='row'>
            <div class='menu-top section' id='menu-top'>
                <div class='widget LinkList' id='LinkList101'>
                    <div class='widget-content'>
                        <ul id='nav1'>
                            <li><a href='/'>Trang chủ</a></li>
                            <li><a href='/'>Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class='social-sec section' id='social-sec'>
            </div>
        </div>
    </div>
    <div class='clear'></div>
    <div class='row' id='header-content'>
        <div class='header section' id='header'>
            <div class='widget Header' id='Header1'>
                <div id='header-inner'>
                    <a href="/">
                        <img alt="title" src="{{ !empty($information['logo']) ?  asset($information['logo']) : asset('/site/img
						/no-image.png') }}" width="200px"/></a>
                </div>
            </div>
        </div>
        <div class='topad section' id='topad'>
            <div class='widget HTML' id='HTML22'>
                <div class='widget-content'>
                    <a href='javascript:void(0);'>

                        <img src="{{ !empty($information['bannertop']) ?  asset($information['bannertop']) : asset('/site/img
						/no-image.png') }}"/></a>
                </div>
                <div class='clear'></div>
                <span class='widget-item-control'>
            <span
                    class='item-control blog-admin'>

            </span>
                </span>
                <div class='clear'></div>
            </div>
        </div>
    </div>
    <div class='clear'></div>

    <div class='row'>
        <div class="vn3cMenu">
            <a class="toggleMenu" href="#">Menu</a>
            <ul class="nav">
                @foreach (\App\Entity\Category::getCategoryIndex() as $idCategoryIndex => $categoryIndex)
                        <li class="">
                            <a href="{{ route('site_category_post', ['cate_slug' => $categoryIndex->slug]) }}">
                                {{ $categoryIndex['title'] }}
                            </a>
                        </li>
                @endforeach
            </ul>
        </div>

    </div>
</div>                                                   