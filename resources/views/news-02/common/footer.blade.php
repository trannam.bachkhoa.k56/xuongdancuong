<div id="footer-wrapper">
    <div id="footer" class="row">
        <div id="column1" class="footer-column section">
            <div id="HTML17" class="widget HTML">
                <div class="widget-title">
                    <h2 class="title">Liên hệ</h2>
                </div>
                <div class="widget-content">
                    <ul class="post-widget">
                        <li>
                            <address>
                                <?= isset($information['lienhe']) ? $information['lienhe'] : '' ?>
                            </address>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="column2" class="footer-column section">
            <div id="HTML16" class="widget HTML">
                <div class="widget-title">
                    <h2 class="title">Danh mục</h2>
                </div>
                <div class="widget-content">
                    <ul class="post-widget">
                        @foreach (\App\Entity\Category::getCategoryIndex() as $idCategoryIndex => $categoryIndex)
                            <li>
                                <a href="{{ route('site_category_post', ['cate_slug' => $categoryIndex->slug]) }}">{{ $categoryIndex->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="column3" class="footer-column section mostView">
            <div id="HTML18" class="widget HTML">
                <div class="widget-title">
                    <h2 class="title">Tin tức mới nhất</h2>
                </div>
                <div class="widget-content">
                    <ul class="post-widget">
                        @foreach(\App\Entity\Post::newPostIndex(4) as $introduction)
                        <li>
                            <a style="background:url({{ !empty($introduction_random['image']) ?
                                             asset($introduction_random['image']) : '' }}) no-repeat center center;background-size: cover"
                               href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introduction->slug]) }}" class="rcp-thumb show-with"></a>
                            <div class="post-panel">
                                <h3 class="rcp-title"><a
                                            href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introduction->slug]) }}">
                                        {{ isset($information['title']) ? $information['title'] : '' }}</a></h3>
                                <span class="recent-date">{{ isset($information['updated_at']) ? $information['updated_at'] : '' }}</span>
                            </div>
                        </li>
                        @endforeach

                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <a href="tel:0908257968" id="call-mobile"><i
                class="fa fa-phone"></i></a>
</div>
<div id="copyrights">
    <a href="#top" class="upbt"><i
                class="fa fa-angle-up"></i></a>
    <div class="copyrights row">
        <div class="copy-left" data-runtime="Memory: 2 mb - Time: 0,22s" style="display: block">
            Thiết kế web bởi
            <a href="/"> {{ isset($information['tacgia']) ? $information['tacgia'] : '' }}</a>
        </div>
    </div>
</div>