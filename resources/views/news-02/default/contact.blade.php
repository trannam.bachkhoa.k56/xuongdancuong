@extends('news-02.layout.site')

@section('title','Liên hệ')
@section('meta_description', $information['meta_description'])
@section('keywords', $information['meta_keyword'])

@section('content')


	<div class="content-wrapper">
		<div class="formContact" style="max-width: 600px; margin: 0px auto;">
			<h1 style="font-size: 18px;text-align: center;padding: 10px 0;">
				Liên hệ
			</h1>
			<p>{{ isset($success) && $success == 1 ? "Cảm ơn bạn đã đóng góp ý kiến với chúng tôi." :  '' }}</p>
			<form id="fContact" action="{{ route('sub_contact') }}" method="post">
				{!! csrf_field() !!}
				<label>
					<span>Họ và tên</span>
					<input type="text" name="name" value="" required>
				</label>
				<label>
					<span>Email</span>
					<input type="email" name="email" value="" required>
				</label>
				<label>
					<span>Điện thoại</span>
					<input type="number" name="phone" value="" required>
				</label>
				<label>
					<span>Địa chỉ</span>
					<input type="text" name="address" value="" required>
				</label>
				<label>
					<span>Nội dung liên hệ</span><textarea name="Message" required></textarea></label>
				<p>
					<button type="submit">Gửi</button>
				</p>
			</form>
		</div>
		<style type="text/css">form#fContact{
				width: 600px; margin: 20px auto;
			}
			form#fContact label{
				display: block; margin: 12px 0;
			}
			form#fContact label span{
				float: left; width: 150px;
			}
			form#fContact label input{
				display: inline-block; width: 300px; height: 28px; border: 1px solid #ccc;
				padding: 0 5px; box-sizing: border-box;
			}
			form#fContact label textarea{
				display: inline-block; width: 400px; height: 150px; resize: none; box-sizing: border-box;
				border: 1px solid #ccc; padding: 5px;
			}
			form#fContact button[type="submit"]{
				display: inline-block; padding: 5px 40px; border: none; background-color: #19749B; color: #fff;
				margin-left: 150px;
			}
			form#fContact #showErrorContact{
				border: 1px dashed #F04E23; margin-bottom: 20px; color: #F04E23; padding: 2px 5px; text-align: center;
			}
			form#fContact #showSuccessContact{
				border: 1px dashed #008000; margin-bottom: 20px; color: #008000; padding: 2px 5px; text-align: center;
			}
		</style>
	</div>


@endsection
