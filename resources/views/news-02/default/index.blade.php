@extends('news-02.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    <div class="content-wrapper">
        <div id="content-wrapper" class="row">
            <div id="ticker" class="ticker section">
                <div id="HTML20" class="widget HTML">
                    <h2 class="title">
                        <i class="fa fa-thumb-tack"></i>Tin tức</h2>
                    <div class="layout-content">
                        <div class="tickercontainer">
                            @include('news-02.partials.title_sologan')
                        </div>
                    </div>
                    <div class="clear"></div>
                    <span class="widget-item-control">
                         <span class="item-control blog-admin">
                         <a title="Edit" target="configHTML20" href="/" class="quickedit">
                             <img width="18" height="18" src="{{ !empty($information['anhtrai']) ?  asset($information['anhtrai']) : '' }}" alt=""></a>
                         </span>
                    </span>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="intro" class="intro">
                <div id="intro-sec" class="intro-sec section">
                    <div id="HTML2" class="widget HTML">
                        <div class="layout-content">
                            <ul>
                                @foreach(\App\Entity\SubPost::showSubPost('anhsl',3) as $id => $img)
                                    <li>
                                        <a style="background:url('{{ isset($img->image) ? asset($img->image) : '' }}') no-repeat center center;background-size:
                                                        cover" href="{{ $img['link']}}" class="rcp-thumb show-with"></a>
                                        <div class="post-panel">
                                            <h3 class="rcp-title">
                                                <a href="{{ $img['link']}}">{{ $img->title }}</a>
                                            </h3>
                                        </div>
                                    </li>
                                @endforeach
                                <div class="clear"></div>
                            </ul>
                        </div>
                        <div class="clear"></div>
                        <span class="widget-item-control">
                            <span class="item-control blog-admin">

                            </span>
                        </span>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <h1 class="hidden">T0073</h1>
            <div id="main-wrapper">
                <div id="recent-layout" class="recent-layout">
                    <div id="recent-sec1" class="recent-sec section">
                        <div id="HTML8" class="widget HTML list fbig">
                            <div class="box-title" style="border-color: rgb(78, 114, 154);">
                                <h2 class="title" style="background: none repeat scroll 0% 0% rgb(78, 114, 154);">
                                    <a>Tin mới</a>
                                </h2>
                            </div>
                            <div class="layout-content">
                                <ul>
                                    @foreach(\App\Entity\Post::newPostIndex(10) as $introduction)
                                        <li>
                                            <div class="rthumbc">
                                                <a style="background:url({{ !empty($introduction['image']) ?
                                             asset($introduction['image']) : '' }}) no-repeat center center;background-size: cover"
                                                   href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introduction->slug]) }}"
                                                   class="recent-thumb show-with">
                                                </a>
                                            </div>
                                            <div class="recent-content">
                                                <h3 class="recent-title">
                                                    <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introduction->slug]) }}">{{ $introduction['title'] }}</a>
                                                </h3>
                                                <?php $date=date_create($introduction['updated_at']);?>
                                                <span class="recent-date">{{ date_format($date,"d/m/Y H:i") }}</span>
                                            </div>
                                            <div class="clear"></div>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                       
                         <div class="clear "></div>
                        @foreach (\App\Entity\Category::getCategoryIndex() as $idCategoryIndex => $categoryIndex)
                                <div id="HTML3" class="widget block HTML fbig1 fbig">
                                    <div class="box-title bodyTitle<?php echo $categoryIndex->category_id ?>">
                                        <h2 class="title bgTitle<?php echo $categoryIndex->category_id ?>">
                                            <a
                                                    href="{{ route('site_category_post', ['cate_slug' => $categoryIndex->slug]) }}">{{ $categoryIndex['title'] }} </a>
                                        </h2>
                                        <a href="{{ route('site_category_post', ['cate_slug' => $categoryIndex->slug]) }}" class="more-link">Xem thêm</a>
                                    </div>
                                    <div class="layout-content">
                                        <ul>
                                            @foreach(\App\Entity\Post::categoryShow($categoryIndex->slug, 5) as $id => $introductionCategory)
                                                @if ($id == 0 )
                                                    <div class="blockMain first">
                                                        <div class="rthumbc">
                                                            <a style="background:url({{ !empty($introductionCategory->image) ? asset($introductionCategory->image) : '' }}) no-repeat center center;
                                                                    background-size: cover" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introductionCategory->slug]) }}" class="first-thumb show-with"></a>
                                                        </div>
                                                        <div class="first-content">
                                                            <h3 class="recent-title"><a href="
                                                            {{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introductionCategory->slug]) }}">
                                                                    {{ $introductionCategory['title'] }}</a>
                                                            </h3>
                                                            <?php $date=date_create($introductionCategory['updated_at']);?>
                                                            <span class="recent-date">{{ date_format($date,"d/m/Y H:i")  }}</span>
                                                            <p class="recent-des"></p>
                                                            <p></p>
                                                            <div class="post-readmore"><a class="bgTitle6" href="
                                                           {{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introductionCategory->slug]) }}">Đọc
                                                                    thêm<i class="fa fa-long-arrow-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <li>
                                                        <div class="rthumbc">
                                                            <a style="background:url({{ !empty($introductionCategory->image) ?
                                                             asset($introductionCategory->image) : '' }})
                                                                    no-repeat center center;background-size: cover"
                                                               href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introductionCategory->slug]) }}
                                                                       " class="recent-thumb show-with"></a>
                                                        </div>

                                                        <div class="recent-content">
                                                            <h3 class="recent-title"><a
                                                                        href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introductionCategory->slug]) }}">
                                                                    {{ $introductionCategory['title'] }}</a>
                                                            </h3>
                                                            <span class="recent-date">{{ $introductionCategory['updated_at'] }}</span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </li>

                                                @endif
                                            @endforeach

                                        </ul>
                                    </div>

                                    <div class="clear"></div>
                                    <span class="widget-item-control">
                                       <span class="item-control blog-admin">
                                       </span>
                                     </span>
                                    <div class="clear"></div>
                                </div>
                        @endforeach



                    </div>
                </div>
            </div>
            <!-- BLOCK -->
            @include('news-02.partials.sidebar')
        </div>
        <div class="clear"></div>
@endsection