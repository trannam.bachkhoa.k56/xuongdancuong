@extends('news-02.layout.site')

@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', asset($product->image) )
@section('meta_url', route('product', [ 'post_slug' => $product->slug]) )

@section('content')

@endsection
