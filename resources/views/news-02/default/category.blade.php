@extends('news-02.layout.site')

@section('title', $category->title)
@section('meta_description',  $category->description )
@section('keywords', '')

@section('content')
    <div class="content-wrapper">
        <div id="content-wrapper" class="row">
            <div id="ticker" class="ticker section">
                <div id="HTML20" class="widget HTML">
                    <h2 class="title">
                        <i class="fa fa-thumb-tack"></i>Tin tức</h2>
                    <div class="layout-content">
                        <div class="tickercontainer">
                            @include('news-02.partials.title_sologan')
                        </div>
                    </div>
                    <div class="clear"></div>
                        <span class="widget-item-control">
                            <span class="item-control blog-admin">
                             </span>
                         </span>
                    <div class="clear"></div>
                </div>
            </div>
            <h2 class="hidden" style="text-transform: uppercase;">{{ $category->title }}</h2>
            <div id="main-wrapper">
                <div id="main" class="main section">
                    <div id="Blog1" class="widget Blog">
                        <div class="blog-posts hfeed">
                            @foreach($posts as $post)
                            <div class="post-outer post-category">
                                <div class="post">
                                    <div class="post-thumb">
                                        <a style="background:url({{ isset($post->image) ? asset($post->image) : "" }})
                                                 no-repeat center center;background-size:cover" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">
                                        </a>
                                    </div>
                                    <div class="post-header"></div>
                                <article>
                                        <div class="post-home" style="margin-top: 10px;">
                                            <div class="post-info">
                                                <h2 class="post-title">
                                                    <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">
                                                        {{$post->title}}
                                                    </a>
                                                </h2>
                                                <div class="post-meta">
                                                   
                                                    <span class="post-timestamp">
                                                        <i class="fa fa-clock-o"></i>
                                                        <?php $date=date_create($post->updated_at);?>
                                                        <a href="" class="timestamp-link"><abbr class="published timeago">{{ date_format($date,"d/m/Y") }}</abbr></a>
                                                    </span>
                                                </div>
                                                <div class="post-snippet"></div>
                                                <div id="post-foot">
                                                    <div class="post-readmore">
                                                        <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">
                                                            Đọc thêm<i class="fa fa-long-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <div class="post-footer"></div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        <div class="pagenavi">
                            <div class="pagging">
                                {{ $posts->links() }}
                            </div>
                        </div>
                    </div>
                    <div id="HTML901" class="widget HTML"></div>
                    <div id="HTML902" class="widget HTML"></div>
                </div>
            </div>
            <!-- BLOCK -->
            @include('news-02.partials.sidebar')
        </div>
    </div>

@endsection

