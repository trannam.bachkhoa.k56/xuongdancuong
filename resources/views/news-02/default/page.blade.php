@extends('news-02.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )

@section('content')
    <div class="content-wrapper">
        <div id="content-wrapper" class="row">
            <div id="ticker" class="ticker section">
                <div id="HTML20" class="widget HTML">
                    <h2 class="title">
                        <i class="fa fa-thumb-tack"></i>Tin tức</h2>
                    <div class="layout-content">
                        <div class="tickercontainer">
                            @include('news-02.partials.title_sologan')
                        </div>
                    </div>
                    <div class="clear"></div>
                        <span class="widget-item-control">
                            <span class="item-control blog-admin">
                            {{--<a--}}
                                {{--title="Edit" target="configHTML20" href="/" class="quickedit"><img--}}
                                {{--width="18" height="18" src="/tp/T0073/images/icon18.png" alt=""></a>--}}
                            </span>
                        </span>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="main-wrapper">
                <div id="main" class="main section">
                    <div id="HTML900" class="widget HTML">
                        <div class="ad-inside">
                            <a href="">
                                <img src="{{ !empty($information['bannertop']) ?  asset($information['bannertop']) : asset('/site/img/no-image.png') }}">
                            </a>
                        </div>
                    </div>
                    <div id="Blog1" class="widget Blog">
                        <div class="blog-posts hfeed">
                            <div class="post-outer">
                                <div class="post">
                                    <div>
                                        <div class="post-header">
                                            <div class="breadcrumbs">
                                                <span>
                                                     <a href="/" class="bhome">Trang chủ</a>
                                                     <i class="fa fa-angle-right"></i>
                                                <span>
                                            </div>
                                            <div class="post-heading">
                                                <h1 class="post-title entry-title news-title">
                                                    {{$post->title}}
                                                </h1>
                                            </div>
                                            <div class="post-meta">
                                                <span class="post-timestamp">
                                                    <i class="fa fa-clock-o"></i>
                                                    <a title="permanent link" rel="bookmark" href="/" class="timestamp-link">
                                                    <abbr title=""  class="published timeago">{{ $post->updated_at }}</abbr>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                        <article>
                                            <div id="post-body-1260884640087456780"  class="post-body entry-content">
                                                <div style="text-align: left;">
                                                    <style>#post-body-1260884640087456780 img{
                                                            max-width: 100% !important;
                                                        }
                                                    </style>
                                                    {{ isset($post->description) ? $post->description : "" }}
                                                </div>
                                            </div>
                                        </article>
                                        <br/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- BLOCK -->
        @include('news-02.partials.sidebar')
        </div>
    </div>

@endsection

