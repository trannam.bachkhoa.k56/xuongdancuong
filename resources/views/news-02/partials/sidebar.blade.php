<div id="sidebar-wrapper">
    <div id="sidebar" class="sidebar section">
        <div id="HTML10" class="widget HTML">
            <div class="widget-title">
                <h2 class="title">Facebook Fanpage</h2>
            </div>
            <div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0';  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-page" data-href="{!! isset($information['fanpage-facebook']) ? $information['fanpage-facebook'] : '' !!}" data-tabs="timeline" data-height="320" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="{!! isset($information['fanpage-facebook']) ? $information['fanpage-facebook'] : '' !!}" class="fb-xfbml-parse-ignore"><a href="{!! isset($information['fanpage-facebook']) ? $information['fanpage-facebook'] : '' !!}"></a></blockquote></div>

            <div class="clear"></div>
        </div>
    </div>
    <div id="sidebar2" class="sidebar section">
        <div id="FollowByEmail1" class="widget FollowByEmail">
            <div class="widget-title">
                <h2 class="title">Theo dõi chúng tôi</h2>
            </div>
            <div class="widget-content">
                <div>Đăng ý Email của bạn để trở thành khách hàng đầu tiên được nhận thông tin mới nhất từ chúng tôi</div>
                <div class="follow-by-email-inner">
                    <div>
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td>
                                    <input type="text" placeholder="Địa chỉ email của bạn..." name="email" class="follow-by-email-address">
                                </td>
                                <td width="64px">
                                    <input type="button" value="Theo dõi" class="follow-by-email-submit">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="Bmag-MagazineResponsiveBloggerTheme" name="uri">
                        <input type="hidden" value="en_US" name="loc">
                    </div>
                </div>
            </div>
            <span class="item-control blog-admin">
                              <div
                                      class="clear"></div>
                              <span
                                      class="widget-item-control">
                              <span
                                      class="item-control blog-admin">
                              <a
                                      title="Edit" target="configFollowByEmail1" href="/" class="quickedit"></a>
                              </span>
            </span>
            <div class="clear"></div>
            </span>
        </div>

        <div id="Label1" class="widget Label">
            <div class="widget-title">
                <h2>Danh mục tin tức</h2>
            </div>
            <div class="widget-content list-label-widget-content">
                <ul>
                    @foreach (\App\Entity\Category::getCategoryIndex() as $idCategoryIndex => $categoryIndex)
                        <li>
                            <a href='{{ route('site_category_post', ['cate_slug' => $categoryIndex->slug]) }}'>{{ $categoryIndex['title'] }}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="clear"></div>
                <span class="widget-item-control">
                              <span
                                      class="item-control blog-admin">
                              <a
                                      title="Edit" target="configLabel1" class="quickedit"><img
                                          width="18" height="18"
                                          src="/tp/T0073/images/icon18.png" alt=""></a>
                              </span>
                </span>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>