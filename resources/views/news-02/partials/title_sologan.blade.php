<div
        class="mask">
    <ul
            class="newsticker">
        @foreach (\App\Entity\Menu::showWithLocation('show-category-index') as $menu_category)
            @foreach (\App\Entity\MenuElement::showMenuPageArray($menu_category->slug) as $id_cate=>$show_menu)
                <li>

                    <a class="post-tag" href="{{ $show_menu['url'] }}">{{ $show_menu['title_show'] }}</a>
                    @foreach(\App\Entity\Post::categoryShow(App\Ultility\Ultility::createSlug($show_menu['title_show']),5) as $id => $introductionCategory)
                        @if ($id == 0)
                            <h3 class="recent-title"><a
                                        href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $introductionCategory->slug]) }}">
                                    {{ $introductionCategory['title'] }}</a></h3>
                        @endif
                    @endforeach
                </li>
            @endforeach
        @endforeach


    </ul>
    <span
            class="tickeroverlay-left">&nbsp;</span><span
            class="tickeroverlay-right">&nbsp;</span>
</div>