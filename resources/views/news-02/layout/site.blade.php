<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <!-- meta -->
    <meta name="ROBOTS" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://anhoa.edu.vn/xmlrpc.php">
    <meta name="format-detection" content="telephone=no">

    <link sizes="16x16" href="{{ isset($information['favicon']) ? asset($information['favicon']) : '' }}" rel="icon" />

    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />


    <link rel="stylesheet" href="{{ asset('news-02/css/tintuc.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('news-02/css/font-awesome.min.css') }}" media="all" type="text/css" />
    {{--<link rel="stylesheet" href="{{ asset('news-02/css/bootstrap.min.css') }}" media="all" type="text/css" />--}}
    <link rel="stylesheet" href="{{ asset('news-02/css/style.css') }}" media="all" type="text/css" />

    <script src="{{ asset('news-02/js/tintuc.js') }}"></script>
    <script src="{{ asset('news-02/js/main.js') }}"></script>
    <script src="{{ asset('news-02/js/jquery.min.js') }}"></script>
    <script src="{{ asset('news-02/js/menu.js') }}"></script>
    {{--<link rel="stylesheet" href="{{ asset('news-02/css/bootstrap.min.css') }}" media="all" type="text/css" />--}}


</head>
<body>
<div id="pages-wrapper" class="index home">
    <div id="top"></div>
    <div id="outer-wrapper">
        <!-- HEADER -->
        @include('news-02.common.header')
        <h1 class="hidden" style="text-indent: -9999px;">@yield('title')</h1>
     
        @yield('content')
        <div class="clear"></div>
        <!-- FOOTER -->
        @include('news-02.common.footer')
    </div>
    <input type="hidden" id="checkStoreId" value="2558">
</div>

<div id="fb-root"></div>

<div style="display: none;">
    <div id="dMsg"></div>
</div>
</body>
</html>
