@extends('xhome.layout.site')

@section('title','Gửi đơn hàng thành công')

@section('content')
    {{--@include('site.partials.menu_main', ['classHome' => ''])--}}
    <section class="order">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p>Bạn đã đặt hàng thành công !</p>
                </div>
            </div>
        </div>
    </section>
@endsection
