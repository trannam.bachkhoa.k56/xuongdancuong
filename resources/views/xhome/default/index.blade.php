@extends('xhome.layout.site')
@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'home')
@section('dynx_totalvalue', '0')
@section('content')

    @include('xhome.partials.slider')
	
	@php 
		if (!empty(\App\Entity\Category::getDetailCategory('san-pham-trang-chu'))) {
			$productIndexs = \App\Entity\Product::showProduct('san-pham-trang-chu', 20);
		} else {
			$productIndexs = \App\Entity\Product::newProduct(20);
		}
	
	@endphp
	@if (!$productIndexs->isEmpty() )
    <section class="content pdt20">
        <div class="container">
            <div class="title">
                <h2 class="colortl titl"><?= isset($information['tieu-de-san-pham-hoac-dich-vu']) ? $information['tieu-de-san-pham-hoac-dich-vu'] : 'Top sản phẩm hoặc dịch vụ' ?></h2>
            </div>
            <div class="row">
				
                @foreach($productIndexs as $hotProduct)
                        <div class="col-6 col-lg-3 pd10 itemProduct">
                            <div class="imgItem relative">
                                <div class="boxDetails">
                                    <div class="borderBoxDetails hvr-shutter-in-vertical">
                                    </div>
                                </div>
                                <div class="CropImg">
                                    <a href="{!! route('product', ['slug' => $hotProduct->slug]) !!}" class="noDecoration thumbs">
                                        <img src="{{ isset($hotProduct['image']) ?  $hotProduct['image'] : '' }}"
                                             alt="{{ isset($hotProduct['title']) ?  $hotProduct['title'] : '' }}">
                                    </a>
                                </div>
                            </div>
                            <div class="infoItem boxP">
                                <a href="{!! route('product', ['slug' => $hotProduct->slug]) !!}" class="noDecoration"><h3>{{ isset($hotProduct['title']) ?  $hotProduct['title'] : '' }}</h3></a>
                                <p class="star">
                                    <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star"
                                                                                    aria-hidden="true"></i><i
                                            class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star"
                                                                                         aria-hidden="true"></i><i
                                            class="fa fa-star" aria-hidden="true"></i>
                                </p>
                                <p class="price red">
                                    @if(empty($hotProduct->price))
                                        <span>liên hệ</span>
									@elseif ($hotProduct->price == 1)
										<span>Miễn phí</span>
                                    @elseif($hotProduct->discount > 0)
                                        <span><del>{{ number_format($hotProduct->price) }} đ</del></span> - <span>{{ number_format($hotProduct->discount) }} đ</span>
                                    @else
                                        <span>{{ number_format($hotProduct->price) }} đ</span>
                                    @endif
                                </p>
                                <p class="comment black">
                                    @if (!empty($hotProduct->views) )
                                        <i class="fa fa-eye" aria-hidden="true"></i> <span>{{ $hotProduct->views }} Lượt xem </span>
                                    @endif
                                    @if (!empty(\App\Entity\Comment::getCountComment($hotProduct->post_id)) )
                                        <i class="fa fa-commenting-o" aria-hidden="true"></i> <span>{{ \App\Entity\Comment::getCountComment($hotProduct->post_id) }} Bình luận</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </a>
                @endforeach
				<script async defer>
					$('.boxP').matchHeight();
				</script>
            </div>
        </div>

    </section>
	@endif
	
    <div class="v2_bnc_intro_company col-xs-12 no-padding mgt30 pdt20 pdb30">
        <div class="container">
            <div class="title">
                <h1 class="colortl titl">{{ isset($information['tieu-de-trang']) ? $information['tieu-de-trang']  : '' }}</h1>
            </div>
            <div class="row" >
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div id="parentScroller">
                        <?= isset($information['sologan']) ? $information['sologan'] : '' ?>

                    </div>
                </div>
				<div class="col-md-6 col-sm-12 col-xs-12" >
						<div id="scroller">
							<form onSubmit="return contact(this);" class="wpcf7-form" method="post"
								  action="/submit/contact" >
								{!! csrf_field() !!}
								<input type="hidden" name="is_json"
									   class="form-control captcha" value="1" placeholder="">

								<div class="form-group">
									<div>
										<input type="text" class="form-control" placeholder="Tên của bạn" name="name" required="">
									</div>
								</div>
								
								<div class="form-group">
									<div>
										<input type="email" class="form-control" placeholder="Email của bạn" name="email">
									</div>
								</div>

								<div class="form-group">
									<div>
										<input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone" required="">
									</div>
								</div>
                                
								<div class="form-group">
									<div>
									<textarea class="form-control" rows="2" name="message"
											  placeholder="Nội dung ghi chú"></textarea>
									</div>
								</div>
								@if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']))
									<input type="hidden" value="{{ $_SERVER['HTTP_REFERER'] }}" name="utm_source" />
								@endif

								@if(!isset($customerDisplay->buttonName) || $customerDisplay->buttonName != null)
								<div class="form-group">
									<div class="center">
										<button type="submit" class="btn  btn-warning">{{ isset($customerDisplay->buttonName) ? $customerDisplay->buttonName : "Đăng ký tư vấn" }}</button>
									</div>
								</div>
								@endif
							</form>
						</div>
					</div>
				
           
            </div>
        </div>
    </div>
    <script async defer>
        $(document).ready(function(){
            $(window).scroll(function () {
                var w = window.innerWidth;
                if (w > 767 && $("#parentScroller").height() > 400) {
                    $("#scroller").stick_in_parent({offset_top: 175});
                }
            });

        });
    </script>
    @include('xhome.partials.slideCustomer')
	@include('xhome.partials.popup')

@endsection
