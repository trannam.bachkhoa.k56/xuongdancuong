@extends('xhome.layout.site')
@section('title', isset($category->title) ? $category->title : '')
@section('meta_description',  isset($category->description) ? $category->description : '' )
@section('keywords', '')

@section('dynx_itemid', '')
@section('dynx_pagetype', 'searchresults')
@section('dynx_totalvalue', '0')

@section('content')
    <section class="moreProduct bgrGray">
        <div class="container">
            <div class="link">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="#">{{$category->title}}</a></li>
              </ol>
            </div>
            <div class="title">
                <h2 class="gray titl">{{$category->title}}</h2>
            </div>
			{!! $category->description !!}
				@if(empty($products))
					<p>Không tồn tại danh mục này</p>
				@else
					<div class=" row">
                    @foreach($products as $product) 
                            <div class="item pd10 col-md-3 col-sm-4 col-xs-12">
                              <div class="imgItem">
                                  <div class="CropImg">
                                      <a href="/{{ $product->slug }}" class="noDecoration thumbs">
                                          <img src="{{ isset($product['image']) ?  $product['image'] : '' }}"
                                               alt="{{ isset($product['title']) ?  $product['title'] : '' }}">
                                      </a>
                                  </div>
                              </div>
                              <div class="infoItem bgrWhite boxP">
                                  <a href="/{{ $product->slug }}" class="noDecoration"><h3>{{$product->title}}</h3></a>
                                  <p class="star">
                                    <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
                                  </p>
                                  <p class="price red">
                                      @if(empty($product->price))
                                          <span>liên hệ</span>
									  @elseif ($product->price == 1)
										<span>Miễn phí</span>
                                      @elseif($product->discount > 0)
                                          <span><del>{{ number_format($product->price) }}</del> đ</span> - <span>{{ number_format($product->discount) }} đ</span>
                                      @else
                                          <span>{{ number_format($product->price) }} đ</span>
                                      @endif
                                  </p>
                                 
                              </div>
                            </div>
                    @endforeach
					</div>
					{{ $products->links() }}
				@endif
				
				
				<script async defer>
					$('.boxP').matchHeight();
				</script>
            </div>
        </div>      
    </section>
    @include('xhome.partials.slideCustomer')
@endsection
