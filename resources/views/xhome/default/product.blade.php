@extends('xhome.layout.site')
@section('title', isset($product->title) ? $product->title : '' )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', !empty($product->image) ? asset($product->image) : '')

@section('dynx_itemid',  $product->product_id )
@section('dynx_pagetype', 'offerdetail')
@php
$totalValue = 0;
if ( !empty($product->price_deal)
&& !empty($product->discount_end)
&& ( time() < strtotime($product->discount_end))
&& !empty($product->discount_start) && (time() > strtotime($product ->discount_start))) {
$totalValue = $product->price_deal;
}
elseif (!empty($product->discount)) {
$totalValue =  $product->discount; }
else
$totalValue = $product->price;
@endphp
@section('dynx_totalvalue', $totalValue)

@section('content')
<section class="moreProduct bgrGray pdb20">
   <div class="container">
      <div class="link">
         <ol class="breadcrumb" >
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
            <?php $showCategory = false; ?>  
            @foreach ($categories as $id => $category) 
            @if ($category->title != 'Sản phẩm khuyến mãi' && !$showCategory)
            <?php $showCategory = true; ?>  
            <li class="breadcrumb-item">
               <a class="" href="{{ route('site_category_product', [ 'cate_slug' => $category->slug]) }}">{{ $category->title }}</a>
            </li>
            @endif    
            @endforeach
            <li class="breadcrumb-item"><a href="">{{$product->title}}</a></li>
         </ol>
      </div>
     
      <div class="infoProduct bgrWhite">
         <div class="row">
            <div class="col-md-6">
               <div class="imagesProduct">
                  @if (!empty($product->image_list))
                     @foreach (explode(',', $product->image_list) as $imageProduct)

                     <div class="pd10">
                        <div class="CropImg">
                           <a class="thumbs">
                           <img src="{{$imageProduct}}" alt="" width="100%" >
                        </a>
                        </div>
                     </div>
                     @endforeach
                  @else
                        <div class="pd10">
                           <div class="CropImg">
                              <a class="thumbs">
                                 <img src="{{ $product->image }}" alt="" width="100%" >
                              </a>
                           </div>
                        </div>
                  @endif
               </div>
               <div class="imagesProducts">
                  @if (!empty($product->image_list))
                  @foreach (explode(',', $product->image_list) as $imageProduct)
                  <div class="pd10">
                     <div class="CropImg">
                        <a class="thumbs">
                        <img src="{{ $imageProduct }}" alt="" width="100%" >
                     </a>
                     </div>
                  </div>
                     @endforeach
                  @else
                        <div class="pd10">
                           <div class="CropImg">
                              <a class="thumbs">
                                 <img src="{{$product->image }}" alt="" width="100%" >
                              </a>
                           </div>
                        </div>
                  @endif
               </div>
               <?php 
                  $url =  (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                ?>
               
			  
			   <div id="loadShare">
				</div>
				<script>
					$(window).load(function() { 
						$("#loadShare").html('<iframe src="https://www.facebook.com/plugins/share_button.php?href={!! $domainUrl.'/'.$product->slug !!}&layout=button_count&size=large&appId=1875296572729968&width=138&height=28" width="138" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>');
					});
				 </script>
							 
               <script src="https://sp.zalo.me/plugins/sdk.js"></script>
               <div class="zalo-share-button" data-href="{{$url}}" data-oaid="579745863508352884" data-layout="1" data-color="blue" style="padding-top: 7px; margin-top: 15px;; margin-left: 10px; margin-right: 10px;" data-customize=false></div>
                <input style="display: none;" type="text" class="form-control" value="{{$url}}" id="myCoppy" placeholder="" aria-describedby="basic-addon1" disabled="">
                <span style="cursor: pointer;" class="input-group-addon badge badge-primary" id="basic-addon1" onclick="copyCode('#myCoppy')">Copy link</span>

             <script async defer>
               function copyCode(element) {
                  var $temp = $("<input>");
                  $("body").append($temp);
                  $temp.val($(element).val()).select();
                  document.execCommand("copy");
                  $temp.remove();
                  alert("Sao chép liên kết thành công: " + $(element).val());
               }
             </script>

            </div>
            <div class="col-md-6">
               <div class="infomartionProduct">
                  <div class="titleProduct">
                     <h1>{{$product->title}}</h1>
                  </div>
                  <p class="mgb5">@if (!empty($product->code)) Mã : {{ $product->code }} @endif</p>
                  <p class="star">
                     <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i> <span class="black">| <i class="fa fa-eye" aria-hidden="true"></i> <span>{{ $product->views }} Lượt xem </span> </span>
                  </p>
                  @if($product->discount > 0 && $product->discount < $product->price)
                     @include('general.clock_countdown')
                  @endif
                  <p class="price red left pd10 mgb0 mgr30 f25" style="width: 100%; border: 1px solid #e5e5e5; background: #fafafa;">
                     @if(empty($product->price))
                        <span>liên hệ</span>
                     @elseif($product->discount > 0 && $product->discount < $product->price)
                        <span><del>₫{{ number_format($product->price) }}</del></span> - <span style="color:#dc3545">₫{{ number_format($product->discount) }}</span>
                        <span class="f14 mgl10" style="background: #d0011b; border: 1px solid #e5e5e5; color:white;">{{ round(($product->price-$product->discount)/$product->price * 100) }}% GIẢM</span>
                     @else
                        <span>₫{{ number_format($product->price) }} đ</span>
                     @endif
                  </p>
				  <div class="exCept">
					{!! nl2br($product->description) !!}
				  </div>
                  <div class="describe">
                     <form onsubmit="return contact(this);" method="post" id="add-to-cart-form" class="formOrder"
                        enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input type="hidden" name="is_json"
                               class="form-control captcha" value="1" placeholder="">
                        <input type="hidden" name="post_id" value="{{$product->post_id}}">
                        @if(empty($product->price))
                        <input type="hidden" value="0" class="form-control" name="price">
                        @elseif($product->discount > 0)
                          <input type="hidden" value="{{$product->discount}}" class="form-control" name="price">
                        @else
                           <input type="hidden" value="{{$product->price}}" class="form-control" name="price">
                        @endif

                        <div class="form-group">
                           <input type="text" class="form-control" name="name" placeholder="Họ và tên (*)" autocomplete="cc-name" required="">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="email" placeholder="Email (*)" autocomplete="email">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="phone" autocomplete="tel" placeholder="Số điện thoại (*)" required="">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="address" placeholder="Địa chỉ (*)" required="">
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" name="message" placeholder="Ghi chú"></textarea>
                        </div>
						      <input type="hidden" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}/{{ $product->slug }}" name="utm_source" />
                        <div>
                           <button class="btn btn-danger" style="width: 100%;" title="Đặt hàng ngay" type="submit">
								Đặt Ngay
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="container pdt20 pdb20">
            <div class="row">
               <div class="col-xs-12 col-md-8">
                  <div class="f-product-view-tab-body tab-content">
                     <div id="f-pr-view-01" class="tab-content tab-pane active contentProduct">
                        {!!$product->content!!}
						      <p class="mgb0 f24 fw6">Bình luận</p>
                        @include('general.sub_comments', ['post_id' =>$product->post_id])
                     </div>
                  </div>
               </div>
               <div class="col-xs-12 col-md-4">
                  <div class="infomartionProduct sidebarProduct" id="scroller"  style="border: 1px solid lightgray;">
                  <div class="titleProduct">
                     <h3>{{$product->title}}</h3>
                  </div>
                  <p class="mgb5">@if (!empty($product->code)) Mã : {{ $product->code }} @endif</p>
                  
                  <p class="price red left pd10 mgb0 mgr30 f20" style="width: 100%; border: 1px solid #e5e5e5; background: white;">
                     @if(empty($product->price))
                        <span>liên hệ</span>
                     @elseif($product->discount > 0 && $product->discount < $product->price)
                        <span><del>₫{{ number_format($product->price) }}</del></span> - <span style="color:#dc3545">₫{{ number_format($product->discount) }}</span>
                        <span class="f14 mgl10" style="background: #d0011b; border: 1px solid #e5e5e5; color:white;">{{ round(($product->price-$product->discount)/$product->price * 100)  }}% GIẢM</span>
                     @else
                        <span>₫{{ number_format($product->price) }} đ</span>
                     @endif
                  </p>
				  <div class="exCept">
					{!! nl2br($product->description) !!}
				  </div>
                  <div class="describe">
                     <form  onsubmit="return contact(this);" method="post" id="add-to-cart-form" class="formOrder noBorder pd0"
                        enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="is_json" class="form-control captcha" value="1" placeholder="">
                        <input type="hidden" name="post_id" value="{{$product->post_id}}">
                        @if(empty($product->price))
                        <input type="hidden" value="0" class="form-control" name="price">
                        @elseif($product->discount > 0 && $product->discount < $product->price)
                          <input type="hidden" value="{{$product->discount}}" class="form-control" name="price">
                        @else
                           <input type="hidden" value="{{$product->price}}" class="form-control" name="price">
                        @endif

                        <div class="form-group">
                           <input type="text" class="form-control" name="name" placeholder="Họ và tên (*)" autocomplete="cc-name" required="">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="email" placeholder="Email (*)" autocomplete="email" required="">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="phone" autocomplete="tel" placeholder="Số điện thoại (*)" required="">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" name="address" placeholder="Địa chỉ (*)" required="">
                        </div>
                        <div class="form-group">
                           <textarea class="form-control" name="message" placeholder="Ghi chú"></textarea>
                        </div>
						      <input type="hidden" value="{{ \App\Ultility\Ultility::getCurrentDomain() }}/{{ $product->slug }}" name="utm_source" />
                        <div>
                           <button class="btn btn-danger" style="width: 100%;" title="Đặt hàng ngay" type="submit">
								Đặt Ngay
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<style>
	.exCept {
		border: 1px solid red;
		padding: 9px;
		margin-top: 15px;
	}
</style>
@include('xhome.partials.similarProduct')

@include('xhome.partials.productVip')
<script async defer>
$('.imagesProduct').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.imagesProducts'
    });
    $('.imagesProducts').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.imagesProduct',
      focusOnSelect: true
    });

         $(document).ready(function(){
             $(window).scroll(function () {
                var w = window.innerWidth;
                if (w > 767) {
                    $("#scroller").stick_in_parent({offset_top: 75});
                }
            });
         
         });
         

      </script>
@include('xhome.partials.slideCustomer')
@endsection
