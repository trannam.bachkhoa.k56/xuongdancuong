@extends('xhome.layout.site')
@section('title', 'Tra cứu tiền hoa hồng của bạn')
@section('meta_description', isset($information['mo-ta-noi-dung']) ? $information['mo-ta-noi-dung'] : '')
@section('keywords', isset($information['tu-khoa']) ? $information['tu-khoa'] : '')

@section('content')
    <section class="content pdt20">
        <div class="container">
            <div class="title">
                <h2 class="colortl titl">{!! isset($information['tieu-de-keu-goi-affiliate']) ? $information['tieu-de-keu-goi-affiliate'] : 'TRA CỨU TIỀN CỦA BẠN TRÊN SHOP' !!}</h2>
            </div>
            <div class="row">
                <div class="col-12 col-lg-12 pd10">
                    @if (!empty($contact))
                    <p>Tổng số tiền {{ $contact->name }} đang có trong thẻ là: <span style="color: red">{{ !empty($contact->money) ? number_format($contact->money) : 0 }}</span> vnđ</p>
                    <div class="row">
                        <div class="col-xs-offset-0 col-md-offset-1 col-xs-12 col-md-5" >
                            <div class="form-group">
                                <h4>Mời bạn bè mua hàng qua facebook</h4>
                                <a href="https://www.facebook.com/sharer/sharer.php?u={!! $domainUrl !!}?affiliate_contact={{ $contact->phone }}">
                                    <button type="button" class="btn btn-primary"> Chia sẻ liên kết lên Facebook</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5" >
                            <div class="form-group">
                                <h4>Mời bạn bè bằng liên kết</h4>
                                <div class="input-group">
                                    <input type="text" class="form-control" value="{!! $domainUrl !!}?affiliate_contact={{ $contact->phone }}" id="myCoppy" placeholder="Username" aria-describedby="basic-addon1" disabled>
                                    <span class="input-group-addon btn btn-primary" id="basic-addon1"  onclick="copyCode('#myCoppy')">@ Coppy</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-offset-0 col-md-offset-1 col-xs-12 col-md-5" >
                            <div class="form-group">
                                <h4>Chia sẻ email</h4>
                                <a href='mailto:?subject=Tôi muốn mời bạn mua 1 sản phẩm tuyệt vời&amp;body=bạn truy cập website <a href="{!! $domainUrl !!}?affiliate_contact={{ $contact->phone }}">tại đây!</a>'
                                   title="Tôi muốn mời bạn mua 1 sản phẩm tuyệt vời">
                                    <img src="http://png-2.findicons.com/files/icons/573/must_have/48/mail.png">
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5" >
                            <div class="form-group">
                                <h4>Mời bạn bè bằng zalo</h4>
                                <div class="input-group">
                                    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
                                    <div class="zalo-share-button" data-href="{!! $domainUrl !!}?affiliate_contact={{ $contact->phone }}" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p><i>Bạn có thể sử dụng số tiền này để mua thêm (hoặc giảm giá khi mua) sản phẩm hoặc dịch vụ bên chúng tôi!</i></p>
                    <p><i>Hoặc có thể liên hệ với shop nếu có chính sách trả hoa hồng cho bạn!</i></p>
                    <p><i>Trân thành cảm ơn!</i></p>
                        @else
                        <p><i>Bạn chưa đăng ký trở thành khách hàng của shop. Vui lòng đăng ký tại: <a href="{!! $domainUrl !!}">{!! $domainUrl !!}</a>!</i></p>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <script>
        function copyCode(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).val()).select();
            document.execCommand("copy");
            $temp.remove();

            alert("Sao chép mã giới thiệu thành công: " + $(element).val());
        }
    </script>
@endsection
