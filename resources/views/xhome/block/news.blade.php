<link rel="stylesheet" type="text/css" media="screen" href="/public/xhome1/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" media="screen" href="/public/xhome1/css/owl.theme.default.css">
<script async defer src="/public/xhome1/js/owl.carousel.min.js"></script>
<section class="news pdb30 pdt30 bggray">
	<div class="container">
		<div class="title">
		   <h2 class="colortl">TIN TỨC MỚI</h2>
		</div>
		<div class="row">
			<div class="newsScroll owl-carousel">
			@foreach(\App\Entity\Post::newPost('tin-tuc', 8) as $id => $new)
			<?php $date=date_create($new->created_at); ?>
			<div class="item pdl15 pdr15">
				<div class="CropImg">
					<a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" class="thumbs">
						<img src="{{ !empty($new->image) ?  asset($new->image) : asset('/site/img/no-image.png') }}" alt="{{ $new->title }}"/>
					</a>
				</div>
				<h3><a class="f18 black textUpper lh25 block mgt10 noDecoration colorhv" href="{{ route('post', ['cate_slug' => 'tin-tuc-noi-bat', 'post_slug' => $new->slug]) }}">{{ $new->title }}</a></h3>
				<div class="Arthor font12 gray mgb10">
					{{ $new->user_email }} | {{ date_format($date,"d/m/Y H:i") }}
				</div>
				<div class="except cutText3">
					{{ isset($new['description']) ?  $new['description'] : '' }}
				</div>
			</div>
			@endforeach
			</div>
		</div>
	</div>
</section>
<script>
	$('.newsScroll').owlCarousel({
        autoplay: true,
        responsiveClass:true,
        dots:true,
        items : 4,
        responsive:{
            0:{
                items:4,
                nav:false
            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:3,
                nav:false,
            }
        }
    });
</script>
