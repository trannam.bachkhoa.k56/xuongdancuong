
<!-- MODULE CHÚNG TÔI LÀ - BÊN PHẢI CÓ HÌNH ẢNH -->

<section class="no-padding mgt30 ">
    <div class="container">
        <div class="title">
            <h1 class="colortl">{{ isset($information['tieu-de-trang']) ? $information['tieu-de-trang']  : '' }}</h1>
        </div>
        <div class="row" style="position: relative;">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div>
                    <div>
                        <?= isset($information['sologan']) ? $information['sologan'] : '' ?>
                    </div>
                </div>
				<a href="" class="btn-warning pd10 radius5" title="">Xem chi tiết <i class="fas fa-angle-double-right"></i> </a>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12" style="">
                <div class="imageCompanyAvatar">
					<img src="http://www.kientrucadong.com/thuvien/elib/2016/08/1471857902-thiet-ke-van-phong-lam-viec-18.jpg" alt="">
				</div>
            </div>
        </div>
    </div>
</section>