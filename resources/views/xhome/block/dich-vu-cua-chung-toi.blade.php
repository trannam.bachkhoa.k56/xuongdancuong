<!-- MODULE DỊCH VỤ CỦA CHÚNG TÔI -->

<section class="no-padding mgt30 ">
    <div class="container">
        <div class="title">
            <h2 class="colortl">DỊCH VỤ CỦA CHÚNG TÔI</h2>
        </div>
        <div class="row" style="position: relative;">
		
		@foreach(\App\Entity\SubPost::showSubPost('dich-vu-cua-chung-toi', 8) as $id => $dichvu)
		
			            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="boxP borderGray radius5 center pd15 sm-mgb10">
					<img src="{{ isset($dichvu['image']) ? asset($dichvu['image']) : '' }}" alt="{{ $dichvu->title }}" height="50">
                    <h4 class="red">{{ $dichvu->title }}</h4>
					<p class="f14 mgb0"> {{ $dichvu->description }}</p>
                </div>
            </div>
			
		@endforeach
			          
			        </div>
    </div>
</section>
<script async defer>
	$(function() {
				$('.boxP').matchHeight();
	});
</script>
<style async defer>
	.borderGray{
		border: 1px solid #ced4da;
	}
</style>
