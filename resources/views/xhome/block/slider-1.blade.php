<!-- HEADER CÓ KÈM SLIDE 1 HÌNH CÓ FORM -->
 <section class="bannerOne">
	<div class= "banner-sektion banner-overlay ">
		<div class="container text-center">
			<div class="col-md-12">
				<div class="banner-heading ">
					<h3>Enjoy Food Order and dine-out </h3>
					<P>Find restaurants, specials, and coupons for free</P>
					<form class="navbar-form" role="search">
						<div class="input-group">
							<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
							 <div class="input-group-btn">
						<button class="btn btn-primary site-btn" type="submit"><i class="fas fa-search"></i></button>
					  </div>
					</div>
				  </form>
				</div>
			</div>
		</div>    
	</div>
</section>
<style>
.site-btn {
    background-color: #e8500e;
    height: 55px;
    line-height: 55px;
    border: none;
    padding: 0 55px 0 50px;
    border-radius: 3px;
    color: #ffffff;
 }
.banner-overlay {
    background: linear-gradient(rgba(0,0,0,.7), rgba(0,0,0,.7)), url("https://images.pexels.com/photos/460599/pexels-photo-460599.jpeg?w=940&h=650&auto=compress&cs=tinysrgb");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    color: #fff;
    padding-top: 200px;
    padding-bottom:100px;
}
 input.form-control {
    height: 55px;
    background-color: #fff;
    color:#000;
    -webkit-border-radius: 1px;
    -moz-border-radius: 1px;
    border-radius: 1px;
    border: 1px solid #e9e6e0;
    -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);
    box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);
}
</style>