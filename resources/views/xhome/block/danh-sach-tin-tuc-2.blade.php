<section class="moreProduct bgrGray pdb30">
   <div class="container">
      <div class="link">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="#">{{ isset($category->title) ?$category->title : 'Tin tức' }}</a></li>
         </ol>
      </div>
      <div class="title">
         <h2 class="gray">{{ isset($category->title) ?$category->title : 'Tin tức' }}</h2>
      </div>
   </div>
   <div class="container">
		<div class="ListNews bgrWhite pd15">
			<div class="row">
				@foreach(\App\Entity\Post::categoryShow('tin-tuc',8) as $post) 
					<?php $date=date_create($post->created_at); ?>
					<div class="col-md-3 col-sm-6 col-xs-12 pdb10 pdt10">
						<div class="listcate">
						   <div class="img">
							  <div class="CropImg">
								 <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="noDecoration thumbs">
									<img src="{{ isset($post['image']) ?  $post['image'] : '' }}"
										 alt="{{ isset($post['title']) ?  $post['title'] : '' }}">
								 </a>
							  </div>
						   </div>
						 </div>
					</div>
					<div class="col-md-9 col-sm-6 col-xs-12">
						<div class="newContent">
						   <h3><a class="black" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="{{ $post['title'] }}" >{{ $post['title'] }}</a></h3>
						   <div class="Arthor font12 gray mgb10">
								{{ $post->user_email }} | {{ date_format($date,"d/m/Y H:i") }}
							</div>
							<div class="except cutText3">
								{{ isset($post['description']) ?  $post['description'] : '' }}
							</div>
					   </div>
					</div>
				@endforeach
			</div>
		</div>
		
   </div>
</section>
<style>
	
</style>