<link rel="stylesheet" type="text/css" media="screen" href="/public/xhome1/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" media="screen" href="/public/xhome1/css/owl.theme.default.css">
<script async defer src="/public/xhome1/js/owl.carousel.min.js"></script>
<section class="news pdb30 pdt30">
	<div class="container">
		<div class="title">
		   <h2 class="colortl">ĐỐI TÁC</h2>
		</div>
		<div class="row">
			<div class="customScroll owl-carousel">
			@foreach(\App\Entity\SubPost::showSubPost('doi-tac', 8) as $id => $doitac)
			<div class="item pdl15 pdr15">
				<div class="CropImg">
					<a href="/" class="thumbs">
						<img src="{{ isset($doitac['image']) ? asset($doitac['image']) : '' }}" alt="{{ $doitac->title }}"/>
					</a>
				</div>
			</div>
			@endforeach
			</div>
		</div>
	</div>
</section>
<script async defer>

	$('.customScroll').owlCarousel({
        autoplay: true,
        responsiveClass:true,
        dots:true,
        items : 6,
        responsive:{
            0:{
                items:2,
                nav:false
            },
            600:{
                items:4,
                nav:false
            },
            1000:{
                items:6,
                nav:false,
            }
        }
    });
</script>
