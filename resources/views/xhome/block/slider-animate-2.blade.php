<!-- SLIDE 1 HÌNH VÀ NHIỀU KIỂU CHỮ Ở NHIỀU SLIDE -->
<div class="owl-carousel owl-theme animate2">
      <div class="item">
        <h4 data-animation-in="fadeInUp" data-animation-out="animate-out fadeOutDown">Slide 1</h4>
        <p data-animation-in="rollIn" data-animation-out="animate-out rollOut">Cras a elementum dolor. Praesent aliquam sapien ac eros semper ullamcorper. Sed imperdiet enim at sodales suscipit. Aenean eget faucibus ipsum.</p>
        <p><a href="#" class="btn" data-animation-in="fadeInLeft" data-animation-out="animate-out fadeOutRight">Button 1</a></p>
      </div>
      <div class="item">
        <h4 data-animation-in="flipInY" data-animation-out="animate-out fadeOutUp">Slide 2</h4>
        <p data-animation-in="flipInX" data-animation-out="animate-out fadeOutLeft">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id dolor pulvinar, mollis orci vitae, molestie elit. Maecenas scelerisque ipsum nibh, id imperdiet nulla lobortis nec.</p>
        <p><a href="#" class="btn" data-animation-in="bounceInLeft" data-animation-out="animate-out bounceOutRight">Button 2</a></p>
      </div>
      <div class="item">
        <h4 data-animation-in="slideInDown" data-animation-out="animate-out slideOutUp">Slide 3</h4>
        <p data-animation-in="slideInRight" data-animation-out="animate-out fadeOut">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id dolor pulvinar, mollis orci vitae, molestie elit. Maecenas scelerisque ipsum nibh, id imperdiet nulla lobortis nec.</p>
        <p><a href="#" class="btn" data-animation-in="slideInUp" data-animation-out="animate-out slideOutDown">Button 3</a></p>
      </div>
  </div>
  
  <style>
	.animated  {
	  -webkit-animation-duration : 3s  ;
	  animation-duration : 3s  ;

	  -webkit-animation-delay : 500ms  ;
	  animation-delay : 500ms  ;
	}

	.animate-out {
	  -webkit-animation-delay : 0ms  ;
	  animation-delay : 0ms  ;
	}

	.animate2 h4 {
	  font-size: 28px;
	}

	.animate2 p {
	  width: 50%;
	  text-align: center;
	  margin: 0 auto 20px;
	}

	.owl-item {
	  display: table;
	}

	.owl-carousel .item {
		height: 450px;
	  background: url("http://funtek.vn/wp-content/uploads/2019/01/tanle3-1200x480.jpg") no-repeat;
	  background-size: 100%;
	  display: table-cell;
	  vertical-align: middle;
	  text-align: center;
	  width: 100%;
	}

	.animate2 .btn {
	  display: inline-block;
	  line-height: 35px;
	  height: 35px;
	  text-align: center;
	  padding: 0 20px;
	  width: auto;
	  background-color: #000;
	  text-decoration: none;
	  color: #fff;
	}
  </style>
  
  <script async defer>
  $(document).ready(function (){
  var owl = $('.owl-carousel');

  owl.owlCarousel({
      loop:false,
      margin:0,
      navSpeed:500,
      nav:true,
      autoplay: true,
      rewind: true,
      items:1
  });


  function setAnimation ( _elem, _InOut ) {
    var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

    _elem.each ( function () {
      var $elem = $(this);
      var $animationType = 'animated ' + $elem.data( 'animation-' + _InOut );

      $elem.addClass($animationType).one(animationEndEvent, function () {
        $elem.removeClass($animationType);
      });
    });
  }

  owl.on('change.owl.carousel', function(event) {
      var $currentItem = $('.owl-item', owl).eq(event.item.index);
      var $elemsToanim = $currentItem.find("[data-animation-out]");
      setAnimation ($elemsToanim, 'out');
  });

  var round = 0;
  owl.on('changed.owl.carousel', function(event) {

      var $currentItem = $('.owl-item', owl).eq(event.item.index);
      var $elemsToanim = $currentItem.find("[data-animation-in]");
    
      setAnimation ($elemsToanim, 'in');
  })
  
  owl.on('translated.owl.carousel', function(event) {
    console.log (event.item.index, event.page.count);
    
      if (event.item.index == (event.page.count - 1))  {
        if (round < 1) {
          round++
          console.log (round);
        } else {
          owl.trigger('stop.owl.autoplay');
          var owlData = owl.data('owl.carousel');
          owlData.settings.autoplay = false;
          owlData.options.autoplay = false;
          owl.trigger('refresh.owl.carousel');
        }
      }
  });

});
  </script>
