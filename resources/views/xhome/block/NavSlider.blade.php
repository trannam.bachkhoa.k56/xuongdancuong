<section class="NavSlider mgb30">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<ul class="NavLeft">
					<li><a href="#"></a>
				</ul>
			</div>
			<div class="col-md-9">
				<div id="slider" class="carousel slide bgGray" data-ride="carousel">
				  <!-- Indicators -->
				  <ul class="carousel-indicators">
					<li data-target="#slider" data-slide-to="0" class="active"></li>
					<li data-target="#slider" data-slide-to="1"></li>
					<li data-target="#slider" data-slide-to="2"></li>
				  </ul>
				  <!-- The slideshow -->
				  <div class="carousel-inner">
					@foreach(\App\Entity\SubPost::showSubPost('slider', 8) as $id => $slide)
					  <div class="carousel-item {{ ($id == 0)? 'active' : ''}}">
						<img style="width: 100%;" src="{{ isset($slide['image']) ?  $slide['image'] : '' }}" alt="{{ isset($slide['title']) ?  $slide['title'] : '' }}">
					  </div>
					@endforeach
				  </div>
				  <!-- Left and right controls -->
				  <a class="carousel-control-prev" href="#slider" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
				  </a>
				  <a class="carousel-control-next" href="#slider" data-slide="next">
					<span class="carousel-control-next-icon"></span>
				  </a>

				</div>
			</div>
		</div>
	</div>
</section>