<section class="customer pdt20">
 <div class="container">
	<div class="title">
	   <h2 class="colortl"><?= isset($information['tieu-de-cho-phan-hoi-khach-hang']) ? $information['tieu-de-cho-phan-hoi-khach-hang'] : 'Phản hồi khách hàng' ?></h2>
	</div>
	<div class="slideCustomer">
	   @foreach(\App\Entity\SubPost::showSubPost('khach-hang', 8) as $id => $customer)
	   <div class="item pd10">
		  <div class="imgItem">
			 @if (isset($customer['ma-chia-se-youtube']) && !empty($customer['ma-chia-se-youtube']))
				{!! $customer['ma-chia-se-youtube'] !!}
			 @elseif (isset($customer['image']) && !empty($customer['image']))
				<a href="#" title="">
				   <img src="{{ isset($customer['image']) ? asset($customer['image']) : '' }}" alt="">
				</a>
			 @endif
		  </div>
		  <div class="infoCustomer">
			 <h4>{{ isset($customer['title']) ?  $customer['title'] : '' }}</h4>
		  </div>
	   </div>
	   @endforeach
	</div>
 </div>
</section>

   <script async defer type="text/javascript">
      $('.slideCustomer').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
              responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      ]
      });
   </script>
