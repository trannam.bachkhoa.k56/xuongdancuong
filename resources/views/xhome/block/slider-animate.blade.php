
<!--SLIDE NHIỀU HÌNH VÀ NHIỀU CHỮ-->

<div id="carouselExampleIndicators" class="carousel slide">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>

    <!-- carousel content -->
    <div class="carousel-inner">

        <!-- first slide -->
        <div class="carousel-item active">
            <img class="d-block w-100 w100" src="http://vuinhiepanh.com/assets/uploads/2017/11/Cu%E1%BB%99c-thi-nhi%E1%BA%BFp-%E1%BA%A3nh-T%C3%A2y-Ninh-2017-3.jpg" alt="First slide">
            <div class="carousel-caption d-md-block">
                <h3 data-animation="animated bounceInLeft">
                    This is the caption for slide 1
                </h3>
                <button class="btn btn-primary" data-animation="animated zoomInUp">Button</button>
            </div>
        </div>

        <!-- second slide -->
        <div class="carousel-item">
            <img class="d-block w-100 w100" src=" http://funtek.vn/wp-content/uploads/2019/01/tanle3-1200x480.jpg" alt="Second slide">
            <div class="carousel-caption d-md-block">
                <h3 data-animation="animated bounceInRight">
                    This is the caption for slide 2
                </h3>
                <button class="btn btn-primary" data-animation="animated zoomInUp">Button</button>
            </div>
            <!-- second slide content -->
        </div>

       
    </div>

    <!-- controls -->
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<style>
	.carousel-caption h3 {
	  animation-delay: 1s;
	}

	.carousel-caption button {
	  animation-delay: 2s;
	}

	.carousel-item img {
	  opacity: 0.8;
	}
	#carouselExampleIndicators .carousel-caption{
		bottom: 35% !important;
	}
</style>

<script async defer>
(function($) {
  function doAnimations(elems) {
    var animEndEv = "webkitAnimationEnd animationend";

    elems.each(function() {
      var $this = $(this),
        $animationType = $this.data("animation");
      $this.addClass($animationType).one(animEndEv, function() {
        $this.removeClass($animationType);
      });
    });
  }

  var $myCarousel = $("#carouselExampleIndicators"),
    $firstAnimatingElems = $myCarousel
      .find(".carousel-item:first")
      .find("[data-animation ^= 'animated']");

  $myCarousel.carousel();

  doAnimations($firstAnimatingElems);

  $myCarousel.on("slide.bs.carousel", function(e) {
    var $animatingElems = $(e.relatedTarget).find(
      "[data-animation ^= 'animated']"
    );
    doAnimations($animatingElems);
  });
})(jQuery);

</script>
