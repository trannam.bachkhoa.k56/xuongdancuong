<section class="content pdt20">
        <div class="container">
            <div class="title">
                <h2 class="clOrange"><?= isset($information['tieu-de-san-pham-hoac-dich-vu']) ? $information['tieu-de-san-pham-hoac-dich-vu'] : 'Top sản phẩm hoặc dịch vụ' ?></h2>
            </div>
            <div class="row">
                @foreach(\App\Entity\Product::newProduct(20) as $hotProduct)
                        <div class="col-12 col-lg-3 pd10 itemProduct">
                            <div class="imgItem relative">
                                <div class="boxDetails">
                                    <div class="borderBoxDetails hvr-shutter-in-vertical">
                                    </div>
                                </div>
                                <div class="CropImg">
                                    <a href="{{ route('product',['cate_slug' => $hotProduct->slug]) }}" class="noDecoration thumbs">
                                        <img src="{{ isset($hotProduct['image']) ?  $hotProduct['image'] : '' }}"
                                             alt="{{ isset($hotProduct['title']) ?  $hotProduct['title'] : '' }}">
                                    </a>
                                </div>
                            </div>
                            <div class="infoItem">
                                <a href="{{ route('product',['cate_slug' => $hotProduct->slug]) }}" class="noDecoration"><h3>{{ isset($hotProduct['title']) ?  $hotProduct['title'] : '' }}</h3></a>
                                <p class="star">
                                    <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star"
                                                                                    aria-hidden="true"></i><i
                                            class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star"
                                                                                         aria-hidden="true"></i><i
                                            class="fa fa-star" aria-hidden="true"></i>
                                </p>
                                <p class="price red">
                                    @if(empty($hotProduct->price))
                                        <span>liên hệ</span>
                                    @elseif($hotProduct->discount > 0)
                                        <span><del>{{ number_format($hotProduct->price) }}</del> đ</span> - <span>{{ number_format($hotProduct->discount) }} đ</span>
                                    @else
                                        <span>{{ number_format($hotProduct->price) }} đ</span>
                                    @endif
                                </p>
                                <p class="comment black">
                                    @if (!empty($hotProduct->views) )
                                        <i class="fa fa-eye" aria-hidden="true"></i> <span>{{ $hotProduct->views }} Lượt xem </span>
                                    @endif
                                    @if (!empty(\App\Entity\Comment::getCountComment($hotProduct->post_id)) )
                                        <i class="fa fa-commenting-o" aria-hidden="true"></i> <span>{{ \App\Entity\Comment::getCountComment($hotProduct->post_id) }} Bình luận</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>

    </section>