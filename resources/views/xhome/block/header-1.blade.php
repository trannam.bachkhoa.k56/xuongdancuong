<section class="headerOne">
	<nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
		<div class="container">
	  <!-- Brand -->
	  <a class="navbar-brand" href="#"><span>Logo</span> Here</a>

	  <!-- Toggler/collapsibe Button -->
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	  </button>

	  <!-- Navbar links -->
	  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav ml-auto">
		  <li class="nav-item">
			<a class="nav-link" href="#">Link</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Link</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Link</a>
		  </li> 
		   <!-- Dropdown -->
		<li class="nav-item dropdown">
		  <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
			Dropdown link
		  </a>
		  <div class="dropdown-menu">
			<a class="dropdown-item" href="#">Link 1</a>
			<a class="dropdown-item" href="#">Link 2</a>
			<a class="dropdown-item" href="#">Link 3</a>
		  </div>
		</li>
		</ul>
	  </div>
		</div>
	</nav>

	<div class="banner">
		<div class="container">
		<div class="banner-text">
		<div class="banner-heading">
		Glad to see you here !
		</div>
		<div class="banner-sub-heading">
		Here goes the secondary heading on hero banner
		</div>
		<button type="button" class="btn btn-warning text-dark btn-banner">Get started</button>
		</div>
		</div>
	</div>
</section>

<style>
	.headerOne h1, .headerOne h2, .headerOne h3, .headerOne h4, .headerOne h5, .headerOne h6{
		color:#323233;
		text-transform:uppercase;
	}
	.headerOne .navbar-brand  span{
		color: #fed136;
		font-size:25px;font-weight:700;letter-spacing:0.1em;
		font-family: 'Kaushan Script','Helvetica Neue',Helvetica,Arial,cursive;
	}
	.headerOne .navbar-brand {
		color: #fff;
		font-size:25px;
		font-family: 'Kaushan Script','Helvetica Neue',Helvetica,Arial,cursive;
		font-weight:700;
		letter-spacing:0.1em;
	}
	.headerOne .navbar-nav .nav-item .nav-link{
		padding: 1.1em 1em!important;
		font-size: 120%;
		font-weight: 500;
		letter-spacing: 1px;
		color: #fff;
	   font-family: 'Gothic A1', sans-serif;
	}
	.headerOne .navbar-nav .nav-item .nav-link:hover{
		color:#fed136;
	}
	.headerOne .navbar-expand-md .navbar-nav .dropdown-menu{
		border-top:3px solid #fed136;
	}
	.headerOne .dropdown-item:hover{
		background-color:#fed136;
		color:#fff;
	}
	.headerOne nav{
		-webkit-transition: padding-top .3s,padding-bottom .3s;
		-moz-transition: padding-top .3s,padding-bottom .3s;
		transition: padding-top .3s,padding-bottom .3s;
		border: none;
	}
	.headerOne .shrink{
		padding-top: 0;
		padding-bottom: 0;
		background-color: #212529;
	}
	.headerOne .banner{
		background-image:url('http://www.hd-freewallpapers.com/latest-wallpapers/abstract-website-backgrounds.jpg');
		text-align: center;
		color: #fff;
		background-repeat: no-repeat;
		background-attachment: scroll;
		background-position: center center;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}
	.headerOne .banner-text{
		padding:200px 0 150px 0;
	}
	.headerOne .banner-heading{
		font-family: 'Lobster', cursive;
		font-size: 75px;
		font-weight: 700;
		line-height: 100px;
		margin-bottom: 30px;
		color:#fff;
	}	
	.headerOne .banner-sub-heading{
		font-family: 'Libre Baskerville', serif;
		font-size: 30px;
		font-weight: 300;
		line-height: 30px;
		margin-bottom: 50px;
		color:#fff;
	}
	.headerOne .btn-banner{
		padding:5px 20px;
		border-radius:10px;
		font-weight:700;
		line-height:1.5;
		text-align:center;
		color:#fff;
		text-transform:uppercase;
	}
	.headerOne .text-intro{
		width:90%;
		margin:auto;
		text-align:center;
		padding-top:30px;
	}
@media (max-width:500px){
	.headerOne .navbar-nav{
		background-color:#000;
		border-top:3px solid #fed136;
		color:#fff;
		z-index:1;
		margin-top:5px;
	}
	.headerOne .navbar-nav .nav-item .nav-link{
		padding: 0.7em 1em!important;
		font-size: 100%;
		font-weight: 500;
	}
	.headerOne .banner-text{
		padding:150px 0 150px 0;
	}
	.headerOne .banner-heading{
		font-size: 30px;
		line-height: 30px;
		margin-bottom: 20px;
	}
	.headerOne .banner-sub-heading{
		font-size: 10px;
		font-weight: 200;
		line-height: 10px;
		margin-bottom: 40px;
	}
}

@media (max-width:768px){
	.headerOne .banner-text{
		padding:150px 0 150px 0;
	}
	.headerOne .banner-sub-heading{
		font-size: 23px;
		font-weight: 200;
		line-height: 23px;
		margin-bottom: 40px;
	}
}
</style>
<script async defer>
	$(document).on("scroll", function(){
		if
      ($(document).scrollTop() > 86){
		  $("#banner").addClass("shrink");
		}
		else
		{
			$("#banner").removeClass("shrink");
		}
	})
</script>
