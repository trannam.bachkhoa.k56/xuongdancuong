
<!-- MODULE CHÚNG TÔI LÀ - BÊN PHẢI LÀ FORM ĐĂNG KÝ -->

<section class="no-padding mgt30 ">
        <div class="container">
            <div class="title">
                <h1 class="colortl">CHÚNG TÔI LÀ</h1>
            </div>
            <div class="row" style="position: relative;">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div>
                        <div>
                            <?= isset($information['sologan']) ? $information['sologan'] : '' ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12"  style="">
                    <div id="scroller">
                        <form onSubmit="return contact(this);" class="wpcf7-form" method="post"
                              action="{{route('sub_contact')}} " >
                            {!! csrf_field() !!}
                            <input type="hidden" name="is_json"
                                   class="form-control captcha" value="1" placeholder="">
                            <div class="form-group">
                                <div>
                                    <input type="text" class="form-control" placeholder="Tên của bạn" name="name"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <input type="email" class="form-control" placeholder="Email của bạn" name="email"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <input type="text" class="form-control" placeholder="Số diện thoại của bạn" name="phone"
                                           required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                <textarea class="form-control" rows="2" name="message"
                                          placeholder="Nội dung ghi chú"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn  btn-warning">ĐĂNG KÝ TƯ VẤN</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>