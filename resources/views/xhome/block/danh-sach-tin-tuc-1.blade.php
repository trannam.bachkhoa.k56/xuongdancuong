<section class="moreProduct bgrGray pdb30">
   <div class="container">
      <div class="link">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i> Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="#">{{ isset($category->title) ?$category->title : 'Tin tức' }}</a></li>
         </ol>
      </div>
      <div class="title">
         <h2 class="gray">{{ isset($category->title) ?$category->title : 'Tin tức' }}</h2>
      </div>
   </div>
   <div class="container">
      <div class="row">
		<div class="col-md-9 col-xs-12">
			<div class="row">
				@foreach(\App\Entity\Post::categoryShow('tin-tuc',8) as $post) 
				 <div class="col-md-4 col-sm-6 col-xs-12 mgb20">
					<div class="listcate">

					   <div class="img">
						  <div class="CropImg">
							 <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="noDecoration thumbs">
								<img src="{{ isset($post['image']) ?  $post['image'] : '' }}"
									 alt="{{ isset($post['title']) ?  $post['title'] : '' }}">
							 </a>
						  </div>
					   </div>
					   <div class="newContent">
					   <a class="black" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="{{ $post['title'] }}" >{{ $post['title'] }}</a>
					   </div>
					</div>
				 </div>
				 @endforeach
			</div>
		</div>
		<div class="col-md-3 col-xs-12">
			<div class="titleCategory">
				SẢN PHẨM MỚI
			</div>
			@foreach(\App\Entity\Post::categoryShow('tin-tuc',5) as $post) 
            <div class="bgrWhite pd10 bdGray">
               <div class="img">
                  <div class="CropImg">
                     <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" class="noDecoration thumbs">
                        <img src="{{ isset($post['image']) ?  $post['image'] : '' }}"
                             alt="{{ isset($post['title']) ?  $post['title'] : '' }}">
                     </a>
                  </div>
               </div>
               <div class="newContent">
               <a class="black" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" title="{{ $post['title'] }}" >{{ $post['title'] }}</a>
               </div>
            </div>
			@endforeach
		</div>
		
	  
	  
		
      </div>
   </div>
</section>
<style>
	.titleCategory{
		background: #000;
		color: white;
		text-align: center;
		font-weight: 700;
		font-size: 20px;
		padding: 5px 0;
	}
</style>