<div class=" bgGray">
    <div id="slide" class="carousel slide bgGray" data-ride="carousel">

      <!-- Indicators -->
      <ul class="carousel-indicators">
        <li data-target="#slide" data-slide-to="0" class="active"></li>
        <li data-target="#slide" data-slide-to="1"></li>
        <li data-target="#slide" data-slide-to="2"></li>
      </ul>

      <!-- The slideshow -->
      <div class="carousel-inner">
        @foreach(\App\Entity\SubPost::showSubPost('slider', 8) as $id => $slide)
          <div class="carousel-item {{ ($id == 0)? 'active' : ''}}">
            <img style="width: 100%;" src="{{ isset($slide['image']) ?  $slide['image'] : '' }}" alt="{{ isset($slide['title']) ?  $slide['title'] : '' }}">
          </div>
        @endforeach
      </div>

      <!-- Left and right controls -->
      <a class="carousel-control-prev" href="#slide" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#slide" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>

    </div>
</div>


