	 <div class="filter">
            <form action="" method="get" id="form-filter">
            <!--     <input type="hidden" name="word" value="{{isset($word)? $word : ''}}"> -->
                <div class="form-group">
                  <label>Sắp xếp theo:</label>
                        <select name="sort"  id="frm_filter_header_sort_by" class="form-control select frm_filter_header_sort_by" dir="auto">
                            <option value="default" style="">--- Mặc định ---</option>
                            <option value="priceIncrease"
                                    {{ (isset($_GET['sort']) && ($_GET['sort'] == 'priceIncrease') ) ? 'selected' : ''}}>
                                Giá tăng dần
                            </option>
                            <option value="priceReduction"
                                    {{ (isset($_GET['sort']) && ($_GET['sort'] == 'priceReduction') ) ? 'selected' : ''}}>
                                Giá giảm dần
                            </option>
                            <option value="sortName"
                                    {{ (isset($_GET['sort']) && ($_GET['sort'] == 'sortName') ) ? 'selected' : ''}}>
                                Xếp theo tên
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                      <label> Danh mục :</label>
                      <select name="category" class="form-control select frm_filter_header_sort_by" style="text-align: center;" >
                         <option value="" >--- Sắp xếp theo danh mục ---</option>
                         @foreach(App\Entity\Category::getCategoryProduct() as $category)
                          <option value="{{$category->category_id}}"
                          {{ (isset($_GET['category']) && ($_GET['category'] == $category->category_id ) ) ? 'selected' : ''}} style="" >{{$category->title}}</option>
                         @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label> Khoảng giá :</label>
                        <input class="form-control select" type="number" name="min" placeholder="min" value="{{ isset($_GET['min']) ? $_GET['min'] : '' }}">
                    </div>

                    <div class="form-group">
                      <label> Đến :</label>
                        <input class="form-control select" type="number" name="max" placeholder="max" value="{{ isset($_GET['max']) ? $_GET['max'] : '' }}">
                    </div>
                    <div class="form-group">
                       <button class="btn btn-success" type="submit"><i class="fas fa-filter"></i></button>
                    </div>

               </form>
               <style type="text/css">
                select.select{
                  display: inline-block;
                  width: auto;
                }
                input.select{
                   display: inline-block;
                  width: 100px;
                }
                .filter .form-group{
                  display: inline-block;
                  margin-right: 20px;

                }
                .filter{
                  text-align: center;
                }
               </style>
                  <script async defer>
                  $('.frm_filter_header_sort_by').change(function(){
                      $(this).parent().parent().submit();
                  });
                  </script>
          </div>
