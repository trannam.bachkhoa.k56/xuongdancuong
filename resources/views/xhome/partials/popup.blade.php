	@foreach(\App\Entity\SubPost::showSubPost('popup', 1) as $id => $popup)	
	<div class="modal fade" id="popup" role="dialog" tabindex="-1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<a class="close" data-dismiss="modal">×</a>
				<div class="modal-body">
					<a href="{{ isset($popup['link-popup']) ?  $popup['link-popup'] : '' }}"><img src="{{ isset($popup['image']) ? asset($popup['image']) : '' }}"/></a>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	@endforeach
	<script async defer>
		$(window).load(function(){
		   setTimeout(function(){
			   $('#popup').modal('show');
		   }, 5000);
		});
	</script>
