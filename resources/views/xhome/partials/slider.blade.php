<div class="">
<div class=" ">
    <div id="demo" class="carousel slide" data-ride="carousel">
      <!-- The slideshow -->
      <div class="carousel-inner">
        @foreach(\App\Entity\SubPost::showSubPost('slider', 8) as $id => $slide)
          <div class="carousel-item {{ ($id == 0)? 'active' : ''}}">
            <a href="{{ !empty($slide['link-tro-den']) ? $slide['link-tro-den'] : '/' }}"><img style="width: 100%;" src="{{ isset($slide['image']) ?  $slide['image'] : '' }}" alt="{{ isset($slide['title']) ?  $slide['title'] : '' }}"></a>
          </div>
        @endforeach
      </div>

      <!-- Left and right controls -->
      <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>

    </div>
</div>
</div>



