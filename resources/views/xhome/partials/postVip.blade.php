<?php
$domain = App\Ultility\Ultility::getCurrentHttpHost();
$userId = App\Entity\Domain::getUserIdWithUrlLike($domain);
$user = App\Entity\User::getUserByUserId($userId['user_id']);
?>
@if(!empty($user) && $user->vip < 1)
<section class="similarProduct">
	<div class="container">
		<div class="title">
               <h2 class="colortl titl">
					Tiêu biểu
               	</h2>
            </div>
		<div class="postVip">
			@foreach(\App\Entity\Post::showPostVip('post',20) as $hotProduct)
			<div class="col-lg-3 col-md-4 col-12">
				<div class="productRelated">
					<div class="imgItem relative">
						<div class="boxDetails">
							<div class="borderBoxDetails hvr-shutter-in-vertical">
							</div>
						</div>
						<div class="CropImg">
							<a href="{{\App\Entity\Domain::showUrlPostVip($hotProduct->theme_id, $hotProduct->user_id)}}/tin-tuc/{{$hotProduct->slug}}" target="_blank" class="noDecoration thumbs">
								<img src="{{ isset($hotProduct['image']) ?  $hotProduct['image'] : '' }}" alt="{{$hotProduct->title}}">
							</a>
						</div>
					</div>
					<div class="infoItem boxP">
						<a href="{{\App\Entity\Domain::showUrlPostVip($hotProduct->theme_id, $hotProduct->user_id)}}/tin-tuc/{{$hotProduct->slug}}" target="_blank" class="noDecoration"><h3 class="CutText2">{{$hotProduct->title}}</h3></a>
						<p class="star">
							<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
						</p>						

						<p class="comment black">
							<i class="fa fa-eye" aria-hidden="true"></i> <span>{{ $hotProduct->views }} Lượt xem </span>
						</p>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
<script async defer>
	  $('.postVip').slick({
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 1500,
		  responsive: [
			{
			  breakpoint: 768,
			  settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 3
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				arrows: false,
				centerMode: true,
				centerPadding: '40px',
				slidesToShow: 1
			  }
			}
		  ]
		    
		});
</script>
@endif
