<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title="{{ $product->title }}" class="noDecoration">
	<div class="item pd10">
	  <div class="imgItem">
		  <img src="{{ !empty($product->image) ?  asset($product->image) : '' }}" alt="{{ $product->title }}">
	  </div>
	  <div class="infoItem">
		  <h3>{{ $product->title }}</h3>
		  <p class="star">
			<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i> 
		  </p>
		  <p class="price black">
			@if(empty($product->price))
				<span>liên hệ</span>
			@elseif($product->discount > 0)
				<span><del>{{ number_format($product->price) }}đ</del></span> - <span class="red fw6">{{ number_format($product->discount) }}đ</span>
				@else
				<span class="red fw6">{{ number_format($product->price) }} đ</span>
			@endif
			
		  </p>
		  <p class="comment black">
			  <i class="far fa-comment-dots"></i> {{ \App\Entity\Comment::getCountComment($product->post_id) }} Bình luận
		  </p>
	  </div>
	</div>
</a>
