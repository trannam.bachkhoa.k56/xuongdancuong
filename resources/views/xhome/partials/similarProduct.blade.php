<section class="similarProduct">
	<div class="container">
		<div class="title">
               <h2 class="colortl titl"><?= isset($information['san-pham-hoac-dich-vu-tuong-tu']) ? $information['san-pham-hoac-dich-vu-tuong-tu'] : 'Sản phẩm tương tự hoặc dịch vụ tương tự thay trong nội dung website -> thông tin trang' ?></h2>
            </div>
		<div class="productSimilar row">
			@foreach(\App\Entity\Product::relativeProduct($product->slug, $product->product_id, 8) as $hotProduct)
			<div class="col-lg-3 col-md-4 col-12">
				<div class="productRelated">
					<div class="imgItem relative">
						<div class="boxDetails">
							<div class="borderBoxDetails hvr-shutter-in-vertical">
							</div>
						</div>
						<div class="CropImg">
							<a href="{{ route('product',['cate_slug' => $hotProduct->slug]) }}" class="noDecoration thumbs">
								<img src="{{ isset($hotProduct['image']) ?  $hotProduct['image'] : '' }}" alt="{{$hotProduct->title}}">
							</a>
						</div>
					</div>
					<div class="infoItem boxP">
						<a href="{{ route('product',['cate_slug' => $hotProduct->slug]) }}" class="noDecoration"><h3 class="CutText2">{{$hotProduct->title}}</h3></a>
						<p class="star">
							<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
						</p>
						<p class="price red bgrGrey left pd10 mgb0 mgr30" style="width: 100%">
							 @if(empty($hotProduct->price))
								<span>liên hệ</span>
							 @elseif($hotProduct->discount > 0)
								<span><del>{{ number_format($hotProduct->price) }} đ</del></span> - <span style="color:#dc3545">{{ number_format($hotProduct->discount) }} đ</span>
							 @else
								<span>{{ number_format($hotProduct->price) }} đ</span>
							 @endif
						  </p>

						<p class="comment black">
							<i class="fa fa-eye" aria-hidden="true"></i> <span>{{ $hotProduct->views }} Lượt xem </span>
						</p>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
