<section class="customer pdt20 pdb30">
         <div class="container">
            <div class="title">
               <h2 class="colortl titl"><?= isset($information['tieu-de-cho-phan-hoi-khach-hang']) ? $information['tieu-de-cho-phan-hoi-khach-hang'] : 'Phản hồi khách hàng' ?></h2>
            </div>
            <div class="slideCustomer row">
				@foreach(\App\Entity\SubPost::showSubPost('khach-hang', 8) as $id => $customer)
				   <div class="item col-12 col-md-3 pd10">
					  <div class="imgItem">
						 @if (isset($customer['ma-chia-se-youtube']) && !empty($customer['ma-chia-se-youtube']))
							<div id="embbedYoutube{{$id}}">
						
							</div>
							<script>
								$(window).load(function() { 
									$("#embbedYoutube{{$id}}").html('{!! $customer['ma-chia-se-youtube'] !!}');
								});
							 </script>
						 @elseif (isset($customer['image']) && !empty($customer['image']))
							<a title="{{ isset($customer['title']) ?  $customer['title'] : '' }}">
							   <img src="{{ isset($customer['image']) ? $customer['image'] : '' }}" alt="" class="mg">
							</a>
						 @endif
					  </div>
					  <div class="infoCustomer">
						 <h4>{{ isset($customer['title']) ?  $customer['title'] : '' }}</h4>
					  </div>
				   </div>
			   @endforeach
            </div>
         </div>
      </section>

   <script async defer type="text/javascript">
      /*$('.slideCustomer').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
              responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      ]
      });*/
   </script>
