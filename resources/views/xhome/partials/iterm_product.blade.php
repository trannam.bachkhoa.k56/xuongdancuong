<div class="v2_bnc_pr_item">
		<figure class="v2_bnc_pr_item_img">
		 <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title="{{ $product->title }}">
		 <img alt="{{ $product->title }}" id="f-pr-image-zoom-id-tab-home-742729" src="{{ !empty($product->image) ?  asset($product->image) : '' }}" 
			class="BNC-image-add-cart-742729 img-responsive"/>
		 </a>
			<figcaption class="v2_bnc_pr_item_boxdetails">
				<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" class="links_fixed">
				   <div class="hvr-shutter-in-vertical">
					  <div class="v2_bnc_pr_item_boxdetails_border">
						 <div class="v2_bnc_pr_item_boxdetails_content">
							<!-- Products Name -->  
				<h3 class="v2_bnc_pr_item_name">
					<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title=" {{ $product->title }}">
					{{ $product->title }}
					</a>
				</h3>
				<!-- End Products Name --> 
				<!-- Details -->
				<div class="v2_bnc_pr_item_short_info">
				<p>Giá :
				@if(empty($product->price))
					<span>liên hệ</span>
				@elseif($product->discount > 0)
				<span><del>{{ number_format($product->price) }}</del> đ</span> -
				<span>{{ number_format($product->discount) }} đ</span>
				@else
				<span>{{ number_format($product->price) }} đ</span>
				@endif
				</p>   
				<p>Liên hê : {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai']  : '' }}</p>                             </div>
				<!-- End Details -->
				</div>
				</div>
				</div>    
				</a>  
			</figcaption>
		</figure>
		<div class="content" style="padding-top: 10px;">
			<h3 class="v2_bnc_pr_item_name">
				<a href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title=" {{ $product->title }}">
					{{ $product->title }}
				</a>
			</h3>
			<p class="star"> 
			<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
			</p>
			<p class="f18">
				@if(empty($product->price))
					<span>liên hệ</span>
				@elseif($product->discount > 0)
				<span><del>{{ number_format($product->price) }}</del> đ</span> -
				<span class="red fw6">{{ number_format($product->discount) }} đ</span>
				@else
				<span>{{ number_format($product->price) }} đ</span>
				@endif
			</p>
			<p class="">
			<i class="fa fa-commenting-o" aria-hidden="true"></i> <span>{{ \App\Entity\Comment::getCountComment($product->post_id) }} bình luận<//span>
			</p>		
		</div>
</div>