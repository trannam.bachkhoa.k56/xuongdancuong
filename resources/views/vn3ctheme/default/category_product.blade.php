@extends('vn3ctheme.layout.site')

@section('type_meta', 'website')
@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description', !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
    <!-- Main Content Pages -->
	<div class="content-pages">
		<!-- Subpages -->
		<div class="sub-home-pages">
			<!-- Portfolio Subpage -->
			<section id="portfolio" class="sub-page">
				<div class="sub-page-inner">
					<div class="section-title">
						<div class="main-title">
							<div class="title-main-page">
								<h4>Kho giao diện</h4>
								<p>Quý khách hàng vui lòng lựa chọn giao diện mẫu tại kho giao diện VN3C</p>
							</div>
						</div>
					</div>

					<div class="section-content">
						<div class="filter-tabs">
							<button class="fil-cat" data-rel="all"><span>0</span> All</button>
							<button class="fil-cat" data-rel="photography"><span>05</span> Websites</button>
							<button class="fil-cat" data-rel="web-design"><span>07</span> Decorations</button>
							<button class="fil-cat" data-rel="branding"><span>12</span> Business Logo</button>
						</div>

						<div class="portfolio-grid portfolio-trigger" id="portfolio-page">
							<div class="row">
								<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 portfolio-item branding photography all">
									<div class="portfolio-img">
										<img src="img/portfolio-img-1.jpg" class="img-responsive" alt="">
									</div>
									<div class="portfolio-data">
										<h4><a href="chi-tiet-giao-dien.php">F-FASHION</a></h4>
										<p class="meta">1.500.000đ</p>
										<div class="portfolio-attr"> 
											<a class="btn" href="chi-tiet-giao-dien.php">Chi tiết</a>
											<a class="btn btnOrange" href="chi-tiet-giao-dien.php">Demo</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- /Portfolio Subpage -->
		</div>
		<!-- /Page changer wrapper -->
	</div>
	<!-- /Main Content -->
@endsection
