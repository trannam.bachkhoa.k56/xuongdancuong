@extends('vn3ctheme.layout.site')

@section('type_meta', 'website')
@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title )
@section('meta_description', !empty($category->meta_description) ? $category->meta_description : $category->description )
@section('keywords', '')

@section('content')
    <!-- Main Content Pages -->
            <div class="content-pages pd20">
                <!-- Subpages -->
                <div class="sub-home-pages">
					<!-- Blog Subpage -->
                    <section id="blog" class="sub-page">
                        <div class="sub-page-inner">
                            <div class="section-title">
                                <div class="main-title">
                                    <div class="title-main-page">
                                        <h4>DỊCH VỤ</h4>
                                        <p>VN3C Cung cấp các dịch vụ thiết kế website như sau</p>
                                    </div>
                                </div>
                            </div>

                            <div class="section-content">
                                <div class="row blog-grid-flex">
                                    <div class="col-md-4 col-sm-6 blog-item">
                                        <div class="blog-article">
                                            <div class="post-format"> <span class="post-format-icon"><i class="lnr lnr-film-play"></i></span> </div>
                                            <div class="comment-like"> <span><i class="fas fa-comment" aria-hidden="true"></i> 30</span> <span><i class="fas fa-heart" aria-hidden="true"></i> 15</span> </div>
                                            <div class="article-img">
                                                <a href="chi-tiet-tin-tuc.php"> <img src="img/3.jpg" class="img-responsive" alt=""></a>
                                            </div>
                                            <div class="article-link"> <a href="chi-tiet-tin-tuc.php"><i class="lnr lnr-arrow-right"></i></a> </div>
                                            <div class="article-content">
                                                <h4><a href="chi-tiet-tin-tuc.php">Content builder posts</a></h4>
                                                <div class="meta"> <span><i>Feb</i> 16,2016</span> <span><i>In</i> <a href="#">Shopping</a></span> <span><i>Tags</i> <a href="#">Trends</a></span> </div>
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority .</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 blog-item">
                                        <div class="blog-article">
                                            <div class="post-format"> <span class="post-format-icon"><i class="lnr lnr-picture"></i></span> </div>
                                            <div class="comment-like"> <span><i class="fas fa-comment" aria-hidden="true"></i> 30</span> <span><i class="fas fa-heart" aria-hidden="true"></i> 15</span> </div>
                                            <div class="article-img">
                                                <a href="chi-tiet-tin-tuc.php"> <img src="img/4.jpg" class="img-responsive" alt=""></a>
                                            </div>
                                            <div class="article-link"> <a href="chi-tiet-tin-tuc.php"><i class="lnr lnr-arrow-right"></i></a> </div>
                                            <div class="article-content">
                                                <h4><a href="chi-tiet-tin-tuc.php">Transitions In Design</a></h4>
                                                <div class="meta"> <span><i>Feb</i> 16,2016</span> <span><i>In</i> <a href="#">Shopping</a></span> <span><i>Tags</i> <a href="#">Trends</a></span> </div>
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority .</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 blog-item">
                                        <div class="blog-article">
                                            <div class="post-format"> <span class="post-format-icon"><i class="lnr lnr-picture"></i></span> </div>
                                            <div class="comment-like"> <span><i class="fas fa-comment" aria-hidden="true"></i> 30</span> <span><i class="fas fa-heart" aria-hidden="true"></i> 15</span> </div>
                                            <div class="article-img">
                                                <a href="chi-tiet-tin-tuc.php"> <img src="img/5.jpg" class="img-responsive" alt=""></a>
                                            </div>
                                            <div class="article-link"> <a href="chi-tiet-tin-tuc.php"><i class="lnr lnr-arrow-right"></i></a> </div>
                                            <div class="article-content">
                                                <h4><a href="chi-tiet-tin-tuc.php">Comfort classy outfits</a></h4>
                                                <div class="meta"> <span><i>Feb</i> 16,2016</span> <span><i>In</i> <a href="#">Shopping</a></span> <span><i>Tags</i> <a href="#">Trends</a></span> </div>
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority .</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 blog-item">
                                        <div class="blog-article">
                                            <div class="post-format"> <span class="post-format-icon"><i class="lnr lnr-film-play"></i></span> </div>
                                            <div class="comment-like"> <span><i class="fas fa-comment" aria-hidden="true"></i> 30</span> <span><i class="fas fa-heart" aria-hidden="true"></i> 15</span> </div>
                                            <div class="article-img">
                                                <a href="chi-tiet-tin-tuc.php"> <img src="img/6.jpg" class="img-responsive" alt=""></a>
                                            </div>
                                            <div class="article-link"> <a href="chi-tiet-tin-tuc.php"><i class="lnr lnr-arrow-right"></i></a> </div>
                                            <div class="article-content">
                                                <h4><a href="chi-tiet-tin-tuc.php">Recent trends in story</a></h4>
                                                <div class="meta"> <span><i>Feb</i> 16,2016</span> <span><i>In</i> <a href="#">Shopping</a></span> <span><i>Tags</i> <a href="#">Trends</a></span> </div>
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority .</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-6 blog-item">
                                        <div class="blog-article">
                                            <div class="post-format"> <span class="post-format-icon"><i class="lnr lnr-music-note"></i></span></div>
                                            <div class="comment-like"> <span><i class="fas fa-comment" aria-hidden="true"></i> 30</span> <span><i class="fas fa-heart" aria-hidden="true"></i> 15</span> </div>
                                            <div class="article-img">
                                                <a href="chi-tiet-tin-tuc.php"> <img src="img/7.jpg" class="img-responsive" alt=""></a>
                                            </div>
                                            <div class="article-link"> <a href="chi-tiet-tin-tuc.php"><i class="lnr lnr-arrow-right"></i></a> </div>
                                            <div class="article-content">
                                                <h4><a href="chi-tiet-tin-tuc.php">Social media websites</a></h4>
                                                <div class="meta"> <span><i>Feb</i> 16,2016</span> <span><i>In</i> <a href="#">Shopping</a></span> <span><i>Tags</i> <a href="#">Trends</a></span> </div>
                                                <p>There are many variations of passages of Lorem Ipsum available, but the majority .</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pagination-nav nav-center">
                                    <a href="#" class="btn btn-prev"><i class="lnr lnr-arrow-left"></i> prev</a> 
                                    <a href="#" class="btn btn-next">next <i class="lnr lnr-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /Blog Subpage -->
                </div>
                <!-- /Page changer wrapper -->
            </div>
            <!-- /Main Content -->
@endsection

