@extends('vn3ctheme.layout.site')

@section('type_meta', 'article')
@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('meta_image', asset($product->image) )
@section('meta_url', route('product', [ 'post_slug' => $product->slug]) )

@section('content')
    <!-- Main Content Pages -->
	<div class="content-pages pd20">
		<!-- Subpages -->
		<div class="sub-home-pages">
			<!-- Titlebar -->
			<div id="titlebar">
				<div class="container">
					<div class="special-block-bg">
						<h2>F-FASHION</h2>
						<!-- Breadcrumbs -->
						<nav id="breadcrumbs">
							<ul>
								<li><a href="#"><i class="lnr lnr-home"></i> Trang chủ</a></li>
								<li><a href="#">Kho giao diện</a></li>
								<li>F-Fashion</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<!-- /Titlebar -->

			<!-- Content -->
			<div class="container detail-container">
				<div class="section">
					<div class="row theme-intro">
						<div class="col-lg-4">
							<h1>Template, Giao diện mẫu <span>Zomart</span></h1>
								<div class="desc">
									Giao diện mẫu mang phong cách hiện đại với những màu sắc tươi mới đem đến cho người dùng cảm giác thân thiện. Giao diện này sẽ đặc biệt phù hợp với những cửa hàng có bán nhiều sản phẩm đặc biệt là...
								</div>
							<div class="other-info clearfix">
								<span class="price float-left ">
									<b>1,499,000 VNĐ</b>
								</span>
							</div>
							<div class="theme-action">
								<a href="dang-ky.php" data-toggle="modal" data-target="#ModalSetup" class="install-theme">Sử dụng giao diện này</a>
								<a href="" class="view-demo action-preview-theme" data-url="">Xem trước giao diện</a>
							</div>
						</div>
						<div class="col-lg-8 theme-image">
							<div class="image-desktop d-none d-md-block">
									<a href="/demo/zomart" target="_blank" class="action-preview-theme" data-url="">
										<img src="https://bizwebtheme.dktcdn.net/themes/2366/themestores/4f9a5235e7cc28a1e8cafd983720bbe7.png?1548812679020">
									</a>
							</div>
							<div class="image-mobile">
								<a href="/demo/zomart?mobile=true" target="_blank" class="action-preview-theme" data-url="https://zomart.bizwebvietnam.net/">
									<img src="https://bizwebtheme.dktcdn.net/themes/2366/themestores/c36f44c4abde04ed383264dabc12594a.png?1548812679020">
								</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="text-block pt-50">
								<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. </p>
								<p>here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary </p>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="block-centered pt-50"> 
								<img class="img-responsive" src="img/portfolio-single-img-2.png" alt="">
							</div>
						</div>
					</div>
					<div class="row create-website">
						<div class="col-lg-8 intro">
							<h4>Bạn chưa chọn được giao diện? Hãy đăng ký tạo website cho thương hiệu riêng của mình</h4>
						</div>
						<div class="col-lg-4">
							<a href="dang-ky.php" class="btn-registration">Đăng ký</a>
						</div>
					</div>
				</div>
				<div class="related-projects pb-50 pt-50">
					<h4>XEM THÊM CÁC MẪU GIAO DIỆN CÙNG NGÀNH NGHỀ</h4>
					<div class="clearfix portfolio-grid pt-30">
						<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 portfolio-item branding photography all">
							<div class="portfolio-img">
								<img src="img/portfolio-img-1.jpg" class="img-responsive" alt="">
							</div>
							<div class="portfolio-data">
								<h4><a href="chi-tiet-giao-dien.php">F-FASHION</a></h4>
								<p class="meta">1.500.000đ</p>
								<div class="portfolio-attr"> 
									<a class="btn" href="chi-tiet-giao-dien.php">Chi tiết</a>
									<a class="btn btnOrange" href="chi-tiet-giao-dien.php">Demo</a>		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- #content -->
		</div>
		<!-- /Page changer wrapper -->
	</div>
	<!-- /Main Content -->
@endsection
