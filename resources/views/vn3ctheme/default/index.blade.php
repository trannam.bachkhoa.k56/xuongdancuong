@extends('vn3ctheme.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )
@section('type_meta', 'website')

@section('content')
    <!-- Main Content Pages -->
    <div class="content-pages">
        <!-- Subpages -->
        <div class="sub-home-pages">
            <!-- Start Page home -->
            <section id="home" class="sub-page start-page">
                <div class="sub-page-inner" style="background: url('/assets/vn3ctheme/img/home-bg.jpg');">
                    <div class="mask"></div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="title-block">
                                <h2>{{ isset($information['tieu-de-trang-chu']) ? $information['tieu-de-trang-chu'] : '' }}</h2>
                                <div class="type-wrap">
                                    <div class="typed-strings">
                                        @foreach (\App\Entity\SubPost::showSubPost('slogan-trang-chu') as $slogan)
                                            <span>{{ $slogan->title }}</span>
                                        @endforeach
                                    </div>
                                    <span class="typed" style="white-space:pre;"></span>
                                </div>
                                <div class="home-buttons">
                                    <a  href="#" class="bt-submit"><i class="lnr lnr-envelope"></i>
                                        {{ isset($information['button-thu-1-trang-chu']) ? $information['button-thu-1-trang-chu'] : '' }}
                                    </a>
                                    <a  href="#" class="bt-submit"><i class="lnr lnr-briefcase"></i>
                                        {{ isset($information['button-thu-2-trang-chu']) ? $information['button-thu-2-trang-chu'] : '' }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /Start Page home -->
        </div>
        <!-- /Page changer wrapper -->
    </div>
    <!-- /Main Content -->
@endsection

