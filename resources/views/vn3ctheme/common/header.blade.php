<!-- Header -->
<header class="header">
    <div class="header-content">
        <div class="profile-picture-block">
            <div class="my-photo">
                <img src="{{ isset($information['logo']) ? $information['logo'] : '' }}" class="img-fluid" alt="{{ isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' }}">
            </div>
        </div>
        <!-- Header Head -->
        <div class="site-title-block">
            <div class="site-title">{{ isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' }}</div>
            <div class="type-wrap">
                <div class="typed-strings">
                    @foreach (\App\Entity\SubPost::showSubPost('slogan-trang-chu') as $slogan)
                    <span>{{ $slogan->title }}</span>
                    @endforeach
                </div>
                <span class="typed" style="white-space:pre;"></span>
            </div>
        </div>
        <!-- /Header Head -->

        <!-- Navigation -->
        <div class="site-nav">
            <!-- Main menu -->
            <ul class="header-main-menu" id="header-main-menu">
                @foreach (\App\Entity\Menu::showWithLocation('menu-product') as $mainMenu)
                    @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
                    <li>
                        <a href="{{ $menuElement['url'] }}"><i class="fas fa-angle-double-right"></i> {{ $menuElement['title_show'] }}</a>
                    </li>
                    @endforeach
                @endforeach
            </ul>
            <!-- /Main menu -->

            <!-- Copyrights -->
            <div class="copyrights">© 2019 VN3C.,JSC All rights reserved.</div>
            <!-- / Copyrights -->
        </div>
        <!-- /Navigation -->
    </div>
</header>
<!-- /Header -->
<nav class="Navigation">
	<ul>
		@foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $mainMenu)
			@foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuElement)
			<li class="active">
				<a href="{{ $menuElement['url'] }}">{{ $menuElement['title_show'] }}</a>
			</li>
			@endforeach
		@endforeach
	</ul>
</nav>
<!-- Mobile Header -->
<div class="responsive-header">
    <div class="responsive-header-name">
        <img class="responsive-logo" src="{{ isset($information['logo']) ? $information['logo'] : '' }}" alt="{{ isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' }}" />
    </div>
    <span class="responsive-icon"><i class="lnr lnr-menu"></i></span>
</div>
<!-- /Mobile Header -->