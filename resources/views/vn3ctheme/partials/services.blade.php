<!-- services -->
<div class="special-block-bg">
	<div class="section-head">
		<h4>
			<span>VN3C Giúp gì cho doanh nghiệp của bạn?</span>
			Dịch vụ của chúng tôi
		</h4>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="services-list">
				<!-- Service Item 1 -->
				<div class="service-block">
					<div class="service-icon">
						<i class="lnr lnr-code"></i>
					</div>
					<div class="service-text">
						<h4>Phần mềm quản lý doanh nghiệp</h4>
						<p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales odio.</p>
					</div>
				</div>
				<!-- /Service Item 1 -->

				<!-- Service Item 2 -->
				<div class="service-block">
					<div class="service-icon">
						<i class="lnr lnr-laptop-phone"></i>
					</div>
					<div class="service-text">
						<h4>Thiết kế website</h4>
						<p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales odio.</p>
					</div>
				</div>
				<!-- Service Item 2 -->
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="services-list">
				<!-- Service Item 3 -->
				<div class="service-block">
					<div class="service-icon">
						<i class="lnr lnr-mic"></i>
					</div>
					<div class="service-text">
						<h4>Thiết kế nhận dạng thương hiệu</h4>
						<p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales odio.</p>
					</div>
				</div>
				<!-- Service Item 3 -->

				<!-- Service Item 4 -->
				<div class="service-block">
					<div class="service-icon">
						<i class="lnr lnr-camera"></i>
					</div>
					<div class="service-text">
						<h4>Giải pháp marketing đa kênh</h4>
						<p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales odio.</p>
					</div>
				</div>
				<!-- Service Item 4 -->
			</div>
		</div>
	</div>
</div>
<!-- /services -->