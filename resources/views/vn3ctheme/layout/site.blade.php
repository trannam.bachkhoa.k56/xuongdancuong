<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta name="ROBOTS" content="index, follow"/>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="title" content="@yield('title')"/>
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta itemprop="image" content="@yield('meta_image')" />
    <!-- facebook gooogle -->
    <!-- <meta property="fb:app_id" content="" />
    <meta property="fb:admins" content=""> -->
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:locale" content="vi_VN"/>
	<meta property="og:type" content="@yield('type_meta')"/>

    <meta property="og:title" content="@yield('title')" />
    <meta property="og:description" content="@yield('meta_description')" />
    <meta property="og:url" content="@yield('meta_url')" />
    <meta property="og:image" content="@yield('meta_image')" />
    <meta property="og:image:secure_url" content="@yield('meta_image')" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="@yield('meta_description')" />
    <meta name="twitter:title" content="@yield('title')" />
    <meta name="twitter:image" content="@yield('meta_image')" />

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="Mulan / CV Template + RTL" />
        <meta name="keywords" content="vcard, resposnive, retina, resume, jquery, css3, bootstrap, portfolio" />
        <meta name="author" content="Marwa El-Manawy" />
        <link rel="icon" href="favicon.ico">
        <title>CÔNG TY CỔ PHẦN CÔNG NGHỆ VN3C VIỆT NAM</title>

        <!-- <meta name="google-site-verification" content="RYNcHP0p0DC3jEmH4EuJTdIEBN9VnG-gLGtFMrWPoRs" />-->

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/bootstrap.min.css') }}" type="text/css">
        <!-- Font icons -->
        <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/font-awesome.min.css') }}" type="text/css">
        <!-- Animation -->
        <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/animate.css') }}" type="text/css">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/owl.carousel.css') }}" type="text/css">
        <!-- Light Case -->
        <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/lightcase.css') }}" type="text/css">
        <!-- Template style -->
        <link rel="stylesheet" href="{{ asset('assets/vn3ctheme/css/main.css') }}" type="text/css">
    </head>
	<!-- End Facebook Pixel Code -->

    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: '{!! route('addToCart') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    window.location.replace("/gio-hang");
                },
                error: function(error) {
                    console.log(error);
                }

            });

            return false;
        }
        function contact(e) {
            var $btn = $(e).find('button').button('loading');
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: '{!! route('sub_contact') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    // gửi thành công
                    if (obj.status == 200) {
                        alert(obj.message);
                        $btn.button('reset');

                        return;
                    }

                    // gửi thất bại
                    if (obj.status == 500) {
                        alert(obj.message);
                        $btn.button('reset');

                        return;
                    }
                },
                error: function(error) {
                    //alert('Lỗi gì đó đã xảy ra!')
                }

            });


            return false;
        }
    </script>

    <head>
<body>

    <div class="wrapper-page">
        @include('vn3ctheme.common.header')

        <!-- Phần nội dung -->
        @yield('content')


    </div>

    <!--JS Files-->
    <script src="{{ asset('assets/vn3ctheme/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vn3ctheme/js/bootstrap.min.js') }}"></script>
    <!--Owl Coursel-->
    <script src="{{ asset('assets/vn3ctheme/js/owl.carousel.min.js') }}"></script>
    <!-- Typing Text -->
    <script src="{{ asset('assets/vn3ctheme/js/typed.js') }}"></script>
    <!--Images LightCase-->
    <script src="{{ asset('assets/vn3ctheme/js/lightcase.js') }}"></script>
    <!-- Portfolio filter -->
    <script src="{{ asset('assets/vn3ctheme/js/jquery.isotope.js') }}"></script>
    <!-- Wow Animation -->
    <script src="{{ asset('assets/vn3ctheme/js/wow.js') }}"></script>
    <!-- Map -->
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyBkdsK7PWcojsO-o_q2tmFOLBfPGL8k8Vg&amp;language=en"></script>
    <!-- Main Script -->
    <script src="{{ asset('assets/vn3ctheme/js/main.js') }}"></script>
</body>
</html>