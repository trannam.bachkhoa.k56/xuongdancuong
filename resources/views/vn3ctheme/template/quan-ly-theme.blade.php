@extends('vn3c.layout.site')


@section('title', 'Quản lý theme đã tạo')
@section('meta_description',  $information['meta_description'] )
@section('keywords', '')

@section('content')
    @include('vn3c.partials.slide')

    <section class="Product bgGray pr-category">
        <div class="container">
            <div class="listProduct">
                <div class="tab-content" id="ListProduct">
                    @if (!\Illuminate\Support\Facades\Auth::check())
                        <div class="row product-row">
                            <div class="col-md-12 col-sm-12 item">
                                Bạn chưa đăng nhập vui lòng đăng nhập để được xem thông tin của mình
                            </div>
                        </div>
                    @else
                        <div class="row product-row">
                            <?php
                                $user = \Illuminate\Support\Facades\Auth::user();
                            $domains = \App\Entity\Domain::leftjoin('themes', 'themes.theme_id', '=', 'domains.theme_id')
                                    ->select(
                                        'themes.theme_id',
                                        'themes.image',
                                        'domains.name',
                                        'domains.url'
                                    )
                                    ->where('user_id', $user->id)->get();
                            ?>
                            @foreach ($domains as $id => $domain)
                                <div class="col-md-4 col-sm-12 item">
                                    <figure class="pro-item">
                                        <a href="{{ $domain->url }}">
                                            <img src="{{ asset($domain->image) }}"/>
                                        </a>
                                        <figcaption>
                                            <div class="button">
                                                <button type="button" class="btn btn-warning">
                                                    <a href="{{ $domain->url }}">Xem website</a>
                                                </button>
                                                <button type="button" class="btn btn-primary">
                                                    <a href="{{ $domain->url }}/admin">Quản trị website</a>
                                                </button>
                                            </div>
                                        </figcaption>
                                        <h3>
                                            <a href="{{ $domain->url }}">{{ $domain->name }}</a>
                                        </h3>
                                    </figure>
                                </div>
                            @endforeach
                        </div>
                    @endif



                </div>
            </div><!-- end listproduct -->

            <div class="show-pr">
                <a href="" title="" class="hvr-radial-in">Xem thêm</a>
            </div>

        </div>
    </section>
@endsection
