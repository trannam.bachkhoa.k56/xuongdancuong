@extends('vn3ctheme.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '' )
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '' )
@section('type_meta', 'website')

@section('content')
    <!-- Main Content Pages -->
    <div class="content-pages">
        <!-- Subpages -->
        <div class="sub-home-pages">
           <!-- About Me Subpage -->
			<section id="about-me" class="sub-page">
				<div class="sub-page-inner">
					<div class="section-title">
						<div class="main-title">
							<div class="title-main-page">
								<h4>VN3C</h4>
								<p>Giới thiệu về công ty cổ phần công nghệ VN3C Việt Nam</p>
							</div>
						</div>
					</div>
					<div class="section-content">
						<!-- about me -->
						<div class="row pb-30">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h3>CÔNG TY CỔ PHẦN CÔNG NGHỆ VN3C VIỆT NAM</h3>
								<span class="about-location"><i class="lnr lnr-map-marker"></i> Tầng 3, số nhà 9B, Lê Văn Thiêm, Hà Nội</span>
								<p class="about-content">
									{!! isset($information['gioi-thieu-ve-cong-ty']) ? $information['gioi-thieu-ve-cong-ty'] : '' !!}
								</p>
								<ul class="bout-list-summry row">
									<li class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="icon-info">
											<i class="lnr lnr-briefcase"></i> 
										</div>
										<div class="details-info">
											<h6>9 Năm</h6>
											<p>Kinh nghiệm</p>
										</div>
									</li>
									<li class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="icon-info">
											<i class="lnr lnr-layers"></i> 
										</div>
										<div class="details-info">
											<h6>300+</h6>
											<p>Dự án</p>
										</div>
									</li>
									<li class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="icon-info">
											<i class="lnr lnr-coffee-cup"></i> 
										</div>
										<div class="details-info">
											<h6>1500+</h6>
											<p>Khách hàng</p>
										</div>
									</li>
								</ul>
							</div>

							<div class="col-xs-6 col-sm-12 col-md-6 col-lg-6">
								<div class="box-img">
									<img src="{{ asset('assets/vn3ctheme/img/about.png') }}" class="img-fluid" alt="image">
								</div>
							</div>
						</div>
						<!-- /about me -->

						@include('vn3ctheme.partials.services')
						@include('vn3ctheme.partials.customer_say')
						@include('vn3ctheme.partials.testemorial')
					</div>
				</div>
			</section>
			<!-- About Me Subpage -->
        </div>
        <!-- /Page changer wrapper -->
    </div>
    <!-- /Main Content -->
@endsection

