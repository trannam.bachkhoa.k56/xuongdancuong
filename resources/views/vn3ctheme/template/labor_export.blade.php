<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Đăng ký xuất khẩu lao động</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('laborExport/css/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" href="{{ asset('laborExport/style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('adminstration/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminstration/font-awesome/css/font-awesome.min.css')}}">

    <!-- jQuery 3 -->
    <script src="{{ asset('adminstration/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('adminstration/bootstrap/dist/js/bootstrap.min.js') }}"></script>
</head>

<body id="page-top">

<header class="masthead">
    <div class="container h-100">
        <div class="row">
            <div class="col-xs-12 col-md-2">
                <p style="text-align: center;">
                    <img src="{{ asset('laborExport/img/halasuco.jpg') }}" alt="logo" width="200"/>
                </p>
            </div>
            <div class="col-xs-12 col-md-10">
                <div class="titleHeader">
                    <h2 class="title" style="text-align: center;">VÌ SAO PHẢI ĐI LÀM THUÊ? </h2>
                    <h1 style="color: #365899; text-align: center;">Mà không đầu tư đi <span style="color: red;">xuất khẩu lao động</span> để trở về thành <span style="color: red;">ông(bà)</span> chủ?</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="camKet">
    <div class="container h-100">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <img src="{{ asset('laborExport/img/free_course.jpg') }}" alt="logo" />
            </div>
            <div class="col-xs-12 col-md-7">
                <h3 style="color: red;"><b>HALASUCO</b></h3>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Cam kết 100% bay.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Công việc và thu nhập đúng với cam kết.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Thời gian học ngắn, chi phí xuất cảnh siêu rẻ.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Có nhiều đơn hàng về thực phẩm, nông nghiệp, cơ khí.</p>
                <p><i class="fa fa-snowflake-o" aria-hidden="true"></i> Có bất kể vấn đề nào không đúng với hợp đồng, công ty cam kết HOÀN TIỀN 100% chi phí.</p>
            </div>
        </div>
    </div>
</div>

<div class="introduction">
    <div class="container h-100">
        <p style="text-align: center"><h3 style="color: red;"><b>HALASUCO được phép đăng ký luôn, đỗ mới học</b></h3></p>
        <div class="row">
            <div class="col-xs-12 col-md-5 col-md-offset-1 col-xs-offset-0">
                <p><i class="fa fa-history" aria-hidden="true"></i> 25 năm phát triển: từ năm 1993 - 2018.</p>
                <p><i class="fa fa-users" aria-hidden="true"></i> 210 thực tập sinh xuất cảnh năm 2017.</p>
            </div>
            <div class="col-xs-12 col-md-5 col-md-offset-1 col-xs-offset-0">
                <p><i class="fa fa-building" aria-hidden="true"></i> 20 công ty Nhật Bản hợp tác 2017.</p>
                <p><i class="fa fa-user-o" aria-hidden="true"></i> 100% Khách hàng hài lòng.</p>
            </div>
        </div>
    </div>
</div>

<section class="service">
    <div class="container h-100">
        <h1>Hoạt động tại công ty</h1>
        <div class="row">
            <div class="col-xs-6 col-md-4">
                <img src="{{ asset('/laborExport/img/product/a2.jpg') }}"/>
            </div>
            <div class="col-xs-6 col-md-4">
                <img src="{{ asset('/laborExport/img/product/a11.jpg') }}"/>
            </div>
            <div class="col-xs-6 col-md-4">
                <img src="{{ asset('/laborExport/img/product/b3.jpg') }}"/>
            </div>
            <div class="col-xs-6 col-md-4">
                <img src="{{ asset('/laborExport/img/product/b5.jpg') }}"/>
            </div>
            <div class="col-xs-6 col-md-4">
                <img src="{{ asset('/laborExport/img/product/b7.jpg') }}"/>
            </div>
            <div class="col-xs-6 col-md-4">
                <img src="{{ asset('/laborExport/img/product/c6.jpg') }}"/>
            </div>
        </div>
    </div>
</section>

<section class="register">
    <div class="container h-100">
        <h3>Đăng ký nhận tài liệu: </h3>
        <p><i>Trân thành cảm ơn!</i></p>
        <div class="row">
            <div class="col-xs-12 col-md-12 item">
                <div id="getfly-optin-form-iframe-1520866864774"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=OXpOUS5Rj1nMhqyK2I8bV1Z0P8uLcb03GMpUw8Nch1Ejj4aRQA&referrer="+r); f.style.width = "100%";f.style.height = "500px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1520866864774");s.appendChild(f); })(); </script>
            </div>
        </div>
    </div>
</section>

<div class="Notification" id="popupVitural">
    <div class="Closed"><i class="fa fa-times" aria-hidden="true"></i></div>
    <div class="Content">
        <h3>Đăng ký nhận báo giá</h3>
        <div id="getfly-optin-form-iframe-1520868111247"></div> <script type="text/javascript"> (function(){ var r = window.document.referrer != ""? window.document.referrer: window.location.origin; var f = document.createElement("iframe");f.setAttribute("src", "https://vn3c.getflycrm.com/api/forms/viewform/?key=EW0FNsa9FBOCtcooexONuhuKtSczJHH3zZlsGlof4BK4Fp65W8&referrer="+r); f.style.width = "100%";f.style.height = "500px";f.setAttribute("frameborder","0");f.setAttribute("marginheight","0"); f.setAttribute("marginwidth","0");var s = document.getElementById("getfly-optin-form-iframe-1520868111247");s.appendChild(f); })(); </script>
    </div>

</div>
<script>
    $(document).ready(function() {
        $('#popupVitural').hide();
        if ($(window).width() > 768) {
            setInterval(function(){
                $('#popupVitural').show().slideDown();
            }, 5000);
        }


        $('#popupVitural .Closed').click(function(){
            $('#popupVitural').slideUp().hide();
            $('#popupVitural').addClass('hide');
        });
    });
</script>

</body>

</html>
