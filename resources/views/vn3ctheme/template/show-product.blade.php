
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xem nhanh website</title>
    <link rel="stylesheet" href="{{ asset('vn3c/viewTemplate/css/mobile.css') }}">
    <link rel="stylesheet" href="{{ asset('vn3c/viewTemplate/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('vn3c/viewTemplate/css/bootstrap.min.css') }}">
    <style>
        body{
            font-family:arial;
            font-size: 12px;
        }

        body a{
            outline:none;
        }

        .hide{
            display:none;
        }
        #btnDownload{
            background-color:#E31659;
        }
        #btnDownload:before{
            background-color:#C142AC;
        }
        #showHtml{
            position:relative;
            z-index:-10;
            width:0%;
        }
        .mobile-only
        {
            display: none !important;
        }
        .screen-only
        {
            display: block;
        }
        @media screen and (max-width: 980px)
        {
            .screen-only
            {
                display: none !important;
            }

            .mobile-only
            {
                display: block !important;
            }
        }
        .mobile-resize{
            background:url({{ asset('vn3c/image/bg-pattern1-mobile.png') }});
        }
        .mobile-resize #mobile-scrollbar .scrollbar-handle {
            position: relative;
            background: #81cdff;
            border: 1px solid #cccccc;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            height: 95px;
            top: 0px;
        }
        .mobile-resize #mobile-scrollbar {
            position: absolute;
            background: rgba(225,225,225,.125);
            border: 1px solid rgba(215,215,215,.125);
            width: 10px;
            height: 100%;
            top: 0;
            left: 100%;
            padding-bottom: 1px;
            box-shadow: inset 0 1px 1px rgba(182,181,181,.5);
            -moz-box-shadow: inset 0 1px 1px rgba(182,181,181,.5);
            -webkit-box-shadow: inset 0 1px 1px rgba(182,181,181,.5);
            height: 480px;
            top: 50px;
            left: 50%;
            margin-left: 184px;
        }
        .mobile-resize #demo-wrapper{
            position:relative;
        }
        .mobile-resize #demo-container{
            background:url({{ asset('vn3c/image/mobile_device.png') }}) no-repeat;
            min-height:620px;
        }
        .mobile-resize #frame{
            width: 320px;
            top: 32px;
            left: 16px;
            height: 480px;
        }
        .mobile-resize #demo-wrapper{
            top:30px !important;
        }
        @media (max-width:900px){
            .mobile-resize #demo-container{
                background:none;
            }
        }
    </style>
    <script type="text/javascript" src="{{ asset('vn3c/viewTemplate/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vn3c/viewTemplate/js/popup.js') }}"></script>
    <script src="{{ asset('js/numeral.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('vn3c/viewTemplate/css/popup.css') }}">
    <script type="text/javascript">
        $(document).ready(function(){
            $('.col2 a.desktop').click(function(e){
                e.preventDefault();
                $('.col2 a').removeClass('current');
                $(this).addClass('current');
                $('#demo-container').css({'max-width':'100%','max-height':'100%','margin':'0px','top':'0px','left':'0px'});
                $('#view-demo').removeClass('mobile-resize');
                $('#mobile-scrollbar').remove();
            });
            $('.col2 a.mobile').click(function(e){
                e.preventDefault();
                $('.col2 a').removeClass('current');
                $(this).addClass('current');
                $('#demo-container').css({'max-width':'360px','max-height':'569px','margin':'-311px 0px 0px -160px','top':'50%','left':'50%'});
                $('#view-demo').addClass('mobile-resize');
            });
        });
    </script>
</head>
<body>
<div id="view-demo">
    <div class="demo-header">
        <div class="col col1" style="text-align: center; ">
            <div class="screen-only">
              <a href="#" class="" style="color: gray; font-size: 22px;">
                  @if (isset($product) && !empty($product->discount))
                      <span class="priceOld">{{ number_format($product->price, 0, ',', '.') }}</span> 
                      <span class="priceDiscount">{{ number_format($product->discount, 0, ',', '.') }}</span> VNĐ
                  @elseif (empty($product->price))
                      <span class="priceDiscount">Miễn phí</span>
                  @else
                      <span class="priceDiscount">{{ number_format($product->price, 0, ',', '.') }}</span> VNĐ
                  @endif
              </a>
            </div>
        </div>
        <div class="col col2">
            <a class="desktop current screen-only" href="javascript:void(0)">
                <svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                    <path d="M14 0H2C.897 0 0 .897 0 2v8c0 1.103.897 2 2 2h4.667v2h-2c-.552 0-1 .447-1 1 0 .553.448 1 1 1h6.667c.552 0 1-.447 1-1 0-.553-.448-1-1-1h-2v-2H14c1.104 0 2-.897 2-2V2c0-1.103-.896-2-2-2zM2 10V2h12v8H2z"></path>
                </svg>
                Xem giao diện máy tính
            </a>
            <a class="mobile screen-only" href="javascript:void(0)">
                <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18.7 0H6.3C5.6 0 5 .6 5 1.3v21.3c0 .8.6 1.4 1.3 1.4h12.3c.7 0 1.4-.587 1.4-1.287V1.3c0-.7-.6-1.3-1.3-1.3zm-6.2 22.6c-.7 0-1.3-.6-1.3-1.3 0-.7.6-1.3 1.3-1.3.7 0 1.3.6 1.3 1.3 0 .7-.6 1.3-1.3 1.3zm4.5-4c0 .2-.2.4-.4.4H8.4c-.2 0-.4-.2-.4-.4V3.4c0-.2.2-.4.4-.4h8.1c.3 0 .5.2.5.4v15.2z"></path>
                </svg>
                Xem giao diện Mobile
            </a>
        </div>
        <div class="col col3">
            <a class="screen-only" style="display:none;">Mã #19361</a>
            <a id="myBtn" href="javascript:void(0)" class="btn-registration screen-only">Đăng ký sửa dụng ngay</a>
            <a class="view-normal hide">
                X
            </a>
        </div>
    </div>
    <!-- End demo-header-->
    <div id="demo-wrapper" class="desktop-view">
        <div id="demo-container">
            <iframe id="frame" src="{{ isset($product['link-demo-san-pham']) ? $product['link-demo-san-pham'] : ''  }}"></iframe>
        </div>
    </div>
    <!-- End demo-wrapper -->
</div>

<!--modal-->
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <h2>Đăng ký nhanh tay, sở hữu ngay</h2>
        </div>
        <div class="modal-body">
            <h3>Gửi thông tin đăng ký</h3>
            <form action="{{ route('sub_contact') }}" method="post">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="exampleInputEmail1">Họ và tên: </label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="name" aria-describedby="emailHelp" placeholder="Nhập họ và tên của bạn">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email: </label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Nhập email của bạn">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Số điện thoại: </label>
                    <input type="number" class="form-control" id="exampleInputEmail1" name="phone" aria-describedby="emailHelp" placeholder="Số điện thoại">
                </div>
                <div class="ip-textarea">
                    <input type="hidden" class="form-control" id="exampleInputEmail1" name="message" value="{!! $product->code !!}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Gửi thông tin</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end modal-->

</body>
</html>