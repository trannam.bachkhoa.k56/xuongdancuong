
         <!-- #main -->

         <div class="fusion-footer">
            <footer role="contentinfo" class="fusion-footer-widget-area fusion-widget-area">
               <div class="fusion-row">
                  <div class="fusion-columns fusion-columns-4 fusion-widget-area">
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-2" class="fusion-footer-widget-column widget widget_text">
                           <div class="textwidget"><img src="{{ isset($information['logo']) ? asset($information['logo']) : '' }}" alt="Logo" /><br><br/>
                              <span style="font-family: UTM-Wedding; font-size: 25px; color: #d2bd56;">{!! isset($information['gioi-thieu-trang-chu']) ? $information['gioi-thieu-trang-chu'] : '' !!}</span>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="text-4" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Thông tin liên hệ</h4>
                           <div class="textwidget">
                              <p><i class="fontawesome-icon  fa fa-map-marker circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Địa chỉ: {{ isset($information['dia-chi']) ? $information['dia-chi'] : '' }}<br><br/>
                                 <i class="fontawesome-icon  fa fa-phone circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i>Số điện thoại: {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai'] : '' }}<br><br/>
                                 <i class="fontawesome-icon  fa fa-envelope circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Email: {{ isset($information['email']) ? $information['email'] : '' }}
                              </p>
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column col-lg-3 col-md-3 col-sm-3">
                        <section id="recent-posts-6" class="fusion-footer-widget-column widget widget_recent_entries">
                           <h4 class="widget-title">Tin tức mới nhất</h4>
                           <ul>

                              @foreach(\App\Entity\Post::categoryShow('tin-tuc',3) as $new)
                              <li>
                                 <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}">{{ isset($new->title) ? $new->title : '' }}</a>
                              </li>
                              @endforeach

                             
                           </ul>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-column fusion-column-last col-lg-3 col-md-3 col-sm-3">
                        <section id="text-5" class="fusion-footer-widget-column widget widget_text">
                           <h4 class="widget-title">Lịch làm việc</h4>
                           <div class="textwidget">
                              <p>Chúng tôi luôn hỗ trợ Quý khách 24/24<br>
                                 <i class="fontawesome-icon  fa fa-phone-square circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> Hotline: {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai'] : '' }}<br>
                                 <i class="fontawesome-icon  fa fa-fax circle-no" style="font-size:18px;margin-right:9px;color:#d2bd56;"></i> CSKH: {{ isset($information['cham-soc-khach-hang']) ? $information['cham-soc-khach-hang'] : '' }}
                              </p>
                              <h5 style="margin-top: 1em; margin-bottom: 0.5em;"><span style="font-weight: 400; color: #ddd;">GIỜ MỞ CỬA</span></h5>
                              {!! isset($information['gio-mo-cua']) ? $information['gio-mo-cua'] : '' !!}
                           </div>
                           <div style="clear:both;"></div>
                        </section>
                     </div>
                     <div class="fusion-clearfix"></div>
                  </div>
                  <!-- fusion-columns -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- fusion-footer-widget-area -->
            <footer id="footer" class="fusion-footer-copyright-area">
               <div class="fusion-row">
                  <div class="fusion-copyright-content">
                     <div class="fusion-copyright-notice">
                        <div>
                             {{ isset($information['copy-right']) ? $information['copy-right'] : '' }}
                        </div>
                     </div>
                  </div>
                  <!-- fusion-fusion-copyright-content -->
               </div>
               <!-- fusion-row -->
            </footer>
            <!-- #footer -->
         </div>