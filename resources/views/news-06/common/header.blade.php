<header class="fusion-header-wrapper fusion-header-shadow">
   <div class="fusion-header-v1 fusion-logo-left fusion-sticky-menu- fusion-sticky-logo- fusion-mobile-logo-1  fusion-mobile-menu-design-modern">
      <div class="fusion-header-sticky-height"></div>
      <div class="fusion-header">
         <div class="fusion-row">
            <div class="fusion-logo" data-margin-top="8px" data-margin-bottom="8px" data-margin-left="0px" data-margin-right="0px">
               <a class="fusion-logo-link"  href="{{ isset($domainUrl) ? $domainUrl : ''}} " >
                  <!-- standard logo -->
                  <img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}" srcset="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }} 1x" width="70" height="70" alt="Logo" class="fusion-standard-logo" style="width: 70px;" />
                  <!-- mobile logo -->
                  <img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}" srcset="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }} 1x" width="24" height="24" alt="logo" retina_logo_url="" class="fusion-mobile-logo" style="width: 24px;"/>
               </a>
            </div>
            <nav class="fusion-main-menu" aria-label="Main Menu">
               <ul role="menubar" id="menu-main" class="fusion-menu">
                     <li role="menuitem"  id="menu-item-13257"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-11799 current_page_item menu-item-13257 fusion-menu-item-button"  ><a  href="/" class="fusion-bar-highlight"><span class="menu-text fusion-button button-default button-medium"><span class="button-icon-divider-left"><i class="glyphicon  fa fa-home"></i></span><span class="fusion-button-text-left">Trang chủ</span></span></a></li>
					@foreach (\App\Entity\Category::getCategoryProduct() as $cateProduct)
						@if ($cateProduct->slug != 'san-pham-trang-chu')
                     <li role="menuitem"  id="menu-item-11974"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11974 fusion-dropdown-menu"  >
                        <a  href="/cua-hang/{{ $cateProduct->slug }}" class="fusion-bar-highlight"><span class="menu-text" style="text-transform: uppercase;">{{ $cateProduct->title  }}</span></a>
                        @if (!empty(\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product')))
						<ul role="menu" class="sub-menu">
							@foreach (\App\Entity\Category::getChildrenCategory($cateProduct->category_id, 'product') as $childProduct)     
								<li role="menuitem"  id="menu-item-13805"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13805 fusion-dropdown-submenu"  >
									<a  href="/cua-hang/{{ $childProduct->slug }}" class="fusion-bar-highlight"><span>{{ $childProduct->title  }}</span></a>
								</li>
							@endforeach 
                        </ul>
						@endif
                    </li>
						@endif
					@endforeach
					
                     <li role="menuitem"  id="menu-item-11974"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11974 fusion-dropdown-menu"  >
                        <a  href="/danh-muc/tin-tuc" class="fusion-bar-highlight"><span class="menu-text" style="text-transform: uppercase;">Tin tức</span></a>
                       @if (!empty(\App\Entity\Category::getChildrenCategory(0, 'post')))
						<ul role="menu" class="sub-menu">
							@foreach (\App\Entity\Category::getChildrenCategory(0, 'post') as $childProduct)   
								<li role="menuitem"  id="menu-item-13805"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13805 fusion-dropdown-submenu"  >
									<a  href="/cua-hang/{{ $childProduct->slug }}" class="fusion-bar-highlight"><span>{{ $childProduct->title  }}</span></a>
								</li>
							@endforeach 
                        </ul>
						@endif
                    </li>
						
					<li role="menuitem"  id="menu-item-11974"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-11974 fusion-dropdown-menu"  >
                        <a  href="/lien-he" class="fusion-bar-highlight"><span class="menu-text" style="text-transform: uppercase;">Liên hệ</span></a>	
					</li> 
					
                  <li class="fusion-custom-menu-item fusion-main-menu-search">
                     <a class="fusion-main-menu-icon fusion-bar-highlight" href="{{ $domainUrl }}" aria-label="Search" data-title="Search" title="Search"></a>
                     <div class="fusion-custom-menu-item-contents">
                        <form role="search" class="searchform fusion-search-form" method="get" action="{{ route('search_product') }}">
                           <div class="fusion-search-form-content">
                              <div class="fusion-search-field search-field">
                                 <label class="screen-reader-text" for="s">Search for:</label>
                                 <input type="text" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" name="word" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ...">
                              </div>
                              <div class="fusion-search-button search-button">
                                 <input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
                              </div>
                           </div>
                        </form>
                     </div>
                  </li>  
               </ul>
            </nav>
            <div class="fusion-mobile-menu-icons">
               <a href="#" class="fusion-icon fusion-icon-bars" aria-label="Toggle mobile menu" aria-expanded="false"></a>
            </div>
            <nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-left"></nav>
         </div>
      </div>
   </div>
   <div class="fusion-clearfix"></div>
</header>