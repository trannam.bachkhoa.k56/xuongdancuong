﻿<!DOCTYPE html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
   <!-- head -->
   <head>
      <!-- meta -->
   <meta charset="UTF-8" />
      <title>@yield('title')</title>
   <meta name="ROBOTS" content="index, follow" />
   <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
   <meta name="title" content="Artisan wines of the world" />

   <meta name="description" content="@yield('meta_description')" />
   <meta name="keywords" content="@yield('keywords')" />

   <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
   <base href="{{ isset($domainUrl) ? $domainUrl : ''}}">

   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <!--Start of Tawk.to Script-->
      <!--End of Tawk.to Script-->
      <link rel="shortcut icon" href="{{ !empty($information['icon-logo']) ?  asset($information['icon-logo']) : '' }}" />
      <!-- wp_head() -->

	<meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />
   
    <link rel="stylesheet" href="{{ asset('news-06/wp-content/plugins/contact-form-7/includes/css/styles.css') }}" media="all" type="text/css" />
    <link rel="stylesheet" href="{{ asset('news-06/wp-content/plugins/essential-grid/public/assets/css/settings.css') }}" media="all" type="text/css" />
    
     <link rel='stylesheet' id='tp-open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.9.8' type='text/css' media='all' />
     <link rel='stylesheet' id='tp-raleway-css'  href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.9.8' type='text/css' media='all' />
     <link rel='stylesheet' id='tp-droid-serif-css'  href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.9.8' type='text/css' media='all' />
     
    
    <link rel="stylesheet" href="{{ asset('news-06/wp-content/plugins/revslider/public/assets/css/settings.css') }}" media="all" type="text/css" />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
         .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
      </style>

      <link rel="stylesheet" href="{{ asset('news-06/wp-content/plugins/popups/public/assets/css/public.css') }}" media="all" type="text/css" />
      <lik rel="stylesheet" href="{{ asset('news-06/wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome.min.css') }}" media="all" type="text/css" />
      <link rel="stylesheet" href="{{ asset('news-06/wp-content/plugins/text-slider/public/../includes/font-awesome/css/font-awesome-ie7.min.css') }}" media="all" type="text/css" />
      <link rel="stylesheet" href="{{ asset('news-06/wp-content/plugins/text-slider/public/assets/css/public.css') }}" media="all" type="text/css" />
      <link rel="stylesheet" href="{{ asset('news-06/wp-content/themes/Tinh/assets/css/style.min.css') }}" media="all" type="text/css" />
      <link rel="stylesheet" href="{{ asset('news-06/wp-content/themes/Tinh-Child-Theme/style.css') }}" media="all" type="text/css" />
      <!--[if lte IE 9]>
      <link rel='stylesheet' id='avada-IE-fontawesome-css'  href='wp-content/themes/Tinh/includes/lib/assets/fonts/fontawesome/font-awesome.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <!--[if IE]>
      <link rel='stylesheet' id='avada-IE-css'  href='wp-content/themes/Tinh/assets/css/ie.min.css?ver=5.6.1' type='text/css' media='all' />
      <![endif]-->
      <link rel="stylesheet"  href="{{ asset('news-06/wp-content/tablepress-combined.min.css') }}" media="all" type="text/css" />
      <link rel="stylesheet" href="{{ asset('news-06/wp-content/uploads/fusion-styles/bf86efcf7a9a72a664aadb68abc7e27e.min.css') }}" media="all" type="text/css" />
      <style id='rocket-lazyload-inline-css' type='text/css'>
         .rll-youtube-player{position:relative;padding-bottom:56.23%;height:0;overflow:hidden;max-width:100%;background:#000;margin:5px}.rll-youtube-player iframe{position:absolute;top:0;left:0;width:100%;height:100%;z-index:100;background:0 0}.rll-youtube-player img{bottom:0;display:block;left:0;margin:auto;max-width:100%;width:100%;position:absolute;right:0;top:0;border:none;height:auto;cursor:pointer;-webkit-transition:.4s all;-moz-transition:.4s all;transition:.4s all}.rll-youtube-player img:hover{-webkit-filter:brightness(75%)}.rll-youtube-player .play{height:72px;width:72px;left:50%;top:50%;margin-left:-36px;margin-top:-36px;position:absolute;background:url(news-06/wp-content/plugins/wp-rocket/inc/front/img/play.png) no-repeat;cursor:pointer}
      </style>

       <script src="{{ asset('news-06/wp-includes/js/jquery/jquery.js') }}"></script>
       <script src="{{ asset('news-06/wp-content/plugins/essential-grid/public/assets/js/lightbox.js') }}"></script>
       <script src="{{ asset('news-06/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js') }}"></script>
       <script src="{{ asset('news-06/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js') }}"></script>
       <script src="{{ asset('news-06/wp-content/plugins/text-slider/public/assets/js/text-slider.min.js') }}"></script>

      
       <!-- 
      <script type='text/javascript' src='news-06/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script> -->

      <!-- Call Now Button 0.3.2 by Jerry Rietveld (callnowbutton.com) -->
      <style>#callnowbutton {display:none;} @media screen and (max-width:650px){#callnowbutton {display:block; position:fixed; text-decoration:none; z-index:2147483647;width:65px; height:65px; border-radius:50%; border-top:1px solid #2dc62d; bottom:15px; box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);left:20px;background:url() center/50px 50px no-repeat #009900;}}</style>
     
      <style>
         #text-slider {
         width: 100%;
         position: relative;
         font-family: 'Open Sans';
         font-size: 90px;
         font-weight: 600;
         line-height: 85px;
         height:auto;
         overflow:hidden;
         }
         #text-slider article {
         width:100%;
         position:absolute;
         top:0;
         left:0;
         }
         #text-slider span {  
         display: block;
         }
         #text-slider-controls {
         width: auto;
         height: auto;
         float:right;
         margin:3%;
         /*position: absolute;
         bottom: 0;
         right: 0;*/
         }
         /*   
         #text-slider-controls .prev {  
         float: right;
         }
         #text-slider-controls .next {  
         float: right;
         }
         */
         #text-slider-controls a {
         text-decoration: none;
         }
         .nav-color {
         color: #000;
         font-size:86px;
         }
         .nav-color:hover {
         color: #eee; 
         }
         #vongtron1{ background-size: 100% auto;background: #fff;} #text-slider{ line-height: 8%; width: 80%; margin: 0 auto;} #text-slider-controls{text-align: center; float: inherit; margin: 0px;} .nav-color { color: #fff; font-size: 16px; background: #143c56; padding: 7.5px 10px; line-height: 50px;}#text-slider article { width: 100%; position: absolute; top: 0; left: 0; font-size: 15px; line-height: 20px; font-family: FuturaBk; font-weight: 520; color: #333333; } @media all and (max-width: 920px) { #text-slider{width:100%;}} .content-slider .lSAction&gt;a { margin: 0 1px;} i.icon-chevron-right.nav-color:hover { background-color: #d2bd56; } i.icon-chevron-left.nav-color:hover { background-color: #d2bd56; }
         

      </style>
       <link rel="stylesheet"  href="{{ asset('news-06/css/style-thang.css') }}" media="all" type="text/css" />
     
     
      <script type="text/javascript">
         var doc = document.documentElement;
         doc.setAttribute('data-useragent', navigator.userAgent);
      </script>
     
 <style type="text/css">
        .fusion-header 
          {
            background-image: none !important;
            background-position: top center;
              background-repeat: no-repeat;
            background-attachment: fixed;
            background-color: rgba(0, 89, 153, 0.7) !important;
          } 
          .fusion-main-menu .fusion-widget-cart-counter > a:before, .fusion-main-menu > ul > li > a {
              color: #fff;
          }
          .fusion-mobile-current-nav-item > a, .fusion-mobile-nav-item a:hover, .fusion-mobile-nav-item.current-menu-item > a {
              background-color: #062d4ab3;
              color: #fff;
          }
      </style>
    
    
     
   </head>
   <!-- body -->
   <body data-rsssl=1 class="home page-template page-template-100-width page-template-100-width-php page page-id-11799 fusion-image-hovers fusion-body ltr fusion-sticky-header no-mobile-sticky-header no-mobile-slidingbar mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-left mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none fusion-search-form-classic fusion-avatar-square">
   {!! isset($information['google-analytics']) ? $information['google-analytics'] : '' !!}
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <div id="wrapper" class="">
         <div id="home" style="position:relative;top:-1px;"></div>
         @include('news-06.common.header')

       <link rel="stylesheet" href="news-06/css/bootstrap.min.css">
   <script src="news-06/js/popper.min.js"></script>
   <script src="news-06/js/bootstrap.bundle.min.js"></script>
   <script src="news-06/js/bootstrap.min.js"></script>
      <!-- SLIDER  -->

  
         <!-- #Header_bg -->
         @yield('content')
         <!-- mfn_hook_content_after --><!-- mfn_hook_content_after -->
         <!-- #Footer -->     
        
   
      @include('news-06.common.footer')
         <!-- fusion-footer -->
      </div>
      <!-- wrapper -->
   
      <!-- #Wrapper -->
      <!-- mfn_hook_bottom --><!-- mfn_hook_bottom -->
      <!-- wp_footer() -->
     
   
     
    <script src="{{ asset('news-06/wp-content/plugins/contact-form-7/includes/js/scripts.js') }}"></script>
    <script src="{{ asset('news-06/wp-content/plugins/popups/public/assets/js/public.js') }}"></script>
    <script type='text/javascript'>
         /* <![CDATA[ */
         var slider_options = [];
         /* ]]> */
      </script>
    <script src="{{ asset('news-06/wp-content/plugins/text-slider/public/assets/js/public.js') }}"></script>
    <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/modernizr.js') }}"></script>
    <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fitvids.js') }}"></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoGeneralVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>

      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-general.js') }}"></script>
     
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVideoVars = {"lightbox_video_width":"1280","lightbox_video_height":"720"};
         /* ]]> */
      </script>
       <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.ilightbox.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.mousewheel.js') }}"></script>


      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionLightboxVars = {"status_lightbox":"1","lightbox_gallery":"1","lightbox_skin":"mac","lightbox_title":"","lightbox_arrows":"1","lightbox_slideshow_speed":"5000","lightbox_autoplay":"","lightbox_opacity":"0.90","lightbox_desc":"","lightbox_social":"1","lightbox_deeplinking":"1","lightbox_path":"horizontal","lightbox_post_images":"","lightbox_animation_speed":"Fast"};
         /* ]]> */
      </script>
       <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-lightbox.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/imagesLoaded.js') }}"></script>
       <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/isotope.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/packery.js') }}"></script>
    

     
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaPortfolioVars = {"lightbox_behavior":"all","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","content_break_point":"800"};
         /* ]]> */
      </script>
         <script src="{{ asset('news-06/wp-content/plugins/fusion-core/js/min/avada-portfolio.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.infinitescroll.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-core/js/min/avada-faqs.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.cycle.js') }}"></script>

        

    
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTestimonialVars = {"testimonials_speed":"4000"};
         /* ]]> */
      </script>

      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-testimonials.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/cssua.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.waypoints.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-waypoints.js') }}"></script>

      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionAnimationsVars = {"disable_mobile_animate_css":"0"};
         /* ]]> */
      </script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-animations.js') }}"></script>


      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionEqualHeightVars = {"content_break_point":"800"};
         /* ]]> */
      </script>


      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-equal-heights.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-content-boxes.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countdown.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-countdown.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.countTo.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.appear.js') }}"></script>
     

      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCountersBox = {"counter_box_speed":"1000"};
         /* ]]> */
      </script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-box.js') }}"></script>
      <script src="{{ asset('news-06/js/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.modal.js') }}"></script>
      <script src="{{ asset('news-06/js/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-modal.js') }}"></script>
     


      
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionMapsVars = {"admin_ajax":"https:\/\/www.haiphatland.vn\/wp-admin\/admin-ajax.php"};
         /* ]]> */
      </script>

      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fusion_maps.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-google-map.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.fade.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.requestAnimationFrame.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-parallax.js') }}"></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoBgVars = {"status_vimeo":"0","status_yt":"1"};
         /* ]]> */
      </script>
       <script src="{{ asset('news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/fusion-video-bg.js') }}"></script>
     


      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionContainerVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0","is_sticky_header_transparent":"1"};
         /* ]]> */
      </script>

      <script src="{{ asset('news-06/js/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-container.js') }}"></script>
      <script src="{{ asset('news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-events.js') }}"></script>
      

  
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionRecentPostsVars = {"infinite_loading_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1"};
         /* ]]> */
      </script>

      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-recent-posts.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-title.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/library/jquery.event.move.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-image-before-after.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easyPieChart.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-counters-circle.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-progress.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBgImageVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column-bg-image.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-column.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/library/Chart.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-chart.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.transition.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tab.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTabVars = {"content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-tabs.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-syntax-highlighter.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-flip-boxes.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-gallery.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.collapse.js'></script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-toggles.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionVideoVars = {"status_vimeo":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-builder/assets/js/min/general/fusion-video.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverintent.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-vertical-menu-widget.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.tooltip.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/bootstrap.popover.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.carouFredSel.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.easing.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.flexslider.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.hoverflow.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.placeholder.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/library/jquery.touchSwipe.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-alert.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionCarouselVars = {"related_posts_speed":"2500","carousel_speed":"2500"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-carousel.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionFlexSliderVars = {"status_vimeo":"","page_smoothHeight":"false","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-flexslider.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-popover.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-tooltip.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-sharing-box.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionBlogVars = {"infinite_blog_text":"<em>Loading the next set of posts...<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","slideshow_autoplay":"","slideshow_speed":"5000","pagination_video_slide":"","status_yt":"1","lightbox_behavior":"all","blog_pagination_type":"Pagination","flex_smoothHeight":"false"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-blog.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-button.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-general-global.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionIe1011Vars = {"form_bg_color":"#ffffff"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-ie1011.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaHeaderVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_mobile":"0","header_sticky_tablet":"1","mobile_menu_design":"modern","sticky_header_shrinkage":"1","nav_height":"86","nav_highlight_border":"3","nav_highlight_style":"bar","logo_margin_top":"8px","logo_margin_bottom":"8px","layout_mode":"wide","header_padding_top":"0px","header_padding_bottom":"0px","offset_scroll":"full"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-header.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMenuVars = {"header_position":"Top","logo_alignment":"Left","header_sticky":"1","side_header_break_point":"1100","mobile_menu_design":"modern","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","mobile_submenu_open":"Open Sub Menu","mobile_submenu_close":"Close Sub Menu","submenu_slideout":"1"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-menu.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionScrollToAnchorVars = {"content_break_point":"800","container_hundred_percent_height_mobile":"0"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-scroll-to-anchor.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var fusionTypographyVars = {"site_width":"1170px","typography_responsive":"1","typography_sensitivity":"0.60","typography_factor":"1.50","elements":"h1, h2, h3, h4, h5, h6"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/includes/lib/assets/min/js/general/fusion-responsive-typography.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/library/bootstrap.scrollspy.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaCommentVars = {"title_style_type":"double solid","title_margin_top":"0px","title_margin_bottom":"30px"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-comments.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-general-footer.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-quantity.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-scrollspy.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-select.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaSidebarsVars = {"header_position":"top","header_layout":"v1","header_sticky":"1","header_sticky_type2_layout":"menu_only","side_header_break_point":"1100","header_sticky_tablet":"1","sticky_header_shrinkage":"1","nav_height":"86","content_break_point":"800"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-sidebars.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/library/jquery.sticky-kit.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-tabs-widget.js'></script>
  
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/library/jquery.toTop.js'></script>
    
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-to-top.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-drop-down.js'></script>
    
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-rev-styles.js'></script>
      <script type='text/javascript'>
         /* <![CDATA[ */
         var avadaMobileImageVars = {"side_header_break_point":"1100"};
         /* ]]> */
      </script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-mobile-image-hover.js'></script>
      <script type='text/javascript' src='news-06/wp-content/themes/Tinh/assets/min/js/general/avada-contact-form-7.js'></script>
   
      <script type='text/javascript' src='news-06/wp-content/plugins/fusion-core/js/min/avada-fusion-slider.js'></script>


		@include ('general.contact', [
		  'phone' => isset($information['so-dien-thoai']) ? $information['so-dien-thoai']  : '',
		  'zalo' => isset($information['chat-zalo']) ? $information['chat-zalo'] : '',
		  'fanpage' => isset($information['ten-tom-tat-fanpage-facebook']) ? $information['ten-tom-tat-fanpage-facebook'] : ''
		])


  
   </body>
</html>