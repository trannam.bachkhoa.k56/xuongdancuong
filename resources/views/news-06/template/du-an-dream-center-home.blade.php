@extends('news-06.layout.site')


@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('content')
   <!-- SLIDER goldsean-->
  <!--  <?php print_r($product)?> -->
  
   @include('news-06.partials.slider_dream_center_home')
   <style type="text/css">
   .h2, h2 , h3 {
       font-size: 24px !important;
   }
  </style>
      <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-12682" class="post-12682 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">{{ $product->title }}</span>                 
                     
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                       
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;">{{ isset($product['tieu-de-3']) ? $product['tieu-de-3'] : ''}}</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></p>
                                    </div>
                                    <div class="fusion-tabs fusion-tabs-1 clean vertical-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#143c57;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#b7960b;border-top-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#b7960b;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style>
                                       
                                      
                                    </div>
                                    <div class="container">
                                       <div class="row">
                                          @if(!empty($product['danh-sach-hinh-anh-1']))
                                          @foreach(explode(',', $product['danh-sach-hinh-anh-1']) as $imageProduct1)   

                                          <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                                             <div class="imageframe-align-center">
                                                <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-5">
                                                   <style scoped="scoped">.element-bottomshadow.image-frame-shadow-5{display:inline-block}</style>
                                                   <span class="fusion-imageframe imageframe-bottomshadow imageframe-5 element-bottomshadow hover-type-none"><a href="{{ $imageProduct1 }}" class="fusion-lightbox" data-rel="iLightbox[c738ee7087039d85dcc]" data-title="" title=""><img src="{{ $imageProduct1 }}" width="2921" height="1500" alt="" class="i{{ $imageProduct1 }} 200w, {{ $imageProduct1 }} 400w, {{ $imageProduct1 }} 600w, {{ $imageProduct1 }} 800w, {{ $imageProduct1 }} 1200w, {{ $imageProduct1 }} 2921w" sizes="(max-width: 800px) 100vw, 1200px" /></a></span>
                                                </div>
                                             </div>
                                          </div>
                                          @endforeach
                                          @endif
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2017/02/gallery-background-dream-center-home.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;;padding-bottom:20px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="container">
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;"><span style="color: #ffffff;">{{ isset($product['tieu-de-4']) ? $product['tieu-de-4'] : ''}}</span></h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-10 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="" height="" alt="" title="" class="img-responsive"/></span></p>
                                    </div> 
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-image-carousel fusion-image-carousel-auto lightbox-enabled fusion-carousel-border">
                                       <div class="fusion-carousel" data-autoplay="yes" data-columns="3" data-itemmargin="13" data-itemwidth="180" data-touchscroll="yes" data-imagesize="auto" data-scrollitems="1">
                                          <div class="fusion-carousel-positioner">
                                             <ul class="fusion-carousel-holder">
                                                @if(!empty($product['danh-sach-hinh-anh-2']))
                                                   @foreach(explode(',', $product['danh-sach-hinh-anh-2']) as $imageProduct2)
                                                      <li class="fusion-carousel-item">
                                                         <div class="fusion-carousel-item-wrapper">
                                                            <div class="fusion-image-wrapper hover-type-none"><img src="{{ $imageProduct2 }}" alt=""/></div>
                                                         </div>
                                                      </li>
                                                   @endforeach
                                                @endif     
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling container"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:0;padding-bottom:20px;padding-left:20px; margin-left: auto;
    margin-right: auto;'>
                           <div class="fusion-builder-row fusion-row row">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1 col-12"  style='margin-top:0px;margin-bottom:20px;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h2 class="block-title" style="text-align: center;">{{ isset($product['tieu-de-5']) ? $product['tieu-de-5'] : ''}}</h2>
                                          <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-11 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="" height="" alt="Ornament Hai Phat Land" title="" class="img-responsive"/></span></p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>

                               @if(!empty($product['danh-sach-hinh-anh-3']))
                              
                              @foreach(explode(',', $product['danh-sach-hinh-anh-3']) as $imageProduct3)
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3 col-lg-4 col-md-6 col-sm-12 col-12"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span style="border:2px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-12 hover-type-zoomin"><a href="{{ $imageProduct3 }}" class="fusion-lightbox" data-rel="iLightbox[2a78c67af5822ef7ab9]" data-title="" title=""><img src="{{ $imageProduct3 }}" width="1000" height="630" alt="" class="img-responsive wp-image-12965" srcset="{{ $imageProduct3 }} 200w, {{ $imageProduct3 }} 400w, {{ $imageProduct3 }} 600w, {{ $imageProduct3 }} 800w, {{ $imageProduct3 }} 1000w" sizes="(max-width: 800px) 100vw, 400px" /></a></span></div>
                                    <div class="fusion-title title fusion-sep-none fusion-title-center fusion-title-size-four" style="margin-top:20px;margin-bottom:20px;">
                                       
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              @endforeach
                              @endif

                           </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #e9eaee;background-image: url('{{ isset($information['logo']) ? $information['logo'] : "" }}');background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 class="block-title" style="text-align: center;">NHÀ PHÂN PHỐI ĐỘC QUYỀN</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-18 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}
" width="" height="" alt="" title="" class="img-responsive"/></span></p>
                                    </div>
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-19 hover-type-none"><img src="{{ isset($information['logo']) ? $information['logo'] : '' }}
" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left"> {{ isset($product->title) ? $product->title : ''}}</p></h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#b7960b;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <p class="rtejustify">
                                          {!! isset($product['thong-tin-chi-tiet-san-pham']) ? $product['thong-tin-chi-tiet-san-pham'] : '' !!}
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:0px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">TIN TỨC &amp; SỰ KIỆN</h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#b7960b;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-recent-posts fusion-recent-posts-1 avada-container layout-thumbnails-on-side layout-columns-1">
                                       <section class="fusion-columns columns fusion-columns-1 columns-1">

                                          @foreach(\App\Entity\Post::categoryShow('su-kien',3) as $new)
                                          <article class="post fusion-column column col col-lg-12 col-md-12 col-sm-12 fusion-animated" data-animationType="slideInRight" data-animationDuration="1.0" data-animationOffset="100%">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" class="hover-type-zoomin" aria-label="{{ isset($new->title) ? $new->title : ''}}"><img src="{{ isset($new->image) ? asset($new->image) : ''}}" alt="" style="width: 144px;height: 88px" /></a></li>
                                                </ul>
                                             </div>

                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" title="" rel="author"></a></span></span>
                                                <h4 class="entry-title"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" style="text-transform: capitalize;
    font-size: 18px;">{{ isset($new->title) ? $new->title : ''}}</a></h4>

                                                <?php
                                                 $date=date_create($new->updated_at);
                                                ?>
                                                   <?php echo date_format($date,"d"); ?>, Tháng <?php echo date_format($date,"m"); ?>, <?php echo date_format($date,"Y"); ?>                              
                                             </div>

                                               
                                                
                                             
                                          </article>
                                          @endforeach

                                       </section>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-modal modal fade modal-1 modalprice282" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
                                       <style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
                                       <div class="modal-dialog modal-lg">
                                          <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                                             <div class="modal-header">
                                                <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Đăng ký nhận Bảng giá Dự án Dream Center Home</h3>
                                             </div>
                                             <div class="modal-body fusion-clearfix">
                                                <p style="text-align: center;">
                                                <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                                   <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right:4%;'>
                                                      <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                         <div class="imageframe-align-center"><span style="border:1px solid #f6f6f6;-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-20 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong.jpg" width="610" height="418" alt="Popup Dream Center Home 282 Nguyen Huy Tuong" title="popup-dream-center-home-282-nguyen-huy-tuong" class="img-responsive wp-image-12823" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong-200x137.jpg 200w, https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong-400x274.jpg 400w, https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong-600x411.jpg 600w, https://www.haiphatland.vn/wp-content/uploads/2017/02/popup-dream-center-home-282-nguyen-huy-tuong.jpg 610w" sizes="(max-width: 800px) 100vw, 610px" /></span></div>
                                                      </div>
                                                   </div>
                                                   <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top: 0px;margin-bottom: 20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                                      <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                         <div class="fusion-text">
                                                            <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của Dự án Dream Center Home tới Quý khách!</p>
                                                            <div role="form" class="wpcf7" id="wpcf7-f4180-p12682-o1" lang="en-US" dir="ltr">
                                                               <div class="screen-reader-response"></div>
                                                               <form action="/chung-cu-dream-center-home#wpcf7-f4180-p12682-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                                                                  <div style="display: none;">
                                                                     <input type="hidden" name="_wpcf7" value="4180" />
                                                                     <input type="hidden" name="_wpcf7_version" value="5.0.3" />
                                                                     <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                                     <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4180-p12682-o1" />
                                                                     <input type="hidden" name="_wpcf7_container_post" value="12682" />
                                                                  </div>
                                                                  <p><span class="wpcf7-form-control-wrap text-702"><input type="text" name="text-702" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên" /></span></p>
                                                                  <p><span class="wpcf7-form-control-wrap email-177"><input type="email" name="email-177" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email" /></span></p>
                                                                  <p><span class="wpcf7-form-control-wrap tel-832"><input type="tel" name="tel-832" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại" /></span></p>
                                                                  <p><input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                                                                  <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                                     <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                     <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                                                  </div>
                                                               </form>
                                                            </div>
                                                         </div>
                                                         <p style="text-align: center;">
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <style type='text/css'>.reading-box-container-1 .element-bottomshadow:before,.reading-box-container-1 .element-bottomshadow:after{opacity:0.7;}</style>
                                    <div class="fusion-reading-box-container reading-box-container-1" style="margin-top:0px;margin-bottom:84px;">
                                       <div class="reading-box element-bottomshadow" style="background-color:#f6f6f6;border-width:1px;border-color:#b7960b;border-right-width:3px;border-right-color:#b7960b;border-style:solid;">
                                          <a class="button fusion-button button-default button-round fusion-button-large button-large button-flat fusion-desktop-button fusion-tagline-button continue fusion-desktop-button-margin continue-right" style="-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;" href="#" target="_self" data-toggle="modal" data-target=".modalprice282"><span>ĐĂNG KÝ NGAY</span></a>
                                          <h2>Nhận ngay Bảng giá gốc Chủ đầu tư</h2>
                                          <div class="reading-box-description fusion-reading-box-additional">Giá chỉ từ 24.1 - 25.8 Triệu /M2. Ngân hàng TP Bank hỗ trợ cho vay 70% Giá trị căn hộ.</div>
                                          <div class="fusion-clearfix"></div>
                                          <a class="button fusion-button button-default button-round fusion-button-large button-large button-flat fusion-mobile-button continue-right" style="-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;" href="#" target="_self" data-toggle="modal" data-target=".modalprice282"><span>ĐĂNG KÝ NGAY</span></a>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div> -->
                        <div id="lien-he-txc">



                           <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">LIÊN HỆ VỚI CHÚNG TÔI</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-36 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">
                                          Video {{ isset($product->title) ? $product->title : ''}}</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-video fusion-youtube">
                                       {!! isset($product['video-san-pham']) ? $product['video-san-pham']  : '' !!}
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left" style="font-size: 24px;">
                                          ĐĂNG KÝ NHẬN BẢN TIN</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của dự án !</p>
                                       <p style="text-align: center;">
                                    </div>


                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div role="form" class="wpcf7" id="wpcf7-f11814-p12020-o1" lang="vi" dir="ltr">
                                          <div class="screen-reader-response"></div>
                                          <!-- FORM DANG KI -->
                                           @include('news-06.partials.form-dangki')
                                          <!-- FORM DANG KI -->   
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
@endsection