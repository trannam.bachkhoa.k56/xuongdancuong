@extends('news-06.layout.site')


@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('content')

<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-center" style="background: linear-gradient(0deg,rgba(0,0,0,0.3),rgba(0,0,0,0.3)),url({{ isset($information['banner-tin-tuc']) ? asset($information['banner-tin-tuc']): '../../../wp-content/uploads/2017/10/background-title-bar.jpg' }}); no-repeat center !important;">
   <div class="fusion-page-title-row">
      <div class="fusion-page-title-wrapper">
         <div class="fusion-page-title-captions">
            <h1 class="entry-title">{{ isset($product->title) ? $product->title : '' }}</h1>
            <div class="fusion-page-title-secondary">
               <div class="fusion-breadcrumbs"><span><a itemprop="url" href="{{ isset($domainUrl) ? $domainUrl : ''}}"><span>Trang chủ</span> <span> / </span></a></span><span>{{ isset($product->title) ? $product->title : '' }}</span></div>
            </div>
         </div>
      </div>
   </div>
</div>




         <main id="main" role="main" class="clearfix " style="">
            <div class="fusion-row" style="">
               <section id="content" style="width: 100%;">
                  <div id="post-14111" class="post-14111 page type-page status-publish hentry">
                                                                            
                     <div class="post-content">
                       
                        {!! isset($product->content) ? $product->content : '' !!}
                        <h2>Hình ảnh thực tế</h2>


                        <div class="fusion-gallery fusion-gallery-container fusion-grid-3 fusion-columns-total-5 fusion-gallery-layout-grid fusion-gallery-1" style="margin: -5px; position: relative; height: 594px;">


                        @if(!empty($product->image_list))
                           @foreach(explode(',', $product->image_list) as $imageProduct)
                              <div style="padding: 5px; display: block; position: absolute; left: 0px; top: 0px;" class="fusion-grid-column fusion-gallery-column fusion-gallery-column-3 hover-type-none">
                                 <div class="fusion-gallery-image"><a href="{{ $imageProduct }}" class="fusion-lightbox" data-rel="iLightbox[857d4dc483a43a31e99]" data-caption=""><img src="{{ $imageProduct }}" alt="" title="" aria-label="" class="img-responsive wp-image-14116 lazyloading" srcset="{{ $imageProduct }} 200w, {{ $imageProduct }} 400w, {{ $imageProduct }} 600w, {{ $imageProduct }} 800w" sizes="(min-width: 2200px) 100vw, (min-width: 824px) 387px, (min-width: 732px) 580px, (min-width: 640px) 732px, " data-was-processed="true"></a></div>
                              </div>
                           @endforeach
                        @endif
                        
                        
                        
                        
                     </div>
                        <style type="text/css" scoped="scoped">.fusion-gallery-1 .fusion-gallery-image {border:0px solid #f6f6f6;}</style>
                       
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
@endsection