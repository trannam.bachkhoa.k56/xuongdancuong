@extends('news-06.layout.site')


@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('content')
   <!-- SLIDER goldsean-->
   @include('news-06.partials.slider_index')
      <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-13764" class="post-13764 page type-page status-publish has-post-thumbnail hentry">
                                                 
                     <div class="post-content">
                        <div id="tong-quan-txc">
                           <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2016/12/background-container-tong-quan-thanh-xuan-complex.jpg");background-position: center top;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                              <div class="fusion-builder-row fusion-row ">
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-first 2_3"  style='margin-top:0px;margin-bottom:0px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-title title fusion-title-size-two" style="margin-top:0px;margin-bottom:30px;">
                                          <h2 class="title-heading-left">
                                             <h1><span style="font-family: UVF-Candlescript-Pro; font-size: 38px; color: #143c57;">{{ $product->title }}</span></h1>
                                          </h2>
                                          <div class="title-sep-container">
                                             <div class="title-sep sep-double sep-dotted" style="border-color:#b7960b;"></div>
                                          </div>
                                       </div>
                                       <div class="fusion-content-boxes content-boxes columns row fusion-columns-4 fusion-columns-total-8 fusion-content-boxes-1 content-boxes-icon-on-top content-left fusion-delayed-animation" data-animation-delay="400" data-animationOffset="100%" style="margin-top:0px;margin-bottom:40px;">
                                          <style type="text/css" scoped="scoped">.fusion-content-boxes-1 .heading .content-box-heading {color:#143c57;}
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link .content-box-heading,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
                                             .fusion-content-boxes-1 .heading .heading-link:hover .content-box-heading {
                                             color: #b7960b;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
                                             color: #b7960b !important;
                                             }.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #876f08;color: rgba(255,255,255,.9);}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: rgba(255,255,255,.9);}
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
                                             background-color: #b7960b !important;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
                                             border-color: #b7960b !important;
                                             }
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-wrapper-hover-animation-pulsate .icon span:after,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-wrapper-hover-animation-pulsate .icon span:after {
                                             -webkit-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             -moz-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             box-shadow: 0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #b7960b, 0 0 0 10px rgba(255,255,255,0.5);
                                             }
                                          </style>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-first-in-row">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-tong-dien-tich.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Diện tích Golseason</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Diện tích Dự án: 22.371 m2</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-2 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-dien-tich-chung-cu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Diện tích căn hộ</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>2- 3 phòng ngủ: 65m2-110m2</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-3 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-chu-dau-tu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Chủ đầu tư</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Công ty TNR HOLDING VIET NAM</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-4 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-vi-tri.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Vị trí</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Số 47 Nguyễn Tuân, Q. Thanh Xuân, Hà Nội</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-5 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-first-in-row">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-tang-dich-vu.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tầng 1, 2, 3</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Trung tâm thương mại, dịch vụ, tiện ích</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-6 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/icon-khu-can-ho.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tầng 4-24</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Khu căn hộ cao cấp</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-7 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover ">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/3.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Đơn vị phân phối</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Sàn giao dịch Bất động sản Hải Phát</p>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="fusion-column content-box-column content-box-column content-box-column-8 col-lg-3 col-md-3 col-sm-3 fusion-content-box-hover  content-box-column-last">
                                             <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInLeft" data-animationDuration="1" data-animationOffset="100%">
                                                <div class="heading heading-with-icon icon-left">
                                                   <div class="image"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/8.png" width="62" height="62" alt="" /></div>
                                                   <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">Tư vấn thiết kế</h2>
                                                </div>
                                                <div class="fusion-clearfix"></div>
                                                <div class="content-container" style="color:#333333;">
                                                   <p>Moore Ruble Yudell</p>
                                                </div>
                                             </div>
                                          </div>
                                          <style type="text/css" scoped="scoped">
                                             .fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
                                             .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
                                             background-color: #b7960b !important;
                                             border-color: #b7960b !important;
                                             }
                                          </style>
                                          <div class="fusion-clearfix"></div>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:20px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none fusion-animated" data-animationType="fadeInRight" data-animationDuration="1.0" data-animationOffset="100%"><img src="{{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}}" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="font-family: UVF-Candlescript-Pro; font-size: 38px; text-align: center;"><span style="color: #143c57;">Vị trí Vàng, kết nối đa chiều</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-text" style="text-align: center;">
                                       <p style="text-align: center;">{!! isset($product->properties) ? $product->properties : '' !!}</p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first lien-ket-vung-txc fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-column-content-centered">
                                       <div class="fusion-column-content">
                                          <div class="wrap-overview">
                                             <div class="overview-info">
                                                
                                                {!! isset($product['thong-tin-chi-tiet-san-pham']) ? $product['thong-tin-chi-tiet-san-pham'] : '' !!}
                                                <div class="clearfix"></div>
                                             </div>
                                            
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-last vi-tri-txc 2_3"  style='margin-top:0px;margin-bottom:20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-column-content-centered">
                                       <div class="fusion-column-content">
                                          <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="{{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}}" width="1161" height="789" alt="" title="map-du-an-goldseason" class="img-responsive wp-image-13821" srcset="{{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 200w, https:/{{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 400w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 600w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 800w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 1161w" sizes="(max-width: 800px) 100vw, 1161px" /></span></div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2017/05/bg-contact-form-goldseason.jpg");background-position: center bottom;background-repeat: no-repeat;padding-top:30px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #b7960b; font-size: 18pt;"><strong>{{ $product->title }}</strong></span></h2>
                                       
                                      
                                       <p style="text-align: center;"><span style="color: #b7960b; font-size: 16pt;"><strong>HOTLINE: {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai'] : '' }} </strong></span></p>
                                       <p style="text-align: center;">
                                       <div role="form" class="wpcf7" id="wpcf7-f4-p13764-o1" lang="vi" dir="ltr">
                                          <div class="screen-reader-response"></div>



                                          <form action="{{ route('sub_contact') }}" method="post" class="wpcf7-form" novalidate="novalidate">
                                             {!! csrf_field() !!}
                                             
                                             <div class="col-sm-6 col-md-3"><label class="name"><span class="wpcf7-form-control-wrap text-858">
                                                <input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nhập Họ tên..." /></span></label></div>

                                             <div class="col-sm-6 col-md-3"><label class="mail"><span class="wpcf7-form-control-wrap email-695">

                                                <input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Nhập Email..." /></span></label></div>
                                             <div class="col-sm-6 col-md-3"><label class="phone"><span class="wpcf7-form-control-wrap tel-82">
                                                <input type="tel" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Nhập SĐT..." /></span></label></div>
                                                <div style="display: none">
                                                   <input type="text" name="address" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nhập địa chỉ..." />
                                                   <textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nhập nội dung...">
                                                      {{ $product->title }}
                                                   </textarea>
                                                </div>

                                             <div class="col-sm-6 col-md-3"><button class="uppercase" type="submit"><span class="fa fa-paper-plane"></span>ĐĂNG KÝ</button></div>
                                             <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                                <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                             </div>
                                          </form>

                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="home-features" class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://foresa-villas.com/wp-content/uploads/2014/11/bg-features.jpg");background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #b7960b;">Tiện ích Dự án</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                     
                                    </div>
                                    <div class="project-features">
                                       <ul class="list-features">
                                       
                                          @foreach(\App\Entity\SubPost::showSubPost('tien-ich-du-an', 8) as $id => $new_project)
                                           @if($id % 2 == 0 )   
                                          <li class="feature odd">
                                             <a href="#">
                                                <img width="218" height="218" src="{{ isset($new_project->image) ? asset($new_project->image) : '' }}" class="attachment-thumbnail size-thumbnail wp-post-image" alt="{{ isset($new_project->title) ? $new_project->title : '' }}" />      
                                                <h3><img alt="{{ isset($new_project->title) ? $new_project->title : '' }}"  src="{{ isset($new_project['icon']) ? asset($new_project['icon']) : '' }}" /><span></span> </h3>
                                             </a> 
                                          </li>
                                          @else
                                          <li class="feature even">
                                               <a href="#">
                                                 <h3 data-fontsize="18" data-lineheight="25"><img alt="{{ isset($new_project->title) ? $new_project->title : '' }}"  src="{{ isset($new_project['icon']) ? asset($new_project['icon']) : '' }}" /><span></span></h3>
                                                 <img width="218" height="218" src="{{ isset($new_project->image) ? asset($new_project->image) : '' }}" class="attachment-thumbnail size-thumbnail wp-post-image lazyloading" alt="{{ isset($new_project->title) ? $new_project->title : '' }}" data-was-processed="true">        </a>
                                          </li>
                                          @endif
                                          @endforeach
                                          
                                          
                                          
                                          
                                         
                                       </ul>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                       
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        


                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2014/06/bkgd13.jpg");background-position: left top;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">{{ isset($product['tieu-de-1']) ? $product['tieu-de-1'] : 'Căn hộ 2 Phòng Ngủ' }}</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                       <p style="text-align: center; font-size: 15px; font-weight: bold;"></p>
                                    </div>
                                    <div class="fusion-text">
                                     <div class="container">
                                        <div class="row">
                                          @if(!empty($product['danh-sach-hinh-anh-1'])) 
                                                         @foreach(explode(',', $product['danh-sach-hinh-anh-1']) as $imageProduct1)
                                           <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                                              
                                              <article id="post-13940" class="fusion-post-medium-alternate  post fusion-clearfix post-13940 type-post status-publish format-standard has-post-thumbnail hentry category-the-vesta category-tin-hai-phat category-tin-tuc tag-du-an-the-vesta tag-du-an-vesta tag-mo-ban-the-vesta tag-nha-o-xa-hoi tag-nha-o-xa-hoi-the-vesta tag-the-vesta tag-the-vesta-phu-lam tag-v3-prime" style="margin-bottom: 0">
                                                
                                                <div class="fusion-flexslider flexslider fusion-post-slideshow">
                                                   <ul class="slides">

                                                     
                                                         <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                                                            <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                                               
                                                               <img width="320" height="202" src="{{ $imageProduct1 }}" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" sizes="(max-width: 320px) 100vw, 320px" draggable="false">
                                                               <div class="fusion-rollover">
                                                                  <div class="fusion-rollover-content">
                                                                    
                                                                     <div class="fusion-rollover-sep"></div>
                                                                     <a class="fusion-rollover-gallery" href="{{ $imageProduct1 }} " data-id="13940" data-rel="iLightbox[gallery]" data-title="" data-caption="">
                                                                                  </a>
                                                                    
                                                                     
                                                                     
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </li>
                                                         
                                                   </ul>
                                               <!--  <ol class="flex-control-nav flex-control-paging"></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1"></a></li><li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1"></a></li></ul> --></div>
                                                <!-- <div class="fusion-post-content post-content">
                                                  <h2 class="entry-title fusion-post-title" data-fontsize="17" data-lineheight="27"><a href="http://news-06.local/tin-tuc/tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san">THAM DỰ HỘI NGHỊ CÁC SÀN GIAO DỊCH BẤT ĐỘNG SẢN</a></h2>
                                                  <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden">2018-09-10 03:18:13</span></p>
                                                  <div class="fusion-post-content-container">
                                                     <p> Ngày 16/12 vừa qua, Công ty Cổ phần bất động sản Hải Phát đã tham dự hội nghị “Đánh giá thị trường giao dịch bất động sản 2016, xu thế 2017 và giao lưu các sàn giao dịch bất động sản” do Hội Môi giới bất động sản Việt Nam tổ chức tại Cung triển lãm Kiến trúc Quy hoạch Xây dựng Quốc gia, Hà Nội.  [...]</p>
                                                  </div>
                                               </div> -->
                                                <!-- <div class="fusion-clearfix"></div>
                                                <div class="fusion-meta-info">
                                                   <div class="fusion-alignright">
                                                      <a href="http://news-06.local/tin-tuc/tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san" class="fusion-read-more">
                                                      Read More  </a>
                                                   </div>
                                                </div> -->
                                             </article>

                                           </div>
                                           @endforeach
                                           @endif
                                        </div>
                                     </div>

                                      
                                       
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2014/06/bkgd13.jpg");background-position: left top;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center; font-family: UVF-Candlescript-Pro; font-size: 38px;"><span style="color: #143c57;">{{ isset($product['tieu-de-2']) ? $product['tieu-de-2'] : 'Căn hộ 3 Phòng Ngủ' }}</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-single sep-solid" style="border-color:#b7960b;border-top-width:2px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#b7960b;"></i></span></div>
                                       </p>
                                       <p style="text-align: center; font-size: 15px; font-weight: bold;"></p>
                                    </div>
                                    <div class="fusion-text">
                                     <div class="container">
                                        <div class="row">
                                          @if(!empty($product['danh-sach-hinh-anh-2'])) 
                                                         @foreach(explode(',', $product['danh-sach-hinh-anh-2']) as $imageProduct2)
                                           <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                                              
                                              <article id="post-13940" class="fusion-post-medium-alternate  post fusion-clearfix post-13940 type-post status-publish format-standard has-post-thumbnail hentry category-the-vesta category-tin-hai-phat category-tin-tuc tag-du-an-the-vesta tag-du-an-vesta tag-mo-ban-the-vesta tag-nha-o-xa-hoi tag-nha-o-xa-hoi-the-vesta tag-the-vesta tag-the-vesta-phu-lam tag-v3-prime" style="margin-bottom: 0">
                                                
                                                <div class="fusion-flexslider flexslider fusion-post-slideshow">
                                                   <ul class="slides">

                                                     
                                                         <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                                                            <div class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                                               
                                                               <img width="320" height="202" src="{{ $imageProduct2 }}" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" sizes="(max-width: 320px) 100vw, 320px" draggable="false">
                                                               <div class="fusion-rollover">
                                                                  <div class="fusion-rollover-content">
                                                                    
                                                                     <div class="fusion-rollover-sep"></div>
                                                                     <a class="fusion-rollover-gallery" href="{{ $imageProduct2 }} " data-id="13940" data-rel="iLightbox[gallery]" data-title="" data-caption="">
                                                                                  </a>
                                                                    
                                                                     
                                                                     
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </li>
                                                         
                                                   </ul>
                                               <!--  <ol class="flex-control-nav flex-control-paging"></ol><ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1"></a></li><li class="flex-nav-next"><a class="flex-next flex-disabled" href="#" tabindex="-1"></a></li></ul> --></div>
                                                <!-- <div class="fusion-post-content post-content">
                                                  <h2 class="entry-title fusion-post-title" data-fontsize="17" data-lineheight="27"><a href="http://news-06.local/tin-tuc/tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san">THAM DỰ HỘI NGHỊ CÁC SÀN GIAO DỊCH BẤT ĐỘNG SẢN</a></h2>
                                                  <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden">2018-09-10 03:18:13</span></p>
                                                  <div class="fusion-post-content-container">
                                                     <p> Ngày 16/12 vừa qua, Công ty Cổ phần bất động sản Hải Phát đã tham dự hội nghị “Đánh giá thị trường giao dịch bất động sản 2016, xu thế 2017 và giao lưu các sàn giao dịch bất động sản” do Hội Môi giới bất động sản Việt Nam tổ chức tại Cung triển lãm Kiến trúc Quy hoạch Xây dựng Quốc gia, Hà Nội.  [...]</p>
                                                  </div>
                                               </div> -->
                                                <!-- <div class="fusion-clearfix"></div>
                                                <div class="fusion-meta-info">
                                                   <div class="fusion-alignright">
                                                      <a href="http://news-06.local/tin-tuc/tham-du-hoi-nghi-cac-san-giao-dich-bat-dong-san" class="fusion-read-more">
                                                      Read More  </a>
                                                   </div>
                                                </div> -->
                                             </article>

                                           </div>
                                           @endforeach
                                           @endif
                                        </div>
                                     </div>

                                      
                                       
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>




                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none txc-ly-do nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://haiphatland.vn/wp-content/uploads/2016/12/background-container-thanh-xuan-complex-hapulico-24t3.svg");background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:20px;padding-bottom:95px;padding-left:20px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last anh-9-ly-do 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">

                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-13 hover-type-none fusion-no-small-visibility"><img src="{{ isset($product['banner-san-pham']) ? $product['banner-san-pham'] : ''}}" width="" height="" alt="" title="" class="img-responsive wp-image-13824"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-14 hover-type-none fusion-no-medium-visibility fusion-no-large-visibility"><img src="https://haiphatland.vn/wp-content/uploads/2016/12/mobile-ly-do-khach-hang-thanh-xuan-complex-hapulico-24t3.svg" width="" height="" alt="Ly do Khach hang Thanh Xuan Complex Hapulico 24T3 Mobile" title="" class="img-responsive"/></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;margin-bottom: 0px;margin-top: 0px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #d2bd56 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"><span class="icon-wrapper" style="border-color:#d2bd56;background-color:rgba(255,255,255,0);"><i class=" fa fa-wifi" style="color:#d2bd56;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>










                        <div id="lien-he-txc">



                           <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">LIÊN HỆ VỚI CHÚNG TÔI</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-36 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">
                                          Video {{ isset($product->title) ? $product->title : ''}}</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-video fusion-youtube">
                                       {!! isset($product['video-san-pham']) ? $product['video-san-pham']  : '' !!}
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left" style="font-size: 24px;">
                                          ĐĂNG KÝ NHẬN BẢN TIN</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của dự án !</p>
                                       <p style="text-align: center;">
                                    </div>


                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div role="form" class="wpcf7" id="wpcf7-f11814-p12020-o1" lang="vi" dir="ltr">
                                          <div class="screen-reader-response"></div>
                                          <!-- FORM DANG KI -->
                                           @include('news-06.partials.form-dangki')
                                          <!-- FORM DANG KI -->   
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>


            <!-- fusion-row -->

         </main>
@endsection