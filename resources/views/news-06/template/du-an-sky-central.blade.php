@extends('news-06.layout.site')


@section('title', $product->title )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('content')
   <!-- SLIDER goldsean-->
   <!-- <?php print_r($product)?> -->
   @include('news-06.partials.slider_sky_central')
   <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-12020" class="post-12020 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">{{ isset($product->title) ? $product->title  : ''}}</span>
                     ]
                     <div class="post-content">
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/bg-intro-hai-phat-land.jpg");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;border-top-width:1px;border-bottom-width:1px;border-color:#eae9e9;border-top-style:dotted;border-bottom-style:dotted;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;color: #fff" data-fontsize="24" data-lineheight="36"> {{ isset($product->title) ? $product->title  : ''}}</h2>
                                       <p style="text-align: center;">
                                         
                                       <!-- div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong.png" width="778" height="57" alt="Title Chung cu 176 Dinh Cong" title="title-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13441" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong-200x15.png 200w, https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong-400x29.png 400w, https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong-600x44.png 600w, https://www.haiphatland.vn/wp-content/uploads/2017/03/title-chung-cu-sky-central-176-dinh-cong.png 778w" sizes="(max-width: 800px) 100vw, 778px" /></span></div> -->
                                       </p>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="font-size: 19px; color: #e1c55b;">
                                          {{ isset($product->description) ?$product->description  : ''}}

                                       </span></p>
                                    </div>
                                    <div class="fusion-button-wrapper fusion-aligncenter">
                                       <style type="text/css" scoped="scoped">.fusion-button.button-1 .fusion-button-text, .fusion-button.button-1 i {color:rgba(255,255,255,.8);}.fusion-button.button-1 {border-width:0px;border-color:rgba(255,255,255,.8);}.fusion-button.button-1 .fusion-button-icon-divider{border-color:rgba(255,255,255,.8);}.fusion-button.button-1.button-3d{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.button-1.button-3d:active{-webkit-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);-moz-box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);box-shadow: inset 0px 1px 0px #fff,0px 4px 0px #846326,1px 6px 6px 3px rgba(0,0,0,0.3);}.fusion-button.button-1:hover .fusion-button-text, .fusion-button.button-1:hover i,.fusion-button.button-1:focus .fusion-button-text, .fusion-button.button-1:focus i,.fusion-button.button-1:active .fusion-button-text, .fusion-button.button-1:active{color:rgba(255,255,255,.9);}.fusion-button.button-1:hover, .fusion-button.button-1:focus, .fusion-button.button-1:active{border-width:0px;border-color:rgba(255,255,255,.9);}.fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:hover .fusion-button-icon-divider, .fusion-button.button-1:active .fusion-button-icon-divider{border-color:rgba(255,255,255,.9);}.fusion-button.button-1{background: #d8a43c;
                                          background-image: -webkit-gradient( linear, left bottom, left top, from( #bc8f34 ), to( #d8a43c ) );
                                          background-image: -webkit-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image:   -moz-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image:     -o-linear-gradient( bottom, #bc8f34, #d8a43c );
                                          background-image: linear-gradient( to top, #bc8f34, #d8a43c );}.fusion-button.button-1:hover,.button-1:focus,.fusion-button.button-1:active{background: #bc8f34;
                                          background-image: -webkit-gradient( linear, left bottom, left top, from( #d8a43c ), to( #bc8f34 ) );
                                          background-image: -webkit-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image:   -moz-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image:     -o-linear-gradient( bottom, #d8a43c, #bc8f34 );
                                          background-image: linear-gradient( to top, #d8a43c, #bc8f34 );}.fusion-button.button-1{width:auto;}
                                       </style>

                                       
                                    </div>
                                    <div class="fusion-modal modal fade modal-1 modal176dinhcong" tabindex="-1" role="dialog" aria-labelledby="modal-heading-1" aria-hidden="true">
                                       <style type="text/css">.modal-1 .modal-header, .modal-1 .modal-footer{border-color:#ebebeb;}</style>
                                       <div class="modal-dialog modal-lg">
                                          <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
                                             <div class="modal-header">
                                                <button class="close" type="button" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h3 class="modal-title" id="modal-heading-1" data-dismiss="modal" aria-hidden="true">Giới thiệu dự án {{ isset($product->title) ? $product->title  : ''}}</h3>
                                             </div>
                                            
                                          </div>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h1 style="text-align: center;">{{ isset($product['tieu-de-1']) ? $product['tieu-de-1']  : ''}}</h1>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-3 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="" title="" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-4 hover-type-none fusion-animated" data-animationType="slideInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="{{ isset($product->image) ? asset($product->image)  : ''}}" width="756" height="569" alt="" title="" class="img-responsive wp-image-13411" srcset="{{ isset($product->image) ? asset($product->image)  : ''}} 200w, {{ isset($product->image) ? asset($product->image)  : ''}} 400w, {{ isset($product->image) ? asset($product->image)  : ''}} 600w, {{ isset($product->image) ? asset($product->image)  : ''}} 756w" sizes="(max-width: 800px) 100vw, 756px" /></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                        {!! isset($product->content) ? $product->content  : '' !!}
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-5 fusion-no-small-visibility">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-5{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-5 element-bottomshadow hover-type-none"><img src="{{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}}" width="1140" height="50" alt="" title="" class="img-responsive wp-image-12741" srcset="{{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 200w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 400w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 600w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 800w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 1140w" sizes="(max-width: 800px) 100vw, 1140px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">{{ isset($product['tieu-de-2']) ? $product['tieu-de-2'] : ''}}</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-6 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5"  style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>

                                 

                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-7 hover-type-none fusion-animated" data-animationType="zoomInLeft" data-animationDuration="1.0" data-animationOffset="100%"><img src="{{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}} " width="2196" height="2022" alt="" title="" class="img-responsive wp-image-13412" srcset="{{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}}  200w, {{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}}  400w, {{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}}  600w, {{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}}  800w, {{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}}  1200w, {{ isset($product['backgruod-1']) ? asset($product['backgruod-1']) : ''}}  2196w" sizes="(max-width: 800px) 100vw, 1200px" /></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=zoomInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last fusion-animated 2_5"  style='margin-top:0px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="font-family: FuturaBk; text-align: center;"><span style="font-size: 19px; color: #e1c55b;">
                                          {{ isset($product->description) ?$product->description  : ''}}

                                       </span></p>
                                       <p style="text-align: center;">
                                       <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-8 hover-type-none"><img src="{{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}}" width="2070" height="2008" alt="" title="lien-ket-vung-chung-cu-176-dinh-cong" class="img-responsive wp-image-13413" srcset="{{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 200w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 400w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 600w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 800w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 1200w, {{ isset($product['backgruod-2']) ? asset($product['backgruod-2']) : ''}} 2070w" sizes="(max-width: 800px) 100vw, 800px" /></span></div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">{{ isset($product['tieu-de-3']) ? $product['tieu-de-3']  : ''}}</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-9 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInDown data-animationDuration=1.0 data-animationOffset=50% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div class="fusion-builder-row fusion-builder-row-inner fusion-row row ">


                                          
                                          @if(!empty($product['danh-sach-hinh-anh-1']))
                                              @foreach(explode(',', $product['danh-sach-hinh-anh-1']) as $imageProduct1)
                                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3 col-lg-4 col-md-6 col-sm-6 col-12"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                   <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-10 hover-type-zoomin"><img src="{{ $imageProduct1 }}" width="1094" height="656" alt="" title="trung-tam-thuong-mai-chung-cu-sky-central-176-dinh-cong" class="img-responsive wp-image-13457" srcset="{{ $imageProduct1 }} 200w, {{ $imageProduct1 }} 400w, {{ $imageProduct1 }} 600w, {{ $imageProduct1 }} 800w, {{ $imageProduct1 }} 1094w" sizes="(max-width: 800px) 100vw, 1094px" /></span></div>
                                                </div>
                                             </div>
                                                      
                                              @endforeach
                                                         
                                          @endif
                                          
                                        

                                       </div>
                                       </p>
                                       
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">{{ isset($product['tieu-de-4']) ? $product['tieu-de-4']  : ''}}</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-16 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="{{ isset($product->title) ? $product->title  : ''}}" title="{{ isset($product->title) ? $product->title  : ''}}" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-17">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-17{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-17 element-bottomshadow hover-type-none fusion-animated" data-animationType="zoomIn" data-animationDuration="1.0" data-animationOffset="100%"><img src="{{ isset($product['backgruod-3']) ? asset($product['backgruod-3'])  : ''}}" width="1970" height="1040" alt="" title="}" class="img-responsive wp-image-13450" srcset="{{ isset($product['backgruod-3']) ? asset($product['backgruod-3'])  : ''}} 200w, {{ isset($product['backgruod-3']) ? asset($product['backgruod-3'])  : ''}} 400w, {{ isset($product['backgruod-3']) ? asset($product['backgruod-3'])  : ''}} 600w, {{ isset($product['backgruod-3']) ? asset($product['backgruod-3'])  : ''}} 800w, {{ isset($product['backgruod-3']) ? asset($product['backgruod-3'])  : ''}} 1200w, {{ isset($product['backgruod-3']) ? asset($product['backgruod-3'])  : ''}}  : ''}} 1970w" sizes="(max-width: 800px) 100vw, 1970px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">{{ isset($product['tieu-de-5']) ? $product['tieu-de-5']  : ''}}</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-18 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="{{ isset($product['tieu-de-5']) ? $product['tieu-de-5']  : ''}}" title="{{ isset($product['tieu-de-5']) ? $product['tieu-de-5']  : ''}}" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInUp data-animationDuration=1.0 data-animationOffset=bottom-in-view class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-tabs fusion-tabs-1 clean vertical-tabs icon-position-left">
                                       <style type="text/css">#wrapper .fusion-tabs.fusion-tabs-1.clean .nav-tabs li a{border-color:#e8943b;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a{background-color:#5a4a42;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ebeaea;border-top-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#e8943b;}</style>
                                       
                                          <div class="fusion-builder-row fusion-builder-row-inner fusion-row row ">

                                             @if(!empty($product['danh-sach-hinh-anh-2']))  
                                                @foreach(explode(',', $product['danh-sach-hinh-anh-2']) as $imageProduct2)   
                                                <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3 col-lg-4 col-md-6 col-sm-6 col-12"  style='margin-top: 0px;margin-bottom: 20px;'>
                                                   <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                                      <div class="imageframe-align-center"><span style="-moz-box-shadow: 2px 3px 7px rgba(0,0,0,.3);-webkit-box-shadow: 2px 3px 7px rgba(0,0,0,.3);box-shadow: 2px 3px 7px rgba(0,0,0,.3);" class="fusion-imageframe imageframe-dropshadow imageframe-19 hover-type-zoomin"><img src="{{ $imageProduct2 }}" width="" height="" alt="" title="" class="img-responsive"/></span></div>
                                                   </div>
                                                </div>
                                                @endforeach
                                             @endif
                                          </div>      
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="element-bottomshadow fusion-image-frame-bottomshadow image-frame-shadow-32 fusion-no-small-visibility">
                                          <style scoped="scoped">.element-bottomshadow.image-frame-shadow-32{display:inline-block}</style>
                                          <span class="fusion-imageframe imageframe-bottomshadow imageframe-32 element-bottomshadow hover-type-none"><img src="{{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}}" width="1140" height="50" alt="" title="" class="img-responsive wp-image-12741" srcset="{{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 200w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 400w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 600w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 800w, {{ isset($product['banner-san-pham']) ? asset($product['banner-san-pham']) : ''}} 1140w" sizes="(max-width: 800px) 100vw, 1140px" /></span>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- CHINH SACH UU DAI -->
               
                        @include('news-06.partials.chinh-sach-uu-dai')  
                        <!-- END CHINH SACH UU DAI -->

                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #e0dede 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:0px;margin-bottom:0pc;"><span class="icon-wrapper" style="border-color:#e0dede;background-color:rgba(255,255,255,0);"><i class=" fa fa-map-marker" style="color:#e0dede;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
      
                        <!-- Tai sao chon chung -cu  -->
                     @include('news-06.partials.tai-sao-chon-chung-cu')  
                      
                        <!-- END Tai sao chon  chung -cu-->
                        

                         <!-- FORM DANG KI -->
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">LIÊN HỆ VỚI CHÚNG TÔI</h2>
                                       <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-36 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-first fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );margin-right: 4%;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">
                                          VIDEO DỰ ÁN {{ isset($product->title) ? $product->title : ''}}</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-video fusion-youtube">
                                       {!! isset($product['video-san-pham']) ? $product['video-san-pham']  : '' !!}
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_2  fusion-one-half fusion-column-last fusion-animated 1_2"  style='margin-top:0px;margin-bottom:20px;width:50%;width:calc(50% - ( ( 4% ) * 0.5 ) );'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                                       <h3 class="title-heading-left">
                                          ĐĂNG KÝ NHẬN BẢN TIN</p>
                                          <p style="text-align: center;">
                                       </h3>
                                       <div class="title-sep-container">
                                          <div class="title-sep sep-double sep-solid" style="border-color:#e0dede;"></div>
                                       </div>
                                    </div>
                                    <div class="fusion-text">
                                       <p>Xin vui lòng để lại địa chỉ email, chúng tôi sẽ cập nhật những tin tức quan trọng của dự án !</p>
                                       <p style="text-align: center;">
                                    </div>


                                    <div class="fusion-text">
                                       <p style="text-align: center;">
                                       <div role="form" class="wpcf7" id="wpcf7-f11814-p12020-o1" lang="vi" dir="ltr">
                                          <div class="screen-reader-response"></div>
                                          <!-- FORM DANG KI -->
                                           @include('news-06.partials.form-dangki')
                                          <!-- FORM DANG KI -->   
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- FORM DANG KI -->   
                       
                       
                     </div>
                  </div>
               </section>
            </div>
            <!-- fusion-row -->
         </main>
@endsection