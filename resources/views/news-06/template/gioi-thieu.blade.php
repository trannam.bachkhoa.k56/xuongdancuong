@extends('news-06.layout.site')

@section('title', isset($post->title) ? $post->title : ''  )
@section('meta_description',  isset($post->description) ? $post->description : '')
@section('keywords', '' )
@section('content')

<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-center" style="background: linear-gradient(0deg,rgba(0,0,0,0.3),rgba(0,0,0,0.3)),url({{ isset($information['banner-tin-tuc']) ? asset($information['banner-tin-tuc']): '../../../wp-content/uploads/2017/10/background-title-bar.jpg' }}); no-repeat center !important;">
   <div class="fusion-page-title-row">
      <div class="fusion-page-title-wrapper">
         <div class="fusion-page-title-captions">
            <h1 class="entry-title">{{ isset($post->title) ? $post->title : '' }}</h1>
            <div class="fusion-page-title-secondary">
               <div class="fusion-breadcrumbs"><span><a itemprop="url" href="{{ isset($domainUrl) ? $domainUrl : ''}}"><span>Trang chủ</span> <span> / </span></a></span><span>{{ isset($post->title) ? $post->title : '' }}</span></div>
            </div>
         </div>
      </div>
   </div>
</div>

<main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
   <div class="fusion-row" style="max-width:100%;">
      <section id="content" class="full-width">
         <div id="post-12592" class="post-12592 page type-page status-publish hentry">
                           
            <div class="post-content">
               <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                  <div class="fusion-builder-row fusion-row ">
                     <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_3_5  fusion-three-fifth fusion-column-first 3_5"  style='margin-top:0px;margin-bottom:20px;width:60%;width:calc(60% - ( ( 4% ) * 0.6 ) );margin-right: 4%;'>
                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                           <div class="imageframe-align-center">
                              <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-1 element-bottomshadow"><img src="{{ 
                                asset($post->image) }}" width="" height="" alt="{{ $post->title }}" title="" class="img-responsive"/></span></div>
                           </div>
                           <div class="fusion-clearfix"></div>
                        </div>
                     </div>
                     <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_5  fusion-two-fifth fusion-column-last 2_5"  style='margin-top:0px;margin-bottom:20px;width:40%;width:calc(40% - ( ( 4% ) * 0.4 ) );'>
                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                           <div class="fusion-title title fusion-title-size-three" style="margin-top:0px;margin-bottom:30px;">
                              <h3 class="title-heading-left"><span style="color: #e86108;">{{ $post->title }}</span></h3>
                              <div class="title-sep-container">
                                 <div class="title-sep sep-double sep-solid" style="border-color:#b7960b;"></div>
                              </div>
                           </div>
                           <div class="fusion-text">
                              <p>{!! $post->content !!}</p>
                           </div>
                           <div class="fusion-clearfix"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                  <div class="fusion-builder-row fusion-row ">
                    <div class="fusion-text">
                              <p>{!! $post['mo-ta-gioi-thieu'] !!}</p>
                           </div>

                     
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
   <!-- fusion-row -->
</main>

         

@endsection