
@extends('news-06.layout.site')

@section('title', isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : 'Dự án bất động sản' )
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : "")
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : "")
    
@section('content')
      @include('news-06.partials.slider_index')
    <main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
            <div class="fusion-row" style="max-width:100%;">
               <section id="content" class="full-width">
                  <div id="post-11799" class="post-11799 page type-page status-publish has-post-thumbnail hentry">
                     <span class="entry-title rich-snippet-hidden">Trang Chủ</span><span class="vcard rich-snippet-hidden"><span class="fn"><a href="author/haiphatland" title="Đăng bởi haiphatland" rel="author">haiphatland</a></span></span><span class="updated rich-snippet-hidden">2017-09-28T15:26:52+00:00</span>                                  
                     <div class="post-content">
                        <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAYO6RivqRJx8E7-G18gH3SZhFcX0CR4Yg&#038;language=vi&#038;ver=1'></script>
                        <script type='text/javascript' src='wp-content/themes/Tinh/includes/lib/assets/min/js/library/infobox_packed.js?ver=1'></script>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">{{ isset($information['ten-cong-ty']) ? $information['ten-cong-ty'] : '' }}</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong>{{ isset($information['gioi-thieu-ngan-ve-cong-ty']) ? $information['gioi-thieu-ngan-ve-cong-ty'] : '' }}</strong></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>

     
                              
                              @foreach(\App\Entity\SubPost::showSubPost('ve-chung-toi', 3) as $id => $about)
                             
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) ); {{ ($id == 2) ? "" : "margin-right: 4%"}}'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="imageframe-align-center">
                                       <div class="imageframe-liftup"><span class="fusion-imageframe imageframe-bottomshadow imageframe-3 element-bottomshadow fusion-animated" data-animationType="slideInRight" data-animationDuration="1.0" data-animationOffset="100%">
										<a href="{{ isset($about['link-den']) ? $about['link-den'] : '' }}"><img src="{{ isset($about['image']) ? asset($about['image']) : '' }}" width="" height="" alt="{{ isset($about['title']) ? $about['title'] : '' }}" title="{{ isset($about['title']) ? $about['title'] : '' }}" class="img-responsive" /></a>
									   </span>
									   </div>
                                    </div>
                                    <div class="fusion-text">
                                        <h3><a href="{{ isset($about['link-den']) ? $about['link-den'] : '' }}"><span style="color: #e86108;">{{ isset($about['title']) ? $about['title'] : '' }}</span></a></h3>
                                       <div>
                                          <p>{!! isset($about['content']) ? $about['content'] : '' !!}</p>
                                       </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              @endforeach

                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("news-06/wp-content/uploads/2016/11/linh-vuc-hoat-dong-background-hai-phat-land.jpg");background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;"><span style="color: #ffffff;">LĨNH VỰC HOẠT ĐỘNG</span></h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#ffffff;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#ffffff;background-color:#e86108;"><i class=" fa fa-star" style="color:#ffffff;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong><span style="color: #ffffff;">Lĩnh vực hoạt động chuyên nghiệp tích luỹ qua nhiều năm kinh nghiệm.</span></strong></p>
                                    </div>
                                    <div class="fusion-content-boxes content-boxes columns row fusion-columns-3 fusion-columns-total-6 fusion-content-boxes-1 content-boxes-icon-with-title content-left fusion-delayed-animation" data-animation-delay="400" data-animationOffset="100%" style="margin-top:0px;margin-bottom:40px;">
                                       <style type="text/css" scoped="scoped">.fusion-content-boxes-1 .heading .content-box-heading {color:#d2bd56;}
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .heading-link .content-box-heading,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover:before,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .fusion-read-more:hover,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
                                          .fusion-content-boxes-1 .heading .heading-link:hover .content-box-heading {
                                          color: #d2bd56;
                                          }
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
                                          color: #d2bd56 !important;
                                          }.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #876f08;color: rgba(255,255,255,.9);}.fusion-content-boxes-1 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: rgba(255,255,255,.9);}
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
                                          background-color: #d2bd56 !important;
                                          }
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
                                          border-color: #d2bd56 !important;
                                          }
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-hover-animation-pulsate .fontawesome-icon:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover.icon-wrapper-hover-animation-pulsate .icon span:after,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover.icon-wrapper-hover-animation-pulsate .icon span:after {
                                          -webkit-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #d2bd56, 0 0 0 10px rgba(255,255,255,0.5);
                                          -moz-box-shadow:0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #d2bd56, 0 0 0 10px rgba(255,255,255,0.5);
                                          box-shadow: 0 0 0 2px rgba(255,255,255,0.1), 0 0 10px 10px #d2bd56, 0 0 0 10px rgba(255,255,255,0.5);
                                          }
                                       </style>

                                       @foreach(\App\Entity\SubPost::showSubPost('linh-vuc-hoat-dong', 6) as $id => $activity)
                                       <div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-4 col-md-4 col-sm-4 fusion-content-box-hover  content-box-column-first-in-row">
                                          <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-wrapper-hover-animation-pulsate fusion-animated" style="background-color:rgba(255,255,255,0);" data-animationType="fadeInRight" data-animationDuration="1" data-animationOffset="100%">
                                             <div class="heading heading-with-icon icon-left">
                                                <div class="icon" style="-webkit-animation-duration: 400ms;animation-duration: 400ms;"><span style="height:44px;width:44px;line-height:23px;border-color:#ffffff;border-width:1;border-style:solid;background-color:#fab524;box-sizing:content-box;border-radius:50%;"><i style="border-color:#fab524;border-width:1px;background-color:#ffffff;box-sizing:content-box;height:42px;width:42px;line-height:42px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e86108;font-size:21px;" class="
                                                   {{ isset($activity['icon']) ? $activity['icon'] : '' }}
                                                "></i></span></div>
                                                <h2 class="content-box-heading" style="font-size:18px;line-height:23px;">{{ isset($activity['title']) ? $activity['title'] : '' }}</h2>
                                             </div>
                                             <div class="fusion-clearfix"></div>
                                             <div class="content-container" style="color:#ffffff;">
                                                <p>{{ isset($activity['description']) ? $activity['description'] : '' }}</p>
                                             </div>
                                          </div>
                                       </div>
                                       @endforeach


                                       
                                      
                                       
                                      
                                     
                                       <style type="text/css" scoped="scoped">
                                          .fusion-content-boxes-1 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
                                          .fusion-content-boxes-1 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
                                          background-color: #d2bd56 !important;
                                          border-color: #d2bd56 !important;
                                          }
                                       </style>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>

                      

                      <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">DỰ ÁN</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong>Chúng tôi hân hạnh mang đến cho quý khách những sản phẩm đa dạng</strong></p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                              <div  data-animationType=fadeInDown data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-animated 1_1"  style='margin-top:0px;margin-bottom:20px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 20px 0px 20px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <Style>
                                       @media only screen and (max-width: 800px){
                                       .projects .portfolio-items .portfolio-item{width:100%!important;}
                                       .projects .portfolio-items .portfolio-item {
                                       margin-bottom: 2%;    float: left;
                                       width: 31.233333333%;margin-right: 1%;
                                       margin-left: 0%!important;
                                       }
                                       }
                                       .projects {
                                       padding: 0 0 0 0;
                                       }.avia-team-member.avia-builder-el-no-sibling {
                                       margin: 0px;
                                       text-align: center;
                                       }
                                       .projects .portfolio-items .portfolio-item {
                                       margin-bottom: 2%;    float: left;
                                       width: 31.333333333%;margin-right: 1%;
                                       margin-left: 1%;  
                                       }
                                       .projects .portfolio-items .portfolio-item.span-3 {
                                       width: 23.125%;
                                       margin-right: 2%;
                                       }
                                       .projects .portfolio-items .portfolio-item.span-3:nth-child( 4n+4 ) {
                                       margin-right: 0px;
                                       }
                                       .projects .portfolio-items .portfolio-item.span-3.column-flush {
                                       width: 25%;
                                       margin-right: 0px !important;
                                       margin-bottom: 0px;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image {
                                       margin-bottom: 0px;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image:hover .portfolio-lightbox {
                                       display: block;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-hover {
                                       background-color: rgba(41, 41, 41, 0.5);    height: 100%;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-hover .hover-buttons {
                                       display: none !important;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox {
                                       position: absolute;
                                       z-index: 100;
                                       width: 100%;
                                       top: 0px;
                                       bottom: 0px;
                                       text-align: left;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox .heading {
                                       padding: 0px 10% 0 10%;
                                       color: #fff;
                                       font-weight: normal;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox p {
                                       padding: 0 10%;
                                       color: #fff;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox .button {border-radius: 50px;
                                       color: #fff;
                                       background: none;
                                       border: 2px solid #fff;
                                       position: absolute;
                                       bottom: 10%;
                                       left: 10%;
                                       transition: all 0.5s linear 0s;
                                       }
                                       .projects .portfolio-items .portfolio-item .featured-image .portfolio-lightbox .button:hover {
                                       background-color: #fab524;
                                       color: #fff !important;
                                       border: 2px solid #fab524;
                                       transition: all 0.5s linear 0s;
                                       }
                                       .projects .portfolio-items.list-masonry {
                                       height: auto !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item {
                                       position: static !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item.span-3 {
                                       width: 23.125%;
                                       margin-right: 2.5% !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item.span-3:nth-child( 4n+4 ) {
                                       margin-right: 0px !important;
                                       }
                                       .projects .portfolio-items.list-masonry .portfolio-item.span-3.no-gutter {
                                       width: 25%;
                                       margin-right: 0 !important;
                                       margin-bottom: 0 !important;
                                       }
                                       .portfolio-item .portfolio-lightbox {
                                       display: none;
                                       }
                                       .mix.portfolio-item{
                                       display:none;
                                       }
                                       .portfolio-items-wrapper {
                                       overflow:hidden;
                                       }
                                       .portfolio-items-wrapper .portfolio-item{
                                       margin-bottom:10px;
                                       }
                                       .portfolio-item .featured-image{
                                       position:relative;
                                       overflow:hidden;
                                       }
                                       .sc-portfolio-grid .portfolio-item .featured-image{
                                       overflow:visible;
                                       }
                                       .portfolio-item .featured-image:after,
                                       .portfolio-item .featured-image:before{
                                       content:'';
                                       display:table;
                                       }
                                       .portfolio-item .featured-image:after{
                                       clear:both;
                                       }
                                       .portfolio-item .featured-image{
                                       zoom:1;
                                       margin-bottom: -6px;
                                       }
                                       .portfolio-item .featured-image img{
                                       width:100%;
                                       height: 100%;
                                       max-height: 230px;
                                       }
                                       .portfolio-hover{
                                       display:none;
                                       z-index:10;
                                       }
                                       .portfolio-hover.style2{
                                       position:absolute;
                                       top:0;
                                       left:0;
                                       bottom:0;
                                       right:0;
                                       text-align:center;
                                       background:#292929;
                                       background:rgba(41,41,41,.95);
                                       display:none;
                                       }
                                       .portfolio-hover.style2{
                                       display:block;
                                       transform:scale(0);
                                       -webkit-transform:scale(0);
                                       -moz-transform:scale(0);
                                       -ms-transform:scale(0);
                                       -o-transform:scale(0);
                                       opacity:0;
                                       transition: transform .3s, opacity .3s;
                                       -webkit-transition: -webkit-transform .3s, opacity .3s;
                                       -moz-transition: -moz-transform .3s, opacity .3s;
                                       }
                                       .portfolio-item .featured-image:hover .portfolio-hover{
                                       display:block;
                                       transform:scale(1);
                                       -webkit-transform:scale(1);
                                       -moz-transform:scale(1);
                                       -ms-transform:scale(1);
                                       -o-transform:scale(1);
                                       opacity:1;
                                       }
                                       .portfolio-item .featured-image .portfolio-hover h6,
                                       .portfolio-item .featured-image .portfolio-hover span,
                                       .portfolio-item .featured-image .portfolio-hover p,
                                       .portfolio-item .featured-image .portfolio-hover .hover-buttons{
                                       opacity:0;
                                       display:block;
                                       transform: translateY(40px);
                                       -webkit-transform: translateY(40px);
                                       -moz-transform: translateY(40px);
                                       -ms-transform: translateY(40px);
                                       -o-transform: translateY(40px);
                                       transition: opacity .3s, transform .3s;
                                       -webkit-transition: opacity .3s, transform .3s;
                                       -moz-transition: opacity .3s, transform .3s;
                                       transition-delay: .2s, .2s;
                                       -webkit-transition-delay: .2s, .2s;
                                       -moz-transition-delay: .2s, .2s;
                                       }
                                       .portfolio-item .featured-image:hover .portfolio-hover h6,
                                       .portfolio-item .featured-image:hover .portfolio-hover span,
                                       .portfolio-item .featured-image:hover .portfolio-hover p,
                                       .portfolio-item .featured-image:hover .portfolio-hover .hover-buttons{
                                       opacity:1;
                                       transform: translateY(0);
                                       -webkit-transform: translateY(0);
                                       -moz-transform: translateY(0);
                                       -ms-transform: translateY(0);
                                       -o-transform: translateY(0);
                                       }
                                       .portfolio-hover.style2>div{
                                       display:table;
                                       width:100%;
                                       height:100%;
                                       }
                                       .portfolio-hover.style2>div>div{
                                       display: table-cell;
                                       vertical-align: middle;
                                       padding:20px 18%;
                                       }
                                       .portfolio-hover.style2 h6{
                                       margin:20px 0 5px;
                                       }
                                       .portfolio-hover.style2 h6 a{
                                       color:#fff;
                                       }
                                       .portfolio-hover.style2 .categories{
                                       color:#fff;
                                       color:rgba(255,255,255,.4);
                                       font-size:12px;
                                       }
                                       .portfolio-hover.style2 p{
                                       color:#fff;
                                       color:rgba(255,255,255,.5);
                                       }
                                       .portfolio-hover .hover-buttons a{
                                       width:45px;
                                       height:45px;
                                       display:inline-block;
                                       border-radius:50%;
                                       -webkit-border-radius:50%;
                                       -moz-border-radius:50%;
                                       background:#eb5858;
                                       text-align:center;
                                       color:#fff;
                                       font-size:24px;
                                       padding-top:5px;
                                       transition: background .3s;
                                       -webkit-transition: background .3s;
                                       -moz-transition: background .3s;
                                       }
                                       .portfolio-hover .hover-buttons a:hover {
                                       background: #d74242;
                                       }
                                       .details-table a {
                                       color: #FF6347;
                                       }
                                    </style>
                                    <section class="widget row portfoliolightbox-vertical-massive projects " id="layers-widget-layers_plus_column_portfoliolightbox-5">
                                       <div class="row portfolio-grid portfolio-items container1 list-grid">
										@php 
											if (!empty(\App\Entity\Category::getDetailCategory('san-pham-trang-chu'))) {
												$productIndexs = \App\Entity\Product::showProduct('san-pham-trang-chu', 20);
											} else {
												$productIndexs = \App\Entity\Product::newProduct(20);
											}
										@endphp
										
										@foreach($productIndexs as $product)
                                          <div id="layers-widget-layers_plus_column_portfoliolightbox-5-146" class="layers-masonry-column clinix-portfoliolightbox portfolio-item span-4  column portfolio-item">
                                             <div class="featured-image">
                                                <img src="{{ isset($product->image) ? asset($product->image) : ''}}" width="611" height="330">
                                                <div class="portfolio-hover style2">
                                                   <div>
                                                      <div>
                                                         <div class="hover-buttons">
                                                            <a class="solarLightBox" data-gallery="portfolioGallery" href="{{ isset($product->image) ? asset($product->image) : ''}}"><i class="fa fa-eye"></i></a>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="portfolio-lightbox">
                                                   <p></p>
                                                   <h5 class="heading">{{ isset($product->title) ?$product->title : ''}}</h5>
                                                   <p>
                                                   <p>{{ isset($product->description) ? substr($product->description, 0 , 60).'...' : ''}}
                                                               
                                                   </p>
                                                   </p>
                                                   <a href="{{  route('product',['cate_slug' => $product->slug]) }}" class="button btn-medium">Xem thêm</a>
                                                </div>
                                             </div>
                                          </div>
                                          
                                       @endforeach    

                                        

                                          
                                        
                                        
                                          </div>
                                       </div>
                                    </section>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>











                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-sep-clear"></div>
                                    <div class="fusion-separator fusion-full-width-sep sep-shadow" style="background:radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-webkit-radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-moz-radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);background:-o-radial-gradient(ellipse at 50% -50% , #b7960b 0px, rgba(255, 255, 255, 0) 80%) repeat scroll 0 0 rgba(0, 0, 0, 0);margin-left: auto;margin-right: auto;margin-top:;"><span class="icon-wrapper" style="border-color:#b7960b;background-color:rgba(255,255,255,0);"><i class=" fa fa-wifi" style="color:#b7960b;"></i></span></div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("news-06/wp-content/uploads/2016/11/background-container-hai-phat-land.jpg");background-position: left center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">TIN TỨC &amp; SỰ KIỆN</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="font-family: FuturaBk; font-size: 16px; text-align: center;"><strong>Chúng tôi luôn mang đến cho bạn những tin tức mới nhất từ thị trường Bất động sản.</strong></p>
                                    </div>
                                    <div class="fusion-recent-posts fusion-recent-posts-1 avada-container layout-thumbnails-on-side layout-columns-3">
                                       <section class="fusion-columns columns fusion-columns-3 columns-3">
                                         
                                          @foreach(\App\Entity\Post::newPostIndex(12) as $new_single)
                                          <article class="post fusion-column column col col-lg-4 col-md-4 col-sm-4 fusion-animated" data-animationType="fadeInUp" data-animationDuration="1.0" data-animationOffset="bottom-in-view">
                                             <div class="fusion-flexslider flexslider floated-slideshow flexslider-hover-type-zoomin">
                                                <ul class="slides">
                                                   <li><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new_single->slug]) }}" class="hover-type-zoomin" aria-label="{{ isset($new_single->title) ? $new_single->title : '' }}"><img src="{{ isset($new_single->image) ? asset($new_single->image) : '' }}" alt=""  style="height: 116px;width: 144px;"  /></a></li>
                                                </ul>
                                             </div>
                                             <div class="recent-posts-content">
                                                <span class="vcard" style="display: none;"><span class="fn"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new_single->slug]) }}"  rel="author"></a></span></span><span class="updated" style="display:none;">{{ isset($new_single->updated_at) ? $new_single->updated_at : '' }}</span>

                                                <h4 class="entry-title" style="font-size: 18px;    text-transform: capitalize;"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new_single->slug]) }}" >{{ isset($new_single->title) ? $new_single->title : '' }}</a>
                                                   <?php
                                                      $date=date_create($new_single->updated_at);
                                                   ?>
                                                 <p class="meta">
                                                   <br>
                                                   <span><?php echo date_format($date,"d"); ?>, Tháng <?php echo date_format($date,"m"); ?>, <?php echo date_format($date,"Y"); ?></span>
                                                </p></h4>


                                             </div>
                                          </article>
                                          @endforeach

                                          
                                       </section>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:40px;padding-right:30px;padding-bottom:0px;padding-left:30px;'>
                           <div class="fusion-builder-row fusion-row ">
                              <div  data-animationType=zoomIn data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last fusion-no-small-visibility fusion-animated 1_1"  style='margin-top:0px;margin-bottom:0px;'>
                                 <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                    <div class="fusion-text">
                                       <h2 style="text-align: center;">ĐỐI TÁC CỦA CHÚNG TÔI</h2>
                                       <p style="text-align: center;">
                                       <div class="fusion-sep-clear"></div>
                                       <div class="fusion-separator sep-double sep-solid" style="border-color:#e86108;border-top-width:1px;border-bottom-width:1px;margin-left: auto;margin-right: auto;margin-top:;width:100%;max-width:25%;"><span class="icon-wrapper" style="border-color:#e86108;background-color:rgba(255,255,255,0);"><i class=" fa fa-star" style="color:#e86108;"></i></span></div>
                                       </p>
                                       <p style="text-align: center;">
                                       <div class="fusion-image-carousel fusion-image-carousel-fixed fusion-carousel-border">
                                          <div class="fusion-carousel" data-autoplay="yes" data-columns="5" data-itemmargin="13" data-itemwidth="180" data-touchscroll="no" data-imagesize="fixed" data-scrollitems="1">
                                             <div class="fusion-carousel-positioner">
                                                <ul class="fusion-carousel-holder">
                                                   @foreach(\App\Entity\SubPost::showSubPost('doi-tac', 20) as $id => $partner)
                                                   <li class="fusion-carousel-item">
                                                      <div class="fusion-carousel-item-wrapper">
                                                         <div class="fusion-image-wrapper hover-type-none"><img width="153" height="72" src="{{ isset($partner->image) ? asset($partner->image)  : ''}}" class="attachment-blog-medium size-blog-medium" alt="Logo PMC" srcset="{{ isset($partner->image) ? asset($partner->image)  : ''}} 153w" sizes="(max-width: 153px) 100vw, 153px" /></div>
                                                      </div>
                                                   </li>
                                                   @endforeach
                                    
                                                   
                                                   
                                                
                                                </ul>
                                                <div class="fusion-carousel-nav"><span class="fusion-nav-prev"></span><span class="fusion-nav-next"></span></div>
                                             </div>
                                          </div>
                                       </div>
                                       </p>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                 </div>
                              </div>
                           </div>
                        </div>


                        <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;margin-bottom: 0px;margin-top: 0px;'>
                            @include('news-06.partials.googlemap') 
                           
                        </div>

                     </div>
                  </div>
               </section>

            </div>
            <!-- fusion-row -->
         </main>
@endsection


