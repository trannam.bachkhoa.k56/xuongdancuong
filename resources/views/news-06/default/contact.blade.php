@extends('news-06.layout.site')

@section('title', 'liên hệ')
@section('meta_description','')
@section('keywords', '' )
@section('content')

<main id="main" role="main" class="clearfix width-100" style="padding-left:30px;padding-right:30px">
   <div class="fusion-row" style="max-width:100%;">
      <section id="content" class="full-width">
         <div id="post-12042" class="post-12042 page type-page status-publish hentry">
            <span class="entry-title rich-snippet-hidden">Liên hệ</span >          
            <div class="post-content">
               
               <div class="fusion-fullwidth fullwidth-box hundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;margin-bottom: 0px;margin-top: 0px;'>
                   @include('news-06.partials.googlemap') 
                  
               </div>

               <div class="container">
                  <div class="row">
                     @if(isset($success))
                        <div class="col-md-12" style="margin:20px 0">
                            <p style="color: red; font-size: 18px; padding: 25px 0 40px 0;text-align: center;">{!! (isset($success)) ? "Cảm ơn bạn đã gửi thông tin liên hệ cho chúng tôi, chúng tôi sẽ phản hồi trong thời gian sớm nhất " : '' !!}</p>
                        </div>
                     @endif
                  </div>
               </div>

               <div class="fusion-fullwidth fullwidth-box nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;'>
                  <div class="fusion-builder-row fusion-row ">
                     <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% ) * 0.3333 ) );margin-right: 4%;'>
                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                           <div class="fusion-text">
                              <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="{{ isset($information['logo']) ? asset($information['logo']) : '' }}" width="" height="" alt="Logo" title="" class="img-responsive"/></span></div>
                              <blockquote>
                                 <p><i class="fontawesome-icon  fa fa-map-marker circle-no" style="font-size:18px;margin-right:9px;color:#b7960b;"></i>  {{ isset($information['dia-chi']) ? $information['dia-chi'] : '' }}</p>
                                 <p><i class="fontawesome-icon  fa fa-phone circle-no" style="font-size:18px;margin-right:9px;color:#b7960b;"></i> Phone: {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai'] : '' }}</p>
                                 <p><i class="fontawesome-icon  fa fa-envelope circle-no" style="font-size:18px;margin-right:9px;color:#b7960b;"></i> Email: {{ isset($information['email']) ? $information['email'] : '' }}</p>
                              </blockquote>
                           </div>
                           <div class="fusion-clearfix"></div>
                        </div>
                     </div>
                     <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3  fusion-two-third fusion-column-last 2_3"  style='margin-top:0px;margin-bottom:20px;width:66.66%;width:calc(66.66% - ( ( 4% ) * 0.6666 ) );'>
                        <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                           <div class="fusion-title title fusion-title-size-one" style="margin-top:0px;margin-bottom:30px;">
                              <h1 class="title-heading-left">
                                 <p style="text-align: left;"><span style="font-family: UVF-Candlescript-Pro; color: #143c57;">Liên hệ với chúng tôi</span></p>
                              </h1>
                              <div class="title-sep-container">
                                 <div class="title-sep sep-double sep-solid" style="border-color:#d2bd56;"></div>
                              </div>
                           </div>
                           <div class="fusion-text">
                              <p style="text-align: center;">
                              <div role="form" class="wpcf7" id="wpcf7-f13061-p12042-o1" lang="vi" dir="ltr">
                                 <div class="screen-reader-response"></div>




<!-- <form action="{{ route('sub_contact') }}" method="post">
                  {!! csrf_field() !!}
                  <div class="row">
                     <div class="col-md-6 col-xs-12">
                        <div class="infoContact">
                           <img src="{{ isset($information['logo-contact']) ? $information['logo-contact'] : '' }}"/>
                           {!! isset($information['dia-chi-lien-he']) ? $information['dia-chi-lien-he'] : '' !!}  
                        </div>
                     </div>
                     <div class="col-md-6 col-xs-12 boxForm">
                        <div class="form">
                           <div class="form-group">
                              <input type="text" class="form-control" id="name" name="name" value="" placeholder="Họ và tên *">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Số điện thoại *">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" id="email" name="email" value="" placeholder="Email *">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" id="address" name="address" value="" placeholder="Địa chỉ">
                           </div>
                           <div class="form-group">
                              <textarea style="height: 134px;" class="form-control" id="Message" name="Message" value="" placeholder="Thông tin"></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="tr col-md-12">
                        <button type="submit" class="btnContact">Gửi</button>
                     </div>
                  </div>
               </form> -->

                                 <form action="{{ route('sub_contact') }}" method="post" class="wpcf7-form" novalidate="novalidate">
                                     {!! csrf_field() !!}
                                    
                                    <div class="row">
                                       <div class="col-md-7 col-sm-7">
                                          <div class="row">
                                             <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                   <div class="contact-us-label">Họ và tên*</div>
                                                   <p>                    <span class="wpcf7-form-control-wrap text-866"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nhập Họ tên..." /></span>
                                                   </p>
                                                </div>
                                                </p>
                                             </div>
                                             <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                   <div class="contact-us-label">Địa chỉ Email*</div>
                                                   <p>                    <span class="wpcf7-form-control-wrap email-78"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Nhập Email..." /></span>
                                                   </p>
                                                </div>
                                                </p>
                                             </div>
                                             </p>
                                          </div>
                                          <div class="row">
                                             <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                   <div class="contact-us-label">Số điện thoại*</div>
                                                   <p>                    <span class="wpcf7-form-control-wrap tel-883"><input type="tel" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Nhập SĐT..." /></span>
                                                   </p>
                                                </div>
                                                </p>
                                             </div>
                                             <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                   <div class="contact-us-label">Địa chỉ</div>
                                                   <p>                    <span class="wpcf7-form-control-wrap text-680"><input type="text" name="address" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nhập địa chỉ..." /></span>
                                                   </p>
                                                </div>
                                                </p>
                                             </div>
                                             </p>
                                          </div>
                                          <div class="row">
                                             <div class="col-md-12 stm-contact-us-checkbox">
                                                <span class="wpcf7-list-item-label"><i>Nhận thông tin mới nhất của chúng tôi</i> </span>
                                             </div>
                                             </p>
                                          </div>
                                          </p>
                                       </div>
                                       <div class="col-md-5 col-sm-5">
                                          <div class="form-group">
                                             <div class="form-group">
                                                <div class="contact-us-label">Nội dung yêu cầu của Quý Khách</div>
                                                <p><span class="wpcf7-form-control-wrap textarea-273"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Nhập nội dung..."></textarea></span>
                                                </p>
                                             </div>
                                             </p>
                                          </div>
                                          <div class="contact-us-submit">
                                             <input type="submit" value="Gửi tới Chúng tôi" class="wpcf7-form-control wpcf7-submit contact-us-submit" />
                                          </div>
                                          </p>
                                       </div>
                                    </div>
                                    <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                                       <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                                       <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                    </div>
                                 </form>
                              </div>
                              </p>
                           </div>
                           <div class="fusion-clearfix"></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
   <!-- fusion-row -->
</main>
@endsection
