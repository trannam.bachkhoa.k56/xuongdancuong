@extends('news-01.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )

@section('content')

    <section class="viewNews wow fadeInUp" data-wow-offset="300">
        <div class="mask"></div>
        <div class="container">
            <div class="infoNews">
                <div class="cont">
                    <h1 class="titleNews">{{ isset($post->title) ? $post->title : "" }}</h1>
                    <div class="DateTime">
                        <?php $date=date_create($post->created_at); ?>
                        <i class="fa fa-calendar"></i> <?= date_format($date,"d-m-Y H:i") ?>
                    </div>
                    <div class="exceptNews mb20">
                        {{ isset($post->description) ? $post->description : "" }}
                    </div>
                    <div class="contentNews">
                        <div class="tc mb20"><img src="{{ isset($post->image) ? $post->image : "" }}"/></div>
                        <p>
                            {!! isset($post->content) ? $post->content : "" !!}
                        </p>
                    </div>
                    <div class="Tags">
                        <i class="fa fa-tag" aria-hidden="true"></i>Tags
                        <?php $tags = explode(',', $post->tags)?>
                        @foreach($tags as $tag)
                            <a href="#">{!! $tag !!}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="RelatedNews">
            <div class="container">
                <ul class="nav nav-tabs navTab" id="tabBar" role="tablist">
                    <li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">Tin mới nhất</a></li>
                    <li role="presentation"><a href="#tab2" aria-controls="home" role="tab" data-toggle="tab">Tin liên quan</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="tab1">
                        <div class="row">
                            <div class="listRelate">
                                @foreach(\App\Entity\Post::newPost('tin-tuc') as $newPost)
                                    <div class="col-md-3 col-sm-6 itemm">
                                        <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $newPost->slug]) }}">
                                            <img src="{{ isset($newPost->image) ? $newPost->image : "" }}"/>
                                            <h3>{{ isset($newPost->title) ? $newPost->title : "" }}</h3>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab2">
                        <div class="row">
                            <div class="listRelate">
                                @foreach(\App\Entity\Post::relativeProduct($post->slug,4) as $relativePost)
                                    <div class="col-md-3 col-sm-6 itemm">
                                        <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $relativePost->slug]) }}">
                                            <img src="{{ isset($relativePost->image) ? $relativePost->image : "" }}"/>
                                            <h3>{{ isset($relativePost->title) ? $relativePost->title : "" }}</h3>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

