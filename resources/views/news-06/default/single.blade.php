@extends('news-06.layout.site')

@section('title', isset($post->title) ? $post->title : ''  )
@section('meta_description',  isset($post->description) ? $post->description : '')
@section('keywords', '' )
@section('content')

<div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-center" style="background: linear-gradient(0deg,rgba(0,0,0,0.3),rgba(0,0,0,0.3)),url({{ isset($information['banner-tin-tuc']) ? asset($information['banner-tin-tuc']): '../../../wp-content/uploads/2017/10/background-title-bar.jpg' }}); no-repeat center !important;">
   <div class="fusion-page-title-row">
      <div class="fusion-page-title-wrapper">
         <div class="fusion-page-title-captions">
            <h1 class="entry-title">{{ isset($post->title) ? $post->title : '' }}</h1>
            <div class="fusion-page-title-secondary">
               <div class="fusion-breadcrumbs"><span><a itemprop="url" href="{{ isset($domainUrl) ? $domainUrl : ''}}"><span>Trang chủ</span> <span> / </span></a></span><span>{{ isset($post->title) ? $post->title : '' }}</span></div>
            </div>
         </div>
      </div>
   </div>
</div>

<main id="main" role="main" class="clearfix " style="">
   <div class="fusion-row" style="">
      <section id="content" style="float: left;">
         <article id="post-13940" class="post post-13940 type-post status-publish format-standard has-post-thumbnail hentry category-the-vesta category-tin-hai-phat category-tin-tuc tag-du-an-the-vesta tag-du-an-vesta tag-mo-ban-the-vesta tag-nha-o-xa-hoi tag-nha-o-xa-hoi-the-vesta tag-the-vesta tag-the-vesta-phu-lam tag-v3-prime">
            <h2 class="entry-title fusion-post-title">{{ isset($post->title) ? $post->title : '' }}</h2>
            <div class="post-content">
               <blockquote>
                  <h4 style="text-align: justify;">{{ isset($post->description) ? $post->description : '' }} <h4>
               </blockquote>
               {!! isset($post->content) ? $post->content : 'Đang cập nhật' !!}
            </div>
            <div class="fusion-meta-info">
               <div class="fusion-meta-info-wrapper"><span class="updated rich-snippet-hidden"></span>
               <?php
                  $date=date_create($post->updated_at);
               ?>
               <span><?php echo date_format($date,"d"); ?>, Tháng <?php echo date_format($date,"m"); ?>, <?php echo date_format($date,"Y"); ?></span></div>
            </div>
            <!-- <div class="fusion-sharing-box fusion-single-sharing-box share-box">
               <h4>Chia sẻ bài viết qua mạng xã hội!</h4>
               <div class="fusion-social-networks boxed-icons">
                  <div class="fusion-social-networks-wrapper">
                     <a  class="fusion-social-network-icon fusion-tooltip fusion-facebook fusion-icon-facebook" style="color:#ffffff;background-color:#3b5998;border-color:#3b5998;border-radius:4px;" href="https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.haiphatland.vn%2Fthe-vesta%2Fnhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html&amp;t=Nhi%E1%BB%81u%20giao%20d%E1%BB%8Bch%20%C4%91%C3%A3%20th%E1%BB%B1c%20hi%E1%BB%87n%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BA%A1i%20l%E1%BB%85%20m%E1%BB%9F%20b%C3%A1n%20The%20Vesta" target="_blank" data-placement="top" data-title="Facebook" data-toggle="tooltip" title="Facebook"><span class="screen-reader-text">Facebook</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-twitter fusion-icon-twitter" style="color:#ffffff;background-color:#55acee;border-color:#55acee;border-radius:4px;" href="https://twitter.com/share?text=Nhi%E1%BB%81u%20giao%20d%E1%BB%8Bch%20%C4%91%C3%A3%20th%E1%BB%B1c%20hi%E1%BB%87n%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BA%A1i%20l%E1%BB%85%20m%E1%BB%9F%20b%C3%A1n%20The%20Vesta&amp;url=https%3A%2F%2Fwww.haiphatland.vn%2Fthe-vesta%2Fnhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html" target="_blank" rel="noopener noreferrer" data-placement="top" data-title="Twitter" data-toggle="tooltip" title="Twitter"><span class="screen-reader-text">Twitter</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-linkedin fusion-icon-linkedin" style="color:#ffffff;background-color:#0077b5;border-color:#0077b5;border-radius:4px;" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html&amp;title=Nhi%E1%BB%81u%20giao%20d%E1%BB%8Bch%20%C4%91%C3%A3%20th%E1%BB%B1c%20hi%E1%BB%87n%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BA%A1i%20l%E1%BB%85%20m%E1%BB%9F%20b%C3%A1n%20The%20Vesta&amp;summary=Ti%E1%BA%BFp%20n%E1%BB%91i%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BB%AB%20%C4%91%E1%BB%A3t%20m%E1%BB%9F%20b%C3%A1n%20th%C3%A1ng%2010%2C%20ng%C3%A0y%2013%2F11%20v%E1%BB%ABa%20qua%2C%20c%C3%B4ng%20ty%20c%E1%BB%95%20ph%E1%BA%A7n%20b%E1%BA%A5t%20%C4%91%E1%BB%99ng%20s%E1%BA%A3n%20H%E1%BA%A3i%20Ph%C3%A1t%20%C4%91%C3%A3%20t%E1%BB%95%20ch%E1%BB%A9c%20m%E1%BB%9F%20b%C3%A1n%20c%C3%A1c%20c%C4%83n%20h%E1%BB%99%20h%E1%BA%A5p%20d%E1%BA%ABn%20cu%E1%BB%91i%20c%C3%B9ng%20c%E1%BB%A7a%20t%C3%B2a%20V3%20Prime%20%E2%80%93%20d%E1%BB%B1%20%C3%A1n%20The%20Vesta%20%C4%91%C3%A3%20thu%20h%C3%BAt%20h%C6%A1n%20300%20kh%C3%A1ch%20tham%20gia." target="_blank" rel="noopener noreferrer" data-placement="top" data-title="LinkedIn" data-toggle="tooltip" title="LinkedIn"><span class="screen-reader-text">LinkedIn</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-reddit fusion-icon-reddit" style="color:#ffffff;background-color:#ff4500;border-color:#ff4500;border-radius:4px;" href="http://reddit.com/submit?url=https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html&amp;title=Nhi%E1%BB%81u%20giao%20d%E1%BB%8Bch%20%C4%91%C3%A3%20th%E1%BB%B1c%20hi%E1%BB%87n%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BA%A1i%20l%E1%BB%85%20m%E1%BB%9F%20b%C3%A1n%20The%20Vesta" target="_blank" rel="noopener noreferrer" data-placement="top" data-title="Reddit" data-toggle="tooltip" title="Reddit"><span class="screen-reader-text">Reddit</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-googleplus fusion-icon-googleplus" style="color:#ffffff;background-color:#dc4e41;border-color:#dc4e41;border-radius:4px;" href="https://plus.google.com/share?url=https://www.haiphatland.vn/the-vesta/nhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html" onclick="javascript:window.open(this.href,&#039;&#039;, &#039;menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600&#039;);return false;" target="_blank" rel="noopener noreferrer" data-placement="top" data-title="Google+" data-toggle="tooltip" title="Google+"><span class="screen-reader-text">Google+</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-pinterest fusion-icon-pinterest" style="color:#ffffff;background-color:#bd081c;border-color:#bd081c;border-radius:4px;" href="http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fwww.haiphatland.vn%2Fthe-vesta%2Fnhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html&amp;description=Ti%E1%BA%BFp%20n%E1%BB%91i%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BB%AB%20%C4%91%E1%BB%A3t%20m%E1%BB%9F%20b%C3%A1n%20th%C3%A1ng%2010%2C%20ng%C3%A0y%2013%2F11%20v%E1%BB%ABa%20qua%2C%20c%C3%B4ng%20ty%20c%E1%BB%95%20ph%E1%BA%A7n%20b%E1%BA%A5t%20%C4%91%E1%BB%99ng%20s%E1%BA%A3n%20H%E1%BA%A3i%20Ph%C3%A1t%20%C4%91%C3%A3%20t%E1%BB%95%20ch%E1%BB%A9c%20m%E1%BB%9F%20b%C3%A1n%20c%C3%A1c%20c%C4%83n%20h%E1%BB%99%20h%E1%BA%A5p%20d%E1%BA%ABn%20cu%E1%BB%91i%20c%C3%B9ng%20c%E1%BB%A7a%20t%C3%B2a%20V3%20Prime%20%E2%80%93%20d%E1%BB%B1%20%C3%A1n%20The%20Vesta%20%C4%91%C3%A3%20thu%20h%C3%BAt%20h%C6%A1n%20300%20kh%C3%A1ch%20tham%20gia.&amp;media=https%3A%2F%2Fwww.haiphatland.vn%2Fwp-content%2Fuploads%2F2017%2F08%2Fmo-ban-v3-prime-the-vesta.jpg" target="_blank" rel="noopener noreferrer" data-placement="top" data-title="Pinterest" data-toggle="tooltip" title="Pinterest"><span class="screen-reader-text">Pinterest</span></a><a  class="fusion-social-network-icon fusion-tooltip fusion-vk fusion-icon-vk fusion-last-social-icon" style="color:#ffffff;background-color:#45668e;border-color:#45668e;border-radius:4px;" href="http://vkontakte.ru/share.php?url=https%3A%2F%2Fwww.haiphatland.vn%2Fthe-vesta%2Fnhieu-giao-dich-da-thuc-hien-thanh-cong-tai-le-mo-ban-vesta.html&amp;title=Nhi%E1%BB%81u%20giao%20d%E1%BB%8Bch%20%C4%91%C3%A3%20th%E1%BB%B1c%20hi%E1%BB%87n%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BA%A1i%20l%E1%BB%85%20m%E1%BB%9F%20b%C3%A1n%20The%20Vesta&amp;description=Ti%E1%BA%BFp%20n%E1%BB%91i%20th%C3%A0nh%20c%C3%B4ng%20t%E1%BB%AB%20%C4%91%E1%BB%A3t%20m%E1%BB%9F%20b%C3%A1n%20th%C3%A1ng%2010%2C%20ng%C3%A0y%2013%2F11%20v%E1%BB%ABa%20qua%2C%20c%C3%B4ng%20ty%20c%E1%BB%95%20ph%E1%BA%A7n%20b%E1%BA%A5t%20%C4%91%E1%BB%99ng%20s%E1%BA%A3n%20H%E1%BA%A3i%20Ph%C3%A1t%20%C4%91%C3%A3%20t%E1%BB%95%20ch%E1%BB%A9c%20m%E1%BB%9F%20b%C3%A1n%20c%C3%A1c%20c%C4%83n%20h%E1%BB%99%20h%E1%BA%A5p%20d%E1%BA%ABn%20cu%E1%BB%91i%20c%C3%B9ng%20c%E1%BB%A7a%20t%C3%B2a%20V3%20Prime%20%E2%80%93%20d%E1%BB%B1%20%C3%A1n%20The%20Vesta%20%C4%91%C3%A3%20thu%20h%C3%BAt%20h%C6%A1n%20300%20kh%C3%A1ch%20tham%20gia." target="_blank" rel="noopener noreferrer" data-placement="top" data-title="Vk" data-toggle="tooltip" title="Vk"><span class="screen-reader-text">Vk</span></a>
                     <div class="fusion-clearfix"></div>
                  </div>
               </div>
            </div> -->
            <section class="related-posts single-related-posts">
               <div class="fusion-title fusion-title-size-three sep-double sep-solid" style="margin-top:0px;margin-bottom:30px;">
                  <h3 class="title-heading-left">
                     Bài viết cùng chuyên mục              
                  </h3>
                  <div class="title-sep-container">
                     <div class="title-sep sep-double sep-solid"></div>
                  </div>
               </div>
               <div class="fusion-carousel fusion-carousel-title-below-image" data-imagesize="fixed" data-metacontent="yes" data-autoplay="yes" data-touchscroll="no" data-columns="3" data-itemmargin="20px" data-itemwidth="180" data-touchscroll="yes" data-scrollitems="1">
                  <div class="fusion-carousel-positioner">
                     <ul class="fusion-carousel-holder">
                        @foreach(\App\Entity\Post::relativeProduct($post->slug,20) as $relativePost)
                        <li class="fusion-carousel-item">
                           <div class="fusion-carousel-item-wrapper">
                              <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                 <img src="{{ isset($relativePost->image) ? asset($relativePost->image) : '' }}" srcset="{{ isset($relativePost->image) ? asset($relativePost->image) : '' }} 1x, {{ isset($relativePost->image) ? asset($relativePost->image) : '' }} 2x" width="500" height="383" alt="{{ isset($relativePost->title) ? $relativePost->title : '' }}" />
                                 <div class="fusion-rollover">
                                    <div class="fusion-rollover-content">
                                       <a class="fusion-rollover-link" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $relativePost->slug]) }}">{{ isset($relativePost->title) ? $relativePost->title : '' }}</a>
                                       <div class="fusion-rollover-sep"></div>
                                       <a class="fusion-rollover-gallery" href="{{ isset($relativePost->image) ? asset($relativePost->image) : '' }}" data-id="13992" data-rel="iLightbox[gallery]" data-title="{{ isset($relativePost->title) ? $relativePost->title : '' }}" data-caption="">
                                        </a>
                                       <a class="fusion-link-wrapper" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $relativePost->slug]) }}" aria-label="{{ isset($relativePost->title) ? $relativePost->title : '' }}"></a>
                                    </div>
                                 </div>
                              </div>
                              <h4 class="fusion-carousel-title">
                                 <a class="fusion-related-posts-title-link" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $relativePost->slug]) }}"_self>{{ isset($relativePost->title) ? $relativePost->title : '' }}</a>
                              </h4>
                              <div class="fusion-carousel-meta">
                                 <?php
                                    $date=date_create($post->updated_at);
                                 ?>
                                 <span class="fusion-date"><?php echo date_format($date,"d"); ?>, Tháng <?php echo date_format($date,"m"); ?>, <?php echo date_format($date,"Y"); ?></span>
                              </div>
                              <!-- fusion-carousel-meta -->
                           </div>
                           <!-- fusion-carousel-item-wrapper -->
                        </li>
                        @endforeach

                     </ul>
                     <!-- fusion-carousel-holder -->
                     <div class="fusion-carousel-nav">
                        <span class="fusion-nav-prev"></span>
                        <span class="fusion-nav-next"></span>
                     </div>
                  </div>
                  <!-- fusion-carousel-positioner -->
               </div>
               <!-- fusion-carousel -->
            </section>
            <!-- related-posts -->
         </article>
      </section>
       @include('news-06.partials.sidebar')

   </div>
   <!-- fusion-row -->
</main>
@endsection
