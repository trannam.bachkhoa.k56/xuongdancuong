@extends('news-05.layout.site')


@section('title', !empty($category->meta_title) ? $category->meta_title : $category->title)
@section('meta_description',  !empty($category->meta_description) ? $category->meta_description : $category->description)
@section('keywords', $category->meta_keyword )
@section('content')
<div id="Header_wrapper"  style="background-image:url({{ isset($information['  backgruod-menu-header']) ? $information['backgruod-menu-header'] : '' }});" class="bg-parallax" data-enllax-ratio="0.3">
            <!-- #Header -->
    <header id="Header">
        <!-- HEADER -->
        @include('news-05.common.header')
        @include('news-05.partials.slider')
        
    </header>
     <div id="Content">
            <div class="content_wrapper clearfix">
               <!-- .sections_group -->
               <div class="sections_group">
                  <div class="entry-content" itemprop="mainContentOfPage">
                     <div class="section mcb-section   "  style="padding-top:30px; padding-bottom:0px; background-color:" >
                        <div class="section_wrapper mcb-section-inner">
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one column_fancy_heading ">
                                    <div class="fancy_heading fancy_heading_arrows">
                                       <div class="animate" data-anim-type="fadeIn">
                                          <h2 class="title"><i class="icon-right-dir"></i>
                                            {{ isset($category->title) ? $category->title : ''}}
                                            <i class="icon-left-dir"></i></h2>
                                       </div>
                                    </div>
                                 </div>

                                 <div class="column mcb-column one column_visual ">
                                    <h4 style="text-align: justify;">{!! isset($category-> description) ? $category->description : '' !!}</h4>
                                  
                                 </div>
                              </div>
                           </div>
                           <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                              
                                @if($products->isEmpty())
                                <p>Không có sản phẩm nào thuộc   {{ isset($category->title) ? $category->title : ''}} này !</p>
                                @else
                                @foreach($products as $product)
                                <div class="column mcb-column one-fourth column_flat_box ">
                                    <div class="flat_box">
                                       <a href="{{ isset($product->image) ? asset($product->image) :'' }}" rel="prettyphoto">
                                          <div class="photo_wrapper">
                                             <div class="icon themebg" ><i class="icon-progress-2"></i></div>
                                             <img class="scale-with-grid" src="{{ isset($product->image) ? asset($product->image) :'' }}" alt="" width="" height=""/>
                                          </div>
                                          <div class="desc_wrapper">
                                             <h4>{{ isset($product->title) ?$product->title :'' }}</h4>
                                          </div>
                                       </a>
                                    </div>
                                </div>
                                @endforeach
                                @endif

                               <div class="column one pager_wrapper">
                                 <div class="pager">
                                    <div class="pagination-ct">
                                      {{ $products->links() }}
                                  </div>
                                 </div>
                              </div>
                                 

                              </div>
                           </div>

                           <!-- <div class="wrap mcb-wrap one  valign-top clearfix" style="" >
                              <div class="mcb-wrap-inner">
                                 <div class="column mcb-column one-third column_flat_box ">
                                    <div class="flat_box">
                                       <div class="animate" data-anim-type="fadeIn">
                                          <a href="http://metaldoor.vn/wp-content/uploads/2016/08/cuadimo-4canh-02.jpg" rel="prettyphoto">
                                             <div class="photo_wrapper">
                                                <div class="icon themebg" ><i class="icon-map"></i></div>
                                                <img class="scale-with-grid" src="http://metaldoor.vn/wp-content/uploads/2016/08/cuadimo-4canh-02.jpg" alt="" width="" height=""/>
                                             </div>
                                             <div class="desc_wrapper">
                                                <h4>Cửa đi mở quay 4 cánh</h4>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-third column_flat_box ">
                                    <div class="flat_box">
                                       <div class="animate" data-anim-type="fadeIn">
                                          <a href="http://metaldoor.vn/wp-content/uploads/2016/08/cuadimo-4canh-03.jpg" rel="prettyphoto">
                                             <div class="photo_wrapper">
                                                <div class="icon themebg" ><i class="icon-map"></i></div>
                                                <img class="scale-with-grid" src="http://metaldoor.vn/wp-content/uploads/2016/08/cuadimo-4canh-03.jpg" alt="" width="" height=""/>
                                             </div>
                                             <div class="desc_wrapper">
                                                <h4>Cửa đi mở quay 4 cánh</h4>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="column mcb-column one-third column_flat_box ">
                                    <div class="flat_box">
                                       <div class="animate" data-anim-type="fadeIn">
                                          <a href="http://metaldoor.vn/wp-content/uploads/2016/08/cuadimo-4canh-01.jpg" rel="prettyphoto">
                                             <div class="photo_wrapper">
                                                <div class="icon themebg" ><i class="icon-map"></i></div>
                                                <img class="scale-with-grid" src="http://metaldoor.vn/wp-content/uploads/2016/08/cuadimo-4canh-01.jpg" alt="" width="" height=""/>
                                             </div>
                                             <div class="desc_wrapper">
                                                <h4>Cửa đi mở quay 4 cánh</h4>
                                             </div>
                                          </a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div> -->
                        </div>
                     </div>
                    
                  </div>
               </div>
               <!-- .four-columns - sidebar -->
            </div>
         </div>

</div>
         
@endsection
