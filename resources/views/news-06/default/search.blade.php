@extends('news-06.layout.site')


@section('title', '')
@section('meta_description',  '')
@section('keywords', '' )
@section('content')
  <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-center" style="background: linear-gradient(0deg,rgba(0,0,0,0.3),rgba(0,0,0,0.3)),url({{ isset($information['banner-tin-tuc']) ? asset($information['banner-tin-tuc']): '../../../wp-content/uploads/2017/10/background-title-bar.jpg' }}); no-repeat center !important;">
            <div class="fusion-page-title-row">
               <div class="fusion-page-title-wrapper">
                  <div class="fusion-page-title-captions">
                     <h1 class="entry-title">{{ isset($category->title) ? $category->title : ''}}</h1>
                     <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs"><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="{{ isset($domainUrl) ? $domainUrl : ''}}"><span itemprop="title">Trang chủ</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Từ khóa tìm kiếm : <?php  echo $_GET['word']; ?></span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <main id="main" role="main" class="clearfix " style="">
            <div class="fusion-row" style="">
               <section id="content" class="" style="float: left;">
                  <div id="posts-container" class="fusion-blog-archive fusion-blog-layout-medium-alternate-wrapper fusion-clearfix">
                     <div class="fusion-posts-container fusion-blog-layout-medium-alternate fusion-blog-pagination  fusion-blog-rollover" data-pages="1">
                     
                        @foreach($products as $post)
                        <article id="post-13940" class="fusion-post-medium-alternate  post fusion-clearfix post-13940 type-post status-publish format-standard has-post-thumbnail hentry category-the-vesta category-tin-hai-phat category-tin-tuc tag-du-an-the-vesta tag-du-an-vesta tag-mo-ban-the-vesta tag-nha-o-xa-hoi tag-nha-o-xa-hoi-the-vesta tag-the-vesta tag-the-vesta-phu-lam tag-v3-prime">
                           <div class="fusion-date-and-formats">
                            <?php
                            $date=date_create($post->updated_at);
                            ?>
                              <div class="fusion-date-box">
                                 <span class="fusion-date">
                                 <?php echo date_format($date,"d"); ?>  </span>
                                 <span class="fusion-month-year">
                                 <?php echo date_format($date,"m"); ?>, <?php echo date_format($date,"Y"); ?> </span>
                              </div>
                              <div class="fusion-format-box">
                                 <i class="fusion-icon-pen"></i>
                              </div>
                           </div>
                           <div class="fusion-flexslider flexslider fusion-flexslider-loading fusion-post-slideshow">
                              <ul class="slides">
                                 <li>
                                    <div  class="fusion-image-wrapper fusion-image-size-fixed" aria-haspopup="true">
                                       <!-- <img width="320" height="202" src="https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-320x202.jpg" class="attachment-blog-medium size-blog-medium wp-post-image" alt="" srcset="https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-320x202.jpg 320w, https://www.haiphatland.vn/wp-content/uploads/2017/08/mo-ban-v3-prime-the-vesta-700x441.jpg 700w" sizes="(max-width: 320px) 100vw, 320px" /> -->
                                       <img width="320" height="202" src="{{ isset($post->image) ? asset($post->image) : ''}}" class="attachment-blog-medium size-blog-medium wp-post-image" alt=""  sizes="(max-width: 320px) 100vw, 320px" />
                                       <div class="fusion-rollover">
                                          <div class="fusion-rollover-content">
                                             <a class="fusion-rollover-link" href="{{ route('product',['cate_slug' => $post->slug]) }}">
                                                {{ isset($post->title) ? $post->title : ''}}">{{ isset($post->title) ? $post->title : ''}}
                                             </a>
                                             <div class="fusion-rollover-sep"></div>
                                             <a class="fusion-rollover-gallery" href="{{ isset($post->image) ? asset($post->image) : ''}} " data-id="13940" data-rel="iLightbox[gallery]" data-title="{{ isset($post->title) ? $post->title : ''}}" data-caption="">
                                             Gallery              </a>
                                             <h4 class="fusion-rollover-title">
                                                <a href="{{ route('product',['cate_slug' => $post->slug]) }}">
                                                {{ isset($post->title) ? $post->title : ''}}              </a>
                                             </h4>
                                             
                                             <a class="fusion-link-wrapper" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}" aria-label="{{ isset($post->title) ? $post->title : ''}}"></a>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="fusion-post-content post-content">
                             <h2 class="entry-title fusion-post-title"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $post->slug]) }}">{{ isset($post->title) ? $post->title : ''}}</a></h2>
                             <p class="fusion-single-line-meta"><span class="vcard rich-snippet-hidden">{{ isset($post->updated_at) ? $post->updated_at : ''}}</span></p>
                             <div class="fusion-post-content-container">
                                <p> 
                                {{ isset($post->description) ? substr($post->description, 0 , 60).'...' : ''}}
                                 </p>
                             </div>
                          </div>
                           <div class="fusion-clearfix"></div>
                           <div class="fusion-meta-info">
                              <div class="fusion-alignright">
                                 <a href="{{ route('product',['cate_slug' => $post->slug]) }}">
                                              
                                 Read More  </a>
                              </div>
                           </div>
                        </article>
                        @endforeach


                     </div>
                  </div>
                  
                  
               </section>

               <!-- SIDEBAR -->
               @include('news-06.partials.sidebar')
            </div>
            <!-- fusion-row -->
         </main>
         
@endsection
