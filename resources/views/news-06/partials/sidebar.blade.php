<aside id="sidebar" role="complementary" class="sidebar fusion-widget-area fusion-content-widget-area fusion-sidebar-right fusion-blogsidebar fusion-sticky-sidebar" style="float: right;" >
   <div class="fusion-sidebar-inner-content">
      <section id="search-2" class="widget widget_search">
	  
         <form role="search" class="searchform fusion-search-form" method="get" action="">

            <div class="fusion-search-form-content">
               <div class="fusion-search-field search-field">
                  <label class="screen-reader-text" for="s">Search for:</label>
                  <input type="text" value="" name="word" class="s" placeholder="Search ..." required aria-required="true" aria-label="Search ..."/>
               </div>
               <div class="fusion-search-button search-button">
                  <input type="submit" class="fusion-search-submit searchsubmit" value="&#xf002;" />
               </div>
            </div>
         </form>
		 
      </section>
      <section id="text-6" class="widget widget_text">
         <div class="textwidget">
            <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="{{ isset($information['banner-phai-tin-tuc']) ? asset($information['banner-phai-tin-tuc']): '' }}" width="" height="" alt="Hotline Hai Phat Land" title="" class="img-responsive"/></span></div>
         </div>
      </section>
      <section id="pyre_tabs-widget-6" class="widget fusion-tabs-widget">
         <div class="fusion-tabs-widget-wrapper fusion-tabs-widget-2 fusion-tabs-classic fusion-tabs-image-default tab-holder">
            <nav class="fusion-tabs-nav">
               <ul class="tabset tabs">
                  <li class="active"><a href="#" data-link="fusion-tab-popular">Tin tức</a></li>
                  <li><a href="#" data-link="fusion-tab-recent">Sự kiện</a></li>
               </ul>
            </nav>
            <div class="fusion-tabs-widget-content tab-box tabs-container">
               <div class="fusion-tab-popular fusion-tab-content tab tab_content" data-name="fusion-tab-popular">

                  <ul class="fusion-tabs-widget-items news-list">
                      <!-- TIN TUC -->
                     @foreach(\App\Entity\Post::categoryShow('tin-tuc',5) as $new)
                     <li>
                        <div class="image">
                           <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" aria-label="{{ isset($new->title) ? $new->title : ''}}"><img  src="{{ isset($new->image) ? asset($new->image) : ''}}" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="{{ isset($new->title) ? $new->title : ''}}" srcset="{{ isset($new->image) ? asset($new->image) : ''}} 150w" sizes="(max-width: 66px) 100vw, 66px" style="width: 52px;height: 52px;" /></a>
                        </div>
                        <div class="post-holder">
                           <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}">{{ isset($new->title) ? $new->title : ''}}</a>
                           <div class="fusion-meta">
                           <?php
                            $date=date_create($new->updated_at);
                           ?>
                              <?php echo date_format($date,"d"); ?>, Tháng <?php echo date_format($date,"m"); ?>, <?php echo date_format($date,"Y"); ?>                              
                           </div>
                        </div>
                     </li>
                     @endforeach
                    
                  </ul>
               </div>
               <div class="fusion-tab-recent fusion-tab-content tab tab_content" data-name="fusion-tab-recent" style="display: none;">
                  <ul class="fusion-tabs-widget-items news-list">
                     @foreach(\App\Entity\Post::categoryShow('su-kien',5) as $new)
                     <li>
                        <div class="image">
                           <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" aria-label="{{ isset($new->title) ? $new->title : ''}}"><img  src="{{ isset($new->image) ? asset($new->image) : ''}}" class="attachment-recent-works-thumbnail size-recent-works-thumbnail wp-post-image" alt="{{ isset($new->title) ? $new->title : ''}}" srcset="{{ isset($new->image) ? asset($new->image) : ''}} 150w" sizes="(max-width: 66px) 100vw, 66px" style="width: 52px;height: 52px;" /></a>
                        </div>
                        <div class="post-holder">
                           <a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}">{{ isset($new->title) ? $new->title : ''}}</a>
                           <div class="fusion-meta">
                           <?php
                            $date=date_create($new->updated_at);
                           ?>
                              <?php echo date_format($date,"d"); ?>, Tháng <?php echo date_format($date,"m"); ?>, <?php echo date_format($date,"Y"); ?>                              
                           </div>
                        </div>
                     </li>
                     @endforeach
                     
                  </ul>
               </div>
            </div>
         </div>
      </section>
    
      <!-- <section id="categories-2" class="widget widget_categories">
         <div class="heading">
            <h4 class="widget-title">Chuyên mục</h4>
         </div>
         <form action="https://www.haiphatland.vn" method="get">
            <label class="screen-reader-text" for="cat">Chuyên mục</label>
            <select  name='cat' id='cat' class='postform' >
               <option value='-1'>Chọn chuyên mục</option>
               <option class="level-0" value="48">Diễn đàn Hải Phát&nbsp;&nbsp;(3)</option>
               <option class="level-0" value="51">Dream Center Home&nbsp;&nbsp;(5)</option>
               <option class="level-0" value="1595">Dự án chung cư goldseason&nbsp;&nbsp;(11)</option>
               <option class="level-0" value="39">Góc Hải Phát&nbsp;&nbsp;(4)</option>
               <option class="level-0" value="1594">Nhà ở xã hội The Vesta&nbsp;&nbsp;(4)</option>
               <option class="level-0" value="1552">Roman Plaza&nbsp;&nbsp;(2)</option>
               <option class="level-0" value="1593">Sky Central&nbsp;&nbsp;(1)</option>
               <option class="level-0" value="44">Thanh Xuân Complex&nbsp;&nbsp;(5)</option>
               <option class="level-0" value="1556">Tiến độ Dự án&nbsp;&nbsp;(1)</option>
               <option class="level-0" value="4">Tin Bất động sản&nbsp;&nbsp;(15)</option>
               <option class="level-0" value="38" selected="selected">Tin Hải Phát&nbsp;&nbsp;(6)</option>
               <option class="level-0" value="1">Tin tức&nbsp;&nbsp;(25)</option>
               <option class="level-0" value="40">Tuyển dụng&nbsp;&nbsp;(4)</option>
               <option class="level-0" value="49">Văn hóa Hải Phát&nbsp;&nbsp;(2)</option>
            </select>
         </form>
         <script type='text/javascript'>
          
            (function() {
               var dropdown = document.getElementById( "cat" );
               function onCatChange() {
                  if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
                     dropdown.parentNode.submit();
                  }
               }
               dropdown.onchange = onCatChange;
            })();
          
         </script>
      </section> -->
      <section id="text-7" class="widget widget_text">
         <div class="heading">
            <h4 class="widget-title">Liên hệ Chủ đầu tư</h4>
         </div>
         <div class="textwidget">
            <p><i class="fontawesome-icon  fa fa-map-marker circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> {{ isset($information['dia-chi']) ? $information['dia-chi'] : '' }}<br><br/>
               <i class="fontawesome-icon  fa fa-phone circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> Hotline: {{ isset($information['so-dien-thoai']) ? $information['so-dien-thoai'] : '' }}<br><br/>
               <i class="fontawesome-icon  fa fa-fax circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> CSKH: {{ isset($information['cham-soc-khach-hang']) ? $information['cham-soc-khach-hang'] : '' }}<br><br/>
               <i class="fontawesome-icon  fa fa-envelope circle-no" style="font-size:18px;margin-right:9px;color:#c8943b;"></i> Email: {{ isset($information['email']) ? $information['email'] : '' }}<br>
            <div role="form" class="wpcf7" id="wpcf7-f11814-o1" lang="vi" dir="ltr">
               <div class="screen-reader-response"></div>
               
               <!-- <form action="{{ route('sub_contact') }}" method="post">
                  {!! csrf_field() !!}
                  <div class="row">
                     <div class="col-md-6 col-xs-12">
                        <div class="infoContact">
                           <img src="{{ isset($information['logo-contact']) ? $information['logo-contact'] : '' }}"/>
                           {!! isset($information['dia-chi-lien-he']) ? $information['dia-chi-lien-he'] : '' !!}  
                        </div>
                     </div>
                     <div class="col-md-6 col-xs-12 boxForm">
                        <div class="form">
                           <div class="form-group">
                              <input type="text" class="form-control" id="name" name="name" value="" placeholder="Họ và tên *">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Số điện thoại *">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" id="email" name="email" value="" placeholder="Email *">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" id="address" name="address" value="" placeholder="Địa chỉ">
                           </div>
                           <div class="form-group">
                              <textarea style="height: 134px;" class="form-control" id="Message" name="Message" value="" placeholder="Thông tin"></textarea>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="tr col-md-12">
                        <button type="submit" class="btnContact">Gửi</button>
                     </div>
                  </div>
               </form> -->


               <form action="{{ route('sub_contact') }}" method="post" class="wpcf7-form" novalidate="novalidate">
                  {!! csrf_field() !!}
                  
                  <p><span class="wpcf7-form-control-wrap text-833">

                     <input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Họ và tên*" />
                  </span></p>
                  <p><span class="wpcf7-form-control-wrap email-20">

                     <input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Địa chỉ Email*" /></span></p>
                  <p><span class="wpcf7-form-control-wrap tel-146">

                     <input type="tel" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Số điện thoại*" /></span></p>

                  <p>
                     <div style="display: none">
                        <input type="text" class="form-control" id="address" name="address" value="" placeholder="Địa chỉ">
                        <textarea style="height: 134px;" class="form-control" id="Message" name="message" value="" placeholder="Thông tin"></textarea>
                     </div>
                     <input type="submit" value="Đăng ký nhận bảng giá" class="wpcf7-form-control wpcf7-submit" /></p>
                  <div class="fusion-alert alert custom alert-custom fusion-alert-center fusion-alert-capitalize alert-dismissable wpcf7-response-output wpcf7-display-none" style="background-color:;color:;border-color:;border-width:1px;">
                     <button style="color:;border-color:;" type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                  </div>
               </form>

            </div>
         </div>
      </section>
   </div>
</aside>