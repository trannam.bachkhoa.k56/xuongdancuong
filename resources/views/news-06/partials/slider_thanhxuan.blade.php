
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <script src="js/popper.min.js"></script>
   <script src="js/bootstrap.bundle.min.js"></script>
   <script src="js/bootstrap.min.js"></script>

<section class="slider hiddenTAB">
    <div class="clearfix">

       <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    
      <div class="carousel-inner">
    
      @foreach(\App\Entity\SubPost::showSubPost('slider-thanh-xuan-complex', 8) as $id => $slide)
        <div class="carousel-item {{ ($id == 0 ) ? 'active' : ''}}">
         <img class="d-block w-100" src="{{ isset($slide['image']) ? asset($slide['image']) : '' }}" alt="{{ isset($slide['title']) ? $slide['title'] : '' }}">
          <div class="carousel-caption d-none d-md-block">
             <h5>{{ ($slide['title'] != 0) ? $slide['title'] : '' }}</h5>
            <p></p>
             
            <p>{!! isset($slide['mo-ta']) ?$slide['mo-ta'] : '' !!}</p>
          </div>
        </div>
      @endforeach

       
       
     </div>
     <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
       <span class="carousel-control-prev-icon" aria-hidden="true"></span>
       <span class="sr-only">Previous</span>
     </a>
     <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
       <span class="carousel-control-next-icon" aria-hidden="true"></span>
       <span class="sr-only">Next</span>
     </a>
</div>
    </div>
</section>
