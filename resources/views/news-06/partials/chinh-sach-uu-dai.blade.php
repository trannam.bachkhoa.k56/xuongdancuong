 <div class="fusion-fullwidth fullwidth-box fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling"  style='background-color: #ffffff;background-image: url("https://www.haiphatland.vn/wp-content/uploads/2016/12/bg-diamond-hai-phat-land.png");background-position: center center;background-repeat: no-repeat;padding-top:20px;padding-right:30px;padding-bottom:20px;padding-left:30px;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;'>
                           <div class="container">
                              <div class="fusion-builder-row fusion-row row ">
                                 
                                 <div  class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1  fusion-one-full fusion-column-first fusion-column-last 1_1 col-12"  style='margin-top:0px;margin-bottom:20px;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h2 style="text-align: center;">CHÍNH SÁCH ƯU ĐÃI &#8211; QUYỀN LỢI KHÁCH HÀNG</h2>
                                          <p style="text-align: center;"><span class="fusion-imageframe imageframe-none imageframe-33 hover-type-none"><img src="{{ isset($information['image-tieu-de']) ? $information['image-tieu-de'] : '' }}" width="189" height="14" alt="Ornament Hai Phat Land" title="ornament-hai-phat-land" class="img-responsive wp-image-12696"/></span></p>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 
                                 @foreach(\App\Entity\SubPost::showSubPost('chinh-sach-uu-dai', 8) as $id => $flash_sale)
                                 <div  data-animationType=slideInLeft data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-first fusion-animated 1_3 col-lg-4 col-md-6 col-sm-6 col-12"  style='margin-top:0px;margin-bottom:20px;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h4><i class="fontawesome-icon fa fa-gift circle-yes " style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;color:#d82731;"></i> {{ isset($flash_sale['title']) ? $flash_sale['title'] : '' }}</h4>
                                          <p><strong>{{ isset($flash_sale['description']) ? $flash_sale['description'] : '' }}</strong></p>
                                          {!!  isset($flash_sale['content']) ? $flash_sale['content'] : '' !!}
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                  @endforeach

                                
                                 <!-- <div  data-animationType=slideInUp data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:calc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );margin-right: 4%;'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h4><i class="fontawesome-icon  fa fa-gift circle-yes" style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;margin-right:9px;color:#d82731;"></i> GIÁ BÁN &amp; CƠ CẤU CĂN HỘ</h4>
                                          <p><strong><i><span lang="VI">Giá bán hấp dẫn, thiết kế đẹp, đa dạng, tối ưu hóa</span></i>:</strong></p>
                                          <ul>
                                             <li>Giá bán<b> chỉ từ </b>25tr/m2.</li>
                                             <li>Mỗi tầng được thiết kế 22 Căn hộ vuông vắn.</li>
                                             <li>Bao gồm 5 loại diện tích<b>: 64m2; 68m2; 71m2; 80m2; 92m2; 99m2</b>.</li>
                                             <li>Số lượng thang máy:<b> 10 thang máy/sàn.</b></li>
                                          </ul>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div>
                                 <div  data-animationType=slideInRight data-animationDuration=1.0 data-animationOffset=100% class="fusion-layout-column fusion_builder_column fusion_builder_column_1_3  fusion-one-third fusion-column-last fusion-animated 1_3"  style='margin-top:0px;margin-bottom:20px;width:33.33%;width:ca
                                 lc(33.33% - ( ( 4% + 4% ) * 0.3333 ) );'>
                                    <div class="fusion-column-wrapper" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;"  data-bg-url="">
                                       <div class="fusion-text">
                                          <h4><i class="fontawesome-icon  fa fa-gift circle-yes" style="border-color:#333333;background-color:#e8a703;font-size:15.84px;line-height:31.68px;height:31.68px;width:31.68px;margin-right:9px;color:#d82731;"></i> TIẾN ĐỘ THANH TOÁN</h4>
                                          <p><strong><i><span lang="VI">Hỗ trợ thanh toán linh hoạt</span></i>:</strong></p>
                                          <ul>
                                             <li>Đợt 1: <b>10% Giá trị căn hộ, ký trực tiếp với Chủ đầu tư.</b></li>
                                             <li>Các đợt đóng tiền tiếp theo linh hoạt.</li>
                                             <li>Nhận bàn giao Căn hộ mới chỉ phải nộp <strong>95%</strong>, nhận <strong>SỔ ĐỎ</strong> mới phải nộp <strong>5%</strong> còn lại.</li>
                                          </ul>
                                       </div>
                                       <div class="fusion-clearfix"></div>
                                    </div>
                                 </div> -->
                              </div>
                           </div>
                        </div>