<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />   
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <link rel="icon" href="{{ !empty($information['icon']) ?  asset($information['icon']) : '' }}" type="image/x-icon" />

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />


    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Open+Sans+Condensed:300,600' rel='stylesheet' type='text/css'>
    <!-- <link href="{{ asset('news-03/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" /> -->
	<link href="{{ asset('news-03/css/stylesheet.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('news-03/css/custom.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('news-03/css/font-awesome.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('news-03/css/queries.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('news-03/css/styles.css') }}" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="{{ asset('news-03/js/jquery.min.js') }}"></script>
    <script src="{{ asset('news-03/js/option_selection-ea4f4a242e299f2227b2b8038152223f741e90780c0c766883939e8902542bda.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.cookie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/handlebars.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/api.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.easing.1.3.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.placeholder.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.fancybox.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.mousewheel-3.0.6.pack.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.dlmenu.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.flexslider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/wow.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.nice-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/fastclick.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/theme.js') }}" type="text/javascript"></script>
    <script>
        new WOW().init();
    </script>
    <script src="{{ asset('news-03/js/jquery.bxslider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.elevateZoom-2.5.5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/instafeed.js') }}" type="text/javascript"></script>

    <script defer="defer" src="{{ asset('news-03/js/ga_urchin_forms-68ca1924c495cfc55dac65f4853e0c9a395387ffedc8fe58e0f2e677f95d7f23.js') }}"></script>
    <link href="{{ asset('news-03/css/bold-options.css') }}" rel="stylesheet" type="text/css" media="all" />
    <!--<script src="{{ asset('news-03/js/options.js') }}" type="text/javascript"></script> -->
    <script type="text/javascript">
        $(window).load(function() {
            $('.prod-image').matchHeight();
        });
    </script>
    <link rel="next" href="/next" />
    <link rel="prev" href="/prev" />
    <script src="{{ asset('news-03/js/jquery.tmpl.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('news-03/js/jquery.products.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/numeral.min.js') }}"></script>
	
</head>
<body class="gridlock index">
	{!! isset($information['ma-nhung-chat-facebook']) ? $information['ma-nhung-chat-facebook'] : '' !!}
    
    @include('news-03.common.header_mobile')
    <div class="page-wrap">
        <!-- HEADER -->
        @include('news-03.common.header')
        @yield('content')
    </div>
    @include('news-03.common.footer')
<!-- End page wrap for sticky footer -->
<!--   FOOTER -->
    <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
            if ($(window).width() >= 741) {

                $(document).ready(function() {
                    //enabling stickUp on the '.navbar-wrapper' class
                    $('nav').stickUp();
                });
            }
        });
    </script>
    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data) {
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function addToOrder(e) {
            var data = $(e).serialize();

            $.ajax({
                type: "POST",
                url: '{!! route('addToCart') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);

                    window.location.replace("/gio-hang");
                },
                error: function(error) {
                    alert('Lỗi gì đó đã xảy ra!')
                }

            });

            return false;
        }
    </script>
	
</body>
</html>
