<!-- <div id="hello" class="desktop-12 tablet-12 mobile-12">
	 <p>FREE SHIPPING ON ALL ORDERS OVER $150 AUD - ALSO WE NOW SELL GIFT CARDS TOO</p>
	 <a href="#"><i class="fa fa-close"></i></a>
</div>-->
<header>
	<div class="row">
		<div class="desktop-6 tablet-6 mobile-3">
			<div class="facebook">
			{!! isset($information['like-fb']) ? $information['like-fb'] : '' !!}

			</div>
		</div>
		<ul id="cart" class="desktop-6 tablet-6 mobile-3">
			<li><a href="/"><i class="icon-home icon-2x"></i></a>
			<!--<li class="seeks"><a id="inline" href="#search_popup" class="open_popup"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
			
			<li class="seeks"><a id="inline" href="{{ isset($information['link-instagram']) ?$information['link-instagram'] : '#'}}" class="open_popup" style="font-size: 16px;"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			<li class="seeks"><a id="inline" href="{{ isset($information['link-fb']) ?$information['link-fb'] : '#'}}" class="open_popup" style="font-size: 16px;"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
			<li class="seeks"><a id="inline" href="#" class="open_popup">{!! isset($information['like-fb']) ?$information['like-fb'] : '' !!}</a></li>
			<!--<li class="seeks-mobile"><a href="/search"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>-->
			<li class="cust"><a href="/account/login"><i class="icon-user icon-2x"></i></a></li>
			<li class="cart-overview">
                <?php $countOrder = \App\Entity\Order::countOrder();?>
				@if($countOrder <= 0)
						<a href="#">Giỏ hàng&nbsp; <i class="icon-shopping-cart icon-2x"></i>&nbsp;
							<span id="item_count">{{ $countOrder }}</span></a>
				@else
						<a href="/gio-hang">Giỏ hàng&nbsp; <i class="icon-shopping-cart icon-2x"></i>&nbsp;
							<span id="item_count">{{ $countOrder }}</span></a>

				@endif
			</li>
			<form action="{{ route('search_product') }}" method="get" id="headearch" class="input-group search-bar search_form searchProduct">
				<input type="search" name="word" value="{{ (!empty($_GET['word'])) ? $_GET['word'] : '' }}" placeholder="Tìm kiếm sản phẩm... " class="input-group-field st-default-search-input search-text" autocomplete="off">
				<button class="btn" type="submit">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</form>
		</ul>
	</div>
</header>
<div class="header-wrapper">
	<div class="row">
		<div id="logo" class="desktop-12 tablet-6 mobile-2">
			<a href="/"><img src="{{ !empty($information['logo']) ?  asset($information['logo']) : '' }}" alt="logo" style="border: 0;" /></a>
		</div>
	</div>
	<div class="clear"></div>
	<nav>
		<ul id="main-nav" role="navigation" class="row">
			@foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
				@foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
					<li>
						<a href="{{ $menuelement['url'] }}" title="">{{ $menuelement['title_show'] }}</a>
					</li>
				@endforeach
			@endforeach
		</ul>
	</nav>
	<div class="clear"></div>
</div>

