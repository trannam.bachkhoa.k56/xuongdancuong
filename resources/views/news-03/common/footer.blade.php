

<div id="footer-wrapper">
    <div id="footer" class="row">
        <div class="desktop-3 tablet-half mobile-half">
            <h4>Danh mục</h4>
            <ul>
                @foreach (\App\Entity\Menu::showWithLocation('side-left-menu') as $Mainmenu)
                    @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                        <li><a href="{{ $menuelement['url'] }}" title="">{{ $menuelement['title_show'] }}</a></li>
                    @endforeach
                @endforeach
            </ul>

        </div>
        <div class="desktop-3 tablet-half mobile-half">
            <h4>Thương hiệu</h4>
            <ul>
                @foreach(\App\Entity\SubPost::showSubPost('thuong-hieu',6) as $id => $img_thuonghieu)
                    <li><a href="#" title="{{ $img_thuonghieu['title'] }}" style="text-transform: uppercase;">{{ $img_thuonghieu['title'] }}</a></li>
                @endforeach
            </ul>
        </div>



        <div class="desktop-3 tablet-half mobile-half">
            <h4>SHOP</h4>
            <ul>
                @foreach(\App\Entity\SubPost::showSubPost('cua-hang-nhom-1',2) as $id => $shop)
                    <li><a style="text-transform: uppercase;" href="{{isset($shop['link-cua-hang']) ?$shop['link-cua-hang'] : ''}}" title="{{ $shop['title'] }}">{{ $shop['title'] }}</a></li>
                @endforeach
                @foreach(\App\Entity\SubPost::showSubPost('cua-hang-nhom-2',2) as $id => $shop)
                    <li><a style="text-transform: uppercase;" href="{{isset($shop['link-cua-hang']) ?$shop['link-cua-hang'] : ''}}" title="{{ $shop['title'] }}">{{ $shop['title'] }}</a></li>
                @endforeach
            </ul>
        </div>


        <div class="desktop-3 tablet-half mobile-3">
            <h4>Kết nối</h4>
            <div id="footer_signup">
                <p style="text-transform: uppercase;">{{ $information['ket-noi'] }}</p>




                <form  id="footer-subscribe-form"  onsubmit="return subcribeEmailSubmit(this)" class="validate margin-bottom-0">
                    {{ csrf_field() }}
                    <input value="" name="" class="email" id="footer-EMAIL" placeholder="Enter Email Address" required="" type="email">
                    <button type="submit" class="btn btn-primary subscribe" name="subscribe" id="subscribe" style="width: 20%;">Gửi</button>
                </form>
            </div>
        </div>
        <div class="clear"></div>
        <div class="credit desktop-12 tablet-6 mobile-3">
            <p>
                <?php echo $information['copyright'] ?>
            </p>
        </div>
    </div>
</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="facebook-inbox-tab" style="display: block; ">
<svg width="60px" height="60px" viewBox="0 0 60 60"><svg x="0" y="0" width="60" height="60"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g><g><circle fill="#3366cc" cx="30" cy="30" r="30"></circle><g transform="translate(10.000000, 11.000000)" fill="#FFFFFF"><path d="M0,18.7150914 C0,8.37723141 8.956743,0 20,0 C31.043257,0 40,8.37723141 40,18.7150914 C40,29.0529515 31.043257,37.4301829 20,37.4301829 C18,37.4301829 16.0763359,37.1551856 14.2544529,36.6459314 L6.95652174,40 L6.95652174,33.0434783 C2.44929143,29.6044708 0,24.5969773 0,18.7150914 Z M16.9276495,19.6993886 L22.109375,25.0798234 L33.0434783,13.4782609 L23.0672554,18.9962636 L17.890625,13.6158288 L6.95652174,25.2173913 L16.9276495,19.6993886 Z"></path></g></g></g></g></svg></svg>
</div>
<div id="facebook-inbox">
	<div id="facebook-inbox-frame">
		<div id="fb-root">&nbsp;</div>
		<div class="fb-page" data-adapt-container-width="true" data-hide-cover="false" data-href="" data-show-facepile="true" data-small-header="true" data-width="250" data-height="350" data-tabs="messages">
			<div class="fb-xfbml-parse-ignore">
				<blockquote cite=""><a href="">Chat với chúng tôi</a></blockquote>
			</div>
		</div>
	</div>
</div>

<style>
#facebook-inbox {
	position: fixed;
    bottom: 10px;
    z-index: 999;
    width: 250px;
    right: 85px;
}
.facebook-inbox-tab {
	position: fixed;
    bottom: 15px;
    z-index: 998;
    text-align: center;
    right: 15px;
	cursor: pointer;
}
.facebook-inbox-tab-icon {
    float: left;
}

.facebook-inbox-tab-title {
    float: left;
    margin-left: 10px;
    line-height: 25px;
}

#facebook-inbox-frame {
    display: none;
    width: 100%;
    min-height: 200px;
    overflow: hidden;
    position: relative;
    background-color: #f5f5f5;
}

#fb-root {
    height: 0px;
}

</style>
<script>
(function( $ ) {
if (true)
	{
		$("#facebook-inbox").show();
		fbInboxFillPage(
		"{{ $information['fanpage-facebook'] }}",
		"{{ asset('news-03/images/fb-icon-1.png') }}",
		"#3366CC",
		"#797979",
		"#FFFFFF",
		"0",
		"Chat với chúng tôi"
	);
	facebookShowPanelButton();
}

function facebookShowPanelButton() {
    $(".facebook-inbox-tab").click(function () {
        var isHiden = $("#facebook-inbox").attr("hiden");
        fbInboxHideBottom(isHiden);
    });
}

function fbInboxHideBottom(isHiden){

    if (isHiden !== undefined && isHiden !== "false") {
        $("#facebook-inbox").attr("hiden", false);
        $("#facebook-inbox-frame").hide();
    } else {
        $("#facebook-inbox-frame").show();
        $("#facebook-inbox").attr("hiden", true);
    }
}

function fbInboxFillPage(facebookPage, imageSrc, bgColor, bdColor, textColor, location, title){
    if (facebookPage.indexOf("facebook.com/") <= -1) {
        facebookPage =  "https://facebook.com/" + facebookPage;
    }
    $("#facebook-inbox-frame .fb-page").attr("data-href", facebookPage);
    $("#facebook-inbox-frame .fb-page blockquote").attr("cite", facebookPage);
    $("#facebook-inbox-frame .fb-page blockquote a").attr("href", facebookPage);
    if(imageSrc != ""){
        $(".facebook-inbox-tab-icon").html("<img src='" + imageSrc + "' />");
    }
    $(".facebook-inbox-tab .facebook-inbox-tab-title").html(title);
    $(".facebook-inbox-tab .facebook-inbox-tab-title").css('font-weight','bold');
}
})( jQuery );
</script>




