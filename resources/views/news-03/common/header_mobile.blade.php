<div id="dl-menu" class="dl-menuwrapper">
    <button class="dl-trigger"><i class="fa fa-align-justify" aria-hidden="true" style="font-size: 28px"></i></button>
    <ul class="dl-menu">
        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
            @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
            <li>
                <a href="{{ $menuelement['url'] }}" title="">{{ $menuelement['title_show'] }}</a>
            </li>
            @endforeach
        @endforeach
    </ul>
</div>
<!-- /dl-menuwrapper -->
<script>
    $(function() {
        $('#dl-menu').dlmenu({
            animationClasses: {
                classin: 'dl-animate-in-2',
                classout: 'dl-animate-out-2'
            }
        });
    });
</script>