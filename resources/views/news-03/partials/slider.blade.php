<section class="homepage-section animate wow fadeIn">
    <div class="flexslider">
        <ul class="slides">
            @foreach(\App\Entity\SubPost::showSubPost('slider',6) as $id => $img)
                <li>
                    <a href="{{ isset($img['link']) ? $img['link'] : '#' }}">
                        <img src="{{ isset($img['image']) ? asset($img['image']) : '' }}" alt="" />
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="clear"></div>
</section>