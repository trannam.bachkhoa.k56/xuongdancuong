@extends('travels-01.layout.site')

@section('title',isset($category->title) ? $category->title : '')
@section('meta_description','')
@section('keywords', '')

@section('content')
	<link rel="stylesheet" href="{{ asset('travels-01/css/herderNoslider.css') }}" media="all" type="text/css" />
	<section class="bread-crumb">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<ul class="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
						<li class="home">
							<a itemprop="url"  href="/" ><span itemprop="title">Trang chủ</span></a>
							<span><i class="fa fa-angle-right"></i></span>
						</li>
						<li><strong itemprop="title">Liên hệ</strong></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<div class="container container-fix-hd contact margin-bottom-30">
		<div class="box-heading relative">
			<h1 class="title-head">{{ isset($category->title) ? $category->title : '' }}</h1>
		</div>
		<div class="row">
			<div class="col-sm-5">
				<div class="contact-box-info clearfix margin-bottom-30">
					<div>
						<div class="item">
							<h3>
								Cửa hàng chính
							</h3>
							<p>
								@if(isset($information['info-shop']))
                                    <?= $information['info-shop'] ?>
								@else

								@endif
							</p>
						</div>
					</div>
				</div>
				<div id="login">
					<h2 class="title-head"><span> Gửi tin nhắn cho chúng tôi</span></h2>

					<p style="color: red">{{ $success == 1 ? 'Gửi liên hệ thành công' : 'Gửi liên hệ thất bại ' }}</p>
					<form  action="{{ route('sub_contact') }}" id='contact fContact' method='post'>
						{!! csrf_field() !!}
						<div id="emtry_contact" class="form-signup form_contact clearfix">
							<div class="row row-8Gutter">
								<div class="col-md-12">
									<fieldset class="form-group">
										<input type="text" placeholder="Họ tên*" name="name" id="name" class="form-control  form-control-lg" required>
									</fieldset>
								</div>
								<div class="col-md-12">
									<fieldset class="form-group">
										<input type="email" placeholder="Email*" name="email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$" data-validation="email"  id="email" class="form-control form-control-lg" required>
									</fieldset>
								</div>
								<div class="col-md-12">
									<fieldset class="form-group">
										<input type="number" placeholder="Điện thoại*" name="phone"  class="form-control form-control-lg" required>
									</fieldset>
								</div>
								<div class="col-md-12">
									<fieldset class="form-group">
										<input type="text" placeholder="Địa chỉ*" name="address"  class="form-control form-control-lg" required>
									</fieldset>
								</div>
							</div>
							<fieldset class="form-group">
								<textarea name="Message" placeholder="Nhập nội dung*" id="comment" class="form-control form-control-lg" rows="6" Required></textarea>
							</fieldset>
							<div>
								<button tyle="submit" class="btn btn-primary" style="padding:0 40px;text-transform: inherit;">Gửi liên hệ</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-7">
				<div class="box-maps margin-bottom-30">
					<div class="iFrameMap">
						<div class="google-map">
							@if(isset($information['nhung-ban-do']))
                                <?= $information['nhung-ban-do'] ?>
							@else

							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
		.google-map {width:100%;}
		.google-map .map {width:100%; height:650px; background:#dedede}
	</style>
@endsection
