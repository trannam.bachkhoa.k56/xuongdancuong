
@extends('news-03.layout.site')

@section('title', isset($information['meta-title']) ? $information['meta-title'] : '')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '')

@section('content')
    {{--<h1 class="hidden">{{isset($information['meta_title']) ? $information['meta_title'] : ''}}</h1>--}}
    {{--@include('travels-01.partials.slider')--}}
    <div class="content-wrapper">
        <!--  SLIDER -->
        @include('news-03.partials.slider')
		
        <section class="homepage-section animate wow fadeIn no-fouc best-sellers" style="margin-top:20px">
            <div class="row">
				<div class="section-title desktop-12 tablet-6 mobile-3">
					<div>
						<div class="section-title lines desktop-12 tablet-6 mobile-3">
							<?php $cateFeat = (\App\Entity\Category::getDetailCategory('san-pham-noi-bat')); ?>
							<h1><a href="">{{ $cateFeat->title }}</a></h1></div>

						<div class="collection-carousel desktop-12 tablet-6 mobile-3">

							@foreach (\App\Entity\Product::showProduct('san-pham-noi-bat',10) as $id => $product_feat)
								<div class="lazyOwl" id="product-listing-9519862605" data-alpha="Black Cockatoo Canvas Art" data-price="16900">
									<div class="product-index-inner">
									</div>
									<div class="prod-image">
										<a class="product-link" href="{{ route('product', [ 'post_slug' => $product_feat->slug]) }}"
										   title="{{ $product_feat->tilte }}">
											<div class="hover-element">
											</div>
											<img src="{{ !empty($product_feat->image) ?  asset($product_feat->image) : '' }}"
												 alt="{{ $product_feat->tilte }}"
												 data-image="{{ !empty($product_feat->image) ?  asset($product_feat->image) : '' }}"
												 data-alt-image="{{ !empty($product_feat->image) ?  asset($product_feat->image) : '' }}" />
										</a>
									</div>
									<div class="product-info">
										<div class="product-info-inner">
											<a href="{{ route('product', [ 'post_slug' => $product_feat->slug]) }}">

												<h3>{{ $product_feat->title }}

												</h3>
											</a>
											 <div class="price">
												<div class="prod-price">
												@if($product_feat->discount > 0 )
													 <del>{{ number_format($product_feat->price) }} đ </del>  
													<span>{{ number_format($product_feat->discount) }}đ</span>	
												@else
													<span>{{ number_format($product_feat->price) }}đ</span>	
												@endif
											   
													<!-- - $899.00 -->
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</div>
						
						@foreach (\App\Entity\Menu::showWithLocation('menu-tab-index') as $mainMenu)
                    @foreach (\App\Entity\MenuElement::showMenuPageArray($mainMenu->slug) as $id => $menuTop)
                <?php $cateSlug = explode('/', $menuTop['url']); ?>
                @if (isset($cateSlug[2]))
                <div class="section-title lines desktop-12 tablet-6 mobile-3">

                    <?php $cateFeat = (\App\Entity\Category::getDetailCategory($cateSlug[2])); ?>
                    <h2><a href="{{ route('site_category_product', ['cate_slug' => $cateSlug[2]]) }}">{{ $cateFeat->title }}</a></h2>
                </div>
                <div class="collection-carousel desktop-12 tablet-6 mobile-3">
                    @foreach (\App\Entity\Product::showProduct($cateFeat->slug, 10) as $id => $product_feat)
                        <div class="lazyOwl" id="product-listing-9519862605" data-alpha="Black Cockatoo Canvas Art" data-price="16900">
                            <div class="product-index-inner">
                            </div>
                            <div class="prod-image">
                                <a class="product-link" href="{{ route('product', [ 'post_slug' => $product_feat->slug]) }}"
                                   title="{{ $product_feat->tilte }}">
                                    <div class="hover-element">
                                    </div>
                                    <img src="{{ !empty($product_feat->image) ?  asset($product_feat->image) : '' }}"
                                         alt="{{ $product_feat->tilte }}"
                                         data-image="{{ !empty($product_feat->image) ?  asset($product_feat->image) : '' }}"
                                         data-alt-image="{{ !empty($product_feat->image) ?  asset($product_feat->image) : '' }}" />
                                </a>
                            </div>
                            <div class="product-info">
                                <div class="product-info-inner">
                                    <a href="{{ route('product', [ 'post_slug' => $product_feat->slug]) }}">

                                        <h3>{{ $product_feat->title }}

                                        </h3>
                                    </a>
                                    <div class="price">
                                        <div class="prod-price">
										@if($product_feat->discount > 0 )
											 <del>{{ number_format($product_feat->price) }} đ </del>  
											<span>{{ number_format($product_feat->discount) }}đ</span>	
										@else
											<span>{{ number_format($product_feat->price) }}đ</span>	
										@endif
                                       
                                            <!-- - $899.00 -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @endif
                    @endforeach
                    @endforeach
					</div>
				</div>
			<div>
		</section>
       <section class="homepage-section animate wow fadeIn no-fouc">
            <div class="row">
                <div class="homepage-promo desktop-12 mobile-12">

                    <div class="promo-inner" style="width: 100%;">
                        <a href="#">
                            <img src="{{ !empty($information['banner-footer']) ?  asset($information['banner-footer']) : '' }}" alt="">
                            <div class="caption">
                                <h3></h3>
                                <p></p>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
        </section>
        <section class="homepage-section animate wow fadeIn no-fouc">
            <div id="instagram-card" class="row">
                <div class="section-title lines">
                    <h2>TIN TỨC MỚI NHẤT</h2></div>
                <div id="instafeed"></div>
            </div>
			<div class="row listNews">
				@foreach(\App\Entity\Post::categoryShow('tin-tuc',4) as $id=>$intro)
					<div class="item desktop-3 min-half">
						<div class="CropImg CropImg70"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $intro->slug]) }}" class="thumbs"><img src="{{ isset($intro['image']) ? asset($intro['image']) : ''}}" alt="{{ isset($intro['title']) ?$intro['title'] : ''}}" /></a></div>
						<h3><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $intro->slug]) }}"></a>{{ isset($intro['title']) ?$intro['title'] : ''}}</h3>
					</div>
				@endforeach
			</div>
        </section>
        <div class="load-wait">
            <i class="icon-spinner icon-spin icon-large"></i>
        </div>
    </div>
@endsection