@extends('news-03.layout.site')

@section('title', isset($category->title) ?$category->title : '')
@section('meta_description',  isset($category->description) ?$category->description : '')
@section('keywords', '')

@section('content')
<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="desktop-12">
				<ul class="breadcrumb">
					<li class="home">
						<a itemprop="url"  href="/" ><span itemprop="title">Trang chủ</span></a>
						<span><i class="fa fa-angle-right"></i> </span>
					</li>
					<li><strong ><span itemprop="title">{{isset($category->title) ? $category->title : ''}}</span></strong></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="content-wrapper">
	<div id="content" class="row">
		<h1 class="page-title desktop-12">
			{{isset($category->title) ? $category->title : ''}}
		</h1>
		<!-- End Sidebar -->
		<div class="desktop-9 tablet-9 mobile-12">
			<div class="newCategory">
				@foreach($posts as $new)
				<div class="item clearfix">
					<div class="desktop-4 tablet-4 mobile-12">
						<div class="CropImg CropImg70"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}" class="thumbs"><img src="{{ $new->image }}" alt="{{ $new->title }}" /></a></div>
					</div>
					<div class="desktop-8 tablet-8 mobile-12">
						<h3 class="title"><a title="{{ $new->title }}" href="{{ route('post', ['cate_slug' => 'tin-tuc-moi', 'post_slug' => $new->slug]) }}">{{ $new->title }}</a></h3>
						<div class="timePost">
							Ngày đăng: <?php $date=date_create($new->created_at);
							echo date_format($date,"d/m/Y"); ?>
						</div>
						<div class="description">
							{{ $new->description }}
						</div>
						<div class="readMore"><a href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $new->slug]) }}">Đọc thêm</a></div>
					</div>
				</div>
				<div class="clear"></div>
				@endforeach
			</div>
			<div class="paginationProduct">
				{{ $posts->links() }}
			</div>
		</div>
		<div class="desktop-3 tablet-3 mobile-12">
			<div class="sidebarNews">
				<div class="box">
					<h3>Fanpage Facebook</h3>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=581707311893144&autoLogAppEvents=1';
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-page" data-href="{{ !empty($information['fanpage-facebook']) ?  asset($information['fanpage-facebook']) : '' }}" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="{{ !empty($information['fanpage-facebook']) ?  asset($information['fanpage-facebook']) : '' }}" class="fb-xfbml-parse-ignore"><a href="{{ !empty($information['fanpage-facebook']) ?  asset($information['fanpage-facebook']) : '' }}">Tiệm Tranh Dương Xỉ - Tranh Canvas Decor</a></blockquote></div>
				</div>
			</div>
		</div>
	</div>
</div>
         
@endsection
