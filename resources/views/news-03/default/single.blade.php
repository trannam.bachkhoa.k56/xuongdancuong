@extends('news-03.layout.site')

@section('title', !empty($post->meta_title) ? $post->meta_title : $post->title)
@section('meta_description',  !empty($post->meta_description) ? $post->meta_description : $post->description)
@section('keywords', $post->meta_keyword )
@section('meta_image', asset($post->image) )
@section('meta_url', route('post', ['cate_slug' =>  $category->slug, 'post_slug' => $post->slug]) )

@section('content')
<section class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="desktop-12">
				<ul class="breadcrumb">
					<li class="home">
						<a itemprop="url"  href="/" ><span itemprop="title">Trang chủ</span></a>
						<span><i class="fa fa-angle-right"></i> </span>
					</li>
					<li><strong ><span itemprop="title">{{ $post->title }}</span></strong></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="content-wrapper">
	<div id="content" class="row">
		<h1 class="page-title desktop-12">
			{{ $post->title }}
		</h1>
		<!-- End Sidebar -->
		<div class="desktop-9 tablet-9 mobile-12">
			<div class="newDetail">
				<div class="description">
					<b>{{ $post->description }}</b>
				</div>
				<div class="mainContent">
					{!! $post->content !!}
				</div>
			</div>
			<div class="relateNews">
				<h2>BÀI VIẾT LIÊN QUAN</h2>
				<ul class="list">
					@foreach (\App\Entity\Post::relativeProduct($post->slug,6) as $id => $postRelative)
					<li><a title="{{ $postRelative->title }}" href="{{ route('post', ['cate_slug' => 'tin-tuc', 'post_slug' => $postRelative->slug]) }}">{{ $postRelative->title }}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="desktop-3 tablet-3 mobile-12">
			<div class="sidebarNews">
				<div class="box">
					<h3>Fanpage Facebook</h3>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=581707311893144&autoLogAppEvents=1';
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-page" data-href="{{ !empty($information['fanpage-facebook']) ?  asset($information['fanpage-facebook']) : '' }}" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="{{ !empty($information['fanpage-facebook']) ?  asset($information['fanpage-facebook']) : '' }}" class="fb-xfbml-parse-ignore"><a href="{{ !empty($information['fanpage-facebook']) ?  asset($information['fanpage-facebook']) : '' }}">Tiệm Tranh Dương Xỉ - Tranh Canvas Decor</a></blockquote></div>
				</div>
			</div>
		</div>
	</div>
</div>        
@endsection



