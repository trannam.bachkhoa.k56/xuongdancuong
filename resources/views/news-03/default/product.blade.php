@extends('news-03.layout.site')

@section('title', isset($product->title) ? $product->title : '' )
@section('meta_description',  !empty($product->meta_description) ? $product->meta_description : $product->description)
@section('keywords', $product->meta_keyword)
@section('content')
    <div class="clearfix"></div>
    <section class="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <?php \App\Entity\Post::getBreadcrumb($product->post_id,'san-pham')?>
                    </ul>
					
				
                </div>
            </div>
        </div>
    </section>
    <div class="content-wrapper">
        <div id="content" class="row">
            <div itemscope itemtype="http://schema.org/Product" id="product-520402141244">
                <div id="mobile-product" class="desktop-12 tablet-6 mobile-3">
                    <ul class="bxslider">
                        @if(!empty($product->image_list))
                            <?php $i = 1;?>
                            @foreach(explode(',', $product->image_list) as $imageProduct)
                                @if($i == 1)
                                    <li>
                                        <img class="featured" data-zoom-image="{{$imageProduct}}"
                                             data-image-id=""
                                             src="{{$imageProduct}}"
                                             alt="">
                                    </li>
                                @else
                                    <li>
                                        <a href="{{$imageProduct}}" class="fancybox">
                                            <img data-image-id="<?php echo $i?>"
                                                 data-zoom-image="{{$imageProduct}}"
                                                 src="{{$imageProduct}}"
                                                 alt="">
                                        </a>
                                    </li>
                                @endif
                            <?php $i++; ?>
                            @endforeach
                        @endif
                    </ul>
                    <div id="bx-pager" style="display:none">
                        @if(!empty($product->image_list))
                            <?php $i = 1;?>
                            @foreach(explode(',', $product->image_list) as $imageProduct)
                                <a class="thumbnail" data-slide-index="1" data-image-id="<?php echo $i?>" href="">
                                    <img src="{{$imageProduct}}" /></a>
                                <?php $i++; ?>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- For Desktop -->
                <!--<div id="product-photos" class="desktop-7 tablet-3 mobile-3">
                    <div class="bigimage desktop-12 tablet-5">
                        <img id="520402141244" src="" data-image-id="" data-zoom-image="" alt='' title="Feather Canvas" />
                    </div>
                    <div id="520402141244-gallery" class="desktop-12 tablet-1">
                        <div class="thumbnail-slider">
                            @if(!empty($product->image_list))
                                <?php $i = 1;?>
                                @foreach(explode(',', $product->image_list) as $imageProduct)
                                    <div class="slide">
                                        <a href="#" data-image="{{$imageProduct}}"
                                           data-image-id="<?php echo $i ?>"
                                           data-zoom-image="{{$imageProduct}}">
                                            <img class="thumbnail"
                                                 src="{{$imageProduct}}"
                                                 data-image-id="<?php echo $i ?>" alt="" data-image-id="<?php echo $i ?>" />
                                        </a>
                                    </div>
                                 <?php $i++; ?>
                                 @endforeach
                            @endif

                        </div>
                    </div>
					
					
					
					
					
					
					
					
					
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {

                        $('.bxslider').bxSlider({
                            pagerCustom: '#bx-pager',
                            prevText: "",
                            nextText: "",
                        });
                        pager: false

                        $('.thumbnail-slider').bxSlider({
                            mode: 'horizontal',
                            minSlides: 2,
                            maxSlides: 5,
                            slideMargin: 5,
                            infiniteLoop: true,
                            pager: false,
                            prevText: "",
                            nextText: "",
                            slideWidth: 128,
                            hideControlOnEnd: true
                        });

                        //initiate the plugin and pass the id of the div containing gallery images
                        $("#520402141244").elevateZoom({
                            gallery: '520402141244-gallery',
                            cursor: 'pointer',
                            galleryActiveClass: 'active',
                            borderColour: '#eee',
                            borderSize: '1',
                            zoomWindowWidth: 605,
                            zoomWindowHeight: 548,
                            zoomWindowOffety: 0,
                            responsive: false,
                            zoomWindowPosition: 1
                        });

                        //pass the images to Fancybox
                        $("#520402141244").bind("click", function(e) {
                            var ez = $('#520402141244').data('elevateZoom');
                            $.fancybox(ez.getGalleryList());
                            return false;
                        });

                    });
                </script>
				-->
				
				
				
				
				
				
				
				
				<div id="product-photos" class="desktop-7 tablet-3 mobile-3">

                        <div class="bigimage desktop-12 tablet-5">
							@if(!empty($product->image_list))
									<?php $i = 1;?>
									@foreach(explode(',', $product->image_list) as $imageProduct)
									@if($i == 1)
										<img id="520402141244" src="{{$imageProduct}}" data-image-id="" data-zoom-image="{{$imageProduct}}" alt='' title="Feather Canvas" />
									@endif
										<?php $i++;?>
									 @endforeach
								@endif
                            <!--<img id="520402141244" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7_1024x1024.jpg?v=1522150047" data-image-id="" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-7.jpg?v=1522150047" alt='' title="Feather Canvas" />-->
                        </div>

                        <div id="520402141244-gallery" class="desktop-12 tablet-1">
							<div class="thumbnail-slider">
								@if(!empty($product->image_list))
									<?php $i = 1;?>
									@foreach(explode(',', $product->image_list) as $imageProduct)
										<div class="slide">
											<a href="#" data-image="{{$imageProduct}}"
											   data-image-id="<?php echo $i ?>"
											   data-zoom-image="{{$imageProduct}}">
												<img class="thumbnail"
													 src="{{$imageProduct}}"
													 data-image-id="<?php echo $i ?>" alt="" data-image-id="<?php echo $i ?>" />
											</a>
										</div>
									 <?php $i++; ?>
									 @endforeach
								@endif
							</div>
						
                            <!--<div class="thumbnail-slider">
                                <div class="slide">
                                    <a href="#" data-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5_1024x1024.jpg?v=1522150044" data-image-id="2149691719740" data-zoom-image="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5.jpg?v=1522150044">
                                        <img class="thumbnail" src="//cdn.shopify.com/s/files/1/1244/6908/products/feather-canvas-by-the-print-emporium-5_compact.jpg?v=1522150044" data-image-id="2149691719740" alt="Feather Canvas-The Print Emporium" data-image-id="2149691719740" />
                                    </a>
                                </div>
                            </div>-->
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {

                            $('.bxslider').bxSlider({
                                pagerCustom: '#bx-pager',
                                prevText: "",
                                nextText: "",
                            });
                            pager: false

                            $('.thumbnail-slider').bxSlider({
                                mode: 'horizontal',
                                minSlides: 2,
                                maxSlides: 5,
                                slideMargin: 5,
                                infiniteLoop: true,
                                pager: false,
                                prevText: "",
                                nextText: "",
                                slideWidth: 128,
                                hideControlOnEnd: true
                            });

                            //initiate the plugin and pass the id of the div containing gallery images
                            $("#520402141244").elevateZoom({
                                gallery: '520402141244-gallery',
                                cursor: 'pointer',
                                galleryActiveClass: 'active',
                                borderColour: '#eee',
                                borderSize: '1',
                                zoomWindowWidth: 605,
                                zoomWindowHeight: 548,
                                zoomWindowOffety: 0,
                                responsive: false,
                                zoomWindowPosition: 1
                            });

                            //pass the images to Fancybox
                            $("#520402141244").bind("click", function(e) {
                                var ez = $('#520402141244').data('elevateZoom');
                                $.fancybox(ez.getGalleryList());
                                return false;
                            });

                        });
                    </script>
				
				
                <div id="product-right" class="desktop-5 tablet-3 mobile-3">
                    <div id="product-description">
                        <h1 itemprop="name">{{ $product->title }}</h1>
                        <div itemprop="offers" itemscope itemtype="">
                            <p id="product-price">
                                <span class="product-price price" itemprop="price">
										@if($product->discount > 0 )
											 <del>{{ number_format($product->price) }} đ </del>  
											<span>{{ number_format($product->discount) }}đ</span>	
										@else
											<span>{{ number_format($product->price) }}đ</span>	
									@endif
								</span>
                            </p>
                                <div class="product-add">
                                    <form method="post" onsubmit="return addToOrder(this);" id="add-to-cart-form" class="form-inline margin-bottom-10 dqdt-form"
                                          enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="qty-selection">
                                        <h5>Chọn số lượng</h5>

                                        <input min="1" type="number" name="quantity[]" class="quantity" value="1" />
                                        <input type="hidden" name="product_id[]" value="{{ $product->product_id }}">

                                    </div>
                                        <button class="btn-cart btn btn-primary  left-to"  title="Chọn sản phẩm"  type="submit">
                                            Thêm mới giỏ hàng
                                        </button>

                                    </form>
                                </div>


                                <p class="add-to-cart-msg"></p>
                                <!-- zipmoney widgets -->
                                <style>
                                    #shopify-zip-cart-widget > iframe {
                                        height: 75px!important;
                                        width: 213px !important;
                                    }
                                </style>
                                <span style="cursor:pointer" id="shopify-zip-cart-widget" data-zm-asset="cartwidget" data-zm-widget="popup" data-zm-popup-asset="termsdialog"></span>
                        </div>
                        <div class="desc">
                            {{ $product->description }}
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div id="lower-description" class="desktop-8 desktop-push-2 tablet-6 mobile-3">
                    <div class="section-title lines">
                        <h2>Mô tả tranh</h2>
                    </div>
                    <div class="rte" itemprop="description">
                        <meta charset="utf-8">
                        <?= $product->content ?>
                    </div>
                </div>
                <div class="clear"></div>
                <!-- product feather-canvas set-price -->
                <!-- product.price 16900: wh_price 16900-->
                <!-- product.price_min 16900 : wh_price_min 16900 -->
                <!-- product.price_max 89900 : wh_price_max 89900 -->
                <!-- wh_discount_value 1 -->
                <!-- compare_at_price : wh_compare_at_price  -->
                <!-- compare_at_price_min 0: wh_compare_at_price_min 0 -->
                <!-- compare_at_price_max 0: wh_compare_at_price_max 0 -->
                <div class="desktop-12 tablet-6 mobile-3" id="related">
                    <div class="section-title lines tablet-6 desktop-12 mobile-3">
                        <h2>Sản phẩm liên quan</h2>
                    </div>
                    <div class="collection-carousel-detail desktop-12 tablet-6 mobile-3">
                        @foreach (\App\Entity\Product::relativeProduct($product->slug, $product->product_id) as $id => $productRelative)
                            <div class="lazyOwl" id="prod-520402141244" data-alpha="{{ $productRelative->title }}" data-price="{{ $product->price }}">
                                <div class="prod-image">
                                    <a class="product-link" href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}"
                                       title="{{ $productRelative->title }}">
                                        <div class="hover-element"></div>
                                        <img src="{{ !empty($productRelative->image) ?  asset($productRelative->image) : '' }}"
                                             alt="{{ $productRelative->title }}" />
                                    </a>
                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="{{ route('product', [ 'post_slug' => $productRelative->slug]) }}">
                                            <h3>{{ $productRelative->title }}</h3>
                                        </a>
                                        <div class="price">
											<div class="prod-price">
											@if($productRelative->discount > 0 )
												 <del>{{ number_format($productRelative->price) }} đ </del>  
												<span>{{ number_format($productRelative->discount) }}đ</span>	
											@else
												<span>{{ number_format($productRelative->price) }}đ</span>	
											@endif
										   
												<!-- - $899.00 -->
											</div>
										</div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                     </div>
                <div class="clear"></div>


            </div>
        </div>
    </div>




@endsection
