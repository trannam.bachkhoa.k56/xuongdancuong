<<<<<<< HEAD
@extends('news-03.layout.site')

@section('title', isset($information['meta-title']) ? $information['meta-title'] : '')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '')

@section('content')
    <section class="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <li class="home">
                            <a itemprop="url"  href="/" ><span itemprop="title">Trang chủ</span></a>
                            <span><i class="fa fa-angle-right"></i> >> </span>
                        </li>
                        <li><strong ><span itemprop="title">Lọc sản phẩm</span></strong></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <style>
        .bread-crumb .breadcrumb
        {
            list-style: none;
        }
        .bread-crumb .breadcrumb li
        {
            display: inline-block;
        }
    </style>
    <div class="content-wrapper">
        <div id="content" class="row">
            <h1 class="page-title">Lọc sản phẩm</h1>
            <div id="collection-description" class="desktop-12 tablet-6 mobile-3">
                <div class="rte">
                </div>
            </div>
            <div class="clear"></div>
            <!-- Start Sidebar -->
            <form action="/loc-san-pham" method="get" id="filterProduct">
            <div class="show-desktop">
                <a class="show mobile-3" href="#"><i class="icon-align-justify icon-2x"></i></a>
                <div class="desktop-2 tablet-4 tablet-push-1 mobile-3" id="sidebar">
                    <ul>
                    </ul>
                    <h4>Danh mục sản phẩm</h4>
                    <ul>

                        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                            @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                                <li style="height: 30px;">
                                    <a href="{{ $menuelement['url'] }}" title="{{ $menuelement['title_show'] }}">{{ $menuelement['title_show'] }}</a>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>

                    <!-- Start Filtering -->

                    <h4>Lọc sản phẩm</h4>
                    @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                        <ul>
                            <li>
                                <b style="font-size: 14px">{{ $filterGroup->group_name }}</b>
                                <ul class="filter">
                                    @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                        <li>
                                            <a onclick="return selectColor(this);" style="display: -webkit-inline-box;">
                                                <input type="checkbox" name="filter[]" class="inputColor" value="{{ $filter->name_filter }}"
                                                       @if(!empty($_GET['filter']) && in_array($filter->name_filter, $_GET['filter']) >0) checked @endif
                                                       onChange="return changeFilter(this);"
                                                />
                                                <a href="#" style="font-size: 15px">{{ $filter->name_filter }}</a>
                                            </a>
                                        </li>
                                    @endforeach
                                    <script>
                                        function changeFilter(e) {
                                            $('#filterProduct').submit();
                                        }
                                    </script>
                                </ul>
                            </li>
                        </ul>
                    @endforeach
                </div>
            </div>
            </form>
            <div class="show-mobile">
                <div class="mobile-menu" id="sidebar-mobile">
                    <select id="mobile-collection-menu">
                        <optgroup label="">
                        </optgroup>
                        <optgroup label="SHOP BY THEME:">
                            @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                                @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                                    <li style="height: 30px;">
                                        <option value="{{ $menuelement['url'] }}">{{ $menuelement['title_show'] }}</option>
                                    </li>
                                @endforeach
                            @endforeach

                        </optgroup>

                    </select>
                    <!-- Start Filtering -->
                </div>
            </div>
            <!-- End Sidebar -->
            <div class="desktop-10 tablet-6 mobile-3">
                <div id="product-loop">

                    @if(empty($products))
                        <p>Không tồn tại danh mục này</p>
                    @else
                        @foreach ($products as $id => $product)
                            <div id="product-listing-520099332156" class="product-index desktop-4 tablet-half mobile-half" data-alpha="{{ $product->title }}" data-price="{{ number_format($product->price) }}đ">
                                <div class="product-index-inner">
                                </div>
                                <div class="prod-image">
                                    <a class="product-link" href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title="{{ $product->title }}">
                                        <div class="hover-element">
                                        </div>
                                        <img src="{{ !empty($product->image) ?  asset($product->image) : '' }}"
                                             alt="{{ $product->title }}"
                                             data-image="{{ !empty($product->image) ?  asset($product->image) : '' }}"
                                             data-alt-image="{{ !empty($product['anh-lon']) ?  asset($product['anh-lon']) : '' }}" />
                                    </a>
                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                                            <h3>{{ $product->title }}</h3>
                                        </a>
                                         <div class="price">
											<div class="prod-price">
											@if($product->discount > 0 )
												 <del>{{ number_format($product->price) }} đ </del>  
												<span>{{ number_format($product->discount) }}đ</span>	
											@else
												<span>{{ number_format($product->price) }}đ</span>	
											@endif
										   
												<!-- - $899.00 -->
											</div>
										</div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @endif
                </div>
            </div>
            <div id="pagination" class="desktop-12 tablet-6 mobile-3">
                @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{ $products->links() }}
                @endif
				<style>
				#pagination ul li
				{
					display: inline-block;
				}
					#pagination ul li a, #pagination ul li span
				{
					    font-size: 16px;
				}
				#pagination ul li span
				{
					color:red;
					    font-weight: bold;
					
				}
				</style>
            </div>
            <script>
                //load anh
                $(document).ready(function() {
                    function preload(arrayOfImages) {
                        $(arrayOfImages).each(function() {
                            (new Image()).src = this;
                        });
                    }

                    var images = [];

                    $('.prod-image').each(function() {
                        var img = $(this).find('img').data('alt-image');
                        images.push(img);
                    });
                    preload(images);

                    $('.product-index').hover( //When the ProductImage is hovered show the alt image.
                        function() {
                            var image = ($(this).find('.product-link img'));

                            if (image.attr('data-alt-image')) {
                                image.attr('src', image.attr('data-alt-image'));
                            };
                        },
                        function() {
                            var image = ($(this).find('.product-link img'));
                            image.attr('src', image.attr('data-image')); //Show normal image for non-hover state.
                        }
                    );

                });
            </script>
        </div>
    </div>
@endsection
=======
@extends('news-03.layout.site')

@section('title', isset($information['meta-title']) ? $information['meta-title'] : '')
@section('meta_description', isset($information['meta-description']) ? $information['meta-description'] : '')
@section('keywords', isset($information['meta-keyword']) ? $information['meta-keyword'] : '')

@section('content')
    <section class="bread-crumb">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb">
                        <li class="home">
                            <a itemprop="url"  href="/" ><span itemprop="title">Trang chủ</span></a>
                            <span><i class="fa fa-angle-right"></i> >> </span>
                        </li>
                        <li><strong ><span itemprop="title">Lọc sản phẩm</span></strong></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <style>
        .bread-crumb .breadcrumb
        {
            list-style: none;
        }
        .bread-crumb .breadcrumb li
        {
            display: inline-block;
        }
    </style>
    <div class="content-wrapper">
        <div id="content" class="row">
            <h1 class="page-title">Lọc sản phẩm</h1>
            <div id="collection-description" class="desktop-12 tablet-6 mobile-3">
                <div class="rte">
                </div>
            </div>
            <div class="clear"></div>
            <!-- Start Sidebar -->
            <form action="/loc-san-pham" method="get" id="filterProduct">
            <div class="show-desktop">
                <a class="show mobile-3" href="#"><i class="icon-align-justify icon-2x"></i></a>
                <div class="desktop-2 tablet-4 tablet-push-1 mobile-3" id="sidebar">
                    <ul>
                    </ul>
                    <h4>Danh mục sản phẩm</h4>
                    <ul>

                        @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                            @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                                <li style="height: 30px;">
                                    <a href="{{ $menuelement['url'] }}" title="{{ $menuelement['title_show'] }}">{{ $menuelement['title_show'] }}</a>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>

                    <!-- Start Filtering -->

                    <h4>Lọc sản phẩm</h4>
                    @foreach(\App\Entity\FilterGroup::showFilterGroup() as $id => $filterGroup)
                        <ul>
                            <li>
                                <b style="font-size: 14px">{{ $filterGroup->group_name }}</b>
                                <ul class="filter">
                                    @foreach(\App\Entity\Filter::showFilter($filterGroup->group_filter_id) as $id => $filter)
                                        <li>
                                            <a onclick="return selectColor(this);" style="display: -webkit-inline-box;">
                                                <input type="checkbox" name="filter[]" class="inputColor" value="{{ $filter->name_filter }}"
                                                       @if(!empty($_GET['filter']) && in_array($filter->name_filter, $_GET['filter']) >0) checked @endif
                                                       onChange="return changeFilter(this);"
                                                />
                                                <a href="#" style="font-size: 15px">{{ $filter->name_filter }}</a>
                                            </a>
                                        </li>
                                    @endforeach
                                    <script>
                                        function changeFilter(e) {
                                            $('#filterProduct').submit();
                                        }
                                    </script>
                                </ul>
                            </li>
                        </ul>
                    @endforeach
                </div>
            </div>
            </form>
            <div class="show-mobile">
                <div class="mobile-menu" id="sidebar-mobile">
                    <select id="mobile-collection-menu">
                        <optgroup label="">
                        </optgroup>
                        <optgroup label="SHOP BY THEME:">
                            @foreach (\App\Entity\Menu::showWithLocation('menu-chinh') as $Mainmenu)
                                @foreach (\App\Entity\MenuElement::showMenuPageArray($Mainmenu->slug) as $id=>$menuelement)
                                    <li style="height: 30px;">
                                        <option value="{{ $menuelement['url'] }}">{{ $menuelement['title_show'] }}</option>
                                    </li>
                                @endforeach
                            @endforeach

                        </optgroup>

                    </select>
                    <!-- Start Filtering -->
                </div>
            </div>
            <!-- End Sidebar -->
            <div class="desktop-10 tablet-6 mobile-3">
                <div id="product-loop">

                    @if(empty($products))
                        <p>Không tồn tại danh mục này</p>
                    @else
                        @foreach ($products as $id => $product)
                            <div id="product-listing-520099332156" class="product-index desktop-4 tablet-half mobile-half" data-alpha="{{ $product->title }}" data-price="{{ number_format($product->price) }}đ">
                                <div class="product-index-inner">
                                </div>
                                <div class="prod-image">
                                    <a class="product-link" href="{{ route('product', [ 'post_slug' => $product->slug]) }}" title="{{ $product->title }}">
                                        <div class="hover-element">
                                        </div>
                                        <img src="{{ !empty($product->image) ?  asset($product->image) : '' }}"
                                             alt="{{ $product->title }}"
                                             data-image="{{ !empty($product->image) ?  asset($product->image) : '' }}"
                                             data-alt-image="{{ !empty($product['anh-lon']) ?  asset($product['anh-lon']) : '' }}" />
                                    </a>
                                </div>
                                <div class="product-info">
                                    <div class="product-info-inner">
                                        <a href="{{ route('product', [ 'post_slug' => $product->slug]) }}">
                                            <h3>{{ $product->title }}</h3>
                                        </a>
                                         <div class="price">
											<div class="prod-price">
											@if($product->discount > 0 )
												 <del>{{ number_format($product->price) }} đ </del>  
												<span>{{ number_format($product->discount) }}đ</span>	
											@else
												<span>{{ number_format($product->price) }}đ</span>	
											@endif
										   
												<!-- - $899.00 -->
											</div>
										</div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @endif
                </div>
            </div>
            <div id="pagination" class="desktop-12 tablet-6 mobile-3">
                @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                    {{ $products->links() }}
                @endif
				<style>
				#pagination ul li
				{
					display: inline-block;
				}
					#pagination ul li a, #pagination ul li span
				{
					    font-size: 16px;
				}
				#pagination ul li span
				{
					color:red;
					    font-weight: bold;
					
				}
				</style>
            </div>
            <script>
                //load anh
                $(document).ready(function() {
                    function preload(arrayOfImages) {
                        $(arrayOfImages).each(function() {
                            (new Image()).src = this;
                        });
                    }

                    var images = [];

                    $('.prod-image').each(function() {
                        var img = $(this).find('img').data('alt-image');
                        images.push(img);
                    });
                    preload(images);

                    $('.product-index').hover( //When the ProductImage is hovered show the alt image.
                        function() {
                            var image = ($(this).find('.product-link img'));

                            if (image.attr('data-alt-image')) {
                                image.attr('src', image.attr('data-alt-image'));
                            };
                        },
                        function() {
                            var image = ($(this).find('.product-link img'));
                            image.attr('src', image.attr('data-image')); //Show normal image for non-hover state.
                        }
                    );

                });
            </script>
        </div>
    </div>
@endsection
>>>>>>> b77353c0c09403b8d55a61f4892f4ec4e27de23b
