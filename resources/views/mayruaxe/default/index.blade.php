@extends('mayruaxe.layout.site')

@section('title', isset($information['meta_title']) ? $information['meta_title'] : '')
@section('meta_description', isset($information['meta_description']) ? $information['meta_description'] : '')
@section('keywords', isset($information['meta_keyword']) ? $information['meta_keyword'] : '')

@section('content')
    @include ('mayruaxe.partials.block1')
    @include ('mayruaxe.partials.block2')
    @include ('mayruaxe.partials.block3')
    @include ('mayruaxe.partials.block4')
    @include ('mayruaxe.partials.block5')
    @include ('mayruaxe.partials.block6')
    @include ('mayruaxe.partials.block7')
    @include ('mayruaxe.partials.block8')
@endsection

