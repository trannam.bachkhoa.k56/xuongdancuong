<div id="SECTION775" class="widget-section ladi-drop lazy-hidden ui-droppable" lp-type="widget_section"
     lp-widget="widget" lp-lang="SECTION" lp-display="block">
    <div class="container">
        <div id="BOX776" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="box" lp-lang="BOX"
             lp-display="block">
            <div class="widget-content">
                <div id="HEADLINE777" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
                     lp-lang="HEADLINE" lp-display="block"><h2 class="widget-content" lp-node="h2">Bạn muốn nhận
                        được Ưu đãi cực tốt</h2></div>
                <div id="PARAGRAPH778" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
                     lp-lang="PARAGRAPH" lp-display="block"><h5 class="widget-content" lp-node="h5">Vui lòng để lại
                        số điện thoại và địa chỉ để được tư vấn và gửi hàng miễn phí toàn quốc.</h5></div>
                <div id="SHAPE779" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE"
                     lp-display="block">
                    <div class="widget-content">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             width="100%" height="100%" viewBox="0 0 1536 1896.0833" fill="rgba(255,255,255,1)">
                            <path d="M1284 897q0-27-18-45l-91-91q-18-18-45-18t-45 18L896 950V448q0-26-19-45t-45-19H704q-26 0-45 19t-19 45v502L451 761q-19-19-45-19t-45 19l-91 91q-18 18-18 45t18 45l362 362 91 91q18 18 45 18t45-18l91-91 362-362q18-18 18-45zm252-1q0 209-103 385.5T1153.5 1561 768 1664t-385.5-103T103 1281.5 0 896t103-385.5T382.5 231 768 128t385.5 103T1433 510.5 1536 896z"></path>
                        </svg>
                    </div>
                </div>
                <div id="BOX780" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="box"
                     lp-lang="BOX" lp-display="block">
                    <div class="widget-content">
                        <div id="HEADLINE781" class="widget-element widget-snap" lp-type="textinline"
                             lp-editor="true" lp-lang="HEADLINE" lp-display="block"><h3 class="widget-content"
                                                                                        lp-node="h3">Nhận tin khuyến
                                mãi từ LDP</h3></div>
                    </div>
                    <div class="ladi-widget-overlay"></div>
                </div>
                <div id="GROUP787" class="widget-element widget-snap widget-group" lp-type="widget_group"
                     lp-lang="GROUP" lp-display="block">
                    <div class="widget-content">
                        <div id="BOX788" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="box"
                             lp-lang="BOX" lp-group="GROUP787" lp-display="block">
                            <div class="widget-content"></div>
                            <div class="ladi-widget-overlay"></div>
                        </div>
                        <div id="BOX789" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="box"
                             lp-lang="BOX" lp-group="GROUP787" lp-display="block">
                            <div class="widget-content"></div>
                            <div class="ladi-widget-overlay"></div>
                        </div>
                        <div id="BOX790" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="box"
                             lp-lang="BOX" lp-group="GROUP787" lp-display="block">
                            <div class="widget-content"></div>
                            <div class="ladi-widget-overlay"></div>
                        </div>
                        <div id="BOX791" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="box"
                             lp-lang="BOX" lp-group="GROUP787" lp-display="block">
                            <div class="widget-content"></div>
                            <div class="ladi-widget-overlay"></div>
                        </div>
                        <div id="COUNTDOWN792" class="widget-element widget-snap" lp-type="countdown"
                             lp-lang="COUNTDOWN" lp-endtime="5555" lp-endtimetype="timedown" lp-group="GROUP787"
                             lp-display="block">
                            <div class="widget-content">
                                <div><span>3</span></div>
                                <div><span>20</span></div>
                                <div><span>35</span></div>
                                <div><span>0</span></div>
                            </div>
                        </div>
                        <div id="HEADLINE793" class="widget-element widget-snap" lp-type="textinline"
                             lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP787" lp-display="block"><h6
                                    class="widget-content" lp-node="h6">NGÀY</h6></div>
                        <div id="HEADLINE794" class="widget-element widget-snap" lp-type="textinline"
                             lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP787" lp-display="block"><h6
                                    class="widget-content" lp-node="h6">GIỜ</h6></div>
                        <div id="HEADLINE795" class="widget-element widget-snap" lp-type="textinline"
                             lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP787" lp-display="block"><h6
                                    class="widget-content" lp-node="h6">GIÂY</h6></div>
                        <div id="HEADLINE796" class="widget-element widget-snap" lp-type="textinline"
                             lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP787" lp-display="block"><h6
                                    class="widget-content" lp-node="h6">PHÚT</h6></div>
                    </div>
                </div>
                <div id="GROUP782" class="widget-element widget-snap widget-group" lp-type="widget_group"
                     lp-lang="GROUP" lp-display="block">
                    <div class="widget-content">
                        <form class="widget-content" onSubmit="return contact(this);">
                        {!! csrf_field() !!}
                        <input type="hidden" name="is_json" class="form-control captcha" value="1" placeholder="">
                        <div id="BUTTON783" class="widget-element widget-snap style-1" >
                            <button type="submit" class="widget-content">
                                GỬI THÔNG TIN ĐẾN CHÚNG TÔI
                            </button>
                        </div>
                        <div id="FORM784" class="widget-element widget-snap" lp-type="contact_form">

                                <div id="ITEM_FORM785"
                                     class="widget-element widget-snap widget-dragg widget-item-child"
                                     lp-type="item_form" lp-lang="ITEM_FORM" lp-display="block">
                                    <input class="widget-content" type="text" placeholder="Họ và tên" name="name"
                                            lp-label="Họ và tên" required="required">
                                </div>
                                <div id="ITEM_FORM798"
                                     class="widget-element widget-snap widget-dragg widget-item-child"
                                     lp-type="item_form" lp-lang="ITEM_FORM" lp-display="block">
                                    <input class="widget-content" type="tel" pattern="[0-9]{9,15}"
                                            required="required" placeholder="Nhập Số điện thoại" name="phone"
                                            lp-label="Điện thoại" lp-name-id="phone">
                                </div>
                                <div id="ITEM_FORM800"
                                     class="widget-element widget-snap widget-dragg widget-item-child"
                                     lp-type="item_form" lp-lang="ITEM_FORM" lp-display="block">
                                    <input
                                            class="widget-content" type="text" placeholder="Nhập địa chỉ"
                                            name="address" required="required">
                                </div>
                                <div id="ITEM_FORM801"
                                     class="widget-element widget-snap widget-dragg widget-item-child"
                                     lp-type="item_form" lp-lang="ITEM_FORM" lp-display="block">
                                    <textarea
                                            class="widget-content" type="text"
                                            placeholder="Để lại lời nhắn cho chúng tôi" name="message" rows="4"
                                            lp-label="Lời nhắn" lp-name-id="message" required="required"></textarea>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="ladi-widget-overlay"></div>
        </div>
    </div>
    <div class="ladi-widget-overlay"></div>
</div>