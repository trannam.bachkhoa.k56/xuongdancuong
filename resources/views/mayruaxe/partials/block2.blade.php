<div id="SECTION541" class="widget-section ladi-drop lazy-hidden ui-droppable screenS" lp-type="widget_section"
     lp-widget="widget" lp-lang="SECTION" lp-display="block">
    <div class="container">
        <div id="HEADLINE542" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h1 class="widget-content" lp-node="h1"><span
                        style="font-weight: bold;">THÔNG SỐ KỸ THUẬT</span></h1></div>
        <div id="PARAGRAPH543" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
             lp-lang="PARAGRAPH" lp-display="block"><h6 class="widget-content" lp-node="h6">Máy rửa xe Lutian LT303B
                là máy chuyên dụng với các tiện ích sử dụng trong công việc gia đình.</h6></div>
        <div id="IMAGE544" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="image" lp-lang="IMAGE"
             lp-display="block">
            <div class="widget-content">
                <div class="lp-show-image" alt="59b6e70c9dc0c6364b8a43ac/123-1538930680.png"></div>
            </div>
            <div class="ladi-widget-overlay"></div>
        </div>
        <div id="LISTOP561" class="widget-element widget-snap widget-dragg" lp-type="listop" lp-editor="true"
             lp-lang="LISTOP" lp-display="block">
            <ol class="widget-content">
                <li color="" style="color: rgb(61, 61, 61);">Xuất xứ: Đài Loan.</li>
                <li color="" style="color: rgb(61, 61, 61);">Trọng Lượng: ~8kg.</li>
                <li color="" style="color: rgb(61, 61, 61);">Điện áp: 220V.</li>
                <li color="" style="color: rgb(61, 61, 61);">Công suất: 1400W.</li>
                <li color="" style="color: rgb(61, 61, 61);">Lưu lượng nước: 6,5L/min.</li>
                <li color="" style="color: rgb(61, 61, 61);">Áp lực nước: 120 Bar.</li>
                <li color="" style="color: rgb(61, 61, 61);">Dây dẫn nước đầu vào: 3m.</li>
                <li color="" style="color: rgb(61, 61, 61);">Dây dẫn nước đầu ra: 10m.</li>
                <li color="" style="color: rgb(61, 61, 61);">Kích thước: 30 * 280 * 480 mm</li>
            </ol>
        </div>
    </div>
    <div class="ladi-widget-overlay"></div>
</div>