<div id="SECTION583" class="widget-section ladi-drop lazy-hidden ui-droppable screenS" lp-type="widget_section"
     lp-widget="widget" lp-lang="SECTION" lp-display="block">
    <div class="container">
        <div id="HEADLINE584" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h2 class="widget-content" lp-node="h2">TẠI SAO NÊN CHỌN - Máy
                Rửa Xe Lutian ?????</h2></div>
        <div id="HEADLINE585" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h6 class="widget-content" lp-node="h6">Lutian là thương hiệu uy
                tín trên thế giới,&nbsp;Căn cứ vào chất lượng, tồn tại trên sự tin tưởng, phục vụ với sự chân thành,
                phát triển về sự đổi mới.</h6></div>
        <div id="GROUP586" class="widget-element widget-snap widget-group" lp-type="widget_group" lp-lang="GROUP"
             lp-display="block">
            <div class="widget-content">
                <div id="LINE587" class="widget-element widget-snap" lp-type="line" lp-lang="LINE"
                     lp-group="GROUP586" lp-display="block">
                    <div class="widget-content">
                        <div class="line"></div>
                    </div>
                </div>
                <div id="PARAGRAPH588" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
                     lp-lang="PARAGRAPH" lp-group="GROUP586" lp-display="block"><p class="widget-content"
                                                                                   lp-node="p">Lutian Được thành lập
                        vào tháng 8 năm 2002, Lutian Machinery Co., Ltd. có diện tích 120.000 ㎡, là thương hiệu sản
                        xuất máy phát điện, máy bơm nước và máy rửa áp lực cao trong hơn 16 năm. Mạng lưới bán hàng
                        của Lutian đã đạt hơn 92 quốc gia và khu vực trên toàn thế giới.</p></div>
                <div id="HEADLINE589" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
                     lp-lang="HEADLINE" lp-group="GROUP586" lp-display="block"><h6 class="widget-content"
                                                                                   lp-node="h6">GIỚI THIỆU
                        LUTIAN</h6></div>
                <div id="SHAPE590" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE"
                     lp-group="GROUP586" lp-display="block">
                    <div class="widget-content">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(204,147,0,1)">
                            <path d="M15,19L9,16.89V5L15,7.11M20.5,3C20.44,3 20.39,3 20.34,3L15,5.1L9,3L3.36,4.9C3.15,4.97 3,5.15 3,5.38V20.5A0.5,0.5 0 0,0 3.5,21C3.55,21 3.61,21 3.66,20.97L9,18.9L15,21L20.64,19.1C20.85,19 21,18.85 21,18.62V3.5A0.5,0.5 0 0,0 20.5,3Z"></path>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div id="GROUP591" class="widget-element widget-snap widget-group" lp-type="widget_group" lp-lang="GROUP"
             lp-display="block">
            <div class="widget-content">
                <div id="LINE592" class="widget-element widget-snap" lp-type="line" lp-lang="LINE"
                     lp-group="GROUP591" lp-display="block">
                    <div class="widget-content">
                        <div class="line"></div>
                    </div>
                </div>
                <div id="PARAGRAPH593" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
                     lp-lang="PARAGRAPH" lp-group="GROUP591" lp-display="block"><p class="widget-content"
                                                                                   lp-node="p">Tầm nhìn của Lutian:
                        Dẫn đầu sản xuất xanh dựa trên công nghệ, khai thác công nghiệp cơ điện trong chiều sâu và
                        tạo ra các sản phẩm chất lượng đẳng cấp thế giới để đáp ứng nhu cầu thị trường.<br><br>Sứ
                        mệnh:&nbsp;Phát triển với đội ngũ nhân viên, để đạt được lợi nhuận với khách hàng, để phát
                        triển với sự đổi mới và tạo ra sự giàu có hơn cho xã hội.</p></div>
                <div id="HEADLINE594" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
                     lp-lang="HEADLINE" lp-group="GROUP591" lp-display="block"><h6 class="widget-content"
                                                                                   lp-node="h6">TẦM NHÌN SỨ
                        MỆNH</h6></div>
                <div id="SHAPE595" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE"
                     lp-group="GROUP591" lp-display="block">
                    <div class="widget-content">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(204,147,0,1)">
                            <path d="M22,22H10V20H22V22M2,22V20H9V22H2M18,18V10H22V18H18M18,3H22V9H18V3M2,18V3H16V18H2M9,14.56A3,3 0 0,0 12,11.56C12,9.56 9,6.19 9,6.19C9,6.19 6,9.56 6,11.56A3,3 0 0,0 9,14.56Z"></path>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div id="IMAGE596" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="image" lp-lang="IMAGE"
             lp-display="block">
            <div class="widget-content">
                <div class="lp-show-image" alt="59b6e70c9dc0c6364b8a43ac/ggdrwes-1539030565.png"></div>
            </div>
            <div class="ladi-widget-overlay"></div>
        </div>
    </div>
    <div class="ladi-widget-overlay"></div>
</div>
<style>
    #IMAGE544 .widget-content:first-child .lp-show-image:first-child {
        background-image: url("assets/mayruaxe/images/s750x700/59b6e70c9dc0c6364b8a43ac/123-1538930680.png");
    }

</style>