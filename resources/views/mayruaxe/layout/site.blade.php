<!DOCTYPE html >
<html mlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#" class="no-js">
<head>
	<title>@yield('title')</title>
	<!-- meta -->
	<meta name="ROBOTS" content="index, follow" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="keywords" content="@yield('keywords')" />
	<!-- facebook gooogle -->
	<!-- <meta property="fb:app_id" content="" />
	<meta property="fb:admins" content=""> -->

    <meta property="og:url"                content="@yield('meta_url')" />
    <meta property="og:type"               content="Website" />
    <meta property="og:title"              content="@yield('title')" />
    <meta property="og:description"        content="@yield('meta_description')" />
    <meta property="og:image"              content="@yield('meta_image')" />

    <!-- Favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ isset($information['fa-vicon']) ? $information['fa-vicon'] : ''}}">
    <!-- FONTS ============================================ -->

    <script>function ladiViewport() {
            var width = (window.outerWidth > 0) ? window.outerWidth : screen.width;
            var content = "user-scalable=no";
            if (width < 768) {
                content += ", width=375";
            } else {
                if (width < 960) {
                    content += ", width=960";
                } else {
                    content += ", width=device-width";
                }
            }
            var meta = document.querySelector('meta[name="viewport"]');
            if (meta == undefined) {
                meta = document.createElement('meta');
                meta.name = 'viewport';
                document.head.prepend(meta);
            }
            meta.content = content;
        };ladiViewport();</script>

    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
    <link rel="preconnect" href="" crossorigin>

    <link rel="stylesheet" href="{{ asset('assets/mayruaxe/css/style.css') }}">
    <script>document.addEventListener("DOMContentLoaded", function () {
            var a = [].slice.call(document.querySelectorAll(".lazy-hidden")),
                b = [].slice.call(document.querySelectorAll("iframe.widget-content")), c = !1, d = 200;
            const e = function () {
                c === !1 && (c = !0, setTimeout(function () {
                    a.forEach(function (a) {
                        var b = a.getBoundingClientRect();
                        b.top <= window.innerHeight && b.bottom > 0 && a.classList.remove("lazy-hidden")
                    }), b.forEach(function (a) {
                        var b = a.getBoundingClientRect(), c = a.getAttribute("data-src");
                        b.top <= window.innerHeight && b.bottom > 0 && Boolean(c) && (a.setAttribute("src", c), a.removeAttribute("data-src"))
                    }), 0 === a.length && 0 === b.length && (document.removeEventListener("scroll", e), window.removeEventListener("resize", e), window.removeEventListener("orientationchange", e)), c = !1
                }, d))
            };
            e(), d = 200, document.addEventListener("scroll", e), window.addEventListener("resize", e), window.addEventListener("orientationchange", e)
        });</script>

    <link rel="stylesheet" media="none" onload="if(media!='all')media='all'" href="{{ asset('assets/mayruaxe/css/animate.min.css') }}?v=160318">
    <script type="text/javascript" async src="{{ asset('assets/mayruaxe/js/ladipage.lib.3.js') }}?v=0118102018"></script>

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '133694767573039');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=133694767573039&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->


</head>

<body>
    <div class="ladi-wraper-page" >
        @include('mayruaxe.common.header')

        @yield('content')

        @include('mayruaxe.common.footer')
    </div>

    {{--@include('zito.common.login')--}}
    <script>
        function subcribeEmailSubmit(e) {
            var email = $(e).find('.emailSubmit').val();
            var token =  $(e).find('input[name=_token]').val();

            $.ajax({
                type: "POST",
                url: '{!! route('subcribe_email') !!}',
                data: {
                    email: email,
                    _token: token
                },
                success: function(data){
                    var obj = jQuery.parseJSON(data);

                    alert(obj.message);
                }
            });
            return false;
        }

        function contact(e) {
            var data = jQuery(e).serialize();
            jQuery(e).find('button[type=submit]').attr('disabled', 'disabled')
            jQuery(e).find('button[type=submit]').attr('style', 'background: gray;')
            jQuery.ajax({
                type: "POST",
                url: '{!! route('sub_contact') !!}',
                data: data,
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    // gửi thành công
                    if (obj.status == 200) {
                        alert(obj.message);
                        jQuery(e).find('button[type=submit]').removeAttr('disabled', 'disabled')
                        jQuery(e).find('button[type=submit]').removeAttr('style')
                        return false;
                    }

                    // gửi thất bại
                    if (obj.status == 500) {
                        alert(obj.message);
                        jQuery(e).find('button[type=submit]').removeAttr('disabled', 'disabled')
                        jQuery(e).find('button[type=submit]').removeAttr('style')

                        return false;
                    }
                },
                error: function(error) {
                    //alert('Lỗi gì đó đã xảy ra!')
                    jQuery(e).find('button[type=submit]').removeAttr('disabled', 'disabled')
                    jQuery(e).find('button[type=submit]').removeAttr('style')

                    return false;
                }

            });


            return false;
        }

    </script>



<style>.fb-livechat, .fb-widget{display: none}.ctrlq.fb-button, .ctrlq.fb-close{position: fixed; right: 24px; cursor: pointer}.ctrlq.fb-button{z-index: 999; background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff; width: 60px; height: 60px; text-align: center; bottom: 30px; border: 0; outline: 0; border-radius: 60px; -webkit-border-radius: 60px; -moz-border-radius: 60px; -ms-border-radius: 60px; -o-border-radius: 60px; box-shadow: 0 1px 6px rgba(0, 0, 0, .06), 0 2px 32px rgba(0, 0, 0, .16); -webkit-transition: box-shadow .2s ease; background-size: 80%; transition: all .2s ease-in-out}.ctrlq.fb-button:focus, .ctrlq.fb-button:hover{transform: scale(1.1); box-shadow: 0 2px 8px rgba(0, 0, 0, .09), 0 4px 40px rgba(0, 0, 0, .24)}.fb-widget{background: #fff; z-index: 1000; position: fixed; width: 360px; height: 435px; overflow: hidden; opacity: 0; bottom: 0; right: 24px; border-radius: 6px; -o-border-radius: 6px; -webkit-border-radius: 6px; box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -webkit-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -moz-box-shadow: 0 5px 40px rgba(0, 0, 0, .16); -o-box-shadow: 0 5px 40px rgba(0, 0, 0, .16)}.fb-credit{text-align: center; margin-top: 8px}.fb-credit a{transition: none; color: #bec2c9; font-family: Helvetica, Arial, sans-serif; font-size: 12px; text-decoration: none; border: 0; font-weight: 400}.ctrlq.fb-overlay{z-index: 0; position: fixed; height: 100vh; width: 100vw; -webkit-transition: opacity .4s, visibility .4s; transition: opacity .4s, visibility .4s; top: 0; left: 0; background: rgba(0, 0, 0, .05); display: none}.ctrlq.fb-close{z-index: 4; padding: 0 6px; background: #365899; font-weight: 700; font-size: 11px; color: #fff; margin: 8px; border-radius: 3px}.ctrlq.fb-close::after{content: "X"; font-family: sans-serif}.bubble{width: 20px; height: 20px; background: #c00; color: #fff; position: absolute; z-index: 999999999; text-align: center; vertical-align: middle; top: -2px; left: -5px; border-radius: 50%;}.bubble-msg{width: 120px; left: -125px; top: -8px; position: relative; background: rgba(59, 89, 152, .8); color: #fff; padding: 5px 8px; border-radius: 8px; text-align: center; font-size: 13px;}</style><div class="fb-livechat"> <div class="ctrlq fb-overlay"></div><div class="fb-widget"> <div class="ctrlq fb-close"></div><div class="fb-page" data-href="https://www.facebook.com/MayruaxegiadinhapluccaoLutian/" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false"> </div><div class="fb-credit"> <a href="https://vn3c.com" target="_blank">Powered by VN3C</a> </div><div id="fb-root"></div></div><a href="https://m.me/MayruaxegiadinhapluccaoLutian" title="Gửi tin nhắn cho chúng tôi qua Facebook" class="ctrlq fb-button"> <div class="bubble">1</div><div class="bubble-msg">Yêu cầu hỗ trợ!</div></a></div><script src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><script>$(document).ready(function(){function detectmob(){if( navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ){return true;}else{return false;}}var t={delay: 125, overlay: $(".fb-overlay"), widget: $(".fb-widget"), button: $(".fb-button")}; setTimeout(function(){$("div.fb-livechat").fadeIn()}, 8 * t.delay); if(!detectmob()){$(".ctrlq").on("click", function(e){e.preventDefault(), t.overlay.is(":visible") ? (t.overlay.fadeOut(t.delay), t.widget.stop().animate({bottom: 0, opacity: 0}, 2 * t.delay, function(){$(this).hide("slow"), t.button.show()})) : t.button.fadeOut("medium", function(){t.widget.stop().show().animate({bottom: "30px", opacity: 1}, 2 * t.delay), t.overlay.fadeIn(t.delay)})})}});</script>

    Xin chào, Anh (Chị) cần hỗ trợ gì không ạ?
<div class="showPhone clearfix">
   <a href="tel:0941408866" class="fancybox">
	<div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show" id="coccoc-alo-phoneIcon" style="">
			<div class="coccoc-alo-ph-circle"></div>
			<div class="coccoc-alo-ph-circle-fill"></div> <div class="coccoc-alo-ph-img-circle"></div>
		</div>
	</a>
</div>
<style>
	.showPhone {
    position: fixed;
    left: 0;
    z-index: 9999;
    bottom: 0%;
    /*padding: 11px 43px;*/
    padding-left: 35px;
    font-size: 18px;
    color: white;
    display: block;
    background: #ef7c45;
    border-radius: 33px;
}
.showPhone:hover
{
    background: #b51e13;
    -webkit-transition: all 1.1s;
    text-decoration: none;
}
.showPhone a
{
    position: relative;
    color: white;
}
.showPhone a .coccoc-alo-phone
{
    position: absolute;
    bottom: 9px;
}
}
.coccoc-alo-phone.coccoc-alo-show {
    visibility: visible;
}
.coccoc-alo-phone {
    position: fixed;
    background-color: transparent;
    width: 200px;
    height: 200px;
    cursor: pointer;
    z-index: 200000!important;
    -webkit-backface-visibility: hidden;
    -webkit-transform: translateZ(0);
    -webkit-transition: visibility .5s;
    -moz-transition: visibility .5s;
    -o-transition: visibility .5s;
    transition: visibility .5s;
}
.coccoc-alo-phone.coccoc-alo-green .coccoc-alo-ph-circle {
    border-color: #00aff2;
    border-color: #bfebfc 9;
    opacity: .5;
}
.coccoc-alo-ph-circle {
    width: 160px;
    height: 160px;
    bottom: -30px;
    left: -26px;
    position: absolute;
    background-color: transparent;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    border: 2px solid rgba(30,30,30,0.4);
    border: 2px solid #bfebfc 9;
    opacity: .1;
    -webkit-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -moz-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -ms-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -o-animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    animation: coccoc-alo-circle-anim 1.2s infinite ease-in-out;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
    -webkit-transform-origin: 50% 50%;
    -moz-transform-origin: 50% 50%;
    -ms-transform-origin: 50% 50%;
    -o-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
.coccoc-alo-phone.coccoc-alo-green .coccoc-alo-ph-circle-fill {
    background-color: rgba(0,165,233,0.9);
    background-color: #a6e3fa 9;
    opacity: .75!important;
}
.coccoc-alo-ph-circle-fill {
    width: 100px;
    height: 100px;
    left: 9px;
    bottom: -12px;
    position: absolute;
    background-color: #000;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    border: 2px solid transparent;
    opacity: .1;
    -webkit-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -moz-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -ms-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -o-animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    animation: coccoc-alo-circle-fill-anim 2.3s infinite ease-in-out;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
    -webkit-transform-origin: 50% 50%;
    -moz-transform-origin: 50% 50%;
    -ms-transform-origin: 50% 50%;
    -o-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
.coccoc-alo-phone.coccoc-alo-green .coccoc-alo-ph-img-circle {
    background-color: #20ad5d;
}
.coccoc-alo-ph-img-circle {
    width: 50px;
    height: 50px;
    bottom: 12px;
    left: 20px;
    position: absolute;
    background:rgba(30,30,30,0.1) url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==") no-repeat center center;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    border: 2px solid transparent;
    opacity: .7;
    -webkit-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -moz-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -ms-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -o-animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    animation: coccoc-alo-circle-img-anim 1s infinite ease-in-out;
    -webkit-transform-origin: 50% 50%;
    -moz-transform-origin: 50% 50%;
    -ms-transform-origin: 50% 50%;
    -o-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
.coccoc-alo-phone.coccoc-alo-green.coccoc-alo-hover .coccoc-alo-ph-img-circle, .coccoc-alo-phone.coccoc-alo-green:hover .coccoc-alo-ph-img-circle {
    background-color: #75eb50;
}
@-moz-keyframes coccoc-alo-circle-anim {
    0% {
        -moz-transform:rotate(0) scale(.5) skew(1deg);
        opacity:.1;
        -moz-opacity:.1;
        -webkit-opacity:.1;
        -o-opacity:.1;
    }
    30% {
        -moz-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.5;
        -moz-opacity:.5;
        -webkit-opacity:.5;
        -o-opacity:.5;
    }
    100% {
        -moz-transform:rotate(0) scale(1) skew(1deg);
        opacity:.6;
        -moz-opacity:.6;
        -webkit-opacity:.6;
        -o-opacity:.1;
    }
}

@-webkit-keyframes coccoc-alo-circle-anim {
    0% {
        -webkit-transform:rotate(0) scale(.5) skew(1deg);
        -webkit-opacity:.1;
    }
    30% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
        -webkit-opacity:.5;
    }
    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
        -webkit-opacity:.1;
    }
}

@-o-keyframes coccoc-alo-circle-anim {
    0% {
        -o-transform:rotate(0) kscale(.5) skew(1deg);
        -o-opacity:.1;
    }
    30% {
        -o-transform:rotate(0) scale(.7) skew(1deg);
        -o-opacity:.5;
    }
    100% {
        -o-transform:rotate(0) scale(1) skew(1deg);
        -o-opacity:.1;
    }
}

@-moz-keyframes coccoc-alo-circle-fill-anim {
    0% {
        -moz-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2;
    }
    50% {
        -moz-transform:rotate(0) -moz-scale(1) skew(1deg);
        opacity:.2;
    }
    100% {
        -moz-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2;
    }
}

@-webkit-keyframes coccoc-alo-circle-fill-anim {
    0% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2;
    }
    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
        opacity:.2;
    }
    100% {
        -webkit-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2;
    }
}

@-o-keyframes coccoc-alo-circle-fill-anim {
    0% {
        -o-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2;
    }
    50% {
        -o-transform:rotate(0) scale(1) skew(1deg);
        opacity:.2;
    }
    100% {
        -o-transform:rotate(0) scale(.7) skew(1deg);
        opacity:.2;
    }
}

@-moz-keyframes coccoc-alo-circle-img-anim {
    0% {
        transform:rotate(0) scale(1) skew(1deg);
    }
    10% {
        -moz-transform:rotate(-25deg) scale(1) skew(1deg);
    }
    20% {
        -moz-transform:rotate(25deg) scale(1) skew(1deg);
    }
    30% {
        -moz-transform:rotate(-25deg) scale(1) skew(1deg);
    }
    40% {
        -moz-transform:rotate(25deg) scale(1) skew(1deg);
    }
    50% {
        -moz-transform:rotate(0) scale(1) skew(1deg);
    }
    100% {
        -moz-transform:rotate(0) scale(1) skew(1deg);
    }
}

@-webkit-keyframes coccoc-alo-circle-img-anim {
    0% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
    }
    10% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
    }
    20% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg);
    }
    30% {
        -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
    }
    40% {
        -webkit-transform:rotate(25deg) scale(1) skew(1deg);
    }
    50% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
    }
    100% {
        -webkit-transform:rotate(0) scale(1) skew(1deg);
    }
}

@-o-keyframes coccoc-alo-circle-img-anim {
    0% {
        -o-transform:rotate(0) scale(1) skew(1deg);
    }
    10% {
        -o-transform:rotate(-25deg) scale(1) skew(1deg);
    }
    20% {
        -o-transform:rotate(25deg) scale(1) skew(1deg);
    }
    30% {
        -o-transform:rotate(-25deg) scale(1) skew(1deg);
    }
    40% {
        -o-transform:rotate(25deg) scale(1) skew(1deg);
    }
    50% {
        -o-transform:rotate(0) scale(1) skew(1deg);
    }
    100% {
        -o-transform:rotate(0) scale(1) skew(1deg);
    }
}
</style>
</body>
</html>
