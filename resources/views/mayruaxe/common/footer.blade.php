<div id="SECTION722" class="widget-section ladi-drop lazy-hidden ui-droppable" lp-type="widget_section"
     lp-widget="widget" lp-lang="SECTION" lp-display="block">
    <div class="container">
        <div id="HTML720" class="widget-element widget-snap" lp-type="customhtml" lp-lang="HTML" lp-display="block">
            <div class="widget-content">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.7412803575003!2d105.80295711543052!3d21.04303558599073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab3d85da4f23%3A0xb024d6423e6b6cd4!2zMTAyIE5ndXnhu4VuIMSQw6xuaCBIb8OgbiwgTmdoxKlhIMSQw7QsIEPhuqd1IEdp4bqleSwgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1545709885731" width="450" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
        <div id="GROUP389" class="widget-element widget-snap widget-group" lp-type="widget_group" lp-lang="GROUP"
             lp-display="block">
            <div class="widget-content">
                <div id="SHAPE390" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE"
                     lp-group="GROUP389" lp-display="block">
                    <div class="widget-content">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(0,0,0,1)">
                            <path d="M6.62,10.79C8.06,13.62 10.38,15.94 13.21,17.38L15.41,15.18C15.69,14.9 16.08,14.82 16.43,14.93C17.55,15.3 18.75,15.5 20,15.5A1,1 0 0,1 21,16.5V20A1,1 0 0,1 20,21A17,17 0 0,1 3,4A1,1 0 0,1 4,3H7.5A1,1 0 0,1 8.5,4C8.5,5.25 8.7,6.45 9.07,7.57C9.18,7.92 9.1,8.31 8.82,8.59L6.62,10.79Z"></path>
                        </svg>
                    </div>
                </div>
                <div id="PARAGRAPH391" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
                     lp-lang="PARAGRAPH" lp-group="GROUP389" lp-display="block"><p class="widget-content"
                                                                                   lp-node="p"><a href="tel:0941.408.866">Hotline:&nbsp;(+84) 941.408.866</a></p></div>
            </div>
        </div>
        <div id="GROUP395" class="widget-element widget-snap widget-group" lp-type="widget_group" lp-lang="GROUP"
             lp-display="block">
            <div class="widget-content">
                <div id="SHAPE396" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE"
                     lp-group="GROUP395" lp-display="block">
                    <div class="widget-content">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             width="100%" height="100%" viewBox="0 0 24 24" fill="rgba(0,0,0,1)">
                            <path d="M12,11.5A2.5,2.5 0 0,1 9.5,9A2.5,2.5 0 0,1 12,6.5A2.5,2.5 0 0,1 14.5,9A2.5,2.5 0 0,1 12,11.5M12,2A7,7 0 0,0 5,9C5,14.25 12,22 12,22C12,22 19,14.25 19,9A7,7 0 0,0 12,2Z"></path>
                        </svg>
                    </div>
                </div>
                <div id="PARAGRAPH397" class="widget-element widget-snap" lp-type="textparagraph" lp-editor="true"
                     lp-lang="PARAGRAPH" lp-group="GROUP395" lp-display="block">
                    <p class="widget-content" lp-node="p">
                        Địa chỉ: 102 Nguyễn Đình Hoàn, Nghĩa Đô, cầu giấy, Hà Nội
                    </p></div>
            </div>
        </div>
        <div id="HEADLINE387" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h2 class="widget-content" lp-node="h2">Liên hệ chúng tôi</h2>
        </div>
    </div>
    <div class="ladi-widget-overlay"></div>
</div>