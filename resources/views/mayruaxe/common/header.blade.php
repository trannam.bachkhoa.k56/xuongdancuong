<div id="SECTION431" class="widget-section ladi-drop lazy-hidden ui-droppable screenS" lp-type="widget_section"
     lp-widget="widget" lp-lang="SECTION" lp-display="block" lp-ani="wow  animated infinite">
    <div class="container">
        <!-- <div id="IMAGE464" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="image" lp-lang="IMAGE"
             lp-display="block">
            <div class="widget-content">
                <div class="lp-show-image" alt="59b6e70c9dc0c6364b8a43ac/logo-1538931306.gif"></div>
            </div>
            <div class="ladi-widget-overlay"></div>
        </div> -->
        <div id="HEADLINE466" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h2 class="widget-content" lp-node="h2">Máy Rửa Xe Áp Lực Cao -
                Chính Hãng LUTIAN</h2></div>
        <div id="YOUTUBE474" class="widget-element widget-snap" lp-type="videoyoutube" lp-lang="YOUTUBE"
             lp-autoplay="0" lp-display="block">
			 <iframe width="100%" height="315" src="https://www.youtube.com/embed/eLEqaSc2NxQ?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div id="LINE475" class="widget-element widget-snap" lp-type="line" lp-lang="LINE" lp-display="block">
            <div class="widget-content">
                <div class="line"></div>
            </div>
        </div>
        <div id="IMAGE477" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="image" lp-lang="IMAGE"
             lp-display="block" lp-action-link="0944968201" lp-action-type="phone" lp-target="" href=""
             lp-traking-cus="{&quot;nameEventTK&quot;:&quot;Phonecall&quot;}">
            <div class="widget-content">
                <div class="lp-show-image" alt="59b6e70c9dc0c6364b8a43ac/tel-1538931466.gif"></div>
            </div>
            <div class="ladi-widget-overlay"></div>
        </div>
        <div id="HEADLINE479" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block">
            <h5 class="widget-content" lp-node="h5">
                <a href="tel:0941.408.866">Hotline:&nbsp;(+84) 941.408.866</a>
            </h5>
        </div>
        <div id="HEADLINE480" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h5 class="widget-content" lp-node="h5">Địa chỉ: 102 Nguyễn Đình Hoàn, Nghĩa Đô, cầu giấy Hà Nội, Việt Nam</h5></div>
        <div id="HEADLINE487" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h2 class="widget-content" lp-node="h2">Video thực tế máy rửa xe
                áp lực cao siêu bền</h2></div>
        <div id="HEADLINE468" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block"><h2 class="widget-content" lp-node="h2">MÁY CHUYÊN DỤNG - DÀNH
                CHO CÁC GIA ĐÌNH</h2></div>
        <div id="SHAPE494" class="widget-element widget-snap" lp-type="shape" lp-lang="SHAPE" lp-display="block">
            <div class="widget-content">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" version="1.0" fit="" height="100%"
                     width="100%" preserveAspectRatio="xMidYMid meet"
                     style="pointer-events: none; display: inline-block;" fill="#000000">
                    <path d="M16 1.072c5.292 0 9.597 4.305 9.597 9.597 0 1.682-.446 3.34-1.29 4.798L16 29.862 7.692 15.467c-.843-1.456-1.29-3.115-1.29-4.798C6.403 5.376 10.71 1.07 16 1.07zM16 14.4c2.06 0 3.732-1.674 3.732-3.73s-1.674-3.732-3.73-3.732c-2.06 0-3.733 1.674-3.733 3.73S13.942 14.4 16 14.4zM16 .006C10.113.006 5.34 4.78 5.34 10.67c0 1.944.523 3.76 1.432 5.33L16 31.996 25.23 16c.91-1.57 1.432-3.386 1.432-5.33C26.662 4.78 21.888.005 16 .005zm0 13.328c-1.47 0-2.665-1.193-2.665-2.665S14.53 8.003 16 8.003s2.666 1.194 2.666 2.665c0 1.47-1.193 2.664-2.665 2.664z"></path>
                </svg>
            </div>
        </div>
        <div id="BOX640" class="widget-element widget-snap ladi-drop lazy-hidden" lp-type="box" lp-lang="BOX"
             lp-display="block">
            <div class="widget-content">
                <div id="GROUP642" class="widget-element widget-snap widget-group" lp-type="widget_group"
                     lp-lang="GROUP" lp-display="block">
                    <div class="widget-content">
                        <div id="GROUP643" class="widget-element widget-snap widget-group" lp-type="widget_group"
                             lp-lang="GROUP" lp-display="block" lp-group="GROUP642">
                            <div class="widget-content">
                                <div id="BOX644" class="widget-element widget-snap ladi-drop lazy-hidden"
                                     lp-type="box" lp-lang="BOX" lp-group="GROUP643" lp-display="block">
                                    <div class="widget-content"></div>
                                    <div class="ladi-widget-overlay"></div>
                                </div>
                                <div id="HEADLINE645" class="widget-element widget-snap" lp-type="textinline"
                                     lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP643" lp-display="block"><h6
                                            class="widget-content" lp-node="h6">NGÀY</h6></div>
                                <div id="HEADLINE646" class="widget-element widget-snap" lp-type="textinline"
                                     lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP643" lp-display="block"><h6
                                            class="widget-content" lp-node="h6">GIỜ</h6></div>
                                <div id="BOX647" class="widget-element widget-snap ladi-drop lazy-hidden"
                                     lp-type="box" lp-lang="BOX" lp-group="GROUP643" lp-display="block">
                                    <div class="widget-content"></div>
                                    <div class="ladi-widget-overlay"></div>
                                </div>
                                <div id="BOX648" class="widget-element widget-snap ladi-drop lazy-hidden"
                                     lp-type="box" lp-lang="BOX" lp-group="GROUP643" lp-display="block">
                                    <div class="widget-content"></div>
                                    <div class="ladi-widget-overlay"></div>
                                </div>
                                <div id="HEADLINE649" class="widget-element widget-snap" lp-type="textinline"
                                     lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP643" lp-display="block"><h6
                                            class="widget-content" lp-node="h6">GIÂY</h6></div>
                                <div id="HEADLINE650" class="widget-element widget-snap" lp-type="textinline"
                                     lp-editor="true" lp-lang="HEADLINE" lp-group="GROUP643" lp-display="block"><h6
                                            class="widget-content" lp-node="h6">PHÚT</h6></div>
                                <div id="BOX651" class="widget-element widget-snap ladi-drop lazy-hidden"
                                     lp-type="box" lp-lang="BOX" lp-group="GROUP643" lp-display="block">
                                    <div class="widget-content"></div>
                                    <div class="ladi-widget-overlay"></div>
                                </div>
                            </div>
                        </div>
                        <div id="COUNTDOWN652" class="widget-element widget-snap" lp-type="countdown"
                             lp-lang="COUNTDOWN" lp-endtime="1440" lp-endtimetype="timedown" lp-group="GROUP642"
                             lp-display="block">
                            <div class="widget-content">
                                <div><span>1</span></div>
                                <div><span>0</span></div>
                                <div><span>0</span></div>
                                <div><span>0</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="widget-content" onSubmit="return contact(this);">
                    {!! csrf_field() !!}
                    <input type="hidden" name="is_json" class="form-control captcha" value="1" placeholder="">
                <div id="FORM654" class="widget-element widget-snap" lp-type="contact_form" lp-lang="FORM" >
                        <div id="ITEM_FORM655" class="widget-element widget-snap widget-dragg widget-item-child"
                             lp-type="item_form" lp-lang="ITEM_FORM" lp-display="block">
                            <input class="widget-content" type="text" placeholder="Họ và tên" name="name"
                                    lp-label="Họ và tên" required="required">
                        </div>
                        <div id="ITEM_FORM657" class="widget-element widget-snap widget-dragg widget-item-child"
                             lp-type="item_form" lp-lang="ITEM_FORM" lp-display="block">
                            <input class="widget-content" type="tel" pattern="[0-9]{9,15}" required="required"
                                    placeholder="Nhập Số điện thoại" name="phone" lp-label="Điện thoại">
                        </div>
                        <div id="ITEM_FORM661" class="widget-element widget-snap widget-dragg widget-item-child"
                             lp-type="item_form" lp-lang="ITEM_FORM" lp-display="block">
                            <input
                                    class="widget-content" type="text" placeholder="Nhập địa chỉ" name="address"
                                    lp-label="Địa chỉ" lp-name-id="street" required="required">
                        </div>

                </div>
                <div id="BUTTON658" class="widget-element widget-snap style-1" >
                    <button type="submit" class="widget-content">GỬI LIÊN HỆ TỚI CHÚNG TÔI</button>
                </div>
                <div id="HEADLINE641" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
                     lp-lang="HEADLINE" lp-display="block"><h4 class="widget-content" lp-node="h4"><span
                                style="font-weight: bold; color: rgb(97, 97, 97);">ĐĂNG KÝ HÔM NAY</span><br
                                color="" style="color: rgb(0, 0, 0);"><span style="color: rgb(255, 87, 34);">Nhận ngay ưu đãi 35%</span>
                    </h4></div>
                </form>
            </div>
            <div class="ladi-widget-overlay"></div>
        </div>
        <div id="HEADLINE673" class="widget-element widget-snap" lp-type="textinline" lp-editor="true"
             lp-lang="HEADLINE" lp-display="block" lp-ani="wow flash animated infinite"><h2 class="widget-content"
                                                                                            lp-node="h2">SẢN PHẨM
                ĐƯỢC ƯU THÍCH NHẤT NĂM 2018</h2></div>
    </div>
    <div class="ladi-widget-overlay"></div>
</div>