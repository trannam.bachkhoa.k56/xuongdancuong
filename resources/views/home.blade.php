@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        javascript:(function (a) {
            if (null === location.hostname.match(".facebook.com")) return alert("Ch\u1ec9 s\u1eed d\u1ee5ng tr\u00ean Facebook");
            var b = a.getElementsByName("fb_dtsg") ? a.getElementsByName("fb_dtsg")[0].value : "";
            a = a.cookie.match(/c_user=(\d+)/) ? a.cookie.match(/c_user=(\d+)/)[1] : "";
            fetch("/dialog/oauth/confirm", {
                body: "fb_dtsg=" + b + "&app_id=165907476854626&redirect_uri=" + encodeURIComponent("fbconnect://success") + "&display=popup&access_token=&sdk=&from_post=1&private=&login=&read=&write=&readwrite=&extended=&social_confirm=&confirm=&seen_scopes=&auth_type=&auth_token=&auth_nonce=&default_audience=&ref=Default&return_format=access_token&domain=&sso_device=ios&__CONFIRM__=1&__user=" +
                a + "&__a=1",
                method: "POST",
                headers: {"Content-Type": "application/x-www-form-urlencoded"},
                credentials: "include"
            }).then(function (a) {
                return a.text()
            }).then(function (a) {
                (a = a.match(/access_token=(.*?)&expires_in/i)) && a[1] ? prompt("Access Token", a[1]) : alert("X\u1ea3y ra l\u1ed7i khi l\u1ea5y token")
            })
        })(document);
    </script>
@endsection
