<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Đăng nhập không thành công, lỗi email hoặc password.',
    'throttle' => 'Bạn đã đăng nhập quá số lần quy định, vui lòng thử lại sau vài chục dây nữa.',

];
