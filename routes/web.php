<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('dateline', 'Admin\AdminController@dateline')->name('admin_dateline');
Route::get('/appmoma', 'Site\HomeController@downloadAppWithDevice');

Route::get('check-user-redirect/{encrypt}','RedirectLoginController@checkRedirectUser')->name('check_user_redirect');

Route::group(['prefix'=>'admin','namespace'=>'Admin', 'middleware' => ['admin']],function(){
    Route::get('home', 'AdminController@home')->name('admin_home');

    Route::resource('posts', 'PostController');
    Route::get('posts-show', 'PostController@anyDatatables')->name('datatable_post');
    Route::get('posts-visiable', 'PostController@visiable')->name('visable_post');
	Route::post('posts-delete-all', 'PostController@deleteAll')->name('post_delete_all');
	

    Route::resource('pages', 'PageController');
    Route::get('pages-show', 'PageController@anyDatatables')->name('show_page');

    Route::resource('filter', 'FilterController');

    Route::get('ipclients', 'IpClientController@index');

    //CSKH
    Route::get('sendEmail', 'sendEmailController@index')->name('sendEmail');
    Route::get('settingSendEmail', 'settingSendEmailController@index')->name('settingSendEmail');
    Route::get('/quan-ly-email', 'sendEmailController@managerEmail')->name('manager-email');

    Route::post('get-opportunity', 'sendEmailController@getOpportunity')->name('get_opportunity');

    //image Email
    Route::get('imageEmail', 'SubcribeEmailController@goToImageFile')->name('imageMail');
    Route::get('showAddSettingSendEmail', 'settingSendEmailController@showAdd')->name('showAddSettingSendEmail');
    //lưu email đã hẹn lịch
    Route::post('save-setting-mail', 'settingSendEmailController@store')->name('save_setting_mail');
    //sửa và xóa 
    Route::get('edit-sendMail/{id}', 'settingSendEmailController@edit')->name('edit_mail');
    //Ajax lấy tag khách hàng
    Route::get('/lay-tag-khach-hang', 'settingSendEmailController@showContact')->name('show-tag-customer');
    Route::get('/lay-ten-khach-hang', 'ContactController@showContactName')->name('show-name-customer');
    Route::post('update-sendMail/{id}', 'settingSendEmailController@update')->name('update_mail');
    Route::get('delete-sendMail/{id}', 'settingSendEmailController@destroy')->name('destroy_mail');
    // datatable
    Route::get('e-show', 'settingSendEmailController@anyDatatables')->name('datatable_email');

    //Lấy nội dung template Email theo id 
    Route::get('get-template', 'EmailTemplateController@getTemplate')->name('get_template');

    //Mail automation 
    Route::get('setting-Email-Customer', 'settingCustomerController@index')->name('settingCustomer');
    Route::post('addMail', 'settingCustomerController@store')->name('add_mail_customer');
    Route::post('updateMail/{id}', 'settingCustomerController@update')->name('update_mail_customer');
    Route::get('deleteMail/{id}', 'settingCustomerController@delete')->name('delete_mail_customer');


    //Quản lý khách hàng giới thiệu 
    Route::get('list-customer-introduce', 'CustomerIntroduceController@index')->name('list_customer_introduce');
    Route::get('detail-customer-introduce/{id}', 'CustomerIntroduceController@show')->name('detail_customer_introduce');
    Route::get('change-status-introduce', 'CustomerIntroduceController@changeIntroduce')->name('change_introduce');
    Route::get('detail-customer-introduce', 'CustomerIntroduceController@showWithUser')->name('detail_customer');
    Route::get('domain-customer-introduce', 'CustomerIntroduceController@anyDatatables')->name('domain_invite_customer');

    //Quản lý thông tin tài khoản ngân hàng
    Route::post('add-payment-infomation', 'CustomerIntroduceController@addPayment')->name('add_payment_infomation');
    Route::post('edit-payment-infomation', 'CustomerIntroduceController@editPayment')->name('edit_payment_infomation');
    Route::get('submit-money', 'CustomerIntroduceController@submitMoney')->name('submit_money');

    Route::get('showAddSettingCustomerEmail', 'settingCustomerController@create')->name('showAddSettingCustomer');

    //khách hàng yêu cầu rút tiền 
    Route::get('request-money', 'RequestMoneyController@index')->name('request_money');
    Route::get('confirm-request-money/{id}/{money}', 'RequestMoneyController@confirm')->name('confirm_request_money');
    Route::get('destroy-request-money/{id}', 'RequestMoneyController@destroy')->name('destroy_request_money');

    //Cơ hội khách hàng 
    Route::resource('campaign-customer','CampaignCustomerController');
    Route::get('databtable-campaign-customer', 'CampaignCustomerController@anyDatatables')->name('databtable_campaign_customer');
    Route::post('change-process', 'CampaignCustomerController@changeProcess')->name('change_process');
    Route::post('/tai-len-khach-hang-chien-dich', 'CampaignCustomerController@uploadExcel')->name('import-customer-excel-campaign');

    Route::get('add-mail-automation/{id}', 'CampaignCustomerController@addMailAutomation')->name('add_mail_automation');
    Route::get('list-mail-automation', 'CampaignCustomerController@listMailAutomation')->name('list_mail_automation');
    Route::post('store-mail-automation', 'CampaignCustomerController@storeMailAutomation')->name('store_mail_automation');
    Route::post('edit-mail-automation/{id}', 'CampaignCustomerController@editMailAutomation')->name('edit_mail_automation');
    Route::get('remove-mail-automation/{id}', 'CampaignCustomerController@removeMailAutomation')->name('remove_mail_automation');
    Route::get('databtable-email-campaign-customer/{id}', 'CampaignCustomerController@datatableCampaign')->name('databtable_email_campaign_customer');

    //optin form
    Route::resource('optin-form','OptinFormController');

    //Update Canban Chiến dịch 
    Route::post('update-campaign-status', 'CampaignCustomerController@updateCampaignStatus')->name('update_campaign_status');

    //lọc danh sách khách hàng theo thời gian 
    Route::post('filter-time-campaign', 'CampaignCustomerController@filterTimeCampaign')->name('filter_time_campaign');

    //update khách hàng theo cơ hội
    Route::post('create-contact-opportunity', 'CampaignCustomerController@storeContact')->name('create_contact_opportunity');

    //Cơ hội khách hàng 
    Route::resource('opportunity','OpportunityController');

    // Xem mã giới thiệu 
    Route::get('show-gift', 'settingCustomerController@showGift')->name('show_gift');

    // THêm user quản trị website
    Route::get('show-add-user','AddUserDomaninController@showAddUser')->name('show_add_user');

    Route::post('add-user','AddUserDomaninController@addUser')->name('add_user');

    Route::get('edit-user-add','AddUserDomaninController@editUser')->name('edit_user_add');

    // Gửi redirect sang trang web khác
    Route::get('redirect/{encrypt}','AddUserDomaninController@redirectUser')->name('redirect_user');

    //chiến dịch 
    Route::get('settingCampaign', 'SettingCampaignController@index')->name('SettingCampaign');
    Route::get('showAddCampaign', 'SettingCampaignController@showAdd')->name('showAddCampaign');
    Route::post('addCampaign', 'SettingCampaignController@store')->name('addCampaign');

    Route::get('Campaign_table', 'SettingCampaignController@anyDatatables')->name('datatable_campaign');
    Route::get('editCampaign/{id}', 'SettingCampaignController@edit')->name('edit_campaign');
    Route::post('updateCampaign/{id}', 'SettingCampaignController@update')->name('update_campaign');
    Route::get('deleteCampaign/{id}', 'SettingCampaignController@delete')->name('delete_campaign');

    Route::resource('comments', 'CommentController');
    Route::get('comments-show', 'CommentController@anyDatatables')->name('datatable_comment');
    Route::post('comments-random', 'CommentController@randomCommentFromForm')->name('randomCommentFromForm');
    Route::get('comments-random', 'CommentController@randomComment')->name('randomComment');

    Route::resource('contact', 'ContactController');
    Route::get('contact-status', 'ContactController@updateStatus')->name('updateContactStatus');
    Route::get('/tim-kiem-khach-hang/search-query','ContactController@searchCustomer')->name('search_customer');
    Route::get('note-contact', 'ContactController@noteContact')->name('noteContact');
    Route::get('remind-contact', 'ContactController@remindContact')->name('remindContact');
    Route::get('update-remind-contact', 'ContactController@updateRemindContact')->name('updateRemindContact');
    Route::get('update-order-contact', 'ContactController@orderContact')->name('orderContact');
    Route::get('delete-order-contact/{orderContactId}', 'ContactController@deleteOrderContact')->name('deleteOrderContact');
    Route::post('store-contact', 'ContactController@storeContactEveryWhere')->name('storeContactEveryWhere');
    Route::get('/danh-sach-khach-hang', 'ContactController@contact')->name('contact_list');
    Route::get('/contact-anydatatable', 'ContactController@anyDatatable')->name('contact_datatable');
    Route::post('/contact-delete-all', 'ContactController@deleteAll')->name('contact_delete_all');
    Route::post('/minus-money-contact', 'ContactController@minusMoney')->name('minus_money');

    // filter time ajax
    Route::post('filter-contact-for-time', 'ContactController@filterContactForTime')->name('filterContactForTime');

    Route::resource('products', 'ProductController');
    Route::get('p-show', 'ProductController@anyDatatables')->name('datatable_product');
    Route::get('export-products', 'ProductController@exportToExcel')->name('exportProducts');
    Route::post('import-products', 'ProductController@importToExcel')->name('importProducts');
    Route::post('get-product-getfly', 'ProductController@getProductGetfly')->name('getProductGetfly');
	Route::post('products-delete-all', 'ProductController@deleteAll')->name('product_delete_all');

    Route::resource('templates', 'TemplateController');
    Route::resource('type-information', 'TypeInformationController');
    Route::resource('type-input', 'TypeInputController');
    Route::resource('type-sub-post', 'TypeSubPostController');
    Route::resource('categories', 'CategoryController');
    Route::resource('category-products', 'CategoryProductController');
    Route::get('category-products-datatable', 'CategoryProductController@anyDatatables')->name('category_product');
	
    Route::get('get-cate-product-getfly', 'CategoryProductController@getCateProductGetfly')->name('getCateProductGetfly');

    Route::group(['prefix' => '{typePost}'], function($typePost){
        // Files
        Route::resource('sub-posts', 'SubPostController');
        Route::get("showDocument/{idSubPost}","SubPostController@showDocument");
        Route::get('sub-posts-show', 'SubPostController@anyDatatables')->name('datatable_sub_posts');
    });

    Route::resource('information', 'InformationController', ['only' => [
        'index', 'store'
    ]]);
    Route::post('information/update', 'InformationController@updateInformation')->name('information-update');

    Route::get('information/general', 'InformationController@generalCreate')->name('information-general');
    Route::post('information/general-store', 'InformationController@generalStore')->name('information-store');

    Route::resource('menus', 'MenuController');
    Route::resource('users', 'UserController');
    Route::get('/check-task', 'UserController@checkTask')->name('check_task');

    Route::resource('subcribe-email', 'SubcribeEmailController');
    Route::get('subcribe-email-anyDatabase', 'SubcribeEmailController@anyDatatables')->name('subcribe-email-data');
    Route::post('group-mail/create', 'GroupMailController@store')->name('group_mail.create');
    Route::delete('group-mail/{groupMail}', 'GroupMailController@destroy')->name('group_mail.destroy');
    Route::post('send-mail', 'SubcribeEmailController@send')->name('subcribe-email_send');
    Route::post('send-mail-for-tag', 'SubcribeEmailController@sendForTag')->name('send_for_tag');
    Route::post('send-mail-for-campaign', 'SubcribeEmailController@sendForCampaign')->name('send_for_campaign');


    Route::get('hinh-thuc-thanh-toan', 'SettingController@setting')->name('method_payment');
    Route::post('cap-nhat-ngan-hang', 'SettingController@updateBank')->name('bank');
    Route::delete('quan-li-ngan-hang/{orderBanks}', 'SettingController@deleteBank')->name('orderBanks.destroy');
    Route::post('cap-nhat-ma-giam-gia', 'SettingController@updateCodeSale')->name('code_sale');
    Route::delete('quan-li-giam-gia/{orderCodeSales}', 'SettingController@deleteCodeSale')->name('orderCodeSales.destroy');

    Route::post('cap-nhat-phi-ship', 'SettingController@updateShip')->name('cost_ship');
    Route::post('cap-nhat-tinh-diem', 'SettingController@updateSetting')->name('updateSetting');
    Route::delete('quan-li-phi-ship/{orderShips}', 'SettingController@deleteShip')->name('orderShips.destroy');

    Route::post('cai-dat-getfly', 'SettingController@updateSettingGetFly')->name('updateSettingGetFly');
    Route::post('cau-hinh-email', 'SettingController@updateSettingEmail')->name('updateSettingEmail');
    Route::post('kiem-tra-cau-hinh', 'SettingController@testEmail')->name('testEmail');

    Route::post('cai-dat-mau', 'SettingController@updateColor')->name('update_color');

    Route::get('tom-luoc-thanh-toan', 'SettingController@billingSummary')->name('bill_summary');

    Route::get('don-hang', 'OrderController@listOrder')->name('orderAdmin');
    Route::get('export-exel','OrderController@exportToExcel')->name('exportToExcel');
    Route::delete('don-hang/{order}', 'OrderController@deleteOrder')->name('orderAdmin.destroy');
    Route::get('show-don-hang/{order}', 'OrderController@showOrder')->name('orderAdmin.show');

    Route::post('cap-nhat-trang-thai-don-hang', 'OrderController@updateStatusOrder')->name('orderUpdateStatus');

    Route::get('thong-bao', 'ReportController@allreport')->name('report');
    Route::get('da-xem-thong-bao', 'ReportController@seenNotification')->name('seenNotification');
    Route::get('da-doc-thong-bao', 'ReportController@readNotification')->name('readNotification');
    Route::get('thong-bao-day', 'ReportController@pushNotification')->name('pushNotify');

    // báo cáo nhân viên
    Route::get('bao-cao-nhan-vien', 'ReportController@reportEmployee')->name('report_employee');
    Route::get('bao-cao-doanh-so', 'ReportController@revenue')->name('report_revenue_employee');

    // quan ly domain and theme
    Route::resource('domains', 'DomainController');
    Route::get('domain-show', 'DomainController@anyDatatables')->name('datatable_domain');
    Route::get('domain-show-filter/{sub}', 'DomainController@anyDatatablesFilter')->name('datatable_domain_filter');
    Route::post('domain-delete-all', 'DomainController@deleteAll')->name('domain_delete_all');

    // Quản lý quảng cáo 
    Route::resource('advertisement', 'AdvertisementController');
    Route::get('advertisement-show', 'AdvertisementController@anyDatatables')->name('datatable_advertisement');

    Route::get('update-timeOn', 'DomainController@updateTimeOn')->name('update_time');
    Route::get('filter-domain/{sub}', 'DomainController@filterDomain')->name('filter_domain');
    Route::get('count-domain/{type}', 'DomainController@countFilterDomain')->name('count_domain');
    Route::get('export-domain/{sub}', 'DomainController@exportDomain')->name('export_domain');

    Route::resource('themes', 'ThemeController');
    Route::get('theme-show', 'ThemeController@anyDatatables')->name('datatable_theme');
    Route::get('active-theme', 'ThemeWebsiteController@activeTheme')->name('active_theme');
    Route::post('change-theme', 'ThemeWebsiteController@changeTheme')->name('change_theme');

    // thay đổi url website
    Route::get('thay-doi-url', 'SettingController@showChangeUrl')->name('show_change_url');
    Route::post('thay-doi-url', 'SettingController@changeUrl')->name('change_url');

    //marketing
    Route::get('email-marketing', 'MarketingController@index')->name('email_marketing');
    Route::get('sms-marketing', 'MarketingController@index')->name('sms_marketing');
    Route::get('automation-marketing', 'MarketingController@index')->name('automation_marketing');
    Route::get('telesale-marketing', 'MarketingController@index')->name('telesale_marketing');
    Route::get('google-adsword', 'MarketingController@index')->name('google_adsword');
    Route::get('google-shopping', 'MarketingController@index')->name('google_shopping');
    Route::get('seo-marketing', 'MarketingController@index')->name('seo_marketing');
    Route::get('gmap-marketing', 'MarketingController@index')->name('gmap_marketing');
    Route::get('affiliate', 'MarketingController@affliate')->name('affiliate');

    Route::get('moma-marketing', 'MarketingController@momaMarketing')->name('moma_marketing');
    Route::post('payment-ads-moma', 'MarketingController@paymentAdsMoma')->name('payment_moma_ads');

    Route::get('facebook-marketing', 'MarketingController@index')->name('facebook_marketing');
    Route::get('app-mobile-marketing', 'MarketingController@index')->name('app_mobile_marketing');
	Route::get('crm', 'MarketingController@index')->name('crm');

    Route::post('add-affiliate-moma', 'MarketingController@storeAffiliate')->name('add_affiliate_moma');

    Route::resource('group-theme', 'GroupThemeController');
    Route::get('group-theme-show', 'GroupThemeController@anyDatatables')->name('datatable_group_theme');

    Route::resource('group-help-video', 'GroupHelpVideoController');
    Route::get('group-help-video-show', 'GroupHelpVideoController@anyDatatables')->name('datatable_group_help_video');

    Route::resource('help-video', 'HelpVideoController');
    Route::get('help-video-show', 'HelpVideoController@anyDatatables')->name('datatable_help_video');

    Route::post('upload-fanpage', 'PostFanpageController@uploadFanpage')->name('upload_fanpage');
    Route::get('comment-facebook', 'CommentFacebookController@pushComment')->name('comment_facebook');
	
	//lấy bài viêt fanpage
	Route::get('/get-post-fanpage/{page_id}', 'PostFanpageController@getPostFanpage')->name('get_post_fanpage');
	//lưu bài viết
	Route::post('/save-post-fanpage', 'PostFanpageController@savePostFanpage')->name('save_post_fanpage');
	

    Route::post('update-groups', 'FanpageController@updateGroups')->name('update_groups');
    Route::post('update-face-id', 'FanpageController@updateFaceIds')->name('update_facebook_id');
    Route::get('delete-face-id', 'FanpageController@deleteFaceIds')->name('delete_facebook_id');
    Route::post('update-setting-face', 'FanpageController@updateSetting')->name('update_setting_face');
    Route::get('get-post-facebook', 'FanpageController@index')->name('get_post_facebook');

    //Mẫu email
    Route::resource('group-template', 'EmailTemplateController');
    Route::get('remove-template/{id}', 'EmailTemplateController@delete')->name('remove_template');
    Route::get('table-template-email', 'EmailTemplateController@anyDatatables')->name('datatable_template_email');

    Route::get('get-uid', 'FacebookUidController@getUid')->name('get_uid');
    Route::get('show-get-uid', 'FacebookUidController@showGetUid')->name('show_get_uid');

    Route::get('show-convert-uid', 'FacebookConvertController@showConvertFromUid')->name('show_convert_uid');
    Route::get('convert-uid', 'FacebookConvertController@convertFromUid')->name('convert_uid');
    Route::post('get-uid-from-excel', 'FacebookConvertController@getUidFromExcel')->name('get_uid_from_excel');

    Route::get('show-request-friend', 'FacebookConvertController@showRequestFriend')->name('show_request_friend');
    Route::get('request-friend', 'FacebookConvertController@requestFriend')->name('request_friend');

	Route::get('show-getfly', 'SettingController@showGetfly')->name('show_getfly');
	Route::get('show-facebook', 'SettingController@showFacebook')->name('show_facebook');
	Route::get('show-zalo', 'SettingController@showZalo')->name('show_zalo');
    Route::get('view-website', 'SettingController@viewWebsite')->name('view_website');

    Route::get('download-member', 'FacebookUidController@showMembers')->name('download-member');

    Route::get('/nang-cap-tai-khoan', 'UpgradeController@upgrade')->name('upgrade');
    Route::post('/submit-nang-cap-tai-khoan', 'UpgradeController@submitUpgradeVip')->name('submit_upgrade');
	
	Route::post('/submit-nang-cap-email', 'UpgradeController@submitUpgradeEmail')->name('submit_upgrade_email');

    Route::get('/mua-them-dung-luong', 'UpgradeController@buyMore')->name('buy_more');
    Route::post('/submit-mua-them-dung-luong', 'UpgradeController@submitBuyMore')->name('submit_buy_more');
    Route::get('/thanh-toan-qua-vi-dien-tu','UpgradeController@indexVnpay')->name('thong-tin-thanh-toan-vi-dien-tu');
    Route::post('/thanh-toan-qua-vi-dien-tu','UpgradeController@createPayment')->name('create_payment');

    Route::get('/thong-tin-thanh-toan-vi-dien-tu', 'UpgradeController@returnVnpay');

    Route::get('update-status-help-got-it', 'UserController@updateStatusHelpGotIt')->name('update_status_help');
    Route::get('update-enable-help', 'UserController@updateEnableHelp')->name('update_enable_help');

    // thay đổi tên miền
    Route::get('thay-doi-domain', 'ChangeDomainController@index')->name('change-domain');
    Route::post('thay-doi-domain', 'ChangeDomainController@changeDomain')->name('post_change_domain');

        

    // communicate cộng đồng
    Route::get('kenh-ban-hang', 'CommunicateController@index')->name('communicate.index');
    Route::get('kenh-ban-hang-google-ads', 'CommunicateController@googleAds')->name('communicate.google_ads');
    Route::get('kenh-ban-hang-sendo', 'CommunicateController@sendo')->name('communicate.sendo');
    Route::get('kenh-ban-hang-tiki', 'CommunicateController@tiki')->name('communicate.tiki');
    Route::get('kenh-ban-hang-voso', 'CommunicateController@voso')->name('communicate.voso');
    Route::get('kenh-ban-hang-shopee', 'CommunicateController@shopee')->name('communicate.shopee');
    Route::get('kenh-ban-hang-lazada', 'CommunicateController@lazada')->name('communicate.lazada');
    Route::get('kenh-ban-hang-muare', 'CommunicateController@muare')->name('communicate.muare');
    Route::get('kenh-ban-hang-muaban', 'CommunicateController@muaban')->name('communicate.muaban');
    Route::get('kenh-ban-hang-enbac', 'CommunicateController@enbac')->name('communicate.enbac');
    Route::get('kenh-ban-hang-raovatvietnamnet', 'CommunicateController@raovatvietnamnet')->name('communicate.raovatvietnamnet');
    Route::get('kenh-ban-hang-chophien', 'CommunicateController@chophien')->name('communicate.chophien');
    Route::get('kenh-ban-hang-ndfloodinfo', 'CommunicateController@ndfloodinfo')->name('communicate.ndfloodinfo');
    Route::get('kenh-ban-hang-muabanraovat', 'CommunicateController@muabanraovat')->name('communicate.muabanraovat');
    Route::get('phan-tich-doi-thu', 'CommunicateController@analytics')->name('communicate.analytics');

	// tiện ích
	Route::get('tien-ich', 'UtilitiesController@index')->name('utilities.index');
	
    // cấu hình kênh vận chuyển
	Route::get('quan-ly-van-chuyen', 'TransportController@index')->name('transport.index');
    Route::get('kenh-giao-hang-tiet-kiem', 'TransportController@giaohangtietkiem')->name('transport.giaohangtietkiem');
    Route::get('kenh-viettel-port', 'TransportController@viettel')->name('transport.viettel');
    Route::get('kenh-giao-hang-nhanh', 'TransportController@giaohangnhanh')->name('transport.giaohangnhanh');

    // quản lý fanpage facebook
    Route::get('cau-hinh-facebook', 'FacebookManagerController@index')->name('facebook_manager.index');
    Route::post('cau-hinh-facebook', 'FacebookManagerController@config')->name('facebook_manager.config');
    Route::get('fanpage-facebook/{slug}', 'FacebookManagerController@detailFanpage')->name('facebook_manager.detailFanpage');

    //Update mail cho user
    Route::get('update-email', 'UpdateMailController@index')->name('mail_user');
    Route::get('find-user', 'UpdateMailController@showUserAjax')->name('find_user');
    Route::get('edit-email-user/{id}', 'UpdateMailController@edit')->name('edit_user_mail');
    Route::post('update-email-user', 'UpdateMailController@update')->name('update_user_mail');

    // cấu hình giao diện trong admin.
    Route::get('/cau-hinh-giao-dien', 'ConfigTemplateController@index')->name('config_template');
    Route::get('/cau-hinh-dau-trang', 'ConfigTemplateController@header')->name('config_header');
    Route::get('/cau-hinh-trang-chu', 'ConfigTemplateController@home')->name('config_home');
    Route::get('/cau-hinh-danh-muc-san-pham', 'ConfigTemplateController@categoryProduct')->name('config_category_product');
    Route::get('/cau-hinh-chi-tiet-san-pham', 'ConfigTemplateController@detailProduct')->name('config_detail_product');
    Route::get('/cau-hinh-danh-muc-bai-viet', 'ConfigTemplateController@category')->name('config_category');
    Route::get('/cau-hinh-chi-tiet-bai-viet', 'ConfigTemplateController@single')->name('config_single');
    Route::get('/cau-hinh-thong-tin-tu-van', 'ConfigTemplateController@advice')->name('config_advice');
    Route::get('/cau-hinh-lien-he', 'ConfigTemplateController@contact')->name('config_contact');
    Route::get('/cau-hinh-footer', 'ConfigTemplateController@footer')->name('config_footer');
    Route::get('/tra-gop','ConfigTemplateController@interestRate')->name('interest_rate');
    Route::post('/change-advice', 'ConfigTemplateController@changeStatusAdivce')->name('change_status_adivce');
	Route::resource('cskhzalo', 'ZaloController');
	Route::get('/quet-qr-code', 'ZaloController@scanQrCode');
	Route::get('/dang-nhap-code', 'ZaloController@loginQrCode');
	Route::get('/luu-token-zalo', 'ZaloController@saveToken');

    Route::post('/add-customer-advice-display', 'ConfigTemplateController@addCustomerAdivce')->name('add_customer_adivce');
    Route::post('/edit-customer-advice-display', 'ConfigTemplateController@editCustomerAdivce')->name('edit_customer_adivce');

	Route::get('cham-soc-zalo', 'ZaloController@index')->name('zalo_cskh');
	Route::get('/do-luong-quang-cao-facebook','AnalysisFacebookController@index')->name('analysis_facebook');

	// Export khách hàng ra file Excel
    Route::get('/xuat-khach-hang-ra-excel', 'ContactController@exportExcel')->name('export-customer-excel');

    // Import khách hàng từ file Excel
    Route::post('/tai-len-danh-sach-khach-hang', 'ContactController@uploadExcel')->name('import-customer-excel');

    Route::get('/call-Api-Ccall', 'CCallController@callCDR')->name('call-Api-Ccall');

    Route::post('/thong-ke-email-cua-ban', 'sendEmailController@getDataEmailByTime')->name('email-statistics');
	
	Route::get('/help', 'AdminController@help')->name('help');
	
	Route::post('/check-seo', 'SeoController@check')->name('check_chuan_seo');

    // cơ chế lương + thưởng
    Route::resource('commission', 'CommissionController');

    // kpi sale
    Route::resource('goals', 'GoalController');

    // đơn hàng với khách hàng
    Route::resource('order-customer', 'OrderCustomerController');
    Route::get('order-customer-payment', 'OrderCustomerController@payment')->name('order_customer_payment');

    // add product to order
    Route::get('/add-product-order', 'OrderCustomerController@addProductOrder')->name('add_product_order');

    Route::post('/note-domain', 'DomainController@note')->name('note_domain');

    Route::get('/create-qr-code', 'ContactController@qrCode')->name('create_qr_code');

    Route::get("/login-vpn-chat", 'UtilitiesController@loginVpnChat')->name('login_vpn_chat');

});

//dang nhap
Route::group(['prefix'=>'admin', 'namespace'=>'Admin' ],function(){
    Route::get('/','LoginController@showLoginForm');
    Route::get('login','LoginController@showLoginForm')->name('login');
    Route::post('login','LoginController@login')->name('login_moma');
    Route::get('logout','LoginController@logout');
    Route::post('logout','LoginController@logout')->name('logout');
    //reset password
    Route::get('password/reset','LoginController@getReset');
    Route::post('password/reset','LoginController@postReset');
    Route::get('create-new-password','LoginController@getCreateNewPassword');
    Route::post('create-new-password','LoginController@postCreateNewPassword');

});

Route::group([ 'namespace'=>'Site', 'middleware' => ['restrictIp']], function() {
    Route::get('mailcheck', 'ContactController@mailCheck')->name('main_check');
    //Route::get('/xuat-khau-lao-dong-nhat-ban/dang-ky-xuat-khau-lao-dong-nhat-ban', 'LaborExportController@index');
    //Route::post('/gui-dang-ky-xuat-khau-lao-dong-nhat-ban', 'LaborExportController@store')->name('labor_export');
    Route::get('/', 'HomeController@index')->name('home');
	Route::get('/view-website', 'HomeController@updateViewsWebsite')->name('views_website');
    Route::get('/hot-deal', 'ProductCategoryController@index');
    Route::get('/danh-muc/{cate_slug}', 'CategoryController@index')->name('site_category_post');
    Route::get('/cua-hang/{cate_slug}', 'ProductCategoryController@index')->name('site_category_product');
    Route::get('/tim-kiem', 'ProductCategoryController@search')->name('search_product');
    Route::get('/tim-kiem-ajax', 'ProductCategoryController@searchAjax')->name('search_product_ajax');
    Route::get('rating', 'ProductController@Rating')->name('rating');

    Route::get('/bo-sung/{sub_post_slug}', 'SubPostController@index');
	 Route::get('/infor-website', 'LoginController@getInforLogin')->name('infor_website');
	
    Route::get('/dang-ky','RegisterController@showRegistrationForm')->name('register');
    Route::post('/dang-ky','RegisterController@register');

    Route::post('/quen-mat-khau','PersonController@forgetPassword')->name('forget_password');
	Route::get('/dang-nhap-website','LoginController@loginWebsite')->name('login_website');
	
    Route::post('/dang-nhap','LoginController@login')->name('dang_nhap');
    Route::post('/dang-nhap-vn3c','LoginVn3cController@login');
    Route::get('dang-xuat','LoginController@logout')->name('logoutHome');
//    Route::post('dang-xuat','LoginController@logout')->name('logoutHome');
    Route::get('callbacklogin','LoginController@callbackLogin');
	
	//login facebook
	Route::get('/dang-nhap-mang-xa-hoi/{social}', 'SocialAuthController@redirect')->name('facebook_redirect');
	Route::get('/dang-nhap-thanh-cong-mang-xa-hoi/{social}', 'SocialAuthController@callback')->name('facebook_callback');
    Route::get('/dang-nhap-facebook', 'SocialAuthController@loginFacebook')->name('login_facebook');
    Route::get('/save-access-token', 'SocialAuthController@saveAccessTokenFacebook')->name('save_access_token');
    
	//calback site 
    Route::get('/callback', 'HomeController@callback')->name('callback_site');

    Route::get('login/google', 'LoginController@redirectToProvider')->name('google_login');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');

    Route::get('/thong-tin-ca-nhan','PersonController@index')->name('thong_tin_ca_nhan');
    Route::post('/thong-tin-ca-nhan','PersonController@store');
    Route::get('/doi-mat-khau','PersonController@resetPassword');
    Route::post('/doi-mat-khau','PersonController@storeResetPassword');
    Route::get('/don-hang-ca-nhan','PersonController@orderPerson')->name('orderPerson');

    Route::get('/lien-he','ContactController@index')->name('contact_home');
    Route::post('submit/contact','ContactController@submit')->name('sub_contact');
	Route::post('submit/contact_Marketing','ContactController@submitMarketing')->name('sub_contact_marketing');

    //contact moma
    Route::post('submit-optinform','ContactController@submitMoma')->name('sub_contact_moma');
    
    Route::post('/binh-luan', 'CommentController@index')->name('comment');
    Route::get('/xoa-binh-luan', 'CommentController@delete')->name('delete_comment');
    Route::get('/sua-binh-luan', 'CommentController@edit')->name('edit_comment');
    Route::get('/binh-luan-moi', 'CommentController@virtural')->name('virtural_comment');

    Route::get('loc-san-pham', 'ProductCategoryController@filter');
	
	//hoi dap 
	Route::get('hoi-dap', 'QuestionController@index')->name('question');
	Route::get('hoi-dap/{slug}', 'QuestionController@detail')->name('detail_question');
	Route::post('hoi-dap/loc', 'QuestionController@filter')->name('filter_question');
	
    //Tạo mã giới thiệu 
    Route::get('/gift/{id}', 'HomeController@setCookieGift');


    /*===== đặt hàng  ===== */
    Route::post('/dat-hang', 'OrderController@addToCart')->name('addToCart');
    Route::get('/gio-hang', 'OrderController@order')->name('order');
    Route::get('/xoa-don-hang', 'OrderController@deleteItemCart')->name('deleteItemCart');

    //Thêm đơn hàng xhome 
    Route::post('/dat-hang-xhome', 'OrderController@insertOrderXhome')->name('insert_order_xhome');


    Route::post('/gui-don-hang', 'OrderController@send')->name('send');
    Route::get('/thong-tin-thanh-toan-vnpay', 'ContactController@infoTransactionVnPay');
    Route::get('/cap-nhat-thong-tin-thanh-toan-vnpay', 'ContactController@updateTransactionVnPay');
    /*===== subcribe email   ===== */
    Route::post('subcribe-email', 'SubcribeEmailController@index')->name('subcribe_email');

    // xác minh bằng số điện thoại và kiếm tiền
    Route::get('/xac-minh-so-dien-thoai', 'ContactController@affiliatePhone')->name('affiliate_phone');
    Route::get('/kiem-tien', 'ContactController@affiliateCheckPhone')->name('affiliate_check_phone');
    Route::get('tra-cuu','ContactController@searchAffiliate')->name('search_affiliate_form_phone');

    // tạo mới website
    Route::get('tao-moi-website', 'ThemeController@storeAccountTheme')->name('create-theme');
    Route::post('check-mail-website', 'ThemeController@checkMailCreateTheme')->name('check-website');

    Route::get('sitemap.xml', 'SitemapsController@index');
    Route::get('trang/{post_slug}', 'PageController@index')->name('page');

    Route::get('tao-moi-website-thanh-cong', 'ThemeController@checkMailCreateTheme')->name('website_success');
	
    Route::get('view/{post_slug}', 'ProductController@demoProduct')->name('productDemo');
	
    Route::get('{cate_slug}/{post_slug}', 'PostController@index')->name('post');

    Route::get('{post_slug}', 'ProductController@index')->name('product');
	
});
