<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace'=>'Api'], function (){
    Route::get('danh-sach-san-pham', 'ProductController@getProductsFromDomain');
    Route::get('danh-sach-bai-viet', 'ProductController@getPostsFromDomain');
    Route::get('thong-tin-website', 'UserController@getInforDomain');
    Route::post('quen-mat-khau', 'UserController@forgetPass');
    Route::get('tim-kiem-website', 'DomainController@searchDomain');
    Route::post('them-danh-muc', 'CategoryController@createCategory');
});
Route::post('login', 'Api\LoginController@index');
Route::get('info-user', 'Api\LoginController@getAuthUser');

Route::group(['prefix' => 'product'] ,function (){
    Route::get('/', 'Api\ProductController@index');
    Route::get('create', 'Api\ProductController@create');
    Route::post('store', 'Api\ProductController@store');
    Route::post('edit/{product_id}', 'Api\ProductController@edit');
    Route::post('update/{product_id}', 'Api\ProductController@update');
    Route::get('delete/{product_id}', 'Api\ProductController@destroy');

});

Route::group(['prefix' => 'order'],function (){
    Route::get('/', 'Api\OrderController@index');
    Route::post('store', 'Api\OrderController@store');
});

Route::group(['prefix'=>'contact'] ,function (){
    Route::get('/', 'Api\ContactController@index');
    Route::post('store', 'Api\ContactController@store');
    Route::get('edit/{contact_id}', 'Api\ContactController@edit');
    Route::post('update/{contact_id}', 'Api\ContactController@update');
    Route::get('delete/{contact_id}', 'Api\ContactController@destroy');
});

Route::get('user', 'Api\UserController@index');
Route::post('update-user', 'Api\UserController@updateUser');

Route::get('get-campaign-getfly', 'Api\GetflyController@getCampaignGetfly');

Route::post('add-campaign-getfly', 'Api\GetflyController@addCampaignGetfly');

Route::post('create-theme', 'Api\ThemeController@index');

